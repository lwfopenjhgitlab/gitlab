---
stage: Fulfillment
group: Purchase
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: index, reference
---

# 订阅极狐GitLab **(PREMIUM)**

极狐GitLab 提供不同订阅级别的功能。您的订阅决定您可以访问哪个级别的功能。订阅有效期为 12 个月。

极狐GitLab 在以下方面为参与者提供订阅：

- [教育](#gitlab-for-education-subscriptions)
- [开源](#gitlab-for-open-source-subscriptions)

## 选择极狐GitLab 订阅级别

当选择订阅级别时，您需要考虑以下两个因素：

- [极狐GitLab SaaS 版或私有化部署版](#choose-between-gitlab-saas-or-gitlab-self-managed)
- [极狐GitLab 版本方案](#choose-a-gitlab-tier)

<a id="choose-between-gitlab-saas-or-gitlab-self-managed"></a>

### 选择 SaaS 版或私有化部署版

订阅的应用方式有所不同，具体取决于您使用 SaaS 版还是私有化部署版：

- [极狐GitLab SaaS 版](jihulab_com/index.md): 一种软件即服务产品。要使用极狐GitLab SaaS 版，您不需要执行任何安装操作，只需要[注册](https://jihulab.com/users/sign_up)并直接开始使用即可。
- [极狐GitLab 私有化部署版](self_managed/index.md): 安装、管理并维护您自己的极狐GitLab 实例。

在极狐GitLab 私有化部署版本实例上，极狐GitLab 订阅为*所有*用户提供一组相同的功能。在极狐GitLab SaaS 版上，您可以为群组或个人命名空间申请订阅。

NOTE:
订阅无法在 SaaS 版和私有化部署版之间互转。必要时应申请并使用一个新的订阅。

<a id="choose-a-gitlab-tier"></a>

### 选择版本方案

您的订阅定价基于[版本方案](https://about.gitlab.cn/pricing/)，这样您可以选择适合您预算的特性。

<!--更多关于每个版本包含的特性信息，参考以下内容：

- [极狐GitLab SaaS 版本特性对比](https://about.gitlab.cn/pricing/gitlab-com/feature-comparison/)
- [极狐GitLab 自助管理版本特性对比](https://about.gitlab.cn/pricing/self-managed/feature-comparison/)
-->

## 找到您的订阅

下图可以帮助您确定您的订阅模式。选择列表项转到相应的帮助页面。

```mermaid
graph TD

A(您的用户账户在 JiHuLab.com 上吗?)
A --> B(是)
A --> C(否)
B --> D(fa:fa-link 在 JiHuLab.com 上查看订阅)
C --> E(fa:fa-link 查看您的私有化部署版本的订阅)

click D "./jihulab_com/index.html#view-your-gitlabcom-subscription"
click E "./self_managed/index.html#view-your-subscription"
```

## 极狐订单管理中心

在[极狐订单管理中心](https://customers.jihulab.com/)，您可以：

- [更改您的个人信息](#change-your-personal-details)
- [更改您的公司详情](#change-your-company-details)
- [更改关联账户](#change-the-linked-account)
- [更改订单管理中心账户的密码](#change-customers-portal-account-password)
<!--
- [Change your payment method](#change-your-payment-method)
- [Change the namespace the subscription is linked to](#change-the-linked-namespace)
-->

<a id="change-your-personal-details"></a>

### 更改您的个人信息

您的个人详细信息在开发票时使用。您的电子邮件地址用于登录极狐订单管理中心并处理许可证相关事宜。

要更改您的个人详细信息，包括姓名和电子邮件地址：

1. 登录[极狐订单管理中心](https://customers.jihulab.com/customers/sign_in)。
1. 选择 **我的账户 > 账户详情**。
1. 展开 **您的个人信息** 部分。
1. 编辑您的个人信息。
1. 选择 **保存更改**。

<a id="change-your-company-details"></a>

### 更改您的公司详情

要更改您的公司详细信息：

1. 登录[极狐订单管理中心](https://customers.jihulab.com/customers/sign_in)。
1. 选择 **我的账户 > 账户详情**。
1. 展开 **您的公司详情** 部分。
1. 编辑公司资料。
1. 选择 **保存更改**。

<!--### Change your payment method

Purchases in the Customers Portal require a credit card on record as a payment method. You can add
multiple credit cards to your account, so that purchases for different products are charged to the
correct card.

If you would like to use an alternative method to pay, please [contact our Sales
team](https://about.gitlab.com/sales/).

To change your payment method:

1. Log in to the [Customers Portal](https://customers.gitlab.com/customers/sign_in).
1. Select **My account > Payment methods**.
1. **Edit** an existing payment method's information or **Add new payment method**.
1. Click **Save Changes**.

#### Set a default payment method

Automatic renewal of a subscription is charged to your default payment method. To mark a payment
method as the default:

1. Log in to the [Customers Portal](https://customers.gitlab.com/customers/sign_in).
1. Select **My account > Payment methods**.
1. **Edit** the selected payment method and check the **Make default payment method** checkbox.
1. Click **Save Changes**.

-->

<a id="change-the-linked-account"></a>

### 更改关联账户

更改与您的极狐订单管理中心账户相关联的 JiHuLab.com 账户：

1. 登录[极狐订单管理中心](https://customers.jihulab.com/customers/sign_in)。
1. 在单独的浏览器选项卡中，访问 [JiHuLab SaaS](https://jihulab.com) 并确保您没有登录。
1. 在极狐订单管理中心页面上，单击 **我的账户 > 账户详情**。
1. 在 **您的极狐GitLab 账户** 下，点击 **更改关联账户**。
1. 登录您要关联到极狐订单管理中心的 [JiHuLab SaaS](https://gitlab.com) 账户。

<!--

### Change the linked namespace

To change the namespace linked to a subscription:

1. Log in to the [Customers Portal](https://customers.gitlab.com/customers/sign_in) with a
   [linked](#change-the-linked-account) GitLab SaaS account.
1. Navigate to the **Manage Purchases** page.
1. Select **Change linked namespace**.
1. Select the desired group from the **This subscription is for** dropdown.
1. Select **Proceed to checkout**.

Subscription charges are calculated based on the total number of users in a group, including its subgroups and nested projects. If the total number of users exceeds the number of seats in your subscription, your account is charged for the additional users.-->

### 更改极狐订单管理中心账户的密码

更改此订单管理中心账户的密码：

1. 登录[极狐订单管理中心](https://customers.jihulab.com/customers/sign_in)。
1. 选择 **我的账户** 下拉菜单并点击 **账户详情**。
1. 对 **您的密码** 部分进行必要的更改。
1. 单击 **保存更改**。

## 社区项目订阅

### 极狐GitLab 教育公益计划

极狐GitLab 教育许可证只能用于教学用途或非商业性学术研究。

如需了解更多有关如何申请和续订的信息，请访问[极狐GitLab 教育版](https://about.gitlab.cn/solutions/education/)。

<a id="gitlab-for-open-source"></a>

### 开源极狐GitLab

对于开源项目，[极狐GitLab 开源项目公益支持计划](https://about.gitlab.cn/solutions/open-source/)提供旗舰版本的功能和每月 50,000 的 CI/CD 分钟数。有关更多信息（包括申请该计划和延长计划成员资格的说明），请参阅[极狐GitLab 开源计划页面](https://about.gitlab.cn/solutions/open-source/)。<!--和[极狐GitLab handbook](https://about.gitlab.com/handbook/marketing/community-relations/community-programs/opensource-program/)-->

#### 满足极狐GitLab 开源项目的要求

为了满足极狐极狐GitLab 开源项目的要求，您需要首先将 OSI 批准的开源许可证添加到您命名空间中的所有项目。

向项目添加许可证：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在概览页面上，选择 **添加许可证**。如果您想要的许可证无法作为许可证模板使用，请手动将完整的、未更改的[所选许可证的文本](https://opensource.org/licenses/alphabetical)复制到 `LICENSE` 文件中。请注意，如果用户不执行此操作，极狐GitLab 默认 **保留所有权利**。

![Add license](img/add-license.png)

申请人必须向各自群组或命名空间中的每个项目添加正确的许可证。当您确定您的项目使用的是 OSI 批准的许可证时，您可以截取屏幕截图。

NOTE:
极狐GitLab 开源项目会惠及整个极狐GitLab 命名空间。要获得极狐GitLab 开源项目的资格，申请人命名空间中的所有项目都必须满足计划要求。申请者提交与申请命名空间中的一个项目相关的材料，开源项目团队使用该项目来验证整个命名空间的资格。

#### 开源项目验证

接下来，截取您项目的屏幕截图以确认该项目的资格。您必须上传三个屏幕截图：

- [OSI 批准的许可证概述](#screenshot-1-license-overview)
- [OSI 批准的许可证内容](#screenshot-2-license-contents)
- [公开可见的设置](#screenshot-3-publicly-visible-settings)

NOTE:
极狐GitLab 开源项目计划惠及极狐GitLab 命名空间中的所有项目，因此符合条件的命名空间中的所有项目都必须满足计划要求。

<a id="screenshot-1-license-overview"></a>

##### 截图一：许可证概述

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择您的项目头像。如果您没有为项目指定头像，则头像显示为单个字母。
1. 截取项目概览的屏幕截图，截图需要清楚地显示您为项目选择的许可证。

![License overview](img/license-overview.png)

##### 截图二：许可证内容

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **仓库** 并找到项目的 `LICENSE` 文件。
1. 对文件内容进行截图。确保屏幕截图包含许可证的标题。

![License file](img/license-file.png)

##### 截图三：公开可见的设置

要获得极狐GitLab 开源项目计划的资格，项目必须公开可见。要检查项目的公开可见性设置：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 从左侧边栏中，选择 **设置 > 通用**。
1. 展开 **可见性，项目功能，权限**。
1. 从 **项目可见性** 下拉列表中，选择 **公开**。
1. 选择 **用户可以请求访问** 复选框。
1. 截取此视图的屏幕截图。截图需要包括尽可能多的公开可见的设置，并确保在截图的左上角包含您的项目名称。

![Publicly visible setting](img/publicly-visible.png)

NOTE:
对于公开可见性的例外情况（例如，申请人名称空间中的项目可能包含敏感数据时），请将您用例的详细信息发送到 `opensource@gitlab.com` 以请求对例外情况的书面许可。

### 极狐GitLab 初创企业未来独角兽计划

对于初创企业，[极狐GitLab 初创企业未来独角兽计划](https://about.gitlab.cn/solutions/startups/)提供极狐GitLab 旗舰版的功能，和连续 12 个月每月 50,000 的 CI/CD 分钟数。有关更多信息（包括申请该计划和延长计划成员资格的说明），请参阅[极狐GitLab 初创企业未来独角兽计划](https://about.gitlab.cn/solutions/startups/)。<!-- and the [GitLab handbook](https://about.gitlab.com/handbook/marketing/community-relations/community-programs/startups-program/).-->

## 联系技术支持

通过我们的支持服务，您可以了解：

- [极狐GitLab 支持](https://about.gitlab.cn/support/)级别
<!-- [Submit a request via the Support Portal](https://support.gitlab.com/hc/en-us/requests/new).-->

我们还鼓励所有用户在我们的项目跟踪器中搜索[极狐GitLab 项目](https://jihulab.com/gitlab-cn/gitlab/-/issues)中的已知议题和现有的功能请求。

这些议题是获取特定产品计划更新和直接与相关极狐GitLab 团队成员沟通的最佳途径。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, for example `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
