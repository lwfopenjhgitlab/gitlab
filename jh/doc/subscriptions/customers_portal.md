---
stage: Fulfillment
group: Purchase
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 极狐订单管理中心

您可以使用极狐订单管理中心管理订阅和账户任务，例如购买额外的席位或存储空间以及管理发票。有关管理订阅的具体说明，请参阅以下页面：

- [极狐GitLab SaaS 订阅](jihulab_com/index.md)
- [极狐GitLab 私有化部署订阅](self_managed/index.md)

<!--如果您是通过授权经销商购买的，则必须直接联系他们以更改您的订阅（您的订阅是只读的）。-->

## 登录极狐订单管理中心

您可以使用 JiHuLab.com 账户或电子邮箱和密码登录极狐订单管理中心（如果您尚未[将您的极狐订单管理中心账户关联到 JiHuLab.com 账户](#link-a-gitlabcom-account)）。

NOTE:
如果您使用 JiHuLab.com 账户在极狐订单管理中心注册了账户，请使用此账户登录。

要使用您的 JiHuLab.com 账户登录极狐订单管理中心：

1. 导航到[极狐订单管理中心](https://customers.jihulab.com)。
1. 选择 **极狐GitLab 账户登录**。
1. 输入您的 JiHuLab.com 账户的用户名/邮箱/手机号和密码。
1. 选择 **登录**。

要使用您的电子邮箱和密码登录极狐订单管理中心：

1. 导航到[极狐订单管理中心](https://customers.jihulab.com)。
1. 输入您的 **电子邮箱** 和 **密码**。
1. 选择 **登录**。

## 更改账户所有者信息

账户所有者的个人详细信息用于开发票。<!--账户所有者的电子邮箱地址用于[客户门户旧版登录](#sign-in-to-customers-portal)和与许可证相关的电子邮箱。-->

要更改账户所有者的信息，包括姓名和电子邮箱地址：

1. 登录[极狐订单管理中心](https://customers.jihulab.com)。
1. 选择右上角的 **我的账户 > 账户详情**。
1. 选择 **您的个人信息** 右侧的 **展开**。
1. 编辑个人详细信息。
1. 选择 **保存更改**。

如果您想将极狐订单管理中心账户的所有权转移给其他人，在您输入该人的个人详细信息后，您还必须：

- [更改极狐订单管理中心账户的密码](#change-customers-portal-account-password)
- [更改关联的 JiHuLab.com 账户](#change-the-linked-account)（如果您已经关联）

## 更改您公司的详细信息

要更改您公司的详细信息，包括公司名称和增值税号：

1. 登录[极狐订单管理中心](https://customers.jihulab.com)。
1. 选择右上角的 **我的账户 > 账户详情**。
1. 选择 **您的公司详情** 右侧的 **展开**。
1. 编辑公司详细信息。
1. 选择 **保存更改**。

<!--## Change your payment method

Purchases in the Customers Portal require a credit card on record as a payment method. You can add
multiple credit cards to your account, so that purchases for different products are charged to the
correct card.

If you would like to use an alternative method to pay, please
[contact our Sales team](https://about.gitlab.com/sales/).

To change your payment method:

1. Sign in to the [Customers Portal](https://customers.gitlab.com/customers/sign_in).
1. Select **My account > Payment methods**.
1. **Edit** an existing payment method's information or **Add new payment method**.
1. Select **Save Changes**.

### Set a default payment method

Automatic renewal of a subscription is charged to your default payment method. To mark a payment
method as the default:

1. Sign in to the [Customers Portal](https://customers.gitlab.com/customers/sign_in).
1. Select **My account > Payment methods**.
1. **Edit** the selected payment method and check the **Make default payment method** checkbox.
1. Select **Save Changes**.-->

<a id="link-a-gitlabcom-account"></a>

## 关联 JiHuLab.com 账户

<!--如果您拥有旧版客户门户账户并使用电子邮箱和密码登录，请遵循此指南。-->
要将 JiHuLab.com 账户关联到您的极狐订单管理中心：

1. 访问 [JiHuLab.com](https://jihulab.com) 并以您想关联到极狐订单管理中心的账户的用户名/邮箱/手机号和密码进行登录。
1. 使用电子邮箱和密码登录[极狐订单管理中心](https://customers.jihulab.com)。
1. 选择右上角的 **我的账户 > 账户详情**。
1. 选择 **您的极狐GitLab 账户** 右侧的 **展开**。
1. 选择 **关联我的极狐GitLab 账户**，系统会自动将当前登录的 JiHuLab.com 账户与极狐订单管理中心账户进行关联。

<a id="change-the-linked-account"></a>

## 更改关联的 JiHuLab.com 账户

<!--Customers are required to use their GitLab.com account to register for a new Customers Portal account.

If you have a legacy Customers Portal account that is not linked to a GitLab.com account, you may still [sign in](https://customers.gitlab.com/customers/sign_in?legacy=true) using an email and password. However, you should [create](https://gitlab.com/users/sign_up) and [link a GitLab.com account](#change-the-linked-account) to ensure continued access to the Customers Portal.-->

要更改关联到您的极狐订单管理中心账户的 JiHuLab.com 账户：

1. 访问 [JiHuLab.com](https://jihulab.com) 并将账户切换到您想关联到极狐订单管理中心的账户。
1. 使用电子邮箱和密码登录[极狐订单管理中心](https://customers.jihulab.com)。
1. 选择右上角的 **我的账户 > 账户详情**。
1. 选择 **您的极狐GitLab 账户** 右侧的 **展开**。
1. 选择 **更改关联账户**，系统会自动将当前登录的 JiHuLab.com 账户与极狐订单管理中心账户进行关联。

<a id="change-customers-portal-account-password"></a>

## 更改极狐订单管理中心账户的密码

要更改极狐订单管理中心账户的密码：

1. 登录[极狐订单管理中心](https://customers.jihulab.com)。
1. 选择右上角的 **我的账户 > 账户详情**。
1. 选择 **您的密码** 右侧的 **展开**。
1. 输入 **当前密码**、**新密码** 和 **确认密码**。
1. 选择 **保存更改**。

<!--## Customers that purchased through a reseller

If you purchased a subscription through an authorized reseller (including GCP and AWS marketplaces), you have access to the Customers Portal to:

- View your subscription.
- Associate your subscription with the relevant group (GitLab SaaS) or download the license (GitLab self-managed).
- Manage contact information.

Other changes and requests must be done through the reseller, including:

- Changes to the subscription.
- Purchase of additional seats, Storage, or Compute.
- Requests for invoices, because those are issued by the reseller, not GitLab.

Resellers do not have access to the Customers Portal, or their customers' accounts.

After your subscription order is processed, you will receive several emails:

- A "Welcome to the Customers Portal" email, including instructions on how to log in.
- A purchase confirmation email with instructions on how to provision access.-->
