---
stage: Fulfillment
group: Purchase
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 季度对账和年度调整 **(PREMIUM)**

极狐GitLab 审核您的席位使用情况，并向您发送超额订单。

此审核发生在：

- **每年**。此流程也称为**年度调整**，要求您为一年中任何时间添加的用户支付全部年度订阅费。
- **每季度**。通过此流程，您只需为剩余的订阅期限付费。

## 季度对账与年度调整

季度对账可以节省大量资金。

假设您在一月份为 100 个用户购买了年度许可证。此图表显示您在一年中的使用情况。用户数有增有减，最大 120 人。深紫色表示您每个季度内拥有最大用户数的月份。

![License overview](img/quarterly_reconciliation.png)

如果您按年计费：

- 在这一年中，较许可证的席位数，您超过了 20 个用户。
- 如果我们假设每个额外的席位是 100 元，那么您支付 100 元 x 20 个用户。

年度总额：**2000 元**

如果您按季度计费，您需要为该季度使用的最大用户数付费，并且只需为剩余的季度付费。

使用相同的示例，如果一个席位每年 100 元，那么每季度是 25 元。

- Q1：您最多有 110 个用户。超过的 10 个用户 x 每用户 25 元 x 3 个季度 = **750 元**。现在为 110 个用户付费。

- Q2：您最多有 105 个用户。由于没有超过 110 个用户，所以不收费。

- Q3：您最多有 120 个用户。超过的 10 个用户 x 每个用户 25 元 x 剩余 1 个季度 = **250 元**。现在为 120 个用户付费。

- Q4：您最多有 120 个用户。没有超过用户数。 即使您有，也不会向您收费，因为在 Q4，不为超出的用户收取任何费用。

年度总额：**1000 元**

通过季度对账，您每年支付的费用更少。

如果您无法参与季度对账，您可以通过使用合同修订，选择退出该流程。在这种情况下，默认进行年度调整。

## 季度订单和付款的时间表

在每个订阅季度结束时，极狐GitLab 会通知您有关超额的信息。
<!--您收到超额通知的日期与您收到账单的日期不同。-->

### 极狐GitLab SaaS

群组所有者在**对账日期**收到一封电子邮件。
该电子邮件传达了[超额席位数量](jihulab_com/index.md#seats-owed)和预计订单金额。

**七天后**，订阅将更新为包括额外的席位，并生成按比例分配的金额的订单。

<!--
如果信用卡已存档，则会自动应用付款。 否则，将根据条款发送订单。

### 私有化部署实例

Administrators receive an email **six days after the reconciliation date**.
This email communicates the [overage seat quantity](self_managed/index.md#users-over-license)
and expected invoice amount.

**Seven days later**, the subscription is updated to include the additional
seats, and an invoice is generated for a prorated amount. If a credit card
is on file, a payment is automatically applied. Otherwise, an invoice is
sent and subject to your payment terms.
-->
