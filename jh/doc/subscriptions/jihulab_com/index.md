stage: Fulfillment
group: Purchase
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: index, reference
---

# 极狐GitLab SaaS 订阅 **(PREMIUM SAAS)**

极狐GitLab SaaS 是极狐GitLab 的软件即服务产品，可在 JiHuLab.com 上获得。
您无需安装任何软件就可以使用极狐GitLab SaaS，您只需要[注册](https://jihulab.com/users/sign_up)即可。注册时，您需要选择：

- [订阅](https://gitlab.cn/pricing/)
- [您想要的席位数量](#how-seat-usage-is-determined)

订阅决定了您的私有项目可以使用哪些功能。拥有公开开源项目的组织可以积极申请我们的[极狐GitLab 开源项目](https://about.gitlab.cn/solutions/open-source/join/)。

通过[极狐GitLab 开源项目](https://about.gitlab.cn/solutions/open-source/)，符合条件的开源项目还可获得 50,000 分钟的 CI/CD 分钟数并免费升级到**旗舰版**。

## 获取极狐GitLab SaaS 订阅

极狐GitLab SaaS 订阅适用于顶级群组。
群组内的各子组及项目成员：

- 可以使用订阅的功能。
- 在订阅中占用席位。

订阅极狐GitLab SaaS：

1. <!--查看[极狐GitLab SaaS 功能对比](https://about.gitlab.cn/pricing/feature-comparison/)-->了解不同级别的功能，决定您想要的级别。
1. 通过[注册页面](https://jihulab.com/users/sign_up)为自己创建一个用户账户。
1. 创建一个[群组](../../user/group/manage.md#create-a-group)。您的订阅级别适用于顶级群组、其子组和项目。
1. 创建额外的用户并[将他们添加到群组](../../user/group/manage.md#add-users-to-a-group)中。 该群组、其子组和项目中的用户可以使用您的订阅级别内的功能，并且会在您的订阅中占用席位。
1. 在左侧边栏中，选择 **设置 > 计费** 并选择一个级别。
1. 填写表格完成购买。

<a id="view-your-gitlabcom-subscription"></a>

## 查看您的极狐GitLab SaaS 订阅

先决条件：

- 您必须拥有群组的所有者角色。

要查看极狐GitLab SaaS 订阅的状态：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 计费**。

显示以下信息：

| 字段              | 描述                                   |
|:----------------|:-------------------------------------|
| **订阅中的用户数量**    | 如果是付费计划，该字段代表您为群组所购买的席位数             |
| **当前正在使用的用户数量** | 使用中的席位数。选择 **查看使用情况** 以查看使用这些席位的用户列表 |
| **已使用的最大用户数量**  | 您使用的最多席位数                            |
| **欠费用户数量**      | **已使用的最大用户数量** 减去 **订阅中的用户数量**       |
| **订阅开始日期**      | 订阅的开始日期。如果是免费计划，则为从付费转为免费的日期         |
| **订阅结束日期**      | 您当前订阅的结束日期。不适用于免费计划                  |

<a id="how-seat-usage-is-determined"></a>

## 确认席位使用情况

> - 引入于极狐GitLab 13.5。
> - 更新于极狐GitLab 13.8，包括公共电子邮件地址。

极狐GitLab SaaS 订阅使用并发 (*席位*) 模型。
您根据计费周期内分配给顶级群组或其子组的最大用户数来支付订阅费用。
您可以在订阅期间添加或删除用户，只要总用户数不超过订阅计数就可以。

顶级群组可以像任何其他群组一样进行[更改](../../user/group/manage.md#change-a-groups-path)。

每个用户都占用席位，但以下情况除外：

- 待批准的用户。
- 具有[旗舰版订阅的访客角色](#free-guest-users)的成员。
- 极狐GitLab 创建的服务账户：
    - [Ghost 用户](../../user/profile/account/delete_account.md#associated-records)
    - 机器人，例如：
        - [支持机器人](../../user/project/service_desk.md#support-bot-user)
        - [项目机器人用户](../../user/project/settings/project_access_tokens.md#bot-users-for-projects)
        - [群组机器人用户](../../user/group/settings/group_access_tokens.md#bot-users-for-groups)

席位使用情况[每季度或每年](../quarterly_reconciliation.md)进行审查。

如果用户导航到不同的顶级群组（例如，他们自己创建的群组），并且该群组没有付费订阅，他们将看不到任何付费功能。

用户也可以属于两个具有不同订阅的不同顶级群组。
在这种情况下，他们只会看到该订阅可用的功能。

<a id="view-seat-usage"></a>

### 查看席位使用情况

查看正在使用的席位列表：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 使用配额**。
1. 在 **席位** 选项卡上，查看使用信息。

席位使用列表、**正在使用的席位**和**订阅中的用户数量**中的数据实时更新。
**已使用的最大用户数量**和**欠费用户数量**的计数每天更新一次。

查看您的订阅信息和席位数：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 计费**。

使用情况统计信息每天更新一次，这可能会导致**使用配额**页面和**计费页面**中的信息之间存在差异。

### 搜索席位使用情况

> 引入于极狐GitLab 13.8。

要在**坐席使用**页面中搜索用户，请在搜索字段中输入字符串，且至少输入 3 个字符。

搜索将返回姓、名或用户名包含搜索字符串的用户。

例如：

| 名    | 字符串   | 是否匹配 |
|:-----|:------|:-----|
| Amir | `ami` | 是    |
| Amir | `amr` | 否    |

### 导出席位使用

> 引入于极狐GitLab 14.2。

将席位使用数据导出为 CSV 文件：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 计费**。
1. 在 **当前正在使用的用户数量** 下，选择 **查看使用情况**。
1. 选择**导出列表**。

生成的列表包含所有正在使用的席位，并且不受当前搜索的影响。

## 欠费用户数量

极狐GitLab 订阅对特定数量的用户有效。

如果计费用户数量超过订阅中包含的数量，即出现**欠费用户数量**，您必须为超出的用户数付费。

例如，如果您为 10 个用户购买订阅：

| 事件               | 计费成员 | 最大用户数量 |
|:-----------------|:-----|:-------|
| 十个用户占用所有的 10 个席位 | 10   | 10     |
| 增加 2 个新用户        | 12   | 12     |
| 3 个用户离开且其账号被删除   | 9    | 12     |

欠费用户数量 = 12 - 10（已使用的最大用户数量 - 订阅用户）

<a id="free-guest-users"></a>

### 免费访客用户 **(ULTIMATE)**

在**旗舰版**中，分配了访客角色的用户不占用席位。
不得在实例中的任何位置为用户分配任何其他角色。

- 如果您的项目是私有或内部的，则具有访客角色的用户拥有[一组权限](../../user/permissions.md#project-members-permissions)。
- 如果您的项目是公开的，则包括具有访客角色用户在内的所有用户都可以访问您的项目。

### 向订阅中添加用户

您的订阅费用基于您在结算周期内使用的最大席位数。
即使您已经达到订阅中的席位数量，您也可以继续添加用户。
极狐GitLab 会[向您收取超额费用](../quarterly_reconciliation.md)。

向订阅添加用户：

1. 登录[极狐订单管理中心](https://customers.jihulab.com/)。
1. 导航至 **购买管理** 页面。
1. 在相关订阅卡上选择 **添加更多席位**。
1. 输入额外用户的数量。
1. 选择**购买席位**。
1. 选择您需要的支付方式，并点击 **立即支付**。

以下内容会通过电子邮件发送给您：

- 付款收据。您还可以在极狐订单管理中心的[**查看发票**](https://customers.jihulab.com/receipts)中访问此信息。

<a id="remove-users-from-your-subscription"></a>

### 从订阅中删除用户

从您的订阅中删除付费用户：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 计费**。
1. 在 **当前正在使用的用户数量** 部分，选择 **查看使用情况**。
1. 在您要删除的用户所在的行的右侧，选择 **删除用户**。
1. 重新输入用户名并选择 **删除用户**。

如果您通过[与另一个群组共享群组](../../user/group/manage.md#share-a-group-with-another-group)的功能将成员添加到群组，您将无法使用此方法删除成员。相反，您可以选择以下其中一种方法：

- 从共享群组中删除成员。您必须是群组所有者才能执行此操作。
- 在群组的成员资格页面，删除整个共享群组的访问权限。

## 席位使用情况告警

> - 引入于极狐GitLab 15.2，[功能标志](../../administration/feature_flags.md)为 `seat_flag_alerts`。
> - 普遍可用于极狐GitLab 15.4。移除功能标志 `seat_flag_alerts`。

如果您具有顶级群组的所有者角色，则系统会向您发送总席位使用情况的告警。

告警显示在群组、子组和项目页面上，并且仅适用于与[季度订阅对账](../quarterly_reconciliation.md)中记录的订阅相关联的顶级群组。
您关闭告警后，只有使用席位后它才会再次显示。

告警会根据以下席位使用情况进行显示。您无法配置警报中所显示的数量。

| 订阅席位    | 席位使用情况         |
|---------|----------------|
| 0-15    | 订阅中剩余 1 席位     |
| 16-25   | 订阅中剩余 2 席位     |
| 26-99   | 已使用订阅中 10% 的席位 |
| 100-999 | 已使用订阅中 8% 的席位  |
| 1000+   | 已使用订阅中 5% 的席位  |


<!--
## 更改关联命名空间

更改订阅相关联的命名空间：

1. 使用[相关](../index.md#change-the-linked-account) JiHuLab.com 账户登录[极狐订单管理中心](https://customers.jihulab.com/customers/sign_in)。
1. 转到 **购买管理** 页面。
1. Select **Change linked namespace**.
1. Select the desired group from the **This subscription is for** dropdown list. For a group to appear here, you must have the Owner role for that group.
1. Select **Proceed to checkout**.

<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
有关演示，请参阅[将 GitLab 订阅链接到命名空间](https://youtu.be/qAq8pyFP-a0)。

订阅费用根据群组中的用户总数计算，包括其子组和嵌套项目。如果[用户总数](#view-seat-usage)超过您订阅中的席位数量，系统会对额外的用户进行收费，并且您需要先支付超额费用，然后才能更改关联的命名空间。

一个订阅只可以与一个命名空间相关联。
-->

<a id="upgrade-your-gitlab-saas-subscription-tier"></a>

## 升级您的极狐GitLab SaaS 订阅级别

升级您的[极狐GitLab 订阅级别](https://about.gitlab.cn/pricing/)：

1. 登录[极狐订单管理中心](https://customers.jihulab.com/customers/sign_in)。
1. 在[购买管理](https://customers.jihulab.com/subscriptions)页面的相关订阅卡上选择**升级计划**。
1. 选择所需的升级级别。
1. 选中 **我接受隐私政策和服务条款** 复选框，并选择 **升级订阅**。
1. 选择付款方式。
1. 选择 **立即支付**，并付款完成升级。

处理完购买后，您会收到新订阅级别的确认信息。

<a id="subscription-expiry"></a>

## 订阅到期

当您的订阅到期时，您可以继续使用极狐GitLab 的付费功能 14 天。
在第 15 天，付费功能将不再可用。您可以继续使用免费功能。

要恢复付费功能，请购买新订阅。

## 续订您的极狐GitLab SaaS 订阅

续订您的订阅：

1. [查看您的账户为续订做准备](#prepare-for-renewal-by-reviewing-your-account)。
1. [续订您的极狐GitLab SaaS 订阅](#renew-or-change-a-gitlab-saas-subscription)。

<a id="prepare-for-renewal-by-reviewing-your-account"></a>

### 查看您的账户为续订做准备

续订之前：

1. 登录[极狐订单管理中心](https://customers.jihulab.com/customers/sign_in)。
1. 在**账户详情**页面，验证或更新发票联系方式。
1. 在极狐GitLab 中，查看您的用户账户列表并[删除不活跃或不需要的用户](#remove-users-from-your-subscription)。

<a id="renew-or-change-a-gitlab-saas-subscription"></a>

### 续订或更改您的极狐GitLab SaaS 订阅

从订阅到期前 30 天开始，极狐GitLab 会在系统界面中通知群组所有者到期日期。

续订您的订阅：

1. 登录[极狐订单管理中心](https://customers.jihulab.com/customers/sign_in)，在您现有的订阅下方，选择 **续订**。
   在订阅到期前 15 天，**续订** 按钮将保持禁用状态（置灰）。
   您可以将鼠标悬停在 **更新** 按钮上以查看激活日期。
1. 查看您的续订详细信息并勾选 **我接受隐私政策和服务条款**。
1. 选择 **续订**。
1. 选择您想要的支付方式并选择 **立即支付** 完成付款。

您升级的订阅级别会在续订的开始日期应用于您的命名空间。处理续订最多可能需要一天时间。

系统会为续订生成发票，您可以在[查看发票](https://customers.jihulab.com/receipts)页面上查看或下载。
<!--如果您在续订过程中遇到困难，请联系[支持团队](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293)寻求帮助。-->

有关升级订阅级别的详细信息，请参阅[升级您的极狐GitLab SaaS 订阅级别](#upgrade-your-gitlab-saas-subscription-tier)。

<a id="automatic-subscription-renewal"></a>

### 自动续订

当订阅设置为自动续订时，系统会在到期日期进行自动续订，不会影响功能的使用。
通过极狐订单管理中心或 JiHuLab.com 购买的订阅默认设置为自动续订。席位数量会在续订时进行调整以适应[您群组中的计费用户数量](#view-seat-usage)。您可以在极狐订单管理中心[查看发票](https://customers.jihulab.com/receipts)页面上查看和下载续订发票<!--If your account has a [saved credit card](../index.md#change-your-payment-method), the card is charged for the invoice amount. -->。如果无法付款或自动续订因其他原因失败，您有 14 天的时间续订，14 天后，您的访问权限将被降级。

#### 电子邮件通知

在订阅自动续订前 15 天，系统会发送一封包含续订信息的电子邮件。

<!-- If your credit card is expired, the email tells you how to update it.-->
- 如果您有任何未解决的超额问题，电子邮件会告诉您联系我们的销售团队。
- 如果没有问题，电子邮件会列出要续订的产品的名称和数量。该电子邮件还包括您欠的总金额。如果您的使用量在续订前增加或减少，则此金额会发生变化。

<!--

<a id="enable-or-disable-automatic-subscription-renewal"></a>

#### 启用或禁用自动化续订

如果您想查看或更改自动续订（与上一期同一级别），请登录[极狐订单管理中心](https://customers.jihulab.com/customers/sign_in)，并且：

- If a **Resume subscription** button is displayed, your subscription was canceled
  previously. Select it to resume automatic renewal.
- If a **Cancel subscription** button is displayed, your subscription is set to automatically
  renew at the end of the subscription period. Select it to cancel automatic renewal.

If you have difficulty during the renewal process, contact the
[Support team](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293) for assistance.-->

## 添加或更改您订阅的联系人

联系人可以续订、取消订阅或将订阅转移到其他命名空间。

更改联系人：

1. 确保您要添加的用户账户在[极狐订单管理中心](https://customers.jihulab.com/customers/sign_in)中。
<!--1. 确认您至少有权访问以下一项
   [这些要求](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/customersdot/associating_purchases.html)。-->
1. [与支持团队一起创建工单](https://support.gitlab.cn)。请在您的请求中包含相关材料。

## CI/CD 分钟数

CI/CD 分钟数是您的[流水线](../../ci/pipelines/index.md)在极狐GitLab 共享 Runner 上的执行时间。

有关更多信息请参考 [CI/CD 分钟数](../../ci/pipelines/cicd_minutes.md)。

### 购买额外 CI/CD 分钟数

您可以为您个人或群组命名空间[购买额外的分钟数](../../ci/pipelines/cicd_minutes.md#purchase-additional-cicd-minutes)。
CI/CD 分钟数是**一次性购买**，因此不会续订。

## 加购订阅额外存储和转移

NOTE:
免费命名空间受 2GB 存储的[软限制](https://about.gitlab.cn/pricing/)。一旦所有存储都可以在使用配额工作流中查看，极狐GitLab 将自动强制执行命名空间存储限制并移除项目限制。此更改将单独公布。用户可以加购存储额度以增加限制上限。

项目有 10GB 的免费存储配额。要超过此配额，您必须先[购买一个或多个存储订阅单元](#purchase-more-storage-and-transfer)。每个单元会为每个命名空间提供 10GB 的额外存储。
存储订阅每年续订一次。有关详细信息，请参阅[使用配额](../../user/usage_quotas.md)。

当购买的存储量为零时，所有超过免费存储配额的项目都将被锁定。只能通过购买更多存储订阅单元来解锁项目。

<a id="purchase-more-storage-and-transfer"></a>

### 购买更多存储和传输订阅

先决条件：

- 您必须具有所有者角色。

您可以为个人或群组命名空间购买存储订阅。

<!--NOTE:
存储订阅每年 **[自动续订](#automatic-subscription-renewal)**。
您可以使用[取消订阅](#enable-or-disable-automatic-subscription-renewal)功能来关闭自动续订。-->

#### 对于个人命名空间

1. 登录极狐GitLab SaaS。
1. 点击右上角个人头像旁边的箭头。
1. 选择 **编辑个人资料**。
1. 在左侧边栏中，选择 **使用量配额**。
1. 选择 **存储** 选项卡，选择 **购买存储**。
1. 填写您的订阅详情信息，选择 **我接受隐私政策和服务条款**。
1. 选择 **购买存储**。
1. 选择您想要的支付方式并选择 **立即支付**。

**购买的存储**总数会随着购买的数量而增加。全部锁定项目会被解锁，超额使用量将从额外存储空间中扣除。

#### 对于群组命名空间

> 引入于极狐GitLab 14.6。

如果您使用的是 JiHuLab SaaS，则可以购买额外的存储空间，这样在您用完所有存储空间后，流水线不会因为存储空间不足而无法使用。
您可以在[极狐GitLab 定价页面](https://about.gitlab.cn/pricing/)找到额外存储的定价。

在 JiHuLab SaaS 上为您的团队购买额外的存储空间：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 使用量配额**。
1. 选择 **存储** 选项卡。
1. 选择 **购买存储**。
1. 按要求填写并完成购买。

付款处理完毕后，您的群组命名空间就可以使用额外的存储空间了。

要确认可用的存储空间，请转到您的群组，然后选择 **设置 > 使用量配额** 并选择 **存储** 选项卡。

**购买的存储**总数会随着购买的数量而增加。全部的锁定项目会被解锁，超额使用量将从额外存储空间中扣除。

## 联系支持人员

更多信息：

- [极狐GitLab 支持](https://about.gitlab.cn/support/)级别
<!--- [通过支持门户提交支持请求](https://support.gitlab.com/hc/en-us/requests/new).-->

我们还鼓励您搜索我们的项目跟踪器以了解[极狐GitLab](https://jihulab.com/gitlab-cn/gitlab/-/issues) 项目中的已知问题和现有功能请求。

这些议题是获取特定产品计划更新和直接与相关的极狐GitLab 团队成员沟通的最佳途径。

## 故障排查

<!--### Credit card declined

If your credit card is declined when purchasing a GitLab subscription, possible reasons include:

- The credit card details provided are incorrect. The most common cause for this is an incomplete or fake address.
- The credit card account has insufficient funds.
- You are using a virtual credit card and it has insufficient funds, or has expired.
- The transaction exceeds the credit limit.
- The transaction exceeds the credit card's maximum transaction amount.

Check with your financial institution to confirm if any of these reasons apply. If they don't
apply, contact [GitLab Support](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293).-->

### 命名空间无法关联订阅

如果您无法将订阅与您的命名空间进行关联，请查看您在命名空间中是否具有所有者角色。
