---
stage: Fulfillment
group: Purchase
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: index, reference
---

# 极狐GitLab 私有化部署订阅 **(PREMIUM SELF)**

您可以安装、管理和维护自己的极狐GitLab 实例。

本页介绍了极狐GitLab 私有化部署订阅的详细信息。

极狐GitLab 订阅管理需要访问极狐订单管理中心。

## 极狐订单管理中心

极狐GitLab 提供[订单管理中心](https://customers.jihulab.com/)，您可以在其中管理订阅和账户详细信息。

## 订阅

极狐GitLab 私有化部署订阅的价格由以下因素决定：

- [极狐GitLab 版本级别](https://about.gitlab.cn/pricing/)
- [订阅席位数](#subscription-seats)

## 选择订阅级别

定价是[基于订阅级别的](https://about.gitlab.cn/pricing/)，因此您可以选择适合您预算的功能。<!--有关各级别可用功能的信息，请参阅[极狐GitLab 私有化部署功能比较](https://https://gitlab.cn/pricing/self-managed/feature-comparison/)。-->

## 许可证席位

对于极狐GitLab 私有化部署订阅，您根据订阅期间的[最大用户数](#maximum-users)支付订阅费用。

<!--
For instances that aren't offline or on a closed network, the maximum number of
simultaneous users in the GitLab self-managed installation is checked each quarter.

If an instance is unable to generate a quarterly usage report, the existing [true-up model](#users-over-license) is used.
Prorated charges are not possible without a quarterly usage report.
-->

### 查看用户总数

您可以查看您的许可证的用户并确定您是否已完成订阅。

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧菜单中，选择 **订阅**。

显示用户列表。

#### 计费用户

计费用户会被计入订阅席位的数量。每个用户都被视为计费用户，但以下情况除外：

- [冻结的用户](../../user/admin_area/moderate_users.md#deactivate-a-user)和[禁用的用户](../../user/admin_area/moderate_users.md#block-a-user)在当前订阅中不算作计费用户。当他们被冻结或禁用时，会释放可计费的用户席位。但是，他们可能会计入订阅席位数的超额部分。
- 等待批准的用户。
- 旗舰版订阅中具有访客角色的成员。
- 旗舰版订阅中没有项目或群组成员身份的用户。
- 极狐GitLab 创建的服务账户：
  - [Ghost 用户](../../user/profile/account/delete_account.md#associated-records)。
  - 机器人，例如：
    - [技术支持机器人](../../user/project/service_desk.md#support-bot-user)。
    - [项目的机器人用户](../../user/project/settings/project_access_tokens.md#bot-users-for-projects)。
    - [群组的机器人用户](../../user/group/settings/group_access_tokens.md#bot-users-for-groups)。

`/admin` 部分中报告的 **计费用户** 每天更新一次。

<a id="maximum-users"></a>

#### 最大用户数

最大用户数反映了当前许可期限内的最高可计费用户数。

<a id="users-over-license-1"></a>

#### 超过许可的用户

超过许可的用户数显示在当前的许可期限内，有多少用户超过许可证允许的数量。

例如，如果：

- 许可证允许 100 个用户，并且
- **最大用户数**为 150，

那么这个值就是 50。

如果**最大用户数**小于或等于 100，则该值为 0。

对于 **超过许可的用户**，试用许可证始终显示为零。

如果您向极狐GitLab 实例添加的用户数量超过了您获得的许可数量，[在续订时](../quarterly_reconciliation.md)，则需要支付额外用户的费用。

如果您在续订过程中不添加这些用户，您的许可证密钥将不起作用。

#### 免费访客用户 **(ULTIMATE)**

在**旗舰版**级别中，拥有访客角色的用户不占用席位。
不得在实例中的任何位置为用户分配任何其他角色。

- 如果您的项目是私有的或内部的，则具有访客角色的用户拥有[一组权限](../../user/permissions.md#project-members-permissions)。
- 如果您的项目是公开的，则包括具有访客角色用户在内的所有用户都可以访问您的项目。
- 用户的最高分配角色是异步更新的，可能需要一些时间。

NOTE:
如果用户创建项目，他们将会被分配维护者或拥有者角色。
要阻止用户创建项目，作为管理员，您可以将用户标记为[外部用户](../../user/admin_area/external_users.md)。

### 管理用户和订阅席位的提示

根据订阅席位的数量管理用户数量可能是一个挑战：

- 如果启用了 LDAP 集成，则配置域名中的任何人都可以注册极狐GitLab 账户。这可能会导致续订时出现意外账单。
- 如果在您的实例上启用了注册，任何可以访问该实例的人都可以注册一个账户。

极狐GitLab 有几个功能可以帮助您管理用户数量：

- 启用[**新注册需要管理员批准**](../../user/admin_area/settings/sign_up_restrictions.md#require-administrator-approval-for-new-sign-ups)选项。
- 为通过 LDAP 或 OmniAuth 的新注册用户，启用 `block_auto_created_users`。
- 启用[用户上限](../../user/admin_area/settings/sign_up_restrictions.md#user-cap)选项。**适用于 13.7 及更高版本**。
- [禁用新注册](../../user/admin_area/settings/sign_up_restrictions.md)，改为手动管理新用户。

- 在[用户统计](../../user/admin_area/index.md#users-statistics)页面中按角色查看用户细分。

<!--## 与极狐GitLab 同步您的订阅数据

> 引入于极狐GitLab 14.1。

先决条件：

- You must be running GitLab Enterprise Edition (EE).
- 您的极狐GitLab 为 14.1 或更高版本。
- 您的实例必须连接到互联网，而不是处于离线环境中。

要在您的私有化部署实例和极狐GitLab 之间同步订阅数据，您必须使用激活码[激活您的实例](../../user/admin_area/license.md)。

激活实例后，将自动执行以下过程：

- [季度订阅对账](../quarterly_reconciliation.md)
- 订阅续订
- 订阅更新，例如添加更多席位或升级极狐GitLab 级别

At approximately 03:00 UTC, a daily sync job sends subscription data to the Customers Portal. For this reason, updates and renewals may not
apply immediately.

The data is sent securely through an encrypted HTTPS connection to `customers.gitlab.com` on port `443`.
If the job fails, it retries up to 12 times over approximately 17 hours.

### Subscription data that GitLab receives

The daily sync job sends **only** the following information to the Customers Portal:

- Date
- Timestamp
- License key
  - Company name (encrypted within license key)
  - Licensee name (encrypted within license key)
  - Licensee email (encrypted within license key)
- Historical maximum user count
- Billable users count
- GitLab version
- Hostname
- Instance ID

Example of a license sync request:

```json
{
  "gitlab_version": "14.1.0-pre",
  "timestamp": "2021-06-14T12:00:09Z",
  "date": "2021-06-14",
  "license_key": "eyJkYXRhIjoiYlR2MFBPSEJPSnNOc1plbGtFRGZ6M
  Ex1mWWhyM1Y3NWFOU0Zj\nak1xTmtLZHU1YzJJUWJzZzVxT3FQRU1PXG5
  KRzErL2ZNd0JuKzBwZmQ3YnY4\nTkFrTDFsMFZyQi9NcG5DVEdkTXQyNT
  R3NlR0ZEc0MjBoTTVna2VORlVcbjAz\nbUgrNGl5N0NuenRhZlljd096R
  nUzd2JIWEZ3NzV2V2lqb3FuQ3RYZWppWVFU\neDdESkgwSUIybFJhZlxu
  Y2k0Mzl3RWlKYjltMkJoUzExeGIwWjN3Uk90ZGp1\nNXNNT3dtL0Vtc3l
  zWVowSHE3ekFILzBjZ2FXSXVQXG5ENWJwcHhOZzRlcFhr\neFg0K3d6Zk
  w3cHRQTTJMTGdGb2Vwai90S0VJL0ZleXhxTEhvaUc2NzVIbHRp\nVlRcb
  nYzY090bmhsdTMrc0VGZURJQ3VmcXFFUS9ISVBqUXRhL3ZTbW9SeUNh\n
  SjdDTkU4YVJnQTlBMEF5OFBiZlxuT0VORWY5WENQVkREdUMvTTVCb25Re
  ENv\nK0FrekFEWWJ6VGZLZ1dBRjgzUXhyelJWUVJGTTErWm9TeTQ4XG5V
  aWdXV0d4\nQ2graGtoSXQ1eXdTaUFaQzBtZGd2aG1YMnl1KzltcU9WMUx
  RWXE4a2VSOHVn\nV3BMN1VFNThcbnMvU3BtTk1JZk5YUHhOSmFlVHZqUz
  lXdjlqMVZ6ODFQQnFx\nL1phaTd6MFBpdG5NREFOVnpPK3h4TE5CQ1xub
  GtacHNRdUxTZmtWWEZVUnB3\nWTZtWGdhWE5GdXhURjFndWhyVDRlTE92
  bTR3bW1ac0pCQnBkVWJIRGNyXG5z\nUjVsTWJxZEVUTXJNRXNDdUlWVlZ
  CTnJZVTA2M2dHblc4eVNXZTc0enFUcW1V\nNDBrMUZpN3RTdzBaZjBcbm
  16UGNYV0RoelpkVk02cWR1dTl0Q1VqU05tWWlU\nOXlwRGZFaEhXZWhjb
  m50RzA5UWVjWEM5em52Y1BjU1xueFU0MDMvVml5R3du\nQXNMTHkyajN5
  b3hhTkJUSWpWQ1BMUjdGeThRSEVnNGdBd0x6RkRHVWg1M0Qz\nMHFRXG5
  5eWtXdHNHN3VBREdCNmhPODFJanNSZnEreDhyb2ZpVU5JVXo4NCtD\nem
  Z1V1Q0K1l1VndPTngyc1l0TU5cbi9WTzlaaVdPMFhtMkZzM2g1NlVXcGI
  y\nSUQzRnRlbW5vZHdLOWU4L0tiYWRESVRPQmgzQnIxbDNTS2tHN1xuQ3
  hpc29D\nNGh4UW5mUmJFSmVoQkh6eHV1dkY5aG11SUsyVmVDQm1zTXZCY
  nZQNGdDbHZL\ndUExWnBEREpDXG41eEhEclFUd3E1clRYS2VuTjhkd3BU
  SnVLQXgvUjlQVGpy\ncHJLNEIzdGNMK0xIN2JKcmhDOTlabnAvLzZcblZ
  HbXk5SzJSZERIcXp3U2c3\nQjFwSmFPcFBFUHhOUFJxOUtnY2hVR0xWMF
  d0Rk9vPVxuIiwia2V5IjoiUURM\nNU5paUdoRlVwZzkwNC9lQWg5bFY0Q
  3pkc2tSQjBDeXJUbG1ZNDE2eEpPUzdM\nVXkrYXRhTFdpb0lTXG5sTWlR
  WEU3MVY4djFJaENnZHJGTzJsTUpHbUR5VHY0\ndWlSc1FobXZVWEhpL3h
  vb1J4bW9XbzlxK2Z1OGFcblB6anp1TExhTEdUQVdJ\nUDA5Z28zY3JCcz
  ZGOEVLV28xVzRGWWtUUVh2TzM0STlOSjVHR1RUeXkzVkRB\nc1xubUdRe
  jA2eCtNNkFBM1VxTUJLZXRMUXRuNUN2R3l3T1VkbUx0eXZNQ3JX\nSWVQ
  TElrZkJwZHhPOUN5Z1dCXG44UkpBdjRSQ1dkMlFhWVdKVmxUMllRTXc5\
  nL29LL2hFNWRQZ1pLdWEyVVZNRWMwRkNlZzg5UFZrQS9mdDVcbmlETWlh
  YUZz\nakRVTUl5SjZSQjlHT2ovZUdTRTU5NVBBMExKcFFiVzFvZz09XG4
  iLCJpdiI6\nImRGSjl0YXlZWit2OGlzbGgyS2ZxYWc9PVxuIn0=\n",
  "max_historical_user_count": 75,
  "billable_users_count": 75,
  "hostname": "gitlab.example.com",
  "instance_id": "9367590b-82ad-48cb-9da7-938134c29088"
}
```

### Troubleshoot automatic subscription sync

If the sync job is not working, ensure you allow network traffic from your GitLab instance
to IP address `104.18.26.123:443` (`customers.gitlab.com`).


## Manually sync your subscription details

You can manually sync your subscription details at any time.

1. On the top bar, select **Main menu > Admin**.
1. On the left sidebar, select **Subscription**.
1. In the **Subscription details** section, select **Sync subscription details**.

A job is queued. When the job finishes, the subscription details are updated.

## Obtain a subscription

To subscribe to GitLab through a GitLab self-managed installation:

1. Go to the [Customers Portal](https://customers.gitlab.com/) and purchase a GitLab self-managed plan.
1. After purchase, an activation code is sent to the email address associated with the Customers Portal account.
   You must [add this code to your GitLab instance](../../user/admin_area/license.md).

NOTE:
If you're purchasing a subscription for an existing **Free** GitLab self-managed
instance, ensure you're purchasing enough seats to
[cover your users](../../user/admin_area/index.md#administering-users).

-->

<a id="view-your-subscription"></a>

## 查看您的订阅

如果您是管理员，则可以查看您的订阅状态：

1. 在顶部栏中，选择 **主菜单 > 管理**。
1. 在左侧边栏中，选择 **订阅**。

**订阅**页面包含以下详细信息：

- 被许可人
- 计划
- 何时上传、何时开始、何时过期

页面还显示以下信息：

| 字段        | 描述                                                                                       |
|:----------|:-----------------------------------------------------------------------------------------|
| 许可证中的用户数  | 您在系统上加载的当前许可证中支付的用户数。除非您在当前订阅期内添加席位<!--[添加席位](#add-seats-to-a-subscription)-->，否则该数字不会改变 |
| 计费用户      | 您系统上的每日计费用户数。当您阻止、停用用户或将用户添加到您的实例时，计数可能会发生变化                                             |
| 最大用户数     | 在加载的许可证期限内，您系统上的最大计费用户数                                                                  |
| 超过许可证的用户数 | 计算为当前许可期限的 `Maximum users` - `Users in License`。此号码会产生追溯费用，必须在续订前支付                      |

## 导出您的许可证使用情况

> 引入于极狐GitLab 14.6。

如果您是管理员，则可以将您的许可证使用情况导出到 CSV 文件中：

1. 在顶部栏中，选择 **主菜单 > 管理**。
1. 在左侧边栏中，选择 **订阅**。
1. 在右上角，选择 **导出许可证使用文件**。

该文件包含极狐GitLab 用于手动处理季度对账或续订的信息。如果您的实例有防火墙或处于离线环境，您必须向极狐GitLab 提供此信息。

**许可证使用** CSV 包括以下详细信息：

- 许可证密钥
- 被许可人电子邮件
- 许可证开始日期
- 许可证结束日期
- 公司
- 生成时间（导出文件时的时间戳）
- 期间内每一天的历史用户数表：
    - 记录计数的时间戳
    - 计费用户数

NOTES:

- 所有日期时间戳均以 UTC 显示。
<!-- A custom format is used for [dates](https://gitlab.com/gitlab-org/gitlab/blob/3be39f19ac3412c089be28553e6f91b681e5d739/config/initializers/date_time_formats.rb#L7) and [times](https://gitlab.com/gitlab-org/gitlab/blob/3be39f19ac3412c089be28553e6f91b681e5d739/config/initializers/date_time_formats.rb#L13) in CSV files.-->

<!--
## Renew your subscription

You can renew your subscription starting from 15 days before your subscription expires.

To renew your subscription,
[prepare for renewal by reviewing your account](#prepare-for-renewal-by-reviewing-your-account),
then [renew your GitLab self-managed subscription](#renew-a-subscription).

### Prepare for renewal by reviewing your account

The [Customers Portal](https://customers.gitlab.com/customers/sign_in) is your
tool for renewing and modifying your subscription. Before going ahead with renewal,
log in and verify or update:

- The invoice contact details on the **Account details** page.
- The credit card on file on the **Payment Methods** page.

NOTE:
Contact our [support team](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293)
if you need assistance accessing the Customers Portal or if you need to change
the contact person who manages your subscription.

It's important to regularly review your user accounts, because:

- Stale user accounts may count as billable users. You may pay more than you should
  if you renew for too many users.
- Stale user accounts can be a security risk. A regular review helps reduce this risk.

#### Users over License

A GitLab subscription is valid for a specific number of seats. The number of users over license
is the number of _Maximum users_ that exceed the _Users in License_ for the current license term.
You must pay for this number of users either before renewal, or at the time of renewal. This is
known as the _true up_ process.

To view the number of _users over license_ go to the **Admin Area**.

##### Users over license example

You purchase a license for 10 users.

| Event                                              | Billable users | Maximum users |
|:---------------------------------------------------|:-----------------|:--------------|
| Ten users occupy all 10 seats.                     | 10               | 10            |
| Two new users join.                                | 12               | 12            |
| Three users leave and their accounts are removed.  | 9                | 12            |

Users over license = 12 - 10 (Maximum users - users in license)

### Add seats to a subscription

The users in license count can be increased by adding seats to a subscription any time during the
subscription period. The cost of seats added during the subscription
period is prorated from the date of purchase through the end of the subscription period.

To add seats to a subscription:

1. Log in to the [Customers Portal](https://customers.gitlab.com/).
1. Navigate to the **Manage Purchases** page.
1. Select **Add more seats** on the relevant subscription card.
1. Enter the number of additional users.
1. Select **Proceed to checkout**.
1. Review the **Subscription Upgrade Detail**. The system lists the total price for all users on the system and a credit for what you've already paid. You are only be charged for the net change.
1. Select **Confirm Upgrade**.

A payment receipt is emailed to you, which you can also access in the Customers Portal under [**View invoices**](https://customers.gitlab.com/receipts).

If your subscription was activated with an activation code, the additional seats are reflected in
your instance immediately. If you're using a license file, you receive an updated file.
To add the seats, [add the license file](../../user/admin_area/license_file.md)
to your instance.

### Renew a subscription

Starting 30 days before a subscription expires, a banner with the expiry date displays for administrators in the GitLab user interface.
You can renew your subscription starting from 15 days before your subscription expires.

We recommend following these steps during renewal:

1. Prior to the renewal date, prune any inactive or unwanted users by [blocking them](../../user/admin_area/moderate_users.md#block-a-user).
1. Determine if you have a need for user growth in the upcoming subscription.
1. Log in to the [Customers Portal](https://customers.gitlab.com/customers/sign_in) and select the **Renew** button beneath your existing subscription.
The **Renew** button remains disabled (grayed-out) until 15 days before a subscription expires.
You can hover your mouse on the **Renew** button to see the date when it will become active.

   NOTE:
   If you need to change your [GitLab tier](https://about.gitlab.com/pricing/), contact our sales team with [the sales contact form](https://about.gitlab.com/sales/) for assistance as this can't be done in the Customers Portal.

1. In the first box, enter the total number of user licenses you'll need for the upcoming year. Be sure this number is at least **equal to, or greater than** the number of billable users in the system at the time of performing the renewal.
1. Enter the number of [users over license](#users-over-license) in the second box for the user overage incurred in your previous subscription term.
1. Review your renewal details and complete the payment process.
1. An activation code for the renewal term is available on the [Manage Purchases](https://customers.gitlab.com/subscriptions) page on the relevant subscription card. Select **Copy activation code** to get a copy.
1. [Add the activation code](../../user/admin_area/license.md) to your instance.

An invoice is generated for the renewal and available for viewing or download on the [View invoices](https://customers.gitlab.com/receipts) page. If you have difficulty during the renewal process, contact our [support team](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293) for assistance.

## Upgrade your subscription tier

To upgrade your [GitLab tier](https://about.gitlab.com/pricing/):

1. Log in to the [Customers Portal](https://customers.gitlab.com/customers/sign_in).
1. Select the **Upgrade** button on the relevant subscription card on the
   [Manage purchases](https://customers.gitlab.com/subscriptions) page.
1. Select the desired upgrade.
1. Confirm the active form of payment, or add a new form of payment.
1. Select the **I accept the Privacy Policy and Terms of Service** checkbox.
1. Select **Purchase**.

The following is emailed to you:

- A payment receipt. You can also access this information in the Customers Portal under
  [**View invoices**](https://customers.gitlab.com/receipts).
- A new activation code for your license.

[Add the activation code](../../user/admin_area/license.md) to your instance.
The new tier takes effect when the new license is activated.

## Add or change the contacts for your subscription

Contacts can renew a subscription, cancel a subscription, or transfer the subscription to a different namespace.

To change the contacts:

1. Ensure an account exists in the
   [Customers Portal](https://customers.gitlab.com/customers/sign_in) for the user you want to add.
1. Verify you have access to at least one of
   [these requirements](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/customersdot/associating_purchases.html).
1. [Create a ticket with the Support team](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293). Include any relevant material in your request.

-->

## 订阅到期

当您的许可证到期时，极狐GitLab 会锁定功能，例如 Git 推送和创建议题等。然后，您的实例会变为只读，并向所有管理员显示许可证到期的消息。

对于极狐GitLab 私有化部署实例，在这之前您有 14 天的宽限期。

- 要恢复功能，请激活新许可证。
- 要退回到免费功能，请删除过期的许可证。

## 激活许可证文件或密钥

如果您有许可证文件或密钥，您可以[在管理中心](../../user/admin_area/license_file.md)激活它。

## 联系技术支持

通过我们的支持服务，您可以了解：

- [极狐GitLab 支持](https://about.gitlab.cn/support/)级别
<!-- [Submit a request via the Support Portal](https://support.gitlab.com/hc/en-us/requests/new).-->

我们还鼓励所有用户在我们的项目跟踪器中搜索[极狐GitLab 项目](https://jihulab.com/gitlab-cn/gitlab/-/issues)中的已知议题和现有的功能请求。

这些议题是获取特定产品计划更新和直接与相关极狐GitLab 团队成员沟通的最佳途径。

<!--
## Troubleshooting

### Credit card declined

If your credit card is declined when purchasing a GitLab subscription, possible reasons include:

- The credit card details provided are incorrect.
- The credit card account has insufficient funds.
- You are using a virtual credit card and it has insufficient funds, or has expired.
- The transaction exceeds the credit limit.
- The transaction exceeds the credit card's maximum transaction amount.

Check with your financial institution to confirm if any of these reasons apply. If they don't
apply, contact [GitLab Support](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293).
-->
