---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 教程：设置议题看板进行团队交接 **(PREMIUM)**

<!-- vale gitlab.FutureTense = NO -->

本教程向您展示如何对于按顺序处理议题的两个团队，设置[议题看板](../../user/project/issue_board.md)和[范围标记](../../user/project/labels.md#scoped-labels)。

在此示例中，您将为用户体验和前端团队创建两个议题看板。
使用以下步骤，您可以为更多子团队（例如后端或 QA）创建议题看板和工作流程。

<!--To learn how we use workflow labels at GitLab, see [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow).-->

要为多个团队设置议题看板：

1. [创建群组](#create-a-group)
1. [创建项目](#create-a-project)
1. [创建标记](#create-labels)
1. [创建团队议题看板](#create-team-issue-boards)
1. [为功能创建议题](#create-issues-for-features)

<a id="the-goal-workflow"></a>

## 目标工作流程

设置完成后，两个团队将能够将议题从一个看板移交给另一个看板，如下所示：

1. 项目负责人将 `Workflow::Ready for design` 和 `Frontend` 标记添加到名为 **重新设计用户个人资料页面** 的功能议题中。
1. UX 团队的产品设计师：
   1. 检查 **UX 工作流程** 看板上的 `Workflow::Ready for design` 列表，并决定重新设计个人资料页面。

      ![Issue board called "UX workflow" with three columns and three issues](img/ux_board_filled_v16_0.png)

   1. 指派自己到 **重新设计用户个人资料页面** 议题。
   1. 将议题卡片拖至 `Workflow::Design` 列表。先前的工作流程标记将自动删除。
   1. 创造✨新设计✨。
   1. [将设计添加到议题](../../user/project/issues/design_management.md)。
   1. 将议题卡片拖到 `Workflow::Ready fordevelopment` 列表中，系统将添加此标记并删除任何其他 `Workflow::` 标记。
   1. 在议题中取消指派自己。
1. 前端团队的开发人员：
   1. 检查 **前端工作流程** 看板上的 `Workflow::Ready for development` 列表，并选择要处理的议题。

      ![Issue board called "Frontend workflow" with three columns and three issues](img/frontend_board_filled_v16_0.png)

   1. 将自己指派给 **重新设计用户个人资料页面** 议题。
   1. 将议题卡片拖到 `Workflow::In development` 列表。先前的工作流程标记将自动删除。
   1. 在[合并请求](../../user/project/merge_requests/index.md)中添加前端代码。
   1. 添加 `Workflow::Complete` 标记。

<a id="create-a-group"></a>

## 创建群组

要为项目的发展做好准备，首先要创建一个群组。
您可以使用群组来同时管理一个或多个相关项目。
您需要将用户添加为群组中的成员，并为他们分配角色。

先决条件：

- 如果您在本教程中使用现有群组，请确保您至少拥有该群组的报告者角色。

创建群组：

1. 在左侧边栏的顶部，选择 **新建** (**{plus}**) 和 **新建群组**。
1. 选择 **创建群组**。
1. 填写字段。将您的群组命名为 `Paperclip Software Factory`。
1. 选择 **创建群组**。

您已创建一个空群组。接下来，您将创建一个项目来存储您的议题和代码。

<a id="create-a-project"></a>

## 创建项目

主要的代码开发工作发生在项目及其仓库中。
项目包含您的代码和流水线，还包含用于规划即将进行的代码更改的议题。

先决条件：

- 如果您在本教程中使用现有项目，请确保您至少具有该项目的报告者角色。

要创建一个空白项目：

1. 在您的群组中，在页面右侧，选择 **新建项目**。
1. 选择 **创建空白项目**。
1. 输入项目详细信息：
   - 在 **项目名称** 字段中，将您的项目命名为 `Paperclip Assistant`。
1. 选择 **创建项目**。

<a id="create-labels"></a>

## 创建标记

您需要一个团队标记和一组工作流程标记来显示议题在开发周期中的哪个位置。

您可以在 `Paperclip Assistant` 项目中创建这些标记，但最好在 `Paperclip Software Factory` 群组中创建这些标记。这样，这些标记也将在您以后创建的所有其他项目中可用。

创建每个标记：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的 **Paperclip Software Factory** 群组。
1. 选择 **管理 > 标记**。
1. 选择 **新建标记**。
1. 在 **标题** 字段中，输入标记的名称。从 `Frontend` 开始。
1. 可选。通过在可用颜色中选择颜色，或在 **背景颜色** 字段中输入特定颜色的十六进制颜色值。
1. 选择 **创建标记**。

重复以上步骤来创建您需要的所有标记：

- `Frontend`
- `Workflow::Ready for design`
- `Workflow::Design`
- `Workflow::Ready for development`
- `Workflow::In development`
- `Workflow::Complete`

<a id="create-team-issue-boards"></a>

## 创建团队议题看板

与标记一样，您可以在 **Paperclip Assistant** 项目中创建议题看板，但最好将它们放在 **Paperclip Software Factory** 群组中。这样，您将能够管理稍后可能在该群组中创建的所有项目中的议题。

要创建新的群组议题看板：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的 **Paperclip Software Factory** 群组。
1. 选择 **计划 > 议题看板**。
1. 创建 UX 工作流程和前端工作流程看板。

要创建 **UX 工作流程** 议题看板：

1. 在议题看板页面的左上角，选择包含当前看板名称的下拉列表。
1. 选择 **创建新看板**。
1. 在 **标题字段** 中，输入 `UX workflow`。
1. 清除 **显示开放列表** 和 **显示已关闭列表** 复选框。
1. 选择 **创建看板**。您应该能够看到一个空看板。
1. 为 `Workflow::Ready for design` 标记创建一个列表：
   1. 在议题看板页面的左上角，选择 **创建列表**。
   1. 在显示的列中，从 **值** 下拉列表中选择 `Workflow::Ready for design` 标记。
   1. 选择 **添加到看板**。
1. 对标签 `Workflow::Design”和“Workflow::Ready fordevelopment` 重复上一步。

![Issue board called "UX workflow" with three columns and no issues](img/ux_board_empty_v16_0.png)

要创建 **前端工作流程** 看板：

1. 在议题看板页面的左上角，选择包含当前看板名称的下拉列表。
1. 选择 **创建新看板**。
1. 在 **标题字段** 中，输入 `Frontend workflow`。
1. 清除 **显示已打开列表** 和 **显示已关闭列表** 复选框。
1. 扩大 **范围**。
1. 在 **标记** 旁边，选择 **编辑**，然后选择 `Frontend` 标记。
1. 选择 **创建看板**。
1. 为 `Workflow::Ready fordevelopment` 标记创建一个列表：
   1. 在议题看板页面的左上角，选择 **创建列表**。
   1. 在出现的列中，从 **值** 下拉列表中选择 `Workflow::Ready fordevelopment` 标记。
   1. 选择 **添加到看板**。
1. 对标记 `Workflow::Indevelopment` 和 `Workflow::Complete` 重复上一步。

![Issue board called "Frontend workflow" with three columns and no issues](img/frontend_board_empty_v16_0.png)

目前，两个看板上的列表都应该是空的。接下来，您将向其中填充一些议题。

<a id="create-issues-for-features"></a>

## 为功能创建议题

要跟踪即将推出的功能、增强的功能和 bugs，您必须创建一些议题。
议题属于项目，但您也可以直接从议题看板创建它们。

要从您的看板创建议题：

1. 在议题看板页面的左上角，选择包含当前看板名称的下拉列表。
1. 选择 **UX 工作流程**。
1. 在 `Workflow::Ready for development` 列表中，选择 **列出操作** (**{ellipsis_v}**) **> 创建新议题**。
1. 填写字段：
   1. 在 **标题** 下，输入 **重新设计用户个人资料页面**。
   1. 在 **项目** 下，选择 **Paperclip Software Factory / Paperclip Assistant**。
1. 选择 **创建议题**。由于您在标记列表中创建了新议题，因此它是使用此标记创建的。
1. 添加 `Frontend` 标记，因为只有带有此标记的议题才会出现在前端团队的看板上：
   1. 选择议题卡片（不是其标题），右侧会出现一个侧边栏。
   1. 在侧边栏的 **标记** 部分中，选择 **编辑**。
   1. 从 **分配标记** 下拉列表中，选择 `Workflow::Ready for design` 和 `Frontend` 标记。选定的标记旁会显示选中标记。
   1. 要将更改应用到标记，请选择 **分配标记** 旁边的 **X** 或选择标记部分之外的任何区域。

重复这些步骤可以创建更多具有相同标记的议题。

您现在应该至少看到一个议题，可供您的产品设计师开始处理！

恭喜！现在您的团队可以开始协作开发软件。
下一步，您可以使用这些看板亲自尝试[目标工作流程](#the-goal-workflow)，模拟两个团队的交互。

<!--
## Learn more about project management in GitLab

Find other tutorials about project management on the [tutorials page](../plan_and_track.md).
-->
