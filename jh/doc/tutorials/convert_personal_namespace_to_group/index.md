---
stage: none
group: Tutorials
info: For assistance with this tutorial, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments-to-other-projects-and-subjects.
---

# 教程：将个人命名空间转换为群组 **(FREE SAAS)**

如果您已开始使用个人[命名空间](../../user/namespace/index.md)，但现在发现它的局限性阻碍了您的项目协作，您可能想要改为切换到群组命名空间。
群组命名空间允许您创建多个子组，并管理它们的成员和权限。

您不必从头开始 - 您可以创建一个新群组，并将现有项目移至该群组。
要了解如何操作，请参阅[教程：将您的个人项目移至群组](../move_personal_project_to_group/index.md)。

但是您可以更进一步，将您的个人命名空间转换为群组命名空间，这样您就可以保留现有的用户名和 URL。例如，您的用户名是 alex，您可以继续为您的群组使用 `https://gitlab.example.com/alex` URL。

本教程向您展示如何使用以下步骤将个人命名空间转换为群组命名空间：

1. [创建群组](#create-a-group)
1. [将项目从个人命名空间转移到群组](#transfer-projects-from-the-personal-namespace-to-the-group)
1. [重命名原来的用户名](#rename-the-original-username)
1. [将新群组命名空间重命名为原始用户名](#rename-the-new-group-namespace-to-the-original-username)

例如，您的个人命名空间的用户名是 `alex`，首先创建一个名为 `alex-group` 的群组命名空间。
然后，将所有项目从 `alex` 移动到 `alex-group` 命名空间。最后，将 `alex` 命名空间重命名为 `alex-user`，将 `alex-group` 命名空间重命名为现在可用的 `alex` 用户名。

<a id="create-a-group"></a>

## 创建群组

1. 在顶部栏中，选择 **主菜单 > 群组 > 查看所有群组**。
1. 在页面右侧，选择 **新建群组**。
1. 在 **群组名称** 中，输入群组的名称。
1. 在 **群组 URL** 中，输入群组的路径，用作命名空间。您不必担心实际路径，这只是暂时的路径。您将在[最后一步](#rename-the-new-group-namespace-to-the-original-username)中将此 URL 更改为个人命名空间的用户名。
1. 选择[可见性级别](../../user/public_access.md)。
1. 可选。填写信息以个性化您的体验。
1. 选择 **创建群组**。

<a id="transfer-projects-from-the-personal-namespace-to-the-group"></a>

## 将项目从个人命名空间转移到群组

接下来，您必须将您的项目从个人命名空间转移到新群组。
一次只能传输一个项目，因此如果要传输多个项目，必须对每个项目执行以下步骤。

在开始转移过程之前，请确保：

- 具有项目的所有者角色。
- 删除[容器镜像](../../user/packages/container_registry/index.md#move-or-rename-container-registry-repositories)。您无法传输包含容器镜像的项目。
- 删除 npm 包。 您不能更新包含 npm 包的项目的根命名空间。

要将项目转移到群组：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 在 **转移项目** 下，选择要将项目转移到的群组。
1. 选择 **转移项目**。
1. 输入项目名称并选择 **确认**。

<a id="rename-the-original-username"></a>

## 重命名原来的用户名

接下来，重命名个人命名空间的原始用户名，以便该用户名可用于新的群组命名空间。
您可以继续将个人命名空间用于其他个人项目，或[删除该用户帐户](../../user/profile/account/delete_account.md)。

从您重命名个人命名空间的那一刻起，用户名可用，因此其他人可能会用它注册一个帐户。为避免这种情况，您应该尽快[重命名新群组](#rename-the-new-group-namespace-to-the-original-username)。

[更改用户的用户名](../../user/profile/index.md#change-your-username)：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏中，选择 **帐户**。
1. 在 **更改用户名** 部分，输入新用户名作为路径。
1. 选择 **更新用户名**。

<a id="rename-the-new-group-namespace-to-the-original-username"></a>

## 将新群组命名空间重命名为原始用户名

最后，将新群组的 URL 重命名为原始个人命名空间的用户名。

要[更改您的群组路径](../../user/group/manage.md#change-a-groups-path)（群组 URL）：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **高级** 部分。
1. 在 **更改群组 URL** 下，输入用户原来的用户名。
1. 选择 **更改群组 URL**。

您现在已经将个人命名空间转换为一个群组，这为处理项目和与更多成员协作开辟了新的可能。
