---
stage: Govern
group: Compliance
info: For assistance with this tutorial, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments-to-other-projects-and-subjects.
---

# 教程：创建合规流水线 **(ULTIMATE)**

您可以使用[合规流水线](../../user/group/compliance_frameworks.md#compliance-pipelines)，来确保特定的合规相关作业在群组中所有项目的流水线上运行。合规流水线通过[合规框架](../../user/group/compliance_frameworks.md)应用于项目。

1. 创建[新群组](#create-a-new-group)
1. 创建[用于合规流水线配置的新项目](#create-a-new-compliance-pipeline-project)
1. 配置一个[合规框架](#configure-compliance-framework)，应用于其他项目
1. 创建一个[新项目并应用合规框架](#create-a-new-project-and-apply-the-compliance-framework)
1. 结合[合规流水线配置和常规流水线配置](#combine-pipeline-configurations)

先决条件：

- 创建新的顶级群组的权限。

<a id="create-a-new-group"></a>

## 创建新群组

合规框架在顶级群组中配置。在本教程中，您将创建一个顶级群组：

- 包含两个项目：
  - 用于存储合规流水线配置的合规流水线项目。
  - 另一个项目，必须在其流水线中运行由合规流水线配置定义的作业。
- 具有适用于项目的合规框架。

创建新群组：

1. 在顶部栏中，选择 **创建新... > 新组**。
1. 选择 **创建群组**。
1. 在 **群组名称** 字段中，输入 `Tutorial group`。
1. 选择 **创建群组**。

<a id="create-a-new-compliance-pipeline-project"></a>

## 创建一个新的合规流水线项目

现在您已准备好创建合规流水线项目。该项目包含[合规流水线配置](../../user/group/compliance_frameworks.md#example-configuration)，应用于所有使用合规框架的项目。

创建合规流水线项目：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到 `Tutorial group` 群组。
1. 选择 **新建项目**。
1. 选择 **创建空白项目**。
1. 在 **项目名称** 字段中，输入 `Tutorial compliance project`。
1. 选择 **创建项目**。

将合规流水线配置添加到 `Tutorial compliance project`：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到 `Tutorial compliance project` 项目。
1. 在左侧边栏中，选择 **CI/CD > 编辑器**。
1. 选择 **配置流水线**。
1. 在流水线编辑器中，将默认配置替换为：

   ```yaml
   ---
   compliance-job:
     script:
       - echo "Running compliance job required for every project in this group..."
   ```

1. 选择 **提交更改**。

<a id="configure-compliance-framework"></a>

## 配置合规框架

合规框架在[新群组](#create-a-new-group)中配置。

配置合规框架：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到 `Tutorial group` 群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **合规框架**。
1. 选择 **添加框架**。
1. 在 **名称** 字段中，输入 `Tutorial compliance framework`。
1. 在 **描述** 字段中，输入 `Tutorial compliance project`。
1. 在 **合规流水线配置（可选）**字段中，输入 `.gitlab-ci.yml@tutorial-group/tutorial-compliance-project`。
1. 在 **背景颜色** 字段中，选择您喜欢的颜色。
1. 选择 **添加框架**。

为方便起见，将新的合规框架设为群组中所有新项目的默认框架：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到 `Tutorial group` 群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **合规框架**。
1. 在 `Tutorial compliance framework` 行中，选择 **选项** (**{ellipsis_v}**)。
1. 选择 **设置默认值**。

<a id="create-a-new-project-and-apply-the-compliance-framework"></a>

## 创建一个新项目并应用合规框架

您的合规框架已准备就绪，因此您现在可以在群组中创建项目，他们会自动在其流水线中运行合规流水线配置。

创建一个新项目来运行合规流水线配置：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到 `Tutorial group` 群组。
1. 选择 **新建项目**。
1. 选择 **创建空白项目**。
1. 在 **项目名称** 字段中，输入 `Tutorial project`。
1. 选择 **创建项目**。

在项目页面上，请注意出现了 `Tutorial compliance framework` 标记，因为它被设置为该群组的默认合规框架。

在没有任何其他流水线配置的情况下，`Tutorial project` 可以运行在 `Tutorial compliance project` 的合规流水线配置中定义的作业。

在 `Tutorial project` 中运行合规流水线配置：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到 `Tutorial project` 项目。
1. 选择 **CI/CD 流水线**。
1. 选择 **运行流水线**。
1. 在 **运行流水线** 页面上，选择 **运行流水线**。

请注意，流水线在 **test** 阶段运行名为 `compliance-job` 的作业。干得好，您已经运行了第一个合规作业！

<a id="combine-pipeline-configurations"></a>

## 结合流水线配置

如果您希望项目运行自己的作业以及合规流水线作业，则必须结合项目的合规流水线配置和常规流水线配置。

要结合流水线配置，您必须定义常规流水线配置，然后更新合规流水线配置来引用它。

要创建常规流水线配置：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到 `Tutorial project` 项目。
1. 在左侧边栏中，选择 **CI/CD > 编辑器**。
1. 选择 **配置流水线**。
1. 在流水线编辑器中，将默认配置替换为：

   ```yaml
   ---
   project-job:
     script:
       - echo "Running project job..."
   ```

1. 选择**提交更改**。

将新项目流水线配置与合规流水线配置相结合：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到 `Tutorial compliance project` 项目。
1. 在左侧边栏中，选择 **CI/CD > 编辑器**。
1. 在现有配置中，添加：

   ```yaml
   include:
     - project: 'tutorial-group/tutorial-project'
       file: '.gitlab-ci.yml'
    ```

1. 选择 **提交更改**。

确认常规流水线配置与合规流水线配置相结合：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到 `Tutorial project` 项目。
1. 选择 **CI/CD 流水线**。
1. 选择 **运行流水线**。
1. 在 **运行流水线** 页面上，选择 **运行流水线**。

请注意，流水线在 **test** 阶段运行两个作业：

- `compliance-job`
- `project-job`

恭喜，您已经创建并配置了合规流水线！

查看更多[合规流水线配置示例](../../user/group/compliance_frameworks.md#example-configuration)。
