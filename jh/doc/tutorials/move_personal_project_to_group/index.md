---
stage: none
group: unassigned
info: For assistance with this tutorial, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments-to-other-projects-and-subjects.
---

# 将您的个人项目移至群组 **(FREE SAAS)**

本教程将向您展示如何将个人项目移动到群组中。

## 为什么群组很重要？

在极狐GitLab 中，使用[群组](../../user/group/index.md)可以同时管理一个或多个相关项目。
群组具有很多的好处。例如，您可以：

- 管理您的项目的权限。
- 查看群组中项目的所有议题和合并请求。
- 查看您命名空间中的所有项目的所有用户。
- 管理使用量配额。

<!--
- Start a trial or upgrade to a paid tier. This option is important if you're
  impacted by the [changes to user limits](https://about.gitlab.com/blog/2022/03/24/efficient-free-tier/),
  and need more users.
-->

但是，如果您在[个人项目](../../user/project/working_with_projects.md#view-personal-projects)中工作，则无法使用这些功能。个人项目是在您的[个人命名空间](../../user/group/index.md#namespaces)下创建的，不是群组的一部分，因此您无法获得群组的任何功能。

但别担心！您可以将现有的个人项目移动到群组中。
接下来将向您展示如何操作。

## 步骤

以下是我们将要做的事情的概述：

1. [创建群组](#create-a-group)。
1. [将您的项目移至群组](#move-your-project-to-a-group)。
1. [使用群组进行工作](#work-with-your-group)。

<a id="create-a-group"></a>

### 创建群组

首先，请确保您有一个合适的群组，以便您将项目转移到其中。
该群组必须允许创建项目，并且您必须至少具有该群组的维护者角色。

如果您没有群组，请创建一个：

1. 在顶部栏上，选择 **菜单 > 群组 > 创建群组**。
1. 选择 **创建群组**。
1. 在 **群组名称** 中，输入群组的名称。
1. 在 **群组 URL** 中，输入群组的路径，用作命名空间。
1. 选择[可见性级别](../../user/public_access.md)。
1. 可选。填写个性化体验信息。
1. 选择 **创建群组**。

<a id="move-your-project-to-a-group"></a>

### 将您的项目移至群组

在将项目移动到群组之前：

- 您必须具有项目的所有者角色。
- 删除任何[容器镜像](../../user/packages/container_registry/index.md#limitations)和 [NPM 包](../../user/packages/npm_registry/index.md#limitations)。

现在您已准备好移动您的项目：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 在 **转移项目** 下，选择要将项目转移到的群组。
1. 选择 **转移项目**。
1. 输入项目名称并选择 **确认**。

您将被重定向到项目的新页面。
如果您有多个个人项目，您可以为每个项目重复这些步骤。

NOTE:
有关这些迁移步骤的更多信息，请参阅[将项目转移到另一个命名空间](../../user/project/settings/index.md#transfer-a-project-to-another-namespace)。迁移可能会导致后续工作，比如更新相关资源和工具（例如网站和包管理器）中的项目路径。

<a id="work-with-your-group"></a>

### 使用群组进行工作

您现在可以在您的群组中查看您的项目：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 在 **子组和项目** 下查找您的项目。

开始体验群组的优势！例如，作为群组所有者，您可以快速查看命名空间中的所有用户：

1. 在您的群组中，选择 **设置 > 使用量配额**。
1. 在 **席位** 选项卡中，显示您群组中所有项目的所有用户。
