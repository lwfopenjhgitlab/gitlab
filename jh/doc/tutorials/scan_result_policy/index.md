---
stage: Govern
group: Security Policies
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 教程：设置扫描结果策略 **(ULTIMATE)**

本教程向您展示如何创建和配置[扫描结果策略](../../user/application_security/policies/scan-result-policies.md)。这些策略可以设置为根据扫描结果采取行动。
例如，在本教程中，您将设置一个策略，如果在合并请求中检测到漏洞，则需要两个指定用户的批准。

先决条件：

用于本教程的命名空间必须：

- 至少包含三个用户，包括您自己的用户。如果您没有其他两个用户，则必须先创建它们。详情参见[创建用户](../../user/profile/account/create_accounts.md)。

要设置扫描结果策略：

1. [创建测试项目](#create-a-test-project)。
1. [添加扫描结果策略](#add-a-scan-result-policy)。
1. [测试扫描结果策略](#test-the-scan-result-policy)。

<a id="create-a-test-project"></a>

## 创建测试项目

1. 在顶部栏中，选择 **主菜单 > 项目**。
1. 选择 **新建项目**。
1. 选择 **创建空白项目**。
1. 完成字段。
   - **项目名称**：`sast-scan-result-policy`。
   - 选择 **启用静态应用程序安全测试（SAST）** 复选框。
1. 选择 **创建项目**。

<a id="add-a-scan-result-policy"></a>

## 添加扫描结果策略

接下来，您将向测试项目添加扫描结果策略：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到 `sast-scan-result-policy` 项目。
1. 在左侧边栏中，转到 **安全与合规 > 策略**。
1. 选择 **新建策略**。
1. 在 **扫描结果策略** 中，选择 **选择策略**。
1. 完成字段。
   - **名称**：`sast-scan-result-policy`
   - **策略状态**：**已启用**
1. 添加以下规则：

   ```plaintext
   IF |Security Scan| from |SAST| find(s) more than |0| |All severity levels| |All vulnerability states| vulnerabilities in an open merge request targeting |All protected branches|
   ```

1. 将 **操作** 设置为以下内容：

   ```plaintext
   THEN Require approval from | 2 | of the following approvers:
   ```

1. 选择两个用户。
1. 选择 **使用合并请求进行配置**。

   该应用程序创建一个新项目来存储链接到它的策略，并创建一个合并请求来定义策略。

1. 选择**合并**。
1. 在顶部栏中，选择 **主菜单 > 项目** 并选择 `sast-scan-result-policy` 项目。
1. 在左侧边栏中，选择 **安全与合规 > 策略**。

   您可以看到在前面的步骤中添加的策略列表。

<a id="test-the-scan-result-policy"></a>

## 测试扫描结果策略

干得好，您已经创建了扫描结果策略。为了测试它，创建一些漏洞并检查结果：

1. 在顶部栏中，选择 **主菜单 > 项目** 并选择 `sast-scan-result-policy` 项目。
1. 在左侧边栏中，选择 **仓库 > 文件**。
1. 从 **添加** (**{plus}**) 下拉列表中，选择 **新建文件**。
1. 在 **文件名** 字段中输入 `main.ts`。
1. 在文件内容中，复制以下内容：

   ```typescript
   // Non-literal require - tsr-detect-non-literal-require
   var lib: String = 'fs'
   require(lib)

   // Eval with variable - tsr-detect-eval-with-expression
   var myeval: String = 'console.log("Hello.");';
   eval(myeval);

   // Unsafe Regexp - tsr-detect-unsafe-regexp
   const regex: RegExp = /(x+x+)+y/;

   // Non-literal Regexp - tsr-detect-non-literal-regexp
   var myregexpText: String = "/(x+x+)+y/";
   var myregexp: RegExp = new RegExp(myregexpText);
   myregexp.test("(x+x+)+y");

   // Markup escaping disabled - tsr-detect-disable-mustache-escape
   var template: Object = new Object;
   template.escapeMarkup = false;

   // Detects HTML injections - tsr-detect-html-injection
   var element: Element =  document.getElementById("mydiv");
   var content: String = "mycontent"
   Element.innerHTML = content;

   // Timing attack - tsr-detect-possible-timing-attacks
   var userInput: String = "Jane";
   var auth: String = "Jane";
   if (userInput == auth) {
     console.log(userInput);
   }
   ```

1. 在 **提交消息** 字段中，输入 `Add vulnerable file`。
1. 在 **目标分支** 字段中，输入 `test-branch`。
1. 选择 **提交更改**。**新建合并请求** 表单打开。
1. 选择 **创建合并请求**。
1. 在新的合并请求中，选择 `Create merge request`。

   等待流水线完成。这可能需要几分钟。

合并请求安全部件确认安全扫描检测到一个潜在漏洞。根据扫描结果策略中的定义，合并请求被阻止并等待批准。

您现在知道如何设置和使用扫描结果策略来捕获漏洞了！
