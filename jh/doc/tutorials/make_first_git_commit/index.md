---
stage: none
group: unassigned
info: For assistance with this tutorial, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments-to-other-projects-and-subjects.
---

# 进行第一次 Git 提交

本教程包含一些关于 Git 的工作原理，引导您完成创建自己的项目、编辑文件以及从命令行将更改提交到 Git 仓库的步骤。

完成后，您将拥有一个可以练习使用 Git 的项目。

## 您需要什么

在您开始之前：

- [在本地机器上安装 Git](../../topics/git/how_to_install_git/index.md)。
- 确保您可以登录极狐GitLab 实例。如果您的组织没有极狐GitLab，请在 JiHuLab.com 上创建一个帐户。
- [创建 SSH 密钥并将它们添加到极狐GitLab](../../user/ssh.md)。SSH 密钥是您在计算机和极狐GitLab 之间安全通信的方式。

## 什么是 Git？

在我们进入步骤之前，让我们回顾一些基本的 Git 概念。

Git 是一个版本控制系统。它用于跟踪文件的更改。

您将文件（如代码或文档）存储在 Git *仓库* 中。当您想要编辑文件时，您可以将仓库 *克隆* 到您的计算机，进行更改，然后将更改 *推送* 回仓库。在极狐GitLab 中，Git 仓库位于 *项目* 中。

每次推送更改时，Git 都会将其记录为唯一的 *提交*。这些提交构成了文件更改的时间和方式以及更改者的历史记录。

```mermaid
graph LR
    subgraph Repository commit history
    A(Author: Alex<br>Date: 3 Jan at 1PM<br>Commit message: Added sales figures for January<br> Commit ID: 123abc12) --->  B
    B(Author: Sam<br>Date: 4 Jan at 10AM<br>Commit message: Removed outdated marketing information<br> Commit ID: aabb1122) ---> C
    C(Author: Zhang<br>Date: 5 Jan at 3PM<br>Commit message: Added a new 'Invoices' file<br> Commit ID: ddee4455)
    end
```

当您在 Git 仓库中工作时，您在 *分支* 中工作。默认情况下，仓库的内容位于默认分支中。要进行更改：

1. 创建自己的分支，即创建时默认分支的快照。
1. 进行更改并将其推送到您的分支。每次推送都会创建一个提交。
1. 准备好后，*合并* 您的分支到默认分支。

```mermaid
flowchart LR
    subgraph Default branch
    A[Commit] --> B[Commit] --> C[Commit] --> D[Commit]
    end
    subgraph My branch
    B --1. Create my branch--> E(Commit)
    E --2. Add my commit--> F(Commit)
    F --2. Add my commit--> G(Commit)
    G --3. Merge my branch to default--> D
    end
```

<!--
If this all feels a bit overwhelming, hang in there. You're about to see these concepts in action.
-->

## 操作步骤

以下是我们将要做的事情的概述：

1. [创建示例项目](#create-a-sample-project)。
1. [克隆仓库](#clone-the-repository)。
1. [创建分支并进行更改](#create-a-branch-and-make-changes)。
1. [提交并推送您的更改](#commit-and-push-your-changes)。
1. [合并您的更改](#merge-your-changes)。
1. [在极狐GitLab 中查看您的更改](#view-your-changes-in-gitlab)。

<a id="create-a-sample-project"></a>

### 创建示例项目

首先，在极狐GitLab 中创建一个示例项目。

1. 在极狐GitLab 中，在顶部栏上，选择 **菜单 > 项目 > 创建新项目**。
1. 选择 **创建空白项目**。
1. 对于 **项目名称**，输入 `My sample project`。为您生成项目标识串。此标识串是您可以在创建项目后用来访问项目的 URL。
1. 确保选中 **使用自述文件初始化仓库**，其它字段取决于您。
1. 选择 **创建项目**。

<a id="clone-the-repository"></a>

### 克隆仓库

现在您可以克隆项目中的仓库。*克隆* 仓库意味着您正在计算机上创建副本，或者在您想要存储和使用文件的任何地方创建副本。

1. 在您的项目页面上，选择**克隆**。复制 **使用 SSH 克隆** 的 URL。

   ![Clone a project with SSH](img/clone_project_v14_9.png)

1. 在您的计算机上打开一个终端，然后转到您要克隆文件的目录。

1. 输入 `git clone` 并粘贴 URL：

   ```shell
   git clone git@gitlab.com:gitlab-example/my-sample-project.git
   ```

1. 进入目录：

   ```shell
   cd my-sample-project
   ```

1. 默认情况下，您已经克隆了仓库的默认分支。通常这个分支是 `main`。为了确认，获取默认分支的名称：

   ```shell
   git branch
   ```

   您所在的分支标有星号。
   按键盘上的 `Q` 返回主终端窗口。

<a id="create-a-branch-and-make-changes"></a>

### 创建分支并进行更改

现在您有了仓库的副本，创建自己的分支，以便您可以独立处理更改。

1. 创建一个名为 `example-tutorial-branch` 的新分支。

   ```shell
   git checkout -b example-tutorial-branch
   ```

1. 在 Visual Studio Code、Sublime、`vi` 或其它任何文本编辑器中，打开 README.md 文件并添加以下文本：

   ```plaintext
   Hello world! I'm using Git!
   ```

1. 保存文件。

1. Git 会跟踪更改的文件。要确认哪些文件已更改，请获取状态。

   ```shell
   git status
   ```

   您应该得到类似于以下内容的输出：

   ```shell
   On branch example-tutorial-branch
   Changes not staged for commit:
   (use "git add <file>..." to update what will be committed)
   (use "git restore <file>..." to discard changes in working directory)
   modified:   README.md

   no changes added to commit (use "git add" and/or "git commit -a")
   ```

<a id="commit-and-push-your-changes"></a>

### 提交并推送您的更改

您已对仓库中的文件进行了更改。现在是时候通过首次提交来记录这些更改了。

1. 将 `README.md` 文件添加到 *暂存* 区域。暂存区是您在提交文件之前放置文件的地方。

   ```shell
   git add README.md
   ```

1. 确认文件已暂存：

   ```shell
   git status
   ```

   您应该得到类似于以下内容的输出，并且文件名应该是绿色文本。

   ```shell
   On branch example-tutorial-branch
   Changes to be committed:
   (use "git restore --staged <file>..." to unstage)
   modified:   README.md
   ```

1. 现在提交暂存文件，并包含一条描述您所做更改的消息。确保用双引号 (") 将消息括起来。

   ```shell
   git commit -m "I added text to the README file"
   ```

1. 更改已提交到您的分支，但您的分支及其提交仍然仅在您的计算机上可用。还没有其它人可以访问它们。将您的分支推送到极狐GitLab：

   ```shell
   git push origin example-tutorial-branch
   ```

您的分支现在在极狐GitLab 上可用，并且对您项目中的其他用户可见。

![Branches dropdown list](img/branches_dropdown_v14_10.png)

<a id="merge-your-changes"></a>

### 合并您的更改

现在您已准备好将您的 `example-tutorial-branch` 分支中的更改合并到默认分支（`main`）。

1. 查看仓库的默认分支。

   ```shell
   git checkout main
   ```

1. 将您的分支合并到默认分支中。

   ```shell
   git merge example-tutorial-branch
   ```

1. 推送更改。

   ```shell
   git push
   ```

NOTE:
对于本教程，您将分支直接合并到仓库的默认分支。在极狐GitLab 中，您通常使用[合并请求](../../user/project/merge_requests/)来合并您的分支。

<a id="view-your-changes-in-gitlab"></a>

### 在极狐GitLab 中查看您的更改

您更新了分支中的 README.md 文件，并将这些更改合并到了 main 分支中。

让我们查看 UI 并确认您的更改。转到您的项目。

- 向下滚动并查看 `README.md` 文件的内容。您的更改应该是可见的。
- 在 `README.md` 文件上方，查看 **最后提交** 列中的文本。您的提交消息显示在此列中：

  ![Commit message](img/commit_message_v14_10.png)

现在您可以返回命令行并切换回您的个人分支（`git checkout example-tutorial-branch`）。您可以继续更新文件或创建新文件。键入 `git status`，查看更改的状态并放弃提交。

如果您没有成功完成，不要担心。Git 中的所有内容都可以恢复，如果您发现无法恢复，您可以随时创建一个新分支并重新开始。

<!--
## Find more Git learning resources

- Get a complete introduction to Git in the <i class="fa fa-youtube-play youtube" aria-hidden="true"></i> [Git for GitLab](https://www.youtube.com/watch?v=4lxvVj7wlZw) beginner's course (1h 33m).
- Find other tutorials about Git and GitLab on the [tutorials page](index.md).
-->
