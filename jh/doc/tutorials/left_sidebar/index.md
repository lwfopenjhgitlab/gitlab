---
stage: none
group: Tutorials
info: For assistance with this tutorial, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments-to-other-projects-and-subjects.
---

# 教程：使用左侧边栏导航 **(FREE)**

> 引入于 16.0 版本。

按照本教程学习如何使用新的左侧边栏来导航 UI。

<!--
Provide feedback in
[issue 409005](https://gitlab.com/gitlab-org/gitlab/-/issues/409005).
-->

## 启用新的左侧边栏

查看新侧边栏：

1. 在左侧边栏中，选择您的头像。
1. 打开 **新版导航栏** 开关。

要关闭此侧边栏，请返回您的头像并关闭切换开关。

## 左侧边栏的布局

左侧边栏的顶部有几个快捷方式。使用这些快捷方式可以显示和隐藏左侧边栏、创建新项目、搜索和查看您的个人资料。您还可以查看议题列表、合并请求和待办事项。

![Top of sidebar](img/sidebar_top_v16_1.png)

如果您隐藏了左侧边栏，则可以通过将光标悬停在极狐GitLab 窗口的左边缘上来临时显示。

左侧边栏的下一个区域会根据您正在查看的信息而变化。例如，您可能正在查看项目、探索项目或群组，或者查看您的个人资料。
使用此区域可以切换到左侧边栏的其他区域。

![Context switching](img/sidebar_middle_v16_1.png)

左侧边栏的其余部分将根据您选择的选项进行设置。例如，如果您在一个项目中，则侧边栏是特定于项目的。

![Project-specific options](img/sidebar_bottom_v16_1.png)

## 找到您的项目

现在让我们回顾一下您将使用左侧边栏执行的一些常见任务。

首先找到您要进行相关工作的项目。

1. 要浏览所有可用项目，请在左侧栏中选择 **探索**：

   ![Explore](img/explore_v16_0.png)

1. 在右侧的项目列表上方，输入搜索条件。搜索会找到具有匹配描述的项目。

   ![Search projects](img/search_projects_v16_0.png)

1. 找到所需项目后，选择项目名称。左侧边栏现在显示特定项目的选项。

   ![Project-specific options](img/project_selected_v16_0.png)

## 自定义侧边栏

如果您倾向于经常使用某些功能，则可以钉选菜单项。

1. 展开这些部分，直到您看到要固定的项目。
1. 悬停并选择图钉 (**{thumbtack}**)。

   ![pin](img/pin_v16_0.png)

菜单项显示在 **已钉选** 部分：

![pinned item](img/pinned_v16_0.png)

## 使用更集中的视图

在左侧边栏上，您还可以选择更专注于您有权访问的区域的视图。
将视图更改为 **您的工作**：

![Your work](img/your_work_v16_0.png)

## 跳转至管理中心

您也可在左侧边栏中找到管理中心：

![Admin Area](img/admin_area_v16_0.png)
