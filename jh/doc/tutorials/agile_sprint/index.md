---
stage: none
group: Tutorials
info: For assistance with this tutorial, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments-to-other-projects-and-subjects.
---

# 教程：使用极狐GitLab 运行敏捷迭代

要在极狐GitLab 中运行敏捷开发迭代，您需要使用多个协同工作的极狐GitLab 功能。

从极狐GitLab 运行敏捷迭代：

1. 创建一个群组。
1. 创建一个项目。
1. 设置迭代周期。
1. 创建范围标记。
1. 创建您的史诗和议题。
1. 创建议题看板。

创建这些核心组件后，您可以开始运行迭代。

## 创建群组

迭代周期是在群组级别创建的，因此如果您还没有，请先[创建一个](../../user/group/manage.md#create-a-group)。

您可以使用群组同时管理一个或多个相关项目。
您可以将用户添加为群组中的成员，并为他们分配角色。角色确定每个用户对群组中项目的[权限级别](../../user/permissions.md)。
成员资格自动级联到所有子组和项目。

## 创建项目

现在在您的群组中[创建一个或多个项目](../../user/project/index.md#create-a-project)。
创建项目有几种不同的方法。项目包含您的代码和流水线，但也包含用于计划即将进行的代码更改的议题。

## 设置迭代周期

在开始创建史诗或议题之前，创建一个[迭代周期](../../user/group/iterations/index.md#iteration-cadences)。
迭代周期包含用于计划和报告议题的单个连续迭代时间框。

创建迭代周期时，您可以决定是自动管理迭代，还是禁用自动调度而[手动管理迭代](../../user/group/iterations/index.md#manual-iteration-management)。

与成员资格类似，迭代会向下级联您的群组、子组和项目层次结构。如果您的团队跨多个群组、子组和项目工作，请在包含团队议题的所有项目共享的最顶级群组中创建迭代周期，如下图所示。

```mermaid
graph TD
    Group --> SubgroupA --> Project1
    Group --> SubgroupB --> Project2
    Group --> IterationCadence
```

## 创建范围标记

您还应该在创建迭代周期的同一群组中[创建范围标记](../../user/project/labels.md)。标记可帮助您分组史诗、议题和合并请求，并帮助您可视化看板中的议题工作流。例如，您可以使用范围标签，如 `workflow::planning`、`workflow::ready for development`、`workflow::in development` 和 `workflow::complete` 来指示议题的状态。您还可以利用范围标签来表示议题或史诗的类型，例如 `type::feature`、`type::defect` 和 `type::maintenance`。

## 创建您的史诗和议题

现在您可以开始规划您的迭代。首先在创建迭代周期的群组中创建[史诗](../../user/group/epics/index.md)，然后在您的一个或多个项目中创建子[议题](../../user/project/issues/index.md)，根据需要为它们添加标记。

## 创建议题看板

[议题看板](../../user/project/issue_board.md)帮助您计划即将到来的迭代，或可视化当前正在进行的迭代的工作流。您可以根据标记、指派人、迭代或里程碑创建列表。您还可以按多个属性过滤看板，并按其史诗对议题进行分组。

在您创建迭代周期和标记的群组中，[创建议题看板](../../user/project/issue_board.md#create-an-issue-board)，并将其命名为“迭代计划”。然后，为每个迭代创建列表，您可以将议题从“开放中”列表拖到迭代列表中，为即将到来的迭代安排它们。

要可视化当前迭代中议题的工作流程，请创建另一个名为“当前迭代”的议题看板。创建看板时：

1. 选择 **编辑看板**。
1. 在 **迭代** 旁边，选择 **编辑**。
1. 从下拉列表中选择 **当前迭代**。
1. 选择 **保存更改**。

您的看板现在只会显示当前迭代中的议题。
您可以开始为之前创建的每个 `workflow::...` 标记添加列表。

现在您已准备好开始开发。
