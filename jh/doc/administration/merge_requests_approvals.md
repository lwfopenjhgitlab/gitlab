---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
---

# 合并请求批准 **(PREMIUM SELF)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/39060) in GitLab 12.8.
-->

合并请求批准规则可防止用户覆盖项目级别的某些设置。
在实例级别启用时，以下设置级联并且不能再更改：

- 在项目中
- 在群组中。14.5 版本中默认启用了级联到群组。

为实例启用合并请求批准设置：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **推送规则**。
1. 展开 **合并请求批准**。
1. 选择必需的选项。
1. 单击 **保存修改**。

## 可用的规则

可以在实例级别设置的合并请求批准设置是：

- **阻止作者批准**。防止项目维护者允许作者合并他们自己的合并请求。
- **防止添加提交的用户批准**。如果用户已向源分支提交了任何提交，则阻止项目维护者允许用户批准合并请求。
- **防止在项目和合并请求中编辑批准规则**。防止用户在项目设置或单个合并请求中修改核准人列表。

另请参阅以下受实例级规则影响的规则：

- [项目合并申请批准规则](../user/project/merge_requests/approvals/index.md)。
- [群组合并请求批准规则](../user/group/manage.md#group-merge-request-approval-settings)，可用于 13.9 及更高版本。
