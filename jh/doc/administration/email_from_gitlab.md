---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto, reference
---

# 从极狐GitLab 发送邮件**(PREMIUM SELF)**

极狐GitLab 为管理员提供了一个工具，用于直接从管理中心向所有用户，或所选群组或项目的用户发送电子邮件。用户通过他们的主电子邮件地址接收电子邮件。

有关来自极狐GitLab 的电子邮件通知的信息，请阅读[极狐GitLab 通知电子邮件](../user/profile/notifications.md)。

## 用例

- 通知您的用户有关新项目、新功能或新产品发布的信息。
- 通知您的用户有关新部署的信息，或者由于特定原因预计会出现停机。

## 从极狐GitLab 向用户发送电子邮件

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 选择 **向用户发送电子邮件**。

   ![administrators](img/email1.png)

1. 撰写电子邮件并选择将其发送到何处（所有用户或所选群组或项目的用户）。电子邮件正文仅支持纯文本消息。不支持 HTML、Markdown 和其它富文本格式，它们以纯文本形式发送给用户。

   ![compose an email](img/email2.png)

NOTE:
从 13.0 版本开始，电子邮件通知只能每 10 分钟发送一次。这有助于最大限度地减少性能问题。

## 退订电子邮件

用户可以通过点击电子邮件中的取消订阅链接，选择取消订阅来自极狐GitLab 的电子邮件。取消订阅是未经身份验证的，以保持此功能简单。

退订时，用户会收到一封电子邮件通知，说明发生了退订。
提供取消订阅选项的端点存在速率限制。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
