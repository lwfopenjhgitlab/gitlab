---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 为接收电子邮件设置 Postfix **(FREE SELF)**

本文档将引导您完成在 Ubuntu 上使用 IMAP 身份验证设置基本 Postfix 邮件服务器的步骤，与[接收电子邮件](incoming_email.md)一起使用。

本文假设您使用的电子邮件地址是 `incoming@gitlab.example.com`，即主机 `gitlab.example.com` 上的用户名`incoming`。执行示例代码片段时不要忘记将其更改为您的实际主机。

## 配置服务器防火墙

1. 打开服务器上的 25 端口，以便人们可以通过 SMTP 向服务器发送电子邮件。
1. 如果邮件服务器与运行极狐GitLab 的服务器不同，请打开服务器上的 143 端口，以便极狐GitLab 可以通过 IMAP 从服务器读取电子邮件。

## 安装包

1. 如果尚未安装 `postfix` 包，请安装它：

   ```shell
   sudo apt-get install postfix
   ```

   当要求选择环境时，选择 “Internet 站点”。当要求确认主机名时，请确保它与 `gitlab.example.com` 匹配。

1. 安装 `mailutils` 包。

   ```shell
   sudo apt-get install mailutils
   ```

## 创建用户

1. 为接收电子邮件创建用户。

   ```shell
   sudo useradd -m -s /bin/bash incoming
   ```

1. 为此用户设置密码。

   ```shell
   sudo passwd incoming
   ```

   一定不要忘记此密码，后面会用到它。

<a id="test-the-out-of-the-box-setup"></a>

## 测试开箱即用的设置

1. 连接到本地 SMTP 服务器：

   ```shell
   telnet localhost 25
   ```

   您应该会看到如下提示：

   ```shell
   Trying 127.0.0.1...
   Connected to localhost.
   Escape character is '^]'.
   220 gitlab.example.com ESMTP Postfix (Ubuntu)
   ```

   如果您收到 `Connection refused` 错误，请验证 `postfix` 是否正在运行：

   ```shell
   sudo postfix status
   ```

   如果不是，请启动它：

   ```shell
   sudo postfix start
   ```

1. 通过在 SMTP 提示符中输入以下内容，向新的 `incoming` 用户发送一封电子邮件来测试 SMTP：

   ```plaintext
   ehlo localhost
   mail from: root@localhost
   rcpt to: incoming@localhost
   data
   Subject: Re: Some issue

   Sounds good!
   .
   quit
   ```

   NOTE:
   `.` 是单独一行的文字句点。

   如果您在输入 `rcpt to: incoming@localhost` 后收到错误，那么您的 Postfix `my_network` 配置不正确。该错误将显示 "Temporary lookup failure"。查看[配置 Postfix 以接收来自 Internet 的电子邮件](#configure-postfix-to-receive-email-from-the-internet)。

1. 检查 `incoming` 用户是否收到了电子邮件：

   ```shell
   su - incoming
   mail
   ```

   您应该看到这样的输出：

   ```plaintext
   "/var/mail/incoming": 1 message 1 unread
   >U   1 root@localhost                           59/2842  Re: Some issue
   ```

   退出邮件应用程序：

   ```shell
   q
   ```

1. 退出 `incoming` 帐户，并恢复为 `root`：

   ```shell
   logout
   ```

## 将 Postfix 配置为使用 Maildir 样式的邮箱

Courier（我们稍后安装它以添加 IMAP 身份验证）要求邮箱具有 Maildir 格式，而不是 mbox。

1. 将 Postfix 配置为使用 Maildir 样式的邮箱：

   ```shell
   sudo postconf -e "home_mailbox = Maildir/"
   ```

1. 重启 Postfix：

   ```shell
   sudo /etc/init.d/postfix restart
   ```

1. 测试新设置：

   1. 按照*[测试开箱即用设置](#test-the-out-of-the-box-setup)*的步骤 1 和 2 进行操作。
   1. 检查 `incoming` 用户是否收到了电子邮件：

      ```shell
      su - incoming
      MAIL=/home/incoming/Maildir
      mail
      ```

      您应该看到这样的输出：

      ```plaintext
      "/home/incoming/Maildir": 1 message 1 unread
      >U   1 root@localhost                           59/2842  Re: Some issue
      ```

      退出邮件应用程序：

      ```shell
      q
      ```

   如果 `mail` 返回错误 `Maildir: Is a directory`，那么您的 `mail` 版本不支持 Maildir 样式的邮箱。通过运行 `sudo apt-get install heirloom-mailx` 来安装 `heirloom-mailx`。然后，再次尝试上述步骤，将 `heirloom-mailx` 替换为 `mail` 命令。

1. 退出 `incoming` 帐户，并恢复为 `root`：

   ```shell
   logout
   ```

## 安装 Courier IMAP 服务器

1. 安装 `courier-imap` 包：

   ```shell
   sudo apt-get install courier-imap
   ```

   并启动 `imapd`：

   ```shell
   imapd start
   ```

1. `courier-authdaemon` 安装后没有启动。没有它，IMAP 身份验证将失败：

   ```shell
   sudo service courier-authdaemon start
   ```

   您还可以将 `courier-authdaemon` 配置为在启动时启动：

   ```shell
   sudo systemctl enable courier-authdaemon
   ```

<a id="configure-postfix-to-receive-email-from-the-internet"></a>

## 配置 Postfix 接收来自 Internet 的电子邮件

1. 让 Postfix 知道它应该考虑本地的域：

   ```shell
   sudo postconf -e "mydestination = gitlab.example.com, localhost.localdomain, localhost"
   ```

1. 让 Postfix 知道它应该考虑作为 LAN 一部分的 IP：

   假设 `192.168.1.0/24` 是您的本地 LAN。如果您在同一本地网络中没有其他计算机，则可以安全地跳过此步骤。

   ```shell
   sudo postconf -e "mynetworks = 127.0.0.0/8, 192.168.1.0/24"
   ```

1. 配置 Postfix 在所有接口上接收邮件，包括互联网：

   ```shell
   sudo postconf -e "inet_interfaces = all"
   ```

1. 配置 Postfix 以使用 `+` 分隔符进行子寻址：

   ```shell
   sudo postconf -e "recipient_delimiter = +"
   ```

1. 重启 Postfix：

   ```shell
   sudo service postfix restart
   ```

## 测试最终设置

1. 在新设置下测试 SMTP：

   1. 连接到 SMTP 服务器：

      ```shell
      telnet gitlab.example.com 25
      ```

      您应该会看到如下提示：

      ```shell
      Trying 123.123.123.123...
      Connected to gitlab.example.com.
      Escape character is '^]'.
      220 gitlab.example.com ESMTP Postfix (Ubuntu)
      ```

      如果您收到 `Connection refused` 错误，请确保您的防火墙设置为允许端口 25 上的入站流量。

   1. 通过在 SMTP 提示符中输入以下内容，向 `incoming` 用户发送一封电子邮件以测试 SMTP：

      ```plaintext
      ehlo gitlab.example.com
      mail from: root@gitlab.example.com
      rcpt to: incoming@gitlab.example.com
      data
      Subject: Re: Some issue

      Sounds good!
      .
      quit
      ```

      NOTE:
      `.` 是单独一行的文字句点。

   1. 检查 `incoming` 用户是否收到了电子邮件：

      ```shell
      su - incoming
      MAIL=/home/incoming/Maildir
      mail
      ```

      您应该看到这样的输出：

      ```plaintext
      "/home/incoming/Maildir": 1 message 1 unread
      >U   1 root@gitlab.example.com                           59/2842  Re: Some issue
      ```

      退出邮件应用程序：

      ```shell
      q
      ```

   1. 退出 `incoming` 帐户，并恢复为 `root`：

      ```shell
      logout
      ```

1. 在新设置下测试 IMAP：

   1. 连接到 IMAP 服务器：

      ```shell
      telnet gitlab.example.com 143
      ```

      您应该会看到如下提示：

      ```shell
      Trying 123.123.123.123...
      Connected to mail.gitlab.example.com.
      Escape character is '^]'.
      - OK [CAPABILITY IMAP4rev1 UIDPLUS CHILDREN NAMESPACE THREAD=ORDEREDSUBJECT THREAD=REFERENCES SORT QUOTA IDLE ACL ACL2=UNION] Courier-IMAP ready. Copyright 1998-2011 Double Precision, Inc.  See COPYING for distribution information.
      ```

   1. 通过在 IMAP 提示符中输入以下内容，以 `incoming` 用户身份登录以测试 IMAP：

      ```plaintext
      a login incoming PASSWORD
      ```

      将 PASSWORD 替换为您之前在  `incoming`  用户上设置的密码。

      您应该看到这样的输出：

      ```plaintext
      a OK LOGIN Ok.
      ```

   1. 断开与 IMAP 服务器的连接：

      ```shell
      a logout
      ```

## 完成

如果所有测试都成功，则 Postfix 已设置完毕并准备好接收电子邮件！继续使用[接收电子邮件](incoming_email.md)指南来配置极狐GitLab。

<!--
---

_This document was adapted from <https://help.ubuntu.com/community/PostfixBasicSetupHowto>, by contributors to the Ubuntu documentation wiki._
-->
