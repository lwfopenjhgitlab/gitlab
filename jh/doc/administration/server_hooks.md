---
stage: Systems
group: Gitaly
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
disqus_identifier: 'https://docs.gitlab.com/ee/administration/custom_hooks.html'
---

# Git 服务器钩子 **(FREE SELF)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/196051) in GitLab 12.8 replacing Custom Hooks.
-->
> 从服务器钩子更名为 Git 服务器钩子于 15.6 版本。

Git 服务器钩子在极狐GitLab 服务器上运行自定义逻辑。您可以使用它们来运行与 Git 相关的任务，例如：

- 执行特定的提交策略。
- 根据仓库的状态执行任务。

Git 服务器钩子使用 `pre-receive`、`post-receive` 和 `update` [Git 服务器端钩子](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks#_server_side_hooks)。

极狐GitLab 管理员在极狐GitLab 服务器的文件系统上配置服务器钩子。如果您没有文件系统访问权限，服务器钩子的替代方案包括：

- [Webhooks](../user/project/integrations/webhooks.md)。
- [极狐GitLab CI/CD](../ci/index.md)。
- [推送规则](../user/project/repository/push_rules.md)，用于用户可配置的 Git 挂钩接口。

Geo 不会将服务器钩子复制到次要节点。

## 为仓库创建服务器钩子

**15.11 及更高的版本**

> 引入于 15.11 版本，`hooks set` 命令取代了直接文件系统访问。现有的 Git 钩子不需要在运行 `hooks set` 命令时迁移。

先决条件：

- [存储名称](gitaly/configure_gitaly.md#gitlab-requires-a-default-repository-storage)，Gitaly 配置文件的路径（在 Linux 软件包实例上默认为 `/var/opt/gitlab/gitaly/config.toml`），以及仓库的[相对路径](repository_storage_types.md#from-project-name-to-hashed-path)。

要为仓库设置服务器钩子：

1. 创建包含自定义钩子的 tarball：
   1. 编写代码使服务器钩子按预期运行。Git 服务器钩子可以使用任何编程语言。
      确保顶部的 [shebang](https://en.wikipedia.org/wiki/Shebang_(Unix)) 中包含语言类型。例如，脚本是 Ruby 语言，那么 shebang 应该是 `#!/usr/bin/env ruby`。

      - 要创建单个服务器钩子，请创建一个名称与钩子类型匹配的文件。例如，对于 `pre-receive` 服务器钩子，文件名应为 `pre-receive`，不带扩展名。
      - 要创建多个服务器钩子，请为与钩子类型匹配的钩子创建一个目录。例如，对于 `pre-receive` 服务器钩子，目录名称应为 `pre-receive.d`。将钩子的文件放入该目录中。

   1. 确保服务器钩子文件可执行，并且与备份文件样式（`*~`）不匹配。服务器钩子位于 tarball 根目录的 `custom_hooks` 目录中。
   1. 使用 tar 命令创建自定义钩子存档。例如，`tar -cf custom_hooks.tar custom_hooks`。
1. 运行带有所需选项的 `hooks set` 子命令，来设置仓库的 Git 钩子。例如，`cat hooks.tar | gitaly hooks set --storage <storage> --repository <relative path> --config <config path>`。

   - 连接到节点需要节点的有效 Gitaly 配置的路径，并使用 `--config` 选项配置。
   - 自定义挂钩 tarball 必须通过 `stdin` 传递。例如，`cat hooks.tar | gitaly hooks set --storage <storage> --repository <relative path> --config <config path>`。
1. 如果您使用 Gitaly 集群，则必须在所有 Gitaly 节点上运行 `hooks set` 子命令。有关更多信息，请参阅 [Gitaly 集群上的服务器钩子](#server-hooks-on-a-gitaly-cluster)。

如果正确实现了服务器钩子代码，应该会在下次触发 Git 钩子时执行创建。

**15.10 及更早的版本**

要为仓库创建服务器钩子：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 转到 **概览 > 项目** 并选择要添加服务器钩子的项目。
1. 在出现的页面，找到 **Gitaly 相对路径** 的值。此路径是服务器钩子必须位于的位置。
   - 如果您使用[哈希存储](repository_storage_types.md#hashed-storage)，请参阅[转换哈希存储路径](repository_storage_types.md#translate-hashed-storage-paths)，了解有关解释相对路径的信息。
   - 如果您不使用[哈希存储](repository_storage_types.md#hashed-storage)：
     - 对于 Linux 软件包安装实例，路径通常是 `/var/opt/gitlab/git-data/repositories/<group>/<project>.git`。
     - 对于源安装实例，路径通常是 `/home/git/repositories/<group>/<project>.git`。
1. 在文件系统上，在正确的位置创建一个名为 `custom_hooks` 的新目录。
1. 在新的 `custom_hooks` 目录中：
   - 要创建单个服务器钩子，请创建一个名称与钩子类型匹配的文件。例如，对于 `pre-receive` 服务器钩子，文件名应该是 `pre-receive` 并且没有扩展名。
   - 要创建许多服务器钩子，请为与钩子类型匹配的钩子创建一个目录。例如，对于 `pre-receive` 服务器钩子，目录名称应该是 `pre-receive.d`。将钩子的文件放在该目录中。
1. 使服务器钩子文件可执行并确保它们归 Git 用户所有。
1. 编写代码使服务器钩子功能如预期。Git 服务器钩子可以使用任何编程语言。确保顶部的 shebang 反映语言类型。例如，脚本在 Ruby 中，则 shebang 可能是 `#!/usr/bin/env ruby`。
1. 使钩子文件可执行，确保它由 Git 用户拥有，并确保它与备份文件样式 (`*~`) 不匹配。
1. 如果您使用 Gitaly 集群，则必须在所有 Gitaly 节点上运行 `hooks set` 子命令。有关更多信息，请参阅 [Gitaly 集群上的服务器钩子](#server-hooks-on-a-gitaly-cluster)。

如果正确实现了服务器钩子代码，应该会在下次触发 Git 钩子时执行创建。

### Gitaly 集群上的服务器钩子

如果您使用 [Gitaly 集群](gitaly/index.md)，单个仓库可能会复制到 Praefect 中的多个 Gitaly 存储。
因此，必须将钩子脚本复制到具有仓库副本的每个 Gitaly 节点。
为此，请按照为适用版本设置自定义仓库钩子的相同步骤操作，并对每个存储重复该步骤。

将脚本复制到的位置取决于仓库的存储位置：

- 对于 15.2 及更早的版本，Gitaly 集群使用极狐GitLab 应用程序报告的[哈希存储路径](repository_storage_types.md#hashed-storage)。
- 对于 15.3 及更高的版本，新仓库是使用 [Praefect 生成的副本路径](gitaly/index.md#praefect-generated-replica-paths-gitlab-150-and-later)创建的，而非哈希存储路径。可以通过[查询 Praefect 仓库元数据](../administration/gitaly/troubleshooting.md#view-repository-metadata)，使用 `-relative-path` 指定预期的极狐GitLab 哈希存储路径来识别副本路径。

## 为所有仓库创建全局服务器钩子

要创建适用于所有存储库的 Git 钩子，请设置全局服务器钩子。全局服务器钩子也适用于：

- [项目和群组 wiki](../user/project/wiki/index.md) 仓库。它们的存储目录名称采用 `<id>.wiki.git` 格式。
- [设计管理](../user/project/issues/design_management.md)项目下的仓库。它们的存储目录名称采用 `<id>.design.git` 格式。

### 选择服务器钩子目录

在创建全局服务器钩子之前，您必须为其选择一个目录。

对于 Linux 软件包安装实例，该目录在 `gitaly['custom_hooks_dir']` 下的 `gitlab.rb` 中设置。您可以：

- 通过取消注释，使用 `/var/opt/gitlab/gitaly/custom_hooks` 目录的默认建议。
- 添加您自己的设置。

对于源安装实例：

- 该目录在配置文件中设置。配置文件的位置取决于极狐GitLab 版本：
  - 对于 13.1 及更高版本，目录在 `[hooks]` 部分下的 `gitaly/config.toml` 中设置。但是，如果 `gitlab-shell/config.yml` 中的值是空白或不存在，极狐GitLab 会优先使用 `gitlab-shell/config.yml` 中的 `custom_hooks_dir` 值。
  - 对于 13.0 及更早版本，目录在 `gitlab-shell/config.yml` 中设置。
- 默认目录是 `/home/git/gitlab-shell/hooks`。

### 创建全局服务器钩子

为所有仓库创建全局服务器钩子：

1. 在极狐GitLab 服务器上，进入配置的全局服务器钩子目录。
1. 在配置的全局服务器钩子目录中，为匹配钩子类型的钩子创建一个目录。例如，对于 `pre-receive` 服务器钩子，目录名称应为 `pre-receive.d`。
1. 在这个新目录中，添加您的服务器钩子。Git 服务器钩子可以使用任何编程语言。确保顶部的 shebang 反映语言类型。例如，脚本在 Ruby 中，则 shebang 可能是 `#!/usr/bin/env ruby`。
1. 使钩子文件可执行，确保它由 Git 用户拥有，并确保它与备份文件模板 (`*~`) 不匹配。

如果服务器钩子代码被正确实现，它应该在下次触发 Git 钩子时执行。钩子按钩子类型子目录中文件名的字母顺序执行。

## 链式服务器钩子

极狐GitLab 可以在链式执行服务器钩子。极狐GitLab 按以下顺序搜索并执行服务器钩子：

- 内置极狐GitLab 服务器钩子。用户无法自定义这些服务器钩子。
- `<project>.git/custom_hooks/<hook_name>`：每个项目的钩子。保留此位置是为了向后兼容。
- `<project>.git/custom_hooks/<hook_name>.d/*`：每个项目钩子的位置。
- `<custom_hooks_dir>/<hook_name>.d/*`：所有可执行全局钩子文件的位置，除了编辑器备份文件。

在服务器钩子目录中：

- 钩子按字母顺序执行。
- 当钩子以非零值退出时停止执行。

## 服务器钩子可用的环境变量

您可以将任何环境变量传递给服务器钩子，但您应该只依赖受支持的环境变量。

所有服务器钩子都支持以下极狐GitLab 环境变量：

| 环境变量 | 描述                                                                                                                                                |
|:---------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| `GL_ID`              | 发起推送的用户的极狐GitLab 标识符或 SSH 密钥。例如，`user-2234`。                                                                               |
| `GL_PROJECT_PATH`    | （13.2 及更高版本）极狐GitLab 项目路径。                                                                                                               |
| `GL_PROTOCOL`        | （13.2 及更高版本）用于此更改的协议。以下之一：`http`（使用 HTTP 的 Git `push`）、`ssh`（使用 SSH 的 Git `push`）或 `web`（所有其他操作）。 |
| `GL_REPOSITORY`      | `project-<id>` 其中 `id` 是项目的 ID。                                                                                                        |
| `GL_USERNAME`        | 发起推送的用户的极狐GitLab 用户名。                                                                                                      |

`pre-receive` 和 `post-receive` 服务器钩子支持以下 Git 环境变量：

| 环境变量 | 描述                                                                                                                                                |
|:-----------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `GIT_ALTERNATE_OBJECT_DIRECTORIES` | 隔离环境中的备用对象目录。请参阅 [Git `receive-pack` 文档](https://git-scm.com/docs/git-receive-pack#_quarantine_environment)。 |
| `GIT_OBJECT_DIRECTORY`             | 隔离环境中的极狐GitLab 项目路径。 请参阅 [Git `receive-pack` 文档](https://git-scm.com/docs/git-receive-pack#_quarantine_environment)。          |
| `GIT_PUSH_OPTION_COUNT`            | [推送选项](../user/project/push_options.md)的数量。请参阅 [Git `pre-receive` 文档](https://git-scm.com/docs/githooks#pre-receive)。                                                         |
| `GIT_PUSH_OPTION_<i>`              | [推送选项](../user/project/push_options.md)的值，其中 `i` 从 `0` 到 `GIT_PUSH_OPTION_COUNT - 1`。请参阅 [Git `pre-receive` 文档](https://git-scm.com/docs/githooks#pre-receive)。      |

## 自定义错误消息

当提交被拒绝或 Git hook 期间发生错误时，您可以在 UI 中显示自定义错误消息。要显示自定义错误消息，您的脚本必须：

- 将自定义错误消息发送到脚本的 `stdout` 或 `stderr`。
- 使用 `GL-HOOK-ERR:` 为每条消息添加前缀，前缀之前不出现任何字符。

例如：

```shell
#!/bin/sh
echo "GL-HOOK-ERR: My custom error message.";
exit 1
```
