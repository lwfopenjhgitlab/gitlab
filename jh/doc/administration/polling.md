---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 轮询间隔倍数 **(FREE SELF)**

GitLab UI 按照适合资源的时间表，轮询不同资源（议题说明、议题标题、流水线状态等）的更新。

根据这些计划调整倍数以调整 GitLab UI 轮询更新的频率。 如果您将倍数设置为：

- 值大于 `1`，UI 轮询变慢。如果您发现来自大量客户端轮询更新的数据库负载问题，增加乘数可能是完全禁用轮询的一个很好的替代方法。例如，如果您将该值设置为 `2`，则所有轮询间隔都乘以 2，这意味着轮询发生的频率减半。
- `0` 和 `1` 之间的值，UI 轮询更频繁，因此更新发生更频繁。**不建议**。
- `0`，所有轮询被禁用。 在下一次轮询时，客户端停止轮询更新。

对于大多数 GitLab 安装，建议使用默认值 (`1`)。

## 配置

调整轮询间隔乘数：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧导航栏，选择 **设置 > 偏好设置**。
1. 展开 **轮询间隔倍数**。
1. 设置轮询间隔乘数的值。该乘数同时应用于所有资源。
1. 选择 **保存更改**。
