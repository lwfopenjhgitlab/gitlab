---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
description: "Set and configure Git protocol v2"
---

# 配置 Git 协议 v2 **(FREE)**

Git 协议 v2 以多种方式改进了 v1 有线协议，并且在极狐GitLab 中默认为 HTTP 请求启用。要启用 SSH，管理员需要进行额外配置。

<!--
More details about the new features and improvements are available in
the [Google Open Source Blog](https://opensource.googleblog.com/2018/05/introducing-git-protocol-version-2.html)
and the [protocol documentation](https://github.com/git/git/blob/master/Documentation/technical/protocol-v2.txt).
-->

## 要求

从客户端，必须安装 `git` `v2.18.0` 或更新版本。

在服务器端，如果我们要配置 SSH，我们需要设置 `sshd` 服务器以接受 `GIT_PROTOCOL` 环境。

在使用[极狐GitLab Helm Charts](https://docs.gitlab.cn/charts/) 和 [All-in-one Docker 镜像](../install/docker.md)的安装实例中，SSH 服务已经配置为接受 `GIT_PROTOCOL` 环境。用户无需再做任何事情。

对于 Linux 软件包安装实例和源安装实例，通过将此行添加到 `/etc/ssh/sshd_config` 文件手动更新服务器的 SSH 配置：

```plaintext
AcceptEnv GIT_PROTOCOL
```

配置完成后，重新启动 SSH daemon 使更改生效：

```shell
# CentOS 6 / RHEL 6
sudo service sshd restart

# All other supported distributions
sudo systemctl restart ssh
```

## 说明

要使用新协议，客户端需要将配置 `-c protocol.version=2` 传递给 Git 命令，或全局设置：

```shell
git config --global protocol.version 2
```

<a id="http-connections"></a>

### HTTP 连接

验证客户端使用 Git v2：

```shell
GIT_TRACE_CURL=1 git -c protocol.version=2 ls-remote https://your-gitlab-instance.com/group/repo.git 2>&1 | grep Git-Protocol
```

您应该看到 `Git-Protocol` header 已发送：

```plaintext
16:29:44.577888 http.c:657              => Send header: Git-Protocol: version=2
```

验证服务器使用 Git v2：

```shell
GIT_TRACE_PACKET=1 git -c protocol.version=2 ls-remote https://your-gitlab-instance.com/group/repo.git 2>&1 | head
```

使用 Git 协议 v2 的示例响应：

```shell
$ GIT_TRACE_PACKET=1 git -c protocol.version=2 ls-remote https://your-gitlab-instance.com/group/repo.git 2>&1 | head
10:42:50.574485 pkt-line.c:80           packet:          git< # service=git-upload-pack
10:42:50.574653 pkt-line.c:80           packet:          git< 0000
10:42:50.574673 pkt-line.c:80           packet:          git< version 2
10:42:50.574679 pkt-line.c:80           packet:          git< agent=git/2.18.1
10:42:50.574684 pkt-line.c:80           packet:          git< ls-refs
10:42:50.574688 pkt-line.c:80           packet:          git< fetch=shallow
10:42:50.574693 pkt-line.c:80           packet:          git< server-option
10:42:50.574697 pkt-line.c:80           packet:          git< 0000
10:42:50.574817 pkt-line.c:80           packet:          git< version 2
10:42:50.575308 pkt-line.c:80           packet:          git< agent=git/2.18.1
```

### SSH 连接

验证客户端使用 Git v2：

```shell
GIT_SSH_COMMAND="ssh -v" git -c protocol.version=2 ls-remote ssh://git@your-gitlab-instance.com/group/repo.git 2>&1 | grep GIT_PROTOCOL
```

您应该看到 `GIT_PROTOCOL` 环境变量被发送：

```plaintext
debug1: Sending env GIT_PROTOCOL = version=2
```

对于服务器端，您可以使用[与 HTTP 相同的示例](#http-connections)，将 URL 更改为使用 SSH。

<!--

### Observe Git protocol version of connections

For information on observing the Git protocol versions are being used in a production environment,
see the [relevant documentation](gitaly/monitoring.md#useful-queries).
-->
