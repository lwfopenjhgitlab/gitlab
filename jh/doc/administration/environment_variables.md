---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 环境变量 **(FREE SELF)**

极狐GitLab 公开某些可用于覆盖其默认值的环境变量。

人们通常使用以下配置文件，来配置极狐GitLab：

- `/etc/gitlab/gitlab.rb` 适用于 Linux 软件包安装实例。
- `gitlab.yml` 适用于源安装实例。

您可以使用以下环境变量来覆盖某些值：

## 支持的环境变量

| 变量                                  | 类型   | 描述                                                                                          |
|--------------------------------------------|---------|---------------------------------------------------------------------------------------------------------|
| `DATABASE_URL`                             | string  | 数据库 URL；格式为：`postgresql://localhost/blog_development`。                            |
| `ENABLE_BOOTSNAP`                          | string  | 切换 [Bootsnap](https://github.com/Shopify/bootsnap) 以加快初始 Rails 启动。默认情况下为非生产环境启用。设置为 `0` 以禁用。                                   |
| `EXTERNAL_VALIDATION_SERVICE_TIMEOUT`      | integer | [外部 CI/CD 流水线验证服务](external_pipeline_validation.md)的超时时间，以秒为单位。默认值为 `5`。 |
| `EXTERNAL_VALIDATION_SERVICE_URL`          | string  | [外部 CI/CD 流水线验证服务](external_pipeline_validation.md) 的 URL。                |
| `EXTERNAL_VALIDATION_SERVICE_TOKEN`        | string  | 用于使用[外部 CI/CD 流水线验证服务](external_pipeline_validation.md) 进行身份验证的 `X-Gitlab-Token`。 |
| `GITLAB_CDN_HOST`                          | string  | 设置 CDN 的基本 URL 以提供静态 assets（例如，`https://mycdnsubdomain.fictional-cdn.com`）。 |
| `GITLAB_EMAIL_DISPLAY_NAME`                | string  | GitLab 发送的电子邮件中 **From** 字段中使用的名称。                                          |
| `GITLAB_EMAIL_FROM`                        | string  | GitLab 发送的电子邮件中 **From** 字段中使用的电子邮件地址。                                  |
| `GITLAB_EMAIL_REPLY_TO`                    | string  | GitLab 发送的电子邮件中 **Reply-To** 字段中使用的电子邮件地址。                              |
| `GITLAB_EMAIL_SUBJECT_SUFFIX`              | string  | GitLab 发送的电子邮件中使用的电子邮件主题后缀。                                                 |
| `GITLAB_HOST`                              | string  | GitLab 服务器的完整 URL（包括 `http://` 或 `https://`）。                                 |
| `GITLAB_MARKUP_TIMEOUT`                    | string  | 由 `gitlab-markup` gem 执行的 `rest2html` 和 `pod2html` 命令的超时时间，以秒为单位。默认值为 `10`。 |
| `GITLAB_ROOT_PASSWORD`                     | string  | 在安装时设置 `root` 用户的密码。                                                  |
| `GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN` | string  | 设置用于 runner 的初始注册令牌。                                                  |
| `RAILS_ENV`                                | string  | Rails 环境；可以是 `production`、`development`、`staging` 或 `test` 之一。                 |
| `GITLAB_RAILS_CACHE_DEFAULT_TTL_SECONDS` | integer | 用于存储在 Rails 缓存中的条目的默认 TTL。默认值为 `28800`。引入于 15.3 版本。 |
| `GITLAB_CI_CONFIG_FETCH_TIMEOUT_SECONDS` | integer | 解析包含在 CI 配置中的远端的超时时间，以秒为单位。必须介于 `0` 和 `60` 之间。默认值为 `30`。引入于 15.11 版本。 |

<!--
## 添加更多变量

我们欢迎合并请求，以通过使用变量来配置更多设置。更改 `config/initializers/1_settings.rb` 文件，并使用命名方案 `GITLAB_#{name in 1_settings.rb in upper case}`。
-->

## Linux 软件包配置

要设置环境变量，遵循[相关文档说明](https://docs.gitlab.cn/omnibus/settings/environment-variables.html).

您可以通过将环境变量 `GITLAB_OMNIBUS_CONFIG` 添加到 `docker run` 命令，来预配置极狐GitLab Docker 镜像。
有关详细信息，请参阅[预配置 Docker 容器](../install/docker.md#pre-configure-docker-container)。