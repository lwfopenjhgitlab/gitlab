---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# 外部流水线验证 **(FREE SELF)**

您可以使用外部服务在创建流水线之前对其进行验证。

极狐GitLab 将 POST 请求发送到外部服务 URL，并将流水线数据作为有效负载。来自外部服务的响应代码确定 GitLab 应该接受还是拒绝流水线。 如果响应是：

- `200`，流水线被接受。
- `406`，流水线被拒绝。
- 其它返回码，流水线被接受并记录。

如果出现错误或请求超时，则接受流水线。

不会创建被外部验证服务拒绝的流水线，也不会出现在 GitLab UI 或 API 的流水线列表中。如果在被拒绝的 UI 中创建流水线，则无法运行流水线。显示外部验证失败。

## 配置外部流水线验证

要配置外部流水线验证，请添加 [`EXTERNAL_VALIDATION_SERVICE_URL` 环境变量](environment_variables.md)并将其设置为外部服务 URL。

默认情况下，对外部服务的请求会在 5 秒后超时。要覆盖默认值，请将 `EXTERNAL_VALIDATION_SERVICE_TIMEOUT` 环境变量设置为所需的秒数。 

## 负载 schema

```json
{
  "type": "object",
  "required" : [
    "project",
    "user",
    "credit_card",
    "pipeline",
    "builds",
    "total_builds_count",
    "namespace"
  ],
  "properties" : {
    "project": {
      "type": "object",
      "required": [
        "id",
        "path",
        "created_at",
        "shared_runners_enabled",
        "group_runners_enabled"
      ],
      "properties": {
        "id": { "type": "integer" },
        "path": { "type": "string" },
        "created_at": { "type": ["string", "null"], "format": "date-time" },
        "shared_runners_enabled": { "type": "boolean" },
        "group_runners_enabled": { "type": "boolean" }
      }
    },
    "user": {
      "type": "object",
      "required": [
        "id",
        "username",
        "email",
        "created_at"
      ],
      "properties": {
        "id": { "type": "integer" },
        "username": { "type": "string" },
        "email": { "type": "string" },
        "created_at": { "type": ["string", "null"], "format": "date-time" },
        "current_sign_in_ip": { "type": ["string", "null"] },
        "last_sign_in_ip": { "type": ["string", "null"] },
        "sign_in_count": { "type": "integer" }
      }
    },
    "credit_card": {
      "type": "object",
      "required": [
        "similar_cards_count",
        "similar_holder_names_count"
      ],
      "properties": {
        "similar_cards_count": { "type": "integer" },
        "similar_holder_names_count": { "type": "integer" }
      }
    },
    "pipeline": {
      "type": "object",
      "required": [
        "sha",
        "ref",
        "type"
      ],
      "properties": {
        "sha": { "type": "string" },
        "ref": { "type": "string" },
        "type": { "type": "string" }
      }
    },
    "builds": {
      "type": "array",
      "items": {
        "type": "object",
        "required": [
          "name",
          "stage",
          "image",
          "services",
          "script"
        ],
        "properties": {
          "name": { "type": "string" },
          "stage": { "type": "string" },
          "image": { "type": ["string", "null"] },
          "services": {
            "type": ["array", "null"],
            "items": { "type": "string" }
          },
          "script": {
            "type": "array",
            "items": { "type": "string" }
          }
        }
      }
    },
    "total_builds_count": { "type": "integer" },
    "namespace": {
      "type": "object",
      "required": [
        "plan",
        "trial"
      ],
      "properties": {
        "plan": { "type": "string" },
        "trial": { "type": "boolean" }
      }
    },
    "provisioning_group": {
      "type": "object",
      "required": [
        "plan",
        "trial"
      ],
      "properties": {
        "plan": { "type": "string" },
        "trial": { "type": "boolean" }
      }
    }
  }
}
```

`namespace` 字段仅在[专业版](https://about.gitlab.cn/pricing/)及更高版本中可用。
