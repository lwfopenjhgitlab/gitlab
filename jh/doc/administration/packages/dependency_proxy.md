---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 管理极狐GitLab 依赖代理 **(FREE SELF)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/7934) in [GitLab Premium](https://about.gitlab.com/pricing/) 11.11.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/273655) from GitLab Premium to GitLab Free in 13.6.
-->

极狐GitLab 可以用作各种常见包管理器的依赖代理。

本文是管理员文档。如果您想了解如何使用依赖代理，请参阅[用户指南](../../user/packages/dependency_proxy/index.md)。

极狐GitLab 依赖代理：

- 默认开启。
- 可由管理员关闭。
- 需要启用 [Puma Web 服务器](../operations/puma.md)。在 13.0 及更高版本中默认启用 Puma。

<a id="turn-off-the-dependency-proxy"></a>

## 关闭依赖代理

默认情况下启用依赖代理。如果您是管理员，则可以关闭依赖代理。要关闭依赖代理，请按照与极狐GitLab 安装相对应的说明进行操作：

- [Omnibus 安装](#omnibus-gitlab-installations)
- [Helm chart 安装](#helm-chart-installations)
- [源安装](#installations-from-source)

<a id="omnibus-gitlab-installations"></a>

### Omnibus 安装

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['dependency_proxy_enabled'] = false
   ```

1. 保存文件并[重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

<a id="helm-chart-installations"></a>

### Helm chart 安装

安装完成后，更新全局 `appConfig` 以关闭依赖代理：

```yaml
global:
  appConfig:
    dependencyProxy:
      enabled: false
      bucket: gitlab-dependency-proxy
      connection: {}
       secret:
       key:
```

有关详细信息，请参阅[使用 charts 全局配置](https://docs.gitlab.cn/charts/charts/globals.html#configure-appconfig-settings)。

<a id="installations-from-source"></a>

### 源安装

1. 安装完成后，配置 `config/gitlab.yml` 中的 `dependency_proxy` 部分。将 `enabled` 设置为 `false` 以关闭依赖代理：

   ```yaml
   dependency_proxy:
     enabled: false
   ```

1. [重新启动极狐GitLab](../restart_gitlab.md#installations-from-source "如何重新启动极狐GitLab")，使更改生效。

### 多节点极狐GitLab 安装

遵循每个 Web 和 Sidekiq 节点的 [Omnibus 安装](#omnibus-gitlab-installations)的步骤。

## 打开依赖代理

依赖代理默认开启，但管理员可以关闭。要打开依赖代理，请按照[关闭依赖代理](#turn-off-the-dependency-proxy)中的说明进行操作，但将 `enabled` 字段设置为 `true`。

## 更改存储路径

默认情况下，依赖代理文件存储在本地，但您可以更改默认本地位置，甚至使用对象存储。

### 更改本地存储路径

Omnibus 安装的依赖代理文件存储在 `/var/opt/gitlab/gitlab-rails/shared/dependency_proxy/` 下，源安装存储在 `shared/dependency_proxy/` 下（相对于 Git 主目录）。
更改本地存储路径：

**Omnibus 安装**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['dependency_proxy_storage_path'] = "/mnt/dependency_proxy"
   ```

1. 保存文件并[重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

**源安装**

1. 编辑 `config/gitlab.yml` 中的 `dependency_proxy` 部分：

   ```yaml
   dependency_proxy:
     enabled: true
     storage_path: shared/dependency_proxy
   ```

1. [重新启动极狐GitLab](../restart_gitlab.md#installations-from-source "如何重新启动 GitLab")，使更改生效。

<a id="using-object-storage"></a>

### 使用对象存储

您可以使用对象存储来存储依赖代理的 blob，而不是依赖本地存储。

[阅读有关在极狐GitLab 中使用对象存储的更多信息](../object_storage.md)。

NOTE:
在 13.2 及更高版本中，我们建议使用[合并对象存储设置](../object_storage.md#consolidated-object-storage-configuration)。本节介绍早期的配置格式。

**Omnibus 安装**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行（必要时取消注释）：

   ```ruby
   gitlab_rails['dependency_proxy_enabled'] = true
   gitlab_rails['dependency_proxy_storage_path'] = "/var/opt/gitlab/gitlab-rails/shared/dependency_proxy"
   gitlab_rails['dependency_proxy_object_store_enabled'] = true
   gitlab_rails['dependency_proxy_object_store_remote_directory'] = "dependency_proxy" # The bucket name.
   gitlab_rails['dependency_proxy_object_store_proxy_download'] = false        # Passthrough all downloads via GitLab instead of using Redirects to Object Storage.
   gitlab_rails['dependency_proxy_object_store_connection'] = {
     ##
     ## If the provider is AWS S3, uncomment the following
     ##
     #'provider' => 'AWS',
     #'region' => 'eu-west-1',
     #'aws_access_key_id' => 'AWS_ACCESS_KEY_ID',
     #'aws_secret_access_key' => 'AWS_SECRET_ACCESS_KEY',
     ##
     ## If the provider is other than AWS (an S3-compatible one), uncomment the following
     ##
     #'host' => 's3.amazonaws.com',
     #'aws_signature_version' => 4             # For creation of signed URLs. Set to 2 if provider does not support v4.
     #'endpoint' => 'https://s3.amazonaws.com' # Useful for S3-compliant services such as DigitalOcean Spaces.
     #'path_style' => false                    # If true, use 'host/bucket_name/object' instead of 'bucket_name.host/object'.
   }
   ```

1. 保存文件并[重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

**源安装**

1. 编辑 `config/gitlab.yml` 中的 `dependency_proxy` 部分（必要时取消注释）：

   ```yaml
   dependency_proxy:
     enabled: true
     ##
     ## The location where build dependency_proxy are stored (default: shared/dependency_proxy).
     ##
     # storage_path: shared/dependency_proxy
     object_store:
       enabled: false
       remote_directory: dependency_proxy  # The bucket name.
       #  proxy_download: false     # Passthrough all downloads via GitLab instead of using Redirects to Object Storage.
       connection:
       ##
       ## If the provider is AWS S3, use the following
       ##
         provider: AWS
         region: us-east-1
         aws_access_key_id: AWS_ACCESS_KEY_ID
         aws_secret_access_key: AWS_SECRET_ACCESS_KEY
         ##
         ## If the provider is other than AWS (an S3-compatible one), comment out the previous 4 lines and use the following instead:
         ##
         #  host: 's3.amazonaws.com'             # default: s3.amazonaws.com.
         #  aws_signature_version: 4             # For creation of signed URLs. Set to 2 if provider does not support v4.
         #  endpoint: 'https://s3.amazonaws.com' # Useful for S3-compliant services such as DigitalOcean Spaces.
         #  path_style: false                    # If true, use 'host/bucket_name/object' instead of 'bucket_name.host/object'.
   ```

1. [重新启动极狐GitLab](../restart_gitlab.md#installations-from-source "如何重新启动极狐GitLab")，使更改生效。

<a id="migrate-local-dependency-proxy-blobs-and-manifests-to-object-storage"></a>

#### 将本地依赖代理 blob 和清单迁移到对象存储

> 引入于 14.8 版本。

[配置对象存储](#using-object-storage)后，使用以下任务将现有的依赖代理 blob 和清单从本地存储迁移到远程存储。处理是在后台 worker 中完成的，不需要停机。

对于 Omnibus 实例：

```shell
sudo gitlab-rake "gitlab:dependency_proxy:migrate"
```

对于源安装实例：

```shell
RAILS_ENV=production sudo -u git -H bundle exec rake gitlab:dependency_proxy:migrate
```

您可以选择跟踪进度并使用 [PostgreSQL 控制台] (https://docs.gitlab.cn/omnibus/settings/database.html#connecting-to-the-bundled-postgresql-database)验证所有包是否已成功迁移：

- Omnibus 安装（14.1 及更早版本）：`sudo gitlab-rails dbconsole`
- Omnibus 安装（14.2 及更高版本）：`sudo gitlab-rails dbconsole --database main`
- 源安装：`sudo -u git -H psql -d gitlabhq_production`

验证 `objectstg`（其中 `file_store = '2'`）具有每个查询的所有依赖代理 blob 和清单的计数：

```shell
gitlabhq_production=# SELECT count(*) AS total, sum(case when file_store = '1' then 1 else 0 end) AS filesystem, sum(case when file_store = '2' then 1 else 0 end) AS objectstg FROM dependency_proxy_blobs;

total | filesystem | objectstg
------+------------+-----------
 22   |          0 |        22

gitlabhq_production=# SELECT count(*) AS total, sum(case when file_store = '1' then 1 else 0 end) AS filesystem, sum(case when file_store = '2' then 1 else 0 end) AS objectstg FROM dependency_proxy_manifests;

total | filesystem | objectstg
------+------------+-----------
 10   |          0 |        10
```

验证 `dependency_proxy` 文件夹中的磁盘上没有文件：

```shell
sudo find /var/opt/gitlab/gitlab-rails/shared/dependency_proxy -type f | grep -v tmp | wc -l
```

## 更改 JWT 过期时间

依赖代理遵循 [Docker v2 令牌身份验证流程](https://docs.docker.com/registry/spec/auth/token/)，向客户端发出 JWT 以用于拉取请求。令牌过期时间可使用应用程序设置 `container_registry_token_expire_delay` 进行配置。可以从 rails 控制台更改：

```ruby
# update the JWT expiration to 30 minutes
ApplicationSetting.update(container_registry_token_expire_delay: 30)
```

默认过期时间为 15 分钟。

## 在代理后面使用依赖代理

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_workhorse['env'] = {
    "http_proxy" => "http://USERNAME:PASSWORD@example.com:8080",
    "https_proxy" => "http://USERNAME:PASSWORD@example.com:8080"
   ```

1. 保存文件并[重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。
