---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab 软件包库管理**(FREE SELF)**

要将极狐GitLab 用作各种常见包管理器的私有仓库，请使用软件包库。
您可以构建和发布软件包，这些包可以作为下游项目中的依赖项使用。

## 支持的格式

软件包库支持以下格式：

| 软件包类型                                                      | GitLab 版本 |
|-------------------------------------------------------------------|----------------|
| Composer                                                          | 13.2+          |
| Conan                                                             | 12.6+          |
| Go                                                                | 13.1+          |
| [Maven](../../user/packages/maven_repository/index.md)            | 11.3+          |
| [npm](../../user/packages/npm_registry/index.md)                  | 11.7+          |
| NuGet                                                             | 12.8+          |
| [PyPI](../../user/packages/pypi_repository/index.md)              | 12.10+         |
| [Generic packages](../../user/packages/generic_packages/index.md) | 13.5+          |
| [Helm Charts](../../user/packages/helm_repository/index.md)       | 14.1+          |

## 速率限制

在下游项目中，将软件包作为依赖项下载时，许多请求是通过包 API 发出的。因此，您可能会达到强制的用户和 IP 速率限制。要解决此问题，您可以为 Packages API 定义特定的速率限制。<!--更多详细信息，请参见[软件包仓库速率限制](../../user/admin_area/settings/package_registry_rate_limits.md)。-->

## 更改存储路径

默认情况下，软件包存储在本地，但您可以更改默认本地位置，甚至使用对象存储。

### 更改本地存储路径

默认情况下，软件包存储在本地路径中，与极狐GitLab 安装方法相关：

- Omnibus：`/var/opt/gitlab/gitlab-rails/shared/packages/`
- 源安装：`/home/git/gitlab/shared/packages/`

要更改本地存储路径：

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下内容：

   ```ruby
   gitlab_rails['packages_storage_path'] = "/mnt/packages"
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production: &base
     packages:
       enabled: true
       storage_path: /mnt/packages
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

Docker 和 Kubernetes 不使用本地存储。

- 对于 Helm chart（Kubernetes）安装：改用对象存储。
- 对于 Docker 安装：`/var/opt/gitlab/` 目录已经安装在主机上的目录中。无需更改容器内的本地存储路径。

<a id="use-object-storage"></a>

### 使用对象存储

您可以使用对象存储来存储软件包，而不是依赖本地存储。

有关详细信息，请参阅如何使用[统一对象存储设置](../object_storage.md#consolidated-object-storage-configuration)。

<a id="migrating-local-packages-to-object-storage"></a>

### 将本地软件包迁移到对象存储

[配置对象存储](#use-object-storage)后，使用以下任务将现有软件包从本地存储迁移到远端存储。
进程在后台 worker 中完成，**不需要停机时间**。

1. 迁移软件包。


   **Omnibus**

   ```shell
   sudo gitlab-rake "gitlab:packages:migrate"
   ```

   **源安装**

   ```shell
   RAILS_ENV=production sudo -u git -H bundle exec rake gitlab:packages:migrate
   ```

1. 使用 PostgreSQL 控制台跟踪进度并验证所有包是否已成功迁移。

   **Omnibus** 14.1 及更早版本

   ```shell
   sudo gitlab-rails dbconsole
   ```

   **Omnibus** 14.2 及更高版本

   ```shell
   sudo gitlab-rails dbconsole --database main
   ```

   **源安装**

   ```shell
   RAILS_ENV=production sudo -u git -H psql -d gitlabhq_production
   ```

1. 使用以下 SQL 查询验证是否所有包都已迁移到对象存储。`objectstg` 的数量应与 `total` 相同：

   ```shell
   gitlabhq_production=# SELECT count(*) AS total, sum(case when file_store = '1' then 1 else 0 end) AS filesystem, sum(case when file_store = '2' then 1 else 0 end) AS objectstg FROM packages_package_files;

   total | filesystem | objectstg
   ------+------------+-----------
    34   |          0 |        34
   ```

1. 最后，确认磁盘上的 `packages` 目录中没有文件：

   **Omnibus**

   ```shell
   sudo find /var/opt/gitlab/gitlab-rails/shared/packages -type f | grep -v tmp | wc -l
   ```

   **源安装**

   ```shell
   sudo -u git find /home/git/gitlab/shared/packages -type f | grep -v tmp | wc -l
   ```
