---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 将极狐GitLab 置于只读状态 **(FREE SELF)**

NOTE:
在 13.9 及更高版本中，将极狐GitLab 置于只读状态的推荐方法是启用[维护模式](../administration/maintenance_mode/index.md)。

在某些情况下，您可能希望将极狐GitLab 置于只读状态。

## 将仓库设为只读

您要完成的第一件事是确保不能对您的仓库进行任何更改。有两种方法可以实现：

- 要么停止 Puma 以使内部 API 无法访问：

  ```shell
  sudo gitlab-ctl stop puma
  ```

- 或者，打开 Rails 控制台：

  ```shell
  sudo gitlab-rails console
  ```

  并将所有项目的仓库设置为只读：

  ```ruby
  Project.all.find_each { |project| project.update!(repository_read_only: true) }
  ```

  当您准备好恢复它时，可以使用以下命令执行此操作：

  ```ruby
  Project.all.find_each { |project| project.update!(repository_read_only: false) }
  ```

<a id="shut-down-the-gitlab-ui"></a>

## 关闭 UI

如果您不介意关闭 UI，那么最简单的方法是停止 `sidekiq` 和 `puma`，并且您有效地确保不能对极狐GitLab 进行任何更改：

```shell
sudo gitlab-ctl stop sidekiq
sudo gitlab-ctl stop puma
```

当您准备好恢复时：

```shell
sudo gitlab-ctl start sidekiq
sudo gitlab-ctl start puma
```

## 将数据库设为只读

如果要允许用户使用 UI，请确保数据库是只读的：

1. 执行[极狐GitLab 备份](../raketasks/backup_restore.md)，以防万一事情没有按预期进行。
1. 以管理员用户身份在控制台输入 PostgreSQL：

   ```shell
   sudo \
       -u gitlab-psql /opt/gitlab/embedded/bin/psql \
       -h /var/opt/gitlab/postgresql gitlabhq_production
   ```

1. 创建 `gitlab_read_only` 用户。密码设置为 `mypassword`，根据自己的喜好更改：

   ```sql
   -- NOTE: Use the password defined earlier
   CREATE USER gitlab_read_only WITH password 'mypassword';
   GRANT CONNECT ON DATABASE gitlabhq_production to gitlab_read_only;
   GRANT USAGE ON SCHEMA public TO gitlab_read_only;
   GRANT SELECT ON ALL TABLES IN SCHEMA public TO gitlab_read_only;
   GRANT SELECT ON ALL SEQUENCES IN SCHEMA public TO gitlab_read_only;

   -- Tables created by "gitlab" should be made read-only for "gitlab_read_only"
   -- automatically.
   ALTER DEFAULT PRIVILEGES FOR USER gitlab IN SCHEMA public GRANT SELECT ON TABLES TO gitlab_read_only;
   ALTER DEFAULT PRIVILEGES FOR USER gitlab IN SCHEMA public GRANT SELECT ON SEQUENCES TO gitlab_read_only;
   ```

1. 获取 `gitlab_read_only` 用户的哈希密码并复制结果：

   ```shell
   sudo gitlab-ctl pg-password-md5 gitlab_read_only
   ```

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加上一步的密码：

   ```ruby
   postgresql['sql_user_password'] = 'a2e20f823772650f039284619ab6f239'
   postgresql['sql_user'] = "gitlab_read_only"
   ```

1. 重新配置极狐GitLab 并重新启动 PostgreSQL：

   ```shell
   sudo gitlab-ctl reconfigure
   sudo gitlab-ctl restart postgresql
   ```

当您准备好恢复只读状态时，删除 `/etc/gitlab/gitlab.rb` 中添加的行，并重新配置极狐GitLab 并重新启动 PostgreSQL：

```shell
sudo gitlab-ctl reconfigure
sudo gitlab-ctl restart postgresql
```

验证所有工作正常后，从数据库中删除 `gitlab_read_only` 用户。
