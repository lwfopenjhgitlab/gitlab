---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 对象存储 **(FREE SELF)**

极狐GitLab 支持使用对象存储服务来保存多种类型的数据。
建议在 NFS 上使用它，并且通常在较大的设置中更好，因为对象存储通常具有更高的性能、可靠性和可扩展性。

要配置对象存储，您有两个选择：

- 推荐。[为所有对象类型配置单个存储连接](#configure-a-single-storage-connection-for-all-object-types-consolidated-form)：所有支持的对象类型共享一个凭据，称为统一形式。
- [配置每个对象类型，定义其自己的存储连接](#configure-each-object-type-to-define-its-own-storage-connection-storage-specific-form)：每个对象都定义了自己的对象存储连接和配置，称为特定存储形式。

  如果您已经在使用特定存储形式，请参阅如何[过渡到统一形式](#transition-to-consolidated-form)。

如果您在本地存储数据，请参阅如何[迁移到对象存储](#migrate-to-object-storage)。

<a id="supported-object-storage-providers"></a>

## 支持的对象存储提供商

极狐GitLab 与 Fog 库紧密集成，您可以查看哪些[提供商](https://fog.io/about/provider_documentation.html)可以与极狐GitLab 一起使用。

具体来说，极狐GitLab 已在多家对象存储提供商上经过供应商和客户的测试：

- [Amazon S3](https://aws.amazon.com/s3/) (不支持[对象锁定](https://docs.aws.amazon.com/AmazonS3/latest/userguide/object-lock.html)）
- [Google Cloud Storage](https://cloud.google.com/storage)
- [Digital Ocean Spaces](https://www.digitalocean.com/products/spaces)（S3 兼容）
- [Oracle Cloud Infrastructure](https://docs.oracle.com/en-us/iaas/Content/Object/Tasks/s3compatibleapi.htm)
- [OpenStack Swift (S3 compatible mode)](https://docs.openstack.org/swift/latest/s3_compat.html)
- [Azure Blob storage](https://learn.microsoft.com/en-us/azure/storage/blobs/storage-blobs-introduction)
- [MinIO](https://min.io/)（S3 兼容）
- 来自各种存储供应商的本地硬件和设备，其列表尚未正式确定。

<a id="configure-a-single-storage-connection-for-all-object-types-consolidated-form"></a>

### 为所有对象类型配置单个存储连接（统一形式）

大多数类型的对象，例如 CI 产物、LFS 文件和上传附件，都可以通过为具有多个存储桶的对象存储指定单个凭据，来保存在对象存储中。

使用统一形式配置对象存储有很多优点：

- 它可以简化您的极狐GitLab 配置，因为连接详细信息在对象类型之间共享。
- 它允许使用[加密的 S3 存储桶](#encrypted-s3-buckets)。
- 它使用正确的 `Content-MD5` header 将文件上传到 S3。

使用统一形式时，会自动启用直接上传。因此，只能使用以下提供商：

- [Amazon S3-compatible providers](#amazon-s3)
- [Google Cloud Storage](#google-cloud-storage-gcs)
- [Azure Blob storage](#azure-blob-storage)

统一形式配置不能用于备份或 Mattermost。备份可以单独配置服务器端加密。请参阅支持的对象存储类型的[完整列表](#configure-each-object-type-to-define-its-own-storage-connection-storage-specific-form)。

启用统一形式可以为所有对象类型启用对象存储。如果未指定所有存储桶，您可能会看到如下错误：

```plaintext
Object storage for <object type> must have a bucket specified
```

如果要对特定对象类型使用本地存储，可以[禁用特定功能的对象存储](#disable-object-storage-for-specific-features)。

<a id="configure-the-common-parameters"></a>

### 配置常用参数

在统一形式中，`object_store` 部分定义了一组通用参数。

| 设置           | 描述                       |
|-------------------|-----------------------------------|
| `enabled`         | 启用或禁用对象存储。 |
| `proxy_download`  | 设置为 `true` 以[启用代理所有提供的文件](#proxy-download)。选项允许减少出口流量，因为这允许客户端直接从远程存储下载而不是代理所有数据。 |
| `connection`      | 下面描述了各种[连接选项](#configure-the-connection-settings)。 |
| `storage_options` | 保存新对象时使用的选项，例如[服务器端加密](#server-side-encryption-headers)。在 13.3 版本中引入。 |
| `objects`         | [特定于对象的配置](#configure-the-parameters-of-each-object)。 |

有关示例，请参阅如何[使用统一形式和 Amazon S3](#full-example-using-the-consolidated-form-and-amazon-s3)。

<a id="configure-the-parameters-of-each-object"></a>

### 配置每个对象的参数

每个对象类型必须至少定义将存储它的存储桶名称。

下表列出了可以使用的有效 `objects`：

|  类型              | 描述                                                               |
|--------------------|----------------------------------------------------------------------------|
| `artifacts`        | [CI 产物](job_artifacts.md)                                           |
| `external_diffs`   | [合并请求差异](merge_request_diffs.md)                              |
| `uploads`          | [用户上传文件](uploads.md)                                                 |
| `lfs`              | [Git 大文件存储对象](lfs/index.md)                             |
| `packages`         | [项目软件包（例如 PyPI、Maven 或 NuGet）](packages/index.md) |
| `dependency_proxy` | [依赖项代理](packages/dependency_proxy.md)                    |
| `terraform_state`  | [Terraform 状态文件](terraform_state.md)                                |
| `pages`            | [Pages](pages/index.md)                                             |

在每个对象类型中，可以定义三个参数：

| 设置          | 是否必需？              | 描述                         |
|------------------|------------------------|-------------------------------------|
| `bucket`         | **{check-circle}** 是\* | 对象类型的存储桶名称。如果 `enabled` 设置为 `false`，则不需要设置。 |
| `enabled`        | **{dotted-circle}** 否 | 覆盖[常用参数](#configure-the-common-parameters)。     |
| `proxy_download` | **{dotted-circle}** 否 | 覆盖[常用参数](#configure-the-common-parameters)。     |

有关示例，请参阅如何[使用统一形式和 Amazon S3](#full-example-using-the-consolidated-form-and-amazon-s3)。

<a id="disable-object-storage-for-specific-features"></a>

#### 为特定功能禁用对象存储

如上所示，可以通过将 `enabled` 标志设置为 `false` 来为特定类型禁用对象存储。例如，禁用 CI 产物的对象存储：

```ruby
gitlab_rails['object_store']['objects']['artifacts']['enabled'] = false
```

如果完全禁用该功能，则不需要存储桶。例如，使用此设置禁用 CI 产物，则不需要存储桶：

```ruby
gitlab_rails['artifacts_enabled'] = false
```

<a id="configure-each-object-type-to-define-its-own-storage-connection-storage-specific-form"></a>

## 配置每个对象类型以定义其自己的存储连接（特定存储形式）

使用特定存储形式，每个对象都定义了自己的对象存储连接和配置。如果您使用的是 13.2 及更高版本，您应该[过渡到统一形式](#transition-to-consolidated-form)。

系统不支持使用非统一形式的[加密 S3 存储桶](#encrypted-s3-buckets)。如果您使用它，您可能会收到 [ETag 不匹配错误](#etag-mismatch)。

NOTE:
对于特定存储的形式，直接上传可能成为默认方式，因为不需要共享文件夹。

要在 13.1 及更早版本中配置对象存储，或者对于统一形式不支持的存储类型，请参阅以下指南：

| 对象存储类型 | 是否支持统一形式？ |
|---------------------|------------------------------------------|
| 备份 | **{dotted-circle}** 否 |
| [容器镜像库](packages/container_registry.md#use-object-storage)（可选） | **{dotted-circle}** 否 |
| [Mattermost](https://docs.mattermost.com/configure/file-storage-configuration-settings.html)| **{dotted-circle}** 否 |
| [自动伸缩 runner 缓存](https://docs.gitlab.cn/runner/configuration/autoscale.html#distributed-runners-caching) （可选） | **{dotted-circle}** 否 |
| [作业产物](job_artifacts.md#using-object-storage)，包括存档的作业日志 | **{check-circle}** 是 |
| [LFS 对象](lfs/index.md#storing-lfs-objects-in-remote-object-storage) | **{check-circle}** 是 |
| [上传文件](uploads.md#using-object-storage) | **{check-circle}** 是 |
| [合并请求差异](merge_request_diffs.md#using-object-storage) | **{check-circle}** 是 |
| [软件包](packages/index.md#use-object-storage) (optional feature) | **{check-circle}** 是 |
| [依赖项代理](packages/dependency_proxy.md#using-object-storage) (optional feature) | **{check-circle}** 是 |
| [Terraform 状态文件](terraform_state.md#using-object-storage) | **{check-circle}** 是 |
| [Pages 内容](pages/index.md#using-object-storage) | **{check-circle}** 是 |

<a id="configure-the-connection-settings"></a>

## 配置连接设置

统一形式和特定存储形式都必须配置一个连接。以下部分描述了可在 `connection` 设置中使用的参数。

<a id="amazon-s3"></a>

### Amazon S3

连接设置与 [fog-aws](https://github.com/fog/fog-aws) 提供的设置相匹配：

| 设置                                     | 描述                        | 默认值 |
|---------------------------------------------|------------------------------------|---------|
| `provider`                                  | 对于兼容的主机，始终使用 `AWS`。 | `AWS` |
| `aws_access_key_id`                         | AWS 凭证，或兼容的凭证。    | |
| `aws_secret_access_key`                     | AWS 凭证，或兼容的凭证。    | |
| `aws_signature_version`                     | 要使用的 AWS 签名版本。`2` 或 `4` 是有效选项。Digital Ocean Spaces 和其他提供商可能需要 `2`。 | `4` |
| `enable_signature_v4_streaming`             | 设置为 `true` 以启用具有 [AWS v4 签名](https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-streaming.html) 的 HTTP 分块传输。Oracle Cloud S3 需要将其设为 `false`。       | `true` |
| `region`                                    | AWS 区域。                        | |
| `host`                                      | 已废弃：使用 `endpoint` 代替。不使用 AWS 时的 S3 兼容主机。例如，`localhost` 或 `storage.example.com`。假定为 HTTPS 和端口 443。 | `s3.amazonaws.com` |
| `endpoint`                                  | 可在配置 S3 兼容服务时使用，例如 [MinIO](https://min.io)，方法是输入 URL，例如 `http://127.0.0.1:9000`。这优先于 `host`。始终使用 `endpoint` 作为统一形式。 | (可选) |
| `path_style`                                | 设置为 `true` 以使用 `host/bucket_name/object` 样式路径而不是 `bucket_name.host/object`。设置为 `true` 来使用 [MinIO](https://min.io)。对于 AWS S3，保留为 `false`。 | `false` |
| `use_iam_profile`                           | 设置为 `true` 以使用 IAM 配置文件而不是访问密钥。 | `false` |
| `aws_credentials_refresh_threshold_seconds` | 在 IAM 中使用临时凭证时设置[自动刷新阈值](https://github.com/fog/fog-aws#controlling-credential-refresh-time-with-iam-authentication)。 | `15` |

<a id="use-amazon-instance-profiles"></a>

#### 使用 Amazon 实例配置文件

您可以将极狐GitLab 配置为使用 Amazon Identity Access and Management（IAM）角色来设置 [Amazon 实例配置文件](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_switch-role-ec2.html)。使用时，极狐GitLab 会在每次访问 S3 存储桶时获取临时凭证，因此配置中不需要硬编码值。

先决条件：

- 极狐GitLab 必须能够连接到[实例元数据端点](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instancedata-data-retrieval.html)。
- 如果极狐GitLab [配置为使用互联网代理](https://docs.gitlab.com/omnibus/settings/environment-variables.html)，必须将端点 IP 地址添加到 `no_proxy` 列表中。

设置实例配置文件：

1. 创建具有必要权限的 IAM 角色。以下示例是名为 `test-bucket` 的 S3 存储桶的角色：

   ```json
   {
       "Version": "2012-10-17",
       "Statement": [
           {
               "Sid": "VisualEditor0",
               "Effect": "Allow",
               "Action": [
                   "s3:PutObject",
                   "s3:GetObject",
                   "s3:DeleteObject"
               ],
               "Resource": "arn:aws:s3:::test-bucket/*"
           }
       ]
   }
   ```

1. [附加此角色](https://aws.amazon.com/premiumsupport/knowledge-center/attach-replace-ec2-instance-profile/)到托管您的极狐GitLab 实例的 EC2 实例。
1. 将 `use_iam_profile` 极狐GitLab 配置选项设置为 `true`。

<a id="encrypted-s3-buckets"></a>

#### 加密的 S3 存储桶

当使用实例配置文件或统一形式进行配置时，Workhorse 会将文件正确上传到[默认启用 SSE-S3 或 SSE-KMS 加密](https://docs.aws.amazon.com/kms/latest/developerguide/services-s3.html)。不支持 AWS KMS 密钥和 SSE-C 加密，因为需要在每个请求中发送加密密钥。

<a id="server-side-encryption-headers"></a>

#### 服务器端加密 headers

在 S3 存储桶上设置默认加密是启用加密的最简单方法，但您可能希望[设置存储桶策略，确保仅上传加密对象](https://repost.aws/knowledge-center/s3-bucket-store-kms-encrypted-objects)。
为此，您必须将极狐GitLab 配置为在 `storage_options` 配置部分发送正确的加密 headers：

| 设置                             | 描述                              |
|-------------------------------------|------------------------------------------|
| `server_side_encryption`            | 加密模式（`AES256` 或 `aws:kms`）。 |
| `server_side_encryption_kms_key_id` | Amazon 资源名称。仅当在 `server_side_encryption` 中使用 `aws:kms` 时才需要。请参阅[关于使用 KMS 加密的官方文档](https://docs.aws.amazon.com/AmazonS3/latest/dev/UsingKMSEncryption.html)。 |

与默认加密的情况一样，这些选项仅在启用 Workhorse S3 客户端时才有效。必须满足以下两个条件之一：

- `use_iam_profile` 在连接设置中为 `true`。
- 正在使用统一形式。

如果在未启用 Workhorse S3 客户端的情况下使用服务器端加密 headers，则会发生 [ETag 不匹配错误](#etag-mismatch)。

### Oracle Cloud S3 连接设置

Oracle Cloud S3 必须确保使用以下设置：

| 设置                         | 值  |
|---------------------------------|---------|
| `enable_signature_v4_streaming` | `false` |
| `path_style`                    | `true`  |

如果 `enable_signature_v4_streaming` 设置为 `true`，您可能会看到 `production.log` 中的以下错误：

```plaintext
STREAMING-AWS4-HMAC-SHA256-PAYLOAD is not supported
```

<a id="google-cloud-storage-gcs"></a>

### Google Cloud Storage (GCS)

以下是 GCS 的有效连接参数：

| 设置                      | 描述       | 示例 |
|------------------------------|-------------------|---------|
| `provider`                   | Provider 名称。    | `Google` |
| `google_project`             | GCP 项目名称。 | `gcp-project-12345` |
| `google_json_key_location`   | JSON 密钥路径。    | `/path/to/gcp-project-12345-abcde.json` |
| `google_application_default` | 设置为 `true` 以使用 [Google Cloud Application Default Credentials](https://cloud.google.com/docs/authentication/production#automatically) 来查找服务帐户凭据。 | |
| `google_json_key_string`     | JSON key 字符串。  | `{ "type": "service_account", "project_id": "example-project-382839", ... }` |
| `google_application_default` | 设置为 `true` 以使用 [Google Cloud Application Default Credentials](https://cloud.google.com/docs/authentication#adc)，找到服务帐户凭据。 | |

极狐GitLab 读取 `google_json_key_location` 的值，然后是 `google_json_key_string`，最后是 `google_application_default`。它使用以上具有值的设置中的第一个。

服务帐户必须有权访问存储桶。有关详细信息，请参阅 [Cloud Storage 身份验证文档](https://cloud.google.com/storage/docs/authentication)。

NOTE:
不支持使用 [Cloud Key Management Service (KMS)](https://cloud.google.com/kms/docs) 的存储桶加密，使用它将导致 [ETag 不匹配错误](#etag-mismatch)。

#### GCS 示例

对于 Omnibus 安装实例，统一形式的 `connection` 设置示例如下：

```ruby
gitlab_rails['object_store']['connection'] = {
  'provider' => 'Google',
  'google_project' => '<GOOGLE PROJECT>',
  'google_json_key_location' => '<FILENAME>'
}
```

####  使用 ADC 的 GCS 示例 

> 引入于 13.6 版本。

Google Cloud 应用程序默认凭据 (ADC) 通常与极狐GitLab 一起使用，使用默认服务帐户，消除了为实例提供凭据的需要。例如：

```ruby
gitlab_rails['object_store']['connection'] = {
  'provider' => 'Google',
  'google_project' => '<GOOGLE PROJECT>',
  'google_application_default' => true
}
```

如果您使用 ADC，请确保：

- 您使用的服务帐户具有 [`iam.serviceAccounts.signBlob` 权限](https://cloud.google.com/iam/docs/reference/credentials/rest/v1/projects.serviceAccounts/signBlob)。通常这是通过将 `Service Account Token Creator` 角色授予服务帐户来完成的。
- 您的虚拟机具有[访问 Google Cloud API 的正确访问范围](https://cloud.google.com/compute/docs/access/create-enable-service-accounts-for-instances#changeserviceaccountandscopes)。如果机器没有正确的范围，错误日志可能会显示：

  ```markdown
  Google::Apis::ClientError (insufficientPermissions: Request had insufficient authentication scopes.)
  ```

<a id="azure-blob-storage"></a>

### Azure Blob 存储

> 引入于 13.4 版本。

尽管 Azure 使用 `container` 一词来表示 blob 的集合，但极狐GitLab 将 `bucket` 一词标准化。请务必在 `bucket` 设置中配置 Azure 容器名称。

Azure Blob 存储只能与[统一形式](#configure-a-single-storage-connection-for-all-object-types-consolidated-form)一起使用，因为一组凭据用于访问多个容器。不支持[特定存储形式](#configure-each-object-type-to-define-its-own-storage-connection-storage-specific-form)。有关更多详细信息，请参阅[如何转换为统一形式](#transition-to-consolidated-form)。

以下是 Azure 的有效连接参数。阅读 [Azure Blob 存储文档](https://learn.microsoft.com/en-us/azure/storage/blobs/storage-blobs-introduction)，了解更多信息。

| 设置                      | 描述    | 示例   |
|------------------------------|----------------|-----------|
| `provider`                   | Provider 名称。 | `AzureRM` |
| `azure_storage_account_name` | 用于访问存储的 Azure Blob 存储帐户的名称。 | `azuretest` |
| `azure_storage_access_key`   | 用于访问容器的存储帐户访问密钥。通常是一个以 base64 编码的 secret 512 位加密密钥。 | `czV2OHkvQj9FKEgrTWJRZVRoV21ZcTN0Nnc5eiRDJkYpSkBOY1JmVWpYbjJy\nNHU3eCFBJUQqRy1LYVBkU2dWaw==\n` |
| `azure_storage_domain`       | 用于连接 Azure Blob 存储 API 的域名（可选）。默认为 `blob.core.windows.net`。如果您使用的是 Azure 中国、Azure 德国或其他一些自定义 Azure 域，请设置此选项。 | `blob.core.windows.net` |

- 对于 Omnibus 安装实例，统一形式的 `connection` 设置示例如下：

  ```ruby
  gitlab_rails['object_store']['connection'] = {
    'provider' => 'AzureRM',
    'azure_storage_account_name' => '<AZURE STORAGE ACCOUNT NAME>',
    'azure_storage_access_key' => '<AZURE STORAGE ACCESS KEY>',
    'azure_storage_domain' => '<AZURE STORAGE DOMAIN>'
  }
  ```

- 对于源安装实例，Workhorse 还需要使用 Azure 凭据进行配置。在 Omnibus 安装中不需要，因为 Workhorse 设置填入了以前的设置。

  1. 编辑`/home/git/gitlab-workhorse/config.toml` 并添加或修改以下行：

     ```toml
     [object_storage]
       provider = "AzureRM"

     [object_storage.azurerm]
       azure_storage_account_name = "<AZURE STORAGE ACCOUNT NAME>"
       azure_storage_access_key = "<AZURE STORAGE ACCESS KEY>"
     ```

  如果您使用的是自定义 Azure 存储域名，则 `azure_storage_domain` **不必**在 Workhorse 配置中进行设置。此信息在极狐GitLab Rails 和 Workhorse 之间的 API 调用中交换。

### Storj Gateway (SJ)

NOTE:
Storj Gateway [不支持](https://github.com/storj/gateway-st/blob/4b74c3b92c63b5de7409378b0d1ebd029db9337d/docs/s3-compatibility.md)多线程复制（参见表中的 `UploadPartCopy`），必须[禁用多线程复制](#multi-threaded-copying)。

[Storj Network](https://www.storj.io/) 提供了一个兼容 S3 的 API 网关。使用以下配置示例：

```ruby
gitlab_rails['object_store']['connection'] = {
  'provider' => 'AWS',
  'endpoint' => 'https://gateway.storjshare.io',
  'path_style' => true,
  'region' => 'eu1',
  'aws_access_key_id' => 'ACCESS_KEY',
  'aws_secret_access_key' => 'SECRET_KEY',
  'aws_signature_version' => 2,
  'enable_signature_v4_streaming' => false
}
```

签名版本必须是 `2`。使用 `v4` 会导致 `HTTP 411 Length Required` 错误。

<a id="full-example-using-the-consolidated-form-and-amazon-s3"></a>

## 使用统一形式和 Amazon S3 的完整示例

以下示例使用 AWS S3 为所有支持的服务启用对象存储：

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行，替换您想要的值：

   ```ruby
   # Consolidated object storage configuration
   gitlab_rails['object_store']['enabled'] = true
   gitlab_rails['object_store']['proxy_download'] = true
   gitlab_rails['object_store']['connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-central-1',
     'aws_access_key_id' => '<AWS_ACCESS_KEY_ID>',
     'aws_secret_access_key' => '<AWS_SECRET_ACCESS_KEY>'
   }
   # OPTIONAL: The following lines are only needed if server side encryption is required
   gitlab_rails['object_store']['storage_options'] = {
     'server_side_encryption' => '<AES256 or aws:kms>',
     'server_side_encryption_kms_key_id' => '<arn:aws:kms:xxx>'
   }
   gitlab_rails['object_store']['objects']['artifacts']['bucket'] = 'gitlab-artifacts'
   gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = 'gitlab-mr-diffs'
   gitlab_rails['object_store']['objects']['lfs']['bucket'] = 'gitlab-lfs'
   gitlab_rails['object_store']['objects']['uploads']['bucket'] = 'gitlab-uploads'
   gitlab_rails['object_store']['objects']['packages']['bucket'] = 'gitlab-packages'
   gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = 'gitlab-dependency-proxy'
   gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = 'gitlab-terraform-state'
   gitlab_rails['object_store']['objects']['ci_secure_files']['bucket'] = 'gitlab-ci-secure-files'
   gitlab_rails['object_store']['objects']['pages']['bucket'] = 'gitlab-pages'
   ```

   如果您使用的是 [AWS IAM 配置文件](#use-amazon-instance-profiles)，请省略 AWS 访问密钥和 Secret 访问键值对。例如：

   ```ruby
   gitlab_rails['object_store']['connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-central-1',
     'use_iam_profile' => true
   }
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 将以下内容放入名为 `object_storage.yaml` 的文件中，用作 [Kubernetes Secret](https://docs.gitlab.cn/charts/charts/globals.html#connection)：

   ```yaml
   provider: AWS
   region: us-east-1
   aws_access_key_id: <AWS_ACCESS_KEY_ID>
   aws_secret_access_key: <AWS_SECRET_ACCESS_KEY>
   ```

   如果您使用的是 [AWS IAM 配置文件](#use-amazon-instance-profiles)，请省略 AWS 访问密钥和 Secret 访问键值对。例如：

   ```yaml
   provider: AWS
   region: us-east-1
   use_iam_profile: true
   ```

1. 创建 Kubernetes Secret：

   ```shell
   kubectl create secret generic -n <namespace> gitlab-object-storage --from-file=connection=object_storage.yaml
   ```

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml`：

   ```yaml
   global:
     appConfig:
       object_store:
         enabled: false
         proxy_download: true
         storage_options: {}
           # server_side_encryption:
           # server_side_encryption_kms_key_id
         connection:
           secret: gitlab-object-storage
       lfs:
         enabled: true
         proxy_download: true
         bucket: gitlab-lfs
         connection: {}
           # secret:
           # key:
       artifacts:
         enabled: true
         proxy_download: true
         bucket: gitlab-artifacts
         connection: {}
           # secret:
           # key:
       uploads:
         enabled: true
         proxy_download: true
         bucket: gitlab-uploads
         connection: {}
           # secret:
           # key:
       packages:
         enabled: true
         proxy_download: true
         bucket: gitlab-packages
         connection: {}
       externalDiffs:
         enabled: true
         when:
         proxy_download: true
         bucket: gitlab-mr-diffs
         connection: {}
       terraformState:
         enabled: true
         bucket: gitlab-terraform-state
         connection: {}
       ciSecureFiles:
         enabled: true
         bucket: gitlab-ci-secure-files
         connection: {}
       dependencyProxy:
         enabled: true
         proxy_download: true
         bucket: gitlab-dependency-proxy
         connection: {}
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

**Docker**

1. 编辑 `docker-compose.yml`：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           # Consolidated object storage configuration
           gitlab_rails['object_store']['enabled'] = true
           gitlab_rails['object_store']['proxy_download'] = true
           gitlab_rails['object_store']['connection'] = {
             'provider' => 'AWS',
             'region' => 'eu-central-1',
             'aws_access_key_id' => '<AWS_ACCESS_KEY_ID>',
             'aws_secret_access_key' => '<AWS_SECRET_ACCESS_KEY>'
           }
           # OPTIONAL: The following lines are only needed if server side encryption is required
           gitlab_rails['object_store']['storage_options'] = {
             'server_side_encryption' => '<AES256 or aws:kms>',
             'server_side_encryption_kms_key_id' => '<arn:aws:kms:xxx>'
           }
           gitlab_rails['object_store']['objects']['artifacts']['bucket'] = 'gitlab-artifacts'
           gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = 'gitlab-mr-diffs'
           gitlab_rails['object_store']['objects']['lfs']['bucket'] = 'gitlab-lfs'
           gitlab_rails['object_store']['objects']['uploads']['bucket'] = 'gitlab-uploads'
           gitlab_rails['object_store']['objects']['packages']['bucket'] = 'gitlab-packages'
           gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = 'gitlab-dependency-proxy'
           gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = 'gitlab-terraform-state'
           gitlab_rails['object_store']['objects']['ci_secure_files']['bucket'] = 'gitlab-ci-secure-files'
           gitlab_rails['object_store']['objects']['pages']['bucket'] = 'gitlab-pages'
   ```

   如果您使用的是 [AWS IAM 配置文件](#use-amazon-instance-profiles)，请省略 AWS 访问密钥和 Secret 访问键值对。例如：

   ```ruby
   gitlab_rails['object_store']['connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-central-1',
     'use_iam_profile' => true
   }
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   production: &base
     object_store:
       enabled: true
       proxy_download: true
       connection:
         provider: AWS
         aws_access_key_id: <AWS_ACCESS_KEY_ID>
         aws_secret_access_key: <AWS_SECRET_ACCESS_KEY>
         region: eu-central-1
       storage_options:
         server_side_encryption: <AES256 or aws:kms>
         server_side_encryption_key_kms_id: <arn:aws:kms:xxx>
       objects:
         artifacts:
           bucket: gitlab-artifacts
         external_diffs:
           bucket: gitlab-mr-diffs
         lfs:
           bucket: gitlab-lfs
         uploads:
           bucket: gitlab-uploads
         packages:
           bucket: gitlab-packages
         dependency_proxy:
           bucket: gitlab-dependency-proxy
         terraform_state:
           bucket: gitlab-terraform-state
         ci_secure_files:
           bucket: gitlab-ci-secure-files
         pages:
           bucket: gitlab-pages
   ```

   如果您使用的是 [AWS IAM 配置文件](#use-amazon-instance-profiles)，请省略 AWS 访问密钥和 Secret 访问键值对。例如：

   ```yaml
   connection:
     provider: AWS
     region: eu-central-1
     use_iam_profile: true
   ```

1. 编辑 `/home/git/gitlab-workhorse/config.toml` 并添加或修改以下行：

   ```toml
   [object_storage]
     provider = "AWS"

   [object_storage.s3]
     aws_access_key_id = "<AWS_ACCESS_KEY_ID>"
     aws_secret_access_key = "<AWS_SECRET_ACCESS_KEY>"
   ```

   如果您使用的是 [AWS IAM 配置文件](#use-amazon-instance-profiles)，请省略 AWS 访问密钥和 Secret 访问键值对。例如：

   ```yaml
   [object_storage.s3]
     use_iam_profile = true
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

<a id="migrate-to-object-storage"></a>

## 迁移到对象存储

要将现有本地数据迁移到对象存储，请参阅以下指南：

- [作业产物](job_artifacts.md#migrating-to-object-storage)，包括存档的作业日志
- [LFS 对象](lfs/index.md#migrating-to-object-storage)
- [上传文件](raketasks/uploads/migrate.md#migrate-to-object-storage)
- [合并请求差异](merge_request_diffs.md#using-object-storage)
- [软件包](packages/index.md#migrating-local-packages-to-object-storage)（可选功能）
- [依赖代理](packages/dependency_proxy.md#migrate-local-dependency-proxy-blobs-and-manifests-to-object-storage)
- [Terraform 状态文件](terraform_state.md#migrate-to-object-storage)
- [Pages 内容](pages/index.md#migrate-pages-deployments-to-object-storage)

<a id="transition-to-consolidated-form"></a>

## 过渡到统一形式

在 13.2 版本之前：

- 所有类型的对象（例如 CI/CD 产物、LFS 文件、上传附件等）的对象存储配置必须独立配置。
- 必须为每种类型复制对象存储连接参数，例如密码和端点 URL。

例如，Omnibus 安装实例可能具有以下配置：

```ruby
# Original object storage configuration
gitlab_rails['artifacts_object_store_enabled'] = true
gitlab_rails['artifacts_object_store_direct_upload'] = true
gitlab_rails['artifacts_object_store_proxy_download'] = true
gitlab_rails['artifacts_object_store_remote_directory'] = 'artifacts'
gitlab_rails['artifacts_object_store_connection'] = { 'provider' => 'AWS', 'aws_access_key_id' => 'access_key', 'aws_secret_access_key' => 'secret' }
gitlab_rails['uploads_object_store_enabled'] = true
gitlab_rails['uploads_object_store_direct_upload'] = true
gitlab_rails['uploads_object_store_proxy_download'] = true
gitlab_rails['uploads_object_store_remote_directory'] = 'uploads'
gitlab_rails['uploads_object_store_connection'] = { 'provider' => 'AWS', 'aws_access_key_id' => 'access_key', 'aws_secret_access_key' => 'secret' }
```

尽管这样提供了灵活性，因为它使极狐GitLab 可以跨不同的云提供商存储对象，但它也产生了额外的复杂性和不必要的冗余。由于极狐GitLab Rails 和 Workhorse 组件都需要访问对象存储，因此统一形式避免了过多的凭据重复。

如果原始形式中的所有行都被省略，则*仅*使用统一形式对象存储配置。要移动到统一形式，请删除原始配置（例如，`artifacts_object_store_enabled` 或 `uploads_object_store_connection`）。

## 将对象迁移到不同的对象存储提供商

您可能需要将对象存储中的极狐GitLab 数据迁移到不同的对象存储提供商。以下步骤向您展示了如何使用 [Rclone](https://rclone.org/) 执行此操作。

这些步骤假设您正在移动 `uploads` 存储桶，但同样的过程适用于其他存储桶。

先决条件：

- 选择运行 Rclone 的计算机。根据您迁移的数据量，Rclone 可能需要运行很长时间，因此您应避免使用可以进入省电模式的笔记本电脑或台式电脑。您可以使用极狐GitLab 服务器来运行 Rclone。

1. [安装](https://rclone.org/downloads/) Rclone。
1. 通过运行以下命令配置 Rclone：

   ```shell
   rclone config
   ```

   配置过程是交互式的。添加至少两个远端：一个用于您的数据当前所在的对象存储提供商（`old`），另一个用于您要移动到的提供商（`new`）。

1. 验证您是否可以读取旧数据。以下示例引用了 `uploads` 存储桶，但您的存储桶可能有不同的名称：

   ```shell
   rclone ls old:uploads | head
   ```

   此命令打印当前存储在 `uploads` 存储桶中的对象的部分列表。如果出现错误，或者列表为空，请返回并使用 `rclone config` 更新 Rclone 配置。

1. 执行初始复制。您不需要使您的极狐GitLab 服务器脱机。

   ```shell
   rclone sync -P old:uploads new:uploads
   ```

1. 第一次同步完成后，使用新对象存储提供商的 Web UI 或命令行界面，验证新存储桶中是否有对象。如果没有，或者如果您在运行 `rclone sync` 时遇到错误，请检查您的 Rclone 配置并重试。

在您完成至少一次从旧位置到新位置的成功 Rclone 复制后，安排维护并使您的极狐GitLab 服务器脱机。在维护窗口期间，您必须做两件事：

1. 执行最后的 `rclone sync` 运行，确定您的用户无法添加新对象，确保您不会在旧存储桶中留下任何东西。
1. 更新极狐GitLab 服务器的对象存储配置，使用新的提供商进行上传。

## 文件系统存储的替代方案

如果您正在横向扩展您的极狐GitLab 实例，或添加容错和冗余，您可能正在考虑删除对块或网络文件系统的依赖关系。
请参阅以下附加指南：

1. 确保 [`git` 用户主目录](https://docs.gitlab.cn/omnibus/settings/configuration.html#moving-the-home-directory-for-a-user)在本地磁盘上。
1. 配置 [SSH 密钥的数据库查找](operations/fast_ssh_key_lookup.md)，消除对共享 `authorized_keys` 文件的需要。
1. [防止作业日志使用本地磁盘](job_logs.md#prevent-local-disk-usage)。
1. [禁用 Pages 本地存储](pages/index.md#disable-pages-local-storage)。

## 故障排除

### 极狐GitLab 备份中不包含对象

如[我们的备份文档](../raketasks/backup_restore.md)中所述，极狐GitLab 备份中不包含对象。您可以改为使用对象存储提供程序启用备份。

### 使用单独的桶

极狐GitLab 推荐为每种数据类型使用单独的存储桶。这确保了极狐GitLab 存储的各种类型的数据之间没有冲突。有计划在未来启用单个存储桶。

使用 Omnibus 和源安装，可以将单个真实存储桶拆分为多个虚拟存储桶。如果您的对象存储桶名为 `my-gitlab-objects`，您可以将上传配置为进入 `my-gitlab-objects/uploads`，将产物配置为 `my-gitlab-objects/artifacts` 等。应用程序的行为就像这些是单独的存储桶一样。请注意，存储桶前缀的使用可能无法与 Helm 备份一起正常工作。

基于 Helm 的安装需要单独的存储桶来[处理备份恢复](https://docs.gitlab.cn/charts/advanced/external-object-storage/#lfs-artifacts-uploads-packages-external-diffs-terraform-state-dependency-proxy)。

### S3 API 兼容性问题

并非所有 S3 提供程序都与极狐GitLab 使用的 Fog 库完全兼容。`production.log` 中的错误：

```plaintext
411 Length Required
```

<a id="proxy-download"></a>

### 代理下载

客户端可以通过接收预先签名的限时 URL 或通过极狐GitLab 将数据从对象存储代理到客户端来下载对象存储中的文件。
直接从对象存储下载文件有助于减少极狐GitLab 需要处理的出口流量。

当文件存储在本地块存储或 NFS 上时，极狐GitLab 必须充当代理。
这不是对象存储的默认行为。

`proxy_download` 设置控制此行为：默认值通常为 `false`。
在每个用例的文档中验证这一点。如果您希望极狐GitLab 代理文件，请将其设置为 `true`。

当不代理文件时，极狐GitLab 会返回一个 HTTP 302 重定向，并带有一个预签名的、有时间限制的对象存储 URL。
这可能会导致以下一些问题：

- 如果极狐GitLab 使用非安全 HTTP 访问对象存储，客户端可能会生成 `https->http` 降级错误并拒绝处理重定向。对此的解决方案是极狐GitLab 使用 HTTPS。例如，LFS 会生成以下错误：

   ```plaintext
   LFS: lfsapi/client: refusing insecure redirect, https->http
   ```

- 客户端需要信任颁发对象存储证书的证书颁发机构，否则可能会返回常见的 TLS 错误，例如：

   ```plaintext
   x509: certificate signed by unknown authority
   ```

- 客户端需要对对象存储进行网络访问。网络防火墙可能会阻止访问。如果没有进行此访问，可能导致的错误包括：

   ```plaintext
   Received status code 403 from server: Forbidden
   ```

[软件包仓库文档](packages/index.md#using-object-storage)中特别提到了获取 `403 Forbidden` 响应，这是某些构建工具工作方式的副作用。

此外，在短时间内，用户可以与其他人共享预先签名的、有时间限制的对象存储 URL，而无需进行身份验证。此外，对象存储提供商和客户端之间可能会产生带宽费用。

<a id="etag-mismatch"></a>

### ETag 不匹配

使用默认的极狐GitLab 设置，一些对象存储后端（例如 MinIO 和阿里巴巴）可能会产生 `ETag mismatch` 错误。

如果您在 Amazon Web Services S3 中看到此 ETag 不匹配错误，这可能是由于[您的存储桶上的加密设置](https://docs.aws.amazon.com/AmazonS3/latest/API/RESTCommonResponseHeaders.html)。要解决此问题，您有两种选择：

- [使用统一形式](#configure-a-single-storage-connection-for-all-object-types-consolidated-form)。
- [使用 Amazon 实例配置文件](#use-amazon-instance-profiles)。

建议 MinIO 使用第一个选项。否则，MinIO 的解决方法是在服务器上使用 `--compat` 参数。

在未启用统一形式或实例配置文件的情况下，极狐GitLab Workhorse 使用未为其计算 `Content-MD5` HTTP header 的预签名 URL 将文件上传到 S3。为确保数据没有损坏，Workhorse 检查发送数据的 MD5 哈希是否等于从 S3 服务器返回的 ETag header。启用加密后，情况并非如此，这会导致 Workhorse 在上传期间报告 `ETag mismatch` 错误。

当统一形式：

- 与 S3 兼容的对象存储或 istance 配置文件一起使用，Workhorse 使用其具有 S3 凭据的内部 S3 客户端，以便它可以计算 `Content-MD5` header，从而不再需要比较从 S3 服务器返回的 ETag header。
- 不与 S3 兼容的对象存储一起使用，Workhorse 回退到使用预签名的 URL。

不支持使用 GCS [Cloud Key Management Service（KMS）](https://cloud.google.com/kms/docs) 加密存储桶，使用它会导致 ETag 不匹配错误。

### 多线程复制

极狐GitLab 使用 [S3 Upload Part Copy API](https://docs.aws.amazon.com/AmazonS3/latest/API/API_UploadPartCopy.html) 来加速桶内文件的复制。Ceph S3 [Kraken 11.0.2 之前](https://ceph.com/releases/kraken-11-0-2-released/)不支持这一点，并且在上传过程中复制文件时返回 404 错误。

可以使用 `:s3_multithreaded_uploads` 功能标志禁用该功能。要禁用该功能，请让具有 [Rails 控制台访问权限](feature_flags.md#how-to-enable-and-disable-features-behind-flags)的极狐GitLab 管理员运行以下命令：

```ruby
Feature.disable(:s3_multithreaded_uploads)
```
