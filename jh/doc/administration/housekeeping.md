---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 例行维护 **(FREE SELF)**

极狐GitLab 支持并自动化您当前仓库中的内务管理任务，例如：

- 压缩 Git 对象。
- 删除无法到达的对象。

## 配置例行维护

Git push 后，极狐GitLab 会自动在仓库上运行 `git gc` 和 `git repack`：

- [`git gc`](https://git-scm.com/docs/git-gc) 运行许多例行维护任务，例如：
  - 压缩 Git 对象，减少磁盘空间并提高性能。
  - 删除可能已经从对仓库的更改中创建的无法访问的对象，例如强制覆盖分支。
- [`git repack`](https://git-scm.com/docs/git-repack) 运行：
  - 根据[配置时间](#housekeeping-options)运行增量重新打包，将所有松散对象打包到一个新的包文件中，并修剪现在冗余的松散对象。
  - 根据[配置时间](#housekeeping-options)运行完整重新打包，将所有包文件和松散对象重新打包到一个新的包文件中，并删除旧的冗余的松散对象和包文件，还可以选择为新的包文件创建位图。

您可以更改发生的频率或将其关闭：

1. 在顶部栏上，选择 **菜单 > 管理员**。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **仓库维护**。
1. 在 **例行维护** 部分，配置[例行维护选项](#housekeeping-options)。
1. 选择 **保存更改**。

<a id="housekeeping-options"></a>

### 例行维护选项

提供以下例行维护选项：

- **启用自动仓库例行维护**：定期运行 `git repack` 和 `git gc`。如果您长时间禁用此设置，GitLab 服务器上的 Git 仓库访问会变慢，并且您的仓库会使用更多磁盘空间。
- **增量重新打包时间**：运行增量 `git repack` 之后的 Git 推送次数。
- **完全重新打包时间**：运行完整的 `git repack` 之后的 Git 推送次数。
- **Git GC 时间**：运行 `git gc` 之后的 Git 推送次数。

例如，请参见以下场景：

- 增量重新打包时间：10。
- 完全重新打包时间：50。
- Git GC 时间：200。

当：

- `pushes_since_gc` 值为 50，运行 `repack -A -l -d --pack-kept-objects`。
- `pushes_since_gc` 值为 200，运行 `git gc`。

例行维护还按照与 `git gc` 操作相同的时间表，从项目中删除未引用的 LFS 文件<!--[删除未引用的 LFS 文件](../raketasks/cleanup.md#remove-unreferenced-lfs-files)-->，为项目释放存储空间。

WARNING:
不鼓励在仓库文件夹<!--[仓库文件夹](repository_storage_types.md#from-project-name-to-hashed-path)-->中手动运行 `git gc` 或 `git repack` 命令。如果创建的包文件获得了不正确的访问权限（即由错误的用户拥有），浏览到项目页面可能会导致 `404` 和 `503` 错误。

## 例行维护如何处理池仓库

池仓库的例行维护与标准仓库不同。它最终由 Gitaly RPC `FetchIntoObjectPool` 执行。

调用它的当前调用堆栈：

1. `Repositories::HousekeepingService#execute_gitlab_shell_gc`
1. `Projects::GitGarbageCollectWorker#perform`
1. `Projects::GitDeduplicationService#fetch_from_source`
1. `ObjectPool#fetch`
1. `ObjectPoolService#fetch`
1. `Gitaly::FetchIntoObjectPoolRequest`

如果需要，从 [Rails 控制台](operations/rails_console.md) 手动调用它，您可以调用 `project.pool_repository.object_pool.fetch`。这是一项可能需要长时间运行的任务，尽管 Gitaly 会在大约 8 小时后超时。

WARNING:
不要在池仓库中运行 `git prune` 或 `git gc`！这可能会导致依赖于相关池的“真实”仓库中的数据丢失。
