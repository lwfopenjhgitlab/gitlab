---
stage: Systems
group: Gitaly
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Gitaly 参考 **(FREE SELF)**

Gitaly 通过 [TOML](https://github.com/toml-lang/toml) 配置文件进行配置。与源安装不同，在 Omnibus GitLab 中，您不会直接编辑此文件。

配置文件作为参数传递给 `gitaly` 可执行文件。通常由 Omnibus GitLab 或您的 init 脚本完成。

[示例配置文件](https://jihulab.com/gitlab-cn/gitaly/blob/master/config.toml.example)可以在 Gitaly 项目中找到。

## 格式

在顶层，`config.toml` 定义了下表中描述的项目。

| 名称 | 类型 | 是否必需 | 描述 |
| ---- | ---- | -------- | ----------- |
| `socket_path` | string | yes（如果未设置 `listen_addr`） | Gitaly 应该打开 Unix 套接字的路径。 |
| `listen_addr` | string | yes（如果未设置`socket_path`） | Gitaly 监听的 TCP 地址。 |
| `tls_listen_addr` | string | no | 用于 Gitaly 侦听的 TCP over TLS 地址。 |
| `bin_dir`    | string | yes    | 包含 Gitaly 可执行文件的目录。 |
| `prometheus_listen_addr` | string | no | Prometheus 指标的 TCP 侦听地址。如果未设置，则不会启动 Prometheus 侦听器。 |

例如：

```toml
socket_path = "/home/git/gitlab/tmp/sockets/private/gitaly.socket"
listen_addr = "localhost:9999"
tls_listen_addr = "localhost:8888"
bin_dir = "/home/git/gitaly"
prometheus_listen_addr = "localhost:9236"
```

### 验证

Gitaly 可以配置为拒绝在其 header 中不包含特定不记名令牌的请求。这是通过 TCP 服务请求时使用的安全措施：

```toml
[auth]
# A non-empty token enables authentication.
token = "the secret token"
```

当 `config.toml` 中的令牌设置不存在或为空字符串时，身份验证被禁用。

可以使用 `transitioning` 设置暂时禁用身份验证。这使您可以监视所有客户端是否正确进行身份验证，而不会导致尚未正确配置的客户端的服务中断：

```toml
[auth]
token = "the secret token"
transitioning = true
```

WARNING:
完成更改令牌设置后，请记住禁用 `transitioning`。

Prometheus 中的所有身份验证尝试都在 [`gitaly_authentications_total` 指标](monitoring.md#useful-queries)下进行计数。

### TLS

Gitaly 支持 TLS 加密。您需要携带自己的证书，因为这不是自动提供的。

| 名称 | 类型 | 是否必需 | 描述 |
| ---- | ---- | -------- | ----------- |
| `certificate_path` | string | no | 证书的路径。 |
| `key_path` | string | no | 密钥的路径。 |

```toml
tls_listen_addr = "localhost:8888"

[tls]
certificate_path = '/home/git/cert.cert'
key_path = '/home/git/key.pem'
```

<!--
[Read more](configure_gitaly.md#enable-tls-support) about TLS in Gitaly.
-->

### 存储

极狐GitLab 仓库被分组到称为存储的目录中，例如 `/home/git/repositories`。它们包含由极狐GitLab 管理的裸仓库，名称为 `default`。

这些名称和路径也在极狐GitLab 的 `gitlab.yml` 配置文件中定义。当您在与极狐GitLab（默认和推荐配置）相同的机器上运行 Gitaly 时，Gitaly 的 `config.toml` 中定义的存储路径必须与 `gitlab.yml` 中的存储路径匹配。

| 名称 | 类型 | 是否必需 | 描述 |
| ---- | ---- | -------- | ----------- |
| `storage` | array | yes | 一组存储分片。 |
| `path` | string | yes | 存储分片的路径。 |
| `name` | string | yes | 存储分片的名称。 |

例如：

```toml
[[storage]]
path = "/path/to/storage/repositories"
name = "my_shard"

[[storage]]
path = "/path/to/other/repositories"
name = "other_storage"
```

### Git

可以在配置文件的 `[git]` 部分设置以下值。

| 名称 | 类型 | 是否必需 | 描述 |
| ---- | ---- | -------- | ----------- |
| `bin_path` | string | no | Git 二进制文件的路径。如果未设置，则使用 `PATH` 解决。 |
| `catfile_cache_size` | integer | no | 缓存的 [cat-file 进程](#cat-file-cache)的最大数量。默认值为 `100`。 |
| `signing_key` | string | no | [GPG 签名密钥](configure_gitaly.md#configure-commit-signing-for-gitlab-ui-commits)的路径。如果未设置，Gitaly 不会签署使用 UI 所做的提交。引入于 15.4 版本。 |

<a id="cat-file-cache"></a>

#### `cat-file` 缓存

许多 Gitaly RPC 需要从存储库中查找 Git 对象。大多数时候，我们为此使用 `git cat-file --batch` 进程。为了获得更好的性能，Gitaly 可以在 RPC 调用中重复使用这些 `git cat-file` 进程。<!--Previously used processes are kept around in a
["Git cat-file cache"](https://about.gitlab.com/blog/2019/07/08/git-performance-on-nfs/#enter-cat-file-cache).-->
为了控制使用多少系统资源，我们设置了可以进入缓存的最大数量的 cat 文件进程。

默认限制为 100 个 `cat-file`，它们构成了一对 `git cat-file --batch` 和 `git cat-file --batch-check` 进程。如果您看到 "too many open files" 或无法创建新进程的错误，您可能需要降低此限制。

理想情况下，该数字应该足够大以处理正常流量。如果提高限制，则应测量前后的缓存命中率。如果命中率没有提高，则上限可能不会产生有意义的差异。这是一个用于查看命中率的 Prometheus 查询示例：

```plaintext
sum(rate(gitaly_catfile_cache_total{type="hit"}[5m])) / sum(rate(gitaly_catfile_cache_total{type=~"(hit)|(miss)"}[5m]))
```

### `gitaly-ruby`

Gitaly 进程使用一个或多个 `gitaly-ruby` 辅助进程来执行用 Ruby 而不是 Go 实现的 RPC。配置文件的 `[gitaly-ruby]` 部分包含这些 helper 进程的设置。

众所周知，这些进程偶尔会遭受内存泄漏。
当 Gitaly 的内存超过 `max_rss` 限制时，Gitaly 会重新启动其 `gitaly-ruby` helper。

| 名称 | 类型 | 是否必需 | 描述 |
| ---- | ---- | -------- | ----------- |
| `dir` | string | yes | 安装 `gitaly-ruby` 的路径（需要启动进程）。 |
| `max_rss` | integer | no | 触发 `gitaly-ruby` 重启的驻留设置大小限制，以字节为单位。默认为 `200000000`（200MB）。 |
| `graceful_restart_timeout` | string | no | 超过 `max_rss` 后，`gitaly-ruby` 进程之前的宽限期被强制终止。默认值为`10m`（10 分钟）。 |
| `restart_delay` | string | no | `gitaly-ruby` 内存在重启前必须保持高电平的时间。默认为 `5m`（5 分钟）。 |
| `num_workers` | integer | no | `gitaly-ruby` worker 进程的数量。如果出现 `ResourceExhausted` 错误，请尝试增加此数字。默认为 `2`，最小值为 `2`。|
| `linguist_languages_path` | string | no | 覆盖动态 `languages.json` 发现。 默认为空字符串（使用动态发现）。 |

示例：

```toml
[gitaly-ruby]
dir = "/home/git/gitaly/ruby"
max_rss = 200000000
graceful_restart_timeout = "10m"
restart_delay = "5m"
num_workers = 2
```

### 极狐GitLab Shell

由于历史原因，[极狐GitLab Shell](https://jihulab.com/gitlab-cn/gitlab-shell) 包含允许极狐GitLab 验证和响应 Git 推送的 Git 钩子。
由于 Gitaly “拥有” Git 推送，因此极狐GitLab Shell 必须与 Gitaly 一起安装。

| 名称 | 类型 | 是否必需 | 描述 |
| ---- | ---- | -------- | ----------- |
| `dir` | string | yes | 极狐GitLab Shell 的安装目录。 |

示例：

```toml
[gitlab-shell]
dir = "/home/git/gitlab-shell"
```

### Prometheus

您可以选择配置 Gitaly 以记录 Prometheus 中 GRPC 方法调用的直方图延迟。

| 名称 | 类型 | 是否必需 | 描述 |
| ---- | ---- | -------- | ----------- |
| `grpc_latency_buckets` | array | no | Prometheus 将每个观察结果存储在一个桶中，这意味着您将获得延迟的近似值。优化桶可以更好地控制近似的准确性。 |

示例：

```toml
prometheus_listen_addr = "localhost:9236"

[prometheus]
grpc_latency_buckets = [0.001, 0.005, 0.025, 0.1, 0.5, 1.0, 10.0, 30.0, 60.0, 300.0, 1500.0]
```

### Logging

以下值在 `[logging]` 部分下配置 Gitaly 中的日志记录。

| 名称 | 类型 | 是否必需 | 描述 |
| ---- | ---- | -------- | ----------- |
| `format` | string | no | 日志格式：`text` 或 `json`。默认值：`text`。 |
| `level`  | string | no | 日志级别：`debug`、`info`、`warn`、`error`、`fatal` 或 `panic`。默认值：`info`。 |
| `sentry_dsn` | string | no | Sentry DSN（数据源名称）用于异常监控。 |
| `sentry_environment` | string | no | [Sentry 环境](https://docs.sentry.io/product/sentry-basics/environments/)用于异常监控。 |
| `ruby_sentry_dsn` | string | no | Sentry DSN（数据源名称）用于`gitaly-ruby` 异常监控。 |

虽然主要的 Gitaly 应用程序日志会转到 `stdout`，但还有一些额外的日志文件会转到配置的目录，例如极狐GitLab Shell 日志。极狐GitLab Shell 不支持 `panic` 或 `trace` 级别的日志：

- `panic` 回退到 `error`。
- `trace` 回退到`debug`。
- 任何其他无效的日志级别默认为 `info`。

示例：

```toml
[logging]
level = "warn"
dir = "/home/gitaly/logs"
format = "json"
sentry_dsn = "https://<key>:<secret>@sentry.io/<project>"
ruby_sentry_dsn = "https://<key>:<secret>@sentry.io/<project>"
```

## Concurrency

您可以调整每个 RPC 端点的 `concurrency`。

| 名称 | 类型 | 是否必需 | 描述 |
| ---- | ---- | -------- | ----------- |
| `concurrency` | array | yes | 一组 RPC 端点。 |
| `rpc` | string | no | RPC 端点的名称（`/gitaly.RepositoryService/GarbageCollect`）。 |
| `max_per_repo` | integer | no | 每个仓库每个 RPC 的并发性。 |

示例：

```toml
[[concurrency]]
rpc = "/gitaly.RepositoryService/GarbageCollect"
max_per_repo = 1
```
