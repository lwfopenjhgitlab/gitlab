---
stage: Systems
group: Gitaly
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Gitaly 和 Gitaly 集群 **(FREE SELF)**

Gitaly 提供对 Git 仓库的高级 RPC 访问。极狐GitLab 使用它来读取和写入 Git 数据。

Gitaly 存在于每个极狐GitLab 安装实例中，并协调 Git 仓库的存储和检索。Gitaly 是：

- 在单个 Omnibus GitLab 实例上运行的后台服务。
- 根据扩展和可用性要求，分离到自己的实例并配置为完整的集群配置。

Gitaly 实现了客户端-服务器架构：

- Gitaly 服务器是运行 Gitaly 本身的任何节点。
- Gitaly 客户端是运行向 Gitaly 服务器发出请求的进程的任何节点。Gitaly 客户端也称为 Gitaly 消费者，包括：
  - Rails 应用
  - Shell
  - Workhorse
  - Elasticsearch Indexer

Gitaly 仅管理极狐GitLab 的 Git 仓库访问。其他类型的极狐GitLab 数据不能使用 Gitaly 访问。

极狐GitLab 通过配置的[仓库存储](../repository_storage_paths.md)访问[仓库](../../user/project/repository/index.md)。每个新仓库都根据它们的[配置权重](../repository_storage_paths.md#configure-where-new-repositories-are-stored)存储在其中一个仓库存储中。每个仓库存储：

- 使用[存储路径](../repository_storage_paths.md)直接访问仓库的 Gitaly 存储，其中每个仓库都存储在单个 Gitaly 节点上。所有请求都路由到此节点。
- [Gitaly 集群](#gitaly-cluster)提供的一个[虚拟存储](#virtual-storage)，每个仓库可以存储在多个 Gitaly 节点上以实现容错。在 Gitaly 集群中：
  - 读取请求分布在多个 Gitaly 节点之间，可以提高性能。
  - 写请求被广播到仓库副本。

<a id="before-deploying-gitaly-cluster"></a>

## 部署 Gitaly 集群之前

Gitaly 集群提供了容错的好处，但也带来了额外的设置和管理复杂性。
在部署 Gitaly 集群之前，请查看：

- 现有[已知问题](#known-issues)。
- [快照限制](#snapshot-backup-and-recovery-limitations)。
- [配置指南](configure_gitaly.md)和[仓库存储选项](../repository_storage_paths.md)，确保 Gitaly 集群是最适合您的设置。

如果您有：

- 尚未迁移到 Gitaly 集群并希望继续使用 NFS，请继续使用您正在使用的服务。但是，NFS 不再受支持。
- 尚未迁移到 Gitaly 集群但想要从 NFS 迁移，您有两种选择：
  - 一个分片的 Gitaly 实例。
  - Gitaly 集群。

如果您有任何问题，请联系技术支持。

<a id="known-issues"></a>

### 已知问题

下表概述了影响 Gitaly 集群使用的当前已知问题。有关这些问题的当前状态，请参阅参考的议题和史诗。

| 问题                                                                                 | 概述                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | 如何规避 |
|:--------------------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------|
| Gitaly Cluster + Geo - 重试失败同步的问题                             | 如果在 Geo 次要站点上使用 Gitaly 集群，同步失败的仓库可能会在 Geo 尝试重新同步它们时继续失败。从此状态恢复需要支持人员的帮助才能运行手动步骤。 | 15.0 版本之前没有已知的解决方案。在 15.0 到 15.2 版本中，启用 `gitaly_praefect_generated_replica_paths` 功能标志。在 15.3 版本中，默认情况下启用功能标志。 |
| 由于升级后未应用迁移，Praefect 无法将数据插入数据库 | 如果数据库没有与已完成的迁移保持同步，则 Praefect 节点无法执行正常操作。 | 确保 Praefect 数据库已启动并在所有迁移完成的情况下运行（例如：`/opt/gitlab/embedded/bin/praefect -config /var/opt/gitlab/praefect/config.toml sql-migrate-status` 应该显示所有应用迁移的列表）。考虑请求技术支持。 |
| 从正在运行的集群中的快照恢复 Gitaly 集群节点 | 因为 Gitaly 集群以一致的状态运行，引入一个落后的节点会导致集群无法协调节点数据和其他节点数据。 | 不要从备份快照中恢复单个 Gitaly 集群节点。如果您必须从备份中恢复，最好[关闭极狐GitLab](../read_only_gitlab.md#shut-down-the-gitlab-ui)，同时对所有 Gitaly 集群节点进行快照，并进行 Praefect 数据库转储。 |
| 重建或替换现有的 Gitaly 集群节点 | 无法替换现有节点，因为依赖 Praefect 数据库来确定每个 Gitaly 节点的当前状态。<!--Changing how Gitaly Cluster stores repositories is proposed in issue [4218](https://gitlab.com/gitlab-org/gitaly/-/issues/4218). Turning on [repository verification](praefect.md#repository-verification) is proposed in issue [4429](https://gitlab.com/gitlab-org/gitaly/-/issues/4429) so Praefect can detect that data is missing and needs to replicated to a new Gitaly node.--> | 目前没有已知的解决方案。 |

<a id="snapshot-backup-and-recovery-limitations"></a>

### 快照备份和恢复限制

Gitaly 集群不支持快照备份。快照备份可能会导致 Praefect 数据库与磁盘存储不同步的问题。由于 Praefect 在还原期间如何重建 Gitaly 磁盘信息的复制元数据，我们建议使用[官方备份和还原 Rake 任务](../../raketasks/backup_restore.md)。

<!--
[增量备份方法](../../raketasks/backup_gitlab.md#incremental-repository-backups)可用于加速 Gitaly Cluster 备份。
-->

如果您无法使用任何一种方法，请联系客户支持以获取恢复帮助。

### 如果您在 Gitaly 集群上遇到问题或限制该怎么办

请联系客户支持以立即获得恢复或恢复方面的帮助。

<a id="disk-requirements"></a>

## 磁盘要求

Gitaly 和 Gitaly 集群需要快速的本地存储才能有效执行，因为它们是基于 I/O 的繁重进程。因此，我们强烈建议所有 Gitaly 节点都使用固态硬盘 (SSD)。

这些 SSD 的吞吐量至少应为：

- 每秒 8,000 次输入/输出操作 (IOPS) 用于读取操作。
- 2,000 IOPS 用于写入操作。

这些 IOPS 值是初始建议值，可能会根据环境工作负载的规模调整为更大或更小的值。如果您在云提供商上运行环境，请参阅他们的文档以了解如何正确配置 IOPS。

对于存储库数据，出于性能和一致性原因，Gitaly 和 Gitaly 集群仅支持本地存储。
不支持 [NFS](../nfs.md) 或基于云的文件系统<!--[基于云的文件系统](../nfs.md#avoid-using-cloud-based-file-systems)-->等替代方案。

## 直接访问仓库

极狐GitLab 不建议使用 Git 客户端或任何其他工具直接访问存储在磁盘上的 Gitaly 仓库，因为 Gitaly 正在不断改进和更改。这些改进可能会使您的预设无效，从而导致性能下降、不稳定甚至数据丢失。例如：

- Gitaly 进行了优化，例如 `info/refs` 广告缓存，它依赖于 Gitaly 通过使用官方 gRPC 接口控制和监视对仓库的访问。
- [Gitaly 集群](#gitaly-cluster)有一些优化，例如容错和[分布式读取](#distributed-reads)，它们依赖于 gRPC 接口和数据库来确定仓库状态。

WARNING:
直接访问 Git 仓库需要您自担风险，并且不受支持。

## Gitaly

下面显示了极狐GitLab 设置为使用对 Gitaly 的直接访问：

![Shard example](img/shard_example_v13_3.png)

在这个例子中：

- 每个仓库都存储在三个 Gitaly 存储之一上：`storage-1`、`storage-2` 或`storage-3`。
- 每个存储都由一个 Gitaly 节点提供服务。
- 三个 Gitaly 节点将数据存储在其文件系统上。

### Gitaly 架构

下面说明了 Gitaly 客户端-服务器架构：

```mermaid
flowchart TD
  subgraph Gitaly clients
    A[GitLab Rails]
    B[GitLab Workhorse]
    C[GitLab Shell]
    D[...]
  end

  subgraph Gitaly
    E[Git integration]
  end

F[Local filesystem]

A -- gRPC --> Gitaly
B -- gRPC--> Gitaly
C -- gRPC --> Gitaly
D -- gRPC --> Gitaly

E --> F
```

<!--
### Configure Gitaly

Gitaly comes pre-configured with Omnibus GitLab, which is a configuration
[suitable for up to 1000 users](../reference_architectures/1k_users.md). For:

- Omnibus GitLab installations for up to 2000 users, see [specific Gitaly configuration instructions](../reference_architectures/2k_users.md#configure-gitaly).
- Source installations or custom Gitaly installations, see [Configure Gitaly](configure_gitaly.md).

GitLab installations for more than 2000 active users performing daily Git write operation may be
best suited by using Gitaly Cluster.

### Backing up repositories

When backing up or syncing repositories using tools other than GitLab, you must [prevent writes](../../raketasks/backup_restore.md#prevent-writes-and-copy-the-git-repository-data)
while copying repository data.
-->

<a id="gitaly-cluster"></a>

## Gitaly 集群

Git 存储是通过极狐GitLab 中的 Gitaly 服务提供的，对于极狐GitLab 的运行至关重要。当用户、仓库和活动的数量增加时，通过以下方式适当扩展 Gitaly 非常重要：

- 在因资源耗尽降低 Git、Gitaly 和极狐GitLab 应用程序的性能之前，增加可供 Git 使用的 CPU 和内存资源。
- 在因达到存储限制而导致写入操作失败之前，增加可用存储。
- 消除单点故障以提高容错能力。如果服务降级会阻止您将更改部署到生产环境，应将 Git 视为关键任务。

Gitaly 可以在集群配置中运行：

- 扩展 Gitaly 服务。
- 增加容错能力。

在此配置中，每个 Git 仓库都可以存储在集群中的多个 Gitaly 节点上。

使用 Gitaly 集群通过以下方式提高容错能力：

- 将写入操作复制到热备用 Gitaly 节点。
- 检测 Gitaly 节点故障。
- 自动将 Git 请求路由到可用的 Gitaly 节点。

NOTE:
Gitaly 集群的技术支持仅限于专业版和旗舰版客户。

下面显示了极狐GitLab 设置为访问 `storage-1`，这是 Gitaly 集群提供的虚拟存储：

![Cluster example](img/cluster_example_v13_3.png)

在这个例子中：

- 存储库存储在名为 `storage-1` 的虚拟存储中。
- 三个 Gitaly 节点提供 `storage-1` 访问：`gitaly-1`、`gitaly-2` 和 `gitaly-3`。
- 三个 Gitaly 节点在三个独立的哈希存储位置共享数据。
- [复制系数](#replication-factor) 是 `3`。每个仓库维护三个副本。

假设单个节点故障，Gitaly 集群的可用性目标是：

- **恢复点目标 (RPO)：**小于 1 分钟。

  写入是异步复制的。任何尚未复制到新提升的主节点的写入都将丢失。

  [强一致性](#strong-consistency) 在某些情况下可以防止丢失。

- **恢复时间目标 (RTO)：**小于 10 秒。
  每个 Praefect 节点每秒运行一次运行状况检查来检测中断。故障转移需要在每个 Praefect 节点上连续进行十次失败的健康检查。

  <!--Faster outage detection, to improve this speed to less than 1 second,
  is tracked [in this issue](https://gitlab.com/gitlab-org/gitaly/-/issues/2608).-->

WARNING:
如果发生完整的集群故障，则应执行灾难恢复计划。这会影响上面讨论的 RPO 和 RTO。

<a id="comparison-to-geo"></a>

### 与 Geo 比较

Gitaly 集群和 Geo 都提供冗余。然而：

- Gitaly Cluster 为数据存储提供容错能力，对用户是不可见的。用户不知道何时使用 Gitaly 集群。
- Geo 为整个极狐GitLab 实例提供复制和灾难恢复。用户知道他们何时使用 Geo 进行复制。Geo 复制多种数据类型，包括 Git 数据。

下表概述了 Gitaly 集群和 Geo 之间的主要区别：

| 工具           | 节点    | 位置 | 延迟容忍度                                                                                     | 故障转移                                                                    | 一致性                           | 提供冗余 |
|:---------------|:---------|:----------|:------------------------------------------------------------------------------------------------------|:----------------------------------------------------------------------------|:--------------------------------------|:------------------------|
| Gitaly 集群 | 多个 | 单个    | [不到 1 秒，理想情况下是个位数毫秒](praefect.md#network-latency-and-connectivity) | [自动](praefect.md#automatic-failover-and-primary-election-strategies) | [强一致性](index.md#strong-consistency) | Git 中的数据存储     |
| Geo            | 多个 | 多个  | 最多一分钟                                                                                      | 手动                                 | 最终一致性                              | 整个极狐GitLab 实例  |

<!--
For more information, see:

- Geo [use cases](../geo/index.md#use-cases).
- Geo [architecture](../geo/index.md#architecture).
-->

<a id="virtual-storage"></a>

### 虚拟存储

虚拟存储使在极狐GitLab 中拥有单个仓库存储，使简化仓库管理成为可能。

带有 Gitaly 集群的虚拟存储通常可以替代直接的 Gitaly 存储配置。
但是，这是以在多个 Gitaly 节点上存储每个仓库所需的额外存储空间为代价的。与直接 Gitaly 存储相比，使用 Gitaly 集群虚拟存储的好处是：

- 改进了容错性，因为每个 Gitaly 节点都有每个仓库的副本。
- 提高了资源利用率，减少了对特定于分片的峰值负载的过度配置需求，因为读取负载分布在 Gitaly 节点上。
- 不需要手动重新平衡性能，因为读取负载分布在 Gitaly 节点上。
- 更简单的管理，因为所有 Gitaly 节点都是相同的。

可以使用[复制系数](#replication-factor)配置仓库副本的数量。

<!--
It can
be uneconomical to have the same replication factor for all repositories.
To provide greater flexibility for extremely large GitLab instances,
variable replication factor is tracked in [this issue](https://gitlab.com/groups/gitlab-org/-/epics/3372).
-->

与普通 Gitaly 存储一样，虚拟存储可以分片。

<!--
### Storage layout

WARNING:
The storage layout is an internal detail of Gitaly Cluster and is not guaranteed to remain stable between releases.
The information here is only for informational purposes and to help with debugging. Performing changes in the
repositories directly on the disk is not supported and may lead to breakage or the changes being overwritten.

Gitaly Cluster's virtual storages provide an abstraction that looks like a single storage but actually consists of
multiple physical storages. Gitaly Cluster has to replicate each operation to each physical storage. Operations
may succeed on some of the physical storages but fail on others.

Partially applied operations can cause problems with other operations and leave the system in a state it can't recover from.
To avoid these types of problems, each operation should either fully apply or not apply at all. This property of operations is called
[atomicity](https://en.wikipedia.org/wiki/Atomicity_(database_systems)).

GitLab controls the storage layout on the repository storages. GitLab instructs the repository storage where to create,
delete, and move repositories. These operations create atomicity issues when they are being applied to multiple physical storages.
For example:

- GitLab deletes a repository while one of its replicas is unavailable.
- GitLab later recreates the repository.

As a result, the stale replica that was unavailable at the time of deletion may cause conflicts and prevent
recreation of the repository.

These atomicity issues have caused multiple problems in the past with:

- Geo syncing to a secondary site with Gitaly Cluster.
- Backup restoration.
- Repository moves between repository storages.

Gitaly Cluster provides atomicity for these operations by storing repositories on the disk in a special layout that prevents
conflicts that could occur due to partially applied operations.

#### Client-generated replica paths

Repositories are stored in the storages at the relative path determined by the [Gitaly client](#gitaly-architecture). These paths can be
identified by them not beginning with the `@cluster` prefix. The relative paths
follow the [hashed storage](../repository_storage_types.md#hashed-storage) schema.

#### Praefect-generated replica paths (GitLab 15.0 and later)

> - [Introduced](https://gitlab.com/gitlab-org/gitaly/-/issues/4218) in GitLab 15.0 [with a flag](../feature_flags.md) named `gitaly_praefect_generated_replica_paths`. Disabled by default.
> - [Enabled on GitLab.com](https://gitlab.com/gitlab-org/gitaly/-/issues/4218) in GitLab 15.2.
> - [Enabled on self-managed](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/4809) in GitLab 15.3.

FLAG:
On self-managed GitLab, by default this feature is available. To hide the feature, ask an administrator to [disable the feature flag](../feature_flags.md) named `gitaly_praefect_generated_replica_paths`. On GitLab.com, this feature is available but can be configured by GitLab.com administrators only.

When Gitaly Cluster creates a repository, it assigns the repository a unique and permanent ID called the _repository ID_. The repository ID is
internal to Gitaly Cluster and doesn't relate to any IDs elsewhere in GitLab. If a repository is removed from Gitaly Cluster and later moved
back, the repository is assigned a new repository ID and is a different repository from Gitaly Cluster's perspective. The sequence of repository IDs
always increases, but there may be gaps in the sequence.

The repository ID is used to derive a unique storage path called _replica path_ for each repository on the cluster. The replicas of
a repository are all stored at the same replica path on the storages. The replica path is distinct from the _relative path_:

- The relative path is a name the Gitaly client uses to identify a repository, together with its virtual storage, that is unique to them.
- The replica path is the actual physical path in the physical storages.

Praefect translates the repositories in the RPCs from the virtual `(virtual storage, relative path)` identifier into physical repository
`(storage, replica_path)` identifier when handling the client requests.

The format of the replica path for:

- Object pools is `@cluster/pools/<xx>/<xx>/<repository ID>`. Object pools are stored in a different directory than other repositories.
  They must be identifiable by Gitaly to avoid pruning them as part of housekeeping. Pruning object pools can cause data loss in the linked
  repositories.
- Other repositories is `@cluster/repositories/<xx>/<xx>/<repository ID>`

For example, `@cluster/repositories/6f/96/54771`.

The last component of the replica path, `54771`, is the repository ID. This can be used to identify the repository on the disk.

`<xx>/<xx>` are the first four hex digits of the SHA256 hash of the string representation of the repository ID. This is used to balance
the repositories evenly into subdirectories to avoid overly large directories that might cause problems on some file
systems. In this case, `54771` hashes to `6f960ab01689464e768366d3315b3d3b2c28f38761a58a70110554eb04d582f7` so the
first four digits are `6f` and `96`.

#### Identify repositories on disk

Use the [`praefect metadata`](troubleshooting.md#view-repository-metadata) subcommand to:

- Retrieve a repository's virtual storage and relative path from the metadata store. After you have the hashed storage path, you can use the Rails
  console to retrieve the project path.
- Find where a repository is stored in the cluster with either:
  - The virtual storage and relative path.
  - The repository ID.

The repository on disk also contains the project path in the Git configuration file. The configuration
file can be used to determine the project path even if the repository's metadata has been deleted.
Follow the [instructions in hashed storage's documentation](../repository_storage_types.md#from-hashed-path-to-project-name).

#### Atomicity of operations

Gitaly Cluster uses the PostgreSQL metadata store with the storage layout to ensure atomicity of repository creation,
deletion, and move operations. The disk operations can't be atomically applied across multiple storages. However, PostgreSQL guarantees
the atomicity of the metadata operations. Gitaly Cluster models the operations in a manner that the failing operations always leave
the metadata consistent. The disks may contain stale state even after successful operations. This is expected and the leftover state
won't interfere with future operations but may use up disk space unnecessarily until a clean up is performed.

There is on-going work on a [background crawler](https://gitlab.com/gitlab-org/gitaly/-/issues/3719) that cleans up the leftover
repositories from the storages.

##### Repository creations

When creating repositories, Praefect:

1. Reserves a repository ID from PostgreSQL. This is atomic and no two creations receive the same ID.
1. Creates replicas on the Gitaly storages in the replica path derived from the repository ID.
1. Creates metadata records after the repository is successfully created on disk.

Even if two concurrent operations create the same repository, they'd be stored in different directories on the storages and not
conflict. The first to complete creates the metadata record and the other operation fails with an "already exists" error.
The failing creation leaves leftover repositories on the storages. There is on-going work on a
[background crawler](https://gitlab.com/gitlab-org/gitaly/-/issues/3719) that clean up the leftover repositories from the storages.

The repository IDs are generated from the `repositories_repository_id_seq` in PostgreSQL. In the above example, the failing operation took
one repository ID without successfully creating a repository with it. Failed repository creations are expected lead to gaps in the repository IDs.

##### Repository deletions

A repository is deleted by removing its metadata record. The repository ceases to logically exist as soon as the metadata record is deleted.
PostgreSQL guarantees the atomicity of the removal and a concurrent delete fails with a "not found" error. After successfully deleting
the metadata record, Praefect attempts to remove the replicas from the storages. This may fail and leave leftover state in the storages.
The leftover state is eventually cleaned up.

##### Repository moves

Unlike Gitaly, Gitaly Cluster doesn't move the repositories in the storages but only virtually moves the repository by updating the
relative path of the repository in the metadata store.
-->

<a id="components"></a>

### 组件

Gitaly 集群由多个组件组成：

- [负载均衡器](praefect.md#load-balancer)，用于分发请求并提供对 Praefect 节点的容错访问。
- [Praefect](praefect.md#praefect) 节点，用于管理集群并将请求路由到 Gitaly 节点。
- [PostgreSQL 数据库](praefect.md#postgresql)，用于持久化集群元数据和 [PgBouncer](praefect.md#use-pgbouncer)，推荐用于池化 Praefect 的数据库连接。
- Gitaly 节点提供仓库存储和 Git 访问。

### 架构

Praefect 是 Gitaly 的路由器和事务管理器，也是运行 Gitaly 集群的必需组件。

![Architecture diagram](img/praefect_architecture_v12_10.png)

<!--
For more information, see [Gitaly High Availability (HA) Design](https://gitlab.com/gitlab-org/gitaly/-/blob/master/doc/design_ha.md).
-->

### 功能

Gitaly 集群提供以下功能：

- Gitaly 节点之间的[分布式读取](#distributed-reads)。
- 次要副本的[强一致性](#strong-consistency)。
- 仓库的[复制系数](#replication-factor)，用于增加冗余。
- [自动故障转移](praefect.md#automatic-failover-and-primary-election-strategies)，从主要 Gitaly 节点到次要 Gitaly 节点。
- 如果复制队列不为空，则报告可能的数据丢失。

<!--
Follow the [Gitaly Cluster epic](https://gitlab.com/groups/gitlab-org/-/epics/1489) for improvements
including [horizontally distributing reads](https://gitlab.com/groups/gitlab-org/-/epics/2013).
-->

<a id="distributed-reads"></a>

#### 分布式读取

Gitaly 集群支持跨为[虚拟存储](#virtual-storage)配置的 Gitaly 节点分布读取操作。

所有标有 `ACCESSOR` 选项的 RPC 都被重定向到最新且健康的 Gitaly 节点。例如，`GetBlob`。

*最新*（Up to date）意味着：

- 没有为此 Gitaly 节点安排的复制操作。
- 最后一个复制操作处于*完成*状态。

如果出现以下情况，则选择主节点来处理请求：

- 没有最新的节点。
- 在节点选择期间发生任何其他错误。

您可以使用 Prometheus [监控读取分布](monitoring.md#monitor-gitaly-cluster)。

<a id="strong-consistency"></a>

#### 强一致性

> - 引入于 13.1 版本，作为 alpha 功能，默认禁用。
> - 升级为 beta 于 13.2版本，默认禁用。
> - 在 13.3 版本中，除非禁用 primary-wins 投票策略，否则禁用。
> - 默认启用于 13.4 版本。
> - 从 13.5 版本开始，您必须在 Gitaly 节点上使用 Git v2.28.0 或更高版本才能启用强一致性。
> - 从 13.6 版本开始，primary-wins 投票策略和 `gitaly_reference_transactions_primary_wins` 功能标志删除。
> - 从 14.0 版本开始，Gitaly 集群仅支持强一致性，删除了 `gitaly_reference_transactions` 功能标志。

Gitaly 集群通过将更改同步写入所有健康、最新的副本来提供强一致性。如果副本在事务发生时已过时或不健康，则写入将异步复制到它。

如果强一致性不可用，Gitaly 集群保证最终一致性。在这种情况下，在对主 Gitaly 节点的写入发生后，Gitaly 集群会将所有写入复制到次要 Gitaly 节点。

强一致性是 14.0 及更高版本中的主要复制方法。一部分操作仍然使用复制作业（最终一致性）而不是强一致性。

<!--
For more information on monitoring strong consistency, see the Gitaly Cluster [Prometheus metrics documentation](monitoring.md#monitor-gitaly-cluster).
-->

<a id="replication-factor"></a>

#### 复制系数

复制系数是 Gitaly 集群维护给定仓库的副本数。更高的复制参数：

- 提供更好的冗余和读取工作负载分布。
- 导致更高的存储成本。

默认情况下，Gitaly 集群将仓库复制到[虚拟存储](#virtual-storage)中的每个存储。

有关配置信息，请参阅[配置复制参数](praefect.md#configure-replication-factor)。

### 配置 Gitaly 集群

有关配置 Gitaly 集群的更多信息，请参阅[配置 Gitaly 集群](praefect.md)。

<!--
### Upgrade Gitaly Cluster

To upgrade a Gitaly Cluster, follow the documentation for
[zero downtime upgrades](../../update/zero_downtime.md#gitaly-or-gitaly-cluster).

### Downgrade Gitaly Cluster to a previous version

If you need to roll back a Gitaly Cluster to an earlier version, some Praefect database migrations may need to be reverted.

To downgrade a Gitaly Cluster (assuming multiple Praefect nodes):

1. Stop the Praefect service on all Praefect nodes:

   ```shell
   gitlab-ctl stop praefect
   ```

1. Downgrade the GitLab package to the older version on one of the Praefect nodes.
1. On the downgraded node, check the state of Praefect migrations:

   ```shell
   /opt/gitlab/embedded/bin/praefect -config /var/opt/gitlab/praefect/config.toml sql-migrate-status
   ```

1. Count the number of migrations with `unknown migration` in the `APPLIED` column.
1. On a Praefect node that has **not** been downgraded, perform a dry run of the rollback to validate which migrations to revert. `<CT_UNKNOWN>`
   is the number of unknown migrations reported by the downgraded node.

   ```shell
   /opt/gitlab/embedded/bin/praefect -config /var/opt/gitlab/praefect/config.toml sql-migrate <CT_UNKNOWN>
   ```

1. If the results look correct, run the same command with the `-f` option to revert the migrations:

   ```shell
   /opt/gitlab/embedded/bin/praefect -config /var/opt/gitlab/praefect/config.toml sql-migrate -f <CT_UNKNOWN>
   ```

1. Downgrade the GitLab package on the remaining Praefect nodes and start the Praefect service again:

   ```shell
   gitlab-ctl start praefect
   ```
-->

<a id="migrate-to-gitaly-cluster"></a>

### 迁移到 Gitaly 集群

WARNING:
Gitaly 集群中存在一些[已知问题](#known-issues)。在继续之前查看以下信息。

在迁移到 Gitaly 集群之前：

- 查看[部署 Gitaly 集群之前](#before-deploying-gitaly-cluster)。
- 升级到最新版本的极狐GitLab。

要迁移到 Gitaly 集群：

1. 创建所需的存储。请参阅[仓库存储建议](praefect.md#repository-storage-recommendations)。
1. 创建并配置 [Gitaly 集群](praefect.md)。
1. [使用 TPC](praefect.md#use-tcp-for-existing-gitlab-instances) 配置现有的 Gitaly 实例，如果还没有这样配置的话。
1. [移动仓库](../operations/moving_repositories.md#moving-repositories)。要迁移到 Gitaly 集群，必须移动存储在 Gitaly 集群之外的现有仓库。没有自动迁移，但可以使用极狐GitLab API 安排移动。

即使您不使用 `default` 仓库存储，也必须确保已对其进行配置。<!--[阅读有关此限制的更多信息](configure_gitaly.md#gitlab-requires-a-default-repository-storage)。-->

### 迁移出 Gitaly 集群

如果发现 Gitaly 集群的限制和取舍不适合您的环境，您可以将 Gitaly 集群迁移到分片的 Gitaly 实例：

1. 创建并配置一个新的 Gitaly 服务器<!--[Gitaly 服务器](configure_gitaly.md#run-gitaly-on-its-own-server)-->。
1. [移动仓库](../operations/moving_repositories.md#moving-repositories)到新创建的存储。您可以按分片或按组移动它们，这样您就有机会将它们分布在多个 Gitaly 服务器上。

<!--
## Direct access to Git in GitLab

Direct access to Git uses code in GitLab known as the "Rugged patches".

Before Gitaly existed, what are now Gitaly clients accessed Git repositories directly, either:

- On a local disk in the case of a single-machine Omnibus GitLab installation.
- Using NFS in the case of a horizontally-scaled GitLab installation.

In addition to running plain `git` commands, GitLab used a Ruby library called
[Rugged](https://github.com/libgit2/rugged). Rugged is a wrapper around
[libgit2](https://libgit2.org/), a stand-alone implementation of Git in the form of a C library.

Over time it became clear that Rugged, particularly in combination with
[Unicorn](https://yhbt.net/unicorn/), is extremely efficient. Because `libgit2` is a library and
not an external process, there was very little overhead between:

- GitLab application code that tried to look up data in Git repositories.
- The Git implementation itself.

Because the combination of Rugged and Unicorn was so efficient, the GitLab application code ended up
with lots of duplicate Git object lookups. For example, looking up the default branch commit a dozen
times in one request. We could write inefficient code without poor performance.

When we migrated these Git lookups to Gitaly calls, we suddenly had a much higher fixed cost per Git
lookup. Even when Gitaly is able to re-use an already-running `git` process (for example, to look up
a commit), you still have:

- The cost of a network roundtrip to Gitaly.
- Inside Gitaly, a write/read roundtrip on the Unix pipes that connect Gitaly to the `git` process.

Using GitLab.com to measure, we reduced the number of Gitaly calls per request until the loss of
Rugged's efficiency was no longer felt. It also helped that we run Gitaly itself directly on the Git
file servers, rather than by using NFS mounts. This gave us a speed boost that counteracted the
negative effect of not using Rugged anymore.

Unfortunately, other deployments of GitLab could not remove NFS like we did on GitLab.com, and they
got the worst of both worlds:

- The slowness of NFS.
- The increased inherent overhead of Gitaly.

The code removed from GitLab during the Gitaly migration project affected these deployments. As a
performance workaround for these NFS-based deployments, we re-introduced some of the old Rugged
code. This re-introduced code is informally referred to as the "Rugged patches".

### Automatic detection

> Automatic detection for Rugged [disabled](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/95445) in GitLab 15.3.

FLAG:
On self-managed GitLab, by default automatic detection of whether Rugged should be used (per storage) is not available.
To make it available, ask an administrator to [disable the feature flag](../../administration/feature_flags.md) named
`skip_rugged_auto_detect`.

The Ruby methods that perform direct Git access are behind
[feature flags](../../development/gitaly.md#legacy-rugged-code), disabled by default. It wasn't
convenient to set feature flags to get the best performance, so we added an automatic mechanism that
enables direct Git access.

When GitLab calls a function that has a "Rugged patch", it performs two checks:

- Is the feature flag for this patch set in the database? If so, the feature flag setting controls
  the GitLab use of "Rugged patch" code.
- If the feature flag is not set, GitLab tries accessing the file system underneath the
  Gitaly server directly. If it can, it uses the "Rugged patch":
  - If using Puma and [thread count](../../install/requirements.md#puma-threads) is set
    to `1`.

The result of these checks is cached.

To see if GitLab can access the repository file system directly, we use the following heuristic:

- Gitaly ensures that the file system has a metadata file in its root with a UUID in it.
- Gitaly reports this UUID to GitLab by using the `ServerInfo` RPC.
- GitLab Rails tries to read the metadata file directly. If it exists, and if the UUID's match,
  assume we have direct access.

Direct Git access is:

- [Disabled](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/95445) by default in GitLab 15.3 and later for
  compatibility with [Praefect-generated replica paths](#praefect-generated-replica-paths-gitlab-150-and-later). It
  can be enabled if Rugged [feature flags](../../development/gitaly.md#legacy-rugged-code) are enabled.
- Enabled by default in GitLab 15.2 and earlier because it fills in the correct repository paths in the GitLab
  configuration file `config/gitlab.yml`. This satisfies the UUID check.

### Transition to Gitaly Cluster

For the sake of removing complexity, we must remove direct Git access in GitLab. However, we can't
remove it as long some GitLab installations require Git repositories on NFS.

There are two facets to our efforts to remove direct Git access in GitLab:

- Reduce the number of inefficient Gitaly queries made by GitLab.
- Persuade administrators of fault-tolerant or horizontally-scaled GitLab instances to migrate off
  NFS.

The second facet presents the only real solution. For this, we developed
[Gitaly Cluster](#gitaly-cluster).

## NFS deprecation notice

Engineering support for NFS for Git repositories is deprecated. Technical support is planned to be
unavailable beginning November 22, 2022. For further information, please see our [NFS Deprecation](../nfs.md#gitaly-and-nfs-deprecation) documentation.
-->
