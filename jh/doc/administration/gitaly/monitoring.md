---
stage: Systems
group: Gitaly
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 监控 Gitaly 和 Gitaly 集群

您可以使用可用的日志和 [Prometheus 指标](../monitoring/prometheus/index.md)来监控 Gitaly 和 Gitaly 集群（Praefect）。

指标定义可用：

- 直接来自为 Gitaly 配置的 Prometheus `/metrics` 端点。
- 在针对 Prometheus 配置的 Grafana 实例上使用 [Grafana Explore](https://grafana.com/docs/grafana/latest/explore/)。

## 监控 Gitaly 速率限制

Gitaly 可以配置为根据以下条件限制请求：

- 请求的并发性。
- 速率限制。

使用 `gitaly_requests_dropped_total` Prometheus 指标监控 Gitaly 请求限制。此指标提供因请求限制而丢弃的请求总数。`reason` 标记指示请求被丢弃的原因：

- `rate`，因为速率限制。
- `max_size`，因为已达到并发队列大小。
- `max_time`，因为请求超过了 Gitaly 中配置的最大队列等待时间。

## 监控 Gitaly 并发限制

您可以使用 Gitaly 日志和 Prometheus 观察并发队列请求的特定行为：

- 在 [Gitaly 日志](../logs/index.md#gitaly-logs) 中，查找字符串（或结构化日志字段）`acquire_ms`。 具有此字段的消息正在报告有关并发限制器的信息。
- 在 Prometheus 中，查找以下指标：
  - `gitaly_concurrency_limiting_in_progress` 表示正在处理多少并发请求。
  - `gitaly_concurrency_limiting_queued` 表示由于达到并发限制，有多少给定存储库的 RPC 请求正在等待。
  - `gitaly_concurrency_limiting_acquiring_seconds` 表示由于并发限制，请求在处理之前必须等待多长时间。

## 监控 Gitaly cgroups

您可以使用 Prometheus 观察控制组（cgroups）的状态：

- `gitaly_cgroups_reclaim_attempts_total`，衡量内存回收尝试的总次数。每次重新启动服务器时，此数字都会重置。
- `gitaly_cgroups_cpu_usage`，衡量每个 cgroup 的 CPU 使用率的指标。
- `gitaly_cgroup_procs_total`，一个衡量 Gitaly 在 cgroups 控制下产生的进程总数的指标。

## `pack-objects` cache

以下 `pack-objects` 缓存指标可用：

- `gitaly_pack_objects_cache_enabled`，启用缓存时，仪表设置为 `1`。可用标签：`dir` 和 `max_age`。
- `gitaly_pack_objects_cache_lookups_total`，缓存查找的计数器。可用标签：`result`。
- `gitaly_pack_objects_generated_bytes_total`，写入缓存的字节数计数器。
- `gitaly_pack_objects_served_bytes_total`，从缓存中读取的字节数计数器。
- `gitaly_streamcache_filestore_disk_usage_bytes`，衡量缓存文件总大小的指标。可用标签：`dir`。
- `gitaly_streamcache_index_entries`，衡量缓存中的条目数。可用标签：`dir`。

其中一些指标以 `gitaly_streamcache` 开头，因为它们是由 Gitaly 中的 `streamcache` 内部库包生成的。

示例：

```plaintext
gitaly_pack_objects_cache_enabled{dir="/var/opt/gitlab/git-data/repositories/+gitaly/PackObjectsCache",max_age="300"} 1
gitaly_pack_objects_cache_lookups_total{result="hit"} 2
gitaly_pack_objects_cache_lookups_total{result="miss"} 1
gitaly_pack_objects_generated_bytes_total 2.618649e+07
gitaly_pack_objects_served_bytes_total 7.855947e+07
gitaly_streamcache_filestore_disk_usage_bytes{dir="/var/opt/gitlab/git-data/repositories/+gitaly/PackObjectsCache"} 2.6200152e+07
gitaly_streamcache_filestore_removed_total{dir="/var/opt/gitlab/git-data/repositories/+gitaly/PackObjectsCache"} 1
gitaly_streamcache_index_entries{dir="/var/opt/gitlab/git-data/repositories/+gitaly/PackObjectsCache"} 1
```

<a id="useful-queries"></a>

## 有用的查询

以下是监控 Gitaly 的有用查询：

- 使用以下 Prometheus 查询，来观察 Gitaly 服务于生产环境的连接类型：

  ```prometheus
  sum(rate(gitaly_connections_total[5m])) by (type)
  ```

- 使用以下 Prometheus 查询，来监控极狐GitLab 安装的身份验证行为：

  ```prometheus
  sum(rate(gitaly_authentications_total[5m])) by (enforced, status)
  ```

  在正确配置身份验证且拥有实时流量的系统中，您会看到如下内容：

  ```prometheus
  {enforced="true",status="ok"}  4424.985419441742
  ```

  可能还有其他数字的比率为 0，但您只需要注意非零数字。

  唯一的非零数应该有 `enforced="true",status="ok"`。如果您有其他非零数字，则说明您的配置有问题。

  `status="ok"` 数字反映了您当前的请求率。在上面的示例中，Gitaly 每秒处理大约 4000 个请求。

- 使用以下 Prometheus 查询来观察生产环境中使用的 [Git 协议版本](../git_protocol.md)：

  ```prometheus
  sum(rate(gitaly_git_protocol_requests_total[1m])) by (grpc_method,git_protocol,grpc_service)
  ```

<a id="monitor-gitaly-cluster"></a>

## 监控 Gitaly 集群

要监控 Gitaly 集群 (Praefect)，您可以使用这些 Prometheus 指标。有两个单独的指标端点可以从中抓取指标：

- 默认的 `/metrics` 端点。
- `/db_metrics`，其中包含需要数据库查询的指标。

### 默认 Prometheus `/metrics` 端点

`/metrics` 端点提供了以下指标：

- `gitaly_praefect_read_distribution`，跟踪[读取分布](index.md#distributed-reads)的计数器。它有两个标签：

  - `virtual_storage`。
  - `storage`。

  它们反映了为此 Praefect 实例定义的配置。

- `gitaly_praefect_replication_latency_bucket`，一个直方图，测量复制作业开始后复制完成所需的时间。在 12.10 及更高版本中可用。
- `gitaly_praefect_replication_delay_bucket`，一个直方图，测量从创建复制作业到开始复制作业之间经过的时间。在 12.10 及更高版本中可用。
- `gitaly_praefect_connections_total`，与 Praefect 的连接总数。引入于 14.7 版本。

要监控[强一致性](index.md#strong-consistency)，您可以使用以下 Prometheus 指标：

- `gitaly_praefect_transactions_total`，创建和投票的事务数量。
- `gitaly_praefect_subtransactions_per_transaction_total`，节点为单个事务投票的次数。如果在单个事务中更新多个引用，这可能会发生多次。
- `gitaly_praefect_voters_per_transaction_total`，参与事务的 Gitaly 节点数。
- `gitaly_praefect_transactions_delay_seconds`，等待事务提交时引入的服务器端延迟。
- `gitaly_hook_transaction_voting_delay_seconds`，等待事务提交时引入的客户端延迟。

要监控[仓库验证](praefect.md#repository-verification)，请使用以下 Prometheus 指标：

- `gitaly_praefect_verification_jobs_dequeued_total`，worker 接受的验证工作的数量。
- `gitaly_praefect_verification_jobs_completed_total`，worker 完成的验证工作的数量。`result` 标签表示作业的最终结果：
  - `valid` 表示存储中存在预期的副本。
  - `invalid` 表示预期存在的副本在存储中不存在。
  - `error` 表示作业失败，必须重试。
- `gitaly_praefect_stale_verification_leases_released_total`，已释放的陈旧验证租约数。

`/metrics` 端点还提供了 `/db_metrics` 端点下可用的所有指标。使用 `/metrics` 作为 `/db_metrics` 指标的功能废弃于 15.9 版本，并将在 16.0 版本中删除。

您还可以监控 [Praefect 日志](../logs/index.md#praefect-logs)。

### 数据库指标 `/db_metrics` 端点

> 引入于 14.5 版本。

`/db_metrics` 端点提供了以下指标：

- `gitaly_praefect_unavailable_repositories`，没有健康的最新副本的存储库数量。
- `gitaly_praefect_replication_queue_depth`，复制队列中的作业数。
- `gitaly_praefect_verification_queue_depth`，等待验证的副本总数。
- `gitaly_praefect_read_only_repositories`，虚拟存储中处于只读模式的存储库数量。
  - 此指标移除于 15.4 版本。

