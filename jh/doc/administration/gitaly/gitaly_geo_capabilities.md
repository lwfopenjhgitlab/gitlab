---
stage: Systems
group: Gitaly
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Gitaly 和 Geo 功能

您通常需要最可用、可快速恢复、高性能且完全有弹性的数据解决方案。但是，需要权衡取舍。

下表旨在指导您根据您的要求选择正确的功能组合。

## Gitaly 功能

| 功能 | 可用性 | 可恢复性 | 数据弹性 | 性能 | 风险/权衡 |
|------------|--------------|----------------|-----------------|-------------|-----------------|
| Gitaly 集群 | 非常高 - 对节点故障的容忍度 | 单节点 RTO 10s，无人工干预 | 数据存储在多个节点上 |好 - 虽然由于投票，写入可能需要稍长的时间，但读取分布提高了读取速度 | **权衡** - 冗余、强一致性存储解决方案的写入速度略有下降 **风险** - [不支持快照备份](../gitaly/index.md#snapshot-backup-and-recovery-limitations)，对于大型数据集，极狐GitLab 备份任务可能很慢 |
| Gitaly 分片 | 单一存储位置是单点故障 | 只需要恢复失败的分片 | 单点故障 | 好 - 可以将仓库分配到分片，从而分散负载 | **权衡** - 需要手动将仓库配置到不同的分片中，从而平衡负载/存储空间 **风险** - 单点故障依赖于单节点故障时的恢复过程 |
| Gitaly + NFS | 单一存储位置是单点故障 | 单节点故障需要备份恢复 | 单点故障 | 平均 - NFS 不适合大量的小读/写，这会对性能产生不利影响 | **权衡** - NFS 并不完全适合 Git 需求 **风险** - 许多 NFS 兼容性问题的实例提供了非常糟糕的客户体验 |

## Geo 功能

如果您的可用性需要跨越多个区域或多个位置，请阅读 [Geo](../geo/index.md)。

| 功能 | 可用性 | 可恢复性 | 数据弹性 | 性能 | 风险/权衡 |
|------------|--------------|----------------|-----------------|-------------|-----------------|
| Geo | 取决于 Geo 站点的架构。可以在单节点和多节点配置中部署次要节点 | 最终一致。恢复点取决于复制滞后，这取决于许多因素，例如网络速度。Geo 支持使用可编写脚本的手动命令从主要站点故障转移到次要站点 | Geo 复制 100% 的计划数据类型并验证 50%。有关详细信息，请参阅[限制表](../geo/replication/datatypes.md#limitations-on-replicationverification)。 | 改进次要站点用户的读取/克隆时间。  | Geo 无意取代其他备份/恢复解决方案。由于复制滞后和从主站点复制坏数据的可能性，客户还应该定期备份他们的主站点并测试恢复过程 |

## 故障模式和可用缓解路径的场景

下表概述了上表中详述的功能的故障模式和缓解路径。注意 - Gitaly 集群安装假定复制参数为 3 或更大的奇数。

| Gitaly 模式 | 单个 Gitaly 节点丢失 | 应用程序/数据损坏 | 区域中断（实例丢失） | 备注 |
| ----------- | -------------------------- | ----------------------------- | ---------------------------------- | ----- |
| 单个 Gitaly 节点 | 停机 - 必须从备份中恢复 | 停机 - 必须从备份中恢复 | 停机 - 必须等待停机结束 | |
| 单个 Gitaly 节点 + Geo 次要节点 | 停机 - 必须从备份恢复，可以执行手动故障转移到次要节点 | 停机 - 必须从备份恢复，错误可能已经传播到次要节点 | 手动干预 - 故障转移到 Geo 次要节点 | |
| 分片 Gitaly 安装 | 部分停机 - 只有受影响节点上的仓库受到影响，必须从备份中恢复 | 部分停机 - 只有受影响节点上的存储库受到影响，必须从备份中恢复 | 停机 - 必须等待停机结束 | |
| 分片 Gitaly 安装 + Geo 次要节点 | 部分停机 - 只有受影响节点上的仓库受影响，必须从备份中恢复，可以对受影响的仓库执行手动故障转移到次要节点 | 部分停机 - 只有受影响节点上的仓库受到影响，必须从备份中恢复，错误可能已经传播到次要节点 | 手动干预 - 故障转移到 Geo 次要节点 | |
| Gitaly 集群安装 | 无停机 - 10 秒后将主仓库交换到另一个节点 | 不适用 - 所有写入都由多个 Gitaly 集群节点投票 |  停机 - 必须等待停机结束 | 目前不支持 Gitaly 集群节点的快照备份 |
| Gitaly 集群安装 + Geo 次要节点 | 无停机 - 10 秒后将主仓库交换到另一个节点 | 不适用 - 所有写入都由多个 Gitaly 集群节点投票 | 手动干预 - 故障转移到 Geo 次要节点 | 目前不支持 Gitaly 集群节点的快照备份 |
