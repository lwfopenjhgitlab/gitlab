---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# 文件钩子 **(FREE SELF)**

使用自定义文件钩子（不要与[服务器钩子](server_hooks.md)或[系统钩子](system_hooks.md)混淆），在不修改极狐GitLab 源代码的情况下引入自定义集成。

文件钩子在每个事件上运行。您可以过滤文件钩子代码中的事件或项目，并根据需要创建许多文件钩子。如果发生事件，系统会异步触发每个文件挂钩。有关事件列表，请参阅[系统钩子](system_hooks.md)文档。

NOTE:
文件钩子必须在极狐GitLab 服务器的文件系统上配置。只有极狐GitLab 服务器管理员可以完成这些任务。如果您没有文件系统访问权限，请浏览[系统钩子](system_hooks.md)或 [webhooks](../user/project/integrations/webhooks.md) 作为选项。

<!--
除了编写和支持自己的文件钩子之外，您还可以直接对 GitLab 源代码进行更改并回馈上游。 通过这种方式，我们可以确保跨版本保留功能并被测试覆盖。
-->

## 设置

文件钩子必须直接放在 `file_hooks` 目录中，子目录被忽略。在 [`file_hooks`](https://jihulab.com/gitlab-cn/gitlab/-/tree/master/file_hooks/examples) 中有一个 `example` 目录，您可以在其中找到一些基本示例。

按照以下步骤设置自定义钩子：

1. 在极狐GitLab 服务器上，定位到插件目录。对于源安装实例，路径通常是 `/home/git/gitlab/file_hooks/`。对于 Linux 软件包安装实例，路径通常是 `/opt/gitlab/embedded/service/gitlab-rails/file_hooks`。

    对于具有多个服务器的配置，您的钩子文件应该存在于每个应用程序服务器上。

1. 在 `file_hooks` 目录中，使用您选择的名称创建一个文件，不要使用空格或特殊字符。
1. 使钩子文件可执行并确保它归 Git 用户所有。
1. 编写代码，使文件钩子功能达到预期。可以使用任何语言，并确保顶部的 "shebang" 正确反映语言类型。例如，脚本在 Ruby 中，则 shebang 可能是 `#!/usr/bin/env ruby`。
1. 文件钩子的数据在 `STDIN` 上以 JSON 格式提供。它与[系统钩子](system_hooks.md)完全相同。

假设文件钩子代码被正确实现，钩子会在适当的时候触发。文件钩子文件列表会针对每个事件进行更新，无需重新启动极狐GitLab 即可应用新的文件钩子。

如果文件钩子以非零退出代码执行或极狐GitLab 无法执行它，则会将一条消息记录到：

- Linux 软件包安装实例：`gitlab-rails/file_hook.log`。
- 源安装实例：`log/file_hook.log`。

NOTE:
在 13.12 及更早版本中，文件名是 `plugin.log`。

## 创建文件钩子

此示例仅对事件 `project_create` 做出响应，极狐GitLab 实例会通知管理员已创建一个新项目。

```ruby
#!/opt/gitlab/embedded/bin/ruby
# By using the embedded ruby version we eliminate the possibility that our chosen language
# would be unavailable from
require 'json'
require 'mail'

# The incoming variables are in JSON format so we need to parse it first.
ARGS = JSON.parse($stdin.read)

# We only want to trigger this file hook on the event project_create
return unless ARGS['event_name'] == 'project_create'

# We will inform our admins of our gitlab instance that a new project is created
Mail.deliver do
  from    'info@gitlab_instance.com'
  to      'admin@gitlab_instance.com'
  subject "new project " + ARGS['name']
  body    ARGS['owner_name'] + 'created project ' + ARGS['name']
end
```

## 验证

编写自己的文件钩子可能会很棘手，如果您可以在不更改系统的情况下检查它会更容易。存在一个 Rake 任务，以便您可以在 staging 环境中使用它来测试您的文件挂钩，然后再在生产环境中使用它。Rake 任务使用样本数据并执行每个文件钩子，输出应该足以验证系统是否看到您的文件钩子以及它是否在没有错误的情况下执行。

```shell
# Omnibus installations
sudo gitlab-rake file_hooks:validate

# Installations from source
cd /home/git/gitlab
bundle exec rake file_hooks:validate RAILS_ENV=production
```

输出示例：

```plaintext
Validating file hooks from /file_hooks directory
* /home/git/gitlab/file_hooks/save_to_file.clj succeed (zero exit code)
* /home/git/gitlab/file_hooks/save_to_file.rb failure (non-zero exit code)
```
