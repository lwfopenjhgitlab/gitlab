---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---


# 启用手机号登录

## 启用 Feature Flag
启用 phone_authenticatable 的 feature 来允许手机号登录。

```ruby
::Feature.enable(:phone_authenticatable)
```

## 验证手机号登录

### 大陆手机号

大陆地区手机号默认不需要输入区域码前缀 `+86`。

1. 打开登录页面 https://jihulab.com/users/sign_in。
2. 可以看到用户名的标签提示文字包含 “手机号” 字样。
3. 输入手机号，比如：`185162621234`。
4. 输入密码。
5. 点击登录，即可完成登录。

![Sign-in with Phone](img/sign-in-with-phone.jpg)

### 其它区域手机号登录

其它区域手机号需要输入手机号码的区域码前缀，比如`+1`。

1. 打开登录页面 https://jihulab.com/users/sign_in。
2. 输入手机号，比如：`+1185162621234`。
3. 输入密码。
4. 点击登录。
5. 如果该用户存在，即可完成登录。
6. 如果该用户不存在，则可以看到关于账号密码不对并且大陆以外地区手机号码需要加前缀的提示文字。

![Sign-in with Phone Failed](img/sign-in-with-phone-failed.jpg)

到这里，你就完成了手机号登录的设置和验证了。