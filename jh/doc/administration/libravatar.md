---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# 在极狐GitLab 中使用 Libravatar 服务 **(FREE SELF)**

<!--
GitLab by default supports the [Gravatar](https://gravatar.com) avatar service.
-->

Libravatar 是一项将您的头像（个人资料图片）传送到其它网站的服务。Libravatar API [主要基于 gravatar](https://wiki.libravatar.org/api/)，因此您可以轻松切换到 Libravatar 头像服务，甚至是您自己的 Libravatar 服务器。

您不能在 FIPS 模式下使用任何 Libravatar 服务，包括 Gravatar。

## 将 Libravatar 服务更改为您自己的服务

NOTE:
您只能在 Libravatar 服务的 URL 中使用 MD5 哈希。

在 [`gitlab.yml` gravatar 部分](https://gitlab.cn/gitlab-cn/gitlab/-/blob/68dac188ec6b1b03d53365e7579422f44cbe7a1c/config/gitlab.yml.example#L469-476)中，配置选项设置如下：

**Omnibus 安装实例**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['gravatar_enabled'] = true
   #### For HTTPS
   gitlab_rails['gravatar_ssl_url'] = "https://seccdn.libravatar.org/avatar/%{hash}?s=%{size}&d=identicon"
   #### Use this line instead for HTTP
   # gitlab_rails['gravatar_plain_url'] = "http://cdn.libravatar.org/avatar/%{hash}?s=%{size}&d=identicon"
   ```

1. 要应用更改，请运行 `sudo gitlab-ctl reconfigure`。

**源安装实例**

1. 编辑 `config/gitlab.yml`：

   ```yaml
     gravatar:
       enabled: true
       # default: https://www.gravatar.com/avatar/%{hash}?s=%{size}&d=identicon
       plain_url: "http://cdn.libravatar.org/avatar/%{hash}?s=%{size}&d=identicon"
       # default: https://secure.gravatar.com/avatar/%{hash}?s=%{size}&d=identicon
       ssl_url: https://seccdn.libravatar.org/avatar/%{hash}?s=%{size}&d=identicon"
   ```

1. 保存文件，然后[重启](restart_gitlab.md#installations-from-source)极狐GitLab，使更改生效。

## 将 Libravatar 服务设置为默认值 (Gravatar)

**Omnibus 安装实例**

1. 从 `/etc/gitlab/gitlab.rb` 中删除 `gitlab_rails['gravatar_ssl_url']` 或 `gitlab_rails['gravatar_plain_url']`。
1. 要应用更改，请运行 `sudo gitlab-ctl reconfigure`。

**源安装实例**

1. 从 `config/gitlab.yml` 中删除 `gravatar:` 部分。
1. 保存文件，然后[重启](restart_gitlab.md#installations-from-source)极狐GitLab，应用更改。

## 禁用 Gravatar 服务

要禁用 Gravatar，例如禁止第三方服务，请完成以下步骤：

**Omnibus 安装实例**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['gravatar_enabled'] = false
   ```

1. 要应用更改，请运行 `sudo gitlab-ctl reconfigure`。

**源安装实例**

1. 编辑 `config/gitlab.yml`：

   ```yaml
     gravatar:
       enabled: false
   ```

1. 保存文件，然后[重启](restart_gitlab.md#installations-from-source)极狐GitLab，应用更改。

### 您自己的 Libravatar 服务器

如果您[运行自己的 Libravatar 服务](https://wiki.libravatar.org/running_your_own/)，则配置中的 URL 不同，但您必须提供相同的占位符，以便极狐GitLab 可以正确解析 URL。

例如，您在 `https://libravatar.example.com` 上托管服务，并且需要在 `gitlab.yml` 中提供的 `ssl_url` 是：

`https://libravatar.example.com/avatar/%{hash}?s=%{size}&d=identicon`

## 丢失图像的默认 URL

Libravatar 支持在 Libravatar 服务上，为找不到的用户电子邮件地址提供不同的缺失图像集。

如要使用 `identicon` 以外的集合，请将 URL 的 `&d=identicon` 部分替换为另一个受支持的集合。例如，您可以使用 `retro` 集，在这种情况下 URL 将如下所示：`ssl_url: "https://seccdn.libravatar.org/avatar/%{hash}?s=%{size}&d=retro"`

## Microsoft Office 365 的使用示例

如果您的用户是 Office 365 用户，则可以使用 `GetPersonaPhoto` 服务。
此服务需要登录，因此此用例在所有用户都可以访问 Office 365 的企业安装中最有用。

```ruby
gitlab_rails['gravatar_plain_url'] = 'http://outlook.office.com/owa/service.svc/s/GetPersonaPhoto?email=%{email}&size=HR120x120'
gitlab_rails['gravatar_ssl_url'] = 'https://outlook.office.com/owa/service.svc/s/GetPersonaPhoto?email=%{email}&size=HR120x120'
```

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
