---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 接收电子邮件 **(FREE SELF)**

极狐GitLab 有几个基于接收传入电子邮件的功能：

- [通过电子邮件回复](reply_by_email.md)：允许极狐GitLab 用户通过回复通知电子邮件，来评论议题和合并请求。
- [通过电子邮件新建议题](../user/project/issues/managing_issues.md#by-sending-an-email)：允许极狐GitLab 用户通过向用户特定的电子邮件地址发送电子邮件，来创建新议题。
- [通过电子邮件新建合并请求](../user/project/merge_requests/creating_merge_requests.md#by-sending-an-email)：允许极狐GitLab 用户通过向用户特定的电子邮件地址发送电子邮件，来创建新的合并请求。
- [服务台](../user/project/service_desk.md)：通过极狐GitLab 为您的客户提供电子邮件支持。

## 要求

我们建议使用一个电子邮件地址来接收**仅**用于极狐GitLab 实例的消息。任何不打算用于极狐GitLab 的接收电子邮件都会收到拒绝通知。

处理接收电子邮件需要启用 IMAP 的电子邮件帐户。极狐GitLab 需要以下三种策略之一：

- 电子邮件子地址（推荐）
- 万能邮箱
- 专用电子邮件地址（仅支持通过电子邮件回复）

让我们逐一介绍这些选项。

<a id="email-sub-addressing"></a>

### 电子邮件子地址

子地址是一种邮件服务器功能，任何发往 `user+arbitrary_tag@example.com` 的电子邮件最终都会出现在 `user@example.com` 的邮箱中。 它受到 Gmail、Google Apps、Yahoo! Mail、Outlook.com 和 iCloud 等提供商的支持，以及您可以在本地运行的 [Postfix 邮件服务器](reply_by_email_postfix_setup.md)。
Microsoft Exchange Server [不支持子地址](#microsoft-exchange-server)，Microsoft Office 365 [默认不支持子地址](#microsoft-office-365)。

NOTE:
如果您的提供商或服务器支持电子邮件子地址，我们建议使用它。专用电子邮件地址仅支持“通过电子邮件回复”功能。
万能邮箱支持与子地址相同的功能，但仍然首选子地址，因为只使用一个电子邮件地址，从而使万能邮箱可用于极狐GitLab 之外的其他目的。

<a id="catch-all-mailbox"></a>

### 万能邮箱

域名的全能邮箱接收发往该域名，但与邮件服务器上存在的任何地址都不匹配的所有电子邮件。

### 专用电子邮件地址

要设置此解决方案，您必须创建一个专用电子邮件地址来接收用户对极狐GitLab 通知的回复。但是，此方法仅支持回复，不支持接收电子邮件的其他功能。

## 可接受的 headers

> 接受 `Received` headers，引入于 14.9 版本，[功能标志](feature_flags.md)为 `use_received_header_for_incoming_emails`。默认启用。

当配置的电子邮件地址出现在以下 headers 之一中时，电子邮件将被正确处理（按检查顺序排序）：

- `To`
- `Delivered-To`
- `Envelope-To` 或 `X-Envelope-To`
- `Received`

`References` header 也被接受，但它专门用于将电子邮件响应与现有讨论主题相关联。它不用于通过电子邮件创建议题。

在 14.6 及更高版本中，[服务台](../user/project/service_desk.md)还会检查接受的 headers。

通常，“To” 字段包含主要收件人的电子邮件地址。
但是，如果出现以下情况，它可能不包括配置的极狐GitLab 电子邮件地址：

- 地址在 “CC” 字段中。
- 使用 “Reply all” 时包含地址。
- 电子邮件已转发。

`Received` header 可以包含多个电子邮件地址。这些是按照它们出现的顺序进行检查的。使用第一个匹配项。

<a id="rejected-headers"></a>

## 拒绝的 headers

为了防止从自动电子邮件系统创建不需要的议题，极狐GitLab 忽略所有包含以下 headers 的接收电子邮件：

- `Auto-Submitted` 的值不是 `no`
- `X-Autoreply` 的值为 `yes`

<a id="set-it-up"></a>

## 设置

如果您想使用 Gmail / Google Apps 接收电子邮件，请确保您已启用 IMAP 访问并允许不太安全的应用程序访问帐户，或打开两步验证并使用应用程序密码。

如果您想使用 Office 365，并且启用了双重身份验证，请确保您使用的是[应用密码](https://support.microsoft.com/en-us/account-billing/manage-app-passwords-for-two-step-verification-d6dc8c6d-4bf7-4851-ad95-6d07799387e9)而不是邮箱的常规密码。

要在 Ubuntu 上设置具有 IMAP 访问权限的基本 Postfix 邮件服务器，请遵循 [Postfix 设置文档](reply_by_email_postfix_setup.md)。

### 安全问题

WARNING:
选择用于接收电子邮件的域名时要小心。

例如，您的顶级公司域是 `hooli.com`。贵公司的所有员工都通过 Google Apps 在该域中拥有一个电子邮件地址，并且贵公司的私有 Slack 实例需要一个有效的 `@holi.com` 电子邮件地址才能注册。

如果您还在 `hooli.com` 托管一个面向公众的极狐GitLab 实例，并将您的传入电子邮件域设置为 `hooli.com`，攻击者可能会滥用“通过电子邮件创建新议题”或“[通过电子邮件创建新的合并请求](../user/project/merge_requests/creating_merge_requests.md#by-sending-an-email)”功能，在注册 Slack 时使用项目的唯一地址作为电子邮件，系统将发送一封确认电子邮件，在攻击者拥有的项目上创建一个新议题或合并请求，允许他们选择确认链接，并在您公司的私有 Slack 实例上验证他们的帐户。

我们建议在子域（例如 `incoming.hooli.com`）上接收电子邮件，并确保您不使用任何仅基于访问电子邮件域（例如“*.hooli.com”）进行身份验证的服务。
或者，使用专用域进行极狐GitLab 电子邮件通信，例如 `hooli-gitlab.com`。

WARNING:
使用已配置为减少垃圾邮件的邮件服务器。
例如，在默认配置上运行的 Postfix 邮件服务器可能会导致滥用。在配置的邮箱上收到的所有消息都会被处理，并且不打算用于极狐GitLab 实例的消息会收到拒绝通知。如果发件人的地址被欺骗，拒绝通知将发送到欺骗的 `FROM` 地址，这可能导致邮件服务器的 IP 或域出现在阻止列表中。

### Linux 软件包安装实例

1. 在 `/etc/gitlab/gitlab.rb` 中找到 `incoming_email` 部分，启用该功能，并填写您的特定 IMAP 服务器和电子邮件帐户的详细信息（请参阅下面的[示例](#configuration-examples)）。

1. 重新配置极狐GitLab，使更改生效：

   ```shell
   sudo gitlab-ctl reconfigure

   # Needed when enabling or disabling for the first time but not for password changes.
   # See https://gitlab.com/gitlab-org/gitlab-foss/-/issues/23560#note_61966788
   sudo gitlab-ctl restart
   ```

1. 验证一切是否正确配置：

   ```shell
   sudo gitlab-rake gitlab:incoming_email:check
   ```

现在应该可以通过电子邮件回复。

### 源安装

1. 进入极狐GitLab 安装目录：

   ```shell
   cd /home/git/gitlab
   ```

1. 手动安装 `gitlab-mail_room` gem：

   ```shell
   gem install gitlab-mail_room
   ```

   注意：此步骤对于避免线程死锁和支持最新的 MailRoom 功能是必要的。

1. 在 `config/gitlab.yml` 中找到 `incoming_email` 部分，启用该功能，并填写您的特定 IMAP 服务器和电子邮件帐户的详细信息（请参阅下面的[示例](#configuration-examples)）。

如果您使用 systemd 单元来管理极狐GitLab：

1. 添加 `gitlab-mailroom.service` 作为 `gitlab.target` 的依赖项：

   ```shell
   sudo systemctl edit gitlab.target
   ```

   在打开的编辑器中，添加以下内容并保存文件：

   ```plaintext
   [Unit]
   Wants=gitlab-mailroom.service
   ```

1. 如果您在同一台机器上运行 Redis 和 PostgreSQL，应该添加对 Redis 的依赖。运行：

   ```shell
   sudo systemctl edit gitlab-mailroom.service
   ```

   在打开的编辑器中，添加以下内容并保存文件：

   ```plaintext
   [Unit]
   Wants=redis-server.service
   After=redis-server.service
   ```

1. 启动 `gitlab-mailroom.service`：

   ```shell
   sudo systemctl start gitlab-mailroom.service
   ```

1. 验证一切是否正确配置：

   ```shell
   sudo -u git -H bundle exec rake gitlab:incoming_email:check RAILS_ENV=production
   ```

如果您使用 SysV 初始化脚本来管理极狐GitLab：

1. 在 `/etc/default/gitlab` 的初始化脚本中启用 `mail_room`：

   ```shell
   sudo mkdir -p /etc/default
   echo 'mail_room_enabled=true' | sudo tee -a /etc/default/gitlab
   ```

1. 重启极狐GitLab：

   ```shell
   sudo service gitlab restart
   ```

1. 验证一切是否正确配置：

   ```shell
   sudo -u git -H bundle exec rake gitlab:incoming_email:check RAILS_ENV=production
   ```

现在应该可以通过电子邮件回复。

<a id="configuration-examples"></a>

### 配置示例

#### Postfix

Postfix 邮件服务器的示例配置。假设邮箱`incoming@gitlab.example.com`。

Linux 软件包安装实例：

```ruby
gitlab_rails['incoming_email_enabled'] = true

# The email address including the %{key} placeholder that will be replaced to reference the
# item being replied to. This %{key} should be included in its entirety within the email
# address and not replaced by another value.
# For example: emailaddress+%{key}@gitlab.example.com.
# The placeholder must appear in the "user" part of the address (before the `@`).
gitlab_rails['incoming_email_address'] = "incoming+%{key}@gitlab.example.com"

# Email account username
# With third party providers, this is usually the full email address.
# With self-hosted email servers, this is usually the user part of the email address.
gitlab_rails['incoming_email_email'] = "incoming"
# Email account password
gitlab_rails['incoming_email_password'] = "[REDACTED]"

# IMAP server host
gitlab_rails['incoming_email_host'] = "gitlab.example.com"
# IMAP server port
gitlab_rails['incoming_email_port'] = 143
# Whether the IMAP server uses SSL
gitlab_rails['incoming_email_ssl'] = false
# Whether the IMAP server uses StartTLS
gitlab_rails['incoming_email_start_tls'] = false

# The mailbox where incoming mail will end up. Usually "inbox".
gitlab_rails['incoming_email_mailbox_name'] = "inbox"
# The IDLE command timeout.
gitlab_rails['incoming_email_idle_timeout'] = 60

# Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
gitlab_rails['incoming_email_expunge_deleted'] = true
```

源安装实例：

```yaml
incoming_email:
    enabled: true

    # The email address including the %{key} placeholder that will be replaced to reference the
    # item being replied to. This %{key} should be included in its entirety within the email
    # address and not replaced by another value.
    # For example: emailaddress+%{key}@gitlab.example.com.
    # The placeholder must appear in the "user" part of the address (before the `@`).
    address: "incoming+%{key}@gitlab.example.com"

    # Email account username
    # With third party providers, this is usually the full email address.
    # With self-hosted email servers, this is usually the user part of the email address.
    user: "incoming"
    # Email account password
    password: "[REDACTED]"

    # IMAP server host
    host: "gitlab.example.com"
    # IMAP server port
    port: 143
    # Whether the IMAP server uses SSL
    ssl: false
    # Whether the IMAP server uses StartTLS
    start_tls: false

    # The mailbox where incoming mail will end up. Usually "inbox".
    mailbox: "inbox"
    # The IDLE command timeout.
    idle_timeout: 60

    # Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
    expunge_deleted: true
```

#### Gmail

Gmail/Google Workspace 的示例配置。假设邮箱为 `gitlab-incoming@gmail.com`。

NOTE:
`incoming_email_email` 不能作为 Gmail 别名帐户。

Linux 软件包安装实例：

```ruby
gitlab_rails['incoming_email_enabled'] = true

# The email address including the %{key} placeholder that will be replaced to reference the
# item being replied to. This %{key} should be included in its entirety within the email
# address and not replaced by another value.
# For example: emailaddress+%{key}@gmail.com.
# The placeholder must appear in the "user" part of the address (before the `@`).
gitlab_rails['incoming_email_address'] = "gitlab-incoming+%{key}@gmail.com"

# Email account username
# With third party providers, this is usually the full email address.
# With self-hosted email servers, this is usually the user part of the email address.
gitlab_rails['incoming_email_email'] = "gitlab-incoming@gmail.com"
# Email account password
gitlab_rails['incoming_email_password'] = "[REDACTED]"

# IMAP server host
gitlab_rails['incoming_email_host'] = "imap.gmail.com"
# IMAP server port
gitlab_rails['incoming_email_port'] = 993
# Whether the IMAP server uses SSL
gitlab_rails['incoming_email_ssl'] = true
# Whether the IMAP server uses StartTLS
gitlab_rails['incoming_email_start_tls'] = false

# The mailbox where incoming mail will end up. Usually "inbox".
gitlab_rails['incoming_email_mailbox_name'] = "inbox"
# The IDLE command timeout.
gitlab_rails['incoming_email_idle_timeout'] = 60

# Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
gitlab_rails['incoming_email_expunge_deleted'] = true
```

源安装实例：

```yaml
incoming_email:
    enabled: true

    # The email address including the %{key} placeholder that will be replaced to reference the
    # item being replied to. This %{key} should be included in its entirety within the email
    # address and not replaced by another value.
    # For example: emailaddress+%{key}@gmail.com.
    # The placeholder must appear in the "user" part of the address (before the `@`).
    address: "gitlab-incoming+%{key}@gmail.com"

    # Email account username
    # With third party providers, this is usually the full email address.
    # With self-hosted email servers, this is usually the user part of the email address.
    user: "gitlab-incoming@gmail.com"
    # Email account password
    password: "[REDACTED]"

    # IMAP server host
    host: "imap.gmail.com"
    # IMAP server port
    port: 993
    # Whether the IMAP server uses SSL
    ssl: true
    # Whether the IMAP server uses StartTLS
    start_tls: false

    # The mailbox where incoming mail will end up. Usually "inbox".
    mailbox: "inbox"
    # The IDLE command timeout.
    idle_timeout: 60

    # Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
    expunge_deleted: true
```

<a id="microsoft-exchange-server"></a>

#### Microsoft Exchange Server

启用 IMAP 的 Microsoft Exchange Server 的示例配置。因为 Exchange 不支持子地址，所以只存在两个选项：

- [万能邮箱](#catch-all-mailbox)（建议仅用于 Exchange）
- [专用电子邮件地址](#dedicated-email-address)（仅支持通过电子邮件回复功能）

<a id="catch-all-mailbox"></a>

##### 万能邮箱

假设万能邮箱为 `incoming@exchange.example.com`。

Linux 软件包安装实例：

```ruby
gitlab_rails['incoming_email_enabled'] = true

# The email address including the %{key} placeholder that will be replaced to reference the
# item being replied to. This %{key} should be included in its entirety within the email
# address and not replaced by another value.
# For example: emailaddress-%{key}@exchange.example.com.
# The placeholder must appear in the "user" part of the address (before the `@`).
# Exchange does not support sub-addressing, so a catch-all mailbox must be used.
gitlab_rails['incoming_email_address'] = "incoming-%{key}@exchange.example.com"

# Email account username
# Typically this is the userPrincipalName (UPN)
gitlab_rails['incoming_email_email'] = "incoming@ad-domain.example.com"
# Email account password
gitlab_rails['incoming_email_password'] = "[REDACTED]"

# IMAP server host
gitlab_rails['incoming_email_host'] = "exchange.example.com"
# IMAP server port
gitlab_rails['incoming_email_port'] = 993
# Whether the IMAP server uses SSL
gitlab_rails['incoming_email_ssl'] = true

# Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
gitlab_rails['incoming_email_expunge_deleted'] = true
```

源安装实例：

```yaml
incoming_email:
    enabled: true

    # The email address including the %{key} placeholder that will be replaced to reference the
    # item being replied to. This %{key} should be included in its entirety within the email
    # address and not replaced by another value.
    # For example: emailaddress-%{key}@exchange.example.com.
    # The placeholder must appear in the "user" part of the address (before the `@`).
    # Exchange does not support sub-addressing, so a catch-all mailbox must be used.
    address: "incoming-%{key}@exchange.example.com"

    # Email account username
    # Typically this is the userPrincipalName (UPN)
    user: "incoming@ad-domain.example.com"
    # Email account password
    password: "[REDACTED]"

    # IMAP server host
    host: "exchange.example.com"
    # IMAP server port
    port: 993
    # Whether the IMAP server uses SSL
    ssl: true

    # Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
    expunge_deleted: true
```

<a id="dedicated-email-address"></a>

##### 专用电子邮件地址

NOTE:
仅支持[通过电子邮件回复功能](reply_by_email.md)。无法支持[服务台](../user/project/service_desk.md)。

假设专用电子邮件地址为 `incoming@exchange.example.com`。

Linux 软件包安装实例：

```ruby
gitlab_rails['incoming_email_enabled'] = true

# Exchange does not support sub-addressing, and we're not using a catch-all mailbox so %{key} is not used here
gitlab_rails['incoming_email_address'] = "incoming@exchange.example.com"

# Email account username
# Typically this is the userPrincipalName (UPN)
gitlab_rails['incoming_email_email'] = "incoming@ad-domain.example.com"
# Email account password
gitlab_rails['incoming_email_password'] = "[REDACTED]"

# IMAP server host
gitlab_rails['incoming_email_host'] = "exchange.example.com"
# IMAP server port
gitlab_rails['incoming_email_port'] = 993
# Whether the IMAP server uses SSL
gitlab_rails['incoming_email_ssl'] = true

# Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
gitlab_rails['incoming_email_expunge_deleted'] = true
```

源安装实例：

```yaml
incoming_email:
    enabled: true

    # Exchange does not support sub-addressing,
    # and we're not using a catch-all mailbox so %{key} is not used here
    address: "incoming@exchange.example.com"

    # Email account username
    # Typically this is the userPrincipalName (UPN)
    user: "incoming@ad-domain.example.com"
    # Email account password
    password: "[REDACTED]"

    # IMAP server host
    host: "exchange.example.com"
    # IMAP server port
    port: 993
    # Whether the IMAP server uses SSL
    ssl: true

    # Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
    expunge_deleted: true
```

<a id="microsoft-office-365"></a>

#### Microsoft Office 365

启用 IMAP 的 Microsoft Office 365 的示例配置。

##### 子地址邮箱

NOTE:
截至 2020 年 9 月，子地址支持[已添加到 Office 365](https://support.microsoft.com/en-us/office/uservoice-pages-430e1a78-e016-472a-a10f-dc2a3df3450a)。此功能默认不启用，必须通过 PowerShell 启用。

这一系列 PowerShell 命令在 Office 365 的组织级别启用[子地址](#email-sub-addressing)，允许组织中的所有邮箱接收子地址邮件。

要启用子地址：

1. 从 [PowerShell 库](https://www.powershellgallery.com/packages/ExchangeOnlineManagement/2.0.5)下载并安装 `ExchangeOnlineManagement` 模块。
1. 在 PowerShell 中，运行以下命令：

   ```powershell
   Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
   Import-Module ExchangeOnlineManagement
   Connect-ExchangeOnline
   Set-OrganizationConfig -AllowPlusAddressInRecipients $true
   Disconnect-ExchangeOnline
   ```

此 Linux 软件包安装实例假定邮箱为 `incoming@office365.example.com`：

```ruby
gitlab_rails['incoming_email_enabled'] = true

# The email address including the %{key} placeholder that will be replaced to reference the
# item being replied to. This %{key} should be included in its entirety within the email
# address and not replaced by another value.
# For example: emailaddress+%{key}@office365.example.com.
# The placeholder must appear in the "user" part of the address (before the `@`).
gitlab_rails['incoming_email_address'] = "incoming+%{key}@office365.example.com"

# Email account username
# Typically this is the userPrincipalName (UPN)
gitlab_rails['incoming_email_email'] = "incoming@office365.example.com"
# Email account password
gitlab_rails['incoming_email_password'] = "[REDACTED]"

# IMAP server host
gitlab_rails['incoming_email_host'] = "outlook.office365.com"
# IMAP server port
gitlab_rails['incoming_email_port'] = 993
# Whether the IMAP server uses SSL
gitlab_rails['incoming_email_ssl'] = true

# Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
gitlab_rails['incoming_email_expunge_deleted'] = true
```

此源安装实例假定邮箱为 `incoming@office365.example.com`：

```yaml
incoming_email:
    enabled: true

    # The email address including the %{key} placeholder that will be replaced to reference the
    # item being replied to. This %{key} should be included in its entirety within the email
    # address and not replaced by another value.
    # For example: emailaddress+%{key}@office365.example.com.
    # The placeholder must appear in the "user" part of the address (before the `@`).
    address: "incoming+%{key}@office365.example.comm"

    # Email account username
    # Typically this is the userPrincipalName (UPN)
    user: "incoming@office365.example.comm"
    # Email account password
    password: "[REDACTED]"

    # IMAP server host
    host: "outlook.office365.com"
    # IMAP server port
    port: 993
    # Whether the IMAP server uses SSL
    ssl: true

    # Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
    expunge_deleted: true
```

##### 万能邮箱

此 Linux 软件包安装实例假定万能邮箱为 `incoming@office365.example.com`。

```ruby
gitlab_rails['incoming_email_enabled'] = true

# The email address including the %{key} placeholder that will be replaced to reference the
# item being replied to. This %{key} should be included in its entirety within the email
# address and not replaced by another value.
# For example: emailaddress-%{key}@office365.example.com.
# The placeholder must appear in the "user" part of the address (before the `@`).
gitlab_rails['incoming_email_address'] = "incoming-%{key}@office365.example.com"

# Email account username
# Typically this is the userPrincipalName (UPN)
gitlab_rails['incoming_email_email'] = "incoming@office365.example.com"
# Email account password
gitlab_rails['incoming_email_password'] = "[REDACTED]"

# IMAP server host
gitlab_rails['incoming_email_host'] = "outlook.office365.com"
# IMAP server port
gitlab_rails['incoming_email_port'] = 993
# Whether the IMAP server uses SSL
gitlab_rails['incoming_email_ssl'] = true

# Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
gitlab_rails['incoming_email_expunge_deleted'] = true
```

此源安装实例假定万能邮箱为 `incoming@office365.example.com`：

```yaml
incoming_email:
    enabled: true

    # The email address including the %{key} placeholder that will be replaced to reference the
    # item being replied to. This %{key} should be included in its entirety within the email
    # address and not replaced by another value.
    # For example: emailaddress+%{key}@office365.example.com.
    # The placeholder must appear in the "user" part of the address (before the `@`).
    address: "incoming-%{key}@office365.example.com"

    # Email account username
    # Typically this is the userPrincipalName (UPN)
    user: "incoming@ad-domain.example.com"
    # Email account password
    password: "[REDACTED]"

    # IMAP server host
    host: "outlook.office365.com"
    # IMAP server port
    port: 993
    # Whether the IMAP server uses SSL
    ssl: true

    # Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
    expunge_deleted: true
```

##### 专用电子邮件地址

NOTE:
仅支持[通过电子邮件回复功能](reply_by_email.md)。无法支持[服务台](../user/project/service_desk.md)。

此 Linux 软件包安装实例假定专用电子邮件地址为 `incoming@office365.example.com`：

```ruby
gitlab_rails['incoming_email_enabled'] = true

gitlab_rails['incoming_email_address'] = "incoming@office365.example.com"

# Email account username
# Typically this is the userPrincipalName (UPN)
gitlab_rails['incoming_email_email'] = "incoming@office365.example.com"
# Email account password
gitlab_rails['incoming_email_password'] = "[REDACTED]"

# IMAP server host
gitlab_rails['incoming_email_host'] = "outlook.office365.com"
# IMAP server port
gitlab_rails['incoming_email_port'] = 993
# Whether the IMAP server uses SSL
gitlab_rails['incoming_email_ssl'] = true

# Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
gitlab_rails['incoming_email_expunge_deleted'] = true
```

此源安装实例假定专用电子邮件地址为 `incoming@office365.example.com`：

```yaml
incoming_email:
    enabled: true

    address: "incoming@office365.example.com"

    # Email account username
    # Typically this is the userPrincipalName (UPN)
    user: "incoming@office365.example.com"
    # Email account password
    password: "[REDACTED]"

    # IMAP server host
    host: "outlook.office365.com"
    # IMAP server port
    port: 993
    # Whether the IMAP server uses SSL
    ssl: true

    # Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
    expunge_deleted: true
```

#### Microsoft Graph

> 引入于 13.11 版本。

极狐GitLab 可以使用 Microsoft Graph API 而不是 IMAP 读取接收电子邮件。因为 [Microsoft 正在弃用使用基本身份验证的 IMAP](https://techcommunity.microsoft.com/t5/exchange-team-blog/announcing-oauth-2-0-support-for-imap-and-smtp-auth-protocols-in/ba-p/1330432)，新的 Microsoft Exchange Online 邮箱很快将需要 Microsoft Graph API。

要为 Microsoft Graph 配置极狐GitLab，您需要在 Azure Active Directory 中注册一个 OAuth2 应用程序，该应用程序对所有邮箱都具有 `Mail.ReadWrite` 权限。请参阅 [MailRoom 分步指南](https://github.com/tpitale/mail_room/#microsoft-graph-configuration)和 [Microsoft 说明](https://learn.microsoft.com/en-us/azure/active-directory/develop/quickstart-register-app)了解更多详细信息。

在配置 OAuth2 应用程序时记录以下内容：

- Azure Active Directory 的租户 ID
- 您的 OAuth2 应用程序的客户端 ID
- 客户端对您的 OAuth2 应用程序进行保密

##### 限制邮箱访问

要使 MailRoom 作为服务帐户工作，您在 Azure Active Directory 中创建的应用程序要求您将 `Mail.ReadWrite` 属性设置为在*所有*邮箱中读取/写入邮件。

为了缓解安全问题，我们建议配置一个应用程序访问策略，以限制所有帐户的邮箱访问，如 [Microsoft 文档](https://learn.microsoft.com/en-us/graph/auth-limit-mailbox-access)中所述。

Omnibus GitLab 的此示例假设您使用以下邮箱：`incoming@example.onmicrosoft.com`：

##### 配置 Microsoft Graph

> 替代 Azure 部署引入于 14.9 版本。

```ruby
gitlab_rails['incoming_email_enabled'] = true

# The email address including the %{key} placeholder that will be replaced to reference the
# item being replied to. This %{key} should be included in its entirety within the email
# address and not replaced by another value.
# For example: emailaddress+%{key}@example.onmicrosoft.com.
# The placeholder must appear in the "user" part of the address (before the `@`).
gitlab_rails['incoming_email_address'] = "incoming+%{key}@example.onmicrosoft.com"

# Email account username
gitlab_rails['incoming_email_email'] = "incoming@example.onmicrosoft.com"

gitlab_rails['incoming_email_inbox_method'] = 'microsoft_graph'
gitlab_rails['incoming_email_inbox_options'] = {
   'tenant_id': '<YOUR-TENANT-ID>',
   'client_id': '<YOUR-CLIENT-ID>',
   'client_secret': '<YOUR-CLIENT-SECRET>',
   'poll_interval': 60  # Optional
}
```

<!--
For Microsoft Cloud for US Government or [other Azure deployments](https://docs.microsoft.com/en-us/graph/deployments), configure the `azure_ad_endpoint` and `graph_endpoint` settings.

- Example for Microsoft Cloud for US Government:

```ruby
gitlab_rails['incoming_email_inbox_options'] = {
   'azure_ad_endpoint': 'https://login.microsoftonline.us',
   'graph_endpoint': 'https://graph.microsoft.us',
   'tenant_id': '<YOUR-TENANT-ID>',
   'client_id': '<YOUR-CLIENT-ID>',
   'client_secret': '<YOUR-CLIENT-SECRET>',
   'poll_interval': 60  # Optional
}
```
-->

源安装中尚不支持 Microsoft Graph API。

### 使用加密凭据

> 引入于 15.9 版本。

您可以选择将加密文件用于传入电子邮件凭据，而不是将传入电子邮件凭据以明文形式存储在配置文件中。

先决条件：

- 要使用加密凭证，您必须首先启用[加密配置](encrypted_configuration.md)。

加密文件支持的配置项有：

- `user`
- `password`

**Omnibus**

1. 如果最初您在 `/etc/gitlab/gitlab.rb` 中的传入电子邮件配置如下所示：

   ```ruby
   gitlab_rails['incoming_email_email'] = "incoming-email@mail.example.com"
   gitlab_rails['incoming_email_password'] = "examplepassword"
   ```

1. 编辑加密的 secret：

   ```shell
   sudo gitlab-rake gitlab:incoming_email:secret:edit EDITOR=vim
   ```

1. 输入传入电子邮件密码的未加密内容：

   ```yaml
   user: 'incoming-email@mail.example.com'
   password: 'examplepassword'
   ```

1. 编辑 `/etc/gitlab/gitlab.rb` 并删除 `email` 和 `password` 的 `incoming_email` 设置。
1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

使用 Kubernetes secret 存储传入电子邮件密码。有关详细信息，请阅读 [Helm IMAP secret](https://docs.gitlab.cn/charts/installation/secrets.html#imap-password-for-incoming-emails)。

**Docker**

1. 如果最初您在 `docker-compose.yml` 中的传入电子邮件配置如下所示：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       image: 'gitlab/gitlab-ee:latest'
       restart: always
       hostname: 'gitlab.example.com'
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['incoming_email_email'] = "incoming-email@mail.example.com"
           gitlab_rails['incoming_email_password'] = "examplepassword"
   ```

1. 进入容器，并编辑加密的 secret：

   ```shell
   sudo docker exec -t <container_name> bash
   gitlab-rake gitlab:incoming_email:secret:edit EDITOR=editor
   ```

1. 输入传入电子邮件密码的未加密内容：

   ```yaml
   user: 'incoming-email@mail.example.com'
   password: 'examplepassword'
   ```

1. 编辑 `docker-compose.yml` 并删除 `email` 和 `password` 的 `incoming_email` 设置。
1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 如果最初您在 `/home/git/gitlab/config/gitlab.yml` 中的传入电子邮件配置如下所示：

   ```yaml
   production:
     incoming_email:
       user: 'incoming-email@mail.example.com'
       password: 'examplepassword'
   ```

1. 编辑加密的 secret：

   ```shell
   bundle exec rake gitlab:incoming_email:secret:edit EDITOR=vim RAILS_ENVIRONMENT=production
   ```

1. 输入传入电子邮件密码的未加密内容：

   ```yaml
   user: 'incoming-email@mail.example.com'
   password: 'examplepassword'
   ```

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并删除 `incoming_email:` 用于 `user` 和 `password` 的设置。
1. 保存文件并重启极狐GitLab 和 Mailroom。

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```
