---
stage: Manage
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 审计事件 **(PREMIUM)**

使用审计事件来跟踪重要事件，包括执行相关操作的人员和时间。您可以使用审计事件来跟踪例如：

- 谁更改了极狐GitLab 项目的特定用户的权限级别，以及何时更改。
- 谁添加了新用户或删除了用户，以及何时更改。

极狐GitLab API、数据库和 `audit_json.log` 记录了许多审计事件。一些审计事件只能通过[审计事件流](audit_event_streaming.md)获得。

您还可以生成审计事件的[审计报告](audit_reports.md)。

<!--
NOTE:
You can't configure a retention policy for audit events, but epic
[7917](https://gitlab.com/groups/gitlab-org/-/epics/7917) proposes to change this.
-->

## 时区

> 引入于 15.7 版本，极狐GitLab UI 显示的日期和时间，使用用户本地时区，而不是 UTC。

查看审计事件的位置不同，使用的时区也不同：

- 在 UI 中，使用您当地的时区（15.7 及更高版本）或 UTC（15.6 及更早版本）。
- [审计事件 API](../api/audit_events.md) 默认返回 UTC 日期和时间。在私有化部署实例上返回[配置的时区](timezone.md)。
- 在 `audit_json.log` 中，使用 UTC。
- 在 CSV 导出中，使用 UTC。

## 查看审计事件

根据您要查看的事件，您必须至少满足以下条件：

- 对于群组中所有用户的群组审计事件，具有该群组的所有者角色。
- 对于项目中所有用户的项目审计事件，具有该项目的维护者角色。
- 对于基于您自己的操作生成的群组和项目审计事件，具有群组或项目的开发者角色。
- [审计员用户](auditor_users.md)可以查看所有用户的群组和项目事件。

您可以查看限定群组或项目范围内的审计事件。

查看群组的审计事件：

1. 进入群组。
1. 在左侧边栏上，选择 **安全与合规 > 审计事件**。 

群组事件不包括项目审计事件，您也可以使用[群组审计事件 API](../api/audit_events.md#group-audit-events)，访问群组事件。群组事件查询的期限最长为 30 天。

查看项目的审计事件：

1. 进入项目。
1. 在左侧边栏中，选择 **安全与合规 > 审计事件**。

您也可以使用[项目审计事件 API](../api/audit_events.md#project-audit-events)，访问项目事件。项目事件查询的期限最长为 30 天。

## 查看实例审计事件 **(PREMIUM SELF)**

您可以查看整个极狐GitLab 实例中，用户操作的审计事件。

查看实例审计事件：

1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **监控 > 审计事件**。

## 导出到 CSV **(PREMIUM SELF)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/1449) in GitLab 13.4.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/285441) in GitLab 13.7.
-->

您可以将实例审计事件的当前视图（包括过滤器）导出为 CSV 文件。

要将实例审计事件导出到 CSV：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，选择 **监控 > 审计事件**。
1. 选择可用的搜索[过滤器](#搜索)。
1. 选择 **导出为 CSV**。

导出的文件：

- 始终按 `created_at` 升序排序。
- 限制为最多 100,000 个事件，达到此限制时，将截断其余记录。

### 格式

数据使用逗号作为列分隔符进行编码，如果需要，使用 `"` 来引用字段，并使用换行符分隔行。
第一行包含标题，下表列出了这些标题以及值的描述：

| 列 | 描述 |
|---------|-------------|
| ID | 审计事件 `id` |
| Author ID | 作者 ID |
| Author Name | 作者的全名 |
| Entity ID | 事件范围 ID |
| Entity Type | 事件范围类型 (`Project`/`Group`/`User`) |
| Entity Path | 事件范围路径 |
| Target ID | 目标 ID|
| Target Type | 目标类型 |
| Target Details | 目标详情 |
| Action | 操作描述 |
| IP Address | 执行该操作的作者的 IP 地址 |
| Created At (UTC) | 格式 `YYYY-MM-DD HH:MM:SS` |

## 查看登录事件 **(FREE)**

登录成功事件是唯一在所有订阅级别均可用的审计事件。要查看登录成功事件：

1. 选择您的头像。
1. 选择 **编辑个人资料 > 认证日志**。

升级到付费订阅级别后，您还可以在审计事件页面上看到登录成功事件。

## 过滤审计事件

在审计事件页面中，可以使用不同的过滤器，具体取决于您所在的页面。

| 审计事件页面 | 可用的过滤器                                                                                                       |
|:-----------------|:-----------------------------------------------------------------------------------------------------------------------|
| 项目          | 执行操作的用户（项目成员）。                                                                 |
| 群组         | 执行操作的用户（群组成员）。                                                                  |
| 实例        | 群组、项目或用户。                                                                                              |
| 全部              | 日期范围按钮和选择器（最大范围为 31 天）。默认为从该月的第一天到今天的日期。 |

<a id="user-impersonation"></a>

## 用户模拟

> - 引入于 13.0 版本。
> - 群组审计事件中包含模拟会话事件，引入于 14.8 版本。

当用户被[模拟](../user/admin_area/index.md#user-impersonation)时，他们的操作将被记录为审计事件，并包含其他详细信息：

- 审计事件包括有关模拟管理员的信息。根据审计事件类型（群组、项目或用户），这些审计事件在审计事件页面中可见。
- 为管理员模拟会话的开始和结束记录额外的审计事件。这些审计事件显示为：
  - 实例审计事件。
  - 用户所属的所有群组的群组审计事件。出于性能原因，群组审计事件仅限于您最早所属的 20 个群组。

![Audit event with impersonated user](img/impersonated_audit_events_v15_7.png)

## 可用的审计事件

根据您的极狐GitLab 版本，您可以查看不同的事件。

<a id="group-events"></a>

### 群组事件

对群组执行的以下操作，会生成群组审计事件：

#### 群组管理

- 群组名或路径的更改。
- 群组仓库大小限制的更改。
- 群组创建或删除。
- 群组可见性的更改
- 用户已添加到群组中以及具有哪些权限。
- 从群组中删除用户。
- 项目仓库导入群组。
- 与群组共享的项目以及权限。
- 删除以前与项目共享的群组。
- LFS 启用或禁用。
- 共享 runner 的分钟限制已更改。
- 启用或禁用成员锁定。
- 启用或禁用请求访问。
- 允许创建项目的角色已更改。
- 群组部署令牌已成功创建、撤销或删除。引入于 14.9 版本。
- 尝试创建群组部署令牌失败。引入于 14.9 版本。
- IP 限制变更。引入于 15.0 版本。
- 群组已关联、更改或取消关联安全策略项目。引入于 15.6 版本。
- 环境受保护或不受保护。引入于 15.8 版本。

#### 认证和授权

- 用户通过群组 SAML 登录。
- 于 14.5 版本，引入以下群组 SAML 配置的更改：
  - 启用状态
  - 对 Web 活动强制执行仅 SSO 身份验证。
  - 对 Git 和依赖代理活动强制执行仅 SSO 身份验证。
  - 强制用户拥有专用的群组管理帐户。
  - 禁止外部派生。
  - Identity provider SSO URL。
  - 证书指纹。
  - 默认成员角色。
  - SSO-SAML 群组同步配置。
- 将用户分配到群组的权限更改。
- 2FA 是否强制执行或宽限期已更改。

#### 合规和安全

- 创建、更新或删除合规框架。引入于 14.5 版本。
- 创建、更新或删除的事件流目的地。引入于 14.6 版本。
- 推送规则的变更。引入于 15.0 版本。
- 对审计事件流目的地的自定义 HTTP header 的更改。引入于 15.3 版本。
- 实例管理员开始或停止模拟群组成员。引入于 14.8 版本。

#### CI/CD

- 共享 runners 分钟数限制的变更。
- 群组 CI/CD 变量已添加、删除，或受保护状态已更改。

#### 代码协作

- 引入于 15.1 版本，更改以下合并请求批准设置：
  - 阻止作者批准。
  - 阻止添加提交的用户批准。
  - 阻止编辑项目中的批准规则和合并请求。
  - 需要用户密码才能批准。
  - 将提交添加到源分支时删除所有批准。

<!--
Group events can also be accessed via the [Group Audit Events API](../api/audit_events.md#group-audit-events)
-->

<a id="project-events"></a>

### 项目事件

对项目执行的以下操作，会生成项目审计事件：

- 添加或删除部署密钥
- 项目创建、删除、重命名、移动（转移）、更改路径
- 项目更改了可见性级别
- 用户已添加到项目中以及具有哪些权限
- 分配给项目的用户的权限更改
- 用户已从项目中删除
- 项目导出已下载
- 项目仓库已下载
- 项目已归档
- 项目取消归档
- 添加、删除或更新受保护的分支
- 发布已添加到项目中
- 发布已更新
- 发布已删除（引入于 15.3 版本）
- 发布里程碑关联已更改
- 提交者批准合并请求的权限已更新
  - 事件消息更改于 14.6 版本
- 作者批准合并请求的权限已更新
- 所需批准的数量已更新
- 在项目审批群组中添加或删除用户和群组
- 项目 CI/CD 变量添加、删除或受保护状态已更改
- 项目访问令牌已成功创建或撤销（引入于 13.9 版本）
- 尝试创建或撤销项目访问令牌失败（引入于 13.9 版本）
- 当项目的默认分支更改时（引入于 13.9 版本）
- 创建、更新或删除 DAST 配置文件、DAST 扫描程序配置文件和 DAST 站点配置文件（引入于 14.1 版本）
- 更改了项目的合规性框架（引入于 14.1 版本）
- 批准所需的用户密码已更新（引入于 14.2 版本）
- 合并请求中修改合并请求批准规则的权限已更新（引入于 14.2 版本）
- 将新提交添加到 MR 时的新批准要求已更新（引入于 14.2 版本）
- 功能标志的策略何时更改（引入于 14.3 版本）
- 允许强制推送到受保护的分支已更改（引入于 14.3 版本）
- 针对受保护分支的合并请求的代码所有者批准要求已更改（引入于 14.3 版本）
- 允许合并和推送到添加或删除的受保护分支的用户和群组（引入于 14.3 版本）
- 项目部署令牌已成功创建、撤销或删除（引入于 14.9 版本）
- 尝试创建项目部署令牌失败（引入于 14.9 版本）
- 添加、编辑或删除状态检查（引入于 15.0 版本）
- 合并提交消息模板已更新（引入于 15.0 版本）
- 压缩提交消息模板已更新（引入于 15.0 版本）
- 合并请求的默认描述模板已更新（引入于 15.0 版本）
- 由于不活动，项目被计划删除（引入于 15.0 版本）
- 项目已关联、更改或取消关联安全策略项目。（引入于 15.6 版本）
- 环境受保护或不受保护。（引入于 15.8 版本）

### 适用于 Kubernetes 的极狐GitLab 代理的事件

> 引入于 15.10 版本。

极狐GitLab 在创建或撤销集群代理令牌时生成审计事件。

<a id="instance-events"></a>

### 实例事件 **(PREMIUM SELF)**

对实例执行的以下操作，会生成实例审计事件：

- 登录事件和身份验证类型（例如标准、LDAP 或 OmniAuth）
- 登录失败
- 添加了 SSH 密钥
- 添加或删除电子邮件
- 更改密码
- 要求重置密码
- 授予 OAuth 访问权限
- 开始或停止用户模拟
- 更改了用户名
- 添加或删除了用户
- 用户请求访问实例
- 用户在管理中心被批准、拒绝或禁用。
- 用户通过 API 被禁用
- 失败的双重身份认证尝试
- 创建或撤销了用户的个人访问令牌成功或失败
- 尝试创建或撤销用户的个人访问令牌失败
- 添加或删除管理员（引入于 14.1 版本）
- SSH 密钥已删除（引入于 14.1 版本）
- GPG 密钥已添加或已删除（引入于 14.1 版本）
- 用户的双重身份验证被禁用（引入于 15.1 版本）
- 启用管理员模式。（引入于 15.7 版本）
- 所有[群组事件](#group-events)和[项目事件](#project-events)。
- 用户在管理中心或通过 API 被取消禁用。（引入于 15.11 版本）
- 用户在管理中心或通过 API 被封禁。（引入于 15.11 版本）
- 用户在管理中心或通过 API 被解禁。（引入于 15.11 版本）

您也可以通过[实例审计事件 API](../api/audit_events.md#instance-audit-events)，访问实例审计事件。

### 极狐GitLab Runner 事件

极狐GitLab 为以下极狐GitLab Runner 操作生成审计事件：

- 实例、群组或项目 runner 已注册。
- 实例、群组或项目 runner 未注册。
- Runner 被分配到项目或从项目中取消分配。
- 重置实例、群组或项目 runner 注册令牌（废弃于 15.6 版本）。

### "已删除用户" 事件

删除用户后为用户创建审计事件。与事件关联的用户名设置为“已删除用户”，因为实际用户名是不可知的。例如，已删除用户对项目的访问权限由于到期而自动删除，则会为“已删除用户”创建审计事件。<!--We are [investigating](https://gitlab.com/gitlab-org/gitlab/-/issues/343933)
whether this is avoidable.-->

<!--
## Unsupported events

Some events are not tracked in audit events. The following epics propose support for more events:

- [Project settings and activity](https://gitlab.com/groups/gitlab-org/-/epics/474).
- [Group settings and activity](https://gitlab.com/groups/gitlab-org/-/epics/475).
- [Instance-level settings and activity](https://gitlab.com/groups/gitlab-org/-/epics/476).

If you don't see the event you want in any of the epics, you can either:

- Use the **Audit Event Proposal** issue template to
  [create an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Audit%20Event%20Proposal) to
  request it.
- [Add it yourself](../development/audit_event_guide/index.md).
-->
