---
stage: Enablement
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab 维护模式 **(PREMIUM SELF)**

> 引入于 13.9 版本。

维护模式允许管理员在执行维护任务时将写入操作减少到最低限度。主要目标是阻止所有更改内部状态的外部操作，包括 PostgreSQL 数据库，尤其是文件、Git 仓库、容器镜像库等。

启用维护模式后，进行中的操作会相对较快地完成，因为没有新操作进入，并且内部状态更改很小。
在这种状态下，各种维护任务更容易，服务可以完全停止或进一步降级，时间比原本需要的时间短得多。例如，停止 cron 作业和排空队列应该很快。

维护模式允许大多数不改变内部状态的外部操作。概括地说，HTTP `POST`、`PUT`、`PATCH` 和 `DELETE` 请求被阻止<!--，并且提供了 [如何处理特殊情况](#rest-api) 的详细概述-->。

## 启用维护模式

以管理员身份启用维护模式的方法有以下三种：

- **Web UI**：
   1. 在顶部栏上，选择 **主菜单 > 管理员**。
   1. 在左侧边栏中，选择 **设置 > 通用**。
   1. 展开 **维护模式**，然后切换 **启用维护模式**。
      您也可以选择为 banner 添加消息。
   1. 选择 **保存修改**。

- **API**：

  ```shell
  curl --request PUT --header "PRIVATE-TOKEN:$ADMIN_TOKEN" "<gitlab-url>/api/v4/application/settings?maintenance_mode=true"
  ```

- [**Rails console**](../operations/rails_console.md#启动-rails-控制台会话)：

  ```ruby
  ::Gitlab::CurrentSettings.update!(maintenance_mode: true)
  ::Gitlab::CurrentSettings.update!(maintenance_mode_message: "New message")
  ```

## 禁用维护模式

有三种方法可以禁用维护模式：

- **Web UI**：
   1. 在顶部栏上，选择 **主菜单 > 管理员**。
   1. 在左侧边栏中，选择 **设置 > 通用**。
   1. 展开 **维护模式**，然后切换 **启用维护模式**。
      您也可以选择为 banner 添加消息。
   1. 选择 **保存修改**。

- **API**：

  ```shell
  curl --request PUT --header "PRIVATE-TOKEN:$ADMIN_TOKEN" "<gitlab-url>/api/v4/application/settings?maintenance_mode=false"
  ```

- [**Rails console**](../operations/rails_console.md#starting-a-rails-console-session)：

  ```ruby
  ::Gitlab::CurrentSettings.update!(maintenance_mode: false)
  ```

## 维护模式下的功能表现

启用维护模式后，页面顶部会显示 banner。
可以使用特定消息自定义 banner。

当用户尝试执行不允许的写入操作时会显示错误。

![Maintenance Mode banner and error message](img/maintenance_mode_error_message.png)

<!--
NOTE:
In some cases, the visual feedback from an action could be misleading, for example when starring a project, the **Star** button changes to show the **Unstar** action, however, this is only the frontend update, and it doesn't take into account the failed status of the POST request. These visual bugs are to be fixed [in follow-up iterations](https://gitlab.com/gitlab-org/gitlab/-/issues/295197).
-->

### 管理员功能

系统管理员可以编辑应用程序设置，允许他们在启用维护模式后禁用它。

### 身份验证

所有用户都可以登录和退出极狐GitLab 实例，但不能创建新用户。

如果在该时间安排了 [LDAP 同步](../auth/ldap/index.md)，它们会因为用户创建被禁用而失败。同样，基于 SAML 的用户创建也会失败。

### Git 操作

所有只读 Git 操作继续工作，例如 `git clone` 和 `git pull`。通过 CLI 和 Web IDE 的所有写入操作都失败，并显示错误消息：`Git push is not allowed because this GitLab instance is current in (read-only) maintenance mode.`

如果启用了 Geo，Git 推送到主服务器和次要服务器都会失败。

### 合并请求，议题，史诗

除上述之外的所有写操作均失败。例如，用户不能更新合并请求或议题。

### 接收电子邮件

通过电子邮件创建新的议题回复、议题（包括新的服务台议题）、合并请求将失败。

### 外发电子邮件

通知电子邮件继续送达，但需要数据库写入的电子邮件（如重置密码）不会送达。

### REST API

对于大多数 JSON 请求，`POST`、`PUT`、`PATCH` 和 `DELETE` 被阻止，并且 API 返回 403 响应和错误消息：`You cannot perform write operations on a read-only instance`。只允许以下请求：

| HTTP 请求 | 允许的路径 |  备注 |
|:----:|:--------------------------------------:|:----:|
| `POST` | `/admin/application_settings/general` | 允许在管理员 UI 中更新应用程序设置。 |
| `PUT`  | `/api/v4/application/settings` | 允许使用 API 更新应用程序设置。 |
| `POST` | `/users/sign_in` | 允许用户登录。 |
| `POST` | `/users/sign_out`| 允许用户退出。 |
| `POST` | `/oauth/token` | 允许用户首次登录 Geo 次要服务器。 |
| `POST` | `/admin/session`, `/admin/session/destroy` | 允许极狐GitLab 管理员的管理员模式。 |
| `POST` | Paths ending with `/compare`| Git 修订路径。 |
| `POST` | `.git/git-upload-pack` | 允许 Git 拉取/克隆。 |
| `POST` | `/api/v4/internal` | 内部 API 路径。 |
| `POST` | `/admin/sidekiq` | 允许在管理 UI 中管理后台作业。 |
| `POST` | `/admin/geo` | 允许在管理员 UI 中更新 Geo 节点。 |
| `POST` | `/api/v4/geo_replication`| 允许在次要站点上，执行某些特定于 Geo 的管理员 UI 操作。 |

### GraphQL API

`POST /api/graphql` 请求是允许的，但变动被阻止并显示错误消息 `You cannot perform write operations on a read-only instance`。

### 持续集成

- 没有新的作业或流水线启动，无论是计划或其他方式。
- 已经运行的作业在 GitLab UI 中继续在 `running` 状态，即使它们在 GitLab Runner 上完成运行。
- 处于 `running` 状态的作业超过项目的时间限制不会超时。
- 流水线无法启动、重试或取消，也无法创造新的作业。
- `/admin/runners` 中的 runners 状态不会更新。
- `gitlab-runner verify` 将返回错误 `ERROR: Verifying runner... is removed`。

禁用维护模式后，将再次执行新作业。在启用维护模式之前处于 `running`  状态的作业会恢复，并且它们的日志会再次开始更新。

NOTE:
建议您在关闭维护模式后，重新启动之前 `running` 的流水线。

### 部署

部署没有通过，因为流水线未完成。

建议在维护模式期间禁用自动部署，并在禁用后启用它们。

#### Terraform 集成

Terraform 集成依赖于运行 CI 流水线，因此它被阻止。

### Container Registry

`docker push` 失败并出现以下错误：`denied: requested access to the resource is denied`，但 `docker pull` 有效。

### Package Registry

软件包库允许您安装软件包，但不能发布包。

### 后台作业

后台作业（cron 作业、Sidekiq）继续按原计划运行，因为后台作业不会自动禁用。
在计划的 Geo 故障转移期间，建议您禁用除与 Geo 相关的所有 cron 作业。

要监控队列并禁用作业：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，选择 **监控 > 后台作业**。

### 事件管理

<!--
[Incident management](../../operations/incident_management/index.md) functions are limited. The creation of [alerts](../../operations/incident_management/alerts.md) and [incidents](../../operations/incident_management/incidents.md#incident-creation) are paused entirely. Notifications and paging on alerts and incidents are therefore disabled.
-->

事件管理功能是有限的。警报和事件的创建完全暂停。因此禁用警报和事件的通知和呼叫。

### 功能标志

- 开发功能标志无法通过 API 打开或关闭，但可以通过 Rails 控制台切换。
- [功能标志服务](../../operations/feature_flags.md)响应功能标志检查，但无法切换功能标志

### Geo secondaries

当主站点处于维护模式时，次要站点也会自动进入维护模式。

在启用维护模式之前不要禁用复制，这一点很重要。

复制和验证继续工作，但代理 Git 推送到主服务器不工作。

### 安全功能

依赖于创建议题，或创建或批准合并请求的功能不工作。

从漏洞报告页面导出漏洞列表不工作。

更改结果或漏洞对象的状态不工作，即使 UI 中未显示错误。

SAST 和 Secret Detection 无法启动，因为它们依赖于传递 CI 作业来创建产物。

## 示例用例：计划的故障转移

在计划故障转移的用例中，主数据库中的少量写入是可以接受的，因为它们被快速复制并且数量不多。

出于同样的原因，我们不会在启用维护模式时自动阻止后台作业。

生成的数据库写入是可以接受的。在这里，权衡是在更多的服务降级和完成复制之间。

但是，在计划的故障转移期间，我们要求用户手动关闭与 Geo 无关的 cron 作业。在没有新的数据库写入和非 Geo cron 作业的情况下，新的后台作业要么根本不创建，要么很少。
