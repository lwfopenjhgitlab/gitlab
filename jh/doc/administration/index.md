---
stage: Enablement
group: Distribution
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
description: 'Learn how to install, configure, update, and maintain your GitLab instance.'
---

# 管理员文档 **(FREE SELF)**

如果您使用 SaaS 版，只有极狐GitLab 团队成员有权限管理工具和设置。
如果您使用私有化部署版，您需要了解如何进行管理。

只有管理员用户有权限访问极狐GitLab 的管理工具和设置页面。

<!--
## Available distributions

GitLab has two product distributions available through [different subscriptions](https://about.gitlab.com/pricing/):

- The open source [GitLab Community Edition (CE)](https://gitlab.com/gitlab-org/gitlab-foss).
- The open core [GitLab Enterprise Edition (EE)](https://gitlab.com/gitlab-org/gitlab).

You can [install either GitLab CE or GitLab EE](https://about.gitlab.com/install/ce-or-ee/).
However, the features you have access to depend on your chosen subscription.

GitLab Community Edition installations have access only to Free features.
-->

## 安装和维护

了解如何安装、配置、更新和维护您的极狐GitLab 实例。

### 安装极狐GitLab

- [安装](../install/index.md)：安装要求、目录结构和安装方式。
- [添加许可证](../user/admin_area/license.md)：在安装过程中添加许可证，解锁付费版本中的功能。

### 配置极狐GitLab

- [调整实例的时间](timezone.md)：自定义极狐GitLab 的默认时区。
- [系统钩子](system_hooks.md)：更改用户、项目和密钥时的通知。
- [安全](../security/index.md)：了解您可以做些什么来进一步保护您的极狐GitLab 实例。
- [轮询](polling.md)：配置 UI 轮询更新的频率。
- [上传文件管理](uploads.md)：配置上传文件存储。
- [环境变量](environment_variables.md)：配置极狐GitLab 时，支持覆盖默认值的环境变量。
- [合规](compliance.md)：来自整个应用程序的一组功能，您可以配置这些功能，帮助确保您的极狐GitLab 实例和 DevOps 工作流程符合合规标准。
- [差异限制](../user/admin_area/diff_limits.md)：配置分支比较页面的差异渲染大小限制。
- [广播消息](../user/admin_area/broadcast_messages.md)：通过 UI 向极狐GitLab 用户发送消息。
- [外部分类策略授权](../user/admin_area/settings/external_authorization.md)
- [添加许可证](../user/admin_area/license.md)：添加许可证，解锁极狐GitLab 付费版本中的功能。
- [管理中心](../user/admin_area/index.md)：用于私有化部署实例范围的配置和维护。
- [启用和禁用功能标志](feature_flags.md)：如何启用和禁用部署在功能标志后面的极狐GitLab 功能。
- [Omnibus 支持日志转发](https://docs.gitlab.cn/omnibus/settings/logs.html#udp-日志转发)。

#### 自定义极狐GitLab 外观

- [Header logo](../user/admin_area/appearance.md#navigation-bar)：更改所有页面和电子邮件 header 上的 logo。
- [Favicon](../user/admin_area/appearance.md#favicon)：将默认的网站 favicon 更改为您自己的 logo。
- [品牌化登录页面](../user/admin_area/appearance.md#sign-in--sign-up-pages)：使用您自己的 logo、标题和描述自定义登录页面。
- [“新建项目”页面](../user/admin_area/appearance.md#new-project-pages)：自定义要在用户创建新项目时，打开的页面上显示的文本。
- [自定义附加邮件文本](../user/admin_area/settings/email.md#custom-additional-text)：向从极狐GitLab 发送的电子邮件添加其它自定义文本。

### 维护极狐GitLab

- [Rake 任务](../raketasks/index.md)：执行维护、备份、自动 webhook 设置等各种任务。
- [运维](operations/index.md)：保持极狐GitLab 正常运行。
- [重启极狐GitLab](restart_gitlab.md)：了解如何重新启动极狐GitLab 及其组件。

<!--
#### Updating GitLab

- [GitLab versions and maintenance policy](../policy/maintenance.md): Understand GitLab versions and releases (Major, Minor, Patch, Security), as well as update recommendations.
- [GitLab in maintenance mode](maintenance_mode/index.md): Put GitLab in maintenance mode.
- [Update GitLab](../update/index.md): Update guides to upgrade your installation to a new version.
- [Upgrading without downtime](../update/index.md#upgrading-without-downtime): Upgrade to a newer major, minor, or patch version of GitLab without taking your GitLab instance offline.

### Upgrading or downgrading GitLab

- [Upgrade from GitLab CE to GitLab EE](../update/index.md#upgrading-between-editions): Learn how to upgrade GitLab Community Edition to GitLab Enterprise Editions.
- [Downgrade from GitLab EE to GitLab CE](../downgrade_ee_to_ce/index.md): Learn how to downgrade GitLab Enterprise Editions to Community Edition.
-->

### 极狐GitLab 平台集成

- [Mattermost](../integration/mattermost/index.md)：集成 [Mattermost](https://mattermost.com)，这是一个用于 Web 消息传递的开源私有云工作平台。
- [PlantUML](integration/plantuml.md)：在代码片段、wiki 和仓库中创建的 AsciiDoc 和 Markdown 文档中，创建图表。

## 用户设置和权限

- [创建用户](../user/profile/account/create_accounts.md)：手动或通过身份验证集成创建用户。
- [Libravatar](libravatar.md)：使用 Libravatar 代替 Gravatar 用于用户头像。
- [注册限制](../user/admin_area/settings/sign_up_restrictions.md)：阻止特定域名的电子邮件地址，或仅将特定域名列入白名单。
- [给用户发邮件](../user/admin_area/email_from_gitlab.md)：从极狐GitLab 向极狐GitLab 用户发送电子邮件。
- [用户群](../user/admin_area/user_cohorts.md)：显示每月的新用户人群及其随时间的活动。
- [审计事件](audit_events.md)：查看极狐GitLab 服务器上所做的更改：
    - 群组级和项目级。
    - 实例级。
- [审计员用户](auditor_users.md)：对极狐GitLab 实例上的所有项目、群组和其它资源具有只读访问权限的用户。
- [滥用报告](../user/admin_area/review_abuse_reports.md)：查看和解决用户的滥用报告。
- [凭据库](../user/admin_area/credentials_inventory.md)：借助凭据库，极狐GitLab 管理员可以跟踪用户在其私有化部署实例中使用的凭据。

## 项目设置

- [默认标记](../user/admin_area/labels.md)：创建自动添加到每个新项目的标记。
- [自定义项目模板](../user/admin_area/custom_project_templates.md)：配置一组项目，在创建新项目时用作自定义模板。

## 软件包库管理

- [容器镜像库](packages/container_registry.md)：配置极狐GitLab 作为容器镜像库。
- [软件包库](packages/index.md)：配置极狐GitLab 作为软件包库。

### 仓库设置

- [仓库存储路径](repository_storage_paths.md)：管理用于存储仓库的路径。
- [仓库存储类型](repository_storage_types.md)：有关不同仓库存储类型的信息。
- [仓库存储 Rake 任务](raketasks/storage.md)：一组 Rake 任务，用于列出现有项目和与其关联的附件，并将其从传统存储迁移到哈希存储。
- [限制仓库大小](../user/admin_area/settings/account_and_limit_settings.md)：为仓库的大小设置硬限制。
- [静态对象外部存储](static_objects_external_storage.md)：为仓库中的静态对象设置外部存储。

## CI/CD 设置

- [极狐GitLab CI/CD 管理设置](../user/admin_area/settings/continuous_integration.md)：在站点范围内启用或禁用 Auto DevOps 并定义产物的最大大小和过期时间。
- [外部流水线验证](external_pipeline_validation.md)：启用、禁用和配置外部流水线验证。
- [作业产物](job_artifacts.md)：启用、禁用和配置作业产物（作业成功完成时输出的一组文件和目录）。
- [作业日志](job_logs.md)：有关作业日志的信息。
- [注册 runners](../ci/runners/runners_scope.md)：了解如何注册和配置 runner。
- [共享 runners 的 CI/CD 分钟数配额](../ci/pipelines/cicd_minutes.md)：限制共享 runners 的 CI/CD 分钟数的使用量。
- [启用或禁用 Auto DevOps](../topics/autodevops/index.md#enable-or-disable-auto-devops)：为您的实例启用或禁用 Auto DevOps。

<!--
## Snippet settings

- [Setting snippet content size limit](snippets/index.md): Set a maximum content size limit for snippets.

## Wiki settings

- [Setting wiki page content size limit](wikis/index.md): Set a maximum content size limit for wiki pages.
-->

## Git 配置选项

- [例行维护](housekeeping.md)：让您的 Git 仓库保持整洁和快速。

## 监控极狐GitLab

- [监控极狐GitLab](monitoring/index.md):
    - [监控正常运行时间](../user/admin_area/monitoring/health_check.md)：使用健康检查端点检查服务器状态。
    - [IP 白名单](monitoring/ip_whitelist.md)：监视在探测时提供健康检查信息的端点。
    - [监控 GitHub 导入](monitoring/github_imports.md)：极狐GitLab GitHub 导入器显示 Prometheus 指标，监控导入器的运行状况和进度。

### 性能监控

- [极狐GitLab 性能监控](monitoring/performance/index.md)：
    - [启用性能监控](monitoring/performance/gitlab_configuration.md)：启用极狐GitLab 性能监控。
    - [使用 Prometheus 进行极狐GitLab 性能监控](monitoring/prometheus/index.md)：配置极狐GitLab 和 Prometheus，测量性能指标。
    - [使用 Grafana 进行极狐GitLab 性能监控](monitoring/performance/grafana_configuration.md)：配置极狐GitLab，通过图表和仪表板可视化时间序列指标。
    - [性能栏](monitoring/performance/performance_bar.md)：获取当前页面的性能信息。

<!--
## Troubleshooting

- [Debugging tips](troubleshooting/debug.md): Tips to debug problems when things go wrong.
- [Log system](logs.md): Where to look for logs.
- [Sidekiq Troubleshooting](troubleshooting/sidekiq.md): Debug when Sidekiq appears hung and is not processing jobs.
- [Troubleshooting Elasticsearch](troubleshooting/elasticsearch.md)
- [Navigating GitLab via Rails console](troubleshooting/navigating_gitlab_via_rails_console.md)
- [GitLab application limits](instance_limits.md)
- [Responding to security incidents](../security/responding_to_security_incidents.md)

### Support Team Docs

The GitLab Support Team has collected a lot of information about troubleshooting GitLab.
The following documents are used by the Support Team or by customers
with direct guidance from a Support Team member. GitLab administrators may find the
information useful for troubleshooting. However, if you are experiencing trouble with your
GitLab instance, you should check your [support options](https://about.gitlab.com/support/)
before referring to these documents.

WARNING:
The commands in the following documentation might result in data loss or
other damage to a GitLab instance. They should be used only by experienced administrators
who are aware of the risks.

- [Diagnostics tools](troubleshooting/diagnostics_tools.md)
- [Linux commands](troubleshooting/linux_cheat_sheet.md)
- [Troubleshooting Kubernetes](troubleshooting/kubernetes_cheat_sheet.md)
- [Troubleshooting PostgreSQL](troubleshooting/postgresql.md)
- [Guide to test environments](troubleshooting/test_environments.md) (for Support Engineers)
- [GitLab Rails console commands](troubleshooting/gitlab_rails_cheat_sheet.md) (for Support Engineers)
- [Troubleshooting SSL](troubleshooting/ssl.md)
- Related links:
  - [GitLab Developer Documentation](../development/README.md)
  - [Repairing and recovering broken Git repositories](https://git.seveas.net/repairing-and-recovering-broken-git-repositories.html)
  - [Testing with OpenSSL](https://www.feistyduck.com/library/openssl-cookbook/online/ch-testing-with-openssl.html)
  - [`strace` zine](https://wizardzines.com/zines/strace/)
- GitLab.com-specific resources:
  - [Group SAML/SCIM setup](troubleshooting/group_saml_scim.md)
-->
