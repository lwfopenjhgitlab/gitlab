---
stage: Create
group: GitLab
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 如何设置 实名制手机号验证 **(SaaS only)**

如果以 SaaS 方式启动极狐GitLab，此功能自动开启。注册页面、用户协议页面以及个人资料修改页面均会出现手机号和验证码输入框。旧用户如果未实名，则会强制进入实名制页面。

## 必要条件

1. 将 `jh/lib/jh/gitlab/saas.rb` 中的 `com_url` 修改成需要部署的域名，或者将 `lib/gitlab.rb` 中的 `self.com?` 方法强制返回 `true`，确保以 SaaS 的模式启动。

例如(修改`jh/lib/jh/gitlab/saas.rb`)：
```ruby
override :com_url
def com_url
    'https://gitlab.cn' # 改成你的部署域名
end
```

或者(修改`lib/gitlab.rb`)

```ruby
def self.com?
  # Check `gl_subdomain?` as well to keep parity with gitlab.com
  Gitlab.config.gitlab.url == Gitlab::Saas.com_url || gl_subdomain?
  return true
end
```

2. 需要申请腾讯云短信（大陆短信）功能，并且获取相应的 ID 和 KEY 以及 APP_ID，并配置短信内容模板获 TEMPLATE_ID 以及公司名称 SIGN_NAME。

## 配置启动

启动极狐GitLab 时，需要在启动命令的环境变量中添加 TC_ID 和 TC_KEY 等，例如：`env TC_ID=AKIDELDpCI...MJAn TC_KEY=QcfVAc...V1GEv TC_APP_ID=123xx TC_TEMPLATE_ID=456xx TC_SIGN_NAME=JiHuxxx `。
