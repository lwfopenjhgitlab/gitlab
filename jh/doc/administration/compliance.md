---
stage: Manage
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 合规功能 **(FREE)**

极狐GitLab 合规性功能可确保您的极狐GitLab 实例符合常见的合规性标准，并且可用于多种付费版本。<!--有关合规管理的更多信息，请参阅合规管理 [解决方案页面](https://about.gitlab.com/solutions/compliance/)。-->

极狐GitLab 中的安全功能也可以帮助您满足相关的合规性标准。

## 策略管理

由于组织标准或监管机构的要求，组织具有独特的政策要求。以下功能可帮助您定义规则和策略，遵守工作流程要求、职责分离和安全供应链最佳实践：

- [**凭证库**](credentials_inventory.md)（实例）：使用凭证库，极狐GitLab 管理员可以跟踪实例中所有用户使用的凭证。
- **精细的用户角色和灵活的权限**（实例、群组和项目）：使用五种不同的用户角色和外部用户来设置管理访问和权限。根据人员的角色设置权限，而不是对仓库的读取或写入权限。 不要与只需要访问议题跟踪器的人共享源代码。
- [**合并请求批准**](../user/project/merge_requests/approvals/index.md)（实例、群组和项目）：配置合并请求所需的批准。
- [**推送规则**](../user/project/repository/push_rules.md)（实例、群组和项目）：控制推送到您的仓库。
- 使用[**受保护分支**](../user/project/protected_branches.md#要求受保护分支的代码所有者批准)和[**自定义 CI/CD 配置路径**](../ci/pipelines/settings.md#指定自定义-cicd-配置文件)进行职责分离（项目）：用户可以利用极狐GitLab 跨项目 YAML 配置，来定义代码部署者和代码开发者。 

<!--
See how to use this setup
  to define these roles in:
  - The [Separation of Duties deploy project](https://gitlab.com/guided-explorations/separation-of-duties-deploy/blob/master/README.md).
  - The [Separation of Duties project](https://gitlab.com/guided-explorations/separation-of-duties/blob/master/README.md).
-->

## 合规的工作流程自动化

合规团队必须确信他们的控制和要求设置正确，并*保持*设置正确。
一种方法是定期手动检查设置，但这容易出错且耗时。更好的方法是使用单一真实来源设置和自动化，来确保合规团队配置的任何内容都保持配置并正常工作。 以下功能可以帮助您自动化合规性：

- <!--[**合规框架**](../user/group/compliance_frameworks.md)-->**合规框架**（群组）：在群组级别创建自定义合规框架，描述任何子项目需要遵循的合规要求类型。
- <!--[**合规流水线**](../user/group/compliance_frameworks.md#configure-a-compliance-pipeline)-->**合规流水线**（群组）：为具有特定合规性框架的任何项目，定义要运行的流水线配置。

## 审计管理

任何合规计划的一个重要部分是能够回过头来了解发生了什么、何时发生以及谁负责。这在审计场景，以及在问题发生时了解问题的根本原因时很有用。
同时拥有低级别的原始审计数据列表以及高级的审计数据摘要列表是很有用的。在这两者之间，合规团队可以快速确定是否存在问题，然后深入研究这些问题的细节。
以下这些功能可以帮助提供对极狐GitLab 的可见性，并审计正在发生的事情：

- [**审计事件**](audit_events.md)（实例、群组和项目）：为了保持代码的完整性，审计事件使管理员能够在高级审计事件系统中，查看在 GitLab 服务器中所做的任何修改，以便您可以控制、分析和跟踪每个更改。
- [**审计报告**](audit_reports.md)（实例、群组和项目）：根据已发生的审计事件创建和访问报告。使用预先构建的 GitLab 报告或 API 来构建您自己的报告。
- [**审计员用户**](auditor_users.md)（实例）：审计员用户是被授予对极狐GitLab 实例上所有项目、群组和其它资源的只读访问权限的用户。
- [**合规报告**](../user/compliance/compliance_report/index.md)（群组）：快速了解您组织的合规状况。

<!--
## Other compliance features

These features can also help with compliance requirements:

- [**Email all users of a project, group, or entire server**](../tools/email.md)
  (for instances): An administrator can email groups of users based on project
  or group membership, or email everyone using the GitLab instance. These emails
  are great for scheduled maintenance or upgrades.
- [**Enforce ToS acceptance**](../user/admin_area/settings/terms.md) (for
  instances): Enforce your users accepting new terms of service by blocking GitLab
  traffic.
- [**External Status Checks**](../user/project/merge_requests/status_checks.md)
  (for projects): Interface with third-party systems you already use during
  development to ensure you remain compliant.
- [**Generate reports on permission levels of users**](../user/admin_area/index.md#user-permission-export)
  (for instances): Administrators can generate a report listing all users' access
  permissions for groups and projects in the instance.
- [**License compliance**](../user/compliance/license_compliance/index.md) (for
  projects): Search dependencies for their licenses. This lets you determine if
  the licenses of your project's dependencies are compatible with your project's
  license.
- [**Lock project membership to group**](../user/group/index.md#prevent-members-from-being-added-to-projects-in-a-group)
  (for groups): Group owners can prevent new members from being added to projects
  within a group.
- [**LDAP group sync**](auth/ldap/ldap_synchronization.md#group-sync) (for
  instances): Gives administrators the ability to automatically sync groups and
  manage SSH keys, permissions, and authentication, so you can focus on building
  your product, not configuring your tools.
- [**LDAP group sync filters**](auth/ldap/ldap_synchronization.md#group-sync)
  (for instances): Gives more flexibility to synchronize with LDAP based on
  filters, meaning you can leverage LDAP attributes to map GitLab permissions.
- [**Omnibus GitLab package supports log forwarding**](https://docs.gitlab.com/omnibus/settings/logs.html#udp-log-forwarding)
  (for instances): Forward your logs to a central system.
- [**Restrict SSH Keys**](../security/ssh_keys_restrictions.md) (for instances):
  Control the technology and key length of SSH keys used to access GitLab.
-->
