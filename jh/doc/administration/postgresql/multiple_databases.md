---
stage: Data Stores
group: Pods
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 多数据库 **(FREE SELF)**

> 引入于 15.7 版本。

WARNING:
此功能尚未准备好用于生产。

默认情况下，极狐GitLab 使用单个应用程序数据库，称为 `main` 数据库。

要扩展极狐GitLab，您可以将极狐GitLab 配置为使用多个应用程序数据库。

由于已知问题，使用多数据库配置极狐GitLab 功能处于 **Alpha** 阶段。

## 已知问题

- 尚不支持将数据从 `main` 数据库迁移到 `ci` 数据库。
- 一旦数据迁移到 `ci` 数据库，您就无法将其迁移回来。

## 设置多数据库

使用以下方法，为新的极狐GitLab 安装实例设置多个数据库。

目前不支持为现有的极狐GitLab 安装实例进行设置。

在您设置多个数据库后，极狐GitLab 为 [CI/CD 功能](../../ci/index.md)使用第二个应用程序数据库，称为 `ci` 数据库。 例如，极狐GitLab 读取和写入 `ci` 数据库中的 `ci_pipelines` 表。

WARNING:
在设置多个数据库之前，您必须停止极狐GitLab，防止脑裂情况，比如 `main` 数据被写入 `ci` 数据库，反之亦然。

### 源安装实例

1. [备份极狐GitLab](../../raketasks/backup_restore.md)，以防出现不可预见的问题。

1. 停止极狐GitLab：

   ```shell
   sudo service gitlab stop
   ```

1. 打开 `config/database.yml`，并在 `production:` 下添加一个 `ci:` 部分。有关新部分的可能值，请参阅 `config/database.yml.decomposed-postgresql`。修改后，`config/database.yml` 应如下所示：

   ```yaml
   production:
     main:
       # ...
     ci:
       adapter: postgresql
       encoding: unicode
       database: gitlabhq_production_ci
       # ...
   ```

1. 保存 `config/database.yml` 文件。

1. 更新服务文件以将 `GITLAB_ALLOW_SEPARATE_CI_DATABASE` 环境变量设置为 `true`。

1. 创建 `gitlabhq_production_ci` 数据库：

   ```shell
   sudo -u postgres psql -d template1 -c "CREATE DATABASE gitlabhq_production OWNER git;"
   sudo -u git -H bundle exec rake db:schema:load:ci
   ```

1. 对 `main` 数据库中的 `ci` 表进行锁定写入，反之亦然：

   ```shell
   sudo -u git -H bundle exec rake gitlab:db:lock_writes
   ```

1. 重启极狐GitLab：

   ```shell
   sudo service gitlab restart
   ```

### Omnibus GitLab 安装实例

1. [备份极狐GitLab](../../raketasks/backup_restore.md)，以防出现不可预见的问题。

1. 停止极狐GitLab：

   ```shell
   sudo gitlab-ctl stop
   ```

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['env'] = { 'GITLAB_ALLOW_SEPARATE_CI_DATABASE' => 'true' }
   gitlab_rails['databases']['ci']['enable'] = true
   gitlab_rails['databases']['ci']['db_database'] = 'gitlabhq_production_ci'
   ```

1. 保存 `/etc/gitlab/gitlab.rb` 文件。

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

1. 可选。重新配置极狐GitLab 应该会创建 `gitlabhq_production_ci`。如果没有，请手动创建 `gitlabhq_production_ci`：

   ```shell
   sudo gitlab-ctl start postgresql
   sudo -u gitlab-psql /opt/gitlab/embedded/bin/psql -h /var/opt/gitlab/postgresql -d template1 -c "CREATE DATABASE gitlabhq_production_ci OWNER gitlab;"
   sudo gitlab-rake db:schema:load:ci
   ```

1. 对 `main` 数据库中的 `ci` 表进行锁定写入，反之亦然：

   ```shell
   sudo gitlab-ctl start postgresql
   sudo gitlab-rake gitlab:db:lock_writes
   ```

1. 重启极狐GitLab：

   ```shell
   sudo gitlab-ctl restart
   ```

<!--
## Further information

For more information on multiple databases, see [issue 6168](https://gitlab.com/groups/gitlab-org/-/epics/6168).

For more information on how multiple databases work in GitLab, see the [development guide for multiple databases](../../development/database/multiple_databases.md).

Since 2022-07-02, GitLab.com has been running with two separate databases. For more information, see this [blog post](https://about.gitlab.com/blog/2022/06/02/splitting-database-into-main-and-ci/).
-->