---
stage: Enablement
group: Database
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 配置 PostgreSQL 以进行扩展 **(FREE SELF)**

在本节中，将指导您<!--在我们的 [参考架构](../reference_architectures/index.md) 之一中-->配置要与极狐GitLab 一起使用的 PostgreSQL 数据库。

## 配置选项

选择以下 PostgreSQL 配置选项之一：

### 使用 Omnibus GitLab 的独立 PostgreSQL **(FREE SELF)**

此设置适用于当您使用 [Omnibus GitLab 安装包](https://about.gitlab.cn/install/)，并使用仅启用其服务的捆绑 PostgreSQL。

> [阅读如何使用 Omnibus GitLab 设置独立的 PostgreSQL 实例](standalone.md)

### 提供您自己的 PostgreSQL 实例 

<!--
This setup is for when you have installed GitLab using the
[Omnibus GitLab packages](https://about.gitlab.com/install/) (CE or EE),
or installed it [from source](../../install/installation.md), but you want to use
your own external PostgreSQL server.
-->

此设置适用于当您使用 [Omnibus GitLab 软件包](https://about.gitlab.cn/install/)安装极狐GitLab，但您想使用您自己的外部 PostgreSQL 服务器。

> [阅读如何设置外部 PostgreSQL 实例](external.md)

### 使用 Omnibus GitLab 进行 PostgreSQL 复制和故障转移 **(PREMIUM SELF)**

<!--
此设置适用于使用 Omnibus GitLab **Enterprise Edition** (EE) 包安装 GitLab 的情况。
-->

所需的所有工具，如 PostgreSQL、PgBouncer 和 Patroni 都捆绑在包中，因此您可以使用它来设置整个 PostgreSQL 基础设施（主、副本）。

[> 阅读如何使用 Omnibus GitLab 设置 PostgreSQL 复制和故障转移](replication_and_failover.md)
