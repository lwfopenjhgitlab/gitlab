---
stage: Enablement
group: Database
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 使用捆绑的 PgBouncer 服务 **(FREE SELF)**

NOTE:
PgBouncer 捆绑在 `gitlab-jh` 包中，免费试用。要获取技术支持，您需要[专业版订阅](https://gitlab.cn/pricing/)。

[PgBouncer](https://www.pgbouncer.org/) 用于在故障转移场景下无缝迁移服务器之间的数据库连接。此外，它还可以在非容错设置中用于池连接，加快响应时间，同时减少资源使用。

专业版包含一个捆绑版本的 PgBouncer，可以通过 `/etc/gitlab/gitlab.rb` 进行管理。

## PgBouncer 作为容错 GitLab 安装的一部分

此内容已移至[新位置](replication_and_failover.md#配置-pgbouncer-节点)。

## PgBouncer 作为非容错 GitLab 安装的一部分

1. 使用命令 `gitlab-ctl pg-password-md5 pgbouncer` 生成 PGBOUNCER_USER_PASSWORD_HASH

1. 使用命令 `gitlab-ctl pg-password-md5 gitlab` 生成 SQL_USER_PASSWORD_HASH。 稍后输入明文 SQL_USER_PASSWORD。

1. 在您的数据库节点上，确保在您的 `/etc/gitlab/gitlab.rb` 中设置以下内容

   ```ruby
   postgresql['pgbouncer_user_password'] = 'PGBOUNCER_USER_PASSWORD_HASH'
   postgresql['sql_user_password'] = 'SQL_USER_PASSWORD_HASH'
   postgresql['listen_address'] = 'XX.XX.XX.Y' # Where XX.XX.XX.Y is the ip address on the node postgresql should listen on
   postgresql['md5_auth_cidr_addresses'] = %w(AA.AA.AA.B/32) # Where AA.AA.AA.B is the IP address of the pgbouncer node
   ```

1. 运行 `gitlab-ctl reconfigure`

   NOTE:
   如果数据库已经在运行，则需要在重新配置后通过运行 `gitlab-ctl restart postgresql` 重新启动。

1. 在您运行 PgBouncer 的节点上，确保在 `/etc/gitlab/gitlab.rb` 中设置以下内容

   ```ruby
   pgbouncer['enable'] = true
   pgbouncer['databases'] = {
     gitlabhq_production: {
       host: 'DATABASE_HOST',
       user: 'pgbouncer',
       password: 'PGBOUNCER_USER_PASSWORD_HASH'
     }
   }
   ```

   您可以为每个数据库传递额外的配置参数，例如：

   ```ruby
   pgbouncer['databases'] = {
     gitlabhq_production: {
        ...
        pool_mode: 'transaction'
     }
   }
   ```

   请谨慎使用这些参数。有关完整的参数列表，请参阅 [PgBouncer 文档](https://www.pgbouncer.org/config.html#section-databases)。

1. 运行 `gitlab-ctl reconfigure`

1. 在运行 Puma 的节点上，确保在 `/etc/gitlab/gitlab.rb` 中设置了以下内容

   ```ruby
   gitlab_rails['db_host'] = 'PGBOUNCER_HOST'
   gitlab_rails['db_port'] = '6432'
   gitlab_rails['db_password'] = 'SQL_USER_PASSWORD'
   ```

1. 运行 `gitlab-ctl reconfigure`

1. 此时，您的实例应该通过 PgBouncer 连接到数据库。如果您遇到问题，请参阅[故障排查](#故障排查)部分

## 备份

不要通过 PgBouncer 连接备份或恢复极狐GitLab：它会导致中断。

<!--
[阅读有关此内容以及如何重新配置备份的更多信息](../../raketasks/backup_restore.md#backup-and-restore-for-installations-using-pgbouncer)。
-->

## 启用监控

<!--
> [Introduced](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/3786) in GitLab 12.0.
-->

如果您启用监控，则必须在**所有** PgBouncer 服务器上启用它。

1. 创建/编辑 `/etc/gitlab/gitlab.rb` 并添加以下配置：

   ```ruby
   # Enable service discovery for Prometheus
   consul['enable'] = true
   consul['monitoring_service_discovery'] =  true

   # Replace placeholders
   # Y.Y.Y.Y consul1.gitlab.example.com Z.Z.Z.Z
   # with the addresses of the Consul server nodes
   consul['configuration'] = {
      retry_join: %w(Y.Y.Y.Y consul1.gitlab.example.com Z.Z.Z.Z),
   }

   # Set the network addresses that the exporters will listen on
   node_exporter['listen_address'] = '0.0.0.0:9100'
   pgbouncer_exporter['listen_address'] = '0.0.0.0:9188'
   ```

1. 运行 `sudo gitlab-ctl reconfigure` 以完成配置。

## 管理控制台

作为 Omnibus GitLab 的一部分，提供了一个命令来自动连接到 PgBouncer 管理控制台。有关如何与控制台交互的详细说明，请参阅 [PgBouncer 文档](https://www.pgbouncer.org/usage.html#admin-console)。

要启动会话，请运行以下命令并为 `pgbouncer` 用户提供密码：

```shell
sudo gitlab-ctl pgb-console
```

要获取有关实例的一些基本信息：

```shell
pgbouncer=# show databases; show clients; show servers;
        name         |   host    | port |      database       | force_user | pool_size | reserve_pool | pool_mode | max_connections | current_connections
---------------------+-----------+------+---------------------+------------+-----------+--------------+-----------+-----------------+---------------------
 gitlabhq_production | 127.0.0.1 | 5432 | gitlabhq_production |            |       100 |            5 |           |               0 |                   1
 pgbouncer           |           | 6432 | pgbouncer           | pgbouncer  |         2 |            0 | statement |               0 |                   0
(2 rows)

 type |   user    |      database       | state  |   addr    | port  | local_addr | local_port |    connect_time     |    request_time     |    ptr    | link
| remote_pid | tls
------+-----------+---------------------+--------+-----------+-------+------------+------------+---------------------+---------------------+-----------+------
+------------+-----
 C    | gitlab    | gitlabhq_production | active | 127.0.0.1 | 44590 | 127.0.0.1  |       6432 | 2018-04-24 22:13:10 | 2018-04-24 22:17:10 | 0x12444c0 |
|          0 |
 C    | gitlab    | gitlabhq_production | active | 127.0.0.1 | 44592 | 127.0.0.1  |       6432 | 2018-04-24 22:13:10 | 2018-04-24 22:17:10 | 0x12447c0 |
|          0 |
 C    | gitlab    | gitlabhq_production | active | 127.0.0.1 | 44594 | 127.0.0.1  |       6432 | 2018-04-24 22:13:10 | 2018-04-24 22:17:10 | 0x1244940 |
|          0 |
 C    | gitlab    | gitlabhq_production | active | 127.0.0.1 | 44706 | 127.0.0.1  |       6432 | 2018-04-24 22:14:22 | 2018-04-24 22:16:31 | 0x1244ac0 |
|          0 |
 C    | gitlab    | gitlabhq_production | active | 127.0.0.1 | 44708 | 127.0.0.1  |       6432 | 2018-04-24 22:14:22 | 2018-04-24 22:15:15 | 0x1244c40 |
|          0 |
 C    | gitlab    | gitlabhq_production | active | 127.0.0.1 | 44794 | 127.0.0.1  |       6432 | 2018-04-24 22:15:15 | 2018-04-24 22:15:15 | 0x1244dc0 |
|          0 |
 C    | gitlab    | gitlabhq_production | active | 127.0.0.1 | 44798 | 127.0.0.1  |       6432 | 2018-04-24 22:15:15 | 2018-04-24 22:16:31 | 0x1244f40 |
|          0 |
 C    | pgbouncer | pgbouncer           | active | 127.0.0.1 | 44660 | 127.0.0.1  |       6432 | 2018-04-24 22:13:51 | 2018-04-24 22:17:12 | 0x1244640 |
|          0 |
(8 rows)

 type |  user  |      database       | state |   addr    | port | local_addr | local_port |    connect_time     |    request_time     |    ptr    | link | rem
ote_pid | tls
------+--------+---------------------+-------+-----------+------+------------+------------+---------------------+---------------------+-----------+------+----
--------+-----
 S    | gitlab | gitlabhq_production | idle  | 127.0.0.1 | 5432 | 127.0.0.1  |      35646 | 2018-04-24 22:15:15 | 2018-04-24 22:17:10 | 0x124dca0 |      |
  19980 |
(1 row)
```

<a id="procedure-for-bypassing-pgbouncer"></a>

## 绕过 PgBouncer 的过程

### Omnibus 安装实例

某些数据库更改必须直接完成，而不是通过 PgBouncer。

<!--
阅读有关受影响任务的更多信息：[数据库恢复](../../raketasks/backup_restore.md#backup-and-restore-for-installations-using-pgbouncer) 和 [GitLab 升级](https://docs.gitlab.cn/omnibus/update/index.html#use-postgresql-ha)。
-->

1. 要查找主节点，请在数据库节点上运行以下命令：

   ```shell
   sudo gitlab-ctl patroni members
   ```

1. 在您正在执行任务的应用程序节点上编辑 `/etc/gitlab/gitlab.rb`，并使用数据库主节点的主机和端口更新 `gitlab_rails['db_host']` 和 `gitlab_rails['db_port']`。

1. 运行重新配置：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

完成任务或程序后，切换回使用 PgBouncer：

1. 改回 `/etc/gitlab/gitlab.rb` 以指向 PgBouncer。
1. 运行重新配置：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

### Helm chart 安装实例

出于与基于 Omnibus 的部署相同的原因，高可用性部署也需要绕过 PgBouncer。
对于此类安装实例：

- 数据库备份和恢复任务由 toolbox 容器执行。
- 迁移任务由 migrations 容器执行。

您应该覆盖每个子 chart 上的 PostgreSQL 端口，以便这些任务可以执行并直接连接到 PostgreSQL：

- [Toolbox](https://jihulab.com/gitlab-cn/charts/gitlab/-/blob/master/charts/gitlab/charts/toolbox/values.yaml#L40)
- [Migrations](https://jihulab.com/gitlab-cn/charts/gitlab/-/blob/master/charts/gitlab/charts/migrations/values.yaml#L46)

## 微调

PgBouncer 的默认设置适合大多数安装。在特定情况下，您可能希望更改特定于性能和特定于资源的变量，以增加可能的吞吐量，或限制可能导致数据库内存耗尽的资源利用率。

您可以在[官方 PgBouncer 文档](https://www.pgbouncer.org/config.html) 上找到参数和相应文档。
下面列出的是 Omnibus GitLab 安装中最相关的参数和它们的默认值：

- `pgbouncer['max_client_conn']`（默认值：`2048`，取决于服务器文件描述符限制）
  这是 PgBouncer 中的“前端”池：从 Rails 到 PgBouncer 的连接。
- `pgbouncer['default_pool_size']`（默认值：`100`）
  这是 PgBouncer 中的“后端”池：从 PgBouncer 到数据库的连接。

`default_pool_size` 的理想数量必须足以处理需要访问数据库的所有已配置服务。下面列出的每个服务都使用以下公式来定义数据库池大小：

- `puma`：`max_threads + headroom`（默认`14`）
    - `max_threads` 是通过以下方式配置的：`gitlab['puma']['max_threads']`（默认值：`4`）
    - `headroom` 可以通过 `DB_POOL_HEADROOM` 环境变量配置（默认为 `10`）
- `sidekiq` : `max_concurrency + 1 + headroom`（默认值：`31`）
    - `max_concurrency` 是通过以下方式配置的：`sidekiq['max_concurrency']`（默认值：`20`）
    - `headroom` 可以通过 `DB_POOL_HEADROOM` 环境变量配置（默认为 `10`）
- `geo-logcursor`：`1+headroom`（默认值：`11`）
    - `headroom` 可以通过 `DB_POOL_HEADROOM` 环境变量配置（默认为 `10`）

要计算 `default_pool_size`，请将 `puma`、`sidekiq` 和`geo-logcursor` 的实例数乘以上面列出的每个可以消耗的连接数。总数将是建议的 `default_pool_size`。

如果您使用多个带有内部负载均衡器的 PgBouncer，您可以将 `default_pool_size` 除以实例数量，以保证它们之间的负载均匀分布。

`pgbouncer['max_client_conn']` 是 PgBouncer 可以接受的连接的硬限制。您不太可能需要更改此设置。如果您达到该限制，您可能需要考虑使用内部负载均衡器添加额外的 PgBouncer。

在为指向 Geo 跟踪数据库的 PgBouncer 设置限制时，您可能会忽略等式中的 `puma`，因为它只是偶尔访问该数据库。

## 故障排查

如果您在通过 PgBouncer 连接时遇到任何问题，首先要检查的始终是日志：

```shell
sudo gitlab-ctl tail pgbouncer
```

此外，您可以在[管理控制台](#管理控制台)中检查 `show databases` 的输出。在输出中，您可能会看到 `gitlabhq_production` 数据库的 `host` 字段中的值。此外，`current_connections` 应该大于 1。

<!--
### Message: `LOG:  invalid CIDR mask in address`

请参阅[在 Geo 文档中](../geo/replication/troubleshooting.md#message-log--invalid-cidr-mask-in-address)建议的修复方法。

### Message: `LOG:  invalid IP mask "md5": Name or service not known`

请参阅[在 Geo 文档中](../geo/replication/troubleshooting.md#message-log--invalid-ip-mask-md5-name-or-service-not-known)建议的修复方法。 
-->


