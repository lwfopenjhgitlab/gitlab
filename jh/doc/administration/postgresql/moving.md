---
stage: Data Stores
group: Database
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 将极狐GitLab 数据库移动到不同的 PostgreSQL 实例 **(FREE SELF)**

有时有必要将您的数据库从一个 PostgreSQL 实例移动到另一个实例。例如，您使用 AWS Aurora 并准备启用数据库负载均衡，则需要将数据库移动到 RDS for PostgreSQL。

要将数据库从一个实例移动到另一个实例：

1. 收集源和目标 PostgreSQL 端点信息：

   ```shell
   SRC_PGHOST=<source postgresql host>
   SRC_PGUSER=<source postgresql user>

   DST_PGHOST=<destination postgresql host>
   DST_PGUSER=<destination postgresql user>
   ```

1. 停止极狐GitLab：

   ```shell
   sudo gitlab-ctl stop
   ```

1. 从源转储数据库：

   ```shell
   /opt/gitlab/embedded/bin/pg_dump -h $SRC_PGHOST -U $SRC_PGUSER -c -C -f gitlabhq_production.sql gitlabhq_production
   /opt/gitlab/embedded/bin/pg_dump -h $SRC_PGHOST -U $SRC_PGUSER -c -C -f praefect_production.sql praefect_production
   ```

1. 将数据库还原到目标（这将覆盖任何现有的同名数据库）：

   ```shell
   /opt/gitlab/embedded/bin/psql -h $DST_PGHOST -U $DST_PGUSER -f praefect_production.sql postgres
   /opt/gitlab/embedded/bin/psql -h $DST_PGHOST -U $DST_PGUSER -f gitlabhq_production.sql postgres
   ```

1. 在 `/etc/gitlab/gitlab.rb` 文件中，为您的目标 PostgreSQL 实例配置有适当的连接详细信息的极狐GitLab 应用程序服务器：

   ```ruby
   gitlab_rails['db_host'] = '<destination postgresql host>'
   ```

<!--
   For more information on GitLab multi-node setups, refer to the [reference architectures](../reference_architectures/index.md).
-->

1. 重新配置，使更改生效：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

1. 重启极狐GitLab：

   ```shell
   sudo gitlab-ctl start
   ```
