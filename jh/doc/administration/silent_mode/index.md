---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 静默模式（Alpha） **(FREE SELF)**

> 引入于 15.11 版本。此功能处于实验状态。

静默模式允许您抑制来自极狐GitLab 的通信，例如电子邮件。静默模式不适用于正在使用的环境。用例：

- 验证 Geo 站点提升。作为[灾难恢复](../geo/disaster_recovery/index.md)解决方案的一部分，您有一个 Geo 次要站点。您希望定期测试将其提升为主要 Geo 站点，作为确保您的灾难恢复计划真正起作用的最佳实践。但是您不想实际执行整个故障转移，因为主要站点位于为您的用户提供最低延迟的区域。而且您不想在每次常规测试期间都停机。因此，您希望主站点保持运行状态，同时提升次要站点。您开始对提升的站点进行冒烟测试，但是提升的站点开始向用户发送电子邮件，推送镜像功能将更改推送到外部 Git 仓库等。这就是静默模式的用武之地，您可以将其作为站点提升的一部分启用，来避免这些问题。
- 验证极狐GitLab 备份。您设置了一个测试实例来测试您的备份是否成功还原。作为恢复的一部分，您启用静默模式，避免向用户发送无效电子邮件。

## 启用静默模式

先决条件：

- 您必须具有管理员权限。

有两种方法可以启用静默模式：

- [**API**](../../api/settings.md)：

  ```shell
  curl --request PUT --header "PRIVATE-TOKEN:$ADMIN_TOKEN" "<gitlab-url>/api/v4/application/settings?silent_mode_enabled=true"
  ```

- [**Rails 控制台**](../operations/rails_console.md#starting-a-rails-console-session)：

  ```ruby
  ::Gitlab::CurrentSettings.update!(silent_mode_enabled: true)
  ```

最多可能需要一分钟才能生效。

## 禁用静默模式

先决条件：

- 您必须具有管理员权限。

有两种方法可以禁用静默模式：

- [**API**](../../api/settings.md)：

  ```shell
  curl --request PUT --header "PRIVATE-TOKEN:$ADMIN_TOKEN" "<gitlab-url>/api/v4/application/settings?silent_mode_enabled=false"
  ```

- [**Rails 控制台**](../operations/rails_console.md#starting-a-rails-console-session)：

  ```ruby
  ::Gitlab::CurrentSettings.update!(silent_mode_enabled: false)
  ```

## 极狐GitLab 功能在静默模式下的行为

### 服务台

传入电子邮件仍然会生成议题，但是将电子邮件发送到[服务台](../../user/project/service_desk.md)的用户不会收到有关议题创建或对其议题发表评论的通知。

### Webhooks

[项目和群组 webhooks](../../user/project/integrations/webhooks.md)和[系统钩子](../system_hooks.md)被抑制。当启用静默模式时，相关 Sidekiq 作业失败 4 次，然后消失。

通过 UI 触发 Webhook 测试会导致 HTTP 状态码 500 响应。

### 远端项目镜像

[远端项目镜像](../../user/project/repository/mirror/index.md)上的更新（推送以及拉取）被抑制。

### 集成

可执行的[集成](../../user/project/integrations/index.md)被抑制。

### 出站电子邮件

出站电子邮件被抑制。

### 出站 HTTP 请求

许多出站 HTTP 请求被抑制。因为计划进行更多抑制，目前不存在未抑制请求的列表。
