---
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# LDAP 同步 **(PREMIUM SELF)**

如果你已经[配置 LDAP 与极狐GitLab 一起使用](index.md)，系统可以自动同步用户和群组。此过程更新用户和群组信息。

您可以更改同步发生的时间。

<a id="user-sync"></a>

## 用户同步

> 阻止 LDAP 用户的个人资料名称同步功能引入于 15.11 版本。

每天一次，系统运行一个 worker 来根据 LDAP 检查和更新极狐GitLab 用户。

该流程执行以下访问检查：

- 确保用户仍然存在于 LDAP 中。
- 如果 LDAP 服务器是 Active Directory，请确保用户处于激活状态（未处于阻止/禁用状态）。只有在 LDAP 配置中设置了 `active_directory: true` 时，才会执行此检查。

在 Active Directory 中，如果用户帐户控制属性 (`userAccountControl:1.2.840.113556.1.4.803`) 设置了 bit 2，则用户被标记为禁用/阻止。

<!-- vale gitlab.Spelling = NO -->

有关详细信息，请参阅 [LDAP 中的 Bitmask Searches](https://ctovswild.com/2009/09/03/bitmask-searches-in-ldap/)。

<!-- vale gitlab.Spelling = YES -->

如果前面的条件失败，则用户在极狐GitLab 中将设置为 `ldap_blocked` 状态，用户无法登录或推送或拉取代码。

该流程还会更新以下用户信息：

- 姓名。如果启用了**禁止用户更改个人资料名称**，则不同步 `name`。
- 电子邮件地址
- SSH 公钥（如果设置了 `sync_ssh_keys`）
- Kerberos 身份（如果启用了 Kerberos）

### 同步 LDAP 用户的个人资料名称

默认情况下，极狐GitLab 会同步 LDAP 用户的个人资料名称字段。

要阻止这种同步，您可以将 `sync_name` 设置为 `false`。

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_servers'] = {
     'main' => {
       'sync_name' => false,
       }
   }
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml`：

   ```yaml
   global:
     appConfig:
       ldap:
         servers:
           main:
             sync_name: false
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

**Docker**

1. 编辑 `docker-compose.yml`：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['ldap_servers'] = {
             'main' => {
               'sync_name' => false,
               }
           }
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production: &base
     ldap:
       servers:
         main:
           sync_name: false
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

### 禁用用户

在以下情况，用户会被禁用：

- [访问检查失败](#user-sync)并且该用户在极狐GitLab 中设置为 `ldap_blocked` 状态。
- 当该用户登录时，LDAP 服务器不可用。

如果用户被禁用，则该用户无法登录，无法推送或拉取代码。

如果满足以下所有条件，则被禁用的用户在使用 LDAP 登录时将被解除禁用：

- 所有访问检查条件均为 `true`。
- 用户登录时 LDAP 服务器可用。

如果运行 LDAP 用户同步时 LDAP 服务器不可用，**所有用户**都会被禁用。

NOTE:
如果在运行 LDAP 用户同步时由于 LDAP 服务器不可用而禁用所有用户，则后续 LDAP 用户同步不会自动取消禁用这些用户。

### 调整 LDAP 用户同步计划

默认情况下，GitLab 每天在服务器时间上午 01:30 运行一次 worker，根据 LDAP 检查和更新极狐GitLab 用户。

您可以通过 cron 格式，设置以下配置值来手动配置 LDAP 用户同步时间。如果需要，您可以使用 [crontab 生成器](http://www.crontabgenerator.com)。
下面的示例显示了如何将 LDAP 用户同步设置为每 12 小时在每小时运行一次。

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_sync_worker_cron'] = "0 */12 * * *"
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml`：

   ```yaml
   global:
     appConfig:
       cron_jobs:
         ldap_sync_worker:
           cron: "0 */12 * * *"
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

**Docker**

1. 编辑 `docker-compose.yml`：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['ldap_sync_worker_cron'] = "0 */12 * * *"
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production: &base
     ee_cron_jobs:
       ldap_sync_worker:
         cron: "0 */12 * * *"
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

## 群组同步

如果您的 LDAP 支持 `memberof` 属性，则当用户第一次登录极狐GitLab 时，会触发用户所在的群组的同步。
这样他们就不必等待每小时同步来授予对他们的群组和项目的访问权限。

群组同步进程每小时运行一次，必须在 LDAP 配置中设置 `group_base` 才能使基于组 CN 的 LDAP 同步工作。允许极狐GitLab 群组成员根据 LDAP 组成员自动更新。

`group_base` 配置应该是一个基本的 LDAP “容器”，例如一个 “组织” 或 “组织单位”，其中包含可用于极狐GitLab 的 LDAP 组。例如，`group_base` 可以是 `ou=groups,dc=example,dc=com`。在配置文件中，示例如下。

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_servers'] = {
     'main' => {
       'group_base' => 'ou=groups,dc=example,dc=com',
       }
   }
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml`：

   ```yaml
   global:
     appConfig:
       ldap:
         servers:
           main:
             group_base: ou=groups,dc=example,dc=com
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

**Docker**

1. 编辑 `docker-compose.yml`：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['ldap_servers'] = {
             'main' => {
               'group_base' => 'ou=groups,dc=example,dc=com',
               }
           }
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production: &base
     ldap:
       servers:
         main:
           group_base: ou=groups,dc=example,dc=com
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

要利用群组同步，群组所有者或具有[维护者角色](../../../user/permissions.md)的用户必须[创建一个或多个 LDAP 群组链接](../../../user/group/access_and_permissions.md#manage-group-memberships-via-ldap)。

### 添加群组链接

有关使用 CN 和 filter 添加群组链接的信息，请参阅[极狐GitLab 群组文档](../../../user/group/access_and_permissions.md#manage-group-memberships-via-ldap)。

### 管理员同步

作为群组同步的扩展，您可以自动管理您的全局极狐GitLab 管理员。为 `admin_group` 指定一个组 CN，LDAP 组的所有成员都被授予管理员权限。配置如下所示。

NOTE:
除非在 `admin_group` 旁边还指定了 `group_base`，否则不会同步管理员。此外，仅指定 `admin_group` 的 CN，而不是完整的 DN。
此外，如果 LDAP 用户具有 `admin` 角色，但不是 `admin_group` 组的成员，系统会在同步时撤销其 `admin` 角色。

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_servers'] = {
     'main' => {
       'group_base' => 'ou=groups,dc=example,dc=com',
       'admin_group' => 'my_admin_group',
       }
   }
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml`：

   ```yaml
   global:
     appConfig:
       ldap:
         servers:
           main:
             group_base: ou=groups,dc=example,dc=com
             admin_group: my_admin_group
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

**Docker**

1. 编辑 `docker-compose.yml`：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['ldap_servers'] = {
             'main' => {
               'group_base' => 'ou=groups,dc=example,dc=com',
               'admin_group' => 'my_admin_group',
               }
           }
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production: &base
     ldap:
       servers:
         main:
           group_base: ou=groups,dc=example,dc=com
           admin_group: my_admin_group
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

### 全局群组成员锁定

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/1793) in GitLab 12.0.
-->

极狐GitLab 管理员可以阻止群组成员邀请新成员加入与 LDAP 同步成员的子组。

全局群组成员锁定仅适用于配置了 LDAP 同步的顶级组的子组。任何用户都不能修改由 LDAP 同步配置的顶级群组的成员身份。

启用全局群组成员锁定时：

- 只有管理员可以管理任何群组的成员资格，包括访问级别。
- 不允许用户与其他群组共享项目，或邀请成员加入群组中创建的项目。

要启用它，您必须：

1. [配置 LDAP](index.md#configure-ldap)。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 确保选中 **锁定成员身份到 LDAP 同步** 复选框。

### 更改 LDAP 群组同步设置管理

默认情况下，具有所有者角色的群组成员可以管理 [LDAP 群组同步设置](../../../user/group/access_and_permissions.md#manage-group-memberships-via-ldap)。

极狐GitLab 管理员可以从群组所有者中删除此权限：

1. [配置 LDAP](index.md#configure-ldap)。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 确保未选中 **允许群组所有者管理 LDAP 相关设置** 复选框。

当禁用**允许群组所有者管理 LDAP 相关设置**时：

- 群组所有者无法更改顶级群组和子组的 LDAP 同步设置。
- 实例管理员可以管理实例上所有群组的 LDAP 同步设置。

### 调整 LDAP 群组同步计划

默认情况下，系统每小时运行一次群组同步过程。
显示的值采用 cron 格式。如果需要，您可以使用 [Crontab Generator](http://www.crontabgenerator.com)。

WARNING:
不要太频繁地启动同步过程，因为这可能导致多个同步同时运行。此问题主要针对具有大量 LDAP 用户的安装实例。在继续之前，请查看 [LDAP 群组同步 benchmark 指标](#benchmarks) 以了解您的安装情况。

您可以通过设置以下配置值来手动配置 LDAP 群组同步时间。下面的示例显示了如何将群组同步设置为每两小时运行一次。

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_group_sync_worker_cron'] = "0 */2 * * * *"
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml`：

   ```yaml
   global:
     appConfig:
       cron_jobs:
         ldap_group_sync_worker:
           cron: "*/30 * * * *"
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

**Docker**

1. 编辑 `docker-compose.yml`：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['ldap_group_sync_worker_cron'] = "0 */2 * * * *"
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production: &base
     ee_cron_jobs:
       ldap_group_sync_worker:
         cron: "*/30 * * * *"
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```


<a id="external-groups"></a>

### 外部组

使用 `external_groups` 设置允许您将属于这些群组的所有用户标记为外部用户。
通过 `LdapGroupSync` 后台任务定期检查群组成员身份。

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_servers'] = {
     'main' => {
       'external_groups' => ['interns', 'contractors'],
       }
   }
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml`：

   ```yaml
   global:
     appConfig:
       ldap:
         servers:
           main:
             external_groups: ['interns', 'contractors']
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

**Docker**

1. 编辑 `docker-compose.yml`：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['ldap_servers'] = {
             'main' => {
               'external_groups' => ['interns', 'contractors'],
             }
           }
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production: &base
     ldap:
       servers:
         main:
           external_groups: ['interns', 'contractors']
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

### 群组同步技术细节

本节概述了 LDAP 执行的查询以及您可以从群组同步中获得什么。

如果 LDAP 组成员身份发生变化，组成员访问权限将从更高级别降级。 例如，如果用户在组中的所有者角色，下一次组同步显示他们应该只有开发人员角色，他们的访问权限会相应调整。唯一的例外是如果用户是组中的最后一个所有者。群组需要至少一名所有者来履行管理职责。

#### 支持的 LDAP 组类型/属性

极狐GitLab 支持使用成员属性的 LDAP 组：

- `member`
- `submember`
- `uniquemember`
- `memberof`
- `memberuid`

这意味着组同步支持（至少）具有以下对象类的 LDAP 组：

- `groupOfNames`
- `posixGroup`
- `groupOfUniqueNames`

如果成员被定义为上述属性之一，则其他对象类应该可以工作。

Active Directory 支持嵌套组。如果在配置文件中设置了`active_directory: true`，则组同步会递归地解析成员身份。

##### 嵌套组成员资格

仅当在配置的 `group_base` 中找到嵌套组时，才会解析嵌套组成员身份。例如，如果极狐GitLab 看到一个带有 DN `cn=nested_group,ou=special_groups,dc=example,dc=com` 的嵌套组，但配置的 `group_base` 是 `ou=groups,dc=example,dc=com`，`cn=nested_group` 被忽略。

#### Queries

- 每个 LDAP 组最多查询一次，使用基础 `group_base` 和过滤器 `(cn=<cn_from_group_link>)`。
- 如果 LDAP 组具有 `memberuid` 属性，系统会为每个成员执行另一个 LDAP 查询以获取每个用户的完整 DN。这些查询使用基础 `base`、范围 “base object” 和过滤器执行，具体取决于是否设置了 `user_filter`。过滤器可以是 `(uid=<uid_from_group>)` 或 `user_filter` 的结合。

#### Benchmarks

写入组同步是为了尽可能提高性能。缓存数据，优化数据库查询，最小化 LDAP 查询。最后一次 benchmark 显示了以下指标：

对于 20,000 个 LDAP 用户、11,000 个 LDAP 组和 1,000 个 GitLab 群组，每个群组具有 10 个 LDAP 组链接：

- 初始同步（在系统中没有分配现有成员）耗时 1.8 小时
- 后续同步（检查成员资格，无写入）耗时 15 分钟

这些指标旨在提供基准，性能可能因多种因素而异。这个基准是极端的，大多数实例没有这么多的用户或群组。磁盘速度、数据库性能、网络和 LDAP 服务器响应时间会影响这些指标。
