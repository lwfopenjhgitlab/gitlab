---
type: reference
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 通用 LDAP 设置 **(FREE SELF)**

<!--
GitLab integrates with [LDAP](https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol)
to support user authentication.

This integration works with most LDAP-compliant directory servers, including:

- Microsoft Active Directory
  - [Microsoft Active Directory Trusts](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/cc771568(v=ws.10)) are not supported.
- Apple Open Directory
- Open LDAP
- 389 Server

Users added through LDAP take a [licensed seat](../../../subscriptions/self_managed/index.md#billable-users).

GitLab Enterprise Editions (EE) include enhanced integration,
including group membership syncing and multiple LDAP server support.
-->

极狐GitLab 与 LDAP 集成，以支持用户身份验证。

此集成适用于大多数 LDAP 兼容目录服务器，包括：

- Microsoft Active Directory。
- Apple Open Directory。
- Open LDAP。
- 389 Server。

通过 LDAP 添加的用户：

- 通常使用许可席位<!--[许可席位](../../../subscriptions/self_managed/index.md#billable-users)-->。
- 可以使用他们的极狐GitLab 用户名或电子邮件和 LDAP 密码，对 Git 进行身份验证，即使 Git 的密码身份验证已禁用<!-- [已禁用](../../../user/admin_area/settings/sign_in_restrictions.md#password-authentication-enabled)-->。

在以下情况下，LDAP DN 与现有的极狐GitLab 用户相关联：

- 现有用户首次使用 LDAP 登录极狐GitLab。
- LDAP 电子邮件地址是现有极狐GitLab 用户的主要电子邮件地址。如果在极狐GitLab 用户数据库中找不到 LDAP 电子邮件属性，则会创建一个新用户。

如果现有的极狐GitLab 用户想要为自己启用 LDAP 登录，他们应该：

1. 检查他们的极狐GitLab 电子邮件地址是否与他们的 LDAP 电子邮件地址匹配。
1. 使用他们的 LDAP 凭据登录极狐GitLab。

## 安全

极狐GitLab 有多种机制来验证用户在 LDAP 中是否仍然处于活动状态。如果用户在 LDAP 中不再处于活动状态，他们将处于 `ldap_blocked` 状态并被注销。在 LDAP 中重新激活之前，他们无法使用任何身份验证提供程序登录。

用户在以下情况下被视为在 LDAP 中处于非活动状态：

- 从目录中完全删除。
- 驻留在配置的 `base` DN 或 `user_filter` 搜索之外。
- 通过用户帐户控制属性在 Active Directory 中标记为禁用或停用。这意味着属性 `userAccountControl:1.2.840.113556.1.4.803` 设置了第 2 位。

出现以下情况，检查所有 LDAP 用户的状态：

- 使用任何身份验证提供程序登录时。在 14.4 及更早版本中，仅在直接使用 LDAP 登录时检查状态。
- 每小时一次，用于使用令牌或 SSH 密钥的活动 Web 会话或 Git 请求。
- 使用 LDAP 用户名和密码通过 HTTP 请求执行 Git 时。
- <!--[用户同步](ldap_synchronization.md#user-sync)-->用户同步期间每天一次。

### 安全风险

您应该只使用 LDAP 集成，如果您的 LDAP 用户不能：

- 更改他们在 LDAP 服务器上的 `mail`、`email` 或 `userPrincipalName` 属性。这些用户可能会接管您的极狐GitLab 服务器上的任何帐户。
- 共享电子邮件地址。具有相同电子邮件地址的 LDAP 用户可以共享相同的极狐GitLab 帐户。

<a id="configure-ldap"></a>

## 配置 LDAP

LDAP 用户必须设置一个电子邮件地址，无论是否用于登录。

下面是仅使用必需选项设置 LDAP 的示例。

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_enabled'] = true
   gitlab_rails['ldap_servers'] = {
     'main' => {
       'label' => 'LDAP',
       'host' =>  'ldap.mydomain.com',
       'port' => 636,
       'uid' => 'sAMAccountName',
       'encryption' => 'simple_tls',
       'base' => 'dc=example,dc=com',
     }
   }
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml`：

   ```yaml
   global:
     appConfig:
       ldap:
         servers:
           main:
             label: 'LDAP'
             host: 'ldap.mydomain.com'
             port: 636
             uid: 'sAMAccountName'
             base: 'dc=example,dc=com'
             encryption: 'simple_tls'
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

有关详细信息，请参阅[如何为使用 Helm chart 安装的极狐GitLab 实例配置 LDAP](https://docs.gitlab.cn/charts/charts/globals.html#ldap)。

**Docker**

1. 编辑 `docker-compose.yml`：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       image: 'gitlab/gitlab-ee:latest'
       restart: always
       hostname: 'gitlab.example.com'
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['ldap_enabled'] = true
           gitlab_rails['ldap_servers'] = {
             'main' => {
               'label' => 'LDAP',
               'host' =>  'ldap.mydomain.com',
               'port' => 636,
               'uid' => 'sAMAccountName',
               'encryption' => 'simple_tls',
               'base' => 'dc=example,dc=com',
             }
           }
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production: &base
     ldap:
       enabled: true
       servers:
         main:
           label: 'LDAP'
           host: 'ldap.mydomain.com'
           port: 636
           uid: 'sAMAccountName'
           encryption: 'simple_tls'
           base: 'dc=example,dc=com'
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

有关各种 LDAP 选项的更多信息，请参阅 [`gitlab.yml.example`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/config/gitlab.yml.example)。

配置 LDAP 后，要测试配置，请使用 [LDAP 检查 Rake 任务](../../raketasks/ldap.md#check)。

### 基本配置

> `hosts` 配置设置引入于 14.7 版本。

您可以配置以下二者之一：

- 使用 `host` 和 `port` 的单个 LDAP 服务器。
- 多 LDAP 服务器使用 `hosts`。此设置优先于 `host` 和 `port`。系统尝试按指定的顺序使用 LDAP 服务器，并使用第一个可访问的 LDAP 服务器。

以下配置设置可用：

| 配置项           | 说明 | 是否必须配置 | 举例 |
|--------------------|-------------|----------|----------|
| `label`            | LDAP 服务器的人性化名称。在登录页面展示。 | **{check-circle}** Yes | `'Paris'` or `'Acme, Ltd.'` |
| `host`             | LDAP 服务器的 IP 地址或域名。当 `hosts` 被定义时被忽略。 | **{check-circle}** Yes | `'ldap.mydomain.com'` |
| `port`             | 与 LDAP 服务器连接的端口。类型为整数而非字符串。当 `hosts` 被定义时被忽略。 | **{check-circle}** Yes | `389` 或 `636` (用于 SSL) |
| `hosts`（14.7 及更高版本） | 一个包含用于打开连接的主机和端口对数组。 | **{dotted-circle}** No | `[['ldap1.mydomain.com', 636], ['ldap2.mydomain.com', 636]]` |
| `uid`              | 用户名的 LDAP 属性。应该是属性，而不是映射到 `uid` 的值。 | **{check-circle}** Yes | `'sAMAccountName'`、`'uid'` 或 `'userPrincipalName'` |
| `bind_dn`          | 绑定用户的完整 DN。 | **{dotted-circle}** No | `'america\momo'` 或 `'CN=Gitlab,OU=Users,DC=domain,DC=com'` |
| `password`         | 绑定用户的密码。 | **{dotted-circle}** No | `'your_great_password'` |
| `encryption`       | 加密方法。`method` 已废弃。 | **{check-circle}** Yes | `'start_tls'`、`'simple_tls'` 或 `'plain'`。`simple_tls` 对应于 LDAP 库中的 “Simple TLS”。`start_tls` 对应于 StartTLS，不要与常规 TLS 混淆。如果您指定 `simple_tls`，通常它在端口 636 上，而 `start_tls` (StartTLS) 将在端口 389 上。`plain` 也在端口 389 上运行。 |
| `verify_certificates` | 如果加密方法为 `start_tls` 或 `simple_tls`，则启用 SSL 证书验证。如果设置为 false，则不会对 LDAP 服务器的 SSL 证书进行验证。默认为 true。 | **{dotted-circle}** No | boolean |
| `timeout`          | 为 LDAP 查询设置超时时间（以秒为单位）。当 LDAP 服务器无法响应时，有助于阻止请求。`0` 值表示没有超时（默认值：`10`） | **{dotted-circle}** No | `10` 或 `30` |
| `active_directory` | 该设置指定 LDAP 服务器是否为 Active Directory LDAP 服务器。对于非 AD 服务器，会跳过 AD 特定查询。如果您的 LDAP 服务器不是 AD，请将其设置为 false。 | **{dotted-circle}** No | boolean |
| `allow_username_or_email_login` | 如果启用，系统将忽略用户在登录时提交的 LDAP 用户名中第一个 `@` 之后的所有内容。如果您在 ActiveDirectory 上使用 `uid: 'userPrincipalName'`，您必须禁用这个设置，因为 userPrincipalName 包含一个 `@`。 | **{dotted-circle}** No | boolean |
| `block_auto_created_users` | 为了严格控制计费用户数量，请启用此设置阻止新用户直到他们被管理员清除（默认值：false）。 | **{dotted-circle}** No | boolean |
| `base` | 搜索用户的基础路径。 | **{check-circle}** Yes | `'ou=people,dc=gitlab,dc=example'` or `'DC=mydomain,DC=com'` |
| `user_filter`      | 过滤 LDAP 用户。格式：[RFC 4515](https://www.rfc-editor.org/rfc/rfc4515.html) 注意：不支持 `omniauth-ldap` 的自定义过滤器语法。 | **{dotted-circle}** No | `user_filter` 字段语法的一些示例：<br/><br/>- `'(employeeType=developer)'`<br/>- `'(&(objectclass=user)(|(samaccountname=momo)(samaccountname=toto)))'` |
| `lowercase_usernames` | 如果启用，系统转换用户名称为小写。 | **{dotted-circle}** No | boolean |
| `retry_empty_result_with_codes` | 如果结果/内容为空，将尝试重试操作时的 LDAP 查询响应代码数组。 | **{dotted-circle}** No | `[80]` |
| `attributes` | 映射到 LDAP 的属性哈希，供极狐GitLab 使用（[请参阅属性部分](#attribute-configuration-settings)）。 | **{dotted-circle}** No | `'attributes' => { 'username' => ['uid'], 'email' => ['mail', 'email'] },` |

### SSL 配置

<!--
| Setting       | Description | Required | Examples |
|---------------|-------------|----------|----------|
| `ca_file`     | Specifies the path to a file containing a PEM-format CA certificate, for example, if you need to use an internal CA. | **{dotted-circle}** No | `'/etc/ca.pem'` |
| `ssl_version` | Specifies the SSL version for OpenSSL to use, if the OpenSSL default is not appropriate. | **{dotted-circle}** No | `'TLSv1_1'` |
| `ciphers`     | Specific SSL ciphers to use in communication with LDAP servers. | **{dotted-circle}** No | `'ALL:!EXPORT:!LOW:!aNULL:!eNULL:!SSLv2'` |
| `cert`        | Client certificate. | **{dotted-circle}** No | `'-----BEGIN CERTIFICATE----- <REDACTED> -----END CERTIFICATE -----'` |
| `key`         | Client private key. | **{dotted-circle}** No | `'-----BEGIN PRIVATE KEY----- <REDACTED> -----END PRIVATE KEY -----'` |
-->

| 配置项       | 说明 | 是否必须配置 | 举例 |
|---------------|-------------|----------|----------|
| `ca_file`     | 例如，当您需要使用内部 CA 时，指定 PEM 格式的 CA 证书文件的路径。 | **{dotted-circle}** No | `'/etc/ca.pem'` |
| `ssl_version` | 如果 OpenSSL 默认值不合适，指定 OpenSSL 的 SSL 版本。 | **{dotted-circle}** No | `'TLSv1_1'` |
| `ciphers`     | 用于与 LDAP 服务器通信的特定 SSL 密码。 | **{dotted-circle}** No | `'ALL:!EXPORT:!LOW:!aNULL:!eNULL:!SSLv2'` |
| `cert`        | 客户凭证。 | **{dotted-circle}** No | `'-----BEGIN CERTIFICATE----- <REDACTED> -----END CERTIFICATE -----'` |
| `key`         | 客户私钥。 | **{dotted-circle}** No | `'-----BEGIN PRIVATE KEY----- <REDACTED> -----END PRIVATE KEY -----'` |

<a id="attribute-configuration-settings"></a>

### 属性配置

<!--
LDAP attributes that GitLab uses to create an account for the LDAP user. The specified
attribute can either be the attribute name as a string (for example, `'mail'`), or an
array of attribute names to try in order (for example, `['mail', 'email']`). Note that
the user's LDAP sign-in is the attribute specified as `uid` above.

| Setting      | Description | Required | Examples |
|--------------|-------------|----------|----------|
| `username`   | The username is used in paths for the user's own projects (like `gitlab.example.com/username/project`) and when mentioning them in issues, merge request and comments (like `@username`). If the attribute specified for `username` contains an email address, the GitLab username is part of the email address before the `@`. | **{dotted-circle}** No | `['uid', 'userid', 'sAMAccountName']` |
| `email`      | LDAP attribute for user email. | **{dotted-circle}** No | `['mail', 'email', 'userPrincipalName']` |
| `name`       | LDAP attribute for user display name. If `name` is blank, the full name is taken from the `first_name` and `last_name`. | **{dotted-circle}** No | Attributes `'cn'`, or `'displayName'` commonly carry full names. Alternatively, you can force the use of `first_name` and `last_name` by specifying an absent attribute such as `'somethingNonExistent'`. |
| `first_name` | LDAP attribute for user first name. Used when the attribute configured for `name` does not exist. | **{dotted-circle}** No | `'givenName'` |
| `last_name`  | LDAP attribute for user last name. Used when the attribute configured for `name` does not exist. | **{dotted-circle}** No | `'sn'` |
-->

系统为 LDAP 用户创建账户时，使用的 LDAP 属性。指定的属性可以是字符串形式的属性名称（例如，`'mail'`），也可以是要按顺序尝试的属性名称数组（例如，`['mail', 'email']`）。用户的 LDAP 登录时指定为 `uid` 的属性。

您必须在 `attributes` 键值对中定义以下属性。

| 配置项       | 说明 | 是否必须配置 | 举例 |
|--------------|-------------|----------|----------|
| `username`   | 用户名用于用户自己项目的路径 (例如 `gitlab.example.com/username/project`) ，以及在议题、合并请求和评论中提及时 (例如 `@username`)。如果 `username` 包含电子邮件地址, 则地址中 `@` 之前的部分作为极狐GitLab 用户名。 | **{dotted-circle}** No | `['uid', 'userid', 'sAMAccountName']` |
| `email`      | 用户电子邮件的 LDAP 属性。 | **{dotted-circle}** No | `['mail', 'email', 'userPrincipalName']` |
| `name`       | 用户显示名称的 LDAP 属性。如果 `name` 为空, 完整的显示名称从 `first_name` 和 `last_name` 获取。 | **{dotted-circle}** No |  `'cn'` 或 `'displayName'` 属性通常带有全名。或者，您可以通过指定不存在的属性，例如  `'somethingNonExistent'`，来强制使用 `first_name` 和 `last_name`。|
| `first_name` | 用户 first name 的 LDAP 属性。当 `name` 属性不存在时使用。 | **{dotted-circle}** No | `'givenName'` |
| `last_name`  | 用户 last name 的 LDAP 属性。当 `name` 属性不存在时使用。 | **{dotted-circle}** No | `'sn'` |


### LDAP 同步配置 **(PREMIUM SELF)**

<!--
| Setting           | Description | Required | Examples |
|-------------------|-------------|----------|----------|
| `group_base`      | Base used to search for groups. | **{dotted-circle}** No | `'ou=groups,dc=gitlab,dc=example'` |
| `admin_group`     | The CN of a group containing GitLab administrators. Note: Not `cn=administrators` or the full DN. | **{dotted-circle}** No | `'administrators'` |
| `external_groups` | An array of CNs of groups containing users that should be considered external. Note: Not `cn=interns` or the full DN. | **{dotted-circle}** No | `['interns', 'contractors']` |
| `sync_ssh_keys`   | The LDAP attribute containing a user's public SSH key. | **{dotted-circle}** No | `'sshPublicKey'` or false if not set |
-->

| 配置项       | 说明 | 是否必须配置 | 举例 |
|-------------------|-------------|----------|----------|
| `group_base`      | 搜索用户组的基础路径。 | **{dotted-circle}** No | `'ou=groups,dc=gitlab,dc=example'` |
| `admin_group`     | 包含 GitLab 管理员的组的 CN。注意：不是`cn=administrators` 或完整的DN。 | **{dotted-circle}** No | `'administrators'` |
| `external_groups` | 包含应被视为外部用户的组 CN 数组。 注意：不是 `cn=interns` 或完整的 DN。 | **{dotted-circle}** No | `['interns', 'contractors']` |
| `sync_ssh_keys`   | 包含用户的公共 SSH 密钥的 LDAP 属性。 | **{dotted-circle}** No | `'sshPublicKey'`  或者 false（如果未设置）。 |

### 使用多 LDAP 服务器 **(PREMIUM SELF)**

如果您在多个 LDAP 服务器上有用户，您可以配置极狐GitLab 使用它们。要添加额外的 LDAP 服务器：

1. 复制 [`main` LDAP 配置](#configure-ldap)。
1. 使用附加服务器的详细信息编辑每个重复配置。
    - 对于每个额外的服务器，选择不同的提供者 ID，如 `main`、`secondary` 或 `tertiary`。使用小写字母数字字符。极狐GitLab 使用提供者 ID 将每个用户与特定的 LDAP 服务器相关联。
    - 对于每个条目，使用唯一的 `label` 值。这些值用于登录页面上的选项卡名称。

以下示例显示了如何使用最少的配置，配置三个 LDAP 服务器：

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_enabled'] = true
   gitlab_rails['ldap_servers'] = {
     'main' => {
       'label' => 'GitLab AD',
       'host' =>  'ad.mydomain.com',
       'port' => 636,
       'uid' => 'sAMAccountName',
       'encryption' => 'simple_tls',
       'base' => 'dc=example,dc=com',
     },

     'secondary' => {
       'label' => 'GitLab Secondary AD',
       'host' =>  'ad-secondary.mydomain.com',
       'port' => 636,
       'uid' => 'sAMAccountName',
       'encryption' => 'simple_tls',
       'base' => 'dc=example,dc=com',
     },

     'tertiary' => {
       'label' => 'GitLab Tertiary AD',
       'host' =>  'ad-tertiary.mydomain.com',
       'port' => 636,
       'uid' => 'sAMAccountName',
       'encryption' => 'simple_tls',
       'base' => 'dc=example,dc=com',
     }
   }
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml`：

   ```yaml
   global:
     appConfig:
       ldap:
         servers:
           main:
             label: 'GitLab AD'
             host: 'ad.mydomain.com'
             port: 636
             uid: 'sAMAccountName'
             base: 'dc=example,dc=com'
             encryption: 'simple_tls'
           secondary:
             label: 'GitLab Secondary AD'
             host: 'ad-secondary.mydomain.com'
             port: 636
             uid: 'sAMAccountName'
             base: 'dc=example,dc=com'
             encryption: 'simple_tls'
           tertiary:
             label: 'GitLab Tertiary AD'
             host: 'ad-tertiary.mydomain.com'
             port: 636
             uid: 'sAMAccountName'
             base: 'dc=example,dc=com'
             encryption: 'simple_tls'
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

**Docker**

1. 编辑 `docker-compose.yml`：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       image: 'gitlab/gitlab-ee:latest'
       restart: always
       hostname: 'gitlab.example.com'
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['ldap_enabled'] = true
           gitlab_rails['ldap_servers'] = {
             'main' => {
               'label' => 'GitLab AD',
               'host' =>  'ad.mydomain.com',
               'port' => 636,
               'uid' => 'sAMAccountName',
               'encryption' => 'simple_tls',
               'base' => 'dc=example,dc=com',
             },

             'secondary' => {
               'label' => 'GitLab Secondary AD',
               'host' =>  'ad-secondary.mydomain.com',
               'port' => 636,
               'uid' => 'sAMAccountName',
               'encryption' => 'simple_tls',
               'base' => 'dc=example,dc=com',
             },

             'tertiary' => {
               'label' => 'GitLab Tertiary AD',
               'host' =>  'ad-tertiary.mydomain.com',
               'port' => 636,
               'uid' => 'sAMAccountName',
               'encryption' => 'simple_tls',
               'base' => 'dc=example,dc=com',
             }
           }
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production: &base
     ldap:
       enabled: true
       servers:
         main:
           label: 'GitLab AD'
           host: 'ad.mydomain.com'
           port: 636
           uid: 'sAMAccountName'
           base: 'dc=example,dc=com'
           encryption: 'simple_tls'
         secondary:
           label: 'GitLab Secondary AD'
           host: 'ad-secondary.mydomain.com'
           port: 636
           uid: 'sAMAccountName'
           base: 'dc=example,dc=com'
           encryption: 'simple_tls'
         tertiary:
           label: 'GitLab Tertiary AD'
           host: 'ad-tertiary.mydomain.com'
           port: 636
           uid: 'sAMAccountName'
           base: 'dc=example,dc=com'
           encryption: 'simple_tls'
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

有关各种 LDAP 选项的更多信息，请参阅 [`gitlab.yml.example`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/config/gitlab.yml.example)。

<!--
This example results in a sign-in page with the following tabs:

- **GitLab AD**.
- **GitLab Secondary AD**.
- **GitLab Tertiary AD**.
-->

### 设置 LDAP 用户过滤器

如果您想要 LDAP 服务器上的一部分 LDAP 用户具有访问极狐GitLab 的权限，第一步应该缩小配置的 `base` 范围。但是，有时必须进一步过滤用户。在这种情况下，您可以设置 LDAP 用户过滤器。过滤器必须符合 [RFC 4515](https://www.rfc-editor.org/rfc/rfc4515.html)。

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_servers'] = {
     'main' => {
       'user_filter' => '(employeeType=developer)'
     }
   }
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml`：

   ```yaml
   global:
     appConfig:
       ldap:
         servers:
           main:
             user_filter: '(employeeType=developer)'
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

**Docker**

1. 编辑 `docker-compose.yml`：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       image: 'gitlab/gitlab-ee:latest'
       restart: always
       hostname: 'gitlab.example.com'
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['ldap_servers'] = {
             'main' => {
               'user_filter' => '(employeeType=developer)'
             }
           }
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production: &base
     ldap:
       servers:
         main:
           user_filter: '(employeeType=developer)'
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

如果要限制对 Active Directory 组的嵌套成员的访问，请使用以下语法：

```plaintext
(memberOf:1.2.840.113556.1.4.1941:=CN=My Group,DC=Example,DC=com)
```

获取 `LDAP_MATCHING_RULE_IN_CHAIN` 过滤器的更多信息，请参阅 [Microsoft 搜索过滤器语法](https://learn.microsoft.com/en-us/windows/win32/adsi/search-filter-syntax) 文档。<!--用户过滤器中对嵌套成员的支持不应与[组同步嵌套组支持](#supported-ldap-group-typesattributes) 混淆。-->**(PREMIUM SELF)**

不支持 OmniAuth LDAP 使用的自定义过滤器语法。

#### 转义 `user_filter` 中的特殊字符

`user_filter` DN 可以包含特殊字符。例如：

- 逗号：

  ```plaintext
  OU=GitLab, Inc,DC=gitlab,DC=com
  ```

- 括号：

  ```plaintext
  OU=GitLab (Inc),DC=gitlab,DC=com
  ```

  这些字符必须按照 [RFC 4515](https://www.rfc-editor.org/rfc/rfc4515.html#section-4) 文档所述进行转义。

- 使用 `\2C` 转义逗号。例如：

  ```plaintext
  OU=GitLab\2C Inc,DC=gitlab,DC=com
  ```

- 使用 `\28` 转义开括号，使用 `\29` 转义闭括号。例如：

  ```plaintext
  OU=GitLab \28Inc\29,DC=gitlab,DC=com
  ```

### 启用 LDAP 用户名小写

某些 LDAP 服务器，根据其配置可以返回大写的用户名。这可能导致一些令人困惑的问题，比如使用大写名称创建链接或命名空间。

极狐GitLab 可以通过启用 `lowercase_usernames` 配置项，自动将 LDAP 服务器提供的用户名小写。默认情况下，该配置选项为 `false`。

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_servers'] = {
     'main' => {
       'lowercase_usernames' => true
     }
   }
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml`：

   ```yaml
   global:
     appConfig:
       ldap:
         servers:
           main:
            lowercase_usernames: true
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

**Docker**

1. 编辑 `docker-compose.yml`：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       image: 'gitlab/gitlab-ee:latest'
       restart: always
       hostname: 'gitlab.example.com'
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['ldap_servers'] = {
             'main' => {
               'lowercase_usernames' => true
             }
           }
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `config/gitlab.yaml`：

   ```yaml
   production:
     ldap:
       servers:
         main:
           lowercase_usernames: true
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

### 禁用 LDAP web 登录

当首选 SAML 等替代方法时，阻止通过 Web UI 使用 LDAP 凭据会很有用。 这允许 LDAP 用于组同步，同时还允许您的 SAML 身份提供商处理额外的检查，如自定义 2FA。

禁用 LDAP Web 登录后，用户在登录页面上看不到 **LDAP** 选项。这不会禁用使用 LDAP 凭据进行 Git 访问。

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['prevent_ldap_sign_in'] = true
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml`：

   ```yaml
   global:
     appConfig:
       ldap:
         preventSignin: true
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

**Docker**

1. 编辑 `docker-compose.yml`：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       image: 'gitlab/gitlab-ee:latest'
       restart: always
       hostname: 'gitlab.example.com'
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['prevent_ldap_sign_in'] = true
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `config/gitlab.yaml`：

   ```yaml
   production:
     ldap:
       prevent_ldap_sign_in: true
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

<a id="use-encrypted-credentials"></a>

### 使用加密凭证

除了将 LDAP 集成凭证以明文形式存储在配置文件中，您可以选择为 LDAP 凭证使用加密文件。

先决条件：

- 要使用加密凭证，您必须首先启用[加密配置](../../encrypted_configuration.md)。

LDAP 的加密配置存在于加密的 YAML 文件中。文件的未加密内容应该是 LDAP 配置中 `servers` 块的 secret 设置的子集。

加密文件的配置项有：

- `bind_dn`
- `password`

**Omnibus**

1. 如果最初您在 `/etc/gitlab/gitlab.rb` 中的 LDAP 配置如下所示：

   ```ruby
     gitlab_rails['ldap_servers'] = {
       'main' => {
         'bind_dn' => 'admin',
         'password' => '123'
       }
     }
   ```

1. 编辑加密的 secret：

   ```shell
   sudo gitlab-rake gitlab:ldap:secret:edit EDITOR=vim
   ```

1. 输入 LDAP secret 的未加密内容：

   ```yaml
   main:
     bind_dn: admin
     password: '123'
   ```

1. 编辑 `/etc/gitlab/gitlab.rb` 并删除 `bind_dn` 和 `password` 的设置。
1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

使用 Kubernetes 密钥存储 LDAP 密码。有关详细信息，请阅读 [Helm LDAP secrets](https://docs.gitlab.com/charts/installation/secrets.html#ldap-password)。

**Docker**

1. 如果最初您在 `docker-compose.yml` 中的 LDAP 配置如下所示：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       image: 'gitlab/gitlab-ee:latest'
       restart: always
       hostname: 'gitlab.example.com'
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['ldap_servers'] = {
             'main' => {
               'bind_dn' => 'admin',
               'password' => '123'
             }
           }
   ```

1. 进入容器，并编辑加密的 secret：

   ```shell
   sudo docker exec -t <container_name> bash
   gitlab-rake gitlab:ldap:secret:edit EDITOR=vim
   ```

1. 输入 LDAP secret 的未加密内容：

   ```yaml
   main:
     bind_dn: admin
     password: '123'
   ```

1. 编辑 `docker-compose.yml` 并删除 `bind_dn` 和 `password` 的设置。
1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 如果最初您在 `/home/git/gitlab/config/gitlab.yml` 中的 LDAP 配置如下所示：

   ```yaml
   production:
     ldap:
       servers:
         main:
           bind_dn: admin
           password: '123'
   ```

1. 编辑加密的 secret：

   ```shell
   bundle exec rake gitlab:ldap:secret:edit EDITOR=vim RAILS_ENVIRONMENT=production
   ```

1. 输入 LDAP secret 的未加密内容：

   ```yaml
   main:
    bind_dn: admin
    password: '123'
   ```

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并删除 `bind_dn` 和 `password` 的设置。
1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

## 更新 LDAP DN 和电子邮件

当 LDAP 服务器在极狐GitLab 中创建用户时，用户的 LDAP 专有名称 (DN) 作为标识符关联到他们的极狐GitLab 帐户。

当用户尝试使用 LDAP 登录时，极狐GitLab 会尝试使用保存在该用户帐户中的 DN 来查找该用户。

- 如果极狐GitLab 通过 DN 和用户的电子邮件地址找到用户：
  - 与极狐GitLab 帐户的电子邮件地址相匹配，极狐GitLab 不会采取任何进一步的操作。
  - 已更改，极狐GitLab 更新其用户电子邮件记录以匹配 LDAP 中的电子邮件。
- 如果极狐GitLab 无法通过 DN 找到用户，它会尝试通过电子邮件找到用户。如果极狐GitLab：
  - 通过电子邮件查找用户，极狐GitLab 更新存储在用户极狐GitLab 帐户中的 DN。这两个值现在都与存储在 LDAP 中的信息匹配。
  - 无法通过电子邮件地址找到用户（DN **和**电子邮件地址已更改）。<!--see [User DN and email have changed](ldap-troubleshooting.md#user-dn-and-email-have-changed).-->

## 禁用匿名 LDAP 身份验证

极狐GitLab 不支持 TLS 客户端身份验证。在您的 LDAP 服务器上完成这些步骤。

1. 禁用匿名身份验证。
1. 启用以下身份验证类型之一：
    - 简单的身份验证。
    - 简单身份验证和安全层 (SASL) 身份验证。

LDAP 服务器中的 TLS 客户端身份验证设置不能是强制性的，并且客户端无法使用 TLS 协议进行身份验证。

## 从 LDAP 中删除的用户

用户从 LDAP 服务器被删除：

- 立即被阻止登录极狐GitLab。
- 不再使用许可证<!--[不再使用许可证](../../../user/admin_area/moderate_users.md)-->。

但是，这些用户可以继续通过 SSH 使用 Git，直到下一次 LDAP 检查缓存运行<!--[LDAP 检查缓存运行](ldap_synchronization.md#adjust-ldap-user-sync-schedule)-->。

要立即删除帐户，您可以手动阻止用户<!--[阻止用户](../../../user/admin_area/moderate_users.md#block-a-user)-->。

## 更新用户电子邮件地址

当使用 LDAP 登录时，LDAP 服务器上的电子邮件地址被认为是用户的真实来源。更新用户电子邮件地址必须在管理用户的 LDAP 服务器上完成。极狐GitLab 的电子邮件地址将更新于：

- 用户下次登录时。
- 下次运行[用户同步](ldap_synchronization.md#user-sync)时。

更新后的用户以前的电子邮件地址成为次要电子邮件地址，用来保留该用户的提交历史记录。

<!--
您可以在我们的 [LDAP 故障排除部分](ldap-troubleshooting.md#user-dn-orand-email-have-changed)中，找到有关用户更新预期行为的更多详细信息。

## Google Secure LDAP

> Introduced in GitLab 11.9.

[Google Cloud Identity](https://cloud.google.com/identity/) provides a Secure
LDAP service that can be configured with GitLab for authentication and group sync.
See [Google Secure LDAP](google_secure_ldap.md) for detailed configuration instructions.

## Synchronize users and groups

For more information on synchronizing users and groups between LDAP and GitLab, see
[LDAP synchronization](ldap_synchronization.md).
-->

<!--
## 故障排查

查看 [LDAP 故障排查管理员指南](ldap-troubleshooting.md).
-->
