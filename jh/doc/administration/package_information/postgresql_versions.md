---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# Linux 软件包附带的 PostgreSQL 版本

NOTE:
此表仅列出了包中发生了有关 PostgreSQL 版本的重大更改的 GitLab 版本，而不是全部。

通常，PostgreSQL 版本会随着极狐GitLab 的主要或次要版本而变化。但是，Linux 软件包的补丁版本有时会更新 PostgreSQL 的补丁级别。

<!--
For example:

- Omnibus 12.7.6 shipped with PostgreSQL 9.6.14 and 10.9.
- Omnibus 12.7.7 shipped with PostgreSQL 9.6.17 and 10.12.
-->

找出每个 Omnibus GitLab 版本随附的 PostgreSQL（和其他组件）版本。

在 PostgreSQL [升级文档](https://docs.gitlab.cn/omnibus/settings/database.md#升级打包的-postgresql-服务器)中阅读有关更新策略和警告的更多信息。

| 极狐GitLab 版本 | PostgreSQL 版本 | 新安装的默认版本 | 升级默认版本 | 说明 |
| --- | --- | --- | --- | --- |
| 16.0.2 | 13.11 | 13.11 | 13.11 | |
| 16.0.0 | 13.8  | 13.8  | 13.8  | |
| 15.11.7 | 13.11 | 13.11 | 12.12 | |
| 15.10.8 | 13.11 | 13.11 | 12.12 | |
| 15.6 | 12.12, 13.8 | 13.8 | 12.12 | 用户可以按照[升级文档](https://docs.gitlab.cn/omnibus/settings/database.html#gitlab-150-and-later)手动升级到 13.8。 |
| 15.0 | 12.10, 13.6 | 13.6 | 13.6 | 用户可以按照[升级文档](https://docs.gitlab.cn/omnibus/settings/database.html#gitlab-150-and-later)手动升级到 13.6。 |
| 14.1 | 12.7, 13.3 | 12.7 | 12.7 | 如果不使用 Geo 或 High Availability，则 PostgreSQL 13 可用于全新安装。 |
| 14.0 | 12.7       | 12.7 | 12.7 | 不再支持使用 repmgr 进行 HA 安装，并且将无法升级到 Linux 软件包 14.0。 |
| 13.8 | 11.9, 12.4 | 12.4 | 12.4 | 软件包升级会为不属于 Geo 或 HA 集群的节点自动执行 PostgreSQL 升级。 |

<!--
| 13.7 | 11.9, 12.4 | 12.4 | 11.9 | For upgrades users can manually upgrade to 12.4 following the [upgrade docs](../settings/database.md#gitlab-133-and-later). |
| 13.4 | 11.9, 12.4 | 11.9 | 11.9 | Package upgrades aborted if users not running PostgreSQL 11 already |
| 13.3 | 11.7, 12.3 | 11.7 | 11.7 | Package upgrades aborted if users not running PostgreSQL 11 already |
| 13.0 | 11.7 | 11.7 | 11.7 | Package upgrades aborted if users not running PostgreSQL 11 already |
| 12.10 | 9.6.17, 10.12, and 11.7 | 11.7 | 11.7 | Package upgrades automatically performed PostgreSQL upgrade for nodes that are not part of a Geo or repmgr cluster. |
| 12.8 | 9.6.17, 10.12, and 11.7 | 10.12 | 10.12 | Users can manually upgrade to 11.7 following the upgrade docs. |
| 12.0 | 9.6.11 and 10.7 | 10.7 | 10.7 | Package upgrades automatically performed PostgreSQL upgrade. |
| 11.11 | 9.6.11 and 10.7 | 9.6.11 | 9.6.11 | Users can manually upgrade to 10.7 following the upgrade docs. |
| 10.0 | 9.6.3 | 9.6.3 | 9.6.3 | Package upgrades aborted if users still on 9.2. |
| 9.0 | 9.2.18 and 9.6.1 | 9.6.1 | 9.6.1 | Package upgrades automatically performed PostgreSQL upgrade. |
| 8.14 | 9.2.18 and 9.6.1 | 9.2.18 | 9.2.18 | Users can manually upgrade to 9.6 following the upgrade docs. |
-->
