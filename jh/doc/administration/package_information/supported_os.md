---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 支持的操作系统 **(FREE SELF)**

极狐GitLab 正式支持 LTS 版本的操作系统。虽然像 Ubuntu 这样的操作系统在 LTS 和非 LTS 版本之间有明显的区别，但还有其他操作系统，例如 openSUSE，不遵循 LTS 概念。因此，为避免混淆，在任何时间点，极狐GitLab 支持的所有操作系统都列在[安装页面](https://gitlab.cn/install/)中。

下面列出了当前支持的操作系统及其可能的 EOL 日期。

NOTE:
`amd64` 和 `x86_64` 指的是相同的 64 位架构。`arm64` 和 `aarch64` 也可以互换，并且指的是相同的架构。

| OS 版本                                                   | 首个支持的极狐GitLab 版本 | 架构            |                         安装文档                         | OS EOL  | 详情                                                      |
| ------------------------------------------------------------ | ------------------------------ | --------------- | :----------------------------------------------------------: |---------| ------------------------------------------------------------ |
| AlmaLinux 8                                                  | 14.5.0   | x86_64、aarch64 | [AlmaLinux 安装文档](https://gitlab.cn/install/#almalinux-8) | 2029    | <https://almalinux.org/>                                     |
| CentOS 7                                                     | 13.10.0   | x86_64          | [CentOS 安装文档](https://gitlab.cn/install/#centos-7) | 2024.06 | <https://wiki.centos.org/About/Product>                      |
| Debian 10                                                    | 13.10.0   | amd64、arm64   | [Debian 安装文档](https://gitlab.cn/install/#debian) | 2024    | <https://wiki.debian.org/LTS>                                |
| Debian 11                                                    | 14.6.0   | amd64、arm64    | [Debian 安装文档](https://gitlab.cn/install/#debian) | 2026    | <https://wiki.debian.org/LTS>                                |
| RHEL 8                                                       | 13.10.0   | x86_64、arm64   | [使用 CentOS 安装文档](https://gitlab.cn/install/#centos-7) | 2029.05 | [RHEL 详情](https://access.redhat.com/support/policy/updates/errata/#Life_Cycle_Dates) |
| RHEL 9                                                      | 16.0.0   | x86_64、arm64   | [使用 CentOS 安装文档](https://gitlab.cn/install/#centos-7) | 2032.05   | [RHEL 详情](https://access.redhat.com/support/policy/updates/errata/#Life_Cycle_Dates) |
| Oracle Linux                                                 | 13.10.0   | x86_64          | [使用 CentOS 安装文档](https://gitlab.cn/install/#centos-7) | 2024.07 | <https://www.oracle.com/a/ocom/docs/elsp-lifetime-069338.pdf>                                                           |
| Scientific Linux                                             | 13.10.0   | x86_64          | [使用 CentOS 安装文档](https://gitlab.cn/install/#centos-7) | 2024.06 | <https://scientificlinux.org/downloads/sl-versions/sl7/>                                                           |
| Ubuntu 18.04                                                 | 13.10.0   | amd64           | [Ubuntu 安装文档](https://gitlab.cn/install/#ubuntu) | 2023.04 | <https://wiki.ubuntu.com/Releases>                           |
| Ubuntu 20.04                                                 | 13.10.0   | amd64、arm64    | [Ubuntu 安装文档](https://gitlab.cn/install/#ubuntu) | 2025.04 | <https://wiki.ubuntu.com/Releases>                           |
| Ubuntu 22.04                                                 | 15.5.0   | amd64、arm64    | [Ubuntu 安装文档](https://gitlab.cn/install/#ubuntu) | 2027.04 | <https://wiki.ubuntu.com/Releases>                           |
| 欧拉（openEuler）                                                | 15.5.4   | x86_64、aarch64    | [软件包](https://packages.gitlab.cn/#browse/search=keyword%3Daarch64%20AND%20format%3Dyum:c7545579a7a1539017d950a48b58bacb) | - | <https://www.openeuler.org/>                           |
| 麒麟（kylinos）                                               | 15.2.0   | arm64    | [软件包](https://packages.gitlab.cn/#browse/search/apt=format%3Dapt%20AND%20group.raw%3Darm64:08909bf0c86cf6c9c697ec801486dcdb) | - | <https://www.kylinos.cn/>                           |

NOTE:
CentOS 8 于 2021 年 12 月 31 日 EOL。<!--In GitLab 14.5 and later,
[CentOS builds work in AlmaLinux](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/954#note_730198505).
We will officially support all distributions that are binary compatible with Red Hat Enterprise Linux.
This gives users a path forward for their CentOS 8 builds at its end of life.-->

<!--
## Update GitLab package sources after upgrading the OS

After upgrading the Operating System (OS) as per its own documentation,
it may be necessary to also update the GitLab package source URL
in your package manager configuration.
If your package manager reports that no further updates are available,
although [new versions have been released](https://about.gitlab.com/releases/categories/releases/), repeat the
"Add the GitLab package repository" instructions
of the [Linux package install guide](https://about.gitlab.com/install/#content).
Future GitLab upgrades will now be fetched according to your upgraded OS.
-->

## 适用于 ARM64 的软件包

极狐GitLab 为某些支持的操作系统提供了 arm64/aarch64 软件包。
您可以在上表中查看您的操作系统架构是否受支持。

WARNING:
目前在 ARM 上运行极狐GitLab 仍然存在一些已知问题和限制<!--[已知问题和限制](https://gitlab.com/groups/gitlab-org/-/epics/4397)-->。

<!--
## OS Versions that are no longer supported

GitLab provides omnibus packages for operating systems only until their
EOL (End-Of-Life). After the EOL date of the OS, GitLab will stop releasing
official packages. The list of deprecated operating systems and the final GitLab
release for them can be found below:

| OS Version      | End Of Life                                                                        | Last supported GitLab version                                                                                                                                                                                                      |
| --------------- | ---------------------------------------------------------------------------------- | -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Raspbian Wheezy | [May 2015](https://downloads.raspberrypi.org/raspbian/images/raspbian-2015-05-07/) | [GitLab CE](https://packages.gitlab.com/app/gitlab/raspberry-pi2/search?q=gitlab-ce_8.17&dist=debian%2Fwheezy) 8.17                                                                                                                |
| OpenSUSE 13.2   | [January 2017](https://en.opensuse.org/Lifetime#Discontinued_distributions)        | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=gitlab-ce-9.1&dist=opensuse%2F13.2) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=gitlab-ee-9.1&dist=opensuse%2F13.2) 9.1          |
| Ubuntu 12.04    | [April 2017](https://ubuntu.com/info/release-end-of-life)                          | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=gitlab-ce_9.1&dist=ubuntu%2Fprecise) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=gitlab-ee_9.1&dist=ubuntu%2Fprecise) 9.1        |
| OpenSUSE 42.1   | [May 2017](https://en.opensuse.org/Lifetime#Discontinued_distributions)            | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=gitlab-ce-9.3&dist=opensuse%2F42.1) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=gitlab-ee-9.3&dist=opensuse%2F42.1) 9.3          |
| OpenSUSE 42.2   | [January 2018](https://en.opensuse.org/Lifetime#Discontinued_distributions)        | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=gitlab-ce-10.4&dist=opensuse%2F42.2) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=gitlab-ee-10.4&dist=opensuse%2F42.2) 10.4       |
| Debian Wheezy   | [May 2018](https://www.debian.org/News/2018/20180601)                              | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=gitlab-ce_11.6&dist=debian%2Fwheezy) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=gitlab-ee_11.6&dist=debian%2Fwheezy) 11.6       |
| Raspbian Jessie | [May 2017](https://downloads.raspberrypi.org/raspbian/images/raspbian-2017-07-05/) | [GitLab CE](https://packages.gitlab.com/app/gitlab/raspberry-pi2/search?q=gitlab-ce_11.7&dist=debian%2Fjessie) 11.7                                                                                                                |
| Ubuntu 14.04    | [April 2019](https://ubuntu.com/info/release-end-of-life)                          | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=gitlab-ce_11.10&dist=ubuntu%2Ftrusty) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=gitlab-ee_11.10&dist=ubuntu%2Ftrusty) 11.10    |
| OpenSUSE 42.3   | [July 2019](https://en.opensuse.org/Lifetime#Discontinued_distributions)           | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=gitlab-ce-12.1&dist=opensuse%2F42.3) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=gitlab-ee-12.1&dist=opensuse%2F42.3) 12.1       |
| OpenSUSE 15.0   | [December 2019](https://en.opensuse.org/Lifetime#Discontinued_distributions)       | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=gitlab-ce-12.5&dist=opensuse%2F15.0) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=gitlab-ee-12.5&dist=opensuse%2F15.0) 12.5       |
| Raspbian Stretch | [June 2020](https://downloads.raspberrypi.org/raspbian/images/raspbian-2019-04-09/) | [GitLab CE](https://packages.gitlab.com/app/gitlab/raspberry-pi2/search?q=gitlab-ce_13.3&dist=raspbian%2Fstretch) 13.3                                                                                                           |
| Debian Jessie   | [June 2020](https://www.debian.org/News/2020/20200709)                             | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=gitlab-ce_13.2&dist=debian%2Fjessie) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=gitlab-ee_13.2&dist=debian%2Fjessie) 13.3       |
| CentOS 6        | [November 2020](https://wiki.centos.org/About/Product)                             | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=13.6&filter=all&filter=all&dist=el%2F6) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=13.6&filter=all&filter=all&dist=el%2F6) 13.6 |
| CentOS 8        | [December 2021](https://wiki.centos.org/About/Product)                             | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=14.6&filter=all&filter=all&dist=el%2F8) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=14.6&filter=all&filter=all&dist=el%2F8) 14.6   |
| OpenSUSE 15.1   | [November 2020](https://en.opensuse.org/Lifetime#Discontinued_distributions)       | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=gitlab-ce-13.12&dist=opensuse%2F15.1) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=gitlab-ee-13.12&dist=opensuse%2F15.1) 13.12    |
| Ubuntu 16.04    | [April 2021](https://ubuntu.com/info/release-end-of-life)                          | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=gitlab-ce_13.12&dist=ubuntu%2Fxenial) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=gitlab-ee_13.12&dist=ubuntu%2Fxenial) 13.12    |
| OpenSUSE 15.2   | [December 2021](https://en.opensuse.org/Lifetime#Discontinued_distributions)       | [GitLab CE](https://packages.gitlab.com/app/gitlab/gitlab-ce/search?q=gitlab-ce-14.7&dist=opensuse%2F15.2) / [GitLab EE](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=gitlab-ee-14.7&dist=opensuse%2F15.2) 14.7       |

NOTE:
An exception to this deprecation policy is when we are unable to provide
packages for the next version of the operating system. The most common reason
for this our package repository provider, PackageCloud, not supporting newer
versions and hence we can't upload packages to it.
-->
