---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# 软件包默认值

除非在 `/etc/gitlab/gitlab.rb` 文件中指定配置，软件包将采用如下所述的默认值。

## 端口

有关 Linux 软件包默认分配的端口列表，请参阅下表：

| 组件名称             | 是否默认启用  | 通信方式 | 替代方式 | 通信端口                            |
| :--------------: | :------------: | :--------------: | :---------: | :------------------------------------:     |
| GitLab Rails        | Yes            | Port             | X           | 80 或 443                                  |
| GitLab Shell        | Yes            | Port             | X           | 22                                         |
| PostgreSQL          | Yes            | Socket           | Port (5432) | X                                          |
| Redis               | Yes            | Socket           | Port (6379) | X                                          |
| Puma                | Yes            | Socket           | Port (8080) | X                                          |
| GitLab Workhorse    | Yes            | Socket           | Port (8181) | X                                          |
| NGINX status        | Yes            | Port             | X           | 8060                                       |
| Prometheus          | Yes            | Port             | X           | 9090                                       |
| Node exporter       | Yes            | Port             | X           | 9100                                       |
| Redis exporter      | Yes            | Port             | X           | 9121                                       |
| PostgreSQL exporter | Yes            | Port             | X           | 9187                                       |
| PgBouncer exporter  | No             | Port             | X           | 9188                                       |
| GitLab Exporter     | Yes            | Port             | X           | 9168                                       |
| Sidekiq exporter    | Yes            | Port             | X           | 8082                                       |
| Sidekiq health check |      Yes      |       Port       |      X      |                    8092[^Sidekiq-health]   |
| Web exporter       | No             | Port             | X           | 8083                                       |
| Geo PostgreSQL      | No             | Socket           | Port (5431) | X                                          |
| Redis Sentinel      | No             | Port             | X           | 26379                                      |
| Incoming email      | No             | Port             | X           | 143                                        |
| Elastic search      | No             | Port             | X           | 9200                                       |
| GitLab Pages        | No             | Port             | X           | 80 或 443                                  |
| GitLab Registry     | No*            | Port             | X           | 80, 443 或 5050                            |
| GitLab Registry     | No             | Port             | X           | 5000                                       |
| LDAP                | No             | Port             | X           | 取决于组件配置     |
| Kerberos            | No             | Port             | X           | 8443 或 8088                               |
| OmniAuth            | Yes            | Port             | X           | 取决于组件配置     |
| SMTP                | No             | Port             | X           | 465                                        |
| Remote syslog       | No             | Port             | X           | 514                                        |
| Mattermost          | No             | Port             | X           | 8065                                       |
| Mattermost          | No             | Port             | X           | 80 或 443                                  |
| PgBouncer           | No             | Port             | X           | 6432                                       |
| Consul              | No             | Port             | X           | 8300、8301(TCP 和 UDP)、8500、8600[^Consul-notes] |
| Patroni             | No             | Port             | X           | 8008                                       |
| GitLab KAS          | YES             | Port             | X           | 8150                                       |
| Gitaly              | No             | Port             | X           | 8075                                       |
| GitLab Workhorse exporter |      Yes      |       Port       |      X      |                     9229                   |
|  Registry exporter   |      No       |       Port       |      X      |                     5001                   |

<!--
说明：

- `组件` - 组件名称。
- `On by default` - Is the component running by default.
- `Communicates via` - How the component talks with the other components.
- `Alternative` - If it is possible to configure the component to use different type of communication. The type is listed with default port used in that case.
- `Connection port` - Port on which the component communicates.
-->

极狐GitLab 还要求文件系统准备好用于存储 Git 仓库和各种其它文件。

请注意，如果您使用 NFS（网络文件系统），文件将通过网络传输，根据实现，该网络需要打开端口 `111` 和 `2049`。

NOTE:
在某些情况下，容器镜像库会默认自动启用。了解更多详情，请参阅[我们的文档](https://docs.gitlab.cn/jh/administration/packages/container_registry.html)。

如果使用额外的 Consul 功能，可能需要打开更多的端口。列表见[官方文档](https://developer.hashicorp.com/consul/docs/install/ports#ports-table)。

 [^Consul-notes]：如果使用额外的 Consul 功能，可能需要打开更多端口。有关列表，请参阅[官方文档](https://developer.hashicorp.com/consul/docs/install/ports#ports-table)。

 [^Sidekiq-health]：如果未设置 Sidekiq 健康检查，则默认为 Sidekiq 指标导出器设置。此默认值已弃用，并在 15.0 版本中删除。
