---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# Linux 软件包和镜像

本文介绍为什么提供软件包和一个带有捆绑依赖项的 Docker 镜像的基本信息。

这些方法非常适合物理和虚拟机安装，以及简单的 Docker 安装。

## 目标

软件包有几个核心目标：

1. 极易安装、升级、维护
1. 支持多种操作系统
1. 云服务商的广泛支持

## Linux 软件包架构

极狐GitLab 的核心是一个 Ruby on Rails 项目。但是，极狐GitLab 作为一个整体应用程序更为复杂，并且具有多个组件。如果这些组件不存在或配置不正确，极狐GitLab 将无法工作或无法预测。

<!--[GitLab 架构概述](https://docs.gitlab.cn/jh/development/architecture.html#gitlab-architecture-overview)展示了其中一些组件以及它们如何交互。这些组件中的每一个都需要配置并保持最新。-->

大多数组件还具有外部依赖项。例如，Rails 应用程序依赖于许多 Ruby gems。其中一些依赖项也有自己的外部依赖项，这些依赖项需要存在于操作系统上才能正常运行。

此外，极狐GitLab 有一个每月发布周期，需要经常维护以保持最新状态。

上面列出的所有内容都给维护 GitLab 安装的用户带来了挑战。

## 外部软件依赖

对于 GitLab 等应用，外部依赖通常会带来以下挑战：

- 在直接和间接依赖项之间保持版本同步
- 特定操作系统上版本的可用性
- 版本更改可以引入或删除以前使用的配置
- 安全隐患：库被标记为有漏洞但尚未发布新版本

请记住，如果您的操作系统存在依赖性，则它不一定存在于其他受支持的操作系统上。

## 优点

软件包捆绑依赖项的一些优点：

1. 安装 GitLab 所需的工作量最小。
1. 启动和运行 GitLab 所需的最低配置。
1. 在 GitLab 版本之间升级所需的最小工作量。
1. 支持多平台。
1. 老平台维护大大简化。
1. 减少支持潜在问题的努力。

## 缺点

软件包捆绑依赖项的一些缺点：

1. 与可能存在的软件重复。
1. 配置灵活性较低。

## 当我可以使用系统包时，为什么要安装Linux 软件包？

答案可以简化为：更少的维护。如果版本不兼容，则不要处理*可能*破坏现有功能的多个包，而是只处理一个。

多个包需要在多个位置正确配置。保持配置同步很容易出错。

如果您具备维护所有当前依赖项的技能，并有足够的时间来处理可能引入的任何未来依赖项，那么上面列出的原因可能不足以让您不使用 Linux 软件包。

在这样做之前，有两件事要记住：

1. 由于使用未经大多数用户测试的库版本时存在多种可能性，因此获得对遇到的任何问题的支持可能会更加困难。
1. 如果您需要独立运行组件，Linux 软件包还允许关闭您不需要的任何服务。例如，您可以将[非捆绑 PostgreSQL 数据库](https://docs.gitlab.cn/omnibus/settings/database.md#using-a-non-packaged-postgresql-database-management-server)与 Linux 软件包一起使用。

请记住，当应用程序具有多个可移动部分时，像 Omnibus 包这样的非标准解决方案可能更适合。

<!--
## 具有多个服务的 Docker 镜像

[GitLab Docker 镜像](https://docs.gitlab.cn/jh/install/docker.html#gitlab-docker-镜像) 基于 omnibus 包。

考虑到从这个镜像生成的容器包含多个进程，这些类型的容器也被称为“胖容器”。

有理由支持和反对这样的镜像，但它们与上面提到的相似：

1. 上手非常简单。
1. 升级到最新版本极其简单。
1. 在多个容器中运行单独的服务并保持它们运行可能会更复杂，并且对于给定的安装可能不需要。

这种方法对于可能还没有准备好进行更复杂的安装，且刚开始使用容器和调度程序的组织很有用。这种方法很适用于较小的组织。
-->