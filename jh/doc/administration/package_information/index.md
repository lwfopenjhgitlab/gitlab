---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# 软件包信息

Linux 软件包与极狐GitLab 正常运行所需的所有依赖项捆绑在一起。更多细节可以在[捆绑依赖文件](omnibus_packages.md)中找到。

## 软件包版本

已发布的软件包版本格式为 `MAJOR.MINOR.PATCH-EDITION.OMNIBUS_RELEASE`

| 组成         | 含义 | 示例 |
|:---------|:-------|:-------|
| `MAJOR.MINOR.PATCH` | 对应的极狐GitLab 版本 | 13.3.0 |
| `EDITION`          | 版本类型标记 | jh |
| `OMNIBUS_RELEASE`   | Linux 软件包版本，通常为 0。如果我们需要在不更改极狐GitLab 版本的情况下构建新包，将增加。 | 0 |

<!--
## 许可

查看[许可文档](licensing.md)。
-->

## 默认值

Linux 软件包需要各种配置才能使组件处于工作状态。如果未提供配置，则使用包中假定的默认值。

这些默认值在软件包[默认值文档](defaults.md)中注明。

## 检查捆绑软件的版本

一旦安装了 Linux 软件包，您可以在 `/opt/gitlab/version-manifest.txt` 中找到极狐GitLab 和所有捆绑库的版本。

如果没有安装这个包，您可以查看 Linux 软件包[源代码库](https://jihulab.com/gitlab-cn/omnibus-gitlab/tree/master)，特别是 [config 目录](https://jihulab.com/gitlab-cn/omnibus-gitlab/tree/master/config)。

<!--
For example, if you take a look at the `8-6-stable` branch, you can conclude that
8.6 packages were running [Ruby 2.1.8](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/8-6-stable/config/projects/gitlab.rb#L48).
Or, that 8.5 packages were bundled with [NGINX 1.9.0](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/8-5-stable/config/software/nginx.rb#L20).
-->

<!--
## Signatures of GitLab, Inc. provided packages

Documentation on package signatures can be found at [Signed Packages](signed_packages.md)
-->

## 在升级时检查更新的配置选项

`/etc/gitlab/gitlab.rb` 中的配置文件是在 Omnibus GitLab 包的初始安装时创建的。在后续包升级时，配置文件不会更新为新配置。这样做是为了避免意外覆盖 `/etc/gitlab/gitlab.rb` 中提供的用户配置。

新的配置选项在 [`gitlab.rb.template` 文件](https://jihulab.com/gitlab-cn/omnibus-gitlab/raw/master/files/gitlab-config-template/gitlab.rb.template)中。

Linux 软件包还提供了方便的命令，可以将现有用户配置与包中包含的模板的最新版本进行比较。

要查看配置文件和最新版本之间的差异，请运行：

```shell
sudo gitlab-ctl diff-config
```

<!--
_**Note:** This command is available from GitLab 8.17_
-->

**重要：** 如果您将此命令的输出复制粘贴到您的 `/etc/gitlab/gitlab.rb` 配置文件中，请确保省略每行的前导 `+` 和 `-`。

## 初始化系统检测

Linux 软件包将尝试查询底层系统以检查它使用的初始化系统。这在 `sudo gitlab-ctl reconfigure` 运行期间表现为 `WARNING`。

根据 init 系统，这个 `WARNING` 可以是以下之一：

```plaintext
/sbin/init: unrecognized option '--version'
```

当底层初始化系统*不是* upstart。

```plaintext
  -.mount loaded active mounted   /
```

当底层初始化系统*是* systemd。

这些警告*可以安全地忽略*。它们不会被抑制，因为这允许每个人更快地调试可能的检测问题。
