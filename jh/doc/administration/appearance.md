---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
disqus_identifier: 'https://docs.gitlab.com/ee/customization/branded_login_page.html'
---

# 极狐GitLab 外观 **(FREE SELF)**

有几个选项可用于自定义极狐GitLab 私有化部署实例的外观：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 外观**。

<a id="navigation-bar"></a>

## 导航栏

默认情况下，导航栏有极狐GitLab Logo，但可以使用所需的任何图像进行自定义。针对 28 像素高（任何宽度）的图像进行了优化，但可以使用任何图像（小于 1 MB）并且会自动调整大小。

选择并上传图像后，单击页面底部的 **更新外观设置**，在极狐GitLab 实例中激活它。

NOTE:
极狐GitLab 流水线电子邮件也显示自定义 logo，除非 logo 是 SVG 格式。如果自定义 logo 是 SVG 格式，则使用默认 logo，因为许多电子邮件客户端不支持 SVG 格式。

## Favicon

默认情况下，favicon（浏览器用作选项卡图标以及 CI 状态图标）使用极狐GitLab Logo，但可以使用所需的任何图标进行自定义。必须使用 32x32 的 `.png` 或 `.ico` 图像。

<a id="system-header-and-footer-messages"></a>

## 系统页眉和页脚消息

您可以向极狐GitLab 实例的界面添加一条页眉消息、一条页脚消息或两者都添加。这些消息出现在实例的所有项目和页面上，包括登录/注册页面。默认颜色是橙色背景上的白色文本，但可以通过单击 **自定义颜色** 进行自定义。

支持有限的[Markdown](../user/markdown.md)，例如粗体、斜体和链接。不支持其它 Markdown 功能，包括列表、图像和引用，因为页眉和页脚消息只能是一行。

如果需要，您可以选择 **在电子邮件中启用页眉和页脚**，将页眉和页脚的文本添加到极狐GitLab 实例发送的所有电子邮件中。

添加消息后，单击页面底部的 **更新外观设置**，在极狐GitLab 实例中激活它。

<a id="sign-in--sign-up-pages"></a>

## 登录/注册页面

您可以将登录/注册页面上的默认消息替换为您自己的消息和 Logo。您可以充分利用描述中的 [Markdown](../user/markdown.md)。

Logo 的最佳尺寸为 640x360 像素，但可以使用任何图像（小于 1MB）并且会自动调整大小。Logo 图像出现在注册页面左侧的标题和说明之间。

添加消息后，单击页面底部的 **更新外观设置**，在极狐GitLab 实例中激活它。您也可以点击 **登录页面** 按钮，查看保存的外观设置：

NOTE:
您还可以在登录消息下方添加自定义帮助消息<!--[自定义帮助消息](settings/help_page.md)-->，或添加[登录文本信息](settings/sign_in_restrictions.md#sign-in-information)。

## 渐进式 Web 应用程序

> 引入于 15.9 版本。

极狐GitLab 可以安装为[渐进式 Web 应用程序](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps)（PWA）。
使用渐进式 Web 应用程序设置自定义其外观，包括其名称、描述和图标。

### 配置 PWA 设置

要配置 PWA 设置：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 外观**。
1. 滚动到 **渐进式 Web 应用程序（PWA）** 部分。
1. 填写字段。
    - **Icon**: 如果您使用标准的极狐GitLab 图标，它的尺寸有 192x192 像素、512x512 像素，也可以作为可隐藏图标。如果您使用自定义图标，则其尺寸必须为 192x192 像素或 512x512 像素。
1. 选择 **更新外观设置**。

<a id="new-project-pages"></a>

## 新建项目页面

您可以在极狐GitLab 中的 **新建项目页面** 中添加新的项目指南消息。
您可以在描述中充分利用 [Markdown](../user/markdown.md)：

该消息显示在 **新建项目** 消息下方，位于 **新建项目页面** 的左侧。

添加消息后，单击页面底部的 **更新外观设置**，在极狐GitLab 实例中激活它。您还可以单击 **新建项目页面** 按钮，将您带到新建项目页面，以便您查看更改。

## Libravatar

支持 [Libravatar](https://www.libravatar.org) 用于头像图像，但您必须[在极狐GitLab 实例上手动启用 Libravatar 支持](../administration/libravatar.md)，以便使用该服务。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
