---
stage: Enablement
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Geo 站点管理中心 **(PREMIUM SELF)**

您可以为极狐GitLab Geo 站点配置各种设置。<!--For more information, see
[Geo documentation](../../administration/geo/index.md).-->

在主站点或次要站点上：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **Geo > 站点**。

## 常用设置

所有 Geo 站点都具有以下设置：

| 设置 | 描述 |
| --------| ----------- |
| Primary | 将 Geo 站点标记为**主要**站点。只能有一个**主要**站点。 |
| Name    | Geo 站点的唯一标识符。强烈建议使用物理位置作为名称。例如 "London Office" 或 “us-east-1”。避免使用“primary”、“secondary”、 “Geo” 或 “DR” 之类的词。 这使得故障转移过程更容易，因为物理位置不会改变，但 Geo 站点角色可以。单个 Geo 站点中的所有节点都使用相同的站点名称。节点使用 `/etc/gitlab/gitlab.rb` 中的 `gitlab_rails['geo_node_name']` 设置，在 PostgreSQL 数据库中查找其 Geo 站点记录。如果 `gitlab_rails['geo_node_name']` 未设置，则使用带有斜杠的节点的 `external_url` 作为后备。`Name` 的值区分大小写，并且允许使用大多数字符。 |
| URL     | 实例面向用户的 URL。 |

您当前正在浏览的站点以蓝色的 `当前` 标记，并且**主要**节点首先列出为 `主站点`。

## 次要站点设置

**次要**站点有许多可用的附加设置：

| 设置                   | 描述 |
|---------------------------|-------------|
| Selective synchronization | 为此**次要**站点启用 Geo 选择性同步<!--[选择性同步](../../administration/geo/replication/configuration.md#selective-synchronization)-->。 |
| Repository sync capacity  | 在回填仓库时，此**次要**站点向**主要**站点发出的并发请求数。 |
| File sync capacity        | 回填文件时，此**次要**站点向**主要**站点发出的并发请求数。 |

## Geo 回填

**次要**站点由**主要**站点通知仓库和文件的更改，并始终尝试尽快同步这些更改。

回填是用仓库和文件填充**次要**站点的行为，这些仓库和文件在**次要**站点添加到数据库*之前*存在。因为可能有大量的仓库和文件，尝试一次下载它们是不可行的；因此，系统对这些操作的并发性设置了上限。

回填需要多长时间取决于最大并发性，但较高的值会对**主要**站点造成更大的压力。限制是可配置的。
如果您的**主要**站点有大量剩余容量，您可以增加这些值，在更短的时间内完成回填。如果负载过重并且回填会降低其对正常请求的可用性，则可以减少它们。

## 设置内部 URL

> 在次要站点中设置内部 URL，引入于 14.7 版本。

您可以为主站点和次要站点之间的同步设置不同的 URL。

**次要**站点使用**主要**站点的内部 URL 来与其通信（例如，同步仓库）。名称内部 URL 将其与用户使用的外部 URL 区分开来。内部 URL 不必是私有地址。

启用 <!--[Geo 辅助代理](../../administration/geo/secondary_proxy/index.md)-->Geo 辅助代理时，主站点使用次要站点的内部 URL 直接与其通信。

内部 URL 默认为外部 URL。要更改它：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **Geo > 站点**。
1. 在您要自定义的站点上选择 **编辑**。
1. 编辑内部 URL。
1. 选择 **保存修改**。

启用后，Geo 的管理中心会直接从主站点的 UI 显示每个站点的复制详细信息，并通过 Geo 辅助代理（如果启用）。

WARNING:
我们建议在配置 Geo 站点时使用 HTTPS 连接。为避免在使用 HTTPS 时中断**主要**和**次要**站点之间的通信，请自定义内部 URL 指向负载均衡器，并在负载均衡器处终止 TLS。

WARNING:
从 13.3 到 13.11 版本，如果您使用用户无法访问的内部 URL，OAuth 授权流程将无法正常工作，因为用户被重定向到内部 URL 而不是外部 URL。

## 负载均衡器后面的多个次要站点

如果为每个 Geo 站点设置了唯一的 `name`，**次要**站点可以使用相同的外部 URL。`gitlab.rb` 中的 `gitlab_rails['geo_node_name']` 设置必须：

- 为每个运行 `puma`、`sidekiq` 或 `geo_logcursor` 的实例设置。
- 匹配 Geo 站点名称。

负载均衡器必须使用粘性会话来避免身份验证失败和跨站点请求错误。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
