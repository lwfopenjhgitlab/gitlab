---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 性能栏 **(FREE SELF)**

> - **Stats** 字段引入于 13.9 版本
> - **Memory** 字段引入于 14.0 版本
> - **Flamegraph** 字段引入于 14.4 版本

您可以显示性能栏，查看 GitLab UI 页面性能的统计信息。
例如：

![Performance bar](img/performance_bar_v14_4.png)

## 可用信息

从左到右，性能栏显示：

- **当前主机**：为页面提供服务的当前主机。
- **数据库查询**：花费的时间（以毫秒为单位）和数据库查询的总数，以 `00ms / 00 (00 cached) pg` 的格式显示。单击显示具有更多详细信息的窗口。您可以使用它来查看每个查询的以下详细信息：
  - **在事务中**：如果查询是在事务上下文中执行的，则显示在查询下方
  - **角色**：启用数据库负载均衡<!--[数据库负载均衡](../../postgresql/database_load_balancing.md)-->时显示。它显示用于查询的服务器角色。“Primary”表示查询已发送到读/写主服务器。“Replica”意味着它被发送到只读副本。
  - **配置名称**：用于区分为不同极狐GitLab 功能配置的不同数据库。显示的名称与用于在极狐GitLab 中配置数据库连接的名称相同。
- **Gitaly 调用**：花费的时间（以毫秒为单位）和 Gitaly 调用的总数。单击显示具有更多详细信息的窗口。
- **Rugged 调用**：花费的时间（以毫秒为单位）和 Rugged 调用的总数。单击显示具有更多详细信息的窗口。
- **Redis 调用**：花费的时间（以毫秒为单位）和 Redis 调用的总数。单击显示具有更多详细信息的窗口。
- **Elasticsearch 调用**：花费的时间（以毫秒为单位）和 Elasticsearch 调用的总数。单击显示具有更多详细信息的窗口。
- **外部 HTTP 调用**：花费的时间（以毫秒为单位）和对其它系统的外部调用总数。单击显示具有更多详细信息的窗口。
- 页面的**加载时间**：如果您的浏览器支持加载时间，则显示以毫秒为单位的多个值，以斜线分隔。单击显示具有更多详细信息的窗口。显示的值，从左到右：
  - **后端**：加载基本页面所需的时间。
  - [**首次内容绘制**](https://developer.chrome.com/docs/lighthouse/performance/first-contentful-paint/)：直到某些内容对用户可见之前的时间。如果您的浏览器不支持此功能，则显示 `NaN`。
  - [**DomContentLoaded**](https://web.dev/critical-rendering-path-measure-crp/) 事件。
  - **请求总数**加载的页面。
- **内存**：所选请求期间消耗的内存量和分配的对象。选择它来显示一个包含更多详细信息的窗口。
- **跟踪**：如果集成了 Jaeger，则 **跟踪** 链接到包含当前请求的 `correlation_id` 的 Jaeger 跟踪页面。
- **+**：将请求详细信息添加到性能栏的链接。请求可以通过其完整 URL（作为当前用户进行身份验证）添加，也可以通过其 `X-Request-Id` 标头的值添加。
- **下载**：下载用于生成性能栏报告的原始 JSON 的链接。
- **内存报告**：生成当前 URL 的内存分析报告的链接。
- **火焰图** 模式：使用选定的 [Stackprof 模式] (https://github.com/tmm1/stackprof#sampling) 生成当前 URL 的火焰图的链接：
  - **Wall** 模式在 wall 的时钟上每隔 *interval* 采样一次。间隔设置为 `10100` 微秒。
  - **CPU** 模式对 CPU 活动的每个 *interval* 进行采样。 间隔设置为 `10100` 微秒。
  - **Object** 模式每隔 *interval* 采样一次。间隔设置为 `100` 次分配。
- **Request Selector**：显示在性能栏右侧的选择框，使您能够查看当前页面打开时发出的任何请求的这些指标。仅捕获每个唯一 URL 的前两个请求。
- **Stats**（可选）：如果设置了 `GITLAB_PERFORMANCE_BAR_STATS_URL` 环境变量，此 URL 将显示在栏中。在 13.9 及更高版本中，仅在 SaaS 中使用。

NOTE:
并非所有指标都适用于所有环境。例如，内存视图需要运行带有[特定补丁](https://jihulab.com/gitlab-cn/gitlab-build-images/-/blob/master/patches/ruby/2.7.4/thread-memory-allocations-2.7.patch)的 Ruby。当使用 GDK 在本地运行极狐GitLab 时，通常情况并非如此，并且无法使用内存视图。

## 键盘快捷键

按 [<kbd>p</kbd> + <kbd>b</kbd> 键盘快捷键](../../../user/shortcuts.md) 显示性能栏，再次按隐藏。

要为非管理员显示性能栏，必须先为他们启用。

## 请求警告

> 请求选择器中的警告图标删除于 14.9 版本

超出预定义限制的请求会在指标旁边显示警告 **{warning}** 图标和说明。在此示例中，Gitaly 调用持续时间超过了阈值。

![Gitaly call duration exceeded threshold](img/performance_bar_gitaly_threshold.png)

## 为非管理员启用性能栏

非管理员默认禁用性能栏。要为给定群组启用：

1. 以具有管理员权限的用户身份登录。
1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 指标与分析**，然后展开 **分析 - 性能栏**。
1. 单击 **允许非管理员访问性能栏**。
1. 在 **允许访问以下群组的成员** 字段中，提供允许访问性能栏的群组的完整路径。
1. 单击 **保存修改**。
