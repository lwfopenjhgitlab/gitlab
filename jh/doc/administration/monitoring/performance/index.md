---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab 性能监控 **(FREE SELF)**

极狐GitLab 带有自己的应用程序性能测量系统。

建议您阅读以下文档，了解如何正确配置极狐GitLab 性能监控：


- [Prometheus 文档](../prometheus/index.md)
- [Grafana 安装/配置](grafana_configuration.md)
- [性能栏](performance_bar.md)

<!--
- [极狐GitLab 配置](gitlab_configuration.md)
- [请求分析](request_profiling.md)
-->

## 极狐GitLab 性能监控简介

极狐GitLab 性能监控使测量各种统计数据成为可能，包括（但不限于）：

- 完成事务（网络请求或 Sidekiq 作业）所花费的时间。
- 运行 SQL 查询和渲染 HAML 视图所花费的时间。
- 执行（检测）Ruby 方法所花费的时间。
- Ruby 对象分配，尤其是保留对象。
- 系统统计信息，例如进程的内存使用情况和打开的文件描述符。
- Ruby 垃圾收集统计信息。

## 指标类型

收集以下两种类型的指标：

1. 事务特定指标。
1. 抽样指标，在一个单独的线程中以一定的时间间隔收集。

### 事务指标

事务指标是可以与单个事务关联的指标，包括事务持续时间、任何执行的 SQL 查询的时间、渲染 HAML 视图所花费的时间等统计信息。为每个 Rack 请求和处理的 Sidekiq 作业收集这些指标。

### 抽样指标

抽样指标是不能与单个事务关联的指标。
示例包括垃圾收集统计信息和保留的 Ruby 对象，这些指标会定期收集，间隔由两部分组成：

1. 用户定义的间隔。
1. 在区间顶部添加一个随机生成的偏移量，同一个偏移量不能连续使用两次。

实际间隔可以在定义间隔的一半和超过该间隔的一半之间。例如，对于用户定义的 15 秒间隔，实际间隔可以是 7.5 到 22.5 之间的任何值。每次采样运行都会重新生成间隔，而不是生成一次并在进程的生命周期内重复使用。

用户定义的时间间隔可以通过环境变量来指定。
识别以下环境变量：

- `RUBY_SAMPLER_INTERVAL_SECONDS`
- `DATABASE_SAMPLER_INTERVAL_SECONDS`
- `ACTION_CABLE_SAMPLER_INTERVAL_SECONDS`
- `PUMA_SAMPLER_INTERVAL_SECONDS`
- `THREADS_SAMPLER_INTERVAL_SECONDS`
- `GLOBAL_SEARCH_SAMPLER_INTERVAL_SECONDS`
