---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab 配置 **(FREE SELF)**

极狐GitLab 性能监控默认禁用。要启用它并更改其任何设置：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，选择 **设置 > 指标与分析**。
1. 添加必要的配置更改。
1. 重新启动整个极狐GitLab，使更改生效：

   - Omnibus GitLab 安装实例：`sudo gitlab-ctl restart`
   - 源安装实例：`sudo service gitlab restart`

NOTE:
删除于 13.0 版本，使用 [Prometheus 集成](../prometheus/index.md) 代替。

## 处理中的迁移

当任何迁移处理中时，指标将被禁用，直到迁移执行完为止。

阅读更多：

- [极狐GitLab 性能监控简介](index.md)
- [Grafana 安装/配置](grafana_configuration.md)
