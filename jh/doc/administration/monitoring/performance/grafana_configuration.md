---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Grafana 配置 **(FREE SELF)**

> 废弃于 16.0 版本。

WARNING:
捆绑的 Grafana 废弃于 16.0 版本，且不再受支持。此功能将于 16.3 版本删除。
获取更多信息，请查看[废弃说明](#deprecation-of-bundled-grafana)。

[Grafana](https://grafana.com/) 是一种工具，可让您通过图表和仪表盘可视化时间序列指标。极狐GitLab 将性能数据写入 Prometheus，Grafana 允许您查询数据，来显示有用的图表。

<a id="deprecation-of-bundled-grafana"></a>

## 废弃捆绑的 Grafana

捆绑的 Grafana 是一项可选的 Omnibus GitLab 服务，它为极狐GitLab 指标提供了一个用户界面。

现在不再支持与 Omnibus GitLab 捆绑在一起的 Grafana 版本。如果您使用的是捆绑的 Grafana，则应切换到 [Grafana Labs](https://grafana.com/grafana/) 的更新版本。

### 切换到新的 Grafana 实例

要从捆绑的 Grafana 切换到 Grafana Labs 的更新版本的 Grafana：

1. 从 Grafana Labs 设置一个 Grafana 版本。
1. 从捆绑的 Grafana [导出现有仪表盘](https://grafana.com/docs/grafana/latest/dashboards/manage-dashboards/#export-a-dashboard)。
1. 在新的 Grafana 实例中[导入现有的仪表盘](https://grafana.com/docs/grafana/latest/dashboards/manage-dashboards/#import-a-dashboard)。
1. [配置极狐GitLab](#integration-with-gitlab-ui) 使用新的 Grafana 实例。

### 临时解决方法

在极狐GitLab 16.0 到 16.2 版本中，您仍然可以通过设置以下内容，强制 Omnibus GitLab 启用和配置 Grafana：

- `grafana['enable'] = true`
- `grafana['enable_deprecated_service'] = true`
 
重新配置极狐GitLab 时，您会看到一条关于废弃的消息。

## 安装

Omnibus GitLab 可以[帮助您安装 Grafana（推荐）](https://docs.gitlab.cn/omnibus/settings/grafana.html)，或使用 Grafana 提供的便于安装的包仓库（Yum/Apt）。
有关详细步骤，请参阅 [Grafana 安装文档](https://grafana.com/docs/grafana/latest/setup-grafana/installation/)。

在第一次启动 Grafana 之前，在 `/etc/grafana/grafana.ini` 中设置管理员用户和密码。如果没有，默认密码是`admin`。

## 配置

1. 以管理用户身份登录 Grafana。
1. 从 **Configuration** 菜单中选择 **Data Sources**。
1. 选择 **Add data source** 按钮。
1. 选择所需的数据源类型。例如，[Prometheus](../prometheus/index.md#prometheus-as-a-grafana-data-source)。
1. 填写数据源的详细信息，然后选择 **Save & Test** 按钮。

Grafana 应指示数据源正在工作。

## 导入仪表盘

您现在可以导入一组[默认 Grafana 仪表盘](https://gitlab.com/gitlab-org/grafana-dashboards)来开始显示有用的信息：

1. 克隆仓库，或下载 ZIP 文件，或 tarball。
1. 按照以下步骤分别导入每个 JSON 文件：

    1. 以管理员用户身份登录 Grafana。
    1. 从 **Dashboards** 菜单中选择 **Manage**。
    1. 选择 **Import** 按钮，然后选择 **Upload JSON file** 按钮。
    1. 找到要导入的 JSON 文件并选择 **Choose for Upload**。 选择 **Import** 按钮。
    1. 仪表盘导入后，选择顶部栏中的 **Save dashboard** 图标。

如果您在导入后不保存仪表盘，则当您离开页面时，仪表盘将被删除。对要导入的每个仪表盘重复此过程。

或者，您可以将所有仪表盘导入您的 Grafana 实例。有关此流水线的更多信息，请参阅 [Grafana 仪表板的自述文件](https://gitlab.com/gitlab-org/grafana-dashboards) 仓库。

<a id="integration-with-gitlab-ui"></a>

## 与 GitLab UI 集成

设置 Grafana 后，您可以启用链接，从极狐GitLab 侧边栏中轻松访问它：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 指标与分析** 并展开 **指标 -  Grafana**。
1. 选择 **添加链接到 Grafana** 复选框。
1. 配置 **Grafana URL**：
   - *如果 Grafana 通过 Omnibus GitLab 在同一服务器上启用，*保持 **Grafana URL** 不变，应该是`/-/grafana`。
   - *否则，*输入 Grafana 实例的完整 URL。
1. 选择 **保存修改**。

极狐GitLab 在 **主菜单 > 管理员 > 监控 > 指标仪表盘** 中显示您的链接。

## 所需范围

> 引入于 13.10 版本

通过上述流程设置 Grafana 时，在 **主菜单 > 管理员 > 应用 > GitLab Grafana** 的页面中不会显示任何范围。但是，`read_user` 范围是必需的，并且会自动提供给应用。当您尝试使用极狐GitLab 作为 OAuth provider 登录时，设置除 `read_user` 以外的任何范围而不包括 `read_user` 会导致此错误：

```plaintext
The requested scope is invalid, unknown, or malformed.
```

如果您看到此错误，请确保在 GitLab Grafana 配置页面中满足以下条件之一：

- 没有范围出现。
- 包括 `read_user` 范围。

> 13.10 之前的版本使用 API 范围而不是 `read_user`。在 13.10 之前的 GitLab 版本中，API 范围：
>
> - 需要通过 GitLab OAuth provider 访问 Grafana。
> - 通过启用 Grafana 应用程序进行设置，如[与 GitLab UI 集成文档](#integration-with-gitlab-ui)中所示。

<!--
## Security Update

Users running GitLab version 12.0 or later should immediately upgrade to one of the
following security releases due to a known vulnerability with the embedded Grafana dashboard:

- 12.0.6
- 12.1.6

After upgrading, the Grafana dashboard is disabled, and the location of your
existing Grafana data is changed from `/var/opt/gitlab/grafana/data/` to
`/var/opt/gitlab/grafana/data.bak.#{Date.today}/`.

To prevent the data from being relocated, you can run the following command prior to upgrading:

```shell
echo "0" > /var/opt/gitlab/grafana/CVE_reset_status
```

To reinstate your old data, move it back into its original location:

```shell
sudo mv /var/opt/gitlab/grafana/data.bak.xxxx/ /var/opt/gitlab/grafana/data/
```

However, you should **not** reinstate your old data _except_ under one of the following conditions:

1. If you're certain that you changed your default administration password when you enabled Grafana.
1. If you run GitLab in a private network, accessed only by trusted users, and your
   Grafana login page has not been exposed to the internet.

If you require access to your old Grafana data but don't meet one of these criteria, you may consider:

1. Reinstating it temporarily.
1. [Exporting the dashboards](https://grafana.com/docs/grafana/latest/reference/export_import/#exporting-a-dashboard) you need.
1. Refreshing the data and [re-importing your dashboards](https://grafana.com/docs/grafana/latest/reference/export_import/#importing-a-dashboard).

WARNING:
These actions pose a temporary vulnerability while your old Grafana data is in use.
Deciding to take any of these actions should be weighed carefully with your need to access
existing data and dashboards.

For more information and further mitigation details, please refer to our
[blog post on the security release](https://about.gitlab.com/releases/2019/08/12/critical-security-release-gitlab-12-dot-1-dot-6-released/).

Read more on:

- [Introduction to GitLab Performance Monitoring](index.md)
- [GitLab Configuration](gitlab_configuration.md)
-->
