---
stage: Monitor
group: Monitor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 健康检查 **(FREE SELF)**

极狐GitLab 提供 liveness 和 readiness 探测，指示服务运行状况和所需服务的可达性。这些探测报告数据库连接、Redis 连接和文件系统访问的状态。这些端点[可以提供给像 Kubernetes 这样的调度程序](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/)，保持流量直到系统准备好或根据需要重新启动容器。

## IP 许可名单

要访问监控资源，需要将请求的客户端 IP 包含在白名单中。

<!--
For details, see [how to add IPs to a whitelist for the monitoring endpoints](../../../administration/monitoring/ip_whitelist.md).
-->

## 在本地使用端点

使用默认许可名单设置，可以使用以下 URL 从本地主机访问探针：

```plaintext
GET http://localhost/-/health
```

```plaintext
GET http://localhost/-/readiness
```

```plaintext
GET http://localhost/-/liveness
```

## 健康

检查应用程序服务器是否正在运行。
它不会验证数据库或其他服务是否正在运行。此端点绕过 Rails 控制器，并在请求处理生命周期的早期作为附加中间件 `BasicHealthCheck` 实现。

```plaintext
GET /-/health
```

示例请求：

```shell
curl "https://gitlab.example.com/-/health"
```

示例响应：

```plaintext
GitLab OK
```

## Readiness

Readiness 探针检查极狐GitLab 实例是否准备好通过 Rails 控制器接受流量。默认情况下，检查仅验证实例检查。

如果指定了 `all=1` 参数，该检查还会验证依赖服务（数据库、Redis、Gitaly 等）并给出每个服务的状态。

```plaintext
GET /-/readiness
GET /-/readiness?all=1
```

示例请求：

```shell
curl "https://gitlab.example.com/-/readiness"
```

示例响应：

```json
{
   "master_check":[{
      "status":"failed",
      "message": "unexpected Master check result: false"
   }],
   ...
}
```

失败时，端点返回 `503` HTTP 状态代码。

该检查免于 Rack Attack。

## Liveness

<!--
WARNING:
In GitLab [12.4](https://about.gitlab.com/upcoming-releases/)
the response body of the Liveness check was changed
to match the example below.
-->

检查应用程序服务器是否正在运行。
此探测器用于了解 Rails 控制器是否由于多线程而没有死锁。

```plaintext
GET /-/liveness
```

示例请求：

```shell
curl "https://gitlab.example.com/-/liveness"
```

示例响应：

成功时，端点返回一个 `200` HTTP 状态代码，以及如下所示的响应。

```json
{
   "status": "ok"
}
```

失败时，端点返回 `503` HTTP 状态代码。

该检查免于 Rack Attack。

<!--
## Sidekiq

Learn how to configure the [Sidekiq health checks](../../../administration/sidekiq_health_check.md).
-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
