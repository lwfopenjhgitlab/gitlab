---
stage: Data Stores
group: Memory
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# IP 许可名单 **(FREE SELF)**

极狐GitLab 提供了一些[监控端点](../../user/admin_area/monitoring/health_check.md)，它们在探测时提供健康检查信息。

要通过 IP 许可名单控制对这些端点的访问，您可以添加单个主机或使用 IP 范围：

**对于 Omnibus 安装实例**

1. 打开 `/etc/gitlab/gitlab.rb` 并添加或取消注释以下内容：

   ```ruby
   gitlab_rails['monitoring_whitelist'] = ['127.0.0.0/8', '192.168.0.1']
   ```

1. 保存文件并[重新配置](../restart_gitlab.md#omnibus-gitlab-reconfigure)极狐GitLab，使更改生效。

---

**对于云原生 Helm chart 安装实例**

您可以在 `gitlab.webservice.monitoring.ipWhitelist` 键下设置所需的 IP。例如：

```yaml
gitlab:
   webservice:
      monitoring:
         # Monitoring IP whitelist
         ipWhitelist:
         - 0.0.0.0/0 # Default
```

---

**对于源安装实例**

1. 编辑 `config/gitlab.yml`：

   ```yaml
   monitoring:
     # by default only local IPs are allowed to access monitoring resources
     ip_whitelist:
       - 127.0.0.0/8
       - 192.168.0.1
   ```

1. 保存文件并[重启](../restart_gitlab.md#源安装实例)极狐GitLab，使更改生效。
