---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 监控极狐GitLab **(FREE SELF)**

浏览监控您的极狐GitLab 实例的功能：

- [性能监控](performance/index.md)：极狐GitLab 性能监控使测量实例的各种统计数据成为可能。
- [Prometheus](prometheus/index.md)：Prometheus 是一个强大的时间序列监控服务，为监控极狐GitLab 和其它软件产品提供了一个灵活的平台。
- [GitHub 导入](github_imports.md)：使用各种 Prometheus 指标监控 GitHub 导入器的运行状况和进度。
- [监控正常运行时间](../../user/admin_area/monitoring/health_check.md): 使用健康检查端点，检查服务器状态。
  - [IP 许可名单](ip_allowlist.md)：配置极狐GitLab 监视在探测时提供健康检查信息的端点。
- [`nginx_status`](https://docs.gitlab.cn/omnibus/settings/nginx.html#启用禁用-nginx_status)：监控您的 NGINX 服务器状态。
