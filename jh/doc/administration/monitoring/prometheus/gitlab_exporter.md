---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# GitLab exporter **(FREE SELF)**

<!--
> Renamed from `GitLab monitor exporter` to `GitLab exporter` in [GitLab 12.3](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/16511).
-->

[GitLab exporter](https://gitlab.com/gitlab-org/gitlab-exporter) 使您能够测量从 Redis 和 Omnibus GitLab 安装实例中的数据库提取的各种极狐GitLab 指标。

对于从源安装实例，您必须自己安装和配置它。

在 Omnibus GitLab 安装实例中启用 GitLab exporter：

1. [启用 Prometheus](index.md#configuring-prometheus)。
1. 编辑 `/etc/gitlab/gitlab.rb`。
1. 添加或查找并取消注释以下行，确保将其设置为 `true`：

   ```ruby
   gitlab_exporter['enable'] = true
   ```

1. 保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

Prometheus 自动开始从暴露在 `localhost:9168` 的 GitLab exporter 收集性能数据。

## 使用不同的 Rack 服务器

> - 引入于 Omnibus GitLab 13.8
> - WEBrick 现在是默认的 Rack 服务器，而不是 Puma。

默认情况下，GitLab exporter 在单线程 Ruby Web 服务器 [WEBrick](https://github.com/ruby/webrick) 上运行。
您可以选择更符合您的性能需求的不同 Rack 服务器。
例如，在包含大量 Prometheus 抓取工具但只有少数监控节点的多节点设置中，您可能会决定运行多线程服务器，例如 Puma。

要将 Rack 服务器更改为 Puma：

1. 编辑 `/etc/gitlab/gitlab.rb`。
1. 添加或查找并取消注释以下行，并将其设置为 `puma`：

   ```ruby
   gitlab_exporter['server_name'] = 'puma'
   ```

1. 保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

支持的 Rack 服务器是 `webrick` 和 `puma`。
