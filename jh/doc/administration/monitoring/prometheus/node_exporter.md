---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Node exporter **(FREE SELF)**

[Node exporter](https://github.com/prometheus/node_exporter) 使您能够测量各种机器资源，例如内存、磁盘和 CPU 利用率。

对于从源安装实例，您必须自己安装和配置它。

要启用 Node exporter：

1. [启用 Prometheus](index.md#configuring-prometheus)。
1. 编辑 `/etc/gitlab/gitlab.rb`。
1. 添加（或查找并取消注释）以下行，确保将其设置为 `true`：

   ```ruby
   node_exporter['enable'] = true
   ```

1. 保存文件，然后[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

Prometheus 开始从暴露在 `localhost:9100` 的 Node exporter 收集性能数据。
