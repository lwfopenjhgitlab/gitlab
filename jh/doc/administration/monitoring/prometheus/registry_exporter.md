---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Registry exporter **(FREE SELF)**

Registry exporter 允许您测量各种 registry 指标。
要启用它：

1. [启用 Prometheus](index.md#configuring-prometheus)。
1. 编辑 `/etc/gitlab/gitlab.rb` 并为 Registry 启用 [debug 模式](https://docs.docker.com/registry/#debug)：

   ```ruby
   registry['debug_addr'] = "localhost:5001"  # localhost:5001/metrics
   ```

1. 保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

Prometheus 自动开始从 `localhost:5001/metrics` 下暴露的 Registry exporter 收集性能数据。

[← 返回 Prometheus 主页面](index.md)
