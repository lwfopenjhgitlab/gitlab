---
stage: Data Stores
group: Memory
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Web exporter（专用指标服务器） **(FREE SELF)**

当[使用 Prometheus 监控极狐GitLab](index.md) 时，极狐GitLab 会运行各种收集器，对应用程序进行采样，获取与使用、负载和性能相关的数据。然后，可以通过运行一个或多个 Prometheus exporter 将这些数据提供给 Prometheus 抓取工具。
Prometheus exporter 是一个 HTTP 服务器，它将度量数据序列化为 Prometheus 抓取工具可以理解的格式。

NOTE:
此页面是关于 Web 应用程序指标的。
<!--要导出后台作业指标，请了解如何[配置 Sidekiq 指标服务器](../../sidekiq.md#configure-the-sidekiq-metrics-server)。-->

我们提供了两种可以导出 Web 应用程序指标的机制：

- 通过主要的 Rails 应用程序。我们使用的应用服务器 [Puma](../../operations/puma.md) 通过其自己的 `/-/metrics` 端点提供指标数据。这是默认设置，在[极狐GitLab 指标](index.md#gitlab-metrics)中有描述。对于收集的指标数量很少的小型安装实例，我们建议使用此默认设置。
- 通过专用的指标服务器。启用此服务器将导致 Puma 启动一个额外的进程，该进程的唯一职责是提供指标。这种方法可以为非常大的安装实例带来更好的故障隔离和性能，但会带来额外的内存使用。我们建议将这种方法用于寻求高性能和可用性的大中型安装实例。

专用服务器和 Rails `/-/metrics` 端点都提供相同的数据，因此它们在功能上是等效的，只是它们的性能特征不同。

要启用专用服务器：

1. [启用 Prometheus](index.md#configuring-prometheus)。
1. 编辑 `/etc/gitlab/gitlab.rb`，添加（或查找并取消注释）以下行。确保 `puma['exporter_enabled']` 设置为 `true`：

   ```ruby
   puma['exporter_enabled'] = true
   puma['exporter_address'] = "127.0.0.1"
   puma['exporter_port'] = 8083
   ```

1. 当使用极狐GitLab 捆绑的 Prometheus 时，确保它的 `scrape_config` 指向 `localhost:8083/metrics`。有关如何配置抓取目标的信息，请参阅[添加自定义抓取配置](index.md#adding-custom-scrape-configurations)页面。对于外部 Prometheus 设置，请参阅[使用外部 Prometheus 服务器](index.md#using-an-external-prometheus-server)。
1. 保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

现在可以从 `localhost:8083/metrics` 提供和抓取指标。

## 启用 HTTPS

> 引入于 15.2 版本。

要通过 HTTPS 而不是 HTTP 提供指标，请在导出器设置中启用 TLS：

1. 编辑 `/etc/gitlab/gitlab.rb`，添加（或查找并取消注释）以下行：

   ```ruby
   puma['exporter_tls_enabled'] = true
   puma['exporter_tls_cert_path'] = "/path/to/certificate.pem"
   puma['exporter_tls_key_path'] = "/path/to/private-key.pem"
   ```

1. 保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

启用 TLS 后，将使用与上述相同的 `port` 和 `address`。
指标服务器不能同时提供 HTTP 和 HTTPS。
