---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# PgBouncer exporter **(FREE SELF)**

[PgBouncer exporter](https://github.com/prometheus-community/pgbouncer_exporter) 使您能够测量各种 [PgBouncer](https://www.pgbouncer.org/) 指标。

对于从源安装实例，您必须自己安装和配置它。

要启用 PgBouncer exporter：

1. [启用 Prometheus](index.md#configuring-prometheus)。
1. 编辑 `/etc/gitlab/gitlab.rb`。
1. 添加（或查找并取消注释）以下行，确保将其设置为 `true`：

   ```ruby
   pgbouncer_exporter['enable'] = true
   ```

1. 保存文件，然后[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

Prometheus 开始从暴露在 `localhost:9188` 的 PgBouncer 导出器收集性能数据。

如果启用了 [`pgbouncer_role`](https://docs.gitlab.cn/omnibus/roles/#postgresql-roles) 角色，则默认启用 PgBouncer exporter。
