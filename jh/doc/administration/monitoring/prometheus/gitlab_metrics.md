---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

<a id="gitlab-prometheus-metrics"></a>

# 极狐GitLab Prometheus 指标 **(FREE SELF)**

要启用极狐GitLab Prometheus 指标：

1. 以具有管理员权限的用户身份登录 GitLab。
1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 指标与分析**。
1. 找到 **指标 - Prometheus** 部分，然后选择 **启用 Prometheus 指标端点**。
1. [重启极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-restart)，使更改生效。

对于源安装实例，您必须自己配置它。

## 收集指标

极狐GitLab 监控自己的内部服务指标，并在 `/-/metrics` 端点提供这些指标。与其它 [Prometheus](https://prometheus.io) exporter 不同，要访问指标，客户端 IP 地址必须是[明确允许的](../ip_allowlist.md)。

这些指标是为 [Omnibus GitLab](https://docs.gitlab.cn/omnibus/) 和 Chart 安装实例启用和收集的。对于源安装实例，这些指标必须手动启用并由 Prometheus 服务器收集。

要从 Sidekiq 节点启用和查看指标，请参阅 [Sidekiq 指标](#sidekiq-metrics)。

## 可用指标

以下指标可用：

| 指标                                                           | 类型        | 引入版本  | 描述                                                                                                           | 标记                                                    |
| :--------------------------------------------------------------- | :---------- | ------: | :-------------------------------------------------------------------------------------------------------------------- | :-------------------------------------------------------- |
| `gitlab_banzai_cached_render_real_duration_seconds`              | Histogram   | 9.4     | 存在缓存输出时将 Markdown 呈现为 HTML 的持续时间                                                    | `controller`, `action`                                    |
| `gitlab_banzai_cacheless_render_real_duration_seconds`           | Histogram   | 9.4     | 缓存输出不存在时将 Markdown 渲染为 HTML 的持续时间                                            | `controller`, `action`                                    |
| `gitlab_cache_misses_total`                                      | Counter     | 10.2    | 缓存读取 miss                                                                                                       | `controller`, `action`                                    |
| `gitlab_cache_operation_duration_seconds`                        | Histogram   | 10.2    | 缓存访问时间                                                                                                     |                                                           |
| `gitlab_cache_operations_total`                                  | Counter     | 12.2    | 控制器或动作的缓存操作                                                                              | `controller`, `action`, `operation`                       |
| `gitlab_cache_read_multikey_count`                               | Histogram   | 15.7    | 多键缓存读取操作中的键数量                                                                      | `controller`, `action`                   |
| `gitlab_ci_pipeline_builder_scoped_variables_duration`           | Histogram   | 14.5   | 为 CI/CD 作业创建范围变量所需的时间（以秒为单位）
| `gitlab_ci_pipeline_creation_duration_seconds`                   | Histogram   | 13.0    | 创建 CI/CD 流水线所需的时间（以秒为单位）                                                                   |        `gitlab`                             |
| `gitlab_ci_pipeline_size_builds`                                 | Histogram   | 13.1    | 按流水线源分组的流水线内的构建总数                                                 | `source`                                                  |
| `gitlab_ci_runner_authentication_success_total`                  | Counter     | 15.2    | runner 认证成功的总次数                                                        | `type`                                                    |
| `gitlab_ci_runner_authentication_failure_total`                  | Counter     | 15.2    | runner 认证失败的总次数                           |                                                |
| `gitlab_ghost_user_migration_lag_seconds`                        | Gauge       | 15.6    | Ghost 用户迁移的最早计划记录的等待时间（秒）                                   |                                                           |
| `gitlab_ghost_user_migration_scheduled_records_total`            | Gauge       | 15.6    | 计划的 ghost 用户迁移总数                                                                   |                                                           |
| `job_waiter_started_total`                                       | Counter     | 12.9    | 在 Web 请求等待作业完成的情况下启动的作业批次数                             | `worker`                                                  |
| `job_waiter_timeouts_total`                                      | Counter     | 12.9    | Web 请求正在等待作业完成时超时的作业批次数                      | `worker`                                                  |
| `gitlab_ci_active_jobs`                                          | Histogram   | 14.2    | 创建流水线时的活动作业计数                                                                         |                                                           |
| `gitlab_database_transaction_seconds`                            | Histogram   | 12.1    | 在数据库事务中花费的时间，以秒为单位                                                                      |                                                           |
| `gitlab_method_call_duration_seconds`                            | Histogram   | 10.2    | 方法调用实际持续时间                                                                                            | `controller`, `action`, `module`, `method`                |
| `gitlab_page_out_of_bounds`                                      | Counter     | 12.8    | 达到 PageLimiter 分页限制的计数器                                                                | `controller`, `action`, `bot`                             |
| `gitlab_rails_boot_time_seconds`                                 | Gauge       | 14.8    | Rails 主进程完成启动所用的时间                                               |                                                           |
| `gitlab_rails_queue_duration_seconds`                            | Histogram   | 9.4     | 测量 GitLab Workhorse 向 Rails 转发请求之间的延迟                                               |                                                           |
| `gitlab_sql_duration_seconds`                                    | Histogram   | 10.2    | SQL 执行时间，不包括 `SCHEMA` 操作和 `BEGIN` / `COMMIT`                                              |                                                           |
| `gitlab_sql_<role>_duration_seconds`                             | Histogram   | 13.10   | SQL 执行时间，不包括 `SCHEMA` 操作和 `BEGIN` / `COMMIT`，按数据库角色（主/副本）分组 |                                                           |
| `gitlab_ruby_threads_max_expected_threads`                       | Gauge       | 13.3    | 预期正在运行和执行应用程序工作的最大线程数                                      |                                                           |
| `gitlab_ruby_threads_running_threads`                            | Gauge       | 13.3    | 按名称运行的 Ruby 线程数                                                                                |                                                           |
| `gitlab_transaction_cache_<key>_count_total`                     | Counter     | 10.2    | Rails 缓存调用总数的计数器（每个键）                                                                         |                                                           |
| `gitlab_transaction_cache_<key>_duration_total`                  | Counter     | 10.2    | 在 Rails 缓存调用中花费的总时间（秒）计数器（每个键）                                                 |                                                           |
| `gitlab_transaction_cache_count_total`                           | Counter     | 10.2    | Rails 缓存调用总数的计数器（聚合）                                                                       |                                                           |
| `gitlab_transaction_cache_duration_total`                        | Counter     | 10.2    | 在 Rails 缓存调用（聚合）中花费的总时间（秒）计数器                                               |                                                           |
| `gitlab_transaction_cache_read_hit_count_total`                  | Counter     | 10.2    | Rails 缓存调用的缓存命中计数器                                                                          | `controller`, `action`                                    |
| `gitlab_transaction_cache_read_miss_count_total`                 | Counter     | 10.2    | Rails 缓存调用的缓存未命中计数器                                                                        | `controller`, `action`                                    |
| `gitlab_transaction_duration_seconds`                            | Histogram   | 10.2    | 成功请求的持续时间（`gitlab_transaction_*` 指标）                                                     | `controller`, `action`                                    |
| `gitlab_transaction_event_build_found_total`                     | Counter     | 9.4     | 为 API /jobs/request 找到的构建计数器                                                                         |                                                           |
| `gitlab_transaction_event_build_invalid_total`                   | Counter     | 9.4     | 由于 API /jobs/request 的并发冲突，构建无效的计数器                                           |                                                           |
| `gitlab_transaction_event_build_not_found_cached_total`          | Counter     | 9.4     | 未找到 API /jobs/request 的构建缓存响应计数器                                                  |                                                           |
| `gitlab_transaction_event_build_not_found_total`                 | Counter     | 9.4     | 未找到 API /jobs/request 的构建计数器                                                                     |                                                           |
| `gitlab_transaction_event_change_default_branch_total`           | Counter     | 9.4     | 更改任何仓库的默认分支时的计数器                                                             |                                                           |
| `gitlab_transaction_event_create_repository_total`               | Counter     | 9.4     | 创建任何仓库时的计数器                                                                                |                                                           |
| `gitlab_transaction_event_etag_caching_cache_hit_total`          | Counter     | 9.4     | ETag 缓存命中计数器                                                                                           | `endpoint`                                                |
| `gitlab_transaction_event_etag_caching_header_missing_total`     | Counter     | 9.4     | ETag 缓存未命中计数器 - 缺少标头                                                                          | `endpoint`                                                |
| `gitlab_transaction_event_etag_caching_key_not_found_total`      | Counter     | 9.4     | ETag 缓存未命中计数器 - 未找到密钥                                                                           | `endpoint`                                                |
| `gitlab_transaction_event_etag_caching_middleware_used_total`    | Counter     | 9.4     | 被访问的 ETag 中间件的计数器                                                                                  | `endpoint`                                                |
| `gitlab_transaction_event_etag_caching_resource_changed_total`   | Counter     | 9.4     | ETag 缓存未命中计数器 - 资源已更改                                                                        | `endpoint`                                                |
| `gitlab_transaction_event_fork_repository_total`                 | Counter     | 9.4     | 仓库派生计数器 (RepositoryForkWorker)。仅在源仓库存在时递增                   |                                                           |
| `gitlab_transaction_event_import_repository_total`               | Counter     | 9.4     | 仓库导入计数器 (RepositoryImportWorker)                                                               |                                                           |
| `gitlab_transaction_event_patch_hard_limit_bytes_hit_total`      | Counter     | 13.9    | 差异补丁大小限制命中计数器                                                                                |                                                           |
| `gitlab_transaction_event_push_branch_total`                     | Counter     | 9.4     | 所有分支推送的计数器                                                                                         |                                                           |
| `gitlab_transaction_event_rails_exception_total`                 | Counter     | 9.4     | Rails 异常数计数器                                                                                |                                                           |
| `gitlab_transaction_event_receive_email_total`                   | Counter     | 9.4     | 收到邮件的计数器                                                                                           | `handler`                                                 |
| `gitlab_transaction_event_remote_mirrors_failed_total`           | Counter     | 10.8    | 失败的远端镜像（mirrors）计数器                                                                                     |                                                           |
| `gitlab_transaction_event_remote_mirrors_finished_total`         | Counter     | 10.8    | 完成的远端镜像（mirrors）计数器                                                                                   |                                                           |
| `gitlab_transaction_event_remote_mirrors_running_total`          | Counter     | 10.8    | 运行的远端镜像（mirrors）计数器                                                                                    |                                                           |
| `gitlab_transaction_event_remove_branch_total`                   | Counter     | 9.4     | 删除任何仓库的分支的计数器                                                                   |                                                           |
| `gitlab_transaction_event_remove_repository_total`               | Counter     | 9.4     | 删除仓库的计数器                                                                                  |                                                           |
| `gitlab_transaction_event_remove_tag_total`                      | Counter     | 9.4     | 删除任何仓库的标签的计数器                                                                       |                                                           |
| `gitlab_transaction_event_sidekiq_exception_total`               | Counter     | 9.4     | Sidekiq 异常计数器                                                                                         |                                                           |
| `gitlab_transaction_event_stuck_import_jobs_total`               | Counter     | 9.4     | 卡住的导入作业数                                                                                            | `projects_without_jid_count`, `projects_with_jid_count`   |
| `gitlab_transaction_event_update_build_total`                    | Counter     | 9.4     | API `/jobs/request/:id` 的更新构建计数器                                                                  |                                                           |
| `gitlab_transaction_new_redis_connections_total`                 | Counter     | 9.4     | 新 Redis 连接计数器                                                                                     |                                                           |                                                           |
| `gitlab_transaction_rails_queue_duration_total`                  | Counter     | 9.4     | 测量 GitLab Workhorse 向 Rails 转发请求之间的延迟                                               | `controller`, `action`                                    |
| `gitlab_transaction_view_duration_total`                         | Counter     | 9.4     | 视图事务时长                                                                                                    | `controller`, `action`, `view`                            |
| `gitlab_view_rendering_duration_seconds`                         | Histogram   | 10.2    | 视图事务时长 (histogram)                                                                                        | `controller`, `action`, `view`                            |
| `http_requests_total`                                            | Counter     | 9.4     | Rack 请求计数                                                                                                    | `method`, `status`                                        |
| `http_request_duration_seconds`                                  | Histogram   | 9.4     | Rack 中间件对成功请求的 HTTP 响应时间                                                       | `method`                                                  |
| `gitlab_transaction_db_count_total`                              | Counter     | 13.1    | SQL 调用总数的计数器                                                                                 | `controller`, `action`                                    |
| `gitlab_transaction_db_<role>_count_total`                       | Counter     | 13.10   | SQL 调用总数的计数器，按数据库角色（主/副本）分组                                    | `controller`, `action`                                    |
| `gitlab_transaction_db_write_count_total`                        | Counter     | 13.1    | 写 SQL 调用总数的计数器                                                                           | `controller`, `action`                                    |
| `gitlab_transaction_db_cached_count_total`                       | Counter     | 13.1    | 缓存 SQL 调用总数的计数器                                                                          | `controller`, `action`                                    |
| `gitlab_transaction_db_<role>_cached_count_total`                | Counter     | 13.1    | 缓存 SQL 调用总数的计数器，按数据库角色（主/副本）分组                             | `controller`, `action`                                    |
| `gitlab_transaction_db_<role>_wal_count_total`                   | Counter     | 14.0    | WAL（预写日志位置）查询总数的计数器，按数据库角色（主/副本）分组       | `controller`, `action`                                    |
| `gitlab_transaction_db_<role>_wal_cached_count_total`            | Counter     | 14.1    | 缓存 WAL（预写日志位置）查询总数的计数器，按数据库角色（主/副本）分组 | `controller`, `action`                                    |
| `http_elasticsearch_requests_duration_seconds` **(PREMIUM)**     | Histogram   | 13.1    | Web 事务期间的 Elasticsearch 请求持续时间                                                               | `controller`, `action`                                    |
| `http_elasticsearch_requests_total` **(PREMIUM)**                | Counter     | 13.1    | Web 事务期间的 Elasticsearch 请求计数                                                                  | `controller`, `action`                                    |
| `pipelines_created_total`                                        | Counter     | 9.4     | 创建的流水线计数器                                                                                          |                                                           |
| `rack_uncaught_errors_total`                                     | Counter     | 9.4     | 处理未捕获错误的 Rack 连接数                                                                       |                                                           |
| `user_session_logins_total`                                      | Counter     | 9.4     | 自 GitLab 启动或重新启动以来有多少用户登录的计数器                                        |                                                           |
| `upload_file_does_not_exist`                                     | Counter     | 10.7    | 上传记录找不到其文件的次数。 |                                                           |
| `failed_login_captcha_total`                                     | Gauge       | 11.0    | 登录期间失败的 CAPTCHA 尝试计数器                                                                       |                                                           |
| `successful_login_captcha_total`                                 | Gauge       | 11.0    | 登录期间成功验证码尝试的计数器                                                                   |                                                           |
| `auto_devops_pipelines_completed_total`                          | Counter     | 12.7    | 已完成 Auto DevOps 流水线的计数器，按状态标记                                                         |                                                           |
| `artifact_report_<report_type>_builds_completed_total`           | Counter     | 15.3    | 具有报告类型产物的已完成 CI 构建计数器，按报告类型分组并按状态标记               |                                                           |
| `gitlab_metrics_dashboard_processing_time_ms`                    | Summary     | 12.10   | 指标仪表盘处理时间（以毫秒为单位）                                                                     | service, stages                                           |
| `action_cable_active_connections`                                | Gauge       | 13.4    | 当前连接的 ActionCable WS 客户端数量                                                                  | `server_mode`                                             |
| `action_cable_broadcasts_total`                                  | Counter     | 13.10   | 发出的 ActionCable 广播的数量                                                                          | `server_mode`                                             |
| `action_cable_pool_min_size`                                     | Gauge       | 13.4    | ActionCable 线程池中的最小 worker 线程数                                                           | `server_mode`                                             |
| `action_cable_pool_max_size`                                     | Gauge       | 13.4    | ActionCable 线程池中的最大 worker 线程数                                                           | `server_mode`                                             |
| `action_cable_pool_current_size`                                 | Gauge       | 13.4    | ActionCable 线程池中的当前 worker 线程数                                                           | `server_mode`                                             |
| `action_cable_pool_largest_size`                                 | Gauge       | 13.4    | 迄今为止在 ActionCable 线程池中观察到的最大 worker 线程数                                           | `server_mode`                                             |
| `action_cable_pool_pending_tasks`                                | Gauge       | 13.4    | ActionCable 线程池中等待执行的任务数                                                     | `server_mode`                                             |
| `action_cable_pool_tasks_total`                                  | Gauge       | 13.4    | ActionCable 线程池中执行的任务总数                                                             | `server_mode`                                             |
| `gitlab_ci_trace_operations_total`                               | Counter     | 13.4    | 构建跟踪上不同操作的总数                                                                 | `operation`                                               |
| `gitlab_ci_trace_bytes_total`                                    | Counter     | 13.4    | 传输的构建跟踪字节总数                                                                         |                                                           |
| `action_cable_single_client_transmissions_total`                 | Counter     | 13.10   | 在任何通道中传输到任何客户端的 ActionCable 消息的数量                                           | `server_mode`                                             |
| `action_cable_subscription_confirmations_total`                  | Counter     | 13.10   | 客户确认的 ActionCable 订阅数量                                                        | `server_mode`                                             |
| `action_cable_subscription_rejections_total`                     | Counter     | 13.10   | 来自客户端的 ActionCable 订阅数被拒绝                                                         | `server_mode`                                             |
| `action_cable_transmitted_bytes`                                 | Histogram   | 14.1    | 通过 ActionCable 传输的消息大小（以字节为单位）                                                                 | `operation`, `channel`                                    |
| `gitlab_issuable_fast_count_by_state_total`                      | Counter     | 13.5    | 发布/合并请求列表页面上的行计数操作总数                                                |                                                           |
| `gitlab_issuable_fast_count_by_state_failures_total`             | Counter     | 13.5    | 发布/合并请求列表页面上的软失败行计数操作数                                          |                                                           |
| `gitlab_ci_trace_finalize_duration_seconds`                      | Histogram   | 13.6    | 构建跟踪块迁移到对象存储的持续时间                                                            |                                                           |
| `gitlab_external_http_total`                                     | Counter     | 13.8    | 对外部系统的 HTTP 调用总数                                                                        | `controller`, `action`                                    |
| `gitlab_external_http_duration_seconds`                          | Counter     | 13.8    | 对外部系统的每次 HTTP 调用所花费的持续时间（以秒为单位）                                                       |                                                           |
| `gitlab_external_http_exception_total`                           | Counter     | 13.8    | 进行外部 HTTP 调用时引发的异常总数                                                     |                                                           |
| `ci_report_parser_duration_seconds`                              | Histogram   | 13.9    | 解析 CI/CD 报告产物的时间                                                                                  | `parser`                                                  |
| `pipeline_graph_link_calculation_duration_seconds`               | Histogram   | 13.9    | 计算链接所花费的总时间，以秒为单位                                                                        |                                                           |
| `pipeline_graph_links_total`                                     | Histogram   | 13.9    | 每个图表的链接数                                                                                             |                                                           |
| `pipeline_graph_links_per_job_ratio`                             | Histogram   | 13.9    | 每个图表的作业链接比率                                                                                       |                                                           |
| `gitlab_ci_pipeline_security_orchestration_policy_processing_duration_seconds` | Histogram   | 13.12    | 在 CI/CD 流水线中处理安全策略所需的时间（以秒为单位）                                |                                                           |
| `gitlab_spamcheck_request_duration_seconds`                      | Histogram   | 13.12   | Rails 和反垃圾邮件引擎之间的请求持续时间                                                      |                                                           |
| `service_desk_thank_you_email`                                   | Counter     | 14.0    | 对新服务台电子邮件的电子邮件回复总数                                                            |                                                           |
| `service_desk_new_note_email`                                    | Counter     | 14.0    | 新服务台评论的电子邮件通知总数                                                       |                                                           |
| `email_receiver_error`                                           | Counter     | 14.1    | 处理传入电子邮件时的错误总数                                                                |                                                           |
| `gitlab_snowplow_events_total`                                   | Counter     | 14.1    | GitLab Snowplow 产品智能事件发出总数                                                   |                                                           |
| `gitlab_snowplow_failed_events_total`                            | Counter     | 14.1    | GitLab Snowplow 产品智能事件发出失败总数                                         |                                                           |
| `gitlab_snowplow_successful_events_total`                        | Counter     | 14.1    | GitLab Snowplow 产品智能事件发出成功总数                                        |                                                           |
| `gitlab_ci_build_trace_errors_total`                             | Counter     | 14.4    | 构建跟踪中不同错误类型的总数                                                                | `error_reason`                                            |
| `gitlab_presentable_object_cacheless_render_real_duration_seconds`              | Histogram   | 15.3     | 用于缓存和表示特定 Web 请求对象的实时持续时间                                                    | `controller`、`action`                                    |
| `cached_object_operations_total`                                      | Counter     | 15.3    | 为特定 Web 请求缓存的对象总数                                                                                                      | `controller`、`action`                                    |
| `gitlab_presentable_object_cacheless_render_real_duration_seconds`              | Histogram   | 15.3     | 实时缓存和代表特定 web 请求对象的持续时间                                                    | `controller`, `action`                                    |
| `cached_object_operations_total`                                      | Counter     | 15.3    | 为特定 web 请求缓存的对象总数                                                                                                      | `controller`, `action`                                    |
| `redis_hit_miss_operations_total`                                | Counter     | 15.6    | Redis 缓存命中和未命中总数                                                                           | `cache_hit`, `caller_id`, `cache_identifier`, `feature_category`, `backing_resource` |
| `redis_cache_generation_duration_seconds`                        | Histogram   | 15.6    | 生成 Redis 缓存的时间                                                                                          | `cache_hit`, `caller_id`, `cache_identifier`, `feature_category`, `backing_resource` |
| `gitlab_diffs_reorder_real_duration_seconds` | Histogram | 15.8 | 在差异批处理请求上，重新排序差异文件所花费的持续时间（以秒为单位） | `controller`, `action` |
| `gitlab_diffs_collection_real_duration_seconds` | Histogram | 15.8 | 在差异批处理请求上，查询合并请求差异文件所花费的持续时间（以秒为单位） | `controller`, `action` |
| `gitlab_diffs_comparison_real_duration_seconds` | Histogram | 15.8 | 在差异批处理请求上，获取比较数据所花费的持续时间（以秒为单位） | `controller`, `action` |
| `gitlab_diffs_unfoldable_positions_real_duration_seconds` | Histogram | 15.8 | 在差异批处理请求中，获取可展开的注释位置所花费的持续时间（以秒为单位） | `controller`, `action` |
| `gitlab_diffs_unfold_real_duration_seconds` | Histogram | 15.8 | 在差异批请求上，展开位置所花费的持续时间（以秒为单位） | `controller`, `action` |
| `gitlab_diffs_write_cache_real_duration_seconds` | Histogram | 15.8 | 在差异批处理请求上，缓存突出显示的行和统计信息所花费的持续时间（以秒为单位） | `controller`, `action` |
| `gitlab_diffs_highlight_cache_decorate_real_duration_seconds` | Histogram | 15.8 | 在差异批处理请求上，设置缓存中突出显示的行所花费的持续时间（以秒为单位） | `controller`, `action` |
| `gitlab_diffs_render_real_duration_seconds` | Histogram | 15.8 | 在差异批处理请求上，序列化和呈现差异所花费的持续时间（以秒为单位） | `controller`, `action` |
| `gitlab_memwd_violations_total`                      | Counter | 15.9  | Ruby 进程违反内存阈值的总次数 | |
| `gitlab_memwd_violations_handled_total`              | Counter | 15.9  | 处理 Ruby 进程内存违规的总次数 | |

## 由功能标志控制的指标

以下指标可以由功能标志控制：

| 指标                                                        | 功能标志                                                       |
|:---------------------------------------------------------------|:-------------------------------------------------------------------|
| `gitlab_view_rendering_duration_seconds`                       | `prometheus_metrics_view_instrumentation`                          |

<!--
## Praefect metrics

You can [configure Praefect](../../gitaly/praefect.md#praefect) to report metrics. For information
on available metrics, see the [relevant documentation](../../gitaly/index.md#monitor-gitaly-cluster).
-->

<a id="sidekiq-metrics"></a>

## Sidekiq 指标

Sidekiq 作业也可以收集指标，如果启用了 Sidekiq exporter，则可以访问这些指标：例如，使用 `gitlab.yml` 中的 `monitoring.sidekiq_exporter` 配置选项。这些指标是从配置端口上的 `/metrics` 路径提供的。

| 指标                                         | 类型    | 引入版本 | 描述 | 标记 |
|:---------------------------------------------- |:------- |:----- |:----------- |:------ |
| `sidekiq_jobs_cpu_seconds`                     | Histogram | 12.4 | 运行 Sidekiq 作业的 CPU 时间秒数                                                              | `queue`, `boundary`, `external_dependencies`, `feature_category`, `job_status`, `urgency` |
| `sidekiq_jobs_completion_seconds`              | Histogram | 12.2 | 完成 Sidekiq 作业的秒数                                                                    | `queue`, `boundary`, `external_dependencies`, `feature_category`, `job_status`, `urgency` |
| `sidekiq_jobs_db_seconds`                      | Histogram | 12.9 | 运行 Sidekiq 作业的数据库时间秒数                                                               | `queue`, `boundary`, `external_dependencies`, `feature_category`, `job_status`, `urgency` |
| `sidekiq_jobs_gitaly_seconds`                  | Histogram | 12.9 | 运行 Sidekiq 作业的 Gitaly 秒数                                                           | `queue`, `boundary`, `external_dependencies`, `feature_category`, `job_status`, `urgency` |
| `sidekiq_redis_requests_duration_seconds`      | Histogram | 13.1 | Sidekiq 作业用于查询 Redis 服务器的持续时间（以秒为单位）                                | `queue`, `boundary`, `external_dependencies`, `feature_category`, `job_status`, `urgency` |
| `sidekiq_elasticsearch_requests_duration_seconds`      | Histogram | 13.1 | Sidekiq 作业在对 Elasticsearch 服务器的请求中花费的持续时间（以秒为单位）                                | `queue`, `boundary`, `external_dependencies`, `feature_category`, `job_status`, `urgency` |
| `sidekiq_jobs_queue_duration_seconds`          | Histogram | 12.5 | Sidekiq 作业在执行前排队的持续时间（以秒为单位）                             | `queue`, `boundary`, `external_dependencies`, `feature_category`, `urgency` |
| `sidekiq_jobs_failed_total`                    | Counter   | 12.2 | Sidekiq 作业失败                                                                                 | `queue`, `boundary`, `external_dependencies`, `feature_category`, `urgency` |
| `sidekiq_jobs_retried_total`                   | Counter   | 12.2 | Sidekiq 作业重试                                                                                | `queue`, `boundary`, `external_dependencies`, `feature_category`, `urgency` |
| `sidekiq_jobs_interrupted_total`               | Counter   | 15.2 | Sidekiq 作业中断                                                                            | `queue`, `boundary`, `external_dependencies`, `feature_category`, `urgency` |
| `sidekiq_jobs_dead_total`                      | Counter   | 13.7 | Sidekiq 死作业（重试次数用完的作业）                                               | `queue`, `boundary`, `external_dependencies`, `feature_category`, `urgency` |
| `sidekiq_redis_requests_total`                 | Counter   | 13.1 | Sidekiq 作业执行期间的 Redis 请求                                                       | `queue`, `boundary`, `external_dependencies`, `feature_category`, `job_status`, `urgency` |
| `sidekiq_elasticsearch_requests_total`         | Counter   | 13.1 | Sidekiq 作业执行期间的 Elasticsearch 请求                                                       | `queue`, `boundary`, `external_dependencies`, `feature_category`, `job_status`, `urgency` |
| `sidekiq_running_jobs`                         | Gauge     | 12.2 | 正在运行的 Sidekiq 作业数                                                                      | `queue`, `boundary`, `external_dependencies`, `feature_category`, `urgency` |
| `sidekiq_concurrency`                          | Gauge     | 12.5 | Sidekiq 作业的最大数量                                                                      |                                                                   |
| `sidekiq_mem_total_bytes`                      | Gauge     | 15.3 | 为消耗对象槽的对象和需要 malloc 的对象分配的字节数 |                                                                   |
| `geo_db_replication_lag_seconds`               | Gauge   | 10.2  | 数据库复制延迟（秒） | `url` |
| `geo_repositories`                             | Gauge   | 10.2  | 主库上可用的仓库总数 | `url` |
| `geo_repositories_synced`                      | Gauge   | 10.2  | 在次要节点上同步的仓库数量 | `url` |
| `geo_repositories_failed`                      | Gauge   | 10.2  | 无法在次要节点上同步的仓库数量 | `url` |
| `geo_lfs_objects`                              | Gauge   | 10.2 | 主节点上的 LFS 对象的数量 | `url` |
| `geo_lfs_objects_checksummed`                  | Gauge   | 14.6  | 在主节点上成功校验和的 LFS 对象的数量 | `url` |
| `geo_lfs_objects_checksum_failed`              | Gauge   | 14.6  | 无法计算主节点上的校验和的 LFS 对象的数量 | `url` |
| `geo_lfs_objects_checksum_total`               | Gauge   | 14.6  | 尝试在主节点上校验和的 LFS 对象的数量 | `url` |
| `geo_lfs_objects_synced`                       | Gauge   | 10.2  | 在次要节点上同步的可同步 LFS 对象的数量 | `url` |
| `geo_lfs_objects_failed`                       | Gauge   | 10.2  | 无法在次要节点上同步的可同步 LFS 对象的数量 | `url` |
| `geo_lfs_objects_registry`                     | Gauge   | 14.6  | Registry 中 LFS 对象的数量 | `url` |
| `geo_lfs_objects_verified`                     | Gauge   | 14.6  | 在次要节点上验证的 LFS 对象的数量 | `url` |
| `geo_lfs_objects_verification_failed`          | Gauge   | 14.6 | 在次要节点上验证失败的 LFS 对象的数量 | `url` |
| `geo_lfs_objects_verification_total`           | Gauge   | 14.6  | 在次要节点上尝试验证的 LFS 对象的数量 | `url` | 
| `geo_attachments`                              | Gauge   | 10.2  | 主节点上可用的文件附件总数 | `url` |
| `geo_attachments_synced`                       | Gauge   | 10.2  | 次要节点上同步的附件数量 | `url` |
| `geo_attachments_failed`                       | Gauge   | 10.2  | 无法在次要节点上同步的附件数量 | `url` |
| `geo_last_event_id`                            | Gauge   | 10.2  | 主节点上最新事件日志条目的数据库 ID | `url` |
| `geo_last_event_timestamp`                     | Gauge   | 10.2  | 主节点上最新事件日志条目的 | `url` |
| `geo_cursor_last_event_id`                     | Gauge   | 10.2  | 次要节点处理的事件日志的最后一个数据库 ID | `url` |
| `geo_cursor_last_event_timestamp`              | Gauge   | 10.2  | 次要节点处理的事件日志的最后一个 UNIX 时间戳 | `url` |
| `geo_status_failed_total`                      | Counter | 10.2  | 从 Geo 节点检索状态失败的次数 | `url` |
| `geo_last_successful_status_check_timestamp`   | Gauge   | 10.2  | 状态更新成功的最后时间戳 | `url` |
| `geo_job_artifacts_synced_missing_on_primary`  | Gauge   | 10.7  | 由于主节点文件丢失而标记为已同步的作业产物数 | `url` |
| `geo_repositories_checksummed`                 | Gauge   | 10.7  | 主节点上校验和的仓库数量 | `url` |
| `geo_repositories_checksum_failed`             | Gauge   | 10.7  | 主节点上无法计算校验和的仓库数量 | `url` |
| `geo_wikis_checksummed`                        | Gauge   | 10.7  | 主节点上校验和的 wiki 数量 | `url` |
| `geo_wikis_checksum_failed`                    | Gauge   | 10.7  | 主节点上无法计算校验和的 wiki 数量 | `url` |
| `geo_repositories_verified`                    | Gauge   | 10.7  | 在次要节点上验证的仓库数量 | `url` |
| `geo_repositories_verification_failed`         | Gauge   | 10.7  | 无法在次要节点上验证的仓库数量 | `url` |
| `geo_repositories_checksum_mismatch`           | Gauge   | 10.7  | 在次要节点上校验和不匹配的仓库数量 | `url` |
| `geo_wikis_verified`                           | Gauge   | 10.7  | 在次要节点上验证的 wiki 数量 | `url` |
| `geo_wikis_verification_failed`                | Gauge   | 10.7  | 在次要节点上无法验证的 wiki 数量 | `url` |
| `geo_wikis_checksum_mismatch`                  | Gauge   | 10.7  | 在次要节点上校验和不匹配的 wiki 数量 | `url` |
| `geo_repositories_checked`                     | Gauge   | 11.1  | 通过 `git fsck` 检查的仓库数量 | `url` |
| `geo_repositories_checked_failed`              | Gauge   | 11.1  | `git fsck` 失败的仓库数量 | `url` |
| `geo_repositories_retrying_verification`       | Gauge   | 11.2  | 在次要节点上 Geo 积极尝试纠正的仓库验证失败的数量  | `url` |
| `geo_wikis_retrying_verification`              | Gauge   | 11.2  | 在次要节点上 Geo 积极尝试纠正的 wiki 验证失败的数量 | `url` |
| `geo_package_files`                            | Gauge   | 13.0  | 主节点上的包文件数 | `url` |
| `geo_package_files_checksummed`                | Gauge   | 13.0  | 主节点上校验和的包文件数 | `url` |
| `geo_package_files_checksum_failed`            | Gauge   | 13.0  | 无法在主节点上计算校验和的包文件数 | `url` |
| `geo_package_files_synced`                     | Gauge   | 13.3  | 在次要节点上同步的可同步包文件的数量 | `url` |
| `geo_package_files_failed`                     | Gauge   | 13.3  | 在次要节点上无法同步的可同步包文件的数量 | `url` |
| `geo_package_files_registry`                   | Gauge   | 13.3  | Registry 中的包文件数 | `url` |
| `geo_terraform_state_versions`                 | Gauge   | 13.5  | 主节点上的 terraform state 版本数 | `url` |
| `geo_terraform_state_versions_checksummed`     | Gauge   | 13.5  | 主节点上成功校验和的 terraform state 版本数 | `url` |
| `geo_terraform_state_versions_checksum_failed` | Gauge   | 13.5  | 主节点上无法计算校验和的 terraform state 版本数 | `url` |
| `geo_terraform_state_versions_checksum_total`  | Gauge   | 13.12  | 尝试在主节点上校验和的 terraform state 版本数 | `url` |
| `geo_terraform_state_versions_synced`          | Gauge   | 13.5  | 在次要节点上同步的可同步 terraform state 版本数 | `url` |
| `geo_terraform_state_versions_failed`          | Gauge   | 13.5  | 在次要节点上无法同步的可同步 terraform state 版本数 | `url` |
| `geo_terraform_state_versions_registry`        | Gauge   | 13.5  | Registry 中的 terraform state 版本数 | `url` |
| `geo_terraform_state_versions_verified`        | Gauge   | 13.12  | 在次要节点上验证的 terraform state 版本数 | `url` |
| `geo_terraform_state_versions_verification_failed` | Gauge   | 13.12  | 在次要节点上 terraform state 版本验证失败的数量 | `url` |
| `geo_terraform_state_versions_verification_total` | Gauge   | 13.12  | 在次要节点上 terraform 状态版本尝试验证的数量 | `url` |
| `global_search_bulk_cron_queue_size`           | Gauge   | 12.10 | 等待同步到 Elasticsearch 的数据库记录数 | |
| `global_search_awaiting_indexing_queue_size`   | Gauge   | 13.2  | 索引暂停时等待同步到 Elasticsearch 的数据库更新数 | |
| `geo_merge_request_diffs`                      | Gauge   | 13.4  | 主节点上的合并请求差异数 | `url` |
| `geo_merge_request_diffs_checksum_total`       | Gauge   | 13.12 | 在主节点上尝试校验和的合并请求差异数 | `url` |
| `geo_merge_request_diffs_checksummed`          | Gauge   | 13.4  | 在主节点上成功校验和的合并请求差异数 | `url` |
| `geo_merge_request_diffs_checksum_failed`      | Gauge   | 13.4  | 在主节点上无法计算校验和的合并请求差异数 | `url` |
| `geo_merge_request_diffs_synced`               | Gauge   | 13.4  | 在次要节点上同步的可同步合并请求差异数 | `url` |
| `geo_merge_request_diffs_failed`               | Gauge   | 13.4  | 在次要节点上无法同步的可同步合并请求差异数 | `url` |
| `geo_merge_request_diffs_registry`             | Gauge   | 13.4  | Registry 中的合并请求差异数 | `url` |
| `geo_merge_request_diffs_verification_total`   | Gauge   | 13.12 | 在次要节点上尝试的合并请求差异验证数 | `url` |
| `geo_merge_request_diffs_verified`             | Gauge   | 13.12 | 在次要节点上验证的合并请求差异数 | `url` |
| `geo_merge_request_diffs_verification_failed`  | Gauge   | 13.12 | 在次要节点上失败的合并请求差异验证数 | `url` |
| `geo_snippet_repositories`                     | Gauge   | 13.4  | 主节点上代码片段的数量 | `url` |
| `geo_snippet_repositories_checksummed`         | Gauge   | 13.4  | 主节点上校验和代码片段的数量 | `url` |
| `geo_snippet_repositories_checksum_failed`     | Gauge   | 13.4  | 主节点上未计算校验和代码片段的数量 | `url` |
| `geo_snippet_repositories_synced`              | Gauge   | 13.4  | 在次要节点上同步的可同步片段数 | `url` |
| `geo_snippet_repositories_failed`              | Gauge   | 13.4  | 在次要节点上同步失败的可同步片段数 | `url` |
| `geo_snippet_repositories_registry`            | Gauge   | 13.4  | Registry 上的代码片段的数量 | `url` |
| `geo_group_wiki_repositories`                  | Gauge   | 13.10 | 主节点上群组 wiki 的数量 | `url` |
| `geo_group_wiki_repositories_checksummed`      | Gauge   | 13.10 | 主节点上校验和群组 wiki 的数量 | `url` |
| `geo_group_wiki_repositories_checksum_failed`  | Gauge   | 13.10 | 主节点上无法计算校验和群组 wiki 的数量 | `url` |
| `geo_group_wiki_repositories_synced`           | Gauge   | 13.10 | 在次要节点上同步的可同步群组 wiki 的数量 | `url` |
| `geo_group_wiki_repositories_failed`           | Gauge   | 13.10 | 在次要节点上同步失败的可同步群组 wiki 的数量 | `url` |
| `geo_group_wiki_repositories_registry`         | Gauge   | 13.10 | Registry 上的群组 wiki 的数量 | `url` |
| `geo_pages_deployments`                        | Gauge   | 14.3  | 主节点上 pages 部署的数量 | `url` |
| `geo_pages_deployments_checksum_total`         | Gauge   | 14.6  | 主节点上尝试校验和的 pages 部署的数量 | `url` |
| `geo_pages_deployments_checksummed`            | Gauge   | 14.6  | 主节点上成功校验和的 pages 部署的数量 | `url` |
| `geo_pages_deployments_checksum_failed`        | Gauge   | 14.6  | 主节点上无法计算校验和的 pages 部署的数量 | `url` |
| `geo_pages_deployments_synced`                 | Gauge   | 14.3  | 在次要节点上同步的可同步 pages 部署的数量 | `url` |
| `geo_pages_deployments_failed`                 | Gauge   | 14.3  | 在次要节点上无法同步的可同步 pages 部署的数量 | `url` |
| `geo_pages_deployments_registry`               | Gauge   | 14.3  | Registry 上的 pages 部署的数量 | `url` |
| `geo_pages_deployments_verification_total`     | Gauge   | 14.6  | 在次要节点上尝试验证的 pages 部署的数量 | `url` |
| `geo_pages_deployments_verified`               | Gauge   | 14.6  | 在次要节点上验证的 pages 部署的数量 | `url` |
| `geo_pages_deployments_verification_failed`    | Gauge   | 14.6  | 在次要节点上验证失败的 pages 部署的数量 | `url` |
| `geo_job_artifacts`                            | Gauge   | 14.8  | 主节点上作业产物的数量 | `url` |
| `geo_job_artifacts_checksum_total`             | Gauge   | 14.8  | 主节点上尝试校验和的作业产物的数量 | `url` |
| `geo_job_artifacts_checksummed`                | Gauge   | 14.8  | 主节点上成功校验和的作业产物的数量 | `url` |
| `geo_job_artifacts_checksum_failed`            | Gauge   | 14.8  | 主节点上无法计算校验和的作业产物的数量 | `url` |
| `geo_job_artifacts_synced`                     | Gauge   | 14.8  | 在次要节点上同步的可同步作业产物的数量 | `url` |
| `geo_job_artifacts_failed`                     | Gauge   | 14.8  | 在次要节点上无法同步的可同步作业产物的数量 | `url` |
| `geo_job_artifacts_registry`                   | Gauge   | 14.8  | Registry 上的作业产物的数量 | `url` |
| `geo_job_artifacts_verification_total`         | Gauge   | 14.8  | 在次要节点上尝试验证的作业产物的数量 | `url` |
| `geo_job_artifacts_verified`                   | Gauge   | 14.8  | 在次要节点上验证的作业产物的数量 | `url` |
| `geo_job_artifacts_verification_failed`        | Gauge   | 14.8  | 在次要节点上验证失败的作业产物的数量 | `url` |
| `limited_capacity_worker_running_jobs`         | Gauge   | 13.5  | 正在运行的作业数 | `worker` |
| `limited_capacity_worker_max_running_jobs`     | Gauge   | 13.5  | 最大运行作业数 | `worker` |
| `limited_capacity_worker_remaining_work_count` | Gauge   | 13.5  | 等待入队的作业数 | `worker` |
| `destroyed_job_artifacts_count_total`          | Counter | 13.6  | 已销毁的过期作业产物数 | |
| `destroyed_pipeline_artifacts_count_total`     | Counter | 13.8  | 已销毁的过期流水线产物数 | |
| `gitlab_optimistic_locking_retries`            | Histogram | 13.10  | 执行乐观重试锁的重试次数 | |
| `geo_uploads`                      | Gauge   | 14.1  | 主节点上上传文件的数量 | `url` |
| `geo_uploads_synced`               | Gauge   | 14.1  | 在次要节点上同步的上传文件的数量 | `url` |
| `geo_uploads_failed`               | Gauge   | 14.1  | 在次要节点上同步失败的上传文件的数量 | `url` |
| `geo_uploads_registry`             | Gauge   | 14.1  | Registry 上的上传文件的数量 | `url` |
| `geo_uploads_checksum_total`       | Gauge   | 14.6 | 主节点上尝试校验和的上传文件的数量 | `url` |
| `geo_uploads_checksummed`          | Gauge   | 14.6  | 主节点上成功校验和的上传文件的数量 | `url` |
| `geo_uploads_checksum_failed`      | Gauge   | 14.6  | 主节点上计算校验和失败的上传文件的数量 | `url` |
| `geo_uploads_verification_total`   | Gauge   | 14.6 | 在次要节点上尝试验证的上传文件的数量 | `url` |
| `geo_uploads_verified`             | Gauge   | 14.6 | 在次要节点上验证的上传文件的数量 | `url` |
| `geo_uploads_verification_failed`  | Gauge   | 14.6 | 在次要节点上验证失败的上传文件的数量 | `url` |
| `geo_container_repositories`           | Gauge   | 15.4  | 主要节点上容器仓库的数量 | `url` |
| `geo_container_repositories_synced`    | Gauge   | 15.4  | 次要节点上已同步的容器仓库的数量 | `url` |
| `geo_container_repositories_failed`    | Gauge   | 15.4  | 次要节点上同步失败的可同步容器仓库的数量 | `url` |
| `geo_container_repositories_registry`  | Gauge   | 15.4  | Registry 中的容器仓库数量 | `url` |
| `gitlab_sli:rails_request_apdex:total` | Counter | 14.4 | request-apdex 测量的数量 | `endpoint_id`, `feature_category`, `request_urgency` |
| `gitlab_sli:rails_request_apdex:success_total` | Counter | 14.4 | 满足其紧迫性的目标持续时间的成功请求数。 除以 `gitlab_sli:rails_requests_apdex:total` 得到成功率 | `endpoint_id`, `feature_category`, `request_urgency` |
| `geo_ci_secure_files`                            | Gauge   | 15.3  | 主节点上的安全文件数 | `url` |
| `geo_ci_secure_files_checksum_total`             | Gauge   | 15.3  | 尝试在主节点上校验和的安全文件数 | `url` |
| `geo_ci_secure_files_checksummed`                | Gauge   | 15.3  | 在主节点上成功校验和的安全文件数 | `url` |
| `geo_ci_secure_files_checksum_failed`            | Gauge   | 15.3  | 无法计算主节点上的校验和的安全文件数 | `url` |
| `geo_ci_secure_files_synced`                     | Gauge   | 15.3  | 在次要节点上同步的可同步安全文件的数量 | `url` |
| `geo_ci_secure_files_failed`                     | Gauge   | 15.3  | 无法在次要节点上同步的可同步安全文件数 | `url` |
| `geo_ci_secure_files_registry`                   | Gauge   | 15.3  | Registry 中的安全文件数 | `url` |
| `geo_ci_secure_files_verification_total`         | Gauge   | 15.3  | 在次要节点上尝试的安全文件验证数 | `url` |
| `geo_ci_secure_files_verified`                   | Gauge   | 15.3  | 在次要节点上验证的安全文件数 | `url` |
| `geo_ci_secure_files_verification_failed`        | Gauge   | 15.3  | 在次要节点上安全文件验证失败的数量 | `url` |
| `geo_dependency_proxy_blob`                      | Gauge   | 15.6  | 在主要节点上的依赖代理 blobs 数量 | |
| `geo_dependency_proxy_blob_checksum_total`       | Gauge   | 15.6  | 在主要节点上尝试校验和的依赖代理 blobs 数量 | |
| `geo_dependency_proxy_blob_checksummed`          | Gauge   | 15.6  | 在主要节点上尝试校验和成功的依赖代理 blobs 数量 | |
| `geo_dependency_proxy_blob_checksum_failed`      | Gauge   | 15.6  | 在主要节点上计算校验和失败的依赖代理 blobs 数量 | |
| `geo_dependency_proxy_blob_synced`               | Gauge   | 15.6  | 在次要节点上已同步的依赖代理 blobs 数量 | |
| `geo_dependency_proxy_blob_failed`               | Gauge   | 15.6  | 在次要节点上同步失败的依赖代理 blobs 数量 | |
| `geo_dependency_proxy_blob_registry`             | Gauge   | 15.6  | 在 Registry 上的依赖代理 blobs 数量 | |
| `geo_dependency_proxy_blob_verification_total`   | Gauge   | 15.6  | 在次要节点上已尝试验证的依赖代理 blobs 数量 | |
| `geo_dependency_proxy_blob_verified`             | Gauge   | 15.6  | 在次要节点上已验证的依赖代理 blobs 数量 | |
| `geo_dependency_proxy_blob_verification_failed`  | Gauge   | 15.6  | 在次要节点上验证失败的依赖代理 blobs 数量 | |
| `geo_dependency_proxy_manifests`                     | Gauge   | 15.6  | 在主要节点上的依赖代理清单的数量 | `url` |
| `geo_dependency_proxy_manifests_checksum_total`      | Gauge   | 15.6  | 在主要节点上尝试校验和的依赖代理清单的数量 | `url` |
| `geo_dependency_proxy_manifests_checksummed`         | Gauge   | 15.6  | 在主要节点上尝试校验和成功的依赖代理清单的数量 | `url` |
| `geo_dependency_proxy_manifests_checksum_failed`     | Gauge   | 15.6  | 在主要节点上计算校验和失败的依赖代理清单的数量 | `url` |
| `geo_dependency_proxy_manifests_synced`              | Gauge   | 15.6  | 在次要节点上已同步的依赖代理清单的数量 | `url` |
| `geo_dependency_proxy_manifests_failed`              | Gauge   | 15.6  | 在次要节点上同步失败的依赖代理清单的数量 | `url` |
| `geo_dependency_proxy_manifests_registry`            | Gauge   | 15.6  | 在 Registry 上的依赖代理清单的数量 | `url` |
| `geo_dependency_proxy_manifests_verification_total`  | Gauge   | 15.6  | 在次要节点上已尝试验证的依赖代理清单的数量 | `url` |
| `geo_dependency_proxy_manifests_verified`            | Gauge   | 15.6  | 在次要节点上已验证的依赖代理清单的数量 | `url` |
| `geo_dependency_proxy_manifests_verification_failed` | Gauge   | 15.6  | 在次要节点上验证失败的依赖代理清单的数量 | `url` |
| `gitlab_memwd_violations_total`                      | Counter | 15.9    | Sidekiq 进程违反内存阈值的总次数                                                                                        | |
| `gitlab_memwd_violations_handled_total`              | Counter | 15.9    | 处理 Sidekiq 进程内存违规的总次数                                                                                      | |
| `sidekiq_watchdog_running_jobs_total`                | Counter | 15.9    | 达到 RSS 限制时当前正在运行的作业                                                                                                            | `worker_class`                                                                                          |

## 数据库负载均衡指标 **(PREMIUM SELF)**

以下指标可用：

| 指标                                                   | 类型      | 引入版本                                                         | 描述                                                                        | Labels                                                                                                                                   |
|:-------------------------------------------------------- |:--------- |:------------------------------------------------------------- |:---------------------------------------------------------------------------------- |:---------------------------------------------------------------------------------------------------------------------------------------- |
| `db_load_balancing_hosts`                                | Gauge     | 12.3   | 当前负载均衡主机数量                                             |                                                                                                                                          |
| `sidekiq_load_balancing_count`                           | Counter   | 13.11                                                         | Sidekiq 作业使用负载均衡并将数据一致性设置为 :sticky 或 :delayed | `queue`, `boundary`, `external_dependencies`, `feature_category`, `job_status`, `urgency`, `data_consistency`, `load_balancing_strategy` |
| `gitlab_transaction_caught_up_replica_pick_count_total`  | Counter   | 14.1                                                          | 追赶副本的搜索尝试次数                                    | `result`                                                                                                                                 |

## 数据库分区指标 **(PREMIUM SELF)**

以下指标可用：

| 指标                                                   | 类型      | 引入版本                                                         | 描述                                                       |
|:--------------------------------- |:--------- |:------------------------------------------------------------- |:----------------------------------------------------------------- |
| `db_partitions_present`           | Gauge     | 13.4  | 存在的数据库分区数                             |
| `db_partitions_missing`           | Gauge     | 13.4  | 当前预期但不存在的数据库分区数 |

## 连接池指标

这些指标记录了数据库[连接池](https://api.rubyonrails.org/classes/ActiveRecord/ConnectionAdapters/ConnectionPool.html)的状态，指标有以下标签：

- `class` - 正在记录的 Ruby 类。
  - `ActiveRecord::Base` 是主数据库连接。
  - `Geo::TrackingBase` 是与 Geo 跟踪数据库的连接（如果启用）。
- `host` - 用于连接数据库的主机名。
- `port` - 用于连接数据库的端口。

| 指标                                        | 类型  | 引入版本 | 描述                                       |
|:----------------------------------------------|:------|:------|:--------------------------------------------------|
| `gitlab_database_connection_pool_size`        | Gauge | 13.0  | 连接池总容量                    |
| `gitlab_database_connection_pool_connections` | Gauge | 13.0  | 池中的当前连接                   |
| `gitlab_database_connection_pool_busy`        | Gauge | 13.0  | 所有者在线的情况下正在使用的连接 |
| `gitlab_database_connection_pool_dead`        | Gauge | 13.0  | 所有者不在线的情况下正在使用的连接   |
| `gitlab_database_connection_pool_idle`        | Gauge | 13.0  | 未使用的连接                            |
| `gitlab_database_connection_pool_waiting`     | Gauge | 13.0  | 当前在此队列中等待的线程           |

## Ruby 指标

一些基本的 Ruby 运行时指标可用：

| 指标                                        | 类型  | 引入版本 | 描述                                       |
|:---------------------------------------- |:--------- |:----- |:----------- |
| `ruby_gc_duration_seconds`               | Counter   | 11.1  | Ruby 在 GC 中花费的时间 |
| `ruby_gc_stat_...`                       | Gauge     | 11.1  | 来自 [GC.stat](https://ruby-doc.org/core-2.6.5/GC.html#method-c-stat) 的各种指标 |
| `ruby_gc_stat_ext_heap_fragmentation`    | Gauge     | 15.2  | Ruby 堆碎片作为活动对象与 eden slots 的程度（范围 0 到 1） |
| `ruby_file_descriptors`                  | Gauge     | 11.1  | 每个进程的文件描述符 |
| `ruby_sampler_duration_seconds`          | Counter   | 11.1  | 收集统计数据所花费的时间 |
| `ruby_process_cpu_seconds_total`         | Gauge     | 12.0  | 每个进程的 CPU 时间总量 |
| `ruby_process_max_fds`                   | Gauge     | 12.0  | 每个进程的最大打开文件描述符数 |
| `ruby_process_resident_memory_bytes`     | Gauge     | 12.0  | 进程的内存使用情况（RSS/驻留集大小） |
| `ruby_process_resident_anon_memory_bytes`| Gauge     | 15.6  | 进程的匿名内存使用情况（RSS/Resident Set Size） |
| `ruby_process_resident_file_memory_bytes`| Gauge     | 15.6  | 进程的文件支持内存使用情况（RSS/Resident Set Size） |
| `ruby_process_unique_memory_bytes`       | Gauge     | 13.0  | 进程的内存使用情况（USS/唯一集大小） |
| `ruby_process_proportional_memory_bytes` | Gauge     | 13.0  | 进程的内存使用情况（PSS/Proportional Set Size） |
| `ruby_process_start_time_seconds`        | Gauge     | 12.0  | 进程开始时间的 UNIX 时间戳 |

## Puma 指标

| 指标                               | 类型  | 引入版本 | 描述                                       |
|:--------------------------------- |:------- |:----- |:----------- |
| `puma_workers`                    | Gauge   | 12.0  | worker 总数 |
| `puma_running_workers`            | Gauge   | 12.0  | booted workers 总数 |
| `puma_stale_workers`              | Gauge   | 12.0  | old workers 总数 |
| `puma_running`                    | Gauge   | 12.0  | 运行线程数 |
| `puma_queued_connections`         | Gauge   | 12.0  | 在 worker 的 “to do” 集中等待 worker 线程的连接数 |
| `puma_active_connections`         | Gauge   | 12.0  | 处理请求的线程数 |
| `puma_pool_capacity`              | Gauge   | 12.0  | workers 现在能够接受的请求数 |
| `puma_max_threads`                | Gauge   | 12.0  | 最大 workers 线程数 |
| `puma_idle_threads`               | Gauge   | 12.0  | 未处理请求的衍生线程数 |
| `puma_killer_terminations_total`  | Gauge   | 12.0  | 被 PumaWorkerKiller 终止的 worker 数量 |

## Redis 指标

这些客户端指标旨在补充 Redis 服务器指标。
这些指标按 Redis 实例细分。
这些指标都有一个 `storage` 标记，指示 Redis 实例（`cache`、`shared_state` 等）。

| 指标                               | 类型  | 引入版本 | 描述                                       |
|:--------------------------------- |:------- |:----- |:----------- |
| `gitlab_redis_client_exceptions_total`                    | Counter   | 13.2  | Redis 客户端异常数，按异常类细分 |
| `gitlab_redis_client_requests_total`                    | Counter   | 13.2  | Redis 客户端请求数 |
| `gitlab_redis_client_requests_duration_seconds`                    | Histogram   | 13.2  | Redis 请求延迟，不包括阻塞命令 |

## 指标共享目录

GitLab Prometheus 客户端需要一个目录来存储多进程服务之间共享的指标数据。
这些文件在 Puma 服务器下运行的所有实例之间共享。
该目录必须可供所有正在运行的 Puma 进程访问，否则指标将无法正常运行。

该目录的位置是使用环境变量 `prometheus_multiproc_dir` 配置的。
为了获得最佳性能，请在 `tmpfs` 中创建此目录。

如果极狐GitLab 使用 [Omnibus GitLab](https://docs.gitlab.com/omnibus/) 安装并且 `tmpfs` 可用，则极狐GitLab 会为您配置指标目录。
