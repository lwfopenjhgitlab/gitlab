---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Redis exporter **(FREE SELF)**

[Redis exporter](https://github.com/oliver006/redis_exporter) 使您能够测量各种 [Redis](https://redis.io) 指标。有关导出内容的更多信息，[阅读上游文档](https://github.com/oliver006/redis_exporter/blob/master/README.md#whats-exported)。

对于从源安装实例，您必须自己安装和配置它。

要启用 Redis exporter：

1. [启用 Prometheus](index.md#configuring-prometheus)。
1. 编辑 `/etc/gitlab/gitlab.rb`。
1. 添加（或查找并取消注释）以下行，确保将其设置为 `true`：

   ```ruby
   redis_exporter['enable'] = true
   ```

1. 保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

Prometheus 开始从暴露在 `localhost:9121` 的 Redis exporter 收集性能数据。
