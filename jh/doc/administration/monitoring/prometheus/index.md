---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用 Prometheus 监控极狐GitLab **(FREE SELF)**

[Prometheus](https://prometheus.io)是一个强大的时序监控服务，为极狐GitLab 等软件产品的监控提供了一个灵活的平台。

极狐GitLab 通过 Prometheus 提供开箱即用的监控，您可轻松访问 GitLab 服务的高质量时间序列监控。

Prometheus 和此页面中列出的各种 exporter 捆绑在 Omnibus GitLab 包中。检查每个 exporter 的文档，了解添加它们的时间线。对于源安装实例，您必须自己安装它们。在后续版本中，会抓取更多的极狐GitLab 指标。

Prometheus 服务默认开启。

Prometheus 及其 exporter 不对用户进行身份验证，任何可以访问它们的人都可以使用它们。

## 概览

Prometheus 通过定期连接到数据源，并通过[各种 exporter](#bundled-software-metrics) 收集其性能指标来工作。要查看和使用监控数据，您可以[直接连接到 Prometheus](#viewing-performance-metrics)，或使用 [Grafana](https://grafana.com) 等仪表盘工具。

<a id="configuring-prometheus"></a>

## 配置 Prometheus

对于源安装实例，您必须自己安装和配置。

Prometheus 及其 exporter 默认开启。
Prometheus 以 `gitlab-prometheus` 用户身份运行，并监听 `http://localhost:9090`。默认情况下，Prometheus 只能从 GitLab 服务器本身访问。
每个 exporter 都会自动设置为 Prometheus 的监控目标，除非单独禁用。

要禁用 Prometheus 及其所有 exporter，以及将来添加的任何 exporter：

1. 编辑 `/etc/gitlab/gitlab.rb`。
1. 添加或查找并取消注释以下行，确保将其设置为 `false`：

   ```ruby
   prometheus_monitoring['enable'] = false
   ```

1. 保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

### 更改 Prometheus 监听的端口和地址

WARNING:
尽管可能，但不建议更改 Prometheus 侦听的端口，因为这可能会影响或与 GitLab 服务器上运行的其他服务发生冲突，继续操作需要您自担风险。

要从 GitLab 服务器外部访问 Prometheus，请更改 Prometheus 侦听的地址/端口：

1. 编辑 `/etc/gitlab/gitlab.rb`。
1. 添加或查找并取消注释以下行：

   ```ruby
   prometheus['listen_address'] = 'localhost:9090'
   ```

   将 `localhost:9090` 替换为您希望 Prometheus 监听的地址或端口。如果您希望允许除 localhost 以外的主机访问 Prometheus，请省略主机，或使用 0.0.0.0 允许公共访问：

   ```ruby
   prometheus['listen_address'] = ':9090'
   # or
   prometheus['listen_address'] = '0.0.0.0:9090'
   ```

1. 保存文件并[重新配置 GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

<a id="adding-custom-scrape-configurations"></a>

### 添加自定义抓取配置

您可以使用 [Prometheus 抓取目标配置](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#%3Cscrape_config%3E)语法。

以下是抓取 `http://1.1.1.1:8060/probe?param_a=test&param_b=additional_test` 的示例配置：

```ruby
prometheus['scrape_configs'] = [
  {
    'job_name': 'custom-scrape',
    'metrics_path': '/probe',
    'params' => {
      'param_a' => ['test'],
      'param_b' => ['additional_test']
    },
    'static_configs' => [
      'targets' => ['1.1.1.1:8060'],
    ],
  },
]
```

### 使用 Omnibus GitLab 的独立 Prometheus

Omnibus GitLab 包可用于配置运行 Prometheus 和 [Grafana](../performance/grafana_configuration.md) 的独立监控节点。

以下步骤是使用 Omnibus GitLab 配置运行 Prometheus 和 Grafana 的监控节点的最低要求：

1. SSH 进入监控节点。
1. [安装](https://gitlab.cn/install/)使用极狐GitLab 下载页面中的 **步骤 1 和 2** 的 Omnibus GitLab 包，但不要按照其余步骤操作。
1. 确保收集 Consul 服务器节点的 IP 地址或 DNS 记录，以进行下一步。
1. 编辑 `/etc/gitlab/gitlab.rb` 并添加内容：

   ```ruby
   roles ['monitoring_role']

   external_url 'http://gitlab.example.com'

   # Prometheus
   prometheus['listen_address'] = '0.0.0.0:9090'
   prometheus['monitor_kubernetes'] = false

   # Grafana
   grafana['enable'] = true
   grafana['admin_password'] = 'toomanysecrets'
   grafana['disable_login_form'] = false

   # Enable service discovery for Prometheus
   consul['enable'] = true
   consul['monitoring_service_discovery'] = true
   consul['configuration'] = {
      retry_join: %w(10.0.0.1 10.0.0.2 10.0.0.3), # The addresses can be IPs or FQDNs
   }

   # Nginx - For Grafana access
   nginx['enable'] = true
   ```

1. 运行 `sudo gitlab-ctl reconfigure` 来编译配置。

下一步是告诉所有其它节点，监控节点在哪里：

1. 编辑 `/etc/gitlab/gitlab.rb`，添加或查找并取消注释以下行：

   ```ruby
   gitlab_rails['prometheus_address'] = '10.0.0.1:9090'
   ```

   其中 10.0.0.1:9090 是 Prometheus 节点的 IP 地址和端口。

1. 保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

在使用 `consul['monitoring_service_discovery'] = true` 启用使用 Service Discovery 进行监视后，请确保在 `/etc/gitlab/gitlab.rb` 中未设置 `prometheus['scrape_configs']`。在 `/etc/gitlab/gitlab.rb` 中同时设置 `consul['monitoring_service_discovery'] = true` 和 `prometheus['scrape_configs']` 会导致错误。

<a id="using-an-external-prometheus-server"></a>

### 使用外部 Prometheus 服务器

WARNING:
Prometheus 和大多数 exporter 不支持身份验证。我们不建议将它们暴露在本地网络之外。

需要进行一些配置更改来允许极狐GitLab 由外部 Prometheus 服务器监控。对于具有多个节点的极狐GitLab 部署实例，建议使用外部服务器。

要使用外部 Prometheus 服务器：

1. 编辑 `/etc/gitlab/gitlab.rb`。
1. 禁用捆绑的 Prometheus：

   ```ruby
   prometheus['enable'] = false
   ```

1. 将每个捆绑服务的 [exporter](#bundled-software-metrics) 设置为侦听网络地址，例如：

   ```ruby
   node_exporter['listen_address'] = '0.0.0.0:9100'
   gitlab_workhorse['prometheus_listen_addr'] = "0.0.0.0:9229"

   # Rails nodes
   gitlab_exporter['listen_address'] = '0.0.0.0'
   gitlab_exporter['listen_port'] = '9168'

   # Sidekiq nodes
   sidekiq['listen_address'] = '0.0.0.0'

   # Redis nodes
   redis_exporter['listen_address'] = '0.0.0.0:9121'

   # PostgreSQL nodes
   postgres_exporter['listen_address'] = '0.0.0.0:9187'

   # Gitaly nodes
   gitaly['prometheus_listen_addr'] = "0.0.0.0:9236"
   ```

1. 如有必要，使用[官方安装说明](https://prometheus.io/docs/prometheus/latest/installation/)，安装并设置专用的 Prometheus 实例。
1. 将 Prometheus 服务器 IP 地址添加到[监控 IP 许可列表](../ip_allowlist.md)。例如：

   ```ruby
   gitlab_rails['monitoring_whitelist'] = ['127.0.0.0/8', '192.168.0.1']
   ```

1. 在**所有** GitLab Rails(Puma, Sidekiq) 服务器上，设置 Prometheus 服务器 IP 地址和监听端口。 例如：

   ```ruby
   gitlab_rails['prometheus_address'] = '192.168.0.1:9090'
   ```

1. 要抓取 NGINX 指标，您还必须配置 NGINX 来允许 Prometheus 服务器 IP。例如：

   ```ruby
   nginx['status']['options'] = {
         "server_tokens" => "off",
         "access_log" => "off",
         "allow" => "192.168.0.1",
         "deny" => "all",
   }
   ```

1. [重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，应用更改。
1. 编辑 Prometheus 服务器的配置文件。
1. 将各个节点的 exporter 添加到 Prometheus 服务器的[抓取目标配置](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#%3Cscrape_config%3E)。例如，使用 `static_configs` 的示例片段：

   ```yaml
   scrape_configs:
     - job_name: nginx
       static_configs:
         - targets:
           - 1.1.1.1:8060
     - job_name: redis
       static_configs:
         - targets:
           - 1.1.1.1:9121
     - job_name: postgres
       static_configs:
         - targets:
           - 1.1.1.1:9187
     - job_name: node
       static_configs:
         - targets:
           - 1.1.1.1:9100
     - job_name: gitlab-workhorse
       static_configs:
         - targets:
           - 1.1.1.1:9229
     - job_name: gitlab-rails
       metrics_path: "/-/metrics"
       scheme: https
       static_configs:
         - targets:
           - 1.1.1.1
     - job_name: gitlab-sidekiq
       static_configs:
         - targets:
           - 1.1.1.1:8082
     - job_name: gitlab_exporter_database
       metrics_path: "/database"
       static_configs:
         - targets:
           - 1.1.1.1:9168
     - job_name: gitlab_exporter_sidekiq
       metrics_path: "/sidekiq"
       static_configs:
         - targets:
           - 1.1.1.1:9168
     - job_name: gitaly
       static_configs:
         - targets:
           - 1.1.1.1:9236
   ```

   WARNING:
   片段中的 `gitlab-rails` 作业假设极狐GitLab 可以通过 HTTPS 访问。如果您的部署不使用 HTTPS，则作业配置将调整为使用 `http` 方案和端口 80。

1. 重新加载 Prometheus 服务器。

<a id="configure-the-storage-retention-size"></a>

### 配置存储保留大小

Prometheus 有几个自定义标志来配置本地存储：

- `storage.tsdb.retention.time`：何时删除旧数据。默认为 `15d`。如果此标志设置为默认值以外的任何值，则覆盖 `storage.tsdb.retention`。
- `storage.tsdb.retention.size`：（实验性）要保留的存储块的最大字节数。首先删除最旧的数据。默认为 `0`（禁用）。此标志是实验性的，可能会在未来的版本中更改。支持的单位：`B`、`KB`、`MB`、`GB`、`TB`、`PB`、`EB`。例如，`512MB`。

要配置存储保留大小：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   prometheus['flags'] = {
     'storage.tsdb.path' => "/var/opt/gitlab/prometheus/data",
     'storage.tsdb.retention.time' => "7d",
     'storage.tsdb.retention.size' => "2GB",
     'config.file' => "/var/opt/gitlab/prometheus/prometheus.yml"
   }
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

## 查看性能指标

您可以访问 Prometheus 默认提供的仪表盘 `http://localhost:9090`。

如果您的极狐GitLab 实例上启用了 SSL，如果由于 HSTS 使用相同的 FQDN，您可能无法在与极狐GitLab 相同的浏览器上访问 Prometheus。

<!--
[A test project exists](https://gitlab.com/gitlab-org/multi-user-prometheus) to provide access via GitLab, but in the interim there are
some workarounds: using a separate FQDN, using server IP, using a separate browser for Prometheus, resetting HSTS, or
having [NGINX proxy it](https://docs.gitlab.com/omnibus/settings/nginx.html#inserting-custom-nginx-settings-into-the-gitlab-server-block).
-->

Prometheus 收集的性能数据可以直接在 Prometheus 控制台中查看，也可以通过兼容的仪表盘工具查看。
Prometheus 界面提供了一种[灵活的查询语言](https://prometheus.io/docs/prometheus/latest/querying/basics/)来处理收集的数据，您可以在其中可视化输出。
对于功能更全的仪表盘，可以使用 Grafana，并且[官方支持 Prometheus](https://prometheus.io/docs/visualization/grafana/)。

Prometheus 查询示例：

- **% Memory available:** `((node_memory_MemAvailable_bytes / node_memory_MemTotal_bytes) or ((node_memory_MemFree_bytes + node_memory_Buffers_bytes + node_memory_Cached_bytes) / node_memory_MemTotal_bytes)) * 100`
- **% CPU utilization:** `1 - avg without (mode,cpu) (rate(node_cpu_seconds_total{mode="idle"}[5m]))`
- **Data transmitted:** `rate(node_network_transmit_bytes_total{device!="lo"}[5m])`
- **Data received:** `rate(node_network_receive_bytes_total{device!="lo"}[5m])`

<a id="prometheus-as-a-grafana-data-source"></a>

## Prometheus 作为 Grafana 数据源

Grafana 允许您将 Prometheus 性能指标作为数据源导入，并将指标呈现为图形和仪表盘，这有助于可视化。

要为单节点极狐GitLab 设置添加 Prometheus 仪表板：

1. 在 Grafana 中新建一个数据源。
1. 命名您的数据源（例如 GitLab）。
1. 在类型下拉框中选择 `Prometheus`。
1. 添加您的 Prometheus 监听地址作为 URL，并设置访问 `Browser`。
1. 将 HTTP 方法设置为 `GET`。
1. 保存并测试您的配置，验证它是否有效。

<a id="gitlab-metrics"></a>

## 极狐GitLab 指标

极狐GitLab 监控自己的内部服务指标，并在 `/-/metrics` 端点提供这些指标。与其他 exporter 不同，此端点需要身份验证，因为它与用户流量在相同的 URL 和端口上可用。

阅读有关[极狐GitLab 指标](gitlab_metrics.md)的更多信息。

<a id="bundled-software-metrics"></a>

## 捆绑软件指标

Omnibus GitLab 中捆绑的许多极狐GitLab 依赖项已预先配置为导出 Prometheus 指标。

### Node exporter

Node exporter 允许您测量各种机器资源，例如内存、磁盘和 CPU 利用率。

[阅读更多关于 node exporter 的内容](node_exporter.md)。

### Web exporter

Web exporter 是一个专用的指标服务器，它允许将最终用户和 Prometheus 流量拆分为两个独立的应用程序，以提高性能和可用性。

[阅读更多关于 web exporter 的内容](web_exporter.md)。

### Redis exporter

Redis exporter 允许您测量各种 Redis 指标。

[阅读更多关于 Redis exporter 的内容](redis_exporter.md)。

### PostgreSQL exporter

PostgreSQL exporter 允许您测量各种 PostgreSQL 指标。

[阅读更多关于 PostgreSQL exporter 的内容](postgres_exporter.md)。

### PgBouncer exporter

PgBouncer exporter 允许您测量各种 PgBouncer 指标。

[阅读更多关于 PgBouncer exporter 的内容](pgbouncer_exporter.md)。

### Registry exporter

Registry exporter 允许您测量各种 Registry 指标。

[阅读更多关于 Registry exporter 的内容](registry_exporter.md)。

### GitLab exporter

GitLab exporter 允许您测量从 Redis 和数据库中提取的各种极狐GitLab 指标。

[阅读更多关于 GitLab exporter 的内容](gitlab_exporter.md)。

## 配置 Prometheus 监控 Kubernetes

如果您的 GitLab 服务器在 Kubernetes 中运行，Prometheus 会从集群中的节点和 [annotated Pods](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#kubernetes_sd_config) 收集指标，包括每个容器的性能数据。 如果您的 CI/CD 环境在同一个集群中运行，这将特别有用，因为您可以使用 Prometheus 项目集成<!--[Prometheus 项目集成](../../../user/project/integrations/prometheus.md)-->来监控它们。

要禁用 Kubernetes 的监控：

1. 编辑 `/etc/gitlab/gitlab.rb`。
1. 添加（或查找并取消注释）以下行并将其设置为 `false`：

   ```ruby
   prometheus['monitor_kubernetes'] = false
   ```

1. 保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

## 故障排查

### `/var/opt/gitlab/prometheus` 占用太多磁盘空间

如果您**不**使用 Prometheus 监控：

1. [禁用 Prometheus](#configuring-prometheus)。
1. 删除 `/var/opt/gitlab/prometheus` 下的数据。

如果您使用 Prometheus 监控：

1. 停止 Prometheus（在运行时删除数据会导致数据损坏）：

   ```shell
   gitlab-ctl stop prometheus
   ```

1. 删除`/var/opt/gitlab/prometheus/data`下的数据。
1. 再次启动服务：

   ```shell
   gitlab-ctl start prometheus
   ```

1. 验证服务是否已启动并正在运行：

   ```shell
   gitlab-ctl status prometheus
   ```

1. 可选。[配置存储保留大小](index.md#configure-the-storage-retention-size)。

### 监控节点未收到数据

如果监控节点没有收到任何数据，请检查导出器是否正在捕获数据：

```shell
curl "http[s]://localhost:<EXPORTER LISTENING PORT>/metrics"
```

或者

```shell
curl "http[s]://localhost:<EXPORTER LISTENING PORT>/-/metrics"
```
