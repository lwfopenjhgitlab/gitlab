---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# PostgreSQL Server Exporter **(FREE SELF)**

[PostgreSQL Server Exporter](https://github.com/prometheus-community/postgres_exporter) 允许您导出各种 PostgreSQL 指标。

对于从源安装实例，您必须自己安装和配置它。

要启用 PostgreSQL Server Exporter：

1. [启用 Prometheus](index.md#configuring-prometheus)。
1. 编辑 `/etc/gitlab/gitlab.rb` 并启用 `postgres_exporter`：

   ```ruby
   postgres_exporter['enable'] = true
   ```

   如果 PostgreSQL Server Exporter 配置在单独的节点上，请确保本地地址列在 `trust_auth_cidr_addresses` 中，否则 Exporter 无法连接到数据库。

1. 保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

Prometheus 开始从 `localhost:9187` 下暴露的 PostgreSQL Server Exporter 收集性能数据。

## 高级配置

在大多数情况下，PostgreSQL Server Exporter 使用默认值，您不需要更改任何内容。要进一步自定义，请使用以下配置选项：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   # The name of the database to connect to.
   postgres_exporter['dbname'] = 'pgbouncer'
   # The user to sign in as.
   postgres_exporter['user'] = 'gitlab-psql'
   # The user's password.
   postgres_exporter['password'] = ''
   # The host to connect to. Values that start with '/' are for unix domain sockets
   # (default is 'localhost').
   postgres_exporter['host'] = 'localhost'
   # The port to bind to (default is '5432').
   postgres_exporter['port'] = 5432
   # Whether or not to use SSL. Valid options are:
   #   'disable' (no SSL),
   #   'require' (always use SSL and skip verification, this is the default value),
   #   'verify-ca' (always use SSL and verify that the certificate presented by
   #   the server was signed by a trusted CA),
   #   'verify-full' (always use SSL and verify that the certification presented
   #   by the server was signed by a trusted CA and the server host name matches
   #   the one in the certificate).
   postgres_exporter['sslmode'] = 'require'
   # An application_name to fall back to if one isn't provided.
   postgres_exporter['fallback_application_name'] = ''
   # Maximum wait for connection, in seconds. Zero or not specified means wait indefinitely.
   postgres_exporter['connect_timeout'] = ''
   # Cert file location. The file must contain PEM encoded data.
   postgres_exporter['sslcert'] = 'ssl.crt'
   # Key file location. The file must contain PEM encoded data.
   postgres_exporter['sslkey'] = 'ssl.key'
   # The location of the root certificate file. The file must contain PEM encoded data.
   postgres_exporter['sslrootcert'] = 'ssl-root.crt'
   ```

1. 保存文件并[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。
