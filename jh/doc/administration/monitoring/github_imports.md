---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 监控 GitHub 导入 **(FREE SELF)**

GitHub 导入器公开了各种 Prometheus 指标，您可以使用这些指标来监控导入器的运行状况和进度。

## 导入持续时间

| 名称                                      | 类型       |
|------------------------------------------|-----------|
| `github_importer_total_duration_seconds` | histogram |

该指标跟踪每个导入项目（从项目创建到导入过程完成）所花费的总时间（以秒为单位）。
项目名称以 `namespace/name` 格式存储在 `project` 标记中（例如 `gitlab-cn/gitlab`）。

## 导入项目数

| 名称                                | 类型     |
|-------------------------------------|---------|
| `github_importer_imported_projects` | counter |

该指标跟踪一段时间内导入的项目总数。该指标不公开任何标记。

## GitHub API 调用次数

| 名称                             | 类型     |
|---------------------------------|---------|
| `github_importer_request_count` | counter |

该指标跟踪所有项目在一段时间内执行的 GitHub API 调用总数。该指标不公开任何标记。

## 速率限制错误

| 名称                               | 类型     |
|-----------------------------------|---------|
| `github_importer_rate_limit_hits` | counter |

该指标跟踪我们达到 GitHub 速率限制的所有项目的次数。该指标不公开任何标记。

## 导入议题数

| 名称                               | 类型    |
|-----------------------------------|---------|
| `github_importer_imported_issues` | counter |

该指标跟踪所有项目中导入议题的数量。

项目名称以 `namespace/name` 格式存储在 `project` 标记中（例如 `gitlab-cn/gitlab`）。

## 导入的拉取请求数

| 名称                                      | 类型    |
|------------------------------------------|---------|
| `github_importer_imported_pull_requests` | counter |

该指标跟踪所有项目中导入的拉取请求的数量。

项目名称以 `namespace/name` 格式存储在 `project` 标记中（例如 `gitlab-cn/gitlab`）。

## 导入评论数

| 名称                              | 类型    |
|----------------------------------|---------|
| `github_importer_imported_notes` | counter |

该指标跟踪所有项目中导入评论的数量。

项目名称以 `namespace/name` 格式存储在 `project` 标记中（例如 `gitlab-cn/gitlab`）。

## 导入的拉取请求审核评论数

| 名称                                   | 类型    |
|---------------------------------------|---------|
| `github_importer_imported_diff_notes` | counter |

该指标跟踪所有项目中导入评论的数量。

项目名称以 `namespace/name` 格式存储在 `project` 标记中（例如 `gitlab-cn/gitlab`）。

## 导入的仓库数量

| 名称                                    | 类型     |
|-----------------------------------------|---------|
| `github_importer_imported_repositories` | counter |

该指标跟踪所有项目中导入的仓库的数量。该指标不公开任何标记。
