---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Kroki 图表 **(FREE SELF)**

> - 引入于 13.7 版本
> - 对 reStructuredText 和 Textile 文档的支持引入于 13.12

在极狐GitLab 中启用并配置 [Kroki](https://kroki.io) 集成后，您可以使用它在 AsciiDoc、Markdown、reStructuredText 和 Textile 文档中创建图表。

## Kroki 服务器

启用 Kroki 后，极狐GitLab 会将图表发送到 Kroki 实例，来将它们显示为图像。
您可以使用免费的公共云实例 `https://kroki.io`，也可以在自己的基础架构上[安装 Kroki](https://docs.kroki.io/kroki/setup/install/)。
安装 Kroki 后，请确保更新服务器 URL 指向您的实例。

### Docker

使用 Docker，通过以下命令运行一个容器：

```shell
docker run -d --name kroki -p 8080:8000 yuzutech/kroki
```

**Kroki URL** 是运行容器的服务器的主机名。

[`yuzutech/kroki`](https://hub.docker.com/r/yuzutech/kroki) 镜像包含以下开箱即用的图表库：

<!-- vale gitlab.Spelling = NO -->

- [Bytefield](https://bytefield-svg.deepsymmetry.org/)
- [Ditaa](https://ditaa.sourceforge.net)
- [Erd](https://github.com/BurntSushi/erd)
- [GraphViz](https://www.graphviz.org/)
- [Nomnoml](https://github.com/skanaar/nomnoml)
- [PlantUML](https://github.com/plantuml/plantuml)
  - [C4 model](https://github.com/RicardoNiepel/C4-PlantUML) (with PlantUML)
- [Svgbob](https://github.com/ivanceras/svgbob)
- [UMlet](https://github.com/umlet/umlet)
- [Vega](https://github.com/vega/vega)
- [Vega-Lite](https://github.com/vega/vega-lite)
- [WaveDrom](https://wavedrom.com/)

<!-- vale gitlab.Spelling = YES -->

如果您想使用其他图表库，请阅读 [Kroki 安装](https://docs.kroki.io/kroki/setup/install/#_images)，了解如何启动 Kroki 配套容器。

## 在极狐GitLab 中启用 Kroki

您需要从管理中心的设置中启用 Kroki 集成。
请使用管理员帐户登录并按照以下步骤操作：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 转到 **设置 > 通用**。
1. 展开 **Kroki** 部分。
1. 选中 **启用 Kroki** 复选框。
1. 输入 **Kroki URL**。

## 创建图表

启用和配置 Kroki 集成后，您可以开始使用分隔块，将图表添加到 AsciiDoc 或 Markdown 文档中：

- **Markdown**

  ````markdown
  ```plantuml
  Bob -> Alice : hello
  Alice -> Bob : hi
  ```
  ````

- **AsciiDoc**

  ```plaintext
  [plantuml]
  ....
  Bob->Alice : hello
  Alice -> Bob : hi
  ....
  ```

- **reStructuredText**

  ```plaintext
  .. code-block:: plantuml

    Bob->Alice : hello
    Alice -> Bob : hi
  ```

- **Textile**

  ```plaintext
  bc[plantuml]. Bob->Alice : hello
  Alice -> Bob : hi
  ```

上面的代码块被转换为一个 HTML 图像标签，其中 source 指向 Kroki 实例。如果正确配置了 Kroki 服务器，应该会呈现一个漂亮的图表而不是代码块：

![PlantUML diagram](../img/kroki_plantuml_diagram.png)

Kroki 支持十几个图表库。以下是 AsciiDoc 的一些示例：

**GraphViz**

```plaintext
[graphviz]
....
digraph finite_state_machine {
  rankdir=LR;
  node [shape = doublecircle]; LR_0 LR_3 LR_4 LR_8;
  node [shape = circle];
  LR_0 -> LR_2 [ label = "SS(B)" ];
  LR_0 -> LR_1 [ label = "SS(S)" ];
  LR_1 -> LR_3 [ label = "S($end)" ];
  LR_2 -> LR_6 [ label = "SS(b)" ];
  LR_2 -> LR_5 [ label = "SS(a)" ];
  LR_2 -> LR_4 [ label = "S(A)" ];
  LR_5 -> LR_7 [ label = "S(b)" ];
  LR_5 -> LR_5 [ label = "S(a)" ];
  LR_6 -> LR_6 [ label = "S(b)" ];
  LR_6 -> LR_5 [ label = "S(a)" ];
  LR_7 -> LR_8 [ label = "S(b)" ];
  LR_7 -> LR_5 [ label = "S(a)" ];
  LR_8 -> LR_6 [ label = "S(b)" ];
  LR_8 -> LR_5 [ label = "S(a)" ];
}
....
```

![GraphViz diagram](../img/kroki_graphviz_diagram.png)

**C4 (based on PlantUML)**

```plaintext
[c4plantuml]
....
@startuml
!include C4_Context.puml

title System Context diagram for Internet Banking System

Person(customer, "Banking Customer", "A customer of the bank, with personal bank accounts.")
System(banking_system, "Internet Banking System", "Allows customers to check their accounts.")

System_Ext(mail_system, "E-mail system", "The internal Microsoft Exchange e-mail system.")
System_Ext(mainframe, "Mainframe Banking System", "Stores all of the core banking information.")

Rel(customer, banking_system, "Uses")
Rel_Back(customer, mail_system, "Sends e-mails to")
Rel_Neighbor(banking_system, mail_system, "Sends e-mails", "SMTP")
Rel(banking_system, mainframe, "Uses")
@enduml
....
```

![C4 PlantUML diagram](../img/kroki_c4_diagram.png)

<!-- vale gitlab.Spelling = NO -->

**Nomnoml**

<!-- vale gitlab.Spelling = YES -->

```plaintext
[nomnoml]
....
[Pirate|eyeCount: Int|raid();pillage()|
  [beard]--[parrot]
  [beard]-:>[foul mouth]
]

[<abstract>Marauder]<:--[Pirate]
[Pirate]- 0..7[mischief]
[jollyness]->[Pirate]
[jollyness]->[rum]
[jollyness]->[singing]
[Pirate]-> *[rum|tastiness: Int|swig()]
[Pirate]->[singing]
[singing]<->[rum]
....
```

![Diagram](../img/kroki_nomnoml_diagram.png)
