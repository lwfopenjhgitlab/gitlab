---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, howto
---

# PlantUML 和极狐GitLab **(FREE)**

在极狐GitLab 中启用和配置 [PlantUML](https://plantuml.com) 集成后，您可以在片段、wiki 和仓库中创建图表。此集成为所有 SaaS 用户启用，不需要任何额外配置。

在私有化部署实例上设置集成，您必须：

1. [配置您的 PlantUML 服务器](#configure-your-plantuml-server)。
1. [配置本地 PlantUML 访问](#configure-local-plantuml-access)。
1. [配置 PlantUML 安全](#configure-plantuml-security)。
1. [启用集成](#enable-plantuml-integration)。

完成集成后，PlantUML 将 `plantuml` 代码块转换为 HTML 图像标签，源指向 PlantUML 实例。PlantUML 图表分隔符 `@startuml`/`@enduml` 不是必需的，它们被 `plantuml` 代码块替换：

- **Markdown** 文件，扩展名为 `.md`：

  ````markdown
  ```plantuml
  Bob -> Alice : hello
  Alice -> Bob : hi
  ```
  ````

  有关其它可接受的扩展名，请查看 [`languages.yaml`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/vendor/languages.yml#L3174) 文件。

- **AsciiDoc** 文件，扩展名为 `.asciidoc`、`.adoc` 或 `.asc`：

  ```plaintext
  [plantuml, format="png", id="myDiagram", width="200px"]
  ----
  Bob->Alice : hello
  Alice -> Bob : hi
  ----
  ```

- **reStructuredText**

  ```plaintext
  .. plantuml::
     :caption: Caption with **bold** and *italic*

     Bob -> Alice: hello
     Alice -> Bob: hi
  ```

   尽管您可以使用 `uml::` 指令来与 [`sphinxcontrib-plantuml`](https://pypi.org/project/sphinxcontrib-plantuml/) 兼容，但极狐GitLab 仅支持 `caption` 选项。

如果正确配置了 PlantUML 服务器，以下示例应该呈现图表而不是代码块：

```plantuml
Bob -> Alice : hello
Alice -> Bob : hi
```

在代码块内，您可以添加 PlantUML 支持的任何图表，例如：

- [Activity](https://plantuml.com/activity-diagram-legacy)
- [Class](https://plantuml.com/class-diagram)
- [Component](https://plantuml.com/component-diagram)
- [Object](https://plantuml.com/object-diagram)
- [Sequence](https://plantuml.com/sequence-diagram)
- [State](https://plantuml.com/state-diagram)
- [Use Case](https://plantuml.com/use-case-diagram)

您可以将参数添加到代码块定义：

- `format`：可以是`png`（默认）或`svg`。谨慎使用 `svg`，因为并非所有浏览器都支持，Markdown 也不支持。
- `id`：添加到图表 HTML 标签的 CSS ID。
- `width`：添加到图像标签的宽度属性。
- `height`：添加到图像标签的高度属性。

Markdown 不支持任何参数，始终使用 PNG 格式。

<a id="configure-your-plantuml-server"></a>

## 配置您的 PlantUML 服务器

在极狐GitLab 中启用 PlantUML 之前，请设置您自己的 PlantUML 服务器来生成图表：

- [在 Docker 中](#docker).
- [在 Debian/Ubuntu 中](#debianubuntu).

### Docker

要在 Docker 中运行 PlantUML 容器，请运行以下命令：

```shell
docker run -d --name plantuml -p 8080:8080 plantuml/plantuml-server:tomcat
```

**PlantUML URL** 是运行容器的服务器的主机名。

在 Docker 中运行极狐GitLab 时，它必须有权访问 PlantUML 容器。
为此，请使用 [Docker Compose](https://docs.docker.com/compose/)。
在下面基本的 docker-compose.yml 文件中，极狐GitLab 可以通过 URL `http://plantuml:8080/` 访问 PlantUML：

```yaml
version: "3"
services:
  gitlab:
    image: 'gitlab/gitlab-ee:12.2.5-ee.0'
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        nginx['custom_gitlab_server_config'] = "location /-/plantuml/ { \n    proxy_cache off; \n    proxy_pass  http://plantuml:8080/; \n}\n"

  plantuml:
    image: 'plantuml/plantuml-server:tomcat'
    container_name: plantuml
```

### Debian/Ubuntu

您可以使用 Tomcat 在 Debian/Ubuntu 发行版中安装和配置 PlantUML 服务器：

1. 运行以下命令，从源代码创建一个`plantuml.war` 文件：

   ```shell
   sudo apt-get install graphviz openjdk-8-jdk git-core maven
   git clone https://github.com/plantuml/plantuml-server.git
   cd plantuml-server
   mvn package
   ```

1. 使用以下命令部署上一步中的 `.war` 文件：

   ```shell
   sudo apt-get install tomcat8
   sudo cp target/plantuml.war /var/lib/tomcat8/webapps/plantuml.war
   sudo chown tomcat8:tomcat8 /var/lib/tomcat8/webapps/plantuml.war
   sudo service tomcat8 restart
   ```

Tomcat 服务应该重新启动。 重启完成后，PlantUML 服务已准备就绪并正在侦听端口 8080 上的请求：`http://localhost:8080/plantuml`

要更改这些默认值，请编辑 `/etc/tomcat8/server.xml` 文件。

NOTE:
使用此方法时，默认 URL 不同。基于 Docker 的镜像使服务在根 URL 处可用，没有相对路径。相应地调整下面的配置。

<a id="configure-local-plantuml-access"></a>

## 配置本地 PlantUML 访问

PlantUML 服务器在您的服务器上本地运行，因此默认情况下无法从外部访问。您的服务器必须捕获对 `https://gitlab.example.com/-/plantuml/` 的外部 PlantUML 调用，并将它们重定向到本地 PlantUML 服务器。根据您的设置，URL 为以下之一：

- `http://plantuml:8080/`
- `http://localhost:8080/plantuml/`

如果您使用 TLS 运行极狐GitLab，则必须配置此重定向，因为 PlantUML 使用不安全的 HTTP 协议。
[Google Chrome 86+](https://www.chromestatus.com/feature/4926989725073408) 等较新的浏览器不会在通过 HTTPS 提供的页面上加载不安全的 HTTP 资源。

启用重定向：

1. 根据您的设置方法，在 `/etc/gitlab/gitlab.rb` 中添加以下行：

   ```ruby
   # Docker deployment
   nginx['custom_gitlab_server_config'] = "location /-/plantuml/ { \n    proxy_cache off; \n    proxy_pass  http://plantuml:8080/; \n}\n"

   # Built from source
   nginx['custom_gitlab_server_config'] = "location /-/plantuml { \n rewrite ^/-/(plantuml.*) /$1 break;\n proxy_cache off; \n proxy_pass http://localhost:8080/plantuml; \n}\n"
   ```

1. 要激活更改，请运行以下命令：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

<a id="configure-plantuml-security"></a>

### 配置 PlantUML 安全

PlantUML 具有允许获取网络资源的功能。如果您自托管 PlantUML 服务器，请设置网络控制来将其隔离。

```plaintext
@startuml
start
    ' ...
    !include http://localhost/
stop;
@enduml
```

<a id="enable-plantuml-integration"></a>

## 启用 PlantUML 集成

配置本地 PlantUML 服务器后，您就可以启用 PlantUML 集成：

1. 以管理员用户身份登录极狐GitLab。
1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，转到 **设置 > 通用** 并展开 **PlantUML** 部分。
1. 选中 **启用 PlantUML** 复选框。
1. 将 PlantUML 实例设置为 `https://gitlab.example.com/-/plantuml/`，然后点击 **保存修改**。

- 对于运行 v1.2020.9 及更高版本的 PlantUML 服务器，例如 [plantuml.com](https://plantuml.com)，您必须设置 `PLANTUML_ENCODING` 环境变量来启用 `deflate` 压缩。在 Omnibus GitLab 中，您可以使用以下命令在 `/etc/gitlab.rb` 中设置此值：

  ```ruby
   gitlab_rails['env'] = { 'PLANTUML_ENCODING' => 'deflate' }
   ```

  在 GitLab Helm chart 中，您可以将变量添加到 [global.extraEnv](https://jihulab.com/gitlab-cn/charts/gitlab/blob/master/doc/charts/globals.md#extraenv ) 部分，如下所示：

  ```yaml
  global:
  extraEnv:
    PLANTUML_ENCODING: deflate
  ```

- 对于极狐GitLab 13.1 及更高版本，PlantUML 集成现在[需要 URL 中的 header前缀](https://github.com/plantuml/plantuml/issues/117#issuecomment-6235450160)，区分不同的编码类型。
