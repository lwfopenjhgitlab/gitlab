---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# Mailgun 和极狐GitLab **(FREE SELF)**

当您使用 Mailgun 为您的极狐GitLab 实例发送电子邮件并在极狐GitLab 中启用和配置 [Mailgun](https://www.mailgun.com/) 集成时，您可以收到他们的 webhook 以跟踪传递失败。要设置集成，您必须：

1. [配置您的 Mailgun 域名](#configure-your-mailgun-domain)。
1. [启用 Mailgun 集成](#enable-mailgun-integration)。

完成集成后，Mailgun `temporary_failure` 和 `permanent_failure` webhook 将发送到您的极狐GitLab 实例。

<a id="configure-your-mailgun-domain"></a>

## 配置您的 Mailgun 域名

> - `/-/members/mailgun/permanent_failures` URL 废弃于 15.0 版本。
> - 处理临时和永久故障的 URL 添加于 15.0 版本。

在极狐GitLab 中启用 Mailgun 之前，请设置您自己的 Mailgun 端点以接收 webhook。

使用 [Mailgun webhook 指南](https://www.mailgun.com/blog/product/a-guide-to-using-mailguns-webhooks/)：

1. 添加一个将 **事件类型** 设置为 **永久失败** 的 webhook。
1. 输入您的实例的 URL 并包含 `/-/mailgun/webhooks` 路径。

   例如：

   ```plaintext
   https://myinstance.gitlab.com/-/mailgun/webhooks
   ```

1. 添加另一个 webhook，将 **事件类型** 设置为 **永久失败**。
1. 输入您的实例的 URL 并使用相同的 `/-/mailgun/webhooks` 路径。

<a id="enable-mailgun-integration"></a>

## 启用 Mailgun 集成

为 webhook 端点配置 Mailgun 域名后，您就可以启用 Mailgun 集成了：

1. 以[管理员](../../user/permissions.md)用户身份登录 GitLab。
1. 在顶部栏，选择 **主菜单 >** **{admin}** **管理员**。
1. 在左侧边栏，转到 **设置 > 通用** 并展开 **Mailgun** 部分。
1. 选中 **启用 Mailgun** 复选框。
1. 输入 Mailgun HTTP webhook 签名密钥，如 [Mailgun 文档](https://documentation.mailgun.com/en/latest/user_manual.html#webhooks-1) 中所述，并显示在您的 Mailgun 帐户的 [API 安全](`https://app.mailgun.com/app/account/security/api_keys`)部分。
1. 选择 **保存更改**。
