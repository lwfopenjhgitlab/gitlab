---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Web 终端（已废弃） **(FREE)**

> - 废弃于 14.5 版本。
> - 在私有化部署版上禁用于 15.0 版本。

WARNING:
此功能废弃于 14.5 版本。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，请求管理员[启用功能标志](../../administration/feature_flags.md) `certificate_based_clusters`。

- 阅读有关未弃用的[可通过 Web IDE 访问的 Web 终端](../../user/project/web_ide/index.md)的更多信息。
- 阅读有关未弃用的[可从正在运行的 CI 作业访问的 Web 终端](../../ci/interactive_web_terminal/index.md)的更多信息。

---

随着 [Kubernetes 集成](../../user/infrastructure/clusters/index.md)的引入，极狐GitLab 可以存储和使用 Kubernetes 集群的凭据。极狐GitLab 使用这些凭据提供对环境的 [web 终端](../../ci/environments/index.md#web-terminals-deprecated)的访问。

NOTE:
只有至少具有项目的[维护者角色](../../user/permissions.md)的用户才能访问 Web 终端。

## 工作原理

可以在本文档中找到有关 Web 终端架构及其工作原理的详细概述。
简单来说：

- 极狐GitLab 依赖用户提供他们自己的 Kubernetes 凭证，并在部署时适当地标记他们创建的 Pod。
- 当用户导航到某个环境的终端页面时，会为他们提供一个 JavaScript 应用程序，该应用程序打开一个返回到极狐GitLab 的 WebSocket 连接。
- WebSocket 在 Workhorse 中处理，而不是在 Rails 应用程序服务器中处理。
- Workhorse 向 Rails 查询连接细节和用户权限。 Rails 使用 Sidekiq 在后台向 Kubernetes 查询它们。
- Workhorse 充当用户浏览器和 Kubernetes API 之间的代理服务器，在两者之间传递 WebSocket 帧。
- Workhorse 定期轮询 Rails，如果用户不再有权访问终端，或者连接详细信息已更改，则终止 WebSocket 连接。

## 安全

极狐GitLab 和[极狐GitLab Runner](https://docs.gitlab.cn/runner/) 采取了一些预防措施，以保持它们之间的交互式 Web 终端数据加密，并且所有内容都受到授权保护。

- 除非配置了 `[session_server]`，否则交互式 Web 终端将完全禁用。
- 每次运行程序启动时，它都会生成一个 `x509` 证书，用于 `wss`（Web Socket Secure）连接。
- 对于每个创建的作业，都会生成一个随机 URL，该 URL 在作业结束时被丢弃。此 URL 用于建立 Web Socket 连接。会话的 URL 格式为 `(IP|HOST):PORT/session/$SOME_HASH`，其中 `IP/HOST` 和 `PORT` 是配置的 `listen_address`。
- 创建的每个会话 URL 都有一个需要发送的授权 header，以建立 `wss` 连接。
- 会话 URL 不会以任何方式向用户公开。极狐GitLab 在内部保存所有状态并相应地代理。

## 启用和禁用终端支持

NOTE:
AWS Classic 负载均衡器 (CLB) 不支持 Web Socket。
如果您希望 Web 终端正常工作，请使用 AWS 网络负载均衡器（NLB）。
阅读 [AWS Elastic Load Balancing 产品比较](https://aws.amazon.com/elasticloadbalancing/features/#compare)，了解更多信息。

由于 Web 终端使用 WebSockets，Workhorse 前面的每个 HTTP/HTTPS 反向代理都必须配置为将 `Connection` 和 `Upgrade` header 传递到链中的下一个 header。极狐GitLab 默认配置为这样做。

但是，如果您在极狐GitLab 前面运行[负载均衡器](../load_balancer.md)，您可能需要对配置进行一些更改。这些指南记录了选择流行的反向代理的必要步骤：

- [Apache](https://httpd.apache.org/docs/2.4/mod/mod_proxy_wstunnel.html)
- [NGINX](https://www.nginx.com/blog/websocket-nginx/)
- [HAProxy](https://www.haproxy.com/blog/websockets-load-balancing-with-haproxy/)
- [Varnish](https://varnish-cache.org/docs/4.1/users-guide/vcl-example-websockets.html)

Workhorse 不允许 WebSocket 请求通过非 WebSocket 端点，因此全局启用对这些标头的支持是安全的。如果您喜欢更窄的规则集，可以将其限制为以 `/terminal.ws` 结尾的 URL。
这种方法可能仍会导致一些误报。

<!--
If you installed from source, or have made any configuration changes to your
Omnibus installation before upgrading to 8.15, you may need to make some changes
to your configuration. Read
[Upgrading Community Edition and Enterprise Edition from source](../../update/upgrading_from_source.md#nginx-configuration)
for more details.
-->

要在极狐GitLab 中禁用 Web 终端支持，请停止在链中的*首个* HTTP 反向代理中传递 `Connection` 和 `Upgrade` hop-by-hop headers。对于大多数用户来说，这是与 Omnibus GitLab 捆绑在一起的 NGINX 服务器，在这种情况下，您需要：

- 找到 `gitlab.rb` 文件的 `nginx['proxy_set_headers']` 部分
- 确保整个块未注释，然后注释掉或删除 `Connection` 和 `Upgrade` 行。

对于您自己的负载均衡器，只需反转上述指南推荐的配置更改即可。

当这些 headers 未通过时，Workhorse 会向尝试使用 Web 终端的用户返回 `400 Bad Request` 响应。反过来，他们会收到 `Connection failed` 消息。

## 限制 WebSocket 连接时间

默认情况下，终端会话不会过期。要限制极狐GitLab 实例中的终端会话生命周期：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 选择[**设置 > Web 终端**](../../user/admin_area/settings/index.md#general)。
1. 设置**最大会话时间**。
