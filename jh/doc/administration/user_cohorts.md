---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 用户群 **(FREE SELF)**

您可以随时间分析用户在极狐GitLab 上的活动。

## 概览

您如何解释用户群表格？让我们回顾一个包含以下用户群的示例：

![User cohort example](img/cohorts_v13_9_a.png)

对于 2020 年 3 月的同类用户群，三个用户已添加到此服务器，并且自本月以来一直处于活跃状态。一个月后（2020 年 4 月），两个用户仍然活跃。五个月后（2020 年 8 月），用户群中的一名用户仍然活跃，占 3 月份加入的 33%。

**未激活用户** 列显示当月添加但从未在实例中进行任何活动的用户数。

我们如何衡量用户活动？在以下情况，极狐GitLab 认为用户处于活动状态：

- 用户登录。
- 用户有 Git 活动（无论是推送还是拉取）。
- 用户访问与仪表盘、项目、议题或合并请求相关的页面。
- 用户使用 API。
- 用户使用 GraphQL API。

## 查看用户人群

要查看用户人群：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 选择 **人群** 选项卡。
