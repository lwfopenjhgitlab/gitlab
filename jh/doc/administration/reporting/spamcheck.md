---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Spamcheck 反垃圾邮件服务 **(FREE SELF)**

> 引入于 14.8 版本

<!--
WARNING:
Spamcheck is available to all tiers, but only on instances using GitLab Enterprise Edition (EE). For [licensing reasons](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/6259#note_726605397), it is not included in the GitLab Community Edition (CE) package. You can [migrate from CE to EE](../../../update/package/convert_to_ee.md).
-->

WARNING:
此功能不适用于社区版。

Spamcheck 是一个反垃圾邮件引擎，最初是为了对抗 SaaS 中不断增加的垃圾邮件，后来公开用于私有化部署实例。

## 启用 Spamcheck

Spamcheck 仅适用于基于软件包的安装实例：

1. 编辑 `/etc/gitlab/gitlab.rb` 并启用 Spamcheck：

   ```ruby
   spamcheck['enable'] = true
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

1. 验证新服务 `spamcheck` 和 `spam-classifier` 是否已启动并正在运行：

   ```shell
   sudo gitlab-ctl status
   ```

## 配置极狐GitLab 使用 Spamcheck

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 报告**。
1. 展开 **垃圾信息及防机器人保护**。
1. 更新垃圾邮件检查设置：
    1. 选中 “通过外部 API 端点启用垃圾信息检查” 复选框。
    1. 对于 **外部垃圾信息检查端点 URL**，使用 `grpc://localhost:8001`。
    1. 将 **垃圾邮件检查 API 密钥** 留空。
1. 选择 **保存更改**。

NOTE:
在单节点实例中，Spamcheck 在`localhost` 上运行，因此以未经身份验证的模式运行。如果极狐GitLab 在一台服务器上运行，而 Spamcheck 在另一台服务器上运行，并侦听公共端点的多节点实例上，建议在 Spamcheck 服务前使用反向代理强制执行某种身份验证，该服务可与 API 密钥。一个示例是为此使用 `JWT` 身份验证并指定不记名令牌作为 API 密钥。

<!--
[Native authentication for Spamcheck is in the works](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/-/issues/171).
-->

## 通过 TLS 运行垃圾邮件检查

Spamcheck 服务本身无法通过 TLS 与极狐GitLab 直接通信。
但是，Spamcheck 可以部署在执行 TLS 终止的反向代理之后。在这种情况下，可以通过在管理设置中为外部 Spamcheck URL 指定 `tls://` 方案而不是 `grpc://` 来使极狐GitLab 通过 TLS 与 Spamcheck 通信。
