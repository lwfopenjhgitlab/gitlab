---
stage: Anti-Abuse
group: Anti-Abuse
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Git 滥用速率限制（管理员） **(ULTIMATE SELF)**

> - 引入于 15.2 版本，[功能标志](../../administration/feature_flags.md) 为 `git_abuse_rate_limit_feature_flag`，默认禁用。
> - 一般可用于 15.10 版本。删除功能标志 `git_abuse_rate_limit_feature_flag`。

这是一个管理员文档。有关群组级别的 Git 滥用率限制的信息，请参阅[群组级别文档](../../user/group/reporting/git_abuse_rate_limit.md)。

Git 滥用率限制可以自动封禁在特定的时间范围内，在实例中的任何项目中下载、克隆或派生仓库超过指定次数的用户。被封禁的用户无法登录实例，也无法通过 HTTP 或 SSH 访问任何非公开群组。速率限制也适用于使用[个人](../../user/profile/personal_access_tokens.md)或[群组](../../user/group/settings/group_access_tokens.md)访问令牌。

Git 滥用率限制不适用于实例管理员、[部署令牌](../../user/project/deploy_tokens/index.md)或[部署密钥](../../user/project/deploy_keys/index.md)。

## 配置 Git 滥用速率限制

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 报告**。
1. 展开 **Git 滥用速率限制**。
1. 更新 Git 滥用速率限制设置：
    1. 在 **仓库数** 字段中输入一个大于或等于 `0` 且小于或等于 `10,000` 的数字。此数字指定用户在被封禁之前，在指定时间段内可以下载仓库的最大数量。当设置为 `0` 时，Git 滥用速率限制被禁用。
    1. 在 **报告时间段（秒）** 字段中输入一个大于或等于 `0` 且小于或等于 `86,400`（10 天）的数字。此数字指定用户在被封禁之前，可以下载最大数量的仓库的时间（以秒为单位）。当设置为 `0` 时，Git 滥用速率限制被禁用。
    1. 可选。通过将用户添加到 **排除的用户** 字段来排除用户（最多 100 个）。被排除的用户不会被自动封禁。
    1. 将用户添加到 **发送通知到** 字段，支持最多 100 个用户。您必须至少选择一个用户。默认选择所有应用程序管理员。
    1. 可选。打开 **当用户超过指定限制时自动禁止用户进入此命名空间**，启用自动封禁。
1. 选择 **保存更改**。

## 自动封禁通知

如果禁用自动封禁，当用户超过限制时不会自动封禁用户，但仍会发送通知给 **发送通知到** 字段下列出的用户。在启用自动封禁之前，您可以使用此设置，来帮助确定速率限制设置的正确值。

如果启用了自动封禁，当用户即将被封禁时，系统会发送邮件通知并自动将用户在极狐GitLab 实例中封禁。

## 解禁用户

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 选择 **已封禁** 选项卡，并搜索您要解禁的用户帐户。
1. 从 **用户管理** 下拉列表中选择 **解禁用户**。
1. 在确认对话框中，选择 **解禁用户**。
