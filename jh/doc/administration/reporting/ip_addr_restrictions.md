---
stage: Anti-Abuse
group: Anti-Abuse
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# IP 地址限制 **(FREE SELF)**

IP 地址限制有助于防止恶意用户将其活动隐藏在多个 IP 地址后面。

极狐GitLab 维护用户在指定时间段内发出请求所使用的唯一 IP 地址列表。当达到指定的限制时，用户从新 IP 地址发出的任何请求都会被拒绝，并显示 `403 Forbidden` 错误。

当用户在指定时间段内没有从该 IP 地址发出进一步的请求时，该 IP 地址将从列表中清除。

NOTE:
当 runner 以特定用户身份运行 CI/CD 作业时，runner IP 地址也会根据用户的唯一 IP 地址列表进行存储。因此，每个用户的 IP 地址限制应考虑配置的活跃 runners 的数量。

## 配置 IP 地址限制

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**)。
1. 选择 **管理中心**。
1. 选择 **设置 > 报告**。
1. 展开 **垃圾邮件和反机器人防护**。
1. 更新 IP 地址限制设置：
   1. 选中 **限制从多个 IP 地址登录** 复选框来启用 IP 地址限制。
   1. 在 **每个用户的 IP 地址** 字段中输入一个大于或等于 `1` 的数字。此数字指定在来自新 IP 地址的请求被拒绝之前，用户在指定时间段内可以访问极狐GitLab 的唯一 IP 地址的最大数量。
   1. 在 **IP 地址到期时间** 字段中输入一个大于或等于 `0` 的数字。此数字指定 IP 地址计入用户限制的时间（以秒为单位），从上次发出请求的时间算起。
1. 选择 **保存更改**。
