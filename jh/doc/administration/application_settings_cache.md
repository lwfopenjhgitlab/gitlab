---
stage: Data Stores
group: Memory
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 应用缓存间隔 **(FREE SELF)**

默认情况下，极狐GitLab 将应用程序设置缓存 60 秒。有时，您可能需要增加该间隔，以便在应用程序设置更改和用户注意到应用程序中的这些更改之间有更多延迟。

我们建议您将此值设置为大于 `0` 秒。将其设置为 `0` 会导致为每个请求加载 `application_settings` 表，导致 Redis 和 PostgreSQL 的额外负载。

## 更改应用程序缓存的过期间隔

要更改到期值：

**Omnibus 安装实例**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['application_settings_cache_seconds'] = 60
   ```

1. 保存文件，然后重新配置并重新启动极狐GitLab，使更改生效：

   ```shell
   gitlab-ctl reconfigure
   gitlab-ctl restart
   ```

**源安装实例**

1. 编辑 `config/gitlab.yml`：

   ```yaml
   gitlab:
     application_settings_cache_seconds: 60
   ```

1. 保存文件，然后[重启](restart_gitlab.md#installations-from-source)极狐GitLab，使更改生效。
