---
stage: Systems
group: Distribution
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# 加密配置 **(FREE SELF)**

> 引入于 13.7 版本。

极狐GitLab 可以从加密的设置文件中读取某些功能的设置。支持的功能有：

- [LDAP `bind_dn` 和 `password`](auth/ldap/index.md#use-encrypted-credentials)。
- [SMTP `user_name` 和 `password`](raketasks/smtp.md#secrets)。

要启用加密配置设置，必须为 `encrypted_settings_key_base` 生成一个新的基本密钥。可以通过以下方式生成 secret：

- 对于 Linux 软件包安装，会自动为您生成新的密钥，但您必须确保 `/etc/gitlab/gitlab-secrets.json` 在所有节点上包含相同的值。
- 对于 Helm chart 安装实例，如果启用了 `shared-secrets` chart，则会自动生成新密钥。否则，您需要遵循[添加 secrets 的指南](https://docs.gitlab.cn/charts/installation/secrets.html#gitlab-rails-secret)。
- 对于源安装实例，可以通过运行以下命令生成新密钥：

  ```shell
  bundle exec rake gitlab:env:info RAILS_ENV=production GITLAB_GENERATE_ENCRYPTED_SETTINGS_KEY_BASE=true
  ```

  此命令会打印有关极狐GitLab 实例的一般信息，并在 `<path-to-gitlab-rails>/config/secrets.yml` 中生成密钥。
  
