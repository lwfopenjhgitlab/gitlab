---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 设置 Geo **(PREMIUM SELF)**

## 先决条件

- 两个（或更多）独立工作的极狐GitLab 站点：
  - 使用一个极狐GitLab 站点作为 Geo **主要**站点。<!--Use the [GitLab reference architectures documentation](../../reference_architectures/index.md) to set this up. You can use different reference architecture sizes for each Geo site.-->如果您已经有一个正在使用的极狐GitLab 实例，可以将其用作**主要**站点。
  - 使用第二个极狐GitLab 站点作为 Geo **次要**站点。<!--Use the [GitLab reference architectures documentation](../../reference_architectures/index.md) to set this up. It's a good idea to sign in and test it.-->但是请注意，作为从**主要**站点复制过程的一部分，**次要站点上的所有数据都会丢失**。

  NOTE:
  Geo 支持多个次要站点。您可以按照相同的步骤进行相应的更改。

- 确保**主要**站点具有[专业版或旗舰版](https://gitlab.cn/pricing/)订阅，可以解锁 Geo。您只需要一份许可证即可访问所有站点。
- 确认所有站点都满足[运行 Geo 的要求](../index.md#requirements-for-running-geo)。例如，站点必须使用相同的极狐GitLab 版本，并且站点必须能够通过某些端口相互通信。
- 确认**主要**和**次要**站点存储配置匹配。如果主要站点使用对象存储，则次要站点也必须使用对象存储。有关更多信息，请参阅 [Geo 与对象存储](../replication/object_storage.md)。
- 确保**主要**和**次要**站点之间的时钟同步。Geo 需要同步时钟才能正常运行。例如，**主要**和**次要**站点之间的时钟漂移超过 1 分钟，则复制失败。

<a id="using-linux-package-installations"></a>

## 使用 Linux 软件包安装

如果您使用 Linux 软件包安装了极狐GitLab（强烈推荐），则设置 Geo 的过程取决于您需要设置单节点 Geo 站点还是多节点 Geo 站点。

### 单节点 Geo 站点

<!--
If both Geo sites are based on the [1K reference architecture](../../reference_architectures/1k_users.md):
-->

1. 根据您选择的 PostgreSQL 实例设置数据库复制（“主要（读写）<-> 次要（只读）”拓扑）：
   - [使用 Linux 软件包 PostgreSQL 实例](database.md)
   - [使用外部 PostgreSQL 实例](external_database.md)
1. [配置极狐GitLab](../replication/configuration.md)，设置**主要**和**次要**站点。
1. 推荐：配置统一 URLs，使用适用于所有 Geo 站点的单个统一的 URL。
1. 可选：[配置对象存储复制](../replication/object_storage.md)。
1. 可选：为次要站点[配置次要 LDAP 服务器](../../auth/ldap/index.md)。查看 [LDAP 说明](../index.md#ldap)。
1. 可选：[为次要站点配置容器镜像库](../replication/container_registry.md)。
1. 遵循[使用 Geo 站点](../replication/usage.md)指南。

### 多节点 Geo 站点

<!--
If one or more of your sites is using the [2K reference architecture](../../reference_architectures/2k_users.md) or larger, see
[Configure Geo for multiple nodes](../replication/multiple_servers.md).
-->

查看[为多个节点配置 Geo](../replication/multiple_servers.md)。

## 使用极狐GitLab Charts

[使用 Geo 配置极狐GitLab charts](https://docs.gitlab.cn/charts/advanced/geo/)。

<!--
## Geo and self-compiled installations

Geo is not supported when you use a [self-compiled GitLab installation](../../../install/installation.md).
-->

## 安装后文档

在**次要**站点上安装极狐GitLab 并执行初始配置后，请参阅[以下文档了解安装后信息](../index.md#post-installation-documentation)。

