---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 计划内故障转移的灾难恢复 **(PREMIUM SELF)**

灾难恢复的主要用例是在发生意外中断时确保业务连续性，但它可以与计划的故障转移结合使用，在区域之间迁移极狐GitLab 实例，而不会延长停机时间。

由于 Geo 站点之间的复制是异步的，因此计划中的故障转移需要一个维护窗口，在该窗口中，**主要**站点的更新被阻止。 此窗口的长度由您的复制容量决定 - 一旦**次要**站点与**主要**站点完全同步，故障转移就可以发生而不会丢失数据。

本文档假定您已经有一个完全配置的、有效的 Geo 设置。在继续之前，请完整阅读它和[灾难恢复](index.md)故障转移文档。计划内故障转移是一项主要操作，如果执行不正确，则存在很高的数据丢失风险。考虑排练该程序，直到您对必要的步骤感到满意并且对能够准确执行它们有高度的信心。

## 并非所有数据都会自动复制

如果您正在使用 Geo <!--[不支持](../replication/datatypes.md#limitations-on-replicationverification)-->不支持的任何极狐GitLab 功能，您必须做出单独的规定以确保**次要**站点具有与该功能相关的任何数据的最新副本。这可能会显着延长所需的预定维护期。

使存储在文件中的数据的这段时间尽可能短的常用策略是使用 `rsync` 来传输数据。可以在维护窗口之前执行初始 `rsync`；随后的 `rsync`（包括维护窗口内的最终传输）仅传输**主要**站点和**次要**站点之间的*更改*。

可以在[移动仓库](../../operations/moving_repositories.md)文档中找到有效使用 `rsync` 的以仓库为中心的策略；这些策略可以适用于任何其他基于文件的数据，例如[极狐GitLab Pages](../../pages/index.md#change-storage-path)。

### 容器镜像库

默认情况下，容器镜像库不会自动复制到次要站点，这需要手动配置，请参阅[次要站点的容器镜像库](../replication/container_registry.md)。

如果您将当前主要站点上的本地存储用于容器镜像库，则可以将容器镜像库对象 `rsync` 到您即将故障转移到的次要站点：

```shell
# Run from the secondary site
rsync --archive --perms --delete root@<geo-primary>:/var/opt/gitlab/gitlab-rails/shared/registry/. /var/opt/gitlab/gitlab-rails/shared/registry
```

或者，您可以备份主要站点上的容器镜像库并将其还原到次要站点：

1. 在您的主要站点上，仅备份镜像库并从备份中排除特定目录：

   ```shell
   # Create a backup in the /var/opt/gitlab/backups folder
   sudo gitlab-backup create SKIP=db,uploads,builds,artifacts,lfs,terraform_state,pages,repositories,packages
   ```

1. 将主要站点生成的备份 tarball 复制到次要站点上的 `/var/opt/gitlab/backups` 文件夹。

1. <!--On your secondary site, restore the registry following the [Restore GitLab](../../../raketasks/backup_restore.md#restore-gitlab)
documentation.-->在您的次要站点上，恢复镜像库。

<a id="preflight-checks"></a>

## 预检检查

NOTE:
在 13.7 及更早版本中，如果您有一个需要同步零项的数据类型，即使复制实际上是最新的，此命令也会报告 `ERROR - Replication is not up-to-date`。此错误已在 13.8 及更高版本中修复。

运行此命令列出所有预检检查，并在安排计划的故障转移之前自动检查复制和验证是否完成，以确保过程顺利进行：

```shell
gitlab-ctl promotion-preflight-checks
```

下面更详细地描述每个步骤。

### 对象存储

如果您有大型极狐GitLab 安装实例或无法容忍停机时间，请考虑在安排计划内的故障转移之前，[迁移到对象存储](../replication/object_storage.md)。
这样做既可以减少维护窗口的长度，也可以减少因计划内故障转移执行不善而导致数据丢失的风险。

在 15.1 版本中，您可以选择允许极狐GitLab 管理**次要**站点的对象存储复制。有关详细信息，请参阅[对象存储复制](../replication/object_storage.md)。

### 查看每个**次要**站点的配置

数据库设置会自动复制到**次要**站点，但 `/etc/gitlab/gitlab.rb` 文件必须手动设置，并且站点之间会有所不同。如果在**主要**站点而非**次要**站点上启用了 Mattermost、OAuth 或 LDAP 集成等功能，则它们在故障转移期间会丢失。

查看两个站点的 `/etc/gitlab/gitlab.rb` 文件，并确保**次要**站点支持**主要**站点在安排计划的故障转移之前所做的一切。

### 运行系统检查

在**主要**和**次要**站点上运行以下命令：

```shell
gitlab-rake gitlab:check
gitlab-rake gitlab:geo:check
```

如果在任一站点上报告任何故障，则应在**安排计划的故障转移之前**解决它们。

### 检查节点之间的 secrets 和 SSH 主机密钥是否匹配

SSH 主机密钥和 `/etc/gitlab/gitlab-secrets.json` 文件在所有节点上应该相同。 通过在所有节点上运行以下命令并比较输出来检查这一点：

```shell
sudo sha256sum /etc/ssh/ssh_host* /etc/gitlab/gitlab-secrets.json
```

如果任何文件不同，[手动复制 secrets](../replication/configuration.md#step-1-manually-replicate-secret-gitlab-values)，和必要时[复制 SSH 主机密钥](../replication/configuration.md#step-2-manually-replicate-the-primary-sites-ssh-host-keys)到**次要**站点。

### 检查是否为 HTTPS 安装了正确的证书

如果**主要**站点和**主要**站点访问的所有外部站点使用公共 CA 颁发的证书，则可以安全地跳过此步骤。

如果**主要**站点使用自定义或自签名 TLS 证书来保护入站连接，或者如果**主要**站点连接到使用自定义或自签名证书的外部服务，则还应在**次要**站点上安装正确的证书。按照[使用自定义证书](../replication/configuration.md#step-4-optional-using-custom-certificates)的说明进行操作。

### 确保 Geo 复制是最新的

在 Geo 复制和验证完全完成之前，维护窗口不会结束。为了使窗口尽可能短，您应该确保这些进程在活动使用期间尽可能接近 100%。

在**次要**站点上：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏中，选择 **Geo > 站点**。复制对象（以绿色显示）应接近 100%，并且不应出现故障（以红色显示）。 如果大部分对象尚未复制（显示为灰色），请考虑给站点更多时间来完成。

   ![Replication status](../replication/img/geo_dashboard_v14_0.png)

如果任何对象无法复制，则应在安排维护窗口之前对此进行调查。在计划的故障转移之后，任何未能复制的内容都会**丢失**。

您可以使用 [Geo 状态 API](../../../api/geo_nodes.md)检查失败的对象和失败的原因。

复制失败的一个常见原因是**主要**站点上的数据丢失 - 您可以通过从备份中恢复数据或删除对丢失数据的引用来解决这些故障。

### 验证复制数据的完整性

此[内容已移至其他位置](background_verification.md)。

### 通知用户定期维护

在**主要**站点上：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏中，选择 **消息**。
1. 在维护窗口添加消息通知用户。 您可以在 **Geo > 站点** 下查看以估计完成同步所需的时间。
1. 选择 **添加广播消息**。

<a id="prevent-updates-to-the-primary-site"></a>

## 阻止对**主要**站点的更新

为确保将所有数据复制到次要站点，需要在**主要**站点上禁用更新（写入请求）：

1. 在**主要**站点上启用 [维护模式](../../maintenance_mode/index.md)。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **监控 > 后台作业**。
1. 在 Sidekiq 仪表盘上，选择 **Cron**。
1. 选择 **全部禁用**，禁用非 Geo 周期性后台作业。
1. 为 `geo_sidekiq_cron_config_worker` cron 作业选择 **启用**。此作业重新启用其他几个对成功完成计划故障转移至关重要的 cron 作业。

## 完成复制和验证所有数据

<!--
NOTE:
GitLab 13.9 through GitLab 14.3 are affected by a bug in which the Geo secondary site statuses appears to stop updating and become unhealthy. For more information, see [Geo Admin Area shows 'Unhealthy' after enabling Maintenance Mode](../replication/troubleshooting.md#geo-admin-area-shows-unhealthy-after-enabling-maintenance-mode).
-->

1. 如果您要手动复制任何不受 Geo 管理的数据，请立即触发最终复制过程。
1. 在**主要**站点：
   1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
   1. 选择 **管理中心**。
   1. 在左侧边栏上，选择 **监控 > 后台作业**。
   1. 在 Sidekiq 仪表盘上，选择 **队列**，然后等待除名称中带有 `geo` 的队列之外的所有队列降为 0。这些队列包含用户已提交的工作；在完成之前故障转移会导致工作丢失。
   1. 在左侧边栏中，选择 **Geo > 站点**，并等待您故障转移到的**次要**站点满足以下条件：

      - 所有复制指标达到 100% 复制，0% 失败。
      - 所有验证指标达到 100% 验证，0% 失败。
      - 数据库复制延迟为 0 毫秒。
      - Geo log cursor 是最新的（后面有 0 个事件）。

1. 在**次要**站点：
   1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
   1. 选择 **管理中心**。
   1. 在左侧边栏上，选择 **监控 > 后台作业**。
   1. 在 Sidekiq 仪表盘上，选择 **队列**，并等待所有 `geo` 队列下降到 0 个排队和 0 个正在运行的作业。
   1. [运行完整性检查](../../raketasks/check.md)，验证 CI 产物、LFS 对象和文件存储中上传的完整性。

此时，您的**次要**站点包含**主要**站点所拥有的所有内容的最新副本，这意味着当您进行故障转移时不会丢失任何内容。

## 提升**次要**站点

复制完成后，[将**次要**站点提升为**主要**站点](index.md)。此过程会导致**次要**站点短暂中断，用户可能需要重新登录。如果您正确执行了这些步骤，则旧的主要 Geo 站点仍应被禁用，而用户流量应转到新升级的站点。

提升完成后，维护窗口结束，您的新**主要**站点现在开始与旧站点不同。如果此时确实出现问题，则故障返回到旧的**主要**站点[是可能的](bring_primary_back.md)，但可能会导致上传到新**主要**站点中的任何数据丢失。

不要忘记在故障转移完成后删除广播消息。

最后，您可以将[旧站点作为次要站点](bring_primary_back.md#configure-the-former-primary-site-to-be-a-secondary-site)。
