---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 自动后台验证 **(PREMIUM SELF)**

<!--
NOTE:
Automatic background verification of repositories and wikis was added in
GitLab EE 10.6 but is enabled by default only on GitLab EE 11.1. You can
disable or enable this feature manually by following
[these instructions](#disabling-or-enabling-the-automatic-background-verification).
-->

自动后台验证可确保传输的数据与计算的校验和相匹配。如果**主要**站点上的数据校验和与**次要**站点上的数据校验和匹配，则数据传输成功。在计划的故障转移之后，任何损坏的数据都可能**丢失**，具体取决于损坏的程度。

如果**主要**站点上的验证失败，则表明 Geo 正在复制损坏的对象。
您可以从备份中恢复它或将其**主要**站点中删除以解决问题。

如果在**主要**站点上验证成功，但在**次要**站点上验证失败，则表明对象在复制过程中已损坏。Geo 积极尝试纠正验证失败，将仓库标记为使用回退期重新同步。如果您想为这些失败重置验证，那么您应该按照[这些说明](#reset-verification-for-projects-where-verification-has-failed)。

如果验证明显滞后于复制，请考虑在安排计划的故障转移之前给站点更多时间。

<a id="disabling-or-enabling-the-automatic-background-verification"></a>

## 禁用或启用自动后台验证

在主要站点上的 **Rails 节点上的 [Rails 控制台](../../operations/rails_console.md)**中运行以下命令。

检查是否启用了自动后台验证：

```ruby
Gitlab::Geo.repository_verification_enabled?
```

要禁用自动后台验证：

```ruby
Feature.disable('geo_repository_verification')
```

要启用自动后台验证：

```ruby
Feature.enable('geo_repository_verification')
```

## 仓库验证

在**主要**站点上：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏中，选择 **Geo > 站点**。
1. 展开该站点的**验证信息**选项卡，查看仓库和 wiki 的自动校验和状态。成功以绿色显示，待处理的工作以灰色显示，失败以红色显示。

   ![Verification status](img/verification_status_primary_v14_0.png)

在**次要**站点上：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏中，选择 **Geo > 站点**。
1. 展开该站点的 **验证信息** 选项卡，查看仓库和 wiki 的自动校验和状态。成功以绿色显示，待处理的工作以灰色显示，失败以红色显示。

   ![Verification status](img/verification_status_secondary_v14_0.png)

## 使用校验和比较 Geo 站点

为了检查 Geo **次要**站点的运行状况，我们对 Git 引用列表及其值使用校验和。校验和包括 `HEAD`、`heads`、`tags`、`notes` 和极狐GitLab 特定的引用，以确保真正的一致性。如果两个站点具有相同的校验和，那么它们肯定拥有相同的引用。我们在每次更新后计算每个站点的校验和，以确保它们都同步。

## 仓库重新验证

由于错误或短暂的基础设施故障，Git 仓库可能会在未标记为验证的情况下发生意外更改。
Geo 不断地重新验证仓库以确保数据的完整性。默认和推荐的重新验证间隔为 7 天，但可以设置短至 1 天的间隔。较短的间隔会降低风险，但会增加负荷，反之亦然。

在**主要**站点上：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏中，选择 **Geo > 站点**。
1. 为**主要**站点选择 **编辑**，自定义最小重新验证间隔：

   ![Re-verification interval](img/reverification-interval.png)

默认启用自动后台重新验证，但您可以根据需要禁用。在主要站点上的 **Rails 节点上的 [Rails 控制台](../../operations/rails_console.md)**中运行以下命令：

要禁用自动后台重新验证：

```ruby
Feature.disable('geo_repository_reverification')
```

要启用自动后台重新验证：

```ruby
Feature.enable('geo_repository_reverification')
```

<a id="reset-verification-for-projects-where-verification-has-failed"></a>

## 为验证失败的项目重置验证

Geo 积极尝试纠正验证失败，将仓库标记为使用回退期重新同步。如果您想手动重置它们，此 Rake 任务会将验证失败或校验和不匹配的项目标记为在没有回退期的情况下重新同步：

在**次要站点上的 Rails 节点上**运行适当的命令。

对于仓库：

```shell
sudo gitlab-rake geo:verification:repository:reset
```

对于 wikis：

```shell
sudo gitlab-rake geo:verification:wiki:reset
```

## 协调与校验和不匹配的差异

如果**主要**和**次要**站点存在校验和验证不匹配，则原因可能不明显。要查找校验和不匹配的原因：

1. 在**主要**站点上：
   1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
   1. 选择 **管理中心**。
   1. 在左侧边栏上，选择 **概览 > 项目**。
   1. 找到您要检查校验和差异的项目并选择其名称。
   1. 在项目管理页面获取 **Gitaly 存储名称**和 **Gitaly 相对路径**。

      ![Project administration page](img/checksum-differences-admin-project-page.png)

1. 在**主要站点上的 Gitaly 节点**和**次要站点上的 Gitaly 节点**上，转到项目的仓库目录。如果使用 Gitaly 集群，在运行这些命令之前检查它是否处于健康状态。

   默认路径是 `/var/opt/gitlab/git-data/repositories`。如果 `git_data_dirs` 是自定义的，请检查服务器上的目录布局以确保：

   ```shell
   cd /var/opt/gitlab/git-data/repositories
   ```

   1. 在**主要**站点上运行以下命令，将输出重定向到文件：

      ```shell
      git show-ref --head | grep -E "HEAD|(refs/(heads|tags|keep-around|merge-requests|environments|notes)/)" > primary-site-refs
      ```

   1. 在**次要**站点上运行以下命令，将输出重定向到文件：

      ```shell
      git show-ref --head | grep -E "HEAD|(refs/(heads|tags|keep-around|merge-requests|environments|notes)/)" > secondary-site-refs
      ```

   1. 在同一系统上复制前面步骤中的文件，并在内容之间进行比较：

      ```shell
      diff primary-site-refs secondary-site-refs
      ```

## 当前限制

自动后台验证不包括附件、LFS 对象、作业产物和文件存储中的用户上传文件。

目前，您可以通过在两个站点上遵循[这些说明](../../raketasks/check.md)，并比较它们之间的输出来手动验证它们的完整性。

<!--
In GitLab EE 12.1, Geo calculates checksums for attachments, LFS objects, and
archived traces on secondary sites after the transfer, compares it with the
stored checksums, and rejects transfers if mismatched. Geo
currently does not support an automatic way to verify these data if they have
been synced before GitLab EE 12.1.
-->

对象存储中的数据**未验证**，因为对象存储负责确保数据的完整性。
