---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 灾难恢复（Geo） **(PREMIUM SELF)**

Geo 会复制您的数据库、您的 Git 仓库和一些其他资产，但存在一些[限制](../index.md#limitations)。

WARNING:
多次要节点配置需要完全重新同步和重新配置所有未升级的次要节点，并导致停机。

<a id="promoting-a-secondary-geo-site-in-single-secondary-configurations"></a>

## 在单次要节点配置中提升**次要** Geo 站点

我们目前不提供自动方式来提升 Geo 副本并进行故障转移，但如果您对机器具有 `root` 访问权限，则可以手动进行。

此过程将**次要** 站点提升为**主要**站点。要尽快恢复 Geo 冗余，您应该在遵循这些说明后，立即添加一个新的**次要**站点。

### 步骤 1. 如果可能，允许复制完成

如果**次要**站点仍在从**主要**站点复制数据，请尽可能遵循[计划的故障转移文档](planned_failover.md)，以避免不必要的数据丢失。

<a id="step-2-permanently-disable-the-primary-site"></a>

### 步骤 2. 永久禁用**主要**站点

WARNING:
如果**主要**站点脱机，可能有保存在**主要**站点上的数据尚未复制到**次要**站点。如果您继续，此数据应被视为丢失。

如果**主要**站点发生中断，您应该尽一切可能避免在两个不同的极狐GitLab 实例中发生写入的脑裂情况，从而使恢复工作复杂化。因此，为了准备故障转移，我们必须禁用**主要**站点。

- 如果您有 SSH 访问权限：

  1. SSH 进入**主要**站点，停止和禁用极狐GitLab：

     ```shell
     sudo gitlab-ctl stop
     ```

  1. 如果服务器意外重启，防止极狐GitLab 再次启动：

     ```shell
     sudo systemctl disable gitlab-runsvdir
     ```

- 如果您没有对**主要**站点的 SSH 访问权限，请使计算机脱机并通过您可以使用的任何方式阻止其重新启动。您可能需要：

  - 重新配置负载均衡器。
  - 更改 DNS 记录（例如，将主 DNS 记录指向**次要**站点以停止使用**主要**站点）。
  - 停止虚拟服务器。
  - 通过防火墙阻止流量。
  - 撤销**主要**站点的对象存储权限。
  - 物理断开机器。

  如果您打算[更新主域名 DNS 记录](#step-4-optional-updating-the-primary-domain-dns-record)，您可能希望现在降低 TTL 以加快传播速度。

### 步骤 3. 提升**次要**站点

<!--
WARNING:
In GitLab 13.2 and 13.3, promoting a secondary site to a primary while the
secondary is paused fails. Do not pause replication before promoting a
secondary. If the secondary site is paused, be sure to resume before promoting.
This issue has been fixed in GitLab 13.4 and later.
-->

提升次要节点时请注意以下事项：

- 如果在次要站点上暂停了复制（例如，作为升级的一部分，当您运行低于 13.4 的版本时），您必须在继续之前使用数据库启用该站点。如果次要站点已暂停，则升级会执行时间点恢复，到最后一个已知状态。在次要节点暂停时在主节点上创建的数据会丢失。
- 此时不应添加新的**次要**站点。如果要添加新的**次要**站点，请在完成将**次要**站点提升为**主要**站点的整个过程后执行此操作。

<!--
- If you encounter an `ActiveRecord::RecordInvalid: Validation failed: Name has already been taken`
  error message during this process, for more information, see this
  [troubleshooting advice](../replication/troubleshooting.md#fixing-errors-during-a-failover-or-when-promoting-a-secondary-to-a-primary-site).
- If you run into errors when using `--force` or `--skip-preflight-checks` before 13.5 during this process,
  for more information, see this
  [troubleshooting advice](../replication/troubleshooting.md#errors-when-using---skip-preflight-checks-or---force).
-->

#### 提升在运行 14.5 及更高版本的单个节点上运行的**次要**站点

1. SSH 到您的**次要**站点并执行：

   - 要将次要站点提升为主要站点：

     ```shell
     sudo gitlab-ctl geo promote
     ```

   - 将次要站点提升为主要站点**无需任何进一步确认**：

     ```shell
     sudo gitlab-ctl geo promote --force
     ```

1. 验证您可以使用之前用于**次要**站点的 URL，连接到新升级的**主要**站点。
1. 如果成功，现在将**次要**站点提升为**主要**站点。

#### 提升在运行 14.4 及更早版本的单个节点上运行的**次要**站点

WARNING:
`gitlab-ctl promote-to-primary-node` 和 `gitlab-ctl promote-db` 命令在 14.5 及更高版本中已弃用，并在 15.0 版本中删除。改用 `gitlab-ctl geo promote`。

1. SSH 到您的**次要**站点并以 root 身份登录：

   ```shell
   sudo -i
   ```

1. 如果您使用的是 13.5 及更高版本，请跳过此步骤。如果没有，请编辑 `/etc/gitlab/gitlab.rb` 并删除以下任何可能存在的行：

   ```ruby
   geo_secondary_role['enable'] = true
   roles ['geo_secondary_role']
   ```

1. 将**次要**站点提升为**主要**站点：

   - 运行[预检检查](planned_failover.md#preflight-checks)：

     ```shell
     gitlab-ctl promote-to-primary-node
     ```

   - 如果您已经单独运行了预检检查或不想运行它们，您可以跳过它们：

     ```shell
     gitlab-ctl promote-to-primary-node --skip-preflight-checks
     ```

     NOTE:
     在 13.7 及更早版本中，如果您有一个包含零项要同步的数据类型并且不跳过预检检查，那么即使复制实际上是最新的，提升次要节点时也会报告 `ERROR - Replication is not up-to-date`。如果复制和验证输出显示已完成，您可以跳过预检检查以使命令完成升级。此错误已在 13.8 及更高版本中修复。

   - 将次要站点提升为主要站点**无需任何进一步确认**，即使预检检查失败：

     ```shell
     gitlab-ctl promote-to-primary-node --force
     ```

1. 验证您可以使用之前用于**次要**站点的 URL 连接到新升级的**主要**站点。
1. 如果成功，现在将**次要**站点提升为**主要**站点。

#### 提升具有多个运行 14.5 及更高版本的节点的**次要**站点

1. SSH 到**次要**站点中的每个 Sidekiq、PostgreSQL 和 Gitaly 节点，并运行以下命令之一：

   - 要将次要站点上的节点提升为主节点：

     ```shell
     sudo gitlab-ctl geo promote
     ```

   - 将次要站点提升为主要站点**无需任何进一步确认**：

     ```shell
     sudo gitlab-ctl geo promote --force
     ```

1. SSH 到您的**次要**站点上的每个 Rails 节点并运行以下命令之一：

   - 要将次要站点提升为主要站点：

     ```shell
     sudo gitlab-ctl geo promote
     ```

   - 将次要站点提升为主要站点**无需任何进一步确认**：

     ```shell
     sudo gitlab-ctl geo promote --force
     ```

1. 验证您可以使用之前用于**次要**站点的 URL 连接到新提升的**主要**站点。
1. 如果成功，则**次要**站点现在升级为**主要**站点。

#### 提升具有多个运行 14.4 及更早版本的节点的**次要**站点

WARNING:
`gitlab-ctl promote-to-primary-node` 和 `gitlab-ctl promote-db` 命令在 14.5 及更高版本中已弃用，并在 15.0 版本中删除。改用 `gitlab-ctl geo promote`。

`gitlab-ctl promote-to-primary-node` 命令还不能与多个节点一起使用，因为它只能在只有一个节点的**次要**站点上执行更改。您必须手动执行此操作。

1. SSH 到**次要**站点中的数据库节点并触发 PostgreSQL 提升为读写：

   ```shell
   sudo gitlab-ctl promote-db
   ```

<!--
   In GitLab 12.8 and earlier, see [Message: `sudo: gitlab-pg-ctl: command not found`](../replication/troubleshooting.md#message-sudo-gitlab-pg-ctl-command-not-found).
-->

1. 在**次要**站点中的每个节点上编辑 `/etc/gitlab/gitlab.rb` 以通过删除以下任何可能存在的行来反映其作为**主要**站点的新状态：

   ```ruby
   geo_secondary_role['enable'] = true
   roles ['geo_secondary_role']
   ```

   进行这些更改后，在每台机器上[重新配置极狐GitLab](../../restart_gitlab.md#reconfigure-a-linux-package-installation)，使更改生效。

1. 将**次要**站点提升为**主要**站点。SSH 进入单个应用程序服务器并执行：

   ```shell
   sudo gitlab-rake geo:set_secondary_as_primary
   ```

1. 验证您是否可以使用之前用于**次要**站点的 URL 连接到新提升的**主要**站点。
1. 如果成功，则**次要**站点现在升级为**主要**站点。

#### 使用运行 14.5 及更高版本的 Patroni 备用集群提升**次要**站点

1. SSH 到**次要**站点中的每个 Sidekiq、PostgreSQL 和 Gitaly 节点，并运行以下命令之一：

   - 要将次要站点提升为主要站点：

     ```shell
     sudo gitlab-ctl geo promote
     ```

   - 将次要站点提升为主要站点**无需任何进一步确认**：

     ```shell
     sudo gitlab-ctl geo promote --force
     ```

1. SSH 到您的**次要**站点上的每个 Rails 节点并运行以下命令之一：

   - 要将次要站点提升为主要站点：

     ```shell
     sudo gitlab-ctl geo promote
     ```

   - 将次要站点提升为主要站点**无需任何进一步确认**：

     ```shell
     sudo gitlab-ctl geo promote --force
     ```

1. 验证您可以使用之前用于**次要**站点的 URL 连接到新提升的**主要**站点。
1. 如果成功，则**次要**站点现在升级为**主要**站点。

#### 使用运行 14.4 及更早版本的 Patroni 备用集群提升**次要**站点

WARNING:
`gitlab-ctl promote-to-primary-node` 和 `gitlab-ctl promote-db` 命令在 14.5 及更高版本中已弃用，并在 15.0 版本中删除。改用 `gitlab-ctl geo promote`。

`gitlab-ctl promote-to-primary-node` 命令还不能与 Patroni 备用集群一起使用，因为它只能在只有一个节点的**次要**站点上执行更改。您必须手动执行此操作。

1. SSH 到**次要**站点中的 Standby Leader 数据库节点并触发 PostgreSQL 提升为读写：

   ```shell
   sudo gitlab-ctl promote-db
   ```

1. 在次要节点中的每个应用程序和 Sidekiq 节点上编辑 `/etc/gitlab/gitlab.rb`，通过删除可能存在的以下任何行来反映其作为主节点的新状态：

   ```ruby
   geo_secondary_role['enable'] = true
   roles ['geo_secondary_role']
   ```

1. 在次要节点中的每个 Patroni 节点上编辑 `/etc/gitlab/gitlab.rb` 以禁用备用集群：

   ```ruby
   patroni['standby_cluster']['enable'] = false
   ```

1. 在每台机器上重新配置极狐GitLab，使更改生效：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

1. 将**次要**站点提升为**主要**站点。SSH 进入单个应用程序服务器并执行：

   ```shell
   sudo gitlab-rake geo:set_secondary_as_primary
   ```

1. 验证您是否可以使用之前用于**次要**站点的 URL 连接到新提升的**主要**站点。
1. 如果成功，则**次要**站点现在升级为**主要**站点。

#### 使用运行 14.5 及更高版本的外部 PostgreSQL 数据库提升**次要**站点

`gitlab-ctl geo promote` 命令可以与外部 PostgreSQL 数据库结合使用。在这种情况下，您必须首先手动提升与**次要**站点关联的副本数据库：

1. 提升与**次要**站点关联的副本数据库。这会将数据库设置为读写。

     ```shell
     #!/bin/bash

     PG_SUPERUSER=postgres

     # The path to your pg_ctl binary. You may need to adjust this path to match
     # your PostgreSQL installation
     PG_CTL_BINARY=/usr/lib/postgresql/10/bin/pg_ctl

     # The path to your PostgreSQL data directory. You may need to adjust this
     # path to match your PostgreSQL installation. You can also run
     # `SHOW data_directory;` from PostgreSQL to find your data directory
     PG_DATA_DIRECTORY=/etc/postgresql/10/main

     # Promote the PostgreSQL database and allow read/write operations
     sudo -u $PG_SUPERUSER $PG_CTL_BINARY -D $PG_DATA_DIRECTORY promote
     ```

1. SSH 到**次要**站点中的每个 Sidekiq、PostgreSQL 和 Gitaly 节点，并运行以下命令之一：

   - 要将次要站点提升为主要站点：

     ```shell
     sudo gitlab-ctl geo promote
     ```

   - 将次要站点提升为主要站点**无需任何进一步确认**：

     ```shell
     sudo gitlab-ctl geo promote --force
     ```

1. SSH 到您的**次要**站点上的每个 Rails 节点并运行以下命令之一：

   - 要将次要站点提升为主要站点：

     ```shell
     sudo gitlab-ctl geo promote
     ```

   - 将次要站点提升为主要站点**无需任何进一步确认**：

     ```shell
     sudo gitlab-ctl geo promote --force
     ```

1. 验证您可以使用之前用于**次要**站点的 URL 连接到新提升的**主要**站点。
1. 如果成功，则**次要**站点现在升级为**主要**站点。

#### 使用运行 14.4 及更早版本的外部 PostgreSQL 数据库提升**次要**站点

WARNING:
`gitlab-ctl promote-to-primary-node` 和 `gitlab-ctl promote-db` 命令在 14.5 及更高版本中已弃用，并在 15.0 版本中删除。改用 `gitlab-ctl geo promote`。

`gitlab-ctl promote-to-primary-node` 命令不能与外部 PostgreSQL 数据库一起使用，因为它只能在具有极狐GitLab 的**次要**节点和同一台机器上的数据库上执行更改。因此，需要手动过程：

1. 提升与**次要**站点关联的副本数据库。这会将数据库设置为读写。

     ```shell
     #!/bin/bash

     PG_SUPERUSER=postgres

     # The path to your pg_ctl binary. You may need to adjust this path to match
     # your PostgreSQL installation
     PG_CTL_BINARY=/usr/lib/postgresql/10/bin/pg_ctl

     # The path to your PostgreSQL data directory. You may need to adjust this
     # path to match your PostgreSQL installation. You can also run
     # `SHOW data_directory;` from PostgreSQL to find your data directory
     PG_DATA_DIRECTORY=/etc/postgresql/10/main

     # Promote the PostgreSQL database and allow read/write operations
     sudo -u $PG_SUPERUSER $PG_CTL_BINARY -D $PG_DATA_DIRECTORY promote
     ```

1. 在**次要**站点中的每个节点上编辑 `/etc/gitlab/gitlab.rb`，通过删除以下任何可能存在的行来反映其作为**主要**站点的新状态：

   ```ruby
   geo_secondary_role['enable'] = true
   roles ['geo_secondary_role']
   ```

   在每个节点上进行这些更改，[重新配置极狐GitLab](../../restart_gitlab.md#reconfigure-a-linux-package-installation) 后，更改才会生效。

1. 将**次要**站点提升为**主要**站点。SSH 进入单个辅助应用程序节点并执行：

   ```shell
   sudo gitlab-rake geo:set_secondary_as_primary
   ```

1. 验证您是否可以使用之前用于**次要**站点的 URL 连接到新提升的**主要**站点。
1. 如果成功，则**次要**站点现在升级为**主要**站点。

<a id="step-4-optional-updating-the-primary-domain-dns-record"></a>

### 步骤 4. （可选）更新主域名 DNS 记录

更新主域名的 DNS 记录以指向**次要**站点，以防止需要将所有对主域名的引用更新到次要域名，例如更改 Git 远程和 API URL。

1. SSH 进入**次要**站点并以 root 身份登录：

   ```shell
   sudo -i
   ```

1. 更新主域名的 DNS 记录。更新主域名的 DNS 记录指向**次要**站点后，编辑**次要**站点上的 `/etc/gitlab/gitlab.rb` 以反映新 URL：

   ```ruby
   # Change the existing external_url configuration
   external_url 'https://<new_external_url>'
   ```

   NOTE:
   只要次要 DNS 记录仍然完好无损，更改 `external_url` 不会阻止通过旧的 URL 进行访问。

1. 更新**次要**站点的 SSL 证书：

   - 如果您使用 [Let's Encrypt 集成](https://docs.gitlab.cn/omnibus/settings/ssl/index.html#enable-the-lets-encrypt-integration)，证书会自动更新。
   - 如果您[手动设置](https://docs.gitlab.cn/omnibus/settings/ssl/index.html#configure-https-manually)次要节点的证书，请从**主要**站点复制到**次要**站点。
     如果您无权访问**主要**站点，请颁发新证书并确保它在主题备用名称中同时包含**主要**站点和**次要**站点的 URL。您可以检查：

     ```shell
     /opt/gitlab/embedded/bin/openssl x509 -noout -dates -subject -issuer \
         -nameopt multiline -ext subjectAltName -in /etc/gitlab/ssl/new-gitlab.new-example.com.crt
     ```

1. 重新配置**次要**站点，使更改生效：

   ```shell
   gitlab-ctl reconfigure
   ```

1. 执行以下命令更新新提升的**主要**站点 URL：

   ```shell
   gitlab-rake geo:update_primary_node_url
   ```

   此命令使用在 `/etc/gitlab/gitlab.rb` 中定义的更改后的 `external_url` 配置。

1. 要确定是否需要这样做，请在 `/etc/gitlab/gitlab.rb` 文件中搜索 `gitlab_rails["geo_node_name"]` 设置。如果它被 `#` 注释掉或根本找不到，那么您需要更新数据库中的**主要**站点的名称。您可以像这样搜索它：

   ```shell
   grep "geo_node_name" /etc/gitlab/gitlab.rb
   ```

   要在数据库中更新**主要**站点的名称：

   ```shell
   gitlab-rails runner 'Gitlab::Geo.primary_node.update!(name: GeoNode.current_node_name)'
   ```

1. 验证您是否可以使用其 URL 连接到新提升的**主要**站点。如果您更新了主域名的 DNS 记录，这些更改可能尚未传播，具体取决于以前的 DNS 记录 TTL。

### 步骤 5.（可选）将**次要** Geo 站点添加到提升的**主要**站点

使用上述过程将**次要**站点提升为**主要**站点不会在新的**主要**站点上启用 Geo。

要使新的**次要**站点上线，请按照 [Geo 设置说明](../index.md#setup-instructions)。

### 步骤 6. 删除次要站点的跟踪数据库

每个**次要**站点都有一个特殊的跟踪数据库，用于保存**主要**站点中所有项目的同步状态。
由于**次要**站点已升级，因此不再需要跟踪数据库中的数据。

可以使用以下命令删除数据：

```shell
sudo rm -rf /var/opt/gitlab/geo-postgresql
```

如果您在 `gitlab.rb` 文件中启用了任何 `geo_secondary[]` 配置选项，可以安全地注释掉或删除这些配置选项，然后[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

## 在多次要站点配置中提升次要 Geo 副本

如果您有多个**次要**站点并且需要推广其中一个，我们建议您遵循[在单次要站点配置中提升**次要** Geo 站点](#promoting-a-secondary-geo-site-in-single-secondary-configurations)，然后您还需要两个额外的步骤。

### 步骤 1. 准备新的**主要**站点，服务于一个或多个**次要**站点

1. SSH 进入新的**主要**站点并以 root 身份登录：

   ```shell
   sudo -i
   ```

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   ## Enable a Geo Primary role (if you haven't yet)
   roles ['geo_primary_role']

   ##
   # Allow PostgreSQL client authentication from the primary and secondary IPs. These IPs may be
   # public or VPC addresses in CIDR format, for example ['198.51.100.1/32', '198.51.100.2/32']
   ##
   postgresql['md5_auth_cidr_addresses'] = ['<primary_site_ip>/32', '<secondary_site_ip>/32']

   # Every secondary site needs to have its own slot so specify the number of secondary sites you're going to have
   # postgresql['max_replication_slots'] = 1 # Set this to be the number of Geo secondary nodes if you have more than one

   ##
   ## Disable automatic database migrations temporarily
   ## (until PostgreSQL is restarted and listening on the private address).
   ##
   gitlab_rails['auto_migrate'] = false
   ```

   （有关这些设置的更多详细信息，您可以阅读[配置主服务器](../setup/database.md#step-1-configure-the-primary-site)）

1. 保存文件并为数据库监听更改和要应用的复制槽更改，重新配置极狐GitLab：

   ```shell
   gitlab-ctl reconfigure
   ```

   重新启动 PostgreSQL 以使其更改生效：

   ```shell
   gitlab-ctl restart postgresql
   ```

1. 现在重新启用迁移，因为 PostgreSQL 已重新启动并侦听私有地址。

   编辑 `/etc/gitlab/gitlab.rb` 和**变更**配置为 `true`：

   ```ruby
   gitlab_rails['auto_migrate'] = true
   ```

   保存文件并重新配置极狐GitLab：

   ```shell
   gitlab-ctl reconfigure
   ```

### 步骤 2. 启动复制过程

现在我们需要让每个**次要**站点监听新的**主要**站点上的更改。为此，您需要再次[启动复制过程](../setup/database.md#step-3-initiate-the-replication-process)，但这次是针对另一个**主要**站点。所有旧的复制设置都将被覆盖。

<!--
## Promoting a secondary Geo cluster in GitLab Cloud Native Helm Charts

When updating a Cloud Native Geo deployment, the process for updating any node that is external to the secondary Kubernetes cluster does not differ from the non Cloud Native approach. As such, you can always defer to [Promoting a secondary Geo site in single-secondary configurations](#promoting-a-secondary-geo-site-in-single-secondary-configurations) for more information.

The following sections assume you are using the `gitlab` namespace. If you used a different namespace when setting up your cluster, you should also replace `--namespace gitlab` with your namespace.

WARNING:
In GitLab 13.2 and 13.3, promoting a secondary site to a primary while the
secondary is paused fails. Do not pause replication before promoting a
secondary. If the site is paused, be sure to resume before promoting. This
issue has been fixed in GitLab 13.4 and later.

### Step 1. Permanently disable the **primary** cluster

WARNING:
If the **primary** site goes offline, there may be data saved on the **primary** site
that has not been replicated to the **secondary** site. This data should be treated
as lost if you proceed.

If an outage on the **primary** site happens, you should do everything possible to
avoid a split-brain situation where writes can occur in two different GitLab
instances, complicating recovery efforts. So to prepare for the failover, you
must disable the **primary** site:

- If you have access to the **primary** Kubernetes cluster, connect to it and disable the GitLab `webservice` and `Sidekiq` pods:

  ```shell
  kubectl --namespace gitlab scale deploy gitlab-geo-webservice-default --replicas=0
  kubectl --namespace gitlab scale deploy gitlab-geo-sidekiq-all-in-1-v1 --replicas=0
  ```

- If you do not have access to the **primary** Kubernetes cluster, take the cluster offline and
  prevent it from coming back online by any means at your disposal.
  You might need to:

  - Reconfigure the load balancers.
  - Change DNS records (for example, point the primary DNS record to the
    **secondary** site to stop usage of the **primary** site).
  - Stop the virtual servers.
  - Block traffic through a firewall.
  - Revoke object storage permissions from the **primary** site.
  - Physically disconnect a machine.

### Step 2. Promote all **secondary** site nodes external to the cluster

WARNING:
If the secondary site [has been paused](../../geo/index.md#pausing-and-resuming-replication), this performs
a point-in-time recovery to the last known state.
Data that was created on the primary while the secondary was paused is lost.

If you are running GitLab 14.5 and later:

1. For each node outside of the **secondary** Kubernetes cluster using Omnibus such as PostgreSQL or Gitaly, SSH into the node and run one of the following commands:

   - To promote the **secondary** site node external to the Kubernetes cluster to primary:

     ```shell
     sudo gitlab-ctl geo promote
     ```

   - To promote the **secondary** site node external to the Kubernetes cluster to primary **without any further confirmation**:

     ```shell
     sudo gitlab-ctl geo promote --force
     ```

1. Find the `toolbox` pod:

   ```shell
   kubectl --namespace gitlab get pods -lapp=toolbox
   ```

1. Promote the secondary:

   ```shell
   kubectl --namespace gitlab exec -ti gitlab-geo-toolbox-XXX -- gitlab-rake geo:set_secondary_as_primary
   ```

If you are running GitLab 14.4 and earlier:

1. SSH in to the database node in the **secondary** site and trigger PostgreSQL to
   promote to read-write:

   ```shell
   sudo gitlab-ctl promote-db
   ```

1. Edit `/etc/gitlab/gitlab.rb` on the database node in the **secondary** site to
   reflect its new status as **primary** by removing any lines that enabled the
   `geo_secondary_role`:

   NOTE:
   Depending on your architecture, these steps need to run on any GitLab node that is external to the **secondary** Kubernetes cluster.

   ```ruby
   ## In pre-11.5 documentation, the role was enabled as follows. Remove this line.
   geo_secondary_role['enable'] = true

   ## In 11.5+ documentation, the role was enabled as follows. Remove this line.
   roles ['geo_secondary_role']
   ```

   After making these changes, [reconfigure GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure) on the database node.

1. Find the task runner pod:

   ```shell
   kubectl --namespace gitlab get pods -lapp=task-runner
   ```

1. Promote the secondary:

   ```shell
   kubectl --namespace gitlab exec -ti gitlab-geo-task-runner-XXX -- gitlab-rake geo:set_secondary_as_primary
   ```

### Step 3. Promote the **secondary** cluster

1. Update the existing cluster configuration.

   You can retrieve the existing configuration with Helm:

   ```shell
   helm --namespace gitlab get values gitlab-geo > gitlab.yaml
   ```

   The existing configuration contains a section for Geo that should resemble:

   ```yaml
   geo:
      enabled: true
      role: secondary
      nodeName: secondary.example.com
      psql:
         host: geo-2.db.example.com
         port: 5431
         password:
            secret: geo
            key: geo-postgresql-password
   ```

   To promote the **secondary** cluster to a **primary** cluster, update `role: secondary` to `role: primary`.

   If the cluster remains as a primary site, you can remove the entire `psql` section; it refers to the tracking database and is ignored whilst the cluster is acting as a primary site.

   Update the cluster with the new configuration:

   ```shell
   helm upgrade --install --version <current Chart version> gitlab-geo gitlab/gitlab --namespace gitlab -f gitlab.yaml
   ```

1. Verify you can connect to the newly promoted primary using the URL used previously for the secondary.

1. Success! The secondary has now been promoted to primary.

## Troubleshooting

This section was moved to [another location](../replication/troubleshooting.md#fixing-errors-during-a-failover-or-when-promoting-a-secondary-to-a-primary-site).
-->
