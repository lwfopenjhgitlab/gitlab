---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 使降级的主要站点重新上线 **(PREMIUM SELF)**

故障转移后，可以回到降级的**主要**站点以恢复您的原始配置。这个过程包括两个步骤：

1. 将旧的**主要**站点变成**次要**站点。
1. 将**次要**站点提升为**主要**站点。

WARNING:
如果您对本网站数据的一致性有任何疑问，我们建议您从头开始设置。

<a id="configure-the-former-primary-site-to-be-a-secondary-site"></a>

## 将之前的**主要**站点配置为**次要**站点

由于之前的**主要**站点与当前的**主要**站点不同步，所以第一步是更新之前的**主要**站点。请注意，当使以前的**主要**站点重新同步时，不会重复存储在磁盘上的数据（如仓库和上传文件）的删除，这可能会导致磁盘使用量增加。
或者，您可以[设置一个新的**次要**极狐 GitLab 实例](../setup/index.md) 来避免这种情况。

更新前**主要**站点：

1. SSH 进入已经落后的前**主要**站点。
1. 如果存在，请删除 `/etc/gitlab/gitlab-cluster.json`。

  如果要重新添加为**次要**站点的站点是使用 `gitlab-ctl geo promote` 命令提升的，那么它可能包含一个 `/etc/gitlab/gitlab-cluster.json` 文件。例如，在 `gitlab-ctl reconfigure` 期间，您可能会注意到如下输出：

   ```plaintext
   The 'geo_primary_role' is defined in /etc/gitlab/gitlab-cluster.json as 'true' and overrides the setting in the /etc/gitlab/gitlab.rb
   ```

  如果是这样，则必须从站点中的每个 Sidekiq、PostgreSQL、Gitaly 和 Rails 节点中删除 `/etc/gitlab/gitlab-cluster.json`，以使 `/etc/gitlab/gitlab.rb` 成为唯一的真实来源。

1. 确保所有服务都已启动：

   ```shell
   sudo gitlab-ctl start
   ```

   NOTE:
   如果您[永久禁用**主要**站点](index.md#step-2-permanently-disable-the-primary-site)，您现在需要撤消这些步骤。对于带有 systemd 的发行版，例如 Debian/Ubuntu/CentOS7+，您必须运行 `sudo systemctl enable gitlab-runsvdir`。对于没有 systemd 的发行版，例如 CentOS 6，您需要从头开始安装极狐GitLab 实例，并按照[设置说明](../setup/index.md)将其设置为**次要**站点，在这种情况下，您无需执行下一步。

   NOTE:
   如果您在灾难恢复过程中[更改了此站点的 DNS 记录](index.md#step-4-optional-updating-the-primary-domain-dns-record)，在此过程中，您可能需要[阻止对该站点的所有写入](planned_failover.md#prevent-updates-to-the-primary-site)。

1. [设置数据库复制](../setup/database.md)。在这种情况下，**次要**站点是指之前的**主要**站点。
   1. 如果 [PgBouncer](../../postgresql/pgbouncer.md) 在**当前次要**站点（当它是主要站点时）上启用，请通过编辑 `/etc/gitlab/gitlab.rb` 禁用它，并运行`sudo gitlab-ctl reconfigure`。
   1. 然后，您可以在**次要**站点上设置数据库复制。

如果您丢失了原来的**主要**站点，请按照[设置说明](../setup/index.md)，设置新的**次要**站点。

## 将**次要**站点提升为**主要**站点

当初始复制完成并且**主要**站点和**次要**站点紧密同步时，您可以执行[计划的故障转移](planned_failover.md)。

## 恢复**次要**站点

如果您的目标是再次拥有两个站点，您还需要通过重复第一步（[将前**主要**站点配置为**次要**站点](#configure-the-former-primary-site-to-be-a-secondary-site)），使您的**次要**站点重新上线。
