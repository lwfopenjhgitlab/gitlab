---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---


# Geo 术语 **(PREMIUM SELF)**

NOTE:
我们正在更新 Geo 文档、用户界面和命令以反映这些变化。并非所有页面都符合这些定义。

这些是描述 Geo 各个方面的定义术语。使用一组明确定义的术语有助于我们有效地沟通并避免混淆。此页面上的语言旨在无处不在且尽可能简单。

 我们提供示例图表和语句来演示术语的正确使用。

| 术语                      | 定义                                                                                                                                                                            | 范围        | 其他近义词                            |
|---------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------|-------------------------------------------------|
| 节点（Node）                      | 以特定角色或整体运行极狐GitLab 的单个服务器（例如 Rails 应用程序节点）。在云环境中，可以是特定的机器类型。                 | 极狐GitLab       | 实例、服务器                                |
| 站点（Site）                      | 运行单个极狐GitLab 应用程序的一个或一组节点。站点可以是单节点或多节点。                                                                             | 极狐GitLab       | 部署、安装实例               |
| 单节点站点（Single-node site）          | 极狐GitLab 的一种特定配置，它只使用一个节点。                                                                                                                     | 极狐GitLab       | 单服务器、单实例
| 多节点站点（Multi-node site）           | 使用多个节点的极狐GitLab 的特定配置。                                                                                                                   | 极狐GitLab       | 多服务器、多实例、高可用 |
| 主要站点（Primary site）              | 一个极狐GitLab 站点，其数据被至少一个次要站点复制。只能有一个主要站点。                                                                  | 特定于 Geo | Geo 部署、主节点                   |
| 次要站点（Secondary site）            | 配置为复制主要站点数据的极狐GitLab 站点。可以有一个或多个次要站点。                                                                    | 特定于 Geo | Geo 部署、副节点                  |
| Geo 部署（Geo deployment）            | 两个或多个极狐GitLab 站点的集合，其中一个主要站点被一个或多个次要站点复制。                                                                | 特定于 Geo |                                                 |                                                 |
| 提升                 | 将站点的角色从次要更改为主要。                                                                                                                                 | 特定于 Geo |                                                 |
| 降级                  | 将站点的角色从主要更改为次要。                                                                                                                                 | 特定于 Geo |                                                 |
| 故障转移                  | 将用户从主要站点转移到次要站点的整个过程。包括提升次要站点，也包含其他部分。例如，计划维护。       | 特定于 Geo |                                                 |
| 复制            | 也叫做“同步”。更新次要站点上的资源来匹配主要站点上的资源的单向过程。                                         | 特定于 Geo |                                                 |

## 示例

### 单节点站点

```mermaid
 graph TD
   subgraph S-Site[Single-node site]
    Node_3[GitLab node]
  end
```

### 多节点站点

```mermaid
 graph TD
   subgraph MN-Site[Multi-node site]
    Node_1[Application node]
    Node_2[Database node]
    Node_3[Gitaly node]
  end
```

### Geo 部署 - 单节点站点

此 Geo 部署有一个单节点主要站点、一个单节点次要站点：

```mermaid
 graph TD
   subgraph Geo deployment
   subgraph Primary[Primary site, single-node]
    Node_1[GitLab node]
  end
  subgraph Secondary1[Secondary site 1, single-node]
    Node_2[GitLab node]
   end
   end
```

### Geo 部署 - 多节点站点

此 Geo 部署有一个多节点主要站点、一个多节点次要站点：

```mermaid
 graph TD
   subgraph Geo deployment
   subgraph Primary[Primary site, multi-node]
    Node_1[Application node]
    Node_2[Database node]
  end
  subgraph Secondary1[Secondary site 1, multi-node]
    Node_5[Application node]
    Node_6[Database node]
   end
   end
```

### Geo 部署 - 混合站点

此 Geo 部署有一个多节点主要站点、一个多节点次要站点和另一个单节点次要站点：

```mermaid
 graph TD
   subgraph Geo deployment
   subgraph Primary[Primary site, multi-node]
    Node_1[Application node]
    Node_2[Database node]
    Node_3[Gitaly node]
  end
  subgraph Secondary1[Secondary site 1, multi-node]
    Node_5[Application node]
    Node_6[Database node]
   end
  subgraph Secondary2[Secondary site 2, single-node]
    Node_7[Single GitLab node]
   end
   end
```
