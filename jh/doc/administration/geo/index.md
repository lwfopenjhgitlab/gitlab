---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Geo **(PREMIUM SELF)**

Geo 是广泛分布的开发团队的解决方案，可作为灾难恢复策略的一部分提供热备份。

## 概览

WARNING:
Geo 在不同版本之间经历了重大变化。支持升级并[记录](#upgrading-geo)，但您应确保使用正确版本的文档进行安装。

对于远离单个极狐GitLab 实例的团队来说，获取大型仓库可能需要很长时间。

Geo 提供极狐GitLab 实例的本地只读站点，这可以减少克隆和获取大型仓库所需的时间，从而加快开发速度。

<!--
To make sure you're using the right version of the documentation, go to [the Geo page on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/administration/geo/index.md) and choose the appropriate release from the **Switch branch/tag** dropdown list. For example, [`v13.7.6-ee`](https://gitlab.com/gitlab-org/gitlab/-/blob/v13.7.6-ee/doc/administration/geo/index.md).
-->

Geo 使用 [Geo 词汇](glossary.md)中描述的一组定义的术语。
请务必熟悉这些术语。

## 用例

实施 Geo 有以下优势：

- 将分布式开发人员克隆和获取大型仓库和项目的时间从几分钟缩短到几秒钟。
- 让您的所有开发人员都能贡献想法，并且并行工作，无论他们身在何处。
- 平衡您的**主要**和**次要**站点之间的只读负载。

此外：

- 除了读取极狐GitLab Web 界面中可用的任何数据外，还可用于克隆和获取项目（请参阅[限制](#limitations)）。
- 克服远程办公室之间的缓慢连接，通过提高分布式团队的速度来节省时间。
- 有助于减少自动化任务、自定义集成和内部工作流程的加载时间。
- 可以在[灾难恢复](disaster_recovery/index.md)场景中快速故障转移到**次要**站点。
- 允许到**次要**站点的[计划故障转移](disaster_recovery/planned_failover.md)。

Geo 提供：

- 只读**次要**站点：维护一个**主要**极狐GitLab 站点，同时仍为每个分布式团队启用只读**次要**站点。
- 身份验证系统挂钩：**次要**站点从**主要**实例接收所有身份验证数据（如用户帐户和登录名）。
- 直观的用户界面：**次要**站点使用您的团队已经习惯的相同 Web 界面。此外，还有视觉通知可以阻止写入操作，并清楚地表明用户在**次要**站点上。

### Gitaly 集群

Geo 不应与 [Gitaly 集群](../gitaly/praefect.md)混淆。有关 Geo 和 Gitaly 集群之间差异的更多信息，请参阅[与 Geo 对比](../gitaly/index.md#comparison-to-geo)。

## 工作原理

除了读取任何数据之外，您的 Geo 实例还可用于克隆和获取项目。这使得远距离处理大型仓库的速度更快。

![Geo overview](replication/img/geo_overview.png)

启用 Geo 后：

- 原始实例称为**主要**站点。
- 复制的只读站点称为**次要**站点。

请记住：

- **次要**站点与**主要**站点对话：
   - 获取登录用户数据 (API)。
   - 复制仓库、LFS 对象和附件 (HTTPS + JWT)。
- **主要**站点不与**次要**站点通信以通知更改 (API)。
- 您可以直接推送到**次要**站点（对于 HTTP 和 SSH，包括 Git LFS）。
- 使用 Geo 时有[限制](#limitations)。

### 架构

下图说明了 Geo 的底层架构。

![Geo architecture](replication/img/geo_architecture.png)

在此图中：

- 有**主要**站点和一个**次要**站点的详细信息。
- 只能在**主要**站点上执行对数据库的写入。 **次要**站点通过 PostgreSQL 流复制接收数据库更新。
- 如果存在，[LDAP 服务器](#ldap) 应配置为针对 [灾难恢复](disaster_recovery/index.md) 方案进行复制。
- **次要**站点使用受 JWT 保护的特殊授权对**主要**站点执行不同类型的同步：
   - 通过 HTTPS 通过 Git 克隆/更新仓库。
   - 附件、LFS 对象和其他文件使用私有 API 端点通过 HTTPS 下载。

从执行 Git 操作的用户的角度来看：

- **主要**站点表现为一个完整的读写极狐GitLab 实例。
- **次要**站点是只读的，但代理 Git 推送操作到**主要**站点。这使得**次要**站点本身似乎支持推送操作。

为了简化图表，省略了一些必要的组件。

- Git over SSH 需要 [`gitlab-shell`](https://jihulab.com/gitlab-cn/gitlab-shell) 和 OpenSSH。
- 需要通过 HTTPS 的 Git `gitlab-workhorse`。

**次要**站点需要两个不同的 PostgreSQL 数据库：

- 从极狐GitLab 主数据库流式传输数据的只读数据库实例。
- [另一个数据库实例](#geo-tracking-database)由**次要**站点内部用于记录已复制的数据。

在**次要**站点中，有一个额外的守护进程：[Geo Log Cursor](#geo-log-cursor)。

<a id="requirements-for-running-geo"></a>

## 运行 Geo 的要求

运行 Geo 需要以下条件：

- 支持 OpenSSH 6.9 或更高版本的操作系统（需要[快速查找数据库中授权的 SSH 密钥](../operations/fast_ssh_key_lookup.md)）。已知以下操作系统附带当前版本的 OpenSSH：
  - [CentOS](https://www.centos.org) 7.4 或更高版本
  - [Ubuntu](https://ubuntu.com) 16.04 或更高版本
- PostgreSQL 12 或 13 与[流复制](https://wiki.postgresql.org/wiki/Streaming_Replication)
  - 请注意，PostgreSQL 12 已弃用，将在 16.0 版本中删除。
- Git 2.9 或更高版本
- 使用 LFS 时用户端的 Git-lfs 2.4.2 或更高版本
- 所有站点都必须运行[相同的极狐GitLab 和 PostgreSQL 版本](setup/database.md#postgresql-replication)。
- 如果在 Geo 站点之间使用不同的操作系统版本，检查 OS 区域设置数据的跨 Geo 站点兼容性<!--[检查 OS 区域设置数据的跨 Geo 站点兼容性](replication/troubleshooting.md#check-os-locale-data-compatibility)-->。  

<!--
- If using different operating system versions between Geo sites, [check OS locale data compatibility](replication/troubleshooting.md#check-os-locale-data-compatibility) across Geo sites.
-->

此外，请查看极狐GitLab [最低要求](../../install/requirements.md)，我们建议您使用最新版本的极狐GitLab 以获得更好的体验。

<a id="firewall-rules"></a>

### 防火墙规则

下表列出了必须在**主要**和**次要**站点之间为 Geo 开放的基本端口。为了简化故障转移，我们建议在两个方向上打开端口。

| 源站点 | 源端口 | 目的地站点 | 目的地端口 | 协议    |
|-------------|-------------|------------------|------------------|-------------|
| 主要     | Any         | 次要        | 80               | TCP (HTTP)  |
| 主要     | Any         | 次要        | 443              | TCP (HTTPS) |
| 次要   | Any         | 主要          | 80               | TCP (HTTP)  |
| 次要   | Any         | 主要          | 443              | TCP (HTTPS) |
| 次要   | Any         | 主要          | 5432             | TCP         |

在[软件包默认值文档](../package_information/defaults.md)中查看极狐GitLab 使用的端口的完整列表。

NOTE:
[Web 终端](../../ci/environments/index.md#web-terminals-deprecated)支持要求您的负载均衡器正确处理 WebSocket 连接。
使用 HTTP 或 HTTPS 代理时，您的负载均衡器必须配置为通过 `Connection` 和 `Upgrade` hop-by-hop headers。有关更多详细信息，请参阅 [web 终端](../integration/terminal.md)集成指南。

NOTE:
对端口 443 使用 HTTPS 协议时，您必须向负载均衡器添加 SSL 证书。如果您希望在极狐GitLab 应用程序服务器上终止 SSL，请使用 TCP 协议。

#### 内部 URL

从任何 Geo 次要站点到主 Geo 站点的 HTTP 请求使用主 Geo 站点的内部 URL。如果在管理中心的主 Geo 站点设置中未明确定义，则使用主要站点公开 URL。

要更新主 Geo 站点的内部 URL：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **Geo > 站点**。
1. 在主要站点上选择 **编辑**。
1. 更改 **内部 URL**，然后选择 **保存更改**。

### LDAP

我们建议，如果您在**主要**站点上使用 LDAP，您还要在每个**次要**站点上设置辅助 LDAP 服务器。否则，用户无法使用 HTTP 基本身份验证在**次要**站点上通过 HTTP 执行 Git 操作。但是，通过 SSH 和个人访问令牌的 Git 仍然有效。

NOTE:
所有 **次要** 站点都可以共享一个 LDAP 服务器，但额外的延迟可能是一个问题。此外，如果将**次要**站点提升为**主要**站点，请考虑在[灾难恢复](disaster_recovery/index.md)场景中可用的 LDAP 服务器。

查看有关如何在 LDAP 服务中设置复制的说明。说明因使用的软件或服务而异。例如，OpenLDAP 提供[这些说明](https://www.openldap.org/doc/admin24/replication.html)。

### Geo 跟踪数据库

跟踪数据库实例用作元数据来控制本地实例磁盘上需要更新的内容。例如：

- 下载新 assets。
- 获取新的 LFS 对象。
- 从最近更新的仓库中获取更改。

因为复制的数据库实例是只读的，所以我们需要为每个**次要**站点添加这个额外的数据库实例。

### Geo Log Cursor

这个守护进程：

- 读取由**主要**站点复制到**次要**数据库实例的事件日志。
- 使用必须执行的更改更新地理跟踪数据库实例。

当跟踪数据库实例中的某些内容被标记为要更新时，**次要**站点上运行的异步作业会执行所需的操作并更新状态。

这种新架构使极狐GitLab 能够灵活应对站点之间的连接问题。**次要**站点与**主要**站点断开连接的时间无关紧要，因为它能够以正确的顺序重播所有事件并再次与**主要**站点同步。

<a id="limitations"></a>

## 限制

WARNING:
此限制列表仅反映最新版本的极狐GitLab。如果您使用的是旧版本，则可能会有额外的限制。

- 直接推送到**次要**站点会将请求重定向（对于 HTTP）或代理（对于 SSH）到**主要**站点的请求，而不是直接处理它，除非在 URI 中使用嵌入了凭据的 HTTP 上的 Git。例如，`https://user:password@secondary.tld`。
- **主要**站点必须在线才能进行 OAuth 登录。现有会话和 Git 不受影响。<!--正在计划支持**次要**站点使用独立于主要站点的 OAuth 提供程序。-->
- 安装需要多个手动步骤，视情况而定，总共可能需要大约一个小时。<!--考虑使用极狐GitLab 环境工具包来部署和操作生产 GitLab 实例，包括自动化日常任务。-->
- 议题/合并请求的实时更新（例如，通过长轮询）在**次要**站点上不起作用。
- 极狐GitLab Runners 无法在**次要**站点注册。
- [选择性同步](replication/configuration.md#selective-synchronization)仅限制复制哪些仓库和文件。整个 PostgreSQL 数据仍然被复制。选择性同步不是为了适应合规性/出口控制用例而构建的。
- [Pages 访问控制](../../user/project/pages/pages_access_control.md)在次要节点上无效。
- [极狐GitLab chart 的 Geo](https://docs.gitlab.cn/charts/advanced/geo/) 不支持统一 URL<!--[统一 URLs](secondary_proxy/index.md#set-up-a-unified-url-for-geo-sites)-->。
- 由于所有未升级的次要站点的完全重新同步和重新配置，多次要站点的灾难恢复会导致停机。
- 对于 Git over SSH，为了使项目克隆 URL 正确显示，无论您正在浏览哪个站点，次要站点都必须使用与主站点相同的端口。
- 对于通过 SSH 对次要站点进行 Git 的推送，不适用于超过 1.86 GB 的推送。
- 备份[无法在次要站点上运行](replication/troubleshooting.md#message-error-canceling-statement-due-to-conflict-with-recovery)。

<!--
### 复制/验证的限制

有所有极狐GitLab [数据类型](replication/datatypes.md)的完整列表以及[对复制和验证的现有支持](replication/datatypes.md#limitations-on-replicationverification)。
-->

### 查看主要站点上的复制数据

如果您尝试查看主要站点上的复制数据，您会收到一条警告，指出这可能不一致：

> 使用统一 URL 时，无法从主要站点查看项目数据。直接访问次要站点。

查看特定次要站点的项目复制数据的唯一方法是直接访问该次要站点。例如，`https://<IP of your secondary site>/admin/geo/replication/projects`。

请注意，启用[管理模式](../settings/sign_in_restrictions.md#admin-mode)后，上述 URL 无效。

使用统一 URL 时，直接访问次要站点意味着您必须将请求路由到次要站点，具体如何完成取决于您的网络配置。例如，使用 DNS 将请求路由到适当的站点，那么您可以编辑本地计算机的 `/etc/hosts` 文件，将请求路由到所需的次要站点。如果 Geo 站点全部位于负载均衡器后面，则根据负载均衡器，您也许能够配置来自您的 IP 的所有请求转到特定的次要站点。

<a id="setup-instructions"></a>

## 设置说明

有关设置说明，请参阅[设置 Geo](setup/index.md)。

<a id="post-installation-documentation"></a>

## 安装后文档

在**次要**站点上安装极狐GitLab 并执行初始配置后，请参阅以下文档获取安装后信息。

### 配置 Geo

有关配置 Geo 的信息，请参阅 [Geo 配置](replication/configuration.md)。

<a id="upgrading-geo"></a>

### 升级 Geo

有关如何将您的 Geo 站点更新到最新极狐GitLab 版本的信息，请参阅[升级 Geo 站点](replication/upgrading_the_geo_sites.md)。

<a id="pausing-and-resuming-replication"></a>

### 暂停和恢复复制

> 引入于 13.2 版本。

WARNING:
在 13.2 和 13.3 中，在次要站点暂停时将次要站点提升为主要站点会失败。在提升次要节点之前不要暂停复制。如果网站已暂停，请务必在提升前恢复。此问题已在 13.4 及更高版本中修复。

WARNING:
只有使用 Linux 软件包管理的数据库的 Geo 安装实例才支持暂停和恢复复制。不支持外部数据库。

在某些情况下，例如在[升级](replication/upgrading_the_geo_sites.md)或[计划故障转移](disaster_recovery/planned_failover.md)期间，需要暂停主节点和次要节点之间的复制。

暂停和恢复复制是通过命令行工具从启用了 `postgresql` 服务的次要站点中的节点完成的。

如果 `postgresql` 位于独立数据库节点上，请确保该节点上的 `gitlab.rb` 包含配置行 `gitlab_rails['geo_node_name'] = 'node_name'`，其中 `node_name` 与 `geo_name_name` 在应用程序节点上相同。

**暂停：（从次要节点）**

```shell
gitlab-ctl geo-replication-pause
```

**恢复：（从次要节点）**

```shell
gitlab-ctl geo-replication-resume
```

### 为多个节点配置 Geo

有关为多个节点配置 Geo 的信息，请参阅[用于多个服务器的 Geo](replication/multiple_servers.md)。

### 配置 Geo 使用对象存储

有关配置 Geo 使用对象存储的信息，请参阅[使用对象存储的 Geo](replication/object_storage.md)。

### 灾难恢复

有关在灾难恢复情况下，使用 Geo 来减轻数据丢失和恢复服务的信息，请参阅[灾难恢复](disaster_recovery/index.md)。

### 复制 Container Registry

有关如何复制 Container Registry 的更多信息，请参阅[**次要**站点的 Container Registry](replication/container_registry.md)。

<!--
### Geo 次要站点代理

有关在次要站点上使用 Geo 代理的更多信息，请参阅[次要站点的 Geo 代理](secondary_proxy/index.md)。

### 安全审核

有关 Geo 安全的更多信息，请参阅 [Geo 安全审核](replication/security_review.md)。
-->

### 调试 Geo

有关调整 Geo 的更多信息，请参阅[调试 Geo](replication/tuning.md)。

<!--
### 设置 location-aware Git URL

For an example of how to set up a location-aware Git remote URL with AWS Route53, see [Location-aware Git remote URL with AWS Route53](replication/location_aware_git_url.md).
-->

### 回填

设置**次要**站点后，它会开始在称为**回填**的过程中从**主要**站点复制丢失的数据。您可以从浏览器中的**主要**站点的 **Geo 节点**仪表盘监控每个 Geo 站点上的同步过程。

回填期间发生的失败计划在回填结束时重试。

## 删除 Geo 站点

有关删除 Geo 站点的详细信息，请参阅[删除**次要** Geo 站点](replication/remove_geo_site.md)。

## 禁用 Geo

要了解如何禁用 Geo，请参阅[禁用 Geo](replication/disable_geo.md)。

<!--
## 常见问题

有关常见问题的答案，请参阅 [FAQ](replication/faq.md)。
-->

## 日志文件

Geo 将结构化日志消息存储在 `geo.log` 文件中。对于 Linux 软件包安装实例，此文件位于 `/var/log/gitlab/gitlab-rails/geo.log`。

此文件包含有关 Geo 何时尝试同步仓库和文件的信息。文件中的每一行都包含一个可以提取的单独 JSON 条目。例如，Elasticsearch 或 Splunk。

例如：

```json
{"severity":"INFO","time":"2017-08-06T05:40:16.104Z","message":"Repository update","project_id":1,"source":"repository","resync_repository":true,"resync_wiki":true,"class":"Gitlab::Geo::LogCursor::Daemon","cursor_delay_s":0.038}
```

此消息显示 Geo 检测到项目 `1` 需要更新仓库。

<!--
## 故障排除

有关故障排除步骤，请参阅 [Geo 故障排除](replication/troubleshooting.md)。
-->
