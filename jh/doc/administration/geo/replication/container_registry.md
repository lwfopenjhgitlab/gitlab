---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 用于次要站点的容器镜像库 **(PREMIUM SELF)**

您可以在您的**次要** Geo 站点上设置一个容器镜像库，以镜像**主要** Geo 站点上的镜像。

NOTE:
容器镜像库复制仅用于灾难恢复目的。我们不建议从次要站点中拉取容器镜像库数据。 <!--For a feature proposal to implement it in the
future, see [Geo: Accelerate container images by serving read request from secondary site](https://gitlab.com/gitlab-org/gitlab/-/issues/365864) for details.-->

## 支持的容器镜像库

Geo 支持以下类型的容器镜像库：

- [Docker](https://docs.docker.com/registry/)
- [OCI](https://github.com/opencontainers/distribution-spec/blob/main/spec.md)

## 支持的镜像格式

Geo 支持以下容器镜像格式：

- [Docker V2, schema 1](https://docs.docker.com/registry/spec/manifest-v2-1/)
- [Docker V2, schema 2](https://docs.docker.com/registry/spec/manifest-v2-2/)
- [OCI (Open Container Initiative)](https://github.com/opencontainers/image-spec)

此外，Geo 还支持 [BuildKit 缓存镜像](https://github.com/moby/buildkit)。

## 支持的存储

### Docker

有关受支持的镜像库存储驱动程序的更多信息，请参阅 [Docker 镜像库存储驱动程序](https://docs.docker.com/registry/storage-drivers/)。

阅读部署 Registry 时的[负载均衡注意事项](https://docs.docker.com/registry/deploying/#load-balancing-considerations)，以及如何为极狐GitLab 集成的[容器镜像库](../../packages/container_registry.md#use-object-storage)设置存储驱动。

### 支持 OCI 产物的镜像库

以下镜像库支持 OCI 产物：

- CNCF Distribution - local/offline verification
- Azure Container Registry (ACR)
- Amazon Elastic Container Registry (ECR)
- Google Artifact Registry (GAR)
- GitHub Packages container registry (GHCR)
- Bundle Bar

获取更多信息，查看 [OCI Distribution Specification](https://github.com/opencontainers/distribution-spec)。

## 配置容器镜像库复制

您可以启用与存储无关的复制，以便将其用于云或本地存储。每当将新镜像推送到**主要**站点时，每个**次要**站点都会将其拉到自己的容器镜像库中。

要配置容器镜像库复制：

1. 配置**主要**站点。
1. 配置**次要**站点。
1. 验证容器镜像库复制。

### 配置**主要**站点

在执行后续步骤之前，请确保您已设置容器镜像库，并在**主要**站点上工作。

为了能够复制新的容器镜像，容器镜像库必须针对每次推送，向**主要**站点发送通知事件。容器镜像库和**主要**站点上的 Web 节点之间共享的令牌，用于使通信更加安全。

1. SSH 进入您的极狐GitLab **主**服务器并以 root 身份登录（对于 HA，只需要一个 Registry 节点）：

   ```shell
   sudo -i
   ```

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   registry['notifications'] = [
     {
       'name' => 'geo_event',
       'url' => 'https://<example.com>/api/v4/container_registry_event/events',
       'timeout' => '500ms',
       'threshold' => 5,
       'backoff' => '1s',
       'headers' => {
         'Authorization' => ['<replace_with_a_secret_token>']
       }
     }
   ]
   ```

   NOTE:
   将 `<example.com>` 替换为主要站点的 `/etc/gitlab/gitlab.rb` 文件中定义的 `external_url`，并将 `<replace_with_a_secret_token>` 替换为以字母开头的区分大小写的字母数字字符串。您可以使用 `< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c 32 | sed "s/^[0-9]*//"; echo`。

   NOTE:
   如果您使用外部镜像库（不是与极狐GitLab 集成的镜像库），您必须自己将这些设置添加到其配置中。在这种情况下，您只需在 `/etc/gitlab/gitlab.rb` 文件的 `registry.notification_secret` 部分中指定通知密码。

1. 仅适用于 HA。在每个 Web 节点上编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   registry['notification_secret'] = '<replace_with_a_secret_token_generated_above>'
   ```

1. 重新配置您刚刚更新的每个节点：


   ```shell
   gitlab-ctl reconfigure
   ```

### 配置**次要**站点

在执行后续步骤之前，请确保您已设置容器镜像库并在**次要**站点上工作。

应在您希望看到复制的容器镜像的每个**次要**站点上执行以下步骤。

因为我们需要允许**次要**站点与**主要**站点容器镜像库进行安全通信，所以我们需要为所有站点提供一个密钥对。**次要**站点使用此密钥生成一个短期 JWT，该 JWT 只能拉取以访问**主要**站点容器镜像库。

对于**次要**站点上的每个应用程序和 Sidekiq 节点：

1. SSH 进入节点并以 `root` 用户身份登录：

   ```shell
   sudo -i
   ```

1. 将 `/var/opt/gitlab/gitlab-rails/etc/gitlab-registry.key` 从**主要**站点复制到节点。

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加：

   ```ruby
   gitlab_rails['geo_registry_replication_enabled'] = true

   # Primary registry's hostname and port, it will be used by
   # the secondary node to directly communicate to primary registry
   gitlab_rails['geo_registry_replication_primary_api_url'] = 'https://primary.example.com:5050/'
   ```

1. 重新配置节点，使更改生效：

   ```shell
   gitlab-ctl reconfigure
   ```

### 验证复制

要验证容器镜像库复制是否正常工作，请在**次要**站点上：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **Geo > 节点**。初始复制或“回填”可能仍在进行中。

您可以从浏览器中的**主要**站点的 **Geo 节点** 仪表盘监控每个 Geo 站点上的同步过程。
