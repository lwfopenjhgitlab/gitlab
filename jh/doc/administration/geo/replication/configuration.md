---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# Geo 配置 **(PREMIUM SELF)**

## 配置新的**次要**站点

NOTE:
这是设置**次要** Geo 站点的最后一步。设置过程的各个阶段必须按记录的顺序完成。如果没有，在继续之前，[完成所有之前的阶段](../setup/index.md#using-omnibus-gitlab)。

确保您[设置数据库复制](../setup/database.md)，并且在**主要站点和次要站点**[配置快速查找授权 SSH 密钥](../../operations/fast_ssh_key_lookup.md)。

配置**次要**站点的基本步骤是：

- 在**主要**站点和**次要**站点之间复制所需的配置。
- 在每个**次要**站点上配置一个跟踪数据库。
- 在每个**次要**站点上启动极狐GitLab。

我们鼓励您先通读所有步骤，然后再在测试/生产环境中执行它们。

NOTE:
**不要**为**次要**站点设置任何自定义身份验证。这由**主要**站点处理。
需要访问**管理中心**的任何更改，都需要在**主要**站点中完成，因为**次要**站点是只读副本。

### 步骤 1. 手动复制 secret GitLab 值

极狐GitLab 在 `/etc/gitlab/gitlab-secrets.json` 文件中存储了许多 secret 值，这些值在站点的所有节点上*必须*相同。在有一种方法可以在站点之间自动复制它们之前，必须手动将它们复制到**次要站点的所有节点**。

1. 通过 SSH 连接到您的**主要站点上的 Rails 节点**，然后执行以下命令：

   ```shell
   sudo cat /etc/gitlab/gitlab-secrets.json
   ```

   将以 JSON 格式显示需要复制的 secrets。

1. SSH **进入您的次要 Geo 站点上的每个节点**，并以 `root` 用户身份登录：

   ```shell
   sudo -i
   ```

1. 备份任何现有的 secrets：

   ```shell
   mv /etc/gitlab/gitlab-secrets.json /etc/gitlab/gitlab-secrets.json.`date +%F`
   ```

1. 将 `/etc/gitlab/gitlab-secrets.json` 从**主要站点上的 Rails 节点**，复制到**次要站点上的每个节点**，或在节点之间复制并粘贴文件内容：

   ```shell
   sudo editor /etc/gitlab/gitlab-secrets.json

   # paste the output of the `cat` command you ran on the primary
   # save and exit
   ```

1. 确保文件权限正确：

   ```shell
   chown root:root /etc/gitlab/gitlab-secrets.json
   chmod 0600 /etc/gitlab/gitlab-secrets.json
   ```

1. 重新配置您的次要站点上的**每个 Rails、Sidekiq 和 Gitaly 节点**，使更改生效：

   ```shell
   gitlab-ctl reconfigure
   gitlab-ctl restart
   ```

<a id="step-2-manually-replicate-the-primary-sites-ssh-host-keys"></a>

### 步骤 2. 手动复制**主要**站点的 SSH 主机密钥

极狐GitLab 与系统安装的 SSH daemon 集成，指定一个用户（通常名为 `git`），通过该用户处理所有访问请求。

在[灾难恢复](../disaster_recovery/index.md)情况下，极狐GitLab 系统管理员将**次要**站点提升为**主要**站点。**主**域名的 DNS 记录也应更新为指向新的**主要**站点（以前是**次要**站点）。这样做可以避免更新 Git 远端和 API URL。

这会导致对新提升的**主要**站点的所有 SSH 请求由于 SSH 主机密钥不匹配而失败。为防止出现这种情况，必须手动将主 SSH 主机密钥复制到**次要**站点。

1. 通过 SSH 连接到**您的次要站点上的每个节点**，并以 `root` 用户身份登录：

   ```shell
   sudo -i
   ```

1. 备份任何现有的 SSH 主机密钥：

   ```shell
   find /etc/ssh -iname 'ssh_host_*' -exec cp {} {}.backup.`date +%F` \;
   ```

1. 从**主要**站点复制 OpenSSH 主机密钥：

   如果您可以使用 **root** 用户访问提供 SSH 流量的**主要站点上的节点**之一（通常是 Rails 应用程序主节点）：

   ```shell
   # Run this from the secondary site, change `<primary_site_fqdn>` for the IP or FQDN of the server
   scp root@<primary_node_fqdn>:/etc/ssh/ssh_host_*_key* /etc/ssh
   ```

   如果您只能通过具有 `sudo` 权限的用户访问：

   ```shell
   # Run this from the node on your primary site:
   sudo tar --transform 's/.*\///g' -zcvf ~/geo-host-key.tar.gz /etc/ssh/ssh_host_*_key*

   # Run this on each node on your secondary site:
   scp <user_with_sudo>@<primary_site_fqdn>:geo-host-key.tar.gz .
   tar zxvf ~/geo-host-key.tar.gz -C /etc/ssh
   ```

1. 在您的**次要站点上的每个节点**上，确保文件权限正确：

   ```shell
   chown root:root /etc/ssh/ssh_host_*_key*
   chmod 0600 /etc/ssh/ssh_host_*_key
   ```

1. 要验证密钥指纹匹配，请在每个站点的主节点和次要节点上执行以下命令：

   ```shell
   for file in /etc/ssh/ssh_host_*_key; do ssh-keygen -lf $file; done
   ```

   您应该得到一个类似于这个的输出，并且它们在两个节点上应该是相同的：

   ```shell
   1024 SHA256:FEZX2jQa2bcsd/fn/uxBzxhKdx4Imc4raXrHwsbtP0M root@serverhostname (DSA)
   256 SHA256:uw98R35Uf+fYEQ/UnJD9Br4NXUFPv7JAUln5uHlgSeY root@serverhostname (ECDSA)
   256 SHA256:sqOUWcraZQKd89y/QQv/iynPTOGQxcOTIXU/LsoPmnM root@serverhostname (ED25519)
   2048 SHA256:qwa+rgir2Oy86QI+PZi/QVR+MSmrdrpsuH7YyKknC+s root@serverhostname (RSA)
   ```

1. 验证您是否拥有现有私钥的正确公钥：

   ```shell
   # This will print the fingerprint for private keys:
   for file in /etc/ssh/ssh_host_*_key; do ssh-keygen -lf $file; done

   # This will print the fingerprint for public keys:
   for file in /etc/ssh/ssh_host_*_key.pub; do ssh-keygen -lf $file; done
   ```

   NOTE:
   私钥和公钥命令的输出应生成相同的指纹。

1. 在您的**次要站点上的每个节点**上重新启动 `sshd`：

   ```shell
   # Debian or Ubuntu installations
   sudo service ssh reload

   # CentOS installations
   sudo service sshd reload
   ```

1. 验证 SSH 是否仍然有效。

   在新终端中通过 SSH 连接到极狐GitLab **次要**服务器。如果无法连接，请根据前面的步骤验证权限是否正确。

### 步骤 3. 添加**次要**站点

1. 通过 SSH 连接到您的次要站点上的**每个 Rails 和 Sidekiq 节点**，并以 root 身份登录：

   ```shell
   sudo -i
   ```

1. 编辑 `/etc/gitlab/gitlab.rb` 并为您的站点添加一个**独特的**名称。您在接下来的步骤中需要它：

   ```ruby
   ##
   ## The unique identifier for the Geo site. See
   ## https://docs.gitlab.com/ee/administration/geo_nodes.html#common-settings
   ##
   gitlab_rails['geo_node_name'] = '<site_name_here>'
   ```

1. 重新配置您的**次要站点上的每个 Rails 和 Sidekiq 节点**，使更改生效：

   ```shell
   gitlab-ctl reconfigure
   ```

1. 导航到主节点上的极狐GitLab 实例：
   1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
   1. 选择 **管理中心**。
   1. 在左侧边栏中，选择 **Geo > 站点**。
   1. 选择 **添加站点**。
       
       ![Add secondary site](img/adding_a_secondary_v15_8.png)
   
   1. 在 `/etc/gitlab/gitlab.rb` 中使用 `gitlab_rails['geo_node_name']` 填写 **Name**。它们必须始终每个字符*完全*匹配。
   1. 用 `/etc/gitlab/gitlab.rb` 中的 `external_url` 填写 **URL**。这些值必须始终匹配，但如果一个以 `/` 结尾而另一个不以 `/` 结尾，则无关紧要。
   1. 可选。在 **内部 URL（可选）** 中，输入主站点的内部 URL。
   1. 可选。选择 **次要** 站点应复制哪些组或存储分片。留空表示复制所有内容。在[选择性同步](#selective-synchronization)文档中阅读更多内容。
   1. 选择 **保存更改**，添加 **次要** 站点。
1. SSH 到您的 **次要站点上的每个 Rails 和 Sidekiq 节点**，并重新启动服务：

   ```shell
   gitlab-ctl restart
   ```

   通过运行以下命令检查您的 Geo 设置是否存在任何常见问题：

   ```shell
   gitlab-rake gitlab:geo:check
   ```

   <!--如果任何检查失败，请查看[故障排除文档](troubleshooting.md)。-->

1. 通过 SSH 连接到您的**主要站点上的 Rails 或 Sidekiq 服务器**，并以 root 身份登录，验证**次要**站点是否可访问或者您的 Geo 设置存在任何常见问题：

   ```shell
   gitlab-rake gitlab:geo:check
   ```

   <!--如果任何检查失败，请查看[故障排除文档](troubleshooting.md)。-->

添加到 Geo 管理页面并重新启动后，**次要**站点在称为 **backfill** 的过程中自动开始从**主要**站点复制丢失的数据。
同时，**主要**站点开始通知每个**次要**站点任何更改，以便**次要**站点可以立即对这些通知采取行动。

确保次要站点正在运行且可访问。您可以使用与主要站点相同的凭据登录到次要站点。

<a id="step-4-optional-using-custom-certificates"></a>

### 步骤 4.（可选）使用自定义证书

如果出现以下情况，您可以安全地跳过此步骤：

- 您的**主要**站点使用公共 CA 颁发的 HTTPS 证书。
- 您的**主要**站点仅使用 CA 颁发的（非自签名）HTTPS 证书连接到外部服务。

<a id="custom-or-self-signed-certificate-for-inbound-connections"></a>

#### 入站连接的自定义或自签名证书

如果您的极狐GitLab Geo **主要**站点使用自定义的或[自签名证书来保护入站 HTTPS 连接](https://docs.gitlab.cn/omnibus/settings/ssl.html#install-custom-public-certificates)，该证书可以是单域名证书，也可以是多域名证书。

根据您的证书类型安装正确的证书：

- **多域名证书**，包括主要站点和次要站点域名：在**次要站点中的所有 Rails、Sidekiq 和 Gitaly 节点**上的 `/etc/gitlab/ssl` 安装证书。
- **单域名证书**，其中证书特定于每个 Geo 站点域名：为您的**次要**站点的域名生成一个有效证书，并按照[这些说明](https://docs.gitlab.cn/omnibus/settings/ssl.html#install-custom-public-certificates)在**次要**站点中的**所有 Rails、Sidekiq 和 Gitaly 节点**上将其安装到 `/etc/gitlab/ssl`。

#### 连接到使用自定义证书的外部服务

需要将外部服务的自签名证书副本添加到需要访问服务的所有**主要**站点节点上的信任库中。

为了让**次要**站点能够访问相同的外部服务，这些证书*必须*添加到**次要**站点的信任库中。

如果您的**主要**站点使用[自定义或自签名证书用于入站 HTTPS 连接](#custom-or-self-signed-certificate-for-inbound-connections)，则**主要**站点的证书需要添加到**次要**站点的信任库：

1. SSH 到您的**次要站点上的每个 Rails、Sidekiq 和 Gitaly 节点**，并以 root 身份登录：

   ```shell
   sudo -i
   ```

1. 从**主要**站点复制受信任的证书：

   如果您可以使用 root 用户访问**主要**站点上提供 SSH 流量的节点之一：

   ```shell
   scp root@<primary_site_node_fqdn>:/etc/gitlab/trusted-certs/* /etc/gitlab/trusted-certs
   ```

   如果您只能通过具有 sudo 权限的用户进行访问：

   ```shell
   # Run this from the node on your primary site:
   sudo tar --transform 's/.*\///g' -zcvf ~/geo-trusted-certs.tar.gz /etc/gitlab/trusted-certs/*

   # Run this on each node on your secondary site:
   scp <user_with_sudo>@<primary_site_node_fqdn>:geo-trusted-certs.tar.gz .
   tar zxvf ~/geo-trusted-certs.tar.gz -C /etc/gitlab/trusted-certs
   ```

1. 在您的次要站点中重新配置**每个更新的 Rails、Sidekiq 和 Gitaly 节点**：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

### 步骤 5. 通过 HTTP/HTTPS 启用 Git 访问

Geo 通过 HTTP/HTTPS 同步仓库，因此需要启用此克隆方法。默认情况下启用此功能，但如果将现有站点转换为 Geo，则应检查：

在**主要**站点上：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **可见性和访问控制**。
1. 如果使用 Git over SSH：
   1. 确保 **启用 Git 访问协议** 设置为 **SSH 和 HTTP(S)**。
   1. 在 **所有主要站点和次要站点** 上，遵循[在数据库中快速查找授权的 SSH 密钥](../../operations/fast_ssh_key_lookup.md)。
1. 如果不使用 Git over SSH，将 **启用 Git 访问协议** 设置为 **仅 HTTP(S)**。


### 步骤 6. 验证**次要**站点的正常运行

您可以使用与**主要**站点相同的凭据登录到**次要**站点。登录后：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏中，选择 **Geo > 站点**。
1. 验证它是否被正确识别为**次要** Geo 站点，并且 Geo 已启用。

初始复制可能需要一些时间。网站的状态或 “backfill” 可能仍在进行中。您可以从浏览器中的**主要**站点的 **Geo 站点**仪表盘监控每个 Geo 站点上的同步过程。

![Geo dashboard](img/geo_dashboard_v14_0.png)

如果您的安装无法正常工作，请查看[故障排除文档](troubleshooting.md)。

仪表盘中最明显的两个问题是：

1. 数据库复制不能正常工作。
1. 实例到实例通知不起作用。在这种情况下，可以是以下内容：
   - 您正在使用自定义证书或自定义 CA（请参阅[故障排除文档](troubleshooting.md)）。
   - 该实例有防火墙（检查您的防火墙规则）。

禁用**次要**站点会停止同步过程。

如果在**主要**站点上为多个仓库分片自定义了 `git_data_dirs`，您必须在每个**次要**站点上复制相同的配置。

<!--
将您的用户指向[使用 Geo 站点指南](usage.md)。
-->

目前，以下是同步的：

- Git 仓库。
- Wikis。
- LFS 对象。
- 议题、合并请求、代码片段和评论附件。
- 用户、群组和项目头像。

<a id="selective-synchronization"></a>

## 选择性同步

Geo 支持选择性同步，这允许管理员选择应该由**次要**站点同步哪些项目。
可以按组或存储分片选择项目的子集。前者非常适合复制属于用户子集的数据，而后者更适合将 Geo 逐步推广到大型极狐GitLab 实例。

需要注意的是选择性同步：

1. 不限制来自**次要**站点的权限。
1. 不隐藏**次要**站点的项目元数据。
   - 由于 Geo 当前依赖 PostgreSQL 复制，所有项目元数据都被复制到**次要**站点，但尚未选择的仓库是空的。
1. 不会减少为 Geo 事件日志生成的事件数量。
   - 只要存在任何**次要**站点，**主要**站点就会生成事件。选择性同步限制是在**次要**站点上实现的，而不是在**主要**站点上。

### 未复制仓库上的 Git 操作

<!--
> [Introduced](https://gitlab.com/groups/gitlab-org/-/epics/2562) in GitLab 12.10 for HTTP(S) and in GitLab 13.0 for SSH.
-->

对于存在于**主要**站点但不存在于**次要**站点上的仓库，支持通过 HTTP(S) 和 SSH 进行 Git 克隆、拉取和推送操作。在以下情况，可能会发生这种情况：

- 选择性同步不包括附加到仓库的项目。
- 正在积极复制仓库，但尚未完成。

## 升级 Geo

请参阅[升级 Geo 站点文档](upgrading_the_geo_sites.md)。

## 故障排除

请参阅[故障排除文档](troubleshooting.md)。
