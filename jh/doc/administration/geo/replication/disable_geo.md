---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 禁用 Geo **(PREMIUM SELF)**

如果您想在测试后恢复到常规的 Omnibus 设置，或者您遇到了灾难恢复情况并且想暂时禁用 Geo，您可以使用这些说明来禁用您的 Geo 设置。

如果正确删除它们，则禁用 Geo，与没有次要 Geo 站点的有效 Geo 设置之间应该没有功能差异。

要禁用 Geo，请按照下列步骤操作：

1. [删除所有次要 Geo 站点](#remove-all-secondary-geo-sites)。
1. [从 UI 中删除主要站点](#remove-the-primary-site-from-the-ui)。
1. [删除次要复制槽](#remove-secondary-replication-slots)。
1. [删除与 Geo 相关的配置](#remove-geo-related-configuration)。
1. [可选。恢复 PostgreSQL 设置以使用密码并侦听 IP](#optional-revert-postgresql-settings-to-use-a-password-and-listen-on-an-ip)。

<a id="remove-all-secondary-geo-sites"></a>

## 删除所有次要 Geo 站点

要禁用 Geo，您需要首先删除所有次要 Geo 站点，这意味着这些站点上将不再发生复制。您可以按照我们的文档[删除您的次要地理站点](remove_geo_site.md)。

如果您要继续使用的当前站点是次要站点，则需要先将其提升为主要站点。
您可以使用我们关于[如何提升次要站点](../disaster_recovery/index.md#step-3-promoting-a-secondary-site)的步骤来执行此操作。

<a id="remove-the-primary-site-from-the-ui"></a>

## 从 UI 中删除主要站点

要删除**主要**站点：

1. [删除所有二级 Geo 站点](remove_geo_site.md)。
1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，选择 **Geo > 节点**。
1. 为**主要**节点选择 **删除**。
1. 出现提示时选择 **删除** 进行确认。

<a id="remove-secondary-replication-slots"></a>

## 删除次要复制槽

要删除次要复制槽，请在 PostgreSQL 控制台 (`sudo gitlab-psql`) 中的主 Geo 节点上运行以下查询之一：

- 如果您已经有一个 PostgreSQL 集群，请按名称删除各个复制槽，以防止从同一集群中删除您的次要数据库。您可以使用以下内容获取所有名称，然后删除每个单独的槽：

  ```sql
  SELECT slot_name, slot_type, active FROM pg_replication_slots; -- view present replication slots
  SELECT pg_drop_replication_slot('slot_name'); -- where slot_name is the one expected from above
  ```

- 要删除所有次要复制槽：

  ```sql
  SELECT pg_drop_replication_slot(slot_name) FROM pg_replication_slots;
  ```

<a id="remove-geo-related-configuration"></a>

## 删除与 Geo 相关的配置

1. 对于主 Geo 站点上的每个节点，通过 SSH 连接到节点并以 root 身份登录：

   ```shell
   sudo -i
   ```

1. 编辑 `/etc/gitlab/gitlab.rb` 并通过删除启用 `geo_primary_role` 的任何行来删除与 Geo 相关的配置：

   ```ruby
   ## In pre-11.5 documentation, the role was enabled as follows. Remove this line.
   geo_primary_role['enable'] = true

   ## In 11.5+ documentation, the role was enabled as follows. Remove this line.
   roles ['geo_primary_role']
   ```

1. 进行这些更改后，[重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

<a id="optional-revert-postgresql-settings-to-use-a-password-and-listen-on-an-ip"></a>

## （可选）恢复 PostgreSQL 设置以使用密码并侦听 IP

如果您想删除 PostgreSQL 特定的设置并恢复到默认值（使用套接字代替），您可以安全地从 `/etc/gitlab/gitlab.rb` 文件中删除以下行：

```ruby
postgresql['sql_user_password'] = '...'
gitlab_rails['db_password'] = '...'
postgresql['listen_address'] = '...'
postgresql['md5_auth_cidr_addresses'] =  ['...', '...']
```
