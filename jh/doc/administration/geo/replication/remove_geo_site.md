---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 删除次要 Geo 站点 **(PREMIUM SELF)**

可以使用**主要**站点的 Geo 管理页面，从 Geo 集群中删除**次要**站点。要删除 **次要** 站点：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **Geo > 节点**。
1. 为您要删除的**次要**站点选择 **删除** 按钮。
1. 出现提示时选择 **删除** 进行确认。

一旦从 Geo 管理页面中删除，您必须停止并卸载**次要**站点。对于次要站点上的每个节点：

1. 停止极狐GitLab：

   ```shell
   sudo gitlab-ctl stop
   ```

1. 卸载极狐GitLab：

   NOTE:
   如果还必须从实例中清除极狐GitLab 数据，请参阅如何[卸载 Linux 软件包及其所有数据](https://docs.gitlab.cn/omnibus/installation/#uninstall-the-linux-package-omnibus)。

   ```shell
   # Stop gitlab and remove its supervision process
   sudo gitlab-ctl uninstall

   # Debian/Ubuntu
   sudo dpkg --remove gitlab-ee

   # Redhat/Centos
   sudo rpm --erase gitlab-ee
   ```

从**次要**站点上的每个节点卸载极狐GitLab 后，必须从**主要**站点的数据库中删除复制槽，如下所示：

1. 在**主要**站点的数据库节点上，启动 PostgreSQL 控制台会话：

   ```shell
   sudo gitlab-psql
   ```

   NOTE:
   使用 `gitlab-rails dbconsole` 不起作用，因为管理复制槽需要超级用户权限。

1. 查找相关复制槽的名称。这是运行复制命令时使用 `--slot-name` 指定的槽：`gitlab-ctl replicate-geo-database`。

   ```sql
   SELECT * FROM pg_replication_slots;
   ```

1. 移除**次要**站点的复制槽：

   ```sql
   SELECT pg_drop_replication_slot('<name_of_slot>');
   ```
