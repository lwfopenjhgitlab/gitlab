---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 升级 Geo 站点 **(PREMIUM SELF)**

WARNING:
在更新您的 Geo 站点之前，请仔细阅读这些部分。不遵循特定于版本的升级步骤可能会导致意外停机。如果您有任何具体问题，[联系技术支持](https://gitlab.cn/support/#contact-support)。

<!--
Upgrading Geo sites involves performing:

1. [Version-specific upgrade steps](version_specific_upgrades.md), depending on the
   version being upgraded to or from.
1. [General upgrade steps](#general-upgrade-steps), for all upgrades.
-->

## 一般升级步骤

NOTE:
这些常规升级步骤不适用于多站点部署，并会导致停机。<!--如果您想避免停机，请考虑使用[零停机升级](../../../update/zero_downtime.md#multi-node--ha-deployment-with-geo)-->。

要在新的极狐GitLab 版本发布时升级 Geo 站点，请升级**主要**站点和所有**次要**站点：

1. 可选。[暂停每个**次要**站点上的复制](../index.md#pausing-and-resuming-replication)，保护**次要**站点的灾难恢复 (DR) 功能。
1. SSH 进入**主要**站点的每个节点。
1. 在**主要**站点上升级极狐GitLab。<!--[Upgrade GitLab on the **primary** site](../../../update/package/index.md#upgrade-using-the-official-repositories).-->
1. 在**主要**站点上执行测试，尤其是当您在步骤 1 中暂停复制以保护 DR 时。<!--[There are some suggestions for post-upgrade testing](../../../update/plan_your_upgrade.md#pre-upgrade-and-post-upgrade-checks) in the upgrade documentation.-->
1. SSH 进入**次要**站点的每个节点。
1. 在每个**次要**站点上升级极狐GitLab。<!--[Upgrade GitLab on each **secondary** site](../../../update/package/index.md#upgrade-using-the-official-repositories).-->
1. 如果您在步骤 1 中暂停了复制，[在每个**次要**站点上恢复复制](../index.md#pausing-and-resuming-replication)。
   然后，在每个**次要**站点上重新启动 Puma 和 Sidekiq。这是为了确保它们针对现在从先前升级的**主要**站点复制的较新数据库模式进行初始化。

   ```shell
   sudo gitlab-ctl restart sidekiq
   sudo gitlab-ctl restart puma
   ```

1. [测试](#check-status-after-upgrading)**主要**和**次要**站点，并检查每个站点的版本。

<a id="check-status-after-upgrading"></a>

### 查看升级后的状态

现在升级过程已完成，您可能需要检查一切是否正常：

1. 在主要和次要站点的应用节点上运行 Geo Rake 任务。状态应该是绿色的：

   ```shell
   sudo gitlab-rake gitlab:geo:check
   ```

1. 检查**主要**站点的 Geo 仪表盘是否有任何错误。
1. 将代码推送到**主要**站点，测试数据复制，查看**次要**站点是否收到。

<!--
If you encounter any issues, see the [Geo troubleshooting guide](troubleshooting.md).
-->
