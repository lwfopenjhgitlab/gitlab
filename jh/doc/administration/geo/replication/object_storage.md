---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# Geo 对象存储 **(PREMIUM SELF)**

Geo 可以与对象存储（AWS S3 或其他兼容的对象存储）结合使用。

目前，**次要**站点可以使用：

- 与**主要**站点相同的存储桶。
- 一个复制的存储桶。
- 本地存储，如果主要使用本地存储。

文件的存储方式（本地或对象存储）记录在数据库中，数据库从**主要** Geo 站点复制到**次要** Geo 站点。

在访问上传的对象时，我们从数据库中获取其存储方式（本地或对象存储），因此**次要** Geo 站点必须与**主要** Geo 站点的存储方式相匹配。

因此，如果**主要** Geo 站点使用对象存储，则**次要** Geo 站点也必须使用它。

- 极狐GitLab 管理复制，遵循[启用极狐GitLab 复制](#enabling-gitlab-managed-object-storage-replication)文档。
- 第三方服务管理复制，遵循第三方复制服务。

<!--
有关 GitLab 管理的复制和第三方复制之间的比较，请参阅 [对象存储复制测试](geo_validation_tests.md#object-storage-replication-tests)。
-->

[阅读有关在极狐GitLab 中使用对象存储的更多信息](../../object_storage.md)。

<a id="enabling-gitlab-managed-object-storage-replication"></a>

## 启用极狐GitLab 管理的对象存储复制

> 一般可用于 15.1 版本。

**次要**站点可以复制存储在**主要**站点上的文件，无论它们是存储在本地文件系统还是对象存储中。

要启用极狐GitLab 复制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **Geo > 节点**。
1. 在**次要**站点上，选择 **编辑**。
1. 在 **同步设置** 部分，找到 **允许此次要节点复制对象存储上的内容** 复选框以启用它。

对于 LFS，请按照文档[设置 LFS 对象存储](../../lfs/index.md#storing-lfs-objects-in-remote-object-storage)。

对于 CI 作业产物，有类似的文档来配置[作业产物对象存储](../../job_artifacts.md#using-object-storage)。

对于用户上传文件，有类似的文档来配置[上传文件对象存储](../../uploads.md#using-object-storage)。

如果要将**主要**站点的文件迁移到对象存储，可以通过以下几种方式配置**次要**站点：

- 使用完全相同的对象存储。
- 使用单独的对象存储，但利用对象存储解决方案的内置复制。
- 使用单独的对象存储并启用**允许此次要节点复制对象存储上的内容**设置。

极狐GitLab 目前不支持以下两种情况：

- **主要**站点使用本地存储。
- **次要**站点使用对象存储。

<!--
<a id="third-party-replication-services"></a>

## Third-party replication services

When using Amazon S3, you can use
[Cross-Region Replication (CRR)](https://docs.aws.amazon.com/AmazonS3/latest/dev/crr.html) to
have automatic replication between the bucket used by the **primary** site and
the bucket used by **secondary** sites.

If you are using Google Cloud Storage, consider using
[Multi-Regional Storage](https://cloud.google.com/storage/docs/storage-classes#multi-regional).
Or you can use the [Storage Transfer Service](https://cloud.google.com/storage-transfer/docs/overview),
although this only supports daily synchronization.

For manual synchronization, or scheduled by `cron`, see:

- [`s3cmd sync`](https://s3tools.org/s3cmd-sync)
- [`gsutil rsync`](https://cloud.google.com/storage/docs/gsutil/commands/rsync)

## Verification of files in object storage

[Files stored in object storage are not verified.](https://gitlab.com/groups/gitlab-org/-/epics/8056)
-->