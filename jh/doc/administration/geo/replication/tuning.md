---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 调试 Geo **(PREMIUM SELF)**

您可以限制站点可以在后台运行的并发操作数。

## 更改同步/验证并发值

在**主要**站点上：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏中，选择 **Geo > 站点**。
1. 选择要调整的次要站点的 **编辑**。
1. 在 **调整设置** 下，有几个变量可以调整以提高 Geo 的性能：

   - 仓库同步并发限制
   - 文件同步并发限制
   - 容器仓库同步并发限制
   - 验证并发限制

增加并发值会增加计划的作业数。
但是，除非可用 Sidekiq 线程的数量也增加，否则这可能不会导致更多的并行下载。例如，仓库同步并发从 25 增加到 50，您可能还希望将 Sidekiq 线程数从 25 增加到 50。请参阅 [Sidekiq 并发文档](../../sidekiq/extra_sidekiq_processes.md#concurrency)了解更多详情。

## 仓库重新验证

请参阅[自动后台验证](../disaster_recovery/background_verification.md)。
