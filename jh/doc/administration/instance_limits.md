---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 应用限制 **(FREE SELF)**

与大多数大型应用程序一样，极狐GitLab 会对某些功能实施限制，保持最低的性能质量。允许某些功能不受限制可能会影响安全性、性能、数据，甚至可能耗尽为应用程序分配的资源。

<a id="rate-limits"></a>

## 速率限制

速率限制可用于提高极狐GitLab 的安全性和持久性。

阅读更多关于[如何配置速率限制](../security/rate_limits.md)。

### 议题创建

此设置限制到议题创建端点的请求速率。

阅读更多关于[议题创建的速率限制](settings/rate_limit_on_issues_creation.md)。

- **默认速率限制**：默认禁用。

### 按用户或 IP 限制

此设置限制每个用户或 IP 的请求速率。

阅读更多关于[用户和 IP 的速率限制](settings/user_and_ip_rate_limits.md)。

- **默认速率限制**：默认禁用。

### 按原始端点限制

此设置限制每个端点的请求速率。

阅读更多关于[原始端点的速率限制](settings/rate_limits_on_raw_endpoints.md)。

- **默认速率限制**：每个项目、每个提交和每个文件路径 300 个请求。

### 按受保护路径限制

此设置限制特定路径上的请求速率。

极狐GitLab 速率默认限制以下路径：

```plaintext
'/users/password',
'/users/sign_in',
'/api/#{API::API.version}/session.json',
'/api/#{API::API.version}/session',
'/users',
'/users/confirmation',
'/unsubscribes/',
'/import/github/personal_access_token',
'/admin/session'
```

阅读更多关于[受保护路径速率限制](settings/protected_paths.md)。

- **默认速率限制**：10 次请求后，客户端必须等待 60 秒才能重试。

### 软件包库

此设置限制每个用户或 IP 对 Packages API 的请求速率率。获取更多信息，查看 [Package Registry 速率限制](settings/package_registry_rate_limits.md)。

- **默认速率限制**：默认禁用。

### Git LFS

> 引入于 14.3 版本。

此设置限制每个用户的 [Git LFS](../topics/git/lfs/index.md) 请求的请求率。<!--获取更多信息，阅读 [GitLab Git Large File Storage (LFS) Administration](../administration/lfs/index.md).-->

- **默认速率限制**：默认禁用。

### Files API

> - 引入于 14.3 版本，功能标志名为 `files_api_throttling`。默认禁用。
> - 于 14.6 版本通用。功能标志 `files_api_throttling` 移除。

此设置限制每个用户或 IP 地址对 Packages API 的请求率。获取更多信息，阅读 [Files API 速率限制](settings/files_api_rate_limits.md)。

- **默认速率限制**：默认禁用。

### 废弃 API 端点

> 引入于 14.4 版本

此设置限制每个用户或 IP 地址对已弃用 API 端点的请求速率。获取更多信息，阅读 [Deprecated API 速率限制](settings/deprecated_api_rate_limits.md)。

- **默认速率限制**：默认禁用。

### 导入/导出

此设置限制群组和项目的导入/导出操作。

| 限制                   | 默认值（每分钟每个用户）(per minute per user) |
|-------------------------|-------------------------------|
| 项目导入          | 6 |
| 项目导出          | 6 |
| 项目导出下载 | 1 |
| 群组导入            | 6 |
| 群组导出            | 6 |
| 群组导出下载   | 1 |

阅读更多关于[导入/导出速率限制](settings/import_export_rate_limits.md)。

### 成员邀请

限制每个群组层级允许的最大每日成员邀请。

- SaaS：免费成员每天可以邀请 20 名成员。专业版试用和旗舰版试用成员每天可邀请 50 名成员。
- 私有化部署实例：邀请不受限制。

### Webhook 速率限制

> - 引入于 13.12 版本。
> - 功能标志移除于 14.1 版本。
> - 在 15.1 版本中，限制从每个 hook 更改为每个顶级命名空间。

限制每个顶级命名空间每分钟可以调用 Webhook 的次数。
仅适用于项目和群组 webhook。

超过速率限制的调用会被记录到 `auth.log` 中。

为私有化部署实例设置此限制，在 [Rails console](operations/rails_console.md#启动-rails-控制台会话)：

```ruby
# If limits don't exist for the default plan, you can create one with:
# Plan.default.create_limits!

Plan.default.actual_limits.update!(web_hook_calls: 10)
```

将限制设置为 `0` 可以禁用它。

- **默认速率限制**：禁用（无限制）。

### 搜索速率限制

> - 引入于 14.9 版本
> - 将对议题、合并请求和史诗的搜索包括到速率限制，引入于 15.9 版本。

此设置限制搜索请求：

| 限制                  | 默认值（每分钟请求数） |
|-------------------------|-------------------------------|
| 经过身份验证的用户      | 30 |
| 未经身份验证的用户    | 10 |

根据启用的[范围](../user/search/index.md#global-search-scopes)的数量，全局搜索请求每分钟可以消耗 2 到 7 个请求。您可能希望禁用一个或多个范围以使用更少的请求。超过每分钟搜索速率限制的搜索请求返回以下错误：

```plaintext
This endpoint has been requested too many times. Try again later.
```

### 流水线创建速率限制

> 引入于 15.0 版本。

此设置将请求速率限制为流水线创建端点。

阅读有关[流水线创建速率限制](settings/rate_limit_on_pipelines_creation.md)的更多信息。

## Gitaly 并发限制

克隆流量会给您的 Gitaly 服务带来很大压力。为防止此类工作负载使您的 Gitaly 服务器不堪重负，您可以在 Gitaly 的配置文件中设置并发限制。

<!--
Read more about [Gitaly concurrency limits](gitaly/configure_gitaly.md#limit-rpc-concurrency).
-->

- **默认速率限制**：禁用。

## 每个议题、合并请求或提交的评论数

针对议题、合并请求或提交的评论数量是有限制的。当达到限制时，仍然可以添加系统注释，历史事件不会丢失，但用户提交的评论失败。

- **最大限制**：5000 条评论。

## 议题、合并请求和史诗的评论和描述的大小

对议题、合并请求和史诗的评论和描述的大小是有限制的。
尝试添加大于限制的文本正文会导致错误，并且项目也不会创建。

将来此限制可能会更改为较低的数字。

- **最大大小**: ~100 万字符 / ~1 MB。

## 提交标题和描述的大小

带有任意大小的消息的提交可能会被推送到极狐GitLab，但系统具有以下显示限制：

- **标题** - 提交信息的第一行。限制为 1 KiB。
- **描述** - 提交信息的其余部分。限制为 1 MiB。

推送提交时，极狐GitLab 会处理标题和描述，将对议题 (#123) 和合并请求 (!123) 的引用替换为议题和合并请求的链接。

当推送大量提交的分支时，只处理最后 100 个提交。

## 里程碑概述中的议题数量

里程碑概览页面上加载的最大议题数为 500。
当数量超过限制时，页面会显示警报并链接到里程碑中所有议题的分页[议题列表](../user/project/issues/managing_issues.md)。

- **限制**：500 个议题。

## 每次 Git push 的流水线数

当使用单个 Git push 推送多个更改时，例如多个标签或分支，只能触发四个标签或分支流水线。此限制可防止在使用 `git push --all` 或 `git push --mirror` 时意外创建大量流水线。

[合并请求流水线](../ci/pipelines/merge_request_pipelines.md)不受限制。
如果 Git 推送同时更新多个合并请求，则可以为每个更新的合并请求触发一个合并请求流水线。

要删除限制，使得任何数量的流水线可以触发单个 Git push 事件，管理员可以启用 `git_push_create_all_pipelines` [功能标志](feature_flags.md)。
不建议启用此功能标志，因为如果一次推送太多更改并意外创建大量流水线，可能会导致极狐GitLab 实例负载过大。

<!--
## Retention of activity history

Activity history for projects and individuals' profiles was limited to one year until [GitLab 11.4](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/52246) when it was extended to two years, and in [GitLab 12.4](https://gitlab.com/gitlab-org/gitlab/-/issues/33840) to three years.
-->

## 嵌入式指标的数量

出于性能原因，在 GitLab Flavored Markdown (GLFM) 中嵌入指标时存在限制。

- **最大限制**：100 个嵌入。

## Webhook 限制

查看 [Webhook 速率限制](#webhook-速率限制)。

### webhooks 数量

要设置私有化部署安装实例的群组或项目 webhook 的最大数量，请在[极狐GitLab Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中运行以下命令：

```ruby
# If limits don't exist for the default plan, you can create one with:
# Plan.default.create_limits!

# For project webhooks
Plan.default.actual_limits.update!(project_hooks: 200)

# For group webhooks
Plan.default.actual_limits.update!(group_hooks: 100)
```

将限制设置为 `0`，可以禁用它。

默认的最大 webhook 数量是每个项目 100 个，每个群组 50 个。

<!--
For GitLab.com, see the [webhook limits for GitLab.com](../user/gitlab_com/index.md#webhooks).
-->

### Webhook 负载大小

最大 Webhook 有效负载大小为 25 MB。

### Webhook 超时

极狐GitLab 在发送 webhook 后等待 HTTP 响应的秒数。

要更改 webhook 超时值：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['webhook_timeout'] = 60
   ```

1. 保存文件。
1. 重新配置并重启极狐GitLab 使更改生效：

   ```shell
   gitlab-ctl reconfigure
   gitlab-ctl restart
   ```

<!--
See also [webhook limits for GitLab.com](../user/gitlab_com/index.md#other-limits).
-->

### 递归 webhooks

> 引入于 14.8 版本。

GitLab 检测并阻止递归或超出可从其它 webhook 触发的并超出限制的 webhook。极狐GitLab 能够继续支持使用 webhook 以非递归方式调用 API 或不触发不合理数量的其它 webhook 的工作流。

当 webhook 配置为调用其自己的 GitLab 实例（例如 API）时，可能会发生递归。然后调用会触发相同的 webhook 并无限循环。

触发其它 webhook 的一系列 webhook 向实例发出的最大请求数为 100。当达到限制时，系统会阻止该系列触发任何更多的 webhook。

被阻止的递归 webhook 调用记录在 `auth.log` 中，并带有消息 `"Recursive webhook blocked from executing"`。

## 拉取镜像间隔

[拉刷新之间的最短等待时间](../user/project/repository/mirror/index.md) 默认为 300 秒（5 分钟）。例如，无论您触发多少次，在给定的 300 秒周期内，拉取刷新只运行一次。

此设置适用于通过 projects API<!--[projects API](../api/projects.md#start-the-pull-mirroring-process-for-a-project)--> 调用的拉取刷新的上下文，或通过选择 **设置 > 仓库 > 镜像仓库** 中的 **立即更新** (**{retry}**) 按钮进行强制更新时。此设置对 Sidekiq 用于 [pull mirroring](../user/project/repository/mirror/pull.md) 的自动 30 分钟间隔计划没有影响。 

要更改自助管理安装实例的此限制，请在[极狐GitLab Rails 控制台](operations/rails_console.md#启动-rails-控制台会话)中运行以下命令：

```ruby
# If limits don't exist for the default plan, you can create one with:
# Plan.default.create_limits!

Plan.default.actual_limits.update!(pull_mirror_interval_seconds: 200)
```

## 来自自动回复的传入电子邮件

极狐GitLab 通过查找 `X-Autoreply` header 来忽略所有从自动回复器发送的传入电子邮件。此类电子邮件不会对议题或合并请求创建评论。

## 通过错误跟踪从 Sentry 发送的数据量

> 限制所有 Sentry 响应功能引入于 15.6 版本。

出于安全原因和限制内存消耗，发送到极狐GitLab 的 Sentry 有效负载的最大限制为 1 MB。

<a id="max-offset-allowed-by-the-rest-api-for-offset-based-pagination"></a>

## REST API 允许的 offset-based 分页的最大 offset

在 REST API 中使用 offset-based 分页时，对结果集中请求的最大 offset 存在限制。此限制仅适用于支持 keyset-based 分页的端点。更多关于分页选项的信息可以在 [API 文档](../api/rest/index.md#pagination)中找到。

要为私有化部署安装实例设置此限制，请在 [Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中运行以下命令：

```ruby
# If limits don't exist for the default plan, you can create one with:
# Plan.default.create_limits!

Plan.default.actual_limits.update!(offset_pagination_limit: 10000)
```

- **默认 offset 分页限制**：`50000`。

将限制设置为 `0` 可以禁用。

<a id="cicd-limits"></a>

## CI/CD 限制

<a id="number-of-jobs-in-active-pipelines"></a>

### 活跃流水线中的作业数

每个项目可以限制活跃流水线中的作业总数。每次创建新流水线时都会检查此限制。活跃流水线是处于以下状态之一的任何流水线：

- `created`
- `pending`
- `running`

如果新流水线会导致作业总数超过限制，则流水线将失败并出现 `job_activity_limit_exceeded` 错误。

- SaaS 订阅者有不同的限制，由订阅方案级别定义，会影响该方案下的所有项目。
- 在[专业版](https://about.gitlab.cn/pricing/)或更高级别的私有化部署实例上，此限制是在影响所有项目的 `default` 方案下定义的。默认情况下禁用此限制 (`0`)。

要为私有化部署实例设置此限制，请在 [Rails 控制台](operations/rails_console.md#启动-rails-控制台会话)中运行以下命令：

```ruby
# If limits don't exist for the default plan, you can create one with:
# Plan.default.create_limits!

Plan.default.actual_limits.update!(ci_active_jobs: 500)
```

将限制设置为 `0`，可以禁用它。

### 作业可以运行的最长时间

作业可以运行的默认最长时间为 60 分钟。运行超过 60 分钟的作业超时。

您可以更改作业在超时之前可以运行的最长时间：

- 在项目级别，在特定项目的 [CI/CD 设置](../ci/pipelines/settings.md#set-a-limit-for-how-long-jobs-can-run)中。此限制必须在 10 分钟到 1 个月之间。
- 在 [runner 级别](../ci/runners/configure_runners.md#set-maximum-job-timeout-for-a-runner)。此限制必须为 10 分钟或更长。

### 流水线中的最大部署作业数

您可以限制流水线中的最大部署作业数。部署是指定了 [`environment`](../ci/environments/index.md) 的任何作业。在创建流水线时，检查流水线中的部署数量。部署过多的流水线会失败，并出现 `deployments_limit_exceeded` 错误。

所有[自助管理和 SaaS 方案](https://about.gitlab.cn/pricing/) 的默认限制为 500。

要更改私有化部署实例的限制，请使用以下 [Rails 控制台](operations/rails_console.md#starting-a-rails-console-session) 命令更改 `default` 方案的限制：

```ruby
# If limits don't exist for the default plan, you can create one with:
# Plan.default.create_limits!

Plan.default.actual_limits.update!(ci_pipeline_deployments: 500)
```

将限制设置为 `0`，可以禁用它。

<a id="number-of-cicd-subscriptions-to-a-project"></a>

### 一个项目的 CI/CD 订阅数

每个项目的订阅总数可以受到限制。每次创建新订阅时都会检查此限制。

如果新订阅导致订阅总数超过限制，则认为订阅无效。

- SaaS 订阅者有不同的限制，由订阅方案级别定义，会影响该方案下的所有项目。
- 在[专业版](https://about.gitlab.cn/pricing/)或更高级别自助管理实例上，此限制是在影响所有项目的 `default` 方案下定义的。 默认情况下，有 `2` 个订阅的限制。

要更改自助管理安装实例的限制，请使用以下[极狐GitLab Rails 控制台](operations/rails_console.md#启动-rails-控制台会话)命令：

```ruby
Plan.default.actual_limits.update!(ci_project_subscriptions: 500)
```

将限制设置为 `0`，可以禁用它。

<a id="limit-the-number-of-pipeline-triggers"></a>

### 限制流水线触发器的数量

> 引入于 14.6 版本。

您可以设置每个项目的最大流水线触发器数限制。每次创建新触发器时都会检查此限制。

如果一个新的触发器会导致流水线触发器的总数超过限制，则该触发器被认为是无效的。

将限制设置为 `0`，可以禁用它。在自助管理实例上默认为 150。

要在私有化部署实例中将此限制设置为 `100`，请在 [Rails 控制台](operations/rails_console.md#启动-rails-控制台会话)中运行以下命令：

```ruby
Plan.default.actual_limits.update!(pipeline_triggers: 100)
```

<!--
This limit is [enabled on GitLab.com](../user/gitlab_com/index.md#gitlab-cicd).
-->

<a id="number-of-pipeline-schedules"></a>

### 流水线计划数量

每个项目可以限制流水线计划的总数。每次创建新的流水线计划时都会检查此限制。如果新的流水线计划会导致流水线计划的总数超过限制，则不会创建。

SaaS 订阅者有不同的限制，由订阅方案级别定义，会影响该方案下的所有项目。
在[专业版](https://about.gitlab.cn/pricing/)或更高级别自助管理实例上，此限制是在影响所有项目的 `default` 方案下定义的。默认情况下，有 `10` 个流水线计划的限制。

要更改自助管理安装实例的限制，请使用以下[极狐GitLab Rails 控制台](operations/rails_console.md#启动-rails-控制台会话)命令：

```ruby
Plan.default.actual_limits.update!(ci_pipeline_schedules: 100)
```

<a id="limit-the-number-of-pipelines-created-by-a-pipeline-schedule-per-day"></a>

### 限制每天由流水线计划创建的流水线数量

> 引入于 14.0 版本。

您可以限制流水线计划每天可以触发的流水线数量。

尝试以超过限制的频率运行流水线的计划，将减慢到最大频率。
频率的计算方法是用 1440（一天中的分钟数）除以限制值。例如，对于最大频率：

- 每分钟一次，限制必须是 `1440`。
- 每 10 分钟一次，限制必须是 `144`。
- 每 60 分钟一次，限制必须是 `24`

最小值为 `24`，或每 60 分钟一次流水线。没有最大值。

要在私有化部署实例中将此限制设置为 `1440`，请在 [Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中运行以下命令：

```ruby
Plan.default.actual_limits.update!(ci_daily_pipeline_schedule_triggers: 1440)
```

<!--
This limit is [enabled on GitLab.com](../user/gitlab_com/index.md#gitlab-cicd).
-->

<a id="limit-the-number-of-schedule-rules-defined-for-security-policy-project"></a>

### 限制为安全策略项目定义的计划规则的数量

> 引入于 15.1 版本。

您可以限制每个安全策略项目的计划规则总数。每次更新带有计划规则的策略时都会检查此限制。如果新的计划规则会导致总数超过限制，则不会处理新的计划规则。

默认情况下，私有化部署实例不限制可处理的计划规则的数量。

要为私有化部署实例设置此限制，请在 [Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中运行以下命令：

```ruby
Plan.default.actual_limits.update!(security_policy_scan_execution_schedules: 100)
```

此限制在 [JihuLab.com 上启用](../user/jihulab_com/index.md#gitlab-cicd)。

### 实例级变量的数量

实例级别 CI/CD 变量的总数在实例级别受到限制。
每次创建新的实例级变量时都会检查此限制。如果新变量会导致变量总数超过限制，则不会创建新变量。

在私有化部署实例上，此限制是作为 `default` 计划定义的。默认情况下，此限制设置为 `25`。

要在私有化部署实例上，将此限制更新为新值，请在 [Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中运行以下命令：

```ruby
Plan.default.actual_limits.update!(ci_instance_level_variables: 30)
```

## 群组级变量的数量

> 引入于 15.7 版本。

群组级别 CI/CD 变量的总数在实例级别受到限制。
每次创建新的群组级别变量时都会检查此限制。如果新变量会导致变量总数超过限制，则不会创建新变量。

在私有化部署实例上，此限制是作为 `default` 计划定义的。默认情况下，此限制设置为 `30000`。

要在私有化部署实例上，将此限制更新为新值，请在 [Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中运行以下命令：

```ruby
Plan.default.actual_limits.update!(group_ci_variables: 40000)
```

### 项目级变量的数量

> 引入于 15.7 版本。

项目级别 CI/CD 变量的总数在实例级别受到限制。
每次创建新的项目级别变量时都会检查此限制。如果新变量会导致变量总数超过限制，则不会创建新变量。

在私有化部署实例上，此限制是作为 `default` 计划定义的。默认情况下，此限制设置为 `8000`。

要私有化部署实例上，将此限制更新为新值，请在 [Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中运行以下命令：

```ruby
Plan.default.actual_limits.update!(project_ci_variables: 10000)
```

<a id="maximum-file-size-per-type-of-artifact"></a>

### 每种产物类型的最大文件大小

如果文件大小超过最大文件大小限制，则 runner 上传的使用 [`artifacts:reports`](../ci/yaml/index.md#artifactsreports) 定义的作业产物将被拒绝。该限制是通过将项目的[最大产物大小设置](../administration/settings/continuous_integration.md#maximum-artifacts-size)与给定产物类型的实例限制进行比较来确定的，并选择较小的值。

限制以兆字节为单位设置，因此可以定义的最小可能值是 `1 MB`。

每种类型的产物都有一个可以设置的大小限制。默认值为 0 表示该特定产物类型没有限制，并且使用项目的最大产物大小设置：

| 产物限制名称                         | 默认值 |
|---------------------------------------------|---------------|
| `ci_max_artifact_size_accessibility`              | 0             |
| `ci_max_artifact_size_api_fuzzing`                | 0             |
| `ci_max_artifact_size_archive`                    | 0             |
| `ci_max_artifact_size_browser_performance`        | 0             |
| `ci_max_artifact_size_cluster_applications`       | 0             |
| `ci_max_artifact_size_cobertura`                  | 0             |
| `ci_max_artifact_size_codequality`                | 0             |
| `ci_max_artifact_size_container_scanning`         | 0             |
| `ci_max_artifact_size_coverage_fuzzing`           | 0             |
| `ci_max_artifact_size_dast`                       | 0             |
| `ci_max_artifact_size_dependency_scanning`        | 0             |
| `ci_max_artifact_size_dotenv`                     | 0             |
| `ci_max_artifact_size_junit`                      | 0             |
| `ci_max_artifact_size_license_management`         | 0             |
| `ci_max_artifact_size_license_scanning`           | 0             |
| `ci_max_artifact_size_load_performance`           | 0             |
| `ci_max_artifact_size_lsif`                       | 100 MB        |
| `ci_max_artifact_size_metadata`                   | 0             |
| `ci_max_artifact_size_metrics_referee`            | 0             |
| `ci_max_artifact_size_metrics`                    | 0             |
| `ci_max_artifact_size_network_referee`            | 0             |
| `ci_max_artifact_size_performance`                | 0             |
| `ci_max_artifact_size_requirements`               | 0             |
| `ci_max_artifact_size_requirements_v2`            | 0             |
| `ci_max_artifact_size_sast`                       | 0             |
| `ci_max_artifact_size_secret_detection`           | 0             |
| `ci_max_artifact_size_terraform`                  | 5 MB          |
| `ci_max_artifact_size_trace`                      | 0             |
| `ci_max_artifact_size_cyclonedx`                  | 1 MB          |

例如，要在私有化部署实例中将 `ci_max_artifact_size_junit` 限制设置为 10 MB，请在 [Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中运行以下命令：

```ruby
Plan.default.actual_limits.update!(ci_max_artifact_size_junit: 10)
```

### 每个 GitLab Pages 网站的文件数

每个 GitLab Pages 网站的文件条目总数（包括目录和软链接）限制为 `200000`。

<!--
This is the default limit for all [GitLab self-managed and SaaS plans](https://about.gitlab.com/pricing/).
-->

您可以使用[极狐GitLab Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)更新私有化部署实例中的限制。
例如，要将限制更改为 `100`：

```ruby
Plan.default.actual_limits.update!(pages_file_entries: 100)
```

### 每个 Pages 网站的自定义域名数

<!--
The total number of custom domains per GitLab Pages website is limited to `150` for [GitLab SaaS](../subscriptions/gitlab_com/index.md).
-->

私有化部署实例的默认限制是 `0`（无限制）。
要对您的私有化部署实例设置限制，请在[管理中心](pages/index.md#set-maximum-number-of-gitlab-pages-custom-domains-for-a-project)设置。

<a id="number-of-registered-runners-per-scope"></a>

### 每个范围的注册 runner 数量

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/321368) in GitLab 13.12.默认禁用。
> - Enabled on GitLab.com in GitLab 14.3.
> - Enabled on self-managed in GitLab 14.4.
> - Feature flag `ci_runner_limits` removed in GitLab 14.4.
> - Feature flag `ci_runner_limits_override` removed in GitLab 14.6.
-->

注册 runner 的总数在群组和项目级别受到限制。每次注册新 runner 时，系统都会根据过去 3 个月内活跃的 runner 检查这些限制。
如果 runner 注册超过 runner 注册令牌确定的范围限制，则 runner 注册失败。
如果限制值设置为零，则禁用限制。

对于 SaaS 订阅者，每个级别定义了不同的限制，影响使用该级别的所有项目。

私有化部署实例的专业版和旗舰版限制由影响所有项目的默认计划定义：

| Runner 范围                               | 默认值 |
|---------------------------------------------|---------------|
| `ci_registered_group_runners`               | 1000          |
| `ci_registered_project_runners`             | 1000          |

要更新这些限制，请在[极狐GitLab Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中运行以下命令：

```ruby
# Use ci_registered_group_runners or ci_registered_project_runners
# depending on desired scope
Plan.default.actual_limits.update!(ci_registered_project_runners: 100)
```

### 作业日志的最大文件大小

> - 引入于 14.1 版本，默认禁用。
> - 默认启用于 14.2 版本，功能标志 `ci_jobs_trace_size_limit` 移除。

极狐GitLab 中的作业日志文件大小限制默认为 100 兆字节。任何超过限制的作业都被标记为失败，并被 runner 丢弃。

您可以在[极狐GitLab Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中更改限制。
用新值（以兆字节为单位）更新 `ci_jobs_trace_size_limit`：

```ruby
Plan.default.actual_limits.update!(ci_jobs_trace_size_limit: 125)
```

极狐GitLab Runner 还有一个 `output_limit` 设置<!--[`output_limit` 设置](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runners-section)-->，用于配置 runner 中的最大日志大小。超过 runner 限制的作业会继续运行，但日志在达到限制时会被截断。

### 每个项目的最大活跃 DAST 配置文件计划数

> 引入于 14.3 版本。

限制每个项目的活跃 DAST 配置文件计划的数量。DAST 配置文件计划可以是活跃的或非活跃的。

您可以在[极狐GitLab Rails 控制台](operations/rails_console.md#启动-rails-控制台会话)中更改限制。
使用新值更新 `dast_profile_schedules`：

```ruby
Plan.default.actual_limits.update!(dast_profile_schedules: 50)
```

### CI/CD 配置 YAML 文件的最大大小和深度

单个 CI/CD 配置 YAML 文件的默认最大大小为 1 兆字节，默认深度为 100。

您可以在[极狐GitLab Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中更改这些限制：

- 要更新最大 YAML 大小，请使用以 MB 为单位的新值更新 `max_yaml_size_bytes`：

  ```ruby
  ApplicationSetting.update(max_yaml_size_bytes: 2.megabytes)
  ```

  `max_yaml_size_bytes` 值不直接与 YAML 文件的大小相关，而是与为相关对象分配的内存相关联。

- 要更新最大 YAML 深度，请使用以 MB 为单位的新值更新 `max_yaml_depth`：

  ```ruby
  ApplicationSetting.update(max_yaml_depth: 125)
  ```

要完全禁用此限制，请禁用控制台中的功能标志：

```ruby
Feature.disable(:ci_yaml_limit_size)
```

<a id="limit-dotenv-variables"></a>

### 限制 dotenv 变量

> 引入于 14.5 版本。

您可以对 dotenv 产物内的最大变量数设置限制。
每次将 dotenv 文件导出为产物时，都会检查此限制。

将限制设置为 `0`，可以禁用它。在私有化部署版实例上默认为 `20`。

要在私有化部署实例上将此限制设置为 `100`，请在[极狐GitLab Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中运行以下命令：

```ruby
Plan.default.actual_limits.update!(dotenv_variables: 100)
```

<!--
This limit is [enabled on GitLab.com](../user/gitlab_com/index.md#gitlab-cicd).
-->

<a id="limit-dotenv-file-size"></a>

### 限制 dotenv 文件大小

> 引入于 14.5 版本。

您可以设置 dotenv 产物的最大大小限制。每次将 dotenv 文件导出为产物时，都会检查此限制。

将限制设置为 `0`，可以禁用它。默认为 5KB。

要在私有化部署安装实例中将此限制设置为 5KB，请在[极狐GitLab Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中运行以下命令：

```ruby
Plan.default.actual_limits.update!(dotenv_size: 5.kilobytes)
```

## 实例监控和指标

### 限制入站事件管理警报

此设置限制一段时间内入站警报有效负载的数量。

阅读有关[事件管理速率限制](../user/admin_area/settings/rate_limit_on_pipelines_creation.md)的更多信息。

### Prometheus 警报 JSON 有效负载

发送到 `notify.json` 端点的 Prometheus 警报有效负载的大小限制为 1 MB。

<a id="generic-alert-json-payloads"></a>

### 通用警报 JSON 有效负载

发送到 `notify.json` 端点的警报有效负载的大小限制为 1 MB。

### 指标仪表盘 YAML 文件

解析后的指标仪表盘 YAML 文件占用的内存不能超过 1 MB。

每个 YAML 文件的最大深度限制为 100。YAML 文件的最大深度是其嵌套最多的 key 的嵌套层数，路径上的每个散列和数组都计入其深度。例如，以下 YAML 中嵌套最多的键的深度为 7：

```yaml
dashboard: 'Test dashboard'
links:
- title: Link 1
  url: https://gitlab.com
panel_groups:
- group: Group A
  priority: 1
  panels:
  - title: "Super Chart A1"
    type: "area-chart"
    y_label: "y_label"
    weight: 1
    max_value: 1
    metrics:
    - id: metric_a1
      query_range: 'query'
      unit: unit
      label: Legend Label
```

<!--
## Environment Dashboard limits **(PREMIUM)**

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/33895) in GitLab 13.4.

See [Environment Dashboard](../ci/environments/environments_dashboard.md#adding-a-project-to-the-dashboard) for the maximum number of displayed projects.

## Environment data on deploy boards

[Deploy boards](../user/project/deploy_boards.md) load information from Kubernetes about
Pods and Deployments. However, data over 10 MB for a certain environment read from
Kubernetes aren't shown.
-->

## 合并请求

### 差异限制

极狐GitLab 有以下限制：

- 单个文件的补丁大小。[在私有化部署实例上可配置](../administration/diff_limits.md)。
- 合并请求的所有差异的总大小。

上限和下限适用于以下各项：

- 更改文件的数量。
- 更改的行数。
- 显示的更改的累积大小。

下限会使多余的差异被折叠。上限可防止渲染发生更多更改。<!--For more information about these limits,
[read the development documentation](../development/diffs.md#diff-limits).-->

### 合并请求报告大小限制

不会加载超过 20 MB 限制的报告。受影响的报告：

- 合并请求安全报告<!--[合并请求安全报告](../user/project/merge_requests/testing_and_reports_in_merge_requests.md#security-reports)-->
- [CI/CD 参数 `artifacts:expose_as`](../ci/yaml/index.md#artifactsexpose_as)
- 单元测试报告<!--[Unit test reports](../ci/unit_test_reports.md)-->

## 高级搜索限制

<a id="maximum-file-size-indexed"></a>

### 索引的最大文件大小

您可以对在 Elasticsearch 中索引的仓库文件的内容设置限制。任何大于此限制的文件仅索引文件名。
文件内容既非索引的，也不是可搜索的。

设置限制有助于减少索引进程的内存使用和整体索引大小。此值默认为 `1024 KiB`（1 MiB），因为任何大于此值的文本文件都可能不适合阅读。

您必须设置一个限制，因为不支持无限的文件大小。将此值设置为大于 GitLab Sidekiq 节点上的内存量会导致 GitLab Sidekiq 节点耗尽内存，因为此内存量是在索引期间预先分配的。

<a id="maximum-field-length"></a>

### 最大字段长度

您可以对为高级搜索编制索引的文本字段的内容设置限制。
设置最大值有助于减少索引进程的负载。如果任何文本字段超过此限制，则文本将被截断为此字符数。其余文本未编入索引，也无法搜索。
适用于所有索引数据，除了被索引的仓库文件，它们有一个单独的限制。获取更多信息，阅读[索引的最大文件大小](#索引的最大文件大小)。

- 在 SaaS 上，字段长度限制为 20,000 个字符。
- 对于自助管理实例，默认情况下，字段长度是无限的。

您可以在启用 Elasticsearch<!--[启用 Elasticsearch](../integration/elasticsearch.md#enable-advanced-search)--> 时为自助管理安装配置此限制。
将限制设置为 `0`，可以禁用它。

<!--
## Wiki limits

- [Wiki page content size limit](wikis/index.md#wiki-page-content-size-limit).
- [Length restrictions for file and directory names](../user/project/wiki/index.md#length-restrictions-for-file-and-directory-names).

## Snippets limits

See the [documentation about Snippets settings](snippets/index.md).

## Design Management limits

See the [Design Management Limitations](../user/project/issues/design_management.md#limitations) section.
-->

## 推送事件限制

<a id="max-push-size"></a>

### 最大推送大小

允许的最大[推送大小](../administration/settings/account_and_limit_settings.md#max-push-size)设置为 5 GB。

### Webhooks 和项目服务

单次推送中的更改总数（分支或标签）。如果更改超过指定限制，则不会执行 hooks。

<!--
More information can be found in these docs:

- [Webhooks push events](../user/project/integrations/webhook_events.md#push-events)
- [Project services push hooks limit](../user/project/integrations/overview.md#push-hooks-limit)
-->

### 动态

单次推送中的更改（分支或标签）总数，确定是创建单个推送事件还是批量推送事件。

<!--
More information can be found in the [Push event activities limit and bulk push events documentation](../user/admin_area/settings/push_event_activities_limit.md).
-->

## Package Registry 限制

<a id="file-size-limits"></a>

### 文件大小限制

上传到[极狐GitLab Package Registry](../user/packages/package_registry/index.md) 的软件包的默认最大文件大小因格式而异：

- Conan: 3 GB
- Generic: 5 GB
- Helm: 5 MB
- Maven: 3 GB
- npm: 500 MB
- NuGet: 500 MB
- PyPI: 3 GB
- Terraform: 1 GB

[JihuLab.com 的最大文件大小](../user/jihulab_com/index.md#package-registry-limits)可能会有所不同。

要为私有化部署安装实例设置这些限制，请在 [Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)中运行以下命令：

```ruby
# File size limit is stored in bytes

# For Conan Packages
Plan.default.actual_limits.update!(conan_max_file_size: 100.megabytes)

# For npm Packages
Plan.default.actual_limits.update!(npm_max_file_size: 100.megabytes)

# For NuGet Packages
Plan.default.actual_limits.update!(nuget_max_file_size: 100.megabytes)

# For Maven Packages
Plan.default.actual_limits.update!(maven_max_file_size: 100.megabytes)

# For PyPI Packages
Plan.default.actual_limits.update!(pypi_max_file_size: 100.megabytes)

# For Debian Packages
Plan.default.actual_limits.update!(debian_max_file_size: 100.megabytes)

# For Helm Charts
Plan.default.actual_limits.update!(helm_max_file_size: 100.megabytes)

# For Generic Packages
Plan.default.actual_limits.update!(generic_packages_max_file_size: 100.megabytes)
```

将限制设置为 `0`，可以允许任何文件大小。

### 返回的软件包版本

当询问给定 NuGet 包名称的版本时，GitLab Package Registry 最多返回 300 个版本。

## Dependency Proxy 限制

> 引入于 14.5 版本。

[Dependency Proxy](../user/packages/dependency_proxy/index.md) 中缓存的镜像的最大文件大小因文件类型而异：

- Image blob: 5 GB
- Image manifest: 10 MB

## 指派人和审核者的最大数量

> - 指派人的最大数量引入于 15.6 版本。
> - 审核者的最大数量引入于 15.9 版本。

问题和合并请求强制执行以下最大值：

- 指派人的最大数量：200
- 审核者的最大数量：200

<!--
## CDN-based limits on GitLab.com

In addition to application-based limits, GitLab.com is configured to use Cloudflare's standard DDoS protection and Spectrum to protect Git over SSH. Cloudflare terminates client TLS connections but is not application aware and cannot be used for limits tied to users or groups. Cloudflare page rules and rate limits are configured with Terraform. These configurations are [not public](https://about.gitlab.com/handbook/communication/#not-public) because they include security and abuse implementations that detect malicious activities and making them public would undermine those operations.
-->

## Container Repository 标签删除限制

容器仓库标签位于 Container Registry 中，因此，每次删除标签都会触发对 Container Registry 的网络请求。因此，我们将单个 API 调用可以删除的标签数量限制为 20 个。

## 项目级安全文件 API 限制

> 引入于 14.8 版本

安全文件 API 强制执行以下限制：

- 文件必须小于 5 MB。
- 项目的安全文件不能超过 100 个。

## Changelog API 限制

> - 引入于 15.1 版本。功能标志为 `changelog_commits_limitation`。默认禁用。
> - 在 SaaS 和私有化部署版上默认启用于 15.3 版本。

Changelog API<!--[Changelog API](../api/repositories.md#add-changelog-data-to-a-changelog-file)--> 强制执行以下限制：

- `from` 和 `to` 之间的提交范围不能超过 15000 次提交。

## 价值流分析限制

- 每个命名空间（例如群组或项目）最多可以有 50 个价值流。
- 每个价值流最多可以有 15 个阶段。

## 列出所有实例限制

要列出所有实例限制值，请从[极狐GitLab Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)运行以下命令：

```ruby
Plan.default.actual_limits
```

输出示例：

```ruby
id: 1,
plan_id: 1,
ci_pipeline_size: 0,
ci_active_jobs: 0,
project_hooks: 100,
group_hooks: 50,
ci_project_subscriptions: 3,
ci_pipeline_schedules: 10,
offset_pagination_limit: 50000,
ci_instance_level_variables: "[FILTERED]",
storage_size_limit: 0,
ci_max_artifact_size_lsif: 100,
ci_max_artifact_size_archive: 0,
ci_max_artifact_size_metadata: 0,
ci_max_artifact_size_trace: "[FILTERED]",
ci_max_artifact_size_junit: 0,
ci_max_artifact_size_sast: 0,
ci_max_artifact_size_dependency_scanning: 350,
ci_max_artifact_size_container_scanning: 150,
ci_max_artifact_size_dast: 0,
ci_max_artifact_size_codequality: 0,
ci_max_artifact_size_license_management: 0,
ci_max_artifact_size_license_scanning: 100,
ci_max_artifact_size_performance: 0,
ci_max_artifact_size_metrics: 0,
ci_max_artifact_size_metrics_referee: 0,
ci_max_artifact_size_network_referee: 0,
ci_max_artifact_size_dotenv: 0,
ci_max_artifact_size_cobertura: 0,
ci_max_artifact_size_terraform: 5,
ci_max_artifact_size_accessibility: 0,
ci_max_artifact_size_cluster_applications: 0,
ci_max_artifact_size_secret_detection: "[FILTERED]",
ci_max_artifact_size_requirements: 0,
ci_max_artifact_size_coverage_fuzzing: 0,
ci_max_artifact_size_browser_performance: 0,
ci_max_artifact_size_load_performance: 0,
ci_needs_size_limit: 2,
conan_max_file_size: 3221225472,
maven_max_file_size: 3221225472,
npm_max_file_size: 524288000,
nuget_max_file_size: 524288000,
pypi_max_file_size: 3221225472,
generic_packages_max_file_size: 5368709120,
golang_max_file_size: 104857600,
debian_max_file_size: 3221225472,
project_feature_flags: 200,
ci_max_artifact_size_api_fuzzing: 0,
ci_pipeline_deployments: 500,
pull_mirror_interval_seconds: 300,
daily_invites: 0,
rubygems_max_file_size: 3221225472,
terraform_module_max_file_size: 1073741824,
helm_max_file_size: 5242880,
ci_registered_group_runners: 1000,
ci_registered_project_runners: 1000,
ci_daily_pipeline_schedule_triggers: 0,
ci_max_artifact_size_cluster_image_scanning: 0,
ci_jobs_trace_size_limit: "[FILTERED]",
pages_file_entries: 200000,
dast_profile_schedules: 1,
external_audit_event_destinations: 5,
dotenv_variables: "[FILTERED]",
dotenv_size: 5120,
pipeline_triggers: 25000,
project_ci_secure_files: 100,
repository_size: 0,
security_policy_scan_execution_schedules: 0,
web_hook_calls_mid: 0,
web_hook_calls_low: 0,
project_ci_variables: "[FILTERED]",
group_ci_variables: "[FILTERED]",
ci_max_artifact_size_cyclonedx: 1,
rpm_max_file_size: 5368709120,
pipeline_hierarchy_size: 1000,
ci_max_artifact_size_requirements_v2: 0,
enforcement_limit: 0,
notification_limit: 0,
dashboard_limit_enabled_at: nil,
web_hook_calls: 0,
project_access_token_limit: 0,
google_cloud_logging_configurations: 5,
ml_model_max_file_size: 10737418240,
limits_history: {}
```

根据 [Rails 控制台中的过滤功能](operations/rails_console.md#filtered-console-output)，某些限制值在列表中显示为 `[FILTERED]`。
