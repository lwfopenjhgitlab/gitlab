---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
disqus_identifier: 'https://docs.gitlab.com/ee/workflow/lfs/lfs_administration.html'
---

# 极狐GitLab Git 大文件存储 (LFS) 管理 **(FREE SELF)**

此页面包含有关在私有化部署实例中配置 Git LFS 的信息。
有关 Git LFS 的用户文档，请参阅 [Git 大文件存储](../../topics/git/lfs/index.md)。

先决条件：

- 用户需要安装 [Git LFS 客户端](https://git-lfs.com/) 1.0.1 或更高版本。

## 启用或禁用 LFS

默认情况下启用 LFS。要禁用它：

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   # Change to true to enable lfs - enabled by default if not defined
   gitlab_rails['lfs_enabled'] = false
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml`：

   ```yaml
   global:
     appConfig:
       lfs:
         enabled: false
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

**Docker**

1. 编辑 `docker-compose.yml`：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['lfs_enabled'] = false
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production: &base
     lfs:
       enabled: false
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

## 更改本地存储路径

Git LFS 对象可以很大。默认情况下，它们存储在安装极狐GitLab 的服务器上。

NOTE:
对于 Docker 安装实例，您可以更改装载数据的路径。 对于 Helm chart 安装实例，请使用[对象存储](https://docs.gitlab.cn/charts/advanced/external-object-storage/)。

要更改默认的本地存储路径位置：

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   # /var/opt/gitlab/gitlab-rails/shared/lfs-objects by default.
   gitlab_rails['lfs_storage_path'] = "/mnt/storage/lfs-objects"
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   # /home/git/gitlab/shared/lfs-objects by default.
   production: &base
     lfs:
       storage_path: /mnt/storage/lfs-objects
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

<a id="storing-lfs-objects-in-remote-object-storage"></a>

## 在远程对象存储中存储 LFS 对象

您可以将 LFS 对象存储在远程对象存储中。这使您可以减少对本地磁盘的读取和写入，并释放磁盘空间。

NOTE:
在 13.2 及更高版本，您应该使用[整合对象存储设置](../object_storage.md#consolidated-object-storage-configuration)。

<a id="migrating-to-object-storage"></a>

### 迁移到对象存储

您可以将 LFS 对象从本地存储迁移到对象存储。处理在后台完成，**不需要停机**。

1. [配置对象存储](../object_storage.md#consolidated-object-storage-configuration)。
1. 迁移 LFS 对象：

   **Omnibus**

   ```shell
   sudo gitlab-rake gitlab:lfs:migrate
   ```

   **Docker**

   ```shell
   sudo docker exec -t <container name> gitlab-rake gitlab:lfs:migrate
   ```

   **源安装**

   ```shell
   sudo -u git -H bundle exec rake gitlab:lfs:migrate RAILS_ENV=production
   ```

1. 可选。使用 PostgreSQL 控制台跟踪进度并验证所有作业 LFS 对象是否已成功迁移。
   1. 打开 PostgreSQL 控制台：

      **Omnibus**

      ```shell
      sudo gitlab-psql
      ```

      **Docker**

      ```shell
      sudo docker exec -it <container_name> /bin/bash
      gitlab-psql
      ```

      **源安装**

      ```shell
      sudo -u git -H psql -d gitlabhq_production
      ```

   1. 使用以下 SQL 查询验证所有 LFS 文件是否已迁移到对象存储。`objectstg` 的数量应与 `total` 相同：

      ```shell
      gitlabhq_production=# SELECT count(*) AS total, sum(case when file_store = '1' then 1 else 0 end) AS filesystem, sum(case when file_store = '2' then 1 else 0 end) AS objectstg FROM lfs_objects;

      total | filesystem | objectstg
      ------+------------+-----------
       2409 |          0 |      2409
      ```

1. 验证 `lfs-objects` 目录中的磁盘上没有文件：

   **Omnibus**

   ```shell
   sudo find /var/opt/gitlab/gitlab-rails/shared/lfs-objects -type f | grep -v tmp | wc -l
   ```

   **Docker**

   假设您将 `/var/opt/gitlab` 挂载到 `/srv/gitlab`：

   ```shell
   sudo find /srv/gitlab/gitlab-rails/shared/lfs-objects -type f | grep -v tmp | wc -l
   ```

   **源安装**

   ```shell
   sudo find /home/git/gitlab/shared/lfs-objects -type f | grep -v tmp | wc -l
   ```

### 迁移回本地存储

NOTE:
对于 Helm chart，您应该使用[对象存储](https://docs.gitlab.cn/charts/advanced/external-object-storage/)。

要迁移回本地存储：

**Omnibus**

1. 迁移 LFS 对象：

   ```shell
   sudo gitlab-rake gitlab:lfs:migrate_to_local
   ```

1. 编辑 LFS 对象的 `/etc/gitlab/gitlab.rb` 和[禁用对象存储](../object_storage.md#selectively-disabling-object-storage)：

   ```ruby
   gitlab_rails['object_store']['objects']['lfs']['enabled'] = false
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Docker**

1.迁移 LFS 对象：

   ```shell
   sudo docker exec -t <container name> gitlab-rake gitlab:lfs:migrate_to_local
   ```

1. 编辑 `docker-compose.yml` 并禁用 LFS 对象的对象存储：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['object_store']['objects']['lfs']['enabled'] = false
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 迁移 LFS 对象：

   ```shell
   sudo -u git -H bundle exec rake gitlab:lfs:migrate_to_local RAILS_ENV=production
   ```

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并禁用 LFS 对象的对象存储：

   ```yaml
   production: &base
     object_store:
       objects:
         lfs:
           enabled: false
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

## 存储统计

您可以看到用于群组和项目上的 LFS 对象的总存储空间：

- 在管理员区域。
- 在[群组](../../api/groups.md)和[项目 APIs](../../api/projects.md) 中。

## 故障排除

<a id="missing-lfs-object"></a>

### 缺少 LFS 对象

在以下任何一种情况下，都可能会出现有关丢失 LFS 对象的错误：

- 将 LFS 对象从磁盘迁移到对象存储时，出现以下错误消息：

  ```plaintext
  ERROR -- : Failed to transfer LFS object
  006622269c61b41bf14a22bbe0e43be3acf86a4a446afb4250c3794ea47541a7
  with error: No such file or directory @ rb_sysopen -
  /var/opt/gitlab/gitlab-rails/shared/lfs-objects/00/66/22269c61b41bf14a22bbe0e43be3acf86a4a446afb4250c3794ea47541a7
  ```

   （为了便于阅读，添加了换行符。）

- 使用 `VERBOSE=1` 参数运行 [LFS 对象的完整性检查](../raketasks/check.md#uploaded-files-integrity)。

数据库可以有不在磁盘上的 LFS 对象的记录。数据库条目可能会阻止推送对象的新副本。要删除这些引用：

1. [启动 Rails 控制台](../operations/rails_console.md)。
1. 查询 rails 控制台报错的对象，返回文件路径：

   ```ruby
   lfs_object = LfsObject.find_by(oid: '006622269c61b41bf14a22bbe0e43be3acf86a4a446afb4250c3794ea47541a7')
   lfs_object.file.path
   ```

1. 检查磁盘或对象存储是否存在：

   ```shell
   ls -al /var/opt/gitlab/gitlab-rails/shared/lfs-objects/00/66/22269c61b41bf14a22bbe0e43be3acf86a4a446afb4250c3794ea47541a7
   ```

1. 如果文件不存在，通过 rails 控制台删除数据库记录：

   ```ruby
   lfs_object.destroy
   ```

### LFS 命令在 TLS v1.3 服务器上失败

如果您将极狐GitLab 配置为[禁用 TLS v1.2](https://docs.gitlab.cn/omnibus/settings/nginx.html)，并且仅启用 TLS v1.3 连接，则 LFS 操作需要 [Git LFS 客户端](https://git-lfs.com) 2.11.0 或更高版本。如果您使用低于 2.11.0 版本的 Git LFS 客户端，极狐GitLab 会显示错误：

```plaintext
batch response: Post https://username:***@gitlab.example.com/tool/releases.git/info/lfs/objects/batch: remote error: tls: protocol version not supported
error: failed to fetch some objects from 'https://username:[MASKED]@gitlab.example.com/tool/releases.git/info/lfs'
```

在 TLS v1.3 配置的极狐GitLab 服务器上使用 CI 时，您必须[升级到极狐GitLab Runner](https://docs.gitlab.cn/runner/install/index.html) 13.2.0 或更高版本才能接收更新 Git LFS 客户端版本通过包含的 [Runner Helper 镜像](https://docs.gitlab.cn/runner/configuration/advanced-configuration.html#helper-image)。

要检查已安装的 Git LFS 客户端的版本，请运行以下命令：

```shell
git lfs version
```

## 查看 PDF 文件时出错

当 LFS 配置了对象存储并将 `proxy_download` 设置为 `false` 时，您在从 Web 浏览器预览 PDF 文件时可能会看到错误：

```plaintext
An error occurred while loading the file. Please try again later.
```

这是由于跨源资源共享 (CORS) 限制造成的：浏览器尝试从对象存储加载 PDF，但对象存储提供程序拒绝请求，因为极狐GitLab 域名与对象存储域名不同。

要解决此问题，请将对象存储提供商的 CORS 设置配置为允许极狐GitLab 域名。有关详细信息，请参阅以下文档：

1. [AWS S3](https://repost.aws/knowledge-center/s3-configure-cors)
1. [Google Cloud Storage](https://cloud.google.com/storage/docs/configuring-cors)
1. [Azure Storage](https://learn.microsoft.com/en-us/rest/api/storageservices/cross-origin-resource-sharing--cors--support-for-the-azure-storage-services)

## 已知限制

- 仅与 Git LFS 客户端版本 1.1.0 及更高版本或 1.0.2 兼容。
- 存储统计为每个链接到它的项目计算每个 LFS 对象。
