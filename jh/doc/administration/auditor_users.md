---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 审计员用户 **(PREMIUM SELF)**

具有审计员访问权限的用户对所有群组、项目和其它资源具有只读访问权限，但以下内容除外：

- [管理中心](../administration/admin_area.md)。
- 项目和群组设置。

<!--
For more information, see [Auditor user permissions and restrictions](#auditor-user-permissions-and-restrictions)
section.
-->

具有审计员访问权限的用户可适用于以下示例场景：

- 您的合规部门希望针对整个极狐GitLab 运行测试，确保用户遵守密码、信用卡和其他敏感数据策略。您可以通过审计员访问来实现这一点，而无需授予合规部门用户管理权限或将其添加到所有项目。
- 如果特定用户需要查看或访问您极狐GitLab 实例中的大多数项目，您可以创建一个具有审计员访问权限的帐户，然后与您要授予访问权限的用户共享凭据，而不是手动将用户添加到所有项目。

## 添加具有审计员访问权限的用户

要创建具有审计员访问权限的新用户帐户（或更改现有用户）：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏中，选择 **概览 > 用户**。
1. 创建新用户或编辑现有用户。将 **访问级别** 设置为 **审计员**。
1. 如果您创建新用户，请选择 **创建用户**。对于现有用户，选择 **保存修改**。

要撤消用户的审计员访问权限，请按照以上步骤操作，但将 **访问级别** 设置为 **普通**。

<!--
You can also give users auditor access using [SAML groups](../integration/saml.md#auditor-groups).
-->

<a id="auditor-user-permissions-and-restrictions"></a>

## 审计员用户权限和限制

审计员访问权限*不是*管理员访问权限的只读版本，因为不允许访问管理中心。

为了访问他们自己的资源和他们所属的群组或项目中的资源，具有审计员访问权限的用户与普通用户具有相同的权限]。

如果您以审计员访问权限登录：

- 对您拥有的项目和群组具有完全访问权限。
- 对您不是其成员的项目和群组具有只读访问权限。
- 根据您的角色对您所属的项目和群组拥有权限。例如，如果您具有开发人员角色，您可以推送提交或评论议题。
- 可以使用 UI 或 API 访问相同的资源。
- 无法查看管理中心，或执行任何管理操作。
- 启用 [debug 日志记录](../ci/variables/index.md#enable-debug-logging)时，无法查看作业日志。
