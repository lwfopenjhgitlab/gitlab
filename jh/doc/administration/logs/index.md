---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 日志系统 **(FREE SELF)**

极狐GitLab 有一个先进的日志系统，所有内容都被记录下来，因此您可以使用各种系统日志文件分析您的实例。除了系统日志文件，极狐GitLab 付费版还提供了[审计事件](../audit_events.md)。

系统日志文件通常是标准日志文件格式的纯文本。
本指南讨论如何阅读和使用这些系统日志文件。

阅读有关日志系统和使用日志的更多信息：

- [自定义 Omnibus GitLab 安装的日志记录](https://docs.gitlab.cn/omnibus/settings/logs.html)，包括调整日志保留、日志转发、将日志从 JSON 切换到纯文本日志记录等。
- [如何解析和分析 JSON 日志](../logs/log_parsing.md)。

## 日志级别

每条日志消息都有一个指定的日志级别，指示其重要性和详细程度。
每个记录器都有一个指定的最低日志级别。
仅当日志级别等于或高于最低日志级别时，记录器才会发出日志消息。

支持以下日志级别：

| 级别 | 名称      |
|:------|:----------|
| 0     | `DEBUG`   |
| 1     | `INFO`    |
| 2     | `WARN`    |
| 3     | `ERROR`   |
| 4     | `FATAL`   |
| 5     | `UNKNOWN` |

极狐GitLab 记录器会发出所有日志消息，因为它们默认设置为 `DEBUG`。

### 覆盖默认日志级别

您可以使用 `GITLAB_LOG_LEVEL` 环境变量覆盖极狐GitLab 记录器的最低日志级别。
有效值可以是 `0` 到 `5` 的值，或者是日志级别的名称。

示例：

```shell
GITLAB_LOG_LEVEL=info
```

对于某些服务，还有其他不受此设置影响的日志级别。
其中一些服务有自己的环境变量来覆盖日志级别。例如：

| 服务              | 日志级别 | 环境变量 |
|:---------------------|:----------|:---------------------|
| 极狐GitLab API           | `INFO`    |                      |
| 极狐GitLab Cleanup       | `INFO`    | `DEBUG`              |
| 极狐GitLab Doctor        | `INFO`    | `VERBOSE`            |
| 极狐GitLab Export        | `INFO`    | `EXPORT_DEBUG`       |
| 极狐GitLab Geo           | `INFO`    |                      |
| 极狐GitLab Import        | `INFO`    | `IMPORT_DEBUG`       |
| 极狐GitLab QA Runtime    | `INFO`    | `QA_LOG_LEVEL`       |
| Google APIs          | `INFO`    |                      |
| Rack Timeout         | `ERROR`   |                      |
| Sidekiq (server)     | `INFO`    |                      |
| Snowplow Tracker     | `FATAL`   |                      |
| gRPC Client (Gitaly) | `WARN`    | `GRPC_LOG_LEVEL`     |

## 日志轮替

特定服务的日志可以通过以下方式管理和轮替：

- `logrotate`
- `svlogd`（`runit` 的服务日志守护进程）
- `logrotate` 和 `svlogd`
- 无

下表包含有关负责管理和轮替所包含服务的日志的信息。[由 `svlogd` 管理](https://docs.gitlab.cn/omnibus/settings/logs.html#runit-logs)的日志被写入一个名为 `current` 的文件。极狐GitLab 中内置的 `logrotate` 服务[管理所有日志](https://docs.gitlab.cn/omnibus/settings/logs.html#logrotate)，但由 `runit` 捕获的日志除外。

| 日志类型                                        | 由 logrotate 管理    | 由 svlogd/runit 管理 |
|:------------------------------------------------|:------------------------|:------------------------|
| [Alertmanager 日志](#alertmanager-logs)         | **{dotted-circle}** No  | **{check-circle}** Yes  |
| [Crond 日志](#crond-logs)                       | **{dotted-circle}** No  | **{check-circle}** Yes  |
| [Gitaly](#gitaly-logs)                          | **{check-circle}** Yes  | **{check-circle}** Yes  |
| [极狐GitLab Exporter for Omnibus](#gitlab-exporter) | **{dotted-circle}** No  | **{check-circle}** Yes  |
| [极狐GitLab Pages 日志](#pages-logs)                | **{check-circle}** Yes  | **{check-circle}** Yes  |
| 极狐GitLab Rails                                    | **{check-circle}** Yes  | **{dotted-circle}** No  |
| [极狐GitLab Shell 日志](#gitlab-shelllog)           | **{check-circle}** Yes  | **{dotted-circle}** No  |
| [Grafana 日志](#grafana-logs)                   | **{dotted-circle}** No  | **{check-circle}** Yes  |
| [LogRotate 日志](#logrotate-logs)               | **{dotted-circle}** No  | **{check-circle}** Yes  |
| [Mailroom](#mail_room_jsonlog-default)          | **{check-circle}** Yes  | **{check-circle}** Yes  |
| [NGINX](#nginx-logs)                            | **{check-circle}** Yes  | **{check-circle}** Yes  |
| [PostgreSQL 日志](#postgresql-logs)             | **{dotted-circle}** No  | **{check-circle}** Yes  |
| [Praefect 日志](#praefect-logs)                 | **{dotted-circle}** Yes | **{check-circle}** Yes  |
| [Prometheus 日志](#prometheus-logs)             | **{dotted-circle}** No  | **{check-circle}** Yes  |
| [Puma](#puma-logs)                              | **{check-circle}** Yes  | **{check-circle}** Yes  |
| [Redis 日志](#redis-logs)                       | **{dotted-circle}** No  | **{check-circle}** Yes  |
| [Registry 日志](#registry-logs)                 | **{dotted-circle}** No  | **{check-circle}** Yes  |
| [Workhorse 日志](#workhorse-logs)               | **{check-circle}** Yes  | **{check-circle}** Yes  |

## `production_json.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/production_json.log`
- 源安装实例：`/home/git/gitlab/log/production_json.log`

由于 [Lograge](https://github.com/roidrage/lograge/) 的存在，其中包含从极狐GitLab 收到的 Rails 控制器请求的结构化日志。
来自 API 的请求被记录到 `api_json.log` 中的单独文件中。

每行都包含可以被 Elasticsearch 和 Splunk 等服务获取的 JSON。
为了便于阅读，示例中添加了换行符：

```json
{
  "method":"GET",
  "path":"/gitlab/gitlab-foss/issues/1234",
  "format":"html",
  "controller":"Projects::IssuesController",
  "action":"show",
  "status":200,
  "time":"2017-08-08T20:15:54.821Z",
  "params":[{"key":"param_key","value":"param_value"}],
  "remote_ip":"18.245.0.1",
  "user_id":1,
  "username":"admin",
  "queue_duration_s":0.0,
  "gitaly_calls":16,
  "gitaly_duration_s":0.16,
  "redis_calls":115,
  "redis_duration_s":0.13,
  "redis_read_bytes":1507378,
  "redis_write_bytes":2920,
  "correlation_id":"O1SdybnnIq7",
  "cpu_s":17.50,
  "db_duration_s":0.08,
  "view_duration_s":2.39,
  "duration_s":20.54,
  "pid": 81836,
  "worker_id":"puma_0"
}
```

此示例是针对特定问题的 GET 请求。每行还包含性能数据，时间以秒为单位：

- `duration_s`：检索请求的总时间
- `queue_duration_s`：请求在极狐GitLab Workhorse 中排队的总时间
- `view_duration_s`：Rails 视图内的总时间
- `db_duration_s`：从 PostgreSQL 检索数据的总时间
- `cpu_s`：在 CPU 上花费的总时间
- `gitaly_duration_s`：Gitaly 通话的总时间
- `gitaly_calls`：调用 Gitaly 的总次数
- `redis_calls`：调用 Redis 的总次数
- `redis_duration_s`：从 Redis 检索数据的总时间
- `redis_read_bytes`：从 Redis 读取的总字节数
- `redis_write_bytes`：写入 Redis 的总字节数
- `redis_<instance>_calls`：调用 Redis 实例的总次数
- `redis_<instance>_duration_s`：从 Redis 实例检索数据的总时间
- `redis_<instance>_read_bytes`：从 Redis 实例读取的总字节数
- `redis_<instance>_write_bytes`：写入 Redis 实例的总字节数
- `pid`：Worker 的 Linux 进程 ID（Worker 重新启动时更改）
- `worker_id`：Worker 的逻辑 ID（Worker 重启时不会改变）

使用 HTTP 传输的用户克隆和获取活动在日志中显示为 `action: git_upload_pack`。

此外，日志还包含原始 IP 地址 (`remote_ip`)、用户 ID (`user_id`) 和用户名 (`username`)。

如果使用高级搜索，某些端点（例如 `/search`）可能会向 Elasticsearch 发出请求，额外记录 `elasticsearch_calls` 和 `elasticsearch_call_duration_s`：

- `elasticsearch_calls`：调用 Elasticsearch 的总次数
- `elasticsearch_duration_s`：Elasticsearch 调用所花费的总时间
- `elasticsearch_timed_out_count`：超时并因此返回部分结果的 Elasticsearch 调用总数

ActionCable 连接和订阅事件也记录到此文件中，它们遵循以前的格式。`method`、`path` 和 `format` 字段不适用，始终为空。
ActionCable 连接或通道类用作 `controller`。

```json
{
  "method":null,
  "path":null,
  "format":null,
  "controller":"IssuesChannel",
  "action":"subscribe",
  "status":200,
  "time":"2020-05-14T19:46:22.008Z",
  "params":[{"key":"project_path","value":"gitlab/gitlab-foss"},{"key":"iid","value":"1"}],
  "remote_ip":"127.0.0.1",
  "user_id":1,
  "username":"admin",
  "ua":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:76.0) Gecko/20100101 Firefox/76.0",
  "correlation_id":"jSOIEynHCUa",
  "duration_s":0.32566
}
```

NOTE:
从 12.5 版本开始，如果发生错误，`class`、`message` 和 `backtrace` 中会包含一个 `exception` 字段。以前的版本包括一个 `error` 字段而不是 `exception.class` 和 `exception.message`。例如：

```json
{
  "method": "GET",
  "path": "/admin",
  "format": "html",
  "controller": "Admin::DashboardController",
  "action": "index",
  "status": 500,
  "time": "2019-11-14T13:12:46.156Z",
  "params": [],
  "remote_ip": "127.0.0.1",
  "user_id": 1,
  "username": "root",
  "ua": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:70.0) Gecko/20100101 Firefox/70.0",
  "queue_duration": 274.35,
  "correlation_id": "KjDVUhNvvV3",
  "queue_duration_s":0.0,
  "gitaly_calls":16,
  "gitaly_duration_s":0.16,
  "redis_calls":115,
  "redis_duration_s":0.13,
  "correlation_id":"O1SdybnnIq7",
  "cpu_s":17.50,
  "db_duration_s":0.08,
  "view_duration_s":2.39,
  "duration_s":20.54,
  "pid": 81836,
  "worker_id": "puma_0",
  "exception.class": "NameError",
  "exception.message": "undefined local variable or method `adsf' for #<Admin::DashboardController:0x00007ff3c9648588>",
  "exception.backtrace": [
    "app/controllers/admin/dashboard_controller.rb:11:in `index'",
    "ee/app/controllers/ee/admin/dashboard_controller.rb:14:in `index'",
    "ee/lib/gitlab/ip_address_state.rb:10:in `with'",
    "ee/app/controllers/ee/application_controller.rb:43:in `set_current_ip_address'",
    "lib/gitlab/session.rb:11:in `with_session'",
    "app/controllers/application_controller.rb:450:in `set_session_storage'",
    "app/controllers/application_controller.rb:444:in `set_locale'",
    "ee/lib/gitlab/jira/middleware.rb:19:in `call'"
  ]
}
```

## `production.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/production.log`
- 源安装实例：`/home/git/gitlab/log/production.log`

它包含有关所有已执行请求的信息。您可以查看请求的 URL 和类型、IP 地址，以及服务该特定请求所涉及的代码部分。此外，您还可以查看执行的所有 SQL 请求，以及每个请求花费了多少时间。此任务对极狐GitLab 贡献者和开发人员更有用。报告错误时使用此日志文件的一部分。例如：

```plaintext
Started GET "/gitlabhq/yaml_db/tree/master" for 168.111.56.1 at 2015-02-12 19:34:53 +0200
Processing by Projects::TreeController#show as HTML
  Parameters: {"project_id"=>"gitlabhq/yaml_db", "id"=>"master"}

  ... [CUT OUT]

  Namespaces"."created_at" DESC, "namespaces"."id" DESC LIMIT 1 [["id", 26]]
  CACHE (0.0ms) SELECT  "members".* FROM "members"  WHERE "members"."source_type" = 'Project' AND "members"."type" IN ('ProjectMember') AND "members"."source_id" = $1 AND "members"."source_type" = $2 AND "members"."user_id" = 1  ORDER BY "members"."created_at" DESC, "members"."id" DESC LIMIT 1  [["source_id", 18], ["source_type", "Project"]]
  CACHE (0.0ms) SELECT  "members".* FROM "members"  WHERE "members"."source_type" = 'Project' AND "members".
  (1.4ms) SELECT COUNT(*) FROM "merge_requests"  WHERE "merge_requests"."target_project_id" = $1 AND ("merge_requests"."state" IN ('opened','reopened')) [["target_project_id", 18]]
  Rendered layouts/nav/_project.html.haml (28.0ms)
  Rendered layouts/_collapse_button.html.haml (0.2ms)
  Rendered layouts/_flash.html.haml (0.1ms)
  Rendered layouts/_page.html.haml (32.9ms)
Completed 200 OK in 166ms (Views: 117.4ms | ActiveRecord: 27.2ms)
```

在此示例中，服务器在 `2015-02-12 19:34:53 +0200` 处理了来自 IP `168.111.56.1` 的 URL 为 `/gitlabhq/yaml_db/tree/master` 的 HTTP 请求。
该请求由 `Projects::TreeController` 处理。

## `api_json.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/api_json.log`
- 源安装实例：`/home/git/gitlab/log/api_json.log`

它可以帮助您查看直接向 API 发出的请求。例如：

```json
{
  "time":"2018-10-29T12:49:42.123Z",
  "severity":"INFO",
  "duration":709.08,
  "db":14.59,
  "view":694.49,
  "status":200,
  "method":"GET",
  "path":"/api/v4/projects",
  "params":[{"key":"action","value":"git-upload-pack"},{"key":"changes","value":"_any"},{"key":"key_id","value":"secret"},{"key":"secret_token","value":"[FILTERED]"}],
  "host":"localhost",
  "remote_ip":"::1",
  "ua":"Ruby",
  "route":"/api/:version/projects",
  "user_id":1,
  "username":"root",
  "queue_duration":100.31,
  "gitaly_calls":30,
  "gitaly_duration":5.36,
  "pid": 81836,
  "worker_id": "puma_0",
  ...
}
```

此条目显示访问的内部端点，检查关联的 SSH 密钥是否可以使用 `git fetch` 或 `git clone` 下载相关项目。在这个例子中，我们看到：

- `duration`：检索请求的总时间（以毫秒为单位）
- `queue_duration`：请求在极狐GitLab Workhorse 中排队的总时间（以毫秒为单位）
- `method`：用于发出请求的 HTTP 方法
- `path`：查询的相对路径
- `params`：在查询字符串或 HTTP 正文中传递的键值对（过滤掉敏感参数，例如密码和令牌）
- `ua`：请求者的用户代理

NOTE:
从 [`Grape Logging`](https://github.com/aserafin/grape_logging) v1.8.4 开始，`view_duration_s` 由 [`duration_s - db_duration_s`](https://github.com/aserafin/grape_logging/blob/v1.8.4/lib/grape_logging/middleware/request_logger.rb#L117-L119) 计算。
因此，`view_duration_s` 会受到多种不同因素的影响，例如 Redis 上的读写过程或外部 HTTP，而不仅仅是序列化过程。

## `application.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/application.log`
- 源安装实例：`/home/git/gitlab/log/application.log`

它可以帮助您发现实例中发生的事件，例如用户创建和项目删除。例如：

```plaintext
October 06, 2014 11:56: User "Administrator" (admin@example.com) was created
October 06, 2014 11:56: Documentcloud created a new project "Documentcloud / Underscore"
October 06, 2014 11:56: Gitlab Org created a new project "Gitlab Org / Gitlab Ce"
October 07, 2014 11:25: User "Claudie Hodkiewicz" (nasir_stehr@olson.co.uk)  was removed
October 07, 2014 11:25: Project "project133" was removed
```

## `application_json.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/application_json.log`
- 源安装实例：`/home/git/gitlab/log/application_json.log`

它在 `application.log` 中包含 JSON 版本的日志，如下例所示：

```json
{
  "severity":"INFO",
  "time":"2020-01-14T13:35:15.466Z",
  "correlation_id":"3823a1550b64417f9c9ed8ee0f48087e",
  "message":"User \"Administrator\" (admin@example.com) was created"
}
{
  "severity":"INFO",
  "time":"2020-01-14T13:35:15.466Z",
  "correlation_id":"78e3df10c9a18745243d524540bd5be4",
  "message":"Project \"project133\" was removed"
}
```

## `integrations_json.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/integrations_json.log`
- 源安装实例：`/home/git/gitlab/log/integrations_json.log`

它包含有关集成活动的信息，例如 Jira、Asana 和 irker 服务。它使用 JSON 格式，如下例所示：

```json
{
  "severity":"ERROR",
  "time":"2018-09-06T14:56:20.439Z",
  "service_class":"Integrations::Jira",
  "project_id":8,
  "project_path":"h5bp/html5-boilerplate",
  "message":"Error sending message",
  "client_url":"http://jira.gitlap.com:8080",
  "error":"execution expired"
}
{
  "severity":"INFO",
  "time":"2018-09-06T17:15:16.365Z",
  "service_class":"Integrations::Jira",
  "project_id":3,
  "project_path":"namespace2/project2",
  "message":"Successfully posted",
  "client_url":"http://jira.example.com"
}
```

## `kubernetes.log`（已废弃）

> 废弃于 14.5 版本。

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/kubernetes.log`
- 源安装实例：`/home/git/gitlab/log/kubernetes.log`

它记录与[基于证书的集群](../../user/project/clusters/index.md)相关的信息，例如连接错误。每行都包含可以被 Elasticsearch 和 Splunk 等服务接收的 JSON。

## `git_json.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/git_json.log`
- 源安装实例：`/home/git/gitlab/log/git_json.log`

在 12.2 版本之后，这个文件从 `githost.log` 重命名为 `git_json.log` 并以 JSON 格式存储。

极狐GitLab 必须与 Git 仓库交互，但在极少数情况下可能会出错。如果发生这种情况，您需要确切地知道发生了什么。此日志文件包含从极狐GitLab 到 Git 仓库的所有失败请求。在大多数情况下，此文件仅对开发人员有用。例如：

```json
{
   "severity":"ERROR",
   "time":"2019-07-19T22:16:12.528Z",
   "correlation_id":"FeGxww5Hj64",
   "message":"Command failed [1]: /usr/bin/git --git-dir=/Users/vsizov/gitlab-development-kit/gitlab/tmp/tests/gitlab-satellites/group184/gitlabhq/.git --work-tree=/Users/vsizov/gitlab-development-kit/gitlab/tmp/tests/gitlab-satellites/group184/gitlabhq merge --no-ff -mMerge branch 'feature_conflict' into 'feature' source/feature_conflict\n\nerror: failed to push some refs to '/Users/vsizov/gitlab-development-kit/repositories/gitlabhq/gitlab_git.git'"
}
```

## `audit_json.log` **(FREE)**

NOTE:
免费版跟踪少量不同的审计事件。专业版跟踪更多审计事件。

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/audit_json.log`
- 源安装实例：`/home/git/gitlab/log/audit_json.log`

对群组或项目设置和成员资格（`target_details`）的更改会记录到此文件中。例如：

```json
{
  "severity":"INFO",
  "time":"2018-10-17T17:38:22.523Z",
  "author_id":3,
  "entity_id":2,
  "entity_type":"Project",
  "change":"visibility",
  "from":"Private",
  "to":"Public",
  "author_name":"John Doe4",
  "target_id":2,
  "target_type":"Project",
  "target_details":"namespace2/project2"
}
```

<a id="sidekiq-logs"></a>

## Sidekiq 日志

<!--
NOTE:
In Omnibus GitLab `12.10` or earlier, the Sidekiq log is at `/var/log/gitlab/gitlab-rails/sidekiq.log`.
-->

对于 Omnibus GitLab 安装，一些 Sidekiq 日志位于 `/var/log/gitlab/sidekiq/current` 中，如下所示。

### `sidekiq.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/sidekiq/current`
- 源安装实例：`/home/git/gitlab/log/sidekiq.log`

极狐GitLab 使用后台作业来处理可能需要很长时间的任务。有关处理这些作业的所有信息都记录在此文件中。例如：

```plaintext
2014-06-10T07:55:20Z 2037 TID-tm504 ERROR: /opt/bitnami/apps/discourse/htdocs/vendor/bundle/ruby/1.9.1/gems/redis-3.0.7/lib/redis/client.rb:228:in `read'
2014-06-10T18:18:26Z 14299 TID-55uqo INFO: Booting Sidekiq 3.0.0 with redis options {:url=>"redis://localhost:6379/0", :namespace=>"sidekiq"}
```

您可以选择为 Sidekiq 生成 JSON 日志，而不是以前的格式。例如：

```json
{
  "severity":"INFO",
  "time":"2018-04-03T22:57:22.071Z",
  "queue":"cronjob:update_all_mirrors",
  "args":[],
  "class":"UpdateAllMirrorsWorker",
  "retry":false,
  "queue_namespace":"cronjob",
  "jid":"06aeaa3b0aadacf9981f368e",
  "created_at":"2018-04-03T22:57:21.930Z",
  "enqueued_at":"2018-04-03T22:57:21.931Z",
  "pid":10077,
  "worker_id":"sidekiq_0",
  "message":"UpdateAllMirrorsWorker JID-06aeaa3b0aadacf9981f368e: done: 0.139 sec",
  "job_status":"done",
  "duration":0.139,
  "completed_at":"2018-04-03T22:57:22.071Z",
  "db_duration":0.05,
  "db_duration_s":0.0005,
  "gitaly_duration":0,
  "gitaly_calls":0
}
```

对于 Omnibus 安装实例，添加配置选项：

```ruby
sidekiq['log_format'] = 'json'
```

对于源安装实例，编辑 `gitlab.yml` 并设置 Sidekiq `log_format` 配置选项：

```yaml
  ## Sidekiq
  sidekiq:
    log_format: json
```

### `sidekiq_client.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/sidekiq_client.log`
- 源安装实例：`/home/git/gitlab/log/sidekiq_client.log`

在 Sidekiq 开始处理作业之前，该文件包含有关作业的日志信息，例如在入队之前。

此日志文件遵循与 [`sidekiq.log`](#sidekiqlog) 相同的结构，因此如果您已如上所述为 Sidekiq 配置此文件，则其结构为 JSON。

## `gitlab-shell.log`

极狐GitLab Shell 被用于执行 Git 命令并提供对 Git 仓库的 SSH 访问。

### 对于 12.10 及更高版本

包含 `git-{upload-pack,receive-pack}` 请求的信息位于 `/var/log/gitlab/gitlab-shell/gitlab-shell.log`。 有关 Gitaly 与极狐GitLab Shell 的钩子的信息位于 `/var/log/gitlab/gitaly/current`。

`/var/log/gitlab/gitlab-shell/gitlab-shell.log` 的示例日志条目：

```json
{
  "duration_ms": 74.104,
  "level": "info",
  "method": "POST",
  "msg": "Finished HTTP request",
  "time": "2020-04-17T20:28:46Z",
  "url": "http://127.0.0.1:8080/api/v4/internal/allowed"
}
{
  "command": "git-upload-pack",
  "git_protocol": "",
  "gl_project_path": "root/example",
  "gl_repository": "project-1",
  "level": "info",
  "msg": "executing git command",
  "time": "2020-04-17T20:28:46Z",
  "user_id": "user-1",
  "username": "root"
}
```

`/var/log/gitlab/gitaly/current` 的示例日志条目：

```json
{
  "method": "POST",
  "url": "http://127.0.0.1:8080/api/v4/internal/allowed",
  "duration": 0.058012959,
  "gitaly_embedded": true,
  "pid": 16636,
  "level": "info",
  "msg": "finished HTTP request",
  "time": "2020-04-17T20:29:08+00:00"
}
{
  "method": "POST",
  "url": "http://127.0.0.1:8080/api/v4/internal/pre_receive",
  "duration": 0.031022552,
  "gitaly_embedded": true,
  "pid": 16636,
  "level": "info",
  "msg": "finished HTTP request",
  "time": "2020-04-17T20:29:08+00:00"
}
```

<!--
### For GitLab versions 12.5 through 12.9

For GitLab 12.5 to 12.9, depending on your installation method, this
file is located at:

- Omnibus 安装实例：`/var/log/gitlab/gitaly/gitlab-shell.log`
- Installation from source: `/home/git/gitaly/gitlab-shell.log`

Example log entries:

```json
{
  "method": "POST",
  "url": "http://127.0.0.1:8080/api/v4/internal/post_receive",
  "duration": 0.031809164,
  "gitaly_embedded": true,
  "pid": 27056,
  "level": "info",
  "msg": "finished HTTP request",
  "time": "2020-04-17T16:24:38+00:00"
}
```

### For GitLab 12.5 and earlier

For GitLab 12.5 and earlier, the file is at `/var/log/gitlab/gitlab-shell/gitlab-shell.log`.

Example log entries:

```plaintext
I, [2015-02-13T06:17:00.671315 #9291]  INFO -- : Adding project root/example.git at </var/opt/gitlab/git-data/repositories/root/dcdcdcdcd.git>.
I, [2015-02-13T06:17:00.679433 #9291]  INFO -- : Moving existing hooks directory and symlinking global hooks directory for /var/opt/gitlab/git-data/repositories/root/example.git.
```

User clone/fetch activity using SSH transport appears in this log as
`executing git command <gitaly-upload-pack...`.
-->

<a id="gitaly-logs"></a>

## Gitaly 日志

该文件位于 `/var/log/gitlab/gitaly/current` 中，由 [runit](http://smarden.org/runit/) 生成。
`runit` 与 Omnibus GitLab 打包在一起，[在 Omnibus GitLab 文档中](https://docs.gitlab.cn/omnibus/architecture/#runit) 提供了对其用途的简要说明。
[日志文件轮替](http://smarden.org/runit/svlogd.8.html)，以 Unix 时间戳格式重命名，并以 `gzip` 压缩（如 `@1584057562.s`）。

### `grpc.log`

该文件位于 `/var/log/gitlab/gitlab-rails/grpc.log`，用于 Omnibus GitLab 包。Gitaly 使用的本机 [gRPC](https://grpc.io/) 日志记录。

### `gitaly_ruby_json.log`

> 引入于 13.6 版本。

该文件位于 `/var/log/gitlab/gitaly/gitaly_ruby_json.log`，由 `gitaly-ruby` 生成。它包含 Gitaly 对 `gitaly-ruby` 进行的 gRPC 调用的访问日志。

### `gitaly_hooks.log`

该文件位于 `/var/log/gitlab/gitaly/gitaly_hooks.log`，由 `gitaly-hooks` 命令生成。它还包含有关在处理来自极狐GitLab API 的响应期间收到的失败的记录。

<a id="puma-logs"></a>

## Puma Logs

### `puma_stdout.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/puma/puma_stdout.log`
- 源安装实例：`/home/git/gitlab/log/puma_stdout.log`

### `puma_stderr.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/puma/puma_stderr.log`
- 源安装实例：`/home/git/gitlab/log/puma_stderr.log`

## `repocheck.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/repocheck.log`
- 源安装实例：`/home/git/gitlab/log/repocheck.log`

每当在项目上运行[仓库检查](../repository_checks.md)时，它都会记录信息。

## `importer.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/importer.log`
- 源安装实例：`/home/git/gitlab/log/importer.log`

它记录导入过程的进度。

## `exporter.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/exporter.log`
- 源安装实例：`/home/git/gitlab/log/exporter.log`

它记录导出过程的进度。

## `features_json.log`

> 引入于 13.7 版本。

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/features_json.log`
- 源安装实例：`/home/git/gitlab/log/features_json.log`

极狐GitLab 开发过程中来自功能标志的修改事件记录在此文件中。例如：

```json
{"severity":"INFO","time":"2020-11-24T02:30:59.860Z","correlation_id":null,"key":"cd_auto_rollback","action":"enable","extra.thing":"true"}
{"severity":"INFO","time":"2020-11-24T02:31:29.108Z","correlation_id":null,"key":"cd_auto_rollback","action":"enable","extra.thing":"true"}
{"severity":"INFO","time":"2020-11-24T02:31:29.129Z","correlation_id":null,"key":"cd_auto_rollback","action":"disable","extra.thing":"false"}
{"severity":"INFO","time":"2020-11-24T02:31:29.177Z","correlation_id":null,"key":"cd_auto_rollback","action":"enable","extra.thing":"Project:1"}
{"severity":"INFO","time":"2020-11-24T02:31:29.183Z","correlation_id":null,"key":"cd_auto_rollback","action":"disable","extra.thing":"Project:1"}
{"severity":"INFO","time":"2020-11-24T02:31:29.188Z","correlation_id":null,"key":"cd_auto_rollback","action":"enable_percentage_of_time","extra.percentage":"50"}
{"severity":"INFO","time":"2020-11-24T02:31:29.193Z","correlation_id":null,"key":"cd_auto_rollback","action":"disable_percentage_of_time"}
{"severity":"INFO","time":"2020-11-24T02:31:29.198Z","correlation_id":null,"key":"cd_auto_rollback","action":"enable_percentage_of_actors","extra.percentage":"50"}
{"severity":"INFO","time":"2020-11-24T02:31:29.203Z","correlation_id":null,"key":"cd_auto_rollback","action":"disable_percentage_of_actors"}
{"severity":"INFO","time":"2020-11-24T02:31:29.329Z","correlation_id":null,"key":"cd_auto_rollback","action":"remove"}
```

## `auth.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/auth.log`
- 源安装实例：`/home/git/gitlab/log/auth.log`

该日志记录：

- 原始端点上超过[速率限制](../../user/admin_area/settings/rate_limits_on_raw_endpoints.md)的请求。
- [受保护的路径](../../user/admin_area/settings/protected_paths.md)滥用请求。
- 在 12.3 及更高版本中，用户 ID 和用户名（如果可用）。

## `graphql_json.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/graphql_json.log`
- 源安装实例：`/home/git/gitlab/log/graphql_json.log`

GraphQL 查询记录在文件中。例如：

```json
{"query_string":"query IntrospectionQuery{__schema {queryType { name },mutationType { name }}}...(etc)","variables":{"a":1,"b":2},"complexity":181,"depth":1,"duration_s":7}
```

## `migrations.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/migrations.log`
- 源安装实例：`/home/git/gitlab/log/migrations.log`

## `mail_room_json.log`（默认）

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/mailroom/current`
- 源安装实例：`/home/git/gitlab/log/mail_room_json.log`

这个结构化的日志文件记录了 `mail_room` gem 中的内部活动。
它的名称和路径是可配置的，所以名称和路径可能与上面不匹配。

<a id="reconfigure-logs"></a>

## 重新配置日志

重新配置日志文件位于 Omnibus 包的 `/var/log/gitlab/reconfigure` 中。源安装实例没有重新配置日志。每当手动运行 `gitlab-ctl reconfigure` 或将其作为升级的一部分时，都会填充重新配置日志。

重新配置日志文件根据启动重新配置时的 UNIX 时间戳命名，例如 `1509705644.log`。

## `sidekiq_exporter.log` 和 `web_exporter.log`

如果 Prometheus 指标和 Sidekiq 导出器都启用了，Sidekiq 会启动一个 Web 服务器并监听定义的端口（默认值：`8082`）。 默认情况下，Sidekiq Exporter 访问日志被禁用，但可以根据您的安装方法启用：

- Omnibus 安装实例：使用 `/etc/gitlab/gitlab.rb` 中的 `sidekiq['exporter_log_enabled'] = true` 选项。
- 源安装实例：使用 `gitlab.yml` 中的 `sidekiq_exporter.log_enabled` 选项。

启用时，根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/sidekiq_exporter.log`
- 源安装实例：`/home/git/gitlab/log/sidekiq_exporter.log`

如果 Prometheus 指标和 Web Exporter 都启用了，Puma 会启动 Web 服务器并监听定义的端口（默认值：`8083`），并根据您的安装方法在某个位置生成访问日志：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/web_exporter.log`
- 源安装实例：`/home/git/gitlab/log/web_exporter.log`

## `database_load_balancing.log` **(PREMIUM SELF)**

包含极狐GitLab [数据库负载平衡](../postgresql/database_load_balancing.md)的详细信息。根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/database_load_balancing.log`
- 源安装实例：`/home/git/gitlab/log/database_load_balancing.log`

## `elasticsearch.log` **(PREMIUM SELF)**

此文件记录与 Elasticsearch 集成相关的信息，包括索引或搜索 Elasticsearch 期间的错误。根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/elasticsearch.log`
- 源安装实例：`/home/git/gitlab/log/elasticsearch.log`

每行都包含可以被 Elasticsearch 和 Splunk 等服务获取的 JSON。为清楚起见，已在以下示例行中添加了换行符：

```json
{
  "severity":"DEBUG",
  "time":"2019-10-17T06:23:13.227Z",
  "correlation_id":null,
  "message":"redacted_search_result",
  "class_name":"Milestone",
  "id":2,
  "ability":"read_milestone",
  "current_user_id":2,
  "query":"project"
}
```

## `exceptions_json.log`

该文件记录了 `Gitlab::ErrorTracking` 跟踪的异常信息，它提供了一种标准且一致的方式来处理已获救的异常。根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/exceptions_json.log`
- 源安装实例：`/home/git/gitlab/log/exceptions_json.log`

每行都包含可由 Elasticsearch 提取的 JSON。例如：

```json
{
  "severity": "ERROR",
  "time": "2019-12-17T11:49:29.485Z",
  "correlation_id": "AbDVUrrTvM1",
  "extra.project_id": 55,
  "extra.relation_key": "milestones",
  "extra.relation_index": 1,
  "exception.class": "NoMethodError",
  "exception.message": "undefined method `strong_memoize' for #<Gitlab::ImportExport::RelationFactory:0x00007fb5d917c4b0>",
  "exception.backtrace": [
    "lib/gitlab/import_export/relation_factory.rb:329:in `unique_relation?'",
    "lib/gitlab/import_export/relation_factory.rb:345:in `find_or_create_object!'"
  ]
}
```

## `service_measurement.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/service_measurement.log`
- 源安装实例：`/home/git/gitlab/log/service_measurement.log`

它仅包含一个结构化日志，其中包含每个服务执行的测量值。
它包含诸如 SQL 调用次数、`execution_time`、`gc_stats` 和`memory usage` 等。

例如：

```json
{ "severity":"INFO", "time":"2020-04-22T16:04:50.691Z","correlation_id":"04f1366e-57a1-45b8-88c1-b00b23dc3616","class":"Projects::ImportExport::ExportService","current_user":"John Doe","project_full_path":"group1/test-export","file_path":"/path/to/archive","gc_stats":{"count":{"before":127,"after":127,"diff":0},"heap_allocated_pages":{"before":10369,"after":10369,"diff":0},"heap_sorted_length":{"before":10369,"after":10369,"diff":0},"heap_allocatable_pages":{"before":0,"after":0,"diff":0},"heap_available_slots":{"before":4226409,"after":4226409,"diff":0},"heap_live_slots":{"before":2542709,"after":2641420,"diff":98711},"heap_free_slots":{"before":1683700,"after":1584989,"diff":-98711},"heap_final_slots":{"before":0,"after":0,"diff":0},"heap_marked_slots":{"before":2542704,"after":2542704,"diff":0},"heap_eden_pages":{"before":10369,"after":10369,"diff":0},"heap_tomb_pages":{"before":0,"after":0,"diff":0},"total_allocated_pages":{"before":10369,"after":10369,"diff":0},"total_freed_pages":{"before":0,"after":0,"diff":0},"total_allocated_objects":{"before":24896308,"after":24995019,"diff":98711},"total_freed_objects":{"before":22353599,"after":22353599,"diff":0},"malloc_increase_bytes":{"before":140032,"after":6650240,"diff":6510208},"malloc_increase_bytes_limit":{"before":25804104,"after":25804104,"diff":0},"minor_gc_count":{"before":94,"after":94,"diff":0},"major_gc_count":{"before":33,"after":33,"diff":0},"remembered_wb_unprotected_objects":{"before":34284,"after":34284,"diff":0},"remembered_wb_unprotected_objects_limit":{"before":68568,"after":68568,"diff":0},"old_objects":{"before":2404725,"after":2404725,"diff":0},"old_objects_limit":{"before":4809450,"after":4809450,"diff":0},"oldmalloc_increase_bytes":{"before":140032,"after":6650240,"diff":6510208},"oldmalloc_increase_bytes_limit":{"before":68537556,"after":68537556,"diff":0}},"time_to_finish":0.12298400001600385,"number_of_sql_calls":70,"memory_usage":"0.0 MiB","label":"process_48616"}
```

## `geo.log` **(PREMIUM SELF)**

Geo 将结构化日志消息存储在 `geo.log` 文件中。对于 Omnibus GitLab 安装，此文件位于 `/var/log/gitlab/gitlab-rails/geo.log`。

此文件包含有关 Geo 何时尝试同步仓库和文件的信息。文件中的每一行都包含一个单独的 JSON 条目，可以提取到 Elasticsearch 或 Splunk。

例如：

```json
{"severity":"INFO","time":"2017-08-06T05:40:16.104Z","message":"Repository update","project_id":1,"source":"repository","resync_repository":true,"resync_wiki":true,"class":"Gitlab::Geo::LogCursor::Daemon","cursor_delay_s":0.038}
```

此消息显示 Geo 检测到项目 `1` 需要更新仓库。

## `update_mirror_service_json.log`

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/update_mirror_service_json.log`
- 源安装实例：`/home/git/gitlab/log/update_mirror_service_json.log`

此文件包含有关在项目镜像期间发生的 LFS 错误的信息。在我们努力将其他项目镜像错误移至此日志时，可以使用[通用日志](#productionlog)。

```json
{
   "severity":"ERROR",
   "time":"2020-07-28T23:29:29.473Z",
   "correlation_id":"5HgIkCJsO53",
   "user_id":"x",
   "project_id":"x",
   "import_url":"https://mirror-source/group/project.git",
   "error_message":"The LFS objects download list couldn't be imported. Error: Unauthorized"
}
```

<a id="registry-logs"></a>

## Registry 日志

对于 Omnibus 安装，容器仓库日志位于 `/var/log/gitlab/registry/current` 中。

<a id="nginx-logs"></a>

## NGINX 日志

对于 Omnibus 安装，NGINX 日志位于：

- `/var/log/gitlab/nginx/gitlab_access.log`：向极狐GitLab 发出的请求日志
- `/var/log/gitlab/nginx/gitlab_error.log`：极狐GitLab 的 NGINX 错误日志
- `/var/log/gitlab/nginx/gitlab_pages_access.log`：对 Pages 静态站点的请求日志
- `/var/log/gitlab/nginx/gitlab_pages_error.log`：Pages 静态站点的 NGINX 错误日志
- `/var/log/gitlab/nginx/gitlab_registry_access.log`：向 Container Registry 发出的请求日志
- `/var/log/gitlab/nginx/gitlab_registry_error.log`：Container Registry 的 NGINX 错误日志
- `/var/log/gitlab/nginx/gitlab_mattermost_access.log`：向 Mattermost 发出的请求日志
- `/var/log/gitlab/nginx/gitlab_mattermost_error.log`：Mattermost 的 NGINX 错误日志

以下是默认的极狐GitLab NGINX 访问日志格式：

```plaintext
$remote_addr - $remote_user [$time_local] "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent"
```

<a id="pages-logs"></a>

## Pages 日志

对于 Omnibus 安装，Pages 日志位于 `/var/log/gitlab/gitlab-pages/current` 中。

例如：

```json
{
  "level": "info",
  "msg": "GitLab Pages Daemon",
  "revision": "52b2899",
  "time": "2020-04-22T17:53:12Z",
  "version": "1.17.0"
}
{
  "level": "info",
  "msg": "URL: https://gitlab.com/gitlab-org/gitlab-pages",
  "time": "2020-04-22T17:53:12Z"
}
{
  "gid": 998,
  "in-place": false,
  "level": "info",
  "msg": "running the daemon as unprivileged user",
  "time": "2020-04-22T17:53:12Z",
  "uid": 998
}
```

<a id="mattermost-logs"></a>

## Mattermost 日志

对于 Omnibus 安装，Mattermost 日志位于以下位置：

- `/var/log/gitlab/mattermost/mattermost.log`
- `/var/log/gitlab/mattermost/current`

<a id="workhorse-logs"></a>

## Workhorse 日志

对于 Omnibus 安装，Workhorse 日志位于 `/var/log/gitlab/gitlab-workhorse/current` 中。

<a id="postgresql-logs"></a>

## PostgreSQL 日志

对于 Omnibus 安装，PostgreSQL 日志位于 `/var/log/gitlab/postgresql/current` 中。

<a id="prometheus-logs"></a>

## Prometheus 日志

对于 Omnibus 安装，Prometheus 日志位于 `/var/log/gitlab/prometheus/current` 中。

<a id="redis-logs"></a>

## Redis 日志

对于 Omnibus 安装，Redis 日志位于 `/var/log/gitlab/redis/current` 中。

<a id="alertmanager-logs"></a>

## Alertmanager 日志

对于 Omnibus 安装，Alertmanager 日志位于 `/var/log/gitlab/alertmanager/current` 中。

<!-- vale gitlab.Spelling = NO -->

<a id="crond-logs"></a>

## Crond 日志

对于 Omnibus 安装，crond 日志位于 `/var/log/gitlab/crond/` 中。

<!-- vale gitlab.Spelling = YES -->

<a id="grafana-logs"></a>

## Grafana 日志

对于 Omnibus 安装，Grafana 日志位于 `/var/log/gitlab/grafana/current` 中。

<a id="logrotate-logs"></a>

## LogRotate 日志

对于 Omnibus 安装，`logrotate` 日志在 `/var/log/gitlab/logrotate/current` 中。

<a id="gitlab-monitor-logs"></a>

## 极狐GitLab Monitor 日志

对于 Omnibus 安装，极狐GitLab Monitor 日志位于 `/var/log/gitlab/gitlab-monitor/` 中。

<a id="gitlab-exporter"></a>

## GitLab Exporter

对于 Omnibus 安装，极狐GitLab Exporter 日志位于 `/var/log/gitlab/gitlab-exporter/current` 中。

<a id="gitlab-agent-server"></a>

## 极狐GitLab 代理服务器

对于 Omnibus 安装，极狐GitLab 代理服务器日志位于 `/var/log/gitlab/gitlab-kas/current` 中。

<a id="praefect-logs"></a>

## Praefect 日志

对于 Omnibus 安装，Praefect 日志位于 `/var/log/gitlab/praefect/` 中。

极狐GitLab 还跟踪 Praefect 的 Prometheus 指标。

<a id="backup-log"></a>

## Backup 日志

> 引入于 14.1 版本。

对于 Omnibus 安装，备份日志位于 `/var/log/gitlab/gitlab-rails/backup_json.log`。

此日志在[创建极狐GitLab 备份](../../raketasks/backup_restore.md)时填充。您可以使用此日志来了解备份过程是如何执行的。

<!--
## 性能条统计

> 引入于 13.7 版本。

根据您的安装方法，此文件位于：

- Omnibus 安装实例：`/var/log/gitlab/gitlab-rails/performance_bar_json.log`
- 源安装实例：`/home/git/gitlab/log/performance_bar_json.log`

性能条统计信息（当前仅 SQL 查询的持续时间）记录在该文件中。例如：

```json
{"severity":"INFO","time":"2020-12-04T09:29:44.592Z","correlation_id":"33680b1490ccd35981b03639c406a697","filename":"app/models/ci/pipeline.rb","method_path":"app/models/ci/pipeline.rb:each_with_object","request_id":"rYHomD0VJS4","duration_ms":26.889,"count":2,"query_type": "active-record"}
```

These statistics are logged on .com only, disabled on self-deployments.

## 收集日志

When [troubleshooting](../troubleshooting/index.md) issues that aren't localized to one of the
previously listed components, it's helpful to simultaneously gather multiple logs and statistics
from a GitLab instance.

NOTE:
GitLab Support often asks for one of these, and maintains the required tools.

### Briefly tail the main logs

If the bug or error is readily reproducible, save the main GitLab logs
[to a file](../troubleshooting/linux_cheat_sheet.md#files-and-directories) while reproducing the
problem a few times:

```shell
sudo gitlab-ctl tail | tee /tmp/<case-ID-and-keywords>.log
```

Conclude the log gathering with <kbd>Control</kbd> + <kbd>C</kbd>.

### GitLabSOS

If performance degradations or cascading errors occur that can't readily be attributed to one
of the previously listed GitLab components, [GitLabSOS](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos/)
can provide a broader perspective of the GitLab instance. For more details and instructions
to run it, read [the GitLabSOS documentation](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos/#gitlabsos).

### Fast-stats

[Fast-stats](https://gitlab.com/gitlab-com/support/toolbox/fast-stats) is a tool
for creating and comparing performance statistics from GitLab logs.
For more details and instructions to run it, read the
[documentation for fast-stats](https://gitlab.com/gitlab-com/support/toolbox/fast-stats#usage).
-->

## 查找具有相关 ID 的相关日志条目

大多数请求都有一个日志 ID，可用于[查找相关日志条目](tracing_correlation_id.md)。
