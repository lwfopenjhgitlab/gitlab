---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 查找具有相关 ID 的相关日志条目 **(FREE SELF)**

极狐GitLab 实例为大多数请求记录一个唯一的请求跟踪 ID（称为“相关 ID”）。对极狐GitLab 的每个单独请求都有自己的关联 ID，然后将其记录在该请求的每个极狐GitLab 组件的日志中。这使得跟踪分布式系统中的行为变得更加容易。如果没有此 ID，则可能很难或不可能匹配相关的日志条目。

<a id="identify-the-correlation-id-for-a-request"></a>

## 识别请求的相关 ID

相关 ID 记录在结构化日志中，键为 `correlation_id`，并记录在极狐GitLab 发送的所有响应头中，标题为 `x-request-id`。
您可以通过在任一位置搜索来找到您的相关 ID。

<a id="getting-the-correlation-id-in-your-browser"></a>

### 在浏览器中获取相关 ID

您可以使用浏览器的开发人员工具来监控和检查您正在访问的站点的网络活动。有关某些流行浏览器的网络监控文档，请参阅下面的链接。

- [Network Monitor - Firefox Developer Tools](https://developer.mozilla.org/en-US/docs/Tools/Network_Monitor)
- [Inspect Network Activity In Chrome DevTools](https://developer.chrome.com/docs/devtools/network/)
- [Safari Web Development Tools](https://developer.apple.com/safari/tools/)
- [Microsoft Edge Network panel](https://learn.microsoft.com/en-us/microsoft-edge/devtools-guide-chromium/network/)

要定位相关请求并查看其相关 ID：

1. 在您的网络监视器中启用持久日志记录。极狐GitLab 中的某些操作会在您提交表单后快速重定向您，因此这有助于捕获所有相关活动。
1. 为了帮助隔离您正在寻找的请求，您可以过滤“文档”请求。
1. 选择感兴趣的请求以查看更多详细信息。
1. 转到 **Headers** 部分并查找 **响应 Headers**。在那里您应该找到一个 `x-request-id` 标头，其值是极狐GitLab 为请求随机生成的。

请参见以下示例：

![Firefox's network monitor showing an request ID header](img/network_monitor_xid.png)

### 从您的日志中获取相关 ID

另一种查找正确关联 ID 的方法是搜索或查看您的日志，并找到您正在查看的日志条目的 `correlation_id` 值。

例如，假设您想了解在极狐GitLab 中重现操作时发生或中断的情况。您可以跟踪极狐GitLab 日志，过滤用户的请求，然后观察请求，直到看到您感兴趣的内容。

### 从 curl 获取相关 ID

如果您使用 `curl`，则可以使用详细选项来显示请求和响应标头，以及其他调试信息。

```shell
➜  ~ curl --verbose "https://gitlab.example.com/api/v4/projects"
# look for a line that looks like this
< x-request-id: 4rAMkV3gof4
```

#### 使用 jq

此示例使用 [jq](https://stedolan.github.io/jq/) 过滤结果并显示我们最可能关心的值。

```shell
sudo gitlab-ctl tail gitlab-rails/production_json.log | jq 'select(.username == "bob") | "User: \(.username), \(.method) \(.path), \(.controller)#\(.action), ID: \(.correlation_id)"'
```

```plaintext
"User: bob, GET /root/linux, ProjectsController#show, ID: U7k7fh6NpW3"
"User: bob, GET /root/linux/commits/master/signatures, Projects::CommitsController#signatures, ID: XPIHpctzEg1"
"User: bob, GET /root/linux/blob/master/README, Projects::BlobController#show, ID: LOt9hgi1TV4"
```

#### 使用 grep

此示例仅使用 `grep` 和 `tr`，它们比 `jq` 更容易安装。

```shell
sudo gitlab-ctl tail gitlab-rails/production_json.log | grep '"username":"bob"' | tr ',' '\n' | egrep 'method|path|correlation_id'
```

```plaintext
{"method":"GET"
"path":"/root/linux"
"username":"bob"
"correlation_id":"U7k7fh6NpW3"}
{"method":"GET"
"path":"/root/linux/commits/master/signatures"
"username":"bob"
"correlation_id":"XPIHpctzEg1"}
{"method":"GET"
"path":"/root/linux/blob/master/README"
"username":"bob"
"correlation_id":"LOt9hgi1TV4"}
```

## 在日志中搜索相关 ID

获得相关 ID 后，您可以开始搜索相关的日志条目。您可以按相关 ID 本身过滤行。
结合 `find` 和 `grep` 应该足以找到您要查找的条目。

```shell
# find <gitlab log directory> -type f -mtime -0 exec grep '<correlation ID>' '{}' '+'
find /var/log/gitlab -type f -mtime 0 -exec grep 'LOt9hgi1TV4' '{}' '+'
```

```plaintext
/var/log/gitlab/gitlab-workhorse/current:{"correlation_id":"LOt9hgi1TV4","duration_ms":2478,"host":"gitlab.domain.tld","level":"info","method":"GET","msg":"access","proto":"HTTP/1.1","referrer":"https://gitlab.domain.tld/root/linux","remote_addr":"68.0.116.160:0","remote_ip":"[filtered]","status":200,"system":"http","time":"2019-09-17T22:17:19Z","uri":"/root/linux/blob/master/README?format=json\u0026viewer=rich","user_agent":"Mozilla/5.0 (Mac) Gecko Firefox/69.0","written_bytes":1743}
/var/log/gitlab/gitaly/current:{"correlation_id":"LOt9hgi1TV4","grpc.code":"OK","grpc.meta.auth_version":"v2","grpc.meta.client_name":"gitlab-web","grpc.method":"FindCommits","grpc.request.deadline":"2019-09-17T22:17:47Z","grpc.request.fullMethod":"/gitaly.CommitService/FindCommits","grpc.request.glProjectPath":"root/linux","grpc.request.glRepository":"project-1","grpc.request.repoPath":"@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.git","grpc.request.repoStorage":"default","grpc.request.topLevelGroup":"@hashed","grpc.service":"gitaly.CommitService","grpc.start_time":"2019-09-17T22:17:17Z","grpc.time_ms":2319.161,"level":"info","msg":"finished streaming call with code OK","peer.address":"@","span.kind":"server","system":"grpc","time":"2019-09-17T22:17:19Z"}
/var/log/gitlab/gitlab-rails/production_json.log:{"method":"GET","path":"/root/linux/blob/master/README","format":"json","controller":"Projects::BlobController","action":"show","status":200,"duration":2448.77,"view":0.49,"db":21.63,"time":"2019-09-17T22:17:19.800Z","params":[{"key":"viewer","value":"rich"},{"key":"namespace_id","value":"root"},{"key":"project_id","value":"linux"},{"key":"id","value":"master/README"}],"remote_ip":"[filtered]","user_id":2,"username":"bob","ua":"Mozilla/5.0 (Mac) Gecko Firefox/69.0","queue_duration":3.38,"gitaly_calls":1,"gitaly_duration":0.77,"rugged_calls":4,"rugged_duration_ms":28.74,"correlation_id":"LOt9hgi1TV4"}
```

### 在分布式架构中搜索

如果您在极狐GitLab 基础架构中进行了一些水平扩展，那么您必须搜索您的所有极狐GitLab 节点。您可以使用某种日志聚合软件（如 Loki、ELK、Splunk 或其他）来完成此操作。

您可以使用像 Ansible 或 PSSH（并行 SSH）这样的工具，它们可以在您的服务器上并行执行相同的命令，或者制作您自己的解决方案。

### 在性能栏中查看请求

您可以使用[性能栏](../monitoring/performance/performance_bar.md)查看有趣的数据，包括对 SQL 和 Gitaly 的调用。

要查看数据，请求的相关 ID 必须与查看性能栏的用户匹配同一会话。对于 API 请求，这意味着您必须使用登录用户的会话 cookie 执行请求。

例如，如果要查看为以下 API 端点执行的数据库查询：

```shell
https://gitlab.com/api/v4/groups/2564205/projects?with_security_reports=true&page=1&per_page=1
```

首先，启用 **开发者工具** 面板。有关如何执行此操作的详细信息，请参阅[在浏览器中获取关联 ID](#getting-the-correlation-id-in-your-browser)。

启用开发者工具后，获取会话 cookie，如下所示：

1. 可选。 在 **开发者工具** 面板中选择 **Fetch/XHR** 请求过滤器。此步骤针对 Google Chrome 开发人员工具进行了描述，并非绝对必要，它只是让您更容易找到正确的请求。
1. 选择左侧的 `results?request_id=<some-request-id>` 请求。
1. 会话 cookie 显示在 `Headers` 面板的 `Request Headers` 部分下。右键单击 cookie 值并选择 `Copy value`。

![Obtaining a session cookie for request](img/obtaining-a-session-cookie-for-request_v14_3.png)

您已将会话 cookie 的值复制到剪贴板，例如：

```shell
experimentation_subject_id=<subject-id>; _gitlab_session=<session-id>; event_filter=all; visitor_id=<visitor-id>; perf_bar_enabled=true; sidebar_collapsed=true; diff_view=inline; sast_entry_point_dismissed=true; auto_devops_settings_dismissed=true; cf_clearance=<cf-clearance>; collapsed_gutter=false; frequently_used_emojis=clap,thumbsup,rofl,tada,eyes,bow
```

通过将会话 cookie 的值粘贴到 `curl` 请求的自定义 header 中来制作 API 请求：

```shell
$ curl --include "https://gitlab.com/api/v4/groups/2564205/projects?with_security_reports=true&page=1&per_page=1" \
--header 'cookie: experimentation_subject_id=<subject-id>; _gitlab_session=<session-id>; event_filter=all; visitor_id=<visitor-id>; perf_bar_enabled=true; sidebar_collapsed=true; diff_view=inline; sast_entry_point_dismissed=true; auto_devops_settings_dismissed=true; cf_clearance=<cf-clearance>; collapsed_gutter=false; frequently_used_emojis=clap,thumbsup,rofl,tada,eyes,bow'

  date: Tue, 28 Sep 2021 03:55:33 GMT
  content-type: application/json
  ...
  x-request-id: 01FGN8P881GF2E5J91JYA338Y3
  ...
  [
    {
      "id":27497069,
      "description":"Analyzer for images used on live K8S containers based on Starboard"
    },
    "container_registry_image_prefix":"registry.gitlab.com/gitlab-org/security-products/analyzers/cluster-image-scanning",
    "..."
  ]
```

响应包含来自 API 端点的数据，以及在 `x-request-id` header 中返回的 `correlation_id` 值，如[识别请求的相关 ID](#identify-the-correlation-id-for-a-request) 部分。

然后，您可以查看此请求的数据库详细信息：

1. 将 `x-request-id` 值粘贴到[性能栏](../monitoring/performance/performance_bar.md)的 `request details` 字段中，然后按 <kbd>Enter/Return</kbd>。此示例使用上述响应返回的 `x-request-id` 值 `01FGN8P881GF2E5J91JYA338Y3`：

   ![Paste request ID into progress bar](img/paste-request-id-into-progress-bar_v14_3.png)

1. 新请求将插入到性能栏右侧的 `Request Selector`  下拉列表中。选择新请求以查看 API 请求的指标：

   ![Select request ID from request selector drop down menu](img/select-request-id-from-request-selector-drop-down-menu_v14_3.png)

   
1. 选择进度条中的 `pg` 链接，查看 API 请求执行的数据库查询：

   ![View pg database details](img/view-pg-details_v14_3.png)

   显示数据库查询对话框：

   ![Database query dialog](img/database-query-dialog_v14_3.png)
