---
stage: Manage
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
description: 'Learn how to create evidence artifacts typically requested by a 3rd party auditor.'
---

# 审计报告 **(FREE)**

极狐GitLab 可以通过生成综合报告来帮助所有者和管理员对审计员做出回应。这些审计报告的范围各不相同，具体取决于需要。

## 用例

- 生成审计事件报告，提供给外部审计员，要求提供某些日志记录功能的证明。
- 提供所有用户的报告，显示他们的群组和项目成员资格，以便进行季度访问审查，审计员可以验证是否符合组织的访问管理策略。

<!--
## APIs

- [Audit events](../api/audit_events.md)
- [GraphQL - User](../api/graphql/reference/index.md#user)
- [GraphQL - GroupMember](../api/graphql/reference/index.md#groupmember)
- [GraphQL - ProjectMember](../api/graphql/reference/index.md#projectmember)
-->

## 功能

- [审计事件](audit_events.md)

<!--
- [Log system](logs.md)
-->
