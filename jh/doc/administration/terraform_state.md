---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Terraform 状态管理 **(FREE)**

极狐GitLab 可以用作 [Terraform](../user/infrastructure/index.md) 状态文件的后端。文件在存储之前被加密。默认情况下启用此功能。

这些文件的存储位置默认为：

- Linux 软件包安装实例：`/var/opt/gitlab/gitlab-rails/shared/terraform_state`。
- 源安装实例：`/home/git/gitlab/shared/terraform_state`。

这些位置可以使用下面描述的选项进行配置。

使用[外部对象存储](https://docs.gitlab.cn/charts/advanced/external-object-storage/#lfs-artifacts-uploads-packages-external-diffs-terraform-state-dependency-proxy)配置[极狐GitLab Helm charts](https://docs.gitlab.cn/charts/) 安装实例。

## 禁用 Terraform 状态

您可以在整个实例中禁用 Terraform 状态。您可能想要减少磁盘空间，或者因为您的实例不使用 Terraform，而禁用 Terraform。

当禁用 Terraform 状态管理时：

- 在左侧边栏中，您不能选择 **运维 > Terraform 状态**。
- 任何访问 Terraform 状态的 CI/CD 作业都失败并出现以下错误：

    ```shell
    Error refreshing state: HTTP remote state endpoint invalid auth
    ```

要禁用 Terraform 管理，请根据您的安装实例执行以下步骤。

先决条件：

- 您必须是管理员。

**Linux 软件包安装：**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['terraform_state_enabled'] = false
   ```

1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#reconfigure-a-linux-package-installation)，使更改生效。

**源安装：**

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   terraform_state:
     enabled: false
   ```

1. 保存文件并[重启极狐GitLab](restart_gitlab.md#installations-from-source)，使更改生效。

## 使用本地存储

默认配置使用本地存储。要更改 Terraform 状态文件在本地存储的位置，请按照以下步骤操作。

**Linux 软件包安装：**

1. 要将存储路径更改为例如 `/mnt/storage/terraform_state`，编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['terraform_state_storage_path'] = "/mnt/storage/terraform_state"
   ```

1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#reconfigure-a-linux-package-installation)，使更改生效。

**源安装：**

1. 要将存储路径更改为 `/mnt/storage/terraform_state`，请编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   terraform_state:
     enabled: true
     storage_path: /mnt/storage/terraform_state
   ```

1. 保存文件并[重启极狐GitLab](restart_gitlab.md#installations-from-source)，使更改生效。

<a id="using-object-storage"></a>

## 使用对象存储 **(FREE SELF)**

我们建议使用支持的对象存储选项之一<!--[支持的对象存储选项之一](object_storage.md#options)-->，而不是将 Terraform 状态文件存储在磁盘上。
此配置依赖于已配置的有效凭据。

<!--
[阅读有关在极狐GitLab 中使用对象存储的更多信息](object_storage.md)。
-->

### 对象存储设置

以下设置是：

- 源安装实例：嵌套在 `terraform_state:` 和 `object_store:` 下。
- Linux 软件包安装实例：以 `terraform_state_object_store_` 为前缀。

| 设置 | 描述 | 默认值 |
|---------|-------------|---------|
| `enabled` | 启用/禁用对象存储 | `false` |
| `remote_directory` | 存储 Terraform 状态文件的存储桶名称 | |
| `connection` | 下面描述的各种连接选项 | |

<a id="migrate-to-object-storage"></a>

### 迁移到对象存储

> 引入于 13.9 版本。

WARNING:
无法将 Terraform 状态文件从对象存储迁移回本地存储，因此请谨慎操作。

要将 Terraform 状态文件迁移到对象存储，请按照以下说明进行操作。

- 对于 Linux 软件包安装实例：

  ```shell
  gitlab-rake gitlab:terraform_states:migrate
  ```

- 对于源安装实例：

  ```shell
  sudo -u git -H bundle exec rake gitlab:terraform_states:migrate RAILS_ENV=production
  ```

对于 13.8 及更早版本，您可以使用 Rake 任务解决：

1. 打开 [Rails 控制台](operations/rails_console.md)。
1. 运行以下命令：

   ```ruby
   Terraform::StateUploader.alias_method(:upload, :model)

   Terraform::StateVersion.where(file_store: ::ObjectStorage::Store::LOCAL).   find_each(batch_size: 10) do |terraform_state_version|
     puts "Migrating: #{terraform_state_version.inspect}"

     terraform_state_version.file.migrate!(::ObjectStorage::Store::REMOTE)
   end
   ```

您可以选择跟踪进度并使用 [PostgreSQL 控制台](https://docs.gitlab.cn/omnibus/settings/database.html#connecting-to-the-bundled-postgresql-database)验证所有包是否已成功迁移：

- `sudo gitlab-rails dbconsole` 用于 Linux 软件包安装实例（14.1 及更早版本）。
- `sudo gitlab-rails dbconsole --database main` 用于 Linux 软件包安装实例（14.2 及更高版本）。
- `sudo -u git -H psql -d gitlabhq_production` 用于源安装实例。

验证下面的 `objectstg`（其中 `file_store=2`）具有所有状态的计数：

```shell
gitlabhq_production=# SELECT count(*) AS total, sum(case when file_store = '1' then 1 else 0 end) AS filesystem, sum(case when file_store = '2' then 1 else 0 end) AS objectstg FROM terraform_state_versions;

total | filesystem | objectstg
------+------------+-----------
   15 |          0 |      15
```

验证 `terraform_state` 文件夹中的磁盘上没有文件：

```shell
sudo find /var/opt/gitlab/gitlab-rails/shared/terraform_state -type f | grep -v tmp | wc -l
```

### S3 兼容的连接设置

<!--
请参阅[不同提供商的可用连接设置](object_storage.md#connection-settings)。
-->

**Omnibus 安装：**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行；替换为您想要的值：

   ```ruby
   gitlab_rails['terraform_state_object_store_enabled'] = true
   gitlab_rails['terraform_state_object_store_remote_directory'] = "terraform"
   gitlab_rails['terraform_state_object_store_connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-central-1',
     'aws_access_key_id' => 'AWS_ACCESS_KEY_ID',
     'aws_secret_access_key' => 'AWS_SECRET_ACCESS_KEY'
   }
   ```

   NOTE:
   如果您使用 AWS IAM 配置文件，请务必省略 AWS 访问密钥和秘密访问密钥/值对。

   ```ruby
   gitlab_rails['terraform_state_object_store_connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-central-1',
     'use_iam_profile' => true
   }
   ```

1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#reconfigure-a-linux-package-installation)，使更改生效。
1. [将任何现有的本地状态迁移到对象存储](#migrate-to-object-storage)。

**源安装：**

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   terraform_state:
     enabled: true
     object_store:
       enabled: true
       remote_directory: "terraform" # The bucket name
       connection:
         provider: AWS # Only AWS supported at the moment
         aws_access_key_id: AWS_ACCESS_KEY_ID
         aws_secret_access_key: AWS_SECRET_ACCESS_KEY
         region: eu-central-1
   ```

1. 保存文件并[重新启动极狐GitLab](restart_gitlab.md#installations-from-source)，使更改生效。
1. [将任何现有的本地状态迁移到对象存储](#migrate-to-object-storage)。
