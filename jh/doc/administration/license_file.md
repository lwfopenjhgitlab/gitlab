---
stage: Fulfillment
group: Provision
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

<!-- To promote the workflow described in license.md, this page is not included in global left nav. -->

<a id="activate-gitlab-ee-with-a-license-file-or-key"></a>

# 使用许可证文件或密钥激活极狐GitLab

如果您从极狐GitLab 收到许可证文件（例如，试用版），您可以将其上传到您的实例或在安装期间添加。许可证文件是一个 base64 编码的 ASCII 文本文件，扩展名为 `.gitlab-license`。

首次登录极狐GitLab 实例时，应显示带有 **添加许可证** 页面链接的注释。

否则，要添加您的许可证：

1. 以管理员身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 在 **添加许可证** 区域，通过上传文件或输入密钥来添加许可证。
1. 选择 **服务条款** 复选框。
1. 选择 **添加许可证**。

NOTE:
对于 14.1.x 到 14.10.x，您可以直接从 URL `<YourGitLabURL>/admin/license/new` 访问 **添加许可证** 页面。

## 在安装过程中添加您的许可证文件

您可以在安装极狐GitLab 时导入许可证文件。

- **源安装**
    - 将 `Gitlab.gitlab-license` 文件放在 `config/` 目录下。
    - 要为许可证指定自定义位置和文件名，请使用文件路径设置 `GITLAB_LICENSE_FILE` 环境变量：

      ```shell
      export GITLAB_LICENSE_FILE="/path/to/license/file"
      ```

- **Omnibus 安装**
    - 将 `Gitlab.gitlab-license` 文件放在 `/etc/gitlab/` 目录下。
    - 要为许可证指定自定义位置和文件名，请将此条目添加到 `gitlab.rb`：

      ```ruby
      gitlab_rails['initial_license_file'] = "/path/to/license/file"
      ```

WARNING:
以上方法仅在安装时添加许可证。要续订或升级许可证，请在 Web 用户界面的 **管理中心** 中添加许可证。

<!--
## Submit license usage data

If you use a license file or key to activate your instance in an offline environment, you must submit your license
usage data monthly.
To submit the data, [export your license usage](../../subscriptions/self_managed/index.md#export-your-license-usage)
and send it by email to the renewals service, `renewals-service@customers.gitlab.com`.

If you don't submit your data each month after your subscription start date, an email is sent to the address
associated with your subscription and a banner displays to remind you to submit your data. The banner displays
in the **Admin Area** on the **Dashboard** and on the **Subscription** pages. You can only dismiss it until the
following month after you submit your license usage data.
-->

<a id="what-happens-when-your-license-expires"></a>

## 许可证到期时会发生什么

在许可证到期前 15 天，系统会显示一个带有即将到期日期的通知给管理员。

当您的许可证到期时，系统会锁定功能，例如 Git 推送和议题创建。您的实例变为只读，并且所有管理员都会看到一条过期消息。在此之前，您有 14 天的宽限期。

要恢复功能，请续订您的订阅。

如果许可证已过期 30 天以上，您必须购买新订阅才能恢复功能。

要返回免费版功能，请[删除所有过期的许可证](#remove-a-license)。

<a id="remove-a-license"></a>

## 删除许可证

要从私有化部署实例中删除许可证文件：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **订阅**。
1. 选择 **删除许可证**。

重复这些步骤删除所有许可证，包括过去应用的许可证。

<a id="view-license-details-and-history"></a>

## 查看许可证详情和历史

要查看您的许可证详细信息：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **订阅**。

您可以上传和查看多个许可证，但只有当前日期范围内的最新许可证是有效许可证。

当您上传未来日期的许可证时，在其适用日期之前不会生效。
您可以在 **订阅历史** 表中查看所有有效订阅。

您还可以将您的许可证使用信息导出到 CSV 文件。

NOTE:
在 13.6 及更早版本中，当您上传新许可证时，可能会继续显示有关过期许可证的通知。当新许可证的开始日期在未来并且即将到期的许可证仍然有效时，就会发生这种情况。新许可证激活后通知消失。

## Rails 控制台中的许可证命令

以下命令可以在 [rails 控制台](../../administration/operations/rails_console.md#starting-a-rails-console-session)中运行。

WARNING:
如果运行不正确或在正确的条件下，任何直接更改数据的命令都可能造成破坏。
我们强烈建议在测试环境中运行它们，并准备好要恢复的实例备份，以防万一。

### 查看当前许可证信息

```ruby
# License information (name, company, email address)
License.current.licensee

# Plan:
License.current.plan

# Uploaded:
License.current.created_at

# Started:
License.current.starts_at

# Expires at:
License.current.expires_at

# Is this a trial license?
License.current.trial?

# License ID for lookup on CustomersDot
License.current.license_id

# License data in Base64-encoded ASCII format
License.current.data
```

#### 与未来开始的许可证交互

```ruby
# Future license data follows the same format as current license data it just uses a different modifier for the License prefix
License.future_dated
```

### 检查项目功能是否在实例上可用

<https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/app/models/license.rb> 中列出的功能。

```ruby
License.current.feature_available?(:jira_dev_panel_integration)
```

#### 检查项目功能是否在项目上可用

[`license.rb`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/app/models/license.rb) 中列出的功能。

```ruby
p = Project.find_by_full_path('<group>/<project>')
p.feature_available?(:jira_dev_panel_integration)
```

### 通过控制台添加许可证

#### 使用 `key` 变量

```ruby
key = "<key>"
license = License.new(data: key)
license.save
License.current # check to make sure it applied
```

#### 使用许可证文件

```ruby
license_file = File.open("/tmp/Gitlab.license")

key = license_file.read.gsub("\r\n", "\n").gsub(/\n+$/, '') + "\n"

license = License.new(data: key)
license.save
License.current # check to make sure it applied
```

这些代码片段可以保存到文件中并[使用 Rails Runner](operations/rails_console.md#using-the-rails-runner) 执行，以便可以通过 shell 自动化脚本应用许可证。

在许可证过期和多个 LDAP 服务器等已知边缘情况下需要这样做。

### 删除许可证

清理[许可证历史表](../administration/license_file.md#view-license-details-and-history)：

```ruby
TYPE = :trial?
# or :expired?

License.select(&TYPE).each(&:destroy!)

# or even License.all.each(&:destroy!)
```

## 故障排查

### 管理中心没有许可证选项

您无法上传您的许可证，因为没有 **订阅** 区域。在以下情况下，可能会出现此问题：

- 如果您原先安装的是社区版（Community Edition）而不是极狐GitLab，您必须在上传许可证前升级到极狐GitLab。
- 如果您使用的是 SaaS 版，您无法上传一个私有化部署版的许可证。

### 续订时用户数超过许可证限制

极狐GitLab 会显示一条消息，提示您购买更多用户。如果您上传的许可证没有足够的用户来覆盖您实例中的用户数量，则会出现此问题。

要解决此问题，请购买额外的席位覆盖这些用户。
<!--For more information, read the [licensing FAQ](https://about.gitlab.com/pricing/licensing-faq/).-->

在 14.2 及更高版本中，对于使用许可证文件的实例，以下规则适用：

- 如果超过许可的用户少于或等于许可文件中用户的 10%，则应用许可并在下次续订时支付超额。
- 如果超过许可的用户超过许可文件中用户的 10%，则在不购买更多用户的情况下无法申请许可。

例如，如果您为 100 个用户购买了一个许可证，那么当您激活您的许可证时，您可以拥有 110 个用户。但是，如果您有 111 个用户，则必须购买更多用户才能激活许可证。

### 添加许可证后仍显示 `Start GitLab Ultimate trial`

要解决此问题，请重新启动 [Puma 或您的整个极狐GitLab 实例](../administration/restart_gitlab.md)。
