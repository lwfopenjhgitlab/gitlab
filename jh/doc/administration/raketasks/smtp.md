---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# SMTP Rake 任务 **(FREE SELF)**

> 在 14.2 版本中引入。

以下是与 SMTP 相关的 Rake 任务。

## Secrets

GitLab 可以使用 SMTP 配置 secret 从加密文件中读取。以下 Rake 任务用于更新加密文件的内容。

### 显示 secret

显示当前 SMTP secret 的内容。

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:smtp:secret:show
```

**源安装实例**

```shell
bundle exec rake gitlab:smtp:secret:show RAILS_ENV=production
```

**示例输出：**

```plaintext
password: '123'
user_name: 'gitlab-inst'
```

### 编辑 secret

在编辑器中打开 secret 内容，并在退出时将生成的内容写入加密的 secret 文件。

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:smtp:secret:edit EDITOR=vim
```

**源安装实例**

```shell
bundle exec rake gitlab:smtp:secret:edit RAILS_ENV=production EDITOR=vim
```

### 写入 raw secret

通过在 `STDIN` 上提供新的 secret 来写入新的 secret。

**Omnibus 安装实例**

```shell
echo -e "password: '123'" | sudo gitlab-rake gitlab:smtp:secret:write
```

**源安装实例**

```shell
echo -e "password: '123'" | bundle exec rake gitlab:smtp:secret:write RAILS_ENV=production
```

### Secrets 示例

**编辑器示例**

在 edit 命令不适用于您的编辑器的情况下，可以使用 write 任务：

```shell
# Write the existing secret to a plaintext file
sudo gitlab-rake gitlab:smtp:secret:show > smtp.yaml
# Edit the smtp file in your editor
...
# Re-encrypt the file
cat smtp.yaml | sudo gitlab-rake gitlab:smtp:secret:write
# Remove the plaintext file
rm smtp.yaml
```

**KMS 集成示例**

它还可以用作接收使用 KMS 加密的内容的应用程序：

```shell
gcloud kms decrypt --key my-key --keyring my-test-kms --plaintext-file=- --ciphertext-file=my-file --location=us-west1 | sudo gitlab-rake gitlab:smtp:secret:write
```

**Google Cloud secret 集成示例**

它还可以用作接收来自 Google Cloud 的 secret 的应用程序：

```shell
gcloud secrets versions access latest --secret="my-test-secret" > $1 | sudo gitlab-rake gitlab:smtp:secret:write
```
