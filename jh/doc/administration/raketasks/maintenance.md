---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 维护 Rake 任务 **(FREE SELF)**

GitLab 提供用于一般维护的 Rake 任务。

## 收集系统信息

此命令收集有关您的 GitLab 安装及其运行的系统的信息。这些在寻求帮助或报告问题时可能很有用。

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:env:info
```

**源安装实例**

```shell
bundle exec rake gitlab:env:info RAILS_ENV=production
```

示例输出：

```plaintext
System information
System:         Ubuntu 20.04
Proxy:          no
Current User:   git
Using RVM:      no
Ruby Version:   2.6.6p146
Gem Version:    2.7.10
Bundler Version:1.17.3
Rake Version:   12.3.3
Redis Version:  5.0.9
Git Version:    2.27.0
Sidekiq Version:5.2.9
Go Version:     unknown

GitLab information
Version:        13.2.2-ee
Revision:       618883a1f9d
Directory:      /opt/gitlab/embedded/service/gitlab-rails
DB Adapter:     PostgreSQL
DB Version:     11.7
URL:            http://gitlab.example.com
HTTP Clone URL: http://gitlab.example.com/some-group/some-project.git
SSH Clone URL:  git@gitlab.example.com:some-group/some-project.git
Elasticsearch:  no
Geo:            no
Using LDAP:     no
Using Omniauth: yes
Omniauth Providers:

GitLab Shell
Version:    13.3.0
Repository storage paths:
- default:  /var/opt/gitlab/git-data/repositories
GitLab Shell path:      /opt/gitlab/embedded/service/gitlab-shell
```

## 显示许可证信息 **(PREMIUM SELF)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/20501) in GitLab 12.6.
-->
> - 13.9 版本中移入专业版。

此命令显示有关您的[许可证](../../user/admin_area/license.md)以及使用的席位数量的信息。适用于极狐GitLab 安装实例。

这些在向支持部门提供证明或以编程方式检查许可证参数时可能很有用。

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:license:info
```

**源安装实例**

```shell
bundle exec rake gitlab:license:info RAILS_ENV=production
```

示例输出：

```plaintext
Today's Date: 2020-02-29
Current User Count: 30
Max Historical Count: 30
Max Users in License: 40
License valid from: 2019-11-29 to 2020-11-28
Email associated with license: user@example.com
```

<a id="check-gitlab-configuration"></a>

## 检查配置

`gitlab:check` Rake 任务运行以下 Rake 任务：

- `gitlab:gitlab_shell:check`
- `gitlab:gitaly:check`
- `gitlab:sidekiq:check`
- `gitlab:incoming_email:check`
- `gitlab:ldap:check`
- `gitlab:app:check`

它检查每个组件是否根据安装指南进行设置，并针对发现的问题提出修复建议。此命令必须从您的应用程序服务器运行，并且在 Gitaly<!--[Gitaly](../gitaly/configure_gitaly.md#run-gitaly-on-its-own-server)--> 等组件服务器上无法正常运行。
<!--If you're running Geo, see also the [Geo Health check Rake task](../geo/replication/troubleshooting.md#health-check-rake-task).-->

<!--
您还可以查看我们的故障排查指南：

- [GitLab](../index.md#故障排查)
- [Omnibus GitLab](https://docs.gitlab.cn/omnibus/index.html#故障排查)
-->

此外，您还应该[验证数据库值可以使用当前 secrets 解密](check.md#验证数据库值是否可以使用当前-secrets-进行解密)。

要运行 `gitlab:check`，运行以下命令：

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:check
```

**源安装实例**

```shell
bundle exec rake gitlab:check RAILS_ENV=production
```

如果要从输出中省略项目名称，请对 `gitlab:check` 使用 `SANITIZE=true`。

示例输出：

```plaintext
Checking Environment ...

Git configured for git user? ... yes
Has python2? ... yes
python2 is supported version? ... yes

Checking Environment ... Finished

Checking GitLab Shell ...

GitLab Shell version? ... OK (1.2.0)
Repo base directory exists? ... yes
Repo base directory is a symlink? ... no
Repo base owned by git:git? ... yes
Repo base access is drwxrws---? ... yes
post-receive hook up-to-date? ... yes
post-receive hooks in repos are links: ... yes

Checking GitLab Shell ... Finished

Checking Sidekiq ...

Running? ... yes

Checking Sidekiq ... Finished

Checking GitLab ...

Database config exists? ... yes
Database is SQLite ... no
All migrations up? ... yes
GitLab config exists? ... yes
GitLab config outdated? ... no
Log directory writable? ... yes
Tmp directory writable? ... yes
Init script exists? ... yes
Init script up-to-date? ... yes
Redis version >= 2.0.0? ... yes

Checking GitLab ... Finished
```

<a id="rebuild-authorized_keys-file"></a>

## 重建 `authorized_keys` 文件

在某些情况下，有必要重建 `authorized_keys` 文件，例如，如果在升级后您在[通过 SSH](../../user/ssh.md) 推送时收到 `Permission denied (publickey)`，和在 `gitlab-shell.log` 文件中查找 `404 Key Not Found` 错误。
要重建 `authorized_keys`，请运行：

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:shell:setup
```

**源安装实例**

```shell
cd /home/git/gitlab
sudo -u git -H bundle exec rake gitlab:shell:setup RAILS_ENV=production
```

示例输出：

```plaintext
This will rebuild an authorized_keys file.
You will lose any data stored in authorized_keys file.
Do you want to continue (yes/no)? yes
```

## 清除 Redis 缓存

如果由于某种原因仪表板显示错误信息，您可能需要清除 Redis 的缓存。 为此，请运行：

**Omnibus 安装实例**

```shell
sudo gitlab-rake cache:clear
```

**源安装实例**

```shell
cd /home/git/gitlab
sudo -u git -H bundle exec rake cache:clear RAILS_ENV=production
```

## 预编译 assets

有时在版本升级期间，您可能最终会出现一些错误的 CSS 或丢失一些图标。在这种情况下，请尝试再次预编译 assets。

这仅适用于源安装，不适用于 Omnibus 包。

**源安装实例**

```shell
cd /home/git/gitlab
sudo -u git -H bundle exec rake gitlab:assets:compile RAILS_ENV=production
```

对于 Omnibus 版本，未优化的 assets（JavaScript、CSS）在上游 GitLab 发布时被冻结。Omnibus 版本包括这些 assets 的优化版本。除非您在安装软件包后在生产机器上修改 JavaScript / CSS 代码，否则应该没有理由在生产机器上重做 `rake gitlab:assets:compile`。如果您怀疑 assets 已损坏，则应重新安装 Omnibus 包。

## 检查与远程站点的 TCP 连接

有时您需要知道您的 GitLab 安装是否可以连接到另一台机器（例如 PostgreSQL 或 Web 服务器）上的 TCP 服务，以便对代理问题进行故障排除。包含一个 Rake 任务来帮助您完成此任务。

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:tcp_check[example.com,80]
```

**源安装实例**

```shell
cd /home/git/gitlab
sudo -u git -H bundle exec rake gitlab:tcp_check[example.com,80] RAILS_ENV=production
```

## 清除 exclusive lease (危险)

极狐GitLab 使用共享锁机制 `ExclusiveLease` 来防止共享资源中的同时操作。一个例子是在仓库上运行定期垃圾收集。

在非常特殊的情况下，被 Exclusive Lease 锁定的操作可能会在不释放锁的情况下失败。如果您等不及它过期，您可以运行此任务手动清除它。

要清楚所有 exclusive leases：

WARNING:
不要在 GitLab 或 Sidekiq 运行时运行命令

```shell
sudo gitlab-rake gitlab:exclusive_lease:clear
```

要指定 lease `type` 或 lease `type + id`，请指定范围：

```shell
# to clear all leases for repository garbage collection:
sudo gitlab-rake gitlab:exclusive_lease:clear[project_housekeeping:*]

# to clear a lease for repository garbage collection in a specific project: (id=4)
sudo gitlab-rake gitlab:exclusive_lease:clear[project_housekeeping:4]
```

## 显示数据库迁移的状态
<!--
请参阅 [升级文档](../../update/index.md#checking-for-background-migrations-before-upgrading) 了解如何在升级 GitLab 时检查迁移是否完成。
-->

要检查特定迁移的状态，您可以使用以下 Rake 任务：

```shell
sudo gitlab-rake db:migrate:status
```

要检查 [Geo 次要站点上的跟踪数据库](../geo/setup/external_database.md#configure-the-tracking-database)，您可以使用以下 Rake 任务：

```shell
sudo gitlab-rake db:migrate:status:geo
```

这会为每个迁移 ID 输出一个 `Status` 为 `up` 或 `down` 的表。

```shell
database: gitlabhq_production

 Status   Migration ID    Migration Name
--------------------------------------------------
   up     migration_id    migration_name
```

<a id="run-incomplete-database-migrations"></a>

## 运行不完整的数据库迁移

数据库迁移可能会停留在不完整的状态，在 `sudo gitlab-rake db:migrate:status` 命令的输出中会出现 `down` 状态。

1. 要完成这些迁移，请使用以下 Rake 任务：

   ```shell
   sudo gitlab-rake db:migrate
   ```

1. 命令完成后，运行 `sudo gitlab-rake db:migrate:status` 检查所有迁移是否已完成（具有 `up` 状态）。

1. 热重载 `puma` 和 `sidekiq` 服务：

   ```shell
   sudo gitlab-ctl hup puma
   sudo gitlab-ctl restart sidekiq
   ```

## 重建数据库索引

WARNING:
这是一项实验性功能，默认情况下未启用。 它需要 PostgreSQL 12 或更高版本。

可以定期重建数据库索引以回收空间并随着时间的推移保持索引膨胀的健康水平。

为了重建估计膨胀率最高的两个索引，请使用以下 Rake 任务：

```shell
sudo gitlab-rake gitlab:db:reindex
```

为了针对特定索引，请使用以下 Rake 任务：

```shell
sudo gitlab-rake gitlab:db:reindex['public.a_specific_index']
```

不支持以下索引类型：

1. 用于约束排除的索引
1. 分区索引
1. 表达索引

或者，此 Rake 任务将注释发送到 Grafana（4.6 或更高版本）端点。 使用以下自定义环境变量以启用注释：

1. `GRAFANA_API_URL` - Grafana 的基本 URL，例如 `http://some-host:3000`。
1. `GRAFANA_API_KEY` - 至少具有 `Editor role` 的 Grafana API 密钥。

您还可以[启用重新索引作为常规 Cron 作业](https://docs.gitlab.cn/omnibus/settings/database.html#数据库自动重新索引)。

## 导入常用指标

有时，您可能需要重新导入为 Metrics 仪表板提供支持的常用指标。

这可能是更新现有指标<!--[更新现有指标](../../development/prometheus_metrics.md#update-existing-metrics)-->的结果，也可能是故障排查措施<!--[故障排查措施](../../operations/metrics/dashboards/index.md#troubleshooting)-->。

要重新导入您可以运行的指标：

```shell
sudo gitlab-rake metrics:setup_common_metrics
```
