---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# GitHub 导入 Rake 任务 **(FREE SELF)**

> Rake 任务不再自动创建不存在的命名空间或群组，引入于 15.9 版本。

要检索和导入 GitHub 仓库，您需要一个 [GitHub 个人访问令牌](https://github.com/settings/tokens)。用户名应该作为第二个参数传递给 Rake 任务，它成为项目的所有者。您可以使用相同的命令恢复导入。

请记住，语法非常具体。删除参数块内和括号之前/之后的任何空格。此外，一些 shell（例如，Zsh）可以单独解释开/关括号（`[]`）。您可能需要转义括号或使用双引号。

先决条件：

- 至少具有要导入目标群组的维护者角色。使用开发者角色废弃于 15.8 版本，并将在 16.0 版本中删除。

## 注意事项

如果在导入时达到 GitHub [速率限制](https://docs.github.com/en/rest/rate-limit)，则导入过程将等待 (`sleep()`)，直到可以继续导入。

## 导入多个项目

要从可用的 GitHub 项目列表中导入项目：

```shell
# Omnibus installations
sudo gitlab-rake "import:github[access_token,root,foo/bar]"

# Installations from source
bundle exec rake "import:github[access_token,root,foo/bar]" RAILS_ENV=production
```

在这种情况下，`access_token` 是 GitHub 个人访问令牌，`root` 是 GitLab 用户名，`foo/bar` 是从您的 GitHub 项目创建的新 GitLab 命名空间/项目。子组也是可能的：`foo/foo/bar`。如果它们不存在，导入器会创建任何缺失的中间命名空间（群组）。

## 导入单个项目

要导入特定的 GitHub 项目（此处名为 `foo/github_repo`）：

```shell
# Omnibus installations
sudo gitlab-rake "import:github[access_token,root,foo/bar,foo/github_repo]"

# Installations from source
bundle exec rake "import:github[access_token,root,foo/bar,foo/github_repo]" RAILS_ENV=production
```
