---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 项目导入导出管理 **(FREE SELF)**

极狐GitLab 提供与项目导入和导出相关的 Rake 任务。有关更多信息，请参阅：

- [项目导入/导出文档](../../user/project/settings/import_export.md)。
- [项目导入/导出 API](../../api/project_import_export.md)。

<!--
- [Developer documentation: project import/export](../../development/import_export.md)
-->

## 项目导入状态

您可以通过[项目导入/导出 API](../../api/project_import_export.md#import-status) 查询导入。
如 API 文档中所述，查询可能会返回导入错误或异常。

## 导入/导出 rake 任务

可以使用以下命令检查极狐GitLab 导入/导出版本：

```shell
# Omnibus installations
sudo gitlab-rake gitlab:import_export:version

# Installations from source
bundle exec rake gitlab:import_export:version RAILS_ENV=production
```

可以使用以下命令列出要导出的数据库表的当前列表：

```shell
# Omnibus installations
sudo gitlab-rake gitlab:import_export:data

# Installations from source
bundle exec rake gitlab:import_export:data RAILS_ENV=production
```

注意以下事项：

- 仅当导入和导出极狐GitLab 实例的版本如[版本历史记录](../../user/project/settings/import_export.md#compatibility)中所述兼容时，才能进行导入。
- 必须启用项目导入选项：

   1. 在顶部栏上，选择 **主菜单 > 管理员**。
   1. 在左侧边栏中，选择 **设置 > 通用**。
   1. 展开 **可见性和访问控制**。
   1. 在 **导入源** 下，选中 **项目导出启用** 选项。
   1. 选择 **保存更改**。

- 导出存储在临时目录中，每 24 小时由特定 worker 删除。

### 使用 Rake 任务导入大型项目

我们可以简单地从终端运行这个脚本：

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `username`      | string | yes | 用户名 |
| `namespace_path` | string | yes | 命名空间路径 |
| `project_path` | string | yes | 项目名 |
| `archive_path` | string | yes | 要导入的导出项目 tarball 的路径 |

```shell
bundle exec rake "gitlab:import_export:import[root, group/subgroup, testingprojectimport, /path/to/file.tar.gz]"
```

如果您正在运行 Omnibus，请运行以下 Rake 任务：

```shell
gitlab-rake "gitlab:import_export:import[root, group/subgroup, testingprojectimport, /path/to/file.tar.gz]"
```

### 使用 Rake 任务导出

您可以使用 Rake 任务导出大型项目。

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `username`      | string | yes | 用户名 |
| `namespace_path` | string | yes | 命名空间路径 |
| `project_path` | string | yes | 项目名 |
| `archive_path` | string | yes | 用于存储导出项目 tarball 的文件路径 |

```shell
gitlab-rake "gitlab:import_export:export[username, namespace_path, project_path, archive_path]"
```

## 故障排除

如果您在导入/导出时遇到问题，您可以使用相同的 Rake 任务启用 debug 模式：

```shell
# Import
IMPORT_DEBUG=true gitlab-rake "gitlab:import_export:import[root, group/subgroup, testingprojectimport, /path/to/file_to_import.tar.gz]"

# Export
EXPORT_DEBUG=true gitlab-rake "gitlab:import_export:export[root, group/subgroup, projectnametoexport, /tmp/export_file.tar.gz]"
```
