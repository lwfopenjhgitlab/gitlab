---
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# LDAP Rake 任务 **(FREE SELF)**

以下是与 LDAP 相关的 Rake 任务。

## 检查

LDAP 检查 Rake 任务测试 `bind_dn` 和 `password` 凭据（如果已配置）并列出 LDAP 用户示例。此任务也作为 `gitlab:check` 任务的一部分执行，但可以使用以下命令独立运行。

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:ldap:check
```

**源安装实例**

```shell
sudo -u git -H bundle exec rake gitlab:ldap:check RAILS_ENV=production
```

默认情况下，该任务返回 100 个 LDAP 用户的样本。通过将数字传递给检查任务来更改此限制：

```shell
rake gitlab:ldap:check[50]
```

## 运行群组同步 **(PREMIUM SELF)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/14735) in GitLab 12.2.
-->

以下任务立即运行群组同步<!--[群组同步](../auth/ldap/index.md#群组同步)-->。当您希望根据 LDAP 更新所有配置的组成员身份，而不等待下一次计划的组同步运行时，这很有价值。

NOTE:
如果您想更改执行群组同步的频率，请改为调整 cron 计划<!--[调整 cron 计划](../auth/ldap/index.md#调整-ldap-群组同步计划)-->。

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:ldap:group_sync
```

**源安装实例**

```shell
bundle exec rake gitlab:ldap:group_sync
```

## 重命名提供商

如果您在 `gitlab.yml` 或 `gitlab.rb` 中更改 LDAP 服务器 ID，则需要更新所有用户身份或用户无法登录。输入旧的和新的提供者，此任务将在数据库中更新所有匹配的身份。

`old_provider` 和 `new_provider` 源自前缀 `ldap` 加上配置文件中的 LDAP 服务器 ID。例如，在 `gitlab.yml` 或 `gitlab.rb` 中，您可能会看到这样的 LDAP 配置：

```yaml
main:
  label: 'LDAP'
  host: '_your_ldap_server'
  port: 389
  uid: 'sAMAccountName'
  ...
```

`main` 是 LDAP 服务器 ID。总之，唯一的提供商是`ldapmain`。

WARNING:
如果您输入了错误的新提供商，用户将无法登录。如果发生这种情况，请使用错误的提供商作为 `old_provider` 和正确的提供商作为 `new_provider` 再次运行任务。

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:ldap:rename_provider[old_provider,new_provider]
```

**源安装实例**

```shell
bundle exec rake gitlab:ldap:rename_provider[old_provider,new_provider] RAILS_ENV=production
```

### 示例

考虑从默认服务器 ID `main`（完整提供程序 `ldapmain`）开始。如果我们将 `main` 改为 `mycompany`，那么 `new_provider` 就是 `ldapmycompany`。要重命名所有用户身份，请运行以下命令：

```shell
sudo gitlab-rake gitlab:ldap:rename_provider[ldapmain,ldapmycompany]
```

示例输出：

```plaintext
100 users with provider 'ldapmain' will be updated to 'ldapmycompany'.
If the new provider is incorrect, users will be unable to sign in.
Do you want to continue (yes/no)? yes

User identities were successfully updated
```

### 其它选项

如果您没有指定 `old_provider` 和 `new_provider`，任务会提示输入：

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:ldap:rename_provider
```

**源安装实例**

```shell
bundle exec rake gitlab:ldap:rename_provider RAILS_ENV=production
```

**示例输出：**

```plaintext
What is the old provider? Ex. 'ldapmain': ldapmain
What is the new provider? Ex. 'ldapcustom': ldapmycompany
```

此任务还接受 `force` 环境变量，它会跳过确认对话框：

```shell
sudo gitlab-rake gitlab:ldap:rename_provider[old_provider,new_provider] force=yes
```

## Secrets

GitLab 可以使用 [LDAP 配置 secret](../auth/ldap/index.md#使用加密凭证) 从加密文件中读取。以下 Rake 任务用于更新加密文件的内容。

### 显示 secret

显示当前 LDAP secret 的内容。

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:ldap:secret:show
```

**源安装实例**

```shell
bundle exec rake gitlab:ldap:secret:show RAILS_ENV=production
```

**示例输出：**

```plaintext
main:
  password: '123'
  bind_dn: 'gitlab-adm'
```

### 编辑 secret

在编辑器中打开 secret 内容，并在退出时将生成的内容写入加密的 secret 文件。

**Omnibus 安装实例**

```shell
sudo gitlab-rake gitlab:ldap:secret:edit EDITOR=vim
```

**源安装实例**

```shell
bundle exec rake gitlab:ldap:secret:edit RAILS_ENV=production EDITOR=vim
```

### 写入 raw secret

通过在 STDIN 上提供新的 secret 来写入新的 secret。

**Omnibus 安装实例**

```shell
echo -e "main:\n  password: '123'" | sudo gitlab-rake gitlab:ldap:secret:write
```

**源安装实例**

```shell
echo -e "main:\n  password: '123'" | bundle exec rake gitlab:ldap:secret:write RAILS_ENV=production
```

### Secrets 示例

**编辑器示例**

在 edit 命令不适用于您的编辑器的情况下，可以使用 write 任务：

```shell
# Write the existing secret to a plaintext file
sudo gitlab-rake gitlab:ldap:secret:show > ldap.yaml
# Edit the ldap file in your editor
...
# Re-encrypt the file
cat ldap.yaml | sudo gitlab-rake gitlab:ldap:secret:write
# Remove the plaintext file
rm ldap.yaml
```

**KMS 集成示例**

它还可以用作接收使用 KMS 加密的内容的应用程序：

```shell
gcloud kms decrypt --key my-key --keyring my-test-kms --plaintext-file=- --ciphertext-file=my-file --location=us-west1 | sudo gitlab-rake gitlab:ldap:secret:write
```

**Google Cloud secret 集成示例**

它还可以用作接收来自 Google Cloud 的 secret 的应用程序：

```shell
gcloud secrets versions access latest --secret="my-test-secret" > $1 | sudo gitlab-rake gitlab:ldap:secret:write
```
