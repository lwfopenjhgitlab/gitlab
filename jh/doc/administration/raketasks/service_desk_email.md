---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 服务台电子邮件 Rake 任务 **(FREE SELF)**

> 引入于 15.9 版本。

以下是与服务台电子邮件相关的 Rake 任务。

## Secrets

极狐GitLab 可以使用[服务台电子邮件](../../user/project/service_desk.md#configuring-a-custom-mailbox)，从加密文件中读取 secret，而不是将它们以明文形式存储在文件系统中。以下 Rake 任务用于更新加密文件的内容。

### 显示 secret

显示当前服务台电子邮件 secret 的内容。

* Omnibus

```shell
sudo gitlab-rake gitlab:service_desk_email:secret:show
```

* Kubernetes

使用 Kubernetes secret 存储服务台电子邮件密码。有关详细信息，请阅读 [Helm IMAP secret](https://docs.gitlab.cn/charts/installation/secrets.html#imap-password-for-service-desk-emails)。


* Docker

```shell
sudo docker exec -t <container name> gitlab:service_desk_email:secret:show
```

* 源安装

```shell
bundle exec rake gitlab:service_desk_email:secret:show RAILS_ENV=production
```

#### 输出示例

```plaintext
password: 'examplepassword'
user: 'service-desk-email@mail.example.com'
```

### 编辑 secret

在您的编辑器中打开 secret 内容，并在您退出时将结果内容写入加密的 secret 文件。

* Omnibus

```shell
sudo gitlab-rake gitlab:service_desk_email:secret:edit EDITOR=vim
```

* Kubernetes

使用 Kubernetes secret 存储服务台电子邮件密码。有关详细信息，请阅读 [Helm IMAP secret](https://docs.gitlab.cn/charts/installation/secrets.html#imap-password-for-service-desk-emails)。

* Docker

```shell
sudo docker exec -t <container name> gitlab:service_desk_email:secret:edit EDITOR=editor
```

* 源安装

```shell
bundle exec rake gitlab:service_desk_email:secret:edit RAILS_ENV=production EDITOR=vim
```

### 写入 raw secret

提供新的 secret 内容，在 `STDIN` 上写入。

* Omnibus

```shell
echo -e "password: 'examplepassword'" | sudo gitlab-rake gitlab:service_desk_email:secret:write
```

* Kubernetes

使用 Kubernetes secret 存储服务台电子邮件密码。有关详细信息，请阅读 [Helm IMAP secret](https://docs.gitlab.cn/charts/installation/secrets.html#imap-password-for-service-desk-emails)。

* Docker

```shell
sudo docker exec -t <container name> /bin/bash
echo -e "password: 'examplepassword'" | gitlab-rake gitlab:service_desk_email:secret:write
```

* 源安装

```shell
echo -e "password: 'examplepassword'" | bundle exec rake gitlab:service_desk_email:secret:write RAILS_ENV=production
```

### Secrets 示例

**编辑器示例**

Write 任务可用于编辑命令不适用于您的编辑器的情况：

```shell
# Write the existing secret to a plaintext file
sudo gitlab-rake gitlab:service_desk_email:secret:show > service_desk_email.yaml
# Edit the service_desk_email file in your editor
...
# Re-encrypt the file
cat service_desk_email.yaml | sudo gitlab-rake gitlab:service_desk_email:secret:write
# Remove the plaintext file
rm service_desk_email.yaml
```

**KMS 集成示例**

它还可以用作使用 KMS 加密的内容的接收应用程序：

```shell
gcloud kms decrypt --key my-key --keyring my-test-kms --plaintext-file=- --ciphertext-file=my-file --location=us-west1 | sudo gitlab-rake gitlab:service_desk_email:secret:write
```

**Google Cloud secret 集成示例**

它还可以用作接收来自 Google Cloud 的 secret 的应用程序：

```shell
gcloud secrets versions access latest --secret="my-test-secret" > $1 | sudo gitlab-rake gitlab:service_desk_email:secret:write
```
