---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Sidekiq 健康检查 **(FREE SELF)**

极狐GitLab 提供 liveness 和 readiness 探测，获取 Sidekiq 集群的服务健康状况和可达性。这些端点可以提供给像 Kubernetes 这样的调度程序来保持流量，直到系统准备好或根据需要重新启动容器。

[配置 Sidekiq](index.md) 时可以设置健康检查服务器。

## Readiness

Readiness 探测检查 Sidekiq worker 是否已准备好处理作业。

```plaintext
GET /readiness
```

如果服务器绑定到 `localhost:8092`，则可以探测进程集群的就绪情况，例如：

```shell
curl "http://localhost:8092/readiness"
```

成功后，端点返回一个 `200` HTTP 状态码，以及如下响应：

```json
{
   "status": "ok"
}
```

## Liveness

检查 Sidekiq 集群是否正在运行。

```plaintext
GET /liveness
```

如果服务器绑定到 `localhost:8092`，则可以探测进程集群的活跃度，例如：

```shell
curl "http://localhost:8092/liveness"
```

成功后，端点返回一个 `200` HTTP 状态码，以及如下响应：

```json
{
   "status": "ok"
}
```
