---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Sidekiq 作业迁移 Rake 任务 **(FREE SELF)**

WARNING:
这种操作应该非常少见。我们不建议对绝大多数极狐GitLab 实例使用它。

Sidekiq 路由规则允许管理员将某些后台作业从常规队列重新路由到备用队列。默认情况下，极狐GitLab 对每个后台作业类型使用一个队列。极狐GitLab 有超过 400 种后台作业类型，因此相应地它有超过 400 个队列。

大多数管理员不需要更改此设置。在某些具有特别大的后台作业处理工作负载的情况下，Redis 性能可能会由于极狐GitLab 侦听的队列数量而受到影响。

如果 Sidekiq 路由规则发生更改，管理员需要小心迁移以避免完全丢失作业。基本的迁移步骤是：

1. 同时侦听旧队列和新队列。
1. 更新路由规则。
1. [重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。
1. 运行[用于迁移排队中的和未来的作业的 Rake 任务](#migrate-queued-and-future-jobs)。
1. 停止侦听旧队列。

<a id="migrate-queued-and-future-jobs"></a>

## 迁移排队中的和未来的作业

第 4 步涉及为已存储在 Redis 中但将在未来运行的作业重写一些 Sidekiq 作业数据。将来有两组作业要运行：计划作业和要重试的作业。我们提供一个单独的 Rake 任务来迁移每个集合：

- `gitlab:sidekiq:migrate_jobs:retry` 用于要重试的作业。
- `gitlab:sidekiq:migrate_jobs:scheduled` 用于计划作业。

尚未运行的排队作业也可以使用 Rake 任务迁移（可用于 15.6 及更高版本）：

- `gitlab:sidekiq:migrate_jobs:queued` 用于异步执行排队的作业。

大多数时候，同时运行这三个任务是正确的选择。三个独立的任务允许在需要时进行更细粒度的控制。一次运行所有三个任务（可用于 15.6 及更高版本）：

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:sidekiq:migrate_jobs:retry gitlab:sidekiq:migrate_jobs:schedule gitlab:sidekiq:migrate_jobs:queued

# source installations
bundle exec rake gitlab:sidekiq:migrate_jobs:retry gitlab:sidekiq:migrate_jobs:schedule gitlab:sidekiq:migrate_jobs:queued RAILS_ENV=production
```

