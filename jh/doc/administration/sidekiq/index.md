---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 配置外部 Sidekiq 实例 **(FREE SELF)**

您可以使用极狐GitLab 包中捆绑的 Sidekiq，来配置外部 Sidekiq 实例。Sidekiq 需要连接到 Redis、PostgreSQL 和 Gitaly 实例。

## 为 PostgreSQL、Gitaly 和 Redis 配置 TCP 访问

默认情况下，极狐GitLab 使用 UNIX 套接字，并且未设置为通过 TCP 进行通信。要更改：

1. 编辑极狐GitLab 实例上的 `/etc/gitlab/gitlab.rb` 文件，并添加以下内容：

   ```ruby

   ## PostgreSQL

   # Replace POSTGRESQL_PASSWORD_HASH with a generated md5 value
   postgresql['sql_user_password'] = 'POSTGRESQL_PASSWORD_HASH'
   postgresql['listen_address'] = '0.0.0.0'
   postgresql['port'] = 5432

   # Add the Sidekiq nodes to PostgreSQL's trusted addresses.
   # In the following example, 10.10.1.30/32 is the private IP
   # of the Sidekiq server.
   postgresql['md5_auth_cidr_addresses'] = %w(127.0.0.1/32 10.10.1.30/32)
   postgresql['trust_auth_cidr_addresses'] = %w(127.0.0.1/32 10.10.1.30/32)

   ## Gitaly

   # Make Gitaly accept connections on all network interfaces
   gitaly['listen_addr'] = "0.0.0.0:8075"
   ## Set up the Gitaly token as a form of authentication since you are accessing Gitaly over the network
   ## https://docs.gitlab.com/ee/administration/gitaly/configure_gitaly.html#about-the-gitaly-token
   gitaly['auth_token'] = 'abc123secret'
   praefect['configuration'][:auth][:token] = 'abc123secret'
   gitlab_rails['gitaly_token'] = 'abc123secret'

   ## Redis configuration

   redis['bind'] = '0.0.0.0'
   redis['port'] = 6379
   # Password to Authenticate Redis
   redis['password'] = 'redis-password-goes-here'
   gitlab_rails['redis_password'] = 'redis-password-goes-here'

   gitlab_rails['auto_migrate'] = false
   ```

1. 运行 `reconfigure`：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

1. 重启 `PostgreSQL` 服务器：

   ```shell
   sudo gitlab-ctl restart postgresql
   ```

1. 重启后，将 `auto_migrate` 设置为 `true` 或注释使用默认设置：

   ```ruby
   gitlab_rails['auto_migrate'] = true
   ```

1. 再次运行 `reconfigure`：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

## 设置 Sidekiq 实例

1. SSH 进入 Sidekiq 服务器。

1. 确认您可以访问 PostgreSQL、Gitaly 和 Redis 端口：

   ```shell
   telnet <GitLab host> 5432 # PostgreSQL
   telnet <GitLab host> 8075 # Gitaly
   telnet <GitLab host> 6379 # Redis
   ```

1. [下载并安装](https://gitlab.cn/install/) Omnibus GitLab 包，完成步骤 1 和 2。**不要完成任何其他步骤。**

1. 从极狐GitLab 实例中复制 `/etc/gitlab/gitlab.rb` 文件并添加以下设置。确保将它们替换为您的值：

<!--
Updates to example must be made at:
- https://gitlab.com/gitlab-org/gitlab/blob/master/doc/administration/sidekiq.md
- all reference architecture pages
-->

   ```ruby
   ########################################
   #####        Services Disabled       ###
   ########################################
   #
   # When running GitLab on just one server, you have a single `gitlab.rb`
   # to enable all services you want to run.
   # When running GitLab on N servers, you have N `gitlab.rb` files.
   # Enable only the services you want to run on each
   # specific server, while disabling all others.
   #
   gitaly['enable'] = false
   postgresql['enable'] = false
   redis['enable'] = false
   nginx['enable'] = false
   puma['enable'] = false
   gitlab_workhorse['enable'] = false
   prometheus['enable'] = false
   alertmanager['enable'] = false
   grafana['enable'] = false
   gitlab_exporter['enable'] = false
   gitlab_kas['enable'] = false

   ##
   ## To maintain uniformity of links across nodes, the
   ## `external_url` on the Sidekiq server should point to the external URL that users
   ## use to access GitLab. This can be either:
   ##
   ## - The `external_url` set on your application server.
   ## - The URL of a external load balancer, which routes traffic to the GitLab application server.
   ##
   external_url 'https://gitlab.example.com'

   # Configure the gitlab-shell API callback URL. Without this, `git push` will
   # fail. This can be your 'front door' GitLab URL or an internal load
   # balancer.
   gitlab_rails['internal_api_url'] = 'GITLAB_URL'
   gitlab_shell['secret_token'] = 'SHELL_TOKEN'

   ########################################
   ####              Redis              ###
   ########################################

   ## Must be the same in every sentinel node.
   redis['master_name'] = 'gitlab-redis' # Required if you have setup redis cluster
   ## The same password for Redis authentication you set up for the master node.
   redis['master_password'] = '<redis_master_password>'

   ### If redis is running on the main Gitlab instance and you have opened the TCP port as above add the following
   gitlab_rails['redis_host'] = '<gitlab_host>'
   gitlab_rails['redis_port'] = 6379

   #######################################
   ###              Gitaly             ###
   #######################################

   ## Replace <gitaly_token> with the one you set up, see
   ## https://docs.gitlab.com/ee/administration/gitaly/configure_gitaly.html#about-the-gitaly-token
   git_data_dirs({
     "default" => {
        "gitaly_address" => "tcp://<gitlab_host>:8075",
        "gitaly_token" => "<gitaly_token>"
     }
   })

   #######################################
   ###            Postgres             ###
   #######################################

   # Replace <database_host> and <database_password>
   gitlab_rails['db_host'] = '<database_host>'
   gitlab_rails['db_port'] = '5432'
   gitlab_rails['db_password'] = '<database_password>'
   ## Prevent database migrations from running on upgrade automatically
   gitlab_rails['auto_migrate'] = false

   #######################################
   ###      Sidekiq configuration      ###
   #######################################
   sidekiq['enable'] = true
   sidekiq['listen_address'] = "0.0.0.0"

   ## Set number of Sidekiq queue processes to the same number as available CPUs
   sidekiq['queue_groups'] = ['*'] * 4

   ## Set number of Sidekiq threads per queue process to the recommend number of 10
   sidekiq['max_concurrency'] = 20
   ```

1. 从极狐GitLab 实例复制 `/etc/gitlab/gitlab-secrets.json` 文件，替换 Sidekiq 实例中的文件。

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

1. 完成该过程并完成数据库迁移后，重新启动 Sidekiq 实例。

## 使用共享存储配置多个 Sidekiq 节点

如果您使用共享文件存储（例如 NFS）运行多个 Sidekiq 节点，则必须指定 UID 和 GID 以确保它们在服务器之间匹配。指定 UID 和 GID 可以防止文件系统中的权限问题。<!--此建议类似于[关于 Geo 设置的建议](../geo/replication/multiple_servers.md#step-4-configure-the-frontend-application-nodes-on-the-geo-secondary-site)。-->

设置多个 Sidekiq 节点：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   user['uid'] = 9000
   user['gid'] = 9000
   web_server['uid'] = 9001
   web_server['gid'] = 9001
   registry['uid'] = 9002
   registry['gid'] = 9002
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

## 使用外部 Sidekiq 时配置容器镜像库

如果您使用的是 Container Registry，并且它在与 Sidekiq 不同的节点上运行，请按照以下步骤操作。

1. 编辑 `/etc/gitlab/gitlab.rb`，配置镜像库 URL：

   ```ruby
   registry_external_url 'https://registry.example.com'
   gitlab_rails['registry_api_url'] = "https://registry.example.com"
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

1. 在托管 Container Registry 的实例中，将 `registry.key` 文件复制到 Sidekiq 节点。

## 配置 Sidekiq 指标服务器

如果要收集 Sidekiq 指标，请启用 Sidekiq 指标服务器。
从 `localhost:8082/metrics` 获取指标，配置指标服务器：

1. 编辑`/etc/gitlab/gitlab.rb`：

   ```ruby
   sidekiq['metrics_enabled'] = true
   sidekiq['listen_address'] = "localhost"
   sidekiq['listen_port'] = "8082"

   # Optionally log all the metrics server logs to log/sidekiq_exporter.log
   sidekiq['exporter_log_enabled'] = true
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

### 启用 HTTPS

> 引入于 15.2 版本。

要通过 HTTPS 而不是 HTTP 提供指标，请在导出器设置中启用 TLS：

1. 编辑 `/etc/gitlab/gitlab.rb` 以添加（或查找并取消注释）以下行：

   ```ruby
   sidekiq['exporter_tls_enabled'] = true
   sidekiq['exporter_tls_cert_path'] = "/path/to/certificate.pem"
   sidekiq['exporter_tls_key_path'] = "/path/to/private-key.pem"
   ```

1. 保存文件并[重新配置极狐GitLab](../restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

启用 TLS 时，使用与上述相同的 `port` 和 `address`。
指标服务器不能同时提供 HTTP 和 HTTPS。

## 配置健康检查

如果您使用健康检查探针观察 Sidekiq，请启用 Sidekiq 健康检查服务器。
例如从 `localhost:8092` 进行健康检查：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   sidekiq['health_checks_enabled'] = true
   sidekiq['health_checks_listen_address'] = "localhost"
   sidekiq['health_checks_listen_port'] = "8092"
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

有关健康检查的更多信息，请参阅 [Sidekiq 健康检查页面](sidekiq_health_check.md)。

## 配置 LDAP 和用户或群组同步

如果您使用 LDAP 进行用户和群组管理，则必须将 LDAP 配置添加到 Sidekiq 节点以及 LDAP 同步 worker。如果 LDAP 配置和 LDAP 同步 worker 未应用于您的 Sidekiq 节点，则不会自动同步用户和群组。

有关为极狐GitLab 配置 LDAP 的更多信息，请参阅：

- [极狐GitLab LDAP 配置文档](../auth/ldap/index.md#configure-ldap)
- [LDAP 同步文档](../auth/ldap/ldap_synchronization.md#adjust-ldap-user-sync-schedule)

要使用 Sidekiq 的同步 worker 启用 LDAP：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_enabled'] = true
   gitlab_rails['prevent_ldap_sign_in'] = false
   gitlab_rails['ldap_servers'] = {
   'main' => {
   'label' => 'LDAP',
   'host' => 'ldap.mydomain.com',
   'port' => 389,
   'uid' => 'sAMAccountName',
   'encryption' => 'simple_tls',
   'verify_certificates' => true,
   'bind_dn' => '_the_full_dn_of_the_user_you_will_bind_with',
   'password' => '_the_password_of_the_bind_user',
   'tls_options' => {
      'ca_file' => '',
      'ssl_version' => '',
      'ciphers' => '',
      'cert' => '',
      'key' => ''
   },
   'timeout' => 10,
   'active_directory' => true,
   'allow_username_or_email_login' => false,
   'block_auto_created_users' => false,
   'base' => 'dc=example,dc=com',
   'user_filter' => '',
   'attributes' => {
      'username' => ['uid', 'userid', 'sAMAccountName'],
      'email' => ['mail', 'email', 'userPrincipalName'],
      'name' => 'cn',
      'first_name' => 'givenName',
      'last_name' => 'sn'
   },
   'lowercase_usernames' => false,

   # Enterprise Edition only
   # https://docs.gitlab.com/ee/administration/auth/ldap/ldap_synchronization.html
   'group_base' => '',
   'admin_group' => '',
   'external_groups' => [],
   'sync_ssh_keys' => false
   }
   }
   gitlab_rails['ldap_sync_worker_cron'] = "0 */12 * * *"
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

## 禁用 Rugged

调用 `libgit2` 的 Rugged Ruby 绑定，[锁定 Sidekiq 进程的 GVL](https://silverhammermba.github.io/emberb/c/#c-in-ruby-threads)，阻止该 worker 进行中的所有作业。如果 Sidekiq 执行的 Rugged 调用速度很慢，这可能会导致后台任务处理出现明显延迟。

默认情况下，当 Git 仓库数据存储在本地存储或 NFS 挂载上时，使用 Rugged。使用 NFS 时建议使用 Rugged<!--[使用 NFS 时建议使用 Rugged](../nfs.md#improving-nfs-performance-with-gitlab)-->，但如果您使用本地存储，禁用 Rugged 可以提高 Sidekiq 性能：

```shell
sudo gitlab-rake gitlab:features:disable_rugged
```

<!--
## Related topics

- [Extra Sidekiq processes](extra_sidekiq_processes.md)
- [Extra Sidekiq routing](extra_sidekiq_routing.md)
- [Sidekiq health checks](sidekiq_health_check.md)
- [Using the GitLab-Sidekiq chart](https://docs.gitlab.com/charts/charts/gitlab/sidekiq/)

## Troubleshooting

See our [administrator guide to troubleshooting Sidekiq](sidekiq_troubleshooting.md).
-->