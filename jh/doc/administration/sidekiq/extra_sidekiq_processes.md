---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 运行多个 Sidekiq 进程 **(FREE SELF)**

极狐GitLab 允许您启动多个 Sidekiq 进程，可以在单个实例上以更高的速率处理后台作业。默认情况下，Sidekiq 启动一个 worker 进程并且只使用一个内核。

NOTE:
此页面中的信息仅适用于 Omnibus GitLab。

<a id="start-multiple-processes"></a>

## 启动多个进程

启动多个进程时，进程数最多应等于（且**不**超过）您要专用于 Sidekiq 的 CPU 内核数。
Sidekiq worker 进程只使用一个 CPU 内核。

要启动多个进程，请使用 `sidekiq['queue_groups']` 数组设置来指定使用 `sidekiq-cluster` 创建的进程数以及它们应该处理的队列。数组中的每一项都相当于一个额外的 Sidekiq 进程，每一项中的值决定了它处理的队列。在绝大多数情况下，所有进程都应该监听所有队列（请参阅[处理特定作业类](processing_specific_job_classes.md) 了解更多详细信息）。

例如，要创建四个 Sidekiq 进程，每个进程监听所有可用队列：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   sidekiq['queue_groups'] = ['*'] * 4
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

在极狐GitLab 中查看 Sidekiq 进程：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，选择 **监控 > 后台作业**。

<a id="concurrency"></a>

## 并发

默认情况下，在 Sidekiq 下定义的每个进程都以等于队列数的线程数加上一个备用线程开始，最多 50 个。
例如，处理所有队列的进程默认使用 50 个线程。

这些线程在单个 Ruby 进程中运行，每个进程只能使用一个 CPU 核心。线程的有用性取决于需要等待一些外部依赖项的工作，例如数据库查询或 HTTP 请求。大多数 Sidekiq 部署都受益于这种线程。

<a id="manage-thread-counts-explicitly"></a>

### 管理线程数

正确的最大线程数（也称为并发）取决于工作负载。典型值范围从 CPU 密集型任务的 `5` 到混合低优先级工作的 `15` 或更高。对于非专业部署，合理的起始范围是 `15` 到 `25`。

我们只建议通过将 `min_concurrency` 和 `max_concurrency` 设置为相同的值来设置显式并发。出于向后兼容的原因，保留了这两个不同的设置，但为了获得更可预测的结果，请使用相同的值，否则您可能会遇到 Sidekiq 作业堆积的问题。

例如，将并发设置为 `20`：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   sidekiq['min_concurrency'] = 20
   sidekiq['max_concurrency'] = 20
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

`min_concurrency` 和 `max_concurrency` 是独立的。将 `min_concurrency` 设置为 `0` 会禁用限制。未明确设置 `min_concurrency` 与将其设置为 0 是等同的。

对于每个队列组，让 `N` 比队列数大 1。并发设置为：

1. `min_concurrency`，如果它等于 `max_concurrency`。
1. `N`，如果它在 `min_concurrency` 和 `max_concurrency` 之间。
1. `max_concurrency`，如果 `N` 超过这个值。
1. `min_concurrency`，如果 `N` 小于这个值。

当 `min_concurrency` 大于 `max_concurrency` 时，它被视为等于 `max_concurrency`。

<!--
You can find example values used by GitLab.com by searching for `concurrency:`
in [the Helm charts](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/releases/gitlab/values/gprd.yaml.gotmpl).-->

这些值根据 Sidekiq 的每个特定部署所做的工作而有所不同。
具有专用于特定队列的进程的任何其他专门部署，应根据以下内容，调整并发数：

- 每种进程的 CPU 使用率。
- 实现的吞吐量。

每个线程都需要一个 Redis 连接，因此添加线程可能会增加 Redis 延迟并可能导致客户端超时。有关详细信息，请参阅[有关 Redis 的 Sidekiq 文档](https://github.com/mperham/sidekiq/wiki/Using-Redis)。

## 修改检查间隔

修改附加 Sidekiq 进程的 Sidekiq 健康检查间隔：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   sidekiq['interval'] = 5
   ```

   该值可以是任何整数秒。

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

## 使用 CLI 进行故障排除

WARNING:
建议使用 `/etc/gitlab/gitlab.rb` 来配置 Sidekiq 进程。如果您遇到问题，建议您联系技术支持。使用命令行需要您自担风险。

出于调试目的，您可以使用命令 `/opt/gitlab/embedded/service/gitlab-rails/bin/sidekiq-cluster` 启动额外的 Sidekiq 进程。此命令使用以下语法接受参数：

```shell
/opt/gitlab/embedded/service/gitlab-rails/bin/sidekiq-cluster [QUEUE,QUEUE,...] [QUEUE, ...]
```

`--dryrun` 参数允许在不实际启动命令的情况下查看要执行的命令。

每个单独的参数表示一组必须由 Sidekiq 进程处理的队列。多个队列可以由同一进程处理，方法是用逗号而不是空格分隔它们。

除了队列，还可以提供队列命名空间，让进程自动侦听该命名空间中的所有队列，而无需列出所有队列名称。<!--For more information about queue namespaces,
see the relevant section in the
[Sidekiq development documentation](../../development/sidekiq/index.md#queue-namespaces).-->

### 监控 `sidekiq-cluster` 命令

`sidekiq-cluster` 命令在启动所需数量的 Sidekiq 进程后不会终止。相反，该进程继续运行并将任何信号转发给子进程。这允许您在向 `sidekiq-cluster` 进程发送信号时停止所有 Sidekiq 进程，而不必将其发送到各个进程。

如果 `sidekiq-cluster` 进程崩溃或收到 `SIGKILL`，子进程会在几秒钟后自行终止。这可确保您不会遇到僵尸 Sidekiq 进程。

这允许您通过将 `sidekiq-cluster` 连接到您选择的 supervisor（例如，runit）来监控进程。

如果一个子进程死亡，则 `sidekiq-cluster` 命令会通知所有剩余的进程终止，然后自行终止。这消除了对 `sidekiq-cluster` 重新实现复杂进程监控/重启代码的需要。相反，您应该确保您的 supervisor 在必要时重新启动 `sidekiq-cluster` 进程。

### PID 文件

`sidekiq-cluster` 命令可以将其 PID 存储在文件中。默认情况下不写入 PID 文件，但这可以通过将 `--pidfile` 选项传递给 `sidekiq-cluster` 来更改。例如：

```shell
/opt/gitlab/embedded/service/gitlab-rails/bin/sidekiq-cluster --pidfile /var/run/gitlab/sidekiq_cluster.pid process_commit
```

请记住，PID 文件包含 `sidekiq-cluster` 命令的 PID，而不是已启动的 Sidekiq 进程的 PID。

### 环境

Rails 环境可以通过将 `--environment` 标志传递给 `sidekiq-cluster` 命令，或将 `RAILS_ENV` 设置为非空值来设置。默认值可以在 `/opt/gitlab/etc/gitlab-rails/env/RAILS_ENV` 中找到。
