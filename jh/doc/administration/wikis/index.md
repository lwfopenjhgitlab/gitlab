---
type: reference, howto
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Wiki 设置 **(FREE SELF)**

调整极狐GitLab 实例的 Wiki 设置。

## Wiki 页面内容大小限制

> 引入于 13.2 版本。

您可以为 wiki 页面设置最大内容大小限制。此限制可以防止滥用该功能。默认值为 **52428800 字节**（50 MB）。

### 工作原理

当通过 UI 或 API 创建或更新 wiki 页面时，将针对内容大小应用限制。通过 Git 推送的本地更改未经验证。

对于现有的 wiki 页面，限制不会生效，直到再次编辑 wiki 页面并且内容发生更改。

### Wiki 页面内容大小限制配置

此设置无法通过管理中心设置。
要配置此设置，请使用 Rails 控制台或[应用程序设置 API](../../api/settings.md)。

NOTE:
限制值必须以字节为单位。最小值为 1024 字节。

#### 通过 Rails 控制台

要通过 Rails 控制台配置此设置：

1. 启动 Rails 控制台：

   ```shell
   # For Omnibus installations
   sudo gitlab-rails console

   # For installations from source
   sudo -u git -H bundle exec rails console -e production
   ```

1. 更新 wiki 页面的最大内容大小：

   ```ruby
   ApplicationSetting.first.update!(wiki_page_max_content_bytes: 50.megabytes)
   ```

要检索当前值，请启动 Rails 控制台并运行：

  ```ruby
  Gitlab::CurrentSettings.wiki_page_max_content_bytes
  ```

#### 通过 API

要通过 API 设置 Wiki 页面大小限制，请使用以下命令：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/application/settings?wiki_page_max_content_bytes=52428800"
```

您还可以使用 API 获取当前值：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/application/settings"
```

<a id="reduce-wiki-repository-size"></a>

### 减小 wiki 仓库大小

Wiki 算作[命名空间存储大小](../../user/admin_area/settings/account_and_limit_settings.md)的一部分，因此您应该使您的 wiki 仓库尽可能小。

有关压缩仓库的工具的更多信息，请阅读有关[减少仓库大小](../../user/project/repository/reducing_the_repo_size_using_git.md)的文档。

## 相关主题

- [Wiki 的用户文档](../../user/project/wiki/index.md)
- [项目 wikis API](../../api/wikis.md)
- [群组 wikis API](../../api/group_wikis.md)
