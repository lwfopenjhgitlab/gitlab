---
stage: Mobile
group: Mobile DevOps
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 安全文件管理 **(FREE)**

> - 引入于 14.8 版本，[功能标志](feature_flags.md)为 `ci_secure_files`。默认禁用。
> - 一般可用于 15.7 版本，删除功能标志 `ci_secure_files`。

您可以安全地存储最多 100 个文件，作为安全文件在 CI/CD 流水线中使用。
这些文件安全地存储在项目仓库之外，并且不受版本控制。
在这些文件中存储敏感信息是安全的。安全文件支持纯文本和二进制文件类型，并且不得超过 5 MB。

您可以使用下面描述的选项配置这些文件的存储位置，但默认位置是：

- Linux 软件包安装实例：`/var/opt/gitlab/gitlab-rails/shared/ci_secure_files`
- 自编译安装实例：`/home/git/gitlab/shared/ci_secure_files`

使用[外部对象存储](https://docs.gitlab.cn/charts/advanced/external-object-storage/#lfs-artifacts-uploads-packages-external-diffs-terraform-state-dependency-proxy)配置[极狐GitLab Helm chart](https://docs.gitlab.cn/charts/) 安装实例。

## 禁用安全文件

您可以在整个极狐GitLab 实例中禁用安全文件。您可能想要禁用安全文件以减少磁盘空间，或删除对该功能的访问。

要禁用安全文件，请根据您的安装实例执行以下步骤。

先决条件：

- 您必须是管理员。

**Linux 软件包安装实例**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['ci_secure_files_enabled'] = false
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**自编译安装实例**

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   ci_secure_files:
     enabled: false
   ```

1. 保存文件并[重新启动极狐GitLab](restart_gitlab.md#installations-from-source)，使更改生效。

## 使用本地存储

默认配置使用本地存储。要更改安全文件本地存储的位置，请按照以下步骤操作。

**Linux 软件包安装实例**

1. 要将存储路径更改为 `/mnt/storage/ci_secure_files`，请编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['ci_secure_files_storage_path'] = "/mnt/storage/ci_secure_files"
   ```

1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#reconfigure-a-linux-package-installation)：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**自编译安装实例**

1. 要将存储路径更改为 `/mnt/storage/ci_secure_files`，请编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   ci_secure_files:
     enabled: true
     storage_path: /mnt/storage/ci_secure_files
   ```

1. 保存文件并[重新启动极狐GitLab](restart_gitlab.md#installations-from-source)，使更改生效。

## 使用对象存储 **(FREE SELF)**

您应该使用[受支持的对象存储选项之一](object_storage.md#supported-object-storage-providers)，而不是将安全文件存储在磁盘上。
此配置依赖于已配置的有效凭据。

[阅读有关在极狐GitLab 中使用对象存储的更多信息](object_storage.md)。

NOTE:
统一对象存储配置不支持此功能。

### 对象存储设置

以下设置：

- 在源安装上嵌套在 `ci_secure_files:` 下，然后嵌套在 `object_store:` 下。
- Linux 软件包安装上以 `ci_secure_files_object_store_` 为前缀。

| 设置 | 描述 | 默认值 |
|---------|-------------|---------|
| `enabled` | 启用/禁用对象存储 | `false` |
| `remote_directory` | 存储安全文件的存储桶名称 | |
| `connection` | 下面描述的各种连接选项 | |

### S3 兼容的连接设置

请参阅[不同提供商的可用连接设置](object_storage.md#configure-the-connection-settings)。

**Linux 软件包安装实例**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行，使用您想要的值：

   ```ruby
   gitlab_rails['ci_secure_files_object_store_enabled'] = true
   gitlab_rails['ci_secure_files_object_store_remote_directory'] = "terraform"
   gitlab_rails['ci_secure_files_object_store_connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-central-1',
     'aws_access_key_id' => 'AWS_ACCESS_KEY_ID',
     'aws_secret_access_key' => 'AWS_SECRET_ACCESS_KEY'
   }
   ```

   NOTE:
   如果您使用 AWS IAM 配置文件，请务必省略 AWS 访问密钥和 secret 访问密钥/值对：

   ```ruby
   gitlab_rails['ci_secure_files_object_store_connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-central-1',
     'use_iam_profile' => true
   }
   ```

1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#reconfigure-a-linux-package-installation)：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

1. [将任何现有的本地状态文件迁移到对象存储](#migrate-to-object-storage)。

**自编译安装实例**

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   ci_secure_files:
     enabled: true
     object_store:
       enabled: true
       remote_directory: "ci_secure_files"  # The bucket name
       connection:
         provider: AWS  # Only AWS supported at the moment
         aws_access_key_id: AWS_ACCESS_KEY_ID
         aws_secret_access_key: AWS_SECRET_ACCESS_KEY
         region: eu-central-1
   ```

1. 保存文件并[重新启动极狐GitLab](restart_gitlab.md#installations-from-source)，使更改生效。
1. [将任何现有本地状态文件迁移到对象存储](#migrate-to-object-storage)。

<a id="migrate-to-object-storage"></a>

### 迁移到对象存储

> 引入于 16.1 版本。

WARNING:
无法将安全文件从对象存储迁移回本地存储，因此请谨慎操作。

要将安全文件迁移到对象存储，请按照以下说明操作。

- Linux 软件包安装实例：

  ```shell
  sudo gitlab-rake gitlab:ci_secure_files:migrate
  ```

- 自编译安装实例：

  ```shell
  sudo -u git -H bundle exec rake gitlab:ci_secure_files:migrate RAILS_ENV=production
  ```
