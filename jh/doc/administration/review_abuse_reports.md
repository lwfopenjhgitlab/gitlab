---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# 查看滥用报告 **(FREE SELF)**

查看和解决来自极狐GitLab 用户的滥用报告。

极狐GitLab 管理员可以在管理中心查看和[解决](#resolving-abuse-reports)滥用报告。

## 通过电子邮件接收滥用报告通知

要通过电子邮件接收新滥用报告的通知：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 报告**。
1. 展开 **滥用报告** 部分。
1. 提供电子邮件地址并选择 **保存修改**。

<!--
The notification email address can also be set and retrieved
[using the API](../../api/settings.md#list-of-settings-that-can-be-accessed-via-api-calls).
-->

## 报告滥用

要了解有关报告滥用的更多信息，请参阅[滥用报告用户文档](../user/report_abuse.md)。

<a id="resolving-abuse-reports"></a>

## 解决滥用报告

要访问滥用报告：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **滥用报告**。

解决滥用报告的方法有 3 种，每种方法都有一个按钮：

- 删除用户和报告：
    - 从实例中[删除被报告的用户](../user/profile/account/delete_account.md)。
    - 从列表中删除滥用报告。
- [禁用用户](#blocking-users)。
- 删除报告：
    - 从列表中删除滥用报告。
    - 删除报告用户的访问限制。

以下是**滥用报告**页面的示例：

![abuse-reports-page-image](img/abuse_reports_page_v13_11.png)

<a id="blocking-users"></a>

### 禁用用户

被禁用的用户无法登录或访问任何仓库，但他们的所有数据仍然存在。

禁用用户：

- 将它们留在滥用报告列表中。
- 将 **禁用用户** 按钮更改为禁用的 **已被禁用** 按钮。

用户收到以下消息通知：

```plaintext
Your account has been blocked. If you believe this is in error, contact a staff member.
```

禁用用户后，您仍然可以：

- 删除用户并在必要时报告。
- 删除报告。

以下是在 **滥用报告** 页面上列出的被禁用用户的示例：

![abuse-report-blocked-user-image](img/abuse_report_blocked_user.png)

<!--
NOTE:
Users can be [blocked](../../api/users.md#block-user) and
[unblocked](../../api/users.md#unblock-user) using the GitLab API.
-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
