---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 外部用户 **(FREE SELF)**

如果希望用户只能访问某些内部或私有项目，则可以选择创建**外部用户**。例如，当承包商正在处理特定项目并且应该只能访问该项目时，此功能可能很有用。

外部用户：

- 无法在其个人命名空间中创建项目、子组和代码片段。
- 只能在明确授予访问权限的顶级群组中创建项目（包括分支）、子组和代码片段。
- 只能访问公开项目和他们被明确授予访问权限的项目，从而对他们隐藏所有其他内部或私有项目（类似于注销情况下）。
- 只能访问公开群组和他们被明确授予访问权限的群组，从而对他们隐藏所有其他内部或私有群组（类似于注销情况下）。
- 只能访问公开的代码片段。

可以通过将用户添加为项目或群组的成员来授予访问权限。
外部用户像普通用户一样，他们在项目或群组中获得一个角色，具有[权限表](../user/permissions.md#project-members-permissions)中提到的所有能力。
例如，如果将外部用户添加为访客用户，而您的项目是内部或私有的，则他们无权访问代码；如果您希望外部用户可以访问代码，则需要授予他们报告者级别或更高级别的访问权限。您应该始终考虑[项目的可见性和权限设置](../user/project/settings/index.md#configure-project-visibility-features-and-permissions)，以及用户的权限级别。

NOTE:
外部用户计入许可证席位。

管理员可以通过以下任一方法将用户标记为外部用户：

- [通过 API](../api/users.md#user-modification)。
- 通过 UI：
    1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
    1. 选择 **管理中心**。
    1. 在左侧边栏中，选择 **概览 > 用户**，创建新用户或编辑现有用户。在那里，您可以找到将用户标记为外部用户的选项。

此外，可以使用以下方法将用户设置为外部用户：

- [SAML 组](../integration/saml.md#external-groups)
- [LDAP 组](../administration/auth/ldap/ldap_synchronization.md#external-groups)

## 将新用户设置为外部用户

默认情况下，新用户不会设置为外部用户。管理员可以更改：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制** 部分。

如果您将新用户默认更改为外部用户，则可以选择通过定义一组内部用户来缩小范围。
**内部用户**字段允许指定电子邮件地址正则表达式，识别默认内部用户。电子邮件地址与正则表达式模式匹配的新用户默认设置为内部，而不是外部协作者。

正则表达式格式在 Ruby 中，但它可转换为 JavaScript，并且设置了忽略大小写标志 (`/regex pattern/i`)。示例如下：

- 使用 `\.internal@domain\.com$`，将以 `.internal@domain.com` 结尾的电子邮件地址标记为内部地址。
- 使用 `^(?:(?!\.ext@domain\.com).)*$\r?`，将电子邮件地址不包括 `.ext@domain.com` 的用户标记为内部用户。

WARNING:
请注意，此正则表达式可能会导致正则表达式拒绝服务（ReDoS）攻击。
