---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 多节点极狐GitLab 的负载均衡器 **(FREE SELF)**

在多节点极狐GitLab 配置中，您需要一个负载均衡器来将流量路由到应用程序服务器。关于使用哪个负载均衡器或确切配置的细节超出了本文档的范围。我们希望，如果您正在管理像极狐GitLab 这样的 HA 系统，您已经可以选择负载均衡器，例如 HAProxy（开源）、F5 Big-IP LTM 和 Citrix NetScaler。本文档概述了与极狐GitLab 一起使用的端口和协议。

## SSL

您想如何在多节点环境中处理 SSL？有几种不同的选择：

- 每个应用程序节点终止 SSL
- 负载均衡器终止 SSL，负载均衡器和应用程序节点之间的通信不安全
- 负载均衡器终止 SSL，负载均衡器和应用程序节点之间的通信*安全*

### 应用程序节点终止 SSL

将负载均衡器配置为将端口 443 上的连接作为 `TCP` 而不是 `HTTP(S)` 协议传递。这会将连接传递到未触及的应用程序节点 NGINX 服务。NGINX 拥有 SSL 证书并监听 443 端口。

有关管理 SSL 证书和配置 NGINX 的详细信息，请参阅 [HTTPS 文档](https://docs.gitlab.cn/omnibus/settings/ssl/index.html)。

### 负载均衡器在没有后端 SSL 的情况下终止 SSL

将您的负载均衡器配置为使用 `HTTP(S)` 协议而不是 `TCP`。
负载均衡器负责管理 SSL 证书和终止 SSL。

因为负载均衡器和极狐GitLab 之间的通信不安全，所以需要一些额外的配置。有关详细信息，请参阅[代理 SSL 文档](https://docs.gitlab.cn/omnibus/settings/ssl/index.html#configure-a-reverse-proxy-or-load-balancer-ssl-termination)。

### 负载均衡器使用后端 SSL 终止 SSL

将您的负载均衡器配置为使用 `HTTP(S)` 协议而不是 `TCP`。
负载均衡器负责管理最终用户看到的 SSL 证书。

在这种情况下，负载均衡器和 NGINX 之间的流量是安全的。无需为代理 SSL 添加配置，因为连接始终是安全的。但是，必须将配置添加到极狐GitLab 才能配置 SSL 证书。有关管理 SSL 证书和配置 NGINX 的详细信息，请参阅 [HTTPS 文档](https://docs.gitlab.cn/omnibus/settings/ssl/index.html)。

## 端口

### 基础端口

| LB 端口 | 后端端口 | 协议                 |
| ------- | ------------ | ------------------------ |
| 80      | 80           | HTTP (*1*)               |
| 443     | 443          | TCP or HTTPS (*1*) (*2*) |
| 22      | 22           | TCP                      |

- (*1*)：Web 终端支持要求您的负载均衡器正确处理 WebSocket 连接。 使用 HTTP 或 HTTPS 代理时，这意味着您的负载均衡器必须配置为通过 `Connection` 和 `Upgrade` hop-by-hop headers。有关详细信息，请参阅 [web 终端](integration/terminal.md)集成指南。
- (*2*)：对端口 443 使用 HTTPS 协议时，您必须向负载均衡器添加 SSL 证书。如果您希望在极狐GitLab 应用程序服务器上终止 SSL，请使用 TCP 协议。

### 极狐GitLab Pages 端口

如果您使用具有自定义域名支持的极狐GitLab Pages，则需要一些额外的端口配置。
极狐GitLab Pages 需要一个单独的虚拟 IP 地址。配置 DNS 将 `/etc/gitlab/gitlab.rb` 中的 `pages_external_url` 指向新的虚拟 IP 地址。有关更多信息，请参阅[极狐GitLab Pages文档](pages/index.md)。

| LB 端口  | 后端端口 | 协议             |
| ------- | ------------- | --------- |
| 80      | Varies (*1*)  | HTTP      |
| 443     | Varies (*1*)  | TCP (*2*) |

- (*1*)：极狐GitLab Pages 的后端端口取决于 `gitlab_pages['external_http']` 和 `gitlab_pages['external_https']` 设置。有关详细信息，请参阅[极狐GitLab Pages 文档](pages/index.md)。
- (*2*)：极狐GitLab Pages 的端口 443 应始终使用 TCP 协议。用户可以使用自定义 SSL 配置自定义域，如果 SSL 在负载平衡器处终止，则无法实现。

### 备用 SSH 端口

一些组织有禁止打开 SSH 端口 22 的策略。在这种情况下，配置一个允许用户在端口 443 上使用 SSH 的备用 SSH 主机名可能会有所帮助。与上面的其他极狐GitLab HTTP 配置相比，备用 SSH 主机名需要一个新的虚拟 IP 地址。

为备用 SSH 主机名配置 DNS，例如 `altssh.gitlab.example.com`。

| LB 端口  | 后端端口 | 协议             |
| ------- | ------------ | -------- |
| 443     | 22           | TCP      |

## 就绪检查

强烈建议多节点部署将负载均衡器配置为使用[就绪检查](../administration/monitoring/health_check.md#readiness)，确保节点准备好接受流量，然后再将流量路由到它。这在使用 Puma 时尤其重要，因为在重新启动期间的短暂时间段内，Puma 不接受请求。

WARNING:
在 15.4 到 15.8 版本中，使用 `all=1` 参数进行就绪检查可能会导致 Praefect 内存使用量增加，并导致内存错误。

<!--
## 故障排除

### 健康检查通过负载均衡器返回 `408` HTTP 码

If you are using [AWS's Classic Load Balancer](https://docs.aws.amazon.com/en_en/elasticloadbalancing/latest/classic/elb-ssl-security-policy.html#ssl-ciphers)
in GitLab 15.0 or later, you must to enable the `AES256-GCM-SHA384` cipher in NGINX.
See [AES256-GCM-SHA384 SSL cipher no longer allowed by default by NGINX](https://docs.gitlab.com/omnibus/update/gitlab_15_changes.html#aes256-gcm-sha384-ssl-cipher-no-longer-allowed-by-default-by-nginx)
for more information.

The default ciphers for a GitLab version can be
viewed in the [`files/gitlab-cookbooks/gitlab/attributes/default.rb`](https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/files/gitlab-cookbooks/gitlab/attributes/default.rb)
file and selecting the Git tag that correlates with your target GitLab version
(for example `15.0.5+ee.0`). If required by your load balancer, you can then define
[custom SSL ciphers](https://docs.gitlab.com/omnibus/settings/ssl.html#use-custom-ssl-ciphers)
for NGINX.
-->
