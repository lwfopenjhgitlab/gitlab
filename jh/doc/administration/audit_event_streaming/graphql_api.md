---
stage: Govern
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 审计事件流 GraphQL API **(ULTIMATE)**

> - API 引入于 14.5 版本，功能标志名为 `ff_external_audit_events_namespace`，默认禁用。
> - API 在 SaaS 版上启用于 14.7 版本，在私有化部署版上默认启用于 14.7 版本。
> - 删除 API 功能标志 `ff_external_audit_events_namespace` 于 14.8 版本。
> - 自定义 HTTP headers API 引入于 15.1 版本，[功能标志为](../feature_flags.md) `streaming_audit_event_headers`。默认禁用。
> - 自定义 HTTP headers API 在 SaaS 和私有化部署版上启用于 15.2 版本。
> - 自定义 HTTP headers API 一般可用于 15.3 版本。功能标志 `streaming_audit_event_headers` 已删除。
> - 用户指定的验证令牌 API 支持引入于 15.4 版本。
> - 用于实例级流目的地的自定义 HTTP header 的 API 引入于 16.1 版本，[功能标志](../feature_flags.md)为 `ff_external_audit_events`。默认禁用。

您可以使用 GraphQL API 维护审计事件流目的地。

## 添加新的流目的地

将新的流传输目的地添加到顶级群组或整个实例。

WARNING:
流传输目的地接收**所有**审计事件数据，其中可能包括敏感信息。确保您信任流传输目的地。

### 添加顶级群组流目的地

先决条件：

- 具有顶级群组的所有者角色。

要启用流传输并将目标添加到顶级群组，请使用 `externalAuditEventDestinationCreate` mutation。

```graphql
mutation {
  externalAuditEventDestinationCreate(input: { destinationUrl: "https://mydomain.io/endpoint/ingest", groupPath: "my-group" } ) {
    errors
    externalAuditEventDestination {
      id
      destinationUrl
      verificationToken
      group {
        name
      }
    }
  }
}
```

您可以选择使用 GraphQL `externalAuditEventDestinationCreate` mutation 指定您自己的验证令牌（而不是默认的极狐GitLab 生成的验证令牌）。验证令牌长度必须在 16 到 24 个字符内，并且尾随空格不会被剪切。您应该设置一个加密随机且唯一的值。例如：

```graphql
mutation {
  externalAuditEventDestinationCreate(input: { destinationUrl: "https://mydomain.io/endpoint/ingest", groupPath: "my-group", verificationToken: "unique-random-verification-token-here" } ) {
    errors
    externalAuditEventDestination {
      id
      destinationUrl
      verificationToken
      group {
        name
      }
    }
  }
}
```

如果满足以下条件，则启用事件流：

- 返回的 `errors` 对象为空。
- API 响应 `200 OK`。

您可以使用 GraphQL `auditEventsStreamingHeadersCreate` mutation 添加 HTTP header。您可以通过[列出该群组的所有流目的地](#list-streaming-destinations)或从上面的 mutation 中检索目的地 ID。

```graphql
mutation {
  auditEventsStreamingHeadersCreate(input: { destinationId: "gid://gitlab/AuditEvents::ExternalAuditEventDestination/24601", key: "foo", value: "bar" }) {
    errors
  }
}
```

如果返回的 `errors` 对象为空，则创建 header。

### 添加实例级流目的地

> 引入于 16.0 版本，[功能标志](../feature_flags.md)为 `ff_external_audit_events`。默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，需要管理员[启用功能标志](../feature_flags.md) `ff_external_audit_events`。在 SaaS 版上，此功能不可用。此功能尚未准备好用于生产。

先决条件：

- 具有实例的管理员访问权限。

要启用流传输并添加目的地，请使用 GraphQL API 中的 `instanceExternalAuditEventDestinationCreate` mutation。

```graphql
mutation {
  instanceExternalAuditEventDestinationCreate(input: { destinationUrl: "https://mydomain.io/endpoint/ingest"}) {
    errors
    instanceExternalAuditEventDestination {
      destinationUrl
      id
      verificationToken
    }
  }
}
```

如果满足以下条件，则启用事件流：

- 返回的 `errors` 对象为空。
- API 响应 `200 OK`。

实例管理员可以使用 GraphQL `auditEventsStreamingInstanceHeadersCreate` mutation 添加 HTTP header。您可以通过[列出实例的所有流传输目的地](#list-streaming-destinations)或从上面的 mutation 中检索目的地 ID。

```graphql
mutation {
  auditEventsStreamingInstanceHeadersCreate(input:
    {
      destinationId: "gid://gitlab/AuditEvents::InstanceExternalAuditEventDestination/42",
      key: "foo",
      value: "bar"
    }) {
    errors
    header {
      id
      key
      value
    }
  }
}
```

如果返回的 `errors` 对象为空，则创建 header。

<a id="list-streaming-destinations"></a>

## 列出流目的地

列出顶级群组或整个实例的流目的地。

### 列出顶级群组流目的地

先决条件：

- 具有顶级群组的所有者角色。

您可以使用 `externalAuditEventDestinations` 查询类型查看顶级群组的流传输目的地列表。

```graphql
query {
  group(fullPath: "my-group") {
    id
    externalAuditEventDestinations {
      nodes {
        destinationUrl
        verificationToken
        id
        headers {
          nodes {
            key
            value
            id
          }
        }
        eventTypeFilters
      }
    }
  }
}
```

如果结果列表为空，则不会为该群组启用审计事件流。

### 列出实例级流目的地

> 引入于 16.0 版本，[功能标志](../feature_flags.md)为 `ff_external_audit_events`。默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，需要管理员[启用功能标志](../feature_flags.md) `ff_external_audit_events`。在 SaaS 版上，此功能不可用。此功能尚未准备好用于生产。

先决条件：

- 具有实例的管理员访问权限。

要查看实例的流目的地列表，请使用 `instanceExternalAuditEventDestinations` 查询类型。

```graphql
query {
  instanceExternalAuditEventDestinations {
    nodes {
      id
      destinationUrl
      verificationToken
      headers {
        nodes {
          id
          key
          value
        }
      }
    }
  }
}
```

如果结果列表为空，则不会为该实例启用审计事件流。

您需要此查询返回的 ID 值来进行更新和删除 mutation。

<a id="update-streaming-destinations"></a>

## 更新流目的地

更新顶级群组或整个实例的流目的地。

### 更新顶级群组流目的地

先决条件：

- 具有顶级群组的所有者角色。

具有组所有者角色的用户可以使用以下命令更新流目的地的自定义 HTTP header `auditEventsStreamingHeadersUpdate` mutation 类型。您可以通过[列出该群组的所有自定义 HTTP header](#list-streaming-destinations) 来检索自定义 HTTP header ID。

```graphql
mutation {
  externalAuditEventDestinationDestroy(input: { id: destination }) {
    errors
  }
}
```

如果满足以下条件，则更新事件流：

- 返回的 `errors` 对象为空。
- API 响应 `200 OK`。

群组所有者可以使用 GraphQL `auditEventsStreamingHeadersDestroy` mutation 删除 HTTP header。您可以通过[列出该群组的所有自定义 HTTP headers](#list-streaming-destinations) 来检索 header ID。

```graphql
mutation {
  auditEventsStreamingHeadersDestroy(input: { headerId: "gid://gitlab/AuditEvents::Streaming::Header/1" }) {
    errors
  }
}
```

如果返回的 `errors` 对象为空，则删除 header。

### 更新实例级流目的地

> 引入于 16.0 版本，[功能标志](../feature_flags.md)为 `ff_external_audit_events`。默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，需要管理员[启用功能标志](../feature_flags.md) `ff_external_audit_events`。在 SaaS 版上，此功能不可用。此功能尚未准备好用于生产。

先决条件：

- 具有实例的管理员访问权限。

要更新实例的流目的地，请使用 `instanceExternalAuditEventDestinationUpdate` mutation 类型。您可以通过[列出实例的所有外部目的地](#list-streaming-destinations)来检索目的地 ID。

```graphql
mutation {
  instanceExternalAuditEventDestinationUpdate(input: { id: "gid://gitlab/AuditEvents::InstanceExternalAuditEventDestination/1", destinationUrl: "https://www.new-domain.com/webhook"}) {
    errors
    instanceExternalAuditEventDestination {
      destinationUrl
      id
      verificationToken
    }
  }
}
```

如果满足以下条件，则更新事件流：

- 返回的 `errors` 对象为空。
- API 响应 `200 OK`。

实例管理员可以使用 `auditEventsStreamingInstanceHeadersUpdate` mutation 类型更新流目的地自定义 HTTP header。您可以通过[列出实例的所有自定义 HTTP header](#list-streaming-destinations) 来检索自定义 HTTP header ID。

```graphql
mutation {
  auditEventsStreamingInstanceHeadersUpdate(input: { headerId: "gid://gitlab/AuditEvents::Streaming::InstanceHeader/2", key: "new-key", value: "new-value" }) {
    errors
    header {
      id
      key
      value
    }
  }
}
```

如果返回的 `errors` 对象为空，则更新 header。

## 删除流目的地

删除顶级群组或整个实例的流目的地。成功删除最后一个目的地后，该群组或实例的流传输将被禁用。

### 删除顶级群组流目的地

先决条件：

- 具有顶级群组的所有者角色。

具有群组所有者角色的用户可以使用 `externalAuditEventDestinationDestroy` mutation 类型删除流目的地。您可以通过[列出该群组的所有流目的地](#list-streaming-destinations) 来检索目的地 ID。

```graphql
mutation {
  externalAuditEventDestinationDestroy(input: { id: destination }) {
    errors
  }
}
```

如果满足以下条件，则删除事件流：

- 返回的 `errors` 对象为空。
- API 响应 `200 OK`。

群组所有者可以使用 GraphQL `auditEventsStreamingHeadersDestroy` mutation 删除 HTTP header。您可以通过[列出该群组的所有自定义 HTTP header](#list-streaming-destinations) 来检索 header ID。

```graphql
mutation {
  auditEventsStreamingHeadersDestroy(input: { headerId: "gid://gitlab/AuditEvents::Streaming::Header/1" }) {
    errors
  }
}
```

如果返回的 `errors` 对象为空，则删除 header。

### 删除实例级流目的地

> 引入于 16.0 版本，[功能标志](../feature_flags.md)为 `ff_external_audit_events`。默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，需要管理员[启用功能标志](../feature_flags.md) `ff_external_audit_events`。在 SaaS 版上，此功能不可用。此功能尚未准备好用于生产。

先决条件：

- 具有实例的管理员访问权限。

要删除流目的地，请使用 `instanceExternalAuditEventDestinationDestroy` mutation 类型。您可以通过[列出实例的所有流目的地](#list-streaming-destinations) 来检索 header ID。

```graphql
mutation {
  instanceExternalAuditEventDestinationDestroy(input: { id: "gid://gitlab/AuditEvents::InstanceExternalAuditEventDestination/1" }) {
    errors
  }
}
```

如果满足以下条件，则删除事件流：

- 返回的 `errors` 对象为空。
- API 响应 `200 OK`。

## 事件类型过滤器

> 事件类型过滤器 API 引入于 15.7 版本。

为群组启用此功能后，您可以使用 API，允许用户按目标过滤审计事件流。
如果在没有过滤器的情况下启用该功能，目标将接收所有审计事件。

设置了事件类型筛选器的流目的地，具有 **已过滤** (**{filter}**) 标记。

### 使用 API 添加事件类型过滤器

先决条件：

- 您必须具有群组的所有者角色。

您可以使用 `auditEventsStreamingDestinationEventsAdd` 查询类型，添加事件类型过滤器列表：

```graphql
mutation {
    auditEventsStreamingDestinationEventsAdd(input: {
        destinationId: "gid://gitlab/AuditEvents::ExternalAuditEventDestination/1",
        eventTypeFilters: ["list of event type filters"]}){
        errors
        eventTypeFilters
    }
}
```

在以下情况，添加事件类型过滤器：

- 返回的 `errors` 对象为空。
- API 响应为 `200 OK`。

### 使用 API 删除事件类型过滤器

先决条件：

- 您必须具有群组的所有者角色。

您可以使用 `auditEventsStreamingDestinationEventsRemove` 查询类型，删除事件类型过滤器列表：

```graphql
mutation {
    auditEventsStreamingDestinationEventsRemove(input: {
    destinationId: "gid://gitlab/AuditEvents::ExternalAuditEventDestination/1",
    eventTypeFilters: ["list of event type filters"]
  }){
    errors
  }
}
```

如果出现以下情况，将删除事件类型过滤器：

- 返回的 `errors` 对象为空。
- API 响应为 `200 OK`。

