---
stage: Govern
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 审计事件流 **(ULTIMATE)**

> - UI 引入于 14.9 版本。
> - 子组事件记录引入于 15.2 版本。
> - 自定义 HTTP headers UI 引入于 15.2 版本，[功能标志](../feature_flags.md)为 `custom_headers_streaming_audit_events_ui`。默认禁用。
> - 自定义 HTTP headers UI 一般可用于 15.3 版本。删除功能标志 `custom_headers_streaming_audit_events_ui`。
> - 提升用户体验于 15.3 版本。

用户可以为顶级群组或实例设置流传输目标，以结构化 JSON 形式接收有关该群组、子组和项目的所有审计事件。

顶级群组所有者和实例管理员可以在第三方系统中管理其审计日志。任何可以接收结构化 JSON 数据的服务都可以用作流传输目的地。

每个流传输目的地最多可以有 20 个包含在每个流传输事件中的自定义 HTTP headers。

NOTE:
极狐GitLab 可以将单个事件多次流传输到同一目的地。使用有效负载中的 `id` 键来删除传入数据中的重复数据。

## 添加新的流目的地

将新的流传输目的地添加到顶级群组或整个实例。

WARNING:
流传输目的地接收**所有**审计事件数据，其中可能包括敏感信息。确保您信任流传输目的地。

### 添加顶级群组流目的地

先决条件：

- 具有顶级群组的所有者角色。

要将流传输目的地添加到顶级群组：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的群组。
1. 选择 **安全 > 审计事件**。
1. 在主区域中，选择 **事件流** 选项卡。
1. 选择 **添加流目的地** 以显示添加目的地部分。
1. 输入要添加的目标 URL。
1. 可选。找到 **自定义 HTTP header** 表。
1. 忽略目前无效的 **启用** 复选框。
1. 选择 **添加 header** 创建新的名称和值对。根据需要输入任意数量的名称和值对。每个流传输目的地最多可以添加 20 个 headers。
1. 填写完所有 headers 后，选择 **添加** 可以添加新的流目的地。

### 添加实例级流目的地

> 引入于 16.1 版本，[功能标志](../feature_flags.md)为 `instance_streaming_audit_events`。默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，需要管理员[启用功能标志](../feature_flags.md) `instance_streaming_audit_events`。在 SaaS 版上，此功能不可用。此功能尚未准备好用于生产。

先决条件：

- 具有实例的管理员访问权限。

要为实例添加流传目的地：

1. 在左侧边栏上，展开最顶部的向下箭头（**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **监控 > 审计事件**。
1. 在主区域中，选择 **事件流** 选项卡。
1. 选择 **添加流目的地**，显示添加目的地部分。
1. 输入要添加的目标 URL。
1. 选择 **添加** 可以添加新的流目的地。

<a id="list-streaming-destinations"></a>

## 列出流目的地

列出顶级群组或整个实例的流目的地。

### 列出顶级群组流目的地

先决条件：

- 具有顶级群组的所有者角色。

要列出顶级群组的流目的地：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的群组。
1. 选择 **安全 > 审计事件**。
1. 在主区域中，选择 **事件流** 选项卡。
1. 在该项目的右侧，选择 **编辑** (**{pencil}**) 可以查看所有自定义 HTTP headers。

### 列出实例级流目的地

> 引入于 16.1 版本，[功能标志](../feature_flags.md)为 `instance_streaming_audit_events`。默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，需要管理员[启用功能标志](../feature_flags.md) `instance_streaming_audit_events`。在 SaaS 版上，此功能不可用。此功能尚未准备好用于生产。

先决条件：

- 具有实例的管理员访问权限。

要列出实例的流目的地：

1. 在左侧边栏上，展开最顶部的向下箭头（**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **监控 > 审计事件**。
1. 在主区域中，选择 **事件流** 选项卡。

<a id="update-streaming-destinations"></a>

## 更新流目的地

更新顶级群组或整个实例的流目的地。

### 更新顶级群组流目的地

先决条件：

- 具有顶级群组的所有者角色。

要更新顶级群组的流目的地：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的群组。
1. 选择 **安全 > 审计事件**。
1. 在主区域中，选择 **事件流** 选项卡。
1. 在项目右侧，选择 **编辑**（**{pencil}**）。
1. 找到 **自定义 HTTP headers** 表。
1. 找到您要更新的 header。
1. 忽略目前无效的 **启用** 复选框。
1. 选择 **添加 header** 创建新的名称和值对。根据需要输入任意数量的名称和值对。每个流传输目的地最多可以添加 20 个 headers。
1. 选择 **保存** 可以更新流目的地。

## 删除流目的地

删除顶级群组或整个实例的流目的地。成功删除最后一个目的地后，该群组或实例的流传输将被禁用。

### 删除顶级群组流目的地

先决条件：

- 具有顶级群组的所有者角色。

要删除流目的地：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的群组。
1. 选择 **安全 > 审计事件**。
1. 在主区域中，选择 **事件流** 选项卡。
1. 在项目右侧，选择 **删除** (**{remove}**)。

要仅删除流目的地的自定义 HTTP headers：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的群组。
1. 选择 **安全 > 审计事件**。
1. 在主区域中，选择 **事件流** 选项卡。
1. 在项目右侧，选择 **编辑**（**{pencil}**）。
1. 找到 **自定义 HTTP headers** 表。
1. 找到您要删除的 header。
1. 在 header 右侧，选择 **删除**（**{remove}**）。
1. 选择 **保存** 可以更新流目的地。

### 删除实例级流目的地

> 引入于 16.1 版本，[功能标志](../feature_flags.md)为 `instance_streaming_audit_events`。默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，需要管理员[启用功能标志](../feature_flags.md) `instance_streaming_audit_events`。在 SaaS 版上，此功能不可用。此功能尚未准备好用于生产。

先决条件：

- 具有实例的管理员访问权限。

要删除实例的流目的地：

1. 在左侧边栏上，展开最顶部的向下箭头（**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **监控 > 审计事件**。
1. 在主区域中，选择 **事件流** 选项卡。
1. 在项目右侧，选择 **删除**（**{remove}**）。

## 验证事件真实性

> 引入于 14.8 版本。

每个流目的地都有一个唯一的验证令牌（`verificationToken`），可用于验证事件的真实性。该令牌要么由所有者指定，要么在创建事件目标时自动生成，并且无法更改。

每个流式传输事件都在 `X-Gitlab-Event-Streaming-Token` HTTP header 中包含验证令牌，可以在[列出流目的地](#list-streaming-destinations)时根据目标值进行验证。

### 验证顶级群组流目的地

> 引入于 15.2 版本。

先决条件：

- 具有顶级群组的所有者角色。

要列出流目的地并查看验证令牌：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的群组。
1. 选择 **安全 > 审计事件**。
1. 在主区域中，选择 **事件流** 选项卡。
1. 查看每个项目右侧的验证令牌。

### 验证实例级流目的地

> 引入于 16.1 版本，[功能标志](../feature_flags.md)为 `instance_streaming_audit_events`。默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，需要管理员[启用功能标志](../feature_flags.md) `instance_streaming_audit_events`。在 SaaS 版上，此功能不可用。此功能尚未准备好用于生产。

先决条件：

- 具有实例的管理员访问权限。

要列出实例的流目的地并查看验证令牌：

1. 在左侧边栏上，展开最顶部的向下箭头（**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **监控 > 审计事件**。
1. 在主区域中，选择 **事件流** 选项卡。
1. 查看每项右侧的验证令牌。

## 覆盖默认内容类型 header

默认情况下，流目的地使用 `application/x-www-form-urlencoded` 的 `content-type` header。但是，您可能希望将 `content-type` header 设置为其他内容。例如，`application/json`。

要覆盖顶级群组流目的地或实例级流目的地的 `content-type` header 默认值，请使用 [UI](#update-streaming-destinations) 或使用 [GraphQL API](graphql_api.md#update-streaming-destinations)。

## 负载 schema

> 文档引入于 15.3 版本。

| 字段            | 描述                                                | 备注                                                                             |
|------------------|------------------------------------------------------------|-----------------------------------------------------------------------------------|
| `author_id`      | 触发事件的用户的用户 ID               |                                                                                   |
| `author_name`    | 触发事件的作者的可读名称 | 当作者不再存在时很有帮助                                          |
| `created_at`     | 触发事件的时间戳                         |                                                                                   |
| `details`        | 包含附加元数据的 JSON 对象                 | 没有定义的架构，但通常包含有关事件的附加信息    |
| `entity_id`      | 审计事件实体的 ID                             |                                                                                   |
| `entity_path`    | 受可审计事件影响的实体的完整路径   |                                                                                   |
| `entity_type`    | 实体类型的字符串表示                | 可接受的值包括 `User`、`Group` 和 `Key` 等。 |
| `event_type`     | 审计事件类型的字符串表示           |                                                                                   |
| `id`             | 审计事件的唯一标识符                     | 如果需要，可用于重复数据删除                                         |
| `ip_address`     | 用于触发事件的主机 IP 地址           |                                                                                   |
| `target_details` | 有关目标的其他详细信息                        |                                                                                   |
| `target_id`      | 审计事件目标的 ID                             |                                                                                   |
| `target_type`    | 目标类型的字符串表示                 |                                                                                   |

### JSON 负载 schema

```json
{
  "properties": {
    "id": {
      "type": "string"
    },
    "author_id": {
      "type": "integer"
    },
    "author_name": {
      "type": "string"
    },
    "details": {},
    "ip_address": {
      "type": "string"
    },
    "entity_id": {
      "type": "integer"
    },
    "entity_path": {
      "type": "string"
    },
    "entity_type": {
      "type": "string"
    },
    "event_type": {
      "type": "string"
    },
    "target_id": {
      "type": "integer"
    },
    "target_type": {
      "type": "string"
    },
    "target_details": {
      "type": "string"
    },
  },
  "type": "object"
}
```
