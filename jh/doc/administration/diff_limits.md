---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 差异限制管理 **(FREE SELF)**

您可以设置显示差异文件（补丁）的最大大小。

有关差异文件的详细信息，查看文件之间的更改。

<!--阅读更多关于[合并请求和差异的内置限制](../../administration/instance_limits.md#merge-requests)。-->

## 配置差异限制

WARNING:
这些设置是实验性的。增加的最大值会增加实例的资源消耗。调整最大值时请记住这一点。

为了加快实例上合并请求视图和分支比较视图的加载时间，您可以为差异配置三个实例级别的最大值：

| 值 | 定义 | 默认值 | 最大值 |
| ----- | ---------- | :-----------: | :-----------: |
| **最大差异补丁大小** | 整个差异的总大小（以字节为单位）。 | 200 KB | 500 KB |
| **差异中的最大文件数** | 差异中更改的文件总数。 | 1000 | 3000 |
| **差异中的最大行数** | 差异中更改的总行数。 | 50,000 | 100,000 |

当差异达到任何这些值的 10% 时，文件将显示在折叠视图中，并带有一个扩展差异的链接。超过任何设置值的差异显示为 **Too large**，无法在 UI 中展开。

要配置这些值：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **差异限制**。
1. 输入差异限制值。
1. 选择 **保存修改**。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
