---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 标记管理 **(FREE SELF)**

要管理极狐GitLab 实例的标记，请从管理中心侧边栏中选择 **标记** (**{labels}**)。有关如何管理标记的更多详细信息，请参阅[标记文档](../user/project/labels.md)。

在管理中心创建的标记会自动添加到新项目中，但在新建的群组中不可用。
在管理中心更新或添加标记不会修改现有项目中的标记。

![Default label set](img/admin_labels_v14_7.png)

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
