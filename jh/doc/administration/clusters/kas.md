---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 安装适用于 Kubernetes 的极狐GitLab 代理服务器（KAS） **(FREE SELF)**

> - 引入于 13.10 版本。
> - 从专业版移动到免费版于 14.5 版本。

代理服务器是与极狐GitLab 一起安装的组件，对于管理 Kubernetes 的极狐GitLab 代理是必需的。

KAS 作为首字母缩略词，指的是以前的名称，`Kubernetes agent server`。

<!--
The agent server for Kubernetes is installed and available on GitLab.com at `wss://kas.gitlab.com`.
-->

如果您使用私有化部署版极狐GitLab，则必须安装代理服务器或指定外部安装。

## 安装选项

作为极狐GitLab 管理员，您可以为以下类型的实例安装代理服务器：

- [Linux 软件包安装实例](#for-linux-package-installations)。
- [Helm chart 安装实例](#for-gitlab-helm-chart)。

<a id="for-linux-package-installations"></a>

### Linux 软件包安装实例

您可以为单个节点或多个节点上的 Linux 软件包安装实例启用代理服务器。

#### 在单个节点上启用

在单个节点上启用代理服务器：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_kas['enable'] = true
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#reconfigure-a-linux-package-installation)。

有关其它配置选项，请参阅 [`gitlab.rb.template`](https://jihulab.com/gitlab-cn/omnibus-gitlab/-/blob/master/files/gitlab-config-template/gitlab.rb.template) 的 **Enable GitLab KAS** 部分。

##### 配置 KAS 侦听 UNIX 套接字

如果您在代理后面使用极狐GitLab，KAS 可能无法正常工作。您可以在单节点安装实例上解决此问题，您可以将 KAS 配置为侦听 UNIX 套接字。

要将 KAS 配置为侦听 UNIX 套接字：

1. 为 KAS 套接字创建目录：

   ```shell
   sudo mkdir -p /var/opt/gitlab/gitlab-kas/sockets/
   ```

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_kas['internal_api_listen_network'] = 'unix'
   gitlab_kas['internal_api_listen_address'] = '/var/opt/gitlab/gitlab-kas/sockets/internal-api.socket'
   gitlab_kas['private_api_listen_network'] = 'unix'
   gitlab_kas['private_api_listen_address'] = '/var/opt/gitlab/gitlab-kas/sockets/private-api.socket'
   gitlab_kas['env'] = {
     'SSL_CERT_DIR' => "/opt/gitlab/embedded/ssl/certs/",
     'OWN_PRIVATE_API_URL' => 'unix:///var/opt/gitlab/gitlab-kas/sockets/private-api.socket'
   }
   ```

1. [重新配置极狐GitLab](../restart_gitlab.md#reconfigure-a-linux-package-installation)。

#### 在多个节点上启用

在多个节点上启用代理服务器：

1. 对于每个代理服务器节点，编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_kas['enable'] = true
   gitlab_kas['api_secret_key'] = '<32_bytes_long_base64_encoded_value>'
   gitlab_kas['private_api_secret_key'] = '<32_bytes_long_base64_encoded_value>'
   gitlab_kas['private_api_listen_address'] = '0.0.0.0:8155'
   gitlab_kas['env'] = {
     'SSL_CERT_DIR' => "/opt/gitlab/embedded/ssl/certs/",
     'OWN_PRIVATE_API_URL' => 'grpc://<ip_or_hostname_of_this_host>:8155'
   }

   gitlab_rails['gitlab_kas_enabled'] = true
   gitlab_rails['gitlab_kas_external_url'] = 'wss://gitlab.example.com/-/kubernetes-agent/'
   gitlab_rails['gitlab_kas_internal_url'] = 'grpc://kas.internal.gitlab.example.com'
   gitlab_rails['gitlab_kas_external_k8s_proxy_url'] = 'https://gitlab.example.com/-/kubernetes-agent/k8s-proxy/'
   ```

   在此配置中：

   - `gitlab_kas['private_api_listen_address']` 是代理服务器监听的地址。您可以将其设置为 `0.0.0.0` 或集群中其他节点可访问的 IP 地址。
   - `OWN_PRIVATE_API_URL` 是 KAS 进程用于服务发现的环境变量。您可以将其设置为正在配置的节点的主机名或 IP 地址。集群中的其他节点必须可以访问该节点。
   - `gitlab_kas['api_secret_key']` 是用于 KAS 和极狐GitLab 之间身份验证的共享密钥。此值必须采用 Base64 编码且长度正好为 32 个字节。
   - `gitlab_kas['private_api_secret_key']` 是用于不同 KAS 实例之间身份验证的共享密钥。此值必须采用 Base64 编码且长度正好为 32 个字节。
   - `gitlab_rails['gitlab_kas_external_url']` 是集群内 `agentk` 面向用户的 URL。
   - `gitlab_rails['gitlab_kas_internal_url']` 是极狐GitLab 后端用来与 KAS 通信的内部 URL。
   - `gitlab_rails['gitlab_kas_external_k8s_proxy_url']` 是 Kubernetes API 代理的面向用户的 URL。

1. [重新配置极狐GitLab](../restart_gitlab.md#reconfigure-a-linux-package-installation)。

<a id="for-gitlab-helm-chart"></a>

### Helm Chart 安装实例

对于 [Helm Chart](https://docs.gitlab.cn/charts/) 安装实例：

1. 将 `global.kas.enabled` 设置为 `true`。例如，在安装了 `helm` 和 `kubectl` 的 shell 中，运行：

   ```shell
   helm repo add gitlab-jh https://charts.gitlab.cn
   helm repo update
   helm upgrade --install gitlab gitlab-jh/gitlab \
     --timeout 600s \
     --set global.hosts.domain=<YOUR_DOMAIN> \
     --set global.hosts.externalIP=<YOUR_IP> \
     --set certmanager-issuer.email=<YOUR_EMAIL> \
     --set global.kas.enabled=true # <-- without this setting, the agent server will not be installed
   ```

1. 要配置代理服务器，请使用 `values.yaml` 文件中的 `gitlab.kas` 子部分：

   ```yaml
   gitlab:
     kas:
       # put your custom options here
   ```

有关详细信息，请参阅[如何使用 KAS chart](https://docs.gitlab.cn/charts/charts/gitlab/kas/)。

## 故障排除

如果您在使用 Kubernetes 代理服务器时遇到问题，请运行以下命令查看服务日志：

```shell
kubectl logs -f -l=app=kas -n <YOUR-GITLAB-NAMESPACE>
```

在 Omnibus GitLab 中，在 `/var/log/gitlab/gitlab-kas/` 中找到日志。

<!--
You can also [troubleshoot issues with individual agents](../../user/clusters/agent/troubleshooting.md).
-->

### GitOps: failed to get project information

如果您收到以下错误消息：

```json
{"level":"warn","time":"2020-10-30T08:37:26.123Z","msg":"GitOps: failed to get project info","agent_id":4,"project_id":"root/kas-manifest001","error":"error kind: 0; status: 404"}
```

Manifest (`root/kas-manifest001`) 指定的项目不存在，或者 manifest 所在的项目是私有的。要解决此问题，请确保项目路径正确且项目的可见性为[设置为公开](../../user/public_access.md)。

### 未找到配置文件

如果您收到以下错误消息：

```plaintext
time="2020-10-29T04:44:14Z" level=warning msg="Config: failed to fetch" agent_id=2 error="configuration file not found: \".gitlab/agents/test-agent/config.yaml\
```

路径不正确：

- 代理注册的仓库。
- 代理配置文件。

要解决此问题，请确保路径正确。

### `dial tcp <GITLAB_INTERNAL_IP>:443: connect: connection refused`

如果您正在私有化部署极狐GitLab 实例，并且：

- 实例未在 SSL 终止代理后面运行。
- 该实例没有在极狐GitLab 实例本身上配置 HTTPS。
- 实例的主机名在本地解析为其内部 IP 地址。

当代理服务器尝试连接极狐GitLab API 时，可能会出现以下错误：

```json
{"level":"error","time":"2021-08-16T14:56:47.289Z","msg":"GetAgentInfo()","correlation_id":"01FD7QE35RXXXX8R47WZFBAXTN","grpc_service":"gitlab.agent.reverse_tunnel.rpc.ReverseTunnel","grpc_method":"Connect","error":"Get \"https://gitlab.example.com/api/v4/internal/kubernetes/agent_info\": dial tcp 172.17.0.4:443: connect: connection refused"}
```

要为 [Omnibus](https://docs.gitlab.cn/omnibus/) 软件包安装解决此问题，请在 `/etc/gitlab/gitlab.rb` 中设置以下参数。将 `gitlab.example.com` 替换为您的极狐GitLab 实例的主机名：

```ruby
gitlab_kas['gitlab_address'] = 'http://gitlab.example.com'
```
