---
stage: Manage
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用趋势 **(FREE SELF)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/235754) in GitLab 13.5 behind a feature flag, disabled by default.
> - [Became enabled by default](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/46962) in GitLab 13.6.
> - [Renamed](https://gitlab.com/gitlab-org/gitlab/-/issues/285220) from Instance Statistics to Usage Trends in GitLab 13.6.
> - It's enabled on GitLab.com.
> - It's recommended for production use.
-->

使用趋势让您大致了解您的实例包含多少数据，以及该数据量随时间变化的速度。
使用趋势数据每天刷新。

## 查看使用趋势

查看使用趋势：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏中，选择 **分析 > 使用趋势**。

## 总计数

在页面顶部，使用趋势显示以下总计数：

- 用户
- 项目
- 群组
- 议题
- 合并请求
- 流水线

这些数字有助于了解您的实例总共包含多少数据。

## 往年趋势图

使用趋势还显示折线图，在总计数中的显示类别中，显示过去 12 个月中每月的总计数。

这些图表可帮助您直观地了解在您的实例上创建这些记录的速度。

![Instance Activity Pipelines chart](img/instance_activity_pipelines_chart_v13_6_a.png)
