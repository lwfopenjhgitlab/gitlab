---
stage: Manage
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 实例级别分析 **(FREE SELF)**

实例级分析可以深入了解整个实例的功能和数据使用情况。

## 查看实例级分析

先决条件：

- 您必须具有实例的管理员访问权限。

要查看实例级分析：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏中，选择 **分析**。

   - [DevOps 报告](dev_ops_reports.md)：概述整个实例的功能使用情况。
   - [使用趋势](usage_trends.md)：显示您的实例包含多少数据，以及数据如何变化。
