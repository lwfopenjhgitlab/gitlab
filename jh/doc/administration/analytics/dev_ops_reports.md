---
stage: Manage
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# DevOps 报告 **(FREE SELF)**

DevOps 报告为您提供从规划到监控，整个实例采用并发 DevOps 的概览。

要查看 DevOps 报告：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **分析 > DevOps 报告**。

<!--
## DevOps 分数

NOTE:
要查看 DevOps 分数，您必须激活极狐GitLab 实例的 [Service Ping](../settings/usage_statistics.md#service-ping)。DevOps 分数是一种比较工具，因此您的分数数据必须首先由 GitLab Inc. 集中处理。

You can use the DevOps score to compare your DevOps status to other organizations.

The DevOps Score tab displays usage of major GitLab features on your instance over
the last 30 days, averaged over the number of billable users in that time period.
You can also see the Leader usage score, calculated from top-performing instances based on
[Service Ping data](../settings/usage_statistics.md#service-ping) that GitLab has collected.
Your score is compared to the lead score of each feature and then expressed
as a percentage at the bottom of said feature. Your overall **DevOps Score** is an average of your
feature scores.

Service Ping data is aggregated on GitLab servers for analysis. Your usage
information is **not sent** to any other GitLab instances.
If you have just started using GitLab, it might take a few weeks for data to be collected before this
feature is available.
-->

## DevOps Adoption **(ULTIMATE SELF)**

> - 引入于 13.7 版本，作为 Beta 功能
> - 概览选项卡引入于 14.1 版本
> - DAST 和 SAST 指标添加于 14.1 版本
> - 模糊测试指标添加于 14.2 版本
> - 依赖项扫描指标添加于 14.2 版本
> - 多选添加于 14.2 版本
> - 概览表格添加于 14.3 版本

DevOps Adoption 展示了开发、安全和运营方面的功能采用情况。

| 类别 | 功能 |
| ---      | ---      |
| 开发   | 批准<br>代码所有者<br>议题<br>合并请求   |
| 安全  | DAST<br>依赖项扫描<br>模糊测试<br>SAST  |
| 运营   | 部署<br>流水线<br>Runners   |

您可以使用群组 DevOps Adoption：

- 确定在采用极狐GitLab 功能方面滞后的特定子组，以便您可以指导他们进行 DevOps 之旅。
- 找到采用某些功能的子组，并指导其它子组如何使用这些功能。
- 验证您是否从极狐GitLab 获得了预期的投资回报。

## 添加或移除群组

要从 DevOps Adoption 报告中添加或删除子组：

1. 选择 **添加或删除群组**。
1. 选择您要添加或删除的子组，然后选择 **保存修改**。

![DevOps Adoption](img/admin_devops_adoption_v14_2.png)
