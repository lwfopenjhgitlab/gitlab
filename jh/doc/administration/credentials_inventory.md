---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# 凭据库 **(ULTIMATE SELF)**

> - 于 14.9 版本，机器人创建的访问令牌未显示在个人访问令牌列表中。

极狐GitLab 管理员负责其实例的整体安全性。为了提供帮助，极狐GitLab 提供了凭据库，跟踪可用于访问私有化部署版实例的所有凭据。

使用凭据库查看您的实例的所有以下内容：

- 个人访问令牌 (PAT)。
- 项目访问令牌（14.8 及更高版本）。
- SSH 密钥。
- GPG 密钥。

您也可以[撤销](#revoke-a-users-personal-access-token)和[删除](#delete-a-users-ssh-key)，支持查看：

- 它们属于谁
- 它们的访问范围
- 它们的使用模式
- 它们何时到期时
- 它们何时被撤销

要访问凭据库：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **凭据**。

<a id="revoke-a-users-personal-access-token"></a>

## 撤销用户的个人访问令牌

> 引入于 13.4 版本

如果您看到 **撤销** 按钮，您可以撤销该用户的 PAT。您是否看到 **Revoke** 按钮取决于令牌状态，以及是否设置了到期日期。有关详细信息，请参阅下表：

| 令牌状态 |  显示撤销按钮？ | 说明 |
| ------------- | -------------- | ------------- |
| Active      |  Yes                | 允许管理员撤销 PAT，例如针对风险帐户 |
| Expired     |  No                 | 令牌不适用；令牌已过期             |
| Revoked     |  No                 | 令牌不适用；令牌已被撤销            |

当 PAT 从凭据库中撤销时，实例会通过电子邮件通知用户。

![Credentials inventory page - Personal access tokens](img/credentials_inventory_personal_access_tokens_v14_9.png)

## 撤销用户的项目访问令牌

> 引入于 14.8 版本。

可以选择项目访问令牌旁边的 **撤销** 按钮，来撤销特定项目访问令牌：

- 撤销令牌项目的访问令牌。
- 将后台 woker 排入队列，用来删除项目机器人用户。

![Credentials inventory page - Project access tokens](img/credentials_inventory_project_access_tokens_v14_9.png)

<a id="delete-a-users-ssh-key"></a>

## 删除用户的 SSH 密钥

> 引入于 13.5 版本

您可以通过导航到凭证清单的 SSH 密钥选项卡，来**删除**用户的 SSH 密钥。
然后实例会通知用户。

![Credentials inventory page - SSH keys](img/credentials_inventory_ssh_keys_v14_9.png)

## 查看现有的 GPG 密钥

> - 引入于 13.10 版本。
> - 功能标志移除于 13.12 版本。

您可以通过导航到凭据库 GPG 密钥选项卡，来查看实例中的所有现有 GPG 以及以下属性：

- GPG 密钥属于谁。
- GPG 密钥的 ID。
- GPG 密钥[已验证或未验证](../user/project/repository/gpg_signed_commits/index.md)

![Credentials inventory page - GPG keys](img/credentials_inventory_gpg_keys_v14_9.png)
