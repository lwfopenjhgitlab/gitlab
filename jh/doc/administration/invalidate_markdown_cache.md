---
stage: Plan
group: Project Management
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# Markdown 缓存 **(FREE)**

出于性能原因，极狐GitLab 在以下字段中缓存 HTML 版本的 Markdown 文本：

- 评论。
- 议题描述。
- 合并请求描述。

这些缓存版本可能会过时，例如在更改 `external_url` 配置选项时。缓存文本中的链接仍将引用旧 URL。

## 使缓存无效

先决条件：

- 您必须是管理员。

为避免缓存 HTML 版本引起的问题，请通过增加应用程序设置中的 `local_markdown_version` 设置，[使用 API](../api/settings.md#change-application-settings) 使现有缓存无效：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/application/settings?local_markdown_version=<increased_number>"
```
