---
stage: Manage
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 非活跃项目删除 **(FREE SELF)**

> - 引入于 15.0 版本，[功能标志](../administration/feature_flags.md)为 `inactive_projects_deletion`。默认禁用。
> - 通过 UI 进行配置的功能引入于 15.1 版本。
> - 功能标志 `inactive_projects_deletion` 删除于 15.4 版本。

大型极狐GitLab 实例的管理员会发现，随着时间的推移，项目会变得不活跃并且不再使用。
这些项目占用了不必要的磁盘空间。
通过非活跃项目删除，您可以识别这些项目，提前警告维护人员，然后在项目仍然处于非活跃状态时将其删除。删除非活跃项目时，该操作会生成由  @GitLab-Admin-Bot 执行的审计事件。

## 配置非活跃项目删除

要配置非活跃项目：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **仓库维护**。
1. 在 **非活跃项目删除** 部分，选择 **删除不活跃的项目**。
1. 配置以下设置。
   - 警告电子邮件将发送给在不活跃的项目中，具有所有者和维护者角色的用户。
   - 提前发送电子邮件的时间必须小于 **删除项目前的非活跃时长**。
1. 选择 **保存更改**。

符合条件的不活跃项目将被安排删除并发送警告电子邮件。如果项目保持不活跃状态，它们将在指定的时长后被删除。

### 配置示例

如果您使用以下设置：

- 已启用**删除不活跃的项目**。
- **删除超过时限的不活跃项目**设置为 `50`。
- **删除项目前的非活跃时长**设置为 `12`。
- **发送警告电子邮件**设置为 `6`。

如果项目小于 50 MB，则该项目不会被视为不活跃。

如果一个项目超过 50 MB，并且它在以下情况下处于不活跃状态：

- 超过 6 个月：发送删除警告电子邮件。此邮件包含项目将被删除的日期。
- 超过 12 个月：该项目按计划删除。

## 确定项目上次活动的时间

您可以通过以下方式查看项目的动态并确定项目上次活动的时间：

- 前往项目的[动态页面](../user/project/working_with_projects.md#view-project-activity)，查看最新活动日期。
- 使用项目 API<!--[项目 API](../api/projects.md)-->，查看项目的 `last_activity_at` 属性。
- 使用[事件 API](../api/events.md#list-a-projects-visible-events)，列出项目的可见事件。查看最新事件的 `created_at` 属性。
