---
stage: Create
group: Gitaly
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# Gitaly 超时 **(FREE SELF)**

Gitaly 超时是可配置的。可以配置超时，以免长时间运行的 Gitaly 调用不必要地占用资源。

要访问 Gitaly 超时设置：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **Gitaly 超时** 部分。

## 可用的超时

以下超时可用。

| 超时 | 默认    | 描述                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
|:--------|:-----------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Default | 55 seconds | 大多数 Gitaly 调用超时（对于 `git` `fetch` 和 `push` 操作或 Sidekiq 作业不强制）。例如，检查磁盘上是否存在仓库。确保在 Web 请求中进行的 Gitaly 调用不能超过整个请求超时。 它应该比可以为 Puma 配置的 [worker 超时](../../../install/requirements.md#puma-settings)短。如果 Gitaly 调用超时超过 worker 超时，则使用 worker 超时的剩余时间来避免不得不终止 worker。 |
| Fast    | 10 seconds | 请求中使用的快速 Gitaly 操作超时，有时是多次。例如，检查磁盘上是否存在仓库。如果快速操作超过此阈值，则存储分片可能存在问题。快速失败有助于保持极狐GitLab 实例的稳定性。                                                                                                                                                                                                                                                                        |
| Medium  | 30 seconds | 应该很快的 Gitaly 操作的超时（可能在请求中），但最好不要在请求中多次使用。例如，加载 blob，应该在 Default 和 Fast 之间设置。                                                                                                                                                                                                                                                                                                                                                      |
