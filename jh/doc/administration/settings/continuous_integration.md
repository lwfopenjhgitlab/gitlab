---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 持续集成和部署管理中心设置 **(FREE SELF)**

管理中心包含 Auto DevOps、runner 和作业产物的实例设置。

## Auto DevOps

要为所有项目启用（或禁用）Auto DevOps：<!--[Auto DevOps](../../../topics/autodevops/index.md)-->

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 选中（或取消选中以禁用）显示 **所有项目默认使用 Auto DevOps 流水线** 的复选框。
1. （可选）设置用于 Auto Deploy 和 Auto Review Apps 的 Auto DevOps base domain<!--[Auto DevOps base domain](../../../topics/autodevops/requirements.md#auto-devops-base-domain)-->。
1. 点击 **保存修改**，使更改生效。

从现在开始，每个现有项目和新创建的没有 `.gitlab-ci.yml` 的项目都使用 Auto DevOps 流水线。

如果您想为特定项目禁用它，您可以在设置中操作。

<a id="enable-shared-runners-for-new-projects"></a>

## 为新项目启用共享 runners

您可以将所有新项目设置为在默认情况下，使实例的共享 runners 可用。

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 展开 **持续集成和部署**。
1. 选中 **为新项目启用共享 Runner** 复选框。

任何时候创建新项目时，共享 runners 都可用。

## 共享 runners 计算单位

作为管理员，您可以为可使用的[计算分钟数](../../ci/pipelines/cicd_minutes.md)设置全局或特定于命名空间的限制。

<a id="enable-a-specific-runner-for-multiple-projects"></a>

## 为多个项目启用指定 runner

如果您已经注册了一个[指定 runner](../../ci/runners/runners_scope.md#specific-runners)，您可以将该 runner 分配给其他项目。

为多个项目启用指定 runner：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 从左侧边栏中，选择 **CI/CD > Runners**。
1. 选择您要编辑的 runner。
1. 在右上角，选择 **编辑** (**{pencil}**)。
1. 在 **限制适用于该 runner 的项目** 下，搜索一个项目。
1. 在项目左侧，选择 **启用**。
1. 对每个其它更多的项目重复此过程。

## 为共享 runners 添加信息

要在所有项目的 runner 设置中显示有关实例共享 runner 的详细信息：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 扩展 **持续集成和部署**。
1. 输入文本，如果需要，包括 Markdown，在 **共享 Runner 的详细信息** 字段中输入您的共享 runner 详细信息。

   ![Shared runner details input](img/continuous_integration_shared_runner_details_input_v14_10.png)

查看渲染细节：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的项目或群组。
1. 选择 **设置 > CI/CD**。
1. 展开 **Runners**。

![Shared runner details example](img/continuous_integration_shared_runner_details_v14_10.png)

<a id="maximum-artifacts-size"></a>

## 最大产物大小

[作业产物](../../administration/job_artifacts.md)的最大大小可以在以下级别设置：

- 实例级别
- 从 12.4 版本，在项目和群组级别。

<!--
有关 GitLab.com 上的设置，请参阅 [Artifacts maximum size](../../gitlab_com/index.md#gitlab-cicd)。
-->
该值以 MB 为单位，默认值为每个作业 100MB。在以下位置更改它：

- 实例级别：

   1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
   1. 选择 **管理中心**。
   1. 在左侧边栏上，选择 **设置 > CI/CD**。
   1. 更改最大产物大小的值（以 MB 为单位）。
   1. 单击 **保存修改**，使更改生效。

- 群组级别（覆盖实例设置）：

   1. 转到群组的 **设置 > CI/CD > 流水线通用设置**。
   1. 更改 **最大产物大小（以 MB 为单位）** 的值。
   1. 单击 **保存修改**，使更改生效。

- 项目级别（覆盖实例和群组设置）：

  1. 转到项目的 **设置 > CI/CD > 流水线通用设置**。
  1. 更改 **最大产物大小（以 MB 为单位）** 的值。
  1. 单击 **保存修改**，使更改生效。

NOTE:
所有级别的设置仅对极狐GitLab 管理员可用。

<a id="default-artifacts-expiration"></a>

## 默认产物过期

<!--[作业产物](../../../administration/job_artifacts.md)-->作业产物的默认过期时间可以在极狐GitLab 实例的管理中心中设置。持续时间的语法在 [`artifacts:expire_in`](../../ci/yaml/index.md#artifactsexpire_in) 中有描述，默认值为 `30 days`。

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 修改默认过期时间的值。
1. 单击 **保存修改**，使更改生效。

这个设置是为每个作业设置的，可以在 [`.gitlab-ci.yml`](../../ci/yaml/index.md#artifactsexpire_in) 中覆盖。
要禁用过期，请将其设置为 `0`。默认单位为秒。

NOTE:
对此设置的任何更改仅适用于新产物。对于在更改此设置之前创建的产物，不会更新到期时间。

<!--
The administrator may need to manually search for and expire previously-created
artifacts, as described in the [troubleshooting documentation](../../../administration/troubleshooting/gitlab_rails_cheat_sheet.md#remove-artifacts-more-than-a-week-old).
-->

<a id="keep-the-latest-artifacts-for-all-jobs-in-the-latest-successful-pipelines"></a>

## 保留最新成功流水线中所有作业的最新产物

> 引入于 13.9 版本

启用（默认）后，每个 Git 引用（[分支和标签](https://git-scm.com/book/en/v2/Git-Internals-Git-References)）的最新流水线的产物是锁定防止删除，且无论到期时间如何均保留。

禁用时，允许任何**新的**成功流水线或固定流水线的最新产物过期。

此设置优先于[项目级别设置](../../ci/jobs/job_artifacts.md#keep-artifacts-from-most-recent-successful-jobs)。
如果在实例级别禁用，则无法为每个项目启用此功能。

要禁用此设置：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 展开 **持续集成和部署**。
1. 清除 **在最新成功的流水线中，保留所有作业的最新产物** 复选框。
1. 点击 **保存修改**。

当您禁用该功能时，最新的产物不会立即过期。
在最新的产物过期并被删除之前，必须运行新的流水线。

NOTE:
所有应用程序设置都有一个可自定义的缓存到期间隔<!--[可自定义的缓存到期间隔](../../../administration/application_settings_cache.md)-->，这可以延迟设置的影响。

<a id="archive-jobs"></a>

## 归档作业

归档作业通过删除作业的某些功能（存储在数据库中运行作业所需的元数据），但保留跟踪和产物以用于审计目的，有助于减少系统上的 CI/CD 占用空间。

设置作业被视为旧的和过期的持续时间：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 展开 **持续集成和部署** 部分。
1. 设置 **存档作业** 的值。
1. 点击 **保存修改** 以使更改生效。

这段时间过后，作业将在后台被存档，无法再重试。使其为空可以永不过期作业。必须不少于1天，例如：<code>15 days</code>、<code>1个月</code>、<code>2 years</code>。

<!--
As of June 22, 2020 the [value is set](../../gitlab_com/index.md#gitlab-cicd) to 3 months on GitLab.com. Jobs created before that date were archived after September 22, 2020.
-->

## 默认保护 CI/CD 变量

将所有新的 [CI/CD 变量](../../../ci/variables/index.md) 设置为默认情况下[受保护](../../../ci/variables/index.md#protect-a-cicd-variable)：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 选择 **默认保护 CI/CD 变量**。

## 最大包含（includes）数量

> 引入于 16.0 版本。

您可以在实例级别设置每个流水线的最大包含（includes）数量。默认值为 `150`。

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 更改 **最大包含（includes）数量**。
1. 选择 **保存更改**，使更改生效。

## 默认 CI/CD 配置文件

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/18073) in GitLab 12.5.
-->

可以在极狐GitLab 实例的管理中心中，设置新项目的默认 CI/CD 配置文件和路径（如果未设置，则为`.gitlab-ci.yml`）：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 在 **默认 CI/CD 配置文件** 字段中输入新文件和路径。
1. 点击 **保存更改**，使更改生效。

也可以为特定项目指定[自定义 CI/CD 配置文件](../../ci/pipelines/settings.md#specify-a-custom-cicd-configuration-file)。

## 设置 CI/CD 限制

> 引入于 14.10 版本

您可以从管理中心配置一些 [CI/CD 限制](../../administration/instance_limits.md#cicd-limits)：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 展开 **持续集成和部署** 部分。
1. 在 **CI/CD 限制** 部分，您可以设置以下限制：
   - **单个流水线中作业的最大数量**
   - **当前活动流水线中的作业总数**
   - **每个项目注册的最大 runner 数量**
   - **一个项目的最大流水线订阅数**
   - **流水线计划的最大数量**
   - **作业可以拥有的 DAG 依赖项的最大数量**
   - **每个群组注册的最大 runner 数量**
   - **每个项目注册的最大 runner 数量**
   - **流水线的层次结构树中下游流水线的最大数量**

## 启用或禁用流水线建议框

默认情况下显示在合并请求中，没有流水线时，如何添加一个的建议。

![Suggest pipeline banner](img/suggest_pipeline_banner_v14_5.png)

要启用或禁用：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 选中或清除 **启用流水线建议提示** 复选框。
1. 选择 **保存修改**。

## 必需的流水线配置 **(ULTIMATE SELF)**

> - 从专业版移动到旗舰版于 15.0 版本。
> - 废弃于 15.9 版本。

WARNING:
此功能废弃于 15.9 版本，计划删除于 17.0。使用[合规流水线功能](../../user/group/compliance_frameworks.md#compliance-pipelines)代替。

您可以将 CI/CD 模板<!--[CI/CD 模板](../../../ci/examples/index.md#cicd-templates)--> 设置为极狐GitLab 实例上所有项目的必需流水线配置。您可以使用以下模板：

- 默认 CI/CD 模板。
- 存储在实例模板仓库<!--[实例模板仓库](instance_template_repository.md)-->中的自定义模板。

  NOTE:
  当您使用实例模板仓库中定义的配置时，nested [`include:`](../../ci/yaml/index.md#include) 关键字（包括 `include:file`、`include :local`、`include:remote` 和 `include:template`) 不起作用。

当流水线运行时，项目 CI/CD 配置合并到所需的流水线配置中。合并后的配置就像所需的流水线配置添加了带有 [`include` 关键字](../../ci/yaml/index.md#include) 的项目配置一样。
要查看项目的完整合并配置，请在流水线编辑器中[查看合并的 YAML](../../ci/pipeline_editor/index.md#view-full-configuration)。

要为所需的流水线配置选择 CI/CD 模板：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 展开 **强制流水线配置** 部分。
1. 从下拉列表中选择 CI/CD 模板。
1. 单击 **保存更改**。

## 软件包库配置

### Maven 转发 **(PREMIUM SELF)**

极狐GitLab 管理员可以禁用将 Maven 请求转发到 [Maven Central](https://search.maven.org/)。

要禁用转发 Maven 请求：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 展开 **软件包库** 部分。
1. 清除复选框 **Forward Maven package requests to the Maven Registry if the packages are not found in the GitLab Package Registry**。
1. 选择 **保存更改**。

### npm 转发 **(PREMIUM SELF)**

极狐GitLab 管理员可以禁用将 npm 请求转发到 [npmjs.com](https://www.npmjs.com/)。

要禁用：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 展开 **软件包库** 部分。
1. 如果在 GitLab 软件包库中找不到包，请清除复选框 **Forward npm package requests to the npm Registry if the packages are not found in the GitLab Package Registry**。
1. 选择 **保存修改**。

### PyPI 转发 **(PREMIUM SELF)**

极狐GitLab 管理员可以禁止将 PyPI 请求转发到 [pypi.org](https://pypi.org/)。

要禁用：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 展开 **软件包库** 部分。
1. 清除复选框 **Forward PyPI package requests to the PyPI Registry if the packages are not found in the GitLab Package Registry**.
1. 选择 **保存修改**。

### 包文件大小限制

极狐GitLab 管理员可以调整每个包类型的最大允许文件大小。

设置最大文件大小：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 展开 **软件包库** 部分。
1. 找到您要调整的包类型。
1. 输入最大文件大小，以字节为单位。
1. 单击 **保存大小限制**。

<a id="restrict-runner-registration-by-all-users-in-an-instance"></a>

## 限制一个实例中所有用户的 runner 注册

> - 引入于 14.1 版本
> - 部署在功能标志 `runner_registration_control` 后，默认禁用
> - 不推荐生产使用
> - 要在极狐GitLab 自助管理实例中使用，请要求极狐GitLab 管理员启用它。

极狐GitLab 管理员可以通过显示和隐藏 UI 区域来调整允许注册 runner 的人。

默认情况下，项目和群组的所有成员都可以注册 runner。

限制实例中的所有用户注册 runner：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 展开 **Runner 注册**。
1. 在 **Runner 注册** 部分，清除 **项目成员可以注册 runners** 和 **群组成员可以注册 runners** 复选框，从 UI 中删除 runner 注册。
1. 选择 **保存更改**。

WARNING:
当注册部分隐藏在 UI 中时，需要注册 runner 的项目或群组的成员必须联系管理员。如果您计划阻止注册，请确保用户可以访问他们运行作业所需的 runners。

## 限制群组内所有成员的 runner 注册

先决条件：

- 必须为[实例中的所有用户](#restrict-runner-registration-by-all-users-in-an-instance)启用 runner 注册。

极狐GitLab 管理员可以调整群组权限，以限制群组成员注册 runner。

限制特定群组成员的 runner 注册：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 群组** 并找到您的群组。
1. 选择 **编辑**。
1. 如果您想要禁用群组中所有成员的 runner 注册，请清除 **可以注册新的群组 runner** 复选框。如果设置为只读，则必须为[实例](#restrict-runner-registration-by-all-users-in-an-instance)启用 runner 注册。
1. 选择 **保存更改**。

## 禁用 runner 版本管理

> 引入于 15.10 版本。

默认情况下，极狐GitLab 实例会定期从 SaaS 获取官方 runner 版本数据，[确定 runner 是否需要升级](../../../ci/runners/runners_scope.md#determine-which-runners-need-to-be-upgraded)。

要禁用您的实例获取此数据：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > CI/CD**。
1. 展开 **Runners**。
1. 在 **Runner 版本管理** 部分，清除 **获取 Runner 发布版本数据** 复选框。
1. 选择 **保存更改**。

## 故障排查

### 413 Request Entity Too Large

当构建作业失败并出现以下错误时，增加[最大产物大小](#maximum-artifacts-size)。

```plaintext
Uploading artifacts as "archive" to coordinator... too large archive <job-id> responseStatus=413 Request Entity Too Large status=413" at end of a build job on pipeline when trying to store artifacts to <object-storage>.
```
