---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 已弃用的 API 速率限制 **(FREE SELF)**

> 引入于 14.4 版本。

不推荐使用的 API 端点是那些已被替代功能替换，但不能在不破坏向后兼容性的情况下删除的端点。
在这些端点上设置限制性速率限制，可以鼓励用户切换到替代方案。

## 已弃用的 API 端点

并非所有已弃用的 API 端点都包含在此速率限制中 - 只是那些可能会对性能产生影响的端点：

- `GET /groups/:id` **没有** `with_projects=0` 查询参数。

## 定义已弃用的 API 速率限制

默认情况下禁用不推荐使用的 API 端点的速率限制。启用后，它们会取代对已弃用端点的请求的一般用户和 IP 速率限制。您可以保留任何一般用户和 IP 速率限制，并增加或减少已弃用 API 端点的速率限制。此覆盖不提供其他新功能。

先决条件：

- 您必须具有实例的管理员访问权限。

要覆盖对已弃用 API 端点的请求的一般用户和 IP 速率限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **已弃用的 API 速率限制**。
1. 选中您要启用的速率限制类型的复选框：
    - **未经身份验证的 API 请求速率限制**
    - **经过身份验证的 API 请求速率限制**
1. *如果您选择了未经身份验证的 API 请求速率限制：*
    1. 选择 **每个 IP 每个速率限制期间的最大未经身份验证的 API 请求数**。
    1. 选择 **未经身份验证的 API 速率限制期（以秒为单位）**。
1. *如果您选择了经过身份验证的 API 请求速率限制：*
    1. 选择 **每个用户每个速率限制期的最大已验证 API 请求数**。
    1. 选择 **已身份验证的 API 速率限制期（以秒为单位）**。

<!--
## Related topics

- [Rate limits](../../../security/rate_limits.md)
- [User and IP rate limits](user_and_ip_rate_limits.md)
-->
