---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# 推送事件活动限制和批量推送事件 **(FREE)**

设置分支或标签的数量，限制一次允许的单个推送事件的数量。如果事件数量大于此数量，系统会改为创建批量推送事件。

例如，如果推送了 4 个分支并且当前限制设置为 3，则动态源将显示：

![Bulk push event](img/bulk_push_event_v12_4.png)

使用此功能，当单个推送包含大量更改（例如，1,000 个分支）时，仅创建 1 个批量推送事件，而不是 1,000 个推送事件。这有助于保持良好的系统性能并防止动态源上的垃圾邮件。

要修改此设置：

- 在管理中心：
  1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
  1. 选择 **管理中心**。
  1. 在左侧边栏，选择 **设置 > 网络**。
  1. 展开 **性能优化**。

<!--
- Through the [Application settings API](../../../api/settings.md#list-of-settings-that-can-be-accessed-via-api-calls)
  as `push_event_activities_limit`.
-->

默认值为 3，但可以大于或等于 0。

![Push event activities limit](img/push_event_activities_limit_v12_4.png)
