---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 受保护路径 **(FREE SELF)**

速率限制是一种提高 Web 应用程序安全性和持久性的技术。<!--有关更多详细信息，请参阅 [速率限制](../../../security/rate_limits.md)。-->

您可以对指定路径进行速率限制（保护）。对于这些路径，系统使用 HTTP 状态代码 `429` 响应受保护路径上的 POST 请求，每个 IP 地址每分钟超过 10 个请求。

例如，以下限制为每分钟最多 10 个请求：

- 用户登录
- 用户注册（如果启用）
- 用户密码重置

10 次请求后，客户端必须等待 60 秒才能重试。 

同时查看：

- 默认保护的[路径列表](../instance_limits.md#by-protected-path)。
- 返回到被阻止请求的标头的[用户和 IP 速率限制](user_and_ip_rate_limits.md#response-headers)。

## 配置受保护路径

默认情况下启用受保护路径的限制，并且可以在 **管理员 > 网络 > 受保护路径** 上禁用或自定义，使用以下选项：

- 每个用户每个期限的最大请求数。
- 速率限制期（以秒为单位）。
- 要保护的路径。

![protected-paths](img/protected_paths.png)

超过速率限制的请求被记录到 `auth.log` 中。
