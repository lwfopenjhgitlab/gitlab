---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

# 极狐GitLab Shell 操作的速率限制 **(FREE SELF)**

> 引入于极狐GitLab 14.7，[功能标志](../administration/feature_flags.md)为 `rate_limit_gitlab_shell`。从 15.8 开始，无需功能标志默认可用。

极狐GitLab 对按用户账户和项目使用 SSH 的 Git 操作应用速率限制。当超过速率限制时，极狐GitLab 会拒绝用户对该项目的进一步连接请求。

速率限制适用于 Git 命令 ([plumbing](https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain)) 级别。
每个命令的速率限制为每分钟 600 次。例如：

- `git push`：速率限制为每分钟 600 次。
- `git pull`：速率限制为每分钟 600 次。

因为相同的命令由 `git-upload-pack`、`git pull` 和 `git clone` 共享，所以它们共享一个速率限制。

私有化部署的极狐GitLab 用户可以禁用此速率限制。

## 配置极狐GitLab Shell 操作限制

`Git operations using SSH` 默认启用，默认为每分钟每用户 600 次。

配置极狐GitLab Shell 操作限制：

1. 在左侧边栏中，选择 **您的工作 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 网络**。
1. 展开 **Git SSH 操作速率限制**。
1. 为 **每分钟最大 Git 操作数** 输入一个值。
1. 选择 **保存更改**。
