---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 原始端点的速率限制 **(FREE SELF)**

此设置默认为每分钟 300 个请求，1 分钟后释放对原始文件的访问。并允许您对原始端点的请求进行速率限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **性能优化**。

<!--
For example, requests over `300` per minute to `https://gitlab.com/gitlab-org/gitlab-foss/raw/master/app/controllers/application_controller.rb` are blocked. Access to the raw file is released after 1 minute.
-->

![Rate limits on raw endpoints](img/rate_limits_on_raw_endpoints.png)

针对此限制：

- 每个项目，每个文件路径独立应用。
- 不能按每个 IP 地址应用。
- 默认激活。要禁用，请将选项设置为 `0`。

超过速率限制的请求被记录到 `auth.log` 中。
