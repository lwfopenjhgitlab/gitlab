---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Federated Learning of Cohorts (FLoC) **(FREE SELF)**

> 引入于 13.12 版本

用户群联合学习 (FLoC) 是 Chrome 浏览器的一项新功能。
它通过将用户分类到不同的群来工作，以便广告商可以使用这些数据来唯一地定位和跟踪用户。有关更多信息，请访问 [FLoC 仓库](https://github.com/WICG/floc)。

为了避免用户在任何极狐GitLab 实例中被跟踪和分类，默认情况下通过发送以下标头禁用 FLoC：

```plaintext
Permissions-Policy: interest-cohort=()
```

要启用：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **Federated Learning of Cohorts（FLOC）**。
1. 选中 **参与 FLoC** 复选框。
1. 单击 **保存更改**。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
