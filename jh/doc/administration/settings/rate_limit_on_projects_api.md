---
type: reference
stage: Data Stores
group: Tenant Scale
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目 API 的速率限制 **(FREE SELF)**

> - 引入于 15.10 版本，功能标志为 `rate_limit_for_unauthenticated_projects_api_access`，默认禁用。
> - 在私有化部署版上默认启用于 16.0 版本。

您可以为[列出所有项目 API](../../api/projects.md#list-all-projects) 的未经身份验证的请求，配置每个 IP 地址的速率限制。

更改速率限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **项目 API 速率限制**。
1. 在 **每个 IP 地址每 10 分钟的最大请求数** 文本框中，输入新值。
1. 选择 **保存更改**。

速率限制：

- 适用于每个 IP 地址。
- 不适用于经过身份验证的请求。
- 可以设置为 0 来禁用速率限制。

速率限制的默认值为 `400`。

超过速率限制的请求将记录到 `auth.log` 文件中。

例如，您将限制设置为 400，则会阻止在 10 分钟内超过 400 速率的对 `GET /projects` API 端点的未经身份验证的请求。十分钟后恢复对端点的访问。
