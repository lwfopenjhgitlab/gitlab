---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 用户和 IP 速率限制 **(FREE SELF)**

速率限制是一种用于提高 Web 应用程序的安全性和持久性的常用技术。<!--For more details, see
[Rate limits](../../../security/rate_limits.md).-->

<!--
The following limits are disabled by default:

- [Unauthenticated API requests (per IP)](#enable-unauthenticated-api-request-rate-limit).
- [Unauthenticated web requests (per IP)](#enable-unauthenticated-web-request-rate-limit).
- [Authenticated API requests (per user)](#enable-authenticated-api-request-rate-limit).
- [Authenticated web requests (per user)](#enable-authenticated-web-request-rate-limit).
-->

以下限制默认禁用。

NOTE:
默认情况下，所有 Git 操作首先尝试未经身份验证。因此，HTTP Git 操作可能会触发为未经身份验证的请求配置的速率限制。

NOTE:
在 14.8 及更高版本中，API 请求的速率限制不会影响前端发出的请求，因为这些请求始终计为 Web 流量。

## 启用未经身份验证的 API 请求速率限制

启用未经身份验证的请求速率限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **用户和 IP 速率限制**。
1. 选择 **启用未经身份验证的 API 请求速率限制**。

   - 可选。更新 **每个 IP 每个速率限制期的最大未经身份验证的 API 请求** 的值。 默认为 `3600`。
   - 可选。更新 **未经身份验证的速率限制期（以秒为单位）** 的值。 默认为 `3600`。

## 启用未经身份验证的 Web 请求速率限制

启用未经身份验证的请求速率限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **用户和 IP 速率限制**。
1. 选择 **启用未经身份验证的 Web 请求速率限制**。

   - 可选。更新 **每个 IP 每个速率限制期的最大未经身份验证的 Web 请求** 的值。默认为 `3600`。
   - 可选。更新 **未经身份验证的速率限制期（以秒为单位）** 的值。默认为 `3600`。

## 启用已身份验证的 API 请求速率限制

启用经过身份验证的 API 请求速率限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **用户和 IP 速率限制**。
1. 选择 **启用已身份验证的 API 请求速率限制**。

   - 可选。更新 **每个用户每个速率限制期的最大已验证 API 请求数** 的值。默认为 `7200`。
   - 可选。更新 **已身份验证的 API 速率限制期（以秒为单位）** 的值。默认为 `3600`。

## 启用经过身份验证的 Web 请求速率限制

启用经过身份验证的请求速率限制

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **用户和 IP 速率限制**。
1. 选择 **启用经过身份验证的 Web 请求速率限制**。

   - 可选。更新 **每个用户每个速率限制期间的最大已验证 Web 请求数** 的值。默认为 `7200`。
   - 可选。更新 **经过身份验证的 Web 速率限制期（以秒为单位）** 的值。默认为 `3600`。

<a id="use-a-custom-rate-limit-response"></a>

## 使用自定义速率限制响应

> 引入于 13.8 版本

超过速率限制的请求会返回 `429` 响应码和纯文本返回体，默认为 `Retry later`。

使用自定义响应：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **用户和 IP 速率限制**。
1. 在 **向达到速率限制的客户发送的纯文本响应** 文本框中，添加纯文本响应消息。

<a id="response-headers"></a>

## 响应 headers

<!--
> [Introduced](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/731) in GitLab 13.8, the `RateLimit` headers. `Retry-After` was introduced in an earlier version.
-->

当客户端超过相关的速率限制时，会阻止以下请求。服务器可以用限速信息进行响应，允许请求者在特定时间段后重试。这些信息附加到响应 headers 中。

| Header                | 示例                         | 描述                                                                                                                                                                                                      |
|:----------------------|:--------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `RateLimit-Limit`     | `60`                            | 客户端的请求配额**每分钟**。如果管理中心设置的速率限制期不是 1 分钟，则此 header 的值将调整为大约最接近的 60 分钟期限。 |
| `RateLimit-Name`      | `throttle_authenticated_web`    | 阻止请求的限制的名称。                                                                                                                                                                      |
| `RateLimit-Observed`  | `67`                            | 时间窗口内与客户端关联的请求数。                                                                                                                                                  |
| `RateLimit-Remaining` | `0`                             | 时间窗口内的剩余配额。`RateLimit-Limit` 的结果 - `RateLimit-Observed`。                                                                                                                     |
| `RateLimit-Reset`     | `1609844400`                    | Unix 时间 - 重置请求配额时的格式化时间。                                                                                                            |
| `RateLimit-ResetTime` | `Tue, 05 Jan 2021 11:00:00 GMT` | [RFC2616](https://tools.ietf.org/html/rfc2616#section-3.3.1) - 重置请求配额时的格式化日期和时间。                                                                                            |
| `Retry-After`         | `30`                            | 配额重置前的剩余持续时间，**以秒为单位**。这是一个[标准 HTTP 标头](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Retry-After)。                                             |

## 使用 HTTP header 绕过速率限制

> 引入于 13.6 版本。

根据您组织的需要，您可能希望启用速率限制，但有些请求会绕过速率限制器。

您可以通过使用自定义标头标记应绕过速率限制器的请求。您必须在极狐GitLab 前面的负载均衡器或反向代理中的某个地方执行此操作。例如： 

1. 为您的绕过速率限制的标头选择一个名称。例如，`Gitlab-Bypass-Rate-Limiting`。
1. 配置您的负载均衡器，在应该绕过极狐GitLab 速率限制的请求上设置 `Gitlab-Bypass-Rate-Limiting: 1`。
1. 将负载均衡器配置为：
   - 擦除 `Gitlab-Bypass-Rate-Limiting`。
   - 在所有应受速率限制影响的请求上，将 `Gitlab-Bypass-Rate-Limiting` 设置为 `1` 以外的值。
1. 设置环境变量 `GITLAB_THROTTLE_BYPASS_HEADER`。
   - 对于 Linux 软件包安装实例，在 `gitlab_rails['env']` 中设置 `'GITLAB_THROTTLE_BYPASS_HEADER' => 'Gitlab-Bypass-Rate-Limiting'`。
   - 对于源安装实例，在 `/etc/default/gitlab` 中设置 `export GITLAB_THROTTLE_BYPASS_HEADER=Gitlab-Bypass-Rate-Limiting`。

您的负载均衡器必须擦除或覆盖所有传入流量的绕过标头，这一点很重要。否则，您必须相信您的用户不会设置该标头并绕过极狐GitLab 速率限制器。

请注意，仅当标头设置为 `1` 时，绕过才有效。

由于绕过 header 而绕过速率限制器的请求在 `production_json.log` 中标记为 `"throttle_safelist":"throttle_bypass_header"`。

要禁用绕过机制，请确保环境变量 `GITLAB_THROTTLE_BYPASS_HEADER` 未设置或为空。

## 允许特定用户绕过经过身份验证的请求速率限制

> 引入于 13.7 版本

与上述绕过标头类似，可以允许特定的一组用户绕过速率限制器，仅适用于经过身份验证的请求：对于未经身份验证的请求，根据定义，极狐GitLab 不知道用户是谁。

允许列表在 `GITLAB_THROTTLE_USER_ALLOWLIST` 环境变量中配置为逗号分隔的用户 ID 列表。如果您希望用户 1、53 和 217 绕过经过身份验证的请求速率限制器，则允许列表配置为 `1,53,217`。

- 对于 Linux 软件包安装实例，在 `gitlab_rails['env']` 中设置 `'GITLAB_THROTTLE_USER_ALLOWLIST' => '1,53,217'`。
- 对于源安装实例，在 `/etc/default/gitlab` 中设置 `export GITLAB_THROTTLE_USER_ALLOWLIST=1,53,217`。

由于用户允许列表而绕过速率限制的请求在 `production_json.log` 中标记为 `"throttle_safelist":"throttle_user_allowlist"`。

在应用程序启动时，白名单会记录在 `auth.log` 中。

## 在强制执行之前尝试限制设置

> 引入于 13.6 版本

您可以通过将 `GITLAB_THROTTLE_DRY_RUN` 环境变量设置为以逗号分隔的节流名称列表来尝试节流设置。

可能的名称包括：

- `throttle_unauthenticated`
  - 废弃于 14.3 版本。使用 `throttle_unauthenticated_api` 或 `throttle_unauthenticated_web` 代替。
    `throttle_unauthenticated` 仍然受支持并选择上述两者。
- `throttle_unauthenticated_api`
- `throttle_unauthenticated_web`
- `throttle_authenticated_api`
- `throttle_authenticated_web`
- `throttle_unauthenticated_protected_paths`
- `throttle_authenticated_protected_paths_api`
- `throttle_authenticated_protected_paths_web`
- `throttle_unauthenticated_packages_api`
- `throttle_authenticated_packages_api`
- `throttle_authenticated_git_lfs`
- `throttle_unauthenticated_files_api`
- `throttle_authenticated_files_api`
- `throttle_unauthenticated_deprecated_api`
- `throttle_authenticated_deprecated_api`

例如，可以通过设置 `GITLAB_THROTTLE_DRY_RUN='throttle_authenticated_web,throttle_authenticated_api'` 来尝试对所有经过身份验证的对非受保护路径的请求进行限制。

要全部启用 dry run 模式，可以将变量设置为 `*`。

设置为 dry run 模式会在达到限制时将消息记录到 `auth.log`，同时让请求正常继续。日志消息包含一个设置为 `track` 的 `env` 字段。`matched` 字段包含被节流设置的名称。

在**启用设置中的速率限制之前**设置环境变量很重要。管理中心的设置立即生效，而设置环境变量需要重启所有 Puma 进程。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
