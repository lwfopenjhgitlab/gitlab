---
type: reference
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 流水线创建的速率限制 **(FREE SELF)**

> 引入于 15.0 版本。

您可以设置一个限制，使用户和进程每分钟请求的流水线数不能超过一定数量。此限制有助于节省资源并提高稳定性。

例如，如果您设置限制为 `10`，并且在一分钟内向 trigger API<!--[trigger API](../../../ci/triggers/)--> 发送了 11 个请求，则阻止第 11 个请求。一分钟后再次允许访问端点。

此限制：

- 每个项目、用户和提交独立应用。
- 不适用于按每个 IP 地址限制。
- 默认禁用。

超出限制的请求会记录在 `application_json.log` 文件中。

## 设置流水线请求限制

要限制流水线请求的数量：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **流水线速率限制**。
1. 在 **每分钟最大请求数** 下，输入一个大于 `0` 的值。
1. 选择 **保存更改**。
1. 启用 `ci_enforce_throttle_pipelines_creation` 功能标志，启用速率限制。
