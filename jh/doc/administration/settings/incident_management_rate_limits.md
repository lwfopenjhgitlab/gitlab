---
type: reference
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 事件管理速率限制 **(ULTIMATE SELF)**

您可以限制在一段时间内可以创建的[事件](../../operations/incident_management/incidents.md)的入站警报数量。入站[事件管理](../../operations/incident_management/index.md)警报限制可以通过减少警报数量或重复议题来帮助防止事件响应者超载。

例如，如果您设置了每 60 秒 10 次请求的限制，并且在一分钟内有 11 次请求被发送到[警报集成端点](../../operations/incident_management/integrations.md)，第十一个请求被阻塞。一分钟后再次允许访问端点。

此限制：

- 每个项目独立应用。
- 不适用于每个 IP 地址。
- 默认禁用。

超出限制的请求将记录到 `auth.log` 中。

## 设置入站警报限制

要设置入站事件管理警报限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **事件管理限制**。
1. 选中 **启用事件管理入站警报限制** 复选框。
1. 可选。为 **每个项目每个速率限制期的最大请求数** 输入自定义值。默认值为 3600。
1. 可选。为 **速率限制期** 输入自定义值。默认值为 3600 秒。
