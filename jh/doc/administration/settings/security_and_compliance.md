---
stage: Secure
group: Composition Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 安全与合规的管理中心设置 **(ULTIMATE SELF)**

软件包元数据同步的设置位于[管理中心](index.md)。

## 选择要同步的软件包库元数据

选择用于[许可证合规](../../compliance/license_scanning_of_cyclonedx_files/index.md)的极狐GitLab 许可证数据库同步的软件包：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 安全与合规**。
1. 展开 **许可证合规**。
1. 选择或清除要同步的软件包库的复选框。
1. 选择 **保存更改**。
