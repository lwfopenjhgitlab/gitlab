---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# Sidekiq 作业大小限制 **(FREE SELF)**

> 引入于 14.3 版本。

[Sidekiq](../sidekiq.md) 作业存储在 Redis 中。为了避免 Redis 内存过多，我们可以：

- 在将它们存储到 Redis 之前压缩作业参数。
- 拒绝压缩后超过指定阈值限制的作业。

要访问 Sidekiq 作业大小限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **Sidekiq 作业大小限制**。
1. 调整压缩阈值或大小限制。可以通过选择 **Track** 模式来禁用压缩。

## 可用设置

| 设置                                   | 默认值          | 描述                                                                                                                                                                   |
|-------------------------------------------|------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 限制模式                             | Compress         | 此模式以指定的阈值压缩作业，如果压缩后超过指定的限制，则拒绝它们。                                               |
| Sidekiq 作业压缩阈值（字节） | 100 000 (100 KB) | 当参数的大小超过这个阈值时，它们会在存储到 Redis 之前被压缩。                                                                          |
| Sidekiq 作业大小限制（字节）            | 0                | 压缩后超过此大小的作业将被拒绝。这避免了 Redis 中过多的内存使用导致不稳定。 将其设置为 0 可防止拒绝作业。     |

更改这些值后，[重新启动 Sidekiq](../restart_gitlab.md)。
