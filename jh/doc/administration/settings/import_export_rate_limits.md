---
type: reference
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 项目和群组的导入导出速率限制 **(FREE SELF)**

您可以为项目和群组的导入和导出配置速率限制：

更改速率限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **导入和导出速率限制**。
1. 更改任何速率限制的值。速率限制按每个用户每分钟，而不是按每个 IP 地址。设置为 `0`，可以禁用速率限制。

| 限制                   | 默认值 |
|-------------------------|---------|
| 项目导入          | 6       |
| 项目导出          | 6       |
| 项目导出下载 | 1       |
| 群组导入            | 6       |
| 群组导出           | 6       |
| 群组导出下载   | 1       |

当用户超过速率限制时，记录到 `auth.log` 中。
