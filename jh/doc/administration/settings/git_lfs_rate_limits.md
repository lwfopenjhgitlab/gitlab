---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# Git LFS 速率限制 **(FREE SELF)**

[Git LFS (Large File Storage)](../../topics/git/lfs/index.md) 是一个用于处理大文件的Git扩展。如果您在仓库中使用 Git LFS，常见的 Git 操作会生成许多 Git LFS 请求。您可以强制执行[一般用户和 IP 速率限制](../settings/user_and_ip_rate_limits.md)，也可以覆盖一般设置并对  Git LFS 请求实施额外限制。此覆盖可以提高 Web 应用程序的安全性和持久性。除了优先级之外，此配置还提供与一般用户和 IP 速率限制相同的功能。

## 配置 Git LFS 速率限制

Git LFS 速率限制默认禁用。如果启用并配置，这些限制将取代[一般用户和 IP 速率限制](user_and_ip_rate_limits.md)：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **Git LFS 速率限制**。
1. 选择 **启用经过身份验证的 Git LFS 请求速率限制**。
1. 输入 **每个用户每个时期的最大经过身份验证的 Git LFS 请求** 的值。
1. 为 **认证的 Git LFS 速率限制时间（秒）** 输入一个值。
1. 选择 **保存更改**。

<!--
## Related topics

- [Rate limiting](../../../security/rate_limits.md)
- [User and IP rate limits](user_and_ip_rate_limits.md)
-->