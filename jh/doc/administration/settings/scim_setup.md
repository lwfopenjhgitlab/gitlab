---
type: reference, howto
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 为私有化部署实例配置 SCIM **(PREMIUM SELF)**

您可以使用开放标准的跨域身份管理系统 (SCIM) 来自动：

- 创建用户。
- 禁用用户。

极狐GitLab 内部 SCIM API 实现了 [RFC7644 协议](https://www.rfc-editor.org/rfc/rfc7644)的一部分。

<!--
If you are a GitLab.com user, see [configuring SCIM for GitLab.com groups](../../../user/group/saml_sso/scim_setup.md).
-->

## 配置极狐GitLab

先决条件：

- 配置 [SAML 单点登录](../../integration/saml.md)。

配置极狐GitLab SCIM：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **SCIM 令牌** 部分并选择 **生成 SCIM 令牌**。
1. 对于身份提供商的配置，保存以下数据：
    - **您的 SCIM 令牌** 字段中的令牌。
    - **SCIM API 端点 URL** 字段中的 URL。
