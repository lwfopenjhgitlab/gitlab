---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# 帐户和限制设置 **(FREE SELF)**

## 默认项目限制

您可以配置新用户能在其个人命名空间中创建的默认最大项目数。此限制仅影响更改设置后创建的新用户帐户。此设置对现有用户不具有追溯性，但您可以单独编辑[现有用户的项目限制](#针对用户的项目限制)。

为新用户配置个人命名空间中的最大项目数：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。
1. 增加或减少 **默认项目限制** 值。

如果您将 **默认项目限制** 设置为 0，则不允许用户在其用户个人命名空间中创建项目。但是，仍然可以在群组中创建项目。

### 针对用户的项目限制

您可以编辑特定用户，并更改此用户可以在其个人命名空间中创建的最大项目数：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览** > **用户**。
1. 从用户列表中选择一个用户。
1. 选择 **编辑**。
1. 增加或减少 **项目限制** 值。

<a id="max-attachment-size"></a>

## 最大附件大小

> 从 10 MB 变更为 100 MB 于 15.7 版本。

评论和回复中附件的最大文件大小为 100 MB。
更改最大附件大小：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。
1. 通过更改 **最大附件大小（MB）** 中的值来增加或减少。

如果您选择的大小大于为 Web 服务器配置的值，您可能会收到错误消息。阅读[故障排查部分](#故障排查)了解更多详情。

<!--
For GitLab.com repository size limits, read [accounts and limit settings](../../gitlab_com/index.md#account-and-limit-settings).
-->

<a id="max-push-size"></a>

## 最大推送大小

您可以更改实例的最大推送大小：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。
1. 通过更改 **最大推送大小（MB）** 中的值来增加或减少。

对于 SaaS 版的限制，查看[极狐GitLab 应用限制](../../jihulab_com/index.md#account-and-limit-settings)。

NOTE:
当您通过 Web UI [将文件添加到仓库](../../project/repository/web_editor.md#创建文件)时，**附件**大小是限制因素，因为 web 服务器必须在极狐GitLab 生成提交之前接收文件。
使用 [Git LFS](../../../topics/git/lfs/index.md) 向仓库添加大型文件。

## 最大导出大小

> 引入于 15.0 版本。

在极狐GitLab 中修改导出的最大文件大小：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。
1. 增加或减少 **最大导出大小（MB）** 中的值。

## 最大导入大小

> 于 13.8 版本，从 50 MB 修改为无限制。

在极狐GitLab 中修改导入的最大文件大小：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。
1. 通过更改 **最大导入大小（MB）** 中的值来增加或减少。

如果您选择的大小大于为 Web 服务器配置的值，您可能会收到错误消息。阅读[故障排查部分](#troubleshooting)了解更多详情。

此设置仅适用于[从极狐GitLab 导出文件导入](../../user/project/settings/import_export.md#import-a-project-and-its-data)的仓库。

<!--
For GitLab.com repository size limits, read [accounts and limit settings](../../gitlab_com/index.md#account-and-limit-settings).
-->

<a id="personal-access-token-prefix"></a>

## 个人访问令牌前缀

> - 引入于 13.7 版本。
> - 默认前缀引入于 14.5 版本。

您可以为个人访问令牌指定前缀。您可以使用前缀来更快地找到令牌，或者与自动化工具一起使用。

默认前缀是 `glpat-`，但管理员可以更改它。

[项目访问令牌](../../user/project/settings/project_access_tokens.md)和[群组访问令牌](../../user/group/settings/group_access_tokens.md)也继承此前缀。

### 设置前缀

更改默认全局前缀：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。
1. 填写 **个人访问令牌前缀** 字段。
1. 单击 **保存更改**。

<!--
You can also configure the prefix by using the
[settings API](../../../api/settings.md).
-->

<a id="repository-size-limit"></a>

## 仓库大小限制 **(PREMIUM SELF)**

极狐GitLab 实例中的仓库可以快速增长，尤其是在您使用 LFS 时，它们的大小可以成倍增长，迅速消耗可用存储。
为防止这种情况发生，您可以为仓库的大小设置硬限制。
您可以全局、按群组或按项目设置此限制，每个项目的限制具有最高优先级。

有许多用例可以设置仓库大小的限制。
例如，考虑以下工作流程：

1. 您的团队开发的应用程序需要将大文件存储在应用程序仓库中。
1. 尽管您已在项目中启用 [Git LFS](../../topics/git/lfs/index.md)，但您的存储量已显着增长。
1. 在超出可用存储空间之前，您设置了每个存储库 10 GB 的限制。

<!--
NOTE:
For GitLab.com repository size limits, read [accounts and limit settings](../../gitlab_com/index.md#account-and-limit-settings).
-->

### 工作原理

只有极狐GitLab 管理员可以设置这些限制。将限制设置为 `0` 意味着没有限制。

这些设置可以在以下位置找到：

- 每个项目的设置：
   1. 在项目的主页上，导航到 **设置 > 通用**。
   1. 填写 **命名、主题、头像** 部分中的 **仓库大小限制 (MB)** 字段。
   1. 单击 **保存更改**。
- 每个群组的设置：
   1. 在群组主页，导航至 **设置 > 通用**。
   1. 在 **名称与可见性** 部分中填写 **仓库大小限制（MB）** 字段。
   1. 单击 **保存更改**。
- 全局设置：
   1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
   1. 选择 **管理中心**。
   1. 选择 **设置 > 通用**。
   1. 展开 **账户和限制**。
   1. 填写 **每个仓库的大小限制 (MB)** 字段。
   1. 单击 **保存更改**。

新项目的第一次推送，包括 LFS 对象，会检查大小。
如果它们的大小总和超过允许的最大仓库大小，则拒绝推送。

NOTE:
仓库大小限制包括仓库文件和 LFS，但不包括产物、上传文件、wiki、软件包或代码片段。仓库大小限制适用于私有和公共项目。

有关手动清除文件的详细信息，查看[使用 Git 减少仓库大小](../../user/project/repository/reducing_the_repo_size_using_git.md)。

## 会话持续时间

<a id="customize-the-default-session-duration"></a>

### 自定义默认会话持续时间

您可以更改允许用户保持登录状态却无活动的时长。

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。在 **会话持续时间（分钟）** 下设置持续时间。

如果启用了[记住我](#turn-emember-me-me-on-off)，则用户的会话可以在不确定的时间内，一直保持活动状态。

有关详细信息，请参阅[用于登录的 cookie](../../profile/index.md#cookies-used-for-sign-in)。

<a id="turn-remember-me-on-or-off"></a>

### 打开或关闭**记住我**

> 打开或关闭**记住我**设置的功能引入于 16.0 版本。

用户可以在登录时选择**记住我**复选框，从特定浏览器访问时，会话将在不确定的时间内，一直保持活跃。如果您需要会话以出于安全或合规性的目的而到期，则可以关闭此设置。关闭此设置，将确保用户的会话在[自定义会话持续时间](#customize-the-default-session-duration)中的不活动分钟数之后到期。

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。
1. 选择或清除 **记住我** 复选框，可以打开或关闭此设置。

### 启用 2FA 时自定义 Git 操作的会话持续时间 **(PREMIUM SELF)**

FLAG:
在私有化部署实例，此功能默认不可用。要启用，询问管理员启用名为 `two_factor_for_cli` 的[功能标志](../../administration/feature_flags.md)。在 SaaS 上，此功能不可用。此功能尚未准备好用于生产用途。此功能标志同时影响 [Git over SSH 操作的 2FA](../../security/two_factor_authentication.md#2fa-for-git-over-ssh-operations)。

当启用 2FA 时，极狐GitLab 管理员可以选择自定义 Git 操作的会话持续时间（以分钟为单位）。默认值为 15，可以设置为 1 到 10080 之间的值。

要设置这些会话的有效时间限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制** 部分。
1. 填写 **启用 2FA 时 Git 操作的会话持续时间（分钟）**字段。
1. 单击 **保存修改**。

## 限制 SSH 密钥的有效期 **(ULTIMATE SELF)**

> - 引入于 14.6 版本，功能标志名为 `ff_limit_ssh_key_lifetime`。默认禁用。
> - 适用于自助管理版于 14.6 版本。
> - 于 14.7 版本移除了功能标志。

用户可以选择指定 [SSH 密钥](../../user/ssh.md)的有效期。
有效期不是必需的，可以设置为任意天数。

SSH 密钥是访问极狐GitLab 的用户凭据。
但是，具有安全要求的组织可能希望通过要求定期轮换这些密钥来实施更多保护。

### 设置有效期

只有极狐GitLab 管理员可以设置有效期。留空意味着没有限制。

设置 SSH 密钥的有效期，请执行以下操作：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。
1. 填写 **SSH 密钥的的最长有效期（天）** 字段。
1. 单击 **保存修改**。

一旦设置了 SSH 密钥的有效期：

- 要求用户设置一个不迟于新 SSH 密钥允许使用期限的到期日期。
- 对现有 SSH 密钥应用有效期限制。没有有效期，或有效期大于最大值的密钥立即变为无效。

NOTE:
当用户的 SSH 密钥失效时，他们可以删除并重新添加相同的密钥。

<a id="limit-the-lifetime-of-access-tokens"></a>

## 限制访问令牌的生命周期 **(ULTIMATE SELF)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/3649) in GitLab 12.6.
-->

用户可以选择指定访问令牌的最大生命周期（以天为单位），包括[个人](../../user/profile/personal_access_tokens.md)、[群组](../../user/group/settings/group_access_tokens.md)和[项目](../../user/project/settings/project_access_tokens.md)访问令牌。
此生命周期不是必需的，可以设置为大于 0 且小于或等于 365 的任何值。如果此设置留空，则访问令牌的默认允许生命周期为 365 天。

访问令牌是对极狐GitLab 进行编程访问所需的唯一令牌。
但是，具有安全要求的组织可能希望通过要求定期轮换这些令牌来实施更多保护。

### 设置有效期

只有极狐GitLab 管理员可以设置有效期。留空表示没有限制。

要设置访问令牌的有效期，请执行以下操作：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。
1. 填写 **访问令牌的最长有效期（天）** 字段。
1. 单击 **保存修改**。

一旦设置了访问令牌的有效期：

- 为新的访问令牌应用有效期，并要求用户设置到期日期，不迟于允许的有效期。
- 三小时后，撤销没有到期日期，或超过允许有效期的旧令牌。三个小时的时间允许管理员在撤销之前，更改或删除有效期。

## 禁用用户个人资料名称更改 **(PREMIUM SELF)**

为了保持[审计事件](../../administration/audit_events.md)中用户详细信息的完整性，极狐GitLab 管理员可以选择禁用用户更改其个人资料名称的能力。

操作步骤：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。
1. 选中 **禁止用户更改个人资料名称** 复选框。

<!--
NOTE:
When this ability is disabled, GitLab administrators can still use the
[Admin UI](../index.md#administering-users) or the
[API](../../../api/users.md#user-modification) to update usernames.
-->

## 阻止新用户创建顶级群组

> 引入于 15.5 版本。

默认情况下，新用户可以创建顶级群组。极狐GitLab 管理员可以阻止新用户创建顶级群组：

- 在 15.5 及更高版本，可以使用：
  - 极狐GitLab UI。
  - [应用设置 API](../../api/settings.md#change-application-settings)。
- 在 15.4 及更早版本，使用[配置文件](../../administration/user_settings.md#use-configuration-files-to-prevent-new-users-from-creating-top-level-groups)。

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。
1. 清除 **允许新用户创建顶级群组** 复选框。

<a id="set-profiles-of-new-users-to-private-by-default"></a>"

## 将新用户的配置文件默认设置为私有

> 引入于 15.8 版本。

默认情况下，新创建的用户具有公开配置文件。极狐GitLab 管理员可以设置新用户默认拥有私有配置文件：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。
1. 选中 **默认将新用户的个人资料设为私有** 复选框。

## 阻止用户删除其帐户 **(FREE SELF)**

> 引入于 16.0 版本，[功能标志](../../../administration/feature_flags.md)为 `deleting_account_disabled_for_users`。默认禁用。

默认情况下，用户可以删除自己的帐户。极狐GitLab 管理员可以阻止用户删除自己的帐户：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制**。
1. 清除 **允许用户删除自己的帐户** 复选框。

<a id="password-expiration-policy"></a>

## 密码过期策略 **(PREMIUM SELF)**

> 引入于 15.7 版本。功能标志为 `password_expiration`，默认禁用。

FLAG:
在私有化部署版上，此功能不可用。要使其可用，询问管理员[启用功能标志](../../../administration/feature_flags.md) `password_expiration`。在 SaaS 版上，此功能不可用。

默认情况下，用户的密码永不过期。管理员可以通过启用**密码过期策略**功能，使用户密码在特定天数后过期，强制用户更改密码。

要启用密码过期策略：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **帐户和限制**。
1. 选中 **启用密码过期策略** 复选框。
1. 在 **密码有效天数** 框中，输入用户密码在多少天后过期。单位为天，有效范围为 14 到 400 之间的整数，默认值为 90。有效期起始时间为用户最后一次更新密码的时间。
2. 在 **设置到期前的提前通知天数**  框中，输入在密码到期前，提前通知用户的天数。单位为天，有效范围为 1 到 30 之间的整数，且不大于**密码有效天数**，默认值为 7。
1. 选择 **保存更改**。

在达到**设置到期前的提前通知天数**后，系统将通过邮件形式通知用户，用户可以根据邮件内容指引更改密码。

如果用户在密码过期后登录，在登录成功后，将自动跳转到设置新密码页面，必须根据页面指引设置新密码。

<a id="troubleshooting"></a>

## 故障排查

### 413 Request Entity Too Large

将文件附加到评论或回复时显示 `413 Request Entity Too Large` 错误，[最大附件大小](#max-attachment-size)可能大于 Web 服务器的允许值。

要将 [Linux 软件包](https://docs.gitlab.cn/omnibus/)安装中的最大附件大小增加到 200 MB，您可能需要在增加最大附件大小之前，将以下行添加到 `/etc/gitlab/gitlab.rb`：

```ruby
nginx['client_max_body_size'] = "200m"
```

### 此仓库已超出其大小限制

如果您在 Rails 异常日志中收到间歇性推送错误，如下所示：

```plaintext
Your push has been rejected, because this repository has exceeded its size limit.
```

[例行维护](../../administration/housekeeping.md)任务可能会导致您的仓库大小增加。
为了解决这个问题，考虑中短期的使用，执行以下操作之一：

- 增加[仓库大小限制](#repository-size-limit)。
- [减少仓库大小](../../user/project/repository/reducing_the_repo_size_using_git.md)。

