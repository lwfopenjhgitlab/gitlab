---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# 实例模板仓库 **(PREMIUM SELF)**

<!--
> - [Improved](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/52360) to behave like group-level templates in GitLab 13.9.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/321247) in GitLab 14.0.
-->

在托管系统中，企业通常需要跨团队共享自己的模板。此功能允许管理员选择一个项目来收集实例范围内的文件模板。然后，这些模板会在项目保持安全的同时，[通过 Web 编辑器](../../user/project/repository/web_editor.md)向所有用户公开。

## 配置

要选择一个项目作为自定义模板仓库：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 模板**。
1. 展开 **模板**。
1. 从下拉列表中，选择要用作模板仓库的项目。
1. 选择 **保存修改**。
1. 将自定义模板添加到选定的仓库。

添加模板后，您可以将它们用于整个实例。
它们在 Web 编辑器的下拉列表中可用，在通过 API 设置可用。

## 支持的文件类型和位置

模板必须添加到仓库中的特定子目录，对应于模板的种类。支持以下类型的自定义模板：

| 类型                    | 目录            | 扩展名     |
| :---------------:       | :-----------:        | :-----------: |
| `Dockerfile`            | `Dockerfile`         | `.dockerfile` |
| `.gitignore`            | `gitignore`          | `.gitignore`  |
| `.gitlab-ci.yml`        | `gitlab-ci`          | `.yml`        |
| `LICENSE`               | `LICENSE`            | `.txt`        |
| `metrics-dashboard.yml` | `metrics-dashboards` | `.yml`        |

每个模板必须位于其各自的子目录中，具有正确的扩展名并且不能为空。层次结构应如下所示：

```plaintext
|-- README.md
|-- Dockerfile
    |-- custom_dockerfile.dockerfile
    |-- another_dockerfile.dockerfile
|-- gitignore
    |-- custom_gitignore.gitignore
    |-- another_gitignore.gitignore
|-- gitlab-ci
    |-- custom_gitlab-ci.yml
    |-- another_gitlab-ci.yml
|-- LICENSE
    |-- custom_license.txt
    |-- another_license.txt
```

当通过 UI 添加新文件时，您的自定义模板会显示在下拉菜单中：

![Custom template dropdown menu](img/file_template_user_dropdown.png)

如果禁用此功能或不存在模板，则选择下拉列表中不会显示 **自定义** 部分。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
