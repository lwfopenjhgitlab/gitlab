---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 仓库文件 API 的速率限制 **(FREE SELF)**

> - 引入于 14.3 版本。
> - 一般可用于 14.6 版本，功能标志 `files_api_throttling` 移除。

仓库文件 API 使您能够获取、创建、更新和删除仓库中的文件。为了提高 Web 应用程序的安全性和持久性，您可以在此 API 上强制执行速率限制。您为文件 API 创建的任何速率限制都会覆盖[一般用户和 IP 速率限制](user_and_ip_rate_limits.md)。

## 定义文件 API 速率限制

默认情况下禁用文件 API 的速率限制。启用后，它们会取代对 Repository files API<!--[Repository files API](../../../api/repository_files.md)--> 的请求的一般用户和 IP 速率限制。您可以保留任何一般用户和 IP 速率限制，并增加或减少 Files API 的速率限制。 此覆盖不提供其他新功能。

先决条件：

- 您必须具有实例的管理员访问权限。

要覆盖对仓库文件 API 的请求的一般用户和 IP 速率限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **文件 API 速率限制**。
1. 选中您要启用的速率限制类型的复选框：
    - **未经身份验证的 API 请求速率限制**
    - **经过身份验证的 API 请求速率限制**
1. *如果您启用了未经身份验证的 API 请求速率限制：*
    1. 选择 **每个 IP 每个时间段最大未经身份验证的 API 请求**。
    1. 选择 **未经身份验证的 API 速率限制时间（以秒为单位）**。
1. *如果您启用了经过身份验证的 API 请求速率限制：*
    1. 选择 **每个用户每个速率限制期的最大已验证 API 请求数**。
    1. 选择 **已身份验证的 API 速率限制期（以秒为单位）**。

<!--
## Related topics

- [Rate limits](../../../security/rate_limits.md)
- [Repository files API](../../../api/repository_files.md)
- [User and IP rate limits](user_and_ip_rate_limits.md)
-->