---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# Package Registry 速率限制 **(FREE SELF)**

通过 [GitLab Package Registry](../../user/packages/package_registry/index.md)，您可以将极狐GitLab 用作各种常见软件包管理器的私有或公开库。您可以发布和共享软件包，其他人可以通过 Packages API<!--[Packages API](../../../api/packages.md)--> 在下游项目中将其作为依赖项使用。

如果下游项目经常下载此类依赖，则通过 Packages API 发出许多请求。因此，您可能会达到强制的[用户和 IP 速率限制](user_and_ip_rate_limits.md)。
要解决此问题，您可以为 Packages API 定义特定的速率限制：

- [未经身份验证的请求（每个 IP）](#enable-unauthenticated-request-rate-limit-for-packages-api)。
- [经过身份验证的 API 请求（每个用户）](#enable-authenticated-api-request-rate-limit-for-packages-api)。

默认情况下禁用这些限制。

启用后，它们会取代对 Packages API 请求的一般用户和 IP 速率限制。因此，您可以保留一般用户和 IP 速率限制，并增加 Packages API 的速率限制。除了优先级之外，与一般用户和 IP 速率限制相比，功能上没有区别。

<a id="enable-unauthenticated-request-rate-limit-for-packages-api"></a>

## 为 packages API 启用未经身份验证的请求速率限制

要启用未经身份验证的请求速率限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **软件包仓库速率限制**。
1. 选择 **启用未经身份验证的请求速率限制**。

   - 可选。更新 **每个 IP 每个速率限制期间的最大未经身份验证的 API 请求数** 的值。 默认为 `800`。
   - 可选。更新 **未经身份验证的速率限制期（以秒为单位）** 值。 默认为 `15`。

<a id="enable-authenticated-api-request-rate-limit-for-packages-api"></a>

## 为 packages API 启用经过身份验证的 API 请求速率限制

要启用经过身份验证的 API 请求速率限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **软件包仓库速率限制**。
1. 选择 **启用已身份验证的 API 请求速率限制**。

   - 可选。更新 **Maximum authentication API requests per rate limit period per user** 的值。默认为 `1000`。
   - 可选。更新 **已身份验证的 API 速率限制期（以秒为单位）** 的值。默认为 `15`。
