---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 登录限制 **(FREE SELF)**

您可以使用**登录限制**自定义 Web 界面以及基于 HTTP(S) 的 Git 的身份验证限制。

## 设置

要访问登录限制设置：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **登录限制** 部分。

<a id="password-authentication-enabled"></a>

## 启用密码验证

您可以通过 HTTP(S) 限制 Web 界面和 Git 的密码验证：

- **Web 界面**：禁用此功能时，**标准**登录选项卡将被删除，并且必须使用外部身份验证提供程序。
- **Git over HTTP(S)**：禁用此功能时，必须使用[个人访问令牌](../../user/profile/personal_access_tokens.md)或 LDAP 密码进行身份验证。

如果外部身份验证提供程序中断，请使用[极狐GitLab Rails 控制台](../operations/rails_console.md)，[重新启用标准 Web 登录表单](#re-enable-standard-web-sign-in-form-in-rails-console)。<!--This configuration can also be changed over the [Application settings REST API](../../../api/settings.md#change-application-settings) while authenticating with an administrator account's personal access token.-->

## 管理员模式

> 引入于 13.10 版本

如果您是管理员，您可能希望在极狐GitLab 中工作而不需要作为管理员的访问权限。虽然您可以创建一个没有管理员访问权限的单独用户账户，但更安全的解决方案是使用*管理员模式*。

使用管理员模式，默认情况下您的账户没有管理员权限。
您可以继续访问您所属的群组和项目，但要访问管理功能，您必须进行身份验证。

启用管理员模式后，它适用于实例上的所有管理员。

当为实例启用管理员模式时，管理员：

- 被允许访问他们所属的群组和项目。
- 无法访问**管理中心**。

### 为您的实例启用管理员模式

管理员可以通过 API、Rails 控制台或 UI 启用管理员模式。

#### 使用 API 启用管理员模式

向您的实例端点发出以下请求：

```shell
curl --request PUT --header "PRIVATE-TOKEN:$ADMIN_TOKEN" "<gitlab.example.com>/api/v4/application/settings?admin_mode=true"
```

将 `<gitlab.example.com>` 替换为您的实例 URL。

有关详细信息，请参阅[可通过 API 调用访问的设置列表](../../api/settings.md)。

#### 使用 Rails 控制台启用管理员模式

打开 [Rails 控制台](../operations/rails_console.md)并运行以下命令：

```ruby
::Gitlab::CurrentSettings.update_attributes!(admin_mode: true)
```

#### 使用 UI 启用管理员模式

要通过 UI 启用管理员模式：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **登录限制**。
1. 在 **管理员模式** 部分，选中 **需要对管理任务进行额外的身份验证** 复选框。

### 为您的会话打开管理员模式

要为当前会话打开管理员模式并访问具有潜在危险的资源：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **进入管理员模式**。
1. 尝试在 URL 中使用 `/admin` 访问 UI 的任何部分（这需要管理员访问权限）。

当管理员模式状态被禁用或关闭时，管理员无法访问资源，除非他们已被明确授予访问权限。例如，管理员尝试打开一个私有群组或项目，除非他们是该群组或项目的成员，否则他们会收到 `404` 错误。

应该为管理员启用 2FA。管理员模式支持 2FA、OmniAuth 提供程序和 LDAP 身份验证。管理员模式状态存储在当前用户会话中并保持活动状态，直到：

- 它被明确禁用。
- 超时后自动禁用。

### 关闭会话的管理员模式

要关闭当前会话的管理模式：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **离开管理员模式**。

### 管理员模式的限制

以下访问方法**不**受管理员模式保护：

- Git 客户端访问（使用公钥的 SSH 或使用个人访问令牌的 HTTPS）。

也就是说，受到管理员模式限制的管理员仍然可以使用 Git 客户端，而无需额外的身份验证步骤。

要使用极狐GitLab REST 或 GraphQL API，管理员必须创建使用 [`admin_mode` 范围](../../user/profile/personal_access_tokens.md#personal-access-token-scopes)，[创建个人访问令牌](../../user/profile/personal_access_tokens.md#create-a-personal-access-token)。

如果拥有 `admin_mode` 范围的个人访问令牌的管理员失去了管理员访问权限，则该用户无法以管理员身份访问 API，即使他们仍然拥有 `admin_mode` 范围的令牌。

<!--
We may address these limitations in the future. For more information see the following epic:
[Admin mode for GitLab Administrators](https://gitlab.com/groups/gitlab-org/-/epics/2158).
-->

### 管理员模式故障排查

如有必要，您可以使用以下两种方法之一，以管理员身份禁用 **管理员模式**：

- **API**：

  ```shell
  curl --request PUT --header "PRIVATE-TOKEN:$ADMIN_TOKEN" "<gitlab-url>/api/v4/application/settings?admin_mode=false"
  ```

- [**Rails console**](../operations/rails_console.md#starting-a-rails-console-session)：

  ```ruby
  ::Gitlab::CurrentSettings.update!(admin_mode: false)
  ```

## 双重验证

启用此功能后，所有用户都必须使用[双重身份验证](../../user/profile/account/two_factor_authentication.md)。

双重身份验证配置为强制后，允许用户在可配置的宽限期（以小时为单位）内跳过强制配置双重身份验证。

![Two-factor grace period](img/two_factor_grace_period.png)

<a id="email-notification-for-unknown-sign-ins"></a>

## 未知登录的电子邮件通知

启用后，极狐GitLab 会通知用户来自未知 IP 地址或设备的登录。有关详细信息，请参阅[未知登录的电子邮件通知](../../user/profile/unknown_sign_in_notification.md)。

![Email notification for unknown sign-ins](img/email_notification_for_unknown_sign_ins_v13_2.png)

<a id="sign-in-information"></a>

## 登录信息

如果 value 不为空，则所有未登录的用户都将重定向到配置的 **首页 URL** 所代表的页面。

如果 value 不为空，则所有用户在注销后重定向到配置的 **退出页面 URL** 所代表的页面。

在**登录限制**部分，滚动到**登录文本**字段。您可以为您的用户添加 Markdown 格式的自定义消息。

例如，您在注明的文本框中包含以下信息：

```markdown
# Custom sign-in text

To access this text box:

1. On the left sidebar, expand the top-most chevron (**{chevron-down}**).
1. Select **Admin Area**.
1. Select **Settings > General**.
1. Expand the **Sign-in restrictions** section.
```

您的用户在导航到您的极狐GitLab 实例的登录屏幕时，会看到**自定义登录文本**。

## 故障排除

<a id="re-enable-standard-web-sign-in-form-in-rails-console"></a>

### 在 Rails 控制台中重新启用标准 Web 登录表单

如果标准的基于用户名和密码的登录表单被禁用，作为[登录限制](#password-authentication-enabled)，您可以重新启用它。

当配置的外部身份验证提供程序（通过 SSO 或 LDAP 配置）面临中断，需要直接登录到极狐GitLab 时，您可以通过 rails 控制台使用此方法。

```ruby
Gitlab::CurrentSettings.update!(password_authentication_enabled_for_web: true)
```
