---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

# Terraform 限制 **(FREE SELF)**

> 引入于 15.7 版本。

您可以限制 [Terraform 状态文件](../terraform_state.md)的总存储量。
该限制适用于每个单独的状态文件版本，并在创建新版本时进行检查。

要添加存储限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **Terraform 限制**。
1. 调整大小限制。

## 可用的设置

| 设置                            | 默认值 | 描述                                                                                                                                             |
|------------------------------------|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------|
| Terraform 状态大小限制（字节） | 0       | 不保存超过此大小的 Terraform 状态文件，并拒绝关联的 Terraform 操作。设置为 0，表示允许无限大小的文件。 |
