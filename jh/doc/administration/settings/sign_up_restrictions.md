---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

<a id="sign-up-restrictions"></a>

# 注册限制 **(FREE SELF)**

您可以对注册实施以下限制：

- 禁用新注册。
- 新注册需要管理员批准。
- 需要用户电子邮件确认。
- 允许或拒绝使用特定电子邮件域名的注册。

<a id="disable-new-sign-ups"></a>

## 禁用新注册

默认情况下，任何访问您的极狐GitLab 域名的用户都可以注册一个帐户。对于运行面向公众的极狐GitLab 实例的客户，如果您不希望公众用户注册帐户，我们**强烈**建议您考虑禁用新注册。

要禁用注册：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **注册限制**。
1. 清除 **已启用注册功能** 复选框，然后选择 **保存修改**。

<a id="require-administrator-approval-for-new-sign-ups"></a>

## 新注册需要管理员批准

> - 引入于 13.5 版本。
> - 默认启用于 13.6 版本。

启用此设置后，任何访问您的 GitLab 域名并使用注册表单注册新帐户的用户，都必须由管理员明确[批准](../../administration/moderate_users.md#approve-or-reject-a-user-sign-up)开始使用他们的帐户。在 13.6 及更高版本中，默认情况下为新的极狐GitLab 实例启用此设置，仅适用于启用注册的情况。

要求管理员批准新注册：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **注册限制**。
1. 选中 **新的注册需要管理员批准** 复选框，然后选择 **保存修改**。

在 13.7 及更高版本中，如果管理员禁用此设置，处于待批准状态的用户将在后台作业中自动获得批准。

NOTE:
此设置不适用于 LDAP 或 OmniAuth 用户。要强制批准使用 OmniAuth 或 LDAP 注册的新用户，请在 [OmniAuth 配置](../../integration/omniauth.md#configure-common-settings)或 [LDAP 配置](../auth/ldap/index.md#basic-configuration-settings)中将 `block_auto_created_users` 设置为 `true`。

## 需要电子邮件确认

您可以在注册期间发送确认电子邮件，并要求用户在允许登录之前确认其电子邮件地址。

要强制确认用于新注册的电子邮件地址：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **注册限制**。
1. 选中 **注册时发送确认电子邮件** 复选框，然后选择 **保存修改**。

<a id="user-cap"></a>

## 用户上限

<!--
> - [Introduced](https://gitlab.com/groups/gitlab-org/-/epics/4315) in GitLab 13.7.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/292600) in GitLab 13.9.
-->

当计费用户数达到用户上限时，任何被添加或请求访问的用户必须由管理员[批准](../../administration/moderate_users.md#approve-or-reject-a-user-sign-up)，才能可以开始使用他们的帐户。

如果管理员[增加](#set-the-user-cap-number)或[删除](#remove-the-user-cap)用户上限，则处于待批准状态的用户将在后台作业中自动获得批准。

NOTE:
计费用户数量[每天更新一次](../../subscriptions/self_management/index.md#billable-users)，用户上限可能仅在超过上限后追溯适用。为了确保立即启用上限，请将其设置为低于当前计费用户数的较低值，例如：`1`。

在使用 LDAP 或 OmniAuth 的实例上，启用和禁用[管理员对新注册用户的批准](#require-administrator-approval-for-new-sign-ups)涉及更改 Rails 配置，并且可能需要停机。

您可以使用用户上限来代替。如上所述，将上限设置为确保立即执行的值。

<a id="set-the-user-cap-number"></a>

### 设置用户上限

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **注册限制**。
1. 在 **用户上限** 中输入一个数字。
1. 选择 **保存更改**。

新用户注册受用户上限限制。

<a id="remove-the-user-cap"></a>

## 删除用户上限

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **注册限制**。
1. 从 **用户上限** 中删除数字。
1. 选择 **保存更改**。

新用户注册不受用户上限限制。处于待批准状态的用户会在后台作业中自动获得批准。

## 电子邮件确认

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/47003) in GitLab 12.2.
> - It's [deployed behind a feature flag](../../../user/feature_flags.md), disabled by default.
> - It's enabled on GitLab.com.
> - It's recommended for production use.
> - To use it in GitLab self-managed instances, ask a GitLab administrator to [enable it](#enable-or-disable-soft-email-confirmation).

WARNING:
This feature might not be available to you. Check the **version history** note above for details.
-->

电子邮件确认通过允许新用户在需要电子邮件确认时，无需立即确认即可登录，从而改善了新用户的注册体验。
极狐GitLab 会向用户显示确认其电子邮件地址的提醒，并且在确认其电子邮件地址之前，用户无法创建或更新流水线。

<a id="minimum-password-length-limit"></a>

## 最小密码长度限制

您可以通过极狐GitLab 用户界面，[更改](../../security/password_length_limits.md#modify-minimum-password-length)用户密码中必须包含的最小字符数。

<a id="password-complexity-requirements"></a>

### 密码复杂度要求 **(PREMIUM SELF)**

> 引入于 15.2 版本。

默认情况下，用户密码的唯一要求是[最小密码长度](#minimum-password-length-limit)。
您可以添加额外的复杂度要求。密码复杂度要求的更改适用于：

- 新用户注册时创建的新密码
- 已有用户重置密码时设置的新密码

现有密码不受影响。要更改密码复杂度要求：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **注册限制**。
1. 在 **最小密码长度（字符数）** 下，选择其他密码复杂度要求。您可以要求使用数字、大写字母、小写字母和符号。
1. 选择 **保存更改**。

## 允许或拒绝使用特定电子邮件域名的注册

您可以指定可用于用户注册的电子邮件域名的列表，可包含的或除外的均可。

这些限制仅适用于外部用户注册。管理员可以通过管理员面板添加具有不允许使用的域名的用户。另外，请注意，用户可以在注册后将其电子邮件地址更改为不允许的域名。

### 电子邮件域名白名单

您可以限制用户仅使用与给定域名列表匹配的电子邮件地址进行注册。

### 电子邮件域名黑名单

您可以在使用特定域名的电子邮件地址时阻止用户注册。这可以降低恶意用户使用一次性电子邮件地址创建垃圾邮件帐户的风险。

### 创建电子邮件域名白名单或黑名单

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **注册限制**。
1. 对于白名单，您必须手动输入该名单。对于黑名单，您可以手动输入列表或上传包含列表条目的 `.txt` 文件。

   白名单和黑名单都接受通配符。例如，您可以使用 `*.company.com` 来接受每个 `company.com` 子域，或使用 `*.io` 来阻止所有以 `.io` 结尾的域名。域名必须用空格、分号、逗号或换行符分隔。

   ![Domain Denylist](img/domain_denylist_v14_1.png)

## 设置 LDAP 用户过滤器

您可以将极狐GitLab 访问权限限制为 LDAP 服务器上的部分 LDAP 用户。

有关更多信息，请参阅[有关设置 LDAP 用户过滤器的文档](../auth/ldap/index.md#set-up-ldap-user-filter)。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
