---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# 控制访问和可见性 **(FREE SELF)**

极狐GitLab 使具有管理员访问权限的用户能够对分支、项目、片段、群组等实施特定控制。

要访问可见性和访问控制选项：

1. 以具有管理员访问级别的用户身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。

<a id="define-which-roles-can-create-projects"></a>

## 定义哪些角色可以创建项目

项目创建的实例级保护定义了哪些角色可以在实例上将项目添加到群组。要更改哪些角色有权创建项目：

1. 以具有管理员访问级别的用户身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 对于 **默认项目创建保护**，选择所需角色：
   - 没有人。
   - 维护者。
   - 开发者和维护者。
1. 选择 **保存更改**。

## 将项目删除的权限设置为仅管理员 **(PREMIUM SELF)**

> 用户界面变更于 15.1 版本。

默认情况下，管理员和具有**所有者**角色的任何人都可以删除项目。要将项目删除仅限于管理员：

1. 以具有管理员权限的用户身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 滚动到：
   - （15.1 及更高版本）**允许删除项目**，然后选择 **管理员**。
   - （15.0 及更早版本）**默认项目删除保护** 并选择 **只有管理员可以删除项目**。
1. 选择 **保存更改**。

<a id="deletion-protection"></a>

## 删除保护 **(PREMIUM SELF)**

> - 引入于 14.2 版本。
> - 从**默认延迟项目删除**重命名为**删除保护**于 15.1 版本。
> - 为个人命名空间中的项目启用于 15.1 版本。
> - 为个人命名空间中的项目禁用于 15.3 版本。
> - 移除立即删除的选项于 15.11 版本，功能标志为 `always_perform_delayed_deletion`。默认禁用。
> - 在私有化部署版上默认启用延迟删除，并移除立即删除的选项于 16.0 版本。

防止意外删除群组和项目的实例级保护。

### 保留期限

> 变更于 15.1 版本。

组和项目将在规定的保留期内保持可恢复。默认情况下为 7 天，但可以更改。
将保留期设置为 `0` 意味着群组和项目会立即被删除并且无法恢复。

在 15.1 及更高版本中，保留期必须在 `1` 和`90` 之间。如果在 15.1 更新之前保留期为 `0`，那么它将自动更改为 `1`，同时在下次更改任何应用程序设置时禁用删除保护。

### 延迟项目删除

> - 用户界面变更于 15.1 版本。
> - 默认启用延迟删除于 16.0 版本，并在 SaaS 和私有化部署版上删除**立即删除**的选项。

要配置延迟项目删除：

1. 以具有管理员权限的用户身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 滚动到：
   - （启用了 `always_perform_delayed_deletion` 功能标志的 15.11 及更高版本，或 16.0 及更高版本）**删除保护**，并将保留期设置为 `1` 和 `90` 之间的值。
   - （15.1 及更高版本）**删除保护**，并选择保留已删除的群组和项目，并选择保留期限。
   - （15.0 及更早版本）**默认延迟项目保护** 并选择 **为新创建的群组默认启用延迟项目删除。**然后在 **默认删除延迟** 中设置保留期限。
1. 选择 **保存更改**。

删除保护仅适用于项目（也未对群组启用）。

在 15.1 及更高版本中，此设置在禁用时对群组强制执行，并且不能被覆盖。

### 延迟群组删除

> - 用户界面变更于 15.1 版本。
> - 在专业版和旗舰版上默认启用于 16.0 版本。

如果保留期限为 `1` 天或更多天，则群组将保持可恢复状态。

在 15.1 及更高版本中，可以通过将 **删除保护** 设置为 **保持删除** 来启用延迟群组删除。
启用了 `always_perform_delayed_deletion` 功能标志的 15.11 及更高版本，或 16.0 及更高版本：

- 移除了 **保持删除** 选项。
- 默认设置了延迟群组删除。

### 覆盖默认值并立即删除

或者，可以立即删除标记为删除的项目：

1. [恢复项目](../../user/project/settings/index.md#restore-a-project)。
1. 删除项目。

## 配置项目可见性默认值

设置默认[新项目的可见性级别](../../user/public_access.md)：

1. 以具有管理员访问级别的用户身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 选择所需的默认项目可见性：
   - **私有** - 必须明确授予每个用户项目访问权限。如果此项目是群组的一部分，则授予群组成员访问权限。
   - **内部** - 除外部用户外，任何登录用户都可以访问该项目。
   - **公开** - 无需任何身份验证即可访问该项目。
1. 选择 **保存更改**。

## 配置片段可见性默认值

设置新[代码片段](../../user/snippets.md)的默认可见性级别：

1. 以具有管理员访问级别的用户身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 选择所需的默认片段可见性。
1. 选择 **保存更改**。

有关片段可见性的更多详细信息，请阅读[项目可见性](../../user/public_access.md)。

## 配置群组可见性默认值

要为新群组设置默认可见性级别：

1. 以具有管理员访问级别的用户身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 选择所需的默认组可见性：
   - **私有** - 群组及其项目只能由成员查看。
   - **内部** - 除外部用户外，任何经过身份验证的用户都可以查看群组和任何内部项目。
   - **公开** - 无需任何身份验证即可查看群组和任何公开项目。
1. 选择 **保存更改**。

<!--
For more details on group visibility, see
[Group visibility](../../group/index.md#group-visibility).
-->

## 限制可见性级别

限制可见性级别时，请考虑这些限制如何与您正在更改的，继承其可见性的子组和项目的权限交互。

要限制群组、项目、片段和特定页面的可见性级别：

1. 以具有管理员访问级别的用户身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 在 **限制可见性级别** 部分中，选择要限制的所需可见性级别。

   - 如果您限制 **公开** 级别：
      - 只有管理员才能创建公开群组、项目和代码片段。
      - 用户配置文件仅对通过 Web 界面的经过身份验证的用户可见。
      - 通过 GraphQL API 的用户属性：
         - 在 15.1 及更高版本中不可见。
         - 从 13.1 到 15.0 版本，仅对经过身份验证的用户可见。
   - 如果您限制 **内部** 级别：
     - 只有管理员才能创建内部群组、项目和代码片段。
   - 如果您限制 **私有** 级别：
     - 只有管理员才能创建私有群组、项目和代码片段。

1. 选择 **保存更改**。

有关项目可见性的更多详细信息，请参阅[项目可见性](../../user/public_access.md)。

## 配置允许的导入源

在您可以从其他系统导入项目之前，您必须为该系统启用导入源。

1. 以具有管理员访问级别的用户身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 选择每个 **导入源** 来允许导入。
1. 选择 **保存更改**。

## 启用项目导出

要启用[项目及其数据](../../user/project/settings/import_export.md#export-a-project-and-its-data)的导出：

1. 以具有管理员访问级别的用户身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 滚动到 **项目导出**。
1. 选择 **启用** 复选框。
1. 选择 **保存更改**。

<a id="enable-migration-of-groups-and-projects-by-direct-transfer"></a>

## 启用通过直接传输迁移群组和项目

> 引入于 15.8 版本。

您可以使用 UI，通过直接传输启用群组迁移。

要在迁移群组时，同时迁移项目，您必须启用 [`bulk_import_projects` 功能标志](../../user/group/import/index.md#migrate-groups-by-direct-transfer-recommended)。

要通过直接传输启用群组迁移：

1. 以具有管理员访问级别的用户身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 滚动到 **允许通过直接传输迁移极狐GitLab 群组和项目**。
1. 选择 **启用** 复选框。
1. 选择 **保存更改**。

API 中提供了与 `bulk_import_enabled` 属性相同的设置。

<a id="configure-enabled-git-access-protocols"></a>

## 配置启用的 Git 访问协议

通过极狐GitLab 访问限制，您可以选择用户可用于与极狐GitLab 通信的协议。禁用访问协议不会阻止对服务器本身的端口访问。用于协议、SSH 或 HTTP(S) 的端口仍然可以访问。极狐GitLab 限制适用于应用程序级别。

要指定启用的 Git 访问协议：

1. 以具有管理员访问级别的用户身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 选择所需的 Git 访问协议：
   - SSH 和 HTTP(S)
   - 仅 SSH
   - 仅 HTTP(S)
1. 选择 **保存更改**。

当同时启用 SSH 和 HTTP(S) 时，用户可以选择任一协议。
如果仅启用一种协议：

- 项目页面仅显示允许协议的 URL，没有更改它的选项。
- 如果需要用户操作（例如添加 SSH 密钥或设置密码），则当您将鼠标悬停在 URL 的协议上时，系统会显示工具提示：

  ![Project URL with SSH only access](../settings/img/restricted_url.png)

极狐GitLab 仅允许对您选择的协议执行 Git 操作。

WARNING:
极狐GitLab 允许使用 HTTP(S) 协议进行 Git 克隆，或从 CI/CD 作业中获取极狐GitLab Runner 完成的请求，即使您选择 **仅 SSH** 也是如此。

## 为 HTTP(S) 自定义 Git 克隆 URL

您可以为 HTTP(S) 自定义项目 Git 克隆 URL，这会影响克隆面板：

例如：

- 您的极狐GitLab 实例位于 `https://example.com`，然后项目克隆 URL 类似于 `https://example.com/foo/bar.git`。
- 您希望克隆 URL 为 `https://git.example.com/gitlab/foo/bar.git`，您可以将此设置为 `https://git.example.com/gitlab/`。

![Custom Git clone URL for HTTP](../settings/img/custom_git_clone_url_for_https_v12_4.png)

要为 HTTP(S) 指定自定义 Git 克隆 URL：

1. 为 **HTTP(S) 的自定义 Git 克隆 URL** 输入根 URL。
1. 选择 **保存更改**。

NOTE:
通过设置 `gitlab_rails['gitlab_ssh_host']` 和其他相关设置，可以在 `gitlab.rb` 中自定义 SSH 克隆 URL。

## 为 RSA、DSA、ECDSA、ED25519、ECDSA_SK、ED25519_SK SSH 密钥配置默认值

这些选项指定允许的 SSH 密钥类型和长度。

要为每种密钥类型指定限制：

1. 从下拉列表中选择所需的选项。
1. 选择 **保存更改**。

有关详细信息，请参阅 [SSH 密钥限制](../../security/ssh_keys_restrictions.md)。

## 启用项目镜像

默认情况下启用此选项。通过禁用它，拉取镜像和推送镜像不再适用于每个仓库。它们只能由管理员用户在每个项目的基础上重新启用。

![Mirror settings](img/mirror_settings_v15_7.png)

<a id="configure-globally-allowed-ip-address-ranges"></a>

## 配置全局允许的 IP 地址范围

> - 引入于 15.1 版本，[功能标志](../../administration/feature_flags.md)为 `group_ip_restrictions_allow_global`。默认禁用。
> - 一般可用于 15.4 版本。功能标志 `group_ip_restrictions_allow_global` 已删除。

管理员可以将 IP 地址范围设置为与[群组级 IP 限制](../../user/group/access_and_permissions.md#restrict-group-access-by-ip-address)相结合。
使用全局允许的 IP 地址允许极狐GitLab 安装实例的各个方面，即使在设置了群组级 IP 地址限制时也能正常工作。

例如，如果极狐GitLab Pages daemon 在 `10.0.0.0/24` 范围内运行，您可以将该范围指定为全局允许范围。
这意味着即使群组级 IP 地址限制不包括 `10.0.0.0/24` 范围，极狐GitLab Pages 仍然可以从流水线中获取产物。

要将 IP 地址范围添加到群组级白名单：

1. 以具有管理员访问级别的用户身份登录极狐GitLab。
1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **可见性和访问控制** 部分。
1. 在 **全局允许的 IP 范围** 中，提供 IP 地址范围列表。这个列表：
   - 对 IP 地址范围的数量没有限制。
   - 大小限制为 1 GB。
   - 适用于 SSH 或 HTTP 授权的 IP 地址范围。您不能按授权类型拆分此列表。
1. 选择 **保存更改**。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
