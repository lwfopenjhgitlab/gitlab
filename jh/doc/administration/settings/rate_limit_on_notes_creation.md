---
type: reference
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 备注创建速率限制 **(FREE SELF)**

> 引入于 13.9 版本

您可以为对备注创建端点的请求，配置每用户速率限制。

要更改备注创建速率限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **备注速率限制**。
1. 在 **每分钟最大请求数** 框中，输入新值。
1. 可选。在 **从速率限制中排除的用户** 框中，列出允许超出限制的用户。
1. 选择 **保存修改**。

此限制：

- 每个用户独立应用。
- 不适用于按每个 IP 地址限制。

默认值为 `300`。

超过速率限制的请求会被记录到 `auth.log` 文件中。

例如，如果您将限制设置为 `300`，则使用 [Projects::NotesController#create](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/app/controllers/projects/notes_controller.rb) 操作的请求超过每分钟 300 次速率的将被阻止。一分钟后允许访问端点。
