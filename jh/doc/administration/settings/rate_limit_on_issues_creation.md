---
type: reference
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 议题创建的速率限制 **(FREE SELF)**

此设置允许您对议题和史诗创建端点的请求进行速率限制。
更改限制值：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **议题速率限制**。
1. 在 **每分钟最大请求数** 下，输入要更改的值。
1. 选择 **保存修改**。

例如，如果您将限制设置为 300，则使用 [Projects::IssuesController#create](https://jihulab.com/gitlab-cn/gitlab/blob/master/app/controllers/projects/issues_controller.rb) 超过每分钟 300 次的操作被阻止。一分钟后允许访问端点。

使用[史诗](../../user/group/epics/index.md)时，史诗创建将与议题共享此速率限制。

![Rate limits on issues creation](img/rate_limit_on_issues_creation_v14_2.png)

针对此限制：

- 每个项目和每个用户独立应用。
- 不能按每个 IP 地址应用。
- 默认禁用。要启用它，请将选项设置为 `0` 以外的任何值。

超过速率限制的请求会被记录到 `auth.log` 文件中。
