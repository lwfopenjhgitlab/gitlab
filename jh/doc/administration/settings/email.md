---
type: reference
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 电子邮件 **(FREE SELF)**

您可以自定义从极狐GitLab 实例发送的电子邮件中的一些内容。

## 自定义 Logo

部分邮件 header 的 Logo 可以自定义，参见 [Logo 自定义部分](../../administration/appearance.md#navigation-bar)。

## 在通知电子邮件正文中包含作者姓名 **(PREMIUM SELF)**

默认情况下，极狐GitLab 会使用议题、合并请求或评论作者的电子邮件地址覆盖通知电子邮件中的地址。启用此设置，可以在邮件正文中包含作者的电子邮件地址。

要在电子邮件正文中包含作者的电子邮件地址：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **电子邮件**。
1. 选中 **在通知电子邮件正文中包含作者姓名** 复选框。
1. 选择 **保存修改**。

## 启用 multipart 邮件 **(PREMIUM SELF)**

极狐GitLab 可以以多格式（HTML 和纯文本）或仅纯文本发送电子邮件。

要启用 multipart 电子邮件：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **电子邮件**。
1. 选择 **启用 multipart 邮件**。
1. 选择 **保存修改**。

<a id="custom-hostname-for-private-commit-emails"></a>

## 私有提交电子邮件的自定义主机名 **(PREMIUM SELF)**

此配置选项设置[私有提交电子邮件](../../profile/index.md#use-an-automatically-generated-private-commit-email)的电子邮件主机名。默认情况下设置为 `users.noreply.YOUR_CONFIGURED_HOSTNAME`。

要更改私人提交电子邮件中使用的主机名：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **电子邮件**。
1. 在 **自定义主机名（用于私有提交电子邮件）** 字段中输入所需的主机名。
1. 选择 **保存修改**。

NOTE:
配置主机名后，系统无法识别使用先前主机名的每封私人提交电子邮件。这可能与某些[推送规则](../../user/project/repository/push_rules.md)直接冲突，例如 `Check whether author is a GitLab user` 和 `Check whether committer is the current authenticated user`。

<a id="custom-additional-text"></a>

## 自定义附加文本 **(PREMIUM SELF)**

您可以在极狐GitLab 发送的任何电子邮件的底部添加其他文本。例如，此附加文本可用于法律、审计或合规性原因。

要向电子邮件添加其它文本：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **电子邮件**。
1. 在 **附加文本** 字段中输入您的文本。
1. 选择 **保存修改**。

## 用户停用电子邮件

当用户的帐户被停用时，系统会向用户发送电子邮件通知。

要禁用这些通知：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **电子邮件**。
1. 清除 **启用用户停用电子邮件** 复选框。
1. 选择 **保存修改**。

### 在停用电子邮件中自定义附加文本 **(FREE SELF)**

> - 引入于 15.9 版本，[功能标志](../../administration/feature_flags.md)为 `deactivation_email_additional_text`。默认禁用。
> - 在 SaaS 版和私有化部署版上启用于 15.9 版本。

FLAG:
在私有化部署版上，此功能默认可用。要隐藏此功能，需要管理员[禁用功能标志](../../administration/feature_flags.md) `deactivation_email_additional_text`。

您可以在极狐GitLab 帐户停用时，在发送给用户的电子邮件底部添加其他文本。此电子邮件文本独立于[自定义附加文本](#custom-additional-text)设置。

要向停用电子邮件添加其他文本：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 偏好设置**。
1. 展开 **电子邮件**。
1. 在 **停用电子邮件的附加文本** 字段中输入您的文本。
1. 选择 **保存更改**。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
