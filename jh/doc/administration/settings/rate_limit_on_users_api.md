---
type: reference
stage: Manage
group: Authentication & Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 用户 API 速率限制 **(FREE SELF)**

> 引入于 14.8 版本

您可以为对 Users API 的请求配置每个用户的速率限制。

要更改速率限制：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 网络**。
1. 展开 **用户 API 调用速率限制**。
1. 在 **每个用户 10 分钟内的最大请求数** 文本框中，输入新值。
1. 可选。在 **从速率限制中排除的用户** 框中，列出允许超出限制的用户。
1. 选择 **保存修改**。

此限制：

- 每个用户独立应用。
- 不适用于按每个 IP 地址限制。

默认值为 `300`。

超过速率限制的请求会被记录到 `auth.log` 文件中。

例如，如果您将限制设置为 300，则每 10 分钟超过 300 个速率的对 `GET /users/:id` API 端点的请求将被阻止。十分钟后允许访问端点。
