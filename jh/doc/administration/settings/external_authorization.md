---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 外部授权控制 **(FREE SELF)**

在高度控制的环境中，访问策略可能需要由外部服务控制，该服务允许基于项目分类和用户权限的访问。极狐GitLab 提供了一种使用您自己定义的服务，检查项目授权的方法。

配置并启用外部服务后，当访问项目时，会向外部服务发出请求，并带有分配给该项目的用户信息和项目分类标签。当服务以已知响应进行回复时，结果将被缓存六个小时。

如果启用了外部授权，系统会进一步阻止呈现跨项目数据的页面和功能。包括：

- 仪表盘下的大多数页面（活动、里程碑、代码片段、分配的合并请求、分配的议题、待办事项列表）。
- 在特定群组下（活动、贡献分析、议题、议题看板、标记、里程碑、合并请求）。
- 全局和群组搜索被禁用。

这是为了防止一次对外部授权服务执行过多的请求。

每当访问被授予或拒绝时，都会记录在一个名为 `external-policy-access-control.log` 的日志文件中。 在 [Linux 软件包文档](https://docs.gitlab.cn/omnibus/settings/logs.html)中阅读有关保留的日志的更多信息。

当使用带有自签名证书的 TLS 身份验证时，CA 证书需要被 OpenSSL 安装信任。使用通过 Omnibus 安装的极狐GitLab 时，请在 [Linux 软件包文档](https://docs.gitlab.cn/omnibus/settings/ssl/index.html) 中了解如何安装自定义 CA。
或者，使用 `openssl version -d` 了解在哪里安装自定义证书。

<a id="configuration"></a>

## 配置

外部授权服务可以由管理员启用：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **外部授权**。
1. 填写字段。
1. 选择 **保存更改**。

### 允许使用部署令牌和部署密钥进行外部授权

> - 引入于 15.9 版本。
> - 部署令牌变更为无法访问容器镜像库或软件包库于 16.0 版本。

您可以使用[部署令牌](../../user/project/deploy_tokens/index.md)或[部署密钥](../../user/project/deploy_keys/index.md)，将实例设置为允许对 Git 操作进行外部授权。

先决条件：

- 您必须使用没有服务 URL 的分类标记进行外部授权。

允许使用部署令牌和密钥进行授权：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **外部授权**，然后：
   - 将服务 URL 字段留空。
   - 选择 **允许部署令牌和部署密钥与外部授权一起使用**。
1. 选择 **保存更改**。

WARNING:
如果启用外部授权，部署令牌将无法访问容器镜像库或软件包库。如果您使用部署令牌访问这些库，则会中断对这些令牌的使用。禁用外部授权，可以将令牌用于容器镜像库或软件包库。

## 工作原理

当极狐GitLab 请求访问时，它会使用以下正文向外部服务发送 JSON POST 请求：

```json
{
  "user_identifier": "jane@acme.org",
  "project_classification_label": "project-label",
  "user_ldap_dn": "CN=Jane Doe,CN=admin,DC=acme",
  "identities": [
    { "provider": "ldap", "extern_uid": "CN=Jane Doe,CN=admin,DC=acme" },
    { "provider": "bitbucket", "extern_uid": "2435223452345" }
  ]
}
```

`user_ldap_dn` 是可选的，仅在用户通过 LDAP 登录时发送。

`identities` 包含与用户关联的所有身份的详细信息。如果没有与用户关联的身份，则这是一个空数组。

当外部授权服务响应状态码 200 时，用户被授予访问权限。当外部服务响应状态码 401 或 403 时，用户被拒绝访问。在任何情况下，请求都会被缓存六个小时。

拒绝访问时，可以选择在 JSON 正文中指定 `reason`：

```json
{
  "reason": "You are not allowed access to this project."
}
```

除 200、401 或 403 之外的任何其他状态代码也会拒绝用户访问，但不会缓存响应。

如果服务超时（500 毫秒后），则会显示 “External Policy Server did not respond” 消息。

## 分类标记

您可以在项目的 **设置 > 通用 > 通用项目设置** 页面中，在 **分类标记** 框中使用自己的分类标记。当项目上没有指定分类标记时，使用[全局设置](#configuration)中定义的默认标记。

标记显示在右上角的所有项目页面上。

![classification label on project page](img/classification_label_on_project_page_v14_8.png)

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
