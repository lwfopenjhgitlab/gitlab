---
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 自定义实例级项目模板 **(PREMIUM SELF)**

极狐GitLab 管理员可以将群组设置为在实例上创建新项目时可选择的项目模板的来源。当您转到 **新建项目 > 从模板创建** 并选择 **实例** 选项卡时，可以选择这些模板。

在创建新项目时，可以根据用户的访问权限选择群组中的每个项目，但不能选择其子组：

- 如果除 **GitLab Pages** 和 **安全与合规** 之外的所有已启用项目功能都设置为 **具有访问权限的任何人**，则任何登录用户都可以选择公共项目作为新项目的模板，同样适用于内部项目。
- 私有项目只能由项目成员的用户选择。

创建新项目时，**指标仪表板** 设置为 **仅项目成员**。在将其设为项目模板之前，请确保将其更改为 **具有访问权限的任何人**。

复制到每个新项目的仓库和数据库信息，与使用 [项目导入/导出功能](../user/project/settings/import_export.md) 导出的数据相同。

要在群组级别设置项目模板，请参阅[自定义组级项目模板](../user/group/custom_project_templates.md)。

## 选择实例级项目模板群组

选择要用作项目模板源的群组：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 模板**。
1. 展开 **自定义项目模板**。
1. 选择要使用的群组。
1. 选择 **保存修改**。

模板群组的子组中的项目**不**包含在模板列表中。

## 从模板复制的内容

复制整个自定义实例级项目模板仓库时包括：

- 分支
- 提交
- 标签

如果用户：

- 在自定义实例级项目模板项目中具有所有者角色或者是极狐GitLab 管理员，所有项目设置都将复制到新项目中。
- 没有所有者角色或不是极狐GitLab 管理员，项目[部署密钥](../user/project/deploy_keys/index.md#view-deploy-keys)和项目 [webhooks](../user/project/integrations/webhooks.md) 不会被复制，因为它们包含敏感数据。

要详细了解迁移的内容，请参阅[导出的项目](../user/project/settings/import_export.md#items-that-are-exported)。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
