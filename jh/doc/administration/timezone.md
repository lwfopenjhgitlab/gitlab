---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 更改您的时区 **(FREE SELF)**

全局时区配置参数可以在 `config/gitlab.yml` 中更改：

```plaintext
# time_zone: 'UTC'
```

如果要更改极狐GitLab 应用程序的默认时区，请取消注释并自定义。

## 查看可用时区

要查看所有可用的时区，请运行 `bundle exec rake time:zones:all`。

对于 Linux 软件包安装实例，运行 `gitlab-rake time:zones:all`。

NOTE:
此 Rake 任务不会在重新配置期间，以 Linux 软件包所需的 TZInfo 格式列出时区。

## 在 Linux 软件包安装实例中更改时区

极狐GitLab 将其时区默认为 UTC。在 `/etc/gitlab/gitlab.rb` 中有一个全局时区配置参数。

要获取时区列表，请登录到您的极狐GitLab 应用程序服务器并运行一个命令，该命令为服务器生成 TZInfo 格式的时区列表。例如，安装 `timedatectl` 并运行 `timedatectl list-timezones`。

要更新，请添加最适合您所在位置的时区。例如：

```ruby
gitlab_rails['time_zone'] = 'Asia/Shanghai'
```

添加配置参数后，重新配置并重启您的极狐GitLab 实例：

```shell
gitlab-ctl reconfigure
gitlab-ctl restart
```

## 更改每个用户的时区

> - 引入于 11.11 版本，功能标志为 `user_time_settings`，默认禁用。
> - 默认启用于 13.9 版本。
> - 功能标志删除于 14.1 版本。

用户可以在他们的个人资料中设置他们的时区。

在 14.3 及之前的版本中，未配置时区的用户默认使用[在实例级别配置](#changed-your-time-zone)的时区。<!--在 JihuLab.com 上，默认时区是 `Asia/Shanghai`。-->

新用户在 14.4 及更高版本中没有默认时区。新用户必须明确设置他们的时区，然后才能显示在他们的个人资料上。

有关详细信息，请参阅[设置您的时区](../user/profile/index.md#set-your-time-zone)。
