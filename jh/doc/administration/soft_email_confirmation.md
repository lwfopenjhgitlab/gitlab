---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 如何启用邮箱免验证登录

## 开启邮件注册

1. 以管理员身份登录。
2. 进入 **管理中心 > 设置 > 通用 > 注册限制**。
3. 勾选并保存 Sign-up enabled 和 Send confirmation email on sign-up。

![Enable Sign-up](img/enable-sign-up.jpg)


## 启用 Feature Flag

启用 soft_email_confirmation 来允许没有验证邮件的用户登录。

```ruby
::Feature.enable(:soft_email_confirmation)
```

到这里，就完成了邮箱免验证登录的设置。

