---
stage: Growth
group: Activation
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# 广播消息 **(FREE SELF)**

> - 目标角色引入于 14.8 版本，功能标志名为 `role_targeted_broadcast_messages`。默认禁用。
> - 主题引入于 14.10 版本，替代背景颜色。

系统可以向极狐GitLab 实例的用户显示广播消息。有两种类型的广播消息：

- 横幅提示（banner）
- 通知

可以使用广播消息 API<!--[广播消息 API](../../api/broadcast_messages.md)--> 管理广播消息。

## 横幅提示

横幅显示在页面顶部，也可以选择显示在命令行中作为 Git 远端响应。

![Broadcast Message Banner](img/broadcast_messages_banner_v15_0.png)

```shell
$ git push
...
remote:
remote: **Welcome** to GitLab :wave:
remote:
...
```

如果一次有多个横幅提示处于有效状态，它们将按创建顺序在页面顶部显示。在命令行中，仅显示最新的横幅。

## 通知

通知显示在页面的右下角，并且可以包含占位符。占位符被替换为激活用户的属性。占位符必须用大括号括起来，例如`{{name}}`。
可用的占位符是：

- `{{email}}`
- `{{name}}`
- `{{user_id}}`
- `{{username}}`
- `{{instance_id}}`

如果用户未登录，则与用户相关的值为空。

![Broadcast Message Notification](img/broadcast_messages_notification_v12_10.png)

如果一次有多个通知处于活动状态，则仅显示最新通知。

## 添加广播消息

要向极狐GitLab 实例上的用户显示消息，请添加广播消息。

添加广播消息：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **消息**。
1. 将消息的文本添加到 **消息** 字段。您可以使用 Markdown、表情符号以及 `a` 和 `br` HTML 标签来设置消息内容的样式。
   `br` 标签插入一个换行符。 `a` HTML 标签接受具有以下 CSS 属性的 `class` 和 `style` 属性：
    - `color`
    - `border`
    - `background`
    - `padding`
    - `margin`
    - `text-decoration`
1. 选择一个 **主题**。默认主题为 `indigo`。
1. 选中 **可忽略** 复选框，允许用户关闭广播消息。
1. （可选）清除 **Git 远端响应** 复选框，可防止广播消息在命令行中作为 Git 远端响应来显示。
1. （可选）选择 **目标角色** 仅向具有所选角色的用户显示广播消息。该消息显示在群组、子组和项目页面上，而不显示在 Git 远程响应中。
1. 如果需要，添加 **目标路径**，仅在与该路径匹配的 URL 上显示广播消息。您可以使用通配符 `*` 来匹配多个 URL，例如 `mygroup/myproject*`。
1. 选择消息开始和结束的日期和时间（UTC）。
1. 选择 **添加广播消息**。

当广播消息过期时，它不再显示在用户界面中，但仍列在广播消息列表中。

## 编辑广播消息

如果您必须对广播消息进行更改，您可以对其进行编辑。

要编辑广播消息：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **消息**。
1. 从广播消息列表中，选择消息的编辑按钮。
1. 进行必要的更改后，选择 **更新广播消息**。

过期消息可以通过更改结束日期再次激活。

## 删除广播消息

如果您不再需要广播消息，可以将其删除。
您可以在广播消息处于有效状态时将其删除。

要删除广播消息：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **消息**。
1. 从广播消息列表中，选择消息的删除按钮。

删除广播消息后，它将从广播消息列表中删除。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
