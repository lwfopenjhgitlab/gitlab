---
stage: Create
group: Code Review
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# 议题关闭样式 **(FREE SELF)**

NOTE:
本页说明管理员如何配置议题关闭样式。
获取更多信息，请查看[自动关闭议题文档](../user/project/issues/managing_issues.md#closing-issues-automatically)。

当提交或合并请求解决了一个或多个议题时，可以在提交或合并请求到达项目的默认分支时自动关闭这些议题。

## 更改议题关闭样式

[默认议题关闭样式](../user/project/issues/managing_issues.md#default-closing-pattern)支持许多单词用语。您可以更改样式来满足您的需要。

NOTE:
要测试议题关闭样式，请使用 <https://rubular.com>。
然而，Rubular 不识别 `%{issue_ref}`。测试您的样式时，将此字符串替换为 `#\d+`，它仅匹配本地议题引用，如 `#123`。

要更改默认议题关闭样式：

**Omnibus**

1. 编辑 `/etc/gitlab/gitlab.rb` 并更改 `gitlab_rails['gitlab_issue_closing_pattern']` 的值：

   ```ruby
   gitlab_rails['gitlab_issue_closing_pattern'] = /<regular_expression>/.source
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 导出 Helm 值：

   ```shell
   helm get values gitlab > gitlab_values.yaml
   ```

1. 编辑 `gitlab_values.yaml` 并更改 `issueClosingPattern` 值：

   ```yaml
   global:
     appConfig:
       issueClosingPattern: "<regular_expression>"
   ```

1. 保存文件并应用新值：

   ```shell
   helm upgrade -f gitlab_values.yaml gitlab gitlab/gitlab
   ```

**Docker**

1. 编辑 `docker-compose.yml` 并更改 `gitlab_rails['gitlab_issue_closing_pattern']` 的值：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           gitlab_rails['gitlab_issue_closing_pattern'] = /<regular_expression>/.source
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   docker compose up -d
   ```

**源安装**

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并更改 `issue_closing_pattern` 的值：

   ```yaml
   production: &base
     gitlab:
       issue_closing_pattern: "<regular_expression>"
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```
