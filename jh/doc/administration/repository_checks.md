---
stage: Systems
group: Gitaly
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 仓库检查 **(FREE SELF)**

您可以使用 [`git fsck`](https://git-scm.com/docs/git-fsck) 来验证提交到仓库的所有数据的完整性。极狐GitLab 管理员可以：

- 使用 UI 为项目手动触发此检查。
- 安排此检查为所有项目自动运行。
- 从命令行运行此检查。
- 运行 [Rake 任务](raketasks/check.md#repository-integrity)来检查 Git 仓库。作为比较不同服务器上存储库的一种方式，可对所有仓库运行 `git fsck` 并生成存储库校验和。

## 使用 UI 检查项目的仓库

使用 UI 检查项目的仓库：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **概览 > 项目**。
1. 选择要检查的项目。
1. 在 **仓库检查** 部分，选择 **触发仓库检查**。

检查是异步运行的，因此可能需要几分钟才能在管理中心的项目页面上看到检查结果。如果检查失败，请参阅[文档](#what-to-do-if-a-check-failed)。

## 为所有项目启用仓库检查

可以将极狐GitLab 配置为定期运行检查，而不是手动检查仓库：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **设置 > 仓库** (`/admin/application_settings/repository`)。
1. 展开 **仓库维护** 部分。
1. 启用 **启用仓库检查**。

启用后，极狐GitLab 会定期对所有项目仓库和 wiki 仓库运行仓库检查，检测可能的数据损坏。一个项目每月检查不超过一次。
管理员可以配置仓库检查的频率。编辑频率：

- 对于 Linux 软件包安装实例，编辑 `/etc/gitlab/gitlab.rb` 中的 `gitlab_rails['repository_check_worker_cron']`。
- 对于源安装，在 `/home/git/gitlab/config/gitlab.yml` 中编辑 `[gitlab.cron_jobs.repository_check_worker]`。

如果任何项目未通过仓库检查，所有极狐GitLab 管理员都会收到有关情况的电子邮件通知。默认情况下，此通知每周在周日开始的午夜发送一次。

可以在 `/admin/projects?last_repository_check_failed=1` 找到已知检查失败的仓库。

## 使用命令行运行检查

您可以在 Gitaly 服务器上的仓库使用命令行运行 [`git fsck`](https://git-scm.com/docs/git-fsck)。要找到仓库：

1. 转到仓库的存储位置：
   - 对于 Linux 软件包安装，仓库默认存储在 `/var/opt/gitlab/git-data/repositories` 目录中。
   - 对于 Helm charts 安装，仓库默认存储在 Gitaly pod 内的 `/home/git/repositories` 目录中。
1. [确定包含您需要检查的仓库的子目录](repository_storage_types.md#from-project-name-to-hashed-path)。
1. 运行检查。例如：

   ```shell
   sudo -u git /opt/gitlab/embedded/bin/git \
      -C /var/opt/gitlab/git-data/repositories/@hashed/0b/91/0b91...f9.git fsck --no-dangling
   ```

   错误 `fatal: detected dubious ownership in repository` 意味着您正在使用错误的帐户运行命令，例如 `root`。

<a id="what-to-do-if-a-check-failed"></a>

## 如果检查失败怎么办

如果存储库检查失败，请在磁盘上的 `repocheck.log` 文件中找到错误：

- Linux 软件包安装实例：`/var/log/gitlab/gitlab-rails`。
- 源安装实例：`/home/git/gitlab/log`。
- Helm charts 安装的 Sidekiq pod 中：`/var/log/gitlab`。

如果定期仓库检查导致错误警报，您可以清除所有仓库检查状态：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 在左侧边栏上，选择 **设置 > 仓库**（`/admin/application_settings/repository`）。
1. 展开 **仓库维护** 部分。
1. 选择 **清除所有仓库检查**。

### 错误：`failed to parse commit <commit SHA> from object database for commit-graph`

您可以在仓库检查日志中看到 `failed to parse commit <commit SHA> from object database for commit-graph` 错误。如果您的 `commit-graph` 缓存已过期，则会发生此错误。`commit-graph` 缓存是辅助缓存，常规 Git 操作不需要。

### 丢弃提交、标签或 blob 消息

仓库检查输出通常包括必须修剪丢弃的标签、blob 和提交：

```plaintext
dangling tag 5c6886c774b713a43158aae35c4effdb03a3ceca
dangling blob 3e268c23fcd736db92e89b31d9f267dd4a50ac4b
dangling commit 919ff61d8d78c2e3ea9a32701dff70ecbefdd1d7
```

这种情况在 Git 仓库中很常见。它们是由强制推送到分支等操作生成的，因为这会在仓库中生成一个提交，该提交不再被 ref 或另一个提交引用。

如果仓库检查失败，输出可能包含这些警告。

忽略这些消息，并从其他输出中确定仓库检查失败的根本原因。

15.8 及更高版本不再将这些消息包含在检查输出中。当从命令行运行时，使用 `--no-dangling` 选项来禁用。
