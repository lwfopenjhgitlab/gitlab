---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# 极狐GitLab CI/CD 实例配置 **(FREE SELF)**

极狐GitLab 管理员可以管理其实例的 CI/CD 配置。

## 在新项目中禁用极狐GitLab CI/CD

极狐GitLab CI/CD 在实例上的所有新项目中默认启用。您可以通过修改以下设置，在新项目中将 CI/CD 设置为默认禁用：

- `gitlab.yml` 用于源安装。
- `gitlab.rb` 用于 Linux 软件包安装。

已启用 CI/CD 的现有项目保持不变。此外，此设置仅更改项目默认设置，因此项目所有者[仍然可以在项目设置中启用 CI/CD](../ci/enable_or_disable_ci.md#enable-cicd-in-a-project)。

对于源安装实例：

1. 使用编辑器打开 `gitlab.yml` 并将 `builds` 设置为 `false`：

   ```yaml
   ## Default project features settings
   default_projects_features:
     issues: true
     merge_requests: true
     wiki: true
     snippets: false
     builds: false
   ```

1. 保存 `gitlab.yml` 文件。

1. 重启极狐GitLab：

   ```shell
   sudo service gitlab restart
   ```

对于 Linux 软件包安装实例：

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加这一行：

   ```ruby
   gitlab_rails['gitlab_default_projects_features_builds'] = false
   ```

1. 保存 `/etc/gitlab/gitlab.rb` 文件。

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

## 设置 `needs` 作业限制 **(FREE SELF)**

`needs` 中可以定义的最大作业数默认为 50。

具有[访问 Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)的极狐GitLab 管理员可以选择自定义限制。例如，要将限制设置为 `100`：

```ruby
Plan.default.actual_limits.update!(ci_needs_size_limit: 100)
```

要禁用有向无环图 (DAG)，请将限制设置为 `0`。作业配置为使用 needs 的流水线，然后返回错误 `job can only need 0 others`。

<a id="change-maximum-scheduled-pipeline-frequency"></a>

## 更改最大计划流水线频率

[计划流水线](../ci/pipelines/schedules.md)可以使用任何 cron 值<!--[cron 值](../topics/cron/index.md)-->进行配置，但它们并不总是在计划时间准确运行。一个称为计划调度 worker 的内部进程将所有已调度的流水线排入队列，但不会连续运行。Worker 按自己的计划运行，准备启动的计划流水线仅在 worker 下次运行时排队。计划流水线不能比 worker 更频繁地运行。

流水线调度 worker 的默认频率是 `3-59/10 * * * *`（每十分钟一次，从 `0:03`、`0:13`、`0:23` 等开始）。<!--GitLab.com 的默认频率列在 [GitLab.com 设置](../user/gitlab_com/index.md#gitlab-cicd) 中。-->

要更改流水线调度 worker 的频率：

1. 编辑实例的 `gitlab.rb` 文件中的 `gitlab_rails['pipeline_schedule_worker_cron']` 值。
1. [重新配置极狐GitLab](restart_gitlab.md#reconfigure-a-linux-package-installation)，使更改生效。

例如，要将流水线的最大频率设置为一天两次，请将 `pipeline_schedule_worker_cron` 设置为 `0 */12 * * *` 的 cron 值（`00:00` 和 `12:00` 每天）。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
