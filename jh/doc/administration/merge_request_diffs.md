---
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 合并请求差异存储 **(FREE SELF)**

合并请求差异是与合并请求相关的差异的大小限制副本。查看合并请求时，作为性能优化，尽可能从这些副本中获取差异。

默认情况下，合并请求差异存储在数据库中名为 `merge_request_diff_files` 的表中。在较大的安装实例中，您可能会发现此表变得太大，在这种情况下，建议切换到外部存储。

合并请求差异可以存储在[磁盘上](#using-external-storage)或[对象存储](#using-object-storage)中。通常，将差异存储在数据库中比存储在磁盘上要好。可以使用一种折衷方案，即仅在数据库之外[存储过时的差异](#alternative-in-database-storage)。

<a id="using-external-storage"></a>

## 使用外部存储

**Linux 软件包安装：**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['external_diffs_enabled'] = true
   ```

1. 外部差异存储在 `/var/opt/gitlab/gitlab-rails/shared/external-diffs` 中。要更改路径，例如，更改为 `/mnt/storage/external-diffs`，请编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['external_diffs_storage_path'] = "/mnt/storage/external-diffs"
   ```

1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#reconfigure-a-linux-package-installation)，使更改生效。极狐GitLab 然后将您现有的合并请求差异迁移到外部存储。

**源安装：**

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   external_diffs:
     enabled: true
   ```

1. 外部差异存储在 `/home/git/gitlab/shared/external-diffs` 中。要更改路径，例如更改为 `/mnt/storage/external-diffs`，请编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   external_diffs:
     enabled: true
     storage_path: /mnt/storage/external-diffs
   ```

1. 保存文件并[重启极狐GitLab](restart_gitlab.md#installations-from-source)，使更改生效。极狐GitLab 然后将您现有的合并请求差异迁移到外部存储。

<a id="using-object-storage"></a>

## 使用对象存储

WARNING:
迁移到对象存储是不可逆的。

我们建议使用 AWS S3 之类的对象存储，而不是将外部差异存储在磁盘上。此配置依赖于已配置的有效 AWS 凭证。

**Linux 软件包安装：**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['external_diffs_enabled'] = true
   ```

1. 编辑[对象存储设置](#object-storage-settings)。
1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。极狐GitLab 然后将您现有的合并请求差异迁移到外部存储。

**源安装：**

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   external_diffs:
     enabled: true
   ```

1. 编辑[对象存储设置](#object-storage-settings)。
1. 保存文件并[重启极狐GitLab](restart_gitlab.md#installations-from-source)，使更改生效。极狐GitLab 然后将您现有的合并请求差异迁移到外部存储。

<!--
[Read more about using object storage with GitLab](object_storage.md).
-->

<a id="object-storage-settings"></a>

### 对象存储设置

NOTE:
在 13.2 及更高版本中，我们建议使用统一对象存储设置<!--[统一对象存储设置](object_storage.md#consolidated-object-storage-configuration)-->。
本节介绍早期的配置格式。

对于源安装，这些设置嵌套在 `external_diffs:` 下，然后是 `object_store:`。在 Omnibus 安装中，它们以 `external_diffs_object_store_` 为前缀。

| 设置 | 描述 | 默认值 |
|---------|-------------|---------|
| `enabled` | 启用/禁用对象存储 | `false` |
| `remote_directory` | 存储外部差异的存储桶名称 | |
| `proxy_download` | 设置为 `true` 以启用代理所有提供的文件。选项允许减少出口流量，因为这允许客户端直接从远程存储下载而不是代理所有数据 | `false` |
| `connection` | 下面描述的各种连接选项 | |

#### S3 兼容连接设置

<!--
See [the available connection settings for different providers](object_storage.md#connection-settings).
-->

**Linux 软件包安装：**

1. 编辑 `/etc/gitlab/gitlab.rb` 并通过替换为您想要的值添加以下行：

   ```ruby
   gitlab_rails['external_diffs_enabled'] = true
   gitlab_rails['external_diffs_object_store_enabled'] = true
   gitlab_rails['external_diffs_object_store_remote_directory'] = "external-diffs"
   gitlab_rails['external_diffs_object_store_connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-central-1',
     'aws_access_key_id' => 'AWS_ACCESS_KEY_ID',
     'aws_secret_access_key' => 'AWS_SECRET_ACCESS_KEY'
   }
   ```

   如果您使用 AWS IAM 配置文件，请省略 AWS 访问密钥和秘密访问密钥/值对。例如：

   ```ruby
   gitlab_rails['external_diffs_object_store_connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-central-1',
     'use_iam_profile' => true
   }
   ```

1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

**源安装：**

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   external_diffs:
     enabled: true
     object_store:
       enabled: true
       remote_directory: "external-diffs" # The bucket name
       connection:
         provider: AWS # Only AWS supported at the moment
         aws_access_key_id: AWS_ACCESS_KEY_ID
         aws_secret_access_key: AWS_SECRET_ACCESS_KEY
         region: eu-central-1
   ```

1. 保存文件并[重启极狐GitLab](restart_gitlab.md#installations-from-source)，使更改生效。

<a id="alternative-in-database-storage"></a>

## 替代的数据库内存储

启用外部差异可能会降低合并请求的性能，因为它们必须在单独的操作中检索到其他数据。通过仅在外部存储过时的差异，同时将当前的差异保留在数据库中，作为妥协的方法。

要启用此功能，请执行以下步骤：

**Linux 软件包安装：**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['external_diffs_when'] = 'outdated'
   ```

1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#omnibus-gitlab-reconfigure)，使更改生效。

**源安装：**

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   external_diffs:
     enabled: true
     when: outdated
   ```

1. 保存文件并[重启极狐GitLab](restart_gitlab.md#installations-from-source)，使更改生效。

启用此功能后，差异最初存储在数据库中，而不是外部。在满足以下任何条件后，它们将被移至外部存储：

- 存在更新版本的合并请求差异
- 合并请求在超过 7 天前被合并
- 合并请求在超过 7 天前关闭

这些规则通过仅在数据库中存储经常访问的差异，来在空间和性能之间取得平衡。不太可能被访问的差异被移动到外部存储。

<a id="correcting-incorrectly-migrated-diffs"></a>

## 纠正错误迁移的差异

当启用[对象存储中的外部差异](#object-storage-settings)时，低于 `v13.0.0` 的极狐GitLab 版本会错误地记录某些合并请求差异的位置。这主要影响导入的合并请求。

如果您正在使用对象存储，或者从未将磁盘存储用于外部差异，则某些合并请求的 **变更** 选项卡无法加载并出现 500 错误，并且该错误的异常是以下形式：

```plain
Errno::ENOENT (No such file or directory @ rb_sysopen - /var/opt/gitlab/gitlab-rails/shared/external-diffs/merge_request_diffs/mr-6167082/diff-8199789)
```

然后您就会受到这个问题的影响。因为不可能安全地自动确定所有这些条件，我们在 13.2.0 版本中提供了一个 Rake 任务，您可以手动运行它来更正数据：

**Linux 软件包安装：**

```shell
sudo gitlab-rake gitlab:external_diffs:force_object_storage
```

**源安装：**

```shell
sudo -u git -H bundle exec rake gitlab:external_diffs:force_object_storage RAILS_ENV=production
```

可以提供环境变量来修改任务的行为。可用的变量是：

| 名称 | 默认值 | 目的 |
| ---- | ------------- | ------- |
| `ANSI`         | `true`  | 使用 ANSI 转义码使输出更易于理解 |
| `BATCH_SIZE`   | `1000`  | 以何种大小批量遍历表 |
| `START_ID`     | `nil`   | 如果设置，则开始扫描此 ID |
| `END_ID`       | `nil`   | 如果设置，则在此 ID 处停止扫描 |
| `UPDATE_DELAY` | `1`     | 更新之间休眠的秒数 |

`START_ID` 和 `END_ID` 变量可用于并行运行更新，方法是将不同的进程分配给表的不同部分。`BATCH` 和 `UPDATE_DELAY` 参数允许在迁移速度与对表的并发访问之间进行权衡。如果您的终端不支持 ANSI 转义码，则 `ANSI` 参数应设置为 false。

默认情况下，`sudo` 不保留现有的环境变量。您应该附加它们，而不是为它们添加前缀。

```shell
sudo gitlab-rake gitlab:external_diffs:force_object_storage START_ID=59946109 END_ID=59946109 UPDATE_DELAY=5
```

## 从外部存储切换到对象存储

自动迁移会移动存储在数据库中的差异，但不会在存储类型之间移动差异。
从外部存储切换到对象存储：

1. 将存储在本地或 NFS 存储上的文件手动移动到对象存储。
1. 运行[上一节](#correcting-incorrectly-migrated-diffs)中的 Rake 任务，更改它们在数据库中的位置。
