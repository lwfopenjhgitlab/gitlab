---
stage: Growth
group: Conversion
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 激活极狐GitLab **(PREMIUM SELF)**

当您在没有许可证的情况下安装新的极狐GitLab 实例时，只启用免费版功能。要启用极狐GitLab 的更多功能，请使用许可证文件激活您的实例。

<!--
## 激活

In GitLab Enterprise Edition 14.1 and later, you need an activation code to activate
your instance.

Prerequisite:

- You must [purchase a subscription](https://about.gitlab.com/pricing/).
- You must be running GitLab Enterprise Edition (EE).
- You must have GitLab 14.1 or later.
- Your instance must be connected to the internet.

To activate your instance with an activation code:

1. Copy the activation code, a 24-character alphanumeric string, from either:
   - Your subscription confirmation email.
   - The [Customers Portal](https://customers.gitlab.com/customers/sign_in), on the **Manage Purchases** page.
1. Sign in to your GitLab self-managed instance.
1. On the top bar, select **Main menu > Admin**.
1. On the left sidebar, select **Subscription**.
1. Paste the activation code in **Activation code**.
1. Read and accept the terms of service.
1. Select **Activate**.

The subscription is activated.
-->

如果您有离线环境，请[使用许可证文件或密钥激活](license_file.md)。

如果您有问题或需要帮助激活您的实例，[联系技术支持](https://about.gitlab.cn/support/#contact-support)。

当[许可证过期](license_file.md#what-happens-when-your-license-expires)时，某些功能被锁定。

## 验证您的极狐GitLab 版本

要激活您的实例，请确保您正在运行极狐GitLab。

要验证版本，请登录极狐GitLab 并选择 **帮助** (**{question-o}**) > **帮助**。极狐GitLab 版本和级别列在页面顶部。

如果您正在运行 GitLab 社区版（CE，Community Edition），可以升级到极狐GitLab。在升级过程中出现问题或需要协助时，请您[联系技术支持](https://about.gitlab.cn/support/#contact-support)。

<!--
### 由于连接错误，无法激活实例

In GitLab 14.1 and later, to activate your subscription with an activation code,
your GitLab instance must be connected to the internet.

If you have an offline or airgapped environment,
[upload a license file](license.md#activate-gitlab-ee-with-a-license-file) instead.

If you have questions or need assistance activating your instance,
[contact GitLab Support](https://about.gitlab.com/support/#contact-support).
-->