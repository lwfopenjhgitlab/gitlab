---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 修改全局用户设置 **(FREE SELF)**

极狐GitLab 管理员可以修改整个极狐GitLab 实例的用户设置。

<a id="use-configuration-files-to-prevent-new-users-from-creating-top-level-groups"></a>

## 使用配置文件阻止新用户创建顶级群组

默认情况下，新用户可以创建顶级群组。要禁用新用户创建顶级组的能力（不影响现有用户的设置），极狐GitLab 管理员可以修改此设置：

- 在 15.5 及更高版本中，使用：
   - [极狐GitLab 用户界面](../administration/settings/account_and_limit_settings.md#prevent-new-users-from-creating-top-level-groups)。
   - [应用程序设置 API](../api/settings.md#change-application-settings)。
- 在 15.4 及更早版本中，按照本节中的步骤，在配置文件中设置。

要禁用新用户使用配置文件创建顶级组的能力：

**Linux 软件包安装实例**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['gitlab_default_can_create_group'] = false
   ```

1. [重新配置并重启GitLab](restart_gitlab.md#reconfigure-a-linux-package-installation)。

**源安装实例**

1. 编辑 `config/gitlab.yml` 并取消注释以下行：

   ```yaml
   # default_can_create_group: false  # default: true
   ```

1. [重启极狐GitLab](restart_gitlab.md#installations-from-source)。

### 防止现有用户创建顶级群组

管理员可以：

- 使用管理中心[组织现有用户创建顶级群组](../administration/admin_area.md#prevent-a-user-from-creating-groups)。
- 使用[修改现有的用户 API 端点](../api/users.md#user-modification)更改`can_create_group` 设置。

## 防止用户更改其用户名

默认情况下，新用户可以更改他们的用户名。要禁用您的用户更改其用户名的能力：

**Omnibus 安装实例**

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['gitlab_username_changing_enabled'] = false
   ```

1. [重新配置和重启极狐GitLab](restart_gitlab.md#reconfigure-a-linux-package-installation)。

**源安装实例**

1. 编辑 `config/gitlab.yml` 并取消注释以下行：

   ```yaml
   # username_changing_enabled: false # default: true - User can change their username/namespace
   ```

1. [重启极狐GitLab](restart_gitlab.md#installations-from-source)。
