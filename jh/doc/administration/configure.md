---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 配置您的实例

自定义和配置您的实例。

- [认证](auth/ldap/index.md)
- [配置](../administration/admin_area.md)
- [仓库存储](repository_storage_paths.md)
- [安装包](packages/index.md)
- [实名制](real_name.md)
