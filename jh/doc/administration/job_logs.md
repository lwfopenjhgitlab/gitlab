---
stage: Verify
group: Runner
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 作业日志 **(FREE SELF)**

作业日志由 runner 在处理作业时发送。您可以在作业页面、流水线、电子邮件通知等中查看日志。

<a id="data-flow"></a>

## 数据流

一般来说，作业日志有两种状态：`log` 和 `archived log`。
在下表中，您可以看到日志经历的阶段：

| 阶段          | 状态        | 环境               | 数据流                                | 存储路径 |
| -------------- | ------------ | ----------------------- | -----------------------------------------| ----------- |
| 1: patching    | log          | 作业运行时   | Runner => Puma => 文件存储 | `#{ROOT_PATH}/gitlab-ci/builds/#{YYYY_mm}/#{project_id}/#{job_id}.log` |
| 2: archiving   | archived log | 作业已完成 | Sidekiq 将日志移动到产物文件夹    | `#{ROOT_PATH}/gitlab-rails/shared/artifacts/#{disk_hash}/#{YYYY_mm_dd}/#{job_id}/#{job_artifact_id}/job.log` |
| 3: uploading   | archived log | 日志已归档 | Sidekiq 将归档日志移动到[对象存储](#将日志上传到对象存储)（如果已配置） | `#{bucket_name}/#{disk_hash}/#{YYYY_mm_dd}/#{job_id}/#{job_artifact_id}/job.log` |

`ROOT_PATH` 因环境而异。对于 Omnibus GitLab，为 `/var/opt/gitlab`，对于从源代码安装，为 `/home/git/gitlab`。

<a id="changing-the-job-logs-local-location"></a>

## 更改作业日志本地位置

NOTE:
对于 Docker 安装实例，您可以更改装载数据的路径。对于 Helm 安装实例，使用对象存储。

要更改作业日志的存储位置：

**Omnibus**

1. 可选。如果您有现有的作业日志，请通过暂时停止 Sidekiq 来暂停持续集成数据处理：

   ```shell
   sudo gitlab-ctl stop sidekiq
   ```

1. 在 `/etc/gitlab/gitlab.rb` 中设置新的存储位置：

   ```ruby
   gitlab_ci['builds_directory'] = '/mnt/gitlab-ci/builds'
   ```

1. 保存文件并重新配置极狐GitLab。

   ```shell
   sudo gitlab-ctl reconfigure
   ```

1. 使用 `rsync` 将作业日志从当前位置移动到新位置：

   ```shell
   sudo rsync -avzh --remove-source-files --ignore-existing --progress /var/opt/gitlab/gitlab-ci/builds/ /mnt/gitlab-ci/builds/
   ```

   使用 `--ignore-existing` 这样就不会用旧版本的相同日志覆盖新的作业日志。

1. 如果您选择了暂停持续集成数据处理，您可以重新启动 Sidekiq：

   ```shell
   sudo gitlab-ctl start sidekiq
   ```

1. 删除旧的作业日志存储位置：

   ```shell
   sudo rm -rf /var/opt/gitlab/gitlab-ci/builds`
   ```

**源安装**

1. 可选。如果您有现有的作业日志，请通过暂时停止 Sidekiq 来暂停持续集成数据处理：

   ```shell
   # For systems running systemd
   sudo systemctl stop gitlab-sidekiq

   # For systems running SysV init
   sudo service gitlab stop
   ```

1. 编辑 `/home/git/gitlab/config/gitlab.yml`，设置新的存储位置。

   ```yaml
   production: &base
     gitlab_ci:
       builds_path: /mnt/gitlab-ci/builds
   ```

1. 保存文件并重启极狐GitLab：

   ```shell
   # For systems running systemd
   sudo systemctl restart gitlab.target

   # For systems running SysV init
   sudo service gitlab restart
   ```

1. 使用 `rsync` 将作业日志从当前位置移动到新位置：

   ```shell
   sudo rsync -avzh --remove-source-files --ignore-existing --progress /home/git/gitlab/builds/ /mnt/gitlab-ci/builds/
   ```

   使用 `--ignore-existing` 这样您就不会用同一日志的旧版本覆盖新的作业日志。

1. 如果您选择了暂停持续集成数据处理，您可以重新启动 Sidekiq：

   ```shell
   # For systems running systemd
   sudo systemctl start gitlab-sidekiq

   # For systems running SysV init
   sudo service gitlab start
   ```

1. 删除旧的作业日志存储位置：

   ```shell
   sudo rm -rf /home/git/gitlab/builds
   ```

## 将日志上传到对象存储

归档日志被视为[作业产物](job_artifacts.md)。
因此，当您[设置对象存储集成](job_artifacts.md#using-object-storage)时，作业日志会与其他作业产物一起自动迁移到其上。

查看[数据流](#data-flow)中的 "Phase 3: uploading" 了解流程。

<a id="prevent-local-disk-usage"></a>

## 防止使用本地磁盘

如果要避免作业日志使用任何本地磁盘，可以使用以下选项之一：

- 启用[增量日志记录](#incremental-logging-architecture) 功能。
- 将[作业日志位置](#changing-the-job-logs-local-location)设置为 NFS 驱动器。

## 如何删除作业日志

没有办法让旧的作业日志自动过期，但如果它们占用太多空间，可以安全地删除它们。如果手动删除日志，则 UI 中的作业输出为空。

例如，要删除超过 60 天的所有作业日志，请从极狐GitLab 实例中的 shell 运行以下命令：

NOTE:
对于 Helm chart，请使用对象存储随附的存储管理工具。

WARNING:
以下命令将永久删除日志文件且不可逆。

**Omnibus**

```shell
find /var/opt/gitlab/gitlab-rails/shared/artifacts -name "job.log" -mtime +60 -delete
```

**Docker**

假设您将 `/var/opt/gitlab` 挂载到 `/srv/gitlab`：

```shell
find /srv/gitlab/gitlab-rails/shared/artifacts -name "job.log" -mtime +60 -delete
```

**源安装**

```shell
find /home/git/gitlab/shared/artifacts -name "job.log" -mtime +60 -delete
```

删除日志后，您可以通过运行检查[上传文件的完整性](raketasks/check.md#uploaded-files-integrity)的 Rake 任务，找到任何损坏的文件引用。
有关详细信息，请参阅如何[删除对缺失产物的引用](raketasks/check.md#delete-references-to-missing-artifacts)。

<a id="incremental-logging-architecture"></a>

## 增量日志架构

> - 部署在功能标志后默认禁用。
> - 推荐生产使用于 13.6 版本。
> - 推荐与 AWS S3 生产使用于 13.7 版本。

<!--
> - To use in GitLab self-managed instances, ask a GitLab administrator to [enable it](#enable-or-disable-incremental-logging).
-->

默认情况下，作业日志以块的形式从极狐GitLab Runner 发送，临时缓存在磁盘上。作业完成后，后台作业会归档作业日志。日志默认移动到产物目录，或者已配置的对象存储上。

在一个横向扩展架构<!--[横向扩展架构](reference_architectures/index.md)-->中，Rails 和 Sidekiq 运行在多个服务器上，文件系统上的这两个位置必须使用 NFS 共享，只不是推荐用法，替代方法为：

1. 配置[对象存储](job_artifacts.md#using-object-storage)用于存储归档作业日志。
1. [启用增量日志功能](#enable-or-disable-incremental-logging)，它使用 Redis 代替磁盘空间来临时缓存作业日志。

<a id="enable-or-disable-incremental-logging"></a>

### 启用或禁用增量日志

在启用功能标志之前：

- 查看[增量日志记录的限制](#limitations)。
- [启用对象存储](job_artifacts.md#using-object-storage)。

要启用增量日志记录：

1. 打开 [Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)。
1. 启用功能标志：

   ```ruby
   Feature.enable(:ci_enable_live_trace)
   ```

   运行作业的日志会继续写入磁盘，但新作业使用增量日志记录。

要禁用增量日志记录：

1. 打开 [Rails 控制台](operations/rails_console.md#starting-a-rails-console-session)。
1. 禁用功能标志：

   ```ruby
   Feature.disable(:ci_enable_live_trace)
   ```

   正在运行的作业继续使用增量日志记录，但新作业会写入磁盘。

### 技术细节

数据流与[数据流部分](#数据流)中描述的相同，有一个变化：*前两个阶段的存储路径不同*。这种增量日志架构将日志块存储在 Redis 和持久存储（对象存储或数据库）中，而不是文件存储中。Redis 用作一流的存储，它可以存储高达 128KB 的数据。发送完整块后，将其刷新到持久存储，对象存储（临时目录）或数据库。
过了一会儿，Redis 中的数据和一个持久化存储被归档到[对象存储](#将日志上传到对象存储)。

数据存储在以下 Redis 命名空间中：`Gitlab::Redis::TraceChunks`。

下面是详细的数据流：

1. Runner 从极狐GitLab 中挑选一个作业
1. Runner 向极狐GitLab 发送一条日志
1. 极狐GitLab 将数据追加到 Redis
1. Redis 中的数据达到128KB 后，将数据刷新到持久化存储（对象存储或数据库）中。
1. 重复以上步骤，直到作业完成。
1. 作业完成后，极狐GitLab 会安排一个 Sidekiq worker 来归档日志。
1. Sidekiq worker 将日志归档到对象存储并清理 Redis 和持久存储（对象存储或数据库）中的日志。

### 限制

- 不支持 Redis 集群
- 在启用功能标志之前，您必须配置 [CI/CD 产物、日志和构建的对象存储](job_artifacts.md#对象存储设置)。启用该标志后，将无法将文件写入磁盘，并且无法防止配置错误。
- 有一个史诗跟踪其他潜在的限制和改进
