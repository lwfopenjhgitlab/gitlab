---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 在极狐GitLab 中进行维护 **(FREE SELF)**

保持您的极狐GitLab 实例正常运行。

- [Rake 任务](../../raketasks/index.md)：常见管理和操作流程的任务，例如[从实例中清理不需要的项目](../../raketasks/cleanup.md)、完整性检查等。
- [移动仓库](moving_repositories.md)：将极狐GitLab 管理的所有仓库移动到另一个文件系统或另一个服务器。
- [Sidekiq MemoryKiller](../sidekiq/sidekiq_memory_killer.md)：配置 Sidekiq MemoryKiller 来重启 Sidekiq。
- [多 Sidekiq 进程](../sidekiq/extra_sidekiq_processes.md)：配置多个 Sidekiq 进程以确保某些队列始终有专门的 worker，无论必须处理的作业数量如何。**(FREE SELF)**
- [Puma](puma.md)：了解 Puma 和 puma-worker-killer。
- [`gitlab-sshd`](gitlab_sshd.md)：使用极狐GitLab SSH daemon 进程替代 OpenSSH。
- 通过对极狐GitLab 数据库[进行快速索引查找来授权 SSH 用户](fast_ssh_key_lookup.md)，和/或[完全取消存储在极狐GitLab 上的用户 SSH 密钥以支持 SSH 证书来加速 SSH 操作](ssh_certificates.md)。
- [文件系统性能基准测试](filesystem_benchmarking.md)：文件系统性能会对极狐GitLab 性能产生很大影响，尤其是对于读取或写入 Git 仓库的操作。此信息有助于根据已知的好和坏的现实世界系统对文件系统性能进行基准测试。
- [Rails 控制台](rails_console.md)：提供一种从命令行与极狐GitLab 实例交互的方法。用于解决问题或检索某些只能通过直接访问极狐GitLab 来完成的数据。

<!--
- [ChatOps Scripts](https://gitlab.com/gitlab-com/chatops): The GitLab.com Infrastructure team uses this repository to house
  common ChatOps scripts they use to troubleshoot and maintain the production instance of GitLab.com.
  These scripts are likely useful to administrators of GitLab instances of all sizes.
-->