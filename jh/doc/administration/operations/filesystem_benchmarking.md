---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 文件系统性能基准测试 **(FREE SELF)**

文件系统性能对极狐GitLab 的整体性能有很大影响，尤其是对于读取或写入 Git 仓库的操作。此信息有助于针对已知的好坏实际系统对文件系统性能进行基准测试。

通常，在谈论文件系统性能时，最大的担忧是网络文件系统 (NFS)。但是，即使是某些本地磁盘也可能具有缓慢的 I/O。此页面上的信息可用于任一场景。

## 执行基准

### 使用 `fio` 进行基准测试

我们推荐使用 [Fio](https://fio.readthedocs.io/en/latest/fio_doc.html) 来测试 I/O 性能。此测试应在 NFS 服务器和与 NFS 服务器通信的应用程序节点上运行。

安装：

- 在 Ubuntu 上：`apt install fio`。
- 在 `yum` 管理的环境中：`yum install fio`。

然后运行以下命令：

```shell
fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --bs=4k --iodepth=64 --readwrite=randrw --rwmixread=75 --size=4G --filename=/path/to/git-data/testfile
```

这会在 `/path/to/git-data/testfile` 中创建一个 4GB 的文件。它使用文件中的 75%/25% 拆分执行 4KB 读取和写入，一次运行 64 个操作。请务必在测试完成后删除该文件。

输出取决于安装的 `fio` 版本。以下是网络固态驱动器 (SSD) 上 `fio` v2.2.10 的示例输出：

```plaintext
test: (g=0): rw=randrw, bs=4K-4K/4K-4K/4K-4K, ioengine=libaio, iodepth=64
    fio-2.2.10
    Starting 1 process
    test: Laying out IO file(s) (1 file(s) / 1024MB)
    Jobs: 1 (f=1): [m(1)] [100.0% done] [131.4MB/44868KB/0KB /s] [33.7K/11.3K/0 iops] [eta 00m:00s]
    test: (groupid=0, jobs=1): err= 0: pid=10287: Sat Feb  2 17:40:10 2019
      read : io=784996KB, bw=133662KB/s, iops=33415, runt=  5873msec
      write: io=263580KB, bw=44880KB/s, iops=11219, runt=  5873msec
      cpu          : usr=6.56%, sys=23.11%, ctx=266267, majf=0, minf=8
      IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=0.1%, 32=0.1%, >=64=100.0%
         submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
         complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.1%, >=64=0.0%
         issued    : total=r=196249/w=65895/d=0, short=r=0/w=0/d=0, drop=r=0/w=0/d=0
         latency   : target=0, window=0, percentile=100.00%, depth=64

    Run status group 0 (all jobs):
       READ: io=784996KB, aggrb=133661KB/s, minb=133661KB/s, maxb=133661KB/s, mint=5873msec, maxt=5873msec
      WRITE: io=263580KB, aggrb=44879KB/s, minb=44879KB/s, maxb=44879KB/s, mint=5873msec, maxt=5873msec
```

请注意此输出中的 `iops` 值。在此示例中，SSD 每秒执行 33,415 次读取操作和 11,219 次写入操作。旋转磁盘每秒可能产生 2,000 和 700 次读写操作。

### 简单的基准测试

NOTE:
这个测试在系统上没有 `fio` 时可以使用。在此测试中可能会收到良好的结果，但由于读取速度和其他各种因素，性能仍然很差。

以下单行命令提供了文件系统读写性能的快速基准。这会将 1,000 个小文件写入执行它的目录，然后读取相同的 1,000 个文件。

1. 更改为相应的 [repository 存储路径] (../repository_storage_paths.md) 的根目录。
1. 为测试创建一个临时目录，以便以后可以将其删除：

   ```shell
   mkdir test; cd test
   ```

1. 运行命令：

   ```shell
   time for i in {0..1000}; do echo 'test' > "test${i}.txt"; done
   ```

1. 要对读取性能进行基准测试，请运行以下命令：

   ```shell
   time for i in {0..1000}; do cat "test${i}.txt" > /dev/null; done
   ```

1. 删除测试文件：

  ```shell
  cd ../; rm -rf test
  ```

`time for ...` 命令的输出类似于以下内容。重要的指标是 `real` 时间。

```shell
$ time for i in {0..1000}; do echo 'test' > "test${i}.txt"; done

real    0m0.116s
user    0m0.025s
sys     0m0.091s

$ time for i in {0..1000}; do cat "test${i}.txt" > /dev/null; done

real    0m3.118s
user    0m1.267s
sys 0m1.663s
```

根据与多个客户的经验，此任务应在 10 秒内完成，表明文件系统性能良好。
