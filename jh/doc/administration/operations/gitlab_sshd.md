---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# `gitlab-sshd` **(FREE SELF)**

> - 引入于 14.5，作为 **Alpha** 功能，适用于私有化部署版。
> - 可用于生产环境于 15.1 版本（云原生安装）/ 15.9 版本（Omnibus 安装）。

`gitlab-sshd` 是用 Go 编写的[一个独立的 SSH 服务器](https://jihulab.com/gitlab-cn/gitlab-shell/-/tree/main/internal/sshd)。它作为 `gitlab-shell` 包的一部分提供。作为 OpenSSH 的替代方案，它占用的内存较少，并且支持在代理后面运行的应用程序的 [IP 地址组访问限制](../../user/group/index.md)。

`gitlab-sshd` 是 OpenSSH 的轻量级替代品，用于提供 SSH 操作。虽然 OpenSSH 使用受限的 shell 方法，但 `gitlab-sshd` 的行为更像是一个现代的多线程服务器应用程序，响应传入的请求。主要区别在于 OpenSSH 使用 SSH 作为传输协议，而 `gitlab-sshd` 使用远程过程调用 (RPC)。<!--See [the blog post](https://about.gitlab.com/blog/2022/08/17/why-we-have-implemented-our-own-sshd-solution-on-gitlab-sass/) for more details.-->

极狐GitLab Shell 的功能不仅限于 Git 操作。

如果您正在考虑从 OpenSSH 切换到 `gitlab-sshd`，请考虑以下问题：

- `gitlab-sshd` 支持 PROXY 协议。它可以在依赖它的代理服务器后面运行，例如 HAProxy。默认情况下不启用 PROXY 协议，但[可以启用](#proxy-protocol-support)。
- `gitlab-sshd` **不**支持 SSH 证书。

## 启用 `gitlab-sshd`

要使用 `gitlab-sshd`：

**Omnibus**

以下说明在与 OpenSSH 不同的端口上启用 `gitlab-sshd`：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_sshd['enable'] = true
   gitlab_sshd['listen_address'] = '[::]:2222' # Adjust the port accordingly
   ```

1. 可选。默认情况下，如果 `/var/opt/gitlab/gitlab-sshd` 中不存在，Omnibus GitLab 会为 `gitlab-sshd` 生成 SSH 主机密钥。如果您希望禁用此自动生成，请添加以下行：

   ```ruby
   gitlab_sshd['generate_host_keys'] = false
   ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

默认情况下，`gitlab-sshd` 作为 `git` 用户运行。因此，`gitlab-sshd` 无法在低于 1024 的特权端口号上运行。这意味着用户必须使用 `gitlab-sshd` 端口访问 Git，或者使用负载均衡器将 SSH 流量定向到 `gitlab-sshd` 端口。

用户可能会看到主机密钥警告，因为新生成的主机密钥与 OpenSSH 主机密钥不同。如果这是一个问题，请考虑禁用主机密钥生成并将现有的 OpenSSH 主机密钥复制到 `/var/opt/gitlab/gitlab-sshd` 中。

**Kubernetes**

以下说明将 OpenSSH 切换为支持 `gitlab-sshd`：

1. 将 `gitlab-shell` chart `sshDaemon` 选项设置为 [`gitlab-sshd`](https://docs.gitlab.cn/charts/charts/gitlab/gitlab-shell/index.html#installation-command-line-options)。例如：

   ```yaml
   gitlab:
     gitlab-shell:
       sshDaemon: gitlab-sshd
   ```

1. 执行 Helm 升级。

默认情况下，`gitlab-sshd` 监听：

- 端口 22 (`global.shell.port`) 上的外部请求。
- 端口 2222 上的内部请求（`gitlab.gitlab-shell.service.internalPort`）。

您可以[在 Helm chart 中配置不同的端口](https://docs.gitlab.cn/charts/charts/gitlab/gitlab-shell/#configuration)。

<a id="proxy-protocol-support"></a>

## PROXY 协议支持

当在 `gitlab-sshd` 前面使用负载均衡器时，极狐GitLab 报告代理的 IP 地址而不是客户端的实际 IP 地址。`gitlab-sshd`支持 [PROXY 协议](https://www.haproxy.org/download/1.8/doc/proxy-protocol.txt)获取真实 IP 地址。

**Omnibus**

要启用代理协议：

1. 编辑 `/etc/gitlab/gitlab.rb`：

    ```ruby
    gitlab_sshd['proxy_protocol'] = true
    # # Proxy protocol policy ("use", "require", "reject", "ignore"), "use" is the default value
    gitlab_sshd['proxy_policy'] = "use"
    ```

1. 保存文件并重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

**Kubernetes**

1. 设置 [`gitlab.gitlab-shell.config` 选项](https://docs.gitlab.cn/charts/charts/gitlab/gitlab-shell/index.html#installation-command-line-options)。例如：

   ```yaml
   gitlab:
     gitlab-shell:
       config:
         proxyProtocol: true
         proxyPolicy: "use"
   ```

1. 执行 Helm 升级。

