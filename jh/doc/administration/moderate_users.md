---
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# 控制用户访问（适用于管理员） **(FREE SELF)**

本文是管理员文档。有关在群组级别审核用户的信息，请参阅[群组级别文档](../user/group/moderate_users.md)。

极狐GitLab 管理员可以通过批准、禁用、封禁或冻结用户来控制用户访问。

<a id="users-pending-approval"></a>

## 等待批准的用户

处于*待批准*状态的用户需要管理员采取行动。管理员已启用以下任何选项时，用户注册可能处于待批准状态：

<!--
- [Require admin approval for new sign-ups](settings/sign_up_restrictions.md#require-administrator-approval-for-new-sign-ups) setting.
- [User cap](settings/sign_up_restrictions.md#user-cap).
- [Block auto-created users (OmniAuth)](../../integration/omniauth.md#configure-initial-settings)
- [Block auto-created users (LDAP)](../../administration/auth/ldap/index.md#basic-configuration-settings)
-->

- 新注册需要管理员批准
- 用户上限
- 阻止自动创建的用户 (OmniAuth)
- 阻止自动创建的用户 (LDAP)

当用户在启用此设置的情况下注册帐户时：

- 用户处于**待批准**状态。
- 用户会看到一条消息，告诉他们他们的帐户正在等待管理员的批准。

待批准的用户：

- 在功能上与[已禁用的](#block-a-user)用户相同。
- 无法登录。
- 无法访问 Git 仓库或极狐GitLab API。
- 无法收到来自极狐GitLab 的任何通知。
- 不消耗订阅席位<!--[订阅席位](../../subscriptions/self_managed/index.md#billable-users)-->。

管理员必须[批准他们的注册](#approve-or-reject-a-user-sign-up)，系统才能允许他们登录。

### 查看等待批准的用户注册

要查看待批准的用户注册：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 选择 **等待批准** 选项卡。

<a id="approve-or-reject-a-user-sign-up"></a>

### 批准或拒绝用户注册

可以从管理中心批准或拒绝等待批准的用户注册。

要批准或拒绝用户注册：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 选择 **等待批准** 选项卡。
1. 可选。选择一个用户。
1. 选择 **{settings}** **用户管理** 下拉菜单。
1. 选择 **批准** 或 **拒绝**。

批准用户后：

- 激活了他们的帐户。
- 将用户的状态更改为激活。
- 消耗订阅席位<!--[席位](../../subscriptions/self_managed/index.md#billable-users)-->。

<a id="block-and-unblock-users"></a>

## 禁用和取消禁用用户

极狐GitLab 管理员可以禁用和取消禁用用户。

<a id="block-a-user"></a>

### 禁用用户

为了完全阻止用户访问极狐GitLab 实例，管理员可以选择禁用用户。

可以通过滥用报告<!--[通过滥用报告](review_abuse_reports.md#blocking-users)-->、通过在 LDAP 中删除用户、或直接从管理中心中删除用户来禁用用户。操作方法：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 可选。选择一个用户。
1. 选择 **{settings}** **用户管理** 下拉菜单。
1. 选择 **禁用**。

被禁用的用户：

- 无法登录。
- 无法访问 Git 仓库或 API。
- 无法收到来自极狐GitLab 的任何通知。
- 无法使用斜杠命令<!--[斜杠命令](../../integration/slash_commands.md)-->。
- 不消耗订阅席位<!--[订阅席位](../../subscriptions/self_managed/index.md#billable-users)-->。

被禁用用户的个人项目以及组和用户历史记录保持不变。

<!--
NOTE:
Users can also be blocked using the [GitLab API](../../api/users.md#block-user).
-->

### 取消禁用用户

可以在管理中心取消禁用被禁用的用户。操作步骤：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 在 **已禁用** 选项卡上选择。
1. 可选。选择一个用户。
1. 选择 **{settings}** **用户管理** 下拉菜单。
1. 选择 **取消禁用**。

用户的状态设置为激活，他们消耗席位<!--[席位](../../subscriptions/self_managed/index.md#billable-users)-->。

<!--
NOTE:
Users can also be unblocked using the [GitLab API](../../api/users.md#unblock-user).
-->

LDAP 用户可能无法使用取消禁用选项。要启用取消禁用选项，首先需要删除 LDAP 身份：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 选择 **已禁用** 选项卡。
1. 选择用户。
1. 选择 **身份** 选项卡。
1. 找到 LDAP provider 并选择**删除**。

## 激活和冻结用户

极狐GitLab 管理员可以激活和冻结用户。

<a id="deactivate-a-user"></a>

### 冻结用户

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/22257) in GitLab 12.4.
-->

为了暂时禁止最近没有活动的极狐GitLab 用户访问，管理员可以选择冻结该用户。

冻结用户在功能上与[禁用用户](#block-and-unblock-users)相同，但有以下区别：

- 不会禁止用户通过 UI 重新登录。
- 一旦已冻结的用户重新登录到 UI，他们的帐户就会被设置为已激活。

被冻结的用户：

- 无法访问 Git 仓库或 API。
- 无法收到来自极狐GitLab 的任何通知。
- 无法使用斜杠命令<!--[斜杠命令](../../integration/slash_commands.md)-->。
- 不消耗订阅席位<!--[订阅席位](../../subscriptions/self_managed/index.md#billable-users)-->。

已冻结用户的个人项目以及群组和用户历史记录保持不变。

NOTE:
如果启用用户冻结电子邮件<!--[用户冻结电子邮件](settings/email.md#user-deactivation-emails)-->，用户会收到有关帐户冻结的通知。

可以在管理中心冻结用户。操作步骤：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 可选。选择一个用户。
1. 选择 **{settings}** **用户管理** 下拉菜单。
1. 选择 **冻结**。

为了使管理员可以看到冻结选项，用户：

- 必须当前处于活动状态。
- 在过去 90 天内不得登录或进行任何活动。

<!--
NOTE:
Users can also be deactivated using the [GitLab API](../../api/users.md#deactivate-user).
-->

<a id="automatically-deactivate-dormant-users"></a>

### 自动冻结休眠用户

> - 引入于 14.0 版本。
> - 可自定义的时长引入于 15.4 版本。
> - 非活动期的下限设置为 90 天，引入于 15.5 版本。

管理员可以启用自动冻结在过去 90 天内未登录或没有活动的如下用户：

- 超过一星期前创建，尚未登录。
- 在指定的时间段内没有活动（默认为 90 天）。

操作步骤：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **设置 > 通用**。
1. 展开 **账户和限制** 部分。
1. 在 **休眠用户** 下，选中 **闲置 90 天后冻结休眠用户**。
1. 在 **不活动时长（天数）** 下，输入停用前的不活动时长。
1. 选择 **保存更改**。

启用此功能后，极狐GitLab 每天运行一次作业，冻结休眠用户。

每天最多可以冻结 100,000 个用户。

NOTE:
极狐GitLab 生成的机器人不属于自动停用休眠用户的范围。

### 自动删除未确认的用户 **(PREMIUM SELF)**

> - 引入于 16.1 版本，[功能标志](../administration/feature_flags.md)为 `delete_unconfirmed_users_setting`。默认禁用。
> - 默认启用于 16.2 版本。

先决条件：

- 您必须是管理员。

您可以启用自动删除同时满足以下条件的用户：

- 从未确认过他们的电子邮件地址。
- 过去注册极狐GitLab 的时间超过指定天数。

您可以使用[设置 API](../api/settings.md)，或在 Rails 控制台中配置以下设置：

```ruby
 Gitlab::CurrentSettings.update(delete_unconfirmed_users: true)
 Gitlab::CurrentSettings.update(unconfirmed_users_delete_after_days: 365)
```

当启用 `delete_unconfirmed_users` 设置时，极狐GitLab 每小时运行一次作业来删除未确认的用户。
该作业仅删除过去注册超过 `unconfirmed_users_delete_after_days` 天的用户。

此作业仅在 `email_confirmation_setting` 设置为 `soft` 或 `hard` 时运行。

每天最多可以删除 240,000 个用户。

### 激活用户

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/22257) in GitLab 12.4.
-->

可以在管理中心激活已冻结的用户。

操作步骤：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 选择 **已冻结** 选项卡。
1. 可选。选择一个用户。
1. 选择 **{settings}** **用户管理** 下拉菜单。
1. 选择 **激活**。

用户的状态设置为激活，他们使用订阅席位<!--[订阅席位](../../subscriptions/self_managed/index.md#billable-users)-->。

<!--
NOTE:
A deactivated user can also activate their account themselves by logging back in via the UI.
Users can also be activated using the [GitLab API](../../api/users.md#activate-user).
-->

## 封禁和解禁用户

> - 引入于 14.2 版本。功能标志为 `ban_user_feature_flag`。默认禁用。
> - 默认启用于 14.8 版本。删除功能标志  `ban_user_feature_flag`。
> - 隐藏已封禁用户的合并请求功能引入于 15.8 版本，[功能标志](../administration/feature_flags.md)为 `hide_merge_requests_from_banned_users`。默认禁用。
> - 隐藏已封禁用户的评论功能引入于 15.11 版本，[功能标志](../administration/feature_flags.md)为 `hidden_notes`。默认禁用。
> - 隐藏已封禁用户的项目功能引入于 16.2 版本，[功能标志](../administration/feature_flags.md)为 `hide_projects_of_banned_users`。默认禁用。

极狐GitLab 管理员可以封禁和解禁用户。被封禁的用户处于禁用状态，他们的项目、议题、合并请求和评论被隐藏。

### 封禁用户

要禁用用户并隐藏他们的贡献，管理员可以封禁该用户。

可以在管理中心禁止用户。操作步骤：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 可选。选择一个用户。
1. 选择 **{settings}** **用户管理** 下拉菜单。
1. 选择 **封禁用户**。

被封禁的用户不消耗订阅席位<!--[订阅席位](../../subscriptions/self_managed/index.md#billable-users)-->。

### 解禁用户

可以在管理中心解禁被封禁的用户。操作步骤：

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 选择 **已封禁** 选项卡。
1. 可选。选择一个用户。
1. 选择 **{settings}** **用户管理** 下拉菜单。
1. 选择 **解禁用户**。

用户的状态设置为激活，他们消耗订阅席位<!--[seat](../../subscriptions/self_managed/index.md#billable-users)-->。

### 删除用户

使用管理中心删除用户。

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 选择 **已封禁** 选项卡。
1. 可选。选择一个用户。
1. 选择 **{settings}** **用户管理** 下拉列表。
1. 选择 **删除用户**。
1. 输入用户名。
1. 选择 **删除用户**。

NOTE:
如果群组有继承的或直接的所有者，您只能删除用户。如果用户是唯一的群组所有者，则您不能删除他们。

您还可以删除用户及其贡献，例如合并请求、议题，和他们是唯一群组所有者的群组。

1. 在左侧边栏上，展开最顶部的向下箭头 (**{chevron-down}**）。
1. 选择 **管理中心**。
1. 选择 **概览 > 用户**。
1. 选择 **已封禁** 选项卡。
1. 可选。选择一个用户。
1. 选择 **{settings}** **用户管理** 下拉列表。
1. 选择 **删除用户和贡献**。
1. 输入用户名。
1. 选择 **删除用户和贡献**。

NOTE:
在 15.1 版本之前，对于删除的用户是直接成员中唯一所有者的群组，群组将被删除。

## 故障排除

修改用户时，您可能需要根据特定条件对他们执行批量操作。以下 Rails 控制台脚本显示了一些示例。您可以[启动 Rails 控制台会话](../administration/operations/rails_console.md#starting-a-rails-console-session)，并使用类似于以下内容的脚本：

### 停用最近没有活动的用户

管理员可以停用最近没有活动的用户。

WARNING:
无论运行是否正确，更改数据的命令可能会造成损坏。始终首先在测试环境中运行命令，并准备好备份实例进行恢复。

```ruby
days_inactive = 90
inactive_users = User.active.where("last_activity_on <= ?", days_inactive.days.ago)

inactive_users.each do |user|
    puts "user '#{user.username}': #{user.last_activity_on}"
    user.deactivate!
end
```

### 禁用最近没有活动的用户

管理员可以禁用最近没有活动的用户。

WARNING:
无论运行是否正确，更改数据的命令可能会造成损坏。始终首先在测试环境中运行命令，并准备好备份实例进行恢复。

```ruby
days_inactive = 90
inactive_users = User.active.where("last_activity_on <= ?", days_inactive.days.ago)

inactive_users.each do |user|
    puts "user '#{user.username}': #{user.last_activity_on}"
    user.block!
end
```

### 禁用或删除没有项目或群组的用户

管理员可以禁用或删除没有项目或群组的用户。

WARNING:
无论运行是否正确，更改数据的命令可能会造成损坏。始终首先在测试环境中运行命令，并准备好备份实例进行恢复。

```ruby
users = User.where('id NOT IN (select distinct(user_id) from project_authorizations)')

# How many users are removed?
users.count

# If that count looks sane:

# You can either block the users:
users.each { |user|  user.blocked? ? nil  : user.block! }

# Or you can delete them:
  # need 'current user' (your user) for auditing purposes
current_user = User.find_by(username: '<your username>')

users.each do |user|
  DeleteUserWorker.perform_async(current_user.id, user.id)
end
```
