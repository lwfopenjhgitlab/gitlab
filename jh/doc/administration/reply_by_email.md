---
stage: Plan
group: Certify
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 通过电子邮件回复 **(FREE SELF)**

极狐GitLab 可以设置为允许用户通过回复通知电子邮件来评论议题和合并请求。

## 要求

确保设置了[接收电子邮件](incoming_email.md)。

## 工作原理

通过电子邮件回复分三个步骤：

1. 极狐GitLab 发送通知邮件。
1. 您回复通知邮件。
1. 极狐GitLab 收到您对通知邮件的回复。

### 极狐GitLab 发送通知邮件

当极狐GitLab 发送通知并启用通过电子邮件回复时，`Reply-To` header 设置为极狐GitLab 配置中定义的地址，`%{key}` 占位符（如果存在）替换为特定的 "reply key"。此外，这个 "reply key" 也被添加到了 `References` header 中。

### 您回复通知电子邮件

当您回复通知电子邮件时，您的电子邮件客户端：

- 将电子邮件发送到它从通知电子邮件中获得的 `Reply-To` 地址
- 将 `In-Reply-To` header 设置为通知电子邮件中 `Message-ID` header 的值
- 将 `References` header 设置为 `Message-ID` 的值加上通知电子邮件的 `References` header 的值。

### 极狐GitLab 收到您对通知电子邮件的回复

当极狐GitLab 收到您的回复时，它会按以下顺序在以下 header 中查找 "reply key"：

1. `To` header
1. `References` header
1. `Delivered-To` header
1. `Envelope-To` header
1. `X-Envelope-To` header
1. `Received` header

如果找到 "reply key"，它会将您的回复作为对通知所涉及的实体的评论（议题、合并请求、提交等）。

有关 `Message-ID`、`In-Reply-To` 和 `References headers` 的更多详细信息，请参阅 [RFC 5322](https://www.rfc-editor.org/rfc/rfc5322#section-3.6.4)。
