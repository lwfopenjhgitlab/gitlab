---
stage: Data Science
group: Anti-Abuse
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# reCAPTCHA **(FREE SELF)**

极狐GitLab 利用 [reCAPTCHA](https://www.google.com/recaptcha/about/) 来防止垃圾邮件和滥用。极狐GitLab 在注册页面上显示 CAPTCHA 表单，确认正在尝试创建帐户的是真实用户，而不是机器人。

<a id="configuration"></a>

## 配置

极狐GitLab 目前支持对接以下人机校验服务：

* [Google reCAPTCHA v2](#google-recaptcha-v2)
* [T-sec 天御](#t-sec)
* [极验](#geetest)

<a id="google-recaptcha-v2"></a>

### Google reCAPTCHA v2

要使用 Google reCAPTCHA v2，首先要创建一个站点密钥和私钥。

1. 前往 [Google reCAPTCHA 页面](https://www.google.com/recaptcha/admin)。
1. 要获取 reCAPTCHA v2 密钥，请填写表格并选择 **提交**。
1. 以管理员身份登录您的极狐GitLab 服务器。
1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 报告** (`admin/application_settings/reporting`)。
1. 展开 **垃圾信息及防机器人保护**。
1. 在 reCAPTCHA 字段中，输入您在前面步骤中获得的密钥。
1. 选择 **启用 reCAPTCHA** 复选框。
1. 要为通过密码登录的操作启用 reCAPTCHA，请选中 **启用 reCAPTCHA 进行登录** 复选框。
1. 选择 **保存更改**。
1. 如果想绕过垃圾信息检查，让响应中直接返回 `recaptcha_html`，你可以：
   1. 打开 `app/services/spam/spam_verdict_service.rb`。
   1. 将 `#execute` 方法的第一行更改为 `return CONDITIONAL_ALLOW`。

<a id="t-sec"></a>

### T-sec 天御

1. 前往 [T-sec 天御页面](https://cloud.tencent.com/product/captcha)，参考相关文档，获取 CaptchaAppId 和 AppSecretKey。
1. 以管理员身份登录您的极狐GitLab 服务器。
1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 报告** (`admin/application_settings/reporting`)。
1. 展开 **垃圾信息及防机器人保护**。
1. 选择 **启用 reCAPTCHA** 复选框，不配置密钥。
1. 要为通过密码登录的操作启用 reCAPTCHA，请选中 **启用 reCAPTCHA 进行登录** 复选框。
1. 选择 **保存更改**。
1. [打开 Rails 控制台](../administration/operations/rails_console.md#starting-a-rails-console-session)，运行以下命令启用功能标志 `tencent_captcha`：

   ```ruby
   Feature.enable(:tencent_captcha)
   ```

1. 将前面步骤中获取的 CaptchaAppId 和 AppSecretKey 配置到环境变量：

   ```yaml
   TC_CAPTCHA_ID='******'
   TC_CAPTCHA_KEY='******'
   ```

<a id="geetest"></a>

### 极验

> 引入于 15.9 版本。

1. 前往[极验页面](https://www.geetest.com/adaptive-captcha)，参考相关文档，获取 ID 和 Key。
1. 以管理员身份登录您的极狐GitLab 服务器。
1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 报告** (`admin/application_settings/reporting`)。
1. 展开 **垃圾信息及防机器人保护**。
1. 选择 **启用 reCAPTCHA** 复选框，不配置密钥。
1. 要为通过密码登录的操作启用 reCAPTCHA，请选中 **启用 reCAPTCHA 进行登录** 复选框。
1. 选择 **保存更改**。
1. [打开 Rails 控制台](../administration/operations/rails_console.md#starting-a-rails-console-session)，运行以下命令启用功能标志 `geetest_captcha`：

   ```ruby
   Feature.enable(:geetest_captcha)
   ```

1. 将前面步骤中获取的 ID 和 Key 配置到环境变量：

   ```yaml
   GEETEST_CAPTCHA_ID='******'
   GEETEST_CAPTCHA_KEY='******'
   ```

## 使用 HTTP header 为用户登录启用 reCAPTCHA

您可以[在用户界面](#configuration)，或通过设置 `X-GitLab-Show-Login-Captcha` HTTP header 为通过密码的用户登录启用 reCAPTCHA。
例如，在 NGINX 中可以通过 `proxy_set_header` 配置变量来完成：

```nginx
proxy_set_header X-GitLab-Show-Login-Captcha 1;
```

在 Omnibus GitLab 中，可以通过 `/etc/gitlab/gitlab.rb` 进行配置：

```ruby
nginx['proxy_set_headers'] = { 'X-GitLab-Show-Login-Captcha' => '1' }
```
