---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 将极狐GitLab 配置为 OAuth 2.0 provider

本文档描述了如何将极狐GitLab 用作 OAuth 2.0 provider。

- OAuth 2 应用程序可以使用极狐GitLab UI（如下所述）创建和管理，也可以使用 Applications API 进行管理。
- 创建应用程序后，外部服务可以使用 OAuth 2 API 管理访问令牌。
- 要允许用户使用第三方 OAuth 2 provider 登录极狐GitLab，请参阅 [OmniAuth 文档](omniauth.md)。

## OAuth 简介

[OAuth 2](https://oauth.net/2/) 代表资源所有者，向客户端应用程序提供对服务器资源的“安全委托访问”。OAuth 2 允许授权服务器在资源所有者或最终用户的批准下，向第三方客户端颁发访问令牌。

可以使用 OAuth 2：

- 允许用户使用他们的 JiHuLab.com 帐户登录您的应用程序。

<!--
- 设置 GitLab.com 以对您的 GitLab 实例进行身份验证。 请参阅 [GitLab OmniAuth](gitlab.md)。
-->

“极狐GitLab 导入器”功能还使用 OAuth 2 授予对仓库的访问权限，而无需将用户凭据共享到您的 JiHuLab.com 帐户。

极狐GitLab 支持多种向实例添加新的 OAuth 2 应用程序的方法：

- [用户所有的应用程序](#user-owned-applications)
- [群组所有的应用程序](#group-owned-applications)
- [实例范围的应用程序](#instance-wide-applications)

这些方法之间的唯一区别是权限级别。默认回调 URL 是 `http://your-gitlab.example.com/users/auth/gitlab/callback`。

<a id="user-owned-applications"></a>

## 用户所有的应用程序

要为您的用户添加新的应用程序：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏中，选择 **应用程序**。
1. 输入[已授权的应用程序](#authorized-applications)中定义的**名称**、**重定向 URI** 和 OAuth 2 范围。**重定向 URI** 是用户在极狐GitLab 授权后发送到的 URL。
1. 选择 **保存应用**。极狐GitLab 提供：

   - **应用程序 ID** 字段中的 OAuth 2 客户端 ID。
   - OAuth 2 客户端密码，可访问于：
     - 14.1 及更早版本的 **Secret** 字段中。
     - 14.2 及更高版本中使用 **Secret** 字段上的 **复制** 按钮。

<a id="group-owned-applications"></a>

## 群组所有的应用程序

> 引入于 13.11 版本。

要为群组添加新的应用程序：

1. 导航到所需的群组。
1. 在左侧边栏中，选择 **设置 > 应用程序**。
1. 输入[已授权的应用程序](#authorized-applications)中定义的**名称**、**重定向 URI** 和 OAuth 2 范围。**重定向 URI** 是用户在极狐GitLab 授权后发送到的 URL。
1. 选择 **保存应用**。极狐GitLab 提供：

   - **应用程序 ID** 字段中的 OAuth 2 客户端 ID。
   - OAuth 2 客户端密码，可访问于：
     - 14.1 及更早版本的 **Secret** 字段中。
     - 14.2 及更高版本中使用 **Secret** 字段上的 **复制** 按钮。

<a id="instance-wide-applications"></a>

## 实例范围的应用程序

为您的极狐GitLab 实例创建应用程序：

1. 在顶部栏上，选择 **菜单 > 管理员**。
1. 在左侧边栏中，选择 **应用程序**。
1. 选择 **新建应用程序**。

在**管理中心**创建应用程序时，您可以将其标记为*可信的*。
此应用程序会自动跳过用户授权步骤。

## 访问令牌过期

> - 引入于 14.3 版本，可以选择退出。
> - 选择退出过期的访问令牌功能删除于 15.0 版本。

WARNING:
14.3 版本中弃用了选择退出过期访问令牌的功能，并在 15.0 中删除了该功能。必须更新所有现有集成，支持访问令牌刷新。

访问令牌在两小时后过期。使用访问令牌的集成必须至少每两个小时生成一次新令牌。

删除应用程序时，与该应用程序关联的所有授权和令牌也将被删除。

<a id="view-all-authorized-applications"></a>

## 已授权的应用程序

您使用极狐GitLab 凭据授权的每个应用程序都显示在 **设置 > 应用程序** 下的**已授权应用**部分。

极狐GitLab OAuth 2 应用程序支持范围，允许给定任何应用程序可以执行的各种操作。下表描述了可用范围。

| 范围              | 描述 |
| ------------------ | ----------- |
| `api`              | 授予对 API 的完整读/写访问权限，包括所有群组和项目、容器镜像库和软件包库。 |
| `read_user`        | 通过 /user API 端点授予对经过身份验证的用户配置文件的只读访问权限，其中包括用户名、公共电子邮件和全名。还授予对 /users 下只读 API 端点的访问权限。 |
| `read_api`         | 授予对 API 的读取权限，包括所有群组和项目、容器镜像库和软件包库。 |
| `read_repository`  | 使用 Git-over-HTTP 或仓库文件 API 授予对私有项目仓库的只读访问权限。|
| `write_repository` | 使用 Git-over-HTTP（不使用 API）授予对私有项目仓库的读写访问权限。 |
| `read_registry`    | 授予对私有项目上的容器镜像库镜像的只读访问权限。 |
| `write_registry`   | 授予对私有项目上的容器镜像库镜像的写访问权限。 |
| `sudo`             | 当以管理员用户身份进行身份验证时，授予以系统中任何用户身份执行 API 操作的权限。 |
| `openid`           | 授予使用 OpenID Connect 通过极狐GitLab 进行身份验证的权限。还授予对用户个人资料和群组成员资格的只读访问权限。 |
| `profile`          | 授予使用 OpenID Connect 对用户配置文件数据的只读访问权限。 |
| `email`            | 授予使用 OpenID Connect 对用户主电子邮件地址的只读访问权限。 |

您可以随时通过单击 **撤销** 来撤销任何访问权限。
