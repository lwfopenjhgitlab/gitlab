---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 外部议题跟踪器 **(FREE)**

极狐GitLab 自带一个[议题跟踪器](../user/project/issues/index.md)，但您可以为每个极狐GitLab 项目配置一个外部议题跟踪器。

配置外部跟踪器后，您可以使用 `CODE-123` 格式在极狐GitLab 合并请求、提交和评论中引用外部议题，其中：

- `CODE` 是跟踪器的唯一代号。
- `123` 是跟踪器中的议题编号。

引用会自动转换为指向议题的链接。

您可以同时启用或禁用极狐GitLab 议题跟踪器。启用后，极狐GitLab 菜单中的 **议题** 链接始终打开内部议题跟踪器。禁用时，该链接在菜单中不可见。

## 配置外部议题跟踪器

要启用外部议题跟踪器，您必须配置适当的[集成](../user/project/integrations/index.md)。

以下外部议题跟踪器集成可用：

<!--
- [Bugzilla](../user/project/integrations/bugzilla.md)
- [Custom Issue Tracker](../user/project/integrations/custom_issue_tracker.md)
- [Engineering Workflow Management](../user/project/integrations/ewm.md)
- [Jira](../integration/jira/index.md)
- [Redmine](../user/project/integrations/redmine.md)
- [YouTrack](../user/project/integrations/youtrack.md)
- [ZenTao](../user/project/integrations/zentao.md)
-->

- [Bugzilla](../user/project/integrations/bugzilla.md)
- [自定义议题跟踪器](../user/project/integrations/custom_issue_tracker.md)
- Engineering Workflow Management<!--[Engineering Workflow Management](../user/project/integrations/ewm.md)-->
- Jira<!--[Jira](../integration/jira/index.md)-->
- Redmine<!--[Redmine](../user/project/integrations/redmine.md)-->
- YouTrack<!--[YouTrack](../user/project/integrations/youtrack.md)-->
- [禅道](../user/project/integrations/zentao.md)
