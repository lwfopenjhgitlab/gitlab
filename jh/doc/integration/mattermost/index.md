---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# GitLab Mattermost
<!--
NOTE:
This document applies to GitLab 11.0 and later.
-->

您可以在您的 GitLab 服务器上运行 GitLab Mattermost<!--[GitLab Mattermost](https://gitlab.com/gitlab-org/gitlab-mattermost)--> 服务。Mattermost 不是 GitLab 的单一应用程序的一部分，与 GitLab 之间有很好的集成，我们的 Omnibus 安装程序允许您轻松安装它。但它是来自一家独立公司的独立应用程序。

## 前提条件

GitLab Mattermost 的每个版本都是在 Linux 的 AMD 64 芯片组上编译和手动测试的。不支持 ARM 芯片组和操作系统，如 Raspberry Pi。

## 快速入门

GitLab Mattermost 希望在自己的虚拟主机上运行。在您的 DNS 设置中，您需要两个指向同一台机器的条目，例如，`gitlab.example.com` 和 `mattermost.example.com`。

GitLab Mattermost 默认是禁用的。要启用它：

1. 编辑 `/etc/gitlab/gitlab.rb` 并添加 Mattermost 外部 URL：

   ```ruby
   mattermost_external_url 'https://mattermost.example.com'
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

1. 确认 GitLab Mattermost 可通过 `https://mattermost.example.com` 访问并被授权连接到 GitLab。使用 GitLab 授权 Mattermost 允许用户将 GitLab 用作 SSO 提供商。

如果应用程序在同一台服务器上运行，Omnibus GitLab 包会尝试使用 GitLab 自动授权 GitLab Mattermost。

自动授权需要访问 GitLab 数据库。如果 GitLab 数据库不可用，您将需要使用[授权 GitLab Mattermost 部分](#授权-gitlab-mattermost)中描述的过程，手动授权 GitLab Mattermost 以访问 GitLab。

## 配置 Mattermost

可以使用 Mattermost 系统控制台配置 Mattermost。[在 Mattermost 文档中](https://docs.mattermost.com/administration/config-settings.html) 提供了大量的 Mattermost 设置列表及其可以设置的位置。

虽然建议使用系统控制台，但您也可以使用以下选项之一配置 Mattermost：

1. 直接通过 `/var/opt/gitlab/mattermost/config.json` 编辑 Mattermost 配置。
1. 通过更改 `gitlab.rb` 中的 `mattermost['env']` 设置来指定用于运行 Mattermost 的环境变量。以这种方式配置的任何设置都将从系统控制台禁用，并且必须重新启动 Mattermost 才能更改。

## 使用 HTTPS 运行 GitLab Mattermost

将 SSL 证书和 SSL 证书密钥放在 `/etc/gitlab/ssl` 中。如果目录不存在，请创建它：

```shell
sudo mkdir -p /etc/gitlab/ssl
sudo chmod 755 /etc/gitlab/ssl
sudo cp mattermost.gitlab.example.key mattermost.gitlab.example.crt /etc/gitlab/ssl/
```

在 `/etc/gitlab/gitlab.rb` 中指定如下配置：

```ruby
mattermost_external_url 'https://mattermost.gitlab.example'
mattermost_nginx['redirect_http_to_https'] = true
```

如果还没有命名您的证书和密钥 `mattermost.gitlab.example.crt` 和 `mattermost.gitlab.example.key`，那么还需要添加如下所示的完整路径。

```ruby
mattermost_nginx['ssl_certificate'] = "/etc/gitlab/ssl/mattermost-nginx.crt"
mattermost_nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/mattermost-nginx.key"
```

其中 `mattermost-nginx.crt` 和 `mattermost-nginx.key` 分别是 SSL 证书和密钥。

设置完成后，运行 `sudo gitlab-ctl reconfigure` 以应用更改。

## 在单独的服务器上运行 GitLab Mattermost

如果您想在两个独立的服务器上运行 GitLab 和 GitLab Mattermost，GitLab 服务仍将设置在您的 GitLab Mattermost 服务器上，但它们不会接受用户请求或消耗系统资源。您可以在 GitLab Mattermost 服务器上使用以下设置和配置详细信息，来有效禁用捆绑到 Omnibus 包中的 GitLab 服务。

```ruby
mattermost_external_url 'http://mattermost.example.com'

# Shut down GitLab services on the Mattermost server
gitlab_rails['enable'] = false
redis['enable'] = false
postgres_exporter['enable'] = false
grafana['enable'] = false
```

然后按照 [授权 GitLab Mattermost 部分](#授权-gitlab-mattermost) 中的相应步骤操作。最后，要启用与 GitLab 的集成，请在 GitLab 服务器上添加以下内容：

```ruby
gitlab_rails['mattermost_host'] = "https://mattermost.example.com"
```

默认情况下，GitLab Mattermost 要求所有用户注册极狐GitLab 并禁用通过电子邮件注册选项。请参阅 [Mattermost 上的 GitLab SSO 文档](https://docs.mattermost.com/deployment/sso-gitlab.html)。

## 使用 GitLab 手动（重新）授权 GitLab Mattermost

### 重新授权 GitLab Mattermost

要重新授权 GitLab Mattermost，您首先需要撤销现有授权。可以在极狐GitLab 的 **设置 > 应用** 区域中完成。然后按照以下步骤完成授权。

### 授权 GitLab Mattermost

导航到 GitLab 中的 **设置 > 应用** 区域。创建一个新应用程序，并为 **Redirect URI** 使用以下内容（如果使用 HTTPS，请将 `http` 替换为 `https`）：

```plaintext
http://mattermost.example.com/signup/gitlab/complete
http://mattermost.example.com/login/gitlab/complete
```

请注意，您无需在 **范围** 下选择任何选项。 选择 **保存应用程序**。

创建应用程序后，您将获得一个“应用程序 ID”和“秘密”。 需要的另一条信息是 GitLab 实例的 URL。 返回运行 GitLab Mattermost 的服务器并使用您在上面收到的值编辑 `/etc/gitlab/gitlab.rb` 配置文件，如下所示：

```ruby
mattermost['gitlab_enable'] = true
mattermost['gitlab_id'] = "12345656"
mattermost['gitlab_secret'] = "123456789"
mattermost['gitlab_scope'] = ""
mattermost['gitlab_auth_endpoint'] = "http://gitlab.example.com/oauth/authorize"
mattermost['gitlab_token_endpoint'] = "http://gitlab.example.com/oauth/token"
mattermost['gitlab_user_api_endpoint'] = "http://gitlab.example.com/api/v4/user"
```

保存更改，然后运行 `sudo gitlab-ctl reconfigure`。 如果没有错误，您的 GitLab 和 GitLab Mattermost 应该正确配置。

### 指定用户和群组的数字标识符

Omnibus GitLab 创建一个用户和群组 `mattermost`。 您可以在 `/etc/gitlab/gitlab.rb` 中为这些用户指定数字标识符，如下所示：

```ruby
mattermost['uid'] = 1234
mattermost['gid'] = 1234
```

运行 `sudo gitlab-ctl reconfigure` 以应用更改。

### 设置自定义环境变量

如有必要，您可以通过 `/etc/gitlab/gitlab.rb` 设置由 Mattermost 使用的自定义环境变量。如果 Mattermost 服务器在公司 Internet 代理后面运行，这会很有用。在 `/etc/gitlab/gitlab.rb` 中提供一个带有哈希值的 `mattermost['env']`。例如：

```ruby
mattermost['env'] = {"HTTP_PROXY" => "my_proxy", "HTTPS_PROXY" => "my_proxy", "NO_PROXY" => "my_no_proxy"}
```

运行 `sudo gitlab-ctl reconfigure` 以应用更改。

### 连接到捆绑的 PostgreSQL 数据库

如果您需要连接到捆绑的 PostgreSQL 数据库并使用默认的 Omnibus GitLab 数据库配置，您可以作为 PostgreSQL 超级用户连接：

```shell
sudo gitlab-psql -d mattermost_production
```

### 备份 GitLab Mattermost

<!--GitLab Mattermost 不包含在常规的 [Omnibus GitLab 备份](https://docs.gitlab.com/jh/raketasks/backup_restore.html#备份-gitlab) Rake 任务。-->

通用的 Mattermost [备份和灾难恢复](https://docs.mattermost.com/deploy/backup-disaster-recovery.html) 文档可用作需要备份的指南。

#### 备份捆绑的 PostgreSQL 数据库

如果您需要备份捆绑的 PostgreSQL 数据库并使用默认的 Omnibus GitLab 数据库配置，您可以使用以下命令进行备份：

```shell
sudo -i -u gitlab-psql -- /opt/gitlab/embedded/bin/pg_dump -h /var/opt/gitlab/postgresql mattermost_production | gzip > mattermost_dbdump_$(date --rfc-3339=date).sql.gz
```

#### 备份 `data` 目录和 `config.json`

Mattermost 有一个 `data` 目录和 `config.json` 文件，它们也需要备份：

```shell
sudo tar -zcvf mattermost_data_$(date --rfc-3339=date).gz -C /var/opt/gitlab/mattermost data config.json
```

### 恢复 GitLab Mattermost

如果您之前[创建了 GitLab Mattermost 的备份](#备份-gitlab-mattermost)，您可以运行以下命令来恢复它：

```shell
# Stop Mattermost so we don't have any open database connections
sudo gitlab-ctl stop mattermost

# Drop the Mattermost database
sudo -u gitlab-psql /opt/gitlab/embedded/bin/dropdb -U gitlab-psql -h /var/opt/gitlab/postgresql -p 5432 mattermost_production

# Create the Mattermost database
sudo -u gitlab-psql /opt/gitlab/embedded/bin/createdb -U gitlab-psql -h /var/opt/gitlab/postgresql -p 5432 mattermost_production

# Perform the database restore
# Replace /tmp/mattermost_dbdump_2021-08-05.sql.gz with your backup
sudo -u mattermost sh -c "zcat /tmp/mattermost_dbdump_2021-08-05.sql.gz | /opt/gitlab/embedded/bin/psql -U gitlab_mattermost -h /var/opt/gitlab/postgresql -p 5432 mattermost_production"

# Restore the data directory and config.json
# Replace /tmp/mattermost_data_2021-08-09.gz with your backup
sudo tar -xzvf /tmp/mattermost_data_2021-08-09.gz -C /var/opt/gitlab/mattermost

# Fix permissions if required
sudo chown -R mattermost:mattermost /var/opt/gitlab/mattermost/data
sudo chown mattermost:mattermost /var/opt/gitlab/mattermost/config.json

# Start Mattermost
sudo gitlab-ctl start mattermost 
```

### Mattermost 命令行工具 (CLI)

NOTE:
此 CLI 将在未来版本中替换为新的 [`mmctl` 命令行工具](https://docs.mattermost.com/manage/mmctl-command-line-tool.html)。

要使用 [Mattermost 命令行工具 (CLI)](https://docs.mattermost.com/administration/command-line-tools.html)，请确保当您运行 CLI 命令时您位于 `/opt/gitlab/embedded/service/mattermost` 目录，且指定了配置文件的位置。可执行文件是 `/opt/gitlab/embedded/bin/mattermost`。

```shell
cd /opt/gitlab/embedded/service/mattermost

sudo /opt/gitlab/embedded/bin/chpst -e /opt/gitlab/etc/mattermost/env -P -U mattermost:mattermost -u mattermost:mattermost /opt/gitlab/embedded/bin/mattermost --config=/var/opt/gitlab/mattermost/config.json version
```

该命令需要大量输入并且很难记住，所以让我们制作一个 bash /zsh 别名使其更容易记住。将以下内容添加到你的 `~/.bashrc` 或 `~/.zshrc` 文件中：

```shell
alias mattermost-cli="cd /opt/gitlab/embedded/service/mattermost && sudo /opt/gitlab/embedded/bin/chpst -e /opt/gitlab/etc/mattermost/env -P -U mattermost:mattermost -u mattermost:mattermost /opt/gitlab/embedded/bin/mattermost --config=/var/opt/gitlab/mattermost/config.json $1"
```

然后使用 `source ~/.zshrc` 或 `source ~/.bashrc` 获取 `~/.zshrc` 或 `~/.bashrc`。

如果成功，您现在可以使用新的 shell 别名 `mattermost-cli` 运行任何 Mattermost CLI 命令：

```shell
$ mattermost-cli version

[sudo] password for username:
{"level":"info","ts":1569614421.9058893,"caller":"utils/i18n.go:83","msg":"Loaded system translations for 'en' from '/opt/gitlab/embedded/service/mattermost/i18n/en.json'"}
{"level":"info","ts":1569614421.9062793,"caller":"app/server_app_adapters.go:58","msg":"Server is initializing..."}
{"level":"info","ts":1569614421.90976,"caller":"sqlstore/supplier.go:223","msg":"Pinging SQL master database"}
{"level":"info","ts":1569614422.0515099,"caller":"mlog/log.go:165","msg":"Starting up plugins"}
{"level":"info","ts":1569614422.0515954,"caller":"app/plugin.go:193","msg":"Syncing plugins from the file store"}
{"level":"info","ts":1569614422.086005,"caller":"app/plugin.go:228","msg":"Found no files in plugins file store"}
{"level":"info","ts":1569614423.9337213,"caller":"sqlstore/post_store.go:1301","msg":"Post.Message supports at most 16383 characters (65535 bytes)"}
{"level":"error","ts":1569614425.6317747,"caller":"go-plugin/stream.go:15","msg":" call to OnConfigurationChange failed, error: Must have a GitLab oauth client id","plugin_id":"com.github.manland.mattermost-plugin-gitlab","source":"plugin_stderr"}
{"level":"info","ts":1569614425.6875598,"caller":"mlog/sugar.go:19","msg":"Ensuring Surveybot exists","plugin_id":"com.mattermost.nps"}
{"level":"info","ts":1569614425.6953356,"caller":"app/server.go:216","msg":"Current version is 5.14.0 (5.14.2/Fri Aug 30 20:20:48 UTC 2019/817ee89711bf26d33f840ce7f59fba14da1ed168/none)"}
{"level":"info","ts":1569614425.6953766,"caller":"app/server.go:217","msg":"Enterprise Enabled: false"}
{"level":"info","ts":1569614425.6954057,"caller":"app/server.go:219","msg":"Current working directory is /opt/gitlab/embedded/service/mattermost/i18n"}
{"level":"info","ts":1569614425.6954265,"caller":"app/server.go:220","msg":"Loaded config","source":"file:///var/opt/gitlab/mattermost/config.json"}
Version: 5.14.0
Build Number: 5.14.2
Build Date: Fri Aug 30 20:20:48 UTC 2019
Build Hash: 817ee89711bf26d33f840ce7f59fba14da1ed168
Build Enterprise Ready: false
DB Version: 5.14.0
```

有关更多详细信息，请参阅 [Mattermost 命令行工具 (CLI)](https://docs.mattermost.com/administration/command-line-tools.html) 和下面的 [Mattermost CLI 故障排查](#mattermost-cli-故障排查)。

## 配置 GitLab 和 Mattermost 集成

Mattermost GitLab 插件随 Omnibus GitLab 一起提供：查看 [Mattermost Plugin for GitLab 文档](https://github.com/mattermost/mattermost-plugin-gitlab)。

您可以使用该插件订阅 Mattermost 以接收有关议题、合并请求和拉取请求的通知，以及有关合并请求审查、未读消息和任务分配的个人通知。如果您想使用斜杠命令来执行创建和查看议题等操作，或触发部署，请使用 GitLab [Mattermost 斜杠命令](https://docs.gitlab.cn/jh/user/project/integrations/mattermost_slash_commands.html)。

plugin 和 slash 命令可以一起使用或单独使用。

## 电子邮件通知

### 为 GitLab Mattermost 设置 SMTP

这些设置由系统管理员通过 Mattermost 系统控制台进行配置。在 **系统控制台** 的 **环境 > SMTP** 选项卡上，您可以输入 SMTP 提供商提供的 SMTP 凭据，或者输入 `127.0.0.1`和端口 `25` 以使用 `sendmail`。[Mattermost 文档](https://docs.mattermost.com/install/smtp-email-setup.html) 中提供了有关所需特定设置的更多信息。

这些设置也可以在 `/var/opt/gitlab/mattermost/config.json` 中配置。

### 电子邮件批处理

启用此功能允许用户控制他们接收电子邮件通知的频率。

通过转到 **环境 > SMTP** 选项卡，并将 **Enable Email Batching** 设置为 **True**，可以在 Mattermost **系统控制台** 中启用电子邮件批处理。

这个设置也可以在 `/var/opt/gitlab/mattermost/config.json` 中配置。

## 升级 GitLab Mattermost

| GitLab 版本  | Mattermost 版本 |
| :------------ |:----------------|
| 13.12 | 5.34 |
| 14.0 | 5.35 |
| 14.1 | 5.36 |
| 14.2 | 5.37 |
| 14.3 | 5.38 |
| 14.4 | 5.39 |
| 14.5 | 5.39 |
| 14.6 | 6.1  |
| 14.7 | 6.2 |

- 14.5 版本维持在 Mattermost 5.39
- 14.6 版本更新到 Mattermost 6.1 而不是 6.0

NOTE:
升级 Mattermost 版本时，务必查看 Mattermost 的 [重要升级说明](https://docs.mattermost.com/administration/important-upgrade-notes.html) 以解决需要执行的任何更改或迁移。

GitLab Mattermost 可以通过常规的 Omnibus GitLab 更新过程进行升级。当升级以前版本的 GitLab 时，只有在 GitLab 之外没有更改 Mattermost 配置设置的情况下才能使用该过程（即没有直接或通过 Mattermost **系统控制台** 对 Mattermost 的 `config.json` 文件进行任何更改，后者会将更改保存回 `config.json`。）

如果只使用 `gitlab.rb` 配置了 Mattermost，您可以使用 Omnibus 升级 GitLab，然后运行 `gitlab-ctl reconfigure` 将 GitLab Mattermost 升级到最新版本。

如果不是这种情况，有两种选择：

1. 通过更改 `config.json` 更新 [`gitlab.rb`](https://jihulab.com/gitlab-cn/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template#L706)。这可能需要添加一些参数，因为并非 `config.json` 中的所有设置都在 `gitlab.rb` 中可用。完成后，Omnibus GitLab 应该能够将 GitLab Mattermost 从一个版本升级到下一个版本。
1. 将 Mattermost 迁移到 Omnibus GitLab 控制的目录之外，以便它可以独立管理和升级。按照 [Mattermost 迁移指南](https://docs.mattermost.com/administration/migrating.html) 将您的 Mattermost 配置设置和数据移动到另一个独立于 Omnibus GitLab 的目录或服务器。

有关旧版本的升级通知和特殊注意事项的完整列表，请参阅 [Mattermost 文档](https://docs.mattermost.com/administration/important-upgrade-notes.html)。

## 将 GitLab Mattermost 升级到 14.6

14.6 版本包括 Mattermost 6.1，还包括 Mattermost 6.0 的迁移。有关升级的信息以及减少这些迁移停机时间的方法，请阅读两个版本的[重要升级说明](https://docs.mattermost.com/administration/important-upgrade-notes.html)。

NOTE:
Mattermost 升级说明指的是与 PostgreSQL 和 MySQL 数据库一起使用时的不同影响。GitLab Linux 软件包中包含的 GitLab Mattermost 使用 PostgreSQL 数据库。

如果您需要连接数据库以执行任何手动迁移，请运行以下命令：

```console
sudo gitlab-psql -d mattermost_production
```

然后，您可以运行[重要升级说明](https://docs.mattermost.com/administration/important-upgrade-notes.html)中描述的必要查询。

<!--
## Upgrading GitLab Mattermost from versions prior to 11.0

With version 11.0, GitLab introduced breaking changes which affected Mattermost configuration.
In versions prior to GitLab 11.0 all
Mattermost-related settings were configurable from the `gitlab.rb` file, which
generated the Mattermost `config.json` file. However, Mattermost also
permitted configuration via its System Console. This configuration ended up in
the same `config.json` file, which resulted in changes made via the System Console being
overwritten when users ran `gitlab-ctl reconfigure`.

To resolve this problem, `gitlab.rb` includes only the
configuration necessary for GitLab<=>Mattermost integration in 11.0. GitLab no longer
generates the `config.json` file, and instead passes limited configuration settings via environment variables.

The settings that continue to be supported in `gitlab.rb` can be found in
[`gitlab.rb.template`](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template).

From GitLab 11.0, other Mattermost settings can be configured through Mattermost's System Console,
by editing `/var/opt/gitlab/mattermost/config.json`, or by using `mattermost['env']` in `gitlab.rb`.

If you would like to keep configuring Mattermost using `gitlab.rb`, you can take the following actions
in preparation for GitLab 11.0:

1. Upgrade to version 10.x which supports the new `mattermost['env']` setting.
1. Configure any settings not listed above through the `mattermost['env']` setting. Mattermost requires
   environment variables to be provided in `MM_<CATEGORY>SETTINGS_<ATTRIBUTE>` format. Below is an example
   of how to convert the old settings syntax to the new one.

The following settings in `gitlab.rb`:

```ruby
mattermost['service_maximum_login_attempts'] = 10
mattermost['team_teammate_name_display'] = "full_name"
mattermost['sql_max_idle_conns'] = 10
mattermost['log_file_level'] = 'INFO'
mattermost['email_batching_interval'] = 30
mattermost['file_enable_file_attachments'] = true
mattermost['ratelimit_memory_store_size'] = 10000
mattermost['support_terms_of_service_link'] = "/static/help/terms.html"
mattermost['privacy_show_email_address'] = true
mattermost['localization_available_locales'] = "en,es,fr,ja,pt-BR"
mattermost['webrtc_enable'] = false
```

Would translate to:

```ruby
mattermost['env'] = {
                    'MM_SERVICESETTINGS_MAXIMUMLOGINATTEMPTS' => '10',
                    'MM_TEAMSETTINGS_TEAMMATENAMEDISPLAY' => 'full_name',
                    'MM_SQLSETTINGS_MAXIDLECONNS' => '10',
                    'MM_LOGSETTINGS_FILELEVEL' => 'INFO',
                    'MM_EMAILSETTINGS_BATCHINGINTERVAL' => '30',
                    'MM_FILESETTINGS_ENABLEFILEATTACHMENTS' => 'true',
                    'MM_RATELIMITSETTINGS_MEMORYSTORESIZE' => '10000',
                    'MM_SUPPORTSETTINGS_TERMSOFSERVICELINK' => '/static/help/terms.html',
                    'MM_PRIVACYSETTINGS_SHOWEMAILADDRESS' => 'true',
                    'MM_LOCALIZATIONSETTINGS_AVAILABLELOCALES' => 'en,es,fr,ja,pt-BR',
                    'MM_WEBRTCSETTINGS_ENABLE' => 'false'
                    }
```

Refer to the [Mattermost Configuration Settings
documentation](https://docs.mattermost.com/administration/config-settings.html)
for details about categories, configuration values, etc.

There are a few exceptions to this rule:

1. `ServiceSettings.ListenAddress` configuration of Mattermost is configured
   by `mattermost['service_address']` and `mattermost['service_port']` settings.
1. Configuration settings named in an inconsistent way are given in the
   following table. Use these mappings when converting them to environment
   variables.

|`gitlab.rb` configuration|Environment variable|
|---|---|
|`mattermost['service_lets_encrypt_cert_cache_file']`|`MM_SERVICESETTINGS_LETSENCRYPTCERTIFICATECACHEFILE`|
|`mattermost['service_user_access_tokens']`|`MM_SERVICESETTINGS_ENABLEUSERACCESSTOKENS`|
|`mattermost['log_console_enable']`|`MM_LOGSETTINGS_ENABLECONSOLE`|
|`mattermost['email_enable_batching']`|`MM_EMAILSETTINGS_ENABLEEMAILBATCHING`|
|`mattermost['email_batching_buffer_size']`|`MM_EMAILSETTINGS_EMAILBATCHINGBUFFERSIZE`|
|`mattermost['email_batching_interval']`|`MM_EMAILSETTINGS_EMAILBATCHINGINTERVAL`|
|`mattermost['email_smtp_auth']`|`MM_EMAILSETTINGS_ENABLESMTPAUTH`|
|`mattermost['email_notification_content_type']`|`MM_EMAILSETTINGS_NOTIFICATIONCONTENTTYPE`|
|`mattermost['ratelimit_enable_ratelimiter']`|`MM_RATELIMITSETTINGS_ENABLE`|
|`mattermost['support_email']`|`MM_SUPPORTSETTINGS_SUPPORTEMAIL`|
|`mattermost['localization_server_locale']`|`MM_LOCALIZATIONSETTINGS_DEFAULTSERVERLOCALE`|
|`mattermost['localization_client_locale']`|`MM_LOCALIZATIONSETTINGS_DEFAULTCLIENTLOCALE`|
|`mattermost['webrtc_gateway_stun_uri']`|`MM_WEBRTCSETTINGS_STUN_URI`|
|`mattermost['webrtc_gateway_turn_uri']`|`MM_WEBRTCSETTINGS_TURN_URI`|
|`mattermost['webrtc_gateway_turn_username']`|`MM_WEBRTCSETTINGS_TURN_USERNAME`|
|`mattermost['webrtc_gateway_turn_sharedkey']`|`MM_WEBRTCSETTINGS_TURN_SHAREDKEY`|

NOTE:
GitLab 11.0 no longer generates `config.json` file from the configuration specified
in `gitlab.rb`. Users are responsible for managing this file which can be done via the
Mattermost System Console or manually.
If a configuration setting is specified via both the `gitlab.rb` (as an environment variable)
and `config.json` files, the environment variable gets precedence.

If you encounter any issues [visit the GitLab Mattermost troubleshooting forum](https://forum.mattermost.org/t/upgrading-to-gitlab-mattermost-in-gitlab-8-9/1735) and share any relevant portions of `mattermost.log` along with the step at which you encountered issues.
-->

### 在 GitLab 之外升级 GitLab Mattermost

如果您选择在 Omnibus GitLab 自动化之外升级 Mattermost，请[遵循本指南](https://docs.mattermost.com/administration/upgrade.html)。

## OAuth2 序列图

下图是极狐GitLab 如何作为 Mattermost 的 OAuth2 provider 工作的序列图。您可以使用它来解决集成的错误：

```mermaid
sequenceDiagram
    User->>Mattermost: GET https://mm.domain.com
    Note over Mattermost, GitLab: Obtain access code
    Mattermost->>GitLab: GET https://gitlab.domain.com/oauth/authorize
    Note over User, GitLab: GitLab user signs in (if necessary)
    Note over GitLab: GitLab verifies client_id matches an OAuth application
    GitLab->>User: GitLab asks user to authorize Mattermost OAuth app
    User->>GitLab: User selects 'Allow'
    Note over GitLab: GitLab verifies redirect_uri matches list of valid URLs
    GitLab->>User: 302 redirect: https://mm.domain.com/signup/gitlab/complete
    User->>Mattermost: GET https://mm.domain.com/signup/gitlab/complete
    Note over Mattermost, GitLab: Exchange access code for access token
    Mattermost->>GitLab: POST http://gitlab.domain.com/oauth/token
    GitLab->>GitLab: Doorkeeper::TokensController#35;create
    GitLab->>Mattermost: Access token
    Note over Mattermost, GitLab: Mattermost looks up GitLab user
    Mattermost->>GitLab: GET https://gitlab.domain.com/api/v4/user
    GitLab->>Mattermost: User details
    Mattermost->>User: Mattermost/GitLab user ready
```

##  Mattermost CLI 故障排查

### 10 秒后重试 ping 数据库失败 err=dial tcp: lookup dockerhost: no such host

大多数 Mattermost 设置现在都通过环境变量进行配置。该错误主要是由于在 `gitlab.rb` 中注释掉了数据库连接字符串，以及在环境变量中设置了数据库连接设置。此外，不再支持 `gitlab.rb` 中的连接字符串用于 MySQL。

您可以通过设置 `mattermost-cli` [shell 别名](#mattermost-命令行工具-cli) 来解决此问题。

## 社区支持资源

有关您的 GitLab Mattermost 部署的帮助和支持，请参阅：

- [故障排查论坛](https://forum.mattermost.org/t/how-to-use-the-troubleshooting-forum/150)用于配置问题和议题。
- [故障排查常见问题](https://docs.mattermost.com/install/troubleshooting.html)。
- [Mattermost GitLab 议题支持手册](https://docs.mattermost.com/process/support.html?highlight=omnibus#gitlab-issues)。
- [GitLab Mattermost 议题跟踪器](https://gitlab.com/gitlab-org/gitlab-mattermost/-/issues) 用于重现步骤验证错误。
