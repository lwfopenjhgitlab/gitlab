---
type: reference
stage: Data Stores
group: Global Search
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Elasticsearch 集成 **(PREMIUM SELF)**

本页介绍如何启用高级搜索。启用后，高级搜索提供更快的搜索响应时间和[改进的搜索功能](../../user/search/advanced_search.md)。

## 版本要求

### Elasticsearch 版本要求

> 对 Elasticsearch 6.8 的支持删除于 15.0 版本。

高级搜索适用于以下版本的 Elasticsearch。

| 极狐GitLab 版本        | Elasticsearch 版本    |
|-----------------------|--------------------------|
| 15.0 或更高  | Elasticsearch 7.x - 8.x  |
| 13.9 - 14.10   | Elasticsearch 6.8 - 7.x  |
| 13.3 - 13.8    | Elasticsearch 6.4 - 7.x  |
| 12.7 - 13.2    | Elasticsearch 6.x - 7.x  |

高级搜索遵循 Elasticsearch 的[生命周期结束策略](https://www.elastic.co/support/eol)。
<!--When we change Elasticsearch supported versions in GitLab, we announce them in [deprecation notes](https://about.gitlab.com/handbook/marketing/blog/release-posts/#deprecations) in monthly release posts
before we remove them.-->

### OpenSearch 版本要求

| 极狐GitLab version        | Elasticsearch 版本   |
|-----------------------|--------------------------|
| 15.0 或更高版本  | OpenSearch 1.x 或更高版本  |

如果您的 Elasticsearch 或 OpenSearch 版本不兼容，为了防止数据丢失，索引会暂停，并且会在 `elasticsearch.log` 文件中记录一条消息。

如果您使用的是兼容版本并且在连接到 OpenSearch 后，您会收到消息 `Elasticsearch version not compatible`，[取消暂停索引](#unpause-indexing)。

## 系统要求

Elasticsearch 需要[极狐GitLab 系统要求](../../install/requirements.md)中记录的资源之外的其他资源。

内存、CPU 和存储资源量取决于您索引到 Elasticsearch 集群中的数据量。大量使用的 Elasticsearch 集群可能需要更多资源。根据[Elasticsearch 官方指南](https://www.elastic.co/guide/en/elasticsearch/guide/current/hardware.html#_memory)，每个节点应该有：

- [内存](https://www.elastic.co/guide/en/elasticsearch/guide/current/hardware.html#_memory)：8 GiB（最小）。
- [CPU](https://www.elastic.co/guide/en/elasticsearch/guide/current/hardware.html#_cpus)：具有多核的现代处理器。 极狐GitLab 对 Elasticsearch 有最小 CPU 要求。多核提供额外的并发性，这比更快的 CPU 更有利。
- [存储](https://www.elastic.co/guide/en/elasticsearch/guide/current/hardware.html#_disks)：使用 SSD 存储。所有 Elasticsearch 节点的总存储大小约为 Git 仓库总大小的 50%。它包括一个主节点和一个副本。[`estimate_cluster_size`](#gitlab-advanced-search-rake-tasks) Rake 任务（引入于 13.10）使用总仓库大小来估计高级搜索存储要求。

## 安装 Elasticsearch

Elasticsearch *不*包含在 Omnibus 包中。您必须[单独安装](https://www.elastic.co/guide/en/elasticsearch/reference/7.16/install-elasticsearch.html)并确保选择您的版本。有关如何安装 Elasticsearch 的详细信息不在范围内。

您可以自己安装 Elasticsearch，或使用云托管产品，例如 [Elasticsearch Service](https://www.elastic.co/elasticsearch/service)（在 AWS、GCP 或 Azure 上可用）或 [Amazon OpenSearch](https://docs.aws.amazon.com/opensearch-service/latest/developerguide/gsg.html) 服务。

您应该在单独的服务器上安装 Elasticsearch。不建议在极狐GitLab 所在的相同服务器上运行 Elasticsearch，这可能会导致极狐GitLab 实例性能下降。

对于单节点 Elasticsearch 集群，由于主分片的分配，功能集群健康状态始终为黄色。Elasticsearch 无法将副本分片分配给与主分片相同的节点。

搜索索引在您执行以下操作后更新：

- 将数据添加到数据库或仓库。
- 在管理中心[启用了 Elasticsearch](#enable-advanced-search)。

## 升级到新的 Elasticsearch 主要版本

> - 对 Elasticsearch 6.8 的支持删除于 15.0 版本。
> - 从 14.10 升级到 15.0 要求您使用 Elasticsearch 7.x 的任何版本。

升级 Elasticsearch 时不需要更改极狐GitLab 配置。

## Elasticsearch 仓库索引器

为了索引 Git 仓库数据，极狐GitLab 使用 Go 编写的索引器。

<!--
Depending on your GitLab version, there are different installation procedures for the Go indexer:

- For Omnibus GitLab 11.8 or greater, see [Omnibus GitLab](#omnibus-gitlab).
- For installations from source or older versions of Omnibus GitLab,
  [install the indexer from source](#from-source).
- If you are using GitLab Development Kit, see [GDK Elasticsearch how-to](https://jihulab.com/gitlab-cn/gitlab-development-kit/-/blob/main/doc/howto/elasticsearch.md).
-->

### Omnibus GitLab

从 11.8 版本开始，Go 索引器包含在 Omnibus GitLab 中。
<!--The former Ruby-based indexer was removed in [GitLab 12.3](https://jihulab.com/gitlab-cn/gitlab/-/issues/6481).-->

<!--
### From source

First, we need to install some dependencies, then we build and install
the indexer itself.

#### Install dependencies

This project relies on [International Components for Unicode](https://icu.unicode.org/) (ICU) for text encoding,
therefore we must ensure the development packages for your platform are
installed before running `make`.

##### Debian / Ubuntu

To install on Debian or Ubuntu, run:

```shell
sudo apt install libicu-dev
```

##### CentOS / RHEL

To install on CentOS or RHEL, run:

```shell
sudo yum install libicu-devel
```

##### macOS

NOTE:
You must first [install Homebrew](https://brew.sh/).

To install on macOS, run:

```shell
brew install icu4c
export PKG_CONFIG_PATH="/usr/local/opt/icu4c/lib/pkgconfig:$PKG_CONFIG_PATH"
```

#### Build and install

To build and install the indexer, run:

```shell
indexer_path=/home/git/gitlab-elasticsearch-indexer

# Run the installation task for gitlab-elasticsearch-indexer:
sudo -u git -H bundle exec rake gitlab:indexer:install[$indexer_path] RAILS_ENV=production
cd $indexer_path && sudo make install
```

The `gitlab-elasticsearch-indexer` is installed to `/usr/local/bin`.

You can change the installation path with the `PREFIX` environment variable.
Please remember to pass the `-E` flag to `sudo` if you do so.

Example:

```shell
PREFIX=/usr sudo -E make install
```

After installation, be sure to [enable Elasticsearch](#enable-advanced-search).

NOTE:
If you see an error such as `Permission denied - /home/git/gitlab-elasticsearch-indexer/` while indexing, you
may need to set the `production -> elasticsearch -> indexer_path` setting in your `gitlab.yml` file to
`/usr/local/bin/gitlab-elasticsearch-indexer`, which is where the binary is installed.
-->

### 查看索引错误

极狐GitLab Elasticsearch 索引器的错误报告在 [`elasticsearch.log`](../../administration/logs/index.md#elasticsearchlog) 文件，和 [`sidekiq.log`](../../administration/logs/index.md#sidekiqlog) 文件，`json.exception.class` 为 `Gitlab::Elastic::Indexer::Error`。
索引 Git 仓库数据时可能会出现这些错误。

<a id="enable-advanced-search"></a>

## 启用高级搜索

<!--
对于仓库数据超过 50GB 的极狐GitLab 实例，您可以按照下面的[如何有效索引大型实例](#how-to-index-large-instances-efficiently)的说明进行操作。
-->

要启用高级搜索，您必须具有对极狐GitLab 的管理员访问权限：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 高级搜索**。

   NOTE:
   要查看高级搜索部分，您需要一个有效的专业版许可证。

1. 为您的 Elasticsearch 集群配置[高级搜索设置](#advanced-search-configuration)。暂时不要选择 **启用 Elasticsearch 的搜索**。
1. 启用 **Elasticsearch 索引** 并选择 **保存更改**。如果索引不存在，这将创建一个空索引。
1. 选择 **索引所有项目**。
1. 在确认消息中选择 **检查进度**，查看后台作业的状态。
1. 个人代码片段必须使用另一个 Rake 任务进行索引：

   ```shell
   # Omnibus installations
   sudo gitlab-rake gitlab:elastic:index_snippets

   # Installations from source
   bundle exec rake gitlab:elastic:index_snippets RAILS_ENV=production
   ```

1. 索引完成后，选择 **启用 Elasticsearch 的搜索** 并选择 **保存更改**。

NOTE:
当您的 Elasticsearch 集群在启用 Elasticsearch 的情况下关闭时，您可能会在更新例如议题之类的文档内容时遇到问题，因为您的实例将作业排队以索引更改，但找不到有效的 Elasticsearch 集群。

<a id="advanced-search-configuration"></a>

### 高级搜索配置

以下 Elasticsearch 设置可用：

| 参数                                            | 描述 |
|-------------------------------------------------------|-------------|
| `Elasticsearch indexing`                              | 启用或禁用 Elasticsearch 索引并创建一个空索引（如果尚不存在）。例如，您可能希望启用索引但禁用搜索以使索引有时间完全完成。此外请记住，此选项对现有数据没有任何影响，只启用/禁用跟踪数据更改并确保索引新数据的后台索引器。 |
| `Pause Elasticsearch indexing`                        | 启用或禁用临时索引暂停。这对于集群迁移/重新索引很有用。仍会跟踪所有更改，但在恢复之前不会将它们提交到 Elasticsearch 索引。 |
| `Search with Elasticsearch enabled`                   | 启用或禁用在搜索中使用 Elasticsearch。 |
| `URL`                                                 | 您的 Elasticsearch 实例的 URL。 使用逗号分隔的列表来支持集群（例如，`http://host1, https://host2:9200`）。 如果您的 Elasticsearch 实例受密码保护，请使用下面描述的 `Username` 和 `Password` 字段。或者，使用凭据，例如 `http://<username>:<password>@<elastic_host>:9200/`。 |
| `Username`                                                 | 您的 Elasticsearch 实例的 `username`。 |
| `Password`                                                 | 您的 Elasticsearch 实例的密码。 |
| `Number of Elasticsearch shards`                      | 出于性能原因，Elasticsearch 索引被分成多个分片。一般来说，应该使用至少 5 个分片，数千万文档的索引需要有更多的分片。在重新创建索引之前，对此值的更改不会生效。您可以在 [Elasticsearch 文档](https://www.elastic.co/guide/en/elasticsearch/reference/current/scalability.html)中阅读有关的更多信息。 |
| `Number of Elasticsearch replicas`                    | 每个 Elasticsearch 分片可以有多个副本。这些是分片的完整副本，可以提供更高的查询性能或针对硬件故障的弹性。 增加此值会增加索引所需的总磁盘空间。 |
| `Limit the number of namespaces and projects that can be indexed`   | 启用此选项后，您可以选择要索引的命名空间和项目。所有其他命名空间和项目都使用数据库搜索。如果启用此选项但未选择任何命名空间或项目，则不会编制索引。 |
| `Using AWS OpenSearch Service with IAM credentials` | 使用 [AWS IAM 授权](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html)、[AWS EC2 实例配置文件凭证](https://docs.aws.amazon.com/codedeploy/latest/userguide/getting-started-create-iam-instance-profile.html#getting-started-create-iam-instance-profile-cli)，或 [AWS ECS 任务凭证](https://docs.aws.amazon.com/AmazonECS/latest/userguide/task-iam-roles.html)。有关 AWS 托管的 OpenSearch 域访问策略配置的详细信息，请参阅 [Amazon OpenSearch 服务中的身份和访问管理](https://docs.aws.amazon.com/opensearch-service/latest/developerguide/ac.html)。 |
| `AWS Region`                                          | 您的 OpenSearch 服务所在的 AWS 区域。 |
| `AWS Access Key`                                      | AWS 访问密钥。 |
| `AWS Secret Access Key`                               | AWS secret 访问密钥。 |
| `Maximum file size indexed`                           | 查看[实例限制文档中的解释](../../administration/instance_limits.md#maximum-file-size-indexed)。 |
| `Maximum field length`                                | 查看[实例限制文档中的解释](../../administration/instance_limits.md#maximum-field-length)。 |
| `Maximum bulk request size (MiB)` | 基于 Golang 的索引器进程使用最大批量请求大小，并指示在将负载提交到 Elasticsearch 的批量 API 之前，它应该在给定的索引进程中收集（并存储在内存中）多少数据。此设置应与批量请求并发设置（见下文）一起使用，并且需要适应 Elasticsearch 主机和运行基于 Golang 的索引器的主机的资源限制，无论是来自 `gitlab-rake` 命令还是 Sidekiq 任务。 |
| `Bulk request concurrency`                            | Bulk 请求并发表示有多少基于 Golang 的索引器进程（或线程）可以并行运行，收集数据并随后提交给 Elasticsearch 的 Bulk API。这会提高索引性能，但会更快地填充 Elasticsearch 批量请求队列。此设置应与最大批量请求大小设置（见上文）一起使用，并且需要适应 Elasticsearch 主机和运行基于 Golang 的索引器的主机的资源限制，无论是来自 `gitlab-rake` 命令还是 Sidekiq 任务。 |
| `Client request timeout` | Elasticsearch HTTP 客户端请求超时值（以秒为单位）。`0` 表示使用系统默认超时值，这取决于构建极狐GitLab 应用程序的库。 |

WARNING:
增加 `Maximum bulk request size (MiB)` 和 `Bulk request concurrency` 的值会对 Sidekiq 性能产生负面影响。如果您在 Sidekiq 日志中看到增加的 `scheduling_latency_s` 持续时间，请将它们恢复为默认值。

### 限制可以被索引的命名空间和项目的数量

如果您选中 **Elasticsearch 索引限制** 下的复选框 `Limit the number of namespaces and projects that can be indexed`，则更多选项可用。

![limit namespaces and projects options](img/limit_namespaces_projects_options.png)

您可以选择要独占索引的命名空间和项目。请注意，如果命名空间是一个群组，它还包括属于这些子组的任何子组和项目，这些子组也将被索引。

如果所有命名空间都被索引，高级搜索仅提供跨组代码/提交搜索（全局）。在这种仅索引命名空间子集的特定场景中，全局搜索不提供代码或提交范围。这仅在索引命名空间的范围内是可能的。没有办法在多个索引命名空间中编码/提交搜索（当只有命名空间的一个子集被索引时）。例如，对两个群组进行了索引，则无法对这两个群组运行单个代码搜索。您只能在第一个群组上运行代码搜索，然后在第二个群组上运行。

您可以通过编写您感兴趣的命名空间或项目名称的一部分来过滤选择下拉列表。

![limit namespace filter](img/limit_namespace_filter.png)

NOTE:
如果未选择命名空间或项目，则不会进行高级搜索索引。

WARNING:
如果您已经为您的实例编制了索引，则必须重新生成索引以删除所有现有数据，以便过滤正常工作。为此，请运行 Rake 任务 `gitlab:elastic:recreate_index` 和 `gitlab:elastic:clear_index_status`。之后，从列表中删除命名空间或项目会按预期从 Elasticsearch 索引中删除数据。

## 启用自定义语言分析器

您可以通过使用 [`smartcn`](https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-smartcn.html) 和/或 [` kuromoji`](https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-kuromoji.html) 来自 Elastic 的分析插件。

要启用语言支持：

1. 安装需要的插件，插件安装说明请参考 [Elasticsearch 文档](https://www.elastic.co/guide/en/elasticsearch/plugins/7.9/installation.html)。插件必须安装在集群中的每个节点上，并且安装后必须重新启动每个节点。有关插件列表，请参阅本节后面的表格。
1. 在顶部栏上，选择 **主菜单 > 管理源**。
1. 在左侧边栏中，选择 **设置 > 高级搜索**。
1. 找到 **自定义分析器：语言支持**。
1. 启用插件支持 **索引**。
1. 选择 **保存更改** 以使更改生效。
1. 触发[零停机时间重新索引](#zero-downtime-reindexing)，或从头开始重新索引所有内容以创建具有更新映射的新索引。
1. 上一步完成后启用插件支持 **搜索**。

有关安装内容的指导，请参阅以下 Elasticsearch 语言插件选项：

| 参数                                             | 描述 |
|-------------------------------------------------------|-------------|
| `Enable Chinese (smartcn) custom analyzer: Indexing`   | 使用 [`smartcn`](https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-smartcn.html) 自定义分析器，为新创建的索引启用或禁用中文支持。 |
| `Enable Chinese (smartcn) custom analyzer: Search`   | 使用 [`smartcn`](https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-smartcn.html) 字段启用或禁用高级搜索。请仅在[安装插件](https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-smartcn.html)后启用此功能，启用自定义分析器索引并重新创建索引。 |
| `Enable Japanese (kuromoji) custom analyzer: Indexing`   | 使用 [`kuromoji`](https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-kuromoji.html) 自定义分析器为新创建的索引启用或禁用日语支持。 |
| `Enable Japanese (kuromoji) custom analyzer: Search`  | 使用 [`kuromoji`](https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-kuromoji.html) 字段启用或禁用高级搜索。请仅在[安装插件](https://www.elastic.co/guide/en/elasticsearch/plugins/current/analysis-kuromoji.html)后启用此功能，启用自定义分析器索引并重新创建索引。 |

## 禁用高级搜索

要禁用 Elasticsearch 集成：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 高级搜索**。
1. 取消选中 **Elasticsearch 索引** 和 **启用 Elasticsearch 的搜索**。
1. 选择 **保存更改**。
1. 可选。删除现有索引：

   ```shell
   # Omnibus installations
   sudo gitlab-rake gitlab:elastic:delete_index

   # Installations from source
   bundle exec rake gitlab:elastic:delete_index RAILS_ENV=production
   ```

<a id="unpause-indexing"></a>

## 取消暂停索引

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 高级搜索**。
1. 展开 **高级搜索**。
1. 清除 **暂停 Elasticsearch 索引** 复选框。

<a id="zero-downtime-reindexing"></a>

## 零停机时间重新索引

这种重新索引方法背后的想法是利用 [Elasticsearch reindex API](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-reindex.html) 和 Elasticsearch 索引别名功能来执行操作。我们设置了一个索引别名，它连接到极狐GitLab 用于读取/写入的 `primary` 索引。当重新索引过程开始时，我们会暂时暂停对 `primary` 索引的写入。然后，我们创建另一个索引并调用将索引数据迁移到新索引上的 Reindex API。重新索引作业完成后，我们通过将索引别名连接到新索引来切换到新索引，这将成为新的 `primary` 索引。最后，我们恢复写入并恢复正常操作。

### 通过高级搜索管理触发重新索引

> - 引入于 13.2 版本。
> - 删除和取消计划索引的功能引入于 13.3 版本。
> - 支持在重新索引期间重试的功能引入于 13.12 版本。

要触发重新索引过程：

1. 以管理员身份登录您的极狐GitLab 实例。
1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 高级搜索**。
1. 展开 **Elasticsearch 零停机重新索引**。
1. 选择 **触发集群重新索引**。

重新索引可能是一个漫长的过程，具体取决于您的 Elasticsearch 集群的大小。

此过程完成后，原索引计划在 14 天后删除。您可以通过在触发重新索引过程的同一页面上按 **取消** 按钮来取消此操作。

在重新索引运行时，您可以在同一部分下跟踪其进度。

#### Elasticsearch 零停机时间重新索引

> 引入于 13.12 版本。

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 高级搜索**。
1. 展开 **Elasticsearch 零停机重新索引**，您会发现以下选项：

- [Slice multiplier](#slice-multiplier)
- [最大运行分片](#maximum-running-slices)

##### Slice multiplier

Slice multiplier 计算[重新索引期间的分片数](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-reindex.html#docs-reindex-slice)。

极狐GitLab 使用[手动分片](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-reindex.html#docs-reindex-manual-slice)来有效和安全地控制重新索引，允许用户仅重试失败的分片。

Multiplier 默认为 `2`，适用于每个索引的分片数。
例如，如果此值为 `2` 并且您的索引有 20 个分片，则重新索引任务将被拆分为 40 个分片。

<a id="maximum-running-slices"></a>

##### 最大运行分片

最大运行分片参数默认为 `60`，对应于在 Elasticsearch 重新索引期间允许并发运行的最大分片数。

将此值设置得太高可能会对性能产生不利影响，因为您的集群可能会因搜索和写入而严重饱和。将此值设置得太低可能会导致重新索引过程需要很长时间才能完成。

最佳值取决于您的集群大小、您是否愿意在重新索引期间接受某些降低的搜索性能，以及重新索引快速完成并恢复索引的重要性。

### 将最近的重新索引作业标记为失败并恢复索引

有时，您可能希望放弃未完成的重新索引作业并恢复索引。您可以通过以下步骤实现此目的：

1. 将最近的重新索引作业标记为失败：

   ```shell
   # Omnibus installations
   sudo gitlab-rake gitlab:elastic:mark_reindex_failed

   # Installations from source
   bundle exec rake gitlab:elastic:mark_reindex_failed RAILS_ENV=production
   ```

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 高级搜索**。
1. 展开 **高级搜索**。
1. 清除 **暂停 Elasticsearch 索引** 复选框。

<a id="advanced-search-migrations"></a>

## 高级搜索迁移

> 引入于 13.6 版本。

在后台运行重新索引迁移，无需手动干预。这通常发生在向高级搜索添加新功能的情况下，意味着添加或更改内容的索引方式。

要确认高级搜索迁移已运行，您可以检查：

```shell
curl "$CLUSTER_URL/gitlab-production-migrations/_search?q=*" | jq .
```

这应该返回类似于：

```json
{
  "took": 14,
  "timed_out": false,
  "_shards": {
    "total": 1,
    "successful": 1,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": {
      "value": 1,
      "relation": "eq"
    },
    "max_score": 1,
    "hits": [
      {
        "_index": "gitlab-production-migrations",
        "_type": "_doc",
        "_id": "20201105181100",
        "_score": 1,
        "_source": {
          "completed": true
        }
      }
    ]
  }
}
```

为了调试迁移问题，您可以检查 [`elasticsearch.log` 文件](../../administration/logs/index.md#elasticsearchlog)。

<!--
### 重试停止的迁移

Some migrations are built with a retry limit. If the migration cannot finish within the retry limit,
it is halted and a notification is displayed in the Advanced Search integration settings.
It is recommended to check the [`elasticsearch.log` file](../../administration/logs/index.md#elasticsearchlog) to
debug why the migration was halted and make any changes before retrying the migration. Once you believe you've
fixed the cause of the failure, select "Retry migration", and the migration is scheduled to be retried
in the background.

If you cannot get the migration to succeed, you may
consider the
[last resort to recreate the index from scratch](elasticsearch_troubleshooting.md#last-resort-to-recreate-an-index).
This may allow you to skip over
the problem because a newly created index skips all migrations as the index
is recreated with the correct up-to-date schema.

### All migrations must be finished before doing a major upgrade

Before doing a major version GitLab upgrade, you should have completed all
migrations that exist up until the latest minor version before that major
version. If you have halted migrations, these need to be resolved and
[retried](#retry-a-halted-migration) before proceeding with a major version
upgrade. Read more about [upgrading to a new major version](../../update/index.md#upgrading-to-a-new-major-version).
-->

<a id="gitlab-advanced-search-rake-tasks"></a>

## 极狐GitLab 高级搜索 Rake 任务

Rake 任务可用于：

- [构建和安装](#build-and-install)索引器。
- [禁用 Elasticsearch](#disable-advanced-search) 时删除索引。
- 将极狐GitLab 数据添加到索引。

以下是一些可用的 Rake 任务：

| 任务                                                                                                                                                    | 描述                                                                                                                                                                               |
|:--------------------------------------------------------------------------------------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [`sudo gitlab-rake gitlab:elastic:info`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)                            | 输出高级搜索集成的调试信息。 |
| [`sudo gitlab-rake gitlab:elastic:index`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)                            | 启用 Elasticsearch 索引并运行 `gitlab:elastic:create_empty_index`、`gitlab:elastic:clear_index_status`、`gitlab:elastic:index_projects` 和 `gitlab:elastic:index_snippets`。                          |
| [`sudo gitlab-rake gitlab:elastic:pause_indexing`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)                            | 暂停 Elasticsearch 索引。更改仍在跟踪中。对于集群/索引迁移很有用。 |
| [`sudo gitlab-rake gitlab:elastic:resume_indexing`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)                            | 恢复 Elasticsearch 索引。 |
| [`sudo gitlab-rake gitlab:elastic:index_projects`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)                   | 遍历所有项目，并将 Sidekiq 作业排队以在后台对它们进行索引。它只能在创建索引后使用。                                                                                                     |
| [`sudo gitlab-rake gitlab:elastic:index_projects_status`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)            | 确定索引的整体状态。它是通过计算索引项目的总数，除以项目总数，然后乘以 100 来完成的。 |
| [`sudo gitlab-rake gitlab:elastic:clear_index_status`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)               | 删除所有项目的所有 IndexStatus 实例。请注意，此命令会导致索引完全擦除，应谨慎使用。                                                                                             |
| [`sudo gitlab-rake gitlab:elastic:create_empty_index`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake) | 生成空索引（默认索引和单独的问题索引）并在 Elasticsearch 端为每个索引分配一个别名，前提是它不存在。                                                                                                      |
| [`sudo gitlab-rake gitlab:elastic:delete_index`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)       | 删除 Elasticsearch 实例上的极狐GitLab 索引和别名（如果存在）。                                                                                                                                   |
| [`sudo gitlab-rake gitlab:elastic:recreate_index`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)     | `gitlab:elastic:delete_index` 和 `gitlab:elastic:create_empty_index` 的包装器任务。                                                                       |
| [`sudo gitlab-rake gitlab:elastic:index_snippets`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)                   | 执行为片段数据编制索引的 Elasticsearch 导入。                                                                                                                          |
| [`sudo gitlab-rake gitlab:elastic:projects_not_indexed`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)             | 显示未编入索引的项目。                                                                                                                                                  |
| [`sudo gitlab-rake gitlab:elastic:reindex_cluster`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)                  | 安排零停机时间的集群重新索引任务。此功能应与在 13.0 之后创建的索引一起使用。 |
| [`sudo gitlab-rake gitlab:elastic:mark_reindex_failed`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)              | 将最近的重新索引作业标记为失败。 |
| [`sudo gitlab-rake gitlab:elastic:list_pending_migrations`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)          | 列出待处理的迁移。待定迁移包括尚未开始的迁移、已开始但尚未完成的迁移以及已停止的迁移。 |
| [`sudo gitlab-rake gitlab:elastic:estimate_cluster_size`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)            | 根据总仓库大小估算集群大小。 |
| [`sudo gitlab-rake gitlab:elastic:enable_search_with_elasticsearch`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)            | 启用 Elasticsearch 的高级搜索。 |
| [`sudo gitlab-rake gitlab:elastic:disable_search_with_elasticsearch`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/tasks/gitlab/elastic.rake)            | 禁用 Elasticsearch 的高级搜索。 |

### 环境变量

除了 Rake 任务，还有一些环境变量可以用来修改进程：

| 环境变量 | 数据类型 | 功能                                                                 |
| -------------------- |:---------:| ---------------------------------------------------------------------------- |
| `UPDATE_INDEX`       | Boolean   | 告知索引器覆盖任何现有的索引数据（true/false）。         |
| `ID_TO`              | Integer   | 告诉索引器仅索引小于或等于该值的项目。    |
| `ID_FROM`            | Integer   | 告诉索引器仅索引大于或等于该值的项目。 |

### 索引一系列项目或特定项目

使用 `ID_FROM` 和 `ID_TO` 环境变量，您可以索引有限数量的项目。这对于分段索引很有用。

```shell
root@git:~# sudo gitlab-rake gitlab:elastic:index_projects ID_FROM=1 ID_TO=100
```

因为 `ID_FROM` 和 `ID_TO` 使用 `or equal to` 比较，您可以通过将它们设置为相同的项目 ID 来仅索引一个项目：

```shell
root@git:~# sudo gitlab-rake gitlab:elastic:index_projects ID_FROM=5 ID_TO=5
Indexing project repositories...I, [2019-03-04T21:27:03.083410 #3384]  INFO -- : Indexing GitLab User / test (ID=33)...
I, [2019-03-04T21:27:05.215266 #3384]  INFO -- : Indexing GitLab User / test (ID=33) is done!
```

## 高级搜索索引范围

执行搜索时，极狐GitLab 索引使用以下范围：

| 范围名称       | 搜索的内容       |
| ---------------- | ---------------------- |
| `commits`        | 提交数据            |
| `projects`       | 项目数据（默认） |
| `blobs`          | 代码                   |
| `issues`         | 议题数据             |
| `merge_requests` | 合并请求数据     |
| `milestones`     | 里程碑数据         |
| `notes`          | 备注数据              |
| `snippets`       | 代码片段数据           |
| `wiki_blobs`     | Wiki 内容          |

<!--
## Tuning

### Guidance on choosing optimal cluster configuration

For basic guidance on choosing a cluster configuration you may refer to [Elastic Cloud Calculator](https://cloud.elastic.co/pricing). You can find more information below.

- Generally, you want to use at least a 2-node cluster configuration with one replica, which allows you to have resilience. If your storage usage is growing quickly, you may want to plan horizontal scaling (adding more nodes) beforehand.
- It's not recommended to use HDD storage with the search cluster, because it takes a hit on performance. It's better to use SSD storage (NVMe or SATA SSD drives for example).
- You can use the [GitLab Performance Tool](https://gitlab.com/gitlab-org/quality/performance) to benchmark search performance with different search cluster sizes and configurations.
- `Heap size` should be set to no more than 50% of your physical RAM. Additionally, it shouldn't be set to more than the threshold for zero-based compressed oops. The exact threshold varies, but 26 GB is safe on most systems, but can also be as large as 30 GB on some systems. See [Heap size settings](https://www.elastic.co/guide/en/elasticsearch/reference/current/important-settings.html#heap-size-settings) and [Setting JVM options](https://www.elastic.co/guide/en/elasticsearch/reference/current/jvm-options.html) for more details.
- Number of CPUs (CPU cores) per node usually corresponds to the `Number of Elasticsearch shards` setting described below.
- A good guideline is to ensure you keep the number of shards per node below 20 per GB heap it has configured. A node with a 30GB heap should therefore have a maximum of 600 shards, but the further below this limit you can keep it the better. This generally helps the cluster stay in good health.
- Number of Elasticsearch shards:
  - Small shards result in small segments, which increases overhead. Aim to keep the average shard size between at least a few GB and a few tens of GB.
  - Another consideration is the number of documents. To determine the number of shards to use, sum the numbers in the **Main menu > Admin > Dashboard > Statistics** pane (the number of documents to be indexed), divide by 5 million, and add 5. For example:
    - If you have fewer than about 2,000,000 documents, use the default of 5 shards
    - 10,000,000 documents: `10000000/5000000 + 5` = 7 shards
    - 100,000,000 documents: `100000000/5000000 + 5` = 25 shards
- `refresh_interval` is a per index setting. You may want to adjust that from default `1s` to a bigger value if you don't need data in real-time. This changes how soon you see fresh results. If that's important for you, you should leave it as close as possible to the default value.
- You might want to raise [`indices.memory.index_buffer_size`](https://www.elastic.co/guide/en/elasticsearch/reference/current/indexing-buffer.html) to 30% or 40% if you have a lot of heavy indexing operations.

### Advanced Search integration settings guidance

- The `Number of Elasticsearch shards` setting usually corresponds with the number of CPUs available in your cluster. For example, if you have a 3-node cluster with 4 cores each, this means you benefit from having at least 3*4=12 shards in the cluster. It's only possible to change the shards number by using [Split index API](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-split-index.html) or by reindexing to a different index with a changed number of shards.
- The `Number of Elasticsearch replicas` setting should most of the time be equal to `1` (each shard has 1 replica). Using `0` is not recommended, because losing one node corrupts the index.

### How to index large instances efficiently

This section may be helpful in the event that the other
[basic instructions](#enable-advanced-search) cause problems
due to large volumes of data being indexed.

WARNING:
Indexing a large instance generates a lot of Sidekiq jobs.
Make sure to prepare for this task by having a
[scalable setup](../../administration/reference_architectures/index.md) or creating
[extra Sidekiq processes](../../administration/sidekiq/extra_sidekiq_processes.md).

1. [Configure your Elasticsearch host and port](#enable-advanced-search).
1. Create empty indices:

   ```shell
   # Omnibus installations
   sudo gitlab-rake gitlab:elastic:create_empty_index

   # Installations from source
   bundle exec rake gitlab:elastic:create_empty_index RAILS_ENV=production
   ```

1. If this is a re-index of your GitLab instance, clear the index status:

   ```shell
   # Omnibus installations
   sudo gitlab-rake gitlab:elastic:clear_index_status

   # Installations from source
   bundle exec rake gitlab:elastic:clear_index_status RAILS_ENV=production
   ```

1. [Enable **Elasticsearch indexing**](#enable-advanced-search).
1. Indexing large Git repositories can take a while. To speed up the process, you can [tune for indexing speed](https://www.elastic.co/guide/en/elasticsearch/reference/current/tune-for-indexing-speed.html#tune-for-indexing-speed):

   - You can temporarily disable [`refresh`](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-refresh.html), the operation responsible for making changes to an index available to search.

   - You can set the number of replicas to 0. This setting controls the number of copies each primary shard of an index will have. Thus, having 0 replicas effectively disables the replication of shards across nodes, which should increase the indexing performance. This is an important trade-off in terms of reliability and query performance. It is important to remember to set the replicas to a considered value after the initial indexing is complete.

   In our experience, you can expect a 20% decrease in indexing time. After completing indexing in a later step, you can return `refresh` and `number_of_replicas` to their desired settings.

   NOTE:
   This step is optional but may help significantly speed up large indexing operations.

   ```shell
   curl --request PUT localhost:9200/gitlab-production/_settings --header 'Content-Type: application/json' \
        --data '{
          "index" : {
              "refresh_interval" : "-1",
              "number_of_replicas" : 0
          } }'
   ```

1. Index projects and their associated data:

   ```shell
   # Omnibus installations
   sudo gitlab-rake gitlab:elastic:index_projects

   # Installations from source
   bundle exec rake gitlab:elastic:index_projects RAILS_ENV=production
   ```

   This enqueues a Sidekiq job for each project that needs to be indexed.
   You can view the jobs in **Main menu > Admin > Monitoring > Background Jobs > Queues Tab**
   and select `elastic_commit_indexer`, or you can query indexing status using a Rake task:

   ```shell
   # Omnibus installations
   sudo gitlab-rake gitlab:elastic:index_projects_status

   # Installations from source
   bundle exec rake gitlab:elastic:index_projects_status RAILS_ENV=production

   Indexing is 65.55% complete (6555/10000 projects)
   ```

   If you want to limit the index to a range of projects you can provide the
   `ID_FROM` and `ID_TO` parameters:

   ```shell
   # Omnibus installations
   sudo gitlab-rake gitlab:elastic:index_projects ID_FROM=1001 ID_TO=2000

   # Installations from source
   bundle exec rake gitlab:elastic:index_projects ID_FROM=1001 ID_TO=2000 RAILS_ENV=production
   ```

   Where `ID_FROM` and `ID_TO` are project IDs. Both parameters are optional.
   The above example will index all projects from ID `1001` up to (and including) ID `2000`.

   NOTE:
   Sometimes the project indexing jobs queued by `gitlab:elastic:index_projects`
   can get interrupted. This may happen for many reasons, but it's always safe
   to run the indexing task again. It will skip repositories that have
   already been indexed.

   As the indexer stores the last commit SHA of every indexed repository in the
   database, you can run the indexer with the special parameter `UPDATE_INDEX` and
   it will check every project repository again to make sure that every commit in
   a repository is indexed, which can be useful in case if your index is outdated:

   ```shell
   # Omnibus installations
   sudo gitlab-rake gitlab:elastic:index_projects UPDATE_INDEX=true ID_TO=1000

   # Installations from source
   bundle exec rake gitlab:elastic:index_projects UPDATE_INDEX=true ID_TO=1000 RAILS_ENV=production
   ```

   You can also use the `gitlab:elastic:clear_index_status` Rake task to force the
   indexer to "forget" all progress, so it retries the indexing process from the
   start.

1. Personal snippets are not associated with a project and need to be indexed separately:

   ```shell
   # Omnibus installations
   sudo gitlab-rake gitlab:elastic:index_snippets

   # Installations from source
   bundle exec rake gitlab:elastic:index_snippets RAILS_ENV=production
   ```

1. Enable replication and refreshing again after indexing (only if you previously disabled it):

   ```shell
   curl --request PUT localhost:9200/gitlab-production/_settings --header 'Content-Type: application/json' \
        --data '{
          "index" : {
              "number_of_replicas" : 1,
              "refresh_interval" : "1s"
          } }'
   ```

   A force merge should be called after enabling the refreshing above.

   For Elasticsearch 6.x, the index should be in read-only mode before proceeding with the force merge:

   ```shell
   curl --request PUT localhost:9200/gitlab-production/_settings --header 'Content-Type: application/json' \
        --data '{
          "settings": {
            "index.blocks.write": true
          } }'
   ```

   Then, initiate the force merge:

   ```shell
   curl --request POST 'localhost:9200/gitlab-production/_forcemerge?max_num_segments=5'
   ```

   After this, if your index is in read-only mode, switch back to read-write:

   ```shell
   curl --request PUT localhost:9200/gitlab-production/_settings --header 'Content-Type: application/json' \
        --data '{
          "settings": {
            "index.blocks.write": false
          } }'
   ```

1. After the indexing has completed, enable [**Search with Elasticsearch enabled**](#enable-advanced-search).

### Deleted documents

Whenever a change or deletion is made to an indexed GitLab object (a merge request description is changed, a file is deleted from the default branch in a repository, a project is deleted, etc), a document in the index is deleted. However, since these are "soft" deletes, the overall number of "deleted documents", and therefore wasted space, increases. Elasticsearch does intelligent merging of segments in order to remove these deleted documents. However, depending on the amount and type of activity in your GitLab installation, it's possible to see as much as 50% wasted space in the index.

In general, we recommend letting Elasticsearch merge and reclaim space automatically, with the default settings. From [Lucene's Handling of Deleted Documents](https://www.elastic.co/blog/lucenes-handling-of-deleted-documents "Lucene's Handling of Deleted Documents"), _"Overall, besides perhaps decreasing the maximum segment size, it is best to leave Lucene's defaults as-is and not fret too much about when deletes are reclaimed."_

However, some larger installations may wish to tune the merge policy settings:

- Consider reducing the `index.merge.policy.max_merged_segment` size from the default 5 GB to maybe 2 GB or 3 GB. Merging only happens when a segment has at least 50% deletions. Smaller segment sizes will allow merging to happen more frequently.

  ```shell
  curl --request PUT localhost:9200/gitlab-production/_settings ---header 'Content-Type: application/json' \
       --data '{
         "index" : {
           "merge.policy.max_merged_segment": "2gb"
         }
       }'
  ```

- You can also adjust `index.merge.policy.reclaim_deletes_weight`, which controls how aggressively deletions are targeted. But this can lead to costly merge decisions, so we recommend not changing this unless you understand the tradeoffs.

  ```shell
  curl --request PUT localhost:9200/gitlab-production/_settings ---header 'Content-Type: application/json' \
       --data '{
         "index" : {
           "merge.policy.reclaim_deletes_weight": "3.0"
         }
       }'
  ```

- Do not do a [force merge](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-forcemerge.html "Force Merge") to remove deleted documents. A warning in the [documentation](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-forcemerge.html "Force Merge") states that this can lead to very large segments that may never get reclaimed, and can also cause significant performance or availability issues.

## Index large instances with dedicated Sidekiq nodes or processes

Indexing a large instance can be a lengthy and resource-intensive process that has the potential
of overwhelming Sidekiq nodes and processes. This negatively affects the GitLab performance and
availability.

As GitLab allows you to start multiple Sidekiq processes, you can create an
additional process dedicated to indexing a set of queues (or queue group). This way, you can
ensure that indexing queues always have a dedicated worker, while the rest of the queues have
another dedicated worker to avoid contention.

For this purpose, use the [queue selector](../../administration/sidekiq/extra_sidekiq_processes.md#queue-selector)
option that allows a more general selection of queue groups using a [worker matching query](../../administration/sidekiq/extra_sidekiq_routing.md#worker-matching-query).

To handle these two queue groups, we generally recommend one of the following two options. You can either:

- [Use two queue groups on one single node](#single-node-two-processes).
- [Use two queue groups, one on each node](#two-nodes-one-process-for-each).

For the steps below, consider:

- `feature_category=global_search` as an indexing queue group with its own Sidekiq process.
- `feature_category!=global_search` as a non-indexing queue group that has its own Sidekiq process.

### Single node, two processes

To create both an indexing and a non-indexing Sidekiq process in one node:

1. On your Sidekiq node, change the `/etc/gitlab/gitlab.rb` file to:

   ```ruby
   sidekiq['enable'] = true
    sidekiq['queue_selector'] = true
    sidekiq['queue_groups'] = [
      "feature_category=global_search",
      "feature_category!=global_search"
    ]
   ```

1. Save the file and [reconfigure GitLab](../../administration/restart_gitlab.md)
for the changes to take effect.

WARNING:
When starting multiple processes, the number of processes cannot exceed the number of CPU
cores you want to dedicate to Sidekiq. Each Sidekiq process can use only one CPU core, subject
to the available workload and concurrency settings. For more details, see how to
[run multiple Sidekiq processes](../../administration/sidekiq/extra_sidekiq_processes.md).

### Two nodes, one process for each

To handle these queue groups on two nodes:

1. To set up the indexing Sidekiq process, on your indexing Sidekiq node, change the `/etc/gitlab/gitlab.rb` file to:

    ```ruby
    sidekiq['enable'] = true
     sidekiq['queue_selector'] = true
     sidekiq['queue_groups'] = [
       "feature_category=global_search"
     ]
    ```

1. Save the file and [reconfigure GitLab](../../administration/restart_gitlab.md)
for the changes to take effect.

1. To set up the non-indexing Sidekiq process, on your non-indexing Sidekiq node, change the `/etc/gitlab/gitlab.rb` file to:

    ```ruby
    sidekiq['enable'] = true
     sidekiq['queue_selector'] = true
     sidekiq['queue_groups'] = [
       "feature_category!=global_search"
     ]
    ```

    to set up a non-indexing Sidekiq process.

1. Save the file and [reconfigure GitLab](../../administration/restart_gitlab.md)
for the changes to take effect.

## Reverting to Basic Search

Sometimes there may be issues with your Elasticsearch index data and as such
GitLab allows you to revert to "basic search" when there are no search
results and assuming that basic search is supported in that scope. This "basic
search" behaves as though you don't have Advanced Search enabled at all for
your instance and search using other data sources (such as PostgreSQL data and Git
data).

## Data recovery: Elasticsearch is a secondary data store only

The use of Elasticsearch in GitLab is only ever as a secondary data store.
This means that all of the data stored in Elasticsearch can always be derived
again from other data sources, specifically PostgreSQL and Gitaly. Therefore, if
the Elasticsearch data store is ever corrupted for whatever reason, you can reindex everything from scratch.
-->
