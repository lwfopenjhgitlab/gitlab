---
stage: Manage
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Jira **(FREE)**

如果您的组织使用 [Jira](https://www.atlassian.com/software/jira) 议题，您可以[从 Jira 迁移您的议题](../../user/project/import/jira.md)，并专门在极狐GitLab 工作。但是，如果您想继续使用 Jira，可以将它与极狐GitLab 集成。极狐GitLab 提供两种类型的 Jira 集成，您可以根据需要的功能使用一种或两种。我们建议您同时启用两者。

## 对比集成

在设置其中一个或两个集成后，您可以将极狐GitLab 项目中的活动与 Jira 中的任何项目进行交叉引用。

<a id="jira-integration"></a>

### Jira 集成

此集成将一个或多个极狐GitLab 项目连接到一个 Jira 实例。您可以自己管理 Jira 实例，或在 [Atlassian Cloud](https://www.atlassian.com/migration/assess/why-cloud) 中托管 Jira 实例。
支持的 Jira 版本为 6.x、7.x、8.x 和 9.x。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see [Agile Management - GitLab-Jira Basic Integration](https://www.youtube.com/watch?v=fWvwkx5_00E&feature=youtu.be).
-->

要设置集成，请在极狐GitLab 中[配置设置](configure.md)。

### Jira 开发面板集成

[Jira 开发面板集成](development_panel.md)连接群组或个人命名空间下的所有极狐GitLab 项目。配置后，相关的极狐GitLab 信息，包括相关的分支、提交和合并请求，显示在[开发面板](https://support.atlassian.com/jira-software-cloud/docs/view-development-information-for-an-issue/)上。

要设置 Jira 开发面板集成，请使用 GitLab for Jira Cloud 应用程序或 Jira DVCS（分布式版本控制系统）连接器，[具体取决于您的实例安装方法](development_panel.md#configure-the-panel)。

### 直接功能比较

| 功能 | Jira 集成 | Jira 开发面板集成 |
|-|-|-|
| 在极狐GitLab 提交或合并请求中提及 Jira 议题 ID，并创建指向 Jira 议题的链接。 | 支持。 | 不支持。 |
| 在极狐GitLab 中提及 Jira 议题 ID，Jira 议题显示极狐GitLab 议题或合并请求。 | 支持。带有极狐GitLab 议题或 MR 标题的 Jira 评论关联到极狐GitLab。第一个提及也被添加到 **Web 链接**下的 Jira 议题中。 | 支持，显示在议题的[开发面板](https://support.atlassian.com/jira-software-cloud/docs/view-development-information-for-an-issue/)中。 |
| 在极狐GitLab 提交消息中提及 Jira 议题 ID，Jira 议题会显示提交消息。 | 支持。整个提交消息作为评论显示在 Jira 议题中，并显示在 **Web 链接** 下。每条消息都链接到极狐GitLab 中的提交。 | 支持，显示在议题的[开发面板](https://support.atlassian.com/jira-software-cloud/docs/view-development-information-for-an-issue/)中，并可选择使用 Jira [Smart 提交](https://confluence.atlassian.com/fisheye/using-smart-commits-960155400.html)对 Jira 议题进行自定义评论。 |
| 在极狐GitLab 分支名称中提及 Jira 议题 ID，Jira 议题会显示分支名称。 | 不支持。 | 支持，显示在议题的[开发面板](https://support.atlassian.com/jira-software-cloud/docs/view-development-information-for-an-issue/)中。 |
| 将 Jira 时间跟踪添加到议题。 | 不支持。 | 支持。可以使用 Jira Smart Commits 指定时间。 |
| 使用 Git 提交或合并请求来转换或关闭 Jira 议题。 | 支持。只有一种转换类型，通过将其设置为完成来关闭议题。 | 支持。使用 Jira Smart Commits 转换到任何状态。 |
| 显示 [Jira 议题](issues.md#view-jira-issues)的列表。 | 支持。 | 不支持。 |
| 从[漏洞或发现](../../user/application_security/vulnerabilities/index.md#create-a-jira-issue-for-a-vulnerability)创建 Jira 议题。 | 支持。 | 不支持。 |
| 从 Jira 议题创建极狐GitLab 分支。 | 不支持。 | 支持，显示在议题的[开发面板](https://support.atlassian.com/jira-software-cloud/docs/view-development-information-for-an-issue/)中。 |
| 在极狐GitLab 合并请求中提及 Jira 议题 ID，部署就会同步。 | 不支持。 | 支持，显示在议题的[开发面板](https://support.atlassian.com/jira-software-cloud/docs/view-development-information-for-an-issue/)中。 |

## Jira 的身份验证

Jira 中的身份验证方法取决于您将 Jira 托管在自己的服务器上还是 [Atlassian Cloud](https://www.atlassian.com/migration/assess/why-cloud)：

- **Jira Server** 支持基本身份验证。连接时，需要**用户名和密码**。无法使用中央身份验证服务 (CAS) 连接到 Jira Server。有关详细信息，请阅读如何[在 Jira Server 中设置用户](jira_server_configuration.md)。
- **Atlassian Cloud 上的 Jira** 支持通过 API 令牌进行身份验证。连接到 Atlassian Cloud 上的 Jira 时，需要电子邮件和 API 令牌。有关详细信息，请参阅[创建 Jira Cloud API 令牌](jira_cloud_configuration.md)。

## 隐私注意事项

所有 Jira 集成都与 Jira 共享数据，以使其在极狐GitLab 之外可见。
如果您将私有极狐GitLab 项目与 Jira 集成，将与有权访问您的 Jira 项目的用户共享私有数据。

[**Jira 项目集成**](#jira-integration)在 Jira 议题中以评论的形式发布极狐GitLab 数据。GitLab for Jira Cloud 应用程序和 Jira DVCS 连接器通过 [**Jira 开发面板**](development_panel.md)共享此数据。
此方法提供了更细粒度的访问控制，因为可以将访问限制为某些用户群组或角色。

<!--
## 第三方 Jira 集成

Developers have built several third-party Jira integrations for GitLab that are
listed on the [Atlassian Marketplace](https://marketplace.atlassian.com/search?product=jira&query=gitlab).
-->
