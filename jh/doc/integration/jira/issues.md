---
stage: Manage
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Jira 议题管理 **(FREE)**

要将议题管理与 Jira 集成，请在极狐GitLab 中[配置 Jira](index.md#jira-integration)，并[启用集成](configure.md)。
配置并启用集成后，您可以通过在极狐GitLab 提交和合并请求中提及 Jira ID，来引用和关闭 Jira 议题。

Jira 集成要求您将任何 Jira 议题 ID 统一为大写。

## 引用 Jira 议题

通过此集成，您可以在处理极狐GitLab 议题和合并请求时交叉引用 Jira 议题。在极狐GitLab 议题、合并请求或评论中提及 Jira 议题，极狐GitLab 会向 Jira 议题添加格式化评论。评论链接到您在极狐GitLab 中的工作。

例如，以下提交引用了 Jira 议题 `GIT-1`：

```shell
git commit -m "GIT-1 this is a test commit"
```

极狐GitLab 添加了对 Jira 议题 `GIT-1` 的**议题关联**部分的引用：

![Example of mentioning or closing the Jira issue](img/jira_issue_reference.png)

极狐 GitLab 还对该议题添加了评论，并使用以下格式：

```plaintext
USER mentioned this issue in RESOURCE_NAME of [PROJECT_NAME|COMMENTLINK]:
ENTITY_TITLE
```

- `USER`：提及议题的用户的姓名，链接到他们的极狐GitLab 用户个人资料。
- `COMMENTLINK`：指向提及的 Jira 议题的链接。
- `RESOURCE_NAME`：引用议题的资源类型，例如提交或合并请求。
- `PROJECT_NAME`：极狐GitLab 项目名称。
- `ENTITY_TITLE`：合并请求的标题，或提交的第一行。

您可以对议题[禁用评论](#disable-comments-on-jira-issues)。

### 要求关联 Jira 议题才能合并合并请求 **(ULTIMATE)**

> - 引入于 13.12 版本，[功能标志](../../administration/feature_flags.md)为 `jira_issue_association_on_merge_request`。默认禁用。
> - 一般可用于 14.2 版本。功能标志 `jira_issue_association_on_merge_request` 已删除。

通过此集成，如果合并请求不涉及 Jira 议题，您可以防止合并请求被合并。
要启用此功能：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 合并请求**。
1. 在 **合并检查** 部分，选择 **需要来自 Jira 的相关议题**。
1. 选择 **保存**。

启用此功能后，无法合并未引用关联 Jira 议题的合并请求。合并请求显示消息 **要合并，必须在标题或描述中提及 Jira issue key。**

## 在极狐GitLab 中关闭 Jira 议题

如果您已配置极狐GitLab 转换 ID，则可以直接从极狐GitLab 关闭 Jira 议题。在提交或合并请求中使用触发词后跟 Jira 议题 ID。
当您推送包含触发词和 Jira 议题 ID 的提交时，极狐GitLab：

1. 评论提及的 Jira 议题。
1. 关闭 Jira 议题。如果 Jira 议题有解决方案，则不会转换。

例如，使用以下触发词中的任何一个来关闭 Jira 议题 `PROJECT-1`：

- `Resolves PROJECT-1`
- `Closes PROJECT-1`
- `Fixes PROJECT-1`

提交或合并请求必须针对项目的[默认分支](../../user/project/repository/branches/default.md)。
您可以在[项目设置](img/jira_project_settings.png)下更改项目的默认分支。

### 关闭议题的用例

考虑以下示例：

1. 用户创建 Jira 议题 `PROJECT-7` 请求开发新功能。
1. 您在极狐GitLab 中创建合并请求，构建此功能。
1. 在合并请求中，添加议题关闭触发词 `Closes PROJECT-7`。
1. 合并请求合并时：
   - 极狐GitLab 为您关闭 Jira 议题：
     ![The GitLab integration closes Jira issue](img/jira_service_close_issue.png)
   - 极狐GitLab 向 Jira 添加格式化评论，链接到解决议题的提交。您可以[禁用评论](#disable-comments-on-jira-issues)。

<a id="view-jira-issues"></a>

## 查看 Jira 议题 **(PREMIUM)**

> 引入于 13.2 版本。

如果您的极狐GitLab 管理员[已配置](configure.md)，您可以直接在极狐GitLab 中浏览、搜索和查看所选 Jira 项目的议题。

为此，在极狐GitLab 中，转到您的项目并选择 **议题 > Jira 议题**。默认情况下，议题列表按**创建日期**排序，最新的议题列在顶部：

![Jira issues integration enabled](img/open_jira_issues_list_v14_6.png)

- 要首先显示最近更新的议题，请选择 **更新日期**。
- 您可以[搜索和过滤](#search-and-filter-the-issues-list)议题列表。
- 在 13.10 及更高版本中，您可以从列表中选择一个议题，在极狐GitLab 中查看它：
  ![Jira issue detail view](img/jira_issue_detail_view_v13.10.png)

议题根据其 [Jira 状态](https://confluence.atlassian.com/adminjiraserver070/defining-status-field-values-749382903.html)分组到选项卡中：

- **开放中**：Jira 状态为除 `Done` 以外的所有议题。
- **已关闭**：Jira 状态为 `Done` 的所有议题。
- **全部**：任何状态的所有议题。

<a id="search-and-filter-the-issues-list"></a>

### 搜索和过滤议题列表 **(PREMIUM)**

要优化议题列表，请使用搜索栏搜索议题摘要（标题）或描述中包含的任何文本。使用以下过滤器的任意组合：

- 要按 `labels` 过滤议题，请将一个或多个标签指定为 URL 中 `labels[]` 参数的一部分。使用多个标签时，仅列出包含所有指定标签的议题：`/-/integrations/jira/issues?labels[]=backend&labels[]=feature&labels[]=QA`
- 要按 `status` 过滤议题，请在 URL 中指定 `status` 参数：`/-/integrations/jira/issues?status=In Progress`
- 要按 `reporter` 过滤议题，请在 URL 中指定 `author_username` 参数，指定报告者的 Jira 显示名称： `/-/integrations/jira/issues?author_username=John Smith`
- 要按 `assignee` 过滤议题，请在 URL 中指定 `assignee_username` 参数，指定指派人的 Jira 显示名称：`/-/integrations/jira/issues?assignee_username=John Smith`

<!--
Enhancements to use these filters through the user interface
[are planned](https://gitlab.com/groups/gitlab-org/-/epics/3622).
-->

## 为漏洞创建 Jira 议题 **(ULTIMATE)**

您可以从[漏洞页面](../../user/application_security/vulnerabilities/index.md#create-a-jira-issue-for-a-vulnerability)，为漏洞创建 Jira 议题。

## 自动议题转换

> 引入于 13.11 版本。

配置自动议题转换时，您可以将引用的 Jira 议题转换为类别为 **完成** 的下一个可用状态。要配置此设置：

1. 参考[配置极狐GitLab](configure.md) 说明。
1. 选中 **启用 Jira 转换** 复选框。
1. 选择 **移至完成** 选项。

## 自定义议题转换

对于高级工作流程，您可以指定自定义 Jira 转换 ID：

1. 使用基于您的 Jira 订阅状态的方法：
   - *（对于 Jira Cloud 用户）* 通过在 **文本** 视图中编辑工作流来获取您的转换 ID。转换 ID 显示在 **转换** 列中。
   - *（对于 Jira Server 用户）* 通过以下方式之一获取您的转换 ID：
     - 通过使用 API，类似 `https://yourcompany.atlassian.net/rest/api/2/issue/ISSUE-123/transitions` 的请求，使用处于 "open" 状态的议题。
     - 将鼠标悬停在所需转换的链接上，然后在 URL 中查找 **操作** 参数。
   转换 ID 可能因工作流而异（例如，bug 而非 story），即使您要更改的状态相同。
1. 参考[配置极狐GitLab](configure.md) 说明。
1. 选择 **启用 Jira 转换** 设置。
1. 选择 **自定义转场** 选项。
1. 在文本字段中输入您的转换 ID。如果您插入多个转换 ID（由 `,` 或 `;` 分隔），议题将按照您指定的顺序一个接一个地移动到每个状态。如果转换失败，序列将中止。

<a id="disable-comments-on-jira-issues"></a>

## 禁用对 Jira 议题的评论

极狐GitLab 可以交叉关联源提交或将请求与 Jira 议题合并，而无需向 Jira 议题添加评论：

1. 参考[配置极狐GitLab](configure.md) 说明。
1. 清除 **启用评论** 复选框。
