---
stage: Manage
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 创建 Jira Cloud API 令牌 **(FREE)**

您需要一个 API 令牌才能在 Atlassian Cloud 中[与 Jira 集成](index.md)。 要创建 API 令牌：

1. 使用对 Jira 项目具有*写入*权限的帐户登录 [Atlassian](https://id.atlassian.com/manage-profile/security/api-tokens)。

    该链接会打开 API 令牌页面。或者，要从您的 Atlassian 配置文件转到此页面，请选择 **帐户设置 > 安全 > 创建和管理 API 令牌**。

1. 选择 **创建 API 令牌**。
1. 在对话框中，为您的令牌输入标签并选择 **创建**。
1. 要复制 API 令牌，请选择 **复制**，然后将令牌粘贴到安全的地方。

[配置极狐GitLab](configure.md) 时，您需要新创建的令牌，以及创建它时使用的电子邮件地址。
