---
stage: Manage
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Jira 开发面板 **(FREE)**

> 从专业版移动到免费版于 13.4 版本。

您可以从 Jira 开发面板查看极狐GitLab 活动。

当您在极狐GitLab 中时，您可以通过 ID 引用 Jira 议题。[引用议题的活动](https://support.atlassian.com/jira-software-cloud/docs/view-development-information-for-an-issue/)显示在 Jira 开发面板中。

在 Jira 开发面板中，您可以从分支创建极狐GitLab 合并请求。
您还可以在 GitLab for Jira Cloud 应用程序中从 Jira 议题创建极狐GitLab 分支（引入于 14.2 版本）。

## 极狐GitLab 中的关联项目

Jira 开发面板连接到整个 Jira 实例，所有极狐GitLab 项目位于：

- 顶级群组，包括其子组中的所有项目。
- 个人命名空间。

这些极狐GitLab 项目可以与该实例中的所有 Jira 项目交互。

## 面板显示的信息

Jira 开发面板中显示的信息取决于您在极狐GitLab 中提到 Jira 议题 ID 的位置。

| 极狐GitLab 中您提及 Jira 议题 ID 的地方    | Jira 开发面板中显示的信息 |
|------------------------------------------------|-------------------------------------------------------|
| 合并请求标题或描述             | 合并请求的链接                             |
| 分支名称                                    | 分支的链接                                    |
| 提交消息                                 | 提交的链接                                    |
| [Jira Smart Commit](#jira-smart-commits)       | 自定义评论、记录时间或工作流转换   |

## Jira Smart Commits

Jira Smart Commits 是处理 Jira 议题的特殊命令。通过这些命令，您可以使用极狐GitLab：

- 向 Jira 议题添加自定义评论。
- 针对 Jira 议题记录时间。
- 将 Jira 议题转换为项目工作流中定义的任何状态。

获取更多信息，查看 [Atlassian 文档](https://confluence.atlassian.com/fisheye/using-smart-commits-960155400.html)。

<a id="configure-the-panel"></a>

## 配置面板

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see [Jira development panel integration](https://www.youtube.com/watch?v=VjVTOmMl85M).
-->

### SaaS

先决条件：

- 您必须至少具有该群组的维护者角色。

在 SaaS 版上配置 Jira 开发面板：

- **[Jira Cloud](https://www.atlassian.com/migration/assess/why-cloud)**：
  - [从 Atlassian Marketplace 安装 GitLab for Jira Cloud 应用程序](https://marketplace.atlassian.com/apps/1221011/gitlab-for-jira-cloud?hosting=cloud&tab=overview)。
  - 此方法在极狐GitLab SaaS 和 Jira 之间实时同步数据。
  - 此方法需要用于设置的入站连接和用于将数据推送到 Jira 的出站连接。
  - 有关更多信息，请参阅 [GitLab for Jira Cloud 应用程序](connect-app.md)。
- **Jira Server**：
  - 使用 [Jira DVCS 连接器](dvcs/index.md)。
  - 此方法每小时同步一次数据，并且仅适用于入站连接。
  - 该方法尝试在极狐GitLab 中设置 webhooks 实时同步数据，需要出站连接。

### 私有化部署版

先决条件：

- 您必须具有实例的管理员访问权限。
- 您的极狐GitLab 安装不得使用[相对 URL](https://docs.gitlab.cn/omnibus/settings/configuration.html#configure-a-relative-url-for-gitlab)（例如，`https:/ /example.com/gitlab`）。

在私有化部署版上配置 Jira 开发面板：

- **[Jira Cloud](https://www.atlassian.com/migration/assess/why-cloud)**：
  - [手动安装 GitLab for Jira Cloud 应用程序](connect-app.md#install-the-gitlab-for-jira-cloud-app-manually)。
  - 此方法需要用于设置的入站连接和用于将数据推送到 Jira 的出站连接。
  - 获取更多信息，查看[为私有化部署实例连接 GitLab for Jira Cloud 应用程序](connect-app.md#connect-the-gitlab-for-jira-cloud-app-for-self-managed-instances)。
- **Jira Server**：
  - 使用 [Jira DVCS 连接器](dvcs/index.md)。
  - 此方法每小时同步一次数据，并且仅适用于入站连接。
  - 此方法尝试在极狐GitLab 中设置 webhooks 以实时同步数据，需要出站连接。

## 故障排除

要在您自己的服务器上对 Jira 开发面板进行故障排除，查看 [Atlassian 文档](https://confluence.atlassian.com/jirakb/troubleshoot-the-development-panel-in-jira-server-574685212.html)。
