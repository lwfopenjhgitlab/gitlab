---
stage: Manage
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# JiHu GitLab for Jira Cloud app **(FREE)**

您可以使用 Atlassian Marketplace 中的 [JiHu GitLab for Jira Cloud](https://marketplace.atlassian.com/apps/1231119/jihu-gitlab-for-jira-cloud?hosting=cloud&tab=overview) 应用程序集成极狐GitLab 和 Jira Cloud。

只有具有管理员访问权限的 Jira 用户才能安装或配置 JiHu GitLab for Jira Cloud 应用程序。

## 安装 JiHu GitLab for Jira Cloud 应用程序 **(FREE SAAS)**

如果您使用 SaaS 和 Jira Cloud，则可以安装 JiHu GitLab for Jira Cloud 应用程序。
如果您不使用这两种环境，请使用 [Jira DVCS 连接器](dvcs/index.md)或[手动安装 JiHu GitLab for Jira Cloud 应用程序](#install-the-gitlab-for-jira-cloud-app-manually)。
我们推荐 JiHu GitLab for Jira Cloud 应用程序，因为数据是实时同步的。DVCS 连接器每小时仅更新一次数据。

要为 Jira Cloud 应用程序配置极狐GitLab，您必须至少在极狐GitLab SaaS 命名空间中具有维护者角色。

此集成方式支持 [Smart Commits](dvcs/index.md#smart-commits)。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see
[Configure the GitLab for Jira Cloud app from the Atlassian Marketplace](https://youtu.be/SwR-g1s1zTo).
-->

安装 JiHu GitLab for Jira Cloud 应用程序：

1. 在 Jira 中，转到 **Jira 设置 > Apps > 查找新应用**，然后搜索 JiHu GitLab。
1. 选择 **JiHu GitLab for Jira Cloud**，然后选择 **Get it now**，或者直接进入[市场上的 App](https://marketplace.atlassian.com/apps/1231119/jihu-gitlab-for-jira-cloud)。

   ![Install GitLab.com app on Jira Cloud](img/jira_dev_panel_setup_com_1.png)

1. 安装后，转到配置页面，选择 **开始**。此页面始终在 **Jira 设置 > 应用 > 管理应用** 下可用。

   ![Start GitLab.com app configuration on Jira Cloud](img/jira_dev_panel_setup_com_2.png)

1. 要添加命名空间，请确保您以至少具有维护者角色的用户身份登录 SaaS。

   ![Sign in to GitLab.com in GitLab for Jira Cloud app](img/jira_dev_panel_setup_com_3_v13_9.png)

1. 要打开可用命名空间列表，请选择 **添加命名空间**。

1. 确定要关联的命名空间，然后选择 **关联**。
   - 您必须至少具有命名空间的维护者角色。
   - 只有 Jira 站点管理员可以为安装实例添加或删除命名空间。

   ![Link namespace in GitLab for Jira Cloud app](img/jira_dev_panel_setup_com_4_v13_9.png)

NOTE:
SaaS 用户仅在添加新命名空间时需要访问权限。为了与 Jira 同步，我们不依赖于用户的令牌。

添加命名空间后：

- 该命名空间下所有项目的所有未来提交、分支和合并请求都同步到 Jira。
- 从 13.8 版本开始，过去的合并请求数据同步到 Jira。

<!--
Support for syncing past branch and commit data is tracked [in this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/263240).
-->

## 更新 JiHu GitLab for Jira Cloud 应用程序

该应用程序的大多数更新都是全自动的，不需要任何用户交互。有关详细信息，请参阅 [Atlassian Marketplace 文档](https://developer.atlassian.com/platform/marketplace/upgrading-and-versioning-cloud-apps/)。

如果应用程序需要额外的权限，[必须首先在 Jira 中手动批准更新](https://developer.atlassian.com/platform/marketplace/upgrading-and-versioning-cloud-apps/#changes-that-require-manual-customer-approval)。

<a id="set-up-oauth-authentication"></a>

## 设置 OAuth 身份验证

JiHu GitLab for Jira Cloud 应用程序正在切换到 OAuth 身份验证。
要启用 OAuth 身份验证，您必须在极狐GitLab 实例上创建一个 OAuth 应用程序。

启用 OAuth 身份验证：

- 要求为私有化部署实例[连接 GitLab for Jira Cloud 应用程序](#connect-the-gitlab-for-jira-cloud-app-for-self-managed-instances)。
- 建议[手动安装 JiHu GitLab for Jira Cloud app](#install-the-gitlab-for-jira-cloud-app-manually)。

要创建 OAuth 应用程序：

1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **应用程序** (`/admin/applications`)。
1. 选择 **新建应用程序**。
1. 在 **重定向 URI** 中：
   - 如果您从官方市场列表安装应用程序，请输入 `https://jihulab.com/-/jira_connect/oauth_callbacks`。
   - 如果您手动安装应用程序，请输入 `<instance_url>/-/jira_connect/oauth_callbacks` 并将 `<instance_url>` 替换为您的实例的 URL。
1. 清除 **受信任** 和 **凭据** 复选框。
1. 在 **范围** 中，仅选中 `api` 复选框。
1. 选择 **保存应用**。
1. 复制 **应用程序 ID** 值。
1. 在左侧边栏中，选择 **设置 > 通用** (`/admin/application_settings/general`)。
1. 展开 **GitLab for Jira App** 部分。
1. 将 **应用程序 ID** 值粘贴到 **Jira Connect Application ID**。
1. 选择 **保存更改**。
1. 可选。启用 `jira_connect_oauth` [功能标志](../../administration/feature_flags.md)，避免[某些浏览器中的身份验证问题](#browser-displays-a-sign-in-message-when-already-signed-in)。

<a id="connect-the-gitlab-for-jira-cloud-app-for-self-managed-instances"></a>

## 为私有化部署实例连接 JiHu GitLab for Jira Cloud 应用程序 **(FREE SELF)**

> 引入于 15.7 版本。

先决条件：

- SaaS 必须作为实例的代理。
- 该实例必须公开可用。
- 该实例的版本必须为 15.7 或更高版本。

从市场安装 JiHu GitLab for Jira Cloud 应用程序后，您可以关联私有化部署实例。
Jira 应用程序只能关联到每个市场列表的一个 URL。<!--官方列表链接到 GitLab.com。-->

如果您的实例不满足先决条件或者您不想使用官方市场列表，您可以[手动安装应用程序](#install-the-gitlab-for-jira-cloud-app-manually)。

无法从 Jira 为私有化部署实例创建分支。<!--For more information, see [issue 391432](https://gitlab.com/gitlab-org/gitlab/-/issues/391432).-->

### 设置您的实例

要在 15.7 及更高版本中为 JiHu GitLab for Jira Cloud 应用程序设置您的私有化部署实例：

1. [设置 OAuth 认证](#set-up-oauth-authentication)。
1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 通用** (`/admin/application_settings/general`)。
1. 展开 **GitLab for Jira App** 部分。
1. 在 **Jira Connect Proxy URL** 中，输入 `https://jihulab.com`。
1. 选择 **保存更改**。

### 关联您的实例

要将您的私有化部署实例链接到 JiHu GitLab for Jira Cloud 应用程序：

1. 安装 [GitLab for Jira Cloud 应用程序](https://marketplace.atlassian.com/apps/1231119/jihu-gitlab-for-jira-cloud?tab=overview&hosting=cloud)。
1. 选择 **GitLab（私有化部署）**。
1. 输入您的极狐GitLab 实例 URL。
1. 选择 **保存**。

<a id="install-the-gitlab-for-jira-cloud-app-manually"></a>

## 手动安装 JiHu GitLab for Jira Cloud 应用程序 **(FREE SELF)**

如果您的极狐GitLab 实例是私有化部署实例并且您不想使用官方市场列表，则可以手动安装该应用程序。

先决条件：

- 该实例必须是公开可用的。
- 您必须设置 [OAuth 认证](#set-up-oauth-authentication)。

### 设置您的 Jira app

每个 Jira Cloud 应用程序都必须从一个位置安装。Jira 从您提供的位置获取一个 [manifest 文件](https://developer.atlassian.com/cloud/jira/platform/connect-app-descriptor/)。Manifest 文件描述了系统的应用程序。要使用 Jira Cloud 支持私有化部署实例，您可以执行以下操作之一：

- [在开发模式下安装应用程序](#install-the-application-in-development-mode)。
- [创建 Marketplace 列表](#create-a-marketplace-listing)。

<a id="install-the-application-in-development-mode"></a>

#### 在开发模式下安装应用程序

您可以配置您的 Atlassian Cloud 实例，允许您从 Marketplace 外部安装应用程序，允许您安装应用程序：

1. 以管理员身份登录您的 Jira 实例。
1. 将您的 Jira 实例置于[开发模式](https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/#step-2--enable-development-mode)。
1. 以具有管理员访问权限的用户身份登录您的极狐GitLab 应用程序。
1. 从您的 Jira 实例安装极狐GitLab 应用程序，参考 [Atlassian 开发人员指南](https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/#step-3--install-and-test-your-app)：
   1. 在您的 Jira 实例中，转到 **应用 > 管理应用** 并选择 **上传应用**：

      ![Button labeled "upload app"](img/jira-upload-app_v13_11.png)

   1. 对于 **应用描述符 URL**，根据您的实例配置，提供 manifest 文件的完整 URL。默认情况下，您的 manifest 文件位于 `/-/jira_connect/app_descriptor.json`。例如，您的极狐GitLab 私有化部署实例域名是 `app.pet-store.cloud`，您的清单文件位于 `https://app.pet-store.cloud/-/jira_connect/app_descriptor.json`。
   1. 选择 **上传**。Jira 获取 ·app_descriptor· 文件的内容并安装它。
   1. 如果上传成功，Jira 会显示一个窗口：**已安装并准备就绪！**要配置集成，请选择 **开始**。

      ![Success modal](img/jira-upload-app-success_v13_11.png)

1. 在 Jira 实例上禁用[开发模式](https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/#step-2--enable-development-mode)。

**JiHu GitLab for Jira Cloud** 应用程序现在显示在 **管理应用程序** 下。您还可以选择 **开始使用**，打开极狐GitLab 实例中的配置页面。

NOTE:
如果极狐GitLab 更新对应用程序描述符进行了更改，您必须卸载，然后重新安装该应用程序。

<a id="install-the-application-in-development-mode"></a>

#### 创建 Marketplace 列表

如果您不想在您的 Jira 实例上使用开发模式，您可以为您的实例创建您自己的 Marketplace 列表。这使您的应用程序可以从 Atlassian Marketplace 安装。

有关完整说明，请查看 Atlassian [指南创建 Marketplace 列表](https://developer.atlassian.com/platform/marketplace/installing-cloud-apps/#creating-the-marketplace-listing)。
要创建 Marketplace 列表：

1. 注册为 Marketplace 供应商。
1. 使用应用程序描述符 URL 列出您的应用程序。
    - 您的 manifest 文件位于：`https://your.domain/your-path/-/jira_connect/app_descriptor.json`
    - 我们建议您将您的应用程序列为 `private`，因为任何用户都可以查看和安装公开应用程序。
1. 为您的应用程序生成测试许可证令牌。

<!--
NOTE:
This method uses [automated updates](#update-the-gitlab-for-jira-cloud-app)
the same way as our GitLab.com Marketplace listing.
-->

## 配置 JiHu GitLab for Jira Cloud 作为您的极狐GitLab 应用程序的代理

使用 GitLab for Jira Cloud 应用程序，极狐GitLab 实例可以充当其他 GitLab 实例的代理。
如果您正在管理多个极狐GitLab 实例但只想[手动安装](#install-the-gitlab-for-jira-cloud-app-manually) JiHu GitLab for Jira 应用程序一次，这将很有用。

要将极狐GitLab 实例配置为充当代理：

1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 通用** (`/admin/application_settings/general`)。
1. 展开 **GitLab for Jira App** 部分。
1. 选择 **启用公钥存储**。
1. 选择 **保存更改**。
1. [手动安装 JiHu GitLab for Jira Cloud 应用程序](#install-the-gitlab-for-jira-cloud-app-manually)。

使用代理的其他 GitLab 实例必须配置 **Jira 连接代理 URL** 设置和 [OAuth application](#set-up-oauth-authentication) **重定向 URI** 以指向代理实例。

## 故障排除

<a id="browser-displays-a-sign-in-message-when-already-signed-in"></a>

### 浏览器在已登录时显示登录消息

当您已经登录时，您可能会收到以下消息，提示您登录 JiHuLab.com：

```plaintext
You need to sign in or sign up before continuing.
```

GitLab for Jira Cloud 应用程序使用 iframe 在设置页面上添加命名空间。某些浏览器会阻止跨站点 cookie，这可能会导致此问题。

要解决此问题，请设置 [OAuth 身份验证](#set-up-oauth-authentication)，并启用 `jira_connect_oauth` [功能标志](../../administration/feature_flags.md)。

### 手动安装失败

如果您从官方市场列表安装 GitLab for Jira Cloud 应用程序并将其替换为手动安装，则可能会出现错误。要解决此问题，请禁用 **Jira Connect 代理 URL** 设置。

- 在 15.7 版本中：

  1. 打开 [Rails 控制台](../../administration/operations/rails_console.md#starting-a-rails-console-session)。
  1. 执行 `ApplicationSetting.current_without_cache.update(jira_connect_proxy_url: nil)`。

- 在 15.8 及更高版本中：

  1. 在顶部栏中，选择 **主菜单 > 管理员**。
  1. 在左侧边栏中，选择 **设置 > 通用** (`/admin/application_settings/general`)。
  1. 展开 **GitLab for Jira App** 部分。
  1. 清除 **Jira Connect 代理 URL** 文本框。
  1. 选择 **保存更改**。

<!--
### Data sync fails with `Invalid JWT` error

If the GitLab for Jira Cloud app continuously fails to sync data, it may be due to an outdated secret token. Atlassian can send new secret tokens that must be processed and stored by GitLab.
If GitLab fails to store the token or misses the new token request, an `Invalid JWT` error occurs.

To resolve this issue on GitLab self-managed, follow one of the solutions below, depending on your app installation method.

- If you installed the app from the official marketplace listing:

  1. Open the GitLab for Jira Cloud app on Jira.
  1. Select **Change GitLab version**.
  1. Select **GitLab.com (SaaS)**.
  1. Select **Change GitLab version** again.
  1. Select **GitLab (self-managed)**.
  1. Enter your **GitLab instance URL**.
  1. Select **Save**.

- If you [installed the GitLab for Jira Cloud app manually](#install-the-gitlab-for-jira-cloud-app-manually):

  - In GitLab 14.9 and later:
    - Contact the [Jira Software Cloud support](https://support.atlassian.com/jira-software-cloud/) and ask to trigger a new installed lifecycle event for the GitLab for Jira Cloud app in your namespace.
  - In all GitLab versions:
    - Re-install the GitLab for Jira Cloud app. This might remove all already synced development panel data.
-->
