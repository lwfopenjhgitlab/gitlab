---
stage: Manage
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 配置 Jira **(FREE)**

您可以通过在极狐GitLab 中配置项目设置，来设置 [Jira 集成](index.md#jira-integration)。
您还可以在[群组级别](../../user/admin_area/settings/project_integration_management.md#manage-group-level-default-settings-for-a-project-integration)配置这些设置，并在[实例级别](../../user/admin_area/settings/project_integration_management.md#manage-instance-level-default-settings-for-a-project-integration)为私有化部署实例配置这些设置。

先决条件：

- 确保您的极狐GitLab 安装实例不使用[相对 URL](https://docs.gitlab.cn/omnibus/settings/configuration.html#configure-a-relative-url-for-gitlab)。
- 对于 **Jira Server**，确保您拥有 Jira 用户名和密码。
- 对于 **Jira Cloud**，确保您拥有 API 令牌和用于创建令牌的电子邮件地址。

要配置您的项目：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 集成**。
1. 选择 **Jira**。
1. 选择 **启用集成**。
1. 选择 **触发** 动作。您的选择决定了提及 Jira （在极狐GitLab 提交、合并请求或两者中）议题，是否会在 Jira 中创建一个返回极狐GitLab 的交叉链接。
1. 要在极狐GitLab 中执行 **触发** 操作时，在 Jira 议题中发表评论，请选择 **启用评论**。
1. 要在极狐GitLab 中进行关闭引用时转换 Jira 议题状态，请选择 **启用 Jira 转换**。
1. 提供 Jira 配置信息：
   - **Web URL**：您要关联到此极狐GitLab 项目的 Jira 实例 Web 界面的基本 URL，例如 `https://jira.example.com`。
   - **Jira API URL**：Jira 实例 API 的基本 URL，例如 `https://jira-api.example.com`。
     如果未设置，则默认为 **Web URL** 值。如果使用 **Jira on Atlassian cloud**，请留空。
   - **用户名或电子邮件**：
     对于 **Jira Server**，使用 `username`。对于 **Jira on Atlassian cloud**，使用 `email`。
   - **Password/API token**：
     为 **Jira Server** 使用 `password` 或为 **Jira on Atlassian cloud** 使用 `API token`。
1. 要使用户能够在极狐GitLab 项目中[查看 Jira 议题](issues.md#view-jira-issues)，请选择 **启用 Jira 议题**，并输入 Jira 项目 key。

   您只能显示特定极狐GitLab 项目中单个 Jira 项目的议题。

   WARNING:
   如果使用此设置启用 Jira 议题，则有权访问此极狐GitLab 项目的所有用户都可以查看指定 Jira 项目中的所有议题。

1. 要启用[为漏洞创建议题](../../user/application_security/vulnerabilities/index.md#create-a-jira-issue-for-a-vulnerability)，选择**启用从漏洞创建 Jira 议题**。
1. 选择 **Jira 议题类型**。如果下拉列表为空，请选择刷新 (**{retry}**) 并重试。
1. 要验证 Jira 连接是否正常工作，请选择 **测试设置**。
1. 选择 **保存更改**。

您的极狐GitLab 项目现在可以与您实例中的所有 Jira 项目进行交互，并且该项目现在显示一个 Jira 链接，可以打开 Jira 项目。

## 在极狐GitLab 中从 Jira Server 迁移到 Jira Cloud

要在极狐GitLab 中从 Jira Server 迁移到 Jira Cloud 并维护您的 Jira 集成：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 集成**。
1. 选择 **Jira**。
1. 在 **Web URL** 中，输入新的 Jira 站点 URL（例如，`https://myjirasite.atlassian.net`）。
1. 在 **用户名或电子邮件** 中，输入在您的 Jira 个人资料中注册的用户名或电子邮件。
1. [创建 API 令牌](jira_cloud_configuration.md)，并复制该值。
1. 在 **密码或 API 令牌** 中，粘贴 API 令牌值。
1. 可选。选择 **测试设置** 以检查连接是否正常。
1. 选择 **保存更改**。
