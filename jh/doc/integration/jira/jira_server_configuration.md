---
stage: Manage
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Jira Server 凭据 **(FREE)**

要[将 Jira 与极狐GitLab 集成](index.md)，您必须为您的 Jira 项目创建一个 Jira 用户帐户才能访问极狐GitLab 中的项目。
此 Jira 用户帐户必须具有对您的 Jira 项目的写入权限。要创建凭据，您必须：

1. [创建 Jira Server 用户](#create-a-jira-server-user)。
1. [创建属于用户的 Jira Server 群组](#create-a-jira-server-group)。
1. [为您的群组创建权限 scheme](#create-a-permission-scheme-for-your-group)。

<a id="create-a-jira-server-user"></a>

## 创建 Jira Server 用户

此过程创建一个名为 `gitlab` 的用户：

1. 以 Jira 管理员身份登录您的 Jira 实例。
1. 在顶部栏的右上角，选择齿轮图标，然后选择 **用户管理**。
1. 创建一个对 Jira 中的项目具有写入权限的新用户帐户（`gitlab`）。
   - **邮箱地址**：Jira 需要一个有效的邮箱地址，并发送一封验证邮件，需要设置密码。
   - **用户名**：Jira 使用电子邮件前缀创建用户名。您可以稍后更改此用户名。
   - **密码**：您必须创建一个密码，因为极狐GitLab 集成不支持 SSO，例如 SAML。要创建密码，请转到用户配置文件，查找用户名，然后设置密码。
1. 选择 **创建用户**。

创建用户后，为其创建一个群组。

<a id="create-a-jira-server-group"></a>

## 创建 Jira Server 群组

在您[创建 Jira Server 用户](#create-a-jira-server-user)之后，创建一个群组来为用户分配权限。

此过程将您创建的 `gitlab` 用户添加到名为 `gitlab-developers` 的新群组中：

1. 以 Jira 管理员身份登录您的 Jira 实例。
1. 在顶部栏的右上角，选择齿轮图标，然后选择 **用户管理**。
1. 在左侧边栏中，选择 **群组**。

   ![Jira create new user](img/jira_create_new_group.png)

1. 在 **添加群组** 部分，为群组输入一个 **名称**（例如，`gitlab-developers`），然后选择**添加群组**。
1. 要将 `gitlab` 用户添加到 `gitlab-developers` 群组，请选择 **编辑成员**。`gitlab-developers` 群组应该作为选定组列在最左边的框中。
1. 在 **添加成员到选定群组** 部分，输入 `gitlab`。
1. 选择 **添加所选用户**。`gitlab` 用户出现在 **群组成员** 部分。

   ![Jira added user to group](img/jira_added_user_to_group.png)

接下来，为您的群组创建权限 scheme。

<a id="create-a-permission-scheme-for-your-group"></a>

## 为您的群组创建权限 scheme

在 Jira 中创建群组后，通过创建权限方案为群组授予权限：

1. 以 Jira 管理员身份登录您的 Jira 实例。
1. 在顶部栏的右上角，选择齿轮图标，然后选择 **议题**。
1. 在左侧边栏中，选择 **权限 Schemes**。
1. 选择 **添加权限 Scheme**，输入 **名称** 和（可选）**描述**，然后选择 **添加**。
1. 在权限 scheme 列表中，找到您的新权限 scheme，然后选择 **权限**。
1. 在 **管理项目** 旁边，选择 **编辑**。
1. 从 **群组** 下拉列表中，选择 `gitlab-developers`，然后选择 **授予**。

   ![Jira group access](img/jira_group_access.png)

记下新的 Jira 用户名和密码，因为在[配置极狐GitLab](configure.md) 时需要它们。
