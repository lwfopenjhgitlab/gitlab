---
stage: Manage
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Jira DVCS 连接器 **(FREE)**

如果您自托管 Jira 实例，并且希望在极狐GitLab 和 Jira 之间同步信息，请使用 Jira DVCS（分布式版本控制系统）连接器。如果您使用 Jira Cloud，您应该使用 [GitLab for Jira Cloud app](../connect-app.md)，除非您特别需要 DVCS 连接器。

配置 Jira DVCS 连接器时，请确保您的极狐GitLab 和 Jira 实例可访问。

- **私有化部署版极狐GitLab**：Jira 必须可以访问您的极狐GitLab 实例。
- **Jira Server**：您的网络必须允许访问您的实例。
- **Jira Cloud**：您的实例必须可以通过互联网访问。

NOTE:
极狐GitLab 15.0 及更高版本（包括 JiHuLab.com）与 Jira Server 一起使用时，您可能会遇到 [Jira 中的会话令牌错误](https://jira.atlassian.com/browse/JSWSERVER-21389)。解决方法是确保 Jira Server 是 9.1.0 及更高版本或 8.20.11 及更高版本。

## Smart Commits

使用 DVCS 将极狐GitLab 与 Jira 连接时，您可以在您的提交消息中，使用称为 [Smart Commits](https://support.atlassian.com/jira-software-cloud/docs/process-issues-with-smart-commits/)。使用智能提交，您可以：

- 评论议题。
- 记录针对议题的时间跟踪信息。
- 将议题转换为 Jira 项目工作流中定义的任何状态。

命令必须位于提交消息的第一行。[Jira Software 文档](https://support.atlassian.com/jira-software-cloud/docs/process-issues-with-smart-commits/) 包含有关 Smart Commits 如何工作以及可用命令的更多信息。

要使 Smart Commits 可以使用，极狐GitLab 上的提交用户必须在 Jira 上有一个具有相同电子邮件地址或用户名的相应用户。

### Smart Commit 语法

Smart Commits 应遵循以下模式：

```plaintext
<ISSUE_KEY> <ignored text> #<command> <optional command parameters>
```

示例如下：

- 向 Jira 议题添加评论：`KEY-123 fixes a bug #comment Bug is fixed.`
- 记录时间跟踪：`KEY-123 #time 2w 4d 10h 52m Tracking work time.`
- 关闭议题：`KEY-123 #close Closing issue`

Smart Commit 消息不能超过一行（无回车），但您仍然可以在一次提交中执行多个操作。例如：

- 添加时间跟踪，添加评论，并转换状态到 **已关闭**：

  ```plaintext
  KEY-123 #time 2d 5h #comment Task completed ahead of schedule #close
  ```

- 添加评论，转换状态到 **进行中**，并添加时间跟踪：

  ```plaintext
  KEY-123 #comment started working on the issue #in-progress #time 12d 5h
  ```

## 为 DVCS 配置极狐GitLab 应用程序

对于单个群组中的项目，我们建议您创建一个[群组应用程序](../../oauth_provider.md#create-a-group-owned-application)。
对于跨多个群组的项目，我们建议您在极狐GitLab 中创建和使用 `jira` 用户，并且该帐户仅用于集成工作。单独的帐户可确保定期帐户维护不会影响您的集成。如果 `jira` 用户或组应用程序不可行，您可以将此集成设置为[实例范围的应用程序](../../oauth_provider.md#create-an-instance-wide-application)，或使用[用户拥有的应用程序](../../oauth_provider.md#create-a-user-owned-application)代替。

1. 导航到 [**应用程序** 部分](../../oauth_provider.md)。
1. 在 **名称** 字段中，为集成输入一个描述性名称，例如 `Jira`。
1. 在 **重定向 URI** 字段中，输入适合您的极狐GitLab 版本的 URI，将 `<gitlab.example.com>` 替换为您的极狐GitLab 实例域名：
   - *对于极狐GitLab 13.0 及更高版本*、*Jira 8.14 及更高版本*，使用[与 Jira 关联极狐GitLab 帐户](https://confluence.atlassian.com/adminjiraserver/linking-gitlab-accounts-1027142272.html)中生成的 `Redirect URL`。
   - *对于极狐GitLab 13.0 及更高版本*、*Jira Cloud*，使用 `https://<gitlab.example.com>/login/oauth/callback`。
   - *对于极狐GitLab 11.3 及更高版本*、*Jira 8.13 及更早版本*，使用 `https://<gitlab.example.com>/login/oauth/callback`。
     如果您使用 SaaS，URL 为 `https://gitlab.com/login/oauth/callback`。
   - *对于极狐GitLab 11.2 及更早版本*，使用 `https://<gitlab.example.com>/-/jira/login/oauth/callback`。

1. 对于 **Scopes**，选择 `api` 并清除任何其他复选框。
   - DVCS 连接器需要一个可写的 `api` 范围来自动创建和管理所需的 webhook。
1. 选择 **提交**。
1. 复制 **应用程序 ID** 和 **Secret** 值。您需要它们来配置 Jira。

## 为 DVCS 配置 Jira

当您想要将您指定的群组的所有极狐GitLab 提交和分支导入 Jira 时，配置此连接。此导入需要几分钟时间，完成后每 60 分钟刷新一次：

1. 完成[极狐GitLab 配置](#configure-a-gitlab-application-for-dvcs)。
1. 访问您的 DVCS 帐户：
   - *Jira Server，*选择 **设置 (gear) > 应用程序 > DVCS 帐户**。
   - *Jira Cloud，*选择 **设置 (gear) > 产品 > DVCS 帐户**。
1. 要创建新集成，请为 **Host** 选择适当的值：
   - *对于 Jira 8.14 及更高版本：*选择 **GitLab** 或 **GitLab Self-Managed**。
   - *对于 Jira Cloud 或 Jira 8.13 及更早版本：*选择 **GitHub Enterprise**。
1. 对于 **团队和用户帐户**，输入：
   - *对于 Jira 8.14 及更高版本：*
      - [极狐GitLab 用户](#configure-a-gitlab-application-for-dvcs)有权访问的顶级极狐GitLab 群组的相对路径。
   - *对于 Jira Cloud 或 Jira 8.13 及更早版本：*
      - [极狐GitLab 用户](#configure-a-gitlab-application-for-dvcs)有权访问的顶级极狐GitLab 群组的相对路径。
      - 您的个人命名空间的相对路径。

1. 在 **Host URL** 字段中，输入适合您的极狐GitLab 版本的 URI，将 `<gitlab.example.com>` 替换为您的极狐GitLab 实例域名：
   - *对于 11.3 及更高版本，*使用 `https://<gitlab.example.com>`。
   - *对于 11.2 及更早版本，*使用 `https://<gitlab.example.com>/-/jira`。

1. 对于 **客户端 ID**，使用上一节中的 **应用程序 ID** 值。
1. 对于 **客户端 Secret**，使用上一节中的 **Secret** 值。
1. 确保选中其余的复选框。
1. 要创建 DVCS 帐户，请选择 **添加**，然后选择 **继续**。
1. Jira 重定向到极狐GitLab，您必须在其中确认授权。极狐GitLab 然后重定向回 Jira，同步的项目应该显示在新帐户中。

要连接来自其他极狐GitLab 顶级群组或个人命名空间的其他极狐GitLab 项目，请使用其他 Jira DVCS 帐户重复前面的步骤。

配置集成后，请阅读有关[如何测试和使用它](../development_panel.md)的更多信息。

## 刷新导入到 Jira 的数据

Jira 每 60 分钟为您的项目导入一次提交和分支。您可以从 Jira 界面手动刷新数据：

1. 以您配置集成的用户身份登录您的 Jira 实例。
1. 转到 **设置（gear）> 应用程序**。
1. 选择 **DVCS 帐户**。
1. 在表中，对于要刷新的代码仓库，在 **上次活动** 列中，选择图标。

## 迁移到 GitLab for Jira Cloud 应用程序

如果您将 DVCS 与 Jira Cloud 一起使用，则应考虑迁移到 GitLab for Jira 应用程序。
Jira Cloud 的 DVCS 将废弃于 16.0 版本。

要开始使用 GitLab for Jira Cloud 应用程序，请遵循指南[安装 GitLab for Jira Cloud 应用程序](../connect-app.md#install-the-gitlab-for-jira-cloud-app)。

### Jira Cloud app DVCS 与 GitLab 功能对比

| 功能            | DVCS（Jira Cloud）  | GitLab for Jira Cloud app             |
|--------------------|--------------------|---------------------------------------|
| Smart Commits      | **{check-circle}** Yes | **{check-circle}** Yes |
| 同步合并请求           | **{check-circle}** Yes | **{check-circle}** Yes |
| 同步分支      | **{check-circle}** Yes | **{check-circle}** Yes |
| 同步提交       | **{check-circle}** Yes | **{check-circle}** Yes|
| 同步构建        | **{dotted-circle}** No | **{check-circle}** Yes |
| 同步部署   | **{dotted-circle}** No | **{check-circle}** Yes |
| 同步功能标志 | **{dotted-circle}** No | **{check-circle}** Yes |
| 同步时间间隔      | 60 分钟        | 实时                            |
| 创建分支    | **{dotted-circle}** No | **{check-circle}** Yes（仅 SaaS） |
| 历史数据同步 | **{check-circle}** Yes | **{dotted-circle}** No |

### 迁移的风险

GitLab for Jira Cloud 应用程序同步历史数据的能力有限。
只有在安装 GitLab for Jira Cloud 应用程序后创建的分支、提交、构建、部署和功能标志才会与 Jira 同步。
