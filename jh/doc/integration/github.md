---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 将您的极狐GitLab 实例与 GitHub 集成 **(FREE SELF)**

您可以将极狐GitLab 实例与 GitHub.com 和 GitHub Enterprise 集成。这种集成使用户能够从 GitHub 导入项目，或使用他们的 GitHub 帐户登录到您的极狐GitLab 实例。

## 安全检查

某些集成可能会危及极狐GitLab 帐户。为了帮助缓解此 [OAuth 2 隐蔽重定向](https://oauth.net/advisories/2014-1-covert-redirect/)漏洞，请将 `/users/auth` 附加到授权回调 URL 的末尾。

但是，GitHub 似乎并没有验证 `redirect_uri` 的子域部分。
这意味着您网站的任何子域上的子域接管、XSS 或开放重定向都可能启用隐蔽重定向攻击。

## 启用 GitHub OAuth

要启用 GitHub OmniAuth provider，您需要来自 GitHub 的 OAuth 2 客户端 ID 和客户端密钥。要获取这些凭据，请登录 GitHub 并按照其[创建 OAuth 应用程序](https://docs.github.com/en/developers/apps/building-oauth-apps/creating-an-oauth-app)的流程进行操作。

在 GitHub 中创建 OAuth 2 应用程序时，您需要以下信息：

- 极狐GitLab 实例的 URL，例如 `https://gitlab.example.com`。
- 授权回调地址；在此情况下为 `https://gitlab.example.com/users/auth`。如果您的极狐GitLab 实例使用非默认端口，请包含端口号。

查看[配置初始设置](omniauth.md#初始化配置)。

配置 GitHub provider 后，您需要以下信息。在接下来的步骤中，您必须在 GitLab 配置文件中替换该信息。

| 在 GitHub 设置  | 在 GitLab 配置文件中替换  | 描述 |
|:---------------------|:---------------------------------------------|:------------|
| Client ID            | `YOUR_APP_ID`                                | OAuth 2 客户端 ID |
| Client Secret        | `YOUR_APP_SECRET`                            | OAuth 2 客户端 Secret |
| URL                  | `https://github.example.com/`                | GitHub 部署 URL |

按照以下步骤将 GitHub OAuth 2 应用程序合并到您的 GitLab 服务器中：

**Omnibus 安装实例**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   对于 GitHub.com：

   ```ruby
   gitlab_rails['omniauth_providers'] = [
     {
       name: "github",
       # label: "Provider name", # optional label for login button, defaults to "GitHub"
       app_id: "YOUR_APP_ID",
       app_secret: "YOUR_APP_SECRET",
       args: { scope: "user:email" }
     }
   ]
   ```

   对于 GitHub Enterprise：

   ```ruby
   gitlab_rails['omniauth_providers'] = [
     {
       name: "github",
       # label: "Provider name", # optional label for login button, defaults to "GitHub"
       app_id: "YOUR_APP_ID",
       app_secret: "YOUR_APP_SECRET",
       url: "https://github.example.com/",
       args: { scope: "user:email" }
     }
   ]
   ```

   **用您的 GitHub URL 替换 `https://github.example.com/`。**

1. 保存文件并[重新配置](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)极狐GitLab，使更改生效。

---

**源安装实例**

1. 导航到您的仓库并编辑 `config/gitlab.yml`：

   对于 GitHub.com：

   ```yaml
   - { name: 'github',
       # label: 'Provider name', # optional label for login button, defaults to "GitHub"
       app_id: 'YOUR_APP_ID',
       app_secret: 'YOUR_APP_SECRET',
       args: { scope: 'user:email' } }
   ```

   对于 GitHub Enterprise：

   ```yaml
   - { name: 'github',
       # label: 'Provider name', # optional label for login button, defaults to "GitHub"
       app_id: 'YOUR_APP_ID',
       app_secret: 'YOUR_APP_SECRET',
       url: "https://github.example.com/",
       args: { scope: 'user:email' } }
   ```

   **用您的 GitHub URL 替换 `https://github.example.com/`。**

1. 保存文件并[重启](../administration/restart_gitlab.md#源安装实例)极狐GitLab，使更改生效。

---

1. 刷新极狐GitLab 登录页面。您现在应该会在常规登录表单下方看到一个 GitHub 图标。

1. 单击该图标以开始身份验证过程。GitHub 要求用户登录并授权 GitLab 应用程序。

## 带有自签名证书的 GitHub Enterprise

如果您尝试使用自签名证书从 GitHub Enterprise 导入项目并且导入失败，则必须禁用 SSL 验证。
您应该通过在提供者配置中将 `verify_ssl` 添加到 `false`，并在 GitLab 服务器中将全局 Git `sslVerify` 选项更改为 `false` 来禁用。

对于 Omnibus 包：

```ruby
gitlab_rails['omniauth_providers'] = [
  {
    name: "github",
    # label: "Provider name", # optional label for login button, defaults to "GitHub"
    app_id: "YOUR_APP_ID",
    app_secret: "YOUR_APP_SECRET",
    url: "https://github.example.com/",
    verify_ssl: false,
    args: { scope: "user:email" }
  }
]
```

您还必须在托管 GitLab 的服务器上禁用 Git SSL 验证。

```ruby
omnibus_gitconfig['system'] = { "http" => ["sslVerify = false"] }
```

对于源安装实例：

```yaml
- { name: 'github',
    # label: 'Provider name', # optional label for login button, defaults to "GitHub"
    app_id: 'YOUR_APP_ID',
    app_secret: 'YOUR_APP_SECRET',
    url: "https://github.example.com/",
    verify_ssl: false,
    args: { scope: 'user:email' } }
```

您还必须在托管 GitLab 的服务器上禁用 Git SSL 验证。

```shell
git config --global http.sslVerify false
```

要使更改生效，如果您通过 Omnibus 安装，[重新配置极狐GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)；或者如果您是从源安装，[重新启动极狐GitLab](../administration/restart_gitlab.md#源安装实例)。

## 故障排查

### 尝试通过 GitHub Enterprise 登录极狐GitLab 时出现 Error 500

检查 GitLab 服务器上的 `production.log`<!--[`production.log`](../administration/logs.md#productionlog)-->，获取更多详细信息。如果您在日志中收到类似 `Faraday::ConnectionFailed (execution expired)` 的错误消息，则您的极狐GitLab 实例和 GitHub Enterprise 之间可能存在连接问题。要验证，启动 rails 控制台<!--[启动 rails 控制台](../administration/operations/rails_console.md#starting-a-rails-console-session)--> 并运行以下命令，将 `<github_url>` 替换为您的 GitHub Enterprise 实例的 URL：

```ruby
uri = URI.parse("https://<github_url>") # replace `GitHub-URL` with the real one here
http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = true
http.verify_mode = 1
response = http.request(Net::HTTP::Get.new(uri.request_uri))
```

如果您收到类似的 `execution expired` 错误，则证实了有关网络连接的理论。在这种情况下，请确保极狐GitLab 服务器能够访问您的 GitHub Enterprise 实例。

### 不允许在没有预先存在的极狐GitLab 帐户的情况下使用您的 GitHub 帐户登录

如果登录时出现消息 `Signing in using your GitHub account without a pre-existing
GitLab account is not allowed. Create a GitLab account first, and then connect it to your
GitHub account`，在极狐GitLab 中：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **帐户**。
1. 在 **服务登录** 部分，选择 **连接到 GitHub**。

之后，您应该可以通过 GitHub 成功登录。
