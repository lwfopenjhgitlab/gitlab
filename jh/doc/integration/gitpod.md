---
type: reference, how-to
stage: Create
group: Editor
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
---

# Gitpod 集成 **(FREE)**

> - 引入于 13.4 版本
> - 功能标志移除于 13.8 版本

使用 [Gitpod](https://gitpod.io/)，您可以将您的开发环境描述为代码，以便为任何极狐GitLab 项目完全设置、编译和测试开发环境。开发环境不仅是自动化的，而且是预构建的，这意味着 Gitpod 会像 CI/CD 服务器一样持续构建您的 Git 分支。

这意味着您不必等待下载依赖项和构建完成，您可以立即开始编码。使用 Gitpod，您可以随时通过浏览器从任何设备立即开始对任何项目、分支和合并请求进行编码：

![Gitpod interface](img/gitpod_web_interface_v13_4.png)
<!--
To use the GitLab Gitpod integration, it must be enabled for your GitLab instance. Users of:

- GitLab.com can use it immediately after it's [enabled in their user settings](#enable-gitpod-in-your-user-settings).
- GitLab self-managed instances can use it after:
  1. It's [enabled and configured by a GitLab administrator](#configure-a-self-managed-instance).
  1. It's [enabled in their user settings](#enable-gitpod-in-your-user-settings).
-->

- 极狐GitLab 私有化部署版用户可以在完成以下操作后使用此功能：
  1. [由极狐GitLab 管理员启用和配置](#configure-a-self-managed-instance)。
  1. [在用户设置中启用](#enable-gitpod-in-your-user-settings)。

要了解有关 Gitpod 的更多信息，请参阅他们的[功能](https://www.gitpod.io/)和[文档](https://www.gitpod.io/docs/)。

<a id="enable-gitpod-in-your-user-settings"></a>

## 在您的用户设置中启用 Gitpod

为您的极狐GitLab 实例启用 Gitpod 集成后，您可以自己启用它：

1. 在右上角，选择您的头像。
1. 选择 **偏好设置**。
1. 在 **偏好设置** 下，找到 **集成** 部分。
1. 选中 **启用 Gitpod 集成** 复选框并选择 **保存修改**。

<a id="configure-a-self-managed-instance"></a>

## 配置私有化部署版实例 **(FREE SELF)**

对于极狐GitLab 私有化部署实例，极狐GitLab 管理员需要：

1. 设置 Gitpod 实例与极狐GitLab 集成。请参阅 [Gitpod 文档](https://www.gitpod.io/docs/self-hosted/latest)，启动并运行您的实例。
1. 在极狐 GitLab 中启用：
  1. 在顶部栏上，选择 **菜单 > 管理员**。
  1. 在左侧边栏中，选择 **设置 > 通用**。
  1. 展开 **Gitpod** 配置部分。
  1. 选中 **启用 Gitpod 集成** 复选框。
  1. 添加您的 Gitpod 实例 URL（例如，`https://gitpod.example.com`）。
  1. 选择 **保存修改**。

然后，您的用户可以[为自己启用它](#enable-gitpod-in-your-user-settings)。

## 在极狐GitLab 中启动 Gitpod

您可以通过以下方式之一直接从极狐GitLab 启动 Gitpod：

- *从您的项目页面：*
  1. 转到您的项目，然后导航到要编辑的页面。
  1. 选择 **Web IDE** 旁边的插入符号(**{chevron-lg-down}**)，然后从列表中选择 **Gitpod**：

     ![Gitpod Button on Project Page](img/gitpod_button_project_page_v13_4.png)

  1. 选择 **在 Gitpod 中打开**。
- *从合并请求：*
  1. 转到您的合并请求。
  1. 在右上角，选择 **代码**，然后选择 **在 Gitpod 中打开**。


Gitpod 为您的分支构建开发环境。
