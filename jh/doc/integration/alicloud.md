---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用阿里云作为 OmniAuth 身份验证 provider **(FREE)**

您可以启用阿里云 OAuth 2.0 OmniAuth provider并使用您的阿里云帐户登录极狐GitLab。

## 创建阿里云应用

登录阿里云平台，在上面创建一个应用。阿里云会生成一个 client ID and secret key 供您使用。

1. 登录到[阿里云平台](https://account.aliyun.com/login/login.htm)。

1. 转到[OAuth 应用管理页面](https://ram.console.aliyun.com/applications)。

1. 选择 **创建应用**。

1. 填写应用详情：

   - **应用名称**：可以是任何值。
   - **显示名称**：可以是任何值。
   - **回调 URL**：此 URL 格式应为 `'GitLab instance URL' + '/users/auth/alicloud/callback'`。例如 `http://test.gitlab.com/users/auth/alicloud/callback`。

   选择 **保存**。

1. 在应用程序详细信息页面中添加 OAuth 范围：
 
   1. 在 **应用名称** 列下，选择您创建的应用程序的名称。应用程序的详细信息页面打开。
   1. 在 **应用 OAuth 范围** 选项卡下，选择 **添加 OAuth 范围**。
   1. 选择 **aliuid** 和 **profile** 复选框。
   1. 选择 **OK**。

   ![AliCloud OAuth scope](img/alicloud_scope.png)

1. 在应用程序详细信息页面中创建一个 secret：

   1. 在 **应用密钥** 选项卡下，选择 **创建密钥**。
   1. 复制生成的 SecretValue。

## 在极狐GitLab 中启用阿里云 OAuth

1. 在 GitLab 服务器上，打开配置文件。

   - **Omnibus 安装实例**

     ```shell
     sudo editor /etc/gitlab/gitlab.rb
     ```

   - **源安装实例**

     ```shell
     cd /home/git/gitlab

     sudo -u git -H editor config/gitlab.yml
     ```

1. [配置初始设置](omniauth.md#configure-initial-settings)。

1. 添加 provider 配置。将 `YOUR_APP_ID` 替换为应用详情页面的ID，将 `YOUR_APP_SECRET` 替换为您注册阿里云应用时获得的 **SecretValue**。

   - **Omnibus 安装实例**

     ```ruby
       gitlab_rails['omniauth_providers'] = [
         {
           name: "alicloud",
           app_id: "YOUR_APP_ID",
           app_secret: "YOUR_APP_SECRET"
         }
       ]
     ```

   - **源安装实例**

     ```yaml
     - { name: 'alicloud',
         app_id: 'YOUR_APP_ID',
         app_secret: 'YOUR_APP_SECRET' }
     ```

1. 保存配置文件。

1. 如果您使用 Omnibus 安装实例，[重新配置极狐GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)；如果您使用源安装实例，[重新启动极狐GitLab](../administration/restart_gitlab.md#installations-from-source)。
