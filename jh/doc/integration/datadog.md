---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Datadog 集成 **(FREE)**

> 引入于 14.1 版本。

此集成使您能够将 CI/CD 流水线和作业信息发送到 [Datadog](https://www.datadoghq.com/)。Datadog 的 [CI Visibility](https://app.datadoghq.com/ci) 产品可帮助您监控作业失败和性能问题，然后对其进行故障排除。
它基于 [Webhooks](../user/project/integrations/webhooks.md)，只需要在极狐GitLab 上进行配置。

## 配置集成

具有 **管理员** 角色的用户可以在项目、群组或实例级别配置集成：

1. 如果您没有 Datadog API 密钥：
    1. 登录 Datadog。
    1. 转到 **集成** 部分。
    1. 在[APIs 页签](https://app.datadoghq.com/account/settings#api)中生成 API 密钥。复制此值，您在后面的步骤中需要它。
1. *对于项目级或群组级集成：*在极狐GitLab 中，转到您的项目或群组。
1. *对于实例级集成：*
   1. 以具有管理员访问权限的用户身份登录极狐GitLab。
   1. 在顶部栏上，选择 **菜单 > 管理员**。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 滚动到 **添加集成**，然后选择 **Datadog**。
1. 选择 **启用**，启用集成。
1. 指定要发送数据的 [**Datadog 站点**](https://docs.datadoghq.com/getting_started/site/)。
1. 提供您的 Datadog **API 密钥**。
<!-- 1. Optional. Select **Enable logs collection** to enable logs collection for the output of jobs. ([Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/346339) in GitLab 14.8.) -->
<!-- TODO: uncomment the archive_trace_events field once :datadog_integration_logs_collection is rolled out. Rollout issue: https://gitlab.com/gitlab-org/gitlab/-/issues/346339 -->
1. 可选。要覆盖用于直接发送数据的 API URL，请提供 **API URL**。仅在高级场景中使用。
1. 可选。如果您使用多个极狐GitLab 实例，请提供唯一的 **服务** 名称以区分您的极狐GitLab 实例。
1. 可选。如果您使用极狐GitLab 实例组（例如 staging 和生产环境），请提供 **环境** 名称。该值附加到集成生成的每个 span。
1. 可选。要为配置集成的所有 span 定义任何自定义标签，请在 **标签** 中每行输入一个标签。每行必须采用 `key:value` 格式。（引入于 14.8 版本。）
1. 可选。选择 **测试设置**，测试您的集成。
1. 选择 **保存修改**。

当集成发送数据时，您可以在 Datadog 账户的 [CI Visibility](https://app.datadoghq.com/ci) 部分查看。

<!--
## Related topics

- [Datadog's CI Visibility](https://docs.datadoghq.com/continuous_integration/) documentation.
-->
