---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, how-to
---

# Sourcegraph 集成 **(FREE)**

> - 在 SaaS 版上启用于 12.5 版本。
> - 在私有化部署版上启用于 14.8 版本。

FLAG:
在私有化部署版上，此功能默认启用。要隐藏此功能，[禁用功能标志](../administration/feature_flags.md) `sourcegraph`。在 SaaS 版上，仅适用于公开项目。

[Sourcegraph](https://sourcegraph.com) 提供代码智能功能，集成到 UI 中。

<!--
For GitLab.com users, see [Sourcegraph for GitLab.com](#sourcegraph-for-gitlabcom).
-->

![Sourcegraph demo](img/sourcegraph_demo_v12_5.png)

NOTE:
此功能需要用户选择加入。在您的极狐GitLab 实例启用 Sourcegraph 后，您可以选择[通过您的用户偏好设置](#enable-sourcegraph-in-user-preferences)启用 Sourcegraph。

## 私有化部署版实例设置 **(FREE SELF)**

在您可以在极狐GitLab 中启用 Sourcegraph 代码智能之前，使用您的极狐GitLab 实例将 Sourcegraph 实例配置为外部服务。

### 设置一个私有化部署的 Sourcegraph 实例 **(FREE SELF)**

如果您是 Sourcegraph 的新手，请前往 [Sourcegraph 安装文档](https://docs.sourcegraph.com/admin)，启动并运行您的实例。

如果您使用 HTTPS 连接到极狐GitLab，则必须为您的 Sourcegraph 实例[配置 HTTPS](https://docs.sourcegraph.com/admin/http_https_configuration)。

### 将您的 Sourcegraph 实例连接到您的极狐GitLab 实例

1. 导航到 Sourcegraph 中的站点管理中心。
1. [配置您的极狐GitLab 外部服务](https://docs.sourcegraph.com/admin/external_service/gitlab)。如果您已经在 Sourcegraph 中搜索了极狐GitLab 仓库，则可以跳过此步骤。
1. 验证您是否可以通过运行测试查询，从 Sourcegraph 实例中搜索极狐GitLab 仓库。
1. 将极狐GitLab 实例 URL 添加到站点配置中的 [`corsOrigin` 设置](https://docs.sourcegraph.com/admin/config/site_config#corsOrigin)。

### 配置极狐GitLab 实例的 Sourcegraph

1. 在顶部栏上，选择 **菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **Sourcegraph** 配置部分。
1. 勾选 **启用 Sourcegraph**。
1. 将 Sourcegraph URL 设置为您的 Sourcegraph 实例，例如 `https://sourcegraph.example.com`。

![Sourcegraph administration settings](img/sourcegraph_admin_v12_5.png)

<a id="enable-sourcegraph-in-user-preferences"></a>

## 在用户偏好设置中启用 Sourcegraph

如果极狐GitLab 管理员启用了 Sourcegraph，您可以在您的用户偏好设置中启用此功能。

在极狐GitLab 中：

1. 在右上角，选择您的头像。
1. 选择 **偏好设置**。
1. 在 **集成** 部分，选中 **Sourcegraph** 下的复选框。
1. 选择 **保存修改**。

![Sourcegraph user preferences](img/sourcegraph_user_preferences_v12_5.png)

## 使用 Sourcegraph 代码智能

启用后，参与项目会在以下代码视图中显示代码智能弹出框：

- 合并请求差异
- 提交视图
- 文件视图

当访问这些视图之一时，您现在可以将鼠标悬停在代码引用上来查看弹出窗口：

- 有关如何定义此引用的详细信息。
- **转到定义**，导航到定义此引用的代码行。
- **查找引用**，导航到配置的 Sourcegraph 实例，显示对突出显示的代码的引用列表。

![Sourcegraph demo](img/sourcegraph_popover_v12_5.png)

<!--
## Sourcegraph for GitLab.com

Sourcegraph powered code intelligence is available for all public projects on GitLab.com.

Support for private projects is not yet available for GitLab.com;
follow the epic [&2201](https://gitlab.com/groups/gitlab-org/-/epics/2201)
for updates.

## 故障排除

### Sourcegraph 不工作

如果您为项目启用了 Sourcegraph 但它不起作用，则 Sourcegraph 可能尚未为项目编制索引。您可以通过访问 `https://sourcegraph.com/gitlab.com/<project-path>` 将 `<project-path>` 替换为极狐GitLab 项目的路径来检查 Sourcegraph 对项目的可用性。
-->

## Sourcegraph 隐私

查看来自 Sourcegraph 的 [扩展文档](https://docs.sourcegraph.com/integration/browser_extension#privacy)：

> Sourcegraph 集成从不向 Sourcegraph.com 发送任何日志、ping、使用统计数据或遥测数据。
> 仅根据需要连接到 Sourcegraph.com，以在公共代码上提供代码智能或其它功能。
> 因此，不会将私有代码、私有仓库名称、用户名或任何其他特定数据发送到 Sourcegraph.com。
