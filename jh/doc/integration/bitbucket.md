---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 将 GitLab 服务器与 Bitbucket Cloud 集成 **(FREE SELF)**

您可以将 Bitbucket.org 设置为 OAuth 2.0 provider，使用您的 Bitbucket.org 帐户凭据登录极狐GitLab。您还可以从 Bitbucket.org 导入您的项目。

- 要将 Bitbucket.org 用作 OmniAuth provider，请遵循 [Bitbucket OmniAuth provider](#bitbucket-omniauth-provider) 部分。
- 要从 Bitbucket 导入项目，请遵循 [Bitbucket OmniAuth provider](#bitbucket-omniauth-provider) 和 [Bitbucket 项目导入](#bitbucket-project-import)部分。

## Bitbucket OmniAuth provider

要启用 Bitbucket OmniAuth provider，您必须向 Bitbucket.org 注册您的应用程序。Bitbucket 会生成一个应用程序 ID 和密钥供您使用。

WARNING:
为了帮助防止 [OAuth 2 隐蔽重定向](https://oauth.net/advisories/2014-1-covert-redirect/) 漏洞，其中用户的极狐GitLab 帐户可能会受到损害，请将 `/users/auth` 附加到 Bitbucket 授权回调 URL 的结尾。

1. 登录 [Bitbucket.org](https://bitbucket.org)。
1. 导航到您的个人用户设置（**Bitbucket settings**）或团队设置（**Manage team**），具体取决于您希望应用程序的注册方式。申请注册为个人还是团队均可。
1. 在 **Access Management** 下的左侧菜单中，选择 **OAuth**。
1. 选择 **Add consumer**。
1. 提供所需的详细信息：

   - **Name：**可以是任何名称。考虑类似 `<Organization>'s GitLab` 或 `<Your Name>'s GitLab` 或其它描述性信息。
   - **Application description：**可选。
   - **Callback URL：**极狐GitLab 安装的 URL，例如 `https://gitlab.example.com/users/auth`。将此字段留空[导致出现 `Invalid redirect_uri` 消息](https://confluence.atlassian.com/bitbucket/oauth-faq-338365710.html)。
   - **URL：**极狐GitLab 安装的 URL，例如 `https://gitlab.example.com`。

1. 至少授予以下权限：

   ```plaintext
   Account: Email, Read
   Projects: Read
   Repositories: Read
   Pull Requests: Read
   Issues: Read
   Wiki: Read and Write
   ```

   ![Bitbucket OAuth settings page](img/bitbucket_oauth_settings_page.png)

1. 选择 **Save**。
1. 选择您新创建的 OAuth 使用者，您现在应该在 OAuth 使用者列表中看到一个 **Key** 和 **Secret**。在继续配置时保持此页面打开。

   ![Bitbucket OAuth key](img/bitbucket_oauth_keys.png)

1. 在 GitLab 服务器上，打开配置文件：

   ```shell
   # For Omnibus packages
   sudo editor /etc/gitlab/gitlab.rb

   # For installations from source
   sudo -u git -H editor /home/git/gitlab/config/gitlab.yml
   ```

1. 添加 Bitbucket provider 配置：

   对于 Omnibus 安装实例：

   ```ruby
   gitlab_rails['omniauth_providers'] = [
     {
       name: "bitbucket",
       # label: "Provider name", # optional label for login button, defaults to "Bitbucket"
       app_id: "BITBUCKET_APP_KEY",
       app_secret: "BITBUCKET_APP_SECRET",
       url: "https://bitbucket.org/"
     }
   ]
   ```

   对于源安装实例：

   ```yaml
   omniauth:
     enabled: true
     providers:
       - { name: 'bitbucket',
           # label: 'Provider name', # optional label for login button, defaults to "Bitbucket"
           app_id: 'BITBUCKET_APP_KEY',
           app_secret: 'BITBUCKET_APP_SECRET',
           url: 'https://bitbucket.org/' }
   ```

   其中 `BITBUCKET_APP_KEY` 是密钥，`BITBUCKET_APP_SECRET` 是来自 Bitbucket 应用程序页面的 Secret。

1. 保存配置文件。
1. 要使更改生效，如果您使用 Omnibus GitLab 安装，请[重新配置 GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)；如果您是从源代码安装的，请[重新启动](../administration/restart_gitlab.md#源安装实例)。

现在，在登录页面上，常规登录表单下方应该有一个 Bitbucket 图标。单击该图标开始身份验证过程。Bitbucket 要求用户登录并授权极狐GitLab 应用程序。如果成功，用户将返回到极狐GitLab 并登录。

<a id="bitbucket-project-import"></a>

## Bitbucket 项目导入

上述配置设置完成后，您可以使用 Bitbucket 登录极狐GitLab 并[开始导入您的项目](../user/project/import/bitbucket.md)。

如果您想从 Bitbucket 导入项目，但不想启用登录，您可以[在管理面板中禁用登录](omniauth.md#enable-or-disable-sign-in-with-an-omniauth-provider-without-disabling-import-sources)。
