---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# CAS OmniAuth 提供商 **(FREE SELF)**

要启用 CAS OmniAuth 提供商，您必须使用 CAS 实例注册您的应用程序。
这需要极狐GitLab 提供给 CAS 的服务 URL，应该类似于：`https://gitlab.example.com:443/users/auth/cas3/callback?url`。
默认情况下启用单点注销 (SLO) 处理，因此您只需为反向信道注销配置 CAS。

1. 在您的极狐GitLab 服务器上，打开配置文件。

   对于 Omnibus 安装包：

   ```shell
   sudo editor /etc/gitlab/gitlab.rb
   ```

   对于源代码安装：

   ```shell
   cd /home/git/gitlab

   sudo -u git -H editor config/gitlab.yml
   ```

1. 配置[通用设置](omniauth.md#configure-common-settings) 以将 `cas3` 添加为单点登录提供商。这为没有现有极狐GitLab 账户的用户启用了即时账户配置。

1. 添加提供商配置：

  对于 Omnibus 软件包：

   ```ruby
   gitlab_rails['omniauth_providers'] = [
     {
       name: "cas3",
       label: "Provider name", # optional label for login button, defaults to "Cas3"
       args: {
           url: "CAS_SERVER",
           login_url: "/CAS_PATH/login",
           service_validate_url: "/CAS_PATH/p3/serviceValidate",
           logout_url: "/CAS_PATH/logout"
       }
     }
   ]
   ```

   对于源代码安装：

   ```yaml
   - { name: 'cas3',
       label: 'Provider name', # optional label for login button, defaults to "Cas3"
       args: {
         url: 'CAS_SERVER',
         login_url: '/CAS_PATH/login',
         service_validate_url: '/CAS_PATH/p3/serviceValidate',
         logout_url: '/CAS_PATH/logout' } }
   ```

1. 将 'CAS_PATH' 更改为您的 CAS 实例的根目录（例如 `cas`）。

1. 如果您的 CAS 实例不使用默认 TGC 生命周期，请至少将 `cas3.session_duration` 更新为当前 TGC 最大生命周期。要明确禁用 SLO，无论 CAS 设置如何，请将其设置为 0。

1. 保存配置文件。

1. 要使更改生效：
    - 如果您通过 Omnibus 安装，[重新配置极狐GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)。
    - 如果您从源代码安装，[重启极狐GitLab](../administration/restart_gitlab.md#installations-from-source)。

在登录页面上，登录表单中现在应该有一个 CAS 选项卡。
