---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 将您的服务器与 GitLab.com 集成 **(FREE SELF)**

从 GitLab.com 导入项目并使用您的 GitLab.com 帐户登录到您的极狐GitLab 实例。

要启用 GitLab.com OmniAuth provider，您必须向 GitLab.com 注册您的应用程序。
GitLab.com 会生成一个应用程序 ID 和密钥供您使用。

1. 登录 GitLab.com。
1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏中，选择 **应用程序**。
1. 提供 **添加新应用程序** 所需的详细信息。
   - 名称：任意填写。考虑类似 `<Organization>'s GitLab` 或 `<Your Name>'s GitLab` 或其它描述性的内容。
   - 重定向 URI：

   ```plaintext
   http://your-gitlab.example.com/import/gitlab/callback
   http://your-gitlab.example.com/users/auth/gitlab/callback
   ```

   导入器需要第一个链接，身份验证需要第二个链接。

   如果您：

   - 计划使用导入器，您可以保持原样。
   - 只想使用此应用程序进行身份验证，我们建议使用更小的范围集。`read_user` 即可。

1. 选择 **保存应用**。
1. 您现在应该会看到一个 **Application ID** 和 **Secret**。在继续配置时保持此页面打开。
1. 在您的极狐GitLab 服务器上，打开配置文件。

   Omnibus 安装实例：

   ```shell
   sudo editor /etc/gitlab/gitlab.rb
   ```

   源安装实例：

   ```shell
   cd /home/git/gitlab

   sudo -u git -H editor config/gitlab.yml
   ```

1. 初始设置见[配置初始设置](omniauth.md#configure-initial-settings)。
1. 添加 provider 配置：

   针对 **GitLab.com** 进行身份验证的 Omnibus 安装实例：

   ```ruby
   gitlab_rails['omniauth_providers'] = [
     {
       name: "gitlab",
       # label: "Provider name", # optional label for login button, defaults to "GitLab.com"
       app_id: "YOUR_APP_ID",
       app_secret: "YOUR_APP_SECRET",
       args: { scope: "read_user" } # optional: defaults to the scopes of the application
     }
   ]
   ```

   或者，针对另一个不同的实例进行身份验证的 Omnibus 安装实例：

   ```ruby
   gitlab_rails['omniauth_providers'] = [
     {
       name: "gitlab",
       label: "Provider name", # optional label for login button, defaults to "GitLab.com"
       app_id: "YOUR_APP_ID",
       app_secret: "YOUR_APP_SECRET",
       args: { scope: "read_user" # optional: defaults to the scopes of the application
             , client_options: { site: "https://gitlab.example.com" } }
     }
   ]
   ```

   针对 **GitLab.com** 进行身份验证的源安装实例：

   ```yaml
   - { name: 'gitlab',
       # label: 'Provider name', # optional label for login button, defaults to "GitLab.com"
       app_id: 'YOUR_APP_ID',
       app_secret: 'YOUR_APP_SECRET',
   ```

   或者，针对另一个不同的实例进行身份验证的源安装实例：

   ```yaml
   - { name: 'gitlab',
       label: 'Provider name', # optional label for login button, defaults to "GitLab.com"
       app_id: 'YOUR_APP_ID',
       app_secret: 'YOUR_APP_SECRET',
       args: { "client_options": { "site": 'https://gitlab.example.com' } }
   ```

   NOTE:
   在 15.1 及更早版本中，`site` 参数需要 `/api/v4` 后缀。我们建议您在升级到 15.2 或更高版本后删除此后缀。

1. 将 `YOUR_APP_ID` 更改为 GitLab.com 应用程序页面中的 Application ID。
1. 将 `YOUR_APP_SECRET` 更改为 GitLab.com 应用程序页面中的 secret。
1. 保存配置文件。
1. 根据极狐GitLab 的安装方式，使用适当的方法应用以上更改：
   - Omnibus 安装实例：[重新配置极狐GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)。
   - 源安装实例：[重启极狐GitLab](../administration/restart_gitlab.md#installations-from-source)。

在登录页面上，现在应该在常规登录表单后面有一个 GitLab.com 图标。选择图标来开始身份验证过程。
GitLab.com 要求用户登录并授权应用程序。如果一切顺利，用户将返回到您的极狐GitLab 实例并登录。

## 减少登录时的访问权限

> - 引入于 14.8 版本，功能标志为 `omniauth_login_minimal_scopes`。默认禁用。
> - 在 SaaS 上启用于 14.9 版本。
> - 功能标志 `omniauth_login_minimal_scopes` 删除于 15.2 版本。


FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能不可用。要使其可用，请让管理员[启用功能标志](../administration/feature_flags.md) `omniauth_login_minimal_scopes`。在 SaaS 版上，此功能不可用。

如果您使用极狐GitLab 实例进行身份验证，则可以在使用 OAuth 应用程序登录时减少访问权限。

任何 OAuth 应用程序都可以使用授权参数：`gl_auth_type=login`，宣称应用程序的用途。如果应用程序配置了 `api` 或 `read_api`，则使用 `read_user` 发出访问令牌来进行登录，因为不需要更高的权限。

极狐GitLab OAuth 客户端配置为传递此参数，但其它应用程序也可以传递它。
