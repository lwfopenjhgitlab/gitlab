---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

# 私有化部署版极狐GitLab 实例的 SAML SSO **(FREE SELF)**

本页介绍如何为私有化部署版极狐GitLab 实例，设置实例范围的 SAML 单点登录（SSO）。

您可以将极狐GitLab 配置为 SAML 服务提供商 (SP），允许极狐GitLab 使用来自 SAML 身份提供程序 (IdP)（例如 Okta）的断言（assertions）来对用户进行身份验证。

<!--
To set up SAML on GitLab.com, see [SAML SSO for GitLab.com groups](../user/group/saml_sso/index.md).
-->

更多信息：

- OmniAuth 提供商设置，查看 [OmniAuth 文档](omniauth.md)。
- 常用术语，查看[常用术语表](#glossary-of-common-terms)。

<a id="configure-saml-support-in-gitlab"></a>

## 在极狐GitLab 中配置 SAML 支持

1. 确保极狐GitLab 配置了 HTTPS。

1. 在您的极狐GitLab 服务器上，打开配置文件。

   对于 Omnibus 安装实例：

   ```shell
   sudo editor /etc/gitlab/gitlab.rb
   ```

   对于源安装实例：

   ```shell
   cd /home/git/gitlab

   sudo -u git -H editor config/gitlab.yml
   ```

1. 编辑初始[配置](omniauth.md#configure-initial-settings)。

1. 要允许您的用户使用 SAML 进行注册而无需先手动创建帐户，请将以下值添加到您的配置中。

   对于 Omnibus 安装实例：

   ```ruby
   gitlab_rails['omniauth_allow_single_sign_on'] = ['saml']
   gitlab_rails['omniauth_block_auto_created_users'] = false
   ```

   对于源安装实例：

   ```yaml
   omniauth:
     enabled: true
     allow_single_sign_on: ["saml"]
     block_auto_created_users: false
   ```

1. 可选。如果电子邮件地址匹配，您可以通过添加以下设置，自动将 SAML 用户与现有的极狐GitLab 用户关联。

   对于 Omnibus 安装实例：

   ```ruby
   gitlab_rails['omniauth_auto_link_saml_user'] = true
   ```

   对于源安装实例：

   ```yaml
   auto_link_saml_user: true
   ```

   或者，用户可以通过[为现有用户启用 OmniAuth](omniauth.md#enable-omniauth-for-an-existing-user)，手动将他们的 SAML 身份关联到现有的极狐GitLab 帐户。

1. 配置以下属性，使您的 SAML 用户无法更改：

   - `NameID`
   - 与 `omniauth_auto_link_saml_user` 一起使用时的 `Email`。

   如果用户可以更改这些属性，则他们可以作为其他授权用户登录。有关如何使这些属性不可更改的信息，请参阅您的 SAML IdP 文档。

1. 添加提供商配置。

   对于 Omnibus 安装实例：

   ```ruby
   gitlab_rails['omniauth_providers'] = [
     {
       name: "saml",
       label: "Provider name", # optional label for login button, defaults to "Saml"
       args: {
         assertion_consumer_service_url: "https://gitlab.example.com/users/auth/saml/callback",
         idp_cert_fingerprint: "43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8",
         idp_sso_target_url: "https://login.example.com/idp",
         issuer: "https://gitlab.example.com",
         name_identifier_format: "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent"
       }
     }
   ]
   ```

   对于源安装实例：

   ```yaml
   omniauth:
     providers:
       - {
         name: 'saml',
         label: 'Provider name', # optional label for login button, defaults to "Saml"
         args: {
           assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
           idp_cert_fingerprint: '43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8',
           idp_sso_target_url: 'https://login.example.com/idp',
           issuer: 'https://gitlab.example.com',
           name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent'
         }
       }
   ```

1. 将 `assertion_consumer_service_url` 的值与极狐GitLab 的 HTTPS 端点匹配。要生成正确的值，请将 `users/auth/saml/callback` 附加到您的极狐GitLab 安装实例的 HTTPS URL。

1. 更改以下值，匹配您的 IdP：

   - `idp_cert_fingerprint`
   - `idp_sso_target_url`
   - `name_identifier_format`
   
   如果您使用 `idp_cert_fingerprint`，它必须是 SHA1 指纹。有关这些值的更多信息，请参阅 [OmniAuth SAML 文档](https://github.com/omniauth/omniauth-saml)。
   有关其他配置设置的更多信息，请参阅[在您的 IdP 上配置 SAML](#configure-saml-on-your-idp)。

1. 将 `issuer` 的值更改为唯一的名称，该名称在 IdP 中标识应用程序。

1. 要使更改生效，如果您安装了：

   - Omnibus 实例，[需要重新配置极狐GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)。
   - 源安装实例，[需要重启极狐GitLab](../administration/restart_gitlab.md#installations-from-source)。

### 在您的 SAML IdP 中注册极狐GitLab

1. 使用 `issuer` 中指定的应用程序名称，在您的 SAML IdP 中注册极狐GitLab SP。

1. 要向 IdP 提供配置信息，请为应用程序构建元数据 URL。要为极狐GitLab 构建元数据 URL，请将 `users/auth/saml/metadata` 附加到极狐GitLab 安装实例的 HTTPS URL。例如：

   ```plaintext
   https://gitlab.example.com/users/auth/saml/metadata
   ```

   IdP **必须**使用 `email` 或 `mail` 提供包含用户电子邮件地址的声明。有关其他可用声明的更多信息，请参阅[配置断言](#configure-assertions)。

1. 在登录页面上，常规登录表单下方现在应该有一个 SAML 图标。选择该图标可以开始身份验证过程。如果身份验证成功，您将返回到极狐GitLab 并登录。

<a id="configure-saml-on-your-idp"></a>

### 在您的 IdP 上配置 SAML

要在 IdP 上配置 SAML 应用程序，您至少需要以下信息：

- Assertion Consumer 服务的 URL
- Issuer
- `NameID`
- [电子邮件地址声明](#configure-assertions)

有关示例配置，请参阅[设置身份提供商](#set-up-identity-providers)。

您的 IdP 可能需要额外的配置。有关详细信息，请参阅 [IdP 上 SAML 应用程序的其他配置](#additional-configuration-for-saml-apps-on-your-idp)。

### 配置极狐GitLab 使用多个 SAML IdP

> 引入于 14.6 版本。

如果出现以下情况，您可以将极狐GitLab 配置为使用多个 SAML IdP：

- 每个提供商都有一个唯一的名称集，与 `args` 中的名称集相匹配。至少有一个提供商必须具有名称 `saml`，规避 14.6 及更高版本中的已知问题。
- 使用以下提供商名称：
  - 在基于提供者名称的属性的 OmniAuth 配置中。例如，`allowBypassTwoFactor`、`allowSingleSignOn` 和 `syncProfileFromProvider`。
  - 作为附加身份关联到每个现有用户。
- `assertion_consumer_service_url` 匹配提供商名称。
- `strategy_class` 是显式设置的，因为它不能从提供商名称中推断出来。

SAML 群组成员身份和群组同步不支持多个 IdP。<!--For more information, see [issue 386605](https://gitlab.com/gitlab-org/gitlab/-/issues/386605).-->

提供商配置示例（源安装实例）：

```yaml
omniauth:
  providers:
    - {
      name: 'saml', # This must match the following name configuration parameter
      args: {
        name: 'saml', # This is mandatory and must match the provider name
        strategy_class: 'OmniAuth::Strategies::SAML',
        assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml_1/callback', # URL must match the name of the provider
        ... # Put here all the required arguments similar to a single provider
      },
      label: 'Provider 1' # Differentiate the two buttons and providers in the UI
    }
    - {
      name: 'saml1', # This must match the following name configuration parameter
      args: {
        name: 'saml1', # This is mandatory and must match the provider name
        strategy_class: 'OmniAuth::Strategies::SAML',
        assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml_2/callback', # URL must match the name of the provider
        ... # Put here all the required arguments similar to a single provider
      },
      label: 'Provider 2' # Differentiate the two buttons and providers in the UI
    }
```

提供商配置示例（Omnibus 安装实例）：

要允许您的用户使用 SAML 进行注册，而无需从任一提供商手动创建帐户，请将以下值添加到您的配置中。

```ruby
gitlab_rails['omniauth_allow_single_sign_on'] = ['saml', 'saml1']
```

```ruby
gitlab_rails['omniauth_providers'] = [
  {
    name: 'saml', # This must match the following name configuration parameter
    args: {
            name: 'saml', # This is mandatory and must match the provider name
            strategy_class: 'OmniAuth::Strategies::SAML',
            assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml_1/callback', # URL must match the name of the provider
            ... # Put here all the required arguments similar to a single provider
          },
    label: 'Provider 1' # Differentiate the two buttons and providers in the UI
  },
  {
    name: 'saml1', # This must match the following name configuration parameter
    args: {
            name: 'saml1', # This is mandatory and must match the provider name
            strategy_class: 'OmniAuth::Strategies::SAML',
            assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml_2/callback', # URL must match the name of the provider
            ... # Put here all the required arguments similar to a single provider
          },
    label: 'Provider 2' # Differentiate the two buttons and providers in the UI
  }
]
```

<a id="set-up-identity-providers"></a>

## 设置身份提供商

极狐GitLab 对 SAML 的支持意味着您可以通过各种 IdP 登录极狐GitLab。

极狐GitLab 提供以下有关设置 Okta 的内容，仅供参考。如果您对配置 IdP 有任何疑问，请联系您的提供商的支持。

### 设置 Okta

1. 在 Okta 管理员部分中，选择 **Applications**。
1. 在页面上选择 **Create App Integration**，然后在下一个页面上选择 **SAML 2.0**。
1. 可选。添加 Logo，您必须裁剪 Logo 并调整其大小。
1. 完成 SAML 通用配置。输入以下参数：

   - `"Single sign-on URL"`：使用 assertion consumer 服务 URL。
   - `"Audience URI"`：使用 issuer。
   - `NameID`
   - [断言（assertions）](#configure-assertions)

1. 在反馈部分，输入您是客户并创建供内部使用的应用程序。
1. 在新应用配置文件的顶部，选择 **SAML 2.0 configuration instructions**。
1. 注意 **Identity Provider Single Sign-On URL**。在您的极狐GitLab 配置文件中，将此 URL 用于 `idp_sso_target_url`。
1. 在您退出 Okta 之前，请确保添加了您的用户和群组（如果有）。

<!--
### Set up Google Workspace

Prerequisites:

- Make sure you have access to a
[Google Workspace Super Admin account](https://support.google.com/a/answer/2405986#super_admin).

1. Use the following information, and follow the instructions in
[Set up your own custom SAML application in Google Workspace](https://support.google.com/a/answer/6087519?hl=en).

   |                  | Typical value                                    | Description                                              |
   |------------------|--------------------------------------------------|----------------------------------------------------------|
   | Name of SAML App | GitLab                                           | Other names OK.                                          |
   | ACS URL          | `https://<GITLAB_DOMAIN>/users/auth/saml/callback` | Assertion Consumer Service URL.             |
   | GITLAB_DOMAIN    | `gitlab.example.com`                             | Your GitLab instance domain.               |
   | Entity ID        | `https://gitlab.example.com`                     | A value unique to your SAML application. Set it to the `issuer` in your GitLab configuration.                         |
   | Name ID format   | EMAIL                                            | Required value. Also known as `name_identifier_format`.                    |
   | Name ID          | Primary email address                            | Your email address. Make sure someone receives content sent to that address.                |
   | First name       | `first_name`                                     | First name. Required value to communicate with GitLab.               |
   | Last name        | `last_name`                                      | Last name. Required value to communicate with GitLab.               |

1. Set up the following SAML attribute mappings:

   | Google Directory attributes       | App attributes |
   |-----------------------------------|----------------|
   | Basic information > Email         | `email`        |
   | Basic Information > First name    | `first_name`   |
   | Basic Information > Last name     | `last_name`    |

   You might use some of this information when you
   [configure SAML support in GitLab](#configure-saml-support-in-gitlab).

When configuring the Google Workspace SAML application, record the following information:

|             | Value        | Description                                                                       |
|-------------|--------------|-----------------------------------------------------------------------------------|
| SSO URL     | Depends      | Google Identity Provider details. Set to the GitLab `idp_sso_target_url` setting. |
| Certificate | Downloadable | Run `openssl x509 -in <your_certificate.crt> -noout -fingerprint` to generate the SHA1 fingerprint that can be used in the `idp_cert_fingerprint` setting.                         |

Google Workspace Administrator also provides the IdP metadata, Entity ID, and SHA-256
fingerprint. However, GitLab does not need this information to connect to the
Google Workspace SAML application.
-->

### 设置其他 IdP

一些 IdP 有关于如何在 SAML 配置中将它们用作 IdP 的文档。
例如：

- [Active Directory Federation Services (ADFS)](https://learn.microsoft.com/en-us/windows-server/identity/ad-fs/operations/create-a-relying-party-trust)
- [Auth0](https://auth0.com/docs/authenticate/protocols/saml/saml-sso-integrations/configure-auth0-saml-identity-provider)

如果您对在 SAML 配置中配置 IdP 有任何疑问，请联系提供商的技术支持。

<a id="configure-assertions"></a>

### 配置断言

| 字段           | 支持的默认键 |
|-----------------|------------------------|
| Email（必需）| `email`、`mail`        |
| Full Name       | `name`                 |
| First Name      | `first_name`、`firstname`、`firstName` |
| Last Name       | `last_name`、`lastname`、`lastName`    |

请参阅 [`attribute_statements`](#map-saml-response-attribute-names) 了解：

- 自定义断言配置示例。
- 如何配置自定义用户名属性。

有关支持断言的完整列表，请参阅 [OmniAuth SAML gem](https://github.com/omniauth/omniauth-saml/blob/master/lib/omniauth/strategies/saml.rb)。

## 基于 SAML 群组成员资格配置用户

您可以：

- 要求用户成为某个群组的成员。
- 根据群组成员身份，为用户分配[外部](../user/admin_area/external_users.md)、管理员或[审计员](../administration/auditor_users.md)角色。

极狐GitLab 在每次 SAML 登录时检查这些群组，并根据需要更新用户属性。
此功能**不**允许您自动将用户添加到极狐GitLab [群组](../user/group/index.md)。

对这些群组的支持取决于：

- 您的[订阅](https://gitlab.cn/pricing/)。
- [SAML 提供商的名称](#configure-saml-support-in-gitlab)。群组成员身份仅由名为 `saml` 的单个 SAML 提供商支持。<!--For more information, see [issue 386605](https://gitlab.com/gitlab-org/gitlab/-/issues/386605).-->

| 群组                        | 版本               | 
|------------------------------|--------------------|
| [Required](#required-groups) | **(FREE SELF)**    | 
| [External](#external-groups) | **(FREE SELF)**    | 
| [Admin](#administrator-groups)       | **(FREE SELF)**    | 
| [Auditor](#auditor-groups)   | **(PREMIUM SELF)** | 

### 先决条件

您必须告诉极狐GitLab 在哪里寻找群组信息。为此，请确保您的 IdP 服务器发送特定的 `AttributeStatement` 以及常规 SAML 响应。例如：

```xml
<saml:AttributeStatement>
  <saml:Attribute Name="Groups">
    <saml:AttributeValue xsi:type="xs:string">Developers</saml:AttributeValue>
    <saml:AttributeValue xsi:type="xs:string">Freelancers</saml:AttributeValue>
    <saml:AttributeValue xsi:type="xs:string">Admins</saml:AttributeValue>
    <saml:AttributeValue xsi:type="xs:string">Auditors</saml:AttributeValue>
  </saml:Attribute>
</saml:AttributeStatement>
```

属性的名称必须包含用户所属的群组。
要告诉极狐GitLab 在哪里可以找到这些群组，请将 `groups_attribute:` 元素添加到您的 SAML 设置中。

<a id="required-groups"></a>

### Required 群组

您的 IdP 在 SAML 响应中将群组信息传递给极狐GitLab。要使用此响应，请配置极狐GitLab 识别：

- 使用 `groups_attribute` 设置在 SAML 响应中查找群组的位置。
- 有关群组或用户的信息，使用群组设置。

使用 `required_groups` 设置配置极狐GitLab，识别需要哪个群组成员才能登录。

如果您不设置 `required_groups` 或将设置留空，任何经过适当身份验证的人都可以使用该服务。

配置示例：

```yaml
{ name: 'saml',
  label: 'Our SAML Provider',
  groups_attribute: 'Groups',
  required_groups: ['Developers', 'Freelancers', 'Admins', 'Auditors'],
  args: {
          assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
          idp_cert_fingerprint: '43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8',
          idp_sso_target_url: 'https://login.example.com/idp',
          issuer: 'https://gitlab.example.com',
          name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent'
  } }
```

<a id="external-groups"></a>

### External 群组

您的 IdP 在 SAML 响应中将群组信息传递给极狐GitLab。要使用此响应，请配置极狐GitLab 识别：

- 使用 `groups_attribute` 设置在 SAML 响应中查找群组的位置。
- 有关群组或用户的信息，使用群组设置。

SAML 可以根据 `external_groups` 设置自动将用户识别为[外部用户](../user/admin_area/external_users.md)。

配置示例：

```yaml
{ name: 'saml',
  label: 'Our SAML Provider',
  groups_attribute: 'Groups',
  external_groups: ['Freelancers'],
  args: {
          assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
          idp_cert_fingerprint: '43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8',
          idp_sso_target_url: 'https://login.example.com/idp',
          issuer: 'https://gitlab.example.com',
          name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent'
  } }
```

<a id="administrator-groups"></a>

### Administrator 群组

您的 IdP 在 SAML 响应中将群组信息传递给极狐GitLab。要使用此响应，请配置极狐GitLab 识别：

- 使用 `groups_attribute` 设置在 SAML 响应中查找群组的位置。
- 有关群组或用户的信息，使用群组设置。

使用 `admin_groups` 设置配置极狐GitLab，识别哪些群组授予用户管理员访问权限。

配置示例：

```yaml
{ name: 'saml',
  label: 'Our SAML Provider',
  groups_attribute: 'Groups',
  admin_groups: ['Admins'],
  args: {
          assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
          idp_cert_fingerprint: '43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8',
          idp_sso_target_url: 'https://login.example.com/idp',
          issuer: 'https://gitlab.example.com',
          name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent'
  } }
```

<a id="auditor-groups"></a>

### Auditor 群组 **(PREMIUM SELF)**

您的 IdP 在 SAML 响应中将群组信息传递给极狐GitLab。要使用此响应，请配置极狐GitLab 识别：

- 使用 `groups_attribute` 设置在 SAML 响应中查找群组的位置。
- 有关群组或用户的信息，使用群组设置。

使用 `auditor_groups` 设置配置极狐GitLab，识别哪些群组包括具有[审计员访问权限](../administration/auditor_users.md)的用户。

配置示例：

```yaml
{ name: 'saml',
  label: 'Our SAML Provider',
  groups_attribute: 'Groups',
  auditor_groups: ['Auditors'],
  args: {
          assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
          idp_cert_fingerprint: '43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8',
          idp_sso_target_url: 'https://login.example.com/idp',
          issuer: 'https://gitlab.example.com',
          name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent'
  } }
```

## 自动管理 SAML 群组同步

<!--
For information on automatically managing GitLab group membership, see [SAML Group Sync](../user/group/saml_sso/group_sync.md).
-->

## 绕过双重身份验证

要将 SAML 身份验证方法，配置为在每个会话的基础上，算作双重身份验证 (2FA)，请在 `upstream_two_factor_authn_contexts` 列表中注册该方法。

1. 确保您的 IdP 正在返回 `AuthnContext`。例如：

```xml
<saml:AuthnStatement>
    <saml:AuthnContext>
        <saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:MediumStrongCertificateProtectedTransport</saml:AuthnContextClassRef>
    </saml:AuthnContext>
</saml:AuthnStatement>
```

1. 编辑您的安装配置，在 `upstream_two_factor_authn_contexts` 列表中注册 SAML 身份验证方法。编辑配置的方式因安装类型而异。

### Omnibus 安装实例

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['omniauth_providers'] = [
     {
       name: "saml",
       args: {
         assertion_consumer_service_url: "https://gitlab.example.com/users/auth/saml/callback",
         idp_cert_fingerprint: "43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8",
         idp_sso_target_url: "https://login.example.com/idp",
         issuer: "https://gitlab.example.com",
         name_identifier_format: "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent",
         upstream_two_factor_authn_contexts:
           %w(
             urn:oasis:names:tc:SAML:2.0:ac:classes:CertificateProtectedTransport
             urn:oasis:names:tc:SAML:2.0:ac:classes:SecondFactorOTPSMS
             urn:oasis:names:tc:SAML:2.0:ac:classes:SecondFactorIGTOKEN
           )
       },
       label: "Company Login" # optional label for SAML login button, defaults to "Saml"
     }
   ]
   ```

1. 保存文件并[重新配置](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)极狐GitLab，使更改生效。

### 源安装实例

1. 编辑 `config/gitlab.yml`：

   ```yaml
   omniauth:
     providers:
       - {
         name: 'saml',
         args: {
           assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
           idp_cert_fingerprint: '43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8',
           idp_sso_target_url: 'https://login.example.com/idp',
           issuer: 'https://gitlab.example.com',
           name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
           upstream_two_factor_authn_contexts:
             [
               'urn:oasis:names:tc:SAML:2.0:ac:classes:CertificateProtectedTransport',
               'urn:oasis:names:tc:SAML:2.0:ac:classes:SecondFactorOTPSMS',
               'urn:oasis:names:tc:SAML:2.0:ac:classes:SecondFactorIGTOKEN'
             ]
         },
         label: 'Company Login'  # optional label for SAML login button, defaults to "Saml"
       }
   ```

1. 保存文件并[重启极狐GitLab](../administration/restart_gitlab.md#installations-from-source)，使更改生效。

## 验证响应签名

IdP 必须签署 SAML 响应，来确保断言未被篡改。

这可以防止在需要特定群组成员身份时进行用户模拟和权限升级。

您可以使用 `idp_cert_fingerprint` 配置响应签名验证。
配置示例：

```yaml
args: {
  assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
  idp_cert_fingerprint: '43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8',
  idp_sso_target_url: 'https://login.example.com/idp',
  issuer: 'https://gitlab.example.com',
  name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
}
```

如果您的 IdP 不支持使用 `idp_cert_fingerprint` 配置它，您可以直接使用 `idp_cert` 配置极狐GitLab。配置示例：

```yaml
args: {
  assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
  idp_cert: '-----BEGIN CERTIFICATE-----
    <redacted>
    -----END CERTIFICATE-----',
  idp_sso_target_url: 'https://login.example.com/idp',
  issuer: 'https://gitlab.example.com',
  name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
}
```

如果您错误地配置了响应签名验证，您可能会看到如下错误消息：

- 密钥验证错误。
- Digest 不匹配。
- 指纹不匹配。

<!--
For more information on solving these errors, see the [troubleshooting SAML guide](../user/group/saml_sso/troubleshooting.md).
-->

## 自定义 SAML 设置

### 将用户重定向到 SAML 服务器进行身份验证

您可以将 `auto_sign_in_with_provider` 设置添加到极狐GitLab 配置中，自动将您重定向到 SAML 服务器进行身份验证，可以在实际登录之前，取消选择登录方式。

对于 Omnibus 安装实例：

```ruby
gitlab_rails['omniauth_auto_sign_in_with_provider'] = 'saml'
```

对于源安装实例：

```yaml
omniauth:
  auto_sign_in_with_provider: saml
```

每次登录尝试都会重定向到 SAML 服务器，因此您无法使用本地凭据登录。确保至少有一个 SAML 用户具有管理员访问权限。

您还可以通过 `https://gitlab.example.com/users/sign_in?auto_sign_in=false` 绕过自动登录功能。

<a id="map-saml-response-attribute-names"></a>

### 映射 SAML 响应属性名称 **(FREE SELF)**

您可以使用 `attribute_statements` 将 SAML 响应中的属性名称映射到 OmniAuth [`info` 哈希](https://github.com/omniauth/omniauth/wiki/Auth-Hash-Schema#schema-10-and-later)。

NOTE:
仅使用此设置，来映射属于 OmniAuth `info` 哈希模式的属性。

例如，如果您的 `SAMLResponse` 包含名为 `EmailAddress` 的属性，请指定 `{ email: ['EmailAddress'] }` 将属性映射到 info 哈希中的相应键。还支持以 URI 命名的属性，例如，`{ email: ['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'] }`。

使用此设置告诉极狐GitLab 在哪里查找创建帐户所需的某些属性。如果您的 IdP 将用户的电子邮件地址发送为 `EmailAddress` 而不是 `email`，请通过在您的配置中设置它来让极狐GitLab 知道：

```yaml
args: {
        assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
        idp_cert_fingerprint: '43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8',
        idp_sso_target_url: 'https://login.example.com/idp',
        issuer: 'https://gitlab.example.com',
        name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
        attribute_statements: { email: ['EmailAddress'] }
}
```

#### 设置用户名

默认情况下，SAML 响应中，电子邮件地址的本地部分用于生成用户的极狐GitLab 用户名。

在 `attribute_statements` 中配置 `nickname`，指定一个或多个包含用户所需用户名的属性：

```yaml
args: {
        assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
        idp_cert_fingerprint: '43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8',
        idp_sso_target_url: 'https://login.example.com/idp',
        issuer: 'https://gitlab.example.com',
        name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
        attribute_statements: { nickname: ['username'] }
}
```

这还将您的 SAML 响应中的 `username` 属性设置为极狐GitLab 中的用户名。

### 允许时钟漂移

IdP 的时钟可能会稍微超前您的系统时钟。
要允许少量时钟漂移，请在您的设置中使用 `allowed_clock_drift`。您必须以秒数和小数部分的形式输入参数值。
给定的值将添加到验证响应的当前时间。

```yaml
args: {
        assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
        idp_cert_fingerprint: '43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8',
        idp_sso_target_url: 'https://login.example.com/idp',
        issuer: 'https://gitlab.example.com',
        name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
        attribute_statements: { email: ['EmailAddress'] },
        allowed_clock_drift: 1  # for one second clock drift
}
```

### 为 `uid` 指定一个唯一的属性

默认情况下，`uid` 在 SAML 响应中设置为 `name_id`。要为 `uid` 指定唯一属性，您可以设置 `uid_attribute`。 在以下示例中，SAML 响应中的 `uid` 属性值设置为 `uid_attribute`。

```yaml
args: {
        assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
        idp_cert_fingerprint: '43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8',
        idp_sso_target_url: 'https://login.example.com/idp',
        issuer: 'https://gitlab.example.com',
        name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
        uid_attribute: 'uid'
}
```

在将 `uid` 设置为唯一属性之前，请确保您已配置以下属性，以便您的 SAML 用户无法更改它们：

- `NameID`
- 与 `omniauth_auto_link_saml_user` 一起使用时的 `Email`

如果用户可以更改这些属性，则他们可以作为其他授权用户登录。
有关如何使这些属性不可更改的信息，请参阅您的 SAML IdP 文档。

## 断言加密（可选）

极狐GitLab 需要使用 SAML 2.0 的 TLS 加密。有时，极狐GitLab 需要额外的断言加密。例如：

- 在负载均衡器上尽早终止 TLS 加密。
- 在您不希望出现在日志中的断言中包含敏感细节。

大多数组织在这一层不需要额外的加密。

SAML 集成支持 `EncryptedAssertion`。要加密您的断言，请在 SAML 设置中定义您的极狐GitLab 实例的私钥和公共证书。

定义密钥和证书时，将密钥文件中的所有换行符替换为 `\n`。
这使得密钥文件成为一个没有换行符的长字符串。

```yaml
args: {
  assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
  idp_cert_fingerprint: '43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8',
  idp_sso_target_url: 'https://login.example.com/idp',
  issuer: 'https://gitlab.example.com',
  name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
  certificate: '-----BEGIN CERTIFICATE-----\n<redacted>\n-----END CERTIFICATE-----',
  private_key: '-----BEGIN PRIVATE KEY-----\n<redacted>\n-----END PRIVATE KEY-----'
}
```

您的 IdP 使用极狐GitLab 的公共证书对断言进行加密。极狐GitLab 使用其私钥解密 `EncryptedAssertion`。

NOTE:
此集成使用 `certificate` 和 `private_key` 设置进行断言加密和请求签名。

## 签署 SAML 身份验证请求（可选）

您可以配置极狐GitLab 来签署 SAML 身份验证请求。此配置是可选的，因为极狐GitLab SAML 请求使用 SAML 重定向绑定。

实施签名：

1. 为您的极狐GitLab 实例创建用于 SAML 的私钥和公共证书。
1. 在配置的 `security` 部分配置签名设置。例如：

```yaml
args: {
  assertion_consumer_service_url: 'https://gitlab.example.com/users/auth/saml/callback',
  idp_cert_fingerprint: '43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8',
  idp_sso_target_url: 'https://login.example.com/idp',
  issuer: 'https://gitlab.example.com',
  name_identifier_format: 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
  certificate: '-----BEGIN CERTIFICATE-----\n<redacted>\n-----END CERTIFICATE-----',
  private_key: '-----BEGIN PRIVATE KEY-----\n<redacted>\n-----END PRIVATE KEY-----',
  security: {
    authn_requests_signed: true,  # enable signature on AuthNRequest
    want_assertions_signed: true,  # enable the requirement of signed assertion
    metadata_signed: false,  # enable signature on Metadata
    signature_method: 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
    digest_method: 'http://www.w3.org/2001/04/xmlenc#sha256',
  }
}
```

然后：

- 使用提供的私钥签署请求。
- 在您的 IdP 的元数据中包含配置的公共 x500 证书，以验证收到的请求的签名。

有关此选项的更多信息，请参阅 [Ruby SAML gem 文档](https://github.com/onelogin/ruby-saml/tree/v1.7.0)。

[OmniAuth SAML gem](https://github.com/omniauth/omniauth-saml) 使用 Ruby SAML gem 来实现 SAML 身份验证的客户端。

NOTE:
SAML 重定向绑定不同于 SAML POST 绑定。在 POST 绑定中，需要签名以防止中间人篡改请求。

## 为通过 SAML 创建的用户生成密码

极狐GitLab [为通过 SAML 创建的用户生成和设置密码](../security/passwords_for_integrated_authentication_methods.md)。

使用 SSO 或 SAML 进行身份验证的用户不得使用密码通过 HTTPS 进行 Git 操作。这些用户可以：

- 设置一个[个人访问令牌](../user/profile/personal_access_tokens.md)。
- 使用 [Git Credential Manager](../user/profile/account/two_factor_authentication.md#git-credential-manager)，它使用 OAuth 进行安全验证。

## 为现有用户关联 SAML 身份

用户可以按照为现有用户启用 OmniAuth 的步骤，手动将其 SAML 身份关联到现有的极狐GitLab 帐户。

## 在私有化部署实例上配置群组 SAML SSO **(PREMIUM SELF)**

如果您必须允许通过私有化部署实例上的多个 SAML IdP 进行访问，请使用群组 SAML SSO。

要配置群组 SAML SSO：

1. 使用 HTTPS 配置极狐GitLab。
1. 启用 OmniAuth 和 `group_saml` 提供商。

   对于 Omnibus 安装实例，编辑 `gitlab.rb`：

   ```ruby
   gitlab_rails['omniauth_enabled'] = true
   gitlab_rails['omniauth_providers'] = [{ name: 'group_saml' }]
   ```

   对于源安装实例，编辑 `gitlab/config/gitlab.yml`：

    ```yaml
    omniauth:
      enabled: true
      providers:
        - { name: 'group_saml' }
    ```

作为多租户解决方案，与推荐的实例范围 SAML 相比，私有化部署实例上的群组 SAML 受到限制。使用实例范围的 SAML 具有以下优势：

- [LDAP 兼容性](../administration/auth/ldap/index.md)。
- [LDAP 群组同步](../user/group/access_and_permissions.md#manage-group-memberships-via-ldap)。
- [Required 群组](#required-groups)。
- [Administrator 群组](#administrator-groups)。
- [Auditor 群组](#auditor-groups)。

<a id="additional-configuration-for-saml-apps-on-your-idp"></a>

## IdP 上 SAML 应用程序的其他配置

在 IdP 上配置 SAML 应用程序时，您的 IdP 可能需要额外的配置，例如：

| 字段 | 值 | 说明 |
|-------|-------|-------|
| SAML profile | Web 浏览器 SSO 配置文件 | 极狐GitLab 使用 SAML 使用户通过浏览器登录。不会直接向 IdP 发出请求。 |
| SAML request binding | HTTP 重定向 | 极狐GitLab（SP）使用 base64 编码的 `SAMLRequest` HTTP 参数，将用户重定向到您的 IdP。 |
| SAML response binding | HTTP POST | 指定您的 IdP 如何发送 SAML 令牌。包括用户浏览器提交回极狐GitLab 的 `SAMLResponse`。 |
| Sign SAML response | Required | 防止篡改。 |
| X.509 certificate in response | Required | 签署响应并根据提供的指纹检查响应。 |
| Fingerprint algorithm | SHA-1 | 极狐GitLab 使用证书的 SHA-1 哈希来签署 SAML 响应。 |
| Signature algorithm | SHA-1/SHA-256/SHA-384/SHA-512 | 确定响应的签名方式。也称为摘要方法，可以在 SAML 响应中指定。 |
| Encrypt SAML assertion | Optional | 在您的身份提供上、用户的浏览器和极狐GitLab 之间使用 TLS。 |
| Sign SAML assertion | Optional | 验证 SAML 断言的完整性。有效时，签署整个响应。 |
| Check SAML request signature | Optional | 检查 SAML 响应上的签名。 |
| Default RelayState | Optional | 指定用户在您的 IdP 通过 SAML 成功登录后，最终应该到达的 URL。 |
| NameID format | Persistent | <!--See [NameID format details](../user/group/saml_sso/index.md#nameid-format).--> |
| Additional URLs | Optional | 可能在某些提供程序的其他字段中包含 issuer、标识符或 assertion consumer 服务 URL。 |

有关配置示例，请参阅[有关特定提供商的说明](#set-up-identity-providers)。

<a id="glossary-of-common-terms"></a>

## 常用术语表

| 术语                           | 描述 |
|--------------------------------|-------------|
| 身份提供商（IdP）        | 管理您的用户身份的服务，例如 Okta 或 OneLogin。 |
| 服务提供商（SP）          | 使用来自 SAML IdP（例如 Okta）的断言来对用户进行身份验证。您可以将极狐GitLab 配置为 SAML 2.0 SP。 |
| 断言（Assertion）                      | 有关用户身份的一条信息，例如他们的姓名或角色。也称为声明或属性。 |
| 单点登录（SSO）          | 身份验证方案的名称。 |
| Assertion consumer 服务 URL | 极狐GitLab 上的回调，用户在成功通过 IdP 进行身份验证后被重定向。 |
| Issuer                         | 极狐GitLab 如何在 IdP 上标识自己。也称为“依赖方信任标识符”。 |
| Certificate fingerprint        | 通过检查服务器是否使用正确的证书对通信进行签名，确认基于 SAML 的通信是安全的。也称为证书指纹。 |

<!--
## Troubleshooting

See our [troubleshooting SAML guide](../user/group/saml_sso/troubleshooting.md).
-->
