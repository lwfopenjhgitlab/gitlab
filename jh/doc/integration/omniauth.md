---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# OmniAuth **(FREE SELF)**

<!--
GitLab leverages OmniAuth to allow users to sign in using Twitter, GitHub, and
other popular services. [OmniAuth](https://rubygems.org/gems/omniauth/) is a
"generalized Rack framework for multiple-provider authentication" built on Ruby.

Configuring OmniAuth does not prevent standard GitLab authentication or LDAP
(if configured) from continuing to work. Users can choose to sign in using any
of the configured mechanisms.

- [Initial OmniAuth Configuration](#initial-omniauth-configuration)
- [Supported Providers](#supported-providers)
- [Enable OmniAuth for an Existing User](#enable-omniauth-for-an-existing-user)
- [OmniAuth configuration sample when using Omnibus GitLab](https://gitlab.com/gitlab-org/omnibus-gitlab/tree/master#omniauth-google-twitter-github-login)
- [Enable or disable Sign In with an OmniAuth provider without disabling import sources](#enable-or-disable-sign-in-with-an-omniauth-provider-without-disabling-import-sources)
-->

<a id="supported-providers"></a>

## 支持的提供商

这是当前支持的 OmniAuth 提供商的列表。在继续阅读每个提供商的文档之前，请务必先阅读本文档，因为它包含所有提供商通用的一些设置。

| 提供商                                              | OmniAuth 提供商名称    |
|---------------------------------------------------------------------|----------------------------|
| Alicloud                                                 | `alicloud`                   |
| [Bitbucket Cloud](bitbucket.md)                                     | `bitbucket`                |
| [DingTalk](ding_talk.md)                                            | `dingtalk`                |
| GitHub                                                 | `github`                   |
| GitLab.com                                             | `gitlab`                   |
| [SAML](saml.md)                                                     | `saml`                     |

<a id="configure-common-settings"></a>

## 普通配置

在配置 OmniAuth 提供商之前，配置对所有提供商通用的设置。

Linux 软件包、Docker 和自编译 | Helm chart | 描述                                                                                                                                              | 默认值
----------------------------|------------|-------------------------------------------------------------------------------------------------------------------------------------------------|-----------
`allow_single_sign_on`     | `allowSingleSignOn` | 自动创建极狐GitLab 账户的提供商列表。提供商名称可在[支持的提供商列表](#supported-providers)的 **OmniAuth 提供商名称**列中找到。                                                          | `false`，意味着不允许在没有预先存在的极狐GitLab 账户的情况下使用 OmniAuth 提供商账户登录。您必须先创建一个极狐GitLab 账户，然后通过您的配置文件设置将其连接到您的 OmniAuth 提供商账户。
`auto_link_ldap_user`      | `autoLinkLdapUser` | 在极狐GitLab 中为通过 OmniAuth 提供商创建的用户创建 LDAP 身份。如果启用了 [LDAP 集成](../administration/auth/ldap/index.md)，则可以启用此设置。要求用户的 `uid` 在 LDAP 和 OmniAuth 提供商中相同。 | `false`。
`block_auto_created_users` | `blockAutoCreatedUsers` | 除非管理员批准，阻止自动创建的用户登录。                                                                                                                            | `true`。如果将值设置为 `false`，请确保定义您可以控制的提供商，如 SAML 或 Google。否则，因特网上的任何用户都能在没有管理员批准的情况下登录进入极狐GitLab。


<a id="configure-initial-settings"></a>

## 初始化配置

NOTE:
在 11.4 及更高版本中，OmniAuth 默认处于启用状态。如果您使用的是早期版本，则必须明确启用它。

在配置 OmniAuth provider 之前，请配置所有提供商通用的设置。

设置                    | 描述 | 默认值
---------------------------|-------------|--------------
`allow_single_sign_on`     | 使您能够列出自动创建极狐GitLab 帐户的提供程序。提供者名称在[支持的提供商列表](#支持的提供商)的 **OmniAuth 提供商名称** 列中可用。 | 默认值为`false`。 如果为`false`，则必须手动创建用户，否则他们无法使用 OmniAuth 登录。
`auto_link_ldap_user`      | 如果启用，则在极狐GitLab 中为通过 OmniAuth 提供商创建的用户创建 LDAP 身份。如果您启用了 [LDAP (ActiveDirectory)](../administration/auth/ldap/index.md) 集成，则可以启用此设置。 要求用户的 `uid` 在 LDAP 和 OmniAuth 提供商中都相同。 | 默认值为 `false`。
`block_auto_created_users` | 如果启用，将阻止自动创建的用户登录，直到他们获得管理员批准。 | 默认值为 `true`。如果您将该值设置为 `false`，请确保只为您可以控制的 `allow_single_sign_on` 定义 provider。否则，互联网上的任何用户都可以在未经管理员批准的情况下登录极狐GitLab。

要更改这些设置：

- **对于 Omnibus 安装实例**

  1. 打开配置文件：

     ```shell
     sudo editor /etc/gitlab/gitlab.rb
     ```

  1. 更新以下部分：

     ```ruby
     # CAUTION!
     # This allows users to sign in without having a user account first. Define the allowed providers
     # using an array, for example, ["github", "gitlab"], or as true/false to allow all providers or none.
     # User accounts will be created automatically when authentication was successful.
     gitlab_rails['omniauth_allow_single_sign_on'] = ['github', 'gitlab']
     gitlab_rails['omniauth_auto_link_ldap_user'] = true
     gitlab_rails['omniauth_block_auto_created_users'] = true
     ```

  1. 保存文件并重新配置极狐GitLab：

     ```shell
     sudo gitlab-ctl reconfigure
     ```


- **对于 Helm Chart 实例**

  1. 导出 Helm 值：

     ```shell
     helm get values gitlab > gitlab_values.yaml
     ```

  1. 编辑 `gitlab_values.yaml`，并更新 `globals.appConfig` 下的 `omniauth` 部分：

     ```yaml
     global:
       appConfig:
         omniauth:
           enabled: true
           allowSingleSignOn: ['saml', 'twitter']
           autoLinkLdapUser: false
           blockAutoCreatedUsers: true
     ```

     获取更多信息，查看[文档](https://docs.gitlab.cn/charts/charts/globals.html#omniauth)。

- **对于源安装实例**

  1. 打开配置文件：

     ```shell
     cd /home/git/gitlab

     sudo -u git -H editor config/gitlab.yml
     ```

  1. 更新以下部分：

     ```yaml
     ## OmniAuth settings
     omniauth:
       # Allow sign-in by using Twitter, Google, etc. using OmniAuth providers
       # Versions prior to 11.4 require this to be set to true
       # enabled: true

       # CAUTION!
       # This allows users to sign in without having a user account first. Define the allowed providers
       # using an array, for example, ["github", "gitlab"], or as true/false to allow all providers or none.
       # User accounts will be created automatically when authentication was successful.
       allow_single_sign_on: ["github", "gitlab"]

       auto_link_ldap_user: true

       # Locks down those users until they have been cleared by the admin (default: true).
       block_auto_created_users: true
     ```

  1. 保存文件并重启极狐GitLab：

     ```shell
     # For systems running systemd
     sudo systemctl restart gitlab.target

     # For systems running SysV init
     sudo service gitlab restart
     ```

配置完这些设置后，您可以配置您选择的[提供商](#supported-providers)。


### 每个提供商的配置

> 引入于 15.3 版本。

如果设置了 `allow_single_sign_on`，极狐GitLab 使用 OmniAuth 的 `auth_hash` 中返回的以下字段之一，在极狐GitLab 中为用户登录建立用户名，选择第一个存在的用户名：

- `username`
- `nickname`
- `email`

您可以在每个提供商的基础上创建极狐GitLab 配置，使用 `args` 将其提供给[提供商](#supported-providers)。如果您在 `args` 中为提供商设置 `gitlab_username_claim` 变量，您可以选择另一个声明用于极狐GitLab 用户名。所选择的声明必须是唯一的，以避免冲突。

- **Omnibus 安装实例**

  ```ruby
  gitlab_rails['omniauth_providers'] = [

    # The generic pattern for configuring a provider with name PROVIDER_NAME

    gitlab_rails['omniauth_providers'] = {
      name: "PROVIDER_NAME"
      ...
      args: { gitlab_username_claim: 'sub' } # For users signing in with the provider you configure, the GitLab username will be set to the "sub" received from the provider
    },

    # Here are examples using GitHub and Kerberos

    gitlab_rails['omniauth_providers'] = {
      name: "github"
      ...
      args: { gitlab_username_claim: 'name' } # For users signing in with GitHub, the GitLab username will be set to the "name" received from GitHub
    },
    {
      name: "kerberos"
      ...
      args: { gitlab_username_claim: 'uid' } # For users signing in with Kerberos, the GitLab username will be set to the "uid" received from Kerberos
    },
  ]
  ```

- **源安装实例**

  ```yaml
  - { name: 'PROVIDER_NAME',
    ...
    args: { gitlab_username_claim: 'sub' }
  }
  - { name: 'github',
    ...
    args: { gitlab_username_claim: 'name' }
  }
  - { name: 'kerberos',
    ...
    args: { gitlab_username_claim: 'uid' }
  }
  ```

### 通过 OmniAuth 创建的用户密码

[为通过集成身份验证创建的用户生成的密码](../security/passwords_for_integrated_authentication_methods.md)指南，概述了极狐GitLab 如何为使用 OmniAuth 创建的用户生成和设置密码。

## 为现有用户启用 OmniAuth

如果您是现有用户，在创建极狐GitLab 帐户后，您可以激活 OmniAuth 提供商。例如，您最初使用 LDAP 登录，则可以启用 OmniAuth 提供商。

1. 使用您的极狐GitLab 凭据、LDAP 或其他 OmniAuth 提供商登录极狐GitLab。
1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏中，选择 **帐户**。
1. 在 **关联帐户** 部分，选择 OmniAuth 提供商。
1. 您被重定向到提供商。授权极狐GitLab 后，您将被重定向回极狐GitLab。

您现在可以使用您选择的 OmniAuth 提供商登录极狐GitLab。

<a id="enable-or-disable-sign-in-with-an-omniauth-provider-without-disabling-import-sources"></a>

## 在不禁用导入源的情况下启用或禁用 OmniAuth 提供商登录

管理员可以为某些 OmniAuth 提供商启用或禁用登录。

NOTE:
默认情况下，为 `config/gitlab.yml` 中配置的所有 OAuth provider 启用登录。

要启用或禁用 OmniAuth provider：

1. 在顶部栏上，选择 **菜单 > 管理员**。
1. 在左侧边栏，选择 **设置**。
1. 展开 **登录限制**。
1. 在 **启用 OAuth 身份验证源** 部分，选中或清除您要启用或禁用的每个 provider 的复选框。

## 禁用 OmniAuth

默认情况下启用 OmniAuth。但是，OmniAuth 仅在提供商已配置且[启用](#enable-or-disable-sign-in-with-an-omniauth-provider-without-disabling-import-sources)时才有效。

如果 OmniAuth 提供商在单独禁用时仍导致问题，您可以通过修改配置文件来禁用整个 OmniAuth 子系统：

- **Omnibus 安装实例**

  ```ruby
  gitlab_rails['omniauth_enabled'] = false
  ```

- **源安装实例**

  ```yaml
  omniauth:
    enabled: false
  ```

## 将现有用户关联到 OmniAuth 用户

> 引入于 13.4 版本。

如果电子邮件地址匹配，您可以自动将 OmniAuth 用户与现有的极狐GitLab 用户关联起来。

以下示例为 OpenID Connect 提供商和 Twitter OAuth 提供商启用自动关联。

- **Omnibus 安装实例**

  ```ruby
  gitlab_rails['omniauth_auto_link_user'] = ["openid_connect", "twitter"]
  ```

- **源安装实例**

  ```yaml
  omniauth:
    auto_link_user: ["openid_connect", "twitter"]
  ```

这种启用自动关联的方法适用于除 SAML 之外的所有提供商。
要为 SAML 启用自动链接，请参阅 [SAML 设置说明](saml.md#configure-saml-support-in-gitlab)。

## 创建外部提供商列表

您可以定义一个外部 OmniAuth 提供商列表。
通过列出的提供商创建帐户或登录极狐GitLab 的用户，无法访问[内部项目](../user/public_access.md#internal-projects-and-groups)。

要定义外部提供商列表，请使用提供商的全名。有关提供商名称，请参阅[支持的提供商列表](#supported-providers)中的 **OmniAuth 提供商名称**列。

NOTE:
如果您从外部提供商列表中删除 OmniAuth 提供商，则必须手动更新使用此登录方法的用户，以便他们的帐户升级为完整的内部帐户。

- **Omnibus 安装实例**

  ```ruby
  gitlab_rails['omniauth_external_providers'] = ['twitter', 'google_oauth2']
  ```

- **源安装实例**

  ```yaml
  omniauth:
    external_providers: ['twitter', 'google_oauth2']
  ```

## 使用自定义 OmniAuth 提供商

NOTE:
以下信息仅适用于源安装实例。

如果您必须与极狐GitLab 随附的 [OmniAuth](https://github.com/omniauth/omniauth) 提供商以外的身份验证解决方案集成，您可以使用自定义 OmniAuth 提供商。

以下步骤是通用的。阅读 OmniAuth 提供商的文档以了解具体的实施细节。

1. 停止极狐GitLab：

   ```shell
   sudo service gitlab stop
   ```

1. 将 gem 添加到您的 [`Gemfile`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/Gemfile)：

   ```shell
   gem "omniauth-your-auth-provider"
   ```

1. 安装新的 OmniAuth 提供商 gem：

   ```shell
   sudo -u git -H bundle install --without development test mysql --path vendor/bundle --no-deployment
   ```

   这些命令与在初始安装期间安装 gems 的命令相同，使用 `--path vendor/bundle --no-deployment` 而不是 `--deployment`。

1. 启动极狐GitLab：

   ```shell
   sudo service gitlab start
   ```

<!--
### Custom OmniAuth provider examples

If you have successfully set up a provider that is not already integrated with GitLab,
let us know.

We can't officially support every possible authentication mechanism available,
but we'd like to at least help those with specific needs.
-->

## 使 OmniAuth 用户配置文件保持最新

您可以从选定的 OmniAuth 提供商启用配置文件同步。您可以同步所有或特定的用户信息。

使用 LDAP 进行身份验证时，用户名和电子邮件始终同步。

- **Omnibus 安装实例**

  ```ruby
  gitlab_rails['omniauth_sync_profile_from_provider'] = ['twitter', 'google_oauth2']
  gitlab_rails['omniauth_sync_profile_attributes'] = ['name', 'email', 'location']
  ```

- **源安装实例**

  ```yaml
  omniauth:
    sync_profile_from_provider: ['twitter', 'google_oauth2']
    sync_profile_attributes: ['email', 'location']
  ```

## 绕过双重身份验证

通过某些 OmniAuth 提供商，用户可以在不使用双重身份验证 (2FA) 的情况下登录。

由于一个已知问题，用户必须在他们的极狐GitLab 帐户上[设置 2FA](../user/profile/account/two_factor_authentication.md#enable-two-factor-authentication) 来绕过 2FA。否则，系统会提示他们在登录极狐GitLab 时设置 2FA。

要绕过 2FA，您可以：

- 使用数组定义允许的提供商（例如，`['twitter', 'google_oauth2']`）。
- 指定 `true` 以允许所有提供商，或指定 `false` 不允许任何提供商。

应该仅为已经拥有 2FA 的提供商配置此选项。默认值为 `false`。

此配置不适用于 SAML。

- **Omnibus 安装实例**

  ```ruby
  gitlab_rails['omniauth_allow_bypass_two_factor'] = ['twitter', 'google_oauth2']
  ```

- **源安装实例**

  ```yaml
  omniauth:
    allow_bypass_two_factor: ['twitter', 'google_oauth2']
  ```

## 自动使用提供商登录

您可以将 `auto_sign_in_with_provider` 设置添加到您的极狐GitLab 配置中，将登录请求重定向到您的 OmniAuth 提供商，来进行身份验证。这样就无需在登录前选择提供商。

例如，为 [Azure v2 集成](azure.md)启用自动登录：

- **Omnibus 安装实例**

  ```ruby
  gitlab_rails['omniauth_auto_sign_in_with_provider'] = 'azure_activedirectory_v2'
  ```

- **源安装实例**

  ```yaml
  omniauth:
    auto_sign_in_with_provider: azure_activedirectory_v2
  ```

请记住，每次登录尝试都会重定向到 OmniAuth 提供商，因此您无法使用本地凭据登录。确保至少有一个 OmniAuth 用户是管理员。

您还可以通过浏览到 `https://gitlab.example.com/users/sign_in?auto_sign_in=false` 来绕过自动登录。

## 使用自定义 OmniAuth 提供商图标

大多数受支持的提供商都包含一个用于呈现登录按钮的内置图标。

要使用您自己的图标，请确保您的图像针对 64 x 64 像素的渲染进行了优化，然后通过以下两种方式之一覆盖该图标：

- **提供自定义图像路径**：

  1. 如果您在极狐GitLab 服务器域之外托管图像，请确保您的内容安全策略配置为允许访问图像文件。
  1. 根据您安装极狐GitLab 的方法，将自定义 `icon` 参数添加到极狐GitLab 配置文件中。<!--Read [OpenID Connect OmniAuth provider](../administration/auth/oidc.md)
     for an example for the OpenID Connect provider.-->

- **直接在配置文件中嵌入图像**：此示例创建图像的 Base64 编码版本，您可以通过[数据 URL](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs) 提供服务：

  1. 使用 GNU `base64` 命令（例如 `base64 -w 0 <logo.png>`）对图像文件进行编码，该命令返回单行 `<base64-data>` 字符串。
  1. 将 Base64 编码的数据添加到极狐GitLab 配置文件中的自定义 `icon` 参数：

     ```yaml
     omniauth:
       providers:
         - { name: '...'
             icon: 'data:image/png;base64,<base64-data>'
             ...
           }
     ```

## 更改应用程序或配置

由于极狐GitLab 中的 OAuth 不支持设置与多个提供上相同的外部认证和授权提供商，因此如果提供商或应用程序发生变化，则必须同时更新极狐GitLab 配置和用户标识。
例如，您可以设置 `saml` 和 `azure_activedirectory_v2`，但不能将第二个 `azure_activedirectory_v2` 添加到同一配置中。

这些说明适用于极狐GitLab 存储 `extern_uid` 的所有身份验证方法，它是唯一用于用户身份验证的数据。

在提供商内更改应用程序时，如果用户 `extern_uid` 没有更改，则只需更新极狐GitLab 配置。

交换配置：

1. 更改 `gitlab.rb` 文件中的提供商配置。
1. 为所有在极狐GitLab 中具有先前提供商身份的用户更新 `extern_uid`。

要查找 `extern_uid`，请查看现有用户的当前 `extern_uid`，以获得与您当前提供商中同一用户的相应字段相匹配的 ID。

有两种方法可以更新 `extern_uid`：

- 使用[用户 API](../api/users.md#user-modification)。传递提供商名称和新的 `extern_uid`。
- 使用 [Rails 控制台](../administration/operations/rails_console.md)：

  ```ruby
  Identity.where(extern_uid: 'old-id').update!(extern_uid: 'new-id')`
  ```

## 限制

大多数受支持的 OmniAuth 提供商不支持 Git over HTTP 密码身份验证。
作为解决方法，您可以使用[个人访问令牌](../user/profile/personal_access_tokens.md)进行身份验证。

