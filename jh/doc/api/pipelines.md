---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

<!--
# Pipelines API **(FREE)**

## Pipelines pagination

By default, `GET` requests return 20 results at a time because the API results
are paginated.

Read more on [pagination](rest/index.md#pagination).
-->

# 流水线 API **(FREE)**

<a id="pipelines-pagination"></a>

## 流水线分页机制

在默认情况下，`GET` 请求每次只返回 20 条结果，这是因为我们采用了分页机制。

阅读[分页](rest/index.md#pagination)，了解更多详情。

<!--
## List project pipelines

> `iid` in response [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/342223) in GitLab 14.6.

List pipelines in a project. Child pipelines are not included in the results,
but you can [get child pipeline](pipelines.md#get-a-single-pipeline) individually.
-->

<a id="list-project-pipelines"></a>

## 列出项目流水线

> - 响应中的 `iid` 引入于极狐GitLab 14.6。
> - 请求和响应中的 `name` 引入于极狐GitLab 15.11，[功能标志](../administration/feature_flags.md)为 `pipeline_name_in_api`。默认禁用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下 `name` 字段不可用。
要使其可用，请要求管理员启用名为 `pipeline_name_in_api` 的[功能标志](../administration/feature_flags.md)。此功能尚未准备好用于生产。
在 JiHuLab.com 上，此功能不可用。

列出项目中的流水线。结果中不包含子流水线，但您可以单独[获取子流水线](pipelines.md#get-a-single-pipeline)。

```plaintext
GET /projects/:id/pipelines
```

| 参数 | 类型             | 是否必需 | 描述                                                                                                                                                                                                                                |
|-----------|----------------|------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`      | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)                                                                                                                                                       |
| `scope`   | string         | no   | 流水线的范围，可接受的值有 `running`、`pending`、`finished`、`branches`、`tags`                                                                                                                                                                    |
| `status`  | string         | no   | 流水线的状态，可接受的值有 `created`、`waiting_for_resource`、`preparing`、`pending`、`running`、`success`、`failed`、`canceled`、`skipped`、`manual`、`scheduled`                                                                                       |
| `source`  | string         | no   | 引入于 14.3 版本。流水线触发方式，可接受的值有 `push`、`web`、`trigger`、`schedule`、`api`、`external`、`pipeline`、`chat`、`webide`、`merge_request_event`、`external_pull_request_event`、`parent_pipeline`、`ondemand_dast_scan` 或者 `ondemand_dast_validation` |
| `ref`     | string         | no   | 流水线的 ref                                                                                                                                                                                                                          |
| `sha`     | string         | no   | 流水线的 SHA                                                                                                                                                                                                                          |
| `yaml_errors`| boolean        | no   | 返回具有无效配置的流水线                                                                                                                                                                                                                      |
| `username`| string         | no   | 触发流水线用户的用户名                                                                                                                                                                                                                       |
| `updated_after` | datetime       | no   | 对于给定的时间戳，返回不早于该时间更新的流水线。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                                                |
| `updated_before` | datetime       | no   | 对于给定的时间戳，返回不晚于该时间更新的流水线。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                                                |
| `name`  | string         | no   | 返回具有指定名称的流水线。引入于 15.11，默认不可用                                                                                                                                                                                                    |
| `order_by`| string         | no   | 按照 `id`、`status`、`ref`、`updated_at` 或者 `user_id`（默认：`id`）对流水线排序                                                                                                                                                                   |
| `sort`    | string         | no   | 按照 `asc` 或者 `desc`（默认：`desc`）对流水线排序                                                                                                                                                                                               |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/pipelines"
```

响应示例：

```json
[
  {
    "id": 47,
    "iid": 12,
    "project_id": 1,
    "status": "pending",
    "source": "push",
    "ref": "new-pipeline",
    "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
    "name": "Build pipeline",
    "web_url": "https://example.com/foo/bar/pipelines/47",
    "created_at": "2016-08-11T11:28:34.085Z",
    "updated_at": "2016-08-11T11:32:35.169Z"
  },
  {
    "id": 48,
    "iid": 13,
    "project_id": 1,
    "status": "pending",
    "source": "web",
    "ref": "new-pipeline",
    "sha": "eb94b618fb5865b26e80fdd8ae531b7a63ad851a",
    "name": "Build pipeline",
    "web_url": "https://example.com/foo/bar/pipelines/48",
    "created_at": "2016-08-12T10:06:04.561Z",
    "updated_at": "2016-08-12T10:09:56.223Z"
  }
]
```

<!--
## Get a single pipeline

> `iid` in response [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/342223) in GitLab 14.6.

Get one pipeline from a project.

You can also get a single [child pipeline](../ci/pipelines/parent_child_pipelines.md).
[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/36494) in GitLab 13.3.
-->

<a id="get-a-single-pipeline"></a>

## 获取单个流水线

> - 响应中的 `iid` 引入于极狐GitLab 14.6。
> - 响应中的 `name` 引入于极狐GitLab 15.11，[功能标志](../administration/feature_flags.md)为 `pipeline_name_in_api`。默认禁用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下 `name` 字段不可用。
要使其可用，请要求管理员启用名为 `pipeline_name_in_api` 的[功能标志](../administration/feature_flags.md)。此功能尚未准备好用于生产。
在 JiHuLab.com 上，此功能不可用。

您也可以获取单个[子流水线](../ci/pipelines/downstream_pipelines.md#parent-child-pipelines))。
引入于 13.3 版本。

```plaintext
GET /projects/:id/pipelines/:pipeline_id
```

| 参数 | 类型 | 是否必需 | 描述 |
|------------|---------|----------|---------------------|
| `id`       | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `pipeline_id` | integer | yes      | 流水线的 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/pipelines/46"
```

响应示例：

```json
{
  "id": 46,
  "iid": 11,
  "project_id": 1,
  "name": "Build pipeline",
  "status": "success",
  "ref": "main",
  "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
  "before_sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
  "tag": false,
  "yaml_errors": null,
  "user": {
    "name": "Administrator",
    "username": "root",
    "id": 1,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://localhost:3000/root"
  },
  "created_at": "2016-08-11T11:28:34.085Z",
  "updated_at": "2016-08-11T11:32:35.169Z",
  "started_at": null,
  "finished_at": "2016-08-11T11:32:35.145Z",
  "committed_at": null,
  "duration": 123.65,
  "queued_duration": 0.010,
  "coverage": "30.0",
  "web_url": "https://example.com/foo/bar/pipelines/46"
}
```

<!--
### Get variables of a pipeline
-->

<a id="get-variables-of-a-pipeline"></a>

### 获取流水线的变量

```plaintext
GET /projects/:id/pipelines/:pipeline_id/variables
```

| 参数 | 类型 | 是否必需 | 描述 |
|------------|---------|----------|---------------------|
| `id`       | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `pipeline_id` | integer | yes      | 流水线的 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/pipelines/46/variables"
```

Example of response

```json
[
  {
    "key": "RUN_NIGHTLY_BUILD",
    "variable_type": "env_var",
    "value": "true"
  },
  {
    "key": "foo",
    "value": "bar"
  }
]
```

<!--
### Get a pipeline's test report

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/202525) in GitLab 13.0.

NOTE:
This API route is part of the [Unit test report](../ci/testing/unit_test_reports.md) feature.
-->

<a id="get-a-pipelines-test-report"></a>

### 获取流水线的测试报告

> 引入于 13.0 版本。

NOTE:
此 API 路由已经成为[单元测试报告](../ci/testing/unit_test_reports.md)特性中的一部分。

```plaintext
GET /projects/:id/pipelines/:pipeline_id/test_report
```

| 参数 | 类型 | 是否必需 | 描述 |
|------------|---------|----------|---------------------|
| `id`       | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `pipeline_id` | integer | yes      | 流水线的 ID |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/pipelines/46/test_report"
```

响应示例：

```json
{
  "total_time": 5,
  "total_count": 1,
  "success_count": 1,
  "failed_count": 0,
  "skipped_count": 0,
  "error_count": 0,
  "test_suites": [
    {
      "name": "Secure",
      "total_time": 5,
      "total_count": 1,
      "success_count": 1,
      "failed_count": 0,
      "skipped_count": 0,
      "error_count": 0,
      "test_cases": [
        {
          "status": "success",
          "name": "Security Reports can create an auto-remediation MR",
          "classname": "vulnerability_management_spec",
          "execution_time": 5,
          "system_output": null,
          "stack_trace": null
        }
      ]
    }
  ]
}
```

<!--
### Get a pipeline's test report summary

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/65471) in GitLab 14.2.

NOTE:
This API route is part of the [Unit test report](../ci/testing/unit_test_reports.md) feature.
-->

### 获取流水线的测试报告摘要

> 引入于 14.2 版本。

NOTE:
此 API 路由已经成为[单元测试报告](../ci/testing/unit_test_reports.md)特性中的一部分。

```plaintext
GET /projects/:id/pipelines/:pipeline_id/test_report_summary
```

| 参数 | 类型 | 是否必需 | 描述 |
|------------|---------|----------|---------------------|
| `id`       | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `pipeline_id` | integer | yes      | 流水线的 ID |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/pipelines/46/test_report_summary"
```

响应示例：

```json
{
    "total": {
        "time": 1904,
        "count": 3363,
        "success": 3351,
        "failed": 0,
        "skipped": 12,
        "error": 0,
        "suite_error": null
    },
    "test_suites": [
        {
            "name": "test",
            "total_time": 1904,
            "total_count": 3363,
            "success_count": 3351,
            "failed_count": 0,
            "skipped_count": 12,
            "error_count": 0,
            "build_ids": [
                66004
            ],
            "suite_error": null
        }
    ]
}
```

## 获取最新的流水线

> 响应中的 `name` 引入于极狐GitLab 15.11，[功能标志](../administration/feature_flags.md)为 `pipeline_name_in_api`。默认禁用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下 `name` 字段不可用。
要使其可用，请要求管理员启用名为 `pipeline_name_in_api` 的[功能标志](../administration/feature_flags.md)。此功能尚未准备好用于生产。
在 JiHuLab.com 上，此功能不可用。

获取项目中特定引用的最新流水线。

```plaintext
GET /projects/:id/pipelines/latest
```

| 参数    | 类型     | 是否必需 | 描述                                                                                                                               |
|-------|--------|------|----------------------------------------------------------------------------------------------------------------------------------|
| `ref` | string | no   | 检查最新流水线的分支或标签。未指定时默认为默认分支。 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/pipelines/latest"
```

响应示例：

```json
{
    "id": 287,
    "iid": 144,
    "project_id": 21,
    "name": "Build pipeline",
    "sha": "50f0acb76a40e34a4ff304f7347dcc6587da8a14",
    "ref": "main",
    "status": "success",
    "source": "push",
    "created_at": "2022-09-21T01:05:07.200Z",
    "updated_at": "2022-09-21T01:05:50.185Z",
    "web_url": "http://127.0.0.1:3000/test-group/test-project/-/pipelines/287",
    "before_sha": "8a24fb3c5877a6d0b611ca41fc86edc174593e2b",
    "tag": false,
    "yaml_errors": null,
    "user": {
        "id": 1,
        "username": "root",
        "name": "Administrator",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
        "web_url": "http://127.0.0.1:3000/root"
    },
    "started_at": "2022-09-21T01:05:14.197Z",
    "finished_at": "2022-09-21T01:05:50.175Z",
    "committed_at": null,
    "duration": 34,
    "queued_duration": 6,
    "coverage": null,
    "detailed_status": {
        "icon": "status_success",
        "text": "passed",
        "label": "passed",
        "group": "success",
        "tooltip": "passed",
        "has_details": false,
        "details_path": "/test-group/test-project/-/pipelines/287",
        "illustration": null,
        "favicon": "/assets/ci_favicons/favicon_status_success-8451333011eee8ce9f2ab25dc487fe24a8758c694827a582f17f42b0a90446a2.png"
    }
}
```

<a id="create-a-new-pipeline"></a>

## 创建新的流水线

> 响应中的 `iid` 引入于 14.6 版本。

```plaintext
POST /projects/:id/pipeline
```

| 参数 | 类型 | 是否必需 | 描述                                                                                                                                                                                       |
|-------------|---------|----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`        | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)                                                                                                                   |
| `ref`       | string  | yes      | 流水线执行使用的分支或标签                                                                                                                                                                            |
| `variables` | array   | no       | 包含流水线中可用变量的[哈希数组](rest/index.md#array-of-hashes)，匹配结构 `[{ 'key': 'UPLOAD_TO_S3', 'variable_type': 'file', 'value' : 'true' }, {'key': 'TEST', 'value': '测试变量'}]`。如果排除了 `variable_type`，则默认为 `env_var`。 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/pipeline?ref=main"
```

响应示例：

```json
{
  "id": 61,
  "iid": 21,
  "project_id": 1,
  "sha": "384c444e840a515b23f21915ee5766b87068a70d",
  "ref": "main",
  "status": "pending",
  "before_sha": "0000000000000000000000000000000000000000",
  "tag": false,
  "yaml_errors": null,
  "user": {
    "name": "Administrator",
    "username": "root",
    "id": 1,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://localhost:3000/root"
  },
  "created_at": "2016-11-04T09:36:13.747Z",
  "updated_at": "2016-11-04T09:36:13.977Z",
  "started_at": null,
  "finished_at": null,
  "committed_at": null,
  "duration": null,
  "queued_duration": 0.010,
  "coverage": null,
  "web_url": "https://example.com/foo/bar/pipelines/61"
}
```

<!--
## Retry jobs in a pipeline

> `iid` in response [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/342223) in GitLab 14.6.
-->

<a id="retry-jobs-in-a-pipeline"></a>

## 重试流水线中的作业

> 响应中的 `iid` 引入于 14.6 版本。

```plaintext
POST /projects/:id/pipelines/:pipeline_id/retry
```

| 参数 | 类型 | 是否必需 | 描述 |
|------------|---------|----------|---------------------|
| `id`       | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `pipeline_id` | integer | yes   | 流水线的 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/pipelines/46/retry"
```

响应示例：

```json
{
  "id": 46,
  "iid": 11,
  "project_id": 1,
  "status": "pending",
  "ref": "main",
  "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
  "before_sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
  "tag": false,
  "yaml_errors": null,
  "user": {
    "name": "Administrator",
    "username": "root",
    "id": 1,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://localhost:3000/root"
  },
  "created_at": "2016-08-11T11:28:34.085Z",
  "updated_at": "2016-08-11T11:32:35.169Z",
  "started_at": null,
  "finished_at": "2016-08-11T11:32:35.145Z",
  "committed_at": null,
  "duration": null,
  "queued_duration": 0.010,
  "coverage": null,
  "web_url": "https://example.com/foo/bar/pipelines/46"
}
```

<!--
## Cancel a pipeline's jobs
-->

<a id="cancel-a-pipelines-jobs"></a>

## 取消流水线中的作业

```plaintext
POST /projects/:id/pipelines/:pipeline_id/cancel
```

| 参数 | 类型 | 是否必需 | 描述 |
|------------|---------|----------|---------------------|
| `id`       | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `pipeline_id` | integer | yes   | 流水线的 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/pipelines/46/cancel"
```

响应示例：

```json
{
  "id": 46,
  "iid": 11,
  "project_id": 1,
  "status": "canceled",
  "ref": "main",
  "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
  "before_sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
  "tag": false,
  "yaml_errors": null,
  "user": {
    "name": "Administrator",
    "username": "root",
    "id": 1,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://localhost:3000/root"
  },
  "created_at": "2016-08-11T11:28:34.085Z",
  "updated_at": "2016-08-11T11:32:35.169Z",
  "started_at": null,
  "finished_at": "2016-08-11T11:32:35.145Z",
  "committed_at": null,
  "duration": null,
  "queued_duration": 0.010,
  "coverage": null,
  "web_url": "https://example.com/foo/bar/pipelines/46"
}
```

<!--
## Delete a pipeline

> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/22988) in GitLab 11.6.

Deleting a pipeline expires all pipeline caches, and deletes all immediately
related objects, such as builds, logs, artifacts, and triggers.
**This action cannot be undone.**

Deleting a pipeline does not automatically delete its
[child pipelines](../ci/pipelines/parent_child_pipelines.md).
See the [related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/39503)
for details.
-->

<a id="delete-a-pipeline"></a>

## 删除流水线

> 引入于 11.6 版本。

删除流水线会使所有流水线缓存过期，并立即删除所有相关对象，包括构建、日志、产物和触发器。
**此操作无法撤消。**

删除流水线不会自动删除其[子流水线](../ci/pipelines/downstream_pipelines.md#parent-child-pipelines)。

```plaintext
DELETE /projects/:id/pipelines/:pipeline_id
```

| 参数 | 类型 | 是否必需 | 描述 |
|------------|---------|----------|---------------------|
| `id`       | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `pipeline_id` | integer | yes      | 流水线的 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" --request "DELETE" "https://gitlab.example.com/api/v4/projects/1/pipelines/46"
```
