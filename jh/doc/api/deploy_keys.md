---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 部署密钥 API **(FREE)**

部署密钥 API 可以在以下字段中返回公钥的响应指纹：

- `指纹`（MD5 哈希）。在启用 FIPS 的系统上不可用。
- `fingerprint_sha256`（SHA256 哈希）。引入于极狐GitLab 15.2。

<a href="#list-all-deploy-keys"></a>

## 列出所有的部署密钥 **(FREE SELF)**

> `projects_with_readonly_access` 引入于极狐GitLab 16.0。

获取极狐GitLab 实例中所有项目的部署密钥列表。
这个 API 端点需要管理员权限，并且在 JihuLab.com 上不可用。

```plaintext
GET /deploy_keys
```

支持的参数：

| 参数   | 类型     | 是否必需 | 描述           |
|:------------|:---------|:---------|:----------------------|
| `public` | boolean | **{dotted-circle}** 否 | 只获取公共的密钥。默认是 `false`。 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/deploy_keys?public=true"
```

响应示例：

```json
[
  {
    "id": 1,
    "title": "Public key",
    "key": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDNJAkI3Wdf0r13c8a5pEExB2YowPWCSVzfZV22pNBc1CuEbyYLHpUyaD0GwpGvFdx2aP7lMEk35k6Rz3ccBF6jRaVJyhsn5VNnW92PMpBJ/P1UebhXwsFHdQf5rTt082cSxWuk61kGWRQtk4ozt/J2DF/dIUVaLvc+z4HomT41fQ==",
    "fingerprint": "4a:9d:64:15:ed:3a:e6:07:6e:89:36:b3:3b:03:05:d9",
    "fingerprint_sha256": "SHA256:Jrs3LD1Ji30xNLtTVf9NDCj7kkBgPBb2pjvTZ3HfIgU",
    "created_at": "2013-10-02T10:12:29Z",
    "expires_at": null,
    "projects_with_write_access": [
      {
        "id": 73,
        "description": null,
        "name": "project2",
        "name_with_namespace": "Sidney Jones / project2",
        "path": "project2",
        "path_with_namespace": "sidney_jones/project2",
        "created_at": "2021-10-25T18:33:17.550Z"
      },
      {
        "id": 74,
        "description": null,
        "name": "project3",
        "name_with_namespace": "Sidney Jones / project3",
        "path": "project3",
        "path_with_namespace": "sidney_jones/project3",
        "created_at": "2021-10-25T18:33:17.666Z"
      }
    ],
    "projects_with_readonly_access": []
  },
  {
    "id": 3,
    "title": "Another Public key",
    "key": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDIJFwIL6YNcCgVBLTHgM6hzmoL5vf0ThDKQMWT3HrwCjUCGPwR63vBwn6+/Gx+kx+VTo9FuojzR0O4XfwD3LrYA+oT3ETbn9U4e/VS4AH/G4SDMzgSLwu0YuPe517FfGWhWGQhjiXphkaQ+6bXPmcASWb0RCO5+pYlGIfxv4eFGQ=="
    "fingerprint": "0b:cf:58:40:b9:23:96:c7:ba:44:df:0e:9e:87:5e:75",
    "fingerprint_sha256": "SHA256:lGI/Ys/Wx7PfMhUO1iuBH92JQKYN+3mhJZvWO4Q5ims",
    "created_at": "2013-10-02T11:12:29Z",
    "expires_at": null,
    "projects_with_write_access": [],
    "projects_with_readonly_access": [
      {
        "id": 74,
        "description": null,
        "name": "project3",
        "name_with_namespace": "Sidney Jones / project3",
        "path": "project3",
        "path_with_namespace": "sidney_jones/project3",
        "created_at": "2021-10-25T18:33:17.666Z"
      }
    ]
  }
]
```

<a href="#list-deploy-keys-for-project"></a>

## 列出项目的部署密钥

获取项目中的部署密钥列表。

```plaintext
GET /projects/:id/deploy_keys
```

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id` | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/deploy_keys"
```

响应示例：

```json
[
  {
    "id": 1,
    "title": "Public key",
    "key": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDNJAkI3Wdf0r13c8a5pEExB2YowPWCSVzfZV22pNBc1CuEbyYLHpUyaD0GwpGvFdx2aP7lMEk35k6Rz3ccBF6jRaVJyhsn5VNnW92PMpBJ/P1UebhXwsFHdQf5rTt082cSxWuk61kGWRQtk4ozt/J2DF/dIUVaLvc+z4HomT41fQ==",
    "fingerprint": "4a:9d:64:15:ed:3a:e6:07:6e:89:36:b3:3b:03:05:d9",
    "fingerprint_sha256": "SHA256:Jrs3LD1Ji30xNLtTVf9NDCj7kkBgPBb2pjvTZ3HfIgU",
    "created_at": "2013-10-02T10:12:29Z",
    "expires_at": null,
    "can_push": false
  },
  {
    "id": 3,
    "title": "Another Public key",
    "key": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDIJFwIL6YNcCgVBLTHgM6hzmoL5vf0ThDKQMWT3HrwCjUCGPwR63vBwn6+/Gx+kx+VTo9FuojzR0O4XfwD3LrYA+oT3ETbn9U4e/VS4AH/G4SDMzgSLwu0YuPe517FfGWhWGQhjiXphkaQ+6bXPmcASWb0RCO5+pYlGIfxv4eFGQ=="
    "fingerprint": "0b:cf:58:40:b9:23:96:c7:ba:44:df:0e:9e:87:5e:75",
    "fingerprint_sha256": "SHA256:lGI/Ys/Wx7PfMhUO1iuBH92JQKYN+3mhJZvWO4Q5ims",
    "created_at": "2013-10-02T11:12:29Z",
    "expires_at": null,
    "can_push": false
  }
]
```

<a href="#list-project-deploy-keys-for-user"></a>

## 列出用户的项目部署密钥

> 引入于 15.1 版本。

获取特定用户（被请求者）以及被经过验证的用户（请求者）的[项目部署密钥](../user/project/deploy_keys/index.md#scope)列表。它只罗列请求者和被请求者的项目密钥。

```plaintext
GET /users/:id_or_username/project_deploy_keys
```

参数：

| 参数          | 类型   | 是否必需 | 描述                                                        |
|------------------- |--------|----------|------------------------------------------------------------------- |
| `id_or_username`   | string | 是      | 要被获取项目部署密钥的用户的 ID 或者用户名。 |

```json
[
    {
        "id": 1,
        "title": "Key A",
        "created_at": "2022-05-30T12:28:27.855Z",
        "expires_at": null,
        "key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTEVaAtU5wiVducsOa01InRFf7QSTxoAm6Xy0PGv/k48M6xCALa9nY+BzlOv47jUT57 Key",
        "fingerprint": "60:8e:10:f0:6a:82:c8:29:5f:bf:c0:38:72:00:6f:8f"
    },
    {
        "id": 2,
        "title": "Key B",
        "created_at": "2022-05-30T13:34:56.219Z",
        "expires_at": null,
        "key": "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAIEAiPWx6WM4lhHNedGfBpPJNPpZ7yKu+dnn1SJejgt4596k6YjzGGphH2TUxwKzxcKDKKezwkpfnxPkSMkuEspGRt/aZZ9wa++Oi7Qkr8prgHc4soW6NUlfDzpvZK2H5E7eQaSeP3SAwGmQKUFHCddNaP0L+hM7zhFNzjFvpaMgJw0=",
        "fingerprint": "75:33:44:7e:55:84:dd:70:29:a3:8e:a3:c0:b9:8b:65"
    }
]
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/20/project_deploy_keys"
```

响应示例：

```json
[
  {
    "id": 1,
    "title": "Key A",
    "created_at": "2022-05-30T12:28:27.855Z",
    "expires_at": "2022-10-30T12:28:27.855Z",
    "key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTEVaAtU5wiVducsOa01InRFf7QSTxoAm6Xy0PGv/k48M6xCALa9nY+BzlOv47jUT57 Key",
    "fingerprint": "60:8e:10:f0:6a:82:c8:29:5f:bf:c0:38:72:00:6f:8f"
  }
]
```

<a href="#get-a-single-deploy-key"></a>

## 获取单个部署密钥

获取单个部署密钥。

```plaintext
GET /projects/:id/deploy_keys/:key_id
```

参数：

| 参数          | 类型   | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | 是 | 项目的 ID 或者 [URL 编码后的群组路径](rest/index.md#namespaced-path-encoding)。 |
| `key_id`  | integer | 是 | 部署密钥的 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/deploy_keys/11"
```

响应示例：

```json
{
  "id": 1,
  "title": "Public key",
  "key": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDNJAkI3Wdf0r13c8a5pEExB2YowPWCSVzfZV22pNBc1CuEbyYLHpUyaD0GwpGvFdx2aP7lMEk35k6Rz3ccBF6jRaVJyhsn5VNnW92PMpBJ/P1UebhXwsFHdQf5rTt082cSxWuk61kGWRQtk4ozt/J2DF/dIUVaLvc+z4HomT41fQ==",
  "fingerprint": "4a:9d:64:15:ed:3a:e6:07:6e:89:36:b3:3b:03:05:d9",
  "fingerprint_sha256": "SHA256:Jrs3LD1Ji30xNLtTVf9NDCj7kkBgPBb2pjvTZ3HfIgU",
  "created_at": "2013-10-02T10:12:29Z",
  "expires_at": null,
  "can_push": false
}
```

<a href="#add-deploy-key"></a>

## 添加部署密钥

为一个项目创建一个新的部署密钥。

如果这个部署密钥已经在另一个项目中存在，那么只有在用户拥有原来项目的密钥的访问权限的时候，才能被加入到当前的项目。

```plaintext
POST /projects/:id/deploy_keys
```

| 参数 | 类型 | 是否必需 | 描述                                                              |
| ---------  | ---- | -------- |-----------------------------------------------------------------|
| `id`       | integer/string | 是 | 项目的 ID 或者 [URL 编码后的群组路径](rest/index.md#namespaced-path-encoding) |
| `title`    | string  | 是 | 新的部署密钥的标题                                                       |
| `key`      | string  | 是 | 新的部署密钥                                                          |
| `can_push` | boolean | 否  | 部署密钥是否能推送代码到代码仓库                                                |
| `expires_at` | datetime | no | 部署密钥的到期日期。如果未提供值，则不会过期。预计采用 ISO 8601 格式（`2019-03-15T08:00:00Z`） |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --header "Content-Type: application/json" \
     --data '{"title": "My deploy key", "key": "ssh-rsa AAAA...", "can_push": "true"}' \
     "https://gitlab.example.com/api/v4/projects/5/deploy_keys/"
```

响应示例：

```json
{
  "key" : "ssh-rsa AAAA...",
  "id" : 12,
  "title" : "My deploy key",
  "can_push": true,
  "created_at" : "2015-08-29T12:44:31.550Z",
  "expires_at": null
}
```

<a href="#update-deploy-key"></a>

## 更新部署密钥

更新某一个项目的部署密钥。

```plaintext
PUT /projects/:id/deploy_keys/:key_id
```

| 参数 | 类型 | 是否必需 | 描述 |
| ---------  | ---- | -------- | ----------- |
| `id`       | integer/string | yes | 项目的 ID 或者 [URL 编码后的群组路径](rest/index.md#namespaced-path-encoding) |
| `title`    | string  | no | 新的部署密钥的标题 |
| `can_push` | boolean | no  | 部署密钥是否能推送代码到代码仓库 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" --header "Content-Type: application/json" \
     --data '{"title": "New deploy key", "can_push": true}' "https://gitlab.example.com/api/v4/projects/5/deploy_keys/11"
```

响应示例：

```json
{
  "id": 11,
  "title": "New deploy key",
  "key": "ssh-rsa AAAA...",
  "created_at": "2015-08-29T12:44:31.550Z",
  "expires_at": null,
  "can_push": true
}
```

<a href="#delete-deploy-key"></a>

## 删除部署密钥

从某一个项目中删除一个部署密钥。如果部署密钥仅仅在这个项目中被使用，则完全从系统中删除。

```plaintext
DELETE /projects/:id/deploy_keys/:key_id
```

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | 是 | 项目的 ID 或者 [URL 编码后的群组路径](rest/index.md#namespaced-path-encoding) |
| `key_id`  | integer | yes | 部署密钥的 ID |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/deploy_keys/13"
```

<a href="#enable-a-deploy-key"></a>

## 开启部署密钥

为某一个项目开启部署密钥以供使用。
会返回被开启的密钥，成功时候的状态码是 201。

```plaintext
POST /projects/:id/deploy_keys/:key_id/enable
```

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | 是 | 项目的 ID 或者 [URL 编码后的群组路径](rest/index.md#namespaced-path-encoding) |
| `key_id`  | integer | 是 | 部署密钥的 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/deploy_keys/12/enable"
```

响应示例：

```json
{
  "key" : "ssh-rsa AAAA...",
  "id" : 12,
  "title" : "My deploy key",
  "created_at" : "2015-08-29T12:44:31.550Z",
  "expires_at": null
}
```

<a href="#add-deploy-keys-to-multiple-projects"></a>

## 将部署密钥添加到多个项目

这个 API 能帮您将同一个部署密钥添加到同一个群组的多个项目中。

首先，您需要找到这些项目的 ID，可以通过获取项目列表：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects"
```

或者先找到某一个群组的 ID：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups"
```

然后罗列该群组中的所有项目（例如，群组 1234）：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1234"
```

通过这些 ID，将同一个部署密钥添加到上述项目：

```shell
for project_id in 321 456 987; do
    curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
         --header "Content-Type: application/json" \
         --data '{"title": "my key", "key": "ssh-rsa AAAA..."}' \
         "https://gitlab.example.com/api/v4/projects/${project_id}/deploy_keys"
done
```
