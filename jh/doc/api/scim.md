---
type: reference, howto
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# SCIM API (SYSTEM ONLY) **(PREMIUM SAAS)**

> 引入于极狐GitLab 15.5。

极狐GitLab 提供了一个 SCIM API，它既实现 [RFC7644 协议](https://www.rfc-editor.org/rfc/rfc7644)又提供 `/Users` 端点。基本 URL 为 `/api/scim/v2/groups/:group_path/Users/`。

要使用此 API，必须为群组启用群组 SSO<!-- [群组 SSO](../user/group/saml_sso/index.md)-->。
此 API 仅在启用群组 SSO 的 SCIM<!--[群组 SSO 的SCIM](../user/group/saml_sso/scim_setup.md)--> 的情况下使用。这是创建 SCIM 身份的先决条件。

<!--Not to be confused with the [internal group SCIM API](../development/internal_api/index.md#scim-api).-->

## 获取群组的 SCIM 身份

> 引入于极狐GitLab 15.5。

```plaintext
GET /groups/:id/scim/identities
```

支持的参数：

| 参数   | 类型             | 是否必需 | 描述                                                          |
|:-----|:---------------|:-----|:------------------------------------------------------------|
| `id` | integer/string | Yes  | 群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |

如果成功，则返回 [`200`](rest/index.md#status-codes) 和以下响应参数：

| 参数           | 类型      | 描述        |
|--------------|---------|-----------|
| `extern_uid` | string  | 用户的外部 UID |
| `user_id`    | integer | 用户 ID     |
| `active`    | boolean | 身份状态      |


响应示例：

```json
[
  {
    "extern_uid": "4",
    "user_id": 48,
    "active": true
  }
]
```

请求示例：

```shell
curl --location --request GET "https://gitlab.example.com/api/v4/groups/33/scim/identities" \
--header "PRIVATE-TOKEN: <PRIVATE-TOKEN>"
```

## 获取单个 SCIM 身份

> 引入于极狐GitLab 16.1。

```plaintext
GET /groups/:id/scim/:uid
```

支持的参数：

| 参数    | 类型      | 是否必需 | 描述                                                         |
|-------|---------|------|------------------------------------------------------------|
| `id`  | integer | yes  | 群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `uid` | string  | yes  | 用户的外部 UID                                                  |

请求示例：

```shell
curl --location --request GET "https://gitlab.example.com/api/v4/groups/33/scim/sydney_jones" --header "<PRIVATE TOKEN>"
```

响应示例：

```json
{
    "extern_uid": "4",
    "user_id": 48,
    "active": true
}
```

## 更新 SCIM 身份的 `extern_uid` 字段

> 引入于极狐GitLab 15.5。

可以更新的字段为：

| SCIM/IdP 字段     | 极狐GitLab 字段  |
|-----------------|--------------|
| `id/externalId` | `extern_uid` |

```plaintext
PATCH /groups/:groups_id/scim/:uid
```

参数：

| 参数    | 类型     | 是否必需 | 描述                                                          |
|-------|--------|------|-------------------------------------------------------------|
| `id`      | integer/string | yes      | 群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `uid` | string | yes  | 用户的外部 UID                                                   |

请求示例：

```shell
curl --location --request PATCH "https://gitlab.example.com/api/v4/groups/33/scim/sydney_jones" \
--header "PRIVATE-TOKEN: <PRIVATE TOKEN>" \
--form "extern_uid=sydney_jones_new"
```
