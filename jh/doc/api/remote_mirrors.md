---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
type: reference, api
---

# 保护远端镜像 API **(FREE)**

在项目仓库设置中定义的[推送镜像](../user/project/repository/mirror/push.md)被称为“远端镜像”。您可以使用远端镜像 API 查询和修改这些镜像的状态。

出于安全原因，API 响应中的 `url` 参数始终会清除用户名和密码信息。

NOTE:
[拉取镜像](../user/project/repository/mirror/pull.md)使用[不同的 API 端点](projects.md#configure-pull-mirroring-for-a-project)以进行显示和更新。

## 列出项目的远端镜像

返回远端镜像及其状态的阵列：

```plaintext
GET /projects/:id/remote_mirrors
```

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/42/remote_mirrors"
```

响应示例：

```json
[
  {
    "enabled": true,
    "id": 101486,
    "last_error": null,
    "last_successful_update_at": "2020-01-06T17:32:02.823Z",
    "last_update_at": "2020-01-06T17:32:02.823Z",
    "last_update_started_at": "2020-01-06T17:31:55.864Z",
    "only_protected_branches": true,
    "keep_divergent_refs": true,
    "update_status": "finished",
    "url": "https://*****:*****@gitlab.com/gitlab-org/security/gitlab.git"
  }
]
```

## 获取单个项目的远端镜像

> 引入于极狐GitLab 14.10。

返回远端镜像及其状态：

```plaintext
GET /projects/:id/remote_mirrors/:mirror_id
```

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/42/remote_mirrors/101486"
```

响应示例：

```json
{
  "enabled": true,
  "id": 101486,
  "last_error": null,
  "last_successful_update_at": "2020-01-06T17:32:02.823Z",
  "last_update_at": "2020-01-06T17:32:02.823Z",
  "last_update_started_at": "2020-01-06T17:31:55.864Z",
  "only_protected_branches": true,
  "keep_divergent_refs": true,
  "update_status": "finished",
  "url": "https://*****:*****@gitlab.com/gitlab-org/security/gitlab.git"
}
```

## 创建拉取镜像

学习如何使用保护 API [配置拉取镜像](projects.md#configure-pull-mirroring-for-a-project)。

## 创建推送镜像

> - 字段 `mirror_branch_regex` 引入于极狐GitLab 15.8，[功能标志](../administration/feature_flags.md)为 `mirror_only_branches_match_regex`。默认禁用。
> - 默认启用于极狐GitLab 16.0。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下字段 `mirror_branch_regex` 可用。
要隐藏此功能，请让管理员禁用名为 `mirror_only_branches_match_regex` 的[功能标志](../administration/feature_flags.md)。
在 JiHuLab.com 上，此功能可用。

推送镜像默认禁用。您可以在创建镜像的时候包括可选参数 `enabled` 来进行开启：

```plaintext
POST /projects/:id/remote_mirrors
```

| 参数                         | 类型      | 是否必需 | 描述                                                      |
|:---------------------------|:--------|:-----|:--------------------------------------------------------|
| `url`                      | String  | yes  | 仓库要镜像到的目的 URL                                           |
| `enabled`                  | Boolean | no   | 镜像是否开启                                                  |
| `only_protected_branches`  | Boolean | no   | 是否只镜像受保护的分支                                             |
| `keep_divergent_refs`      | Boolean | no   | 是否跳过不同的引用                                               |
| `mirror_branch_regex` **(PREMIUM)**     | String | no | 包含正则表达式。仅镜像名称与正则表达式匹配的分支。需要禁用 `only_protected_branches` |

请求示例：

```shell
curl --request POST --data "url=https://username:token@example.com/gitlab/example.git" \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/42/remote_mirrors"
```

响应示例：

```json
{
    "enabled": false,
    "id": 101486,
    "last_error": null,
    "last_successful_update_at": null,
    "last_update_at": null,
    "last_update_started_at": null,
    "only_protected_branches": false,
    "keep_divergent_refs": false,
    "update_status": "none",
    "url": "https://*****:*****@example.com/gitlab/example.git"
}
```

## 更新远端镜像的属性

FLAG:
在私有化部署的极狐GitLab 上，默认情况下字段 `mirror_branch_regex` 不可用。
要使其可用，请让管理员启用名为 `mirror_only_branches_match_regex` 的[功能标志](../administration/feature_flags.md)。
在 JiHuLab.com 上，此功能不可用。

打开或关闭远端镜像，或更改镜像的分支类型：

```plaintext
PUT /projects/:id/remote_mirrors/:mirror_id
```

| 参数                                  | 类型      | 是否必需  | 描述                                                    |
|:------------------------------------|:--------|:------|:------------------------------------------------------|
| `mirror_id`                         | Integer | yes   | 远端镜像 ID                                               |
| `enabled`                           | Boolean | no    | 镜像是否开启                                                |
| `only_protected_branches`           | Boolean | no    | 是否只镜像受保护的分支                                           |
| `keep_divergent_refs`               | Boolean | no    | 是否跳过不同的引用                                             |
| `mirror_branch_regex`**(PREMIUM)**  | String  | no    | 确定是否仅镜像名称与正则表达式匹配的分支。`only_protected_branches` 启用时不可用 |

请求示例：

```shell
curl --request PUT --data "enabled=false" --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/42/remote_mirrors/101486"
```

响应示例：

```json
{
    "enabled": false,
    "id": 101486,
    "last_error": null,
    "last_successful_update_at": "2020-01-06T17:32:02.823Z",
    "last_update_at": "2020-01-06T17:32:02.823Z",
    "last_update_started_at": "2020-01-06T17:31:55.864Z",
    "only_protected_branches": true,
    "keep_divergent_refs": true,
    "update_status": "finished",
    "url": "https://*****:*****@gitlab.com/gitlab-org/security/gitlab.git"
}
```

## 删除远端镜像

> 引入于极狐GitLab 14.10。

删除远端镜像。

```plaintext
DELETE /projects/:id/remote_mirrors/:mirror_id
```

| 参数          | 类型      | 是否必需 | 描述      |
|:------------|:--------|:-----|:--------|
| `mirror_id` | Integer | yes  | 远端镜像 ID |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/42/remote_mirrors/101486"
```
