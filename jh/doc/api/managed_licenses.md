---
stage: Fulfillment
group: Utilization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
remove_date: '2023-08-22'
redirect_to: 'index.md'
---

# 管理证书 API（已移除） **(ULTIMATE)**

此功能废弃于极狐GitLab 15.9 并移除于 16.0。

<!-- This redirect file can be deleted after <2023-08-22>. -->
<!-- Redirects that point to other docs in the same project expire in three months. -->
<!-- Redirects that point to docs in a different project or site (link is not relative and starts with `https:`) expire in one year. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/redirects.html -->
