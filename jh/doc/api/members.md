---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组和项目成员 API **(FREE)**

> `created_by` 字段引入于极狐GitLab 14.10。

<a id="roles"></a>

## 角色

Git 分配给用户或群组的[角色](../user/permissions.md)在 `Gitlab::Access` 模块被定义为 `access_level`。

- 无访问权限（`0`）
- 最小访问权限（`5`）（引入于 13.5）
- 访客（`10`）
- 报告者（`20`）
- 开发者（`30`）
- 维护者（`40`）
- 拥有者（`50`）（对 14.9 及更高版本中的项目有效）

NOTE:
在 14.9 及更高版本中，私有命名空间中的项目拥有 `50`（拥有者）的 `access_level`。
在 14.8 及更高版本中，私有命名空间中的项目拥有 `40`（维护者）的 `access_level` 。

## 限制

`group_saml_identity` 参数仅对 SSO 启用的群组<!--(../user/group/saml_sso/index.md)-->拥有者可见。

<!--The `email` attribute is only visible to group Owners for any [enterprise user](../user/enterprise_user/index.md).-->

## 列出群组或项目的所有成员

获取经过身份验证的用户可见的群组或项目成员的列表。仅返回直接成员，不返回祖先群组的继承成员。

这个功能使用分页参数 `page` 和 `per_page` 限制用户列表。

```plaintext
GET /groups/:id/members
GET /projects/:id/members
```

| 参数         | 类型                | 是否必需 | 描述                                                               |
|------------|-------------------|------|------------------------------------------------------------------|
| `id`       | integer/string    | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目或群组路径](rest/index.md#namespaced-path-encoding) |
| `query`    | string            | no   | 查询成员的查询字符串                                                       |
| `user_ids` | array of integers | no   | 根据特定用户 ID 过滤结果                                                   |
| `skip_users`   | array of integers | no     | 从结果中过滤跳过的用户                                                      |
| `show_seat_info`   | boolean | no     | 显示用户的席位信息                      |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/members"
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/members"
```

响应示例：

```json
[
  {
    "id": 1,
    "username": "raymond_smith",
    "name": "Raymond Smith",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root",
    "created_at": "2012-09-22T14:13:35Z",
    "created_by": {
      "id": 2,
      "username": "john_doe",
      "name": "John Doe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
      "web_url": "http://192.168.1.8:3000/root"
    },
    "expires_at": "2012-10-22T14:13:35Z",
    "access_level": 30,
    "group_saml_identity": null
  },
  {
    "id": 2,
    "username": "john_doe",
    "name": "John Doe",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root",
    "created_at": "2012-09-22T14:13:35Z",
    "created_by": {
      "id": 1,
      "username": "raymond_smith",
      "name": "Raymond Smith",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
      "web_url": "http://192.168.1.8:3000/root"
    },
    "expires_at": "2012-10-22T14:13:35Z",
    "access_level": 30,
    "email": "john@example.com",
    "group_saml_identity": {
      "extern_uid":"ABC-1234567890",
      "provider": "group_saml",
      "saml_provider_id": 10
    }
  }
]
```

<a id="list-all-members-of-a-group-or-project-including-inherited-and-invited-members"></a>

## 列出群组或项目中包括继承和邀请成员在内的所有成员

获取经过身份验证的用户可以查看的群组或项目成员的列表，包括继承的成员、受邀用户和祖先群组的权限。

如果用户是该群组或项目的成员，并且也是一个或多个祖先群组的成员，仅返回其具有最高 `access_level` 的成员资格，其代表了用户的有效权限。

如果出现以下任一情况，则返回受邀群组的成员：

- 受邀群组是公开的。
- 请求者也是受邀群组的成员。

该函数采用分页参数 `page` 和 `per_page` 限制用户列表。

```plaintext
GET /groups/:id/members/all
GET /projects/:id/members/all
```

| 参数         | 类型                | 是否必需 | 描述                                                               |
|------------|-------------------|------|------------------------------------------------------------------|
| `id`       | integer/string    | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组或项目路径](rest/index.md#namespaced-path-encoding) |
| `query`    | string            | no   | 查询成员的查询串                                                         |
| `user_ids` | array of integers | no   | 根据特定用户 ID 过滤结果                                                   |
| `show_seat_info`  |  boolean  | no | 显示用户席位信息                                                         |
| `state`    | string            | no   | 根据成员状态过滤结果：`awaiting` 或 `active` **(PREMIUM)**                   |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/members/all"
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/members/all"
```

响应示例：

```json
[
  {
    "id": 1,
    "username": "raymond_smith",
    "name": "Raymond Smith",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root",
    "created_at": "2012-09-22T14:13:35Z",
    "created_by": {
      "id": 2,
      "username": "john_doe",
      "name": "John Doe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
      "web_url": "http://192.168.1.8:3000/root"
    },
    "expires_at": "2012-10-22T14:13:35Z",
    "access_level": 30,
    "group_saml_identity": null
  },
  {
    "id": 2,
    "username": "john_doe",
    "name": "John Doe",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root",
    "created_at": "2012-09-22T14:13:35Z",
    "created_by": {
      "id": 1,
      "username": "raymond_smith",
      "name": "Raymond Smith",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
      "web_url": "http://192.168.1.8:3000/root"
    },
    "expires_at": "2012-10-22T14:13:35Z",
    "access_level": 30,
    "email": "john@example.com",
    "group_saml_identity": {
      "extern_uid":"ABC-1234567890",
      "provider": "group_saml",
      "saml_provider_id": 10
    }
  },
  {
    "id": 3,
    "username": "foo_bar",
    "name": "Foo bar",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root",
    "created_at": "2012-10-22T14:13:35Z",
    "created_by": {
      "id": 2,
      "username": "john_doe",
      "name": "John Doe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
      "web_url": "http://192.168.1.8:3000/root"
    },
    "expires_at": "2012-11-22T14:13:35Z",
    "access_level": 30,
    "group_saml_identity": null
  }
]
```

## 获取群组或项目成员

获取群组或项目成员。仅返回直接成员，不返回祖先群组的继承成员。

```plaintext
GET /groups/:id/members/:user_id
GET /projects/:id/members/:user_id
```

| 参数        | 类型             | 是否必需 | 描述                                                               |
|-----------|----------------|------|------------------------------------------------------------------|
| `id`      | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目或群组信息](rest/index.md#namespaced-path-encoding) |
| `user_id` | integer        | yes  | 成员的用户 ID                                           |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/members/:user_id"
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/members/:user_id"
```

响应示例：

```json
{
  "id": 1,
  "username": "raymond_smith",
  "name": "Raymond Smith",
  "state": "active",
  "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
  "web_url": "http://192.168.1.8:3000/root",
  "access_level": 30,
  "email": "john@example.com",
  "created_at": "2012-10-22T14:13:35Z",
  "created_by": {
    "id": 2,
    "username": "john_doe",
    "name": "John Doe",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root"
  },
  "expires_at": null,
  "group_saml_identity": null
}
```

## 获取群组或项目中包括继承和邀请成员在内的成员

> 引入于极狐GitLab 12.4。

获取群组或项目成员，包括通过祖先群组继承或邀请的成员。详情请参见[列出所有继承成员的端点](#list-all-members-of-a-group-or-project-including-inherited-and-invited-members)。

```plaintext
GET /groups/:id/members/all/:user_id
GET /projects/:id/members/all/:user_id
```

| 参数        | 类型             | 是否必需 | 描述                                                               |
|-----------|----------------|------|------------------------------------------------------------------|
| `id`      | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目或群组路径](rest/index.md#namespaced-path-encoding) |
| `user_id` | integer        | yes  | 成员的用户 ID                                         |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/members/all/:user_id"
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/members/all/:user_id"
```

响应示例：

```json
{
  "id": 1,
  "username": "raymond_smith",
  "name": "Raymond Smith",
  "state": "active",
  "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
  "web_url": "http://192.168.1.8:3000/root",
  "access_level": 30,
  "created_at": "2012-10-22T14:13:35Z",
  "created_by": {
    "id": 2,
    "username": "john_doe",
    "name": "John Doe",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root"
  },
  "email": "john@example.com",
  "expires_at": null,
  "group_saml_identity": null
}
```

## 列出群组中所有计费成员

> 引入于极狐GitLab 13.5。

获取计费成员的列表。该列表包括子群组和项目中的成员。

此 API 端点仅适用于顶级群组，不适用于子群组。

该函数采用[分页](rest/index.md#pagination)参数 `page` 和 `per_page` 限制用户列表。

在极狐GitLab 13.7 及更高版本中，使用 `search` 参数按名称搜索可计费群组成员，然后 `sort` 对结果进行排序。

```plaintext
GET /groups/:id/billable_members
```

| 参数                         | 类型             | 是否必需 | 描述                                                            |
|----------------------------|----------------|------|---------------------------------------------------------------|
| `id`                       | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `search`                   | string         | no   | 按名称、用户名或公共电子邮件搜索群组成员的查询字符串                                    |
| `sort`                     | string         | no   | 包含指定排序参数和顺序的参数的查询字符串。请参阅下面支持的值                                |
| `include_awaiting_members` | boolean        | no   | 是否包含待审批成员                                                     |

`sort` 参数支持的值为：

| 值                       | 描述        |
|-------------------------|-----------|
| `access_level_asc`      | 访问级别，升序   |
| `access_level_desc`     | 访问级别，降序   |
| `last_joined`           | 最新加入      |
| `name_asc`              | 名称，升序     |
| `name_desc`             | 名称，降序     |
| `oldest_joined`         | 最早加入      |
| `oldest_sign_in`        | 最早登录      |
| `recent_sign_in`        | 最新登录      |
| `last_activity_on_asc`  | 最新活动日期，升序 |
| `last_activity_on_desc` | 最新活动日期，降序 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/billable_members"
```

响应示例：

```json
[
  {
    "id": 1,
    "username": "raymond_smith",
    "name": "Raymond Smith",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root",
    "last_activity_on": "2021-01-27",
    "membership_type": "group_member",
    "removable": true,
    "created_at": "2021-01-03T12:16:02.000Z",
    "last_login_at": "2022-10-09T01:33:06.000Z"
  },
  {
    "id": 2,
    "username": "john_doe",
    "name": "John Doe",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root",
    "email": "john@example.com",
    "last_activity_on": "2021-01-25",
    "membership_type": "group_member",
    "removable": true,
    "created_at": "2021-01-04T18:46:42.000Z",
    "last_login_at": "2022-09-29T22:18:46.000Z"
  },
  {
    "id": 3,
    "username": "foo_bar",
    "name": "Foo bar",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root",
    "last_activity_on": "2021-01-20",
    "membership_type": "group_invite",
    "removable": false,
    "created_at": "2021-01-09T07:12:31.000Z",
    "last_login_at": "2022-10-10T07:28:56.000Z"
  }
]
```

## 列出群组中计费成员的成员资格

> 引入于极狐GitLab 13.11。

获取群组可计费成员的成员资格列表。

列出用户所属的所有项目和群组。仅包含群组层级中的项目和群组。
例如，如果请求的群组是 `Root Group`，并且请求的用户是 `Root Group / Sub Group One` 和 `Other Group / Sub Group Two` 的直接成员，那么仅返回 `Root Group / Sub Group One`，因为 `Other Group / Sub Group Two` 不在 `Root Group` 的层级结构中。

响应仅代表直接成员资格，不包括继承的成员资格。

此 API 端点仅适用于顶级群组，不适用于子群组。

此 API 端点需要群组的管理员权限。

此 API 端点采用[分页](rest/index.md#pagination)参数 `page` 和 `per_page` 限制成员资格列表。

```plaintext
GET /groups/:id/billable_members/:user_id/memberships
```

| 参数        | 类型             | 是否必需 | 描述                                                       |
|-----------|----------------|------|----------------------------------------------------------|
| `id`      | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `user_id` | integer        | yes  | 计费成员的用户 ID                       |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/billable_members/:user_id/memberships"
```

响应示例：

```json
[
  {
    "id": 168,
    "source_id": 131,
    "source_full_name": "Root Group / Sub Group One",
    "source_members_url": "https://gitlab.example.com/groups/root-group/sub-group-one/-/group_members",
    "created_at": "2021-03-31T17:28:44.812Z",
    "expires_at": "2022-03-21",
    "access_level": {
      "string_value": "Developer",
      "integer_value": 30
    }
  },
  {
    "id": 169,
    "source_id": 63,
    "source_full_name": "Root Group / Sub Group One / My Project",
    "source_members_url": "https://gitlab.example.com/root-group/sub-group-one/my-project/-/project_members",
    "created_at": "2021-03-31T17:29:14.934Z",
    "expires_at": null,
    "access_level": {
      "string_value": "Maintainer",
      "integer_value": 40
    }
  }
]
```

<a id="remove-a-billable-member-from-a-group"></a>

## 从群组中移除计费成员

从群组及其子群组和项目中移除计费成员。

用户无需成为群组成员即可获得移除资格。例如，如果用户被直接添加到群组内的项目中，您仍然可以使用此 API 将其移除。

```plaintext
DELETE /groups/:id/billable_members/:user_id
```

| 参数        | 类型             | 是否必需 | 描述                                                            |
|-----------|----------------|------|---------------------------------------------------------------|
| `id`      | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `user_id` | integer        | yes  | 成员的用户 ID                                    |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/billable_members/:user_id"
```

## 更改群组中用户的成员资格状态

> 引入于极狐GitLab 15.0。

更改群组中用户的成员资格状态。该状态适用于所有子群组和项目。

```plaintext
PUT /groups/:id/members/:user_id/state
```

| 参数        | 类型             | 是否必需 | 描述                                                            |
|-----------|----------------|------|---------------------------------------------------------------|
| `id`      | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `user_id` | integer        | yes  | 成员的用户 ID                                                      |
| `state`   | string         | yes  | 成员的新状态：`awaiting` 或 `active`                                |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/members/:user_id/state?state=active"
```

响应示例：

```json
{
  "success":true
}
```

## 向群组或项目添加成员

向群组或项目添加成员。

```plaintext
POST /groups/:id/members
POST /projects/:id/members
```

| 参数                 | 类型               | 是否必需 | 描述                                                                                                                                                                               |
|--------------------|------------------|------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`               | integer/string   | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目或群组路径](rest/index.md#namespaced-path-encoding)                                                                                                                 |
| `user_id`          | integer/string   | yes  | 新成员的用户 ID 或以逗号分隔的多个 ID                                                                                                                                                           |
| `access_level`     | integer          | yes  | 有效访问级别                                                                                                                                                                           |
| `expires_at`       | string           | no   | 日期字符串，格式：`YEAR-MONTH-DAY`                                                                                                                                                        |
| `invite_source`    | string           | no   | 开启成员创建流程的邀请源                                                                                                                                                                     |
| `tasks_to_be_done` | array of strings | no   | 邀请者希望成员关注的任务。任务将作为议题添加到指定项目。可能的值是：`ci`、`code` 和 `issues`。如果指定，则需要 `tasks_project_id`。引入于极狐GitLab 14.5，[功能标志](../administration/feature_flags.md)为 `invite_members_for_task`。默认禁用 |
| `tasks_project_id` | integer          | no   | 创建任务议题的项目 ID。如果指定，则需要 `tasks_to_be_done`。引入于极狐GitLab 14.5，[功能标志](../administration/feature_flags.md)为 `invite_members_for_task`。默认禁用                                             |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --data "user_id=1&access_level=30" "https://gitlab.example.com/api/v4/groups/:id/members"
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --data "user_id=1&access_level=30" "https://gitlab.example.com/api/v4/projects/:id/members"
```

响应示例：

```json
{
  "id": 1,
  "username": "raymond_smith",
  "name": "Raymond Smith",
  "state": "active",
  "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
  "web_url": "http://192.168.1.8:3000/root",
  "created_at": "2012-10-22T14:13:35Z",
  "created_by": {
    "id": 2,
    "username": "john_doe",
    "name": "John Doe",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root"
  },
  "expires_at": "2012-10-22T14:13:35Z",
  "access_level": 30,
  "email": "john@example.com",
  "group_saml_identity": null
}
```

## 编辑群组或项目成员

更新群组或项目成员。

```plaintext
PUT /groups/:id/members/:user_id
PUT /projects/:id/members/:user_id
```

| 参数             | 类型             | 是否必需 | 描述                                                                    |
|----------------|----------------|------|-----------------------------------------------------------------------|
| `id`           | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目或群组路径](rest/index.md#namespaced-path-encoding) |
| `user_id`      | integer        | yes  | 成员的用户 ID                                                              |
| `access_level` | integer        | yes  | 有效访问级别                                                                |
| `expires_at`   | string         | no   | 日期资源串，格式：`YEAR-MONTH-DAY`                                             |
| `member_role_id`  | integer        | no   | 成员角色 ID **(ULTIMATE)**                                                              |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/members/:user_id?access_level=40"
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/members/:user_id?access_level=40"
```

响应示例：

```json
{
  "id": 1,
  "username": "raymond_smith",
  "name": "Raymond Smith",
  "state": "active",
  "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
  "web_url": "http://192.168.1.8:3000/root",
  "created_at": "2012-10-22T14:13:35Z",
  "created_by": {
    "id": 2,
    "username": "john_doe",
    "name": "John Doe",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root"
  },
  "expires_at": "2012-10-22T14:13:35Z",
  "access_level": 40,
  "email": "john@example.com",
  "group_saml_identity": null
}
```

### 为群组成员设置覆盖标记

> 引入于极狐GitLab 13.0。

默认情况下，LDAP 群组成员的访问级别设置为 LDAP 通过 Group Sync 指定的值。
您可以通过调用此端点来允许访问级别覆盖。

```plaintext
POST /groups/:id/members/:user_id/override
```

| 参数        | 类型             | 是否必需 | 描述                                                            |
|-----------|----------------|------|---------------------------------------------------------------|
| `id`      | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `user_id` | integer        | yes  | 成员的用户 ID                                       |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/members/:user_id/override"
```

响应示例：

```json
{
  "id": 1,
  "username": "raymond_smith",
  "name": "Raymond Smith",
  "state": "active",
  "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
  "web_url": "http://192.168.1.8:3000/root",
  "created_at": "2012-10-22T14:13:35Z",
  "created_by": {
    "id": 2,
    "username": "john_doe",
    "name": "John Doe",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root"
  },
  "expires_at": "2012-10-22T14:13:35Z",
  "access_level": 40,
  "email": "john@example.com",
  "override": true
}
```

### 移除群组成员覆盖

> 引入于极狐GitLab 13.0。

将覆盖标记设置为 False 并允许 LDAP Group Sync 将访问级别重置为 LDAP 规定的值。

```plaintext
DELETE /groups/:id/members/:user_id/override
```

| 参数        | 类型             | 是否必需 | 描述                                                            |
|-----------|----------------|------|---------------------------------------------------------------|
| `id`      | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `user_id` | integer        | yes  | 成员的用户 ID                                       |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/members/:user_id/override"
```

响应示例：

```json
{
  "id": 1,
  "username": "raymond_smith",
  "name": "Raymond Smith",
  "state": "active",
  "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
  "web_url": "http://192.168.1.8:3000/root",
  "created_at": "2012-10-22T14:13:35Z",
  "created_by": {
    "id": 2,
    "username": "john_doe",
    "name": "John Doe",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c2525a7f58ae3776070e44c106c48e15?s=80&d=identicon",
    "web_url": "http://192.168.1.8:3000/root"
  },
  "expires_at": "2012-10-22",
  "access_level": 40,
  "email": "john@example.com",
  "override": false
}
```

## 移除群组或项目成员

从已明确为用户分配角色的群组或项目中移除用户。

用户必须是群组成员才有资格移除。
例如，如果用户被直接添加到群组内的项目，而不是这个群组中，您不能使用此 API 进行移除。替代方法请参见[从群组中移除计费成员](#remove-a-billable-member-from-a-group)。

```plaintext
DELETE /groups/:id/members/:user_id
DELETE /projects/:id/members/:user_id
```

| 参数                   | 类型             | 是否必需  | 描述                                                              |
|----------------------|----------------|-------|-----------------------------------------------------------------|
| `id`                 | integer/string | yes   | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目或群组路径](rest/index.md#namespaced-path-encoding) |
| `user_id`            | integer        | yes   | 成员的用户 ID                                                        |
| `skip_subresources`  | boolean        | false | 是否应跳过删除子群组和项目中已移除成员的直接成员资格。默认为 `false`                          |
| `unassign_issuables` | boolean        | false | 是否应从给定群组或项目中的任何议题或合并请求中取消分配已移除的成员。默认为 `false`                   |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/members/:user_id"
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/members/:user_id"
```

## 通过群组成员申请

通过群组及其子群组及项目的成员申请。

```plaintext
PUT /groups/:id/members/:member_id/approve
```

| 参数          | 类型             | 是否必需 | 描述                                                             |
|-------------|----------------|------|----------------------------------------------------------------|
| `id`        | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的根群组路径](rest/index.md#namespaced-path-encoding) |
| `member_id` | integer        | yes  | 成员 ID                                            |

请求示例：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/members/:member_id/approve"
```

## 通过群组的所有成员申请

通过群组及其子群组及项目的所有成员申请。

```plaintext
POST /groups/:id/members/approve_all
```

| 参数   | 类型             | 是否必需 | 描述                                                             |
|------|----------------|------|----------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的根群组路径](rest/index.md#namespaced-path-encoding) |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/members/approve_all"
```

## 列出群组及其子群组和项目的成员申请

> 引入于极狐GitLab 14.6。

获取群组及其子群组和项目中处于 `awaiting` 状态的所有成员的列表，以及那些被邀请但没有极狐GitLab 账户的成员的列表。

此请求返回根群组层次结构中所有群组和项目的所有匹配的群组和项目成员。

当成员是尚未注册极狐GitLab 账户的受邀用户时，将返回受邀电子邮件地址。

此 API 端点仅适用于顶级群组，不适用于子群组。

此 API 端点需要管理员权限。

此 API 端点采用[分页](rest/index.md#pagination)参数 `page` 和 `per_page` 限制成员列表。

```plaintext
GET /groups/:id/pending_members
```

| 参数   | 类型             | 是否必需 | 描述                                                            |
|------|----------------|------|---------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/pending_members"
```

响应示例：

```json
[
  {
    "id": 168,
    "name": "Alex Garcia",
    "username": "alex_garcia",
    "email": "alex@example.com",
    "avatar_url": "http://example.com/uploads/user/avatar/1/cd8.jpeg",
    "web_url": "http://example.com/alex_garcia",
    "approved": false,
    "invited": false
  },
  {
    "id": 169,
    "email": "sidney@example.com",
    "avatar_url": "http://gravatar.com/../e346561cd8.jpeg",
    "approved": false,
    "invited": true
  },
  {
    "id": 170,
    "email": "zhang@example.com",
    "avatar_url": "http://gravatar.com/../e32131cd8.jpeg",
    "approved": true,
    "invited": true
  }
]
```

## 授予群组访问项目的权限

请参见[与群组共享项目](projects.md#share-project-with-group)。
