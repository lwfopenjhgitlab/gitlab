---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 部署令牌 API **(FREE)**

## 列出所有部署令牌 **(FREE SELF)**

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/21811) -->引入于极狐GitLab 12.9。

获取极狐GitLab 实例中的所有部署令牌的列表。您需要有管理员权限才能使用这个端点。

```plaintext
GET /deploy_tokens
```

参数：

| 参数       | 类型      | 是否必需                   | 描述      |
|----------|---------|------------------------|---------|
| `active` | boolean | **{dotted-circle}** No | 受限于有效状态 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/deploy_tokens"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "MyToken",
    "username": "gitlab+deploy-token-1",
    "expires_at": "2020-02-14T00:00:00.000Z",
    "revoked": false,
    "expired": false,
    "scopes": [
      "read_repository",
      "read_registry"
    ]
  }
]
```

## 项目部署令牌

项目部署令牌 API 端点至少需要该项目的维护者角色。

### 列出项目部署令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/21811) -->引入于极狐GitLab 12.9。

获取项目的部署令牌的列表。

```plaintext
GET /projects/:id/deploy_tokens
```

参数：

| 参数       | 类型             | 是否必需                   | 描述                                                    |
|:---------|:---------------|:-----------------------|:------------------------------------------------------|
| `id`     | integer/string | **{check-circle}** Yes | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `active` | boolean        | **{dotted-circle}** No | 受限于有效状态                                               |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/deploy_tokens"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "MyToken",
    "username": "gitlab+deploy-token-1",
    "expires_at": "2020-02-14T00:00:00.000Z",
    "revoked": false,
    "expired": false,
    "scopes": [
      "read_repository",
      "read_registry"
    ]
  }
]
```

### 获取项目部署令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/82467) -->引入于极狐GitLab 14.9。

通过 ID 获取单个项目的部署令牌。

```plaintext
GET /projects/:id/deploy_tokens/:token_id
```

参数：

| 参数         | 类型             | 是否必需                   | 描述                                                            |
|------------|----------------|------------------------|---------------------------------------------------------------|
| `id`       | integer/string | **{check-circle}** Yes | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `token_id` | integer        | **{check-circle}** Yes | 部署令牌的 ID                                  |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/deploy_tokens/1"
```

响应示例：

```json
{
  "id": 1,
  "name": "MyToken",
  "username": "gitlab+deploy-token-1",
  "expires_at": "2020-02-14T00:00:00.000Z",
  "revoked": false,
  "expired": false,
  "scopes": [
    "read_repository",
    "read_registry"
  ]
}
```

### 创建项目部署令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/21811) -->引入于极狐GitLab 12.9。

为项目创建新的部署令牌。

```plaintext
POST /projects/:id/deploy_tokens
```

参数：

| 参数           | 类型               | 是否必需                   | 描述                                                                                                                         |
|--------------|------------------|------------------------|----------------------------------------------------------------------------------------------------------------------------|
| `id`         | integer/string   | **{check-circle}** Yes | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)                                                              |
| `name`       | string           | **{check-circle}** Yes | 新部署令牌的名称                                                                                                                   |
| `expires_at` | datetime         | **{dotted-circle}** No | 部署令牌的过期时间。如果不提供值，则不会过期。预计为 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                             |
| `username`   | string           | **{dotted-circle}** No | 部署令牌的用户名。默认为 `gitlab+deploy-token-{n}`                                                                                     |
| `scopes`     | array of strings | **{check-circle}** Yes | 表示部署令牌的范围。必须至少是 `read_repository`、`read_registry`、`write_registry`、`read_package_registry` 或 `write_package_registry` 中的一个 |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --header "Content-Type: application/json" \
     --data '{"name": "My deploy token", "expires_at": "2021-01-01", "username": "custom-user", "scopes": ["read_repository"]}' \
     "https://gitlab.example.com/api/v4/projects/5/deploy_tokens/"
```

响应示例：

```json
{
  "id": 1,
  "name": "My deploy token",
  "username": "custom-user",
  "expires_at": "2021-01-01T00:00:00.000Z",
  "token": "jMRvtPNxrn3crTAGukpZ",
  "revoked": false,
  "expired": false,
  "scopes": [
    "read_repository"
  ]
}
```

### 删除项目部署令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/21811) -->引入于极狐GitLab 12.9。

从项目中移除部署令牌。

```plaintext
DELETE /projects/:id/deploy_tokens/:token_id
```

参数：

| 参数         | 类型             | 是否必需                   | 描述                                                            |
|------------|----------------|------------------------|---------------------------------------------------------------|
| `id`       | integer/string | **{check-circle}** Yes | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `token_id` | integer        | **{check-circle}** Yes | 部署令牌的 ID                                |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" \
    "https://gitlab.example.com/api/v4/projects/5/deploy_tokens/13"
```

## 群组部署令牌

至少具有群组维护者角色的用户可以列出群组部署令牌。只有群组所有者可以创建和删除组部署令牌。

### 列出群组部署令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/21811) -->引入于极狐GitLab 12.9。

获取群组部署令牌的列表。

```plaintext
GET /groups/:id/deploy_tokens
```

参数：

| 参数       | 类型             | 是否必需                   | 描述                                                    |
|:---------|:---------------|:-----------------------|:------------------------------------------------------|
| `id`     | integer/string | **{check-circle}** Yes | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `active` | boolean        | **{dotted-circle}** No | 受限于有效状态                                               |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/deploy_tokens"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "MyToken",
    "username": "gitlab+deploy-token-1",
    "expires_at": "2020-02-14T00:00:00.000Z",
    "revoked": false,
    "expired": false,
    "scopes": [
      "read_repository",
      "read_registry"
    ]
  }
]
```

### 获取群组部署令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/82467) -->引入于极狐GitLab 14.9。

根据 ID 获取单个群组的部署令牌。

```plaintext
GET /groups/:id/deploy_tokens/:token_id
```

参数：

| 参数         | 类型             | 是否必需                   | 描述                                                            |
|------------|----------------|------------------------|---------------------------------------------------------------|
| `id`       | integer/string | **{check-circle}** Yes | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `token_id` | integer        | **{check-circle}** Yes | 部署令牌的 ID                                 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/deploy_tokens/1"
```

响应示例：

```json
{
  "id": 1,
  "name": "MyToken",
  "username": "gitlab+deploy-token-1",
  "expires_at": "2020-02-14T00:00:00.000Z",
  "revoked": false,
  "expired": false,
  "scopes": [
    "read_repository",
    "read_registry"
  ]
}
```

### 创建群组部署令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/21811) -->引入于极狐GitLab 12.9。

为群组创建新的部署令牌。

```plaintext
POST /groups/:id/deploy_tokens
```

参数：

| 参数           | 类型               | 是否必需                   | 描述                                                                                                                         |
|--------------|------------------|------------------------|----------------------------------------------------------------------------------------------------------------------------|
| `id`         | integer/string   | **{check-circle}** Yes | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)                                                              |
| `name`       | string           | **{check-circle}** Yes | 新的部署令牌的名称                                                                                                                  |
| `expires_at` | datetime         | **{dotted-circle}** No | 部署令牌的过期日期。如果没有提供值，则不会失效。预计为 ISO 8601格式（`2019-03-15T08:00:00Z`）                                                             |
| `username`   | string           | **{dotted-circle}** No | 部署令牌的用户名。默认为 `gitlab+deploy-token-{n}`                                                                                     |
| `scopes`     | array of strings | **{check-circle}** Yes | 表示部署令牌的范围。必须至少是 `read_repository`、`read_registry`、`write_registry`、`read_package_registry` 或 `write_package_registry` 中的一个 |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --header "Content-Type: application/json" \
     --data '{"name": "My deploy token", "expires_at": "2021-01-01", "username": "custom-user", "scopes": ["read_repository"]}' \
     "https://gitlab.example.com/api/v4/groups/5/deploy_tokens/"
```

响应示例：

```json
{
  "id": 1,
  "name": "My deploy token",
  "username": "custom-user",
  "expires_at": "2021-01-01T00:00:00.000Z",
  "token": "jMRvtPNxrn3crTAGukpZ",
  "revoked": false,
  "expired": false,
  "scopes": [
    "read_registry"
  ]
}
```

### 删除群组部署令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/21811) -->引入于极狐GitLab 12.9。

从群组移除部署令牌。

```plaintext
DELETE /groups/:id/deploy_tokens/:token_id
```

参数：

| 参数         | 类型             | 是否必需                   | 描述                                                            |
|------------|----------------|------------------------|---------------------------------------------------------------|
| `id`       | integer/string | **{check-circle}** Yes | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `token_id` | integer        | **{check-circle}** Yes | 部署令牌的 ID                                 |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/deploy_tokens/13"
```
