---
stage: Create
group: Code Review
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
type: reference, api
---

# 合并请求上下文提交 API **(FREE)**

## 列出 MR 上下文提交

获取合并请求上下文提交列表。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/context_commits
```

参数：

| 参数                  | 类型      | 是否必需 | 描述                                                            |
|---------------------|---------|------|---------------------------------------------------------------|
| `id`                | integer | **{check-circle}** Yes  | 经过身份验证的客户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer | **{check-circle}** Yes  | 合并请求的内部 ID                   |

```json
[
    {
        "id": "4a24d82dbca5c11c61556f3b35ca472b7463187e",
        "short_id": "4a24d82d",
        "created_at": "2017-04-11T10:08:59.000Z",
        "parent_ids": null,
        "title": "Update README.md to include `Usage in testing and development`",
        "message": "Update README.md to include `Usage in testing and development`",
        "author_name": "Example \"Sample\" User",
        "author_email": "user@example.com",
        "authored_date": "2017-04-11T10:08:59.000Z",
        "committer_name": "Example \"Sample\" User",
        "committer_email": "user@example.com",
        "committed_date": "2017-04-11T10:08:59.000Z"
    }
]
```

## 创建 MR 上下文提交

创建合并请求上下文提交列表。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/context_commits
```

参数：

| 参数                  | 类型      | 是否必需 | 描述                                                            |
|---------------------|---------|------|---------------------------------------------------------------|
| `id`                | integer | **{check-circle}** Yes  | 经过身份验证的客户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer | **{check-circle}** Yes  | 合并请求的内部 ID                               |

```plaintext
POST /projects/:id/merge_requests/
```

| 参数        | 类型           | 是否必需 | 描述         |
|-----------|--------------|------|------------|
| `commits` | string array | **{check-circle}** Yes | 上下文提交的 SHA |

```json
[
    {
        "id": "6d394385cf567f80a8fd85055db1ab4c5295806f",
        "message": "Added contributing guide\n\nSigned-off-by: Example User <user@example.com>\n",
        "parent_ids": [
            "1a0b36b3cdad1d2ee32457c102a8c0b7056fa863"
        ],
        "authored_date": "2014-02-27T10:05:10.000+02:00",
        "author_name": "Example User",
        "author_email": "user@example.com",
        "committed_date": "2014-02-27T10:05:10.000+02:00",
        "committer_name": "Example User",
        "committer_email": "user@example.com"
    }
]
```

## 删除 MR 上下文提交

删除合并请求上下文提交列表。

```plaintext
DELETE /projects/:id/merge_requests/:merge_request_iid/context_commits
```

参数：

| 参数                  | 类型           | 是否必需 | 描述                                                            |
|---------------------|--------------|------|---------------------------------------------------------------|
| `id`                | integer      | **{check-circle}** Yes  | 经过身份验证的客户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer      | **{check-circle}** Yes  | 合并请求的内部 ID                                                    |
| `commits`           | string array | **{check-circle}** Yes  | 上下文提交的 SHA                                                    |
