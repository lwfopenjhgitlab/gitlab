---
stage: Govern
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 资源标签事件 API **(FREE)**

资源标签事件持续跟踪谁、何时以及哪个标记被添加到（或从其中删除）议题、合并请求或史诗。

## 议题

### 列出项目议题标签事件

获取单个议题的所有标签事件的列表。

```plaintext
GET /projects/:id/issues/:issue_iid/resource_label_events
```

| 参数                   | 类型            | 是否必需 | 描述                                                            |
| ------------------- | ---------------- | ---------- |---------------------------------------------------------------|
| `id`                | integer/string   | yes        | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid`         | integer          | yes        | 议题的 IID                                                       |

```json
[
  {
    "id": 142,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-20T13:38:20.077Z",
    "resource_type": "Issue",
    "resource_id": 253,
    "label": {
      "id": 73,
      "name": "a1",
      "color": "#34495E",
      "description": ""
    },
    "action": "add"
  },
  {
    "id": 143,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-20T13:38:20.077Z",
    "resource_type": "Issue",
    "resource_id": 253,
    "label": {
      "id": 74,
      "name": "p1",
      "color": "#0033CC",
      "description": ""
    },
    "action": "remove"
  }
]
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/resource_label_events"
```

### 获取单个议题标签事件

为特定项目议题返回单个标签事件。

```plaintext
GET /projects/:id/issues/:issue_iid/resource_label_events/:resource_label_event_id
```

参数:

| 参数                   | 类型            | 是否必需 | 描述                                                            |
| --------------- | -------------- | -------- |---------------------------------------------------------------|
| `id`            | integer/string | yes      | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid`     | integer        | yes      | 议题的 IID                                                       |
| `resource_label_event_id` | integer        | yes      | 标签事件的 ID                                                      |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/resource_label_events/1"
```

## 史诗 **(PREMIUM)**

### 列出群组史诗标签事件

获取单个史诗的所有标签事件的列表。

```plaintext
GET /groups/:id/epics/:epic_id/resource_label_events
```

| 参数                   | 类型            | 是否必需 | 描述                                                            |
| ------------------- | ---------------- | ---------- |---------------------------------------------------------------|
| `id`                | integer/string   | yes        | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `epic_id`           | integer          | yes        | 史诗的 ID                                                        |

```json
[
  {
    "id": 106,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-19T11:43:01.746Z",
    "resource_type": "Epic",
    "resource_id": 33,
    "label": {
      "id": 73,
      "name": "a1",
      "color": "#34495E",
      "description": ""
    },
    "action": "add"
  },
  {
    "id": 107,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-19T11:43:01.746Z",
    "resource_type": "Epic",
    "resource_id": 33,
    "label": {
      "id": 37,
      "name": "glabel2",
      "color": "#A8D695",
      "description": ""
    },
    "action": "add"
  }
]
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/epics/11/resource_label_events"
```

### 获取单个史诗标签事件

返回特定群组史诗的单个标签事件。

```plaintext
GET /groups/:id/epics/:epic_id/resource_label_events/:resource_label_event_id
```

参数：

| 参数                   | 类型            | 是否必需 | 描述                                                            |
| --------------- | -------------- | -------- |---------------------------------------------------------------|
| `id`            | integer/string | yes      | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `epic_id`       | integer        | yes      | 史诗的 ID                                                        |
| `resource_label_event_id` | integer        | yes      | 标签事件的 ID                                                      |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/epics/11/resource_label_events/107"
```

## 合并请求

### 列出项目合并请求标签事件

获取单个合并请求所有标签事件的列表。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/resource_label_events
```

| 参数                   | 类型            | 是否必需 | 描述                                                            |
| ------------------- | ---------------- | ---------- |---------------------------------------------------------------|
| `id`                | integer/string   | yes        | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer          | yes        | 合并请求的 IID                                                     |

```json
[
  {
    "id": 119,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-20T06:17:28.394Z",
    "resource_type": "MergeRequest",
    "resource_id": 28,
    "label": {
      "id": 74,
      "name": "p1",
      "color": "#0033CC",
      "description": ""
    },
    "action": "add"
  },
  {
    "id": 120,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-20T06:17:28.394Z",
    "resource_type": "MergeRequest",
    "resource_id": 28,
    "label": {
      "id": 41,
      "name": "project",
      "color": "#D1D100",
      "description": ""
    },
    "action": "add"
  }
]
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/resource_label_events"
```

### 获取单个合并请求标签事件

返回特定项目合并请求的单个标签事件。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/resource_label_events/:resource_label_event_id
```

参数：

| 参数                   | 类型            | 是否必需 | 描述                                                            |
| ------------------- | -------------- | -------- |---------------------------------------------------------------|
| `id`                | integer/string | yes      | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | yes      | 合并请求的 IID                                                     |
| `resource_label_event_id`     | integer        | yes      | 标签事件的 ID                                                      |

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/resource_label_events/120"
```
