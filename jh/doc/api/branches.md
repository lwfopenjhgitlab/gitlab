---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
type: reference, api
---

# 分支 API **(FREE)**

这个 API 工作在[仓库分支](../user/project/repository/branches/index.md)之上。

也可以查看[受保护的分支 API](protected_branches.md)。

## 仓库分支

获取一个项目的按分支名的字母顺序排列的仓库分支列表。

NOTE:
如果对应仓库是公开的仓库，那么这个 API 可以在没有授权的情况下访问。

```plaintext
GET /projects/:id/repository/branches
```

参数：

| 参数     | 类型           | 是否必需 | 描述                                                                               |
| :------- | :------------- | :------- |:---------------------------------------------------------------------------------|
| `id`     | integer/string | yes      | 授权用户拥有的 ID 或 URL 编码的项目路径。                                                        |
| `search` | string         | no       | 返回包含搜索字符串的分支列表。您可以使用 `^term` 来查找以 `term` 作为开头的分支；使用 `term$` 来查找以 `term` 作为结尾的分支。 |
| `regex` |  string    | no   | 返回名称与 [re2](https://github.com/google/re2/wiki/Syntax) 正则表达式匹配的分支列表。             |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/branches"
```

响应示例：

```json
[
  {
    "name": "main",
    "merged": false,
    "protected": true,
    "default": true,
    "developers_can_push": false,
    "developers_can_merge": false,
    "can_push": true,
    "web_url": "https://gitlab.example.com/my-group/my-project/-/tree/main",
    "commit": {
      "author_email": "john@example.com",
      "author_name": "John Smith",
      "authored_date": "2012-06-27T05:51:39-07:00",
      "committed_date": "2012-06-28T03:44:20-07:00",
      "committer_email": "john@example.com",
      "committer_name": "John Smith",
      "id": "7b5c3cc8be40ee161ae89a06bba6229da1032a0c",
      "short_id": "7b5c3cc",
      "title": "add projects API",
      "message": "add projects API",
      "parent_ids": [
        "4ad91d3c1144c406e50c7b33bae684bd6837faf8"
      ]
    }
  },
  ...
]
```

<a href="#get-single-repository-branch">

## 获取单个仓库分支

获取项目的单个仓库分支。

NOTE:
如果对应仓库是公开的仓库，那么这个 API 可以在没有授权的情况下访问。

```plaintext
GET /projects/:id/repository/branches/:branch
```

参数：

| 参数     | 类型           | 是否必需 | 描述                                                                  |
| :------- | :------------- | :------- |:--------------------------------------------------------------------|
| `id`     | integer/string | yes      | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)。 |
| `branch` | string         | yes      | 分支的 [URL 编码的名称](rest/index.md#namespaced-path-encoding)。            |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/branches/main"
```

响应示例：

```json
{
  "name": "master",
  "merged": false,
  "protected": true,
  "default": true,
  "developers_can_push": false,
  "developers_can_merge": false,
  "can_push": true,
  "web_url": "https://gitlab.example.com/my-group/my-project/-/tree/main",
  "commit": {
    "author_email": "john@example.com",
    "author_name": "John Smith",
    "authored_date": "2012-06-27T05:51:39-07:00",
    "committed_date": "2012-06-28T03:44:20-07:00",
    "committer_email": "john@example.com",
    "committer_name": "John Smith",
    "id": "7b5c3cc8be40ee161ae89a06bba6229da1032a0c",
    "short_id": "7b5c3cc",
    "title": "add projects API",
    "message": "add projects API",
    "parent_ids": [
      "4ad91d3c1144c406e50c7b33bae684bd6837faf8"
    ]
  }
}
```

<a href="#protect-repository-branch">

## 受保护的仓库分支

通过查询 [`POST /projects/:id/protected_branches`](protected_branches.md#protect-repository-branches) 来获取
关于受保护的仓库分支的信息。

<a href="#unprotect-repository-branch">

## 不受保护的仓库分支

通过查询 [`DELETE /projects/:id/protected_branches/:name`](protected_branches.md#unprotect-repository-branches) 来获取
关于不受保护的仓库分支的信息。

<a href="#create-repository-branch">

## 创建仓库分支

在仓库中创建一个新的分支。

```plaintext
POST /projects/:id/repository/branches
```

参数：

| 参数     | 类型    | 是否必需 | 描述                            |
| :------- | :------ | :------- |:------------------------------|
| `id`     | integer | yes      | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)。 |
| `branch` | string  | yes      | 要创建分支的名字。                     |
| `ref`    | string  | yes      | 要创建新分支的基准提交的 SHA 值或者是基准分支的名字。 |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/branches?branch=newbranch&ref=main"
```

响应示例：

```json
{
  "commit": {
    "author_email": "john@example.com",
    "author_name": "John Smith",
    "authored_date": "2012-06-27T05:51:39-07:00",
    "committed_date": "2012-06-28T03:44:20-07:00",
    "committer_email": "john@example.com",
    "committer_name": "John Smith",
    "id": "7b5c3cc8be40ee161ae89a06bba6229da1032a0c",
    "short_id": "7b5c3cc",
    "title": "add projects API",
    "message": "add projects API",
    "parent_ids": [
      "4ad91d3c1144c406e50c7b33bae684bd6837faf8"
    ]
  },
  "name": "newbranch",
  "merged": false,
  "protected": false,
  "default": false,
  "developers_can_push": false,
  "developers_can_merge": false,
  "can_push": true,
  "web_url": "https://gitlab.example.com/my-group/my-project/-/tree/newbranch"
}
```

<a href="#delete-repository-branch">

## 删除仓库分支

删除一个仓库的分支。

NOTE:
为了避免错误操作，需要提供一个解释性的说明。

```plaintext
DELETE /projects/:id/repository/branches/:branch
```

参数：

| 参数     | 类型           | 是否必需 | 描述                                      |
| :------- | :------------- | :------- | :---------------------------------------- |
| `id`     | integer/string | yes      | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)。 |
| `branch` | string         | yes      | 分支的名字。                              |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/branches/newbranch"
```

<a href="#delete-merged-branches">

## 删除合并的分支

删除所有已经合并进入项目的默认分支的分支。

NOTE:
[受保护的分支](../user/project/protected_branches.md)不会被这个操作删除。

```plaintext
DELETE /projects/:id/repository/merged_branches
```

参数：

| 参数 | 类型           | 是否必需 | 描述                                                                  |
| :--- | :------------- | :------- |:--------------------------------------------------------------------|
| `id` | integer/string | yes      | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)。 |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/merged_branches"
```
