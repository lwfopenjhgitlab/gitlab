---
stage: Manage
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# REST API 资源 **(FREE)**

[极狐GitLab REST API](index.md) 的可用资源可以在以下上下文中进行分组：

- [项目](#project-resources)
- [群组](#group-resources)
- [独立式部署](#standalone-resources)

请参见：

- [为多个项目添加部署密钥](deploy_keys.md#add-deploy-keys-to-multiple-projects)
- [多个模板的 API 资源](#templates-api-resources)

<a id="project-resources"></a>

## 项目资源

以下 API 资源在项目上下文中可用：

| 资源                                                     | 可用端点                                                                                                                                                                              |
|:-------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [访问请求](access_requests.md)                             | `/projects/:id/access_requests`（群组同样可用）                                                                                                                                           |
| [访问令牌](project_access_tokens.md)                       | `/projects/:id/access_tokens`（群组同样可用）                                                                                                                                             |
| [代理](cluster_agents.md)                                | `/projects/:id/cluster_agents`                                                                                                                                                    |
| [分支](branches.md)                                      | `/projects/:id/repository/branches/`, `/projects/:id/repository/merged_branches`                                                                                                  |
| [提交](commits.md)                                       | `/projects/:id/repository/commits`, `/projects/:id/statuses`                                                                                                                      |
| [容器镜像库](container_registry.md)                         | `/projects/:id/registry/repositories`                                                                                                                                             |
| [自定义属性](custom_attributes.md)                          | `/projects/:id/custom_attributes`（群组和用户同样可用）                                                                                                                                      |
| [Composer 发行版](packages/composer.md)                   | `/projects/:id/packages/composer` （群组同样可用）                                                                                                                                        |
| [Conan 发行版](packages/conan.md)                         | `/projects/:id/packages/conan` （独立式部署同样可用）                                                                                                                                        |
| [Debian 发行版](packages/debian_project_distributions.md) | `/projects/:id/debian_distributions`（群组同样可用）                                                                                                                                      |
| [Debian 软件包](packages/debian.md)                       | `/projects/:id/packages/debian` （群组同样可用）                                                                                                                                          |
| [依赖项](dependencies.md) **(ULTIMATE)**                  | `/projects/:id/dependencies`                                                                                                                                                      |
| [部署密钥](deploy_keys.md)                                 | `/projects/:id/deploy_keys`（独立式部署同样可用）                                                                                                                                            |
| [部署令牌](deploy_tokens.md)                               | `/projects/:id/deploy_tokens`（群组和独立式部署同样可用）                                                                                                                                       |
| [部署](deployments.md)                                   | `/projects/:id/deployments`                                                                                                                                                       |
| [讨论](discussions.md)（主题模式评论）                           | `/projects/:id/issues/.../discussions`, `/projects/:id/snippets/.../discussions`, `/projects/:id/merge_requests/.../discussions`, `/projects/:id/commits/.../discussions`（群组同样可用） |
| [备注草稿](draft_notes.md)（评论）                             | `/projects/:id/merge_requests/.../draft_notes`                                                                                                                                    |
| [表情符号反应](award_emoji.md)                               | `/projects/:id/issues/.../award_emoji`、`/projects/:id/merge_requests/.../award_emoji`、`/projects/:id/snippets/.../award_emoji`                                                    |
| [环境](environments.md)                                  | `/projects/:id/environments`                                                                                                                                                      |
| [错误跟踪](error_tracking.md)                              | `/projects/:id/error_tracking/settings`                                                                                                                                           |
| [事件](events.md)                                        | `/projects/:id/events`（用户和独立式部署同样可用）                                                                                                                                              |
| [功能标志用户列表](feature_flag_user_lists.md)                 | `/projects/:id/feature_flags_user_lists`                                                                                                                                          |
| [功能标志](feature_flags.md)                               | `/projects/:id/feature_flags`                                                                                                                                                     |
| [冻结周期](freeze_periods.md)                              | `/projects/:id/freeze_periods`                                                                                                                                                    |
| [Go proxy](packages/go_proxy.md)                       | `/projects/:id/packages/go`                                                                                                                                                       |
| [Helm 仓库](packages/helm.md)                            | `/projects/:id/packages/helm_repository`                                                                                                                                          |
| [集成](integrations.md)（曾名为"服务"）                         | `/projects/:id/integrations`                                                                                                                                                      |
| [邀请](invitations.md)                                   | `/projects/:id/invitations`（群组同样可用）                                                                                                                                               |
| [议题板](boards.md)                                       | `/projects/:id/boards`                                                                                                                                                            |
| [议题链接](issue_links.md)                                 | `/projects/:id/issues/.../links`                                                                                                                                                  |
| [议题统计](issues_statistics.md)                           | `/projects/:id/issues_statistics`（群组和独立式部署同样可用）                                                                                                                                   |
| [议题](issues.md)                                        | `/projects/:id/issues`（群组和独立式部署同样可用）                                                                                                                                              |
| [迭代](iterations.md) **(PREMIUM)**                      | `/projects/:id/iterations`（群组同样可用）                                                                                                                                                |
| [项目 CI/CD 作业令牌范围](project_job_token_scopes.md)         | `/projects/:id/job_token_scope`                                                                                                                                              |
| [作业](jobs.md)                                          | `/projects/:id/jobs`, `/projects/:id/pipelines/.../jobs`                                                                                                                          |
| [作业产物](job_artifacts.md)                               | `/projects/:id/jobs/:job_id/artifacts`                                                                                                                                            |
| [标记](labels.md)                                        | `/projects/:id/labels`                                                                                                                                                            |
| [管理许可证](managed_licenses.md) **(ULTIMATE)**            | `/projects/:id/managed_licenses`                                                                                                                                                  |
| [Maven 仓库](packages/maven.md)                          | `/projects/:id/packages/maven`（群组和独立式部署同样可用）                                                                                                                                      |
| [成员](members.md)                                       | `/projects/:id/members`（群组同样可用）                                                                                                                                                   |
| [合并请求审批](merge_request_approvals.md) **(PREMIUM)**     | `/projects/:id/approvals`, `/projects/:id/merge_requests/.../approvals`                                                                                                           |
| [合并请求](merge_requests.md)                              | `/projects/:id/merge_requests`（群组和独立式部署同样可用）                                                                                                                                      |
| [合并列车](merge_trains.md)                                | `/projects/:id/merge_trains`                                                                                                                                                      |
| [元数据](metadata.md)                                     | `/metadata`                                                                                                                                                                       |
| [备注](notes.md)（评论）                                     | `/projects/:id/issues/.../notes`, `/projects/:id/snippets/.../notes`, `/projects/:id/merge_requests/.../notes` （群组同样可用）                                                           |
| [通知设置](notification_settings.md)                       | `/projects/:id/notification_settings`（群组和独立式部署同样可用）                                                                                                                               |
| [NPM 仓库](packages/npm.md)                              | `/projects/:id/packages/npm`                                                                                                                                                      |
| [NuGet 软件包](packages/nuget.md)                         | `/projects/:id/packages/nuget` （群组同样可用）                                                                                                                                           |
| [软件包](packages.md)                                     | `/projects/:id/packages`                                                                                                                                                          |
| [Pages 域](pages_domains.md)                            | `/projects/:id/pages`（独立式部署同样可用）                                                                                                                                                  |
| [流水线计划](pipeline_schedules.md)                         | `/projects/:id/pipeline_schedules`                                                                                                                                                |
| [流水线触发](pipeline_triggers.md)                          | `/projects/:id/triggers`                                                                                                                                                          |
| [流水线](pipelines.md)                                    | `/projects/:id/pipelines`                                                                                                                                                         |
| [项目徽章](project_badges.md)                              | `/projects/:id/badges`                                                                                                                                                            |
| [项目集群](project_clusters.md)                            | `/projects/:id/clusters`                                                                                                                                                          |
| [项目导入/导出](project_import_export.md)                    | `/projects/:id/export`, `/projects/import`, `/projects/:id/import`                                                                                                                |
| [项目里程碑](milestones.md)                                 | `/projects/:id/milestones`                                                                                                                                                        |
| [项目代码片段](project_snippets.md)                          | `/projects/:id/snippets`                                                                                                                                                          |
| [项目模板](project_templates.md)                           | `/projects/:id/templates`                                                                                                                                                         |
| [项目漏洞](project_vulnerabilities.md) **(ULTIMATE)**      | `/projects/:id/vulnerabilities`                                                                                                                                                   |
| [项目 Wiki](wikis.md)                                    | `/projects/:id/wikis`                                                                                                                                                             |
| [项目级别变量](project_level_variables.md)                   | `/projects/:id/variables`                                                                                                                                                         |
| [项目](projects.md) 包括设置网络钩子                             | `/projects`, `/projects/:id/hooks`（用户同样可用）                                                                                                                                        |
| [受保护分支](protected_branches.md)                         | `/projects/:id/protected_branches`                                                                                                                                                |
| [受保护环境](protected_environments.md)                     | `/projects/:id/protected_environments`                                                                                                                                            |
| [受保护标签](protected_tags.md)                             | `/projects/:id/protected_tags`                                                                                                                                                    |
| [PyPI 软件包](packages/pypi.md)                           | `/projects/:id/packages/pypi` （群组同样可用）                                                                                                                                            |
| [发布链接](releases/links.md)                              | `/projects/:id/releases/.../assets/links`                                                                                                                                         |
| [发布](releases/index.md)                                | `/projects/:id/releases`                                                                                                                                                          |
| [远端镜像](remote_mirrors.md)                              | `/projects/:id/remote_mirrors`                                                                                                                                                    |
| [仓库](repositories.md)                                  | `/projects/:id/repository`                                                                                                                                                        |
| [仓库文件](repository_files.md)                            | `/projects/:id/repository/files`                                                                                                                                                  |
| [仓库子模块](repository_submodules.md)                      | `/projects/:id/repository/submodules`                                                                                                                                             |
| [资源标记事件](resource_label_events.md)                     | `/projects/:id/issues/.../resource_label_events`, `/projects/:id/merge_requests/.../resource_label_events` （群组同样可用）                                                               |
| [Ruby gems](packages/rubygems.md)                      | `/projects/:id/packages/rubygems`                                                                                                                                                 |
| [Runner](runners.md)                                   | `/projects/:id/runners`（独立式部署同样可用）                                                                                                                                                |
| [搜索](search.md)                                        | `/projects/:id/search`（群组和独立式部署同样可用）                                                                                                                                              |
| [标签](tags.md)                                          | `/projects/:id/repository/tags`                                                                                                                                                   |
| [Terraform 模块](packages/terraform-modules.md)          | `/projects/:id/packages/terraform/modules` （独立式部署同样可用）                                                                                                                            |
| [用户星标的指标仪表板](metrics_user_starred_dashboards.md )      | `/projects/:id/metrics/user_starred_dashboards`                                                                                                                                   |
| [可见评审讨论](visual_review_discussions.md) **(PREMIUM)**   | `/projects/:id/merge_requests/:merge_request_id/visual_review_discussions`                                                                                                        |
| [漏洞](vulnerabilities.md) **(ULTIMATE)**                | `/vulnerabilities/:id`                                                                                                                                                            |
| [漏洞导出](vulnerability_exports.md) **(ULTIMATE)**        | `/projects/:id/vulnerability_exports`                                                                                                                                             |
| [漏洞发现](vulnerability_findings.md) **(ULTIMATE)**       | `/projects/:id/vulnerability_findings`                                                                                                                                            |

<a id="group-resources"></a>

## 群组资源

以下 API 资源在群组上下文中可用：

| 资源                                                   | 可用端点                                                  |
|:-----------------------------------------------------|:------------------------------------------------------|
| [访问请求](access_requests.md)                           | `/groups/:id/access_requests/`（项目同样可用）                |
| [访问令牌](group_access_tokens.md)                       | `/groups/:id/access_tokens`（项目同样可用）                   |
| [自定义属性](custom_attributes.md)                        | `/groups/:id/custom_attributes`（项目和用户同样可用）            |
| [Debian 发行版](packages/debian_group_distributions.md) | `/groups/:id/-/packages/debian`（项目同样可用）               |
| [部署令牌](deploy_tokens.md)                             | `/groups/:id/deploy_tokens`（项目和独立式部署同样可用）             |
| [讨论](discussions.md)（评论和主题）                          | `/groups/:id/epics/.../discussions`（项目同样可用）           |
| [史诗议题](epic_issues.md) **(PREMIUM)**                 | `/groups/:id/epics/.../issues`                        |
| [史诗链接](epic_links.md) **(PREMIUM)**                  | `/groups/:id/epics/.../epics`                         |
| [史诗](epics.md) **(PREMIUM)**                         | `/groups/:id/epics`                                   |
| [群组](groups.md)                                      | `/groups`, `/groups/.../subgroups`                    |
| [群组徽章](group_badges.md)                              | `/groups/:id/badges`                                  |
| [群组议题板](group_boards.md)                             | `/groups/:id/boards`                                  |
| [群组迭代](group_iterations.md) **(PREMIUM)**            | `/groups/:id/iterations`（项目同样可用）                      |
| [群组标记](group_labels.md)                              | `/groups/:id/labels`                                  |
| [群组级别的变量](group_level_variables.md)                  | `/groups/:id/variables`                               |
| [群组里程碑](group_milestones.md)                         | `/groups/:id/milestones`                              |
| [群组发布](group_releases.md)                            | `/groups/:id/releases`                                |
| [群组 Wiki](group_wikis.md) **(PREMIUM)**              | `/groups/:id/wikis`                                   |
| [邀请](invitations.md)                                 | `/groups/:id/invitations`（项目同样可用）                     |
| [议题](issues.md)                                      | `/groups/:id/issues`（项目和独立式部署同样可用）                    |
| [议题统计](issues_statistics.md)                         | `/groups/:id/issues_statistics`（项目和独立式部署同样可用）         |
| [链接史诗](linked_epics.md)                              | `/groups/:id/epics/.../related_epics`                 |
| [成员角色](member_roles.md)                              |  `/groups/:id/member_roles`           |
| [成员](members.md)                                     | `/groups/:id/members`（项目同样可用）                         |
| [合并请求](merge_requests.md)                            | `/groups/:id/merge_requests`（项目和独立式部署同样可用）            |
| [备注](notes.md)（评论）                                   | `/groups/:id/epics/.../notes`（项目同样可用）                 |
| [通知设置](notification_settings.md)                     | `/groups/:id/notification_settings`（项目和独立式部署同样可用）     |
| [资源标记事件](resource_label_events.md)                   | `/groups/:id/epics/.../resource_label_events`（项目同样可用） |
| [搜索](search.md)                                      | `/groups/:id/search`（项目和独立式部署同样可用）                    |

<a id="standalone-resources"></a>

## 独立式资源

以下 API 资源在项目和群组上下文之外（包括`/users`）可用：

| 资源                                                                                            | 可用端点                                                                                                                                   |
|:----------------------------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------|
| [外观](appearance.md) **(FREE SELF)**                                                           | `/application/appearance`                                                                                                              |
| [应用](applications.md)                                                                         | `/applications`                                                                                                                        |
| [审计事件](audit_events.md) **(PREMIUM SELF)**                                                    | `/audit_events`                                                                                                                        |
| [头像](avatar.md)                                                                               | `/avatar`                                                                                                                              |
| [广播消息](broadcast_messages.md)                                                                 | `/broadcast_messages`                                                                                                                  |
| [代码片段](snippets.md)                                                                           | `/snippets`                                                                                                                            |
| [自定义属性](custom_attributes.md)                                                                 | `/users/:id/custom_attributes`（群组和项目同样可用）                                                                                              |
| [部署密钥](deploy_keys.md)                                                                        | `/deploy_keys`（项目同样可用）                                                                                                                 |
| [部署令牌](deploy_tokens.md)                                                                      | `/deploy_tokens`（群组和项目同样可用）                                                                                                            |
| [事件](events.md)                                                                               | `/events`, `/users/:id/events` （项目同样可用）                                                                                                |
| [功能标志](features.md)                                                                           | `/features`                                                                                                                            |
| [Geo 节点](geo_nodes.md) **(PREMIUM SELF)**                                                     | `/geo_nodes`                                                                                                                           |
| [群组活动分析](group_activity_analytics.md)                                                         | `/analytics/group_activity/{issues_count}`                                                                                             |
| [群组仓库存储移动](group_repository_storage_moves.md) **(PREMIUM SELF)**                              | `/group_repository_storage_moves`                                                                                                      |
| [从 GitHub 导入仓库](import.md#import-repository-from-github)                                      | `/import/github` |
| [从 Bitbucket Server 导入仓库](import.md#import-repository-from-bitbucket-server)                  | `/import/bitbucket_server` |
| [实例集群](instance_clusters.md) **(FREE SELF)**                                                  | `/admin/clusters`                                                                                                                      |
| [实例级别的 CI/CD 变量](instance_level_ci_variables.md) **(FREE SELF)**                              | `/admin/ci/variables`                                                                                                                  |
| [议题统计](issues_statistics.md)                                                                  | `/issues_statistics`（群组和项目同样可用）                                                                                                        |
| [议题](issues.md)                                                                               | `/issues`（群组和项目同样可用）                                                                                                                   |
| [作业](jobs.md)                                                                                 | `/job`                                                                                                                                 |
| [密钥](keys.md)                                                                                 | `/keys`                                                                                                                                |
| [许可证](license.md) **(FREE SELF)**                                                             | `/license`                                                                                                                             |
| [Markdown](markdown.md)                                                                       | `/markdown`                                                                                                                            |
| [合并请求](merge_requests.md)                                                                     | `/merge_requests`（群组和项目同样可用）                                                                                                           |
| [指标仪表板注释](metrics_dashboard_annotations.md)                                                   | `/environments/:id/metrics_dashboard/annotations`, `/clusters/:id/metrics_dashboard/annotations`                                       |
| [命名空间](namespaces.md)                                                                         | `/namespaces`                                                                                                                          |
| [通知设置](notification_settings.md)                                                              | `/notification_settings`（群组和项目同样可用）                                                                                                    |
| [Pages 域](pages_domains.md)                                                                   | `/pages/domains` （项目同样可用）                                                                                                              |
| [个人访问令牌](personal_access_tokens.md)                                                           | `/personal_access_tokens`                                                                                                              |
| [计划限制](plan_limits.md)                                                                        | `/application/plan_limits`                                                                                                             |
| [项目仓库存储移动](project_repository_storage_moves.md) **(FREE SELF)**                               | `/project_repository_storage_moves`                                                                                                    |
| [项目](projects.md)                                                                             | `/users/:id/projects`（项目同样可用）                                                                                                          |
| [Runner](runners.md)                                                                          | `/runners`（项目同样可用）                                                                                                                     |
| [搜索](search.md)                                                                               | `/search`（群组和项目同样可用）                                                                                                                   |
| [服务数据](usage_data.md)                                                                         | `/usage_data`（仅对极狐GitLab 实例[管理员](../user/permissions.md)用户可用） |
| [设置](settings.md) **(FREE SELF)**                                                             | `/application/settings`                                                                                                                |
| [Sidekiq 指标](sidekiq_metrics.md) **(FREE SELF)**                                              | `/sidekiq`                                                                                                                             |
| [Sidekiq 队列管理](admin_sidekiq_queues.md) **(FREE SELF)**                                       | `/admin/sidekiq/queues/:queue_name`                                                                                                    |
| [代码片段仓库存储移动](snippet_repository_storage_moves.md) **(FREE SELF)**                             | `/snippet_repository_storage_moves`                                                                                                    |
| [统计](statistics.md)                                                                           | `/application/statistics`                                                                                                              |
| [建议](suggestions.md)                                                                          | `/suggestions`                                                                                                                         |
| [系统钩子](system_hooks.md)                                                                       | `/hooks`                                                                                                                               |
| [待办事项](todos.md)                                                                              | `/todos`                                                                                                                               |
| [主题](topics.md)                                                                               | `/topics`                                                                                                                              |
| [用户](users.md)                                                                                | `/users`                                                                                                                               |
| [验证 `.gitlab-ci.yml` 文件](lint.md)                                                             | `/lint`                                                                                                                                |
| [版本](version.md)                                                                              | `/version`                                                                                                                             |

<a id="templates-api-resources"></a>

## 模板 API 资源

端点在以下内容中可用：

- [Dockerfile 模板](templates/dockerfiles.md)
- [`.gitignore` 模板](templates/gitignores.md)
- [极狐GitLab CI/CD YAML 模板](templates/gitlab_ci_ymls.md)
- [开源证书模板](templates/licenses.md)
