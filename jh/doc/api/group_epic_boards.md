---
stage: Plan
group: Product Planning
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组史诗看板 API **(PREMIUM)**

> 引入于极狐GitLab 15.9。

对[群组史诗看板](../user/group/epics/epic_boards.md)的每个 API 调用都必须经过身份验证。

如果用户不是某个群组的成员并且该群组是私有的的，则 `GET` 请求结果会返回 `404` 状态码。

## 列出群组中的所有史诗看板

列出给定群组中的史诗看板。

```plaintext
GET /groups/:id/epic_boards
```

| 参数   | 类型             | 是否必需 | 描述                                                                                                              |
|------|----------------|------|-----------------------------------------------------------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户可以访问的群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/epic_boards"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "group epic board",
    "group": {
      "id": 5,
      "name": "Documentcloud",
      "web_url": "http://example.com/groups/documentcloud"
    },
    "hide_backlog_list": false,
    "hide_closed_list": false,
    "labels": [
      {
        "id": 1,
        "title": "Board Label",
        "color": "#c21e56",
        "description": "label applied to the epic board",
        "group_id": 5,
        "project_id": null,
        "template": false,
        "text_color": "#FFFFFF",
        "created_at": "2023-01-27T10:40:59.738Z",
        "updated_at": "2023-01-27T10:40:59.738Z"
      }
    ],
    "lists": [
      {
        "id": 1,
        "label": {
          "id": 69,
          "name": "Testing",
          "color": "#F0AD4E",
          "description": null
        },
        "position": 1,
        "list_type": "label"
      },
      {
        "id": 2,
        "label": {
          "id": 70,
          "name": "Ready",
          "color": "#FF0000",
          "description": null
        },
        "position": 2,
        "list_type": "label"
      },
      {
        "id": 3,
        "label": {
          "id": 71,
          "name": "Production",
          "color": "#FF5F00",
          "description": null
        },
        "position": 3,
        "list_type": "label"
      }
    ]
  }
]
```

## 单个群组史诗看板

获取单个群组史诗看板。

```plaintext
GET /groups/:id/epic_boards/:board_id
```

| 参数         | 类型             | 是否必需 | 描述                                                                         |
|------------|----------------|------|----------------------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户可以访问的群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 史诗看板 ID                                                    |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/epic_boards/1"
```

响应示例：

```json
  {
    "id": 1,
    "name": "group epic board",
    "group": {
      "id": 5,
      "name": "Documentcloud",
      "web_url": "http://example.com/groups/documentcloud"
    },
    "labels": [
      {
        "id": 1,
        "title": "Board Label",
        "color": "#c21e56",
        "description": "label applied to the epic board",
        "group_id": 5,
        "project_id": null,
        "template": false,
        "text_color": "#FFFFFF",
        "created_at": "2023-01-27T10:40:59.738Z",
        "updated_at": "2023-01-27T10:40:59.738Z"
      }
    ],
    "lists" : [
      {
        "id" : 1,
        "label" : {
          "id": 69,
          "name" : "Testing",
          "color" : "#F0AD4E",
          "description" : null
        },
        "position" : 1,
        "list_type": "label"
      },
      {
        "id" : 2,
        "label" : {
          "id": 70,
          "name" : "Ready",
          "color" : "#FF0000",
          "description" : null
        },
        "position" : 2,
        "list_type": "label"
      },
      {
        "id" : 3,
        "label" : {
          "id": 71,
          "name" : "Production",
          "color" : "#FF5F00",
          "description" : null
        },
        "position" : 3,
        "list_type": "label"
      }
    ]
  }
```

## 列出群组史诗看板列表

> 引入于极狐GitLab 15.9。

获取史诗看板列表。不包括 `open` 和 `closed` 的列表。

```plaintext
GET /groups/:id/epic_boards/:board_id/lists
```

| 参数         | 类型             | 是否必需 | 描述                                                                         |
|------------|----------------|------|----------------------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户可以访问的群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 史诗看板 ID                                                                    |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/epic_boards/1/lists"
```

响应示例：

```json
[
  {
    "id" : 1,
    "label" : {
      "name" : "Testing",
      "color" : "#F0AD4E",
      "description" : null
    },
    "position" : 1,
    "list_type" : "label",
    "collapsed" : false
  },
  {
    "id" : 2,
    "label" : {
      "name" : "Ready",
      "color" : "#FF0000",
      "description" : null
    },
    "position" : 2,
    "list_type" : "label",
    "collapsed" : false
  },
  {
    "id" : 3,
    "label" : {
      "name" : "Production",
      "color" : "#FF5F00",
      "description" : null
    },
    "position" : 3,
    "list_type" : "label",
    "collapsed" : false
  }
]
```

## 单个群组史诗看板列表

> 引入于极狐GitLab 15.9。

获取单个看板列表。

```plaintext
GET /groups/:id/epic_boards/:board_id/lists/:list_id
```

| 参数         | 类型             | 是否必需 | 描述                                                                         |
|------------|----------------|------|----------------------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户可以访问的群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 史诗看板 ID                                                                    |
| `list_id`  | integer        | yes  | 史诗看板列表 ID                                          |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/epic_boards/1/lists/1"
```

响应示例：

```json
{
  "id" : 1,
  "label" : {
    "name" : "Testing",
    "color" : "#F0AD4E",
    "description" : null
  },
  "position" : 1,
  "list_type" : "label",
  "collapsed" : false
}
```
