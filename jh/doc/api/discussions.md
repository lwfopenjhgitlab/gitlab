---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference, api
---

# 讨论 API **(FREE)**

讨论是以下实体中备注的集合：

- 代码片段
- 议题
- [史诗](../user/group/epics/index.md)
- 合并请求
- 提交

包括[标准和主题评论](../user/discussions/index.md)和系统备注。
系统备注是关于对象更改的备注（例如，当里程碑更改时）。
标记备注不被包含在此 API 中，而是作为单独的事件记录在[资源标签事件](resource_label_events.md)中。

## 讨论分页

默认情况下，`GET` 请求一次返回 20 个结果，因为 API 返回的结果是经过分页的。

阅读更多关于[分页](rest/index.md#pagination)的内容。

## 议题

### 列出项目议题讨论项目

获取单个议题的所有讨论项目。

```plaintext
GET /projects/:id/issues/:issue_iid/discussions
```

| 参数                 | 类型              | 是否必需    |  描述         |
| ------------------- | ---------------- | ---------- | ------------ |
| `id`                | integer/string   | yes        | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid`         | integer          | yes        | 议题的 IID |

```json
[
  {
    "id": "6a9c1750b37d513a43987b574953fceb50b03ce7",
    "individual_note": false,
    "notes": [
      {
        "id": 1126,
        "type": "DiscussionNote",
        "body": "discussion text",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-03T21:54:39.668Z",
        "updated_at": "2018-03-03T21:54:39.668Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Issue",
        "project_id": 5,
        "noteable_iid": null
      },
      {
        "id": 1129,
        "type": "DiscussionNote",
        "body": "reply to the discussion",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-04T13:38:02.127Z",
        "updated_at": "2018-03-04T13:38:02.127Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Issue",
        "project_id": 5,
        "noteable_iid": null,
        "resolvable": false
      }
    ]
  },
  {
    "id": "87805b7c09016a7058e91bdbe7b29d1f284a39e6",
    "individual_note": true,
    "notes": [
      {
        "id": 1128,
        "type": null,
        "body": "a single comment",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-04T09:17:22.520Z",
        "updated_at": "2018-03-04T09:17:22.520Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Issue",
        "project_id": 5,
        "noteable_iid": null,
        "resolvable": false
      }
    ]
  }
]
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>"\
  "https://gitlab.example.com/api/v4/projects/5/issues/11/discussions"
```

### 获得议题的单个讨论

返回特定项目议题的单个讨论。

```plaintext
GET /projects/:id/issues/:issue_iid/discussions/:discussion_id
```

参数：

| 参数             | 类型           | 是否必需   |  描述       |
| --------------- | -------------- | -------- | ----------- |
| `id`            | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid`     | integer        | yes      | 议题的 IID |
| `discussion_id` | integer        | yes      | 讨论项的 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/issues/11/discussions/<discussion_id>"
```

### 创建新的议题主题

为单个项目议题创建新主题。类似于创建一个备注，不同的是可以在主题中添加新评论（回复）。

```plaintext
POST /projects/:id/issues/:issue_iid/discussions
```

参数：

| 参数             | 类型           | 是否必需   | 描述                                                            |
| --------------- | -------------- | -------- |---------------------------------------------------------------|
| `id`            | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)  |
| `issue_iid`     | integer        | yes      | 议题的 IID                                                       |
| `body`          | string         | yes      | 主题的内容                                                         |
| `created_at`    | string         | no       | 日期时间字符串，ISO 8601 格式，如：`2016-03-11T03:45:40Z`。需要管理员或项目/组所有者的权限 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/issues/11/discussions?body=comment"
```

### 向现有议题主题添加备注

为现有主题添加备注，也可以[从单个评论创建主题](../user/discussions/index.md#create-a-thread-by-replying-to-a-standard-comment)。

WARNING:
评论可以添加到标准评论之外的其他项目中，例如系统备注，使它们成为一个主题。

```plaintext
POST /projects/:id/issues/:issue_iid/discussions/:discussion_id/notes
```

参数：

| 参数             | 类型           | 是否必需   | 描述                                                             |
| --------------- | -------------- | -------- |----------------------------------------------------------------|
| `id`            | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)   |
| `issue_iid`     | integer        | yes      | 议题的 IID                                                        |
| `discussion_id` | integer        | yes      | 主题的 ID                                                         |
| `note_id`       | integer        | yes      | 主题备注的 ID                                                       |
| `body`          | string         | yes      | 备注或回复的内容                                                       |
| `created_at`    | string         | no       | 日期时间字符串，ISO 8601 格式，如： `2016-03-11T03:45:40Z`。需要管理员或项目/组所有者的权限 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/issues/11/discussions/<discussion_id>/notes?body=comment"
```

### 修改现有议题主题中的备注

修改一个议题中已经存在的主题的备注。

```plaintext
PUT /projects/:id/issues/:issue_iid/discussions/:discussion_id/notes/:note_id
```

参数：

| 参数             | 类型           | 是否必需   |  描述       |
| --------------- | -------------- | -------- | ----------- |
| `id`            | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid`     | integer        | yes      | 议题的 IID |
| `discussion_id` | integer        | yes      | 主题的 ID |
| `note_id`       | integer        | yes      | 主题备注的 ID |
| `body`          | string         | yes      | 备注或回复的内容 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/issues/11/discussions/<discussion_id>/notes/1108?body=comment"
```

### 删除议题主题备注

删除议题中已经存在的主题备注。

```plaintext
DELETE /projects/:id/issues/:issue_iid/discussions/:discussion_id/notes/:note_id
```

参数：

| 参数             | 类型           | 是否必需   |  描述       |
| --------------- | -------------- | -------- | ----------- |
| `id`            | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid`     | integer        | yes      | 议题的 IID |
| `discussion_id` | integer        | yes      | 讨论的 ID |
| `note_id`       | integer        | yes      | 讨论备注的 ID |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/issues/11/discussions/636"
```

## 代码片段

### 项目代码片段主题列表

获取单个代码片的所有主题列表。

```plaintext
GET /projects/:id/snippets/:snippet_id/discussions
```

| 参数             | 类型           | 是否必需   |  描述       |
| ------------------- | ---------------- | ---------- | ------------|
| `id`                | integer/string   | yes        | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `snippet_id`        | integer          | yes        | 代码片的 ID |

```json
[
  {
    "id": "6a9c1750b37d513a43987b574953fceb50b03ce7",
    "individual_note": false,
    "notes": [
      {
        "id": 1126,
        "type": "DiscussionNote",
        "body": "discussion text",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-03T21:54:39.668Z",
        "updated_at": "2018-03-03T21:54:39.668Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Snippet",
        "project_id": 5,
        "noteable_iid": null
      },
      {
        "id": 1129,
        "type": "DiscussionNote",
        "body": "reply to the discussion",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-04T13:38:02.127Z",
        "updated_at": "2018-03-04T13:38:02.127Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Snippet",
        "project_id": 5,
        "noteable_iid": null,
        "resolvable": false
      }
    ]
  },
  {
    "id": "87805b7c09016a7058e91bdbe7b29d1f284a39e6",
    "individual_note": true,
    "notes": [
      {
        "id": 1128,
        "type": null,
        "body": "a single comment",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-04T09:17:22.520Z",
        "updated_at": "2018-03-04T09:17:22.520Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Snippet",
        "project_id": 5,
        "noteable_iid": null,
        "resolvable": false
      }
    ]
  }
]
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/snippets/11/discussions"
```

### 获得代码片段的单个讨论

返回特定项目代码片段的单个讨论。

```plaintext
GET /projects/:id/snippets/:snippet_id/discussions/:discussion_id
```

参数：

| 参数             | 类型           | 是否必需   |  描述       |
| --------------- | -------------- | -------- | ----------- |
| `id`            | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `snippet_id`    | integer        | yes      | 代码片段的 ID |
| `discussion_id` | integer        | yes      | 讨论项的 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/snippets/11/discussions/<discussion_id>"
```

### 创建新的代码片段主题

为单个项目议题创建新主题。类似于创建一个备注，不同的是可以在主题中添加新评论（回复）。

```plaintext
POST /projects/:id/snippets/:snippet_id/discussions
```

参数：

| 参数             | 类型           | 是否必需   | 描述                                                             |
| --------------- | -------------- | -------- |----------------------------------------------------------------|
| `id`            | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)   |
| `snippet_id`    | integer        | yes      | 代码片段的 ID                                                       |
| `body`          | string         | yes      | 讨论的内容                                                          |
| `created_at`    | string         | no       | 日期时间字符串，ISO 8601 格式，如： `2016-03-11T03:45:40Z`。需要管理员或项目/组所有者的权限 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>"\
  "https://gitlab.example.com/api/v4/projects/5/snippets/11/discussions?body=comment"
```

### 添加备注到现有代码片段的主题

为现有主题添加备注。

```plaintext
POST /projects/:id/snippets/:snippet_id/discussions/:discussion_id/notes
```

参数：

| 参数             | 类型           | 是否必需   | 描述                                                             |
| --------------- | -------------- | -------- |----------------------------------------------------------------|
| `id`            | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)   |
| `snippet_id`    | integer        | yes      | 代码片段的 ID                                                       |
| `discussion_id` | integer        | yes      | 主题的 ID                                                         |
| `note_id`       | integer        | yes      | 主题备注的 ID                                                       |
| `body`          | string         | yes      | 备注或回复的内容                                                       |
| `created_at`    | string         | no       | 日期时间字符串，ISO 8601 格式，如： `2016-03-11T03:45:40Z`。需要管理员或项目/组所有者的权限 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/snippets/11/discussions/<discussion_id>/notes?body=comment"
```

### 修改现有的代码片段主题备注

修改一个代码片段中已经存在的主题备注。

```plaintext
PUT /projects/:id/snippets/:snippet_id/discussions/:discussion_id/notes/:note_id
```

参数：

| 参数             | 类型           | 是否必需   |  描述       |
| --------------- | -------------- | -------- | ----------- |
| `id`            | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `snippet_id`    | integer        | yes      | 代码片段的 ID |
| `discussion_id` | integer        | yes      | 主题的 ID |
| `note_id`       | integer        | yes      | 主题备注的 ID |
| `body`          | string         | yes      | 备注或回复的内容 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/snippets/11/discussions/<discussion_id>/notes/1108?body=comment"
```

### 删除代码片段的主题备注

删除代码片段中已经存在的主题的备注。

```plaintext
DELETE /projects/:id/snippets/:snippet_id/discussions/:discussion_id/notes/:note_id
```

参数：

| 参数             | 类型           | 是否必需   |  描述      |
| --------------- | -------------- | -------- | ---------- |
| `id`            | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `snippet_id`    | integer        | yes      | 代码片段的 ID |
| `discussion_id` | integer        | yes      | 讨论的 ID |
| `note_id`       | integer        | yes      | 讨论备注的 ID |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/snippets/11/discussions/636"
```

## 史诗 **(ULTIMATE)**

### 群组史诗主题列表

获取单个史诗的所有主题列表。

```plaintext
GET /groups/:id/epics/:epic_id/discussions
```

| 参数             | 类型           | 是否必需   |  描述       |
| ------------------- | ---------------- | ---------- | ------------ |
| `id`                | integer/string   | yes        | 群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `epic_id`           | integer          | yes        | 史诗的 ID |

```json
[
  {
    "id": "6a9c1750b37d513a43987b574953fceb50b03ce7",
    "individual_note": false,
    "notes": [
      {
        "id": 1126,
        "type": "DiscussionNote",
        "body": "discussion text",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-03T21:54:39.668Z",
        "updated_at": "2018-03-03T21:54:39.668Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Epic",
        "project_id": 5,
        "noteable_iid": null,
        "resolvable": false
      },
      {
        "id": 1129,
        "type": "DiscussionNote",
        "body": "reply to the discussion",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-04T13:38:02.127Z",
        "updated_at": "2018-03-04T13:38:02.127Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Epic",
        "project_id": 5,
        "noteable_iid": null,
        "resolvable": false
      }
    ]
  },
  {
    "id": "87805b7c09016a7058e91bdbe7b29d1f284a39e6",
    "individual_note": true,
    "notes": [
      {
        "id": 1128,
        "type": null,
        "body": "a single comment",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-04T09:17:22.520Z",
        "updated_at": "2018-03-04T09:17:22.520Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Epic",
        "project_id": 5,
        "noteable_iid": null,
        "resolvable": false
      }
    ]
  }
]
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>"\
  "https://gitlab.example.com/api/v4/groups/5/epics/11/discussions"
```

### 获得史诗的单个讨论

返回特定群组史诗的单个讨论。

```plaintext
GET /groups/:id/epics/:epic_id/discussions/:discussion_id
```

参数：

| 参数             | 类型           | 是否必需   |  描述       |
| --------------- | -------------- | -------- | ----------- |
| `id`            | integer/string | yes      | 群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `epic_id`       | integer        | yes      | 史诗的 ID |
| `discussion_id` | integer        | yes      | 讨论项的 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/groups/5/epics/11/discussions/<discussion_id>"
```

### 创建新的史诗主题

为单个群组史诗创建新主题。类似于创建一个备注，不同的是可以在主题中添加新评论（回复）。

```plaintext
POST /groups/:id/epics/:epic_id/discussions
```

参数：

| 参数             | 类型           | 是否必需   | 描述                                                            |
| --------------- | -------------- | -------- |---------------------------------------------------------------|
| `id`            | integer/string | yes      | 群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)  |
| `epic_id`       | integer        | yes      | 史诗的 ID                                                        |
| `body`          | string         | yes      | 主题的内容                                                         |
| `created_at`    | string         | no       | 日期时间字符串，ISO 8601 格式，如：`2016-03-11T03:45:40Z`。需要管理员或项目/组所有者的权限 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/groups/5/epics/11/discussions?body=comment"
```

### 添加备注到现有史诗的主题

为现有主题添加备注，也可以[从单个评论创建主题](../user/discussions/index.md#create-a-thread-by-replying-to-a-standard-comment)。

```plaintext
POST /groups/:id/epics/:epic_id/discussions/:discussion_id/notes
```

参数：

| 参数             | 类型           | 是否必需   | 描述                                                             |
| --------------- | -------------- | -------- |----------------------------------------------------------------|
| `id`            | integer/string | yes      | 群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)   |
| `epic_id`       | integer        | yes      | 史诗的 ID                                                         |
| `discussion_id` | integer        | yes      | 主题的 ID                                                         |
| `note_id`       | integer        | yes      | 主题备注的 ID                                                       |
| `body`          | string         | yes      | 备注或回复的内容                                                       |
| `created_at`    | string         | no       | 日期时间字符串，ISO 8601 格式，如： `2016-03-11T03:45:40Z`。需要管理员或项目/组所有者的权限 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/groups/5/epics/11/discussions/<discussion_id>/notes?body=comment"
```

### 修改现有史诗主题中的备注

修改一个史诗中已经存在的主题的备注。

```plaintext
PUT /groups/:id/epics/:epic_id/discussions/:discussion_id/notes/:note_id
```

参数：

| 参数             | 类型           | 是否必需   |  描述       |
| --------------- | -------------- | -------- | ----------- |
| `id`            | integer/string | yes      | 群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `epic_id`       | integer        | yes      | 史诗的 ID |
| `discussion_id` | integer        | yes      | 主题的 ID |
| `note_id`       | integer        | yes      | 主题备注的 ID |
| `body`          | string         | yes      | 备注或回复的内容 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/groups/5/epics/11/discussions/<discussion_id>/notes/1108?body=comment"
```

### 删除一个史诗主题的备注

删除一个史诗中已经存在的主题的备注。

```plaintext
DELETE /groups/:id/epics/:epic_id/discussions/:discussion_id/notes/:note_id
```

参数：

| 参数             | 类型           | 是否必需   |  描述       |
| --------------- | -------------- | -------- | ----------- |
| `id`            | integer/string | yes      | 群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `epic_id`       | integer        | yes      | 史诗的 ID |
| `discussion_id` | integer        | yes      | 主题的 ID |
| `note_id`       | integer        | yes      | 主题备注的 ID |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/groups/5/epics/11/discussions/636"
```

## 合并请求

### 项目合并请求主题列表

获取单个合并请求的所有主题列表。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/discussions
```

| 参数                 | 类型              | 是否必需    |  描述         |
| ------------------- | ---------------- | ---------- | ------------ |
| `id`                | integer/string   | yes        | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer          | yes        | 合并请求的 IID |

```json
[
  {
    "id": "6a9c1750b37d513a43987b574953fceb50b03ce7",
    "individual_note": false,
    "notes": [
      {
        "id": 1126,
        "type": "DiscussionNote",
        "body": "discussion text",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-03T21:54:39.668Z",
        "updated_at": "2018-03-03T21:54:39.668Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Merge request",
        "project_id": 5,
        "noteable_iid": null,
        "resolved": false,
        "resolvable": true,
        "resolved_by": null,
        "resolved_at": null
      },
      {
        "id": 1129,
        "type": "DiscussionNote",
        "body": "reply to the discussion",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-04T13:38:02.127Z",
        "updated_at": "2018-03-04T13:38:02.127Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Merge request",
        "project_id": 5,
        "noteable_iid": null,
        "resolved": false,
        "resolvable": true,
        "resolved_by": null
      }
    ]
  },
  {
    "id": "87805b7c09016a7058e91bdbe7b29d1f284a39e6",
    "individual_note": true,
    "notes": [
      {
        "id": 1128,
        "type": null,
        "body": "a single comment",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-04T09:17:22.520Z",
        "updated_at": "2018-03-04T09:17:22.520Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Merge request",
        "project_id": 5,
        "noteable_iid": null,
        "resolved": false,
        "resolvable": true,
        "resolved_by": null
      }
    ]
  }
]
```

差异评论也包含位置信息：

```json
[
  {
    "id": "87805b7c09016a7058e91bdbe7b29d1f284a39e6",
    "individual_note": false,
    "notes": [
      {
        "id": 1128,
        "type": "DiffNote",
        "body": "diff comment",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-04T09:17:22.520Z",
        "updated_at": "2018-03-04T09:17:22.520Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Merge request",
        "project_id": 5,
        "noteable_iid": null,
        "commit_id": "4803c71e6b1833ca72b8b26ef2ecd5adc8a38031",
        "position": {
          "base_sha": "b5d6e7b1613fca24d250fa8e5bc7bcc3dd6002ef",
          "start_sha": "7c9c2ead8a320fb7ba0b4e234bd9529a2614e306",
          "head_sha": "4803c71e6b1833ca72b8b26ef2ecd5adc8a38031",
          "old_path": "package.json",
          "new_path": "package.json",
          "position_type": "text",
          "old_line": 27,
          "new_line": 27,
          "line_range": {
            "start": {
              "line_code": "588440f66559714280628a4f9799f0c4eb880a4a_10_10",
              "type": "new"
            },
            "end": {
              "line_code": "588440f66559714280628a4f9799f0c4eb880a4a_11_11",
              "type": "old"
            }
          }
        },
        "resolved": false,
        "resolvable": true,
        "resolved_by": null
      }
    ]
  }
]
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/discussions"
```

### 获得合并请求的单个讨论

返回特定项目合并请求的单个讨论。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/discussions/:discussion_id
```

参数：

| 参数                 | 类型             | 是否必需   |  描述       |
| ------------------- |----------------| -------- | ----------- |
| `id`                | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | yes      | 合并请求的 IID |
| `discussion_id`     | string         | yes      | 讨论项的 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/discussions/<discussion_id>"
```

<a id="create-new-merge-request-thread"></a>

### 创建新的合并请求主题

> `commit id` 引入于 13.7 版本。

为单个项目合并请求创建新主题。与创建备注类似，不同的是其他评论（回复）可以稍后添加。对于其他方法，请参阅提交 API 中的[向提交发表评论](commits.md#post-comment-to-commit)和备注 API 中的[创建新合并请求备注](notes.md#create-new-merge-request-note)。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/discussions
```

所有评论的参数：

| 参数                                      | 类型           | 是否必需   | 描述                                                               |
| ---------------------------------------- | -------------- | -------- |------------------------------------------------------------------|
| `id`                                     | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)     |
| `merge_request_iid`                      | integer        | yes      | 合并请求的 IID                                                        |
| `body`                                   | string         | yes      | 主题的内容                                                            |
| `commit_id`                              | string         | no       | 主题所在的提交的 SHA 值                                                   |
| `created_at`                             | string         | no       | 日期时间字符串， ISO 8601 格式， 如： `2016-03-11T03:45:40Z`。需要管理员或项目/组所有者的权限 |
| `position`                               | hash           | no       | 创建差异备注的位置                                                        |
| `position[base_sha]`                     | string         | yes      | 原分支提交的 SHA 值                                                     |
| `position[start_sha]`                    | string         | yes      | 目标分支相关的提交的 SHA 值                                                 |
| `position[head_sha]`                     | string         | yes      | 此合并请求的头部 SHA 值                                                   |
| `position[position_type]`                | string         | yes      | 位置引用的类型，允许的值为： `text` 或 `image`                                  |
| `position[new_path]`                     | string         | yes （如果位置引用类型是 `text`） | 更改后的文件路径                                                         |
| `position[new_line]`                     | integer        | no       | 对于 `text` 类型的差异备注：变更后行号                                      |
| `position[old_path]`                     | string         | yes （如果位置引用类型是 `text`） | 更改前的文件路径                                                         |
| `position[old_line]`                     | integer        | no       | 对于 `text` 类型的差异备注：变更前行号                                          |
| `position[line_range]`                   | hash           | no       | 多行差异备注的行范围                                                       |
| `position[width]`                        | integer        | no       | 对于 `image` 类型的差异备注：图像宽度                                          |
| `position[height]`                       | integer        | no       | 对于 `image` 类型的差异备注：图像高度                                          |
| `position[x]`                            | float          | no       | 对于 `image` 类型的差异备注：横坐标                                           |
| `position[y]`                            | float          | no       | 对于 `image` 类型的差异备注：纵坐标                                           |

#### 在概览页面上创建一个新主题

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/discussions?body=comment"
```

<a id="create-a-new-thread-in-the-merge-request-diff"></a>

#### 在合并请求的差异中创建主题

- `position[old_path]` 和 `position[new_path]` 都是必须的，必须引用更改之前和之后的文件路径。
- 要在添加的行上创建主题（在合并请求差异中以绿色突出显示），使用 `position[new_line]` 并且不要包含 `position[old_line]`。
- 要在删除的行上创建线程（在合并请求差异中以红色突出显示），使用 `position[old_line]` 并且不要包含 `position[new_line]`。
- 要在未修改的行上创建主题，需要包含 `position[new_line]` 和 `position[old_line]`。如果早期的修改改变了此文件的行号，这些位置信息可能会不一样<!--For the discussion about a fix, see [issue 32516](https://gitlab.com/gitlab-org/gitlab/-/issues/325161).>。
<!--- If you specify incorrect `base`, `head`, `start`, or `SHA` parameters, you might run
  into the bug described in [issue #296829)](https://gitlab.com/gitlab-org/gitlab/-/issues/296829).-->

创建一个新主题：

1. [获取最新的合并请求版本](merge_requests.md#get-merge-request-diff-versions)：

   ```shell
   curl --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/versions"
   ```

1. 请注意最新版本的详细信息，它首先列出在响应数组中。

   ```json
   [
     {
       "id": 164560414,
       "head_commit_sha": "f9ce7e16e56c162edbc9e480108041cf6b0291fe",
       "base_commit_sha": "5e6dffa282c5129aa67cd227a0429be21bfdaf80",
       "start_commit_sha": "5e6dffa282c5129aa67cd227a0429be21bfdaf80",
       "created_at": "2021-03-30T09:18:27.351Z",
       "merge_request_id": 93958054,
       "state": "collected",
       "real_size": "2"
     },
     "previous versions are here"
   ]
   ```

1. 创建一个新的差异主题，下面的例子在添加的行上创建一个新主题：

   ```shell
   curl --request POST --header "PRIVATE-TOKEN: <your_access_token>"\
     --form 'position[position_type]=text'\
     --form 'position[base_sha]=<use base_commit_sha from the versions response>'\
     --form 'position[head_sha]=<use head_commit_sha from the versions response>'\
     --form 'position[start_sha]=<use start_commit_sha from the versions response>'\
     --form 'position[new_path]=file.js'\
     --form 'position[old_path]=file.js'\
     --form 'position[new_line]=18'\
     --form 'body=test comment body'\
     "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/discussions"
   ```

#### 多行评论的参数

仅用于多行评论的参数：

| 参数                                      | 类型           | 是否必需   | 描述                          |
| ---------------------------------------- | -------------- | -------- |-----------------------------|
| `position[line_range][start]`            | hash           | no       | 多行备注的开始行号                   |
| `position[line_range][start][line_code]` | string         | yes      | 起始行的[行代码](#line-code)       |
| `position[line_range][start][type]`      | string         | yes      | 这个提交添加的行使用 `new`，否则使用 `old` |
| `position[line_range][end]`              | hash           | no       | 多行备注的结束行号                   |
| `position[line_range][end][line_code]`   | string         | yes      | 结束行的[行代码](#line-code)       |
| `position[line_range][end][type]`        | string         | yes      | 这个提交添加的行使用 `new`，否则使用 `old` |

<a id="line-code"></a>

#### 行代码

行代码的形式是 `<SHA>_<old>_<new>`，像这样：`adc83b19e793491b1c6ea0fd8b46cd9f32e292fc_5_5`。

- `<SHA>` 是文件名的 SHA1 哈希值。
- `<old>` 是更改前的行号。
- `<new>` 是更改后的行号。

例如，如果一个提交（`<COMMIT_ID>`）删除了自述文件中的第 463 行，您可以通过引用 *old* 文件中的第 463 行来发起删除行评论：

```shell
curl --request POST --header "PRIVATE-TOKEN: [ACCESS_TOKEN]" \
  --form "note=Very clever to remove this unnecessary line!" \
  --form "path=README" --form "line=463" --form "line_type=old" \
  "https://gitlab.com/api/v4/projects/47/repository/commits/<COMMIT_ID>/comments"
```

如果一个提交（`<COMMIT_ID>`）添加了第 157 行到 `hello.rb`，您可以通过引用 *new* 文件中的第 157 行来发起添加行评论：

```shell
curl --request POST --header "PRIVATE-TOKEN: [ACCESS_TOKEN]" \
  --form "note=This is brilliant!" --form "path=hello.rb" \
  --form "line=157" --form "line_type=new" \
  "https://gitlab.com/api/v4/projects/47/repository/commits/<COMMIT_ID>/comments"
```

### 解决合并请求中的主题

解决或取消解决合并请求中的讨论主题。

先决条件：

- 您必须至少拥有开发者的权限，或者是被评审的变更的作者。

```plaintext
PUT /projects/:id/merge_requests/:merge_request_iid/discussions/:discussion_id
```

参数：

| 参数                 | 类型             | 是否必需   |  描述       |
| ------------------- |----------------| -------- | ----------- |
| `id`                | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | yes      | 合并请求的 IID |
| `discussion_id`     | string         | yes      | 主题的 ID |
| `resolved`          | boolean        | yes      | 解决或取消解决此讨论 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/discussions/<discussion_id>?resolved=true"
```

### 添加备注到现有合并请求的主题

为现有主题添加备注，也可以[从单个评论创建主题](../user/discussions/index.md#create-a-thread-by-replying-to-a-standard-comment)。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/discussions/:discussion_id/notes
```

参数：

| 参数                 | 类型             | 是否必需   |  描述       |
| ------------------- |----------------| -------- | ----------- |
| `id`                | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | yes      | 合并请求的 IID |
| `discussion_id`     | string         | yes      | 主题的 ID |
| `note_id`           | integer        | yes      | 主题备注的 ID |
| `body`              | string         | yes      | 备注或回复的内容 |
| `created_at`        | string         | no       | 日期时间字符串，ISO 8601 格式，如： `2016-03-11T03:45:40Z` （需要管理员或项目/组所有者的权限）|

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/discussions/<discussion_id>/notes?body=comment"
```

### 修改现有合并请求主题中的备注

修改一个合并请求中已经存在的主题的备注。

```plaintext
PUT /projects/:id/merge_requests/:merge_request_iid/discussions/:discussion_id/notes/:note_id
```

参数：

| 参数                 | 类型             | 是否必需   | 描述                                                        |
| ------------------- |----------------| -------- |-----------------------------------------------------------|
| `id`                | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | yes      | 合并请求的 IID                                                 |
| `discussion_id`     | string         | yes      | 主题的 ID                                                    |
| `note_id`           | integer        | yes      | 主题备注的 ID                                                  |
| `body`              | string         | no       | 备注或回复的内容，`body` 或 `resolved` 必须设置其中一个                     |
| `resolved`          | boolean        | no       | 解决或取消解决此备注，`body` 或 `resolved` 必须设置其中一个                   |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/discussions/<discussion_id>/notes/1108?body=comment"
```

解决一个评论：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/discussions/<discussion_id>/notes/1108?resolved=true"
```

### 删除一个合并请求主题的备注

删除一个合并请求中已经存在的主题的备注。

```plaintext
DELETE /projects/:id/merge_requests/:merge_request_iid/discussions/:discussion_id/notes/:note_id
```

参数：

| 参数                 | 类型             | 是否必需   |  描述       |
| ------------------- |----------------| -------- | ----------- |
| `id`                | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | yes      | 合并请求的 IID |
| `discussion_id`     | string         | yes      | 主题的 ID |
| `note_id`           | integer        | yes      | 主题备注的 ID |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/discussions/636"
```

## 提交

### 项目提交主题列表

获取单个提交的所有主题列表。

```plaintext
GET /projects/:id/repository/commits/:commit_id/discussions
```

| 参数                 | 类型             | 是否必需    |  描述        |
| ------------------- |----------------| ---------- | ------------ |
| `id`                | integer/string | yes        | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `commit_id`         | string         | yes        | 提交的 SHA |

```json
[
  {
    "id": "6a9c1750b37d513a43987b574953fceb50b03ce7",
    "individual_note": false,
    "notes": [
      {
        "id": 1126,
        "type": "DiscussionNote",
        "body": "discussion text",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-03T21:54:39.668Z",
        "updated_at": "2018-03-03T21:54:39.668Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Commit",
        "project_id": 5,
        "noteable_iid": null,
        "resolvable": false
      },
      {
        "id": 1129,
        "type": "DiscussionNote",
        "body": "reply to the discussion",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-04T13:38:02.127Z",
        "updated_at": "2018-03-04T13:38:02.127Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Commit",
        "project_id": 5,
        "noteable_iid": null,
        "resolvable": false
      }
    ]
  },
  {
    "id": "87805b7c09016a7058e91bdbe7b29d1f284a39e6",
    "individual_note": true,
    "notes": [
      {
        "id": 1128,
        "type": null,
        "body": "a single comment",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-04T09:17:22.520Z",
        "updated_at": "2018-03-04T09:17:22.520Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Commit",
        "project_id": 5,
        "noteable_iid": null,
        "resolvable": false
      }
    ]
  }
]
```

差异评论也包含位置信息：

```json
[
  {
    "id": "87805b7c09016a7058e91bdbe7b29d1f284a39e6",
    "individual_note": false,
    "notes": [
      {
        "id": 1128,
        "type": "DiffNote",
        "body": "diff comment",
        "attachment": null,
        "author": {
          "id": 1,
          "name": "root",
          "username": "root",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/00afb8fb6ab07c3ee3e9c1f38777e2f4?s=80&d=identicon",
          "web_url": "http://localhost:3000/root"
        },
        "created_at": "2018-03-04T09:17:22.520Z",
        "updated_at": "2018-03-04T09:17:22.520Z",
        "system": false,
        "noteable_id": 3,
        "noteable_type": "Commit",
        "project_id": 5,
        "noteable_iid": null,
        "position": {
          "base_sha": "b5d6e7b1613fca24d250fa8e5bc7bcc3dd6002ef",
          "start_sha": "7c9c2ead8a320fb7ba0b4e234bd9529a2614e306",
          "head_sha": "4803c71e6b1833ca72b8b26ef2ecd5adc8a38031",
          "old_path": "package.json",
          "new_path": "package.json",
          "position_type": "text",
          "old_line": 27,
          "new_line": 27
        },
        "resolvable": false
      }
    ]
  }
]
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/repository/commits/<commit_id>/discussions"
```

### 获得提交的单个讨论

返回特定项目提交的单个讨论。

```plaintext
GET /projects/:id/repository/commits/:commit_id/discussions/:discussion_id
```

参数：

| 参数                 | 类型             | 是否必需   |  描述       |
| ------------------- |----------------| -------- | ----------- |
| `id`                | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `commit_id`         | string         | yes      | 提交的 SHA |
| `discussion_id`     | string         | yes      | 讨论项的 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/repository/commits/<commit_id>/discussions/<discussion_id>"
```

### 创建新的提交主题

为单个项目提交创建新主题。类似于创建一个备注，不同的是可以在主题中添加新评论（回复）。

```plaintext
POST /projects/:id/repository/commits/:commit_id/discussions
```

参数：

| 参数                       | 类型           | 是否必需   | 描述                                                               |
| ------------------------- | -------------- | -------- |------------------------------------------------------------------|
| `id`                      | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)     |
| `commit_id`               | string         | yes      | 提交的 SHA 值                                                        |
| `body`                    | string         | yes      | 主题的内容                                                            |
| `created_at`              | string         | no       | 日期时间字符串， ISO 8601 格式， 如： `2016-03-11T03:45:40Z`。需要管理员或项目/组所有者的权限 |
| `position`                | hash           | no       | 创建差异备注的位置                                                        |
| `position[base_sha]`      | string         | yes      | 父提交的 SHA 值                                                       |
| `position[start_sha]`     | string         | yes      | 父提交的 SHA 值                                                       |
| `position[head_sha]`      | string         | yes      | 此提交的 SHA 值 （和 `commit_id` 一致）                                    |
| `position[position_type]` | string         | yes      | 位置引用的类型，允许的值为： `text` 或 `image`                                  |
| `position[new_path]`      | string         | no       | 更改后的文件路径                                                         |
| `position[new_line]`      | integer        | no       | 变更后行号                                                            |
| `position[old_path]`      | string         | no       | 变更前的文件路径                                                         |
| `position[old_line]`      | integer        | no       | 变更前的行号                                                           |
| `position[width]`         | integer        | no       | 对于 `image` 类型的差异备注：图像宽度                                       |
| `position[height]`        | integer        | no       | 对于 `image` 类型的差异备注：图像高度                                        |
| `position[x]`             | integer        | no       | 对于 `image` 类型的差异备注：横坐标                                         |
| `position[y]`             | integer        | no       | 对于 `image` 类型的差异备注：纵坐标                                         |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/repository/commits/<commit_id>/discussions?body=comment"
```

创建 API 请求的规则与[在合并请求中差异中创建主题](#create-a-new-thread-in-the-merge-request-diff)的规则相同。以下为例外：

- `base_sha`
- `head_sha`
- `start_sha`

### 添加备注到现有提交的主题

为现有主题添加备注。

```plaintext
POST /projects/:id/repository/commits/:commit_id/discussions/:discussion_id/notes
```

参数：

| 参数                 | 类型             | 是否必需   | 描述                                                             |
| ------------------- |----------------| -------- |----------------------------------------------------------------|
| `id`                | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)   |
| `commit_id`         | string         | yes      | 提交的 SHA                                                        |
| `discussion_id`     | string         | yes      | 主题的 ID                                                         |
| `note_id`           | integer        | yes      | 主题备注的 ID                                                       |
| `body`              | string         | yes      | 备注或回复的内容                                                       |
| `created_at`        | string         | no       | 日期时间字符串，ISO 8601 格式，如： `2016-03-11T03:45:40Z`。需要管理员或项目/组所有者的权限 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>"\
  "https://gitlab.example.com/api/v4/projects/5/repository/commits/<commit_id>/discussions/<discussion_id>/notes?body=comment
```

### 修改现有提交主题中的备注

修改一个提交中已经存在的主题的备注。

```plaintext
PUT /projects/:id/repository/commits/:commit_id/discussions/:discussion_id/notes/:note_id
```

参数：

| 参数                 | 类型             | 是否必需   |  描述      |
| ------------------- |----------------| -------- | ---------- |
| `id`                | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `commit_id`         | string         | yes      | 提交的 SHA |
| `discussion_id`     | string         | yes      | 主题的 ID |
| `note_id`           | integer        | yes      | 主题备注的 ID |
| `body`              | string         | no       | 备注内容 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/repository/commits/<commit_id>/discussions/<discussion_id>/notes/1108?body=comment"
```

解决一个备注：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/repository/commits/<commit_id>/discussions/<discussion_id>/notes/1108?resolved=true"
```

### 删除一个提交主题的备注

删除一个提交中已经存在的主题的备注。

```plaintext
DELETE /projects/:id/repository/commits/:commit_id/discussions/:discussion_id/notes/:note_id
```

参数：

| 参数                 | 类型             | 是否必需   |  描述       |
| ------------------- |----------------| -------- | ----------- |
| `id`                | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `commit_id`         | string         | yes      | 提交的 SHA |
| `discussion_id`     | string         | yes      | 主题的 ID |
| `note_id`           | integer        | yes      | 主题备注的 ID |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/repository/commits/<commit_id>/discussions/<discussion_id>/notes/636"
```
