---
stage: Govern
group: Threat Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 漏洞 API **(ULTIMATE)**

> 引入于极狐GitLab 12.6。

NOTE:
以前的漏洞 API 已重命名为漏洞发现 API 并且它的文档移动到了[其他位置](vulnerability_findings.md)。
<!--This document now describes the new Vulnerabilities API that provides access to
[Vulnerabilities](https://gitlab.com/groups/gitlab-org/-/epics/634).-->


WARNING:
此 API 正被弃用，现在处于不稳定状态。
响应负载可能会在极狐GitLab 发布时发生变化或损坏。
请使用 GraphQL API<!--[GraphQL API](graphql/reference/index.md#queryvulnerabilities) --> 代替。
您也可以参阅 [GraphQL 示例](#replace-vulnerability-rest-api-with-graphql)。

对漏洞的每个 API 调用都必须经过[身份验证](rest/index.md#authentication)。

如果经过身份验证的用户没有权限[查看漏洞](../user/permissions.md#project-members-permissions)，此请求返回 `403 Forbidden` 状态代码。

## 单个漏洞

获取单个漏洞。

```plaintext
GET /vulnerabilities/:id
```

| 参数   | 类型                | 是否必需 | 描述                                  |
|------|-------------------|------|-------------------------------------|
| `id` | integer or string | yes  | 获取的漏洞的 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/vulnerabilities/1"
```

响应示例：

```json
{
  "id": 1,
  "title": "Predictable pseudorandom number generator",
  "description": null,
  "state": "opened",
  "severity": "medium",
  "confidence": "medium",
  "report_type": "sast",
  "project": {
    "id": 32,
    "name": "security-reports",
    "full_path": "/gitlab-examples/security/security-reports",
    "full_name": "gitlab-examples / security / security-reports"
  },
  "author_id": 1,
  "updated_by_id": null,
  "last_edited_by_id": null,
  "closed_by_id": null,
  "start_date": null,
  "due_date": null,
  "created_at": "2019-10-13T15:08:40.219Z",
  "updated_at": "2019-10-13T15:09:40.382Z",
  "last_edited_at": null,
  "closed_at": null
}
```

## 确认漏洞

确认给定的漏洞。如果漏洞已被确认，则返回状态代码 `304`。

如果经过身份验证的用户没有权限[确认漏洞](../user/permissions.md#project-members-permissions)，此请求会返回 `403` 状态码。

```plaintext
POST /vulnerabilities/:id/confirm
```

| 参数   | 类型                | 是否必需 | 描述                                          |
|------|-------------------|------|---------------------------------------------|
| `id` | integer or string | yes  | 确认的漏洞的 ID|

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/vulnerabilities/5/confirm"
```

响应示例：

```json
{
  "id": 2,
  "title": "Predictable pseudorandom number generator",
  "description": null,
  "state": "confirmed",
  "severity": "medium",
  "confidence": "medium",
  "report_type": "sast",
  "project": {
    "id": 32,
    "name": "security-reports",
    "full_path": "/gitlab-examples/security/security-reports",
    "full_name": "gitlab-examples / security / security-reports"
  },
  "author_id": 1,
  "updated_by_id": null,
  "last_edited_by_id": null,
  "closed_by_id": null,
  "start_date": null,
  "due_date": null,
  "created_at": "2019-10-13T15:08:40.219Z",
  "updated_at": "2019-10-13T15:09:40.382Z",
  "last_edited_at": null,
  "closed_at": null
}
```

## 解决漏洞

解决给定的漏洞。如果漏洞已解决，则返回 `304` 状态码。

如果经过身份验证的用户没有权限[解决漏洞](../user/permissions.md#project-members-permissions)，此请求会返回 `403` 状态码。

```plaintext
POST /vulnerabilities/:id/resolve
```

| 参数   | 类型                | 是否必需 | 描述                                          |
|------|-------------------|------|---------------------------------------------|
| `id` | integer or string | yes  | 解决的漏洞的 ID  |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/vulnerabilities/5/resolve"
```

响应示例：

```json
{
  "id": 2,
  "title": "Predictable pseudorandom number generator",
  "description": null,
  "state": "resolved",
  "severity": "medium",
  "confidence": "medium",
  "report_type": "sast",
  "project": {
    "id": 32,
    "name": "security-reports",
    "full_path": "/gitlab-examples/security/security-reports",
    "full_name": "gitlab-examples / security / security-reports"
  },
  "author_id": 1,
  "updated_by_id": null,
  "last_edited_by_id": null,
  "closed_by_id": null,
  "start_date": null,
  "due_date": null,
  "created_at": "2019-10-13T15:08:40.219Z",
  "updated_at": "2019-10-13T15:09:40.382Z",
  "last_edited_at": null,
  "closed_at": null
}
```

## 消除漏洞

消除给定的漏洞。如果漏洞已被消除，则返回 `304` 状态码。

如果经过身份验证的用户没有权限[消除漏洞](../user/permissions.md#project-members-permissions)，此请求会返回 `403` 状态码。

```plaintext
POST /vulnerabilities/:id/dismiss
```

| 参数   | 类型                | 是否必需 | 描述                                         |
|------|-------------------|------|--------------------------------------------|
| `id` | integer or string | yes  | 消除的漏洞的 ID  |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/vulnerabilities/5/dismiss"
```

响应示例：

```json
{
  "id": 2,
  "title": "Predictable pseudorandom number generator",
  "description": null,
  "state": "closed",
  "severity": "medium",
  "confidence": "medium",
  "report_type": "sast",
  "project": {
    "id": 32,
    "name": "security-reports",
    "full_path": "/gitlab-examples/security/security-reports",
    "full_name": "gitlab-examples / security / security-reports"
  },
  "author_id": 1,
  "updated_by_id": null,
  "last_edited_by_id": null,
  "closed_by_id": null,
  "start_date": null,
  "due_date": null,
  "created_at": "2019-10-13T15:08:40.219Z",
  "updated_at": "2019-10-13T15:09:40.382Z",
  "last_edited_at": null,
  "closed_at": null
}
```

## 将漏洞还原到检测状态

将给定的漏洞恢复到检测状态。如果漏洞已处于检测状态，则返回 `304` 状态码。

如果经过身份验证的用户没有权限[将漏洞恢复到检测状态](../user/permissions.md#project-members-permissions)，此请求会返回 `403` 状态码。

```plaintext
POST /vulnerabilities/:id/revert
```

| 参数   | 类型                | 是否必需 | 描述                                                                |
|------|-------------------|------|-------------------------------------------------------------------|
| `id` | integer or string | yes  | 还原到检测状态的漏洞的 ID  |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/vulnerabilities/5/dismiss"
```

响应示例：

```json
{
  "id": 2,
  "title": "Predictable pseudorandom number generator",
  "description": null,
  "state": "detected",
  "severity": "medium",
  "confidence": "medium",
  "report_type": "sast",
  "project": {
    "id": 32,
    "name": "security-reports",
    "full_path": "/gitlab-examples/security/security-reports",
    "full_name": "gitlab-examples / security / security-reports"
  },
  "author_id": 1,
  "updated_by_id": null,
  "last_edited_by_id": null,
  "closed_by_id": null,
  "start_date": null,
  "due_date": null,
  "created_at": "2019-10-13T15:08:40.219Z",
  "updated_at": "2019-10-13T15:09:40.382Z",
  "last_edited_at": null,
  "closed_at": null
}
```

<a id="replace-vulnerability-rest-api-with-graphql"></a>

## 使用 GraphQL 替代漏洞 REST API 

为即将弃用的漏洞 REST API 端点做准备，通过以下示例使用 GraphQL API 执行等效操作。

### GraphQL - 单个漏洞

使用 `Query.vulnerability`<!--[`Query.vulnerability`](graphql/reference/#queryvulnerability)-->。

```graphql
{
  vulnerability(id: "gid://gitlab/Vulnerability/20345379") {
    title
    description
    state
    severity
    reportType
    project {
      id
      name
      fullPath
    }
    detectedAt
    confirmedAt
    resolvedAt
    resolvedBy {
      id
      username
    }
  }
}
```

响应示例：

```json
{
  "data": {
    "vulnerability": {
      "title": "Improper Input Validation in railties",
      "description": "A remote code execution vulnerability in development mode Rails beta3 can allow an attacker to guess the automatically generated development mode secret token. This secret token can be used in combination with other Rails internals to escalate to a remote code execution exploit.",
      "state": "RESOLVED",
      "severity": "CRITICAL",
      "reportType": "DEPENDENCY_SCANNING",
      "project": {
        "id": "gid://gitlab/Project/6102100",
        "name": "security-reports",
        "fullPath": "gitlab-examples/security/security-reports"
      },
      "detectedAt": "2021-10-14T03:13:41Z",
      "confirmedAt": "2021-12-14T01:45:56Z",
      "resolvedAt": "2021-12-14T01:45:59Z",
      "resolvedBy": {
        "id": "gid://gitlab/User/480804",
        "username": "thiagocsf"
      }
    }
  }
}
```

### GraphQL - 确认漏洞

使用 `Mutation.vulnerabilityConfirm`。 <!--[`Mutation.vulnerabilityConfirm`](graphql/reference/#mutationvulnerabilityconfirm)-->

```graphql
mutation {
  vulnerabilityConfirm(input: { id: "gid://gitlab/Vulnerability/23577695"}) {
    vulnerability {
      state
    }
    errors
  }
}
```

响应示例：

```json
{
  "data": {
    "vulnerabilityConfirm": {
      "vulnerability": {
        "state": "CONFIRMED"
      },
      "errors": []
    }
  }
}
```

### GraphQL - 解决漏洞

使用 `Mutation.vulnerabilityResolve`。<!--[`Mutation.vulnerabilityResolve`](graphql/reference/#mutationvulnerabilityresolve).-->

```graphql
mutation {
  vulnerabilityResolve(input: { id: "gid://gitlab/Vulnerability/23577695"}) {
    vulnerability {
      state
    }
    errors
  }
}
```

响应示例：

```json
{
  "data": {
    "vulnerabilityConfirm": {
      "vulnerability": {
        "state": "RESOLVED"
      },
      "errors": []
    }
  }
}
```

### GraphQL - 消除漏洞

使用 `Mutation.vulnerabilityDismiss`。<!-- [`Mutation.vulnerabilityDismiss`](graphql/reference/#mutationvulnerabilitydismiss).-->

```graphql
mutation {
  vulnerabilityDismiss(input: { id: "gid://gitlab/Vulnerability/23577695"}) {
    vulnerability {
      state
    }
    errors
  }
}
```

响应示例：

```json
{
  "data": {
    "vulnerabilityConfirm": {
      "vulnerability": {
        "state": "DISMISSED"
      },
      "errors": []
    }
  }
}
```

### GraphQL - 将漏斗还原到检测状态

使用 `Mutation.vulnerabilityRevertToDetected`。<!--[`Mutation.vulnerabilityRevertToDetected`](graphql/reference/#mutationvulnerabilityreverttodetected).-->

```graphql
mutation {
  vulnerabilityRevertToDetected(input: { id: "gid://gitlab/Vulnerability/20345379"}) {
    vulnerability {
      state
    }
    errors
  }
}
```

响应示例：

```json
{
  "data": {
    "vulnerabilityConfirm": {
      "vulnerability": {
        "state": "DETECTED"
      },
      "errors": []
    }
  }
}
```
