---
stage: Verify
group: Pipeline Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference, api
---

# 项目级别 CI/CD 变量 API **(FREE)**

## 列出项目变量

获取项目变量列表。

```plaintext
GET /projects/:id/variables
```

| 参数   | 类型             | 是否必需 | 描述                                                                     |
|------|----------------|------|------------------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的项目命名空间/项目名称](index.md#namespaced-path-encoding) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/variables"
```

```json
[
  {
    "variable_type": "env_var",
    "key": "TEST_VARIABLE_1",
    "value": "TEST_1",
    "protected": false,
    "masked": true,
    "raw": false,
    "environment_scope": "*"
  },
  {
    "variable_type": "env_var",
    "key": "TEST_VARIABLE_2",
    "value": "TEST_2",
    "protected": false,
    "masked": false,
    "raw": false,
    "environment_scope": "*"
  }
]
```

<a id="get-a-single-variable"></a>

## 获取单个变量

获取单个变量的详细信息。如果多个变量的 key 相同，则使用 `filter` 选择正确的 `environment_scope`。

```plaintext
GET /projects/:id/variables/:key
```

| 参数       | 类型             | 是否必需 | 描述                                                                       |
|----------|----------------|------|--------------------------------------------------------------------------|
| `id`     | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的项目命名空间/项目名称](index.md#namespaced-path-encoding)   |
| `key`    | string         | yes  | 变量的 `key`                                                                |
| `filter` | hash           | no   | 可用过滤项：`[environment_scope]`。详情请参见 [`filter` 参数详情](#the-filter-parameter) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/variables/TEST_VARIABLE_1"
```

```json
{
  "key": "TEST_VARIABLE_1",
  "variable_type": "env_var",
  "value": "TEST_1",
  "protected": false,
  "masked": true,
  "raw": false,
  "environment_scope": "*"
}
```

## 创建变量

创建变量。如果已经存在具有相同 `key` 的变量，则新变量必须有不同的 `environment_scope`。否则，极狐GitLab 会返回类似于 `VARIABLE_NAME has already been taken` 的消息。

```plaintext
POST /projects/:id/variables
```


| 参数                  | 类型             | 是否必需 | 描述                                                                                                          |
|---------------------|----------------|------|-------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的项目命名空间/项目名称](index.md#namespaced-path-encoding)                                |
| `key`               | string         | yes  | 变量的 `key`。不能超过 255 个字符。仅支持 `A-Z`、`a-z`、`0-9` 和 `_`                                                          |
| `value`             | string         | yes  | 变量的 `value`                                                                                                 |
| `variable_type`     | string         | no   | 变量类型。可用类型为： `env_var`（默认）和 `file`                                                                           |
| `protected`         | boolean        | no   | 变量是否受保护。默认为 `false`                                                                                         |
| `masked`            | boolean        | no   | 变量是否隐藏。默认为 `false`                                                                                          |
| `raw`               | boolean        | no   | 变量是否被视为原始字符串。默认值为 `false`。当为 `true` 时，值中的变量不会[扩展](../ci/variables/index.md#prevent-cicd-variable-expansion) |
| `environment_scope` | string         | no   | 变量的 `environment_scope`。默认为 `*`                                                                             |


```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/variables" --form "key=NEW_VARIABLE" --form "value=new value"
```

```json
{
  "variable_type": "env_var",
  "key": "NEW_VARIABLE",
  "value": "new value",
  "protected": false,
  "masked": false,
  "raw": false,
  "environment_scope": "*"
}
```

<a id="update-a-variable"></a>

## 更新变量

更新项目变量。如果有多个变量具有相同的 key，则使用 `filter` 选择正确的 `environment_scope`。

```plaintext
PUT /projects/:id/variables/:key
```

| 参数                  | 类型             | 是否必需 | 描述                                                                      |
|---------------------|----------------|------|-------------------------------------------------------------------------|
| `id`                | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的项目命名空间/项目名称](index.md#namespaced-path-encoding)  |
| `key`               | string         | yes  | 变量的 `key`                                                               |
| `value`             | string         | yes  | 变量的 `value`                                                             |
| `variable_type`     | string         | no   | 变量类型。可用类型为：`env_var`（默认）和`file`                                         |
| `protected`         | boolean        | no   | 变量是否受保护                                                                 |
| `masked`            | boolean        | no   | 变量是否隐藏                                                                  |
| `raw`               | boolean        | no   | 变量是否被视为原始字符串。默认值为 `false`。当为 `true` 时，值中的变量不会[扩展](../ci/variables/index.md#prevent-cicd-variable-expansion)                     |
| `environment_scope` | string         | no   | 变量的 `environment_scope`                                                 |
| `filter`            | hash           | no   | 可用过滤项：`[environment_scope]`。详情请参见 [`filter` 参数详情](#the-filter-parameter) |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/variables/NEW_VARIABLE" --form "value=updated value"
```

```json
{
  "variable_type": "env_var",
  "key": "NEW_VARIABLE",
  "value": "updated value",
  "protected": true,
  "masked": false,
  "raw": false,
  "environment_scope": "*"
}
```

<a id="update-a-variable"></a>

## 删除变量

删除项目变量。如果有多个变量具有相同的 key，则使用 `filter` 选择正确的 `environment_scope`。

```plaintext
DELETE /projects/:id/variables/:key
```

| 参数       | 类型             | 是否必需 | 描述                                                                       |
|----------|----------------|------|--------------------------------------------------------------------------|
| `id`     | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的项目命名空间/项目名称](index.md#namespaced-path-encoding)   |
| `key`    | string         | yes  | 变量的 `key`                                                                |
| `filter` | hash           | no   | 可用过滤项：`[environment_scope]`。详情请参见 [`filter` 参数详情](#the-filter-parameter) |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/variables/VARIABLE_1"
```

<a id="delete-a-variable"></a>

## `filter` 参数

> - 引入于极狐GitLab 13.2。
> - 功能标志移除于极狐GitLab 13.4。

当多个变量拥有相同的 `key`，[获取](#get-a-single-variable)、[更新](#update-a-variable)或[删除](#delete-a-variable)请求或许会返回：

```plaintext
There are multiple variables with provided parameters. Please use 'filter[environment_scope]'.
```

使用 `filter[environment_scope]` 选择匹配 `environment_scope` 参数的变量。

例如：

- 获取：

  ```shell
  curl --globoff --header "PRIVATE-TOKEN: <your_access_token>" \
       "https://gitlab.example.com/api/v4/projects/1/variables/SCOPED_VARIABLE_1?filter[environment_scope]=production"
  ```

- 更新：

  ```shell
  curl --request PUT --globoff --header "PRIVATE-TOKEN: <your_access_token>" \
       "https://gitlab.example.com/api/v4/projects/1/variables/SCOPED_VARIABLE_1?value=scoped-variable-updated-value&environment_scope=production&filter[environment_scope]=production"
  ```

- 删除：

  ```shell
  curl --request DELETE --globoff --header "PRIVATE-TOKEN: <your_access_token>" \
       "https://gitlab.example.com/api/v4/projects/1/variables/SCOPED_VARIABLE_1?filter[environment_scope]=production"
  ```
```
