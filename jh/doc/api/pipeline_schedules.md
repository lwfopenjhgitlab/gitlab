---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 计划流水线 API **(FREE)**

您可以在[计划流水线](../ci/pipelines/schedules.md)了解更多。

## 获取所有的计划流水线信息
<!-- Get a list of the pipeline schedules of a project. -->
获取某个项目的所有计划流水线信息。

```plaintext
GET /projects/:id/pipeline_schedules
```

| 参数 | 类型    | 是否必需 | 描述         |
|-----------|---------|----------|---------------------|
| `id`      | 数字/字符串 | 是      | 经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding)  |
| `scope`   | 字符串  | 否       | 计划流水线的范围，`active`、`inactive` 其中之一 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/29/pipeline_schedules"
```

```json
[
    {
        "id": 13,
        "description": "Test schedule pipeline",
        "ref": "main",
        "cron": "* * * * *",
        "cron_timezone": "Asia/Tokyo",
        "next_run_at": "2017-05-19T13:41:00.000Z",
        "active": true,
        "created_at": "2017-05-19T13:31:08.849Z",
        "updated_at": "2017-05-19T13:40:17.727Z",
        "owner": {
            "name": "Administrator",
            "username": "root",
            "id": 1,
            "state": "active",
            "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
            "web_url": "https://gitlab.example.com/root"
        }
    }
]
```

## 获取一个计划流水线信息

<!-- Get the pipeline schedule of a project. -->
获取某个项目的一个计划流水线信息。

```plaintext
GET /projects/:id/pipeline_schedules/:pipeline_schedule_id
```

| 参数 | 类型    | 是否必需 | 描述         |
|--------------|---------|----------|--------------------------|
| `id`         | integer/string | 是      | 经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding)      |
| `pipeline_schedule_id` | integer | 是      | 计划流水线调度器 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/29/pipeline_schedules/13"
```

```json
{
    "id": 13,
    "description": "Test schedule pipeline",
    "ref": "main",
    "cron": "* * * * *",
    "cron_timezone": "Asia/Tokyo",
    "next_run_at": "2017-05-19T13:41:00.000Z",
    "active": true,
    "created_at": "2017-05-19T13:31:08.849Z",
    "updated_at": "2017-05-19T13:40:17.727Z",
    "last_pipeline": {
        "id": 332,
        "sha": "0e788619d0b5ec17388dffb973ecd505946156db",
        "ref": "main",
        "status": "pending"
    },
    "owner": {
        "name": "Administrator",
        "username": "root",
        "id": 1,
        "state": "active",
        "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
        "web_url": "https://gitlab.example.com/root"
    },
    "variables": [
        {
            "key": "TEST_VARIABLE_1",
            "variable_type": "env_var",
            "value": "TEST_1"
        }
    ]
}
```

## 获取计划流水线触发的所有流水线

> 引入于极狐GitLab 15.3。

获取项目中计划流水线触发的所有流水线。

```plaintext
GET /projects/:id/pipeline_schedules/:pipeline_schedule_id/pipelines
```

支持的参数：

| 参数                     | 类型             | 是否必需 | 描述                                                          |
|------------------------|----------------|------|-------------------------------------------------------------|
| `id`                   | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目路径](index.md#namespaced-path-encoding) |
| `pipeline_schedule_id` | integer        | yes  | 计划流水线 ID                                                    |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/29/pipeline_schedules/13/pipelines"
```

响应示例：

```json
[
  {
    "id": 47,
    "iid": 12,
    "project_id": 29,
    "status": "pending",
    "source": "scheduled",
    "ref": "new-pipeline",
    "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
    "web_url": "https://example.com/foo/bar/pipelines/47",
    "created_at": "2016-08-11T11:28:34.085Z",
    "updated_at": "2016-08-11T11:32:35.169Z"
  },
  {
    "id": 48,
    "iid": 13,
    "project_id": 29,
    "status": "pending",
    "source": "scheduled",
    "ref": "new-pipeline",
    "sha": "eb94b618fb5865b26e80fdd8ae531b7a63ad851a",
    "web_url": "https://example.com/foo/bar/pipelines/48",
    "created_at": "2016-08-12T10:06:04.561Z",
    "updated_at": "2016-08-12T10:09:56.223Z"
  }
]
```

## 创建新的计划流水线

为某个项目创建新的计划流水线。

```plaintext
POST /projects/:id/pipeline_schedules
```

| 参数 | 类型    | 是否必需 | 描述         |
|-----------------|----------------|----------|-------------------------------------------------------------------------------------------------------------------------|
| `id`            | integer/string | 是      |  经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding)       |
| `description`   | string         | 是      | 计划流水线的描述                                                                               |
| `ref`           | string         | 是      | 触发的标签或者分支名                                                                              |
| `cron`          | string         | 是      | cron，例如: `0 1 * * *`                                 |
| `cron_timezone` | string         | 否       | `ActiveSupport::TimeZone` 支持 cron 时区的设置, 例如: `Pacific Time (US & Canada)` (默认为: `UTC`)        |
| `active`        | boolean        | 否       |计划流水线的激活状态。如果设置成 false，那么计划流水线的该初始值会是 false (默认为：`true`) |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --form description="Build packages" --form ref="main" --form cron="0 1 * * 5" --form cron_timezone="UTC" \
     --form active="true" "https://gitlab.example.com/api/v4/projects/29/pipeline_schedules"
```

```json
{
    "id": 14,
    "description": "Build packages",
    "ref": "main",
    "cron": "0 1 * * 5",
    "cron_timezone": "UTC",
    "next_run_at": "2017-05-26T01:00:00.000Z",
    "active": true,
    "created_at": "2017-05-19T13:43:08.169Z",
    "updated_at": "2017-05-19T13:43:08.169Z",
    "last_pipeline": null,
    "owner": {
        "name": "Administrator",
        "username": "root",
        "id": 1,
        "state": "active",
        "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
        "web_url": "https://gitlab.example.com/root"
    }
}
```

## 编辑计划流水线

更新某个项目的计划流水线。当更新完成，流水线会自动重新调整。

```plaintext
PUT /projects/:id/pipeline_schedules/:pipeline_schedule_id
```

| 参数 | 类型    | 是否必需 | 描述         |
|------------------------|----------------|----------|------------------------------------------------------------------------------------------------------------------|
| `id`                   | integer/string | 是      | 经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding) |
| `pipeline_schedule_id` | integer        | 是      | 流水线的计划 ID                                                                                       |
| `description`          | string         | 否       | 计划流水线的描述                                                                       |
| `ref`                  | string         | 否       | 触发的标签或者分支名                                                                      |
| `cron`                 | string         | 否       | cron，例如: `0 1 * * *`                              |
| `cron_timezone`        | string         | 否       |  `ActiveSupport::TimeZone` 支持 cron 时区的设置，例如: `Pacific Time (US & Canada)`，又或者 `TZInfo::Timezone` 例如：`America/Los_Angeles` |
| `active`               | boolean        | no       | 计划流水线的激活状态。如果设置成 false，那么计划流水线的该初始值会是 false (默认为：`true`)            |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     --form cron="0 2 * * *" "https://gitlab.example.com/api/v4/projects/29/pipeline_schedules/13"
```

```json
{
    "id": 13,
    "description": "Test schedule pipeline",
    "ref": "main",
    "cron": "0 2 * * *",
    "cron_timezone": "Asia/Tokyo",
    "next_run_at": "2017-05-19T17:00:00.000Z",
    "active": true,
    "created_at": "2017-05-19T13:31:08.849Z",
    "updated_at": "2017-05-19T13:44:16.135Z",
    "last_pipeline": {
        "id": 332,
        "sha": "0e788619d0b5ec17388dffb973ecd505946156db",
        "ref": "main",
        "status": "pending"
    },
    "owner": {
        "name": "Administrator",
        "username": "root",
        "id": 1,
        "state": "active",
        "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
        "web_url": "https://gitlab.example.com/root"
    }
}
```

## 获取计划流水线的所有权

更新某项目的计划流水线的所有权。

```plaintext
POST /projects/:id/pipeline_schedules/:pipeline_schedule_id/take_ownership
```

| 参数 | 类型    | 是否必需 | 描述         |
|---------------|---------|----------|--------------------------|
| `id`          | integer/string | 是      | 经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding)      |
| `pipeline_schedule_id`  | integer | 是      | 流水线的调度器 ID          |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/29/pipeline_schedules/13/take_ownership"
```

```json
{
    "id": 13,
    "description": "Test schedule pipeline",
    "ref": "main",
    "cron": "0 2 * * *",
    "cron_timezone": "Asia/Tokyo",
    "next_run_at": "2017-05-19T17:00:00.000Z",
    "active": true,
    "created_at": "2017-05-19T13:31:08.849Z",
    "updated_at": "2017-05-19T13:46:37.468Z",
    "last_pipeline": {
        "id": 332,
        "sha": "0e788619d0b5ec17388dffb973ecd505946156db",
        "ref": "main",
        "status": "pending"
    },
    "owner": {
        "name": "shinya",
        "username": "maeda",
        "id": 50,
        "state": "active",
        "avatar_url": "http://www.gravatar.com/avatar/8ca0a796a679c292e3a11da50f99e801?s=80&d=identicon",
        "web_url": "https://gitlab.example.com/maeda"
    }
}
```

## 删除计划流水线

删除某个项目的计划流水线。

```plaintext
DELETE /projects/:id/pipeline_schedules/:pipeline_schedule_id
```

| 参数 | 类型    | 是否必需 | 描述         |
|----------------|---------|----------|--------------------------|
| `id`           | integer/string | yes      |  经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding)      |
| `pipeline_schedule_id`   | integer | yes      |  流水线的调度器 ID           |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/29/pipeline_schedules/13"
```

```json
{
    "id": 13,
    "description": "Test schedule pipeline",
    "ref": "main",
    "cron": "0 2 * * *",
    "cron_timezone": "Asia/Tokyo",
    "next_run_at": "2017-05-19T17:00:00.000Z",
    "active": true,
    "created_at": "2017-05-19T13:31:08.849Z",
    "updated_at": "2017-05-19T13:46:37.468Z",
    "last_pipeline": {
        "id": 332,
        "sha": "0e788619d0b5ec17388dffb973ecd505946156db",
        "ref": "main",
        "status": "pending"
    },
    "owner": {
        "name": "shinya",
        "username": "maeda",
        "id": 50,
        "state": "active",
        "avatar_url": "http://www.gravatar.com/avatar/8ca0a796a679c292e3a11da50f99e801?s=80&d=identicon",
        "web_url": "https://gitlab.example.com/maeda"
    }
}
```

## 立刻运行计划流水线

立即触发一条新的计划流水线，不会影响该流水线的下次计划运行。

```plaintext
POST /projects/:id/pipeline_schedules/:pipeline_schedule_id/play
```

| 参数 | 类型    | 是否必需 | 描述         |
| ----------------       | ---------      | ---------- | --------------------------                                                                                      |
| `id`                   | integer/string | yes        | 经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding) |
| `pipeline_schedule_id` | integer        | yes        |  流水线的调度器 ID                                                                                         |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/42/pipeline_schedules/1/play"
```

响应示例：

```json
{
  "message": "201 Created"
}
```

## 计划流水线的参数

## 创建某条新的计划流水线参数

为某条计划流水线创建新的参数。

```plaintext
POST /projects/:id/pipeline_schedules/:pipeline_schedule_id/variables
```

| 参数 | 类型    | 是否必需 | 描述         |
|------------------------|----------------|----------|--------------------------|
| `id`                   | integer/string | 是      | 经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding)    |
| `pipeline_schedule_id` | integer        | 是      |  流水线的计划 ID |
| `key`                  | string         | 是      | 不长于 255 位字符的参数的`key`；只允许 `A-Z`、`a-z`、`0-9` 以及 `_`  |
| `value`                | string         | 是      | 该参数的 `value` |
| `variable_type`        | string         | 否       | 参数的类型，包括：`env_var` (默认值) 和 `file` |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --form "key=NEW_VARIABLE" \
     --form "value=new value" "https://gitlab.example.com/api/v4/projects/29/pipeline_schedules/13/variables"
```

```json
{
    "key": "NEW_VARIABLE",
    "variable_type": "env_var",
    "value": "new value"
}
```

## 编辑某条计划流水线的参数

更新流水线计划的变量。

```plaintext
PUT /projects/:id/pipeline_schedules/:pipeline_schedule_id/variables/:key
```

| 参数 | 类型    | 是否必需 | 描述         |
|------------------------|----------------|----------|--------------------------|
| `id`                   | integer/string | 是      | 经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding)      |
| `pipeline_schedule_id` | integer        | 是      | 流水线的调度器 ID |
| `key`                  | string         | 是      | 参数的`key`  |
| `value`                | string         | 是      | 该参数的 `value` |
| `variable_type`        | string         | 否       | 参数的类型，包括: `env_var` (默认值) and `file` |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     --form "value=updated value" \
     "https://gitlab.example.com/api/v4/projects/29/pipeline_schedules/13/variables/NEW_VARIABLE"
```

```json
{
    "key": "NEW_VARIABLE",
    "value": "updated value",
    "variable_type": "env_var"
}
```

## 删除某条计划流水线的参数

删除某条计划流水线的参数。

```plaintext
DELETE /projects/:id/pipeline_schedules/:pipeline_schedule_id/variables/:key
```

| 参数 | 类型    | 是否必需 | 描述         |
|------------------------|----------------|----------|--------------------------|
| `id`                   | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding)      |
| `pipeline_schedule_id` | integer        | yes      | 流水线的调度器 ID |
| `key`                  | string         | yes      | 参数的`key` |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/29/pipeline_schedules/13/variables/NEW_VARIABLE"
```

```json
{
    "key": "NEW_VARIABLE",
    "value": "updated value"
}
```
