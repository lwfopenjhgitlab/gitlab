---
stage: Manage
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 系统钩子 API **(FREE SELF)**

所有 API 都需要进行管理员认证。

您可以从极狐GitLab 用户界面配置系统钩子中的 URL 地址：

1. 在左侧边栏中，展开最上方的箭头(**{chevron-down}**)。
1. 选择 **管理中心**。
1. 选择 **系统钩子** (`/admin/hooks`)。

阅读更多关于[系统钩子](../administration/system_hooks.md)。

## 获取系统钩子列表

获取所有系统钩子的列表。

```plaintext
GET /hooks
```

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/hooks"
```

响应示例：

```json
[
  {
    "id":1,
    "url":"https://gitlab.example.com/hook",
    "created_at":"2016-10-31T12:32:15.192Z",
    "push_events":true,
    "tag_push_events":false,
    "merge_requests_events": true,
    "repository_update_events": true,
    "enable_ssl_verification":true
  }
]
```

## 获取一个系统钩子

> 引入于极狐GitLab 14.9。

通过 ID 获取系统钩子。

```plaintext
GET /hooks/:id
```

| 参数 | 类型 | 是否必需 | 描述      |
| --------- | ---- | -------- | ----------- |
| `id` | integer | yes | 系统钩子的 ID |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/hooks/1"
```

响应示例：

```json
[
  {
    "id": 1,
    "url": "https://gitlab.example.com/hook",
    "created_at": "2016-10-31T12:32:15.192Z",
    "push_events": true,
    "tag_push_events": false,
    "merge_requests_events": true,
    "repository_update_events": true,
    "enable_ssl_verification": true
  }
]
```

## 添加一个新的系统钩子

添加一个新的系统钩子。

```plaintext
POST /hooks
```

| 参数 | 类型 | 是否必需 | 描述      |
| --------- | ---- | -------- | ----------- |
| `url` | string | yes | 系统钩子的地址 |
| `token` | string | no | 用来验证接受数据的令牌，令牌不会响应中返回。 |
| `push_events` | boolean |  no | 该值为真时，推送事件触发系统钩子。|
| `tag_push_events` | boolean | no | 该值为真时，推送新标签事件触发系统钩子。|
| `merge_requests_events` | boolean | no | 该值为真时，合并请求事件触发系统钩子。|
| `repository_update_events` | boolean | no | 该值为真时，仓库更新事件触发系统钩子。|
| `enable_ssl_verification` | boolean | no | 触发系统钩子时进行 SSL 验证。|

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/hooks?url=https://gitlab.example.com/hook"
```

响应示例：

```json
[
  {
    "id":1,
    "url":"https://gitlab.example.com/hook",
    "created_at":"2016-10-31T12:32:15.192Z",
    "push_events":true,
    "tag_push_events":false,
    "merge_requests_events": true,
    "repository_update_events": true,
    "enable_ssl_verification":true
  }
]
```

## 测试系统钩子

使用模拟数据执行系统钩子。

```plaintext
POST /hooks/:id
```

| 参数 | 类型 | 是否必需 | 描述      |
| --------- | ---- | -------- | ----------- |
| `id` | integer | yes | 系统钩子的 ID |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/hooks/1"
```

响应始终是模拟数据：

```json
{
   "project_id" : 1,
   "owner_email" : "example@gitlabhq.com",
   "owner_name" : "Someone",
   "name" : "Ruby",
   "path" : "ruby",
   "event_name" : "project_create"
}
```

## 删除一个系统钩子

删除一个系统钩子。

```plaintext
DELETE /hooks/:id
```

| 参数 | 类型 | 是否必需 | 描述      |
| --------- | ---- | -------- | ----------- |
| `id` | integer | yes | 删除系统钩子 |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/hooks/2"
```
