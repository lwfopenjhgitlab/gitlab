---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 草稿备注 API **(FREE)**

草稿备注是合并请求的未决、未发表的评论。他们可以开始讨论，也可以作为回复与现有讨论相关联。在发布之前，只有作者可以查看。

## 列出所有合并请求草稿备注

获取单个合并请求的所有草稿备注的列表。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/draft_notes
```

| 参数                 | 类型                | 是否必需 | 描述                                                                                  |
|--------------------|-------------------|------|-------------------------------------------------------------------------------------|
| `id`               | integer or string | yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) 
| `merge_request_iid` | integer           | yes  |项目合并请求 IID                                                   

```json
[{
  id: 5,
  author_id: 23,
  merge_request_id: 11,
  resolve_discussion: false,
  discussion_id: nil,
  note: "Example title",
  commit_id: nil,
  line_code: nil,
  position:
  {
    base_sha: nil,
    start_sha: nil,
    head_sha: nil,
    old_path: nil,
    new_path: nil,
    position_type: "text",
    old_line: nil,
    new_line: nil,
    line_range: nil
  }
}]
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/14/merge_requests/11/draft_notes"
```

## 获取单个草稿备注

返回给定合并请求的单个草稿备注。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/draft_notes/:draft_note_id
```

| 参数                  | 类型                | 是否必需 | 描述                                                                                   |
|---------------------|-------------------|------|--------------------------------------------------------------------------------------|
| `id`                | integer or string | yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) 
| `draft_note_id`     | integer           | yes  | 草稿备注的 ID                                                              
| `merge_request_iid` | integer           | yes  | 项目合并请求的 IID                                                  

```json
{
  id: 5,
  author_id: 23,
  merge_request_id: 11,
  resolve_discussion: false,
  discussion_id: nil,
  note: "Example title",
  commit_id: nil,
  line_code: nil,
  position:
  {
    base_sha: nil,
    start_sha: nil,
    head_sha: nil,
    old_path: nil,
    new_path: nil,
    position_type: "text",
    old_line: nil,
    new_line: nil,
    line_range: nil
  }
}
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/14/merge_requests/11/draft_notes/5"
```

## 创建草稿备注

为特定合并请求创建草稿备注。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/draft_notes
```

| 参数                          | 类型                | 是否必需 | 描述                                                                                   |
|-----------------------------|-------------------|------|--------------------------------------------------------------------------------------|
| `id`                        | integer or string | yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) 
| `merge_request_iid`         | integer           | yes  | 项目合并请求 IID                                                 
| `note`                      | string            | yes  | 备注内容                                                               
| `commit_id`                 | string            | no   | 关联草稿备注的提交 SHA                                   
| `in_reply_to_discussion_id` | integer           | no   | 草稿备注回复的讨论 ID                                    
| `resolve_discussion`        | boolean           | no   | 应该解决相关讨论                                      

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/14/merge_requests/11/draft_notes?note=note
```

## 更改现存的草稿备注

为特定合并请求更改草稿备注。

```plaintext
PUT /projects/:id/merge_requests/:merge_request_iid/draft_notes/:draft_note_id
```

| 参数                  | 类型                | 是否必需 | 描述                                                                                   |
|---------------------|-------------------|------|--------------------------------------------------------------------------------------|
| `id`                | integer or string | yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) 
| `draft_note_id`     | integer           | yes  | 草稿备注 ID                                                          
| `merge_request_iid` | integer           | yes  | 项目合并请求 IID                                                 
| `note`              | string            | no   | 备注内容                                                               

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/14/merge_requests/11/draft_notes/5"
```

## 删除草稿备注

删除给定合并请求的现有草稿备注。

```plaintext
DELETE /projects/:id/merge_requests/:merge_request_iid/draft_notes/:draft_note_id
```

| 参数                  | 类型                | 是否必需 | 描述                                                                                   |
|---------------------|-------------------|------|--------------------------------------------------------------------------------------|
| `draft_note_id`     | integer           | yes  | 草稿备注的 ID                                                            
| `id`                | integer or string | yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)
| `merge_request_iid` | integer           | yes  | 项目合并请求的 IID                                                  

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/14/merge_requests/11/draft_notes/5"
```

## 发布草稿备注

为给定的合并请求发布现有草稿备注。

```plaintext
PUT /projects/:id/merge_requests/:merge_request_iid/draft_notes/:draft_note_id/publish
```

| 参数                  | 类型                | 是否必需 | 描述                                                                                   |
|---------------------|-------------------|------|--------------------------------------------------------------------------------------|
| `draft_note_id`     | integer           | yes  | 草稿备注的 ID                                                            
| `id`                | integer or string | yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) 
| `merge_request_iid` | integer           | yes  | 项目合并请求的 IID                                                

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/14/merge_requests/11/draft_notes/5/publish"
```

## 发布所有待发布草稿备注

批量发布属于用户的给定合并请求的所有现有草稿备注。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/draft_notes/bulk_publish
```

| 参数                  | 类型                | 是否必需 | 描述                                                                                   |
|---------------------|-------------------|------|--------------------------------------------------------------------------------------|
| `id`                | integer or string | yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) 
| `merge_request_iid` | integer           | yes  | 项目合并请求 IID                                                  

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/14/merge_requests/11/draft_notes/bulk_publish"
```
