---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 资源权重事件 API **(FREE)**

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/32542)--> 引入于极狐GitLab 13.2。

资源权重事件会持续跟踪极狐GitLab [议题](../user/project/issues/index.md)发生的情况。

用它们来追踪设置了哪个权重、谁以及何时进行了设置。

## 议题

### 列出项目议题权重事件

获取单个议题的所有权重事件的列表。

```plaintext
GET /projects/:id/issues/:issue_iid/resource_weight_events
```

| 参数 | 类型 | 是否必需 | 描述                                                           |
| ----------- | -------------- | -------- |--------------------------------------------------------------|
| `id`        | integer/string | yes      | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `issue_iid` | integer        | yes      | 议题的 IID。                                                     |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/resource_weight_events"
```

响应示例：

```json
[
  {
    "id": 142,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-20T13:38:20.077Z",
    "issue_id": 253,
    "weight": 3
  },
  {
    "id": 143,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-21T14:38:20.077Z",
    "issue_id": 253,
    "weight": 2
  }
]
```

### 获得单个议题权重事件

返回特定项目议题的单个权重事件。

```plaintext
GET /projects/:id/issues/:issue_iid/resource_weight_events/:resource_weight_event_id
```

参数：

| 参数 | 类型 | 是否必需 | 描述                                                           |
| ----------------------------- | -------------- | -------- |--------------------------------------------------------------|
| `id`                          | integer/string | yes      | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `issue_iid`                   | integer        | yes      | 议题的 IID。                                                     |
| `resource_weight_event_id`    | integer        | yes      | 权重事件的 ID。                                                    |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/resource_weight_events/143"
```

响应示例：

```json
{
"id": 143,
"user": {
  "id": 1,
  "name": "Administrator",
  "username": "root",
  "state": "active",
  "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
  "web_url": "http://gitlab.example.com/root"
},
"created_at": "2018-08-21T14:38:20.077Z",
"issue_id": 253,
"weight": 2
}
```
