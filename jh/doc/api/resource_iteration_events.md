---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 资源迭代事件 API **(PREMIUM)**

> - <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/229463)-->引入于极狐GitLab 13.4。
> - <!--[Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/229463)-->引入于极狐GitLab 13.5。
> - 在 13.9 版本中移到了极狐GitLab 专业版。

资源迭代事件跟踪极狐GitLab [议题](../user/project/issues/index.md)发生的情况。

使用它们来跟踪设置了哪个迭代、由谁完成以及何时完成。

## 议题

### 列出项目议题迭代事件

获取单个议题的所有迭代事件的列表。

```plaintext
GET /projects/:id/issues/:issue_iid/resource_iteration_events
```

| 参数 | 类型 | 是否必需 | 描述                                                      |
| ----------- | -------------- | -------- |---------------------------------------------------------|
| `id`        | integer/string | yes      | 项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding)。 |
| `issue_iid` | integer        | yes      | 议题的 IID。                                                |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/resource_iteration_events"
```

响应示例：

```json
[
  {
    "id": 142,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-20T13:38:20.077Z",
    "resource_type": "Issue",
    "resource_id": 253,
    "iteration":   {
      "id": 50,
      "iid": 9,
      "group_id": 5,
      "title": "Iteration I",
      "description": "Ipsum Lorem",
      "state": 1,
      "created_at": "2020-01-27T05:07:12.573Z",
      "updated_at": "2020-01-27T05:07:12.573Z",
      "due_date": null,
      "start_date": null
    },
    "action": "add"
  },
  {
    "id": 143,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-21T14:38:20.077Z",
    "resource_type": "Issue",
    "resource_id": 253,
    "iteration":   {
      "id": 53,
      "iid": 13,
      "group_id": 5,
      "title": "Iteration II",
      "description": "Ipsum Lorem ipsum",
      "state": 2,
      "created_at": "2020-01-27T05:07:12.573Z",
      "updated_at": "2020-01-27T05:07:12.573Z",
      "due_date": null,
      "start_date": null
    },
    "action": "remove"
  }
]
```

### 获取单个议题迭代事件

返回特定项目议题的单个迭代事件。

```plaintext
GET /projects/:id/issues/:issue_iid/resource_iteration_events/:resource_iteration_event_id
```

Parameters:

| 参数 | 类型 | 是否必需 | 描述                                                          |
| ----------------------------- | -------------- | -------- |-------------------------------------------------------------|
| `id`                          | integer/string | yes      | 项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding)。 |
| `issue_iid`                   | integer        | yes      | 议题的 IID。                                                    |
| `resource_iteration_event_id` | integer        | yes      | 迭代事件的 ID。                                                   |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/resource_iteration_events/143"
```

响应示例：

```json
{
  "id": 143,
  "user": {
    "id": 1,
    "name": "Administrator",
    "username": "root",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://gitlab.example.com/root"
  },
  "created_at": "2018-08-21T14:38:20.077Z",
  "resource_type": "Issue",
  "resource_id": 253,
  "iteration":   {
    "id": 53,
    "iid": 13,
    "group_id": 5,
    "title": "Iteration II",
    "description": "Ipsum Lorem ipsum",
    "state": 2,
    "created_at": "2020-01-27T05:07:12.573Z",
    "updated_at": "2020-01-27T05:07:12.573Z",
    "due_date": null,
    "start_date": null
  },
  "action": "remove"
}
```
