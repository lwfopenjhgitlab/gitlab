---
stage: Govern
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 审计事件 API **(PREMIUM)**

> - 引入于 12.4 版本。
> - Author Email 添加到响应主体于极狐GitLab 15.9。

<a id="instance-audit-events"></a>

## 实例审计事件 **(PREMIUM SELF)**

此实例事件 API 允许您检索[实例事件](../administration/audit_events.md#instance-events)。
此 API 无法检索群组或项目事件。

要使用 API 检索审计事件，您必须以管理员身份进行[身份验证](rest/index.md#authentication)。

<a id="retrieve-all-instance-audit-events"></a>

### 检索所有实例审计事件

> 对键集分页的支持引入于极狐GitLab 15.11。

```plaintext
GET /audit_events
```

| 参数       | 类型  | 是否必需  | 描述         |
| --------- | ---- | -------- | ----------- |
| `created_after` | string | no | 返回在给定时间或之后创建的审计事件。格式：ISO 8601 （`YYYY-MM-DDTHH:MM:SSZ`）。  |
| `created_before` | string | no | 返回在给定时间或之前创建的审计事件。格式：ISO 8601 （`YYYY-MM-DDTHH:MM:SSZ`）。 |
| `entity_type` | string | no | 返回给定 entity_type 的审计事件。有效的值为：`User`、 `Group` 或者 `Project`。  |
| `entity_id` | integer | no | 返回给定 entity_id 的审计事件。要求 `entity_type` 属性必须提供。 |

此端点支持基于偏移量和[基于键集](rest/index.md#keyset-based-pagination)的分页。在请求连续的结果页时，您应该使用基于键集的分页。

阅读更多关于[分页](rest/index.md#pagination)的内容。

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/audit_events"
```

返回示例：

```json
[
  {
    "id": 1,
    "author_id": 1,
    "entity_id": 6,
    "entity_type": "Project",
    "details": {
      "custom_message": "Project archived",
      "author_name": "Administrator",
      "author_email": "admin@example.com",
      "target_id": "flightjs/flight",
      "target_type": "Project",
      "target_details": "flightjs/flight",
      "ip_address": "127.0.0.1",
      "entity_path": "flightjs/flight"
    },
    "created_at": "2019-08-30T07:00:41.885Z"
  },
  {
    "id": 2,
    "author_id": 1,
    "entity_id": 60,
    "entity_type": "Group",
    "details": {
      "add": "group",
      "author_name": "Administrator",
      "author_email": "admin@example.com",
      "target_id": "flightjs",
      "target_type": "Group",
      "target_details": "flightjs",
      "ip_address": "127.0.0.1",
      "entity_path": "flightjs"
    },
    "created_at": "2019-08-27T18:36:44.162Z"
  },
  {
    "id": 3,
    "author_id": 51,
    "entity_id": 51,
    "entity_type": "User",
    "details": {
      "change": "email address",
      "from": "hello@flightjs.com",
      "to": "maintainer@flightjs.com",
      "author_name": "Andreas",
      "author_email": "admin@example.com",
      "target_id": 51,
      "target_type": "User",
      "target_details": "Andreas",
      "ip_address": null,
      "entity_path": "Andreas"
    },
    "created_at": "2019-08-22T16:34:25.639Z"
  }
]
```

### 检索单个实例审计事件

```plaintext
GET /audit_events/:id
```

| 参数       | 类型 | 是否必需   | 描述         |
| --------- | ---- | -------- | ----------- |
| `id` | integer | yes | 审计事件的 ID。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/audit_events/1"
```

返回示例：

```json
{
  "id": 1,
  "author_id": 1,
  "entity_id": 6,
  "entity_type": "Project",
  "details": {
    "custom_message": "Project archived",
    "author_name": "Administrator",
    "author_email": "admin@example.com",
    "target_id": "flightjs/flight",
    "target_type": "Project",
    "target_details": "flightjs/flight",
    "ip_address": "127.0.0.1",
    "entity_path": "flightjs/flight"
  },
  "created_at": "2019-08-30T07:00:41.885Z"
}
```

<a id="group-audit-events"></a>

## 群组审计事件

> - 引入于 12.5 版本。
> - 对 keyset 分页的支持添加于 15.2 版本。
> - 返回给定群组内项目的项目审计事件引入于极狐GitLab 16.1，功能标志为 `audit_event_group_rollup`，默认禁用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下无法返回给定群组内项目的项目审核事件。要使其可用，请让管理员[启用名为 `audit_event_group_rollup` 的功能标志](../administration/feature_flags.md)。在 JiHuLab.com 上，此功能不可用。该功能尚未准备好用于生产使用。

此群组事件 API 允许您检索[群组审计事件](../administration/audit_events.md#group-events)。 

具有所有者权限的用户可以检索所有用户的群组审计事件。
具有开发者或维护者权限的用户只能基于各自的操作对审计事件进行分组。

此端点支持基于偏移量和[基于键集](rest/index.md#keyset-based-pagination)的分页。
当请求连续的结果页时，建议使用基于键集的分页。

<a id="retrieve-all-group-audit-events"></a>

### 检索所有群组审计事件

> 对键集分页的支持引入于极狐GitLab 15.2。

```plaintext
GET /groups/:id/audit_events
```

| 参数       | 类型 | 是否必需   | 描述         |
| --------- | ---- | -------- | ----------- |
| `id` | integer/string | yes | 群组的 ID 或者 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `created_after` | string | no | 返回在给定时间或之后创建的群组事件。格式：ISO 8601 （`YYYY-MM-DDTHH:MM:SSZ`）。 |
| `created_before` | string | no | 返回在给定时间或之前创建的群组事件。格式：ISO 8601 （`YYYY-MM-DDTHH:MM:SSZ`）。 |

默认情况下，`GET` 请求一次返回 20 个结果，因为 API 返回的结果是经过分页的。

阅读更多关于[分页](rest/index.md#pagination)的内容。

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/groups/60/audit_events"
```

返回示例：

```json
[
  {
    "id": 2,
    "author_id": 1,
    "entity_id": 60,
    "entity_type": "Group",
    "details": {
      "custom_message": "Group marked for deletion",
      "author_name": "Administrator",
      "author_email": "admin@example.com",
      "target_id": "flightjs",
      "target_type": "Group",
      "target_details": "flightjs",
      "ip_address": "127.0.0.1",
      "entity_path": "flightjs"
    },
    "created_at": "2019-08-28T19:36:44.162Z"
  },
  {
    "id": 1,
    "author_id": 1,
    "entity_id": 60,
    "entity_type": "Group",
    "details": {
      "add": "group",
      "author_name": "Administrator",
      "author_email": "admin@example.com",
      "target_id": "flightjs",
      "target_type": "Group",
      "target_details": "flightjs",
      "ip_address": "127.0.0.1",
      "entity_path": "flightjs"
    },
    "created_at": "2019-08-27T18:36:44.162Z"
  }
]
```

### 检索特定群组审计事件

仅对群组所有者和管理员可用。

```plaintext
GET /groups/:id/audit_events/:audit_event_id
```

| 参数       | 类型  | 是否必需  | 描述         |
| --------- | ---- | -------- | ----------- |
| `id` | integer/string | yes | 群组的 ID 或者 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `audit_event_id` | integer | yes | 审计事件的 ID。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/groups/60/audit_events/2"
```

返回示例：

```json
{
  "id": 2,
  "author_id": 1,
  "entity_id": 60,
  "entity_type": "Group",
  "details": {
    "custom_message": "Group marked for deletion",
    "author_name": "Administrator",
    "author_email": "admin@example.com",
    "target_id": "flightjs",
    "target_type": "Group",
    "target_details": "flightjs",
    "ip_address": "127.0.0.1",
    "entity_path": "flightjs"
  },
  "created_at": "2019-08-28T19:36:44.162Z"
}
```

<a id="project-audit-events"></a>

## 项目审计事件

项目事件 API 允许您检索[项目事件](../administration/audit_events.md#project-events)。

具有维护者权限（或以上）的用户可以检索所有用户的项目事件。
具有开发者权限的用户受限于基于其个人操作的项目事件。

<a id="retrieve-all-project-audit-events"></a>

### 检索所有项目审计事件

> 对键集分页的支持引入于极狐GitLab 15.10。

```plaintext
GET /projects/:id/audit_events
```

| 参数       | 类型  | 是否必需  | 描述         |
| --------- | ---- | -------- | ----------- |
| `id` | integer/string | yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `created_after` | string | no | 返回在给定时间或之后创建的项目事件。格式：ISO 8601 （`YYYY-MM-DDTHH:MM:SSZ`）。  |
| `created_before` | string | no | 返回在给定时间或之前创建的项目审计事件。格式：ISO 8601 （`YYYY-MM-DDTHH:MM:SSZ`）。 |

默认情况下，`GET` 请求一次返回 20 个结果，因为 API 返回的结果是经过分页的。请求连续结果页面时，您应该使用[键集分页](rest/index.md#keyset-based-pagination)。

阅读更多关于[分页](rest/index.md#pagination)的内容。

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/projects/7/audit_events"
```

返回示例：

```json
[
  {
    "id": 5,
    "author_id": 1,
    "entity_id": 7,
    "entity_type": "Project",
    "details": {
      "change": "prevent merge request approval from committers",
      "from": "",
      "to": "true",
      "author_name": "Administrator",
      "author_email": "admin@example.com",
      "target_id": 7,
      "target_type": "Project",
      "target_details": "twitter/typeahead-js",
      "ip_address": "127.0.0.1",
      "entity_path": "twitter/typeahead-js"
    },
    "created_at": "2020-05-26T22:55:04.230Z"
  },
  {
    "id": 4,
    "author_id": 1,
    "entity_id": 7,
    "entity_type": "Project",
    "details": {
      "change": "prevent merge request approval from authors",
      "from": "false",
      "to": "true",
      "author_name": "Administrator",
      "author_email": "admin@example.com",
      "target_id": 7,
      "target_type": "Project",
      "target_details": "twitter/typeahead-js",
      "ip_address": "127.0.0.1",
      "entity_path": "twitter/typeahead-js"
    },
    "created_at": "2020-05-26T22:55:04.218Z"
  }
]
```

### 检索特定项目审计事件

仅对项目中至少具有维护者角色的用户可用。

```plaintext
GET /projects/:id/audit_events/:audit_event_id
```

| 参数       | 类型  | 是否必需  | 描述         |
| --------- | ---- | -------- | ----------- |
| `id` | integer/string | yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `audit_event_id` | integer | yes | 审计事件的 ID。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/projects/7/audit_events/5"
```

返回示例：

```json
{
  "id": 5,
  "author_id": 1,
  "entity_id": 7,
  "entity_type": "Project",
  "details": {
    "change": "prevent merge request approval from committers",
    "from": "",
    "to": "true",
    "author_name": "Administrator",
    "author_email": "admin@example.com",
    "target_id": 7,
    "target_type": "Project",
    "target_details": "twitter/typeahead-js",
    "ip_address": "127.0.0.1",
    "entity_path": "twitter/typeahead-js"
  },
  "created_at": "2020-05-26T22:55:04.230Z"
}
```
