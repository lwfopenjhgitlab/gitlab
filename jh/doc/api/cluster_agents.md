---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 代理 API **(FREE)**

> - <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/83270)-->引入于极狐GitLab 14.10。
> - 代理令牌 API<!-- [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/347046) --> 引入于极狐GitLab 15.0。

使用代理 API 与 Kubernetes 的极狐GitLab 代理一起工作。

## 列出项目的代理

返回项目注册的代理列表。

您必须至少具有开发者的角色才能使用这个端点。

```plaintext
GET /projects/:id/cluster_agents
```

参数：

| 参数   | 类型                | 是否必需 | 描述                                                                                                                             |
|------|-------------------|------|--------------------------------------------------------------------------------------------------------------------------------|
| `id` | integer or string | yes  | 授权用户维护的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)|

响应：

响应是包含以下字段的代理列表：

| 参数                                   | 类型      | 描述                                            |
|--------------------------------------|---------|-----------------------------------------------|
| `id`                                 | integer | 代理 ID                                         |
| `name`                               | string  | 代理名称                                          |
| `config_project`                     | object  | 代表代理从属的项目的对象                                  |
| `config_project.id`                  | integer | 项目 ID                                         |
| `config_project.description`         | string  | 项目描述                                          |
| `config_project.name`                | string  | 项目名称                                          |
| `config_project.name_with_namespace` | string  | 带有项目命名空间的全名                                   |
| `config_project.path`                | string  | 项目路径                                          |
| `config_project.path_with_namespace` | string  | 带有项目命名空间的完整路径                                 |
| `config_project.created_at`          | string  | 创建项目的 ISO8601 日期时间                            |
| `created_at`                         | string  | 创建代理的 ISO8601 日期时间                            |
| `created_by_user_id`                 | integer | 创建代理的用户的 ID |

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/20/cluster_agents"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "agent-1",
    "config_project": {
      "id": 20,
      "description": "",
      "name": "test",
      "name_with_namespace": "Administrator / test",
      "path": "test",
      "path_with_namespace": "root/test",
      "created_at": "2022-03-20T20:42:40.221Z"
    },
    "created_at": "2022-04-20T20:42:40.221Z",
    "created_by_user_id": 42
  },
  {
    "id": 2,
    "name": "agent-2",
    "config_project": {
      "id": 20,
      "description": "",
      "name": "test",
      "name_with_namespace": "Administrator / test",
      "path": "test",
      "path_with_namespace": "root/test",
      "created_at": "2022-03-20T20:42:40.221Z"
    },
    "created_at": "2022-04-20T20:42:40.221Z",
    "created_by_user_id": 42
  }
]
```

## 获取代理详细信息

获取单个代理的详细信息。

您必须至少具有开发者的角色才能使用这个端点。

```plaintext
GET /projects/:id/cluster_agents/:agent_id
```

参数：

| 参数         | 类型                | 是否必需 | 描述                                                            |
|------------|-------------------|------|---------------------------------------------------------------|
| `id`       | integer or string | yes  | 授权用户维护的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `agent_id` | integer           | yes  | 代理 ID                                           |

响应：

响应是包含以下字段的单个代理：

| 参数                                   | 类型      | 描述                                            |
|--------------------------------------|---------|-----------------------------------------------|
| `id`                                 | integer | 代理 ID                                         |
| `name`                               | string  | 代理名称                                          |
| `config_project`                     | object  | 代表代理从属的项目的对象                                  |
| `config_project.id`                  | integer | 项目 ID                                         |
| `config_project.description`         | string  | 项目描述                                          |
| `config_project.name`                | string  | 项目名称                                          |
| `config_project.name_with_namespace` | string  | 带有项目命名空间的全名                                   |
| `config_project.path`                | string  | 项目路径                                          |
| `config_project.path_with_namespace` | string  | 带有项目命名空间的完整路径                                 |
| `config_project.created_at`          | string  | 创建项目的 ISO8601 日期时间                            |
| `created_at`                         | string  | 创建代理的 ISO8601 日期时间                            |
| `created_by_user_id`                 | integer | 创建代理的用户的 ID |

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/20/cluster_agents/1"
```

响应示例：

```json
{
  "id": 1,
  "name": "agent-1",
  "config_project": {
    "id": 20,
    "description": "",
    "name": "test",
    "name_with_namespace": "Administrator / test",
    "path": "test",
    "path_with_namespace": "root/test",
    "created_at": "2022-03-20T20:42:40.221Z"
  },
  "created_at": "2022-04-20T20:42:40.221Z",
  "created_by_user_id": 42
}
```

## 注册项目代理

注册项目代理。

您必须至少具有维护者的角色才能使用这个端点。

```plaintext
POST /projects/:id/cluster_agents
```

参数：

| 参数     | 类型                | 是否必需 | 描述                                                            |
|--------|-------------------|------|---------------------------------------------------------------|
| `id`   | integer or string | yes  | 授权用户维护的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `name` | string            | yes  | 代理名称                                                          |

响应：

响应是包含以下字段的新代理：

| 参数                                   | 类型      | 描述                                            |
|--------------------------------------|---------|-----------------------------------------------|
| `id`                                 | integer | 代理 ID                                         |
| `name`                               | string  | 代理名称                                          |
| `config_project`                     | object  | 代表代理从属的项目的对象                                  |
| `config_project.id`                  | integer | 项目 ID                                         |
| `config_project.description`         | string  | 项目描述                                          |
| `config_project.name`                | string  | 项目名称                                          |
| `config_project.name_with_namespace` | string  | 带有项目命名空间的全名                                   |
| `config_project.path`                | string  | 项目路径                                          |
| `config_project.path_with_namespace` | string  | 带有项目命名空间的完整路径                                 |
| `config_project.created_at`          | string  | 创建项目的 ISO8601 日期时间                            |
| `created_at`                         | string  | 创建代理的 ISO8601 日期时间                            |
| `created_by_user_id`                 | integer | 创建代理的用户的 ID  |

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/20/cluster_agents" \
    -H "Content-Type:application/json" \
    -X POST --data '{"name":"some-agent"}'
```

响应示例：

```json
{
  "id": 1,
  "name": "agent-1",
  "config_project": {
    "id": 20,
    "description": "",
    "name": "test",
    "name_with_namespace": "Administrator / test",
    "path": "test",
    "path_with_namespace": "root/test",
    "created_at": "2022-03-20T20:42:40.221Z"
  },
  "created_at": "2022-04-20T20:42:40.221Z",
  "created_by_user_id": 42
}
```

## 删除注册代理

删除现存的代理注册。

您必须至少具有维护者的角色才能使用这个端点。

```plaintext
DELETE /projects/:id/cluster_agents/:agent_id
```

参数：

| 参数     | 类型                | 是否必需 | 描述                                                            |
|--------|-------------------|------|---------------------------------------------------------------|
| `id`   | integer or string | yes  | 授权用户维护的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `agent_id`  | integer              | yes  | 代理 ID                                                          |
请求示例：

```shell
curl --request DELETE --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/20/cluster_agents/1
```

## 列出代理的令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/347046) -->引入于极狐GitLab 15.0。

返回代理的活跃令牌的列表。

您必须至少具有开发者的角色才能使用这个端点。

```plaintext
GET /projects/:id/cluster_agents/:agent_id/tokens
```

支持的参数：

| 参数         | 类型                | 是否必需 | 描述                                                            |
|------------|-------------------|------|---------------------------------------------------------------|
| `id`       | integer or string | yes  | 授权用户维护的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `agent_id` | integer or string | yes  | 代理 ID                                          |

响应：

响应是包含以下字段的令牌列表：

| 参数                   | 类型             | 描述                                                  |
|----------------------|----------------|-----------------------------------------------------|
| `id`                 | integer        | 令牌 ID                                               |
| `name`               | string         | 令牌名称                                                |
| `description`        | string or null | 令牌描述                                                |
| `agent_id`           | integer        | 令牌从属的代理的 ID                                         |
| `status`             | string         | 令牌的状态。有效值为 `active` 和 `revoked`                     |
| `created_at`         | string         | 创建令牌的 ISO8601 日期时间                                  |
| `created_by_user_id` | string         | 创建令牌的用户的 ID |

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/20/cluster_agents/5/tokens"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "abcd",
    "description": "Some token",
    "agent_id": 5,
    "status": "active",
    "created_at": "2022-03-25T14:12:11.497Z",
    "created_by_user_id": 1
  },
  {
    "id": 2,
    "name": "foobar",
    "description": null,
    "agent_id": 5,
    "status": "active",
    "created_at": "2022-03-25T14:12:11.497Z",
    "created_by_user_id": 1
  }
]
```

NOTE:
只有在获取单个代理令牌的时候，才返回令牌的 `last_used_at` 字段。

## 获取单个代理令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/347046) -->引入于极狐GitLab 15.0。

获取单个代理令牌。

您必须至少具有开发者的角色才能使用这个端点。

如果代理令牌已被撤销，则返回 `404`。

```plaintext
GET /projects/:id/cluster_agents/:agent_id/tokens/:token_id
```

支持的参数：

| 参数         | 类型                | 是否必需 | 描述                                                            |
|------------|-------------------|------|---------------------------------------------------------------|
| `id`       | integer or string | yes  | 授权用户维护的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `agent_id` | integer           | yes  | 代理 ID                                          |
| `token_id` | integer           | yes  | 令牌 ID                                           |

响应：

响应是包含以下字段的单个令牌：

| 参数                   | 类型             | 描述                           |
|----------------------|----------------|------------------------------|
| `id`                 | integer        | 令牌 ID                        |
| `name`               | string         | 令牌名称                         |
| `description`        | string or null | 令牌描述                         |
| `agent_id`           | integer        | 令牌从属的代理的 ID                  |
| `status`             | string         | 令牌状态。有效值为 `active` 和 `revoked` |
| `created_at`         | string         | 创建令牌的 ISO8601 日期时间           |
| `created_by_user_id` | string         | 创建令牌的用户的 ID                  |
| `last_used_at`       | string or null | 上次使用令牌的 ISO8601 日期时间         |

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/20/cluster_agents/5/token/1"
```

响应示例：

```json
{
  "id": 1,
  "name": "abcd",
  "description": "Some token",
  "agent_id": 5,
  "status": "active",
  "created_at": "2022-03-25T14:12:11.497Z",
  "created_by_user_id": 1,
  "last_used_at": null
}
```

## 创建代理令牌

> - 引入于极狐GitLab 15.0。
> - 双令牌限制引入于极狐GitLab 16.1。

为代理创建新的令牌。

您必须至少具有维护者的角色才能使用这个端点。

代理一次只能拥有两个活动令牌。

```plaintext
POST /projects/:id/cluster_agents/:agent_id/tokens
```

支持的参数：

| 参数            | 类型                | 是否必需 | 描述                                                            |
|---------------|-------------------|------|---------------------------------------------------------------|
| `id`          | integer or string | yes  | 授权用户维护的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `agent_id`    | integer           | yes  | 代理 ID                                                         |
| `name`        | string            | yes  | 令牌名称                                                          |
| `description` | string            | no   | 令牌描述                               |

响应：

响应是包含以下字段的新令牌：

| 参数                   | 类型             | 描述                           |
|----------------------|----------------|------------------------------|
| `id`                 | integer        | 令牌 ID                        |
| `name`               | string         | 令牌名称                         |
| `description`        | string or null | 令牌描述                         |
| `agent_id`           | integer        | 令牌从属的代理的 ID                  |
| `status`             | string         | 令牌状态。有效值为 `active` 和 `revoked` |
| `created_at`         | string         | 创建令牌的 ISO8601 日期时间           |
| `created_by_user_id` | string         | 创建令牌的用户的 ID                  |
| `last_used_at`       | string or null | 上次使用的令牌的 ISO8601 日期时间        |
| `token`              | string         | Secret 令牌值                   |

NOTE:
只有响应 `POST` 端点的时候才能返回 `token`，且之后无法检索。

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/20/cluster_agents/5/tokens" \
    -H "Content-Type:application/json" \
    -X POST --data '{"name":"some-token"}'
```

响应示例：

```json
{
  "id": 1,
  "name": "abcd",
  "description": "Some token",
  "agent_id": 5,
  "status": "active",
  "created_at": "2022-03-25T14:12:11.497Z",
  "created_by_user_id": 1,
  "last_used_at": null,
  "token": "qeY8UVRisx9y3Loxo1scLxFuRxYcgeX3sxsdrpP_fR3Loq4xyg"
}
```

## 撤回代理令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/347046) -->引入于极狐GitLab 15.0。

撤回代理令牌。

您必须至少具有维护者的角色才能使用这个端点。

```plaintext
DELETE /projects/:id/cluster_agents/:agent_id/tokens/:token_id
```

支持的参数：

| 参数         | 类型                | 是否必需 | 描述                                                            |
|------------|-------------------|------|---------------------------------------------------------------|
| `id`       | integer or string | yes  | 授权用户维护的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `agent_id` | integer           | yes  | 代理 ID                                          |
| `token_id` | integer           | yes  | 令牌 ID                                           |
请求示例：

```shell
curl --request DELETE --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/20/cluster_agents/5/tokens/1
```
