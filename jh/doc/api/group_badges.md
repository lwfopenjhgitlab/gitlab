---
stage: Data Stores
group: Tenant Scale
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组徽章 API **(FREE)**

> 引入于极狐GitLab 10.6。

## 占位符令牌

[徽章](../user/project/badges.md)支持在链接和镜像 URL 中实时替换的占位符。允许的占位符为：

<!-- vale gitlab.Spelling = NO -->

- **%{project_path}**：替换为项目路径。
- **%{project_title}**：替换为项目标题。
- **%{project_name}**：替换为项目名称。
- **%{project_id}**：替换为项目 ID。
- **%{default_branch}**：替换为项目默认分支。
- **%{commit_sha}**：替换为上一个项目的提交 SHA。

<!-- vale gitlab.Spelling = YES -->

因为这些端点不在项目的上下文中，所以用于替换占位符的信息来自第一个创建的群组的项目。如果该群组没有任何项目，则返回带有占位符的原始 URL。

## 列出群组的所有徽章

列出群组徽章列表。

```plaintext
GET /groups/:id/badges
```

| 参数     | 类型             | 是否必需 | 描述                                                            |
|--------|----------------|------|---------------------------------------------------------------|
| `id`   | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `name` | string         | no   | 返回的徽章的名称（区分大小写） |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/badges?name=Coverage"
```

响应示例：

```json
[
  {
    "name": "Coverage",
    "id": 1,
    "link_url": "http://example.com/ci_status.svg?project=%{project_path}&ref=%{default_branch}",
    "image_url": "https://shields.io/my/badge",
    "rendered_link_url": "http://example.com/ci_status.svg?project=example-org/example-project&ref=master",
    "rendered_image_url": "https://shields.io/my/badge",
    "kind": "group"
  }
]
```

## 获取群组徽章

获取群组的徽章。

```plaintext
GET /groups/:id/badges/:badge_id
```

| 参数         | 类型             | 是否必需 | 描述                                                            |
|------------|----------------|------|---------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `badge_id` | integer        | yes  | 徽章 ID                                                         |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/badges/:badge_id"
```

响应示例：

```json
{
  "name": "Coverage",
  "id": 1,
  "link_url": "http://example.com/ci_status.svg?project=%{project_path}&ref=%{default_branch}",
  "image_url": "https://shields.io/my/badge",
  "rendered_link_url": "http://example.com/ci_status.svg?project=example-org/example-project&ref=master",
  "rendered_image_url": "https://shields.io/my/badge",
  "kind": "group"
}
```


## 向群组添加徽章

向群组添加徽章。

```plaintext
POST /groups/:id/badges
```

| 参数          | 类型             | 是否必需 | 描述                                                                 |
|-------------|----------------|------|--------------------------------------------------------------------|
| `id`        | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `link_url`  | string         | yes  | 徽章链接的 URL                                                          |
| `image_url` | string         | yes  | 徽章镜像的 URL                                                          |
| `name`      | string         | no   | 徽章的名称                                                              |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --data "link_url=https://gitlab.com/gitlab-org/gitlab-foss/commits/master&image_url=https://shields.io/my/badge1&position=0" \
     "https://gitlab.example.com/api/v4/groups/:id/badges"
```

响应示例：

```json
{
  "id": 1,
  "name": "mybadge",
  "link_url": "https://gitlab.com/gitlab-org/gitlab-foss/commits/master",
  "image_url": "https://shields.io/my/badge1",
  "rendered_link_url": "https://gitlab.com/gitlab-org/gitlab-foss/commits/master",
  "rendered_image_url": "https://shields.io/my/badge1",
  "kind": "group"
}
```

## 编辑群组徽章

更新群组的徽章。

```plaintext
PUT /groups/:id/badges/:badge_id
```

| 参数          | 类型             | 是否必需 | 描述                                                                 |
|-------------|----------------|------|--------------------------------------------------------------------|
| `id`        | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `badge_id`  | integer        | yes  | 徽章 ID                                                              |
| `link_url`  | string         | no   | 徽章链接的 URL                                                          |
| `image_url` | string         | no   | 徽章镜像的 URL                                                          |
| `name`      | string         | no   | 徽章的名称                                                              |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/:id/badges/:badge_id"
```

响应示例：

```json
{
  "id": 1,
  "name": "mybadge",
  "link_url": "https://gitlab.com/gitlab-org/gitlab-foss/commits/master",
  "image_url": "https://shields.io/my/badge",
  "rendered_link_url": "https://gitlab.com/gitlab-org/gitlab-foss/commits/master",
  "rendered_image_url": "https://shields.io/my/badge",
  "kind": "group"
}
```

## 从群组移除徽章

从群组移除徽章。

```plaintext
DELETE /groups/:id/badges/:badge_id
```

| 参数         | 类型             | 是否必需 | 描述                                                            |
|------------|----------------|------|---------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `badge_id` | integer        | yes  | 徽章 ID                                                         |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/badges/:badge_id"
```

## 预览群组徽章

返回解析占位符插值后 `link_url` 和 `image_url` 的最终 URL。

```plaintext
GET /groups/:id/badges/render
```

| 参数          | 类型             | 是否必需 | 描述                                                            |
|-------------|----------------|------|---------------------------------------------------------------|
| `id`        | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `link_url`  | string         | yes  | 徽章链接的 URL                                                     |
| `image_url` | string         | yes  | 徽章镜像的 URL                                |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/badges/render?link_url=http%3A%2F%2Fexample.com%2Fci_status.svg%3Fproject%3D%25%7Bproject_path%7D%26ref%3D%25%7Bdefault_branch%7D&image_url=https%3A%2F%2Fshields.io%2Fmy%2Fbadge"
```

响应示例：

```json
{
  "link_url": "http://example.com/ci_status.svg?project=%{project_path}&ref=%{default_branch}",
  "image_url": "https://shields.io/my/badge",
  "rendered_link_url": "http://example.com/ci_status.svg?project=example-org/example-project&ref=master",
  "rendered_image_url": "https://shields.io/my/badge"
}
```
