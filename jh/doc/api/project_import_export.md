---
stage: Manage
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目导入和导出 API **(FREE)**

使用项目导入和导出 API 通过文件传输导入和导出项目。

在使用项目导入和导出 API 之前，您可能需要使用[群组导入和导出API](group_import_export.md)。

## 先决条件

有关项目导入和导出 API 的先决条件，请参阅：

- [项目导出](../user/project/settings/import_export.md#export-a-project-and-its-data)的先决条件。
- [项目导入](../user/project/settings/import_export.md#import-a-project-and-its-data)的先决条件。

<a id="schedule-an-export"></a>

## 计划导出

开始新的导出任务。

此端点还接受 `upload` 哈希参数。
它包含将导出的项目上传到 Web 服务器或任何与 S3 兼容的平台的所有必要信息。
对于导出，极狐GitLab：

- 只支持将二进制数据文件上传到最终服务器。
- 发送带有上传请求的 `Content-Type: application/gzip` 标头。确保您的预签名 URL 将其作为签名的一部分包含进来。
- 可能需要一些时间才能完成项目导出过程。确保上传 URL 的有效期不是很短，并且在整个导出过程中可用。
- 管理员可以修改最大导出文件大小。默认情况下，最大值是无限的 (`0`)。要改变这一点，
  使用以下任一方法编辑 `max_export_size`：
    - [应用程序设置 API](settings.md#change-application-settings)
    - [极狐GitLab 用户界面](../user/admin_area/settings/account_and_limit_settings.md)。
- JiHuLab.com 上的最大导入文件的大小有固定限制。有关更多信息，请参阅[账户和限制设置](../user/jihulab_com/index.md#account-and-limit-settings)。

如果存在 `upload` 参数，则 `upload[url]` 参数是必需的。

对于上传到 Amazon S3 的相关内容，请参阅[生成用于上传对象的预签名 URL](https://docs.aws.amazon.com/AmazonS3/latest/userguide/PresignedUrlUploadObject.html) 文档脚本以生成 `upload[url]`。

```plaintext
POST /projects/:id/export
```

| 参数                   | 类型             | 是否必需 | 描述 |
| --------------------- |----------------| ------- | ---- |
| `id`                  | integer/string | yes     | 经过身份验证的用户所拥有的项目的 ID 或[网址编码路径](rest/index.md#namespaced-path-encoding) |
| `description`         | string         | no      | 覆盖项目描述 |
| `upload`              | hash           | no      | 包含将导出的项目上载到 Web 服务器的信息的哈希 |
| `upload[url]`         | string         | yes     | 用于上传项目的网址 |
| `upload[http_method]` | string         | no      | 用于上传导出项目的 HTTP 方法。仅允许使用 `PUT` 和 `POST` 方法。默认值为 `PUT` |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
    "https://gitlab.example.com/api/v4/projects/1/export" \
    --data "upload[http_method]=PUT" \
    --data-urlencode "upload[url]=https://example-bucket.s3.eu-west-3.amazonaws.com/backup?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIMBJHN2O62W8IELQ%2F20180312%2Feu-west-3%2Fs3%2Faws4_request&X-Amz-Date=20180312T110328Z&X-Amz-Expires=900&X-Amz-SignedHeaders=host&X-Amz-Signature=8413facb20ff33a49a147a0b4abcff4c8487cc33ee1f7e450c46e8f695569dbd"
```

```json
{
  "message": "202 Accepted"
}
```

## 导出状态

获取导出状态。

```plaintext
GET /projects/:id/export
```

| 参数       | 类型            | 是否必需 | 描述 |
| --------- | -------------- | ------- | ---- |
| `id`      | integer/string | yes     | 经过身份验证的用户所拥有的项目的 ID 或[网址编码路径](rest/index.md#namespaced-path-encoding)。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/1/export"
```

状态可以是以下之一：

- `none`：没有导出处于 *queued*、*started*、*finished* 或 *being regenerated* 状态。
- `queued`：收到导出请求，在队列中等待处理。
- `started`：导出过程已开始并正在进行中。它包括：
  - 处理导出过程。
  - 对生成的文件执行的操作，如发送电子邮件通知用户下载文件，或将导出的文件上载到 Web 服务器。
- `finished`：导出过程已完成，用户已收到通知。
- `regeneration_in_progress`：导出文件可供下载，并且正在处理生成新导出的请求。

`_links` 仅在导出完成后显示。

`created_at` 是项目创建时间戳，而不是导出开始时间。

```json
{
  "id": 1,
  "description": "Itaque perspiciatis minima aspernatur corporis consequatur.",
  "name": "Gitlab Test",
  "name_with_namespace": "Gitlab Org / Gitlab Test",
  "path": "gitlab-test",
  "path_with_namespace": "gitlab-org/gitlab-test",
  "created_at": "2017-08-29T04:36:44.383Z",
  "export_status": "finished",
  "_links": {
    "api_url": "https://gitlab.example.com/api/v4/projects/1/export/download",
    "web_url": "https://gitlab.example.com/gitlab-org/gitlab-test/download_export"
  }
}
```

## 导出下载

下载完成的导出。

```plaintext
GET /projects/:id/export/download
```

| 参数  | 类型            | 是否必需 | 描述 |
| ---- | -------------- | ------- | ---- |
| `id` | integer/string | yes     | 经过身份验证的用户所拥有的项目的 ID 或[网址编码路径](rest/index.md#namespaced-path-encoding)。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" --remote-header-name \
     --remote-name "https://gitlab.example.com/api/v4/projects/5/export/download"
```

```shell
ls *export.tar.gz
2017-12-05_22-11-148_namespace_project_export.tar.gz
```

## 导入文件

> 需要维护者角色而不是开发者角色引入于极狐GitLab 16.0 并反向移植于极狐GitLab 15.11.1 和 15.10.5。

```plaintext
POST /projects/import
```

| 参数               | 类型            | 是否必需 | 描述                                                                                                                                                                                                                                                                                                                                                               |
| ----------------- | -------------- | ------- |------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `namespace`       | integer/string | no      | 要将项目导入到的命名空间的 ID 或路径。默认为当前用户的命名空间.<br/><br/>至少需要目标群组的维护者角色才能导入。为此目的使用开发者角色在极狐GitLab 15.8 中被废弃，并将在 16.0 中删除 |
| `name`            | string         | no      | 要导入的项目的名称。如果未提供，则默认为项目的路径                                                                                                                                                                                                                                                                                                                                        |
| `file`            | string         | yes     | 要上传的文件                                                                                                                                                                                                                                                                                                                                                           |
| `path`            | string         | yes     | 新项目的名称和路径                                                                                                                                                                                                                                                                                                                                                        |
| `overwrite`       | boolean        | no      | 如果存在具有相同路径的项目，则导入将覆盖它。默认为 `false`                                                                                                                                                                                                                                                                                                                                |
| `override_params` | Hash           | no      | 支持[项目 API](projects.md) 中定义的所有字段                                                                                                                                                                                                                                                                                                                                 |

传递的覆盖参数优先于导出文件中定义的所有值。

要从本地文件系统上传文件，请使用 `--form` 参数。
这时 cURL 需要使用 Header `Content-Type: multipart/form-data`。
同时 `file=` 参数必须指向文件系统上的文件，并增加前缀 `@`。例如：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --form "path=api-project" \
     --form "file=@/path/to/file" "https://gitlab.example.com/api/v4/projects/import"
```

cURL 不支持从远端服务器发送文件。此示例演示了如何使用 Python 的 `open` 方法导入项目：

```python
import requests

url =  'https://gitlab.example.com/api/v4/projects/import'
files = { "file": open("project_export.tar.gz", "rb") }
data = {
    "path": "example-project",
    "namespace": "example-group"
}
headers = {
    'Private-Token': "<your_access_token>"
}

requests.post(url, headers=headers, data=data, files=files)
```

```json
{
  "id": 1,
  "description": null,
  "name": "api-project",
  "name_with_namespace": "Administrator / api-project",
  "path": "api-project",
  "path_with_namespace": "root/api-project",
  "created_at": "2018-02-13T09:05:58.023Z",
  "import_status": "scheduled",
  "correlation_id": "mezklWso3Za",
  "failed_relations": []
}
```

NOTE:
最大导入文件大小可由管理员设置，默认值为 `0`（无限制）。
作为管理员，您可以修改最大支持导入的文件大小。请修改[应用程序设置 API](settings.md#change-application-settings) 或[管理中心](../user/admin_area/settings/account_and_limit_settings.md)中的 `max_import_size`。在 13.8 版本中，该值的默认值从 50 MB 修改为 0。

## 从远程对象存储导入文件（Beta）

> 引入于极狐GitLab 13.12，处于 Beta<!--[Beta](../policy/experiment-beta-support.md#beta)--> 阶段。[功能标志](../administration/feature_flags.md)为 `import_project_from_remote_file`。默认启用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能可用。要隐藏该功能，请要求管理员禁用名为 `import_project_from_remote_file` 的[功能标志](../administration/feature_flags.md)。
在 JiHuLab.com 上，此功能可用。

```plaintext
POST /projects/remote-import
```

| 参数               | 类型            | 是否必需  | 描述 |
| ----------------- | -------------- | -------- | ---- |
| `namespace`       | integer/string | no       | 要将项目导入到的命名空间的 ID 或路径。默认为当前用户的命名空间。 |
| `name`            | string         | no       | 要导入的项目的名称。如果未提供，则默认为项目的路径。 |
| `url`             | string         | yes      | 要导入的文件的网址。|
| `path`            | string         | yes      | 新项目的名称和路径。|
| `overwrite`       | boolean        | no       | 如果存在具有相同路径的项目，则导入将覆盖它。默认为 `false`。 |
| `override_params` | Hash           | no       | 支持[项目 API](projects.md) 中定义的所有字段。 |

传递的覆盖参数优先于导出文件中定义的所有值。

```shell
curl --request POST \
  --header "PRIVATE-TOKEN: <your_access_token>" \
  --header "Content-Type: application/json" \
  --url "https://gitlab.example.com/api/v4/projects/remote-import" \
  --data '{"url":"https://remoteobject/file?token=123123","path":"remote-project"}'
```

```json
{
  "id": 1,
  "description": null,
  "name": "remote-project",
  "name_with_namespace": "Administrator / remote-project",
  "path": "remote-project",
  "path_with_namespace": "root/remote-project",
  "created_at": "2018-02-13T09:05:58.023Z",
  "import_status": "scheduled",
  "correlation_id": "mezklWso3Za",
  "failed_relations": [],
  "import_error": null
}
```

`Content-Length` header 必须返回一个有效的数字。最大文件大小为 10 GB。
`Content-Type` header 必须是 `application/gzip`。

## 从 AWS S3 导入文件

> - 引入于 14.9 版本，作为 Beta 功能，带有一个名为 `import_project_from_remote_file_s3` 的功能标志。默认情况下禁用。
> - JiHuLab.com 中启用于极狐GitLab 14.10。
> - 在私有化部署版本上启用于极狐GitLab 15.11。移除 `import_project_from_remote_file_s3` 功能标志。

```plaintext
POST /projects/remote-import-s3
```

| 参数                 | 类型            | 是否必需 | 描述                                                                                                                         |
| ------------------- | -------------- | ------- |----------------------------------------------------------------------------------------------------------------------------|
| `namespace`         | integer/string | no      | 要将项目导入到的命名空间的 ID 或路径。默认为当前用户的命名空间                                                                                          |
| `name`              | string         | no      | 要导入的项目的名称。如果未提供，则默认为项目的路径                                                                                                  |
| `path`              | string         | yes     | 新项目的完整路径                                                                                                                   |
| `region`            | string         | yes     | 存储文件的 [AWS S3 区域名称](https://docs.aws.amazon.com/AmazonS3/latest/userguide/Welcome.html#Regions)                            |
| `bucket_name`       | string         | yes     | 存储文件的[存储桶名称](https://docs.aws.amazon.com/AmazonS3/latest/userguide/bucketnamingrules.html)                                 |
| `file_key`          | string         | yes     | 识别文件的 [AWS S3 文件密钥](https://docs.aws.amazon.com/AmazonS3/latest/userguide/UsingObjects.html)                               |
| `access_key_id`     | string         | yes     | [AWS S3 访问密钥 ID](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-and-secret-access-keys) |
| `secret_access_key` | string         | yes     | [AWS S3 秘密访问密钥](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-and-secret-access-keys)  |

传递的覆盖参数优先于导出文件中定义的所有值。

```shell
curl --request POST \
  --url "https://gitlab.example.com/api/v4/projects/remote-import-s3" \
  --header "PRIVATE-TOKEN: <your gitlab access key>" \
  --header 'Content-Type: application/json' \
  --data '{
  "name": "Sample Project",
  "path": "sample-project",
  "region": "<Your S3 region name>",
  "bucket_name": "<Your S3 bucket name>",
  "file_key": "<Your S3 file key>",
  "access_key_id": "<Your AWS access key id>",
  "secret_access_key": "<Your AWS secret access key>"
}'
```

此示例使用连接到 Amazon S3 的模块从 Amazon S3 存储桶导入：

```python
import requests
from io import BytesIO

s3_file = requests.get(presigned_url)

url =  'https://gitlab.example.com/api/v4/projects/import'
files = {'file': ('file.tar.gz', BytesIO(s3_file.content))}
data = {
    "path": "example-project",
    "namespace": "example-group"
}
headers = {
    'Private-Token': "<your_access_token>"
}

requests.post(url, headers=headers, data=data, files=files)
```

```json
{
  "id": 1,
  "description": null,
  "name": "Sample project",
  "name_with_namespace": "Administrator / sample-project",
  "path": "sample-project",
  "path_with_namespace": "root/sample-project",
  "created_at": "2018-02-13T09:05:58.023Z",
  "import_status": "scheduled",
  "correlation_id": "mezklWso3Za",
  "failed_relations": [],
  "import_error": null
}
```

<a id="import-status"></a>

## 导入状态

获取导入的状态。

```plaintext
GET /projects/:id/import
```

| 参数  | 类型            | 是否必需 | 描述 ｜
| ---- | -------------- | ------- | ---- |
| `id` | integer/string | yes     | 经过身份验证的用户所拥有的项目的 ID 或[网址编码路径](rest/index.md#namespaced-path-encoding)。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/1/import"
```

状态可以是以下之一：

- `none`
- `scheduled`
- `failed`
- `started`
- `finished`

如果状态为 `failed`，则会在 `import_error` 中显示导入错误消息。
如果状态为 `failed`、`started` 或 `finished`，则 `failed_relations` 数组可能填充无法导入的任何关联，导入错误由以下原因导致：

- 不可恢复的错误
- 重试次数用尽。典型示例：查询超时

NOTE:
在 `failed_relations` 中的 `id` 字段代表的是故障记录，而不是对应关联。

NOTE:
`failed_relations` 数组的上限为 100。

```json
{
  "id": 1,
  "description": "Itaque perspiciatis minima aspernatur corporis consequatur.",
  "name": "Gitlab Test",
  "name_with_namespace": "Gitlab Org / Gitlab Test",
  "path": "gitlab-test",
  "path_with_namespace": "gitlab-org/gitlab-test",
  "created_at": "2017-08-29T04:36:44.383Z",
  "import_status": "started",
  "import_type": "github",
  "correlation_id": "mezklWso3Za",
  "failed_relations": [
    {
      "id": 42,
      "created_at": "2020-04-02T14:48:59.526Z",
      "exception_class": "RuntimeError",
      "exception_message": "A failure occurred",
      "source": "custom error context",
      "relation_name": "merge_requests",
      "line_number": 0
    }
  ]
}
```

当从 GitHub 导入时，`stats` 字段会列出已从 GitHub 中提取的对象数量以及已导入的数量：

```json
{
  "id": 1,
  "description": "Itaque perspiciatis minima aspernatur corporis consequatur.",
  "name": "Gitlab Test",
  "name_with_namespace": "Gitlab Org / Gitlab Test",
  "path": "gitlab-test",
  "path_with_namespace": "gitlab-org/gitlab-test",
  "created_at": "2017-08-29T04:36:44.383Z",
  "import_status": "started",
  "import_type": "github",
  "correlation_id": "mezklWso3Za",
  "failed_relations": [
    {
      "id": 42,
      "created_at": "2020-04-02T14:48:59.526Z",
      "exception_class": "RuntimeError",
      "exception_message": "A failure occurred",
      "source": "custom error context",
      "relation_name": "merge_requests",
      "line_number": 0
    }
  ],
  "stats": {
    "fetched": {
      "diff_note": 19,
      "issue": 3,
      "label": 1,
      "note": 3,
      "pull_request": 2,
      "pull_request_merged_by": 1,
      "pull_request_review": 16
    },
    "imported": {
      "diff_note": 19,
      "issue": 3,
      "label": 1,
      "note": 3,
      "pull_request": 2,
      "pull_request_merged_by": 1,
      "pull_request_review": 16
    }
  }
}
```

## 相关主题

- [使用文件导出迁移项目](../user/project/settings/import_export.md)
- [项目导入和导出 Rake 任务](../administration/raketasks/project_import_export.md)
