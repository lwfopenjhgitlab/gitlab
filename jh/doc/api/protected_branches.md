---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 受保护的分支 API **(FREE)**

## 有效的访问级别

访问级别定义在方法 `ProtectedRefAccess.allowed_access_levels` 中。目前，这些级别是已经确认的：

```plaintext
0  => No access
30 => Developer access
40 => Maintainer access
60 => Admin access
```

<a id="list-protected-branches"></a>

## 列出受保护的分支

> 部署 key 信息引入于极狐GitLab 16.0。

从项目中获取 UI 中定义的[受保护分支](../user/project/protected_branches.md)列表。如果设置了通配符，则返回通配符，而不是与该通配符匹配的分支的具体名称。

```plaintext
GET /projects/:id/protected_branches
```

| 参数      | 类型  | 是否必需  | 描述         |
| --------- | ---- | -------- | ----------- |
| `id` | integer/string | yes | 认证用户拥有的 ID 或者[项目 URL 编码路径](rest/index.md#namespaced-path-encoding) |
| `search` | string | no | 要搜索的受保护分支的名称或部分名称 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/protected_branches"
```

响应示例：


```json
[
  {
    "id": 1,
    "name": "master",
    "push_access_levels": [
      {
        "id":  1,
        "access_level": 40,
        "access_level_description": "Maintainers"
      },
      {
        "id": 2,
        "access_level": 40,
        "access_level_description": "Deploy key",
        "deploy_key_id": 1
      }
    ],
    "merge_access_levels": [
      {
        "id":  1,
        "access_level": 40,
        "access_level_description": "Maintainers"
      }
    ],
    "allow_force_push":false,
    "code_owner_approval_required": false
  },
  {
    "id": 1,
    "name": "release/*",
    "push_access_levels": [
      {
        "id":  1,
        "access_level": 40,
        "access_level_description": "Maintainers"
      }
    ],
    "merge_access_levels": [
      {
        "id":  1,
        "access_level": 40,
        "access_level_description": "Maintainers"
      }
    ],
    "allow_force_push":false,
    "code_owner_approval_required": false
  },
  ...
]
```

专业版或旗舰版的用户还可以看到 `user_id`、`group_id` 和 `inherited` 参数。如果 `inherited` 参数存在，表示该设置是从项目的群组继承的。

响应示例：

```json
[
  {
    "id": 1,
    "name": "master",
    "push_access_levels": [
      {
        "id":  1,
        "access_level": 40,
        "user_id": null,
        "group_id": null,
        "access_level_description": "Maintainers"
      },
      {
        "id": 2,
        "access_level": 40,
        "access_level_description": "Deploy key",
        "deploy_key_id": 1,
        "user_id": null,
        "group_id": null
      }
    ],
    "merge_access_levels": [
      {
        "id":  1,
        "access_level": null,
        "user_id": null,
        "group_id": 1234,
        "access_level_description": "Example Merge Group"
      }
    ],
    "allow_force_push":false,
    "code_owner_approval_required": false,
    "inherited": true
  },
  ...
]
```

## 获取单个受保护分支或受保护分支通配符

获取单个受保护分支或通配符保护分支。

```plaintext
GET /projects/:id/protected_branches/:name
```

| 参数      | 类型  | 是否必需  | 描述         |
| --------- | ---- | -------- | ----------- |
| `id` | integer/string | yes | 认证用户拥有的 ID 或者[项目 URL 编码路径](rest/index.md#namespaced-path-encoding) |
| `name` | string | yes | 分支或通配符的名称 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/protected_branches/master"
```

响应示例：

```json
{
  "id": 1,
  "name": "master",
  "push_access_levels": [
    {
      "id":  1,
      "access_level": 40,
      "access_level_description": "Maintainers"
    }
  ],
  "merge_access_levels": [
    {
      "id":  1,
      "access_level": 40,
      "access_level_description": "Maintainers"
    }
  ],
  "allow_force_push":false,
  "code_owner_approval_required": false
}
```

专业版或旗舰版的用户还可以看到 `user_id` 和 `group_id` 参数：

响应示例：

```json
{
  "id": 1,
  "name": "master",
  "push_access_levels": [
    {
      "id":  1,
      "access_level": 40,
      "user_id": null,
      "group_id": null,
      "access_level_description": "Maintainers"
    }
  ],
  "merge_access_levels": [
    {
      "id":  1,
      "access_level": null,
      "user_id": null,
      "group_id": 1234,
      "access_level_description": "Example Merge Group"
    }
  ],
  "allow_force_push":false,
  "code_owner_approval_required": false
}
```

## 保护仓库分支

使用受保护分支通配符保护单个仓库分支或多个项目仓库分支。

```plaintext
POST /projects/:id/protected_branches
```

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/protected_branches?name=*-stable&push_access_level=30&merge_access_level=30&unprotect_access_level=40"
```

| 参数                                         | 类型  | 是否必需  | 描述         |
| -------------------------------------------- | ---- | -------- | ----------- |
| `id`                                         | integer/string | yes | 认证用户拥有的 ID 或者[项目 URL 编码路径](rest/index.md#namespaced-path-encoding) |
| `name`                                       | string         | yes | 分支或通配符的名称 |
| `push_access_level`                          | integer        | no  | 允许推送的访问级别（默认值：`40`，维护者角色） |
| `merge_access_level`                         | integer        | no  | 允许合并的访问级别（默认值：`40`，维护者角色） |
| `unprotect_access_level`                     | integer        | no  | 允许取消保护的访问级别（默认值：`40`，维护者角色） |
| `allow_force_push`                           | boolean        | no  | 允许所有具有推送权限的用户强制推送（默认：`false`） |
| `allowed_to_push` **(PREMIUM)**              | array          | no  | 允许推送的访问级别数组，每个级别由这些 Hash 格式定义：`{user_id: integer}`，`{group_id: integer}`，或者 `{access_level: integer}` |
| `allowed_to_merge` **(PREMIUM)**             | array          | no  | 允许合并的访问级别数组，每个级别由这些 Hash 格式定义：`{user_id: integer}`，`{group_id: integer}`，或者 `{access_level: integer}` |
| `allowed_to_unprotect` **(PREMIUM)**         | array          | no  | 允许取消保护的访问级别数组，每个级别由这些 Hash 格式定义：`{user_id: integer}`，`{group_id: integer}`，或者 `{access_level: integer}` |
| `code_owner_approval_required` **(PREMIUM)** | boolean        | no  | 如果分支在 [`CODEOWNERS` 文件](../user/project/code_owners.md) 中，则阻止推送到此分支。（默认值：`false`） |

响应示例：

```json
{
  "id": 1,
  "name": "*-stable",
  "push_access_levels": [
    {
      "id":  1,
      "access_level": 30,
      "access_level_description": "Developers + Maintainers"
    }
  ],
  "merge_access_levels": [
    {
      "id":  1,
      "access_level": 30,
      "access_level_description": "Developers + Maintainers"
    }
  ],
  "unprotect_access_levels": [
    {
      "id":  1,
      "access_level": 40,
      "access_level_description": "Maintainers"
    }
  ],
  "allow_force_push":false,
  "code_owner_approval_required": false
}
```

专业版或旗舰版的用户还可以看到 `user_id` 和 `group_id` 参数：

响应示例：

```json
{
  "id": 1,
  "name": "*-stable",
  "push_access_levels": [
    {
      "id":  1,
      "access_level": 30,
      "user_id": null,
      "group_id": null,
      "access_level_description": "Developers + Maintainers"
    }
  ],
  "merge_access_levels": [
    {
      "id":  1,
      "access_level": 30,
      "user_id": null,
      "group_id": null,
      "access_level_description": "Developers + Maintainers"
    }
  ],
  "unprotect_access_levels": [
    {
      "id":  1,
      "access_level": 40,
      "user_id": null,
      "group_id": null,
      "access_level_description": "Maintainers"
    }
  ],
  "allow_force_push":false,
  "code_owner_approval_required": false
}
```

### 具有用户/组级别访问权限的示例 **(PREMIUM)**

在数组 `allowed_to_push` / `allowed_to_merge` / `allowed_to_unprotect` 中的每一项都应该用这些格式定义：`{user_id: integer}`， `{group_id: integer}`，或者 `{access_level: integer}`。
每个用户都必须有权访问该项目，并且每个组都必须[共享该项目](../user/project/members/share_project_with_groups.md)。
这些访问级别允许[对受保护的分支访问进行更精细的控制](../user/project/protected_branches.md)。

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/protected_branches?name=*-stable&allowed_to_push%5B%5D%5Buser_id%5D=1"
```

响应示例：

```json
{
  "id": 1,
  "name": "*-stable",
  "push_access_levels": [
    {
      "id":  1,
      "access_level": null,
      "user_id": 1,
      "group_id": null,
      "access_level_description": "Administrator"
    }
  ],
  "merge_access_levels": [
    {
      "id":  1,
      "access_level": 40,
      "user_id": null,
      "group_id": null,
      "access_level_description": "Maintainers"
    }
  ],
  "unprotect_access_levels": [
    {
      "id":  1,
      "access_level": 40,
      "user_id": null,
      "group_id": null,
      "access_level_description": "Maintainers"
    }
  ],
  "allow_force_push":false,
  "code_owner_approval_required": false
}
```

### 允许推送和允许合并访问的示例 **(PREMIUM)**

> 在 13.9 版本中移至专业版。

请求示例：

```shell
curl --request POST \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     --header "Content-Type: application/json" \
     --data '{
      "name": "master",
      "allowed_to_push": [{"access_level": 30}],
      "allowed_to_merge": [{
          "access_level": 30
        },{
          "access_level": 40
        }
      ]}'
     "https://gitlab.example.com/api/v4/projects/5/protected_branches"
```

响应示例：

```json
{
  "id": 5,
  "name": "master",
  "push_access_levels": [
    {
      "id": 1,
      "access_level": 30,
      "access_level_description": "Developers + Maintainers",
      "user_id": null,
      "group_id": null
    }
  ],
  "merge_access_levels": [
    {
      "id": 1,
      "access_level": 30,
      "access_level_description": "Developers + Maintainers",
      "user_id": null,
      "group_id": null
    },
    {
      "id": 2,
      "access_level": 40,
      "access_level_description": "Maintainers",
      "user_id": null,
      "group_id": null
    }
  ],
  "unprotect_access_levels": [
    {
      "id": 1,
      "access_level": 40,
      "access_level_description": "Maintainers",
      "user_id": null,
      "group_id": null
    }
  ],
  "allow_force_push":false,
  "code_owner_approval_required": false
}
```

## 取消保护仓库分支

取消保护给定的受保护分支或者受保护分支通配符。

```plaintext
DELETE /projects/:id/protected_branches/:name
```

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/protected_branches/*-stable"
```

| 参数      | 类型  | 是否必需  | 描述         |
| --------- | ---- | -------- | ----------- |
| `id` | integer/string | yes | 认证用户拥有的 ID 或者[项目 URL 编码路径](rest/index.md#namespaced-path-encoding) |
| `name` | string | yes | 分支的名称 |

## 更新受保护的分支

> 引入于极狐GitLab 15.6。

更新受保护的分支。

```plaintext
PATCH /projects/:id/protected_branches/:name
```

```shell
curl --request PATCH --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/protected_branches/feature-branch?allow_force_push=true&code_owner_approval_required=true"
```

| 参数                                           | 类型             | 是否必需 | 描述                                                                                        |
|----------------------------------------------|----------------|------|-------------------------------------------------------------------------------------------|
| `id`                                         | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                   |
| `name`                                       | string         | yes  | 分支名称                                                                                      |
| `allow_force_push`                           | boolean        | no   | 启用后，可以推送到该分支的成员也可以强制推送                                                                    |
| `code_owner_approval_required` **(PREMIUM)** | boolean        | no   | 如果它与 [`CODEOWNERS` 文件](../user/project/code_owners.md)中的项目匹配，则阻止推送到该分支。默认为 `false`        |
| `allowed_to_push` **(PREMIUM)**              | array          | no   | 推送访问级别的数组，每个级别由一个哈希描述                                                                     |
| `allowed_to_merge` **(PREMIUM)**             | array          | no   | 合并访问级别的数组，每个级别由一个哈希描述                                                                     |
| `allowed_to_unprotect` **(PREMIUM)**         | array          | no   | 非受保护的访问级别的数组，每个级别由一个哈希描述  |

`allowed_to_push`、`allowed_to_merge` 和 `allowed_to_unprotect` 数组中的元素应该是 `user_id`、`group_id` 或 `access_level` 之一，形式为 `{user_id: integer}`、`{group_id: integer}` 或 `{access_level: integer}`。

要更新：

- `user_id`：确保更新后的用户有权访问该项目。您还必须在相应的哈希中传递 `access_level` 的 `id`。
- `group_id`：确保更新的群组[已共享此项目](../user/project/members/share_project_with_groups.md)。
  您还必须在相应的哈希中传递 `access_level` 的 `id`。

要删除：

- 您必须将 `_destroy` 传递为 `true`。参见以下示例：

### 示例：创建 `push_access_level` 记录

```shell
curl --header 'Content-Type: application/json' --request PATCH \
     --data '{"allowed_to_push": [{"access_level": 40}]}' \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/22034114/protected_branches/master"
```

响应示例：

```json
{
   "name": "master",
   "push_access_levels": [
      {
         "id": 12,
         "access_level": 40,
         "access_level_description": "Maintainers",
         "user_id": null,
         "group_id": null
      }
   ]
}
```

### 示例：更新 `push_access_level` 记录

```shell
curl --header 'Content-Type: application/json' --request PATCH \
     --data '{"allowed_to_push": [{"id": 12, "access_level": 0}]' \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/22034114/protected_branches/master"
```

响应示例：

```json
{
   "name": "master",
   "push_access_levels": [
      {
         "id": 12,
         "access_level": 0,
         "access_level_description": "No One",
         "user_id": null,
         "group_id": null
      }
   ]
}
```

### 示例：删除 `push_access_level` 记录

```shell
curl --header 'Content-Type: application/json' --request PATCH \
     --data '{"allowed_to_push": [{"id": 12, "_destroy": true}]}' \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/22034114/protected_branches/master"
```

响应示例：

```json
{
   "name": "master",
   "push_access_levels": []
}
```

## 相关主题

- [受保护的分支](../user/project/protected_branches.md)
- [分支](../user/project/repository/branches/index.md)
- [分支 API](branches.md)
