---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 合并请求 API **(FREE)**

> - `reference` 废弃于极狐GitLab 13.10。
> - `draft` 引入于极狐GitLab 14.0 以取代 `work_in_progress`。
> - `merged_by` 废弃于极狐GitLab 14.7。
> - `merge_user` 引入于极狐GitLab 14.7 以取代 `merged_by`。
> - `merge_status` 已废弃并替换为 `detailed_merge_status` 于极狐GitLab 15.6。
> - `with_merge_status_recheck` 变更于极狐GitLab 15.11，[功能标志](../administration/feature_flags.md)为 `restrict_merge_status_recheck`，权限不足的用户的请求将被忽略。默认禁用。
> - `approvals_before_merge` 废弃于极狐GitLab 16.0。
> - `prepared_at` 引入于极狐GitLab 16.1。

API 调用非公开信息需要进行身份验证。

## API v5 中的移除内容

`approvals_before_merge` 参数已被弃用，并计划在 API v5 中移除，使用[合并请求批准 API](merge_request_approvals.md) 代替。

<a id="list-merge-request"></a>

## 列出合并请求

获取经过身份验证的用户有权限访问的所有合并请求。
默认情况下只返回当前用户创建的合并请求。
如果需要获取所有合并请求，请使用参数 `scope=all`。

`state` 参数可用于获取带有给定状态
（`opened`、`close`、`locked` 或 `merged`）的合并请求，您也可以不针对状态进行筛选（`all`）。
需要注意的是，通过 `locked` 进行筛选通常不会得到任何返回结果，这是因为它是一个临时的过渡状态。
`page` 和 `per_page` 分页参数可用于对合并请求进行约束。

```plaintext
GET /merge_requests
GET /merge_requests?state=opened
GET /merge_requests?state=all
GET /merge_requests?milestone=release
GET /merge_requests?labels=bug,reproduced
GET /merge_requests?author_id=5
GET /merge_requests?author_username=gitlab-bot
GET /merge_requests?my_reaction_emoji=star
GET /merge_requests?scope=assigned_to_me
GET /merge_requests?search=foo&in=title
```

支持的参数：

| 参数                              | 类型 | 是否必需 | 描述                                                                                                                                                                                                   |
|---------------------------------| -------------- | -------- |------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `state`                         | string         | **{dotted-circle}** No       | 返回所有合并请求或返回处于 `opened`、`closed`、`locked` 或者 `merged` 状态的合并请求                                                                                                                                         |
| `order_by`                      | string         | **{dotted-circle}** No       | 返回根据 `created_at`、`title` 或者 `updated_at` 排序的合并请求。默认值是 `created_at`。引入于 14.8 版本                                                                                                                      |
| `sort`                          | string         | **{dotted-circle}** No       | 返回根据 `asc` 或者 `desc` 排序的合并请求。默认值是 `desc`                                                                                                                                                             |
| `milestone`                     | string         | **{dotted-circle}** No       | 返回与给定里程碑相关联的合并请求。使用 `None` 则返回没有与任何里程碑关联的合并请求。使用 `Any` 则返回与里程碑关联的合并请求                                                                                                                                |
| `view`                          | string         | **{dotted-circle}** No       | 若为 `simple` 则返回合并请求的 `iid`、URL、标题、描述和基本状态                                                                                                                                                            |
| `labels`                        | string         | **{dotted-circle}** No       | 返回匹配以逗号分隔的标记列表的合并请求。使用 `None` 则返回所有没有标记的合并请求。使用 `Any` 则返回包含至少一个标记的合并请求。预定义名称不区分大小写                                                                                                                   |
| `with_labels_details`           | boolean        | **{dotted-circle}** No       | 若为 `true` 则返回更详尽的标记信息：`:name`、`:color`、`:description`、`:description_html`、`:text_color`。默认值是 `false`                                                                                                 |
| `with_merge_status_recheck`     | boolean        | **{dotted-circle}** No       | 若为 `true` 请求时会尽可能（但不保证）异步重新计算 `merge_status` 的值。默认为 `false`。引入于 13.0 版本。在极狐GitLab 15.11 及更高版本中，启用 `restrict_merge_status_recheck` [功能标志](../administration/feature_flags.md)，以便在至少没有开发者角色的用户请求时忽略此参数 |
| `created_after`                 | datetime       | **{dotted-circle}** No       | 对于给定的时间，返回不早于该时间创建的合并请求。时间应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                    |
| `created_before`                | datetime       | **{dotted-circle}** No       | 对于给定的时间，返回不晚于该时间创建的合并请求。时间应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                    |
| `deployed_after`                | datetime       | **{dotted-circle}** No       | 对于给定的日期/时间，返回不早于该时间部署的合并请求。时间应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                 |
| `deployed_before`               | datetime       | **{dotted-circle}** No       | 对于给定的日期/时间，返回不晚于该时间部署的合并请求。时间应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                 |
| `scope`                         | string         | **{dotted-circle}** No       | 返回满足范围 `created_by_me`、`assigned_to_me` 或 `all` 的合并请求。默认值是 `created_by_me`                                                                                                                           |
| `author_id`                     | integer        | **{dotted-circle}** No       | 对于给定的用户 `id`，返回这名用户创建的合并请求。与 `author_username` 冲突。和 `scope=all` 或者 `scope=assigned_to_me` 配合使用                                                                                                       |
| `author_username`               | string         | **{dotted-circle}** No       | 对于给定的用户 `username`，返回这名用户创建的合并请求。与 `author_id` 冲突                                                                                                                                                    |
| `assignee_id`                   | integer        | **{dotted-circle}** No       | 对于给定的用户 `id`，返回分配给这名用户的合并请求。使用 `None` 则返回无指派人的合并请求。使用 `Any` 则返回存在指派人的合并请求                                                                                                                            |
| `approver_ids` **(PREMIUM)**    | integer array  | **{dotted-circle}** No       | 返回将所有拥有给定 `id` 的用户指定为单个批准人的合并请求。使用 `None` 则返回没有批准人的合并请求。使用 `Any` 则返回拥有批准人的合并请求                                                                                                                       |
| `approved`                      | string         | **{dotted-circle}** No       | 根据 `approved` 状态过滤合并请求。`yes` 仅返回批准的合并请求；`no` 仅返回未批准的合并请求。引入于 15.11。仅当开启 `mr_approved_filter` 功能标志时可用。                                                                                                |
| `approved_by_ids` **(PREMIUM)** | integer array  | **{dotted-circle}** No       | 返回被所有拥有给定 `id` 的用户批准的合并请求。最大为 5。使用 `None` 则返回未被批准的合并请求。使用 `Any` 则返回批准的合并请求                                                                                                                           |
| `reviewer_id`                   | integer        | **{dotted-circle}** No       | 返回将拥有给定用户 `id` 的用户作为[审核人](../user/project/merge_requests/getting_started.md#reviewer)的合并请求。使用 `None` 则返回没有评审人的合并请求。使用 `Any` 则返回有任意评审人的合并请求。与 `reviewer_username` 冲突                                  |
| `reviewer_username`             | string         | **{dotted-circle}** No       | 返回将拥有给定用户 `username` 的用户作为[审核人](../user/project/merge_requests/getting_started.md#reviewer)的合并请求。使用 `None` 则返回没有评审人的合并请求。使用 `Any` 则返回有任意评审人的合并请求。与 `reviewer_id` 冲突。引入于 13.8 版本                      |
| `my_reaction_emoji`             | string         | **{dotted-circle}** No       | 对于给定的 `emoji`，返回经过身份验证的用户使用该表情回应的合并请求。使用 `None` 则返回没有使用表情回应的合并请求。使用 `Any` 则返回使用至少一个表情回应的合并请求                                                                                                         |
| `source_branch`                 | string         | **{dotted-circle}** No       | 返回包含给定源分支的合并请求                                                                                                                                                                                       |
| `target_branch`                 | string         | **{dotted-circle}** No       | 返回包含给定目标分支的合并请求                                                                                                                                                                                      |
| `search`                        | string         | **{dotted-circle}** No       | 根据 `title` 和 `description` 搜索合并请求                                                                                                                                                                    |
| `in`                            | string         | **{dotted-circle}** No       | 修改 `search` 属性的范围。可以使用 `title`、`description` 或用逗号进行分隔的字符串。默认值是 `title,description`                                                                                                                   |
| `wip`                           | string         | **{dotted-circle}** No       | 根据 `wip` 状态筛选合并请求。使用 `yes` 则 *仅* 返回草稿状态的合并请求，使用 `no` 则仅返回 *非草稿* 状态的合并请求                                                                                                                              |
| `not`                           | Hash           | **{dotted-circle}** No       | 返回不匹配所提供参数的合并请求。可接受的值：`labels`、`milestone`、`author_id`、`author_username`、`assignee_id`、`assignee_username`、`reviewer_id`、`reviewer_username` 和 `my_reaction_emoji`                                   |
| `environment`                   | string         | **{dotted-circle}** No       | 返回部署到给定环境的合并请求                                                                                                                                                                                       |
| `updated_before`                | datetime       | **{dotted-circle}** No       | 对于给定的时间戳，返回不晚于该时间更新的合并请求。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                  |
| `updated_after`                 | datetime       | **{dotted-circle}** No       | 对于给定的时间戳，返回不早于该时间更新的合并请求。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                  |

```json
[
  {
    "id": 1,
    "iid": 1,
    "project_id": 3,
    "title": "test1",
    "description": "fixed login page css paddings",
    "state": "merged",
    "merged_by": { // Deprecated and will be removed in API v5, use `merge_user` instead
      "id": 87854,
      "name": "Douwe Maan",
      "username": "DouweM",
      "state": "active",
      "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
      "web_url": "https://gitlab.com/DouweM"
    },
    "merge_user": {
      "id": 87854,
      "name": "Douwe Maan",
      "username": "DouweM",
      "state": "active",
      "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
      "web_url": "https://gitlab.com/DouweM"
    },
    "merged_at": "2018-09-07T11:16:17.520Z",
    "prepared_at": "2018-09-04T11:16:17.520Z",
    "closed_by": null,
    "closed_at": null,
    "created_at": "2017-04-29T08:46:00Z",
    "updated_at": "2017-04-29T08:46:00Z",
    "target_branch": "master",
    "source_branch": "test1",
    "upvotes": 0,
    "downvotes": 0,
    "author": {
      "id": 1,
      "name": "Administrator",
      "username": "admin",
      "state": "active",
      "avatar_url": null,
      "web_url" : "https://gitlab.example.com/admin"
    },
    "assignee": {
      "id": 1,
      "name": "Administrator",
      "username": "admin",
      "state": "active",
      "avatar_url": null,
      "web_url" : "https://gitlab.example.com/admin"
    },
    "assignees": [{
      "name": "Miss Monserrate Beier",
      "username": "axel.block",
      "id": 12,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/axel.block"
    }],
    "reviewers": [{
      "id": 2,
      "name": "Sam Bauch",
      "username": "kenyatta_oconnell",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/956c92487c6f6f7616b536927e22c9a0?s=80&d=identicon",
      "web_url": "http://gitlab.example.com//kenyatta_oconnell"
    }],
    "source_project_id": 2,
    "target_project_id": 3,
    "labels": [
      "Community contribution",
      "Manage"
    ],
    "draft": false,
    "work_in_progress": false,
    "milestone": {
      "id": 5,
      "iid": 1,
      "project_id": 3,
      "title": "v2.0",
      "description": "Assumenda aut placeat expedita exercitationem labore sunt enim earum.",
      "state": "closed",
      "created_at": "2015-02-02T19:49:26.013Z",
      "updated_at": "2015-02-02T19:49:26.013Z",
      "due_date": "2018-09-22",
      "start_date": "2018-08-08",
      "web_url": "https://gitlab.example.com/my-group/my-project/milestones/1"
    },
    "merge_when_pipeline_succeeds": true,
    "merge_status": "can_be_merged",
    "detailed_merge_status": "not_open",
    "sha": "8888888888888888888888888888888888888888",
    "merge_commit_sha": null,
    "squash_commit_sha": null,
    "user_notes_count": 1,
    "discussion_locked": null,
    "should_remove_source_branch": true,
    "force_remove_source_branch": false,
    "allow_collaboration": false,
    "allow_maintainer_to_push": false,
    "web_url": "http://gitlab.example.com/my-group/my-project/merge_requests/1",
    "references": {
      "short": "!1",
      "relative": "my-group/my-project!1",
      "full": "my-group/my-project!1"
    },
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    },
    "squash": false,
    "task_completion_status":{
      "count":0,
      "completed_count":0
    }
  }
]
```

<a id="merge-requests-list-response-notes"></a>

### 合并请求列出响应说明

- 引入于 13.0 版本，列出合并请求时不会对
  `merge_status`（此属性对 `has_conflicts` 也有影响）进行主动更新，因为这可能是一项昂贵的操作。
  如果您需要这些字段的值，请在请求中将 `with_merge_status_recheck` 参数设置为 `true`。
- 有关合并请求对象字段的说明，请阅读[单个合并请求响应说明](#single-merge-request-response-notes)。

<a id="list-project-merge-requests"></a>

## 列出项目合并请求

获取给定项目下的所有合并请求。
`state` 参数可用于获取带有给定状态
（`opened`、`close`、`locked` 或 `merged`）的合并请求，您也可以不针对状态进行筛选（`all`）。
`page` 和 `per_page` 分页参数可用于对合并请求进行约束。

```plaintext
GET /projects/:id/merge_requests
GET /projects/:id/merge_requests?state=opened
GET /projects/:id/merge_requests?state=all
GET /projects/:id/merge_requests?iids[]=42&iids[]=43
GET /projects/:id/merge_requests?milestone=release
GET /projects/:id/merge_requests?labels=bug,reproduced
GET /projects/:id/merge_requests?my_reaction_emoji=star
```

`project_id` 表示合并请求所在项目的 ID。
`project_id` 总是和 `target_project_id` 相等。

如果是来自同一项目的合并请求，
`source_project_id`、`target_project_id` 和 `project_id`
是相同的。对于来源派生项目的合并请求，
`target_project_id` 和 `project_id` 是一样的
`source_project_id` 是派生项目的 ID。

支持的参数：

| 参数                              | 类型             | 是否必需 | 描述                                                                                                                                                                                                   |
|---------------------------------|----------------| -------- |------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                            | integer/string | **{check-circle}** Yes       | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                                                                                                                              |
| `state`                         | string         | **{dotted-circle}** No       | 返回所有合并请求或返回处于 `opened`、`closed`、`locked` 或者 `merged` 状态的合并请求                                                                                                                                         |
| `order_by`                      | string         | **{dotted-circle}** No       | 返回根据 `created_at`、`title` 或者 `updated_at` 排序的合并请求。默认值是 `created_at`。引入于 14.8 版本                                                                                                                      |
| `sort`                          | string         | **{dotted-circle}** No       | 返回根据 `asc` 或者 `desc` 排序的合并请求。默认值是 `desc`                                                                                                                                                             |
| `milestone`                     | string         | **{dotted-circle}** No       | 返回与给定里程碑相关联的合并请求。使用 `None` 则返回没有与任何里程碑关联的合并请求。使用 `Any` 则返回与里程碑关联的合并请求                                                                                                                                |
| `view`                          | string         | **{dotted-circle}** No       | 若为 `simple` 则返回合并请求的 `iid`、URL、标题、描述和基本状态                                                                                                                                                            |
| `labels`                        | string         | **{dotted-circle}** No       | 返回匹配以逗号分隔的标记列表的合并请求。使用 `None` 则返回所有没有标记的合并请求。使用 `Any` 则返回包含至少一个标记的合并请求。预定义名称不区分大小写                                                                                                                   |
| `with_labels_details`           | boolean        | **{dotted-circle}** No       | 若为 `true` 则返回更详尽的标记信息：`:name`、`:color`、`:description`、`:description_html`、`:text_color`。默认值是 `false`                                                                                                 |
| `with_merge_status_recheck`     | boolean        | **{dotted-circle}** No       | 若为 `true` 请求时会尽可能（但不保证）异步重新计算 `merge_status` 的值。默认为 `false`。引入于 13.0 版本。在极狐GitLab 15.11 及更高版本中，启用 `restrict_merge_status_recheck` [功能标志](../administration/feature_flags.md)，以便在至少没有开发者角色的用户请求时忽略此参数 |
| `created_after`                 | datetime       | **{dotted-circle}** No       | 对于给定的时间，返回不早于该时间创建的合并请求。时间应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                    |
| `created_before`                | datetime       | **{dotted-circle}** No       | 对于给定的时间，返回不晚于该时间创建的合并请求。时间应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                    |
| `scope`                         | string         | **{dotted-circle}** No       | 返回满足范围 `created_by_me`、`assigned_to_me` 或 `all` 的合并请求                                                                                                                                                |
| `author_id`                     | integer        | **{dotted-circle}** No       | 对于给定的用户 `id`，返回这名用户创建的合并请求。与 `author_username` 冲突                                                                                                                                                    |
| `author_username`               | string         | **{dotted-circle}** No       | 对于给定的用户 `username`，返回这名用户创建的合并请求。与 `author_id` 冲突                                                                                                                                                    |
| `assignee_id`                   | integer        | **{dotted-circle}** No       | 对于给定的用户 `id`，返回分配给这名用户的合并请求。使用 `None` 则返回无指派人的合并请求。使用 `Any` 则返回存在指派人的合并请求                                                                                                                            |
| `approver_ids` **(PREMIUM)**    | integer array  | **{dotted-circle}** No       | 返回将所有拥有给定 `id` 的用户指定为单个批准人的合并请求。使用 `None` 则返回没有批准人的合并请求。使用 `Any` 则返回拥有批准人的合并请求                                                                                                                       |
| `approved`                      | string         | **{dotted-circle}** No       | 使用 `approved` 状态过滤合并请求。`yes` 仅返回批准的合并请求；`no` 仅返回未批准的合并请求。引入于 15.11。仅当启用 `mr_approved_filter` 功能标志时可用                                                                                                 |
| `approved_by_ids` **(PREMIUM)** | integer array  | **{dotted-circle}** No       | 返回被所有拥有给定 `id` 的用户批准的合并请求。最大为 5。使用 `None` 则返回未被批准的合并请求。使用 `Any` 则返回批准的合并请求                                                                                                                           |
| `reviewer_id`                   | integer        | **{dotted-circle}** No       | 返回将拥有给定用户 `id` 的用户作为[审核人](../user/project/merge_requests/reviews/index.md)的合并请求。使用 `None` 则返回没有评审人的合并请求。使用 `Any` 则返回有任意评审人的合并请求。与 `reviewer_username` 冲突                                             |
| `reviewer_username`             | string         | **{dotted-circle}** No       | 返回将拥有给定用户 `username` 的用户作为[审核人](../user/project/merge_requests/reviews/index.md)的合并请求。使用 `None` 则返回没有评审人的合并请求。使用 `Any` 则返回有任意评审人的合并请求。与 `reviewer_id` 冲突。引入于 13.8 版本                                 |
| `my_reaction_emoji`             | string         | **{dotted-circle}** No       | 对于给定的 `emoji`，返回经过身份验证的用户使用该表情回应的合并请求。使用 `None` 则返回没有使用表情回应的合并请求。使用 `Any` 则返回使用至少一个表情回应的合并请求                                                                                                         |
| `source_branch`                 | string         | **{dotted-circle}** No       | 返回包含给定源分支的合并请求                                                                                                                                                                                       |
| `target_branch`                 | string         | **{dotted-circle}** No       | 返回包含给定目标分支的合并请求                                                                                                                                                                                      |
| `search`                        | string         | **{dotted-circle}** No       | 根据 `title` 和 `description` 搜索合并请求                                                                                                                                                                    |
| `iids[]`                        | integer array  | **{dotted-circle}** No       | 返回拥有给定 `iid` 的请求                                                                                                                                                                                     |
| `wip`                           | string         | **{dotted-circle}** No       | 根据 `wip` 状态筛选合并请求。使用 `yes` 则 *仅* 返回草稿状态的合并请求，使用 `no` 则仅返回 *非草稿* 状态的合并请求                                                                                                                              |
| `not`                           | Hash           | **{dotted-circle}** No       | 返回不匹配所提供参数的合并请求。可接受的值：`labels`、`milestone`、`author_id`、`author_username`、`assignee_id`、`assignee_username`、`reviewer_id`、`reviewer_username` 和 `my_reaction_emoji`                                   |
| `environment`                   | string         | **{dotted-circle}** No       | 返回部署到给定环境的合并请求                                                                                                                                                                                       |
| `updated_before`                | datetime       | **{dotted-circle}** No       | 对于给定的时间戳，返回不晚于该时间更新的合并请求。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                  |
| `updated_after`                 | datetime       | **{dotted-circle}** No       | 对于给定的时间戳，返回不早于该时间更新的合并请求。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                  |

```json
[
  {
    "id": 1,
    "iid": 1,
    "project_id": 3,
    "title": "test1",
    "description": "fixed login page css paddings",
    "state": "merged",
    "merged_by": { // Deprecated and will be removed in API v5, use `merge_user` instead
      "id": 87854,
      "name": "Douwe Maan",
      "username": "DouweM",
      "state": "active",
      "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
      "web_url": "https://gitlab.com/DouweM"
    },
    "merge_user": {
      "id": 87854,
      "name": "Douwe Maan",
      "username": "DouweM",
      "state": "active",
      "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
      "web_url": "https://gitlab.com/DouweM"
    },
    "merged_at": "2018-09-07T11:16:17.520Z",
    "prepared_at": "2018-09-04T11:16:17.520Z",
    "closed_by": null,
    "closed_at": null,
    "created_at": "2017-04-29T08:46:00Z",
    "updated_at": "2017-04-29T08:46:00Z",
    "target_branch": "master",
    "source_branch": "test1",
    "upvotes": 0,
    "downvotes": 0,
    "author": {
      "id": 1,
      "name": "Administrator",
      "username": "admin",
      "state": "active",
      "avatar_url": null,
      "web_url" : "https://gitlab.example.com/admin"
    },
    "assignee": {
      "id": 1,
      "name": "Administrator",
      "username": "admin",
      "state": "active",
      "avatar_url": null,
      "web_url" : "https://gitlab.example.com/admin"
    },
    "assignees": [{
      "name": "Miss Monserrate Beier",
      "username": "axel.block",
      "id": 12,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/axel.block"
    }],
    "reviewers": [{
      "id": 2,
      "name": "Sam Bauch",
      "username": "kenyatta_oconnell",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/956c92487c6f6f7616b536927e22c9a0?s=80&d=identicon",
      "web_url": "http://gitlab.example.com//kenyatta_oconnell"
    }],
    "source_project_id": 2,
    "target_project_id": 3,
    "labels": [
      "Community contribution",
      "Manage"
    ],
    "draft": false,
    "work_in_progress": false,
    "milestone": {
      "id": 5,
      "iid": 1,
      "project_id": 3,
      "title": "v2.0",
      "description": "Assumenda aut placeat expedita exercitationem labore sunt enim earum.",
      "state": "closed",
      "created_at": "2015-02-02T19:49:26.013Z",
      "updated_at": "2015-02-02T19:49:26.013Z",
      "due_date": "2018-09-22",
      "start_date": "2018-08-08",
      "web_url": "https://gitlab.example.com/my-group/my-project/milestones/1"
    },
    "merge_when_pipeline_succeeds": true,
    "merge_status": "can_be_merged",
    "detailed_merge_status": "not_open",
    "sha": "8888888888888888888888888888888888888888",
    "merge_commit_sha": null,
    "squash_commit_sha": null,
    "user_notes_count": 1,
    "discussion_locked": null,
    "should_remove_source_branch": true,
    "force_remove_source_branch": false,
    "allow_collaboration": false,
    "allow_maintainer_to_push": false,
    "web_url": "http://gitlab.example.com/my-group/my-project/merge_requests/1",
    "references": {
      "short": "!1",
      "relative": "!1",
      "full": "my-group/my-project!1"
    },
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    },
    "squash": false,
    "task_completion_status":{
      "count":0,
      "completed_count":0
    },
    "has_conflicts": false,
    "blocking_discussions_resolved": true
  }
]
```

关于响应数据，请留意[合并请求列出响应说明](#merge-requests-list-response-notes)一节。

<a id="list-group-merge-requests"></a>

## 列出群组合并请求

获取给定群组及其子群组下的所有合并请求。
`state` 参数可用于获取带有给定状态
（`opened`、`close`、`locked` 或 `merged`）的合并请求，您也可以不针对状态进行筛选（`all`）。
`page` 和 `per_page` 分页参数可用于对合并请求进行约束。

```plaintext
GET /groups/:id/merge_requests
GET /groups/:id/merge_requests?state=opened
GET /groups/:id/merge_requests?state=all
GET /groups/:id/merge_requests?milestone=release
GET /groups/:id/merge_requests?labels=bug,reproduced
GET /groups/:id/merge_requests?my_reaction_emoji=star
```

`group_id` 表示合并请求对应项目所在群组的 ID。

参数：

| 参数                              | 类型             | 是否必需 | 描述                                                                                                                                                                                                   |
|---------------------------------|----------------| -------- |------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                            | integer/string | **{check-circle}** Yes       | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                                                                                                                              |
| `state`                         | string         | **{dotted-circle}** No       | 返回所有合并请求或返回处于 `opened`、`closed`、`locked` 或者 `merged` 状态的合并请求                                                                                                                                         |
| `order_by`                      | string         | **{dotted-circle}** No       | 返回根据 `created_at`、`title` 或者 `updated_at` 排序的合并请求。默认值是 `created_at`。引入于 14.8 版本                                                                                                                      |
| `sort`                          | string         | **{dotted-circle}** No       | 返回根据 `asc` 或者 `desc` 排序的合并请求。默认值是 `desc`                                                                                                                                                             |
| `milestone`                     | string         | **{dotted-circle}** No       | 返回与给定里程碑相关联的合并请求。使用 `None` 则返回没有与任何里程碑关联的合并请求。使用 `Any` 则返回与里程碑关联的合并请求                                                                                                                                |
| `view`                          | string         | **{dotted-circle}** No       | 若为 `simple` 则返回合并请求的 `iid`、URL、标题、描述和基本状态                                                                                                                                                            |
| `labels`                        | string         | **{dotted-circle}** No       | 返回匹配以逗号分隔的标记列表的合并请求。使用 `None` 则返回所有没有标记的合并请求。使用 `Any` 则返回包含至少一个标记的合并请求。预定义名称不区分大小写                                                                                                                   |
| `with_labels_details`           | boolean        | **{dotted-circle}** No       | 若为 `true` 则返回更详尽的标记信息：`:name`、`:color`、`:description`、`:description_html`、`:text_color`。默认值是 `false`                                                                                                 |
| `with_merge_status_recheck`     | boolean        | **{dotted-circle}** No       | 若为 `true` 请求时会尽可能（但不保证）异步重新计算 `merge_status` 的值。默认为 `false`。引入于 13.0 版本。在极狐GitLab 15.11 及更高版本中，启用 `restrict_merge_status_recheck` [功能标志](../administration/feature_flags.md)，以便在至少没有开发者角色的用户请求时忽略此参数 |
| `created_after`                 | datetime       | **{dotted-circle}** No       | 对于给定的时间，返回不早于该时间创建的合并请求。时间应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                    |
| `created_before`                | datetime       | **{dotted-circle}** No       | 对于给定的时间，返回不晚于该时间创建的合并请求。时间应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                    |
| `scope`                         | string         | **{dotted-circle}** No       | 返回满足范围 `created_by_me`、`assigned_to_me` 或 `all` 的合并请求                                                                                                                                                |
| `author_id`                     | integer        | **{dotted-circle}** No       | 对于给定的用户 `id`，返回这名用户创建的合并请求。与 `author_username` 冲突                                                                                                                                                    |
| `author_username`               | string         | **{dotted-circle}** No       | 对于给定的用户 `username`，返回这名用户创建的合并请求。与 `author_id` 冲突                                                                                                                                                    |
| `assignee_id`                   | integer        | **{dotted-circle}** No       | 对于给定的用户 `id`，返回分配给这名用户的合并请求。使用 `None` 则返回无指派人的合并请求。使用 `Any` 则返回存在指派人的合并请求                                                                                                                            |
| `approver_ids` **(PREMIUM)**    | integer array  | **{dotted-circle}** No       | 返回将所有拥有给定 `id` 的用户指定为单个批准人的合并请求。使用 `None` 则返回没有批准人的合并请求。使用 `Any` 则返回拥有批准人的合并请求                                                                                                                       |
| `approved`                      | string         | **{dotted-circle}** No       | 使用 `approved` 状态过滤合并请求。`yes` 仅返回批准的合并请求；`no` 仅返回未批准的合并请求。引入于 15.11。仅当启用 `mr_approved_filter` 功能标志时可用                                                                                                 |
| `approved_by_ids` **(PREMIUM)** | integer array  | **{dotted-circle}** No       | 返回被所有拥有给定 `id` 的用户批准的合并请求。最大为 5。使用 `None` 则返回未被批准的合并请求。使用 `Any` 则返回批准的合并请求                                                                                                                           |
| `reviewer_id`                   | integer        | **{dotted-circle}** No       | 返回将拥有给定用户 `id` 的用户作为[审核人](../user/project/merge_requests/reviews/index.md)的合并请求。使用 `None` 则返回没有评审人的合并请求。使用 `Any` 则返回有任意评审人的合并请求。与 `reviewer_username` 冲突                                             |
| `reviewer_username`             | string         | **{dotted-circle}** No       | 返回将拥有给定用户 `username` 的用户作为[审核人](../user/project/merge_requests/reviews/index.md)的合并请求。使用 `None` 则返回没有评审人的合并请求。使用 `Any` 则返回有任意评审人的合并请求。与 `reviewer_id` 冲突。引入于 13.8 版本                                 |
| `my_reaction_emoji`             | string         | **{dotted-circle}** No       | 对于给定的 `emoji`，返回经过身份验证的用户使用该表情回应的合并请求。使用 `None` 则返回没有使用表情回应的合并请求。使用 `Any` 则返回使用至少一个表情回应的合并请求                                                                                                         |
| `source_branch`                 | string         | **{dotted-circle}** No       | 返回包含给定源分支的合并请求                                                                                                                                                                                       |
| `target_branch`                 | string         | **{dotted-circle}** No       | 返回包含给定目标分支的合并请求                                                                                                                                                                                      |
| `search`                        | string         | **{dotted-circle}** No       | 根据 `title` 和 `description` 搜索合并请求                                                                                                                                                                    |
| `non_archived`                  | boolean        | **{dotted-circle}** No       | 仅返回来自非归档项目的合并请求。默认为 `true`                                                                                                                                                                           |
| `not`                           | Hash           | **{dotted-circle}** No       | 返回不匹配所提供参数的合并请求。可接受的值：`labels`、`milestone`、`author_id`、`author_username`、`assignee_id`、`assignee_username`、`reviewer_id`、`reviewer_username` 和 `my_reaction_emoji`                                   |
| `approved_by_usernames`         | string array   | **{dotted-circle}** No       | 返回被所有拥有特定 `username` 的用户批准的合并请求，最多 5 个。使用 `None` 则返回未被批准的合并请求。使用 `Any` 则返回被审批的合并请求                                                                                                                   |
| `updated_before`                | datetime       | **{dotted-circle}** No       | 对于给定的时间戳，返回不晚于该时间更新的合并请求。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                  |
| `updated_after`                 | datetime       | **{dotted-circle}** No       | 对于给定的时间戳，返回不早于该时间更新的合并请求。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                                                                  |

```json
[
  {
    "id": 1,
    "iid": 1,
    "project_id": 3,
    "title": "test1",
    "description": "fixed login page css paddings",
    "state": "merged",
    "merged_by": { // Deprecated and will be removed in API v5, use `merge_user` instead
      "id": 87854,
      "name": "Douwe Maan",
      "username": "DouweM",
      "state": "active",
      "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
      "web_url": "https://gitlab.com/DouweM"
    },
    "merge_user": {
      "id": 87854,
      "name": "Douwe Maan",
      "username": "DouweM",
      "state": "active",
      "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
      "web_url": "https://gitlab.com/DouweM"
    },
    "merged_at": "2018-09-07T11:16:17.520Z",
    "prepared_at": "2018-09-04T11:16:17.520Z",
    "closed_by": null,
    "closed_at": null,
    "created_at": "2017-04-29T08:46:00Z",
    "updated_at": "2017-04-29T08:46:00Z",
    "target_branch": "master",
    "source_branch": "test1",
    "upvotes": 0,
    "downvotes": 0,
    "author": {
      "id": 1,
      "name": "Administrator",
      "username": "admin",
      "state": "active",
      "avatar_url": null,
      "web_url" : "https://gitlab.example.com/admin"
    },
    "assignee": {
      "id": 1,
      "name": "Administrator",
      "username": "admin",
      "state": "active",
      "avatar_url": null,
      "web_url" : "https://gitlab.example.com/admin"
    },
    "assignees": [{
      "name": "Miss Monserrate Beier",
      "username": "axel.block",
      "id": 12,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/axel.block"
    }],
    "reviewers": [{
      "id": 2,
      "name": "Sam Bauch",
      "username": "kenyatta_oconnell",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/956c92487c6f6f7616b536927e22c9a0?s=80&d=identicon",
      "web_url": "http://gitlab.example.com//kenyatta_oconnell"
    }],
    "source_project_id": 2,
    "target_project_id": 3,
    "labels": [
      "Community contribution",
      "Manage"
    ],
    "draft": false,
    "work_in_progress": false,
    "milestone": {
      "id": 5,
      "iid": 1,
      "project_id": 3,
      "title": "v2.0",
      "description": "Assumenda aut placeat expedita exercitationem labore sunt enim earum.",
      "state": "closed",
      "created_at": "2015-02-02T19:49:26.013Z",
      "updated_at": "2015-02-02T19:49:26.013Z",
      "due_date": "2018-10-22",
      "start_date": "2018-09-08",
      "web_url": "gitlab.example.com/my-group/my-project/milestones/1"
    },
    "merge_when_pipeline_succeeds": true,
    "merge_status": "can_be_merged",
    "detailed_merge_status": "not_open",
    "sha": "8888888888888888888888888888888888888888",
    "merge_commit_sha": null,
    "squash_commit_sha": null,
    "user_notes_count": 1,
    "discussion_locked": null,
    "should_remove_source_branch": true,
    "force_remove_source_branch": false,
    "web_url": "http://gitlab.example.com/my-group/my-project/merge_requests/1",
    "references": {
      "short": "!1",
      "relative": "my-project!1",
      "full": "my-group/my-project!1"
    },
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    },
    "squash": false,
    "task_completion_status":{
      "count":0,
      "completed_count":0
    },
    "has_conflicts": false,
    "blocking_discussions_resolved": true
  }
]
```


关于响应数据，请留意[合并请求列出响应说明](#merge-requests-list-response-notes)一节。

<a id="get-single-mr"></a>

## 获取单个合并请求

显示某一合并请求的具体信息。

**注意**: 响应中 `changes_count` 的值是一个字符串，而不是整数。
当合并请求有太多的改动要展示和存储时，我们将上限设置为 1,000。
此时改动统计返回值会是 `"1000+"`。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid
```

支持的参数：

| 参数 | 类型 | 是否必需                     | 描述 |
|----------------------------------|----------------|--------------------------|------------------------------------------------------------------------------------------------------------|
| `id`                             | integer/string | **{check-circle}** Yes   | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid`              | integer        | **{check-circle}** Yes   | 合并请求的内部 ID |
| `render_html`                    | boolean        | **{dotted-circle}** No   | 若为 `true`，则对于标题和描述均响应 HTML 渲染后的内容 |
| `include_diverged_commits_count` | boolean        | **{dotted-circle}** No   | 若为 `true`，则响应落后于目标分支的提交数量 |
| `include_rebase_in_progress`     | boolean        | **{dotted-circle}** No   | 若为 `true`，响应包括为标题和描述呈现的 HTML |

### 响应

| 参数                                | 类型      | 描述                                                                                                                                                                                                                                        |
|-----------------------------------|---------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `approvals_before_merge`          | integer | **(PREMIUM)** 合并合并请求前所需要的批准数量。要配置批准规则，请参见[合并请求批准 API](merge_request_approvals.md)。废弃于 16.0                                                                                                                                                |
| `assignee`                        | object      | 合并请求的第一分配人                                                                                                                                                                                                                                |
| `assignees`                       | array      | 合并请求的分配人                                                                                                                                                                                                                                  |
| `author`                          | object      | 创建此合并请求的用户                                                                                                                                                                                                                                |
| `blocking_discussions_resolved`   | boolean      | 显示是否在合并请求合并前需要解决所有讨论                                                                                                                                                                                                                      |
| `changes_count`                   | string     | 对合并请求所做的改动数量。创建合并请求时为空，并异步填充。请参见[新合并请求的空 API 字段](#empty-api-fields-for-new-merge-requests)                                                                                                                                                |
| `closed_at`                       | datetime    | 关闭合并请求的时间戳                                                                                                                                                                                                                                |
| `closed_by`                       | object      | 关闭此合并请求的用户                                                                                                                                                                                                                                |
| `created_at`                      | datetime    | 创建合并请求的时间戳                                                                                                                                                                                                                                |
| `description`                     | string     | 合并请求的描述。包含渲染为 HTML 用于缓存的 Markdown                                                                                                                                                                                                         |
| `detailed_merge_status`           | string     | 合并请求的详细合并状态。阅读[合并状态](#merge-status)以获得潜在值列表                                                                                                                                                                                               |
| `diff_refs`                       | object      | 此合并请求的基础 SHA、头部 SHA 和起始 SHA 的引用。对应于合并请求的最新差异版本。创建合并请求时为空，并异步填充。请参阅[新合并请求的空 API 字段](#empty-api-fields-for-new-merge-requests)                                                                                                              |
| `discussion_locked`               | boolean      | 显示对合并请求的评论是否仅对成员锁定                                                                                                                                                                                                                        |
| `downvotes`                       | integer      | 合并请求的否决票数                                                                                                                                                                                                                                 |
| `draft`                           | boolean      | 显示合并请求是否为草稿                                                                                                                                                                                                                               |
| `first_contribution`              | boolean      | 显示合并请求是否是作者的第一个贡献                                                                                                                                                                                                                         |
| `first_deployed_to_production_at` | datetime    | 第一次部署完成的时间戳                                                                                                                                                                                                                               |
| `force_remove_source_branch`      | boolean      | 表示项目设置是否会导致合并后删除源分支                                                                                                                                                                                                                       |
| `has_conflicts`                   | boolean      | 显示合并请求是否存在冲突且无法合并。取决于 `merge_status` 参数。除非 `merge_status` 是 `cannot_be_merged`，否则返回 `false`                                                                                                                                               |
| `head_pipeline`                   | object      | 在合并请求的分支 HEAD 上运行的流水线。包含比 `pipeline` 更完整的信息，应该代替它使用                                                                                                                                                                                       |
| `id`                              | integer      | 合并请求 ID                                                                                                                                                                                                                                   |
| `iid`                             | integer      | 合并请求的内部 ID                                                                                                                                                                                                                                |
| `labels`                          | array      | 合并请求的标记                                                                                                                                                                                                                                   |
| `latest_build_finished_at`        | datetime    | 合并请求的最新构建完成的时间戳                                                                                                                                                                                                                           |
| `latest_build_started_at`         | datetime    | 合并请求的最新构建开始的时间戳                                                                                                                                                                                                                           |
| `merge_commit_sha`                | string     | 合并请求提交的 SHA。在合并前返回 `null`                                                                                                                                                                                                                 |
| `merge_error`                     | string     | 合并失败时显示的错误消息。要检查可合并性，请改用 `detailed_merge_status`                                                                                                                                                                                          |
| `merge_user`                      | object      | 合并此合并请求的用户，将其设置为自动合并的用户，或者为 `null`。引入于极狐GitLab 14.7                                                                                                                                                                                       |
| `merge_status`                    | string     | 合并请求的状态。可以是 `unchecked`、`checking`、`can_be_merged`、`cannot_be_merged` 或 `cannot_be_merged_recheck`。影响 `has_conflicts` 参数。有关响应数据的重要说明，请阅读[单个合并请求响应说明](#single-merge-request-response-notes)。弃用于极狐GitLab 15.6。使用 `detailed_merge_status` 代替 |
| `merge_when_pipeline_succeeds`    | boolean      | 显示合并是否已设置为在其流水线成功时合并                                                                                                                                                                                                                      |
| `merged_at`                       | datetime    | 合并请求合并的时间戳                                                                                                                                                                                                                                |
| `merged_by`                       | object      | 合并此合并请求或将其设置为自动合并的用户。弃用于极狐GitLab 14.7，并计划在 API version 5 中移除。使用 `merge_user` 代替                                                                                                                                                           |
| `milestone`                       | object      | 合并请求的里程碑                                                                                                                                                                                                                                  |
| `pipeline`                        | object      | 在合并请求的分支 HEAD 上运行的流水线。考虑改用 `head_pipeline`，因为它包含更多信息                                                                                                                                                                                      |
| `prepared_at`                     | datetime | 准备合并请求的时间戳。仅在所有[准备步骤](#preparation-steps)完成后，此字段才会填充一次，并且如果添加更多更改，也不会更新                                                                                                                                                                   |
| `project_id`                      | integer      | 合并请求项目的 ID                                                                                                                                                                                                                                |
| `reference`                              | string     | 合并请求的内部引用。默认以缩短格式返回。弃用于极狐GitLab 12.7 中，并计划在 API version 5 中移除。使用 `references` 代替                                                                                                                                                          |
| `references`                            | object      | 合并请求的内部引用。包括 `short`、 `relative` 和 `full` 引用。`references.relative` 是相对于合并请求的群组或项目。从合并请求的项目中获取时，`relative` 和 `short` 格式是相同的。当跨群组或项目请求时，`relative` 和 `full`  格式相同                                                                           |
| `reviewers`                             | array      | 合并请求的评审者                                                                                                                                                                                                                                  |
| `sha`                             | string     | 合并请求的差异头 SHA                                                                                                                                                                                                                              |
| `should_remove_source_branch`     | boolean      | 显示合并请求的源分支是否将在合并后删除                                                                                                                                                                                                                       |
| `source_branch`                            | string     | 合并请求的源分支                                                                                                                                                                                                                                  |
| `source_project_id`               | integer      | 合并请求源项目的 ID                                                                                                                                                                                                                               |
| `squash`                              | boolean      | 显示是否启用合并时压缩                                                                                                                                                                                                                               |
| `squash_commit_sha`               | string     | 压缩提交的 SHA。合并前为空                                                                                                                                                                                                                           |
| `state`                              | string     | 合并请求的状态。可以是 `opened`、`closed`、`merged` 或 `locked`                                                                                                                                                                                         |
| `subscribed`                            | boolean      | 显示当前经过身份验证的用户是否订阅了此合并请求                                                                                                                                                                                                                   |
| `target_branch`                   | string     | 合并请求的目标分支                                                                                                                                                                                                                                 |
| `target_project_id`               | integer      | 合并请求目标项目的 ID                                                                                                                                                                                                                              |
| `task_completion_status`          | object      | 任务的完成状态                                                                                                                                                                                                                                   |
| `title`                              | string     | 合并请求的标题                                                                                                                                                                                                                                   |
| `updated_at`                      | datetime    | 更新合并请求的时间戳                                                                                                                                                                                                                                |
| `upvotes`                             | integer      | 合并请求的赞成票数                                                                                                                                                                                                                                 |
| `user`                              | object      | 请求合并请求的用户的权限                                                                                                                                                                                                                              |
| `user_notes_count`                | integer      | 合并请求的用户注释数                                                                                                                                                                                                                                |
| `web_url`                         | string     | 合并请求的 Web URL                                                                                                                                                                                                                             |
| `work_in_progress`                | boolean      | 弃用：改用 `draft`。显示合并请求是否为草稿                                                                                                                                                                                                                 |


```json
{
  "id": 155016530,
  "iid": 133,
  "project_id": 15513260,
  "title": "Manual job rules",
  "description": "",
  "state": "opened",
  "created_at": "2022-05-13T07:26:38.402Z",
  "updated_at": "2022-05-14T03:38:31.354Z",
  "merged_by": null, // Deprecated and will be removed in API v5. Use `merge_user` instead.
  "merge_user": null,
  "merged_at": null,
  "prepared_at": "2018-09-04T11:16:17.520Z",
  "closed_by": null,
  "closed_at": null,
  "target_branch": "master",
  "source_branch": "manual-job-rules",
  "user_notes_count": 0,
  "upvotes": 0,
  "downvotes": 0,
  "author": {
    "id": 4155490,
    "username": "marcel.amirault",
    "name": "Marcel Amirault",
    "state": "active",
    "avatar_url": "https://gitlab.com/uploads/-/system/user/avatar/4155490/avatar.png",
    "web_url": "https://gitlab.com/marcel.amirault"
  },
  "assignees": [],
  "assignee": null,
  "reviewers": [],
  "source_project_id": 15513260,
  "target_project_id": 15513260,
  "labels": [],
  "draft": false,
  "work_in_progress": false,
  "milestone": null,
  "merge_when_pipeline_succeeds": false,
  "merge_status": "can_be_merged",
  "detailed_merge_status": "can_be_merged",
  "sha": "e82eb4a098e32c796079ca3915e07487fc4db24c",
  "merge_commit_sha": null,
  "squash_commit_sha": null,
  "discussion_locked": null,
  "should_remove_source_branch": null,
  "force_remove_source_branch": true,
  "reference": "!133", // Deprecated. Use `references` instead.
  "references": {
    "short": "!133",
    "relative": "!133",
    "full": "marcel.amirault/test-project!133"
  },
  "web_url": "https://gitlab.com/marcel.amirault/test-project/-/merge_requests/133",
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "squash": false,
  "task_completion_status": {
    "count": 0,
    "completed_count": 0
  },
  "has_conflicts": false,
  "blocking_discussions_resolved": true,
  "approvals_before_merge": null, // deprecated, use [Merge request approvals API](merge_request_approvals.md)
  "subscribed": true,
  "changes_count": "1",
  "latest_build_started_at": "2022-05-13T09:46:50.032Z",
  "latest_build_finished_at": null,
  "first_deployed_to_production_at": null,
  "pipeline": { // Use `head_pipeline` instead.
    "id": 538317940,
    "iid": 1877,
    "project_id": 15513260,
    "sha": "1604b0c46c395822e4e9478777f8e54ac99fe5b9",
    "ref": "refs/merge-requests/133/merge",
    "status": "failed",
    "source": "merge_request_event",
    "created_at": "2022-05-13T09:46:39.560Z",
    "updated_at": "2022-05-13T09:47:20.706Z",
    "web_url": "https://gitlab.com/marcel.amirault/test-project/-/pipelines/538317940"
  },
  "head_pipeline": {
    "id": 538317940,
    "iid": 1877,
    "project_id": 15513260,
    "sha": "1604b0c46c395822e4e9478777f8e54ac99fe5b9",
    "ref": "refs/merge-requests/133/merge",
    "status": "failed",
    "source": "merge_request_event",
    "created_at": "2022-05-13T09:46:39.560Z",
    "updated_at": "2022-05-13T09:47:20.706Z",
    "web_url": "https://gitlab.com/marcel.amirault/test-project/-/pipelines/538317940",
    "before_sha": "1604b0c46c395822e4e9478777f8e54ac99fe5b9",
    "tag": false,
    "yaml_errors": null,
    "user": {
      "id": 4155490,
      "username": "marcel.amirault",
      "name": "Marcel Amirault",
      "state": "active",
      "avatar_url": "https://gitlab.com/uploads/-/system/user/avatar/4155490/avatar.png",
      "web_url": "https://gitlab.com/marcel.amirault"
    },
    "started_at": "2022-05-13T09:46:50.032Z",
    "finished_at": "2022-05-13T09:47:20.697Z",
    "committed_at": null,
    "duration": 30,
    "queued_duration": 10,
    "coverage": null,
    "detailed_status": {
      "icon": "status_failed",
      "text": "failed",
      "label": "failed",
      "group": "failed",
      "tooltip": "failed",
      "has_details": true,
      "details_path": "/marcel.amirault/test-project/-/pipelines/538317940",
      "illustration": null,
      "favicon": "/assets/ci_favicons/favicon_status_failed-41304d7f7e3828808b0c26771f0309e55296819a9beea3ea9fbf6689d9857c12.png"
    }
  },
  "diff_refs": {
    "base_sha": "1162f719d711319a2efb2a35566f3bfdadee8bab",
    "head_sha": "e82eb4a098e32c796079ca3915e07487fc4db24c",
    "start_sha": "1162f719d711319a2efb2a35566f3bfdadee8bab"
  },
  "merge_error": null,
  "first_contribution": false,
  "user": {
    "can_merge": true
  },
  "approvals_before_merge": { // Available for GitLab Premium and Ultimate tiers only
    "id": 1,
    "title": "test1",
    "approvals_before_merge": null
  },
}
```

<!--
### Single merge request response notes

- The `merge_status` field may hold one of the following values:
  - `unchecked`: This merge request has not yet been checked.
  - `checking`: This merge request is currently being checked to see if it can be merged.
  - `can_be_merged`: This merge request can be merged without conflict.
  - `cannot_be_merged`: There are merge conflicts between the source and target branches.
  - `cannot_be_merged_recheck`: Currently unchecked. Before the current changes, there were conflicts.
- The `diff_refs` in the response correspond to the latest diff version of the merge request.
- [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/29984) in GitLab 12.8, the mergeability (`merge_status`)
  of each merge request is checked asynchronously when a request is made to this endpoint. Poll this API endpoint
  to get updated status. This affects the `has_conflicts` property as it is dependent on the `merge_status`. It returns
  `false` unless `merge_status` is `cannot_be_merged`.
- `references.relative` is relative to the group or project that the merge request is being requested. When the merge
  request is fetched from its project, `relative` format would be the same as `short` format, and when requested across
  groups or projects, it is expected to be the same as `full` format.
- [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/349031) in GitLab 14.7,
  field `merge_user` can be either user who merged this merge request,
  user who set it to merge when pipeline succeeds or `null`.
  Field `merged_by` (user who merged this merge request or `null`) has been deprecated.
-->

<a id="single-merge-request-response-notes"></a>

### 单个合并请求响应说明

- 引入于 13.10 版本。当向这个端点发出请求时，会异步检查每个合并请求的可合并性（`merge_status`）。轮询此 API 端点以获取最新状态。`has_conflicts` 参数会受到影响，因为其依赖 `merge_status`。
  除非 `merge_status` 是 `cannot_be_merged`，否则都会返回 `false`。

<a id="merge-status"></a>"

### 合并状态

> - `merge_status` 字段废弃于极狐GitLab 15.6。
> - `detailed_merge_status` 字段引入于极狐GitLab 15.6。

使用 `detailed_merge_status` 代替 `merge_status` 以列出所有可能的状态。

- `detailed_merge_status` 字段可以包含以下与合并请求相关的值之一：
    - `blocked_status`：被另一个合并请求阻止。
    - `broken_status`：由于潜在的冲突，无法合并到目标分支。
    - `checking`: Git is testing if a valid merge is possible.
    - `unchecked`: Git has not yet tested if a valid merge is possible.
    - `ci_must_pass`：CI/CD 流水线必须在合并前成功。
    - `ci_still_running`：CI/CD 流水线仍在运行。
    - `discussions_not_resolved`：所有讨论必须在合并前解决。
    - `draft_status`：无法合并，因为合并请求处于草稿状态。
    - `external_status_checks`：所有状态检查必须在合并前通过。
    - `mergeable`：分支可以干净地合并到目标分支中。
    - `not_approved`：合并前需要批准。
    - `not_open`：合并请求在合并前必须是打开状态。
    - `policies_denied`：合并请求包含被拒绝的策略。

<a id="preparation-steps"></a>

### 准备步骤

仅在所有准备步骤完成后，`prepared_at` 字段才会填充一次。如果向合并请求添加更多更改，则不会更新：

- 创建差异。
- 执行 Web hook。
- 创建流水线。
- 检查可合并性。
- 链接 Git LFS 对象。
- 发送通知。

<!--
## Get single MR participants

Get a list of merge request participants.
-->

<a id="get-single-mr-participants"></a>

## 获取单个合并请求参与人

获取参与到某一合并请求的用户。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/participants
```

<!--
Parameters:
-->

支持的参数：

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 合并请求的内部 ID |

```json
[
  {
    "id": 1,
    "name": "John Doe1",
    "username": "user1",
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/c922747a93b40d1ea88262bf1aebee62?s=80&d=identicon",
    "web_url": "http://localhost/user1"
  },
  {
    "id": 2,
    "name": "John Doe2",
    "username": "user2",
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/10fc7f102be8de7657fb4d80898bbfe3?s=80&d=identicon",
    "web_url": "http://localhost/user2"
  }
]
```

## 获取单个 MR 评审人

获取合并请求评审人列表。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/reviewers
```

支持的参数：

| 参数                  | 类型             | 是否必需                   | 描述                                                            |
|---------------------|----------------|------------------------|---------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes | 合并请求的内部 ID                                                    |

```json
[
  {
    "user": {
      "id": 1,
      "name": "John Doe1",
      "username": "user1",
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/c922747a93b40d1ea88262bf1aebee62?s=80&d=identicon",
      "web_url": "http://localhost/user1"
    },
    "state": "unreviewed",
    "created_at": "2022-07-27T17:03:27.684Z"
  },
  {
    "user": {
      "id": 2,
      "name": "John Doe2",
      "username": "user2",
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/10fc7f102be8de7657fb4d80898bbfe3?s=80&d=identicon",
      "web_url": "http://localhost/user2"
    },
    "state": "reviewed",
    "created_at": "2022-07-27T17:03:27.684Z"
  }
]
```

<!--
## Get single MR commits

Get a list of merge request commits.
-->

<a id="get-single-mr-commits"></a>

## 获取合并请求提交

获取某个合并请求的所有提交信息。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/commits
```

<!--
Parameters:
-->

支持的参数：

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 合并请求的内部 ID |

```json
[
  {
    "id": "ed899a2f4b50b4370feeea94676502b42383c746",
    "short_id": "ed899a2f4b5",
    "title": "Replace sanitize with escape once",
    "author_name": "Example User",
    "author_email": "user@example.com",
    "created_at": "2012-09-20T11:50:22+03:00",
    "message": "Replace sanitize with escape once"
  },
  {
    "id": "6104942438c14ec7bd21c6cd5bd995272b3faff6",
    "short_id": "6104942438c",
    "title": "Sanitize for network graph",
    "author_name": "Example User",
    "author_email": "user@example.com",
    "created_at": "2012-09-20T09:06:12+03:00",
    "message": "Sanitize for network graph"
  }
]
```

<!--
## Get single MR changes

Shows information about the merge request including its files and changes.

[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/46190) in GitLab 13.6,
diffs associated with the set of changes have the same size limitations applied as other diffs
returned by the API or viewed via the UI. When these limits impact the results, the `overflow`
field contains a value of `true`. Diff data without these limits applied can be retrieved by
adding the `access_raw_diffs` parameter, accessing diffs not from the database but from Gitaly directly.
This approach is generally slower and more resource-intensive, but isn't subject to size limits
placed on database-backed diffs. [Limits inherent to Gitaly](../development/diffs.md#diff-limits)
still apply.
-->

<a id="get-single-merge-request-changes"></a>

## 获取合并请求改动

WARNING:
此端点废弃于极狐GitLab 15.7 并将在 API v5 中移除。使用[列出合并请求差异](#list-merge-request-diffs)端点代替。

显示合并请求的相关信息，包括改动的文件和内容。

引入于 13.6 版本，无论是 API 还是网页端界面，
文件改动差异数据都有相同的大小限制。当这一限制影响响应结果时，`overflow` 的值将为 `true`。
可以通过添加 `access_raw_diffs` 参数来获取不加限制的差异数据，此时将不再从数据库而是直接从 Gitaly 访问差异。
这种方法通常速度较慢且资源密集，但不受数据库的限制。<!--[Gitaly 本身设定的限制](../development/merge_request_concepts/diffs/index.md#diff-limits) 仍然适用。-->

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/changes
```

<!--
Parameters:
-->

参数：

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|-------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes       | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)|
| `merge_request_iid` | integer        | **{check-circle}** Yes       | 合并请求的内部 ID |
| `access_raw_diffs`  | boolean        | **{dotted-circle}** No       | 从 Gitaly 获取文件差异 |

```json
{
  "id": 21,
  "iid": 1,
  "project_id": 4,
  "title": "Blanditiis beatae suscipit hic assumenda et molestias nisi asperiores repellat et.",
  "state": "reopened",
  "created_at": "2015-02-02T19:49:39.159Z",
  "updated_at": "2015-02-02T20:08:49.959Z",
  "target_branch": "secret_token",
  "source_branch": "version-1-9",
  "upvotes": 0,
  "downvotes": 0,
  "author": {
    "name": "Chad Hamill",
    "username": "jarrett",
    "id": 5,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/b95567800f828948baf5f4160ebb2473?s=40&d=identicon",
    "web_url" : "https://gitlab.example.com/jarrett"
  },
  "assignee": {
    "name": "Administrator",
    "username": "root",
    "id": 1,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=40&d=identicon",
    "web_url" : "https://gitlab.example.com/root"
  },
  "assignees": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "reviewers": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "source_project_id": 4,
  "target_project_id": 4,
  "labels": [ ],
  "description": "Qui voluptatibus placeat ipsa alias quasi. Deleniti rem ut sint. Optio velit qui distinctio.",
  "draft": false,
  "work_in_progress": false,
  "milestone": {
    "id": 5,
    "iid": 1,
    "project_id": 4,
    "title": "v2.0",
    "description": "Assumenda aut placeat expedita exercitationem labore sunt enim earum.",
    "state": "closed",
    "created_at": "2015-02-02T19:49:26.013Z",
    "updated_at": "2015-02-02T19:49:26.013Z",
    "due_date": null
  },
  "merge_when_pipeline_succeeds": true,
  "merge_status": "can_be_merged",
  "detailed_merge_status": "can_be_merged",
  "subscribed" : true,
  "sha": "8888888888888888888888888888888888888888",
  "merge_commit_sha": null,
  "squash_commit_sha": null,
  "user_notes_count": 1,
  "changes_count": "1",
  "should_remove_source_branch": true,
  "force_remove_source_branch": false,
  "squash": false,
  "web_url": "http://gitlab.example.com/my-group/my-project/merge_requests/1",
  "references": {
    "short": "!1",
    "relative": "!1",
    "full": "my-group/my-project!1"
  },
  "discussion_locked": false,
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "task_completion_status":{
    "count":0,
    "completed_count":0
  },
  "changes": [
    {
      "old_path": "VERSION",
      "new_path": "VERSION",
      "a_mode": "100644",
      "b_mode": "100644",
      "diff": "--- a/VERSION\ +++ b/VERSION\ @@ -1 +1 @@\ -1.9.7\ +1.9.8",
      "new_file": false,
      "renamed_file": false,
      "deleted_file": false
    }
  ],
  "overflow": false
}
```

<a id="list-merge-request-diffs"></a>"

## 列出合并请求差异

列出合并请求中更改的文件的差异。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/diffs
```

Supported attributes:

| 参数                  | 类型                | 是否必需                   | 描述                                                                      |
|---------------------|-------------------|------------------------|-------------------------------------------------------------------------|
| `id`                | integer or string | **{check-circle}** Yes | 经过身份验证的用户拥有的项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer           | **{check-circle}** Yes | 合并请求的内部 ID                                                              |
| `page`              | integer           | **{dotted-circle}** no | 要返回的结果页面。默认为 1                                                         |
| `per_page`          | integer           | **{dotted-circle}** no | 每页的结果数量。默认为 20                                              |

如果成功，则返回 [`200 OK`](rest/index.md#status-codes) 和以下响应参数：

| 参数             | 类型      | 描述           |
|:---------------|:--------|:-------------|
| `old_path`     | string  | 文件的旧路径       |
| `new_path`     | string  | 文件的新路径       |
| `a_mode`       | string  | 文件的旧文件模式     |
| `b_mode`       | string  | 文件的新文件模式     |
| `diff`         | string  | 对文件所做改动的差异表示 |
| `new_file`     | boolean | 表示文件是否是新添加的  |
| `renamed_file` | boolean | 表示是否重命名了文件   |
| `deleted_file` | boolean | 表示是否移除了文件    |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/merge_requests/1/diffs?page=1&per_page=2"
```

响应示例：

```json
[
  {
    "old_path": "README",
    "new_path": "README",
    "a_mode": "100644",
    "b_mode": "100644",
    "diff": "--- a/README\ +++ b/README\ @@ -1 +1 @@\ -Title\ +README",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  },
  {
    "old_path": "VERSION",
    "new_path": "VERSION",
    "a_mode": "100644",
    "b_mode": "100644",
    "diff": "--- a/VERSION\ +++ b/VERSION\ @@ -1 +1 @@\ -1.9.7\ +1.9.8",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  }
]
```

<a id="list-mr-pipelines"></a>

## 列出合并请求流水线

列出合并请求的流水线。可以使用分页参数 `page` 和 `per_page` 限制合并请求的流水线列表。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/pipelines
```

<!--
Parameters:
-->

支持的参数：

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes     | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 合并请求的内部 ID |

```json
[
  {
    "id": 77,
    "sha": "959e04d7c7a30600c894bd3c0cd0e1ce7f42c11d",
    "ref": "master",
    "status": "success"
  }
]
```

<!--
## Create MR Pipeline

> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/31722) in GitLab 12.3.

Create a new [pipeline for a merge request](../ci/pipelines/merge_request_pipelines.md).
A pipeline created via this endpoint doesn't run a regular branch/tag pipeline.
It requires `.gitlab-ci.yml` to be configured with `only: [merge_requests]` to create jobs.

The new pipeline can be:

- A detached merge request pipeline.
- A [merged results pipeline](../ci/pipelines/merged_results_pipelines.md)
  if the [project setting is enabled](../ci/pipelines/merged_results_pipelines.md#enable-merged-results-pipelines).
-->

<a id="create-mr-pipeline"></a>

## 创建合并请求流水线

创建一个新的[用于合并请求的流水线](../ci/pipelines/merge_request_pipelines.md)。
通过此节点创建的流水线不会运行常规的分支/标签流水线。
需要将 `.gitlab-ci.yml` 配置为 `only: [merge_requests]` 来创建作业。

新的流水线可以是：

- 游离的合并请求流水线。
- [合并结果流水线](../ci/pipelines/merged_results_pipelines.md)，
  前提是[启用了相应的设置](../ci/pipelines/merged_results_pipelines.md#enable-merged-results-pipelines)。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/pipelines
```

<!--
Parameters:
-->

支持的参数：

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes     | 合并请求的内部 ID |

```json
{
  "id": 2,
  "sha": "b83d6e391c22777fca1ed3012fce84f633d7fed0",
  "ref": "refs/merge-requests/1/head",
  "status": "pending",
  "web_url": "http://localhost/user1/project1/pipelines/2",
  "before_sha": "0000000000000000000000000000000000000000",
  "tag": false,
  "yaml_errors": null,
  "user": {
    "id": 1,
    "name": "John Doe1",
    "username": "user1",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/c922747a93b40d1ea88262bf1aebee62?s=80&d=identicon",
    "web_url": "http://example.com"
  },
  "created_at": "2019-09-04T19:20:18.267Z",
  "updated_at": "2019-09-04T19:20:18.459Z",
  "started_at": null,
  "finished_at": null,
  "committed_at": null,
  "duration": null,
  "coverage": null,
  "detailed_status": {
    "icon": "status_pending",
    "text": "pending",
    "label": "pending",
    "group": "pending",
    "tooltip": "pending",
    "has_details": false,
    "details_path": "/user1/project1/pipelines/2",
    "illustration": null,
    "favicon": "/assets/ci_favicons/favicon_status_pending-5bdf338420e5221ca24353b6bff1c9367189588750632e9a871b7af09ff6a2ae.png"
  }
}
```


<a id="create-mr"></a>

## 创建合并请求

创建一个新的合并请求。

```plaintext
POST /projects/:id/merge_requests
```

| 参数                                     | 类型             | 是否必需                   | 描述                                                                                                                                                         |
|----------------------------------------|----------------|------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                                   | integer/string | **{check-circle}** Yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)                                                                                |
| `source_branch`                        | string         | **{check-circle}** Yes | 源分支                                                                                                                                                        |
| `target_branch`                        | string         | **{check-circle}** Yes | 目标分支                                                                                                                                                       |
| `title`                                | string         | **{check-circle}** Yes | 合并请求的标题                                                                                                                                                    |
| `allow_collaboration`                  | boolean        | **{dotted-circle}** No | 允许可以合并到目标分支的成员的提交                                                                                                                                          |
| `allow_maintainer_to_push`             | boolean        | **{dotted-circle}** No | `allow_collaboration` 的别名                                                                                                                                  |
| `approvals_before_merge` **(PREMIUM)** | integer        | **{dotted-circle}** No | 合并前所需的批准数量（参见以下内容）。要配置批准规则，请参见[合并请求批准 API](merge_request_approvals.md)。废弃于 16.0                                                                            |
| `assignee_id`                          | integer        | **{dotted-circle}** No | 指派人的用户 ID                                                                                                                                                  |
| `assignee_ids`                         | integer array  | **{dotted-circle}** No | 指派人的用户 ID。设置为 `0` 或提供一个空串以取消所有指派                                                                                                                           |
| `reviewer_ids`                         | integer array  | **{dotted-circle}** No | 审核人的用户 ID。设置为 `0` 或提供一个空串则不添加审核人。引入于 13.8 版本                                                                                                               |
| `description`                          | string         | **{dotted-circle}** No | 合并请求的描述。不能超过 1,048,576 个字符                                                                                                                                 |
| `target_project_id`                    | integer        | **{dotted-circle}** No | 目标项目的 ID                                                                                                                                                   |
| `labels`                               | string         | **{dotted-circle}** No | 合并请求的标记，以半角逗号间隔                                                                                                                                            |
| `milestone_id`                         | integer        | **{dotted-circle}** No | 里程碑的全局 ID                                                                                                                                                  |
| `remove_source_branch`                 | boolean        | **{dotted-circle}** No | 是否在合并请求被合并时删除源分支                                                                                                                                           |
| `squash`                               | boolean        | no                     | 表示合并请求是否设置为在合并时被压缩。[项目设置](../user/project/merge_requests/squash_and_merge.md#configure-squash-options-for-a-project)可能会覆盖此值。使用 `squash_on_merge` 来考虑项目压缩选项 |
| `squash_on_merge`                      | boolean        | no                     | 表示合并请求是否在合并时被压缩                                                                                                                                            |


```json
{
  "id": 1,
  "iid": 1,
  "project_id": 3,
  "title": "test1",
  "description": "fixed login page css paddings",
  "state": "merged",
  "created_at": "2017-04-29T08:46:00Z",
  "updated_at": "2017-04-29T08:46:00Z",
  "target_branch": "master",
  "source_branch": "test1",
  "upvotes": 0,
  "downvotes": 0,
  "author": {
    "id": 1,
    "name": "Administrator",
    "username": "admin",
    "state": "active",
    "avatar_url": null,
    "web_url" : "https://gitlab.example.com/admin"
  },
  "assignee": {
    "id": 1,
    "name": "Administrator",
    "username": "admin",
    "state": "active",
    "avatar_url": null,
    "web_url" : "https://gitlab.example.com/admin"
  },
  "source_project_id": 2,
  "target_project_id": 3,
  "labels": [
    "Community contribution",
    "Manage"
  ],
  "draft": false,
  "work_in_progress": false,
  "milestone": {
    "id": 5,
    "iid": 1,
    "project_id": 3,
    "title": "v2.0",
    "description": "Assumenda aut placeat expedita exercitationem labore sunt enim earum.",
    "state": "closed",
    "created_at": "2015-02-02T19:49:26.013Z",
    "updated_at": "2015-02-02T19:49:26.013Z",
    "due_date": "2018-09-22",
    "start_date": "2018-08-08",
    "web_url": "https://gitlab.example.com/my-group/my-project/milestones/1"
  },
  "merge_when_pipeline_succeeds": true,
  "merge_status": "can_be_merged",
  "detailed_merge_status": "not_open",
  "merge_error": null,
  "sha": "8888888888888888888888888888888888888888",
  "merge_commit_sha": null,
  "squash_commit_sha": null,
  "user_notes_count": 1,
  "discussion_locked": null,
  "should_remove_source_branch": true,
  "force_remove_source_branch": false,
  "allow_collaboration": false,
  "allow_maintainer_to_push": false,
  "web_url": "http://gitlab.example.com/my-group/my-project/merge_requests/1",
  "references": {
    "short": "!1",
    "relative": "!1",
    "full": "my-group/my-project!1"
  },
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "squash": false,
  "subscribed": false,
  "changes_count": "1",
  "merged_by": { // Deprecated and will be removed in API v5, use `merge_user` instead
    "id": 87854,
    "name": "Douwe Maan",
    "username": "DouweM",
    "state": "active",
    "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
    "web_url": "https://gitlab.com/DouweM"
  },
  "merge_user": {
    "id": 87854,
    "name": "Douwe Maan",
    "username": "DouweM",
    "state": "active",
    "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
    "web_url": "https://gitlab.com/DouweM"
  },
  "merged_at": "2018-09-07T11:16:17.520Z",
  "prepared_at": "2018-09-04T11:16:17.520Z",
  "closed_by": null,
  "closed_at": null,
  "latest_build_started_at": "2018-09-07T07:27:38.472Z",
  "latest_build_finished_at": "2018-09-07T08:07:06.012Z",
  "first_deployed_to_production_at": null,
  "pipeline": {
    "id": 29626725,
    "sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
    "ref": "patch-28",
    "status": "success",
    "web_url": "https://gitlab.example.com/my-group/my-project/pipelines/29626725"
  },
  "diff_refs": {
    "base_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00",
    "head_sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
    "start_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00"
  },
  "diverged_commits_count": 2,
  "task_completion_status":{
    "count":0,
    "completed_count":0
  }
}
```

有关合并请求对象字段的说明，请阅读[单个合并请求响应说明](#single-merge-request-response-notes)。

<a id="update-mr"></a>

## 更新合并请求

更新一个合并请求。您可以修改目标分支、标题，甚至关闭合并请求。

```plaintext
PUT /projects/:id/merge_requests/:merge_request_iid
```

| 参数                              | 类型             | 是否必需                   | 描述                                                                                                                                                         |
|---------------------------------|----------------|------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                            | integer/string | **{check-circle}** Yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)                                                                                |
| `merge_request_iid`             | integer        | **{check-circle}** Yes | 合并请求的 ID                                                                                                                                                   |
| `target_branch`                 | string         | **{dotted-circle}** No | 目标分支                                                                                                                                                       |
| `title`                         | string         | **{dotted-circle}** No | 合并请求的标题                                                                                                                                                    |
| `assignee_id`                   | integer        | **{dotted-circle}** No | 指派人的用户 ID。设置为 `0` 或提供一个空串以取消所有指派                                                                                                                           |
| `assignee_ids`                  | integer array  | **{dotted-circle}** No | 指派人的用户 ID。设置为 `0` 或提供一个空串以取消所有指派                                                                                                                           |
| `reviewer_ids`                  | integer array  | **{dotted-circle}** No | 审核人的用户 ID。设置为 `0` 或提供一个空串则不添加审核人。引入于 13.8 版本                                                                                                               |
| `milestone_id`                  | integer        | **{dotted-circle}** No | 里程碑的全局 ID。设置为 `0` 或提供一个空值以取消里程碑关联                                                                                                                          |
| `labels`                        | string         | **{dotted-circle}** No | 合并请求的标记，以半角逗号间隔。提供一个空串以删除所有标记                                                                                                                              |
| `add_labels`                    | string         | **{dotted-circle}** No | 需要给合并请求添加的标记，使用半角逗号分隔                                                                                                                                      |
| `remove_labels`                 | string         | **{dotted-circle}** No | 需要给合并请求删除的标记，使用半角逗号分隔                                                                                                                                      |
| `description`                   | string         | **{dotted-circle}** No | 合并请求的描述。不能超过 1,048,576 个字符                                                                                                                                 |
| `state_event`                   | string         | **{dotted-circle}** No | 合并请求的新状态，`close` 或者 `reopen`                                                                                                                               |
| `remove_source_branch`          | boolean        | **{dotted-circle}** No | 是否在合并请求被合并时删除源分支                                                                                                                                           |
| `squash`                        | boolean        | no                     | 表示合并请求是否设置为在合并时被压缩。[项目设置](../user/project/merge_requests/squash_and_merge.md#configure-squash-options-for-a-project)可能会覆盖此值。使用 `squash_on_merge` 来考虑项目压缩选项 |
| `squash_on_merge`               | boolean        | no                     | 表示合并请求是否在合并时被压缩                                                                                                                                            |
| `discussion_locked`             | boolean        | **{dotted-circle}** No | 标记此合并请求讨论是否被锁定。只有项目成员可以往锁定的合并请求添加、编辑或解决讨论                                                                                                                  |
| `allow_collaboration`           | boolean        | **{dotted-circle}** No | 是否允许具有目标分支合并权限的用户提交更改                                                                                                                                      |
| `allow_maintainer_to_push`      | boolean        | **{dotted-circle}** No | `allow_collaboration` 的别称                                                                                                                                  |

至少需要包含上述属性中的其中一个。

```json
{
  "id": 1,
  "iid": 1,
  "project_id": 3,
  "title": "test1",
  "description": "fixed login page css paddings",
  "state": "merged",
  "created_at": "2017-04-29T08:46:00Z",
  "updated_at": "2017-04-29T08:46:00Z",
  "target_branch": "master",
  "source_branch": "test1",
  "upvotes": 0,
  "downvotes": 0,
  "author": {
    "id": 1,
    "name": "Administrator",
    "username": "admin",
    "state": "active",
    "avatar_url": null,
    "web_url" : "https://gitlab.example.com/admin"
  },
  "assignee": {
    "id": 1,
    "name": "Administrator",
    "username": "admin",
    "state": "active",
    "avatar_url": null,
    "web_url" : "https://gitlab.example.com/admin"
  },
  "assignees": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "reviewers": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "source_project_id": 2,
  "target_project_id": 3,
  "labels": [
    "Community contribution",
    "Manage"
  ],
  "draft": false,
  "work_in_progress": false,
  "milestone": {
    "id": 5,
    "iid": 1,
    "project_id": 3,
    "title": "v2.0",
    "description": "Assumenda aut placeat expedita exercitationem labore sunt enim earum.",
    "state": "closed",
    "created_at": "2015-02-02T19:49:26.013Z",
    "updated_at": "2015-02-02T19:49:26.013Z",
    "due_date": "2018-09-22",
    "start_date": "2018-08-08",
    "web_url": "https://gitlab.example.com/my-group/my-project/milestones/1"
  },
  "merge_when_pipeline_succeeds": true,
  "merge_status": "can_be_merged",
  "detailed_merge_status": "not_open",
  "merge_error": null,
  "sha": "8888888888888888888888888888888888888888",
  "merge_commit_sha": null,
  "squash_commit_sha": null,
  "user_notes_count": 1,
  "discussion_locked": null,
  "should_remove_source_branch": true,
  "force_remove_source_branch": false,
  "allow_collaboration": false,
  "allow_maintainer_to_push": false,
  "web_url": "http://gitlab.example.com/my-group/my-project/merge_requests/1",
  "references": {
    "short": "!1",
    "relative": "!1",
    "full": "my-group/my-project!1"
  },
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "squash": false,
  "subscribed": false,
  "changes_count": "1",
  "merged_by": { // Deprecated and will be removed in API v5, use `merge_user` instead
    "id": 87854,
    "name": "Douwe Maan",
    "username": "DouweM",
    "state": "active",
    "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
    "web_url": "https://gitlab.com/DouweM"
  },
  "merge_user": {
    "id": 87854,
    "name": "Douwe Maan",
    "username": "DouweM",
    "state": "active",
    "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
    "web_url": "https://gitlab.com/DouweM"
  },
  "merged_at": "2018-09-07T11:16:17.520Z",
  "prepared_at": "2018-09-04T11:16:17.520Z",
  "closed_by": null,
  "closed_at": null,
  "latest_build_started_at": "2018-09-07T07:27:38.472Z",
  "latest_build_finished_at": "2018-09-07T08:07:06.012Z",
  "first_deployed_to_production_at": null,
  "pipeline": {
    "id": 29626725,
    "sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
    "ref": "patch-28",
    "status": "success",
    "web_url": "https://gitlab.example.com/my-group/my-project/pipelines/29626725"
  },
  "diff_refs": {
    "base_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00",
    "head_sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
    "start_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00"
  },
  "diverged_commits_count": 2,
  "task_completion_status":{
    "count":0,
    "completed_count":0
  }
}
```

有关合并请求对象字段的说明，请阅读[单个合并请求响应说明](#single-merge-request-response-notes)。

<a id="delete-a-merge-request"></a>

## 删除合并请求

仅使用于管理员和项目拥有者。删除一个合并请求。

```plaintext
DELETE /projects/:id/merge_requests/:merge_request_iid
```

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 合并请求的内部 ID |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/4/merge_requests/85"
```

<!--
## Merge a merge request

Accept and merge changes submitted with MR using this API.
-->

<a id="merge-a-merge-request"></a>

## 合并合并请求

接受并使用 API 合并一个合并请求。

```plaintext
PUT /projects/:id/merge_requests/:merge_request_iid/merge
```

<!--
Parameters:
-->

支持的参数：

| 参数 | 类型 | 是否必需                       | 描述                                                              |
|--------------------------------|----------------|----------------------------|-----------------------------------------------------------------|
| `id`                           | integer/string | **{check-circle}** Yes     | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid`            | integer        | **{check-circle}** Yes     | 合并请求的内部 ID                                                      |
| `merge_commit_message`         | string         | **{dotted-circle}** No     | 自定义的合并提交的提交息                                                    |
| `squash_commit_message`        | string         | **{dotted-circle}** No     | 自定义的压缩合并的提交信息                                                   |
| `squash`                       | boolean        | **{dotted-circle}** No     | 若为 `true` 则合并时所有提交将会压缩为单个提交                                     |
| `should_remove_source_branch`  | boolean        | **{dotted-circle}** No     | 若为 `true` 则合并时删除源分支                                             |
| `merge_when_pipeline_succeeds` | boolean        | **{dotted-circle}** No     | 若为 `true` 则合并请求在流水线通过时自动合并                                      |
| `sha`                          | string         | **{dotted-circle}** No     | 如果提供此参数，则仅在头结点的 SHA 与提供值吻合时才执行合并                                |

```json
{
  "id": 1,
  "iid": 1,
  "project_id": 3,
  "title": "test1",
  "description": "fixed login page css paddings",
  "state": "merged",
  "created_at": "2017-04-29T08:46:00Z",
  "updated_at": "2017-04-29T08:46:00Z",
  "target_branch": "master",
  "source_branch": "test1",
  "upvotes": 0,
  "downvotes": 0,
  "author": {
    "id": 1,
    "name": "Administrator",
    "username": "admin",
    "state": "active",
    "avatar_url": null,
    "web_url" : "https://gitlab.example.com/admin"
  },
  "assignee": {
    "id": 1,
    "name": "Administrator",
    "username": "admin",
    "state": "active",
    "avatar_url": null,
    "web_url" : "https://gitlab.example.com/admin"
  },
  "assignees": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "reviewers": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "source_project_id": 2,
  "target_project_id": 3,
  "labels": [
    "Community contribution",
    "Manage"
  ],
  "draft": false,
  "work_in_progress": false,
  "milestone": {
    "id": 5,
    "iid": 1,
    "project_id": 3,
    "title": "v2.0",
    "description": "Assumenda aut placeat expedita exercitationem labore sunt enim earum.",
    "state": "closed",
    "created_at": "2015-02-02T19:49:26.013Z",
    "updated_at": "2015-02-02T19:49:26.013Z",
    "due_date": "2018-09-22",
    "start_date": "2018-08-08",
    "web_url": "https://gitlab.example.com/my-group/my-project/milestones/1"
  },
  "merge_when_pipeline_succeeds": true,
  "merge_status": "can_be_merged",
  "detailed_merge_status": "not_open",
  "merge_error": null,
  "sha": "8888888888888888888888888888888888888888",
  "merge_commit_sha": null,
  "squash_commit_sha": null,
  "user_notes_count": 1,
  "discussion_locked": null,
  "should_remove_source_branch": true,
  "force_remove_source_branch": false,
  "allow_collaboration": false,
  "allow_maintainer_to_push": false,
  "web_url": "http://gitlab.example.com/my-group/my-project/merge_requests/1",
  "references": {
    "short": "!1",
    "relative": "!1",
    "full": "my-group/my-project!1"
  },
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "squash": false,
  "subscribed": false,
  "changes_count": "1",
  "merged_by": { // Deprecated and will be removed in API v5, use `merge_user` instead
    "id": 87854,
    "name": "Douwe Maan",
    "username": "DouweM",
    "state": "active",
    "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
    "web_url": "https://gitlab.com/DouweM"
  },
  "merge_user": {
    "id": 87854,
    "name": "Douwe Maan",
    "username": "DouweM",
    "state": "active",
    "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
    "web_url": "https://gitlab.com/DouweM"
  },
  "merged_at": "2018-09-07T11:16:17.520Z",
  "prepared_at": "2018-09-04T11:16:17.520Z",
  "closed_by": null,
  "closed_at": null,
  "latest_build_started_at": "2018-09-07T07:27:38.472Z",
  "latest_build_finished_at": "2018-09-07T08:07:06.012Z",
  "first_deployed_to_production_at": null,
  "pipeline": {
    "id": 29626725,
    "sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
    "ref": "patch-28",
    "status": "success",
    "web_url": "https://gitlab.example.com/my-group/my-project/pipelines/29626725"
  },
  "diff_refs": {
    "base_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00",
    "head_sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
    "start_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00"
  },
  "diverged_commits_count": 2,
  "task_completion_status":{
    "count":0,
    "completed_count":0
  }
}
```

在操作失败的时候，API 会返回以下 HTTP 状态码：

| HTTP 状态码 | 信息 | 原因 |
|:---------|--------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------|
| `401`    | `Unauthorized`                             | 这个用户没有接受这个合并请求的权限 |
| `405`    | `Method Not Allowed`                       | 这个合并请求不能被接受 |
| `422`    | `Branch cannot be merged`                  | 合并请求无法被合并 |
| `409`    | `SHA does not match HEAD of source branch` | 提供的 `sha` 参数和分支头节点提交不符 |

<!--
For important notes on response data, read [Single merge request response notes](#single-merge-request-response-notes).
-->

有关合并请求对象字段的说明，请阅读[单个合并请求响应说明](#single-merge-request-response-notes)。

<!--
## Merge to default merge ref path

Merge the changes between the merge request source and target branches into `refs/merge-requests/:iid/merge`
ref, of the target project repository, if possible. This ref has the state the target branch would have if
a regular merge action was taken.

This is not a regular merge action given it doesn't change the merge request target branch state in any manner.
-->

<a id="merge-to-default-merge-ref-path"></a>

## 合并至默认 ref 路径

在可能的情况下将合并请求源分支和目标分支之间的改动合并到目标项目仓库的 `refs/merge-requests/:iid/merge`。
如果有定期地进行合并，此 ref 应具有跟目标分支一致的状态。

这不是常规的合并操作，因为它不会以任何方式更改合并请求目标分支的状态。

<!--
This ref (`refs/merge-requests/:iid/merge`) isn't necessarily overwritten when submitting
requests to this API, though it makes sure the ref has the latest possible state.

If the merge request has conflicts, is empty or already merged, you receive a `400` and a descriptive error message.

It returns the HEAD commit of `refs/merge-requests/:iid/merge` in the response body in case of `200`.
-->

当通过 API 提交请求时，这个 ref（`refs/merge-requests/:iid/merge`）不一定会被覆盖，
尽管我们确保 ref 具有最新的可能状态。

如果合并请求有冲突、为空或已被合并，您会收到 `400` 和一则错误消息。

如果响应的是 `200`，它会在响应正文中返回 `refs/merge-requests/:iid/merge` 的头结点提交。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/merge_ref
```

<!--
Parameters:
-->

参数：

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 合并请求的内部 ID |

```json
{
  "commit_id": "854a3a7a17acbcc0bbbea170986df1eb60435f34"
}
```

<!--
## Cancel Merge When Pipeline Succeeds

This API returns specific HTTP status codes on failure:
-->

<a id="cancel-merge-when-pipeline-succeeds"></a>

## 流水线通过时取消合并

在操作失败的时候，API 会返回下面的这些 HTTP 状态码：

| HTTP 状态码 | 信息 | 原因 |
|-------------|----------------------|--------------------------------------------------------------------|
| `401`       | `Unauthorized`       | 这个用户没有取消合并请求合并的权限 |
| `405`       | `Method Not Allowed` | 这个合并请求已经被合并或已经被关闭 |
| `406`       | `Not Acceptable`     | 这个合并请求并不会在流水线通过时被自动合并 |

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/cancel_merge_when_pipeline_succeeds
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 合并请求的内部 ID |

```json
{
  "id": 1,
  "iid": 1,
  "project_id": 3,
  "title": "test1",
  "description": "fixed login page css paddings",
  "state": "merged",
  "created_at": "2017-04-29T08:46:00Z",
  "updated_at": "2017-04-29T08:46:00Z",
  "target_branch": "master",
  "source_branch": "test1",
  "upvotes": 0,
  "downvotes": 0,
  "author": {
    "id": 1,
    "name": "Administrator",
    "username": "admin",
    "state": "active",
    "avatar_url": null,
    "web_url" : "https://gitlab.example.com/admin"
  },
  "assignee": {
    "id": 1,
    "name": "Administrator",
    "username": "admin",
    "state": "active",
    "avatar_url": null,
    "web_url" : "https://gitlab.example.com/admin"
  },
  "assignees": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "reviewers": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "source_project_id": 2,
  "target_project_id": 3,
  "labels": [
    "Community contribution",
    "Manage"
  ],
  "draft": false,
  "work_in_progress": false,
  "milestone": {
    "id": 5,
    "iid": 1,
    "project_id": 3,
    "title": "v2.0",
    "description": "Assumenda aut placeat expedita exercitationem labore sunt enim earum.",
    "state": "closed",
    "created_at": "2015-02-02T19:49:26.013Z",
    "updated_at": "2015-02-02T19:49:26.013Z",
    "due_date": "2018-09-22",
    "start_date": "2018-08-08",
    "web_url": "https://gitlab.example.com/my-group/my-project/milestones/1"
  },
  "merge_when_pipeline_succeeds": false,
  "merge_status": "can_be_merged",
  "detailed_merge_status": "not_open",
  "merge_error": null,
  "sha": "8888888888888888888888888888888888888888",
  "merge_commit_sha": null,
  "squash_commit_sha": null,
  "user_notes_count": 1,
  "discussion_locked": null,
  "should_remove_source_branch": true,
  "force_remove_source_branch": false,
  "allow_collaboration": false,
  "allow_maintainer_to_push": false,
  "web_url": "http://gitlab.example.com/my-group/my-project/merge_requests/1",
  "references": {
    "short": "!1",
    "relative": "!1",
    "full": "my-group/my-project!1"
  },
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "squash": false,
  "subscribed": false,
  "changes_count": "1",
  "merged_by": { // Deprecated and will be removed in API v5, use `merge_user` instead
    "id": 87854,
    "name": "Douwe Maan",
    "username": "DouweM",
    "state": "active",
    "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
    "web_url": "https://gitlab.com/DouweM"
  },
  "merge_user": {
    "id": 87854,
    "name": "Douwe Maan",
    "username": "DouweM",
    "state": "active",
    "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
    "web_url": "https://gitlab.com/DouweM"
  },
  "merged_at": "2018-09-07T11:16:17.520Z",
  "prepared_at": "2018-09-04T11:16:17.520Z",
  "closed_by": null,
  "closed_at": null,
  "latest_build_started_at": "2018-09-07T07:27:38.472Z",
  "latest_build_finished_at": "2018-09-07T08:07:06.012Z",
  "first_deployed_to_production_at": null,
  "pipeline": {
    "id": 29626725,
    "sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
    "ref": "patch-28",
    "status": "success",
    "web_url": "https://gitlab.example.com/my-group/my-project/pipelines/29626725"
  },
  "diff_refs": {
    "base_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00",
    "head_sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
    "start_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00"
  },
  "diverged_commits_count": 2,
  "task_completion_status":{
    "count":0,
    "completed_count":0
  }
}
```

有关合并请求对象字段的说明，请阅读[单个合并请求响应说明](#single-merge-request-response-notes)。

<!--
## Rebase a merge request

Automatically rebase the `source_branch` of the merge request against its
`target_branch`.

If you don't have permissions to push to the merge request's source branch -
you receive a `403 Forbidden` response.
-->

<a id="rebase-a-merge-request"></a>

## 合并请求变基

自动将合并请求的 `source_branch` 变基到 `target_branch` 上。

如果您无权推送到合并请求的源分支，您将收到 `403 Forbidden` 响应。

```plaintext
PUT /projects/:id/merge_requests/:merge_request_iid/rebase
```

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|--------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 合并请求的内部 ID |
| `skip_ci`           | boolean        | **{dotted-circle}** No      | 设置为 `true` 以跳过流水线创建 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/76/merge_requests/1/rebase"
```

<!--
This is an asynchronous request. The API returns a `HTTP 202 Accepted` response
if the request is enqueued successfully, with a response containing:
-->

这是一个异步请求。如果请求被成功添加到队列则会响应 `HTTP 202 Accepted`：

```json
{
  "rebase_in_progress": true
}
```

<!--
You can poll the [Get single MR](#get-single-mr) endpoint with the
`include_rebase_in_progress` parameter to check the status of the
asynchronous request.

If the rebase operation is ongoing, the response includes the following:
-->

您可以使用[获取单个合并请求](#get-single-mr)端点的 `include_rebase_in_progress` 参数
来轮询检查异步请求状态。

如果变基操作正在进行，则响应包括以下内容：

```json
{
  "rebase_in_progress": true,
  "merge_error": null
}
```

<!--
After the rebase operation has completed successfully, the response includes
the following:
-->

在变基操作成功的时候，响应内容如下：

```json
{
  "rebase_in_progress": false,
  "merge_error": null
}
```

<!--
If the rebase operation fails, the response includes the following:
-->

在变基操作成功的时候，响应内容如下：

```json
{
  "rebase_in_progress": false,
  "merge_error": "Rebase failed. Please rebase locally"
}
```

<a id="comments-on-merge-requests"></a>

## 合并请求评论

评论相关操作由[备注](notes.md)的相关接口实现。

<!--
## List issues that close on merge

Get all the issues that would be closed by merging the provided merge request.
-->

<a id="list-issues-that-close-on-merge"></a>

## 列出合并时关闭的议题

列出在一个合并请求被合并时会自动关闭的议题。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/closes_issues
```

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | yes      | 合并请求的内部 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/76/merge_requests/1/closes_issues"
```

<!--
Example response when the GitLab issue tracker is used:
-->

当使用极狐GitLab 议题管理时的示例响应信息：

```json
[
   {
      "state" : "opened",
      "description" : "Ratione dolores corrupti mollitia soluta quia.",
      "author" : {
         "state" : "active",
         "id" : 18,
         "web_url" : "https://gitlab.example.com/eileen.lowe",
         "name" : "Alexandra Bashirian",
         "avatar_url" : null,
         "username" : "eileen.lowe"
      },
      "milestone" : {
         "project_id" : 1,
         "description" : "Ducimus nam enim ex consequatur cumque ratione.",
         "state" : "closed",
         "due_date" : null,
         "iid" : 2,
         "created_at" : "2016-01-04T15:31:39.996Z",
         "title" : "v4.0",
         "id" : 17,
         "updated_at" : "2016-01-04T15:31:39.996Z"
      },
      "project_id" : 1,
      "assignee" : {
         "state" : "active",
         "id" : 1,
         "name" : "Administrator",
         "web_url" : "https://gitlab.example.com/root",
         "avatar_url" : null,
         "username" : "root"
      },
      "updated_at" : "2016-01-04T15:31:51.081Z",
      "id" : 76,
      "title" : "Consequatur vero maxime deserunt laboriosam est voluptas dolorem.",
      "created_at" : "2016-01-04T15:31:51.081Z",
      "iid" : 6,
      "labels" : [],
      "user_notes_count": 1,
      "changes_count": "1"
   }
]
```

<!--
Example response when an external issue tracker (for example, Jira) is used:
-->

当使用外部议题管理（例如 Jira）时的示例响应信息：

```json
[
   {
       "id" : "PROJECT-123",
       "title" : "Title of this issue"
   }
]
```

<!--
## Subscribe to a merge request

Subscribes the authenticated user to a merge request to receive notification. If the user is already subscribed to the merge request, the
status code `HTTP 304 Not Modified` is returned.
-->

<a id="subscribe-to-a-merge-request"></a>

## 订阅合并请求

经过身份验证的用户可以订阅合并请求以接收该议题的通知。
如果用户已经订阅了某个合并请求，将返回 `HTTP 304 Not Modified` 状态码。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/subscribe
```

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 合并请求的内部 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/17/subscribe"
```

响应示例：

```json
{
  "id": 1,
  "iid": 1,
  "project_id": 3,
  "title": "test1",
  "description": "fixed login page css paddings",
  "state": "merged",
  "created_at": "2017-04-29T08:46:00Z",
  "updated_at": "2017-04-29T08:46:00Z",
  "target_branch": "master",
  "source_branch": "test1",
  "upvotes": 0,
  "downvotes": 0,
  "author": {
    "id": 1,
    "name": "Administrator",
    "username": "admin",
    "state": "active",
    "avatar_url": null,
    "web_url" : "https://gitlab.example.com/admin"
  },
  "assignee": {
    "id": 1,
    "name": "Administrator",
    "username": "admin",
    "state": "active",
    "avatar_url": null,
    "web_url" : "https://gitlab.example.com/admin"
  },
  "assignees": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "reviewers": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "source_project_id": 2,
  "target_project_id": 3,
  "labels": [
    "Community contribution",
    "Manage"
  ],
  "draft": false,
  "work_in_progress": false,
  "milestone": {
    "id": 5,
    "iid": 1,
    "project_id": 3,
    "title": "v2.0",
    "description": "Assumenda aut placeat expedita exercitationem labore sunt enim earum.",
    "state": "closed",
    "created_at": "2015-02-02T19:49:26.013Z",
    "updated_at": "2015-02-02T19:49:26.013Z",
    "due_date": "2018-09-22",
    "start_date": "2018-08-08",
    "web_url": "https://gitlab.example.com/my-group/my-project/milestones/1"
  },
  "merge_when_pipeline_succeeds": true,
  "merge_status": "can_be_merged",
  "detailed_merge_status": "not_open",
  "sha": "8888888888888888888888888888888888888888",
  "merge_commit_sha": null,
  "squash_commit_sha": null,
  "user_notes_count": 1,
  "discussion_locked": null,
  "should_remove_source_branch": true,
  "force_remove_source_branch": false,
  "allow_collaboration": false,
  "allow_maintainer_to_push": false,
  "web_url": "http://gitlab.example.com/my-group/my-project/merge_requests/1",
  "references": {
    "short": "!1",
    "relative": "!1",
    "full": "my-group/my-project!1"
  },
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "squash": false,
  "subscribed": false,
  "changes_count": "1",
  "merged_by": { // Deprecated and will be removed in API v5, use `merge_user` instead
    "id": 87854,
    "name": "Douwe Maan",
    "username": "DouweM",
    "state": "active",
    "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
    "web_url": "https://gitlab.com/DouweM"
  },
  "merge_user": {
    "id": 87854,
    "name": "Douwe Maan",
    "username": "DouweM",
    "state": "active",
    "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
    "web_url": "https://gitlab.com/DouweM"
  },
  "merged_at": "2018-09-07T11:16:17.520Z",
  "prepared_at": "2018-09-04T11:16:17.520Z",
  "closed_by": null,
  "closed_at": null,
  "latest_build_started_at": "2018-09-07T07:27:38.472Z",
  "latest_build_finished_at": "2018-09-07T08:07:06.012Z",
  "first_deployed_to_production_at": null,
  "pipeline": {
    "id": 29626725,
    "sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
    "ref": "patch-28",
    "status": "success",
    "web_url": "https://gitlab.example.com/my-group/my-project/pipelines/29626725"
  },
  "diff_refs": {
    "base_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00",
    "head_sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
    "start_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00"
  },
  "diverged_commits_count": 2,
  "task_completion_status":{
    "count":0,
    "completed_count":0
  }
}
```

有关合并请求对象字段的说明，请阅读[单个合并请求响应说明](#single-merge-request-response-notes)。

<!--
## Unsubscribe from a merge request

Unsubscribes the authenticated user from a merge request to not receive
notifications from that merge request. If the user is
not subscribed to the merge request, the status code `HTTP 304 Not Modified` is returned.
-->

<a id="unsubscribe-from-a-merge-request"></a>

## 取消订阅合并请求

经过身份验证的用户可以取消订阅合并请求以不再接收该合并请求的通知。
如果用户并没有订阅这个合并请求，将返回 `HTTP 304 Not Modified` 状态码。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/unsubscribe
```

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 合并请求的内部 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/17/unsubscribe"
```

响应示例：

```json
{
  "id": 1,
  "iid": 1,
  "project_id": 3,
  "title": "test1",
  "description": "fixed login page css paddings",
  "state": "merged",
  "created_at": "2017-04-29T08:46:00Z",
  "updated_at": "2017-04-29T08:46:00Z",
  "target_branch": "master",
  "source_branch": "test1",
  "upvotes": 0,
  "downvotes": 0,
  "author": {
    "id": 1,
    "name": "Administrator",
    "username": "admin",
    "state": "active",
    "avatar_url": null,
    "web_url" : "https://gitlab.example.com/admin"
  },
  "assignee": {
    "id": 1,
    "name": "Administrator",
    "username": "admin",
    "state": "active",
    "avatar_url": null,
    "web_url" : "https://gitlab.example.com/admin"
  },
  "assignees": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "reviewers": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "source_project_id": 2,
  "target_project_id": 3,
  "labels": [
    "Community contribution",
    "Manage"
  ],
  "draft": false,
  "work_in_progress": false,
  "milestone": {
    "id": 5,
    "iid": 1,
    "project_id": 3,
    "title": "v2.0",
    "description": "Assumenda aut placeat expedita exercitationem labore sunt enim earum.",
    "state": "closed",
    "created_at": "2015-02-02T19:49:26.013Z",
    "updated_at": "2015-02-02T19:49:26.013Z",
    "due_date": "2018-09-22",
    "start_date": "2018-08-08",
    "web_url": "https://gitlab.example.com/my-group/my-project/milestones/1"
  },
  "merge_when_pipeline_succeeds": true,
  "merge_status": "can_be_merged",
  "detailed_merge_status": "not_open",
  "sha": "8888888888888888888888888888888888888888",
  "merge_commit_sha": null,
  "squash_commit_sha": null,
  "user_notes_count": 1,
  "discussion_locked": null,
  "should_remove_source_branch": true,
  "force_remove_source_branch": false,
  "allow_collaboration": false,
  "allow_maintainer_to_push": false,
  "web_url": "http://gitlab.example.com/my-group/my-project/merge_requests/1",
  "references": {
    "short": "!1",
    "relative": "!1",
    "full": "my-group/my-project!1"
  },
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "squash": false,
  "subscribed": false,
  "changes_count": "1",
  "merged_by": { // Deprecated and will be removed in API v5, use `merge_user` instead
    "id": 87854,
    "name": "Douwe Maan",
    "username": "DouweM",
    "state": "active",
    "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
    "web_url": "https://gitlab.com/DouweM"
  },
  "merge_user": {
    "id": 87854,
    "name": "Douwe Maan",
    "username": "DouweM",
    "state": "active",
    "avatar_url": "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
    "web_url": "https://gitlab.com/DouweM"
  },
  "merged_at": "2018-09-07T11:16:17.520Z",
  "prepared_at": "2018-09-04T11:16:17.520Z",
  "closed_by": null,
  "closed_at": null,
  "latest_build_started_at": "2018-09-07T07:27:38.472Z",
  "latest_build_finished_at": "2018-09-07T08:07:06.012Z",
  "first_deployed_to_production_at": null,
  "pipeline": {
    "id": 29626725,
    "sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
    "ref": "patch-28",
    "status": "success",
    "web_url": "https://gitlab.example.com/my-group/my-project/pipelines/29626725"
  },
  "diff_refs": {
    "base_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00",
    "head_sha": "2be7ddb704c7b6b83732fdd5b9f09d5a397b5f8f",
    "start_sha": "c380d3acebd181f13629a25d2e2acca46ffe1e00"
  },
  "diverged_commits_count": 2,
  "task_completion_status":{
    "count":0,
    "completed_count":0
  }
}
```

有关合并请求对象字段的说明，请阅读[单个合并请求响应说明](#single-merge-request-response-notes)。

<!--
## Create a to-do item

Manually creates a to-do item for the current user on a merge request.
If there already exists a to-do item for the user on that merge request,
status code `HTTP 304 Not Modified` is returned.
-->

<a id="create-a-to-do-item"></a>

## 创建待办事项

为当前用户基于某个合并请求手动创建待办事项。
如果这个合并请求已经是用户的待办事项，将返回 `HTTP 304 Not Modified` 状态码。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/todo
```

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 合并请求的内部 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/27/todo"
```

响应示例：

```json
{
  "id": 113,
  "project": {
    "id": 3,
    "name": "GitLab CI/CD",
    "name_with_namespace": "GitLab Org / GitLab CI/CD",
    "path": "gitlab-ci",
    "path_with_namespace": "gitlab-org/gitlab-ci"
  },
  "author": {
    "name": "Administrator",
    "username": "root",
    "id": 1,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/root"
  },
  "action_name": "marked",
  "target_type": "MergeRequest",
  "target": {
    "id": 27,
    "iid": 7,
    "project_id": 3,
    "title": "Et voluptas laudantium minus nihil recusandae ut accusamus earum aut non.",
    "description": "Veniam sunt nihil modi earum cumque illum delectus. Nihil ad quis distinctio quia. Autem eligendi at quibusdam repellendus.",
    "state": "merged",
    "created_at": "2016-06-17T07:48:04.330Z",
    "updated_at": "2016-07-01T11:14:15.537Z",
    "target_branch": "allow_regex_for_project_skip_ref",
    "source_branch": "backup",
    "upvotes": 0,
    "downvotes": 0,
    "author": {
      "name": "Jarret O'Keefe",
      "username": "francisca",
      "id": 14,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/a7fa515d53450023c83d62986d0658a8?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/francisca",
      "discussion_locked": false
    },
    "assignee": {
      "name": "Dr. Gabrielle Strosin",
      "username": "barrett.krajcik",
      "id": 4,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/733005fcd7e6df12d2d8580171ccb966?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/barrett.krajcik"
    },
    "assignees": [{
      "name": "Miss Monserrate Beier",
      "username": "axel.block",
      "id": 12,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/axel.block"
    }],
    "reviewers": [{
      "name": "Miss Monserrate Beier",
      "username": "axel.block",
      "id": 12,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/axel.block"
    }],
    "source_project_id": 3,
    "target_project_id": 3,
    "labels": [],
    "draft": false,
    "work_in_progress": false,
    "milestone": {
      "id": 27,
      "iid": 2,
      "project_id": 3,
      "title": "v1.0",
      "description": "Quis ea accusantium animi hic fuga assumenda.",
      "state": "active",
      "created_at": "2016-06-17T07:47:33.840Z",
      "updated_at": "2016-06-17T07:47:33.840Z",
      "due_date": null
    },
    "merge_when_pipeline_succeeds": false,
    "merge_status": "unchecked",
    "detailed_merge_status": "not_open",
    "subscribed": true,
    "sha": "8888888888888888888888888888888888888888",
    "merge_commit_sha": null,
    "squash_commit_sha": null,
    "user_notes_count": 7,
    "changes_count": "1",
    "should_remove_source_branch": true,
    "force_remove_source_branch": false,
    "squash": false,
    "web_url": "http://example.com/my-group/my-project/merge_requests/1",
    "references": {
      "short": "!1",
      "relative": "!1",
      "full": "my-group/my-project!1"
    }
  },
  "target_url": "https://gitlab.example.com/gitlab-org/gitlab-ci/merge_requests/7",
  "body": "Et voluptas laudantium minus nihil recusandae ut accusamus earum aut non.",
  "state": "pending",
  "created_at": "2016-07-01T11:14:15.530Z"
}
```

<!--
## Get MR diff versions

Get a list of merge request diff versions. For an explanation of the SHAs in the response,
read [SHAs in the API response](#shas-in-the-api-response).
-->

<a id="get-merge-request-diff-versions"></a>

## 获取合并请求差异版本

获取合并请求的一系列差异版本。有关响应中的 SHA，请阅读[API 响应中的 SHA](#shas-in-the-api-response) 一节。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/versions
```

| 参数 | 类型 | 是否必需                        | 描述 |
|---------------------|---------|-----------------------------|-------------------------------------|
| `id`                | String  | **{check-circle}** Yes      | 项目的 ID |
| `merge_request_iid` | integer | **{check-circle}** Yes      | 合并请求的内部 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/merge_requests/1/versions"
```

响应示例：

```json
[{
  "id": 110,
  "head_commit_sha": "33e2ee8579fda5bc36accc9c6fbd0b4fefda9e30",
  "base_commit_sha": "eeb57dffe83deb686a60a71c16c32f71046868fd",
  "start_commit_sha": "eeb57dffe83deb686a60a71c16c32f71046868fd",
  "created_at": "2016-07-26T14:44:48.926Z",
  "merge_request_id": 105,
  "state": "collected",
  "real_size": "1"
}, {
  "id": 108,
  "head_commit_sha": "3eed087b29835c48015768f839d76e5ea8f07a24",
  "base_commit_sha": "eeb57dffe83deb686a60a71c16c32f71046868fd",
  "start_commit_sha": "eeb57dffe83deb686a60a71c16c32f71046868fd",
  "created_at": "2016-07-25T14:21:33.028Z",
  "merge_request_id": 105,
  "state": "collected",
  "real_size": "1"
}]
```

<!--
### SHAs in the API response
-->

<a id="shas-in-the-api-response"></a>

### API 响应中的 SHA

| SHA 字段 | 用处 |
|--------------------|----------------------------------------------------------------------------------|
| `head_commit_sha`  | 源分支的 HEAD提交 |
| `base_commit_sha`  | 源分支和目标分支的 merge-base 提交 |
| `start_commit_sha` | 当这一版的变更创建时目标分支的 HEAD 提交 |

<!--
## Get a single MR diff version

Get a single merge request diff version. For an explanation of the SHAs in the response,
read [SHAs in the API response](#shas-in-the-api-response).
-->

<a id="get-a-single-mr-diff-version"></a>

## 获取单个合并请求差异版本

获取合并请求的某一个差异版本。有关响应中的 SHA，请阅读[API 响应中的 SHA](#shas-in-the-api-response) 一节。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/versions/:version_id
```

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|---------|----------|---------------------------------------|
| `id`                | String  | **{check-circle}** Yes      | 项目的 ID|
| `merge_request_iid` | integer | **{check-circle}** Yes      | 合并请求的内部 ID |
| `version_id`        | integer | **{check-circle}** Yes      | 差异版本的 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/merge_requests/1/versions/1"
```

响应示例：

```json
{
  "id": 110,
  "head_commit_sha": "33e2ee8579fda5bc36accc9c6fbd0b4fefda9e30",
  "base_commit_sha": "eeb57dffe83deb686a60a71c16c32f71046868fd",
  "start_commit_sha": "eeb57dffe83deb686a60a71c16c32f71046868fd",
  "created_at": "2016-07-26T14:44:48.926Z",
  "merge_request_id": 105,
  "state": "collected",
  "real_size": "1",
  "commits": [{
    "id": "33e2ee8579fda5bc36accc9c6fbd0b4fefda9e30",
    "short_id": "33e2ee85",
    "title": "Change year to 2018",
    "author_name": "Administrator",
    "author_email": "admin@example.com",
    "created_at": "2016-07-26T17:44:29.000+03:00",
    "message": "Change year to 2018"
  }, {
    "id": "aa24655de48b36335556ac8a3cd8bb521f977cbd",
    "short_id": "aa24655d",
    "title": "Update LICENSE",
    "author_name": "Administrator",
    "author_email": "admin@example.com",
    "created_at": "2016-07-25T17:21:53.000+03:00",
    "message": "Update LICENSE"
  }, {
    "id": "3eed087b29835c48015768f839d76e5ea8f07a24",
    "short_id": "3eed087b",
    "title": "Add license",
    "author_name": "Administrator",
    "author_email": "admin@example.com",
    "created_at": "2016-07-25T17:21:20.000+03:00",
    "message": "Add license"
  }],
  "diffs": [{
    "old_path": "LICENSE",
    "new_path": "LICENSE",
    "a_mode": "0",
    "b_mode": "100644",
    "diff": "--- /dev/null\n+++ b/LICENSE\n@@ -0,0 +1,21 @@\n+The MIT License (MIT)\n+\n+Copyright (c) 2018 Administrator\n+\n+Permission is hereby granted, free of charge, to any person obtaining a copy\n+of this software and associated documentation files (the \"Software\"), to deal\n+in the Software without restriction, including without limitation the rights\n+to use, copy, modify, merge, publish, distribute, sublicense, and/or sell\n+copies of the Software, and to permit persons to whom the Software is\n+furnished to do so, subject to the following conditions:\n+\n+The above copyright notice and this permission notice shall be included in all\n+copies or substantial portions of the Software.\n+\n+THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\n+IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\n+FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\n+AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\n+LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\n+OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\n+SOFTWARE.\n",
    "new_file": true,
    "renamed_file": false,
    "deleted_file": false
  }]
}
```

<!--
## Set a time estimate for a merge request

Sets an estimated time of work for this merge request.
-->

<a id="set-a-time-estimate-for-a-merge-request"></a>

## 为合并提交设置预计用时

为某个合并提交设置您预计需要在其所耗费的时间。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/time_estimate
```

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 合并请求的内部 ID |
| `duration`          | string         | **{check-circle}** Yes      | 预计用时，如 `3h30m` |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/93/time_estimate?duration=3h30m"
```

响应示例：

```json
{
  "human_time_estimate": "3h 30m",
  "human_total_time_spent": null,
  "time_estimate": 12600,
  "total_time_spent": 0
}
```

<!--
## Reset the time estimate for a merge request

Resets the estimated time for this merge request to 0 seconds.
-->

<a id="reset-the-time-estimate-for-a-merge-request"></a>

## 为合并请求重置预计用时

将合并请求的预计用时重置为零。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/reset_time_estimate
```

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 项目合并请求的内部 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/93/reset_time_estimate"
```

响应示例：

```json
{
  "human_time_estimate": null,
  "human_total_time_spent": null,
  "time_estimate": 0,
  "total_time_spent": 0
}
```

<!--
## Add spent time for a merge request

Adds spent time for this merge request.
-->

<a id="add-spent-time-for-a-merge-request"></a>

## 为合并请求添加已用时间

为合并请求添加您已经在其耗费的时间。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/add_spent_time
```

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|-------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 合并请求的内部 ID |
| `duration`          | string         | **{check-circle}** Yes      | 所耗费的时间，如 `3h30m` |
| `summary`           | string         | **{dotted-circle}** No       | 简要描述这段时间是如何耗费的 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/93/add_spent_time?duration=1h"
```

响应示例：

```json
{
  "human_time_estimate": null,
  "human_total_time_spent": "1h",
  "time_estimate": 0,
  "total_time_spent": 3600
}
```

<!--
## Reset spent time for a merge request

Resets the total spent time for this merge request to 0 seconds.
-->

<a id="reset-spent-time-for-a-merge-request"></a>

## 为合并请求重置已用时间

将合并请求的已用时间重置为零。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/reset_spent_time
```

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes     | 项目合并请求的内部 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/93/reset_spent_time"
```

响应示例：

```json
{
  "human_time_estimate": null,
  "human_total_time_spent": null,
  "time_estimate": 0,
  "total_time_spent": 0
}
```

<!--
## Get time tracking stats
-->

<a id="get-time-tracking-stats"></a>
## 获取工时统计信息

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/time_stats
```

| 参数 | 类型 | 是否必需 | 描述 |
|---------------------|----------------|----------|---------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | **{check-circle}** Yes      | 合并请求的内部 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/93/time_stats"
```

响应示例：

```json
{
  "human_time_estimate": "2h",
  "human_total_time_spent": "1h",
  "time_estimate": 7200,
  "total_time_spent": 3600
}
```

<!--
## Approvals

For approvals, see [Merge request approvals](merge_request_approvals.md)
-->

<a id="approvals"></a>

## 批准

请阅读[批准合并请求](merge_request_approvals.md)一文了解更多信息。

<!--
## List merge request state events

To track which state was set, who did it, and when it happened, check out
[Resource state events API](resource_state_events.md#merge-requests).
-->

<a id="list-merge-request-state-events"></a>

## 列出合并请求状态事件

要追踪合并请求当前状态、由谁设置、发生时间，可移步[资源状态事件 API](resource_state_events.md#merge-requests)。

##  故障排除

<a id="empty-api-fields-for-new-merge-requests"></a>

### 新合并请求的空 API 字段

创建合并请求时，`diff_refs` 和 `changes_count` 字段最初为空。这些字段在创建合并请求后异步填充。
<!--For more information, see the issue
[Some merge request API fields (`diff_refs`, `changes_count`) empty after MR is created](https://gitlab.com/gitlab-org/gitlab/-/issues/386562),
and the [related discussion](https://forum.gitlab.com/t/diff-refs-empty-after-mr-is-created/78975)
in the GitLab forums.-->


