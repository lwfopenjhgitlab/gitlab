---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 功能标志 API **(FREE SELF)**

这个 API 用于管理基于 Flipper 的极狐GitLab 研发中使用的功能标志<!--[极狐GitLab 研发中使用的功能标志](../development/feature_flags/index.md)-->。

所有方法都需要管理员权限。 
您需要注意的是，当前 API 仅支持布尔和时间的百分比的门值。

## 列出所有功能

列出所有的持久功能及其门值。

```plaintext
GET /features
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/features"
```

响应示例：

```json
[
  {
    "name": "experimental_feature",
    "state": "off",
    "gates": [
      {
        "key": "boolean",
        "value": false
      }
    ],
    "definition": null
  },
  {
    "name": "my_user_feature",
    "state": "on",
    "gates": [
      {
        "key": "percentage_of_actors",
        "value": 34
      }
    ],
    "definition": {
      "name": "my_user_feature",
      "introduced_by_url": "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/40880",
      "rollout_issue_url": "https://gitlab.com/gitlab-org/gitlab/-/issues/244905",
      "group": "group::ci",
      "type": "development",
      "default_enabled": false
    }
  },
  {
    "name": "new_library",
    "state": "on",
    "gates": [
      {
        "key": "boolean",
        "value": true
      }
    ],
    "definition": null
  }
]
```

## 列出所有功能定义

列出所有功能定义。

```plaintext
GET /features/definitions
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/features/definitions"
```

响应示例：

```json
[
  {
    "name": "geo_pages_deployment_replication",
    "introduced_by_url": "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68662",
    "rollout_issue_url": "https://gitlab.com/gitlab-org/gitlab/-/issues/337676",
    "milestone": "14.3",
    "log_state_changes": null,
    "type": "development",
    "group": "group::geo",
    "default_enabled": true
  }
]
```

## 设置或创建功能

设置功能的门值。如果特定名称的功能不存在，则进行创建。
值可以是布尔或整数，用以表示时间的百分比。


WARNING:
在启用仍在开发中的功能之前，您应该了解[安全和稳定性风险](../administration/feature_flags.md#risks-when-enabling-features-still-in-development)。

```plaintext
POST /features/:name
```

| 参数              | 类型             | 是否必需 | 描述                                                                                                                                             |
|-----------------|----------------|------|------------------------------------------------------------------------------------------------------------------------------------------------|
| `name`          | string         | yes  | 要创建或更新的功能的名称。                                                                                                                                  |
| `value`         | integer/string | yes  | 用于启用/禁用的 `true` 或 `false`，或用于时间的百分比的整数。                                                                                                        |
| `key`           | string         | no   | `percentage_of_actors` 或 `percentage_of_time` （默认）。                                                                                            |
| `feature_group` | string         | no   | 功能组名称                                                                                                                                          |
| `user`          | string         | no   | 极狐GitLab 用户名或以逗号分隔的多个用户名。                                                                                                                      |
| `group`         | string         | no   | 极狐GitLab 组的路径。例如：`gitlab-org`，或以逗号分隔的多个组路径。                                                                                                    |
| `namespace`     | string         | no   | 极狐GitLab 组或用户命名空间的路径。例如：`john-doe`，或以逗号分隔的多个命名空间路径。<!-- [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/353117)-->引入于极狐GitLab 15.0。 |
| `project`       | string         | no   | 项目路径。例如：`gitlab-org/gitlab-foss`，或以逗号分隔的多个项目路径。                                                                                                |
| `repository`    | string         | no   | 仓库路径。例如 `gitlab-org/gitlab-test.git`、`gitlab-org/gitlab-test.wiki.git` 或 `snippets/21.git`。使用逗号分隔多个仓库路径。                                       |
| `force`         | boolean        | no   | 跳过功能标志验证检查，例如 YAML 定义。                                                                                                                         |

您可以在单个 API 调用中为 `feature_group`、`user`、`group`、`namespace`、`project` 和 `repository` 启用或禁用功能。

```shell
curl --data "value=30" --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/features/new_library"
```

响应示例：

```json
{
  "name": "new_library",
  "state": "conditional",
  "gates": [
    {
      "key": "boolean",
      "value": false
    },
    {
      "key": "percentage_of_time",
      "value": 30
    }
  ],
  "definition": {
    "name": "my_user_feature",
    "introduced_by_url": "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/40880",
    "rollout_issue_url": "https://gitlab.com/gitlab-org/gitlab/-/issues/244905",
    "group": "group::ci",
    "type": "development",
    "default_enabled": false
  }
}
```

### 设置 Actor 上线的百分比

设置 Actor 上线的百分比。

```plaintext
POST https://gitlab.example.com/api/v4/features/my_user_feature?private_token=<your_access_token>
Content-Type: application/x-www-form-urlencoded
value=42&key=percentage_of_actors&
```

响应示例：

```json
{
  "name": "my_user_feature",
  "state": "conditional",
  "gates": [
    {
      "key": "boolean",
      "value": false
    },
    {
      "key": "percentage_of_actors",
      "value": 42
    }
  ],
  "definition": {
    "name": "my_user_feature",
    "introduced_by_url": "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/40880",
    "rollout_issue_url": "https://gitlab.com/gitlab-org/gitlab/-/issues/244905",
    "group": "group::ci",
    "type": "development",
    "default_enabled": false
  }
}
```

<!--将 `my_user_feature` 上线为 Actor 的 `42%`。-->

## 删除功能

移除功能门。不论门是否存在，响应都相同。

```plaintext
DELETE /features/:name
```
