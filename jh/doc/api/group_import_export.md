---
stage: Manage
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组导入和导出 API **(FREE)**

您可以使用群组导入和导出 API 导出群组结构并将其导入到新位置。当您将[项目导入和导出 API](project_import_export.md) 与群组导入和导出 API 一起使用时，您可以保留具有群组关系的连接，注入项目议题和群组史诗之间的连接。

群组导出包括以下信息：

- 群组里程碑
- 群组看板
- 群组标签
- 群组徽章
- 群组成员
- 群组 Wiki **(PREMIUM SELF)**
- 子群组。每个子群组包括以上所有信息

要保留导入项目的群组级别关系，您应该首先运行群组导入和导出。这样您就可以将项目导出导入到所需的群组结构中。

由于已知问题，导入的群组具有 `private` 可见性级别，除非您将它们导入到父群组中。默认情况下，如果将群组导入父群组，子群组将继承与父群组相同的可见性级别。

要保留成员列表及其对导入群组的各自权限，请查看这些群组中的用户。在导入所需的群组之前确保这些用户存在。

<!--## 先决条件

- 有关群组导入和导出 API 的先决条件信息，请参阅[通过上传导出文件迁移群组](../user/group/import/index.md#preparation)的先决条件。-->

## 计划新导出

开始一个新群组导出。

```plaintext
POST /groups/:id/export
```

| 参数 | 类型           | 是否必需 | 描述                              |
| --------- | -------------- | -------- | ---------------------------------------- |
| `id`      | integer/string | 是      | 经过身份验证的用户拥有的群组 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/export"
```

```json
{
  "message": "202 Accepted"
}
```

## 导出下载

下载完成的导出数据。

```plaintext
GET /groups/:id/export/download
```

| 参数 | 类型           | 是否必需 | 描述                              |
| --------- | -------------- | -------- | ---------------------------------------- |
| `id`      | integer/string | 是      | 经过身份验证的用户拥有的群组 ID |

```shell
group=1
token=secret
curl --request GET\
     --header "PRIVATE-TOKEN: ${token}" \
     --output download_group_${group}.tar.gz \
     "https://gitlab.example.com/api/v4/groups/${group}/export/download"
```

```shell
ls *export.tar.gz
2020-12-05_22-11-148_namespace_export.tar.gz
```

导出群组所需时间可能依赖于群组的大小。结果可能会是以下任何一种：

- 导出的内容（如果成功的话）
- 404 信息

## 导入文件

管理员可以在私有化部署的实例上设置最大导入文件的大小（默认为 `0`（无限制））。
作为管理员，您可以修改最大导入文件的大小：

- 管理员[管理中心](../user/admin_area/settings/account_and_limit_settings.md)。
- [应用程序设置 API](settings.md#change-application-settings) 中的 `max_import_size` 选项。

有关 JiHuLab.com 上最大导入文件大小的信息，请参阅[账户和限制设置](../user/jihulab_com/index.md#account-and-limit-settings)。

```plaintext
POST /groups/import
```

| 参数 | 类型           | 是否必需 | 描述                              |
| --------- | -------------- | -------- | ---------------------------------------- |
| `name` | string | 是 | 导入的群组名称 |
| `path` | string | 是 | 新群组的名称和路径 |
| `file` | string | 是 | 要上传的文件 |
| `parent_id` | integer | 否 | 导入群组的父群组 ID。如果不提供则默认为当前用的命名空间。|


从您的文件系统上传一个文件，请使用 `--form` 参数。这会让 cURL 使用 `Content-Type: multipart/form-data` 头信息来发送数据。`file=` 参数必须指向您文件系统的一个文件，并在前面加上 `@`。比如：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --form "name=imported-group" --form "path=imported-group" \
     --form "file=@/path/to/file" "https://gitlab.example.com/api/v4/groups/import"
```

## 相关主题

- [项目导入和导出 API](project_import_export.md)
