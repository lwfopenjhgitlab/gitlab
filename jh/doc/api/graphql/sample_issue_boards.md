---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 使用 GraphQL 识别议题板**(FREE)**

本页介绍了如何使用 GraphiQL explorer 识别
`gitlab-docs` 文档仓库中现有的[议题板](../../user/project/issue_board.md)。

## 设置 GraphiQL Explorer

此过程提供了一个实质性示例，您可以将其复制并粘贴到您自己的
[GraphiQL 资源管理器](https://gitlab.com/-/graphql-explorer)实例中：

1. 复制以下代码摘录：

   ```graphql
   query {
     project(fullPath: "gitlab-org/gitlab-docs") {
       name
       forksCount
       statistics {
         wikiSize
       }
       issuesEnabled
       boards {
         nodes {
           id
           name
         }
       }
     }
   }
   ```

1. 打开 [GraphiQL Explorer](https://jihulab.com/-/graphql-explorer)页面。
1. 将上面列出的 `query` 粘贴到 GraphiQL Explorer 工具的左侧窗口中。
1. 点击 **Play**，结果如下：

![GraphiQL explorer search for boards](img/sample_issue_boards_v13_2.png)

如果要查看这些议题板，请使用输出中显示的数字标识符。从屏幕截图中，第一个标识符是 `105011`。导航到以下 URL，其中包括标识符：

```markdown
https://gitlab.com/gitlab-org/gitlab-docs/-/boards/105011
```

<!--关于参数的更多内容，请参见[GraphQL API 资源](reference/index.md)。-->
