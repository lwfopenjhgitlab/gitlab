---
stage: Manage
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# GraphQL API 移除项目 **(FREE)**

与 REST API 不同，GraphQL 是无版本 API。
有时，项目必须从 GraphQL API 更新或删除。
根据我们的[移除项目的流程](index.md#deprecation-and-removal-process)，以下是已移除的项目。

## 极狐GitLab 16.0

移除于极狐GitLab 16.0 的字段。

### GraphQL 字段

| 字段名称         | GraphQL 类型                      | 废弃于    | 替代字段                     |
|--------------|---------------------------------|--------|--------------------------|
| `name`       | `PipelineSecurityReportFinding` | 15.1   | `title`                  |
| `external`   | `ReleaseAssetLink`              | 15.9   | 无                        |
| `confidence` | `PipelineSecurityReportFinding` | 15.4   | 无                        |
| `PAUSED`     | `CiRunnerStatus`                | 14.8   | `CiRunner.paused: true`  |
| `ACTIVE`     | `CiRunnerStatus`                | 14.8   | `CiRunner.paused: false` |

### GraphQL Mutations

| 参数名称 | Mutation                 | 废弃于                                                               | 替代参数                                              |
|------| --------------------     |-------------------------------------------------------------------|---------------------------------------------------|
| -    | `vulnerabilityFindingDismiss` | 15.5 | `vulnerabilityDismiss` 或 `securityFindingDismiss` |
| -    | `apiFuzzingCiConfigurationCreate` | 15.1 | `todos`                                           |
| -    | `CiCdSettingsUpdate` | 15.0      | `ProjectCiCdSettingsUpdate`                       |

## 极狐GitLab 15.0

移除于极狐GitLab 15.0 的字段。

### GraphQL Mutations

移除于极狐GitLab 15.0：

| 参数名称 | Mutation                 | 废弃于  | 目前使用                      |
|------| --------------------     |------|---------------------------|
| -    | `clusterAgentTokenDelete`| 14.7 | `clusterAgentTokenRevoke` |

### GraphQL 字段

移除于极狐GitLab 15.0：

| 参数名称 | 字段名称        | 废弃于  | 目前使用 |
|------|-------------|------|------|
| -    | `pipelines` | 14.5 | 无    |

### GraphQL 类型

| 字段名称                                       | GraphQL 类型               | 废弃于  | 目前使用                                                                                                    |
|--------------------------------------------|--------------------------|------|---------------------------------------------------------------------------------------------------------|
| `defaultMergeCommitMessageWithDescription` | `GraphQL::Types::String` | 14.5 | 无。定义您项目中的[合并提交模版](../../user/project/merge_requests/commit_templates.md)并使用 `defaultMergeCommitMessage` |

## 极狐GitLab 14.0

移除于极狐GitLab 14.0 的字段：

### GraphQL Mutations

| 参数名称          | Mutation                 | 废弃于  | 目前使用                            |
|---------------| --------------------     |------|---------------------------------|
| `updated_ids` | `todosMarkAllDone`       | 13.2 | `todos`                         |
| `updated_ids` | `todoRestoreMany`        | 13.2 | `todos`                         |
| `global_id`   | `dastScannerProfileCreate`| 13.6 | `todos`                         |
| -             | `addAwardEmoji`          | 13.2 | `awardEmojiAdd`                 |
| -             | `removeAwardEmoji`       | 13.2 | `awardEmojiRemove`              |
| -             | `toggleAwardEmoji`       | 13.2 | `ToggleAwardEmoji`              |
| -             | `runDastScan`            | 13.5 | `dastOnDemandScanCreate`        |
| -             | `dismissVulnerability`   | 13.5 | `vulnerabilityDismiss`          |
| -             | `revertVulnerabilityToDetected`   | 13.5 | `vulnerabilityRevertToDetected` |

### GraphQL 类型

| 字段名称                                        | GraphQL 类型               | 废弃于  | 目前使用              |
|---------------------------------------------|--------------------------|------|-------------------|
| `blob`                                      | `SnippetType`            | 13.3 | `blobs`           |
| `global_id`                                 | `DastScannerProfileType` | 13.6 | `blobs`           |
| `vulnerabilities_count_by_day_and_severity` | `GroupType`, `QueryType` | 13.3 | 无。由于安全原因，不再支持明文令牌 |

## 极狐GitLab 13.6

在极狐GitLab 14.0 之前，废弃的项目可以在 `XX.6` 发布中移除。

移除于极狐GitLab 13.6 的字段：

| 字段名称                 | GraphQL 类型           | 废弃于   | 目前使用                                                             |
|----------------------|----------------------|-------|------------------------------------------------------------------|
| `date`               | `Timelog`            | 12.10 | `spentAt`                                                        |
| `designs`            | `Issue`, `EpicIssue` | 12.2  | `designCollection`                                               |
| `latestPipeline`     | `Commit`             | 12.5  | `pipelines`                                                      |
| `mergeCommitMessage` | `MergeRequest`       | 11.8  | `latestMergeCommitMessage`                                       |
| `token`              | `GrafanaIntegration` | 12.7  | 无。由于安全原因，不再支持明文令牌|
