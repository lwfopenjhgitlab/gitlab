---
stage: Govern
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用 GraphQL 设置审计报告  **(FREE)**

本页介绍如何使用 GraphiQL Explorer 为特定的用户子集设置审计报告。

您可以使用 `cURL` 通过 HTTP 端点直接运行相同的查询。有关更多信息，请参阅[命令行](getting_started.md#command-line)。

[示例用户查询](#set-up-the-graphiql-explorer)在极狐GitLab 实例中通过用户名或全局 ID<!--[全局 ID](../../development/api_graphql_styleguide.md#global-ids)--> 查询用户子集。
查询包括：

- [`pageInfo`](#pageinfo)
- [`nodes`](#nodes)

<a id="pageinfo"></a>

## `pageInfo`

包含实现分页所需的数据。极狐GitLab 使用基于光标的[分页](getting_started.md#pagination)。详情请参见 GraphQL 文档中的[分页](https://graphql.org/learn/pagination/)。

<a id="nodes"></a>

## `nodes`

在 GraphQL 查询中，`nodes` 用于表示[图表上的 `nodes`](https://en.wikipedia.org/wiki/Vertex_(graph_theory)) 的集合。
在这种情况下，节点集合是 `User` 对象的集合。每一个我们都输出：

- 用户 `id`
- 代表用户项目或群组成员资格的 `membership` 片段。
输出片段用 `...memberships` 表示法表示。

极狐GitLab GraphQL API 非常广泛，可以输出各种实体的大量数据。
<!--更新最新信息，请参见[参考文档](reference/index.md)。-->

<a id="set-up-the-graphiql-explorer"></a>

## 设置 GraphiQL Explorer

此过程提供了一个实质性示例，您可以将其复制并粘贴到 GraphiQL Explorer 中。从以下位置可获取 GraphiQL Explorer：

- JiHuLab.com 用户：[https://jihulab.com/-/graphql-explorer](https://jihulab.com/-/graphql-explorer)
- 私有化部署用户：`https://gitlab.example.com/-/graphql-explorer`

1. 复制以下代码摘录：

   ```graphql
   {
     users(usernames: ["user1", "user2", "user3"]) {
       pageInfo {
         endCursor
         startCursor
         hasNextPage
       }
       nodes {
         id
         ...memberships
       }
     }
   }

   fragment membership on MemberInterface {
     createdAt
     updatedAt
     accessLevel {
       integerValue
       stringValue
     }
     createdBy {
       id
     }
   }

   fragment memberships on User {
     groupMemberships {
       nodes {
         ...membership
         group {
           id
           name
         }
       }
     }

     projectMemberships {
       nodes {
         ...membership
         project {
           id
           name
         }
       }
     }
   }
   ```

1. 打开 [GraphiQL Explorer 工具](https://jihulab.com/-/graphql-explorer)。
1. 将上面列出的 `query` 粘贴到 GraphiQL Explorer 工具的左侧窗口中。
1. 选择 **Play**，结果如下：

![GraphiQL explorer search for boards](img/user_query_example_v13_2.png)

NOTE:
[GraphQL API 返回 GlobalID，而不是标准 ID](getting_started.md#queries-and-mutations)。它还期望 GlobalID 是输入而不仅仅是一个整数。

此 GraphQL 查询返回用户已*明确*成为其成员的群组和项目。
由于 GraphiQL Explorer 使用会话令牌来授权对资源的访问，输出仅限于当前经过身份验证的用户可以访问的项目和群组。

如果您以实例管理员的身份登录，则无论所有权如何，您都可以访问所有记录。

更多关于：

- GraphQL 特定实体的信息，例如分片和接口，请访问官方的 [GraphQL 文档](https://graphql.org/learn/)。
- 单个属性的信息，请访问 GraphQL API 资源<!--[GraphQL API 资源](reference/index.md)-->。
