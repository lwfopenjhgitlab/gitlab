---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 使用 GraphQL 查询用户 **(FREE)**

本页介绍如何使用 GraphiQL Explorer 查询用户。

您可以使用 `cURL` 通过 HTTP 端点直接运行相同的查询。有关详细信息，请参阅我们从[命令行](getting_started.md#command-line)开始的指南。

[示例用户查询](#set-up-the-graphiql-explorer)在极狐GitLab 实例中使用用户名或全局 ID<!--[全局 ID](../../development/api_graphql_styleguide.md#global-ids)-->查询用户子集：
查询包括：

- [`pageInfo`](#pageinfo)
- [`nodes`](#nodes)

## `pageInfo`

包括进行分页所需的信息。极狐GitLab 使用基于光标的[分页](getting_started.md#pagination)。
详细信息请参见 GraphQL 文档中的[分页](https://graphql.org/learn/pagination/)。

## `nodes`

在 GraphQL 查询中，`nodes` 用于表示[图上的 `nodes`](https://en.wikipedia.org/wiki/Vertex_(graph_theory)) 的集合。
在这种情况下，节点集合是 `User` 对象的集合。
对于每个对象，我们输出：

- 用户 `id`
- `membership` 片段，表示用户属于项目或群组的成员资格。输出片段用 `...memberships` 符号表示

极狐GitLab GraphQL API 非常广泛，可以输出各种实体的大量数据。
<!--请参阅官方的[参考文档](reference/index.md)以获取最新信息。-->

<a id="set-up-the-graphiql-explorer"></a>

## 设置 GraphiQL Explorer

此过程提供了一个实质性示例，您可以将其复制并粘贴到 GraphiQL Explorer 中.
GraphiQL Explorer 可用于：

- [https://gitlab.com/-/graphql-explorer](https://jihulab.com/-/graphql-explorer) 上的 JiHuLab.com 用户
- `https://gitlab.example.com/-/graphql-explorer` 上的私有化部署用户

1. 复制以下代码摘录：

   ```graphql
    {
      users(usernames: ["user1", "user3", "user4"]) {
        pageInfo {
          endCursor
          startCursor
          hasNextPage
        }
        nodes {
          id
          username,
          publicEmail
          location
          webUrl
          userPermissions {
            createSnippet
          }
        }
      }
    }
   ```

1. 打开 [GraphiQL Explorer 工具](https://jihulab.com/-/graphql-explorer)。
1. 将上面列出的 `query` 粘贴到 GraphiQL Explorer 工具的左侧窗口中。
1. 选择 **Play**，结果如下：

![GraphiQL explorer search for boards](img/users_query_example_v13_8.png)

NOTE:
[GraphQL API 返回 GlobalID，而不是标准 ID。](getting_started.md#queries-and-mutations)。还期望输入为 GlobalID 而不是一个整数。

此 GraphQL 查询返回具有所列用户名的三个用户的指定信息。
由于 GraphiQL Explorer 使用会话令牌来授权访问资源，输出仅限于当前经过身份验证的用户可以访问的项目和群组。

如果您以实例管理员的身份登录，则无论所有权如何，您都可以访问所有记录。

如果您以管理员身份登录，则可以通过将 `admins: true` 参数添加到将第二行更改为以下内容的查询来仅显示匹配的管理员：

```graphql
  users(usernames: ["user1", "user3", "user4"], admins: true) {
    ...
  }
```

或者您也可以获取所有管理员：

```graphql
  users(admins: true) {
    ...
  }
```

关于以下内容的更多信息：

- GraphQL 特定实体，例如 Fragments 和接口，请参见
  [GraphQL 文档](https://graphql.org/learn/)。
<!--- Individual attributes, see the [GraphQL API Resources](reference/index.md).-->
