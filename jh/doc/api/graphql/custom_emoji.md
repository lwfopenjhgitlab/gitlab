---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 通过 GraphQL 使用自定义表情符号  **(FREE)**

> - 引入于极狐GitLab 13.6，[功能标志](../../administration/feature_flags.md)为 `custom_emoji`。默认禁用。
> - 在 JiHuLab.com 中启用于极狐GitLab 14.0。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能不可用。要使其可用，请让管理员[启用功能标志](../../administration/feature_flags.md)，功能标志名称为 `custom_emoji` 。
在 JiHuLab.com 上，此功能可用。
此功能已准备好用于生产用途。

要在评论和描述中使用自定义表情符号，您可以使用 GraphQL API 将其添加到顶级群组中。

参数：

| 参数           | 类型             | 是否必需                   | 描述                                                              |
|:-------------|:---------------|:-----------------------|:----------------------------------------------------------------|
| `group_path` | integer/string | **{check-circle}** Yes | ID 或 [URL 编码的顶级群组路径](../rest/index.md#namespaced-path-encoding) |
| `name`       | string         | **{check-circle}** Yes | 自定义表情符号的名称                                                      |
| `file`       | string         | **{check-circle}** Yes | 自定义表情符号镜像的 URL                                                  |

## 创建自定义表情符号

```graphql
mutation CreateCustomEmoji($groupPath: ID!) {
  createCustomEmoji(input: {groupPath: $groupPath, name: "party-parrot", url: "https://cultofthepartyparrot.com/parrots/hd/parrot.gif"}) {
    clientMutationId
    customEmoji {
      name
    }
    errors
  }
}
```

将自定义表情添加到群组后，成员可以像使用评论中的其他表情一样进行使用。

## 获取群组的自定义表情符号

```graphql
query GetCustomEmoji($groupPath: ID!) {
  group(fullPath: $groupPath) {
    id
    customEmoji {
      nodes {
        name
      }
    }
  }
}
```

## 设置 GraphiQL Explorer

此过程提供了一个实质性示例，您可以将其复制并粘贴到 GraphiQL Explorer。
GraphiQL Explorer 可用于：

- [https://jihulab.com/-/graphql-explorer](https://jihulab.com/-/graphql-explorer) 上的 JiHuLab.com 用户。
- `https://gitlab.example.com/-/graphql-explorer` 上的私有化部署用户。

1. 复制以下代码摘录：

  ```graphql
      query GetCustomEmoji {
        group(fullPath: "gitlab-org") {
          id
          customEmoji {
            nodes {
              name,
              url
            }
          }
        }
      }
  ```

2. 打开 [GraphiQL Explorer 工具](https://jihulab.com/-/graphql-explorer)。 
3. 将上面列出的 `query` 粘贴到 GraphiQL Explorer 工具的左侧窗口中。
4. 选择 **Play**，显示以下内容：

![GraphiQL explore custom emoji query](img/custom_emoji_query_example.png)

更多关于：

- GraphQL 特定实体的信息，例如分片和接口，请参见官方的 [GraphQL 文档](https://graphql.org/learn/)。
- 个别属性，请参见 GraphQL API 资源<!--[GraphQL API 资源](reference/index.md)-->。

