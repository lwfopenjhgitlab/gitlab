---
stage: Manage
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# GraphQL API **(FREE)**

> 启用并广泛可用于极狐GitLab 12.1。功能标志 `graphql` 已移除。

[GraphQL](https://graphql.org/) 是一种 API 查询语言。您可以用它来请求您需要的确切数据，从而限制您需要的请求数量。

GraphQL 数据按类型排列，因此您的客户端可以通过[客户端 GraphQL 库](https://graphql.org/code/#graphql-clients)使用 API 并避免手动解析。

<!--There are no fixed endpoints and no data model, so you can add
to the API without creating [breaking changes](../../development/contributing/#breaking-changes).
This enables us to have a [versionless API](https://graphql.org/learn/best-practices/#versioning).-->

## 愿景

我们希望 GraphQL API 成为与极狐GitLab 以编程方式交互的**主要**方式。
为了实现这一点，需要全面覆盖 - REST API 中的东西也应该存在于 GraphQL API。

为了帮助我们实现这一愿景，与 REST API 相比，前端应该优先使用 GraphQL 进行新功能开发。

我们没有计划废弃 REST API。为了减轻同时支持两个 API 的技术负担，它们应该尽可能多地分担。

## 使用 GraphQL

如果您不熟悉极狐GitLab GraphQL API，请参阅[了解极狐GitLab GraphQL API](getting_started.md)。

<!--您可以在 [GraphQL API 参考](reference/index.md) 中查看可用资源。-->
该引用是从极狐GitLab GraphQL 架构自动生成的，并且写入 Markdown 文件。

GitLab GraphQL API 端点位于 `/api/graphql`。

<a id="graphiql"></a>

### GraphiQL

使用交互式 [GraphiQL Explorer](https://jihulab.com/-/graphql-explorer) 或在您 `https://<your-gitlab-site.com>/-/graphql-explorer` 上的私有化部署的极狐GitLab 实例上探索 GraphQL API。

有关详细信息，请参阅 [GraphiQL](getting_started.md#graphiql)。

### 查看 GraphQL 示例

您可以使用从 JiHuLab.com 上的公共项目中提取数据的示例查询：

- [创建审计报告](audit_report.md)
- [识别议题板](sample_issue_boards.md)
- [查询用户](users_example.md)
- [使用自定义表情符号](custom_emoji.md)

[了解](getting_started.md)页面包含自定义 GraphQL 查询的不同方法。

## 重大更改

极狐GitLab GraphQL API 是[无版本的](https://graphql.org/learn/best-practices/#versioning)，对 API 的更改主要是向后兼容的。

但是，极狐GitLab 有时会以不向后兼容的方式更改 GraphQL API。这些更改被视为重大更改，并且可以包括移除或重命名字段、参数或架构的其他部分。
创建重大更改时，极狐GitLab 遵循[废弃和移除流程](#deprecation-and-removal-process)。

为避免重大更改影响您的集成，您应该熟悉[废弃和移除流程](#deprecation-and-removal-process)并经常
[针对未来的重大更改架构验证您的 API 调用](#verify-against-the-future-breaking-change-schema)。

功能标志后面和默认禁用的字段不遵循废弃和移除流程，并且可以随时移除，恕不另行通知。

<!--更多内容，请参见[废弃极狐GitLab 功能](../../development/deprecation_guidelines/index.md)。-->

WARNING:
极狐GitLab 会尽力遵循[废弃和移除流程](#deprecation-and-removal-process)。
在极少数情况下，如果废弃流程会带来很大的风险，极狐GitLab 可能会立即对 GraphQL API 进行重大更改以修补关键安全或性能问题。

<a id="verify-against-the-future-breaking-change-schema"></a>

### 针对未来重大更改架构进行验证

> 引入于极狐GitLab 15.6。

您可以对 GraphQL API 进行调用，就好像所有已弃用的项目都已经被移除一样。
这样，您可以在项目实际从架构中删除之前，在[重大更改发布](#deprecation-and-removal-process)之前验证 API 调用。

要进行这些调用，请向极狐GitLab GraphQL API 端点（例如，适用于极狐GitLab SaaS GraphQL 的 `https://gitlab.com/api/graphql?remove_deprecated=true` ）
添加一个 `remove_deprecated=true` 查询参数。

<a id="deprecation-and-removal-process"></a>

### 废弃和移除流程

极狐GitLab GraphQL API 的废弃和移除流程与更广泛的极狐GitLab 废弃流程<!--[废弃流程](https://about.gitlab.com/handbook/product/gitlab-the-product/#breaking-changes-deprecations-and-removing-features)-->保持一致。

标记为从极狐GitLab GraphQL API 中移除的部分结构已废弃<!--[已弃用](https://about.gitlab.com/handbook/product/gitlab-the-product/#deprecation)-->，
但仍可用于至少六个版本。然后将其完全移除于下一个 `XX.0` 的主要版本中。<!--[移除](https://about.gitlab.com/handbook/product/gitlab-the-product/#removal)-->

<!--Items are marked as deprecated in:

- The [schema](https://spec.graphql.org/October2021/#sec--deprecated).
- The [GraphQL API reference](reference/index.md).
- The [deprecation feature removal schedule](../../update/deprecations.md), which is linked from release posts.
- Introspection queries of the GraphQL API.

The deprecation message provides an alternative for the deprecated schema item,
if applicable.
-->

NOTE:
如果您使用 GraphQL API，我们建议您从 GraphQL API 调用中尽快移除已废弃的架构，以避免遇到重大更改。

您应该[在没有弃用的架构项的情况下，针对架构验证您的 API 调用](#verify-against-the-future-breaking-change-schema)。

#### 废弃示例

以下字段在不同的小版本发布中都已废弃，但都移除于 14.0。

| 字段废弃版本 | 原因                                                                      |
|--------|-------------------------------------------------------------------------|
| 12.7   | 极狐GitLab 每次大版本发布都有 12 个小版本发布，为确保字段在超过 6 次发布中可用，其移除于 14.0 大版本发布（不是 13.0） |
| 13.6   | 14.0 中的发布允许 6 个月的可用性               |

### 移除条目列表

请查看先前发布中的[移除条目列表](removed_items.md)。

## 可用查询

GraphQL API 包括根级别的以下查询：

查询        | 描述
--------------|------------
`project`     | 项目信息及其相关信息，例如议题和合并请求
`group`       | 基本的群组信息和史诗
`user`        | 特定用户的信息
`namespace`   | 命名空间和其中的 `projects`
`currentUser` | 经过身份验证的用户的信息
`users`       | 用户合集的信息
`metaData`    | 极狐GitLab 和 GraphQL API 的元数据
`snippets`    | 经过身份验证的用户可见的代码片段

新的关联和根级别的对象会定期添加。
<!--See the [GraphQL API Reference](reference/index.md) for up-to-date information.

Root-level queries are defined in
[`app/graphql/types/query_type.rb`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/graphql/types/query_type.rb).-->

### 多路查询

极狐GitLab 支持使用 [`@apollo/client/link/batch-http`](https://www.apollographql.com/docs/react/api/link/apollo-link-batch-http/) 将查询批处理到单个请求中。
有关多路查询的信息也可用于极狐GitLab 在后端使用的库，即 [GraphQL Ruby](https://graphql-ruby.org/queries/multiplex.html)。

## 限制

极狐GitLab GraphQL API 有以下限制：

 限制                               | 默认                                                                                                                                                               
----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------
 最大页面大小                           | 每页 100 个记录（节点）。适用于 API 中的多数连接。特定连接的最大页面大小限制可能不同，或大或小                                                                                                             
 [最大查询复杂度](#max-query-complexity) | 未授权请求：`200`；授权请求：`250`                                                                                                                                           
 请求超时时间                           | 30 秒                                                                                                                                                             
 最大查询大小                           | 每个查询 10,000 个字符。如果达到此限制，请使用[变量](https://graphql.org/learn/queries/#variables)和 [Fragment](https://graphql.org/learn/queries/#fragments) 来减少查询大小。可以删除空格作为减少大小的方法。 

<a id="max-query-complexity"></a>

### 最大查询复杂度

极狐GitLab GraphQL API 对查询的*复杂度*进行评分。一般来说，较大的查询具有更高的复杂度分数。
此限制旨在保护 API 执行可能对其整体性能产生负面影响的查询。

您可以[查询](getting_started.md#query-complexity)查询的复杂度分数以及请求的限制。

如果查询超出复杂性限制，则会返回错误消息响应。

通常情况下，查询中的每个字段都会在复杂度得分上加 `1`，尽管对于特定领域会更高或更低。有时，添加某些参数也可能增加查询的复杂性。

NOTE:
未来可能会修改复杂性限制。此外，查询的复杂性也可能会更改。

## 解决检测为垃圾邮件的 Mutation

> 引入于极狐GitLab 13.11。

GraphQL Mutation 可能会被检测为垃圾邮件。如果其被检测为垃圾邮件并且：

- 未配置 CAPTCHA 服务，会出现
  [GraphQL 顶级错误](https://spec.graphql.org/June2018/#sec-Errors)。例如：

  ```json
  {
    "errors": [
      {
        "message": "Request denied. Spam detected",
        "locations": [ { "line": 6, "column": 7 } ],
        "path": [ "updateSnippet" ],
        "extensions": {
          "spam": true
        }
      }
    ],
    "data": {
      "updateSnippet": {
        "snippet": null
      }
    }
  }
  ```

- 配置了 CAPTCHA 服务，您会收到带有以下内容的响应：
  - `needsCaptchaResponse` 设置为 `true`
  -  设置了 `spamLogId` 和 `captchaSiteKey` 字段

  例如：

  ```json
  {
    "errors": [
      {
        "message": "Request denied. Solve CAPTCHA challenge and retry",
        "locations": [ { "line": 6, "column": 7 } ],
        "path": [ "updateSnippet" ],
        "extensions": {
          "needsCaptchaResponse": true,
          "captchaSiteKey": "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI",
          "spamLogId": 67
        }
      }
    ],
    "data": {
      "updateSnippet": {
        "snippet": null,
      }
    }
  }
  ```

- 使用合适的 CAPTCHA API 通过 `captchaSiteKey` 获取 CAPTCHA 响应值。
  仅支持 [Google reCAPTCHA v2](https://developers.google.com/recaptcha/docs/display)。
- 设置 `X-GitLab-Captcha-Response` 和 `X-GitLab-Spam-Log-Id` Header，重新提交请求。

NOTE:
极狐GitLab GraphiQL 实现不允许传递 Header，因此我们必须将其写为 cURL 查询。`--data-binary` 用于在 JSON 嵌入式查询中正确处理转义的双引号。

```shell
export CAPTCHA_RESPONSE="<CAPTCHA response obtained from CAPTCHA service>"
export SPAM_LOG_ID="<spam_log_id obtained from initial REST response>"
curl --header "Authorization: Bearer $PRIVATE_TOKEN" --header "Content-Type: application/json" --header "X-GitLab-Captcha-Response: $CAPTCHA_RESPONSE" --header "X-GitLab-Spam-Log-Id: $SPAM_LOG_ID" --request POST --data-binary '{"query": "mutation {createSnippet(input: {title: \"Title\" visibilityLevel: public blobActions: [ { action: create filePath: \"BlobPath\" content: \"BlobContent\" } ] }) { snippet { id title } errors }}"}' "https://gitlab.example.com/api/graphql"
```
