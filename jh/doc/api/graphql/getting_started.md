---
stage: Manage
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 运行 GraphQL API 查询和修改 **(FREE)**

本指南演示了极狐GitLab GraphQL API 的基本用法。

关于开发 API 本身的实施细节，请阅读 [GraphQL API 风格指南](../../development/api_graphql_styleguide.md)。

## 运行示例

可以使用以下内容运行记录的示例：

- [命令行](#command-line)
- [GraphiQL](#graphiql)
- [Rails console](#rails-console)

<a id="command-line"></a>

### 命令行

在您的本地计算机中，您可以在命令行的 `curl` 请求中运行 GraphQL 查询。 GraphQL 请求可以作为对 `/api/graphql` 的 `POST` 请求发出，并将查询作为有效负载。
您可以通过生成[个人访问令牌](../../user/profile/personal_access_tokens.md)授权您的请求，将其用作 Bearer 令牌。
此令牌至少需要 `read_api` 范围。

示例：

```shell
GRAPHQL_TOKEN=<your-token>
curl "https://gitlab.com/api/graphql" --header "Authorization: Bearer $GRAPHQL_TOKEN" \
     --header "Content-Type: application/json" --request POST \
     --data "{\"query\": \"query {currentUser {name}}\"}"
```

要在查询字符串中嵌套字符串，
需要将数据用单引号引起来或使用 `\\` 对字符串进行转义：

```shell
curl "https://gitlab.com/api/graphql" --header "Authorization: Bearer $GRAPHQL_TOKEN" \
    --header "Content-Type: application/json" --request POST \
    --data '{"query": "query {project(fullPath: \"<group>/<subgroup>/<project>\") {jobs {nodes {id duration}}}}"}'
      # or "{\"query\": \"query {project(fullPath: \\\"<group>/<subgroup>/<project>\\\") {jobs {nodes {id duration}}}}\"}"
```

<a id="graphiql"></a>

### GraphiQL

GraphiQL 允许您通过语法高亮和自动完成功能直接针对服务器端点运行查询。
还允许您探索架构和类型。

下面的例子：

- 可以直接针对极狐GitLab 运行。
- 无需任何进一步设置即可针对 JiHuLab.com 运行。确保您已登录并导航到 [GraphiQL Explorer](https://jihulab.com/-/graphql-explorer)。

如果要在本地或私有化部署实例上运行查询，则必须：

- 使用名为 `graphql-sandbox` 的项目在其下创建 `gitlab-org` 群组，并在项目中创建几个议题。
- 或编辑查询，用您自己的群组和项目替换 `gitlab-org/graphql-sandbox`。

有关详细信息，请参阅[运行 GraphiQL](index.md#graphiql)。

NOTE:
对于 12.0，请启用 `graphql` [功能标志](../features.md#set-or-create-a-feature)。

<a id="rails-console"></a>"

### Rails console **(FREE SELF)**

GraphQL 查询可以在 [Rails console 会话](../../administration/operations/rails_console.md#starting-a-rails-console-session)中运行。例如，搜索项目：

```ruby
current_user = User.find_by_id(1)
query = <<~EOQ
query securityGetProjects($search: String!) {
  projects(search: $search) {
    nodes {
      path
    }
  }
}
EOQ

variables = { "search": "gitlab" }

result = GitlabSchema.execute(query, variables: variables, context: { current_user: current_user })
result.to_h
```

<a id="queries-and-mutations"></a>

## 查询和修改

极狐GitLab GraphQL API 可用于：

- 数据检索查询
- 创建、更新和删除数据的[修改](#mutations)

NOTE:
在极狐GitLab GraphQL API 中，`id` 指的是[全局 ID](https://graphql.org/learn/global-object-identification/)，
它是一个对象标识符，格式为 `"gid://gitlab/Issue/123"`。

<!--[GitLab GraphQL Schema](reference/index.md) outlines which objects and fields are
available for clients to query and their corresponding data types.-->

示例：只获取 `gitlab-org` 群组中当前经过身份验证的用户可以访问的（上限）所有项目的名称。

```graphql
query {
  group(fullPath: "gitlab-org") {
    id
    name
    projects {
      nodes {
        name
      }
    }
  }
}
```

示例：获取特定项目和议题 #2 的标题。

```graphql
query {
  project(fullPath: "gitlab-org/graphql-sandbox") {
    name
    issue(iid: "2") {
      title
    }
  }
}
```

### 图遍历

检索子节点时使用：

- `edges { node { } }` 语法
- 简短形式的 `nodes { }` 语法

在它下面是我们正在遍历的一个图，因此得名 GraphQL。

示例：获取项目名称及其所有议题的标题。

```graphql
query {
  project(fullPath: "gitlab-org/graphql-sandbox") {
    name
    issues {
      nodes {
        title
        description
      }
    }
  }
}
```

更多有关查询的信息：
[GraphQL 文档](https://graphql.org/learn/queries/)

### 授权

授权使用与极狐GitLab 应用程序（和 JiHuLab.com）相同的引擎。
如果您已登录极狐GitLab 并使用 GraphiQL，则所有查询都以您-经过身份验证的用户的身份进行操作。
有关更多信息，请阅读[极狐GitLab API 文档](../rest/index.md#authentication)。

<a id="mutations"></a>

### 修改

修改会改变数据。我们可以更新、删除或创建新记录。
修改通常使用 InputTypes 和变量，这里都没有出现。

修改拥有：

- 输入。例如参数，例如您想要给哪个对象添加哪个表情符号。
- 返回报表。也就是说，成功时您想得到什么。
- 错误。经常询问是否一切正常，以防万一。

#### 创建修改

示例：将 `:tea:` 反应表情符号添加到某个议题中。

```graphql
mutation {
  awardEmojiAdd(input: { awardableId: "gid://gitlab/Issue/27039960",
      name: "tea"
    }) {
    awardEmoji {
      name
      description
      unicode
      emoji
      unicodeVersion
      user {
        name
      }
    }
    errors
  }
}
```

示例：为议题添加评论。在本例中，我们使用 `GitLab.com` 议题的 ID。如果您使用的是本地实例，则必须获取您可以编辑的议题的 ID。

```graphql
mutation {
  createNote(input: { noteableId: "gid://gitlab/Issue/27039960",
      body: "*sips tea*"
    }) {
    note {
      id
      body
      discussion {
        id
      }
    }
    errors
  }
}
```

#### 更新修改

当您看到您创建的备注的结果 `id` 时，请进行记录。

```graphql
mutation {
  updateNote(input: { id: "gid://gitlab/Note/<note ID>",
      body: "*SIPS TEA*"
    }) {
    note {
      id
      body
    }
    errors
  }
}
```

#### 删除修改

删除评论。

```graphql
mutation {
  destroyNote(input: { id: "gid://gitlab/Note/<note ID>" }) {
    note {
      id
      body
    }
    errors
  }
}
```

您会获取类似以下示例中的内容：

```json
{
  "data": {
    "destroyNote": {
      "errors": [],
      "note": null
    }
  }
}
```

我们请求备注的详细信息，但是不存在，所以我们得到 `null`。

有关修改的详细信息，请参见 [GraphQL 文档](https://graphql.org/learn/queries/#mutations)。

### 更新项目设置

您可以在单个 GraphQL 修改中更新多个项目设置。
<!--This example is a workaround for [the major change](../../update/deprecations.md#default-cicd-job-token-ci_job_token-scope-changed)-->

```graphql
mutation DisableCI_JOB_TOKENscope {
  projectCiCdSettingsUpdate(input:{fullPath: "<namespace>/<project-name>", inboundJobTokenScopeEnabled: false, jobTokenScopeEnabled: false}) {
    ciCdSettings {
      inboundJobTokenScopeEnabled
      jobTokenScopeEnabled
    }
    errors
  }
}
```

### 内省查询

客户端可以通过[内省查询](https://graphql.org/learn/introspection/)来查询 GraphQL 端点以获取有关其自身架构的信息。
[GraphiQL 查询 Explorer](https://jihulab.com/-/graphql-explorer) 使用内省查询：

- 了解我们的 GraphQL 架构
- 实现自动完成
- 提供其交互式的 `Docs` 选项卡

示例：获取架构中的所有类型名称。

```graphql
{
  __schema {
    types {
      name
    }
  }
}
```

示例：获取与议题相关的所有字段。 `kind` 表明类型的枚举值，例如 `OBJECT`、`SCALAR` 或 `INTERFACE`。

```graphql
query IssueTypes {
  __type(name: "Issue") {
    kind
    name
    fields {
      name
      description
      type {
        name
      }
    }
  }
}
```

更多关于内省的内容，请参见 [GraphQL 文档](https://graphql.org/learn/introspection/)。

<a id="query-complexity"></a>

### 查询复杂度

查询的计算[复杂度分数和限制](index.md#max-query-complexity)可以通过查询 `queryComplexity` 的方式向客户端展示。

```graphql
query {
  queryComplexity {
    score
    limit
  }

  project(fullPath: "gitlab-org/graphql-sandbox") {
    name
  }
}
```

## 排序

一些极狐GitLab GraphQL 端点允许您指定如何为对象的集合进行排序。您只能按照架构允许的方式进行排序。

示例：议题可以按创建日期排序：

```graphql
query {
  project(fullPath: "gitlab-org/graphql-sandbox") {
   name
    issues(sort: created_asc) {
      nodes {
        title
        createdAt
      }
    }
  }
}
```

<a id="pagination"></a>

## 分页

分页是一种只要求记录子集的方式，例如前十个。如果想要更多，我们可以从服务器再次请求十个，例如发送 `please give me the next ten records`。

默认情况下，极狐GitLab GraphQL API 每页返回 100 条记录。如果要改变这个行为，请使用 `first` 或 `last` 参数。两个参数都有一个值，所以
`first: 10` 返回前十条记录，`last: 10` 返回最后十条记录。
每页返回的记录数量是有限制的，一般是
`100`。

示例：仅检索前两个议题（切片）。`cursor` 字段给出可以从中检索与该位置相关的更多记录的位置。

```graphql
query {
  project(fullPath: "gitlab-org/graphql-sandbox") {
    name
    issues(first: 2) {
      edges {
        node {
          title
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
}
```

示例：检索接下来的三个。（光标值
`eyJpZCI6IjI3MDM4OTMzIiwiY3JlYXRlZF9hdCI6IjIwMTktMTEtMTQgMDU6NTY6NDQgVVRDIn0`
可能不同，但它是为上面返回的第二个议题返回的 `cursor` 值。）

```graphql
query {
  project(fullPath: "gitlab-org/graphql-sandbox") {
    name
    issues(first: 3, after: "eyJpZCI6IjI3MDM4OTMzIiwiY3JlYXRlZF9hdCI6IjIwMTktMTEtMTQgMDU6NTY6NDQgVVRDIn0") {
      edges {
        node {
          title
        }
        cursor
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
}
```

更多关于分页和光标的内容请参见 [GraphQL 文档](https://graphql.org/learn/pagination/)。
