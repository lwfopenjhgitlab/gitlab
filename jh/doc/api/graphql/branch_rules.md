---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 列出项目的分支规则 **(FREE)**

> 引入于极狐GitLab 15.8。

本指南展示如何使用 [GraphiQL Explorer](getting_started.md#graphiql) 查询给定项目中的分支规则。


[示例查询](#set-up-the-graphiql-explorer)通过其完整路径（例如 `gitlab-org/gitlab-docs`）在极狐GitLab 实例中查找项目。
在查询中，我们请求项目的所有已配置分支规则。

NOTE:
您可以使用 `cURL` 通过 HTTP 端点直接运行相同的查询。关于更多信息，请参阅[命令行](getting_started.md#command-line)的入门指南。

<a id="set-up-the-graphiql-explorer"></a>

## 设置 GraphiQL Explorer

此过程提供了一个实质性示例，您可以将其复制并粘贴到您的 [GraphiQL Explorer](https://jihulab.com/-/graphql-explorer) 实例：

1. 复制以下代码摘录：

   ```graphql
   query {
     project(fullPath: "gitlab-org/gitlab-docs") {
       branchRules {
         nodes {
           name
           isDefault
           isProtected
           matchingBranchesCount
           createdAt
           updatedAt
           branchProtection {
             allowForcePush
             codeOwnerApprovalRequired
             mergeAccessLevels {
               nodes {
                 accessLevel
                 accessLevelDescription
                 user {
                   name
                 }
                 group {
                   name
                 }
               }
             }
             pushAccessLevels {
               nodes {
                 accessLevel
                 accessLevelDescription
                 user {
                   name
                 }
                 group {
                   name
                 }
               }
             }
             unprotectAccessLevels {
               nodes {
                 accessLevel
                 accessLevelDescription
                 user {
                   name
                 }
                 group {
                   name
                 }
               }
             }
           }
           externalStatusChecks {
             nodes {
               id
               name
               externalUrl
             }
           }
           approvalRules {
             nodes {
               id
               name
               type
               approvalsRequired
               eligibleApprovers {
                 nodes {
                   name
                 }
               }
             }
           }
         }
       }
     }
   }
   ```

1. 打开 [GraphiQL Explorer 工具](https://jihulab.com/-/graphql-explorer)。
1. 将上面列出的 `query` 粘贴到 GraphiQL Explorer 工具的左侧窗口中。
1. 选择 **Play**，结果如下：

   ![GraphiQL explorer query for branch rules](img/list_branch_rules_query_example_v15_8.png)

如果没有显示分支规则，可能是因为：

- 没有配置分支规则。
- 您的角色无权查看分支规则。管理员可以访问所有记录。

<!--
## 在 GDK 中运行查询

Instead of requesting access, it may be easier for you to run the query in the
[GitLab Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit).

1. Sign in as the default admin, `root`, with the credentials from
   [the GDK documentation](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/gdk_commands.md#get-the-login-credentials).
1. Ensure you have some branch rules configured for the `flightjs/Flight` project.
1. Replace the full path in the query:

   ```graphql
   query {
     project(fullPath: "flightjs/Flight") {
   ```

1. In your GDK instance, visit the GraphiQL explorer tool: `http://gdk.test:3000/-/graphql-explorer`.
1. Paste the `query` listed above into the left window of your GraphiQL explorer tool.
1. Select **Play** to view the result.

For more information on each field, see the [GraphQL API Resources](reference/index.md).
-->
