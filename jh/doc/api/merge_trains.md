---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 合并队列 API **(PREMIUM)**

> - 引入于极狐GitLab 12.9。
> - 通过使用此 API，您可以消耗[合并队列](../ci/pipelines/merge_trains.md)条目。

每个用于合并队列的 API 调用都必须通过至少开发者[角色](../user/permissions.md)的身份验证。

如果用户不是项目成员并且项目是私有的，则对该项目的 `GET` 请求会返回 `404` 状态码。

如果项目无法使用合并队列，则返回 `403` 状态码。

## 合并队列 API 分页

默认情况下，`GET` 请求一次返回 20 个结果，因为 API 结果是分页的。

阅读更多关于[分页](rest/index.md#pagination)的内容。

## 列出项目的合并队列

获取请求项目的所有合并队列：

```plaintext
GET /projects/:id/merge_trains
GET /projects/:id/merge_trains?scope=complete
```

| 参数      | 类型             | 是否必需 | 描述                                                    |
|---------|----------------|------|-------------------------------------------------------|
| `id`    | integer/string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `scope` | string         | no   | 按照给定范围，返回过滤的合并队列。可用范围为 `active`（待合并）和 `complete`（已合并） |
| `sort`  | string         | no   | 按照 `asc` 或 `desc` 的顺序返回合并队列。默认为 `desc`                |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/merge_trains"
```

响应示例：

```json
[
  {
    "id": 110,
    "merge_request": {
      "id": 126,
      "iid": 59,
      "project_id": 20,
      "title": "Test MR 1580978354",
      "description": "",
      "state": "merged",
      "created_at": "2020-02-06T08:39:14.883Z",
      "updated_at": "2020-02-06T08:40:57.038Z",
      "web_url": "http://local.gitlab.test:8181/root/merge-train-race-condition/-/merge_requests/59"
    },
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://local.gitlab.test:8181/root"
    },
    "pipeline": {
      "id": 246,
      "sha": "bcc17a8ffd51be1afe45605e714085df28b80b13",
      "ref": "refs/merge-requests/59/train",
      "status": "success",
      "created_at": "2020-02-06T08:40:42.410Z",
      "updated_at": "2020-02-06T08:40:46.912Z",
      "web_url": "http://local.gitlab.test:8181/root/merge-train-race-condition/pipelines/246"
    },
    "created_at": "2020-02-06T08:39:47.217Z",
    "updated_at": "2020-02-06T08:40:57.720Z",
    "target_branch": "feature-1580973432",
    "status": "merged",
    "merged_at": "2020-02-06T08:40:57.719Z",
    "duration": 70
  }
]
```

## 列出合并队列中的合并请求

将所有合并请求添加到请求的目标分支的合并队列中。

```plaintext
GET /projects/:id/merge_trains/:target_branch
```

支持的参数：

| 参数              | 类型             | 是否必需 | 描述                                                                                                             |
|-----------------|----------------|------|----------------------------------------------------------------------------------------------------------------|
| `id`            | integer/string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)                                                     |
| `target_branch` | string         | yes  | 合并队列的目标分支                                                                                                      |
| `scope`         | string         | no   | 返回按给定范围过滤的合并队列。可用范围为 `active`（待合并）和 `complete`（已合并）                                                            |
| `sort`          | string         | no   | 返回按 `asc` 或 `desc` 顺序排序的合并队列。默认为 `desc` |

请求示例：

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/597/merge_trains/main"
```

响应示例：

```json
[
  {
    "id": 267,
    "merge_request": {
      "id": 273,
      "iid": 1,
      "project_id": 597,
      "title": "My title 9",
      "description": null,
      "state": "opened",
      "created_at": "2022-10-31T19:06:05.725Z",
      "updated_at": "2022-10-31T19:06:05.725Z",
      "web_url": "http://localhost/namespace18/project21/-/merge_requests/1"
    },
    "user": {
      "id": 933,
      "username": "user12",
      "name": "Sidney Jones31",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/6c8365de387cb3db10ecc7b1880203c4?s=80\u0026d=identicon",
      "web_url": "http://localhost/user12"
    },
    "pipeline": {
      "id": 273,
      "iid": 1,
      "project_id": 598,
      "sha": "b83d6e391c22777fca1ed3012fce84f633d7fed0",
      "ref": "main",
      "status": "pending",
      "source": "push",
      "created_at": "2022-10-31T19:06:06.231Z",
      "updated_at": "2022-10-31T19:06:06.231Z",
      "web_url": "http://localhost/namespace19/project22/-/pipelines/273"
    },
    "created_at": "2022-10-31T19:06:06.237Z",
    "updated_at":"2022-10-31T19:06:06.237Z",
    "target_branch":"main",
    "status":"idle",
    "merged_at":null,
    "duration":null
  }
]
```

## 获取合并队列上合并请求的状态

获取请求的合并请求的合并队列信息。

```plaintext
GET /projects/:id/merge_trains/merge_requests/:merge_request_iid
```

支持的参数：

| 参数                  | 类型             | 是否必需 | 描述                                                         |
|---------------------|----------------|------|------------------------------------------------------------|
| `id`                | integer/string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | yes  | 合并请求的内部 ID                       |

请求示例：

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/597/merge_trains/merge_requests/1"
```

响应示例：

```json
{
  "id": 267,
  "merge_request": {
    "id": 273,
    "iid": 1,
    "project_id": 597,
    "title": "My title 9",
    "description": null,
    "state": "opened",
    "created_at": "2022-10-31T19:06:05.725Z",
    "updated_at": "2022-10-31T19:06:05.725Z",
    "web_url": "http://localhost/namespace18/project21/-/merge_requests/1"
  },
  "user": {
    "id": 933,
    "username": "user12",
    "name": "Sidney Jones31",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/6c8365de387cb3db10ecc7b1880203c4?s=80\u0026d=identicon",
    "web_url": "http://localhost/user12"
  },
  "pipeline": {
    "id": 273,
    "iid": 1,
    "project_id": 598,
    "sha": "b83d6e391c22777fca1ed3012fce84f633d7fed0",
    "ref": "main",
    "status": "pending",
    "source": "push",
    "created_at": "2022-10-31T19:06:06.231Z",
    "updated_at": "2022-10-31T19:06:06.231Z",
    "web_url": "http://localhost/namespace19/project22/-/pipelines/273"
  },
  "created_at": "2022-10-31T19:06:06.237Z",
  "updated_at":"2022-10-31T19:06:06.237Z",
  "target_branch":"main",
  "status":"idle",
  "merged_at":null,
  "duration":null
}
```
