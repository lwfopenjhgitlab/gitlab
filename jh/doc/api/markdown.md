---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Markdown API **(FREE)**

将 Markdown 内容转换为 HTML。

仅在 APIv4 中可用。

## 需要身份验证

> 引入于极狐GitLab 15.3。[功能标志](../administration/feature_flags.md)名为 `authenticate_markdown_api`。默认启用。

FLAG：
在私有化部署的极狐GitLab 上，默认情况下启用此功能并且需要身份验证。
要删除身份验证要求，请管理员禁用名为 `authenticate_markdown_api` 的[功能标志](../administration/feature_flags.md)。
在 JiHuLab.com 上，此功能可用。

所有对 Markdown API 的 API 调用都必须经过[身份验证](rest/index.md#authentication)。

## 渲染任意 Markdown 文档。

```plaintext
POST /markdown
```

| 参数        | 类型      | 是否必需 | 描述                                                                                             |
|-----------|---------|------|------------------------------------------------------------------------------------------------|
| `text`    | string  | yes  | 渲染的 Markdown 文本                                                                                |
| `gfm`     | boolean | no   | 使用极狐GitLab Flavored Markdown 的渲染文本。默认为 `false`                                                 |
| `project` | string  | no   | 使用极狐 GitLab Flavored Markdown 创建引用时，将 `project` 用作上下文 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
  --header "Content-Type:application/json" \
  --data '{"text":"Hello world! :tada:", "gfm":true, "project":"group_example/project_example"}' "https://gitlab.example.com/api/v4/markdown"
```

响应示例：

```json
{ "html": "<p dir=\"auto\">Hello world! <gl-emoji title=\"party popper\" data-name=\"tada\" data-unicode-version=\"6.0\">🎉</gl-emoji></p>" }
```
