---
stage: Govern
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 个人访问令牌 API **(FREE)**

您可以阅读更多有关[个人访问令牌](../user/profile/personal_access_tokens.md#personal-access-tokens)的内容。

## 列出个人访问令牌

> - 引入于极狐GitLab 13.3。
> - 从极狐GitLab 旗舰版移动到免费版于 13.6。
> - `created_after`、`created_before`、`last_used_after`、`last_used_before`、`revoked`、`search` 和 `state` 引入于极狐GitLab 15.5。

获取经过身份验证的用户有权访问的所有个人访问令牌。默认情况下，返回一个未过滤的列表：

- 只有当前用户创建的非管理员个人访问令牌。
- 管理员的所有个人访问令牌。

管理员：

- 可以使用 `user_id` 参数按用户过滤。
- 可以在所有个人访问令牌上使用其他过滤器（极狐GitLab 15.5 及更高版本）。

非管理员：

- 不能使用 `user_id` 参数过滤除他们自己以外的任何用户，否则他们会收到 `401 Unauthorized` 响应。
- 只能在他们自己的个人访问令牌上过滤（极狐GitLab 15.5 及更高版本）。

```plaintext
GET /personal_access_tokens
GET /personal_access_tokens?created_after=2022-01-01T00:00:00
GET /personal_access_tokens?created_before=2022-01-01T00:00:00
GET /personal_access_tokens?last_used_after=2022-01-01T00:00:00
GET /personal_access_tokens?last_used_before=2022-01-01T00:00:00
GET /personal_access_tokens?revoked=true
GET /personal_access_tokens?search=name
GET /personal_access_tokens?state=inactive
GET /personal_access_tokens?user_id=1
```

支持的参数：

| 参数                 | 类型                  | 是否必需                   | 描述                                           |
|--------------------|---------------------|------------------------|----------------------------------------------|
| `created_after`    | datetime (ISO 8601) | **{dotted-circle}** No | 将结果限制为在指定时间后创建的 PAT                          |
| `created_before`   | datetime (ISO 8601) | **{dotted-circle}** No | 将结果限制为在指定时间前创建的 PAT                          |
| `last_used_after`  | datetime (ISO 8601) | **{dotted-circle}** No | 将结果限制为在指定时间后最后一次使用的 PAT                      |
| `last_used_before` | datetime (ISO 8601) | **{dotted-circle}** No | 将结果限制为在指定时间前最后一次使用的 PAT                      |
| `revoked`          | boolean             | **{dotted-circle}** No | 将结果限制为具有指定撤销状态的 PAT。有效值为 `true` 和 `false`    |
| `search`           | string              | **{dotted-circle}** No | 将结果限制为名称包含搜索字符串的 PAT                         |
| `state`            | string              | **{dotted-circle}** No | 将结果限制为具有特定状态的 PAT。 有效值为 `active` 和 `inactive` |
| `user_id`          | integer or string   | **{dotted-circle}** No | 将结果限制为特定用户所拥有的 PAT                           |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/personal_access_tokens"
```

响应示例：

```json
[
    {
        "id": 4,
        "name": "Test Token",
        "revoked": false,
        "created_at": "2020-07-23T14:31:47.729Z",
        "scopes": [
            "api"
        ],
        "user_id": 24,
        "last_used_at": "2021-10-06T17:58:37.550Z",
        "active": true,
        "expires_at": null
    }
]
```

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/personal_access_tokens?user_id=3"
```

响应示例：

```json
[
    {
        "id": 4,
        "name": "Test Token",
        "revoked": false,
        "created_at": "2020-07-23T14:31:47.729Z",
        "scopes": [
            "api"
        ],
        "user_id": 3,
        "last_used_at": "2021-10-06T17:58:37.550Z",
        "active": true,
        "expires_at": null
    }
]
```
请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/personal_access_tokens?revoked=true"
```

响应示例：

```json
[
    {
        "id": 41,
        "name": "Revoked Test Token",
        "revoked": true,
        "created_at": "2022-01-01T14:31:47.729Z",
        "scopes": [
            "api"
        ],
        "user_id": 8,
        "last_used_at": "2022-05-18T17:58:37.550Z",
        "active": false,
        "expires_at": null
    }
]
```

您可以通过合并的属性进行过滤：

```plaintext
GET /personal_access_tokens?revoked=true&created_before=2022-01-01
```

## 获取单个个人访问令牌

使用以下任意一个方法获取个人访问令牌：

- 使用个人访问令牌的 ID
- 将其传递给 header 中的 API

### 使用个人访问令牌 ID

> 引入于极狐GitLab 15.1。

通过 ID 获取单个个人访问令牌。用户可以获得自己的令牌。
管理员可以获得任何令牌。

```plaintext
GET /personal_access_tokens/:id
```

| 参数   | 类型             | 是否必需 | 描述                                 |
|------|----------------|------|------------------------------------|
| `id` | integer/string | yes  | 个人访问令牌 ID |

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/personal_access_tokens/<id>"
```

#### 响应

> `404` HTTP 状态码引入于极狐GitLab 15.3。

- `401: Unauthorized` 如果是：
    - 用户无权访问具有指定 ID 的令牌。
    - 具有指定 ID 的令牌不存在。
- `404: Not Found` 如果用户是管理员但具有指定 ID 的令牌不存在。

### 使用请求 header

> 引入于极狐GitLab 15.5。

通过在 header 中传递令牌来获取单个个人访问令牌和令牌信息。

```plaintext
GET /personal_access_tokens/self
```

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/personal_access_tokens/self"
```

响应示例：

```json
{
    "id": 4,
    "name": "Test Token",
    "revoked": false,
    "created_at": "2020-07-23T14:31:47.729Z",
    "scopes": [
        "api"
    ],
    "user_id": 3,
    "last_used_at": "2021-10-06T17:58:37.550Z",
    "active": true,
    "expires_at": null
}
```

## 轮换个人访问令牌

> 引入于极狐GitLab 16.0。

轮换个人访问令牌。撤销先前的令牌并创建一个在一周后过期的新令牌。

```plaintext
POST /personal_access_tokens/:id/rotate
```

| 参数   | 类型             | 是否必需 | 描述                                 |
|------|----------------|------|------------------------------------|
| `id` | integer/string | yes  | 个人访问令牌 ID |

NOTE:
非管理员可以轮换他们自己的令牌。管理员可以轮换任何用户的令牌。

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/personal_access_tokens/<personal_access_token_id>/rotate"
```

### 响应

- `200: OK`：如果现有令牌已成功撤销并且新令牌已成功创建。
- `400: Bad Request`：如果没有成功轮换。
- `401: Unauthorized`：如果出现以下任一情况：
    - 用户无权访问具有指定 ID 的令牌。
    - 具有指定 ID 的令牌不存在。
- `404: Not Found`：如果用户是管理员但具有指定 ID 的令牌不存在。

## 撤回个人访问令牌

可以使用以下内容之一撤回个人访问令牌：

- 使用个人访问令牌的 ID 
- 将其传递到 Header 中的 API 

### 使用个人访问令牌 ID

> - 引入于极狐GitLab 13.3。
> - 从极狐GitLab 旗舰版移动到免费版于 13.6。

使用 ID 撤回个人访问令牌。

```plaintext
DELETE /personal_access_tokens/:id
```

| 参数   | 类型             | 是否必需 | 描述                                 |
|------|----------------|------|------------------------------------|
| `id` | integer/string | yes  | 个人访问令牌 ID|

NOTE:
非管理员只能撤回他们自己的令牌。管理员可以撤回任何用户的令牌。

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/personal_access_tokens/<personal_access_token_id>"
```

#### 响应

- 撤回成功：`204: No Content`
- 撤回失败：`400: Bad Request` 

### 使用请求 Header

> - 引入于极狐GitLab 15.0。限定为带有 `api` 范围的令牌。
> - 引入于极狐GitLab 15.4，任何令牌都可以使用该端点。

撤销使用请求 header 传入的个人访问令牌。要求：

- 15.0 到 15.3 中为 `api` 范围
- 15.4 及更高版本中为任何范围

```plaintext
DELETE /personal_access_tokens/self
```

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/personal_access_tokens/self"
```

#### 响应

- 撤回成功：`204: No Content`
- 撤回失败：`400: Bad Request`

## 创建个人访问令牌（仅限管理员）

有关创建个人访问令牌的内容，请参见[用户 API 文档](users.md#create-a-personal-access-token)。
