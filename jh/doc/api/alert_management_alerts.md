---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 警报管理 API **(FREE)**

警报管理 API 仅限于指标图像。<!--有关更多 API 接口，请参见 [GraphQL API](graphql/reference/index.md#alertmanagementalert)。-->

## 上传指标图像

```plaintext
POST /projects/:id/alert_management_alerts/:alert_iid/metric_images
```

| 参数         | 类型     | 是否必需  | 描述                                  |
|-------------|---------|----------|--------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `alert_iid` | integer | yes      | 项目警报的内部 ID。 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>"  --form 'file=@/path/to/file.png' \
--form 'url=http://example.com' --form 'url_text=Example website' "https://gitlab.example.com/api/v4/projects/5/alert_management_alerts/93/metric_images"
```

响应示例：

```json
{
  "id": 17,
  "created_at": "2020-11-12T20:07:58.156Z",
  "filename": "sample_2054",
  "file_path": "/uploads/-/system/alert_metric_image/file/17/sample_2054.png",
  "url": "https://example.com/metric",
  "url_text": "An example metric"
}
```

## 指标图像列表

```plaintext
GET /projects/:id/alert_management_alerts/:alert_iid/metric_images
```

| 参数         | 类型     | 是否必需  | 描述                                  |
|-------------|---------|----------|--------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `alert_iid` | integer | yes      | 项目警报的内部 ID。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/alert_management_alerts/93/metric_images"
```

响应示例：

```json
[
  {
    "id": 17,
    "created_at": "2020-11-12T20:07:58.156Z",
    "filename": "sample_2054",
    "file_path": "/uploads/-/system/alert_metric_image/file/17/sample_2054.png",
    "url": "https://example.com/metric",
    "url_text": "An example metric"
  },
  {
    "id": 18,
    "created_at": "2020-11-12T20:14:26.441Z",
    "filename": "sample_2054",
    "file_path": "/uploads/-/system/alert_metric_image/file/18/sample_2054.png",
    "url": "https://example.com/metric",
    "url_text": "An example metric"
  }
]
```

## 更新指标图像

```plaintext
PUT /projects/:id/alert_management_alerts/:alert_iid/metric_images/:image_id
```

| 参数        | 类型     | 是否必需   | 描述                                                                   |
|-------------|---------|----------|----------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的项目的 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding)。 |
| `alert_iid` | integer | yes      | 项目警报的内部 ID。                                                          |
| `image_id` | integer | yes      | 图像的 ID。                                                              |
| `url` | string | no      | 查看更多指标信息的 URL。                                                       |
| `url_text` | string | no      | 图像或 URL 的描述。                                                         |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" --request PUT  --form 'url=http://example.com' --form 'url_text=Example website' "https://gitlab.example.com/api/v4/projects/5/alert_management_alerts/93/metric_images/1"
```

响应示例：

```json
{
  "id": 23,
  "created_at": "2020-11-13T00:06:18.084Z",
  "filename": "file.png",
  "file_path": "/uploads/-/system/alert_metric_image/file/23/file.png",
  "url": "https://example.com/metric",
  "url_text": "An example metric"
}
```

## 删除指标图像

```plaintext
DELETE /projects/:id/alert_management_alerts/:alert_iid/metric_images/:image_id
```

| 参数        | 类型     | 是否必需   | 描述                                                                   |
|-------------|---------|----------|----------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的项目的 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding)。 |
| `alert_iid` | integer | yes      | 项目警报的内部 ID。                                                          |
| `image_id` | integer | yes      | 图像的 ID。                                                              |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" --request DELETE "https://gitlab.example.com/api/v4/projects/5/alert_management_alerts/93/metric_images/1"
```

可以返回以下状态码：

- `204 No Content`：图像删除成功。
- `422 Unprocessable`：该图像不能被删除。
