---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组迭代 API **(PREMIUM)**

> - <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/118742)-->引入于极狐GitLab 13.5。
> - 移动到极狐GitLab 专业版于 13.9。

该页面描述群组迭代 API。
您还可以阅读[项目迭代 API](iterations.md) 页面。

## 列出群组迭代

返回群组迭代列表。

```plaintext
GET /groups/:id/iterations
GET /groups/:id/iterations?state=opened
GET /groups/:id/iterations?state=closed
GET /groups/:id/iterations?search=version
GET /groups/:id/iterations?include_ancestors=false
GET /groups/:id/iterations?updated_before=2013-10-02T09%3A24%3A18Z
GET /groups/:id/iterations?updated_after=2013-10-02T09%3A24%3A18Z
```

| 参数                  | 类型      | 是否必需 | 描述                                                                                                                      |
|---------------------|---------|------|-------------------------------------------------------------------------------------------------------------------------|
| `state`             | string  | no   | 返回 `opened`、`upcoming`、`current (previously started)`、`closed` 或 `all` 迭代。从 14.1 开始，通过 `started` 状态进行筛选已经废弃，请使用 `current` |
| `search`            | string  | no   | 仅返回标题匹配提供的资源串的迭代                                                                                                        |
| `include_ancestors` | boolean | no   | 包括来自父群组及其祖先群组的迭代。默认为 `true`                                                                                             |
| `updated_before`    | datetime | no      | 仅返回给定日期时间之前更新的迭代。预计格式为 ISO 8601（`2019-03-15T08:00:00Z`）。引入于 15.10                                                       |
| `updated_after`     | datetime | no      | 仅返回给定日期时间之后更新的迭代。预计格式为 ISO ISO 8601（`2019-03-15T08:00:00Z`）。引入于 15.10                                                   |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/iterations"
```

响应示例：

```json
[
  {
    "id": 53,
    "iid": 13,
    "sequence": 1,
    "group_id": 5,
    "title": "Iteration II",
    "description": "Ipsum Lorem ipsum",
    "state": 2,
    "created_at": "2020-01-27T05:07:12.573Z",
    "updated_at": "2020-01-27T05:07:12.573Z",
    "due_date": "2020-02-01",
    "start_date": "2020-02-14",
    "web_url": "http://gitlab.example.com/groups/my-group/-/iterations/13"
  }
]
```
