---
stage: Manage
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目关系导出 API **(FREE)**

> - 引入于极狐GitLab 14.4，在 `bulk_import` [功能标志](../administration/feature_flags.md)后，默认禁用。
> - 新的应用程序设置 `bulk_import_enabled` 引入于极狐GitLab 15.8。移除功能标志 `bulk_import`。

项目关系导出 API 将项目的部分结构导出为每个顶级关系（例如里程碑、议题和标签）的单独文件。

项目关系导出 API 主要用于[群组迁移](../user/group/import/index.md#migrate-groups-by-direct-transfer-recommended)，不能与[项目导入和导出 API](project_import_export.md) 一起使用。

## 计划新导出

开启新项目关系导出：

```plaintext
POST /projects/:id/export_relations
```

| 参数   | 类型             | 是否必需 | 描述            |
|------|----------------|------|---------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的项目的 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/export_relations"
```

```json
{
  "message": "202 Accepted"
}
```

## 导出状态

查看关系导出的状态。

```plaintext
GET /projects/:id/export_relations/status
```

| 参数   | 类型             | 是否必需 | 描述            |
|------|----------------|------|---------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的项目的 ID |

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/export_relations/status"
```

状态可以是：

- `0`：`started`
- `1`：`finished`
- `-1`：`failed`

- `0` - `started`
- `1` - `finished`
- `-1` - `failed`

```json
[
  {
    "relation": "project_badges",
    "status": 1,
    "error": null,
    "updated_at": "2021-05-04T11:25:20.423Z"
  },
  {
    "relation": "boards",
    "status": 1,
    "error": null,
    "updated_at": "2021-05-04T11:25:20.085Z"
  }
]
```

## 导出下载

下载完成的关系导出：

```plaintext
GET /projects/:id/export_relations/download
```

| 参数         | 类型             | 是否必需 | 描述                                                               |
|------------|----------------|------|------------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户拥有的项目的 ID                                                    |
| `relation` | string         | yes  | 要下载的项目顶级关系的名称 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" --remote-header-name \
     --remote-name "https://gitlab.example.com/api/v4/projects/1/export_relations/download?relation=labels"
```

```shell
ls labels.ndjson.gz
labels.ndjson.gz
```

## 相关主题

- [群组关系导出 API](group_relations_export.md)
