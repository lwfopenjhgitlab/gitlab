---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# CI Lint API **(FREE)**

<a id="validate-a-ci-yaml-configuration-with-a-namespace"></a>

## 使用命名空间验证 CI YAML 配置

> 引入于极狐GitLab 13.6。

检查 CI/CD YAML 配置是否有效。此端点具有特定于命名空间的上下文。

```plaintext
POST /projects/:id/ci/lint
```

| 参数             | 类型      | 是否必需 | 描述                                                                                                       |
|----------------|---------|------|----------------------------------------------------------------------------------------------------------|
| `content`      | string  | yes  | CI/CD 配置内容                                                                                               |
| `dry_run`      | boolean | no   | 运行[流水线创建模拟](../ci/lint.md#simulate-a-pipeline)，或仅进行静态检查。默认为 false                                        |
| `include_jobs` | boolean | no   | 如果静态检查或流水线模拟中存在的作业列表应包含在响应中，则默认为 false                                                                   |
| `ref`          | string  | no   | 当 `dry_run` 为 `true`，设置要用的分支或标签。如果未设置，则默认为项目的默认分支 |

请求示例：

```shell
curl --header "Content-Type: application/json" "https://gitlab.example.com/api/v4/projects/:id/ci/lint" --data '{"content": "{ \"image\": \"ruby:2.6\", \"services\": [\"postgres\"], \"before_script\": [\"bundle install\", \"bundle exec rake db:create\"], \"variables\": {\"DB_NAME\": \"postgres\"}, \"types\": [\"test\", \"deploy\", \"notify\"], \"rspec\": { \"script\": \"rake spec\", \"tags\": [\"ruby\", \"postgres\"], \"only\": [\"branches\"]}}"}'
```

响应示例：

- 有效配置：

  ```json
  {
    "valid": true,
    "merged_yaml": "---\n:test_job:\n  :script: echo 1\n",
    "errors": [],
    "warnings": []
  }
  ```

- 无效配置：

  ```json
  {
    "valid": false,
    "merged_yaml": "---\n:test_job:\n  :script: echo 1\n",
    "errors": [
      "jobs config should contain at least one visible job"
    ],
    "warnings": []
  }
  ```

## 验证项目的 CI 配置

> 引入于极狐GitLab 13.5。

检查项目的最新（项目默认分支的 `HEAD`）`.gitlab-ci.yml` 配置是否有效。此端点使用所有可用的命名空间特定数据，包括变量和本地包含。

```plaintext
GET /projects/:id/ci/lint
```

| 参数             | 类型      | 是否必需 | 描述                                                                                                      |
|----------------|---------|------|---------------------------------------------------------------------------------------------------------|
| `dry_run`      | boolean | no   | 运行流水线创建模拟，或仅进行静态检查                                                                                      |
| `include_jobs` | boolean | no   | 如果静态检查或流水线模拟中存在的作业列表应包含在响应中，则默认为 false                                                                  |
| `ref`          | string  | no   | 当 `dry_run` 为 `true`，设置要使用的分支或标签。如未设置，则默认为项目的默认分支 |

请求示例：

```shell
curl "https://gitlab.example.com/api/v4/projects/:id/ci/lint"
```

响应示例：

- 有效配置：

```json
{
  "valid": true,
  "merged_yaml": "---\n:test_job:\n  :script: echo 1\n",
  "errors": [],
  "warnings": []
}
```

- 无效配置：

```json
{
  "valid": false,
  "merged_yaml": "---\n:test_job:\n  :script: echo 1\n",
  "errors": [
    "jobs config should contain at least one visible job"
  ],
  "warnings": []
}
```

<!--- start_remove The following content will be removed on remove_date: '2023-08-22' -->

## 验证 CI YAML 配置（已废弃）

WARNING:
此端点废弃于极狐GitLab 15.7，并移除于极狐GitLab 16.0。使用 [`POST /projects/:id/ci/lint`](#validate-a-ci-yaml-configuration-with-a-namespace) 代替。

检查 CI/CD YAML 配置是否有效。此端点验证基本的 CI/CD 配置语法。没有任何特定于命名空间的上下文。

当实例[允许新注册](../user/admin_area/settings/sign_up_restrictions.md#disable-new-sign-ups)时，访问此端点不需要身份验证。
并且：

- 没有[允许列表或拒绝列表](../user/admin_area/settings/sign_up_restrictions.md#allow-or-deny-sign-ups-using-specific-email-domains)
- 不存在[新注册需要管理员批准](../user/admin_area/settings/sign_up_restrictions.md#require-administrator-approval-for-new-sign-ups)
- 没有额外的[注册限制](../user/admin_area/settings/sign_up_restrictions.md)

否则，需要进行身份验证。

```plaintext
POST /ci/lint
```

| 参数                    | 类型      | 是否必需 | 描述                                       |
|-----------------------|---------|------|------------------------------------------|
| `content`             | string  | yes  | CI/CD 配置内容                               |
| `include_merged_yaml` | boolean | no   | [扩展 CI/CD 配置](#yaml-expansion)是否应该包括在响应中 |
| `include_jobs`        | boolean | no   | 作业列表是否应该包括在响应中。默认为否                      |

```shell
curl --header "Content-Type: application/json" --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/ci/lint" --data '{"content": "{ \"image\": \"ruby:2.6\", \"services\": [\"postgres\"], \"before_script\": [\"bundle install\", \"bundle exec rake db:create\"], \"variables\": {\"DB_NAME\": \"postgres\"}, \"types\": [\"test\", \"deploy\", \"notify\"], \"rspec\": { \"script\": \"rake spec\", \"tags\": [\"ruby\", \"postgres\"], \"only\": [\"branches\"]}}"}'
```

YAML 对缩进和间距非常敏感，请务必粘贴极狐GitLab CI/CD YAML 配置的准确内容。

响应示例：

- 有效内容：

  ```json
  {
    "status": "valid",
    "errors": [],
    "warnings": []
  }
  ```

- 带有告警的有效内容：

  ```json
  {
    "status": "valid",
    "errors": [],
    "warnings": ["jobs:job may allow multiple pipelines to run for a single action due to
    `rules:when` clause with no `workflow:rules` - read more:
    https://docs.gitlab.com/ee/ci/troubleshooting.html#pipeline-warnings"]
  }
  ```

- 无效内容：

  ```json
  {
    "status": "invalid",
    "errors": [
      "variables config should be a hash of key value pairs"
    ],
    "warnings": []
  }
  ```

- 没有内容属性：

  ```json
  {
    "error": "content is missing"
  }
  ```

### YAML 扩展

> 引入于极狐GitLab 13.5。

CI Lint 返回配置的扩展版本。扩展对于添加了 [`include: local`](../ci/yaml/index.md#includelocal) 的 CI 配置不起作用，并且不完全支持 [`extends:`](../ci/yaml/index.md#extends) 关键字。

将使用 `include_merged_yaml` 和 `include_jobs` 传递给 CI Lint API 的 `.gitlab-ci.yml` 示例内容设置为 True：

```yaml
include:
  remote: 'https://example.com/remote.yaml'

test:
  stage: test
  script:
    - echo 1
```

`https://example.com/remote.yaml` 示例内容：

```yaml
another_test:
  stage: test
  script:
    - echo 2
```

响应示例：

```json
{
  "status": "valid",
  "errors": [],
  "merged_yaml": "---\n:another_test:\n  :stage: test\n  :script: echo 2\n:test:\n  :stage: test\n  :script: echo 1\n",
  "jobs": [
    {
      "name":"test",
      "stage":"test",
      "before_script":[],
      "script":["echo 1"],
      "after_script":[],
      "tag_list":[],
      "environment":null,
      "when":"on_success",
      "allow_failure":false,
      "only":{
        "refs":["branches","tags"]
      },
      "except":null
    },
    {
      "name":"another_test",
      "stage":"test",
      "before_script":[],
      "script":["echo 2"],
      "after_script":[],
      "tag_list":[],
      "environment":null,
      "when":"on_success",
      "allow_failure":false,
      "only":{
        "refs":["branches","tags"]
      },
      "except":null
    }
  ]
}
```

<!--- end_remove -->

## 使用 jq 创建并处理 YAML & JSON 负载

要将 YAML 配置 `POST` 到 CI Lint 端点，必须对其进行正确转义和 JSON 编码。
您可以使用 `jq` 和 `curl` 转义 YAML 并将其上传到极狐GitLab API。

### 为 JSON 编码进行转义

转义引号并在 JSON 负载中以适合嵌入的格式对 YAML 进行编码，您可以使用 `jq`。例如，创建一个名为 `example-gitlab-ci.yml` 的文件：

```yaml
.api_test:
  rules:
    - if: $CI_PIPELINE_SOURCE=="merge_request_event"
      changes:
        - src/api/*
deploy:
  extends:
    - .api_test
  rules:
    - when: manual
      allow_failure: true
  script:
    - echo "hello world"
```

使用 `jq` 进行转义并将 YAML 文件编码为 JSON：

```shell
jq --raw-input --slurp < example-gitlab-ci.yml
```

要转义和编码输入 YAML 文件（`example-gitlab-ci.yml`），并在单行命令中使用 `curl` 和 `jq` 将其 `POST` 到极狐GitLab API：

```shell
jq --null-input --arg yaml "$(<example-gitlab-ci.yml)" '.content=$yaml' \
| curl "https://gitlab.com/api/v4/ci/lint?include_merged_yaml=true" \
--header 'Content-Type: application/json' \
--data @-
```

### 解析 CI Lint 响应

要重定 CI Lint 响应的格式，您可以使用 `jq`。您可以将 CI Lint 响应通过管道传输到 `jq`，或将 API 响应存储为文本文件并将其作为参数提供：

```shell
jq --raw-output '.merged_yaml | fromjson' <your_input_here>
```

输入示例：

```json
{"status":"valid","errors":[],"merged_yaml":"---\n:.api_test:\n  :rules:\n  - :if: $CI_PIPELINE_SOURCE==\"merge_request_event\"\n    :changes:\n    - src/api/*\n:deploy:\n  :rules:\n  - :when: manual\n    :allow_failure: true\n  :extends:\n  - \".api_test\"\n  :script:\n  - echo \"hello world\"\n"}
```

变成：

```yaml
:.api_test:
  :rules:
  - :if: $CI_PIPELINE_SOURCE=="merge_request_event"
    :changes:
    - src/api/*
:deploy:
  :rules:
  - :when: manual
    :allow_failure: true
  :extends:
  - ".api_test"
  :script:
  - echo "hello world"
```

您可以使用单行命令：

1. 转义 YAML
1. 用 JSON 编码
1. 使用 curl 将其 POST 为 API
1. 设定响应的格式

```shell
jq --null-input --arg yaml "$(<example-gitlab-ci.yml)" '.content=$yaml' \
| curl "https://gitlab.com/api/v4/ci/lint?include_merged_yaml=true" \
--header 'Content-Type: application/json' --data @- \
| jq --raw-output '.merged_yaml | fromjson'
```
