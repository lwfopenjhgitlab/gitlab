---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 标签 API **(FREE)**

## 项目仓库标签的列表

> `order_by` 参数的 `version` 值引入于极狐GitLab 15.4。

获取一个项目仓库标签的列表，按照更新时间和日期倒排。如果项目是可以公开访问的，那么调用该 API 时不需要身份认证信息。

```plaintext
GET /projects/:id/repository/tags
```

参数：

| 参数 | 类型 | 是否必需 | 描述                                                                     |
| --------- | ---- | -------- |------------------------------------------------------------------------|
| `id` | integer/string| yes | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。            |
| `order_by` | string | no | 返回按照 `name`、`updated` 或 `version` 排序的标签。默认按照 `updated` 排序。             |
| `sort` | string | no | 按照 `asc` 或者 `desc` 排序标签列表。默认是 `desc`。                                  |
| `search` | string | no | 返回匹配搜索条件的标签列表。您可以用 `^term` 和 `term$` 查看以 `term` 开头或结尾的标签。其他正则表达式暂时不支持。 |

```json
[
  {
    "commit": {
      "id": "2695effb5807a22ff3d138d593fd856244e155e7",
      "short_id": "2695effb",
      "title": "Initial commit",
      "created_at": "2017-07-26T11:08:53.000+02:00",
      "parent_ids": [
        "2a4b78934375d7f53875269ffd4f45fd83a84ebe"
      ],
      "message": "Initial commit",
      "author_name": "John Smith",
      "author_email": "john@example.com",
      "authored_date": "2012-05-28T04:42:42-07:00",
      "committer_name": "Jack Smith",
      "committer_email": "jack@example.com",
      "committed_date": "2012-05-28T04:42:42-07:00"
    },
    "release": {
      "tag_name": "1.0.0",
      "description": "Amazing release. Wow"
    },
    "name": "v1.0.0",
    "target": "2695effb5807a22ff3d138d593fd856244e155e7",
    "message": null,
    "protected": true
  }
]
```

## 获取一个仓库标签

通过标签的名称获取一个仓库标签。如果项目是可以公开访问的，那么调用该 API 时不需要身份认证信息。

```plaintext
GET /projects/:id/repository/tags/:tag_name
```

参数：

| 参数 | 类型 | 是否必需 | 描述                                                            |
| --------- | ---- | -------- |---------------------------------------------------------------|
| `id` | integer/string | yes | 项目 ID 或者 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `tag_name` | string | yes | 标签的名称                                                         |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/tags/v1.0.0"
```

响应示例：

```json
{
  "name": "v5.0.0",
  "message": null,
  "target": "60a8ff033665e1207714d6670fcd7b65304ec02f",
  "commit": {
    "id": "60a8ff033665e1207714d6670fcd7b65304ec02f",
    "short_id": "60a8ff03",
    "title": "Initial commit",
    "created_at": "2017-07-26T11:08:53.000+02:00",
    "parent_ids": [
      "f61c062ff8bcbdb00e0a1b3317a91aed6ceee06b"
    ],
    "message": "v5.0.0\n",
    "author_name": "Arthur Verschaeve",
    "author_email": "contact@arthurverschaeve.be",
    "authored_date": "2015-02-01T21:56:31.000+01:00",
    "committer_name": "Arthur Verschaeve",
    "committer_email": "contact@arthurverschaeve.be",
    "committed_date": "2015-02-01T21:56:31.000+01:00"
  },
  "release": null,
  "protected": false
}
```

## 创建一个标签

在仓库中创建一个标签，指向提供的 ref。

```plaintext
POST /projects/:id/repository/tags
```

参数：

| 参数 | 类型 | 是否必需 | 描述                                                         |
| --------------------- | -------------- | -------- |------------------------------------------------------------|
| `id`                  | integer/string | yes      | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `tag_name`            | string         | yes      | 标签名称                                                       |
| `ref`                 | string         | yes      | 创建标签时，可以使用提交 SHA 值，另一个标签名称或者分支名称                           |
| `message`             | string         | no       | 创建有备注的标签                                                   |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/tags?tag_name=test&ref=master"
```

响应示例：

```json
{
  "commit": {
    "id": "2695effb5807a22ff3d138d593fd856244e155e7",
    "short_id": "2695effb",
    "title": "Initial commit",
    "created_at": "2017-07-26T11:08:53.000+02:00",
    "parent_ids": [
      "2a4b78934375d7f53875269ffd4f45fd83a84ebe"
    ],
    "message": "Initial commit",
    "author_name": "John Smith",
    "author_email": "john@example.com",
    "authored_date": "2012-05-28T04:42:42-07:00",
    "committer_name": "Jack Smith",
    "committer_email": "jack@example.com",
    "committed_date": "2012-05-28T04:42:42-07:00"
  },
  "release": null,
  "name": "v1.0.0",
  "target": "2695effb5807a22ff3d138d593fd856244e155e7",
  "message": null,
  "protected": false
}
```

当创建一个简易的标签时，message 可以是 `null`。否则 message 应包含备注信息。

当创建有备注的标签时，target 字段是标签对象 ID。当创建简易标签时，target 字段是提交 ID。

当错误发生时，返回值包含 HTTP 状态码 `405`和错误消息。

## 删除一个标签

删除指定名称的仓库标签。

```plaintext
DELETE /projects/:id/repository/tags/:tag_name
```

参数：

| 参数 | 类型 | 是否必需 | 描述      |
| ---------- | -------------- | -------- | -------------------------------------------------------------------------------------------------------------- |
| `id`       | integer/string | yes      | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `tag_name` | string         | yes      | 标签名称 |
