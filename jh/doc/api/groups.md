---
stage: Data Stores
group: Tenant Scale
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组 API **(FREE)**

使用 REST API 与[群组](../user/group/index.md)交互。

响应中返回的字段根据经过身份验证的用户的[权限](../user/permissions.md)而有所不同。

<a id="list-groups"></a>

## 列出群组

> 对键集分页的支持引入于极狐GitLab 14.3。

获取经过身份验证的用户的可见群组列表。进行未授权访问时，只返回公共群组。

默认情况下，此请求一次返回 20 个结果，因为 API 结果是[分页](rest/index.md#pagination)的。

当进行未授权访问时，此端点还支持[键集分页](rest/index.md#keyset-based-pagination)：

- 当请求连续的结果页时，您应该使用键集分页。
- 超出特定的偏移量限制（由[基于偏移分页的 REST API 允许的最大偏移](../administration/instance_limits.md#max-offset-allowed-by-the-rest-api-for-offset-based-pagination)指定)，偏移分页不可用。

参数：

| 参数                       | 类型                | 是否必需 | 描述                                                                                                                                                                                                                  |
|--------------------------|-------------------|------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `skip_groups`            | array of integers | no   | 跳过通过的群组 ID                                                                                                                                                                                                          |
| `all_available`          | boolean           | no   | 显示所有您可以访问的群组（对于经过身份验证的用户，默认为 `false`；对于管理员，默认为 `true`）<!--Attributes `owned` and `min_access_level` have precedence   -->                                                                                           |
| `search`                 | string            | no   | 返回匹配搜索条件的授权群组的列表                                                                                                                                                                                                    |
| `order_by`               | string            | no   | 根据 `name`、`path`、`id` 或 `similarity` 为群组排序（如果查询，引入于极狐GitLab 14.1）。默认为 `name`                                                                                                                                        |
| `sort`                   | string            | no   | 按照 `asc` 或 `desc` 顺序为群组排序。默认为 `asc`                                                                                                                                                                                 |
| `statistics`             | boolean           | no   | 包括群组统计（仅对管理员）。<br>*注意：* REST API 响应不提供 UI 中显示的完整 `RootStorageStatistics` 数据。要匹配 UI 中的数据，请使用 GraphQL 而不是 REST<!--For more information, see the [Group GraphQL reference](../api/graphql/reference/index.md#group)--> |
| `with_custom_attributes` | boolean           | no   | 响应中包括[自定义属性](custom_attributes.md)（仅对管理员）                                                                                                                                                                           |
| `owned`                  | boolean           | no   | 限制为当前用户明确拥有的群组                                                                                                                                                                                                      |
| `min_access_level`       | integer           | no   | 限制为当前用户至少有此[角色 (`access_level`)](members.md#roles) 的群组                                                                                                                                                              |
| `top_level_only`         | boolean           | no   | 限制为顶级群组，不包括所有子群组                                                                                                                                                                                                    |

```plaintext
GET /groups
```

```json
[
  {
    "id": 1,
    "name": "Foobar Group",
    "path": "foo-bar",
    "description": "An interesting group",
    "visibility": "public",
    "share_with_group_lock": false,
    "require_two_factor_authentication": false,
    "two_factor_grace_period": 48,
    "project_creation_level": "developer",
    "auto_devops_enabled": null,
    "subgroup_creation_level": "owner",
    "emails_disabled": null,
    "mentions_disabled": null,
    "lfs_enabled": true,
    "default_branch_protection": 2,
    "avatar_url": "http://localhost:3000/uploads/group/avatar/1/foo.jpg",
    "web_url": "http://localhost:3000/groups/foo-bar",
    "request_access_enabled": false,
    "full_name": "Foobar Group",
    "full_path": "foo-bar",
    "file_template_project_id": 1,
    "parent_id": null,
    "created_at": "2020-01-15T12:36:29.590Z"
  }
]
```

当添加参数 `statistics=true` 并且经过身份验证的用户是管理员，会返回额外群组统计。

```plaintext
GET /groups?statistics=true
```

```json
[
  {
    "id": 1,
    "name": "Foobar Group",
    "path": "foo-bar",
    "description": "An interesting group",
    "visibility": "public",
    "share_with_group_lock": false,
    "require_two_factor_authentication": false,
    "two_factor_grace_period": 48,
    "project_creation_level": "developer",
    "auto_devops_enabled": null,
    "subgroup_creation_level": "owner",
    "emails_disabled": null,
    "mentions_disabled": null,
    "lfs_enabled": true,
    "default_branch_protection": 2,
    "avatar_url": "http://localhost:3000/uploads/group/avatar/1/foo.jpg",
    "web_url": "http://localhost:3000/groups/foo-bar",
    "request_access_enabled": false,
    "full_name": "Foobar Group",
    "full_path": "foo-bar",
    "file_template_project_id": 1,
    "parent_id": null,
    "created_at": "2020-01-15T12:36:29.590Z",
    "statistics": {
      "storage_size": 363,
      "repository_size": 33,
      "wiki_size": 100,
      "lfs_objects_size": 123,
      "job_artifacts_size": 57,
      "pipeline_artifacts_size": 0,
      "packages_size": 0,
      "snippets_size": 50,
      "uploads_size": 0
    },
    "wiki_access_level": "private"
  }
]
```

[专业版或旗舰版](https://about.gitlab.cn/pricing/)的用户也可以看到 `wiki_access_level` 参数。

您可以通过名称或路径进行搜索，参见下文。

您可以使用以下内容通过[自定义属性](custom_attributes.md)进行过滤：

```plaintext
GET /groups?custom_attributes[key]=value&custom_attributes[other_key]=other_value
```

## 列出群组的子群组

获取群组中可见直接子群组的列表。

默认情况下，此请求一次返回 20 个结果，因为 API 结果是[分页](rest/index.md#pagination)的。

如果您将此列表请求为：

- 未经身份验证的用户，响应仅返回公共群组。
- 经过身份验证的用户，响应仅返回您所在的群组并且不包括公共群组。

参数：

| 参数                       | 类型                | 是否必需 | 描述                                                                                                                        |
|--------------------------|-------------------|------|---------------------------------------------------------------------------------------------------------------------------|
| `id`                     | integer/string    | yes  | 直系父群组的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                                         |
| `skip_groups`            | array of integers | no   | 跳过通过的群组 ID                                                                                                                |
| `all_available`          | boolean           | no   | 显示所有您可以访问的群组（对于经过身份验证的用户，默认为 `false`；对于管理员，默认为 `true`）<!--Attributes `owned` and `min_access_level` have precedence   --> |
| `search`                 | string            | no   | 返回匹配查询条件的授权群组列表。仅搜索子群组短路径（不是完整路径）                                                                                         |
| `order_by`               | string            | no   | 根据 `name`、`path` 或 `id` 为群组排序。默认为 `name`                                                                                  |
| `sort`                   | string            | no   | 按照 `asc` 或 `desc` 顺序为群组排序。默认为 `asc`                                                                                       |
| `statistics`             | boolean           | no   | 包括群组统计（仅为管理员）                                                                                                             |
| `with_custom_attributes` | boolean           | no   | 响应中包括[自定义属性](custom_attributes.md)（仅为管理员）                                                                                 |
| `owned`                  | boolean           | no   | 限制为当前用户明确拥有的群组                                                                                                            |
| `min_access_level`       | integer           | no   | 限制为当前用户至少有此[角色 (`access_level`)](members.md#roles) 的群组                                                                    |

```plaintext
GET /groups/:id/subgroups
```

```json
[
  {
    "id": 1,
    "name": "Foobar Group",
    "path": "foo-bar",
    "description": "An interesting group",
    "visibility": "public",
    "share_with_group_lock": false,
    "require_two_factor_authentication": false,
    "two_factor_grace_period": 48,
    "project_creation_level": "developer",
    "auto_devops_enabled": null,
    "subgroup_creation_level": "owner",
    "emails_disabled": null,
    "mentions_disabled": null,
    "lfs_enabled": true,
    "default_branch_protection": 2,
    "avatar_url": "http://gitlab.example.com/uploads/group/avatar/1/foo.jpg",
    "web_url": "http://gitlab.example.com/groups/foo-bar",
    "request_access_enabled": false,
    "full_name": "Foobar Group",
    "full_path": "foo-bar",
    "file_template_project_id": 1,
    "parent_id": 123,
    "created_at": "2020-01-15T12:36:29.590Z"
  }
]
```

[专业版或旗舰版](https://about.gitlab.cn/pricing/)的用户也可以看到 `wiki_access_level` 参数。

## 列出群组的下级群组

> 引入于极狐GitLab 13.5。

获取该群组的可见下级群组列表。当进行未授权的访问时，仅返回公共组。

默认情况下，此请求一次返回 20 个结果，因为 API 结果是[分页](rest/index.md#pagination)的。

参数：

| 参数                       | 类型                | 是否必需 | 描述                                                                                                                        |
|--------------------------|-------------------|------|---------------------------------------------------------------------------------------------------------------------------|
| `id`                     | integer/string    | yes  | 直系父群组的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                                         |
| `skip_groups`            | array of integers | no   | 跳过通过的群组 ID                                                                                                                |
| `all_available`          | boolean           | no   | 显示所有您可以访问的群组（对于经过身份验证的用户，默认为 `false`；对于管理员，默认为 `true`）<!--Attributes `owned` and `min_access_level` have precedence   --> |
| `search`                 | string            | no   | 返回匹配查询条件的授权群组列表。仅搜索下级群组短路径（不是完整路径）                                                                                        |
| `order_by`               | string            | no   | 根据 `name`、`path` 或 `id` 为群组排序。默认为 `name`                                                                                  |
| `sort`                   | string            | no   | 按照 `asc` 或 `desc` 顺序为群组排序。默认为 `asc`                                                                                       |
| `statistics`             | boolean           | no   | 包括群组统计（仅为管理员）                                                                                                             |
| `with_custom_attributes` | boolean           | no   | 响应中包括[自定义属性](custom_attributes.md)（仅为管理员）                                                                                 |
| `owned`                  | boolean           | no   | 限制为当前用户明确拥有的群组                                                                                                            |
| `min_access_level`       | integer           | no   | 限制为当前用户至少有此[角色 (`access_level`)](members.md#roles) 的群组                                                                    |

```plaintext
GET /groups/:id/descendant_groups
```

```json
[
  {
    "id": 2,
    "name": "Bar Group",
    "path": "bar",
    "description": "A subgroup of Foo Group",
    "visibility": "public",
    "share_with_group_lock": false,
    "require_two_factor_authentication": false,
    "two_factor_grace_period": 48,
    "project_creation_level": "developer",
    "auto_devops_enabled": null,
    "subgroup_creation_level": "owner",
    "emails_disabled": null,
    "mentions_disabled": null,
    "lfs_enabled": true,
    "default_branch_protection": 2,
    "avatar_url": "http://gitlab.example.com/uploads/group/avatar/1/bar.jpg",
    "web_url": "http://gitlab.example.com/groups/foo/bar",
    "request_access_enabled": false,
    "full_name": "Bar Group",
    "full_path": "foo/bar",
    "file_template_project_id": 1,
    "parent_id": 123,
    "created_at": "2020-01-15T12:36:29.590Z"
  },
  {
    "id": 3,
    "name": "Baz Group",
    "path": "baz",
    "description": "A subgroup of Bar Group",
    "visibility": "public",
    "share_with_group_lock": false,
    "require_two_factor_authentication": false,
    "two_factor_grace_period": 48,
    "project_creation_level": "developer",
    "auto_devops_enabled": null,
    "subgroup_creation_level": "owner",
    "emails_disabled": null,
    "mentions_disabled": null,
    "lfs_enabled": true,
    "default_branch_protection": 2,
    "avatar_url": "http://gitlab.example.com/uploads/group/avatar/1/baz.jpg",
    "web_url": "http://gitlab.example.com/groups/foo/bar/baz",
    "request_access_enabled": false,
    "full_name": "Baz Group",
    "full_path": "foo/bar/baz",
    "file_template_project_id": 1,
    "parent_id": 123,
    "created_at": "2020-01-15T12:36:29.590Z"
  }
]
```

[专业版或旗舰版](https://about.gitlab.cn/pricing/)的用户也可以看到 `wiki_access_level` 参数。

<a id="list-a-groups-projects"></a>

## 列出群组项目

获取该群组中的项目列表。进行未授权访问时，仅返回公共项目。

默认情况下，此请求一次返回 20 个结果，因为 API 结果是[分页](rest/index.md#pagination)的。

```plaintext
GET /groups/:id/projects
```

参数：

| 参数                                     | 类型             | 是否必需 | 描述                                                                                                              |
|----------------------------------------|----------------|------|-----------------------------------------------------------------------------------------------------------------|
| `id`                                   | integer/string | yes  | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                                         |
| `archived`                             | boolean        | no   | 受归档状态限制                                                                                                         |
| `visibility`                           | string         | no   | 受可见性 `public`、`internal` 或 `private` 限制                                                                         |
| `order_by`                             | string         | no   | 返回按照 `id`、`name`、`path`、`created_at`、`updated_at`、`similarity` （1）或 `last_activity_at` 字段排序的项目。默认为 `created_at` |
| `sort`                                 | string         | no   | 返回按照 `asc` 或 `desc` 顺序排序的项目。默认为 `desc`                                                                          |
| `search`                               | string         | no   | 返回匹配搜索条件的授权项目列表                                                                                                 |
| `simple`                               | boolean        | no   | 仅返回每个项目的有限字段。这是一个没有身份验证的空操作，只返回简单的字段                                                                            |
| `owned`                                | boolean        | no   | 受限于当前用户拥有的项目                                                                                                    |
| `starred`                              | boolean        | no   | 受限于当前用户星标的项目                                                                                                    |
| `with_issues_enabled`                  | boolean        | no   | 受限于开启议题功能的项目。默认为 `false`                                                                                        |
| `with_merge_requests_enabled`          | boolean        | no   | 受限于开启合并请求功能的项目。默认为 `false`                                                                                      |
| `with_shared`                          | boolean        | no   | 包括共享给此群组的项目。默认为 `true`                                                                                          |
| `include_subgroups`                    | boolean        | no   | 包括此群组的子群组中的项目。默认为 `false`                                                                                       |
| `min_access_level`                     | integer        | no   | 限制为当前用户至少有此[角色 (`access_level`)](members.md#roles) 的项目                                                          |
| `with_custom_attributes`               | boolean        | no   | 包括响应中的[自定义属性](custom_attributes.md)（仅对管理员）                                                                      |
| `with_security_reports` **(ULTIMATE)** | boolean        | no   | 仅返回构建中有安全报告产物的项目。也就是说"开启安全报告的项目"。默认为 `false`                                                                    |


1. 按相似度排序：按照从提供的 `search` URL 参数计算的相似度分数对结果进行排序。
   使用 `order_by=similarity` 时，会忽略 `sort` 参数。当未提供 `search` 参数时，API 返回按 `name` 排序的项目。

响应示例：

```json
[
  {
    "id": 9,
    "description": "foo",
    "default_branch": "master",
    "tag_list": [], //deprecated, use `topics` instead
    "topics": [],
    "archived": false,
    "visibility": "internal",
    "ssh_url_to_repo": "git@gitlab.example.com/html5-boilerplate.git",
    "http_url_to_repo": "http://gitlab.example.com/h5bp/html5-boilerplate.git",
    "web_url": "http://gitlab.example.com/h5bp/html5-boilerplate",
    "name": "Html5 Boilerplate",
    "name_with_namespace": "Experimental / Html5 Boilerplate",
    "path": "html5-boilerplate",
    "path_with_namespace": "h5bp/html5-boilerplate",
    "issues_enabled": true,
    "merge_requests_enabled": true,
    "wiki_enabled": true,
    "jobs_enabled": true,
    "snippets_enabled": true,
    "created_at": "2016-04-05T21:40:50.169Z",
    "last_activity_at": "2016-04-06T16:52:08.432Z",
    "shared_runners_enabled": true,
    "creator_id": 1,
    "namespace": {
      "id": 5,
      "name": "Experimental",
      "path": "h5bp",
      "kind": "group"
    },
    "avatar_url": null,
    "star_count": 1,
    "forks_count": 0,
    "open_issues_count": 3,
    "public_jobs": true,
    "shared_with_groups": [],
    "request_access_enabled": false
  }
]
```

NOTE:
可以使用 `namespace` 参数区分群组中的项目和共享给群组的项目。当一个项目已被共享到该群组时，它的 `namespace` 与请求所针对的群组不同。

<a id="list-a-groups-shared-projects"></a>

## 列出群组的共享项目

获取共享给此群组的项目列表。进行未授权访问时，仅返回公共共享项目。

默认情况下，此请求一次返回 20 个结果，因为 API 结果是[分页](rest/index.md#pagination)的。

```plaintext
GET /groups/:id/projects/shared
```

参数：

| 参数                            | 类型             | 是否必需 | 描述                                                                                              |
|-------------------------------|----------------|------|-------------------------------------------------------------------------------------------------|
| `id`                          | integer/string | yes  | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                         |
| `archived`                    | boolean        | no   | 受归档状态限制                                                                                         |
| `visibility`                  | string         | no   | 受可见性 `public`、`internal` 或 `private` 限制                                                         |
| `order_by`                    | string         | no   | 返回按照 `id`、`name`、`path`、`created_at`、`updated_at` 或 `last_activity_at` 字段排序的项目。默认为 `created_at` |
| `sort`                        | string         | no   | 返回按照 `asc` 或 `desc` 顺序排序的项目。默认为 `desc`                                                          |
| `search`                      | string         | no   | 返回匹配搜索条件的授权项目列表                                                                                 |
| `simple`                      | boolean        | no   | 仅返回每个项目的有限字段。这是一个没有身份验证的空操作，只返回简单的字段                                                            |
| `starred`                     | boolean        | no   | 受限于当前用户星标的项目                                                                                    |
| `with_issues_enabled`         | boolean        | no   | 受限于开启议题功能的项目。默认为 `false`                                                                        |
| `with_merge_requests_enabled` | boolean        | no   | 受限于开启合并请求功能的项目。默认为 `false`                                                                      |
| `min_access_level`            | integer        | no   | 限制为当前用户至少有此[角色 (`access_level`)](members.md#roles) 的项目                                          |
| `with_custom_attributes`      | boolean        | no   | 包括响应中的[自定义属性](custom_attributes.md)（仅对管理员）                                                      |

响应示例：

```json
[
  {
    "id":8,
    "description":"Shared project for Html5 Boilerplate",
    "name":"Html5 Boilerplate",
    "name_with_namespace":"H5bp / Html5 Boilerplate",
    "path":"html5-boilerplate",
    "path_with_namespace":"h5bp/html5-boilerplate",
    "created_at":"2020-04-27T06:13:22.642Z",
    "default_branch":"master",
    "tag_list":[], //deprecated, use `topics` instead
    "topics":[],
    "ssh_url_to_repo":"ssh://git@gitlab.com/h5bp/html5-boilerplate.git",
    "http_url_to_repo":"https://gitlab.com/h5bp/html5-boilerplate.git",
    "web_url":"https://gitlab.com/h5bp/html5-boilerplate",
    "readme_url":"https://gitlab.com/h5bp/html5-boilerplate/-/blob/master/README.md",
    "avatar_url":null,
    "star_count":0,
    "forks_count":4,
    "last_activity_at":"2020-04-27T06:13:22.642Z",
    "namespace":{
      "id":28,
      "name":"H5bp",
      "path":"h5bp",
      "kind":"group",
      "full_path":"h5bp",
      "parent_id":null,
      "avatar_url":null,
      "web_url":"https://gitlab.com/groups/h5bp"
    },
    "_links":{
      "self":"https://gitlab.com/api/v4/projects/8",
      "issues":"https://gitlab.com/api/v4/projects/8/issues",
      "merge_requests":"https://gitlab.com/api/v4/projects/8/merge_requests",
      "repo_branches":"https://gitlab.com/api/v4/projects/8/repository/branches",
      "labels":"https://gitlab.com/api/v4/projects/8/labels",
      "events":"https://gitlab.com/api/v4/projects/8/events",
      "members":"https://gitlab.com/api/v4/projects/8/members"
    },
    "empty_repo":false,
    "archived":false,
    "visibility":"public",
    "resolve_outdated_diff_discussions":false,
    "container_registry_enabled":true,
    "container_expiration_policy":{
      "cadence":"7d",
      "enabled":true,
      "keep_n":null,
      "older_than":null,
      "name_regex":null,
      "name_regex_keep":null,
      "next_run_at":"2020-05-04T06:13:22.654Z"
    },
    "issues_enabled":true,
    "merge_requests_enabled":true,
    "wiki_enabled":true,
    "jobs_enabled":true,
    "snippets_enabled":true,
    "can_create_merge_request_in":true,
    "issues_access_level":"enabled",
    "repository_access_level":"enabled",
    "merge_requests_access_level":"enabled",
    "forking_access_level":"enabled",
    "wiki_access_level":"enabled",
    "builds_access_level":"enabled",
    "snippets_access_level":"enabled",
    "pages_access_level":"enabled",
    "security_and_compliance_access_level":"enabled",
    "emails_disabled":null,
    "shared_runners_enabled":true,
    "lfs_enabled":true,
    "creator_id":1,
    "import_status":"failed",
    "open_issues_count":10,
    "ci_default_git_depth":50,
    "ci_forward_deployment_enabled":true,
    "ci_allow_fork_pipelines_to_run_in_parent_project":true,
    "public_jobs":true,
    "build_timeout":3600,
    "auto_cancel_pending_pipelines":"enabled",
    "ci_config_path":null,
    "shared_with_groups":[
      {
        "group_id":24,
        "group_name":"Commit451",
        "group_full_path":"Commit451",
        "group_access_level":30,
        "expires_at":null
      }
    ],
    "only_allow_merge_if_pipeline_succeeds":false,
    "request_access_enabled":true,
    "only_allow_merge_if_all_discussions_are_resolved":false,
    "remove_source_branch_after_merge":true,
    "printing_merge_request_link_enabled":true,
    "merge_method":"merge",
    "suggestion_commit_message":null,
    "auto_devops_enabled":true,
    "auto_devops_deploy_strategy":"continuous",
    "autoclose_referenced_issues":true,
    "repository_storage":"default"
  }
]
```

<a id="details-of-a-group"></a>

## 群组详细信息

> `membership_lock` 字段引入于极狐GitLab 14.10。

获取群组的所有详细信息。如果该群组可公开访问，则无需授权即可访问此端点。
以防如果该群组可公开访问，请求用户是管理员。如果用户是管理员或群组所有者，经过授权，则返回 `runners_token`。

```plaintext
GET /groups/:id
```

参数：

| 参数                       | 类型             | 是否必需 | 描述                                                                                                                                        |
|--------------------------|----------------|------|-------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                     | integer/string | yes  | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                                                                             |
| `with_custom_attributes` | boolean        | no   | 响应中包括[自定义属性](custom_attributes.md)（仅对管理员）                                                                                                 |
| `with_projects`          | boolean        | no   | 包括属于特定群组的项目的详细信息（默认为 `true`）。（已废弃，计划在 API v5 中移除。获取群组内所有项目的详细信息，请使用[列出群组的项目端点](#list-a-groups-projects)) |

NOTE:
项目中的 `projects` 和 `shared_projects` 参数已被废弃，并且计划在 API v5 中移除。
获取群组内所有项目的详细信息，请使用[列出群组中的项目](#list-a-groups-projects)或[列出群组的共享项目](#list-a-groups-shared-projects)端点。

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/4"
```

此端点返回：

- 极狐GitLab 12.5 及更早版本中的所有项目和共享项目。
- 极狐GitLab 12.6 及更高版本中最多 100 个项目和共享项目。要获取群组内所有项目的详细信息，请使用[列出群组的项目端点](#list-a-groups-projects)。

- 响应示例：

```json
{
  "id": 4,
  "name": "Twitter",
  "path": "twitter",
  "description": "Aliquid qui quis dignissimos distinctio ut commodi voluptas est.",
  "visibility": "public",
  "avatar_url": null,
  "web_url": "https://gitlab.example.com/groups/twitter",
  "request_access_enabled": false,
  "full_name": "Twitter",
  "full_path": "twitter",
  "runners_token": "ba324ca7b1c77fc20bb9",
  "file_template_project_id": 1,
  "parent_id": null,
  "created_at": "2020-01-15T12:36:29.590Z",
  "shared_with_groups": [
    {
      "group_id": 28,
      "group_name": "H5bp",
      "group_full_path": "h5bp",
      "group_access_level": 20,
      "expires_at": null
    }
  ],
  "prevent_sharing_groups_outside_hierarchy": false,
  "projects": [ // Deprecated and will be removed in API v5
    {
      "id": 7,
      "description": "Voluptas veniam qui et beatae voluptas doloremque explicabo facilis.",
      "default_branch": "master",
      "tag_list": [], //deprecated, use `topics` instead
      "topics": [],
      "archived": false,
      "visibility": "public",
      "ssh_url_to_repo": "git@gitlab.example.com:twitter/typeahead-js.git",
      "http_url_to_repo": "https://gitlab.example.com/twitter/typeahead-js.git",
      "web_url": "https://gitlab.example.com/twitter/typeahead-js",
      "name": "Typeahead.Js",
      "name_with_namespace": "Twitter / Typeahead.Js",
      "path": "typeahead-js",
      "path_with_namespace": "twitter/typeahead-js",
      "issues_enabled": true,
      "merge_requests_enabled": true,
      "wiki_enabled": true,
      "jobs_enabled": true,
      "snippets_enabled": false,
      "container_registry_enabled": true,
      "created_at": "2016-06-17T07:47:25.578Z",
      "last_activity_at": "2016-06-17T07:47:25.881Z",
      "shared_runners_enabled": true,
      "creator_id": 1,
      "namespace": {
        "id": 4,
        "name": "Twitter",
        "path": "twitter",
        "kind": "group"
      },
      "avatar_url": null,
      "star_count": 0,
      "forks_count": 0,
      "open_issues_count": 3,
      "public_jobs": true,
      "shared_with_groups": [],
      "request_access_enabled": false
    },
    {
      "id": 6,
      "description": "Aspernatur omnis repudiandae qui voluptatibus eaque.",
      "default_branch": "master",
      "tag_list": [], //deprecated, use `topics` instead
      "topics": [],
      "archived": false,
      "visibility": "internal",
      "ssh_url_to_repo": "git@gitlab.example.com:twitter/flight.git",
      "http_url_to_repo": "https://gitlab.example.com/twitter/flight.git",
      "web_url": "https://gitlab.example.com/twitter/flight",
      "name": "Flight",
      "name_with_namespace": "Twitter / Flight",
      "path": "flight",
      "path_with_namespace": "twitter/flight",
      "issues_enabled": true,
      "merge_requests_enabled": true,
      "wiki_enabled": true,
      "jobs_enabled": true,
      "snippets_enabled": false,
      "container_registry_enabled": true,
      "created_at": "2016-06-17T07:47:24.661Z",
      "last_activity_at": "2016-06-17T07:47:24.838Z",
      "shared_runners_enabled": true,
      "creator_id": 1,
      "namespace": {
        "id": 4,
        "name": "Twitter",
        "path": "twitter",
        "kind": "group"
      },
      "avatar_url": null,
      "star_count": 0,
      "forks_count": 0,
      "open_issues_count": 8,
      "public_jobs": true,
      "shared_with_groups": [],
      "request_access_enabled": false
    }
  ],
  "shared_projects": [ // Deprecated and will be removed in API v5
    {
      "id": 8,
      "description": "Velit eveniet provident fugiat saepe eligendi autem.",
      "default_branch": "master",
      "tag_list": [], //deprecated, use `topics` instead
      "topics": [],
      "archived": false,
      "visibility": "private",
      "ssh_url_to_repo": "git@gitlab.example.com:h5bp/html5-boilerplate.git",
      "http_url_to_repo": "https://gitlab.example.com/h5bp/html5-boilerplate.git",
      "web_url": "https://gitlab.example.com/h5bp/html5-boilerplate",
      "name": "Html5 Boilerplate",
      "name_with_namespace": "H5bp / Html5 Boilerplate",
      "path": "html5-boilerplate",
      "path_with_namespace": "h5bp/html5-boilerplate",
      "issues_enabled": true,
      "merge_requests_enabled": true,
      "wiki_enabled": true,
      "jobs_enabled": true,
      "snippets_enabled": false,
      "container_registry_enabled": true,
      "created_at": "2016-06-17T07:47:27.089Z",
      "last_activity_at": "2016-06-17T07:47:27.310Z",
      "shared_runners_enabled": true,
      "creator_id": 1,
      "namespace": {
        "id": 5,
        "name": "H5bp",
        "path": "h5bp",
        "kind": "group"
      },
      "avatar_url": null,
      "star_count": 0,
      "forks_count": 0,
      "open_issues_count": 4,
      "public_jobs": true,
      "shared_with_groups": [
        {
          "group_id": 4,
          "group_name": "Twitter",
          "group_full_path": "twitter",
          "group_access_level": 30,
          "expires_at": null
        },
        {
          "group_id": 3,
          "group_name": "Gitlab Org",
          "group_full_path": "gitlab-org",
          "group_access_level": 10,
          "expires_at": "2018-08-14"
        }
      ]
    }
  ],
  "ip_restriction_ranges": null
}
```

`prevent_sharing_groups_outside_hierarchy` 参数仅在顶级群组中。

[专业版及旗舰版](https://about.gitlab.cn/pricing/)的用户还可以查看以下参数：

- `shared_runners_minutes_limit`
- `extra_shared_runners_minutes_limit`
- `marked_for_deletion_on`
- `membership_lock`
- `wiki_access_level`

额外响应参数：

```json
{
  "id": 4,
  "description": "Aliquid qui quis dignissimos distinctio ut commodi voluptas est.",
  "shared_runners_minutes_limit": 133,
  "extra_shared_runners_minutes_limit": 133,
  "marked_for_deletion_on": "2020-04-03",
  "membership_lock": false,
  "wiki_access_level": "disabled",
  ...
}
```

添加参数 `with_projects=false` 时，不会返回项目。

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/4?with_projects=false"
```

响应示例：

```json
{
  "id": 4,
  "name": "Twitter",
  "path": "twitter",
  "description": "Aliquid qui quis dignissimos distinctio ut commodi voluptas est.",
  "visibility": "public",
  "avatar_url": null,
  "web_url": "https://gitlab.example.com/groups/twitter",
  "request_access_enabled": false,
  "full_name": "Twitter",
  "full_path": "twitter",
  "file_template_project_id": 1,
  "parent_id": null
}
```

### 下载群组头像

获取群组头像。如果群组可公开访问，则可以不经授权访问此端点。

```plaintext
GET /groups/:id/avatar
```

| 参数   | 类型             | 是否必需 | 描述                 |
|------|----------------|------|--------------------|
| `id` | integer/string | yes  | 群组 ID |

示例：

```shell
curl --header "PRIVATE-TOKEN: $GITLAB_LOCAL_TOKEN" \
  --remote-header-name \
  --remote-name \
  "https://gitlab.example.com/api/v4/groups/4/avatar"
```

### 禁用结果限制 **(FREE SELF)**

100 个结果的限制可能会破坏使用极狐GitLab 12.4 及更早版本所开发的集成。

对于极狐GitLab 12.5 到 13.12，可以使用[列出群组的项目](#list-a-groups-projects)端点在迁移中禁用限制。

请求具有 Rails 控制台访问权限的极狐GitLab 管理员运行以下命令：

```ruby
Feature.disable(:limit_projects_in_groups_api)
```

对于极狐GitLab 14.0 及更高版本，无法禁用限制。

<a id="new-group"></a>

## 新建群组

NOTE:
在极狐GitLab SaaS 上，您必须使用极狐GitLab UI 创建没有父群组的群组。您不能使用 API 来执行此操作。

创建一个新的项目组。仅适用于可以创建群组的用户。

```plaintext
POST /groups
```

参数：

| 参数                                                      | 类型      | 是否必需 | 描述                                                                                                       |
|---------------------------------------------------------|---------|------|----------------------------------------------------------------------------------------------------------|
| `name`                                                  | string  | yes  | 群组名称                                                                                                     |
| `path`                                                  | string  | yes  | 群组路径                                                                                                     |
| `auto_devops_enabled`                                   | boolean | no   | 默认为群组内所有项目的 Auto DevOps 流水线                                                                              |
| `avatar`                                                | mixed   | no   | 群组头像的镜像文件。引入于极狐GitLab 12.9                                                                               |
| `default_branch_protection`                             | integer | no   | 参见 [`default_branch_protection` 选项](#options-for-default_branch_protection)。默认为全局级别默认分支保护设置              |
| `description`                                           | string  | no   | 群组描述                                                                                                     |
| `emails_disabled`                                       | boolean | no   | 禁用电子邮件通知                                                                                                 |
| `lfs_enabled`                                           | boolean | no   | 为群组中的项目启用/禁用大型文件存储（LFS）                                                                                  |
| `mentions_disabled`                                     | boolean | no   | 禁用群组被提及的能力                                                                                               |
| `parent_id`                                             | integer | no   | 创建嵌套群组的父群组 ID                                                                                            |
| `project_creation_level`                                | string  | no   | 开发者是否能够在群组中创建项目。可以是 `noone`（没人可以）、`maintainer`（拥有维护者角色的用户）或 `developer`（拥有开发者或维护者角色的用户）                  |
| `request_access_enabled`                                | boolean | no   | 允许用户请求成员访问权限                                                                                             |
| `require_two_factor_authentication`                     | boolean | no   | 要求群组中的所有用户设置两步验证                                                                                         |
| `share_with_group_lock`                                 | boolean | no   | 防止在群组中与另一个群组共享项目                                                                                         |
| `subgroup_creation_level`                               | string  | no   | 允许[创建子群组](../user/group/subgroups/index.md#create-a-subgroup)。可以是 `owner`（拥有者）或 `maintainer`（拥有维护者角色的用户） |
| `two_factor_grace_period`                               | integer | no   | 执行两步验证之前的时间（小时）                                                                                          |
| `visibility`                                            | string  | no   | 群组可见性：`private`、`internal` 或 `public`                                                                    |
| `membership_lock` **(PREMIUM)**                         | boolean | no   | 用户无法到此群组的项目中                                                                                             |
| `extra_shared_runners_minutes_limit` **(PREMIUM SELF)** | integer | no   | 仅可由管理员添加。此群组的额外计算单元                                                                                      |
| `shared_runners_minutes_limit` **(PREMIUM SELF)**           | integer | no   | 仅可由管理员添加。此群组每月最大的计算单元。可以是 `nil`（默认，继承系统默认、`0`（无限）或 `> 0` ）                                               |
| `wiki_access_level` **(PREMIUM)**                            | string  | no       | Wiki 访问级别，可以是 `disabled`、`private` 或 `enabled`                                                           |

<a id="options-for-default_branch_protection"></a>

### `default_branch_protection` 选项

`default_branch_protection` 参数确定具有开发者或维护者角色的用户是否可以推送到适用的[默认分支](../user/project/repository/branches/default.md)，如下表所述：

| 值   | 描述                                                                                                                                                                                                                                                                                                                                                                                                                                     |
|-----|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `0` | 无保护。具有开发者或维护者角色的用户可以：<br>- 推送新提交<br>- 强制推送<br>- 删除分支                                                                                                                                                                                                                                                                                                                                                                                   |
| `1` | 部分保护。具有开发者或维护者角色的用户可以：<br>- 推送新提交                                                                                                                                                                                                                                                                                                                                                                                                      |
| `2` | 完整保护。只有具有维护者角色的用户可以：<br>- 推送新提交                                                                                                                                                                                                                                                                                                                                                                                                        |
| `3` | 保护以防止推送。具有维护者角色的用户可以：<br>- 推送新提交<br>- 强制推送<br>- 接收合并请求<br>具有开发者角色的用户可以：<br>- 接受合并请求                                                                                                                                                                                                                                                                                                                                                    |
| `4`   | 防止除初始推送之外的推送。具有开发者权限的用户可以：<br>- 将提交推送到空仓库。<br> 具有维护者角色的用户可以：<br>- 推送新提交<br>- 强制推送更改<br>- 接受合并请求<br >具有开发者角色的用户可以：<br>- 接受合并请求 |


## 新建子群组

新建子群组与[新建群组](#new-group)类似。您需要[列出群组](#list-groups)调用中的 `parent_id`。然后您可以输入以下内容：

- `subgroup_path`
- `subgroup_name`

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --header "Content-Type: application/json" \
     --data '{"path": "<subgroup_path>", "name": "<subgroup_name>", "parent_id": <parent_group_id> }' \
     "https://gitlab.example.com/api/v4/groups/"
```

## 向群组转移项目

将项目转移到群组命名空间。仅对实例管理员可用，尽管不需要管理员实例访问权限的[替代 API 端点](projects.md#transfer-a-project-to-a-new-namespace)也可用。
当项目仓库中存在打标签的包时，传输项目可能会失败。

```plaintext
POST  /groups/:id/projects/:project_id
```

参数：

| 参数           | 类型             | 是否必需 | 描述                                                      |
|--------------|----------------|------|---------------------------------------------------------|
| `id`         | integer/string | yes  | ID 或 [URL 编码的目标群组路径](rest/index.md#namespaced-path-encoding) |
| `project_id` | integer/string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)   |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/4/projects/56"
```

## 获取用户可以将群组转移为的群组

> 引入于极狐GitLab 15.4。

检索用户可以将群组转移到的群组列表。

```plaintext
GET /groups/:id/transfer_locations
```

| 参数       | 类型                | 是否必需                   | 描述                                                         |
|----------|-------------------|------------------------|------------------------------------------------------------|
| `id`     | integer or string | **{check-circle}** Yes | ID 或 [URL 编码的要转移的群组路径 ](rest/index.md#namespaced-path-encoding). |
| `search` | string            | **{dotted-circle}** No | 要搜索的群组名称                     |

请求示例：

```shell
curl --request GET "https://gitlab.example.com/api/v4/groups/1/transfer_locations"
```

响应示例：

```json
[
  {
    "id": 27,
    "web_url": "https://gitlab.example.com/groups/gitlab",
    "name": "GitLab",
    "avatar_url": null,
    "full_name": "GitLab",
    "full_path": "GitLab"
  },
  {
    "id": 31,
    "web_url": "https://gitlab.example.com/groups/foobar",
    "name": "FooBar",
    "avatar_url": null,
    "full_name": "FooBar",
    "full_path": "FooBar"
  }
]
```

## 向新父群组转移群组/将子群组变更为顶级群组

> 引入于极狐GitLab 14.6。

向新父群组转移群组或将子群组变更为顶级群组。管理员和用户可用：

- 具有要转移的群组的所有者角色。
- 如果转移群组，则有权在新的父群组中[创建子群组](../user/group/subgroups/index.md#create-a-subgroup)。
- 如果将子群组转换为顶级群组，则具有创建顶级群组的权限<!--[创建顶级群组的权限](../administration/user_settings.md#prevent-users-from-creating-top-level-groups)-->。

```plaintext
POST  /groups/:id/transfer
```

参数：

| 参数         | 类型      | 是否必需 | 描述                                                                                                                                          |
|------------|---------|------|---------------------------------------------------------------------------------------------------------------------------------------------|
| `id`       | integer | yes  | 要转移群组的 ID                                                                                                                                   |
| `group_id` | integer | no   | 新父群组的 ID。如果未指定，则将要转移的群组改为顶级组 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/4/transfer?group_id=7"
```

## 更新群组

> `unique_project_download_limit`、`unique_project_download_limit_interval_in_seconds` 和 `unique_project_download_limit_allowlist` 引入于极狐GitLab 15.3，[功能标志](../administration/feature_flags.md)为 `limit_unique_project_downloads_per_namespace_user`。默认禁用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下 `unique_project_download_limit`、`unique_project_download_limit_interval_in_seconds`、`unique_project_download_limit_allowlist` 和 `auto_ban_user_on_excessive_projects_download` 不可用。
要使它们可用，请让管理员启用名为 `limit_unique_project_downloads_per_namespace_user` 的[功能标志](../administration/feature_flags.md)。

更新项目群组。仅对群组拥有者和管理员可用。

```plaintext
PUT /groups/:id
```

| 参数                                                                 | 类型      | 是否必需 | 描述                                                                                                                                           |
|--------------------------------------------------------------------|---------|------|----------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                                                               | integer | yes  | 群组 ID                                                                                                                                        |
| `name`                                                             | string  | no   | 群组名称                                                                                                                                         |
| `path`                                                             | string  | no   | 群组路径                                                                                                                                         |
| `auto_devops_enabled`                                              | boolean | no   | 默认为群组内所有项目的 Auto DevOps 流水线                                                                                                                  |
| `avatar`                                                           | mixed   | no   | 群组头像的镜像文件。引入于极狐GitLab 12.9                                                                                                                   |
| `default_branch_protection`                                        | integer | no   | 参见 [`default_branch_protection` 选项](#options-for-default_branch_protection)                                                                  |
| `description`                                                      | string  | no   | 群组描述                                                                                                                                         |
| `emails_disabled`                                                  | boolean | no   | 禁用电子邮件通知                                                                                                                                     |
| `lfs_enabled`                                                      | boolean | no   | 为群组中的项目启用/禁用大型文件存储（LFS）                                                                                                                      |
| `mentions_disabled`                                                | boolean | no   | 禁用群组被提及的能力                                                                                                                                   |
| `prevent_sharing_groups_outside_hierarchy`                         | boolean | no   | 参见[防止群组层次结构外的群组共享](../user/group/index.md#prevent-group-sharing-outside-the-group-hierarchy)。此参数仅对顶级群组可用。引入于极狐GitLab 14.1                    |
| `project_creation_level`                                           | string  | no   | 开发者是否能够在群组中创建项目。可以是 `noone`（没人可以）、`maintainer`（拥有维护者角色的用户）或 `developer`（拥有开发者或维护者角色的用户）                                                      |
| `request_access_enabled`                                           | boolean | no   | 允许用户请求成员访问权限                                                                                                                                 |
| `require_two_factor_authentication`                                | boolean | no   | 要求群组中的所有用户设置两步验证                                                                                                                             |
| `shared_runners_setting`                                           | string  | no   | 参见 [`shared_runners_setting` 选项](#options-for-shared_runners_setting)。启用或禁用群组的子群组和项目的共享 Runner                                               |
| `share_with_group_lock`                                            | boolean | no   | 防止在群组中与另一个群组共享项目                                                                                                                             |
| `subgroup_creation_level`                                          | string  | no   | 允许[创建子群组](../user/group/subgroups/index.md#create-a-subgroup)。可以是 `owner`（拥有者）或 `maintainer`（拥有维护者角色的用户）                                     |
| `two_factor_grace_period`                                          | integer | no   | 执行两步验证之前的时间（小时）                                                                                                                              |
| `visibility`                                                       | string  | no   | 群组可见性：`private`、`internal` 或 `public`                                                                                                        |
| `extra_shared_runners_minutes_limit` **(PREMIUM SELF)**            | integer | no   | 仅可由管理员设置。此群组的额外计算单元                                                                                                                          |
| `file_template_project_id` **(PREMIUM)**                           | integer | no   | 加载自定义文件模板的项目 ID                                                                                                                              |
| `membership_lock` **(PREMIUM)**                                    | boolean | no   | 用户无法添加到此群组的项目中                                                                                                                               |
| `prevent_forking_outside_group` **(PREMIUM)**                      | boolean | no   | 如果启用，用户**不能**从此群组派生项目到外部命名空间                                                                                                                 |
| `shared_runners_minutes_limit` **(PREMIUM SELF)**                      | integer | no   | 仅可由管理员添加。此群组每月最大的计算单元。可以是 `nil`（默认，继承系统默认、`0`（无限）或 `> 0`）                                                                                    |
| `unique_project_download_limit` **(ULTIMATE)**                     | integer | no   | 在被禁止之前，用户在指定时间段内可以下载的唯一项目的最大数量。仅适用于顶级群组。默认值：0，最大值：10,000                                                                                     |
| `unique_project_download_limit_interval_in_seconds` **(ULTIMATE)** | integer | no   | 用户在被禁止之前可以下载最大数量项目的时间段。仅适用于顶级群组。默认值：0，最大值：864,000 秒（10 天）                                                                                    |
| `unique_project_download_limit_allowlist` **(ULTIMATE)**           | array of strings | no   | 从唯一项目下载限制中排除的用户名列表。仅适用于顶级群组。默认值：`[]`，最大值：100 个用户名                                                                                            |
| `unique_project_download_limit_alertlist` **(ULTIMATE)**     |  array of integers  | no   | 超过唯一项目下载限制时通过电子邮件发送的用户 ID 列表。仅适用于顶级群组。默认值：`[]`，最大值：100 个用户 ID。引入于极狐GitLab 15.9。                                                              |
| `auto_ban_user_on_excessive_projects_download` **(ULTIMATE)**      | boolean | no   | 启用后，当用户下载超过 `unique_project_download_limit` 和 `unique_project_download_limit_interval_in_seconds` 指定的最大唯一项目数时，用户将被自动禁止加入该群组。引入于极狐GitLab 15.4 |
| `ip_restriction_ranges` **(PREMIUM)**                              | string  | no   | 以逗号分隔的 IP 地址或子网掩码列表，用于限制群组访问。引入于极狐GitLab 15.4                                                                                                |
| `wiki_access_level` **(PREMIUM)**                       | string  | no       | Wiki 访问级别。可以是 `disabled`、`private` 或 `enabled`                                                                                               |


NOTE:
响应中的 `projects` 和 `shared_projects` 参数已被弃用并且计划在 API v5 中删除。
要获取群组内所有项目的详细信息，请使用[列出群组的项目](#list-a-groups-projects)或[列出群组的共享项目](#list-a-groups-shared-projects )端点。

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/5?name=Experimental"
```

此端点返回：

- 极狐GitLab 12.5 及更早版本中的所有项目和共享项目。
- 极狐GitLab 12.6 及更高版本中的最多 100 个项目和共享项目。

要获取群组内所有项目的详细信息，请使用[列出群组的项目端点](#list-a-groups-projects)。

- 响应示例：

```json
{
  "id": 5,
  "name": "Experimental",
  "path": "h5bp",
  "description": "foo",
  "visibility": "internal",
  "avatar_url": null,
  "web_url": "http://gitlab.example.com/groups/h5bp",
  "request_access_enabled": false,
  "full_name": "Foobar Group",
  "full_path": "h5bp",
  "file_template_project_id": 1,
  "parent_id": null,
  "created_at": "2020-01-15T12:36:29.590Z",
  "prevent_sharing_groups_outside_hierarchy": false,
  "projects": [ // Deprecated and will be removed in API v5
    {
      "id": 9,
      "description": "foo",
      "default_branch": "master",
      "tag_list": [], //deprecated, use `topics` instead
      "topics": [],
      "public": false,
      "archived": false,
      "visibility": "internal",
      "ssh_url_to_repo": "git@gitlab.example.com/html5-boilerplate.git",
      "http_url_to_repo": "http://gitlab.example.com/h5bp/html5-boilerplate.git",
      "web_url": "http://gitlab.example.com/h5bp/html5-boilerplate",
      "name": "Html5 Boilerplate",
      "name_with_namespace": "Experimental / Html5 Boilerplate",
      "path": "html5-boilerplate",
      "path_with_namespace": "h5bp/html5-boilerplate",
      "issues_enabled": true,
      "merge_requests_enabled": true,
      "wiki_enabled": true,
      "jobs_enabled": true,
      "snippets_enabled": true,
      "created_at": "2016-04-05T21:40:50.169Z",
      "last_activity_at": "2016-04-06T16:52:08.432Z",
      "shared_runners_enabled": true,
      "creator_id": 1,
      "namespace": {
        "id": 5,
        "name": "Experimental",
        "path": "h5bp",
        "kind": "group"
      },
      "avatar_url": null,
      "star_count": 1,
      "forks_count": 0,
      "open_issues_count": 3,
      "public_jobs": true,
      "shared_with_groups": [],
      "request_access_enabled": false
    }
  ],
  "ip_restriction_ranges": null
}
```

`prevent_sharing_groups_outside_hierarchy` 参数仅在顶级群组中的响应中存在。
 
[专业版或旗舰版](https://about.gitlab.com/pricing/)的用户也可以看到 `wiki_access_level` 参数。

### 禁用结果限制 **(FREE SELF)**

100 个结果的限制可能会破坏使用极狐GitLab 12.4 及更早版本开发的集成。

对于 12.5 到 13.12，可以使用[列出群组的项目](#list-a-groups-projects)端点在迁移中禁用限制。

请求具有 Rails 控制台访问权限的极狐GitLab 管理员运行以下命令：

```ruby
Feature.disable(:limit_projects_in_groups_api)
```

对于 14.0 及更高版本，无法禁用限制。

<a id="options-for-shared_runners_setting"></a>

### `shared_runners_setting` 选项

`shared_runners_setting` 参数决定共享 Runner 是否对群组的子群组和项目进行启用。

| 值                            | 描述                                                                    |
|------------------------------|-----------------------------------------------------------------------|
| `enabled`                    | 为群组中所有项目和子群组启用共享 Runner                                               |
| `disabled_and_overridable`      | 禁用此群组中所有项目和子群组的共享 Runner，但允许子群组覆盖此设置                                  |
| `disabled_and_unoverridable` | 禁用此群组中所有项目和子群组的共享 Runner，防止子群组覆盖此设置                                   |
| `disabled_with_override`  | （已弃用。使用 `disabled_and_overridable`）禁用该群组中所有项目和子组的共享 Runner，但允许子组覆盖此设置 |

### 上传群组头像

> 引入于极狐GitLab 12.9。

要从文件系统上传头像文件，请使用 `--form` 参数。这会使 curl 使用标题 `Content-Type: multipart/form-data` 发布数据。
`file=` 参数必须指向文件系统上的文件，并且前面有 `@`。例如：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/22" \
     --form "avatar=@/tmp/example.png"
```

### 移除群组头像

> 引入于极狐GitLab 15.4。

要移除群组头像，请为 `avatar` 参数赋空值。

请求示例：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/22" \
     --data "avatar="
```

## 移除群组

> - 立即删除子群组引入于极狐GitLab 15.3，[功能标志](../administration/feature_flags.md)为 `immediate_delete_subgroup_api`。默认禁用。
> - 立即删除子群组[在 JiHuLab.com 和私有化部署中启用](https://gitlab.com/gitlab-org/gitlab/-/issues/368276)于极狐GitLab 15.4。
> - 立即删除子群组默认启用于极狐GitLab 15.4。
> - 用于立即删除子组的标志 `immediate_delete_subgroup_api` 移除于极狐GitLab 15.9。


仅对群组所有者和管理员可用。

此端点：

- 在专业版和旗舰版中，将群组标记为删除。默认情况下，会在 7 天后删除，但您可以在[实例设置](../user/admin_area/settings/visibility_and_access_controls.md#deletion-protection)中更改保留期限。
- 在免费版本中，立即删除群组并对后台作业进行排队以删除群组中的所有项目。
- 如果子组被标记为删除（极狐GitLab 15.4 及更高版本），则立即删除该子群组。端点不会立即删除顶级群组。

```plaintext
DELETE /groups/:id
```

参数：

| 参数   | 类型             | 是否必需 | 描述                                                                                                    |
|------|----------------|------|-------------------------------------------------------------------------------------------------------|
| `id` | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                                 |
| `permanently_remove` **(PREMIUM)** | boolean/string   | no       | 如果标记为删除，则立即删除子群组。引入于极狐GitLab 15.4                                                                     |
| `full_path` **(PREMIUM)**          | string           | no       | 使用 `permanently_remove` 的子群组的完整路径。引入于极狐GitLab 15.4。要找到子群组路径，请查看[群组详细信息](groups.md#details-of-a-group) |

如果用户有授权，则响应为 `202 Accepted`。

NOTE：
如果 JiHuLab.com 群组链接到订阅，则无法删除它。要删除这样的群组，首先要将订阅和其他群组进行链接。<!--[订阅链接](../subscriptions/index.md#change-the-linked-namespace)-->

## 还原标记为删除的群组 **(PREMIUM)**

> 引入于极狐GitLab 12.8。

还原标记为删除的群组。

```plaintext
POST /groups/:id/restore
```

参数：

| 参数   | 类型             | 是否必需 | 描述                                                    |
|------|----------------|------|-------------------------------------------------------|
| `id` | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |

## 查找群组

获取所有名称或路径中匹配您的字符串的群组。

```plaintext
GET /groups?search=foobar
```

```json
[
  {
    "id": 1,
    "name": "Foobar Group",
    "path": "foo-bar",
    "description": "An interesting group"
  }
]
```

## 列出提供用户 **(PREMIUM)**

> 引入于极狐GitLab 14.8。

获取给定群组提供的用户列表。不包括由子群组提供的用户。
<!--此列表中的用户被认为是[企业用户](../user/enterprise_user/index.md)-->

至少需要该群组的维护者角色。

```plaintext
GET /groups/:id/provisioned_users
```

参数：

| 参数               | 类型             | 是否必需 | 描述                                                    |
|:-----------------|:---------------|:-----|:------------------------------------------------------|
| `id`             | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `username`       | string         | no   | 返回具有特定用户名的单个用户                                        |
| `search`         | string         | no   | 根据名称、电子邮件和用户名搜索用户                                     |
| `active`         | boolean        | no   | 仅返回活跃用户                                               |
| `blocked`        | boolean        | no   | 仅返回阻塞用户                                               |
| `created_after`  | datetime       | no   | 返回特定时间后创建的用户                                          |
| `created_before` | datetime       | no   | 返回特定时间前创建的用户                                          |

响应示例：

```json
[
  {
    "id": 66,
    "username": "user22",
    "name": "John Doe22",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/xxx?s=80&d=identicon",
    "web_url": "http://my.gitlab.com/user22",
    "created_at": "2021-09-10T12:48:22.381Z",
    "bio": "",
    "location": null,
    "public_email": "",
    "skype": "",
    "linkedin": "",
    "twitter": "",
    "website_url": "",
    "organization": null,
    "job_title": "",
    "pronouns": null,
    "bot": false,
    "work_information": null,
    "followers": 0,
    "following": 0,
    "local_time": null,
    "last_sign_in_at": null,
    "confirmed_at": "2021-09-10T12:48:22.330Z",
    "last_activity_on": null,
    "email": "user22@example.org",
    "theme_id": 1,
    "color_scheme_id": 1,
    "projects_limit": 100000,
    "current_sign_in_at": null,
    "identities": [ ],
    "can_create_group": true,
    "can_create_project": true,
    "two_factor_enabled": false,
    "external": false,
    "private_profile": false,
    "commit_email": "user22@example.org",
    "shared_runners_minutes_limit": null,
    "extra_shared_runners_minutes_limit": null
  },
  ...
]
```

## 服务账户 **(PREMIUM)**

### 创建服务账户用户

> 引入于极狐GitLab 16.1。

使用自动生成的电子邮件地址和用户名创建服务账户用户。

```plaintext
POST /groups/:id/service_accounts
```

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/345/service_accounts"
```

响应示例：

```json
{
  "id": 57,
  "username": "service_account_group_345_6018816a18e515214e0c34c2b33523fc",
  "name": "Service account user"
}
```

### 为服务账户用户创建个人访问令牌

> 引入于极狐GitLab 16.1。

```plaintext
POST /groups/:id/service_accounts/:user_id/personal_access_tokens
```

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gdk.test:3443/api/v4/groups/35/service_accounts/71/personal_access_tokens" --data "scopes[]=api" --data "name=service_accounts_token"
```

响应示例：

```json
{
  "id":6,
  "name":"service_accounts_token",
  "revoked":false,
  "created_at":"2023-06-13T07:47:13.900Z",
  "scopes":["api"],
  "user_id":71,
  "last_used_at":null,
  "active":true,
  "expires_at":"2024-06-12",
  "token":"<token_value>"
}
```

### 轮换服务账户用户的个人访问令牌

> 引入于极狐GitLab 16.1。

```plaintext
POST /groups/:id/service_accounts/:user_id/personal_access_tokens/:token_id/rotate
```

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gdk.test:3443/api/v4/groups/35/service_accounts/71/personal_access_tokens/6/rotate"
```

响应示例：

```json
{
  "id":7,
  "name":"service_accounts_token",
  "revoked":false,
  "created_at":"2023-06-13T07:54:49.962Z",
  "scopes":["api"],
  "user_id":71,
  "last_used_at":null,
  "active":true,
  "expires_at":"2023-06-20",
  "token":"<token_value>"
}
```

## 钩子 **(PREMIUM)**

也称为群组钩子和网络钩子。不同于系统范围的[系统钩子](system_hooks.md)和仅限于一个项目的[项目钩子](projects.md#hooks)。

### 列出群组钩子

获取群组钩子列表。

```plaintext
GET /groups/:id/hooks
```

| 参数   | 类型             | 是否必需 | 描述                                                    |
|------|----------------|------|-------------------------------------------------------|
| `id` | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |

### 获取群组钩子

获取群组的特定钩子。

| 参数        | 类型             | 是否必需 | 描述                                                    |
|-----------|----------------|------|-------------------------------------------------------|
| `id`      | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `hook_id` | integer        | yes  | 群组钩子 ID                               |

```plaintext
GET /groups/:id/hooks/:hook_id
```

```json
{
  "id": 1,
  "url": "http://example.com/hook",
  "group_id": 3,
  "push_events": true,
  "push_events_branch_filter": "",
  "issues_events": true,
  "confidential_issues_events": true,
  "merge_requests_events": true,
  "tag_push_events": true,
  "note_events": true,
  "confidential_note_events": true,
  "job_events": true,
  "pipeline_events": true,
  "wiki_page_events": true,
  "deployment_events": true,
  "releases_events": true,
  "subgroup_events": true,
  "enable_ssl_verification": true,
  "repository_update_events": false,
  "alert_status": "executable",
  "disabled_until": null,
  "url_variables": [ ],
  "created_at": "2012-10-12T17:04:47Z"
}
```

### 添加群组钩子

向特定群组添加钩子。

```plaintext
POST /groups/:id/hooks
```

| 参数                           | 类型             | 是否必需 | 描述                                                      |
|------------------------------|----------------|------|---------------------------------------------------------|
| `id`                         | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `url`                        | string         | yes  | 钩子 URL                                                  |
| `push_events`                | boolean        | no   | 在推送事件上触发钩子                                              |
| `push_events_branch_filter`  | string         | No       | 在推送事件上仅为匹配分支触发钩子                                        |
| `issues_events`              | boolean        | no   | 在议题事件上触发钩子                                              |
| `confidential_issues_events` | boolean        | no   | 在私密议题事件上触发钩子                                            |
| `merge_requests_events`      | boolean        | no   | 在合并请求事件上触发钩子                                            |
| `tag_push_events`            | boolean        | no   | 在标签推送事件上触发钩子                                            |
| `note_events`                | boolean        | no   | 在备注事件上触发钩子                                              |
| `confidential_note_events`   | boolean        | no   | 在私密备注事件上触发钩子                                            |
| `job_events`                 | boolean        | no   | 在作业事件上触发钩子                                              |
| `pipeline_events`            | boolean        | no   | 在流水线事件上触发钩子                                             |
| `wiki_page_events`           | boolean        | no   | 在 Wiki 页面事件上触发钩子                                        |
| `deployment_events`          | boolean        | no   | 在部署事件上触发钩子                                              |
| `releases_events`            | boolean        | no   | 在发布事件上触发钩子                                              |
| `subgroup_events`            | boolean        | no   | 在子群组事件上触发钩子                                             |
| `enable_ssl_verification`    | boolean        | no   | 触发钩子时进行 SSL 验证                                          |
| `token`                      | string         | no   | 验证接收负载的 Secret 令牌，不在响应中返回                              |

### 编辑群组钩子

为特定群组编辑钩子。

```plaintext
PUT /groups/:id/hooks/:hook_id
```

| 参数                           | 类型             | 是否必需 | 描述                                                         |
|------------------------------|----------------|------|------------------------------------------------------------|
| `id`                         | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `hook_id`                    | integer        | yes  | 群组钩子 ID                                                    |
| `url`                        | string         | yes  | 钩子 URL                                                     |
| `push_events`                | boolean        | no   | 在推送事件上触发钩子                                                 |
| `push_events_branch_filter`  | string         | No       | 在推送事件上仅为匹配分支触发钩子                                           |
| `issues_events`              | boolean        | no   | 在议题事件上触发钩子                                                 |
| `confidential_issues_events` | boolean        | no   | 在私密议题事件上触发钩子                                               |
| `merge_requests_events`      | boolean        | no   | 在合并请求事件上触发钩子                                               |
| `tag_push_events`            | boolean        | no   | 在标签推送事件上触发钩子                                               |
| `note_events`                | boolean        | no   | 在备注事件上触发钩子                                                 |
| `confidential_note_events`   | boolean        | no   | 在私密备注事件上触发钩子                                               |
| `job_events`                 | boolean        | no   | 在作业事件上触发钩子                                                 |
| `pipeline_events`            | boolean        | no   | 在流水线事件上触发钩子                                                |
| `wiki_page_events`           | boolean        | no   | 在 Wiki 页面事件上触发钩子                                           |
| `deployment_events`          | boolean        | no   | 在部署事件上触发钩子                                                 |
| `releases_events`            | boolean        | no   | 在发布事件上触发钩子                                                 |
| `subgroup_events`            | boolean        | no   | 在子群组事件上触发钩子                                                |
| `enable_ssl_verification`    | boolean        | no   | 触发钩子时进行 SSL 验证                                             |
| `token`                      | string         | no   | 验证接收负载的 Secret 令牌，不在响应中返回。当您更改 webhook URL 时，秘密令牌将被重置并且不会保留                                  |

### 删除群组钩子

从群组中移除钩子。这是一种幂等方法，可以多次调用。
钩子可用或不可用。

```plaintext
DELETE /groups/:id/hooks/:hook_id
```

| 参数        | 类型             | 是否必需 | 描述                                                    |
|-----------|----------------|------|-------------------------------------------------------|
| `id`      | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `hook_id` | integer        | yes  | 群组钩子 ID                          |

## 群组审计事件 **(PREMIUM)**

可以通过[群组审计事件 API](audit_events.md#group-audit-events) 访问群组审计事件。

## 与 LDAP 同步群组 **(PREMIUM SELF)**

和其关联 LDAP 群组同步群组。仅群组拥有者和管理员可用。

```plaintext
POST /groups/:id/ldap_sync
```

参数：

- `id`（必需）-用户群组的 ID 或路径

## 群组成员

请参见[群组成员](members.md)。

## LDAP 群组链接

列出、添加和删除 LDAP 群组链接。

### 列出 LDAP 群组链接 **(PREMIUM SELF)**

列出 LDAP 群组链接

```plaintext
GET /groups/:id/ldap_group_links
```

| 参数   | 类型             | 是否必需 | 描述                                                    |
|------|----------------|------|-------------------------------------------------------|
| `id` | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |

### 用 CN 或筛选器添加 LDAP 群组链接 **(PREMIUM SELF)**

使用 CN 或筛选器添加 LDAP 群组链接。仅专业版及旗舰版支持按筛选器添加群组链接。

```plaintext
POST /groups/:id/ldap_group_links
```

| 参数             | 类型             | 是否必需 | 描述                                                         |
|----------------|----------------|------|------------------------------------------------------------|
| `id`           | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `cn`           | string         | no   | LDAP 群组的 CN                                                |
| `filter`       | string         | no   | 群组的 LDAP 过滤器                                               |
| `group_access` | integer        | yes  | LDAP 群组成员的[角色 (`access_level`)](members.md#roles)          |
| `provider`     | string         | yes  | LDAP 群组链接的 LDAP 提供者                                        |

NOTE:
要定义 LDAP 群组链接，提供 `cn` 或 `filter`，但不要两个都提供。

### 删除 LDAP 群组链接 **(PREMIUM SELF)**

删除 LDAP 群组链接。已废弃。预计在后续发布中移除。

```plaintext
DELETE /groups/:id/ldap_group_links/:cn
```

| 参数   | 类型             | 是否必需 | 描述                                                    |
|------|----------------|------|-------------------------------------------------------|
| `id` | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `cn` | string         | yes  | LDAP 群组的 CN                                             |

删除特定 LDAP 提供者的 LDAP 群组链接。已废弃。预计在后续发布中移除。

```plaintext
DELETE /groups/:id/ldap_group_links/:provider/:cn
```

| 参数         | 类型             | 是否必需 | 描述                                                    |
|------------|----------------|------|-------------------------------------------------------|
| `id`       | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `cn`       | string         | yes  | LDAP 群组的 CN                                           |
| `provider` | string         | yes  | LDAP 群组链接的 LDAP 提供者                  |

### 使用 CN 或筛选器删除 LDAP 群组链接 **(PREMIUM SELF)**

使用 CN 或筛选器删除 LDAP 群组链接。通过筛选器删除仅在专业版及旗舰版中支持。

```plaintext
DELETE /groups/:id/ldap_group_links
```

| 参数         | 类型             | 是否必需 | 描述                                                    |
|------------|----------------|------|-------------------------------------------------------|
| `id`       | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `cn`       | string         | no   | LDAP 群组的 CN                                           |
| `filter`   | string         | no   | 群组的 LDAP 筛选器                                          |
| `provider` | string         | yes  | LDAP 群组链接的 LDAP 提供者                                   |

NOTE:
要删除 LDAP 群组链接，提供 `cn` 或 `filter`，但不要两个都提供。

## SAML 群组链接 **(PREMIUM)**

> - 引入于极狐GitLab 15.3.0。
> - `access_level` 类型于极狐GitLab 15.3.3 从 `string` 更改为 `integer`。

列出、获取、添加和删除 SAML 群组链接。

### 列出 SAML 群组链接

列出 SAML 群组链接。

```plaintext
GET /groups/:id/saml_group_links
```

支持的参数：

| 参数   | 类型             | 是否必需 | 描述                                                    |
|:-----|:---------------|:-----|:------------------------------------------------------|
| `id` | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |

如果成功，则返回 [`200`](rest/index.md#status-codes) 和以下响应参数：

| 参数                | 类型      | 描述                                                                                  |
|:------------------|:--------|:------------------------------------------------------------------------------------|
| `[].name`         | string  | SAML 群组的名称                                                                          |
| `[].access_level` | integer | SAML 群组成员的[角色 (`access_level`)](members.md#roles)。参数的字符串类型来自 15.3.0 到 15.3.3 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/saml_group_links"
```

响应示例：

```json
[
  {
    "name": "saml-group-1",
    "access_level": 10
  },
  {
    "name": "saml-group-2",
    "access_level": 40
  }
]
```

### 获取 SAML 群组链接

获取群组的 SAML 群组链接。

```plaintext
GET /groups/:id/saml_group_links/:saml_group_name
```

支持的参数：

| 参数                | 类型             | 是否必需 | 描述                                                    |
|:------------------|:---------------|:-----|:------------------------------------------------------|
| `id`              | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `saml_group_name` | string         | yes  | SAML 群组的名称                                 |

如果成功，则返回 [`200`](rest/index.md#status-codes) 和以下响应参数：

| 参数             | 类型      | 描述                                                                           |
|:---------------|:--------|:-----------------------------------------------------------------------------|
| `name`         | string  | SAML 群组的名称                                                                   |
| `access_level` | integer | SAML 群组成员的[角色 (`access_level`)](members.md#roles)。参数的字符串类型来自 15.3.0 到 15.3.3 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/saml_group_links/saml-group-1"
```

响应示例：

```json
{
"name": "saml-group-1",
"access_level": 10
}
```

### 添加 SAML 群组链接

为群组添加 SAML 群组链接。

```plaintext
POST /groups/:id/saml_group_links
```

支持的参数：

| 参数                | 类型                | 是否必需 | 描述                                                                              |
|:------------------|:------------------|:-----|:--------------------------------------------------------------------------------|
| `id`              | integer or string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                           |
| `saml_group_name` | string            | yes  | SAML 群组的名称                                                                      |
| `access_level`    | integer           | yes  | SAML 群组成员的[成员 (`access_level`)](members.md#roles) |

如果成功，则返回 [`201`](rest/index.md#status-codes) 和以下响应参数：

| 参数             | 类型      | 描述                                                                           |
|:---------------|:--------|:-----------------------------------------------------------------------------|
| `name`         | string  | SAML 群组的名称                                                                   |
| `access_level` | integer | SAML 群组成员的[角色 (`access_level`)](members.md#roles)。参数的字符串类型来自 15.3.0 到 15.3.3 |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --header "Content-Type: application/json" --data '{ "saml_group_name": "<your_saml_group_name`>", "access_level": <chosen_access_level> }' --url  "https://gitlab.example.com/api/v4/groups/1/saml_group_links"
```

响应示例：

```json
{
"name": "saml-group-1",
"access_level": 10
}
```

### 删除 SAML 群组链接

删除群组的 SAML 群组链接。

```plaintext
DELETE /groups/:id/saml_group_links/:saml_group_name
```

支持的参数：

| 参数                | 类型             | 是否必需 | 描述                                                    |
|:------------------|:---------------|:-----|:------------------------------------------------------|
| `id`              | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `saml_group_name` | string         | yes  | SAML 群组的名称                                  |

如果成功，则返回没有返回主体内容的 [`204`](rest/index.md#status-codes) 状态码。

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/saml_group_links/saml-group-1"
```

## 群组中的命名空间

默认情况下，群组一次只能获得 20 个命名空间，因为 API 结果是分页的。

要获得更多（最多 100 个）命名空间，请将以下内容作为参数传递给 API 调用：

```plaintext
/groups?per_page=100
```

要切换页面，请添加：

```plaintext
/groups?per_page=100&page=2
```

## 群组徽章

详情请参见[群组徽章](group_badges.md)。

## 群组导入/导出

详情请参见[群组导入/导出](group_import_export.md)和[群组关系导出](group_relations_export.md)。

## 和其他群组共享群组

这些端点创建和删除用于与另一个群组共享群组的链接。有关更多信息，请参见[极狐GitLab 群组](../user/group/manage.md#share-a-group-with-another-group)页面中的相关讨论。

### 创建和其他群组共享群组的链接

和其他群组共享群组。如果成功，则返回 `200` 和[群组详情](#details-of-a-group)。

```plaintext
POST /groups/:id/share
```

| 参数             | 类型             | 是否必需 | 描述                                                    |
|----------------|----------------|------|-------------------------------------------------------|
| `id`           | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `group_id`     | integer        | yes  | 要共享的群组的 ID                                            |
| `group_access` | integer        | yes  | 要授予群组的[角色 (`access_level`](members.md#roles)          |
| `expires_at`   | string         | no   | 共享过期日期。ISO 8601 格式：2016-09-26                         |

### 删除和其他群组共享群组的链接

取消和其他群组共享群组。如果成功，返回 `204`，不返回任何内容。

```plaintext
DELETE /groups/:id/share/:group_id
```

| 参数         | 类型             | 是否必需 | 描述                                                    |
|------------|----------------|------|-------------------------------------------------------|
| `id`       | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `group_id` | integer        | yes  | 要共享的群组 ID                                             |

## 推送规则 **(PREMIUM)**

> 引入于极狐GitLab 13.4。

### 获取群组推送规则

获取群组的[推送规则](../user/group/access_and_permissions.md#group-push-rules)。

仅群组拥有者和管理员可用。

```plaintext
GET /groups/:id/push_rule
```

| 参数   | 类型             | 是否必需 | 描述                                                       |
|------|----------------|------|----------------------------------------------------------|
| `id` | integer/string | yes  | 群组 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |

```json
{
  "id": 2,
  "created_at": "2020-08-17T19:09:19.580Z",
  "commit_message_regex": "[a-zA-Z]",
  "commit_message_negative_regex": "[x+]",
  "branch_name_regex": "[a-z]",
  "deny_delete_tag": true,
  "member_check": true,
  "prevent_secrets": true,
  "author_email_regex": "^[A-Za-z0-9.]+@gitlab.com$",
  "file_name_regex": "(exe)$",
  "max_file_size": 100
}
```

[专业版及旗舰版](https://about.gitlab.cn/pricing/)的用户也能看见 `commit_committer_check` 和 `reject_unsigned_commits` 参数：

```json
{
  "id": 2,
  "created_at": "2020-08-17T19:09:19.580Z",
  "commit_committer_check": true,
  "reject_unsigned_commits": false,
  ...
}
```

### 添加群组推送规则

将[推送规则](../user/group/access_and_permissions.md#group-push-rules)添加到特定群组。

仅群组拥有者和管理员可用。

```plaintext
POST /groups/:id/push_rule
```

| 参数                              | 类型             | 是否必需 | 描述                                                         |
|---------------------------------|----------------|------|------------------------------------------------------------|
| `id`                            | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)      |
| `deny_delete_tag`               | boolean        | no   | 拒绝删除标签                                                     |
| `member_check`                  | boolean        | no   | 仅允许极狐GitLab 用户写提交                                          |
| `prevent_secrets`               | boolean        | no   | 拒绝可能包括 Secret 的文件                                          |
| `commit_message_regex`          | string         | no   | 所有提交信息必需匹配此参数提供的正则表达式。例如：`Fixed \d+\..*`                   |
| `commit_message_negative_regex` | string         | no   | 不允许匹配此参数中提供的正则表达式的提交信息。例如：`ssh\:\/\/`                      |
| `branch_name_regex`             | string         | no   | 所有的分支名称必需匹配此参数中提供的正则表达式。例如： `(feature                      |hotfix)\/*` |
| `author_email_regex`            | string         | no   | 所有的提交作者电子邮件必需匹配此参数中提供的正则表达式。例如：`@my-company.com$`          |
| `file_name_regex`               | string         | no   | **不**允许匹配此参数中提供的正则表达式的文件名称。例如：`(jar                        |exe)$` |
| `max_file_size`                 | integer        | no   | 允许的最大文件大小（MB）                                              |
| `commit_committer_check`        | boolean        | no   | 仅允许使用验证过的电子邮件推送的提交                                         |
| `reject_unsigned_commits`       | boolean        | no   | 仅允许通过 GPG 签署的提交 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/19/push_rule"
```

响应：

```json
{
    "id": 19,
    "created_at": "2020-08-31T15:53:00.073Z",
    "commit_message_regex": "[a-zA-Z]",
    "commit_message_negative_regex": "[x+]",
    "branch_name_regex": null,
    "deny_delete_tag": false,
    "member_check": false,
    "prevent_secrets": false,
    "author_email_regex": "^[A-Za-z0-9.]+@gitlab.com$",
    "file_name_regex": null,
    "max_file_size": 100
}
```

### 编辑群组推送规则

编辑特定群组的推送规则。

仅群组拥有者和管理员可用。

```plaintext
PUT /groups/:id/push_rule
```

| 参数                              | 类型             | 是否必需 | 描述                                                    |
|---------------------------------|----------------|------|-------------------------------------------------------|
| `id`                            | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `deny_delete_tag`               | boolean        | no   | 拒绝删除标签                                                |
| `member_check`                  | boolean        | no   | 仅允许现存极狐GitLab 用户写提交                                   |
| `prevent_secrets`               | boolean        | no   | 拒绝可能包括 Secret 的文件                                     |
| `commit_message_regex`          | string         | no   | 所有提交信息必需匹配此参数提供的正则表达式。例如：`Fixed \d+\..*`              |
| `commit_message_negative_regex` | string         | no   | 不允许匹配此参数中提供的正则表达式的提交信息。例如：`ssh\:\/\/`                 |
| `branch_name_regex`             | string         | no   | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `deny_delete_tag`               | boolean        | no   | 拒绝删除标签                                                |
| `member_check`                  | boolean        | no   | 仅允许极狐GitLab 用户写提交                                     |
| `prevent_secrets`               | boolean        | no   | 拒绝可能包括 Secret 的文件                                     |
| `commit_message_regex`          | string         | no   | 所有提交信息必需匹配此参数提供的正则表达式。例如：`Fixed \d+\..*`              |
| `commit_message_negative_regex` | string         | no   | 不允许匹配此参数中提供的正则表达式的提交信息。例如：`ssh\:\/\/`                 |
| `branch_name_regex`             | string         | no   | 所有的分支名称必需匹配此参数中提供的正则表达式。例如：`(feature                  |hotfix)\/*` |
| `author_email_regex`            | string         | no   | 所有的提交作者电子邮件必需匹配此参数中提供的正则表达式。例如：`@my-company.com$`     |
| `file_name_regex`               | string         | no   | **不**允许匹配此参数中提供的正则表达式的文件名称。例如：`(jar                   |exe)$` |
| `max_file_size`                 | integer        | no   | 允许的最大文件大小（MB）                       |
| `commit_committer_check`        | boolean        | no   | 仅允许使用验证过的电子邮件推送的提交 |
| `reject_unsigned_commits`       | boolean        | no   | 仅允许通过 GPG 签署的提交           |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/19/push_rule"
```

响应：

```json
{
    "id": 19,
    "created_at": "2020-08-31T15:53:00.073Z",
    "commit_message_regex": "[a-zA-Z]",
    "commit_message_negative_regex": "[x+]",
    "branch_name_regex": null,
    "deny_delete_tag": false,
    "member_check": false,
    "prevent_secrets": false,
    "author_email_regex": "^[A-Za-z0-9.]+@staging.gitlab.com$",
    "file_name_regex": null,
    "max_file_size": 100
}
```

### 删除群组推送规则 

删除群组[推送规则](../user/group/access_and_permissions.md#group-push-rules)。

仅群组拥有者和管理员可用。

```plaintext
DELETE /groups/:id/push_rule
```

| 参数   | 类型             | 是否必需 | 描述                                                    |
|------|----------------|------|-------------------------------------------------------|
| `id` | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
