---
stage: Govern
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 事件 API **(FREE)**

## 过滤器参数

<a id="actions"></a>

### 活动

有关 `action` 参数的可用类型请查看[用户贡献事件](../user/profile/contributions_calendar.md#user-contribution-events)。

<a id="target-types"></a>

### 目标类型

`target_type` 可用的参数是：

- `issue`
- `milestone`
- `merge_request`
- `note`
- `project`
- `snippet`
- `user`

使用 API 无法将事件与史诗关联。

<a id="date-formatting"></a>

### 日期格式

日期参数格式应遵照下列格式：


```plaintext
YYYY-MM-DD
```

## 事件保存期限

出于系统性能考虑，系统将从事件列表中移除超过 3 年的事件。

## 列出当前授权用户的事件

获取授权用户事件列表，范围必需包括 `read_user` 或者 `api` 。

使用 API 无法将事件与史诗关联。

```plaintext
GET /events
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `action` | string | no | 仅包括特定事件。[活动类型](#actions) |
| `target_type` | string | no | 仅包括特定事件。[目标类型](#target-types) |
| `before` | date | no |  仅包括在特定日期之前创建的事件，[查看日期格式](#date-formatting)。 |
| `after` | date | no |  仅包括在特定日期之后创建的事件，[查看日期格式](#date-formatting)。 |
| `scope` | string | no | 包括用户项目中的所有事件。 |
| `sort` | string | no | 按 `created_at` 以 `asc` 或者 `desc` 进行排序，默认为 `desc`。|


请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/events?target_type=issue&action=created&after=2017-01-31&before=2017-03-01&scope=all"
```

响应示例：

```json
[
  {
    "id": 1,
    "title":null,
    "project_id":1,
    "action_name":"opened",
    "target_id":160,
    "target_type":"Issue",
    "author_id":25,
    "target_title":"Qui natus eos odio tempore et quaerat consequuntur ducimus cupiditate quis.",
    "created_at":"2017-02-09T10:43:19.667Z",
    "author":{
      "name":"User 3",
      "username":"user3",
      "id":25,
      "state":"active",
      "avatar_url":"http://www.gravatar.com/avatar/97d6d9441ff85fdc730e02a6068d267b?s=80\u0026d=identicon",
      "web_url":"https://gitlab.example.com/user3"
    },
    "author_username":"user3"
  },
  {
    "id": 2,
    "title":null,
    "project_id":1,
    "action_name":"opened",
    "target_id":159,
    "target_type":"Issue",
    "author_id":21,
    "target_title":"Nostrum enim non et sed optio illo deleniti non.",
    "created_at":"2017-02-09T10:43:19.426Z",
    "author":{
      "name":"Test User",
      "username":"ted",
      "id":21,
      "state":"active",
      "avatar_url":"http://www.gravatar.com/avatar/80fb888c9a48b9a3f87477214acaa63f?s=80\u0026d=identicon",
      "web_url":"https://gitlab.example.com/ted"
    },
    "author_username":"ted"
  }
]
```

<a id="get-user-contribution-events"></a>

### 获取用户贡献事件

获取指定用户的贡献事件，筛选历史事件到最新的事件，范围必需包括 `read_user` 或者 `api`。

使用 API 无法将事件与史诗关联。

```plaintext
GET /users/:id/events
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id` | integer | yes | 用户 ID 或者用户名。 |
| `action` | string | no | 仅包括特定事件的[活动类型](#actions)。|
| `target_type` | string | no | 仅包括特定事件的[目标类型](#target-types)。|
| `before` | date | no |  仅包括在特定日期之前创建的事件，[查看日期格式](#date-formatting)。 |
| `after` | date | no |  仅包括在特定日期之后创建的事件，[查看日期格式](#date-formatting)。|
| `sort` | string | no | 按 `created_at` 以 `asc`或者 `desc` 进行排序，默认为 `desc`。 |
| `page` | integer | no | 返回结果页面，默认为 1。|
| `per_page` | integer | no | 每页显示结果数量，默认为 20。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/:id/events"
```

响应示例：

```json
[
  {
    "id": 3,
    "title": null,
    "project_id": 15,
    "action_name": "closed",
    "target_id": 830,
    "target_type": "Issue",
    "author_id": 1,
    "target_title": "Public project search field",
    "author": {
      "name": "Dmitriy Zaporozhets",
      "username": "root",
      "id": 1,
      "state": "active",
      "avatar_url": "http://localhost:3000/uploads/user/avatar/1/fox_avatar.png",
      "web_url": "http://localhost:3000/root"
    },
    "author_username": "root"
  },
  {
    "id": 4,
    "title": null,
    "project_id": 15,
    "action_name": "pushed",
    "target_id": null,
    "target_type": null,
    "author_id": 1,
    "author": {
      "name": "Dmitriy Zaporozhets",
      "username": "root",
      "id": 1,
      "state": "active",
      "avatar_url": "http://localhost:3000/uploads/user/avatar/1/fox_avatar.png",
      "web_url": "http://localhost:3000/root"
    },
    "author_username": "john",
    "push_data": {
      "commit_count": 1,
      "action": "pushed",
      "ref_type": "branch",
      "commit_from": "50d4420237a9de7be1304607147aec22e4a14af7",
      "commit_to": "c5feabde2d8cd023215af4d2ceeb7a64839fc428",
      "ref": "master",
      "commit_title": "Add simple search to projects in public area"
    },
    "target_title": null
  },
  {
    "id": 5,
    "title": null,
    "project_id": 15,
    "action_name": "closed",
    "target_id": 840,
    "target_type": "Issue",
    "author_id": 1,
    "target_title": "Finish & merge Code search PR",
    "author": {
      "name": "Dmitriy Zaporozhets",
      "username": "root",
      "id": 1,
      "state": "active",
      "avatar_url": "http://localhost:3000/uploads/user/avatar/1/fox_avatar.png",
      "web_url": "http://localhost:3000/root"
    },
    "author_username": "root"
  },
  {
    "id": 7,
    "title": null,
    "project_id": 15,
    "action_name": "commented on",
    "target_id": 1312,
    "target_type": "Note",
    "author_id": 1,
    "target_title": null,
    "created_at": "2015-12-04T10:33:58.089Z",
    "note": {
      "id": 1312,
      "body": "What an awesome day!",
      "attachment": null,
      "author": {
        "name": "Dmitriy Zaporozhets",
        "username": "root",
        "id": 1,
        "state": "active",
        "avatar_url": "http://localhost:3000/uploads/user/avatar/1/fox_avatar.png",
        "web_url": "http://localhost:3000/root"
      },
      "created_at": "2015-12-04T10:33:56.698Z",
      "system": false,
      "noteable_id": 377,
      "noteable_type": "Issue"
    },
    "author": {
      "name": "Dmitriy Zaporozhets",
      "username": "root",
      "id": 1,
      "state": "active",
      "avatar_url": "http://localhost:3000/uploads/user/avatar/1/fox_avatar.png",
      "web_url": "http://localhost:3000/root"
    },
    "author_username": "root"
  }
]
```

<a id="list-a-projects-visible-events"></a>

## 列出项目的可见事件

NOTE: 
这个端点存在时间比其他端点时间长，文档位于[项目 API 页面](projects.md)。

获取特定项目的可见事件列表。


```plaintext
GET /projects/:project_id/events
```
参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `project_id` | integer/string | yes | ID 或者[项目 URL 路径](rest/index.md#namespaced-path-encoding)。|
| `action` | string | no | 仅包括特定事件的[活动类型](#actions)。|
| `target_type` | string | no | 仅包括特定事件的[目标类型](#target-types)。|
| `before` | date | no |  仅包括在特定日期之前创建的事件，[查看日期格式](#date-formatting)。|
| `after` | date | no |  仅包括在特定日期之后创建的事件，[查看日期格式](#date-formatting)。 |
| `sort` | string | no | 按 `created_at` 以 `asc`或者 `desc` 进行排序，默认为 `desc`。 | |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:project_id/events?target_type=issue&action=created&after=2017-01-31&before=2017-03-01"
```

响应示例：

```json
[
  {
    "id": 8,
    "title":null,
    "project_id":1,
    "action_name":"opened",
    "target_id":160,
    "target_iid":160,
    "target_type":"Issue",
    "author_id":25,
    "target_title":"Qui natus eos odio tempore et quaerat consequuntur ducimus cupiditate quis.",
    "created_at":"2017-02-09T10:43:19.667Z",
    "author":{
      "name":"User 3",
      "username":"user3",
      "id":25,
      "state":"active",
      "avatar_url":"http://www.gravatar.com/avatar/97d6d9441ff85fdc730e02a6068d267b?s=80\u0026d=identicon",
      "web_url":"https://gitlab.example.com/user3"
    },
    "author_username":"user3"
  },
  {
    "id": 9,
    "title":null,
    "project_id":1,
    "action_name":"opened",
    "target_id":159,
    "target_iid":159,
    "target_type":"Issue",
    "author_id":21,
    "target_title":"Nostrum enim non et sed optio illo deleniti non.",
    "created_at":"2017-02-09T10:43:19.426Z",
    "author":{
      "name":"Test User",
      "username":"ted",
      "id":21,
      "state":"active",
      "avatar_url":"http://www.gravatar.com/avatar/80fb888c9a48b9a3f87477214acaa63f?s=80\u0026d=identicon",
      "web_url":"https://gitlab.example.com/ted"
    },
    "author_username":"ted"
  },
  {
    "id": 10,
    "title": null,
    "project_id": 1,
    "action_name": "commented on",
    "target_id": 1312,
    "target_iid": 1312,
    "target_type": "Note",
    "author_id": 1,
    "data": null,
    "target_title": null,
    "created_at": "2015-12-04T10:33:58.089Z",
    "note": {
      "id": 1312,
      "body": "What an awesome day!",
      "attachment": null,
      "author": {
        "name": "Dmitriy Zaporozhets",
        "username": "root",
        "id": 1,
        "state": "active",
        "avatar_url": "https://gitlab.example.com/uploads/user/avatar/1/fox_avatar.png",
        "web_url": "https://gitlab.example.com/root"
      },
      "created_at": "2015-12-04T10:33:56.698Z",
      "system": false,
      "noteable_id": 377,
      "noteable_type": "Issue",
      "noteable_iid": 377
    },
    "author": {
      "name": "Dmitriy Zaporozhets",
      "username": "root",
      "id": 1,
      "state": "active",
      "avatar_url": "https://gitlab.example.com/uploads/user/avatar/1/fox_avatar.png",
      "web_url": "https://gitlab.example.com/root"
    },
    "author_username": "root"
  }
]
```
