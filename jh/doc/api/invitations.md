---
stage: Growth
group: Acquisition
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 邀请 API **(FREE)**

邀请或向群组或项目添加用户，并列出待处理邀请。

## 有效访问级别

要发送邀请，您必须有权访问您要发送电子邮件的项目或群组。`Gitlab::Access` 模块中定义了有效访问级别。目前有以下有效访问级别：

- 无访问级别（`0`）
- 最小访问级别（`5`）：引入于 13.5<!-- ([Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/220203) in GitLab 13.5.)-->
- 访客（`10`）
- 报告者（`20`）
- 开发者（`30`）
- 维护者（`40`）
- 拥有者（`50`）：对 14.9 及更高版本中的项目有效

NOTE:
在 14.9 及更高版本中，项目中最大角色为拥有者；在 14.8 及更早版本中，项目中最大角色为维护者。

## 向群组或项目添加成员

添加新成员。您可以通过指定用户 ID 或发送电子邮件邀请用户加入。

```plaintext
POST /groups/:id/invitations
POST /projects/:id/invitations
```

| 参数                 | 类型               | 是否必需                              | 描述                                                                                               |
|--------------------|------------------|-----------------------------------|--------------------------------------------------------------------------------------------------|
| `id`               | integer/string   | yes                               | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目或群组路径](rest/index.md#namespaced-path-encoding)                                 |
| `email`            | string           | yes (if `user_id` isn't provided) | 新成员的电子邮件，或由逗号分隔的多个电子邮件                                                                           |
| `user_id`          | integer/string   | yes (if `email` isn't provided)   | 新成员的 ID，或由逗号分隔的多个 ID。引入于 14.10                                                                   |
| `access_level`     | integer          | yes                               | 有效访问级别                                                                                           |
| `expires_at`       | string           | no                                | 日期资源串。格式：YEAR-MONTH-DAY                                                                          |
| `invite_source`    | string           | no                                | 发起成员创建流程的邀请源                                                                                     |
| `tasks_to_be_done` | array of strings | no                                | 邀请者希望成员关注的任务。任务以议题的形式添加到特定项目中。值可以是：`ci`、`code` 和 `issues`。如果指定了值，则需要 `tasks_project_id`。引入于 14.6 |
| `tasks_project_id` | integer          | no                                | 创建任务议题的项目 ID。如果指定了值，则需要 `tasks_to_be_done`。引入于 14.6                                              |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --data "email=test@example.com&user_id=1&access_level=30" "https://gitlab.example.com/api/v4/groups/:id/invitations"
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --data "email=test@example.com&user_id=1&access_level=30" "https://gitlab.example.com/api/v4/projects/:id/invitations"
```

响应示例：

所有电子邮件发送成功时：

```json
{  "status":  "success"  }
```

电子邮件发送失败时：

```json
{
  "status": "error",
  "message": {
               "test@example.com": "Invite email has already been taken",
               "test2@example.com": "User already exists in source",
               "test_username": "Access level is not included in the list"
             }
}
```

## 列出群组和项目中所有待处理的邀请


获取经过身份验证的用户可以查看的受邀群组或项目成员的列表。不通过继承的祖先群组，仅将邀请返回给直接成员。

此函数使用分页参数 `page` 和 `per_page` 限制成员列表。

```plaintext
GET /groups/:id/invitations
GET /projects/:id/invitations
```

| 参数         | 类型             | 是否必需 | 描述                                                                                                                                              |
|------------|----------------|------|-------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目或群组路径](rest/index.md#namespaced-path-encoding)                                                                                |
| `page`     | integer        | no   | 检索的页面                                                                                                                                           |
| `per_page` | integer        | no   | 每个页面返回的成员邀请数量                                                                                    |
| `query`    | string         | no   | 通过邀请电子邮件搜索受邀成员的查询字符串。查询文本必须与电子邮件地址完全匹配。为空时，返回所有邀请 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/invitations?query=member@example.org"
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/invitations?query=member@example.org"
```

响应示例：

```json
 [
   {
     "id": 1,
     "invite_email": "member@example.org",
     "created_at": "2020-10-22T14:13:35Z",
     "access_level": 30,
     "expires_at": "2020-11-22T14:13:35Z",
     "user_name": "Raymond Smith",
     "created_by_name": "Administrator"
   },
]
```

## 更新群组或项目的邀请

更新待处理的邀请访问级别或访问截止日期。

```plaintext
PUT /groups/:id/invitations/:email
PUT /projects/:id/invitations/:email
```

| 参数      | 类型             | 是否必需 | 描述                                                                       |
|---------|----------------|------|--------------------------------------------------------------------------|
| `id`    | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目或群组路径](rest/index.md#namespaced-path-encoding)        |
| `email` | string         | yes  | 接收邀请的电子邮件地址 |
| `access_level`    |  integer | no  | 有效访问级别（默认为 `30`，开发者角色）         |
| `expires_at` | string         | no  | 日期时间串。格式：ISO 8601（`YYYY-MM-DDTHH:MM:SSZ`） |
```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/55/invitations/email@example.org?access_level=40"
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/55/invitations/email@example.org?access_level=40"
```

响应示例：

```json
{
  "expires_at": "2012-10-22T14:13:35Z",
  "access_level": 40,
}
```

## 删除群组或项目的邀请

删除通过电子邮件进行的邀请。

```plaintext
DELETE /groups/:id/invitations/:email
DELETE /projects/:id/invitations/:email
```

| 参数      | 类型             | 是否必需 | 描述                                                                       |
|---------|----------------|------|--------------------------------------------------------------------------|
| `id`    | integer/string | yes  | 经过身份验证的用户邀请的 ID 或 [URL 编码的项目或群组路径](rest/index.md#namespaced-path-encoding)         |
| `email` | string         | yes  | 接收邀请的电子邮件地址 |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/55/invitations/email@example.org"
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/55/invitations/email@example.org"
```

- 删除成功：返回 `204`
- 无权删除邀请：返回 `403` 
- 未找到该电子邮件的授权邀请或其他任何邀请：返回 `404` 
- 请求有效但无法删除邀请：返回 `409`
