---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组访问令牌 API **(FREE)**

您可以阅读更多有关[群组访问令牌](../user/group/settings/group_access_tokens.md)的内容。

## 列出群组访问令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77236) -->引入于极狐GitLab 14.7。

获取[群组访问令牌](../user/group/settings/group_access_tokens.md)列表。

```plaintext
GET groups/:id/access_tokens
```

| 参数   | 类型                | 是否必需 | 描述                                                          |
|------|-------------------|------|-------------------------------------------------------------|
| `id` | integer or string | yes  | 群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/<group_id>/access_tokens"
```

```json
[
   {
      "user_id" : 141,
      "scopes" : [
         "api"
      ],
      "name" : "token",
      "expires_at" : "2021-01-31",
      "id" : 42,
      "active" : true,
      "created_at" : "2021-01-20T22:11:48.151Z",
      "revoked" : false,
      "access_level": 40
   }
]
```

## 获取群组访问令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/82714) -->引入于极狐GitLab 14.10。

通过 ID 获取[群组访问令牌](../user/group/settings/group_access_tokens.md)。

```plaintext
GET groups/:id/access_tokens/:token_id
```

| 参数         | 类型                | 是否必需 | 描述                                                          |
|------------|-------------------|------|-------------------------------------------------------------|
| `id`       | integer or string | yes  | 群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `token_id` | integer or string | yes  | 群组访问令牌的 ID                                                  |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/<group_id>/access_tokens/<token_id>"
```

```json
{
   "user_id" : 141,
   "scopes" : [
      "api"
   ],
   "name" : "token",
   "expires_at" : "2021-01-31",
   "id" : 42,
   "active" : true,
   "created_at" : "2021-01-20T22:11:48.151Z",
   "revoked" : false,
   "access_level": 40
}
```

## 创建群组访问令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77236) -->引入于极狐GitLab 14.7。

创建[群组访问令牌](../user/group/settings/group_access_tokens.md)。您必须拥有群组所有者的角色才能创建群组访问令牌。

```plaintext
POST groups/:id/access_tokens
```

| 参数             | 类型                | 是否必需 | 描述                                                                                  |
|----------------|-------------------|------|-------------------------------------------------------------------------------------|
| `id`           | integer or string | yes  | 群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                         |
| `name`         | String            | yes  | 群组访问令牌的名称                                                                           |
| `scopes`       | `Array[String]`   | yes  | [范围列表](../user/group/settings/group_access_tokens.md#scopes-for-a-group-access-token) |
| `access_level` | Integer           | no   | 访问级别。有效值为 `10`（访客）、`20`（报告者）、`30`（开发者）、`40`（维护者）、`50`（所有者）                          |
| `expires_at`   | Date              | no   | 令牌于指定日期的午夜时间（UTC）过期                                                                 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
--header "Content-Type:application/json" \
--data '{ "name":"test_token", "scopes":["api", "read_repository"], "expires_at":"2021-01-31", "access_level": 30 }' \
"https://gitlab.example.com/api/v4/groups/<group_id>/access_tokens"
```

```json
{
   "scopes" : [
      "api",
      "read_repository"
   ],
   "active" : true,
   "name" : "test",
   "revoked" : false,
   "created_at" : "2021-01-21T19:35:37.921Z",
   "user_id" : 166,
   "id" : 58,
   "expires_at" : "2021-01-31",
   "token" : "D4y...Wzr",
   "access_level": 30
}
```

## 轮换群组访问令牌

> 引入于极狐GitLab 16.0。

轮换群组访问令牌。撤销之前的令牌并创建一个一周后过期的新令牌。

```plaintext
POST /groups/:id/access_tokens/:token_id/rotate
```

| 参数         | 类型                | 是否必需 | 描述                                                          |
|------------|-------------------|------|-------------------------------------------------------------|
| `id`       | integer or string | yes  | 群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `token_id` | integer or string | yes  | 项目访问令牌 ID                        |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/<group_id>/access_tokens/<token_id>/rotate"
```

### 响应

- `200: OK`：如果已成功撤销现有令牌并创建新令牌。
- `400: Bad Request`：如果没有成功轮换。
- `401: Unauthorized`：如果有以下任一情况：
    - 用户无权访问具有指定 ID 的令牌。
    - 具有指定 ID 的令牌不存在。
- `404: Not Found`：如果用户是管理员但具有指定 ID 的令牌不存在。

## 撤回群组访问令牌

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77236) -->引入于极狐GitLab 14.7。

撤回[群组访问令牌](../user/group/settings/group_access_tokens.md)。

```plaintext
DELETE groups/:id/access_tokens/:token_id
```

| 参数         | 类型                | 是否必需 | 描述                                                          |
|------------|-------------------|------|-------------------------------------------------------------|
| `id`       | integer or string | yes  | 群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `token_id` | integer or string | yes  | 群组访问令牌的 ID                                                  |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/<group_id>/access_tokens/<token_id>"
```

### 响应

- 撤回成功：`204: No Content`
- 撤回失败：`400 Bad Request` 或 `404 Not Found`
