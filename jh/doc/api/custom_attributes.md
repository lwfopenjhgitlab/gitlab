---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 自定义属性 API **(FREE SELF)**

对自定义属性的每个 API 调用都必须通过管理员身份验证。

自定义属性当前可用于用户、群组和项目，在本文档中将它们统称为"资源"。

## 获取自定义属性列表

获取指定资源的所有自定义属性。

```plaintext
GET /users/:id/custom_attributes
GET /groups/:id/custom_attributes
GET /projects/:id/custom_attributes
```

| 参数  | 类型      | 是否必需 | 描述     |
|------|---------|------|--------|
| `id` | integer | 是    | 资源的 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/42/custom_attributes"
```

响应示例：

```json
[
   {
      "key": "location",
      "value": "Antarctica"
   },
   {
      "key": "role",
      "value": "Developer"
   }
]
```

## 获取单个指定的自定义属性

获取指定资源的指定单个自定义属性。

```plaintext
GET /users/:id/custom_attributes/:key
GET /groups/:id/custom_attributes/:key
GET /projects/:id/custom_attributes/:key
```

| 参数  | 类型      | 是否必需 | 描述      |
|------|---------|------|---------|
| `id` | integer | 是    | 资源的 ID  |
| `key` | string  | 是    | 自定义属性的键 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/42/custom_attributes/location"
```

响应示例：

```json
{
   "key": "location",
   "value": "Antarctica"
}
```

## 设置自定义属性

为指定资源设置自定义属性。如果这个自定义属性已经存在，则会更新该属性的值，否则将会为其创建全新的属性。

```plaintext
PUT /users/:id/custom_attributes/:key
PUT /groups/:id/custom_attributes/:key
PUT /projects/:id/custom_attributes/:key
```

| 参数名     | 类型      | 是否必须 | 描述      |
|---------|---------|------|---------|
| `id`    | integer | 是    | 资源的 ID  |
| `key`   | string  | 是    | 自定义属性的键 |
| `value` | string  | 是    | 自定义属性的值 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     --data "value=Greenland" "https://gitlab.example.com/api/v4/users/42/custom_attributes/location"
```

响应示例：

```json
{
   "key": "location",
   "value": "Greenland"
}
```

## 删除自定义属性

删除资源的指定自定义属性。

```plaintext
DELETE /users/:id/custom_attributes/:key
DELETE /groups/:id/custom_attributes/:key
DELETE /projects/:id/custom_attributes/:key
```

| 参数名   | 类型      | 是否必需 | 描述                             |
|-------|---------|------|--------------------------------|
| `id`  | integer | 是    | 资源的 ID                        |
| `key` | string  | 是    | 自定义属性的键 |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/42/custom_attributes/location"
```
