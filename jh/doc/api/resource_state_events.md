---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 资源状态事件 API **(FREE)**

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35210/)-->引入于极狐GitLab 13.2。

资源状态事件跟踪极狐GitLab [议题](../user/project/issues/index.md)、[合并请求](../user/project/merge_requests/index.md)和[极狐GitLab 15.4 版本及以上的史诗](../user/group/epics/index.md)，以追踪设置了哪个状态，是谁做的，以及何时发生的。

## 议题

### 列出项目议题状态事件

获取单个议题的所有状态事件的列表。

```plaintext
GET /projects/:id/issues/:issue_iid/resource_state_events
```

| 参数   | 类型           | 是否必需 | 描述                                                             |
| ----------- | -------------- | -------- |----------------------------------------------------------------|
| `id`        | integer/string | yes      | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `issue_iid` | integer        | yes      | 议题的 IID。                                                       |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/resource_state_events"
```
响应示例：

```json
[
  {
    "id": 142,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-20T13:38:20.077Z",
    "resource_type": "Issue",
    "resource_id": 11,
    "state": "opened"
  },
  {
    "id": 143,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-21T14:38:20.077Z",
    "resource_type": "Issue",
    "resource_id": 11,
    "state": "closed"
  }
]
```

### 获取单个议题状态事件

返回特定项目议题的单个状态事件。

```plaintext
GET /projects/:id/issues/:issue_iid/resource_state_events/:resource_state_event_id
```

参数:

| 参数   | 类型           | 是否必需 | 描述                                                             |
| ----------------------------- | -------------- | -------- |----------------------------------------------------------------|
| `id`                          | integer/string | yes      | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `issue_iid`                   | integer        | yes      | 议题的 IID。                                                       |
| `resource_state_event_id`     | integer        | yes      | 状态事件的 ID。                                                      |

请求示例:

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/resource_state_events/143"
```

响应示例：

```json
{
  "id": 143,
  "user": {
    "id": 1,
    "name": "Administrator",
    "username": "root",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://gitlab.example.com/root"
  },
  "created_at": "2018-08-21T14:38:20.077Z",
  "resource_type": "Issue",
  "resource_id": 11,
  "state": "closed"
}
```

## 合并请求

### 列出项目合并请求状态事件

获取单个合并请求的所有状态事件的列表。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/resource_state_events
```

| 参数   | 类型           | 是否必需 | 描述                                                             |
| ------------------- | -------------- | -------- |----------------------------------------------------------------|
| `id`                | integer/string | yes      | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `merge_request_iid` | integer        | yes      | 合并请求的 IID。                                                     |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/resource_state_events"
```

响应示例：

```json
[
  {
    "id": 142,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-20T13:38:20.077Z",
    "resource_type": "MergeRequest",
    "resource_id": 11,
    "state": "opened"
  },
  {
    "id": 143,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-21T14:38:20.077Z",
    "resource_type": "MergeRequest",
    "resource_id": 11,
    "state": "closed"
  }
]
```

### 获取单个合并请求状态事件

返回特定项目合并请求的单个状态事件。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/resource_state_events/:resource_state_event_id
```

参数:

| 参数   | 类型           | 是否必需 | 描述                                                             |
| ----------------------------- | -------------- | -------- |----------------------------------------------------------------|
| `id`                          | integer/string | yes      | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `merge_request_iid`           | integer        | yes      | 合并请求的 IID。                                                     |
| `resource_state_event_id`     | integer        | yes      | 状态事件的 ID。                                                      |

请求示例：

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/resource_state_events/120"
```

响应示例：

```json
{
  "id": 120,
  "user": {
    "id": 1,
    "name": "Administrator",
    "username": "root",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://gitlab.example.com/root"
  },
  "created_at": "2018-08-21T14:38:20.077Z",
  "resource_type": "MergeRequest",
  "resource_id": 11,
  "state": "closed"
}
```

## 史诗

> 引入于极狐GitLab 15.4。

### 列出群组史诗状态事件

返回单个史诗的所有状态事件列表。

```plaintext
GET /groups/:id/epics/:epic_id/resource_state_events
```

| 参数        | 类型             | 是否必需 | 描述                                                            |
|-----------|----------------|------|---------------------------------------------------------------|
| `id`      | integer/string | yes  | 群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `epic_id` | integer        | yes  | 史诗 ID                                                         |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/epics/11/resource_state_events"
```

响应示例：

```json
[
  {
    "id": 142,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-20T13:38:20.077Z",
    "resource_type": "Epic",
    "resource_id": 11,
    "state": "opened"
  },
  {
    "id": 143,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-21T14:38:20.077Z",
    "resource_type": "Epic",
    "resource_id": 11,
    "state": "closed"
  }
]
```

### 获取单个史诗状态事件

返回单个群组史诗的单个状态事件。

```plaintext
GET /groups/:id/epics/:epic_id/resource_state_events/:resource_state_event_id
```

参数：

| 参数                        | 类型             | 是否必需 | 描述                                                            |
|---------------------------|----------------|------|---------------------------------------------------------------|
| `id`                      | integer/string | yes  | 群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `epic_id`                 | integer        | yes  | 史诗 ID                                                         |
| `resource_state_event_id` | integer        | yes  | 状态事件 ID                                                       |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/epics/11/resource_state_events/143"
```

响应示例：

```json
{
  "id": 143,
  "user": {
    "id": 1,
    "name": "Administrator",
    "username": "root",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://gitlab.example.com/root"
  },
  "created_at": "2018-08-21T14:38:20.077Z",
  "resource_type": "Epic",
  "resource_id": 11,
  "state": "closed"
}
```

