---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 受保护的标签 API **(FREE)**

**有效访问级别**

当前级别为：

- `0`：无权限
- `30`：开发者角色
- `40`：维护者角色

## 列出受保护标签

> 部署 key 信息引入于极狐GitLab 16.0。

从项目中获取[受保护标签](../user/project/protected_tags.md)的列表。此功能使用分页参数 `page` 和 `per_page` 来限制受保护标签的列表。

```plaintext
GET /projects/:id/protected_tags
```

| 参数   | 类型             | 是否必需 | 描述                                                            |
|------|----------------|------|---------------------------------------------------------------|
| `id` | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/protected_tags"
```

响应示例：

```json
[
  {
    "name": "release-1-0",
    "create_access_levels": [
      {
        "id":1,
        "access_level": 40,
        "access_level_description": "Maintainers"
      },
      {
        "id": 2,
        "access_level": 40,
        "access_level_description": "Deploy key",
        "deploy_key_id": 1
      }
    ]
  },
  ...
]
```

## 获取单个受保护标签或通配符受保护标签

获取单个受保护标签或通配符受保护标签。

```plaintext
GET /projects/:id/protected_tags/:name
```

| 参数     | 类型             | 是否必需 | 描述                                                            |
|--------|----------------|------|---------------------------------------------------------------|
| `id`   | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `name` | string         | yes  | 标签或通配符的名称                      |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/protected_tags/release-1-0"
```

响应示例：

```json
{
  "name": "release-1-0",
  "create_access_levels": [
    {
      "id": 1,
      "access_level": 40,
      "access_level_description": "Maintainers"
    }
  ]
}
```

## 保护仓库标签

使用通配符保护标签保护单个仓库标签或多个项目仓库标签。

```plaintext
POST /projects/:id/protected_tags
```

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
   "https://gitlab.example.com/api/v4/projects/5/protected_tags" -d '{
   "allowed_to_create" : [
      {
         "user_id" : 1
      },
      {
         "access_level" : 30
      }
   ],
   "create_access_level" : 30,
   "name" : "*-stable"
}'
```

| 参数                    | 类型             | 是否必需 | 描述                                                                                                                                                                                                                     |
|-----------------------|----------------|------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                  | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)                                                                                                                                                          |
| `name`                | string         | yes  | 标签或通配符的名称                                                                                                                                                                                                              |
| `create_access_level` | string         | no   | 允许创建的访问级别（默认为 `40`，维护者角色）                                                                                                                                                                                              |
| `allowed_to_create`   | array          | no   | 允许创建标签的访问级别阵列，每个由格式为 `{user_id: integer}`、`{group_id: integer}` 或 `{access_level: integer}` 的哈希进行描述|

响应示例：

```json
{
  "name": "*-stable",
  "create_access_levels": [
    {
      "id": 1,
      "access_level": 30,
      "access_level_description": "Developers + Maintainers"
    }
  ]
}
```

## 取消保护仓库标签

取消保护特定的受保护标签或通配符受保护标签。

```plaintext
DELETE /projects/:id/protected_tags/:name
```

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/protected_tags/*-stable"
```

| 参数     | 类型             | 是否必需 | 描述                                                            |
|--------|----------------|------|---------------------------------------------------------------|
| `id`   | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `name` | string         | yes  | 标签名称                                                          |

## 相关主题

- 所有标签的[标签 API](tags.md)
- [标签](../user/project/repository/tags/index.md)用户文档
- [受保护的标签](../user/project/protected_tags.md)用户文档
