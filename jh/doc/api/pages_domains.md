---
stage: Plan
group: Knowledge
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Pages 域 API **(FREE)**

[极狐GitLab Pages](https://about.gitlab.cn/stages-devops-lifecycle/pages/) 中用于连接自定义域和 TLS 证书的端点。
必须启用极狐GitLab Pages 功能才能使用这些端点。更多内容请参见[管理](../administration/pages/index.md)和[使用](../user/project/pages/index.md)此功能的文档。

## 列出所有 Pages 域

先决条件：

- 您必须对实例具管理员访问权限。

获取所有 Pages 域的列表。

```plaintext
GET /pages/domains
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/pages/domains"
```

```json
[
  {
    "domain": "ssl.domain.example",
    "url": "https://ssl.domain.example",
    "project_id": 1337,
    "auto_ssl_enabled": false,
    "certificate": {
      "expired": false,
      "expiration": "2020-04-12T14:32:00.000Z"
    }
  }
]
```

## 列出 Pages 域

获取项目 Pages 域的列表。用户必需拥有查看 Pages 域的权限。

```plaintext
GET /projects/:id/pages/domains
```

| 参数   | 类型             | 是否必需 | 描述                                                                                            |
|------|----------------|------|-----------------------------------------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |


```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/pages/domains"
```

```json
[
  {
    "domain": "www.domain.example",
    "url": "http://www.domain.example"
  },
  {
    "domain": "ssl.domain.example",
    "url": "https://ssl.domain.example",
    "auto_ssl_enabled": false,
    "certificate": {
      "subject": "/O=Example, Inc./OU=Example Origin CA/CN=Example Origin Certificate",
      "expired": false,
      "certificate": "-----BEGIN CERTIFICATE-----\n … \n-----END CERTIFICATE-----",
      "certificate_text": "Certificate:\n … \n"
    }
  }
]
```

## 单个 Pages 域

获取单个项目 Pages 域。用户必需拥有查看 Pages 域的权限。

```plaintext
GET /projects/:id/pages/domains/:domain
```

| 参数       | 类型             | 是否必需 | 描述                                                            |
|----------|----------------|------|---------------------------------------------------------------|
| `id`     | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `domain` | string         | yes  | 用户指示的自定义域             |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/pages/domains/www.domain.example"
```

```json
{
  "domain": "www.domain.example",
  "url": "http://www.domain.example"
}
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/pages/domains/ssl.domain.example"
```

```json
{
  "domain": "ssl.domain.example",
  "url": "https://ssl.domain.example",
  "auto_ssl_enabled": false,
  "certificate": {
    "subject": "/O=Example, Inc./OU=Example Origin CA/CN=Example Origin Certificate",
    "expired": false,
    "certificate": "-----BEGIN CERTIFICATE-----\n … \n-----END CERTIFICATE-----",
    "certificate_text": "Certificate:\n … \n"
  }
}
```

## 创建 Pages 域

创建 Pages 域。用户必需拥有创建 Pages 域的权限。

```plaintext
POST /projects/:id/pages/domains
```

| 参数                 | 类型             | 是否必需 | 描述                                                                                                                               |
|--------------------|----------------|------|----------------------------------------------------------------------------------------------------------------------------------|
| `id`               | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding)                                                                    |
| `domain`           | string         | yes  | 用户指示的自定义域                                                                                                                        |
| `auto_ssl_enabled` | boolean        | no   | 启用由自定义域的 Let's Encrypt 签发的 SSL 证书的[自动生成](../user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.md) |
| `certificate`      | file/string    | no   | PEM 格式的证书，其中 intermediate 从最具体到最不具体进行排序                                                                |
| `key`              | file/string    | no   | PEM 格式的证书密钥                                                                                                                      |

创建带有 `.pem` 文件中的证书的 Pages 域：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --form "domain=ssl.domain.example" --form "certificate=@/path/to/cert.pem" \
     --form "key=@/path/to/key.pem" "https://gitlab.example.com/api/v4/projects/5/pages/domains"
```

使用包含证书的变量创建 Pages 域：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --form "domain=ssl.domain.example" --form "certificate=$CERT_PEM" \
     --form "key=$KEY_PEM" "https://gitlab.example.com/api/v4/projects/5/pages/domains"
```

创建带有[自动证书](../user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.md#enabling-lets-encrypt-integration-for-your-custom-domain)的 Pages 域：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --form "domain=ssl.domain.example" \
     --form "auto_ssl_enabled=true" "https://gitlab.example.com/api/v4/projects/5/pages/domains"
```

```json
{
  "domain": "ssl.domain.example",
  "url": "https://ssl.domain.example",
  "auto_ssl_enabled": true,
  "certificate": {
    "subject": "/O=Example, Inc./OU=Example Origin CA/CN=Example Origin Certificate",
    "expired": false,
    "certificate": "-----BEGIN CERTIFICATE-----\n … \n-----END CERTIFICATE-----",
    "certificate_text": "Certificate:\n … \n"
  }
}
```

## 更新 Pages 域

更新现存项目 Pages 域。用户必需拥有更改现存 Pages 域的权限。

```plaintext
PUT /projects/:id/pages/domains/:domain
```

| 参数                 | 类型             | 是否必需 | 描述                                                                                                                               |
|--------------------|----------------|------|----------------------------------------------------------------------------------------------------------------------------------|
| `id`               | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding)                                                                    |
| `domain`           | string         | yes  | 用户指示的自定义域                                                                                                                        |
| `auto_ssl_enabled` | boolean        | no   | 启用由自定义域的 Let's Encrypt 签发的 SSL 证书的[自动生成](../user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.md) |
| `certificate`      | file/string    | no   | PEM 格式的证书，其中 intermediate 从最具体到最不具体进行排序                           |
| `key`              | file/string    | no   | PEM 格式的证书密钥                                                                                                                      |

### 添加证书

添加 `.pem` 文件的 Pages 域的证书。

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" --form "certificate=@/path/to/cert.pem" \
     --form "key=@/path/to/key.pem" "https://gitlab.example.com/api/v4/projects/5/pages/domains/ssl.domain.example"
```

使用包含证书的变量添加 Pages 域的证书：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" --form "certificate=$CERT_PEM" \
     --form "key=$KEY_PEM" "https://gitlab.example.com/api/v4/projects/5/pages/domains/ssl.domain.example"
```

```json
{
  "domain": "ssl.domain.example",
  "url": "https://ssl.domain.example",
  "auto_ssl_enabled": false,
  "certificate": {
    "subject": "/O=Example, Inc./OU=Example Origin CA/CN=Example Origin Certificate",
    "expired": false,
    "certificate": "-----BEGIN CERTIFICATE-----\n … \n-----END CERTIFICATE-----",
    "certificate_text": "Certificate:\n … \n"
  }
}
```

### 为 Pages 自定义域启用 Let's Encrypt 集成

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     --form "auto_ssl_enabled=true" "https://gitlab.example.com/api/v4/projects/5/pages/domains/ssl.domain.example"
```

```json
{
  "domain": "ssl.domain.example",
  "url": "https://ssl.domain.example",
  "auto_ssl_enabled": true
}
```

### 移除证书

要移除与 Pages 域相关联的 SSL 证书，请运行：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" --form "certificate=" \
     --form "key=" "https://gitlab.example.com/api/v4/projects/5/pages/domains/ssl.domain.example"
```

```json
{
  "domain": "ssl.domain.example",
  "url": "https://ssl.domain.example",
  "auto_ssl_enabled": false
}
```

## 删除 Pages 域

删除现存的项目 Pages 域。

```plaintext
DELETE /projects/:id/pages/domains/:domain
```

| 参数       | 类型             | 是否必需 | 描述                                                            |
|----------|----------------|------|---------------------------------------------------------------|
| `id`     | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `domain` | string         | yes  | 用户指示的自定义域              |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/pages/domains/ssl.domain.example"
```
