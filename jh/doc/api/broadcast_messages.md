---
stage: Growth
group: Acquisition
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 广播消息 API **(FREE SELF)**

- > `target_access_levels` 引入于 14.8 版本，通过名为 `role_targeted_broadcast_messages` 的[功能标志](../administration/feature_flags.md)来控制。默认禁用。
- > `color` 参数移除于 15.6 版本。

广播消息 API 工作在[广播消息功能](../user/admin_area/broadcast_messages.md)之上。

在 12.8 版本中，对于 GET 请求不再需要任何授权操作。所有的广播消息 API 都只能被管理员访问，对于其他类型的请求来说：

- 访客的请求会得到 `401 Unauthorized`。
- 普通的用户会得到 `403 Forbidden`。

<a href="#get-all-broadcast-messages"></a>

## 获取所有的广播消息 **(FREE)**

列举出所有的广播消息。

```plaintext
GET /broadcast_messages
```

请求示例：

```shell
curl "https://gitlab.example.com/api/v4/broadcast_messages"
```

响应示例：

```json
[
  {
    "message":"Example broadcast message",
    "starts_at":"2016-08-24T23:21:16.078Z",
    "ends_at":"2016-08-26T23:21:16.080Z",
    "font":"#FFFFFF",
    "id":1,
    "active": false,
    "target_access_levels": [10,30],
    "target_path": "*/welcome",
    "broadcast_type": "banner",
    "dismissable": false
  }
]
```

<a href="#get-a-specific-broadcast-message"></a>

## 获取特定广播消息 **(FREE)**

获取特定广播消息。

```plaintext
GET /broadcast_messages/:id
```

参数：

| 参数 | 类型    | 是否必需 | 描述                      |
| :--- | :------ | :------- | :------------------------ |
| `id` | integer | yes      | 需要获取的广播消息的 ID。 |

请求示例：

```shell
curl "https://gitlab.example.com/api/v4/broadcast_messages/1"
```

响应示例：

```json
{
  "message":"Deploy in progress",
  "starts_at":"2016-08-24T23:21:16.078Z",
  "ends_at":"2016-08-26T23:21:16.080Z",
  "font":"#FFFFFF",
  "id":1,
  "active":false,
  "target_access_levels": [10,30],
  "target_path": "*/welcome",
  "broadcast_type": "banner",
  "dismissable": false
}
```

<a href="#create-a-broadcast-message"></a>

## 创建广播消息

创建新的广播消息。

```plaintext
POST /broadcast_messages
```

参数：

| 参数                   | 类型              | 是否必需 | 描述                                                                                     |
| :--------------------- | :---------------- | :------- | :--------------------------------------------------------------------------------------- |
| `message`              | string            | yes      | 要展示的消息。                                                                           |
| `starts_at`            | datetime          | no       | 开始时间（默认为当前 UTC 时间）。日期格式应当为 ISO 8601（如：`2019-03-15T08:00:00Z`）。 |
| `ends_at`              | datetime          | no       | 结束时间（默认为当前 UTC 时间）。日期格式应当为 ISO 8601（如：`2019-03-15T08:00:00Z`）。 |
| `font`                 | string            | no       | 16 进制的前景颜色值。                                                                    |
| `target_access_levels` | array of integers | no       | 广播消息的目标的访问权限（角色）。                                                       |
| `target_path`          | string            | no       | 广播消息的目标路径。                                                                     |
| `broadcast_type`       | string            | no       | 广播的外观类型（默认为横幅）。                                                           |
| `dismissable`          | boolean           | no       | 是否可以手动关闭广播消息。                                                               |

`target_access_levels` 在 `Gitlab::Access` 模块中定义。可以有以下几种权限（角色）：

- 访客（`10`）
- 报告者（`20`）
- 开发者（`30`）
- 维护者（`40`）
- 拥有者（`50`）

请求示例：

```shell
curl --data "message=Deploy in progress&target_access_levels[]=10&target_access_levels[]=30" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/broadcast_messages"
```

响应示例：

```json
{
  "message":"Deploy in progress",
  "starts_at":"2016-08-26T00:41:35.060Z",
  "ends_at":"2016-08-26T01:41:35.060Z",
  "font":"#FFFFFF",
  "id":1,
  "active": true,
  "target_access_levels": [10,30],
  "target_path": "*/welcome",
  "broadcast_type": "notification",
  "dismissable": false
}
```

<a href="#update-a-broadcast-message"></a>

## 更新广播消息

更新现存的广播消息。

```plaintext
PUT /broadcast_messages/:id
```

参数：

| 参数                   | 类型              | 是否必需 | 描述                                                                                   |
| :--------------------- | :---------------- | :------- |:-------------------------------------------------------------------------------------|
| `id`                   | integer           | yes      | 需要修改的广播消息的 ID。                                                                       |
| `message`              | string            | no       | 想要展示的消息内容。                                                                           |
| `starts_at`            | datetime          | no       | 开始时间（UTC）。日期格式应当为 ISO 8601（如：`2019-03-15T08:00:00Z`）。                                |
| `ends_at`              | datetime          | no       | 结束时间（UTC）。日期格式应当为 ISO 8601（如：`2019-03-15T08:00:00Z`）。                                |
| `font`                 | string            | no       | 16 进制的前景颜色值。                                                                         |
| `target_access_levels` | array of integers | no       | 广播消息的目标的访问权限（角色）。                                                                    |
| `target_path`          | string            | no       | 广播消息的目标路径。                                                                           |
| `broadcast_type`       | string            | no       | 广播的外观类型（默认为横幅）。                                                                      |
| `dismissable`          | boolean           | no       | 是否可以手动关闭广播消息。                                                                        |

`target_access_levels` 在 `Gitlab::Access` 模块中定义。可以有以下几种权限（角色）：

- 访客（`10`）
- 报告者（`20`）
- 开发者（`30`）
- 维护者（`40`）
- 拥有者（`50`）

请求示例：

```shell
curl --request PUT --data "message=Update message" \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/broadcast_messages/1"
```

响应示例：

```json
{
  "message":"Update message",
  "starts_at":"2016-08-26T00:41:35.060Z",
  "ends_at":"2016-08-26T01:41:35.060Z",
  "font":"#FFFFFF",
  "id":1,
  "active": true,
  "target_access_levels": [10,30],
  "target_path": "*/welcome",
  "broadcast_type": "notification",
  "dismissable": false
}
```

<a href="#delete-a-broadcast-message"></a>

## 删除一条广播消息

删除一条广播消息。

```plaintext
DELETE /broadcast_messages/:id
```

参数：

| 参数 | 类型    | 是否必需 | 描述                      |
| :--- | :------ | :------- | :------------------------ |
| `id` | integer | yes      | 想要删除的广播消息的 ID。 |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/broadcast_messages/1"
```
