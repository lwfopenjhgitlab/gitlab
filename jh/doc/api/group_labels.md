---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组标记 API **(FREE)**

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/21368) -->引入于极狐GitLab 11.8。

此 API 支持管理群组标签<!--[群组标签](../user/project/labels.md#types-of-labels)-->。
它允许用户列出、创建、更新和删除群组标签。此外，用户可以订阅或取消订阅群组标签。

NOTE:
`description_html` - 于极狐GitLab 12.7 添加到响应 JSON 中<!--[added](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/21413)-->。

## 列出群组标记

获取特定群组的所有标记。

```plaintext
GET /groups/:id/labels
```

| 参数                          | 类型             | 是否必需 | 描述                                                                                                                                |
|-----------------------------|----------------|------|-----------------------------------------------------------------------------------------------------------------------------------|
| `id`                        | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                                                     |
| `with_counts`               | boolean        | no   | 是否包括议题和合并请求的数量。默认为 `false`。<!--([Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/31543)-->引入于极狐GitLab 12.2 |
| `include_ancestor_groups`   | boolean        | no   | 包括上级群组。默认为 `true`                                                                                                                 |
| `include_descendant_groups` | boolean        | no   | 包括下级群组。默认为 `false`。<!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/259024)-->引入于极狐GitLab 13.6                      |
| `only_group_labels`         | boolean        | no   | 是否只包括群组标记。默认为 `true`。<!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/259024)-->引入于极狐GitLab 13.6            |
| `search`                    | string         | no   | 筛选标记的关键字。<!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/259024)-->引入于极狐GitLab 13.6                                |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/labels?with_counts=true"
```

响应示例：

```json
[
  {
    "id": 7,
    "name": "bug",
    "color": "#FF0000",
    "text_color" : "#FFFFFF",
    "description": null,
    "description_html": null,
    "open_issues_count": 0,
    "closed_issues_count": 0,
    "open_merge_requests_count": 0,
    "subscribed": false
  },
  {
    "id": 4,
    "name": "feature",
    "color": "#228B22",
    "text_color" : "#FFFFFF",
    "description": null,
    "description_html": null,
    "open_issues_count": 0,
    "closed_issues_count": 0,
    "open_merge_requests_count": 0,
    "subscribed": false
  }
]
```

## 获取单个群组标记

获取特定群组的单个标记。

```plaintext
GET /groups/:id/labels/:label_id
```

| 参数                          | 类型                | 是否必需 | 描述                                                                                                                     |
|-----------------------------|-------------------|------|------------------------------------------------------------------------------------------------------------------------|
| `id`                        | integer or string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                                          |
| `label_id`                  | integer or string | yes  | 群组标记的 ID 或标题                                                                                                           |
| `include_ancestor_groups`   | boolean           | no   | 包括上级群组。默认为 `true`                                                                                                      |
| `include_descendant_groups` | boolean           | no   | 包括下级群组。默认为 `false`。<!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/259024) -->引入于极狐GitLab 13.6          |
| `only_group_labels`         | boolean           | no   | 是否只包括群组标记。默认为 `true`。<!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/259024)-->引入于极狐GitLab 13.6 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/labels/bug"
```

响应示例：

```json
{
  "id": 7,
  "name": "bug",
  "color": "#FF0000",
  "text_color" : "#FFFFFF",
  "description": null,
  "description_html": null,
  "open_issues_count": 0,
  "closed_issues_count": 0,
  "open_merge_requests_count": 0,
  "subscribed": false
}
```

## 创建新的群组标记

为特定群组创建新的群组标记。

```plaintext
POST /groups/:id/labels
```

| 参数            | 类型             | 是否必需 | 描述                                                                                                                                     |
|---------------|----------------|------|----------------------------------------------------------------------------------------------------------------------------------------|
| `id`          | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                                                          |
| `name`        | string         | yes  | 标记名称                                                                                                                                   |
| `color`       | string         | yes  | 以 6 位十六进制表示法呈现的标记颜色，带有前导 '#' 符号（例如，#FFAABB）或 [CSS 颜色名称](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#Color_keywords)之一 |
| `description` | string         | no   | 标记描述                                                                                                                                   |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --header "Content-Type: application/json" \
     --data '{"name": "Feature Proposal", "color": "#FFA500", "description": "Describes new ideas" }' \
     "https://gitlab.example.com/api/v4/groups/5/labels"
```

响应示例：

```json
{
  "id": 9,
  "name": "Feature Proposal",
  "color": "#FFA500",
  "text_color" : "#FFFFFF",
  "description": "Describes new ideas",
  "description_html": "Describes new ideas",
  "open_issues_count": 0,
  "closed_issues_count": 0,
  "open_merge_requests_count": 0,
  "subscribed": false
}
```

## 更新群组标记

更新现存的群组标记。至少需要一个参数以更新群组标记。

```plaintext
PUT /groups/:id/labels/:label_id
```

| 参数            | 类型                | 是否必需 | 描述                                                                                                                                     |
|---------------|-------------------|------|----------------------------------------------------------------------------------------------------------------------------------------|
| `id`          | integer or string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                                                          |
| `label_id`    | integer or string | yes  | 群组标记的 ID 或标题                                                                                                                           |
| `new_name`    | string            | no   | 标记的新名称                                                                                                                                 |
| `color`       | string            | no   | 以 6 位十六进制表示法呈现的标记颜色，带有前导 '#' 符号（例如，#FFAABB）或 [CSS 颜色名称](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#Color_keywords)之一 |
| `description` | string            | no   | 标记描述                                                                                                                                   |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" --header "Content-Type: application/json" \
     --data '{"new_name": "Feature Idea" }' "https://gitlab.example.com/api/v4/groups/5/labels/Feature%20Proposal"
```

响应示例：

```json
{
  "id": 9,
  "name": "Feature Idea",
  "color": "#FFA500",
  "text_color" : "#FFFFFF",
  "description": "Describes new ideas",
  "description_html": "Describes new ideas",
  "open_issues_count": 0,
  "closed_issues_count": 0,
  "open_merge_requests_count": 0,
  "subscribed": false
}
```

NOTE:
参数中带有 `name` 的旧的端点 `PUT /groups/:id/labels` 仍可用，但已被废弃。

## 删除群组标记

删除特定名称的群组标记。

```plaintext
DELETE /groups/:id/labels/:label_id
```

| 参数         | 类型                | 是否必需 | 描述                                                            |
|------------|-------------------|------|---------------------------------------------------------------|
| `id`       | integer or string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `label_id` | integer or string | yes  | 群组标记的 ID 或标题                                                  |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/labels/bug"
```

NOTE:
参数中带有 `name` 的旧的端点 `DELETE /groups/:id/labels` 仍可用，但已被废弃。

## 订阅群组标记

为经过身份验证的用户订阅群组标记以接收通知。如果用户已经订阅了该标记，则返回状态码 `304`。

```plaintext
POST /groups/:id/labels/:label_id/subscribe
```

| 参数         | 类型                | 是否必需 | 描述                                                            |
|------------|-------------------|------|---------------------------------------------------------------|
| `id`       | integer or string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `label_id` | integer or string | yes  | 群组标记的 ID 或标题                                                  |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/labels/9/subscribe"
```

响应示例：

```json
{
  "id": 9,
  "name": "Feature Idea",
  "color": "#FFA500",
  "text_color" : "#FFFFFF",
  "description": "Describes new ideas",
  "description_html": "Describes new ideas",
  "open_issues_count": 0,
  "closed_issues_count": 0,
  "open_merge_requests_count": 0,
  "subscribed": true
}
```

## 取消订阅群组标记

取消订阅群组标记的经过身份验证的用户不再接收其通知。如果用户没有订阅标记，则返回状态码 `304`。

```plaintext
POST /groups/:id/labels/:label_id/unsubscribe
```

| 参数         | 类型                | 是否必需 | 描述                                                            |
|------------|-------------------|------|---------------------------------------------------------------|
| `id`       | integer or string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `label_id` | integer or string | yes  | 群组标记的 ID 或标题                                                  |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/labels/9/unsubscribe"
```

响应示例：

```json
{
  "id": 9,
  "name": "Feature Idea",
  "color": "#FFA500",
  "text_color" : "#FFFFFF",
  "description": "Describes new ideas",
  "description_html": "Describes new ideas",
  "open_issues_count": 0,
  "closed_issues_count": 0,
  "open_merge_requests_count": 0,
  "subscribed": false
}
```
