---
stage: Ecosystem
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 交互式 API 文档

介绍极狐GitLab API 的交互式文档工具。

## 关于 OpenAPI 规范

[OpenAPI 规范](https://swagger.io/specification/)（旧称为 Swagger）定义了一个
RESTful API 的标准的、与语言无关的接口。 OpenAPI 定义文件是 YAML 格式，由极狐GitLab 浏览器自动呈现为更易读的界面。

有关极狐GitLab API 的一般信息，请参阅 [API 文档](../index.md)。

## 总览

<!--
The following link is absolute rather than relative because it needs to be viewed through the GitLab
Open API file viewer: https://docs.gitlab.com/ee/user/project/repository/index.html#openapi-viewer.
-->

交互式 API 文档工具<!--[交互式 API 文档工具](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/api/openapi/openapi.yaml)-->允许直接在 JiHuLab.com 网站上进行 API 测试。只有少数可用的端点与 OpenAPI 规范一起记录，但当前列表演示了该工具的功能。

![API viewer screenshot](img/apiviewer01-fs8.png)

## 端点参数

当您展开端点列表时，您会看到描述、输入参数（如果需要）和示例服务器响应。一些参数包括默认值或允许值列表。

![API viewer screenshot](img/apiviewer04-fs8.png)

## 开始交互式会话

[个人访问令牌](../../user/profile/personal_access_tokens.md) (PAT) 是一种开始交互式会话的方法。
为此，请在主页中选择 **Authorize**，然后对话框会提示您输入您的 PAT，该 PAT 对当前 Web 会话有效。

要测试端点，首先在端点定义页面上选择 **Try it out**。根据需要输入参数，然后选择 **Execute**。在下面的示例中，我们执行了对 `version` 端点（不需要参数）的请求。
该工具显示请求的 `curl` 命令和 URL，然后是返回的服务器响应。您可以通过编辑相关参数来创建新响应，然后再次选择 **Execute**。

![API viewer screenshot](img/apiviewer03-fs8.png)

## 愿景

API 代码是唯一的事实来源，API 文档应该与其实现紧密耦合。OpenAPI 规范提供了一种标准化且全面的 API 文档记录方法。它应该是记录极狐GitLab REST API 的首选格式，会产生更准确、可靠和用户友好的文档，从而增强使用极狐GitLab REST API 的整体体验。

为了实现这一目标，应该要求每次 API 代码更改时都更新 OpenAPI 规范。这样，我们可以确保文档始终是最新且准确的，从而减少用户混淆和错误的风险。

OpenAPI 文档应根据 API 代码自动生成，以保持最新且准确。这将为我们的文档团队节省时间和精力。

<!--You can follow the current progress of this vision in the [Document the REST API in OpenAPI V2 epic](https://gitlab.com/groups/gitlab-org/-/epics/8926).-->
