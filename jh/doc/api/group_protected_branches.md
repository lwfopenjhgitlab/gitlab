---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组级别的保护分支 API **(PREMIUM SELF)**

> 引入于极狐GitLab 15.9，[功能标志](../administration/feature_flags.md)为 `group_protected_branches`。默认禁用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能不可用。要使其可用，请让管理员启用名为 `group_protected_branches` 的[功能标志](../administration/feature_flags.md)。
在 JiHuLab.com 上，此功能不可用。

## 有效访问级别

访问级别在 `ProtectedRefAccess.allowed_access_levels` 方法中定义。
访问级别为：

```plaintext
0  => No access
30 => Developer access
40 => Maintainer access
60 => Admin access
```

## 列出受保护分支

从群组中获取受保护分支的列表。如果设置了通配符，则返回通配符而不是与该通配符匹配的分支的具体名称。

```plaintext
GET /groups/:id/protected_branches
```

| 参数       | 类型                | 是否必需 | 描述                                                                                                      |
|----------|-------------------|------|---------------------------------------------------------------------------------------------------------|
| `id`     | integer or string | yes  | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `search` | string            | no   | 要搜索的受保护分支的名称或部分名称                                      |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/5/protected_branches"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "master",
    "push_access_levels": [
      {
        "id":  1,
        "access_level": 40,
        "user_id": null,
        "group_id": 1234,
        "access_level_description": "Maintainers"
      }
    ],
    "merge_access_levels": [
      {
        "id":  1,
        "access_level": 40,
        "user_id": null,
        "group_id": 1234,
        "access_level_description": "Maintainers"
      }
    ],
    "allow_force_push":false,
    "code_owner_approval_required": false
  },
  {
    "id": 1,
    "name": "release/*",
    "push_access_levels": [
      {
        "id":  1,
        "access_level": 40,
        "user_id": null,
        "group_id": null,
        "access_level_description": "Maintainers"
      }
    ],
    "merge_access_levels": [
      {
        "id":  1,
        "access_level": 40,
        "user_id": null,
        "group_id": 1234,
        "access_level_description": "Maintainers"
      }
    ],
    "allow_force_push":false,
    "code_owner_approval_required": false
  },
  ...
]
```

## 获取单个受保护分支或通配符受保护分支 

获取单个受保护分支或通配符受保护分支。

```plaintext
GET /groups/:id/protected_branches/:name
```

| 参数     | 类型                | 是否必需 | 描述                                                                      |
|--------|-------------------|------|-------------------------------------------------------------------------|
| `id`   | integer or string | yes  | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `name` | string            | yes  | 分支或通配符的名称                                                               |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/5/protected_branches/master"
```

响应示例：

```json
{
  "id": 1,
  "name": "master",
  "push_access_levels": [
    {
      "id":  1,
      "access_level": 40,
      "user_id": null,
      "group_id": null,
      "access_level_description": "Maintainers"
    }
  ],
  "merge_access_levels": [
    {
      "id":  1,
      "access_level": null,
      "user_id": null,
      "group_id": 1234,
      "access_level_description": "Example Merge Group"
    }
  ],
  "allow_force_push":false,
  "code_owner_approval_required": false
}
```

## 保护仓库分支

使用通配符受保护分支保护单个仓库分支。

```plaintext
POST /groups/:id/protected_branches
```

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/5/protected_branches?name=*-stable&push_access_level=30&merge_access_level=30&unprotect_access_level=40"
```


| 参数                             |类型 | 是否必需 | 说明                                                                                           |
|--------------------------------| ---- |------|----------------------------------------------------------------------------------------------|
| `id`                           |integer or string | yes    | 经过身份验证的用户拥有的群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                     |
| `name`                           |string | yes    | 分支或通配符的名称                                                                                    |
| `allow_force_push`             |boolean | no   | 允许所有具有推送权限的用户强制推送。默认值：`false`                                                                |
| `allowed_to_merge`             |array | no   | 允许合并的访问级别数组，每个访问级别由形式为 `{user_id: integer}`、`{group_id: integer}` 或 `{access_level: integer}`的哈希描述 |
| `allowed_to_push`              |array | no   | 允许推送的访问级别数组，每个级别都由形式为 `{user_id: integer}`、`{group_id: integer}` 或 `{access_level: integer}` 的哈希描述 |
| `allowed_to_unprotect`         |array | no   | 允许取消保护的访问级别数组，每个级别都由形式为 `{user_id: integer}`、`{group_id: integer}` 或 `{access_level: integer}` 的哈希描述 |
| `code_owner_approval_required` |boolean | no   | 如果分支与 [`CODEOWNERS` 文件](../user/project/codeowners/index.md)中的项目匹配，则阻止推送到该分支。默认值：`false`     |
| `merge_access_level`           |integer | no   | 允许合并的访问级别。默认值：`40`，维护者角色                                                                     |
| `push_access_level`            |integer | no   | 允许推送的访问级别。默认值：`40`，维护者角色                                                                     |
| `unprotect_access_level`       |integer | no   | 允许取消保护的访问级别。默认值：`40`，维护者角色                                                                   |

响应示例：

```json
{
  "id": 1,
  "name": "*-stable",
  "push_access_levels": [
    {
      "id":  1,
      "access_level": 30,
      "user_id": null,
      "group_id": null,
      "access_level_description": "Developers + Maintainers"
    }
  ],
  "merge_access_levels": [
    {
      "id":  1,
      "access_level": 30,
      "user_id": null,
      "group_id": null,
      "access_level_description": "Developers + Maintainers"
    }
  ],
  "unprotect_access_levels": [
    {
      "id":  1,
      "access_level": 40,
      "user_id": null,
      "group_id": null,
      "access_level_description": "Maintainers"
    }
  ],
  "allow_force_push":false,
  "code_owner_approval_required": false
}
```

### 具有用户/群组级别访问权限的示例

`allowed_to_push` / `allowed_to_merge` / `allowed_to_unprotect` 数组中的元素应采用 `{user_id: integer}`、`{group_id: integer}` 或`{access_level: integer}` 的形式。每个用户都必须有权访问该项目，并且每个群组都必须[共享此项目](../user/project/members/share_project_with_groups.md)。这些访问级别允许[对受保护分支访问进行更精细的控制](../user/project/protected_branches.md)。

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/5/protected_branches?name=*-stable&allowed_to_push%5B%5D%5Buser_id%5D=1"
```

响应示例：

```json
{
  "id": 1,
  "name": "*-stable",
  "push_access_levels": [
    {
      "id":  1,
      "access_level": null,
      "user_id": 1,
      "group_id": null,
      "access_level_description": "Administrator"
    }
  ],
  "merge_access_levels": [
    {
      "id":  1,
      "access_level": 40,
      "user_id": null,
      "group_id": null,
      "access_level_description": "Maintainers"
    }
  ],
  "unprotect_access_levels": [
    {
      "id":  1,
      "access_level": 40,
      "user_id": null,
      "group_id": null,
      "access_level_description": "Maintainers"
    }
  ],
  "allow_force_push":false,
  "code_owner_approval_required": false
}
```

### 拥有允许推送和允许合并访问权限的示例

请求示例：

```shell
curl --request POST \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     --header "Content-Type: application/json" \
     --data '{
      "name": "master",
      "allowed_to_push": [{"access_level": 30}],
      "allowed_to_merge": [{
          "access_level": 30
        },{
          "access_level": 40
        }
      ]}'
     "https://gitlab.example.com/api/v4/groups/5/protected_branches"
```

响应示例：

```json
{
    "id": 5,
    "name": "master",
    "push_access_levels": [
        {
            "id": 1,
            "access_level": 30,
            "access_level_description": "Developers + Maintainers",
            "user_id": null,
            "group_id": null
        }
    ],
    "merge_access_levels": [
        {
            "id": 1,
            "access_level": 30,
            "access_level_description": "Developers + Maintainers",
            "user_id": null,
            "group_id": null
        },
        {
            "id": 2,
            "access_level": 40,
            "access_level_description": "Maintainers",
            "user_id": null,
            "group_id": null
        }
    ],
    "unprotect_access_levels": [
        {
            "id": 1,
            "access_level": 40,
            "access_level_description": "Maintainers",
            "user_id": null,
            "group_id": null
        }
    ],
    "allow_force_push":false,
    "code_owner_approval_required": false
}
```

## 取消保护仓库分支

取消保护特定受保护分支或通配符受保护分支。

```plaintext
DELETE /groups/:id/protected_branches/:name
```

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/5/protected_branches/*-stable"
```

| 参数     | 类型 | 是否必需 | 描述 |
|--------| -- | -------- | ----------- |
| `id`   | integer or string | yes | 经过身份验证的用户拥有的群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `name` | string | yes | 分支名称 |

响应示例：

```json
{
   "name": "master",
   "push_access_levels": [
      {
         "id": 12,
         "access_level": 40,
         "access_level_description": "Maintainers",
         "user_id": null,
         "group_id": null
      }
   ]
}
```

## 更新受保护分支

更新受保护分支。

```plaintext
PATCH /groups/:id/protected_branches/:name
```

```shell
curl --request PATCH --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/5/protected_branches/feature-branch?allow_force_push=true&code_owner_approval_required=true"
```

| 参数                             | 类型                | 是否必需 | 描述                                                                                     |
|--------------------------------|-------------------|------|----------------------------------------------------------------------------------------|
| `id`                           | integer or string | yes  | 经过身份验证的用户拥有的群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)               |
| `name`                         | string            | yes  | 分支名称                                                                                   |
| `allow_force_push`             | boolean           | no   | 启用后，可以向此分支推送的成员也可以进行强制推送                                                               |
| `allowed_to_push`              | array             | no   | 推送访问级别数组，由哈希描述                                                                         |
| `allowed_to_merge`             | array             | no   | 合并访问级别数组，由哈希描述                                                                         |
| `allowed_to_unprotect`         | array             | no   | 取消保护访问级别数组，由哈希描述                                                                       |
| `code_owner_approval_required` | boolean           | no   | 如果分支匹配 [`CODEOWNERS` 文件](../user/project/codeowners/index.md)中的项，则阻止向该分支推送。默认值：`false` |


`allowed_to_push`、`allowed_to_merge` 和 `allowed_to_unprotect` 数组中的元素应该：

- 是 `user_id`、`group_id` 或 `access_level` 之一。
- 采用 `{user_id: integer}`、`{group_id: integer}` 或 `{access_level: integer}` 的形式。

要更新：

- `user_id`：确保更新后的用户有权访问该项目。您还必须在相应的哈希中传递 `access_level` 的 `id`。
- `group_id`：确保更新的群组[已共享此项目](../user/project/members/share_project_with_groups.md)。
  您还必须在相应的哈希中传递 `access_level` 的 `id`。

要删除：

- 您必须将 `_destroy` 组传递为 `true`。请参阅以下示例。

### 示例：创建 `push_access_level` 记录

```shell
curl --header 'Content-Type: application/json' --request PATCH \
     --data '{"allowed_to_push": [{access_level: 40}]}' \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/22034114/protected_branches/master"
```

响应示例：

```json
{
   "name": "master",
   "push_access_levels": [
      {
         "id": 12,
         "access_level": 40,
         "access_level_description": "Maintainers",
         "user_id": null,
         "group_id": null
      }
   ]
}
```

### 示例：更新 `push_access_level` 记录

```shell
curl --header 'Content-Type: application/json' --request PATCH \
     --data '{"allowed_to_push": [{"id": 12, "access_level": 0}]' \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/22034114/protected_branches/master"
```

响应示例：

```json
{
   "name": "master",
   "push_access_levels": [
      {
         "id": 12,
         "access_level": 0,
         "access_level_description": "No One",
         "user_id": null,
         "group_id": null
      }
   ]
}
```

### 示例：删除 `push_access_level` 记录

```shell
curl --header 'Content-Type: application/json' --request PATCH \
     --data '{"allowed_to_push": [{"id": 12, "_destroy": true}]}' \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/22034114/protected_branches/master"
```

响应示例：

```json
{
   "name": "master",
   "push_access_levels": []
}
```
