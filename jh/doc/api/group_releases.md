---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组发布 API **(FREE)**

使用 REST API 评审您的群组[发布](../user/project/releases/index.md)。

NOTE:
更多关于项目发布 API 的内容，请访问[发布 API](releases/index.md) 页面。

FLAG:
在私有化部署的极狐GitLab 上，此功能默认不可用。如果想使用此功能，您可以让管理员为您开启名为 `group_releases_finder_inoperator` 的[功能标志](../administration/feature_flags.md)。

## 列出群组发布

返回群组发布列表。

```plaintext
GET /groups/:id/releases
GET /groups/:id/releases?simple=true
```

参数：

| 参数       | 类型             | 是否必需 | 描述                                                            |
|----------|----------------|------|---------------------------------------------------------------|
| `id`     | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `sort`   | string         | no   | 排序方向。降序为 `desc`（默认），升序为 `asc`                                 |
| `simple` | boolean        | no   | 仅为每个发布返回受限的字段                                                 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/releases"
```

响应示例：

```json
[
  {
    "name": "standard release",
    "tag_name": "releasetag",
    "description": "",
    "created_at": "2022-01-10T15:23:15.529Z",
    "released_at": "2022-01-10T15:23:15.529Z",
    "author": {
      "id": 1,
      "username": "root",
      "name": "Administrator",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "https://gitlab.com/root"
    },
    "commit": {
      "id": "e8cbb845ae5a53a2fef2938cf63cf82efc10d993",
      "short_id": "e8cbb845",
      "created_at": "2022-01-10T15:20:29.000+00:00",
      "parent_ids": [],
      "title": "Update test",
      "message": "Update test",
      "author_name": "Administrator",
      "author_email": "admin@example.com",
      "authored_date": "2022-01-10T15:20:29.000+00:00",
      "committer_name": "Administrator",
      "committer_email": "admin@example.com",
      "committed_date": "2022-01-10T15:20:29.000+00:00",
      "trailers": {},
      "web_url": "https://gitlab.com/groups/gitlab-org/-/commit/e8cbb845ae5a53a2fef2938cf63cf82efc10d993"
    },
    "upcoming_release": false,
    "commit_path": "/testgroup/test/-/commit/e8cbb845ae5a53a2fef2938cf63cf82efc10d993",
    "tag_path": "/testgroup/test/-/tags/testtag"
  }
]
```
