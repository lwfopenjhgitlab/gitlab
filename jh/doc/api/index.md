---
stage: Manage
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 使用极狐GitLab 开发 **(FREE)**

实现自动化并与极狐GitLab 交互，并与外部应用程序集成。

- [集成](../integration/index.md)
- [Webhook](../user/project/integrations/webhooks.md)
- [REST API](rest/index.md)
- [GraphQL API](graphql/index.md)
- [OAuth 2.0 身份认证供应商 API](oauth2.md)
<!--- [极狐GitLab CLI (glab)](../integration/glab/index.md)
- [Visual Studio Code 扩展](../user/project/repository/vscode.md)
- [Code Suggestions](../user/project/repository/code_suggestions.md)-->
