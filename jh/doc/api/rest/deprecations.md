---
stage: Manage
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# REST API 废弃与移除

移除 v4 API 后，将发生以下 API 更改。

此更改的日期未知。
<!--For details, see [issue 216456](https://gitlab.com/gitlab-org/gitlab/-/issues/216456)
and [issue 387485](https://gitlab.com/gitlab-org/gitlab/-/issues/387485).-->

## `geo_nodes` API 端点

突破性变更。 <!--[Related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/369140).-->

[`geo_nodes` API 端点](../geo_nodes.md)已弃用，取而代之的是 [`geo_sites`](../geo_sites.md)。
它是关于[如何引用 Geo 部署](../../administration/geo/glossary.md)的全局更改的一部分。
节点在整个应用程序中被重命名为站点。两个端点的功能保持不变。

## `merged_by` API 字段

突破性变更。<!--[Related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/350534).-->

[合并请求 API](../merge_requests.md#list-merge-requests) 中的 `merged_by` 字段已被弃用，取而代之的是 `merge_user` 字段，它可以在执行操作（设置为自动合并，添加到合并队列）时更正确地识别合并请求的合并者，而不仅是简单的合并。

我们鼓励 API 用户使用新的 `merge_user` 字段。`merged_by` 字段将在极狐GitLab REST API 的 v5 中移除。

## `merge_status` API 字段

突破性变更。<!--[Related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/382032)-->

[合并请求 API](../merge_requests.md#merge-status) 中的 `merge_status` 字段已被弃用，取而代之的是 `detailed_merge_status` 字段，它可以更正确地识别合并请求的所有潜在状态。
我们鼓励 API 用户使用新的 `detailed_merge_status` 字段。`merge_status` 字段将在极狐GitLab REST API 的 v5 中删除。

###  用户 API 中 `private_profile` 参数的空值

突破性变更。<!--[Related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/387005).-->

通过 API 创建和更新用户时，`null` 是 `private_profile` 参数的有效值，其将在内部转换为默认值。在极狐GitLab REST API 的 v5 中，`null` 将不再是此参数的有效值；如果使用，响应将是 400。进行此更改后，唯一有效的值将是 `true` 和 `false`。

## 单个合并请求更改 API 端点

突破性变更。<!--[Related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/322117)-->

获取[来自单个合并请求的更改](../merge_requests.md#get-single-merge-request-changes)的端点已被弃用，取而代之的是[列出合并请求差异](../merge_requests.md#list-merge-request-diffs)端点。
我们鼓励 API 用户使用新的 diffs 端点。

极狐GitLab REST API 的 v5 中将删除 `changes from a single merge request` 端点。

## 管理许可证 API 端点

突破性变更。<!--[Related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/397067).-->

获取[给定项目的所有管理许可证](../managed_licenses.md)的端点已被弃用，取而代之的是[许可证批准策略](../../user/compliance/license_approval_policies.md)功能。
我们鼓励希望继续根据检测到的许可证进行批准的用户创建新的[许可证批准策略](../../user/compliance/license_approval_policies.md)。

`managed licenses` 端点将在极狐GitLab REST API 的 v5 中被删除。

## 合并请求批准 API 中的批准者和批准者群组字段

突破性变更。<!--[Related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/353097).-->

获取项目批准配置的端点返回 `approvers` 和 `approval_groups` 的空数组。
这些字段已弃用，取而代之的是为合并请求[获取项目级规则](../merge_request_approvals.md#get-project-level-rules)的端点。我们鼓励 API 用户改为切换到此端点。

这些字段将从极狐GitLab REST API v5 的 `get configuration` 端点中删除。

## Runner 中使用 `paused` 取代 `active`

突破性变更。<!--[Related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/351109).-->

极狐GitLab Runner GraphQL API 端点中出现的 `active` 标识符将在 16.0 中重命名为 `paused`。

- 在 REST API 的 v4 中，从 14.8 开始，您可以使用 `paused` 代替 `active`
- 在 REST API 的 v5 中，此更改将影响获取或返回 `active` 参数的端点，例如：
    - `GET /runners`
    - `GET /runners/all`
    - `GET /runners/:id` / `PUT /runners/:id`
    - `PUT --form "active=false" /runners/:runner_id`
    - `GET /projects/:id/runners` / `POST /projects/:id/runners`
    - `GET /groups/:id/runners`

极狐GitLab Runner 的 16.0 版本将在注册 Runner 时使用 `paused` 参数。

## Runner 状态不返回 `paused`

突破性变更。<!--[Related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/344648).-->

在 REST API 的未来 v5 中，极狐GitLab Runner 的端点将不会返回 `paused` 或 `active`。

Runner 的状态只会与 Runner 联系状态相关，例如：
`online`、`offline` 或 `not_connected`。`paused` 或 `active` 状态将不再出现。

在检查 Runner 是否 `paused` 时，我们建议 API 用户检查布尔参数 `paused` 是否为 `true`。在检查 Runner 是否为 `active` 时，请检查 `paused` 是否为 `false`。
