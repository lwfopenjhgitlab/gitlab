---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目里程碑 API **(FREE)**

使用 REST API 项目 [里程碑](../user/project/milestones/index.md)。

## 列举项目里程碑

返回项目里程碑列表。

```plaintext
GET /projects/:id/milestones
GET /projects/:id/milestones?iids[]=42
GET /projects/:id/milestones?iids[]=42&iids[]=43
GET /projects/:id/milestones?state=active
GET /projects/:id/milestones?state=closed
GET /projects/:id/milestones?title=1.0
GET /projects/:id/milestones?search=version
GET /projects/:id/milestones?updated_before=2013-10-02T09%3A24%3A18Z
GET /projects/:id/milestones?updated_after=2013-10-02T09%3A24%3A18Z
```

参数：

| 参数                         | 类型   | 是否必需 | 描述 |
| ----------------------------      | ------ |--| ----------- |
| `id`                              | integer 或 string | 是 | 认证用户拥有的项目 ID 或 [URL 编码路径](rest/index.md#namespaced-path-encoding) |
| `iids[]`                          | integer array | 可选 | 仅返回具有给定 `iid` 的里程碑 (注：当 `include_parent_milestones` 被设置为 `true` 时会被忽略) |
| `state`                           | string | 可选 | 仅返回 `active` 或 `closed` 的里程碑 |
| `title`                           | string | 可选 | 仅返回具有给定 `title` 的里程碑 |
| `search`                          | string | 可选 | 仅返回标题或描述与给定字符串匹配的里程碑 |
| `include_parent_milestones`       | boolean | 可选 | 包括来自上级群组的里程碑 |
| `updated_before`                  | datetime | 否 | 仅返回特定时间之前更新的里程碑。预计格式为 ISO 8601（`2019-03-15T08:00:00Z`）。引入于 15.10 |
| `updated_after`                   | datetime | 否 | 仅返回特定时间之后更新的里程碑。预计格式为 ISO 8601（`2019-03-15T08:00:00Z`）。引入于 15.10 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/milestones"
```

响应示例：

```json
[
  {
    "id": 12,
    "iid": 3,
    "project_id": 16,
    "title": "10.0",
    "description": "Version",
    "due_date": "2013-11-29",
    "start_date": "2013-11-10",
    "state": "active",
    "updated_at": "2013-10-02T09:24:18Z",
    "created_at": "2013-10-02T09:24:18Z",
    "expired": false
  }
]
```

## 获取单个里程碑

获取单个项目里程碑。

```plaintext
GET /projects/:id/milestones/:milestone_id
```

参数：

| 参数      | 类型           | 是否必需 | 描述                                                                                                     |
|----------------|----------------|----------|---------------------------------|
| `id`           | integer 或 string | 是      | 认证用户拥有的项目 ID 或 [URL 编码路径](rest/index.md#namespaced-path-encoding) |
| `milestone_id` | integer        | 是    | 项目里程碑 ID     |

## 创建新的里程碑

创建一个新的项目里程碑。

```plaintext
POST /projects/:id/milestones
```

参数：

| 参数     | 类型           | 是否必需 | 描述                                                                |
|---------------|----------------|----------|-------------------------------------------------------------------|
| `id`          | integer 或 string | 是      | 认证用户拥有的项目 ID 或 [URL 编码路径](rest/index.md#namespaced-path-encoding) |
| `title`       | string         | 是      | 里程碑的标题                                                            |
| `description` | string         | 否       | 里程碑的描述                                                            |
| `due_date`    | string         | 否       | 里程碑的截止日期（`YYYYMMDD`）                                              |
| `start_date`  | string         | 否       | 里程碑的开始日期（`YYYYMMDD`）                                                        |

## 编辑里程碑

编辑里程碑。

```plaintext
PUT /projects/:id/milestones/:milestone_id
```

参数：

| 参数      | 类型           | 是否必需 | 描述                                                                                                    |
|----------------|----------------|----------|----------------------------------------------------------------------------------------------------------------|
| `id`           | integer 或 string | 是      | 认证用户拥有的项目 ID 或 [URL 编码路径](rest/index.md#namespaced-path-encoding) |
| `milestone_id` | integer        | 是      | 项目里程碑的ID                        |
| `title`        | string         | 否       | 里程碑的标题                         |
| `description`  | string         | 否       | 里程碑的描述                       |
| `due_date`     | string         | 否       | 里程碑的截止日期（`YYYYMMDD`）                                 |
| `start_date`   | string         | 否       | 里程碑的开始日期（`YYYYMMDD`）                       |
| `state_event`  | string         | 否       | 里程碑的状态事件（closed 或 active）       |

## 删除项目里程碑

> 在 15.0 版本中，将最低用户角色从开发者更改为报告者。

仅适用于项目中至少具有报告者角色的用户。

```plaintext
DELETE /projects/:id/milestones/:milestone_id
```

参数：

| 参数      | 类型           | 是否必需 | 描述                          |
|----------------|----------------|----------|---------------------------------------------|
| `id`           | integer 或 string | 是      | 认证用户拥有的项目 ID 或 [URL 编码路径](rest/index.md#namespaced-path-encoding) |
| `milestone_id` | integer        | 是      | 项目里程碑的 ID                       |

## 获取分配给单个里程碑的所有议题

获取分配给单个项目里程碑的所有议题。

```plaintext
GET /projects/:id/milestones/:milestone_id/issues
```

参数：

| 参数      | 类型           | 是否必需 | 描述                                                           |
|----------------|----------------|----------|-------------------------------------------|
| `id`           | integer 或 string | 是      | 认证用户拥有的项目 ID 或 [URL 编码路径](rest/index.md#namespaced-path-encoding) |
| `milestone_id` | integer        | 是      | 项目里程碑的 ID                                    |

## 获取分配给单个里程碑的所有合并请求

获取分配给单个项目里程碑的所有合并请求。

```plaintext
GET /projects/:id/milestones/:milestone_id/merge_requests
```

参数：

| 参数      | 类型           | 是否必需 | 描述                                                              |
|----------------|----------------|----------|--------------------------------------|
| `id`           | integer 或 string | 是      | 认证用户拥有的项目 ID 或 [URL 编码路径](rest/index.md#namespaced-path-encoding) |
| `milestone_id` | integer        | 是      | 项目里程碑的ID                                    |

## 将项目里程碑提升为群组里程碑

> 在 15.0 版本中，将最低用户角色从开发者更改为报告者。

仅适用于项目中至少具有报告者角色的用户。

```plaintext
POST /projects/:id/milestones/:milestone_id/promote
```

参数：

| 参数      | 类型           | 是否必需 | 描述                                                   |
|----------------|----------------|----------|----------------------------------------------|
| `id`           | integer 或 string | 是      | 认证用户拥有的项目 ID 或 [URL 编码路径](rest/index.md#namespaced-path-encoding) |
| `milestone_id` | integer        | 是      | 项目里程碑的 ID                                   |

## 获取单个里程碑的所有燃尽图事件 **(PREMIUM)**

> 在 13.9 版本中移至专业版。

获取单个里程碑的所有燃尽图事件。

```plaintext
GET /projects/:id/milestones/:milestone_id/burndown_events
```

参数：

| 参数      | 类型           | 是否必需 | 描述                                                   |
|----------------|----------------|----------|--------------------------------------------------------------|
| `id`           | integer 或 string | 是      | 认证用户拥有的项目 ID 或 [URL 编码路径](rest/index.md#namespaced-path-encoding) |
| `milestone_id` | integer        | 是      | 项目里程碑的 ID                      |
