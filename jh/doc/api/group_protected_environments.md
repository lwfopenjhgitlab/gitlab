---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: concepts, howto
---

# 群组级受保护环境 API **(PREMIUM)**

> - <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/215888) -->引入于极狐GitLab 14.0。部署在 `group_level_protected_environments` 标志后，默认禁用。
> - <!--[Feature flag `group_level_protected_environments`](https://gitlab.com/gitlab-org/gitlab/-/issues/331085) -->功能标记 `group_level_protected_environments` 移除于极狐GitLab 14.3。
> - <!--[Generally available](https://gitlab.com/gitlab-org/gitlab/-/issues/331085) -->普遍可用于极狐GitLab 14.3。

您还可以阅读[群组级受保护环境](../ci/environments/protected_environments.md#group-level-protected-environments)。

## 有效访问级别

`ProtectedEnvironments::DeployAccessLevel::ALLOWED_ACCESS_LEVELS` 中定义了访问级别。级别为：

```plaintext
30 => Developer access
40 => Maintainer access
60 => Admin access
```

## 列出群组级受保护环境

获取群组的受保护环境的列表。

```plaintext
GET /groups/:id/protected_environments
```

| 参数   | 类型             | 是否必需 | 描述                                                            |
|------|----------------|------|---------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户维护的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/protected_environments/"
```

响应示例：

```json
[
  {
    "name":"production",
    "deploy_access_levels":[
      {
        "id": 12,
        "access_level": 40,
        "access_level_description": "Maintainers",
        "user_id": null,
        "group_id": null
      }
    ],
    "required_approval_count": 0
  }
]
```

## 获取单个受保护环境

获取单个受保护环境。

```plaintext
GET /groups/:id/protected_environments/:name
```

| 参数     | 类型             | 是否必需 | 描述                                                                                                                                                    |
|--------|----------------|------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`   | integer/string | yes  | 经过身份验证的用户维护的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                                                                         |
| `name` | string         | yes  | 受保护环境的部署级别。是 `production`、`staging`、`testing`、`development` 或 `other` 之一。阅读更多关于[部署级别](../ci/environments/index.md#deployment-tier-of-environments)的内容 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/protected_environments/production"
```

响应示例：

```json
{
  "name":"production",
  "deploy_access_levels":[
    {
      "id": 12,
      "access_level":40,
      "access_level_description":"Maintainers",
      "user_id":null,
      "group_id":null
    }
  ],
  "required_approval_count": 0
}
```

## 保护单个环境

保护单个环境。

```plaintext
POST /groups/:id/protected_environments
```

| 参数                        | 类型             | 是否必需 | 描述                                                                                                                                                                                                                                                                              |
|---------------------------|----------------|------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                      | integer/string | yes  | 经过身份验证的用户维护的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                                                                                                                                                                                                   |
| `name`                    | string         | yes  | 受保护环境的部署级别。是 `production`、`staging`、`testing`、`development` 或 `other` 之一。阅读更多关于[部署级别](../ci/environments/index.md#deployment-tier-of-environments)的内容                                                                                                                           |
| `deploy_access_levels`    | array          | yes  | 允许部署的访问级别的阵列，通过哈希描述。是 `user_id`、`group_id` 或 `access_level` 之一。它们各自采用 `{user_id: integer}`、`{group_id: integer}` 或 `{access_level: integer}` 的形式                                                                                                                                |
| `required_approval_count` | integer        | no   | 部署到环境所需的批准的数量。是部署批准的一部分，暂时不可用                                                                                                                                                                                                                                                   |
| `approval_rules`          | array          | no   | 允许批准的访问级别的阵列，通过哈希描述。是 `user_id`、`group_id` 或 `access_level` 之一。它们各自采用 `{user_id: integer}`、`{group_id: integer}` 或 `{access_level: integer}` 的形式。您也可以使用 `required_approvals` 字段指定特定实体所需的批准的数量。详情请参见[多个批准规则](../ci/environments/deployment_approvals.md#multiple-approval-rules) |

可分配的 `user_id` 是属于特定群组的拥有维护者角色（及以上）的用户。
可分配的 `group_id` 是特定群组下面的子群组。

```shell
curl --header 'Content-Type: application/json' --request POST --data '{"name": "production", "deploy_access_levels": [{"group_id": 9899826}]}' --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/22034114/protected_environments"
```

响应示例：

```json
{
  "name":"production",
  "deploy_access_levels":[
    {
      "id": 12,
      "access_level": 40,
      "access_level_description": "protected-access-group",
      "user_id": null,
      "group_id": 9899826
    }
  ],
  "required_approval_count": 0
}
```

## 更新受保护环境

> 引入于极狐GitLab 15.4。

更新单个环境。

```plaintext
PUT /groups/:id/protected_environments/:name
```

| 参数                        | 类型             | 是否必需 | 描述                                                                                                                                                                                                                                                                |
|---------------------------|----------------|------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                      | integer/string | yes  | 经过身份验证的用户维护的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                                                                                                                                                                                     |
| `name`                    | string         | yes  | 受保护环境的部署级别：`production`、`staging`、`testing`、`development` 或 `other`。更多内容请参见[部署级别](../ci/environments/index.md#deployment-tier-of-environments)                                                                                                                    |
| `deploy_access_levels`    | array          | no   | 哈希描述的允许部署的访问级别阵列：`user_id`、`group_id` 或 `access_level`。形式为 `{user_id: integer}`、`{group_id: integer}` 或 `{access_level: integer}`                                                                                                                                 |
| `required_approval_count` | integer        | no   | 需要部署到此环境中的批准数量                                                                                                                                                                                                                                                    |
| `approval_rules`          | array          | no   | 哈希描述的允许批准的访问级别阵列：`user_id`、`group_id` 或 `access_level`。形式为 `{user_id: integer}`、`{group_id: integer}` 或 `{access_level: integer}`。您也可以使用 `required_approvals` 字段指定特定实体必需批准的数量。更多内容请参见[多个批准规则](../ci/environments/deployment_approvals.md#multiple-approval-rules) |

更新：

- **`user_id`**：确保更新后的用户属于具有维护者角色（或以上）的特定群组。您还必须在相应的哈希中传递 `deploy_access_level` 或 `approval_rule` 的 `id`。
- **`group_id`**：确保更新后的群组是此受保护环境所属群组的子组。您还必须在相应的哈希中传递 `deploy_access_level` 或 `approval_rule` 的 `id`。

删除：

- 您必须将 `_destroy` 传递为 `true`，参见下例。

### 示例：创建 `deploy_access_level` 记录

```shell
curl --header 'Content-Type: application/json' --request PUT \
     --data '{"deploy_access_levels": [{"group_id": 9899829, access_level: 40}], "required_approval_count": 1}' \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/22034114/protected_environments/production"
```

响应示例：

```json
{
   "name": "production",
   "deploy_access_levels": [
      {
         "id": 12,
         "access_level": 40,
         "access_level_description": "protected-access-group",
         "user_id": null,
         "group_id": 9899829,
         "group_inheritance_type": 1
      }
   ],
   "required_approval_count": 0
}
```

### 示例：更新 `deploy_access_level` 记录

```shell
curl --header 'Content-Type: application/json' --request PUT \
     --data '{"deploy_access_levels": [{"id": 12, "group_id": 22034120}], "required_approval_count": 2}' \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/22034114/protected_environments/production"
```

```json
{
   "name": "production",
   "deploy_access_levels": [
      {
         "id": 12,
         "access_level": 40,
         "access_level_description": "protected-access-group",
         "user_id": null,
         "group_id": 22034120,
         "group_inheritance_type": 0
      }
   ],
   "required_approval_count": 2
}
```

### 示例：删除 `deploy_access_level` 记录

```shell
curl --header 'Content-Type: application/json' --request PUT \
     --data '{"deploy_access_levels": [{"id": 12, "_destroy": true}], "required_approval_count": 0}' \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/22034114/protected_environments/production"
```

响应示例：

```json
{
   "name": "production",
   "deploy_access_levels": [],
   "required_approval_count": 0
}
```

### 示例：创建 `approval_rule` 记录

```shell
curl --header 'Content-Type: application/json' --request PUT \
     --data '{"approval_rules": [{"group_id": 134, "required_approvals": 1}]}' \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/22034114/protected_environments/production"
```

响应示例：

```json
{
   "name": "production",
   "approval_rules": [
      {
         "id": 38,
         "user_id": null,
         "group_id": 134,
         "access_level": null,
         "access_level_description": "qa-group",
         "required_approvals": 1,
         "group_inheritance_type": 0
      }
   ]
}
```

### 示例：更新 `approval_rule` 记录

```shell
curl --header 'Content-Type: application/json' --request PUT \
     --data '{"approval_rules": [{"id": 38, "group_id": 135, "required_approvals": 2}]}' \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/22034114/protected_environments/production"
```

```json
{
   "name": "production",
   "approval_rules": [
      {
         "id": 38,
         "user_id": null,
         "group_id": 135,
         "access_level": null,
         "access_level_description": "security-group",
         "required_approvals": 2,
         "group_inheritance_type": 0
      }
   ]
}
```

### 示例：删除 `approval_rule` 记录

```shell
curl --header 'Content-Type: application/json' --request PUT \
     --data '{"approval_rules": [{"id": 38, "_destroy": true}]}' \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/22034114/protected_environments/production"
```

响应示例：

```json
{
   "name": "production",
   "approval_rules": []
}
```

## 不保护单个环境

不保护特定的受保护环境。

```plaintext
DELETE /groups/:id/protected_environments/:name
```

| 参数     | 类型             | 是否必需 | 描述                                                                                                                                                    |
|--------|----------------|------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`   | integer/string | yes  | 经过身份验证的用户维护的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                                                                         |
| `name` | string         | yes  | 受保护环境的部署级别。是 `production`、`staging`、`testing`、`development` 或 `other` 之一。阅读更多关于[部署级别](../ci/environments/index.md#deployment-tier-of-environments)的内容 |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/protected_environments/staging"
```

响应应该返回 200。
