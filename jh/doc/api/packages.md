---
stage: Package
group: Package Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 软件包 API **(FREE)**

> 在极狐GitLab 15.3 中为项目级 API 引入了对[极狐GitLab CI/CD 作业令牌](../ci/jobs/ci_job_token.md)身份验证的支持。

本文主要介绍[极狐GitLab 软件包](../administration/packages/index.md)的 API。

## 列出软件包

### 在项目内

获取项目软件包列表。所有软件包类型都包含在结果中。进行未授权访问时，仅返回公共项目的软件包。
默认情况下，返回具有 `default` 和 `error` 状态的包。使用 `status` 参数查看其他软件包。

默认情况下，返回具有 `default` 和 `error` 状态的包。使用 `status` 参数查看其他包。

```plaintext
GET /projects/:id/packages
```

| 参数                    | 类型             | 是否必需 | 描述                                                                                                            |
|-----------------------|----------------|------|---------------------------------------------------------------------------------------------------------------|
| `id`                  | integer/string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)                                                         |
| `order_by`            | string         | no   | 用作排序的字段：`created_at`（默认）、`name`、`version` 或 `type`                                                            |
| `sort`                | string         | no   | 排序顺序。升序：`asc`（默认）或降序：`desc`                                                                                   |
| `package_type`        | string         | no   | 按类型过滤返回的软件包：`conan`、`maven`、`npm`、`pypi`、`composer`、`nuget`、`helm`、`terraform_module` 或 `golang`（引入于 12.9） 
| `package_name`        | string         | no   | 使用名称进行模糊搜索，过滤项目软件包（引入于 12.9）                                                                                  
| `include_versionless` | boolean        | no   | 设置为 True 时，响应中包括无版本控制的软件包（引入于 13.8）                                                                           
| `status`              | string         | no   | 使用状态过滤返回的软件包：`default`（默认）、`hidden`、`processing`、`error` 或 `pending_destruction`（引入于 13.9）                

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/packages"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "com/mycompany/my-app",
    "version": "1.0-SNAPSHOT",
    "package_type": "maven",
    "created_at": "2019-11-27T03:37:38.711Z"
  },
  {
    "id": 2,
    "name": "@foo/bar",
    "version": "1.0.3",
    "package_type": "npm",
    "created_at": "2019-11-27T03:37:38.711Z"
  },
  {
    "id": 3,
    "name": "Hello/0.1@mycompany/stable",
    "conan_package_name": "Hello",
    "version": "0.1",
    "package_type": "conan",
    "_links": {
      "web_path": "/foo/bar/-/packages/3",
      "delete_api_path": "https://gitlab.example.com/api/v4/projects/1/packages/3"
    },
    "created_at": "2029-12-16T20:33:34.316Z",
    "tags": []
  }
]
```

默认情况下，`GET` 请求返回 20 个结果，因为 API 是[分页](rest/index.md#pagination)的。

尽管您可以按状态过滤软件包，但使用具有 `processing` 状态的包可能会导致数据格式错误或软件包损坏。

### 群组内

> 引入于极狐GitLab 12.5。

获取群组级别的项目包列表。进行未授权访问时，仅返回公共项目的包。
默认情况下，返回具有 `default` 和 `error` 状态的软件包。使用 `status` 参数查看其他软件包。

默认情况下，返回具有 `default` 和 `error` 状态的包。使用 `status` 参数查看其他包。

```plaintext
GET /groups/:id/packages
```

| 参数                    | 类型             | 是否必需  | 描述                                                                                          |
|-----------------------|----------------|-------|---------------------------------------------------------------------------------------------|
| `id`                  | integer/string | yes   | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                       |
| `exclude_subgroups`   | boolean        | false | 如果参数被包括为 True，则不会列出子群组的项目中的软件包。默认为 `false`                                                  |
| `order_by`            | string         | no    | 用作排序的字段：`created_at`（默认）、`name`、`version`、`type` 或 `project_path`                           |
| `sort`                | string         | no    | 排序顺序。升序：`asc`（默认）或降序：`desc`                                                                 |
| `package_type`        | string         | no    | 根据类型过滤返回的软件包。`conan`、`maven`、`npm`、`pypi`、`composer`、`nuget`、`helm` 或 `golang`（引入于 12.9） |
| `package_name`        | string         | no    | 使用名称进行模糊搜索，过滤项目软件包（引入于 13.0）                                                                
| `include_versionless` | boolean        | no    | 设置为 True 时，响应中包括无版本控制的软件包（引入于 13.8）                                                         
| `status`              | string         | no    | 使用状态过滤返回的软件包。`default`（默认）、`hidden`、`processing`、`error` 或 `pending_destruction`（引入于 13.9）  

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/packages?exclude_subgroups=false"
```

> **已废弃：**
>
> 响应中的 `pipeline` 属性已替换为 `pipelines`，引入于 13.6。两个属性在 13.7 前都可用。
> 响应中的 `build_info` 属性已替换为 `pipeline`，引入于 12.10。

响应示例：

```json
[
  {
    "id": 1,
    "name": "com/mycompany/my-app",
    "version": "1.0-SNAPSHOT",
    "package_type": "maven",
    "_links": {
      "web_path": "/namespace1/project1/-/packages/1",
      "delete_api_path": "/namespace1/project1/-/packages/1"
    },
    "created_at": "2019-11-27T03:37:38.711Z",
    "pipelines": [
      {
        "id": 123,
        "status": "pending",
        "ref": "new-pipeline",
        "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
        "web_url": "https://example.com/foo/bar/pipelines/47",
        "created_at": "2016-08-11T11:28:34.085Z",
        "updated_at": "2016-08-11T11:32:35.169Z",
        "user": {
          "name": "Administrator",
          "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon"
        }
      }
    ]
  },
  {
    "id": 2,
    "name": "@foo/bar",
    "version": "1.0.3",
    "package_type": "npm",
    "_links": {
      "web_path": "/namespace1/project1/-/packages/1",
      "delete_api_path": "/namespace1/project1/-/packages/1"
    },
    "created_at": "2019-11-27T03:37:38.711Z",
    "pipelines": [
      {
        "id": 123,
        "status": "pending",
        "ref": "new-pipeline",
        "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
        "web_url": "https://example.com/foo/bar/pipelines/47",
        "created_at": "2016-08-11T11:28:34.085Z",
        "updated_at": "2016-08-11T11:32:35.169Z",
        "user": {
          "name": "Administrator",
          "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon"
        }
      }
    ]
  }
]
```

默认情况下，`GET` 请求返回 20 个结果，因为 API 是[分页](rest/index.md#pagination)的。

`_links` 对象包含以下属性：

- `web_path`：您可以在极狐GitLab 中访问并查看软件包详细信息的路径。
- `delete_api_path`：删除软件包的 API 路径。仅当请求用户有权限时才可用。

尽管您可以按状态过滤软件包，但使用具有 `processing` 状态的软件包可能会导致数据格式错误或软件包损坏。

## 获取项目软件包

获取单个项目软件包。仅返回状态为 `default` 的软件包。

```plaintext
GET /projects/:id/packages/:package_id
```

| 参数           | 类型             | 是否必需 | 描述                                                    |
|--------------|----------------|------|-------------------------------------------------------|
| `id`         | integer/string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `package_id` | integer        | yes  | 软件包 ID                                 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/packages/:package_id"
```

> **已废弃：**
>
> 响应中的 `pipeline` 属性已替换为 `pipelines`，引入于 13.6。两个属性在 13.7 之前都可用。
> 响应中的 `build_info` 属性已替换为 `pipeline`，引入于 12.10。

响应示例：

```json
{
  "id": 1,
  "name": "com/mycompany/my-app",
  "version": "1.0-SNAPSHOT",
  "package_type": "maven",
  "_links": {
    "web_path": "/namespace1/project1/-/packages/1",
    "delete_api_path": "/namespace1/project1/-/packages/1"
  },
  "created_at": "2019-11-27T03:37:38.711Z",
  "last_downloaded_at": "2022-09-07T07:51:50.504Z"
  "pipelines": [
    {
      "id": 123,
      "status": "pending",
      "ref": "new-pipeline",
      "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
      "web_url": "https://example.com/foo/bar/pipelines/47",
      "created_at": "2016-08-11T11:28:34.085Z",
      "updated_at": "2016-08-11T11:32:35.169Z",
      "user": {
        "name": "Administrator",
        "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon"
      }
    }
  ],
  "versions": [
    {
      "id":2,
      "version":"2.0-SNAPSHOT",
      "created_at":"2020-04-28T04:42:11.573Z",
      "pipelines": [
        {
          "id": 234,
          "status": "pending",
          "ref": "new-pipeline",
          "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
          "web_url": "https://example.com/foo/bar/pipelines/58",
          "created_at": "2016-08-11T11:28:34.085Z",
          "updated_at": "2016-08-11T11:32:35.169Z",
          "user": {
            "name": "Administrator",
            "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon"
          }
        }
      ]
    }
  ]
}
```

`_links` 对象包含以下属性：

- `web_path`：您可以在极狐GitLab 中访问并查看软件包详细信息的路径。仅当软件包的状态为 `default` 时可用。
- `delete_api_path`：删除软件包的 API 路径。仅当请求用户有权限时才可用。

## 列出软件包文件

获取单个软件包的软件包文件列表。

```plaintext
GET /projects/:id/packages/:package_id/package_files
```

| 参数           | 类型             | 是否必需 | 描述                                                    |
|--------------|----------------|------|-------------------------------------------------------|
| `id`         | integer/string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `package_id` | integer        | yes  | 软件包 ID                               |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/packages/4/package_files"
```

响应示例：

```json
[
  {
    "id": 25,
    "package_id": 4,
    "created_at": "2018-11-07T15:25:52.199Z",
    "file_name": "my-app-1.5-20181107.152550-1.jar",
    "size": 2421,
    "file_md5": "58e6a45a629910c6ff99145a688971ac",
    "file_sha1": "ebd193463d3915d7e22219f52740056dfd26cbfe",
    "file_sha256": "a903393463d3915d7e22219f52740056dfd26cbfeff321b",
    "pipelines": [
      {
        "id": 123,
        "status": "pending",
        "ref": "new-pipeline",
        "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
        "web_url": "https://example.com/foo/bar/pipelines/47",
        "created_at": "2016-08-11T11:28:34.085Z",
        "updated_at": "2016-08-11T11:32:35.169Z",
        "user": {
          "name": "Administrator",
          "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon"
        }
      }
    ]
  },
  {
    "id": 26,
    "package_id": 4,
    "created_at": "2018-11-07T15:25:56.776Z",
    "file_name": "my-app-1.5-20181107.152550-1.pom",
    "size": 1122,
    "file_md5": "d90f11d851e17c5513586b4a7e98f1b2",
    "file_sha1": "9608d068fe88aff85781811a42f32d97feb440b5",
    "file_sha256": "2987d068fe88aff85781811a42f32d97feb4f092a399"
  },
  {
    "id": 27,
    "package_id": 4,
    "created_at": "2018-11-07T15:26:00.556Z",
    "file_name": "maven-metadata.xml",
    "size": 767,
    "file_md5": "6dfd0cce1203145a927fef5e3a1c650c",
    "file_sha1": "d25932de56052d320a8ac156f745ece73f6a8cd2",
    "file_sha256": "ac849d002e56052d320a8ac156f745ece73f6a8cd2f3e82"
  }
]
```

默认情况下，`GET` 请求返回 20 个结果，因为 API 是[分页](rest/index.md#pagination)的。

## 列出软件包流水线

> 引入于极狐GitLab 16.1。

获取单个软件包的流水线列表。结果按 `id` 降序排序。

结果是[分页](rest/index.md#keyset-based-pagination)的，每页最多返回 20 条记录。

```plaintext
GET /projects/:id/packages/:package_id/pipelines
```

| 参数           | 类型             | 是否必需 | 描述                                                         |
|--------------|----------------|------|------------------------------------------------------------|
| `id`         | integer/string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `package_id` | integer        | yes  | 软件包 ID                                       |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/packages/:package_id/pipelines"
```

响应示例：

```json
[
  {
    "id": 1,
    "iid": 1,
    "project_id": 9,
    "sha": "2b6127f6bb6f475c4e81afcc2251e3f941e554f9",
    "ref": "mytag",
    "status": "failed",
    "source": "push",
    "created_at": "2023-02-01T12:19:21.895Z",
    "updated_at": "2023-02-01T14:00:05.922Z",
    "web_url": "http://gdk.test:3001/feature-testing/composer-repository/-/pipelines/1",
    "user": {
      "id": 1,
      "username": "root",
      "name": "Administrator",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80\u0026d=identicon",
      "web_url": "http://gdk.test:3001/root"
    }
  },
  {
    "id": 2,
    "iid": 2,
    "project_id": 9,
    "sha": "e564015ac6cb3d8617647802c875b27d392f72a6",
    "ref": "master",
    "status": "canceled",
    "source": "push",
    "created_at": "2023-02-01T12:23:23.694Z",
    "updated_at": "2023-02-01T12:26:28.635Z",
    "web_url": "http://gdk.test:3001/feature-testing/composer-repository/-/pipelines/2",
    "user": {
      "id": 1,
      "username": "root",
      "name": "Administrator",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80\u0026d=identicon",
      "web_url": "http://gdk.test:3001/root"
    }
  }
]
```

## 删除项目软件包

删除项目软件包。

```plaintext
DELETE /projects/:id/packages/:package_id
```

| 参数           | 类型             | 是否必需 | 描述                                                    |
|--------------|----------------|------|-------------------------------------------------------|
| `id`         | integer/string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `package_id` | integer        | yes  | 软件包 ID                                  |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/packages/:package_id"
```

可以返回以下状态码：

- 成功删除：`204 No Content`
- 无法找到软件包：`404 Not Found`

<!--If [request forwarding](../user/packages/package_registry/supported_functionality.md#forwarding-requests) is enabled,
deleting a package can introduce a [dependency confusion risk](../user/packages/package_registry/supported_functionality.md#deleting-packages).-->

## 删除软件包文件

> 引入于极狐GitLab 13.12。

WARNING:
删除软件包文件可能会损坏您的软件包，使其无法使用或无法从您的 Package Manager 客户端拉出。删除软件包文件时，请确保您确定要这么做。

删除软件包文件：

```plaintext
DELETE /projects/:id/packages/:package_id/package_files/:package_file_id
```

| 参数                | 类型             | 是否必需 | 描述                                                    |
|-------------------|----------------|------|-------------------------------------------------------|
| `id`              | integer/string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `package_id`      | integer        | yes  | 软件包 ID                                                |
| `package_file_id` | integer        | yes  | 软件包文件 ID                             |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/packages/:package_id/package_files/:package_file_id"
```

可以返回以下状态码：

- 成功删除：`204 No Content`
- 用户没有权限删除：`403 Forbidden`
- 无法找到软件包或软件包文件：`404 Not Found`
