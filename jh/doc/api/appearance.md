---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 外观 API **(FREE SELF)**

外观 API 允许您维护极狐GitLab 的外观，就好像您在 `/admin/appearance` 使用极狐GitLab 的界面。此 API 需要管理员权限。

## 获取当前外观配置

列出极狐GitLab 实例的当前外观配置。

```plaintext
GET /application/appearance
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/application/appearance"
```

响应示例：

```json
{
  "title": "GitLab Test Instance",
  "description": "gitlab-test.example.com",
  "pwa_name": "GitLab PWA",
  "pwa_short_name": "GitLab",
  "pwa_description": "GitLab as PWA",
  "pwa_icon": "/uploads/-/system/appearance/pwa_icon/1/pwa_logo.png",
  "logo": "/uploads/-/system/appearance/logo/1/logo.png",
  "header_logo": "/uploads/-/system/appearance/header_logo/1/header.png",
  "favicon": "/uploads/-/system/appearance/favicon/1/favicon.png",
  "new_project_guidelines": "Please read the FAQs for help.",
  "profile_image_guidelines": "Custom profile image guidelines",
  "header_message": "",
  "footer_message": "",
  "message_background_color": "#e75e40",
  "message_font_color": "#ffffff",
  "email_header_and_footer_enabled": false
}
```

## 更改外观配置

使用 API 调用来修改极狐GitLab 实例外观配置。

```plaintext
PUT /application/appearance
```

| 参数                                | 类型     | 是否必需 | 描述                                                                                                                              |
|-----------------------------------|--------| -------- |---------------------------------------------------------------------------------------------------------------------------------|
| `title`                           | string | no       | 登录/注册页面上的实例标题。                                                                                                                  
| `description`                     | string | no       | 登录/注册页面上显示的 Markdown 格式的文本。                                                                                                     
| `pwa_name`                        | string | no       | Progressive Web App 的全名。用于 `manifest.json` 中的 `name` 参数。引入于极狐GitLab 15.8。                                                       |
| `pwa_short_name`                  | string | no       | Progressive Web App 的简称。引入于极狐GitLab 15.8。                                                                                       |
| `pwa_description`                 | string | no       | Progressive Web App 作用的解释。用于 `manifest.json` 中的 `description` 参数。引入于极狐GitLab 15.8。                                              |
| `pwa_icon`                        | mixed  | no       | 用于 Progressive Web App 的图标。请参阅[更改标识](#change-logo)。引入于极狐GitLab 15.8。 |
| `logo`                            | mixed  | no       | 登录/注册页面上使用的实例图像。请参阅[更改标识](#change-logo)。                                                                                     
| `header_logo`                     | mixed  | no       | 用于主导航栏的实例图像。                                                                                                                    
| `favicon`                         | mixed  | no       | `.ico` 或 `.png` 格式的 favicon 图标。                                                                                                 
| `new_project_guidelines`          | string | no       | 新项目页面上显示的 Markdown 格式的文本。                                                                                                       
| `profile_image_guidelines`        | string | no       | 头像下方的个人资料页面上显示的 Markdown 格式的文本。                                                                                                 
| `header_message`                  | string | no       | 系统标题栏中的消息。                                                                                                                      
| `footer_message`                  | string | no       | 系统页脚栏中的消息。                                                                                                                      
| `message_background_color`        | string | no       | 系统页眉/页脚栏的背景颜色。                                                                                                                  
| `message_font_color`              | string | no       | 系统页眉/页脚栏的字体颜色。                                                                                                                  
| `email_header_and_footer_enabled` | boolean | no       | 如果启用，则为所有外发电子邮件添加页眉和页脚。                                                                                                         

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/application/appearance?email_header_and_footer_enabled=true&header_message=test"
```

响应示例：

```json
{
  "title": "GitLab Test Instance",
  "description": "gitlab-test.example.com",
  "pwa_name": "GitLab PWA",
  "pwa_short_name": "GitLab",
  "pwa_description": "GitLab as PWA",
  "pwa_icon": "/uploads/-/system/appearance/pwa_icon/1/pwa_logo.png",
  "logo": "/uploads/-/system/appearance/logo/1/logo.png",
  "header_logo": "/uploads/-/system/appearance/header_logo/1/header.png",
  "favicon": "/uploads/-/system/appearance/favicon/1/favicon.png",
  "new_project_guidelines": "Please read the FAQs for help.",
  "profile_image_guidelines": "Custom profile image guidelines",
  "header_message": "test",
  "footer_message": "",
  "message_background_color": "#e75e40",
  "message_font_color": "#ffffff",
  "email_header_and_footer_enabled": true
}
```

<a id="change-logo"></a>

## 更改标识

将 logo 上传到您的极狐GitLab 实例。

要从文件系统上传头像，请使用 `--form` 参数。这会让 cURL 使用标头 `Content-Type: multipart/form-data` 发布数据。`file=` 参数必须指向您文件系统上的一个图像文件并且是前面有个 `@` 符号。

```plaintext
PUT /application/appearance
```

| 参数          | 类型    | 是否必需 | 描述           |
|-------------|-------| ------- |--------------|
| `logo`      | mixed | Yes     | 要上传的文件       |
| `pwa_icon`  | mixed | Yes    | 要上传的文件，引入于极狐GitLab 15.8 |

请求示例：

```shell
curl --location --request PUT "https://gitlab.example.com/api/v4/application/appearance?data=image/png" \
--header "Content-Type: multipart/form-data" \
--header "PRIVATE-TOKEN: <your_access_token>" \
--form "logo=@/path/to/logo.png"
```

返回对象：

```json
{
  "logo":"/uploads/-/system/appearance/logo/1/logo.png"
}
```
