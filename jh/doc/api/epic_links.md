---
stage: Plan
group: Product Planning
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 史诗关联 API **(ULTIMATE)**

管理父子[史诗关联](../user/group/epics/manage_epics.md#multi-level-child-epics)。

对 `epic_links` 的每个 API 调用都必须经过认证。

如果一个用户不是某个群组的成员，并且该群组是私有的，对该群组的 `GET` 请求
的请求会返回 `404` 的状态码。

多级史诗仅在[旗舰版](https://about.gitlab.cn/pricing/)中可用。
如果多级史诗功能不可用，将返回 `403` 状态码。

## 列出与给定史诗相关的史诗

获取一个史诗的所有子史诗。

```plaintext
GET /groups/:id/epics/:epic_iid/epics
```

| 参数       | 类型             | 是否必需 | 描述                                                                       |
| ---------- | -------------- | -------- |--------------------------------------------------------------------------|
| `id`       | integer/string | yes      | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `epic_iid` | integer        | yes      | 史诗的内部 ID。                                                                |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/epics/5/epics/"
```

响应示例：

```json
[
  {
    "id": 29,
    "iid": 6,
    "group_id": 1,
    "parent_id": 5,
    "title": "Accusamus iste et ullam ratione voluptatem omnis debitis dolor est.",
    "description": "Molestias dolorem eos vitae expedita impedit necessitatibus quo voluptatum.",
    "author": {
      "id": 10,
      "name": "Lu Mayer",
      "username": "kam",
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/018729e129a6f31c80a6327a30196823?s=80&d=identicon",
      "web_url": "http://localhost:3001/kam"
    },
    "start_date": null,
    "start_date_is_fixed": false,
    "start_date_fixed": null,
    "start_date_from_milestones": null,       //deprecated in favor of start_date_from_inherited_source
    "start_date_from_inherited_source": null,
    "end_date": "2018-07-31",                 //deprecated in favor of due_date
    "due_date": "2018-07-31",
    "due_date_is_fixed": false,
    "due_date_fixed": null,
    "due_date_from_milestones": "2018-07-31", //deprecated in favor of start_date_from_inherited_source
    "due_date_from_inherited_source": "2018-07-31",
    "created_at": "2018-07-17T13:36:22.770Z",
    "updated_at": "2018-07-18T12:22:05.239Z",
    "labels": []
  }
]
```

## 指定一个子史诗

在两个史诗之间创建一个关联，指定一个为父史诗，另一个为子史诗。一个父史诗可以有多个子史诗。如果新的子史诗已经属于另一个史诗，那么它就会从之前的父史诗中取消指派。


```plaintext
POST /groups/:id/epics/:epic_iid/epics/:child_epic_id
```

| 参数       | 类型             | 是否必需 | 描述                                                       |
| --------------- | -------------- | -------- | ------------------------------------------------------------------------------------------------------------------ |
| `id`            | integer/string | yes      | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。      |
| `epic_iid`      | integer        | yes      | 史诗的内部 ID。                                                                                     |
| `child_epic_id` | integer        | yes      | 子史诗的全局 ID。内部 ID 不能被使用，因为它们会与其他群组的史诗冲突。 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/epics/5/epics/6"
```

响应示例：

```json
{
  "id": 6,
  "iid": 38,
  "group_id": 1,
  "parent_id": 5,
  "title": "Accusamus iste et ullam ratione voluptatem omnis debitis dolor est.",
  "description": "Molestias dolorem eos vitae expedita impedit necessitatibus quo voluptatum.",
  "author": {
    "id": 10,
    "name": "Lu Mayer",
    "username": "kam",
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/018729e129a6f31c80a6327a30196823?s=80&d=identicon",
    "web_url": "http://localhost:3001/kam"
  },
  "start_date": null,
  "start_date_is_fixed": false,
  "start_date_fixed": null,
  "start_date_from_milestones": null,       //deprecated in favor of start_date_from_inherited_source
  "start_date_from_inherited_source": null,
  "end_date": "2018-07-31",                 //deprecated in favor of due_date
  "due_date": "2018-07-31",
  "due_date_is_fixed": false,
  "due_date_fixed": null,
  "due_date_from_milestones": "2018-07-31", //deprecated in favor of start_date_from_inherited_source
  "due_date_from_inherited_source": "2018-07-31",
  "created_at": "2018-07-17T13:36:22.770Z",
  "updated_at": "2018-07-18T12:22:05.239Z",
  "labels": []
}
```

## 创建并指定一个子史诗

创建一个新的史诗，并将其与提供的父史诗关联起来。返回是已关联史诗对象。

```plaintext
POST /groups/:id/epics/:epic_iid/epics
```

| 参数       | 类型             | 是否必需 | 描述                                                       |
| --------------- | -------------- | -------- | ------------------------------------------------------------------------------------------------------------------ |
| `id`            | integer/string | yes      | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。      |
| `epic_iid`      | integer        | yes      | (未来的父)史诗的内部 ID。                                                                      |
| `title`         | string         | yes      | 一个新创建的史诗的标题。                                                                                 |
| `confidential`  | boolean        | no       | 史诗是否应该是保密的。如果 `confidential_epics` 功能标志被禁用，则该参数被忽略。默认为父史诗的保密状态。 |

```shell
curl --request POST --header  "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/epics/5/epics?title=Newpic"
```

响应示例：

```json
{
  "id": 24,
  "iid": 2,
  "title": "child epic",
  "group_id": 49,
  "parent_id": 23,
  "has_children": false,
  "has_issues": false,
  "reference":  "&2",
  "url": "http://localhost/groups/group16/-/epics/2",
  "relation_url": "http://localhost/groups/group16/-/epics/1/links/24"
}
```

## 重新排序一个子史诗

```plaintext
PUT /groups/:id/epics/:epic_iid/epics/:child_epic_id
```

| 参数       | 类型             | 是否必需 | 描述                                                       |
| ---------------- | -------------- | -------- | ------------------------------------------------------------------------------------------------------------------ |
| `id`             | integer/string | yes      | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。    |
| `epic_iid`       | integer        | yes      | 史诗的内部 ID。                                                                                   |
| `child_epic_id`  | integer        | yes      | 子史诗的全局 ID。内部 ID 不能被使用，因为它们会与其他群组的史诗冲突。 |
| `move_before_id` | integer        | no       | 应该放在子史诗之前的同级史诗的全局 ID。                                      |
| `move_after_id`  | integer        | no       | 应该放在子史诗之后的同级史诗的全局 ID。                                     |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/epics/4/epics/5"
```

响应示例：

```json
[
  {
    "id": 29,
    "iid": 6,
    "group_id": 1,
    "parent_id": 5,
    "title": "Accusamus iste et ullam ratione voluptatem omnis debitis dolor est.",
    "description": "Molestias dolorem eos vitae expedita impedit necessitatibus quo voluptatum.",
    "author": {
      "id": 10,
      "name": "Lu Mayer",
      "username": "kam",
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/018729e129a6f31c80a6327a30196823?s=80&d=identicon",
      "web_url": "http://localhost:3001/kam"
    },
    "start_date": null,
    "start_date_is_fixed": false,
    "start_date_fixed": null,
    "start_date_from_milestones": null,       //deprecated in favor of start_date_from_inherited_source
    "start_date_from_inherited_source": null,
    "end_date": "2018-07-31",                 //deprecated in favor of due_date
    "due_date": "2018-07-31",
    "due_date_is_fixed": false,
    "due_date_fixed": null,
    "due_date_from_milestones": "2018-07-31", //deprecated in favor of start_date_from_inherited_source
    "due_date_from_inherited_source": "2018-07-31",
    "created_at": "2018-07-17T13:36:22.770Z",
    "updated_at": "2018-07-18T12:22:05.239Z",
    "labels": []
  }
]
```

## 解除对子史诗的指派

从父史诗中取消对子史诗的指派。

```plaintext
DELETE /groups/:id/epics/:epic_iid/epics/:child_epic_id
```

| 参数       | 类型             | 是否必需 | 描述                                                       |
| --------------- | -------------- | -------- | ------------------------------------------------------------------------------------------------------------------ |
| `id`            | integer/string | yes      | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。    |
| `epic_iid`      | integer        | yes      | 史诗的内部 ID。                                                                                      |
| `child_epic_id` | integer        | yes      | 子史诗的全局 ID。内部 ID 不能被使用，因为它们会与其他群组的史诗冲突。 |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/epics/4/epics/5"
```

响应示例：

```json
{
  "id": 5,
  "iid": 38,
  "group_id": 1,
  "parent_id": null,
  "title": "Accusamus iste et ullam ratione voluptatem omnis debitis dolor est.",
  "description": "Molestias dolorem eos vitae expedita impedit necessitatibus quo voluptatum.",
  "author": {
    "id": 10,
    "name": "Lu Mayer",
    "username": "kam",
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/018729e129a6f31c80a6327a30196823?s=80&d=identicon",
    "web_url": "http://localhost:3001/kam"
  },
  "start_date": null,
  "start_date_is_fixed": false,
  "start_date_fixed": null,
  "start_date_from_milestones": null,       //deprecated in favor of start_date_from_inherited_source
  "start_date_from_inherited_source": null,
  "end_date": "2018-07-31",                 //deprecated in favor of due_date
  "due_date": "2018-07-31",
  "due_date_is_fixed": false,
  "due_date_fixed": null,
  "due_date_from_milestones": "2018-07-31", //deprecated in favor of start_date_from_inherited_source
  "due_date_from_inherited_source": "2018-07-31",
  "created_at": "2018-07-17T13:36:22.770Z",
  "updated_at": "2018-07-18T12:22:05.239Z",
  "labels": []
}
```
