---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

# Dockerfiles API **(FREE)**

极狐GitLab 为实例级 Dockerfile 模板提供了一个 API 端点。
默认模板定义在 [`vendor/Dockerfile`](https://jihulab.com/gitlab-cn/gitlab-foss/-/tree/master/vendor/Dockerfile)。

## 覆盖 Dockerfile API 模板 **(PREMIUM SELF)**

在[极狐GitLab 专业版及旗舰版](https://about.gitlab.com/pricing/)中，实例的管理员可以在[管理页面](../../user/admin_area/settings/instance_template_repository.md)覆盖模版。

## Dockerfile 模版列表

获取所有 Dockerfile 模板。

```plaintext
GET /templates/dockerfiles
```

```shell
curl "https://gitlab.example.com/api/v4/templates/dockerfiles"
```

响应示例：

```json
[
  {
    "key": "Binary",
    "name": "Binary"
  },
  {
    "key": "Binary-alpine",
    "name": "Binary-alpine"
  },
  {
    "key": "Binary-scratch",
    "name": "Binary-scratch"
  },
  {
    "key": "Golang",
    "name": "Golang"
  },
  {
    "key": "Golang-alpine",
    "name": "Golang-alpine"
  },
  {
    "key": "Golang-scratch",
    "name": "Golang-scratch"
  },
  {
    "key": "HTTPd",
    "name": "HTTPd"
  },
  {
    "key": "Node",
    "name": "Node"
  },
  {
    "key": "Node-alpine",
    "name": "Node-alpine"
  },
  {
    "key": "OpenJDK",
    "name": "OpenJDK"
  },
  {
    "key": "PHP",
    "name": "PHP"
  },
  {
    "key": "Python",
    "name": "Python"
  },
  {
    "key": "Python-alpine",
    "name": "Python-alpine"
  },
  {
    "key": "Python2",
    "name": "Python2"
  },
  {
    "key": "Ruby",
    "name": "Ruby"
  },
  {
    "key": "Ruby-alpine",
    "name": "Ruby-alpine"
  },
  {
    "key": "Rust",
    "name": "Rust"
  },
  {
    "key": "Swift",
    "name": "Swift"
  }
]
```

## 单一 Dockerfile 模板

获取一个 Dockerfile 模板。

```plaintext
GET /templates/dockerfiles/:key
```

| 参数 | 类型 | 是否必需 | 描述 |
| ---------- | ------ | -------- | ----------- |
| `key`      | string | yes      | Dockerfile 模板的密钥 |

```shell
curl "https://gitlab.example.com/api/v4/templates/dockerfiles/Binary"
```

响应示例：

```json
{
  "name": "Binary",
  "content": "# This file is a template, and might need editing before it works on your project.\n# This Dockerfile installs a compiled binary into a bare system.\n# You must either commit your compiled binary into source control (not recommended)\n# or build the binary first as part of a CI/CD pipeline.\n\nFROM buildpack-deps:buster\n\nWORKDIR /usr/local/bin\n\n# Change `app` to whatever your binary is called\nAdd app .\nCMD [\"./app\"]\n"
}
```

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading,for example `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
