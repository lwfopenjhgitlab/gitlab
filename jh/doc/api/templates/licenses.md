---
stage: Secure
group: Composition Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

# 许可证 API **(FREE)**

在极狐GitLab 中，有一个可用的 API 端点来处理各种开放的源码许可证模板。有关许可证各种条款的更多信息，请参阅[该网站](https://choosealicense.com/)或者其他任何一个网上的资源。

## 许可证模板列表

获取所有许可证模板。

```plaintext
GET /templates/licenses
```

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ------- | -------- | --------------------- |
| `popular` | boolean | no       | 如果通过，则只返回流行的许可证 |

```shell
curl "https://gitlab.example.com/api/v4/templates/licenses?popular=1"
```

响应示例：

```json
[
    {
        "key": "apache-2.0",
        "name": "Apache License 2.0",
        "nickname": null,
        "featured": true,
        "html_url": "http://choosealicense.com/licenses/apache-2.0/",
        "source_url": "http://www.apache.org/licenses/LICENSE-2.0.html",
        "description": "A permissive license that also provides an express grant of patent rights from contributors to users.",
        "conditions": [
            "include-copyright",
            "document-changes"
        ],
        "permissions": [
            "commercial-use",
            "modifications",
            "distribution",
            "patent-use",
            "private-use"
        ],
        "limitations": [
            "trademark-use",
            "no-liability"
        ],
        "content": "                                 Apache License\n                           Version 2.0, January 2004\n [...]"
    },
    {
        "key": "gpl-3.0",
        "name": "GNU General Public License v3.0",
        "nickname": "GNU GPLv3",
        "featured": true,
        "html_url": "http://choosealicense.com/licenses/gpl-3.0/",
        "source_url": "http://www.gnu.org/licenses/gpl-3.0.txt",
        "description": "The GNU GPL is the most widely used free software license and has a strong copyleft requirement. When distributing derived works, the source code of the work must be made available under the same license.",
        "conditions": [
            "include-copyright",
            "document-changes",
            "disclose-source",
            "same-license"
        ],
        "permissions": [
            "commercial-use",
            "modifications",
            "distribution",
            "patent-use",
            "private-use"
        ],
        "limitations": [
            "no-liability"
        ],
        "content": "                    GNU GENERAL PUBLIC LICENSE\n                       Version 3, 29 June 2007\n [...]"
    },
    {
        "key": "mit",
        "name": "MIT License",
        "nickname": null,
        "featured": true,
        "html_url": "http://choosealicense.com/licenses/mit/",
        "source_url": "http://opensource.org/licenses/MIT",
        "description": "A permissive license that is short and to the point. It lets people do anything with your code with proper attribution and without warranty.",
        "conditions": [
            "include-copyright"
        ],
        "permissions": [
            "commercial-use",
            "modifications",
            "distribution",
            "private-use"
        ],
        "limitations": [
            "no-liability"
        ],
        "content": "The MIT License (MIT)\n\nCopyright (c) [year] [fullname]\n [...]"
    }
]
```

## 单一许可证模板

获取一个许可证模板，通过传递参数替换许可证占位符。

```plaintext
GET /templates/licenses/:key
```

| 参数 | 类型 | 是否必需 | 描述 |
| ---------- | ------ | -------- | ----------- |
| `key`      | string | yes      | 许可证模板密钥 |
| `project`  | string | no       | 受版权保护的项目名称 |
| `fullname` | string | no       | 版权所有者的全名 |

NOTE: 
如果您省略 `fullname` 参数，但验证您的请求，则经过身份验证的用户替换版权所有者占位符。

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/templates/licenses/mit?project=My+Cool+Project"
```

响应示例：

```json
{
    "key": "mit",
    "name": "MIT License",
    "nickname": null,
    "featured": true,
    "html_url": "http://choosealicense.com/licenses/mit/",
    "source_url": "http://opensource.org/licenses/MIT",
    "description": "A permissive license that is short and to the point. It lets people do anything with your code with proper attribution and without warranty.",
    "conditions": [
        "include-copyright"
    ],
    "permissions": [
        "commercial-use",
        "modifications",
        "distribution",
        "private-use"
    ],
    "limitations": [
        "no-liability"
    ],
    "content": "The MIT License (MIT)\n\nCopyright (c) 2016 John Doe\n [...]"
}
```

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, for example `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
