---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
type: reference, api
---

# 更改建议 API **(FREE)**

本页面介绍了[更改建议](../user/project/merge_requests/reviews/suggestions.md) API。

每一个 API 调用都必须进行身份验证。

## 创建建议

要通过 API 创建建议，请使用讨论 API [在合并请求差异中创建新主题](discussions.md#create-new-merge-request-thread)。
建议的格式为：

````markdown
```suggestion:-3+0
example text
```
````

## 应用建议

在合并请求中，应用一个更改建议。用户角色不能低于开发者角色。

```plaintext
PUT /suggestions/:id/apply
```

| 参数 | 类型 | 是否必需 | 描述      |
| --------- | ---- | -------- | ----------- |
| `id` | integer | yes | 更改建议的 ID |
| `commit_message` | string | no | 自定义的提交说明以代替默认生成的或项目默认的说明 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/suggestions/5/apply"
```

响应示例：

```json
{
  "id": 5,
  "from_line": 10,
  "to_line": 10,
  "applicable": true,
  "applied": false,
  "from_content": "This is an eaxmple\n",
  "to_content": "This is an example\n"
}
```

## 应用多个建议

```plaintext
PUT /suggestions/batch_apply
```

| 参数               | 类型      | 是否必需 | 描述                                                                                                       |
|------------------|---------|------|----------------------------------------------------------------------------------------------------------|
| `ids`            | integer | yes  | 建议 ID                                                                                    |
| `commit_message` | string  | no   | 使用自定义提交消息代替默认生成的消息或项目的默认消息 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" --header 'Content-Type: application/json' --data '{"ids": [5, 6]}' "https://gitlab.example.com/api/v4/suggestions/batch_apply"
```

响应示例：

```json
[
  {
    "id": 5,
    "from_line": 10,
    "to_line": 10,
    "applicable": true,
    "applied": false,
    "from_content": "This is an eaxmple\n",
    "to_content": "This is an example\n"
  }
  {
    "id": 6,
    "from_line": 19
    "to_line": 19,
    "applicable": true,
    "applied": false,
    "from_content": "This is another eaxmple\n",
    "to_content": "This is another example\n"
  }
 ]
```
