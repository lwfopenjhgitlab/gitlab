---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目迭代 API **(PREMIUM)**

> - 引入于 13.5 版本。
> - 在 13.9 版本中移至专业版。

本页描述项目迭代 API。
<!--[群组迭代 API](group_iterations.md) 有单独的页面。-->

从 13.5 版本开始，我们没有项目级迭代，但您可以使用此端点获取项目上级群组的迭代。

## 列出项目迭代

返回项目迭代的列表。

```plaintext
GET /projects/:id/iterations
GET /projects/:id/iterations?state=opened
GET /projects/:id/iterations?state=closed
GET /projects/:id/iterations?search=version
GET /projects/:id/iterations?include_ancestors=false
```

| 参数           | 类型    | 是否必需 | 描述 |
| ------------------- | ------- | -------- | ----------- |
| `state`             | string  | 否       | 返回 `opened`、`upcoming`、`current (previously started)`、`closed` 或 `all` 迭代。从 14.1 版本开始不推荐使用 `started` 状态过滤，请改用 `current`。 |
| `search`            | string  | 否       | 仅返回标题与提供的字符串匹配的迭代。                 |
| `include_ancestors` | boolean | 否       | 包括来自父组及其祖先的迭代。默认为 `true`。                 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/iterations"
```

响应示例：

```json
[
  {
    "id": 53,
    "iid": 13,
    "group_id": 5,
    "title": "Iteration II",
    "description": "Ipsum Lorem ipsum",
    "state": 2,
    "created_at": "2020-01-27T05:07:12.573Z",
    "updated_at": "2020-01-27T05:07:12.573Z",
    "due_date": "2020-02-01",
    "start_date": "2020-02-14",
    "web_url": "http://gitlab.example.com/groups/my-group/-/iterations/13"
  }
]
```
