---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 通知设置 API **(FREE)**

使用 REST API 更改[通知设置](../user/profile/notifications.md)。

## 有效通知级别

`NotificationSetting.level` 模型枚举中定义通知级别。当前级别有：

- `disabled`
- `participating`
- `watch`
- `global`
- `mention`
- `custom`

如果使用 `custom` 级别，可以控制特定的电子邮件事件。可用事件由 `NotificationSetting.email_events` 返回。当前事件为：

- `new_note`
- `new_issue`
- `reopen_issue`
- `close_issue`
- `reassign_issue`
- `issue_due`
- `new_merge_request`
- `push_to_merge_request`
- `reopen_merge_request`
- `close_merge_request`
- `reassign_merge_request`
- `merge_merge_request`
- `failed_pipeline`
- `fixed_pipeline`
- `success_pipeline`
- `moved_project`
- `merge_when_pipeline_succeeds`
- `new_epic` **(ULTIMATE)**

## 全局通知设置

获取当前通知设置和电子邮件地址。

```plaintext
GET /notification_settings
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/notification_settings"
```

响应示例：

```json
{
  "level": "participating",
  "notification_email": "admin@example.com"
}
```

## 更新全局通知设置

更新当前通知设置和电子邮件地址。

```plaintext
PUT /notification_settings
```

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/notification_settings?level=watch"
```

| 参数                             | 类型      | 是否必需 | 描述                               |
|--------------------------------|---------|------|----------------------------------|
| `level`                        | string  | no   | 全局通知级别                           |
| `notification_email`           | string  | no   | 发送通知的电子邮件地址                      |
| `new_note`                     | boolean | no   | 启用/禁用通知                          |
| `new_issue`                    | boolean | no   | 启用/禁用通知                          |
| `reopen_issue`                 | boolean | no   | 启用/禁用通知                          |
| `close_issue`                  | boolean | no   | 启用/禁用通知                          |
| `reassign_issue`               | boolean | no   | 启用/禁用通知                          |
| `issue_due`                    | boolean | no   | 启用/禁用通知                          |
| `new_merge_request`            | boolean | no   | 启用/禁用通知                          |
| `push_to_merge_request`        | boolean | no   | 启用/禁用通知                          |
| `reopen_merge_request`         | boolean | no   | 启用/禁用通知                          |
| `close_merge_request`          | boolean | no   | 启用/禁用通知                          |
| `reassign_merge_request`       | boolean | no   | 启用/禁用通知                          |
| `merge_merge_request`          | boolean | no   | 启用/禁用通知                          |
| `failed_pipeline`              | boolean | no   | 启用/禁用通知                          |
| `fixed_pipeline`               | boolean | no   | 启用/禁用通知                          |
| `success_pipeline`             | boolean | no   | 启用/禁用通知                          |
| `moved_project`                | boolean | no   | 启用/禁用通知（引入于 13.3）                |
| `merge_when_pipeline_succeeds` | boolean | no   | 启用/禁用通知（引入于 13.9）                |
| `new_epic`                     | boolean | no   | 启用/禁用通知（引入于 11.3） **(ULTIMATE)** |

响应示例：

```json
{
  "level": "watch",
  "notification_email": "admin@example.com"
}
```

## 群组/项目级别通知设置

获取当前群组或项目通知设置。

```plaintext
GET /groups/:id/notification_settings
GET /projects/:id/notification_settings
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/notification_settings"
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/8/notification_settings"
```

| 参数   | 类型                | 是否必需 | 描述                                                            |
|------|-------------------|------|---------------------------------------------------------------|
| `id` | integer or string | yes  | ID 或 [URL 编码的群组或项目路径](rest/index.md#namespaced-path-encoding) |

响应示例：

```json
{
  "level": "global"
}
```

## 更新群组/项目级别通知设置

更新当前群组/项目通知设置。

```plaintext
PUT /groups/:id/notification_settings
PUT /projects/:id/notification_settings
```

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/notification_settings?level=watch"
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/8/notification_settings?level=custom&new_note=true"
```

| 参数                             | 类型      | 是否必需 | 描述                                                            |
|--------------------------------|---------|------|---------------------------------------------------------------|
| `id`                         | integer or string  | yes  | ID 或 [URL 编码的群组/项目路径](rest/index.md#namespaced-path-encoding) |
| `level`          | string  | no   | 全局通知级别                                                        |
| `new_note`                     | boolean | no   | 启用/禁用通知                                                       |
| `new_issue`                    | boolean | no   | 启用/禁用通知                                                       |
| `reopen_issue`                 | boolean | no   | 启用/禁用通知                                                       |
| `close_issue`                  | boolean | no   | 启用/禁用通知                                                       |
| `reassign_issue`               | boolean | no   | 启用/禁用通知                                                       |
| `issue_due`                    | boolean | no   | 启用/禁用通知                                                       |
| `new_merge_request`            | boolean | no   | 启用/禁用通知                                                       |
| `push_to_merge_request`        | boolean | no   | 启用/禁用通知                                                       |
| `reopen_merge_request`         | boolean | no   | 启用/禁用通知                                                       |
| `close_merge_request`          | boolean | no   | 启用/禁用通知                                                       |
| `reassign_merge_request`       | boolean | no   | 启用/禁用通知                                                       |
| `merge_merge_request`          | boolean | no   | 启用/禁用通知                                                       |
| `failed_pipeline`              | boolean | no   | 启用/禁用通知                                                       |
| `fixed_pipeline`               | boolean | no   | 启用/禁用通知                                                       |
| `success_pipeline`             | boolean | no   | 启用/禁用通知                                                       |
| `moved_project`                | boolean | no   | 启用/禁用通知（引入于 13.3）                                             |
| `merge_when_pipeline_succeeds` | boolean | no   | 启用/禁用通知（引入于 13.9）                                             |
| `new_epic`                     | boolean | no   | 启用/禁用通知（引入于 11.3） **(ULTIMATE)**                              |

响应示例：

```json
{
  "level": "watch"
}
```

```json
{
  "level": "custom",
  "events": {
    "new_note": true,
    "new_issue": false,
    "reopen_issue": false,
    "close_issue": false,
    "reassign_issue": false,
    "issue_due": false,
    "new_merge_request": false,
    "push_to_merge_request": false,
    "reopen_merge_request": false,
    "close_merge_request": false,
    "reassign_merge_request": false,
    "merge_merge_request": false,
    "failed_pipeline": false,
    "fixed_pipeline": false,
    "success_pipeline": false
  }
}
```

[极狐GitLab 旗舰版](https://about.gitlab.cn/pricing/)的用户也可以看见 `new_epic` 参数：

```json
{
  "level": "custom",
  "events": {
    "new_note": true,
    "new_issue": false,
    "new_epic": false,
    ...
  }
}
```
