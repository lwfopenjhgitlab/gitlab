---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 发布链接 API **(FREE)**

> 对[极狐GitLab CI/CD 作业令牌认证](../../ci/jobs/ci_job_token.md)的支持引入于 15.1 版本。

本 API 用来操作极狐GitLab [发布](../../user/project/releases/index.md) 的链接。<!--，如需其他有关发布资源的操作，请查看 [发布 API](index.md)-->

极狐GitLab 支持 `http`、`https` 以及 `ftp` 资源的链接。

## 列出发布链接

<!--- start_remove The following content will be removed on remove_date: '2023-08-22' -->

> Release Links 中的 `external` 字段废弃于 15.9 且移除于 16.0。

<!--- end_remove -->

从发布中获取资产链接。

```plaintext
GET /projects/:id/releases/:tag_name/assets/links
```

| 参数     | 类型           | 是否必需 | 描述                                                       |
| ------------- | -------------- | -------- |----------------------------------------------------------|
| `id`          | integer/string | yes      | ID 或者 [项目 URL 路径](../rest/index.md#namespaced-path-encoding)。 |
| `tag_name`    | string         | yes      | 发布关联的标签。                                                 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/24/releases/v0.1/assets/links"
```

响应示例：

```json
[
  {
    "id":2,
    "name":"awesome-v0.2.msi",
    "url":"http://192.168.10.15:3000/msi",
    "link_type":"other"
  },
  {
    "id":1,
    "name":"awesome-v0.2.dmg",
    "url":"http://192.168.10.15:3000",
    "link_type":"other"
  }
]
```

## 获取发布链接

<!--- start_remove The following content will be removed on remove_date: '2023-08-22' -->

> Release Links 中的 `external` 字段废弃于 15.9 且移除于 16.0。

<!--- end_remove -->

从发布中获取资产链接。

```plaintext
GET /projects/:id/releases/:tag_name/assets/links/:link_id
```

| 参数     | 类型           | 是否必需 | 描述                                                       |
| ------------- | -------------- | -------- |----------------------------------------------------------|
| `id`          | integer/string | yes      | ID 或者 [项目 URL 路径](../rest/index.md#namespaced-path-encoding)。 |
| `tag_name`    | string         | yes      | 发布关联的标签。                                                 |
| `link_id`    | integer         | yes      | 链接的 ID。                                                  |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/24/releases/v0.1/assets/links/1"
```

响应示例：

```json
{
  "id":1,
  "name":"awesome-v0.2.dmg",
  "url":"http://192.168.10.15:3000",
  "link_type":"other"
}
```

<a id="create-a-release-link"></a>

## 创建发布链接

<!--- start_remove The following content will be removed on remove_date: '2023-08-22' -->

> Release Links 中的 `external` 字段废弃于 15.9 且移除于 16.0。

<!--- end_remove -->

为发布创建一个链接。

```plaintext
POST /projects/:id/releases/:tag_name/assets/links
```

| 参数     | 类型             | 是否必需 | 描述                                                                                                       |
| ------------- |----------------|------|----------------------------------------------------------------------------------------------------------|
| `id`          | integer/string | yes  | ID 或者[项目 URL 路径](../rest/index.md#namespaced-path-encoding)。                                             |
| `tag_name`    | string         | yes  | 发布关联的标签。                                                                                                 |
| `name`        | string         | yes  | 链接的名称。在发布中必须唯一。                                                                                          |
| `url`         | string         | yes  | 链接的 URL。在发布中必须唯一。                                                                                        |
| `filepath`    | string         | no   | 已废弃：使用 `direct_asset_path` 代替。                                                                           |
| `direct_asset_path` | string   | no   | [Direct Asset 链接](../../user/project/releases/release_fields.md#permanent-links-to-release-assets)的可选路径。 |
| `link_type`   | string         | no   | 链接类型：`other`、`runbook`、`image`、`package`，默认为 `other`。                                                    |

请求示例：

```shell
curl --request POST \
    --header "PRIVATE-TOKEN: <your_access_token>" \
    --data name="hellodarwin-amd64" \
    --data url="https://gitlab.example.com/mynamespace/hello/-/jobs/688/artifacts/raw/bin/hello-darwin-amd64" \
    --data direct_asset_path="/bin/hellodarwin-amd64" \
    "https://gitlab.example.com/api/v4/projects/20/releases/v1.7.0/assets/links"
```

响应示例：

```json
{
  "id":2,
  "name":"hellodarwin-amd64",
  "url":"https://gitlab.example.com/mynamespace/hello/-/jobs/688/artifacts/raw/bin/hello-darwin-amd64",
  "direct_asset_url":"https://gitlab.example.com/mynamespace/hello/-/releases/v1.7.0/downloads/bin/hellodarwin-amd64",
  "external":false, // deprecated in GitLab 15.9, will be removed in GitLab 16.0.
  "link_type":"other"
}
```

<a id="update-a-release-link"></a>

## 更新发布链接

<!--- start_remove The following content will be removed on remove_date: '2023-08-22' -->

> Release Links 中的 `external` 字段废弃于 15.9 且移除于 16.0。

<!--- end_remove -->

更新发布的链接。

```plaintext
PUT /projects/:id/releases/:tag_name/assets/links/:link_id
```

| 参数     | 类型             | 是否必需 | 描述                                                                                                         |
| ------------- |----------------|------|------------------------------------------------------------------------------------------------------------|
| `id`          | integer/string | yes  | ID 或者[项目 URL 路径](../rest/index.md#namespaced-path-encoding)。                                                    |
| `tag_name`    | string         | yes  | 发布关联的标签。                                                                                                   |
| `link_id`     | integer        | yes  | 链接 ID。                                                                                                     |
| `name`        | string         | no   | 链接名称。                                                                                                      |
| `url`         | string         | no   | 链接 URL。                                                                                                    |
| `filepath` | string         | no   | 已废弃，使用 `direct_asset_path` 代替。   
| `direct_asset_path`  | string         | no   | [Direct Asset 链接](../../user/project/releases/release_fields.md#permanent-links-to-release-assets)的可选路径。 |
| `link_type`        | string         | no   | 链接类型: `other`、`runbook`、`image`、`package`，默认为 `other`。                                                     |

NOTE:
`name` 和 `url`至少指定一个。

请求示例：

```shell
curl --request PUT --data name="new name" --data link_type="runbook" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/24/releases/v0.1/assets/links/1"
```

响应示例：

```json
{
  "id":1,
  "name":"new name",
  "url":"http://192.168.10.15:3000",
  "link_type":"runbook"
}
```

## 删除发布链接

<!--- start_remove The following content will be removed on remove_date: '2023-08-22' -->

> Release Links 中的 `external` 字段废弃于 15.9 且移除于 16.0。

<!--- end_remove -->

删除发布的链接。

```plaintext
DELETE /projects/:id/releases/:tag_name/assets/links/:link_id
```

| 参数     | 类型           | 是否必需 | 描述                                                     |
| ------------- | -------------- | -------- |--------------------------------------------------------|
| `id`          | integer/string | yes      | ID 或者[项目 URL 路径](../rest/index.md#namespaced-path-encoding)。 |
| `tag_name`    | string         | yes      | 发布关联的标签。                                               |
| `link_id`    | integer         | yes      | 链接 ID。                                                 |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/24/releases/v0.1/assets/links/1"
```

响应示例：

```json
{
  "id":1,
  "name":"new name",
  "url":"http://192.168.10.15:3000",
  "link_type":"other"
}
```
