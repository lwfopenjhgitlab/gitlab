---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 发布 API **(FREE)**

> - Release Evidence 引入于极狐GitLab 12.5。
> - 由于性能原因，`description_html` 在极狐GitLab 13.12 中为可选字段。
    Please pass the `include_html_description` query string parameter if you need it.
> - 创建、更新和删除操作的权限模型修复于极狐GitLab 14.1。
    更多内容请参见[发布权限](../../user/project/releases/index.md#release-permissions)。

使用此 API 来操作极狐GitLab [发布](../../user/project/releases/index.md)条目。如需将链接作为 assets 进行操作，请参阅[发布链接 API](links.md)。

## 验证

对于身份验证，Release API 接受：

- 使用 `PRIVATE-TOKEN` header 的[个人访问令牌](../../user/profile/personal_access_tokens.md)。
- [极狐GitLab CI/CD 作业令牌](../../ci/jobs/ci_job_token.md) `$CI_JOB_TOKEN` 使用 `JOB-TOKEN` header。

## 列出发布

> - 更改为允许 `JOB-TOKEN` 于 14.5 版本。
<!--- start_remove The following content will be removed on remove_date: '2023-08-22' -->
> - Release Links 中的 `external` 字段废弃于 15.9 并移除于 16.0。
<!--- end_remove -->

返回发布的分页列表，按 `released_at` 排序。

```plaintext
GET /projects/:id/releases
```

| 参数     | 类型           | 是否必需 | 描述                             |
| ------------- | -------------- | -------- | ----------------------------------------------------------------------------------- |
| `id`          | integer/string | yes      | ID 或者[项目 URL 路径](../rest/index.md#namespaced-path-encoding)。 |
| `order_by`    | string         | no       | 用作排序方法的字段。`released_at`（默认）或 `created_at`。 |
| `sort`        | string         | no       | 排序的方向。`desc` （默认）用于降序或 `asc` 用于升序。 |
| `include_html_description` | boolean        | no       | 如果为 `true`，则响应包括 HTML 渲染的 Markdown 格式的发布描述。   |

如果成功，返回 [`200 OK`](../../api/rest/index.md#status-codes) 及以下响应参数：

| 参数                                    | 类型     | 描述                           |
|:--------------------------------------|:-------|:-----------------------------|
| `[]._links`                           | object | 发布链接                         |
| `[]._links.closed_issues_url`         | string | 发布的关闭议题的 HTTP URL            |
| `[]._links.closed_merge_requests_url` | string | 发布的关闭合并请求的 HTTP URL          |
| `[]._links.edit_url`                  | string | 发布的编辑页面的 HTTP URL            |
| `[]._links.merged_merge_requests_url` | string | 发布的已合并合并请求的 HTTP URL         |
| `[]._links.opened_issues_url`         | string | 发布的开放议题的 HTTP URL            |
| `[]._links.opened_merge_requests_url` | string | 发布的开放合并请求的 HTTP URL          |
| `[]._links.self`                      | string | 发布的 HTTP URL|


请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/24/releases"
```

响应示例：

```json
[
   {
      "tag_name":"v0.2",
      "description":"## CHANGELOG\r\n\r\n- Escape label and milestone titles to prevent XSS in GLFM autocomplete. !2740\r\n- Prevent private snippets from being embeddable.\r\n- Add subresources removal to member destroy service.",
      "name":"Awesome app v0.2 beta",
      "created_at":"2019-01-03T01:56:19.539Z",
      "released_at":"2019-01-03T01:56:19.539Z",
      "author":{
         "id":1,
         "name":"Administrator",
         "username":"root",
         "state":"active",
         "avatar_url":"https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80\u0026d=identicon",
         "web_url":"https://gitlab.example.com/root"
      },
      "commit":{
         "id":"079e90101242458910cccd35eab0e211dfc359c0",
         "short_id":"079e9010",
         "title":"Update README.md",
         "created_at":"2019-01-03T01:55:38.000Z",
         "parent_ids":[
            "f8d3d94cbd347e924aa7b715845e439d00e80ca4"
         ],
         "message":"Update README.md",
         "author_name":"Administrator",
         "author_email":"admin@example.com",
         "authored_date":"2019-01-03T01:55:38.000Z",
         "committer_name":"Administrator",
         "committer_email":"admin@example.com",
         "committed_date":"2019-01-03T01:55:38.000Z"
      },
      "milestones": [
         {
            "id":51,
            "iid":1,
            "project_id":24,
            "title":"v1.0-rc",
            "description":"Voluptate fugiat possimus quis quod aliquam expedita.",
            "state":"closed",
            "created_at":"2019-07-12T19:45:44.256Z",
            "updated_at":"2019-07-12T19:45:44.256Z",
            "due_date":"2019-08-16",
            "start_date":"2019-07-30",
            "web_url":"https://gitlab.example.com/root/awesome-app/-/milestones/1",
            "issue_stats": {
               "total": 98,
               "closed": 76
            }
         },
         {
            "id":52,
            "iid":2,
            "project_id":24,
            "title":"v1.0",
            "description":"Voluptate fugiat possimus quis quod aliquam expedita.",
            "state":"closed",
            "created_at":"2019-07-16T14:00:12.256Z",
            "updated_at":"2019-07-16T14:00:12.256Z",
            "due_date":"2019-08-16",
            "start_date":"2019-07-30",
            "web_url":"https://gitlab.example.com/root/awesome-app/-/milestones/2",
            "issue_stats": {
               "total": 24,
               "closed": 21
            }
         }
      ],
      "commit_path":"/root/awesome-app/commit/588440f66559714280628a4f9799f0c4eb880a4a",
      "tag_path":"/root/awesome-app/-/tags/v0.11.1",
      "assets":{
         "count":6,
         "sources":[
            {
               "format":"zip",
               "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.2/awesome-app-v0.2.zip"
            },
            {
               "format":"tar.gz",
               "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.2/awesome-app-v0.2.tar.gz"
            },
            {
               "format":"tar.bz2",
               "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.2/awesome-app-v0.2.tar.bz2"
            },
            {
               "format":"tar",
               "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.2/awesome-app-v0.2.tar"
            }
         ],
         "links":[
            {
               "id":2,
               "name":"awesome-v0.2.msi",
               "url":"http://192.168.10.15:3000/msi",
               "link_type":"other"
            },
            {
               "id":1,
               "name":"awesome-v0.2.dmg",
               "url":"http://192.168.10.15:3000",
               "link_type":"other"
            }
         ],
         "evidence_file_path":"https://gitlab.example.com/root/awesome-app/-/releases/v0.2/evidence.json"
      },
      "evidences":[
        {
          "sha": "760d6cdfb0879c3ffedec13af470e0f71cf52c6cde4d",
          "filepath": "https://gitlab.example.com/root/awesome-app/-/releases/v0.2/evidence.json",
          "collected_at": "2019-01-03T01:56:19.539Z"
        }
     ]
   },
   {
      "tag_name":"v0.1",
      "description":"## CHANGELOG\r\n\r\n-Remove limit of 100 when searching repository code. !8671\r\n- Show error message when attempting to reopen an MR and there is an open MR for the same branch. !16447 (Akos Gyimesi)\r\n- Fix a bug where internal email pattern wasn't respected. !22516",
      "name":"Awesome app v0.1 alpha",
      "created_at":"2019-01-03T01:55:18.203Z",
      "released_at":"2019-01-03T01:55:18.203Z",
      "author":{
         "id":1,
         "name":"Administrator",
         "username":"root",
         "state":"active",
         "avatar_url":"https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80\u0026d=identicon",
         "web_url":"https://gitlab.example.com/root"
      },
      "commit":{
         "id":"f8d3d94cbd347e924aa7b715845e439d00e80ca4",
         "short_id":"f8d3d94c",
         "title":"Initial commit",
         "created_at":"2019-01-03T01:53:28.000Z",
         "parent_ids":[

         ],
         "message":"Initial commit",
         "author_name":"Administrator",
         "author_email":"admin@example.com",
         "authored_date":"2019-01-03T01:53:28.000Z",
         "committer_name":"Administrator",
         "committer_email":"admin@example.com",
         "committed_date":"2019-01-03T01:53:28.000Z"
      },
      "assets":{
         "count":4,
         "sources":[
            {
               "format":"zip",
               "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.zip"
            },
            {
               "format":"tar.gz",
               "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.tar.gz"
            },
            {
               "format":"tar.bz2",
               "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.tar.bz2"
            },
            {
               "format":"tar",
               "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.tar"
            }
         ],
         "links":[

         ],
         "evidence_file_path":"https://gitlab.example.com/root/awesome-app/-/releases/v0.1/evidence.json"
      },
      "evidences":[
        {
          "sha": "c3ffedec13af470e760d6cdfb08790f71cf52c6cde4d",
          "filepath": "https://gitlab.example.com/root/awesome-app/-/releases/v0.1/evidence.json",
          "collected_at": "2019-01-03T01:55:18.203Z"
        }
      ],
      "_links": {
         "closed_issues_url": "https://gitlab.example.com/root/awesome-app/-/issues?release_tag=v0.1&scope=all&state=closed",
         "closed_merge_requests_url": "https://gitlab.example.com/root/awesome-app/-/merge_requests?release_tag=v0.1&scope=all&state=closed",
         "edit_url": "https://gitlab.example.com/root/awesome-app/-/releases/v0.1/edit",
         "merged_merge_requests_url": "https://gitlab.example.com/root/awesome-app/-/merge_requests?release_tag=v0.1&scope=all&state=merged",
         "opened_issues_url": "https://gitlab.example.com/root/awesome-app/-/issues?release_tag=v0.1&scope=all&state=opened",
         "opened_merge_requests_url": "https://gitlab.example.com/root/awesome-app/-/merge_requests?release_tag=v0.1&scope=all&state=opened",
         "self": "https://gitlab.example.com/root/awesome-app/-/releases/v0.1"
      }
   }
]
```

## 通过标签名称获取发布

> - 更改为允许 `JOB-TOKEN` 于 14.5 版本。
<!--- start_remove The following content will be removed on remove_date: '2023-08-22' -->
> - Release Links 中的 `external` 字段废弃于 15.9 并移除于 16.0。
<!--- end_remove -->

获取特定标签的发布。

```plaintext
GET /projects/:id/releases/:tag_name
```

| 参数     | 类型           | 是否必需 | 描述                             |
|----------------------------| -------------- | -------- | ----------------------------------------------------------------------------------- |
| `id`                       | integer/string | yes      | ID 或者[项目 URL 路径](../rest/index.md#namespaced-path-encoding)。  |
| `tag_name`                 | string         | yes      | 与发布关联的 Git 标签。                                         |
| `include_html_description` | boolean        | no       | 如果为 `true`，则响应包括 HTML 渲染的 Markdown 格式的发布描述。   |

如果成功，返回 [`200 OK`](../../api/rest/index.md#status-codes) 及以下响应参数：

| 参数                                    | 类型 | 描述                           |
|:--------------------------------------|:-------|:-----------------------------|
| `[]._links`                           | object | 发布链接                         |
| `[]._links.closed_issues_url`         | string | 发布的关闭议题的 HTTP URL            |
| `[]._links.closed_merge_requests_url` | string | 发布的关闭合并请求的 HTTP URL          |
| `[]._links.edit_url`                  | string | 发布的编辑页面的 HTTP URL            |
| `[]._links.merged_merge_requests_url` | string | 发布的已合并合并请求的 HTTP URL         |
| `[]._links.opened_issues_url`         | string | 发布的开放议题的 HTTP URL            |
| `[]._links.opened_merge_requests_url` | string | 发布的开放合并请求的 HTTP URL          |
| `[]._links.self`                      | string | 发布的 HTTP URL  |


请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/24/releases/v0.1"
```

响应示例：

```json
{
   "tag_name":"v0.1",
   "description":"## CHANGELOG\r\n\r\n- Remove limit of 100 when searching repository code. !8671\r\n- Show error message when attempting to reopen an MR and there is an open MR for the same branch. !16447 (Akos Gyimesi)\r\n- Fix a bug where internal email pattern wasn't respected. !22516",
   "name":"Awesome app v0.1 alpha",
   "created_at":"2019-01-03T01:55:18.203Z",
   "released_at":"2019-01-03T01:55:18.203Z",
   "author":{
      "id":1,
      "name":"Administrator",
      "username":"root",
      "state":"active",
      "avatar_url":"https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80\u0026d=identicon",
      "web_url":"https://gitlab.example.com/root"
   },
   "commit":{
      "id":"f8d3d94cbd347e924aa7b715845e439d00e80ca4",
      "short_id":"f8d3d94c",
      "title":"Initial commit",
      "created_at":"2019-01-03T01:53:28.000Z",
      "parent_ids":[

      ],
      "message":"Initial commit",
      "author_name":"Administrator",
      "author_email":"admin@example.com",
      "authored_date":"2019-01-03T01:53:28.000Z",
      "committer_name":"Administrator",
      "committer_email":"admin@example.com",
      "committed_date":"2019-01-03T01:53:28.000Z"
   },
   "milestones": [
       {
         "id":51,
         "iid":1,
         "project_id":24,
         "title":"v1.0-rc",
         "description":"Voluptate fugiat possimus quis quod aliquam expedita.",
         "state":"closed",
         "created_at":"2019-07-12T19:45:44.256Z",
         "updated_at":"2019-07-12T19:45:44.256Z",
         "due_date":"2019-08-16",
         "start_date":"2019-07-30",
         "web_url":"https://gitlab.example.com/root/awesome-app/-/milestones/1",
         "issue_stats": {
            "total": 98,
            "closed": 76
         }
       },
       {
         "id":52,
         "iid":2,
         "project_id":24,
         "title":"v1.0",
         "description":"Voluptate fugiat possimus quis quod aliquam expedita.",
         "state":"closed",
         "created_at":"2019-07-16T14:00:12.256Z",
         "updated_at":"2019-07-16T14:00:12.256Z",
         "due_date":"2019-08-16",
         "start_date":"2019-07-30",
         "web_url":"https://gitlab.example.com/root/awesome-app/-/milestones/2",
         "issue_stats": {
            "total": 24,
            "closed": 21
         }
       }
   ],
   "commit_path":"/root/awesome-app/commit/588440f66559714280628a4f9799f0c4eb880a4a",
   "tag_path":"/root/awesome-app/-/tags/v0.11.1",
   "assets":{
      "count":5,
      "sources":[
         {
            "format":"zip",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.zip"
         },
         {
            "format":"tar.gz",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.tar.gz"
         },
         {
            "format":"tar.bz2",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.tar.bz2"
         },
         {
            "format":"tar",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.tar"
         }
      ],
      "links":[
         {
            "id":3,
            "name":"hoge",
            "url":"https://gitlab.example.com/root/awesome-app/-/tags/v0.11.1/binaries/linux-amd64",
            "link_type":"other"
         }
      ]
   },
   "evidences":[
     {
       "sha": "760d6cdfb0879c3ffedec13af470e0f71cf52c6cde4d",
       "filepath": "https://gitlab.example.com/root/awesome-app/-/releases/v0.1/evidence.json",
       "collected_at": "2019-07-16T14:00:12.256Z"
     },
   "_links": {
      "closed_issues_url": "https://gitlab.example.com/root/awesome-app/-/issues?release_tag=v0.1&scope=all&state=closed",
      "closed_merge_requests_url": "https://gitlab.example.com/root/awesome-app/-/merge_requests?release_tag=v0.1&scope=all&state=closed",
      "edit_url": "https://gitlab.example.com/root/awesome-app/-/releases/v0.1/edit",
      "merged_merge_requests_url": "https://gitlab.example.com/root/awesome-app/-/merge_requests?release_tag=v0.1&scope=all&state=merged",
      "opened_issues_url": "https://gitlab.example.com/root/awesome-app/-/issues?release_tag=v0.1&scope=all&state=opened",
      "opened_merge_requests_url": "https://gitlab.example.com/root/awesome-app/-/merge_requests?release_tag=v0.1&scope=all&state=opened",
      "self": "https://gitlab.example.com/root/awesome-app/-/releases/v0.1"
    }
  ]
}
```

<a id="download-a-release-asset"></a>

## 下载发布资产

> 引入于极狐GitLab 15.4。

通过使用以下格式发出请求来下载发布资产文件：

```plaintext
GET /projects/:id/releases/:tag_name/downloads/:direct_asset_path
```

| 参数                  | 类型             | 是否必需 | 描述                                                                                                                                                  |
|---------------------|----------------|------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | yes  | ID 或 [URL 编码的项目路径](../rest/index.md#namespaced-path-encoding)                                                                                       |
| `tag_name`          | string         | yes  | 发布关联的 Git 标签                                                                                                                                        |
| `filepath`          | string         | yes  | 已废弃：使用 `direct_asset_path` 代替                                                                                                                       |
| `direct_asset_path` | string         | yes  | [创建](links.md#create-a-release-link)或[更新](links.md#update-a-release-link)链接时指定的发布资产文件的路径 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/24/releases/v0.1/downloads/bin/asset.exe"
```

### 获取最新发布

> 引入于极狐GitLab 15.4。

最新版本信息可通过永久 API URL 访问。

URL 的格式为：

```plaintext
GET /projects/:id/releases/permalink/latest
```

要调用需要发布标签的任何其他 GET API，请将后缀附加到 `permalink/latest` API 路径。

例如，要获取最新的 [Release Evidence](#collect-release-evidence)，您可以使用：

```plaintext
GET /projects/:id/releases/permalink/latest/evidence
```

要对最新的发布[下载资产](#download-a-release-asset)，您可以使用：

```plaintext
GET /projects/:id/releases/permalink/latest/downloads/bin/asset.exe
```

#### 排序偏好设置

默认情况下，极狐GitLab 使用 `released_at` 时间获取版本。查询参数 `?order_by=released_at` 的使用是可选的。<!--and support for `?order_by=semver` is tracked [in issue 352945](https://gitlab.com/gitlab-org/gitlab/-/issues/352945).)。-->

## 创建发布

<!--- start_remove The following content will be removed on remove_date: '2023-08-22' -->

> Release Links 中的 `external` 字段废弃于 15.9 并移除于 16.0。

<!--- end_remove -->

创建一个发布。创建发布需要开发人员级别的项目访问权限。

```plaintext
POST /projects/:id/releases
```

| 参数     | 类型              | 是否必需                                                                                                     | 描述                                                                                              |
| -------------------|-----------------|----------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------|
| `id`               | integer/string  | yes                                                                                                      | ID 或者[项目 URL 路径](../rest/index.md#namespaced-path-encoding)。                                    |
| `name`             | string          | no                                                                                                       | 发布名称。                                                                                           |
| `tag_name`         | string          | yes                                                                                                      | 创建发布的标签。                                                                                        |
| `tag_message`      | string          | no                                                                                                       | 创建新的带有注释的标签时使用的消息。                                                                              |
| `description`      | string          | no                                                                                                       | 发布的描述。您可以使用 [Markdown](../../user/markdown.md)。                                                 |
| `ref`              | string          | 如果 `tag_name` 不存在，是必需的。如果 `tag_name` 中指定的标签不存在，则从 `ref` 创建发布并使用 `tag_name` 进行标记。它可以是提交 SHA、另一个标签名称或分支名称。 |
| `milestones`       | array of string | no                                                                                                       | 与发布相关联的每个里程碑的标题。专业版客户可以指定群组里程碑。                                                                 |
| `assets:links`     | array of hash   | no                                                                                                       | 一组 assets 链接。                                                                                   |
| `assets:links:name`| string          | 需要 `assets:links`                                                                                        | 链接的名称。链接名称在发布中必须是唯一的。                                                                           |
| `assets:links:url` | string          | 需要 `assets:links`                                                                                        | 链接的 URL。链接 URL 在发布中必须是唯一的。                                                                      |
| `assets:links:filepath` | string          | no                                                                                                       | 已废弃：使用 `direct_asset_path` 代替。                                                                  
| `assets:links:direct_asset_path`  | string  | no                                                                                                       | [直接资产链接](../../user/project/releases/release_fields.md#permanent-links-to-release-assets)的可选路径。 |
| `assets:links:link_type` | string          | no                                                                                                       | 链接的类型：`other`、`runbook`、`image`、`package`。默认为 `other`。                                          
| `released_at`      | datetime        | no                                                                                                       | 发布日期和时间。默认为当前时间。采用 ISO 8601 格式 (`2019-03-15T08:00:00Z`)。仅在创建即将到来的发布，或历史发布时提供此字段。                |

请求示例：

```shell
curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: <your_access_token>" \
     --data '{ "name": "New release", "tag_name": "v0.3", "description": "Super nice release", "milestones": ["v1.0", "v1.0-rc"], "assets": { "links": [{ "name": "hoge", "url": "https://google.com", "direct_asset_path": "/binaries/linux-amd64", "link_type":"other" }] } }' \
     --request POST "https://gitlab.example.com/api/v4/projects/24/releases"
```

响应示例：

```json
{
   "tag_name":"v0.3",
   "description":"Super nice release",
   "name":"New release",
   "created_at":"2019-01-03T02:22:45.118Z",
   "released_at":"2019-01-03T02:22:45.118Z",
   "author":{
      "id":1,
      "name":"Administrator",
      "username":"root",
      "state":"active",
      "avatar_url":"https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80\u0026d=identicon",
      "web_url":"https://gitlab.example.com/root"
   },
   "commit":{
      "id":"079e90101242458910cccd35eab0e211dfc359c0",
      "short_id":"079e9010",
      "title":"Update README.md",
      "created_at":"2019-01-03T01:55:38.000Z",
      "parent_ids":[
         "f8d3d94cbd347e924aa7b715845e439d00e80ca4"
      ],
      "message":"Update README.md",
      "author_name":"Administrator",
      "author_email":"admin@example.com",
      "authored_date":"2019-01-03T01:55:38.000Z",
      "committer_name":"Administrator",
      "committer_email":"admin@example.com",
      "committed_date":"2019-01-03T01:55:38.000Z"
   },
   "milestones": [
       {
         "id":51,
         "iid":1,
         "project_id":24,
         "title":"v1.0-rc",
         "description":"Voluptate fugiat possimus quis quod aliquam expedita.",
         "state":"closed",
         "created_at":"2019-07-12T19:45:44.256Z",
         "updated_at":"2019-07-12T19:45:44.256Z",
         "due_date":"2019-08-16",
         "start_date":"2019-07-30",
         "web_url":"https://gitlab.example.com/root/awesome-app/-/milestones/1",
         "issue_stats": {
            "total": 99,
            "closed": 76
         }
       },
       {
         "id":52,
         "iid":2,
         "project_id":24,
         "title":"v1.0",
         "description":"Voluptate fugiat possimus quis quod aliquam expedita.",
         "state":"closed",
         "created_at":"2019-07-16T14:00:12.256Z",
         "updated_at":"2019-07-16T14:00:12.256Z",
         "due_date":"2019-08-16",
         "start_date":"2019-07-30",
         "web_url":"https://gitlab.example.com/root/awesome-app/-/milestones/2",
         "issue_stats": {
            "total": 24,
            "closed": 21
         }
       }
   ],
   "commit_path":"/root/awesome-app/commit/588440f66559714280628a4f9799f0c4eb880a4a",
   "tag_path":"/root/awesome-app/-/tags/v0.11.1",
   "evidence_sha":"760d6cdfb0879c3ffedec13af470e0f71cf52c6cde4d",
   "assets":{
      "count":5,
      "sources":[
         {
            "format":"zip",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.3/awesome-app-v0.3.zip"
         },
         {
            "format":"tar.gz",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.3/awesome-app-v0.3.tar.gz"
         },
         {
            "format":"tar.bz2",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.3/awesome-app-v0.3.tar.bz2"
         },
         {
            "format":"tar",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.3/awesome-app-v0.3.tar"
         }
      ],
      "links":[
         {
            "id":3,
            "name":"hoge",
            "url":"https://gitlab.example.com/root/awesome-app/-/tags/v0.11.1/binaries/linux-amd64",
            "link_type":"other"
         }
      ],
      "evidence_file_path":"https://gitlab.example.com/root/awesome-app/-/releases/v0.3/evidence.json"
   }
}
```

### 群组里程碑 **(PREMIUM SELF)**

> 引入于 13.5 版本。

与项目相关的群组里程碑可以在 `milestones` 数组中，为[创建发布](#create-a-release)和[更新发布](#update-a-release) API 调用所指定。只能指定与项目所在的群组关联的里程碑，添加上级群组的里程碑会引发错误。

<a id="collect-release-evidence"></a>

## 收集发布 Evidence **(PREMIUM SELF)**

> - 引入于极狐GitLab 12.10。
> - 更改为允许 `JOB-TOKEN` 于极狐GitLab 14.5。

为已有的发布创建 Evidence。

```plaintext
POST /projects/:id/releases/:tag_name/evidence
```

| 参数     | 类型           | 是否必需 | 描述                             |
| ------------- | -------------- | -------- | ----------------------------------------------------------------------------------- |
| `id`          | integer/string | yes      | ID 或者[项目 URL 路径](../rest/index.md#namespaced-path-encoding)。 |
| `tag_name`    | string         | yes      | 与发布关联的 Git 标签。                                         |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/24/releases/v0.1/evidence"
```

响应示例：

```json
200
```

<a id="update-a-release"></a>

## 更新发布

> 更改为允许 `JOB-TOKEN` 于 14.5 版本。

更新一个发布。更新发布需要开发者级别的项目访问权限。

```plaintext
PUT /projects/:id/releases/:tag_name
```

| 参数     | 类型           | 是否必需 | 描述                             |
| ------------- | --------------- | -------- | ----------------------------------------------------------------------------------------------------------- |
| `id`          | integer/string  | yes      | ID 或者 [项目 URL 路径](../rest/index.md#namespaced-path-encoding)。                         |
| `tag_name`    | string          | yes      | 与发布关联的 Git 标签。                                                                 |
| `name`        | string          | no       | 发布名称。                                                                                           |
| `description` | string          | no       | 发布的描述。您可以使用 [Markdown](../../user/markdown.md)。                             |
| `milestones`  | array of string | no       | 与发布相关联的每个里程碑的标题。专业版客户可以指定群组里程碑。要从发布中删除所有里程碑，请设置为 `[]`。 |
| `released_at` | datetime        | no       | 发布准备就绪的日期。采用 ISO 8601 格式 (`2019-03-15T08:00:00Z`)。          |

请求示例：

```shell
curl --header 'Content-Type: application/json' --request PUT --data '{"name": "new name", "milestones": ["v1.2"]}' \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/24/releases/v0.1"
```

响应示例：

```json
{
   "tag_name":"v0.1",
   "description":"## CHANGELOG\r\n\r\n- Remove limit of 100 when searching repository code. !8671\r\n- Show error message when attempting to reopen an MR and there is an open MR for the same branch. !16447 (Akos Gyimesi)\r\n- Fix a bug where internal email pattern wasn't respected. !22516",
   "name":"new name",
   "created_at":"2019-01-03T01:55:18.203Z",
   "released_at":"2019-01-03T01:55:18.203Z",
   "author":{
      "id":1,
      "name":"Administrator",
      "username":"root",
      "state":"active",
      "avatar_url":"https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80\u0026d=identicon",
      "web_url":"https://gitlab.example.com/root"
   },
   "commit":{
      "id":"f8d3d94cbd347e924aa7b715845e439d00e80ca4",
      "short_id":"f8d3d94c",
      "title":"Initial commit",
      "created_at":"2019-01-03T01:53:28.000Z",
      "parent_ids":[

      ],
      "message":"Initial commit",
      "author_name":"Administrator",
      "author_email":"admin@example.com",
      "authored_date":"2019-01-03T01:53:28.000Z",
      "committer_name":"Administrator",
      "committer_email":"admin@example.com",
      "committed_date":"2019-01-03T01:53:28.000Z"
   },
   "milestones": [
      {
         "id":53,
         "iid":3,
         "project_id":24,
         "title":"v1.2",
         "description":"Voluptate fugiat possimus quis quod aliquam expedita.",
         "state":"active",
         "created_at":"2019-09-01T13:00:00.256Z",
         "updated_at":"2019-09-01T13:00:00.256Z",
         "due_date":"2019-09-20",
         "start_date":"2019-09-05",
         "web_url":"https://gitlab.example.com/root/awesome-app/-/milestones/3",
         "issue_stats": {
            "opened": 11,
            "closed": 78
         }
      }
   ],
   "commit_path":"/root/awesome-app/commit/588440f66559714280628a4f9799f0c4eb880a4a",
   "tag_path":"/root/awesome-app/-/tags/v0.11.1",
   "evidence_sha":"760d6cdfb0879c3ffedec13af470e0f71cf52c6cde4d",
   "assets":{
      "count":4,
      "sources":[
         {
            "format":"zip",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.zip"
         },
         {
            "format":"tar.gz",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.tar.gz"
         },
         {
            "format":"tar.bz2",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.tar.bz2"
         },
         {
            "format":"tar",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.tar"
         }
      ],
      "links":[

      ],
      "evidence_file_path":"https://gitlab.example.com/root/awesome-app/-/releases/v0.1/evidence.json"
   }
}
```

## 删除发布

> 更改为允许 `JOB-TOKEN` 于 14.5 版本。

删除一个发布。删除发布不会删除关联的标签。需要项目维护者级别的访问权限才能删除发布。

```plaintext
DELETE /projects/:id/releases/:tag_name
```

| 参数     | 类型           | 是否必需 | 描述                             |
| ------------- | -------------- | -------- | ----------------------------------------------------------------------------------- |
| `id`          | integer/string | yes      | ID 或者[项目 URL 路径](../rest/index.md#namespaced-path-encoding)。 |
| `tag_name`    | string         | yes      | 与发布关联的 Git 标签。                                         |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/24/releases/v0.1"
```

响应示例：

```json
{
   "tag_name":"v0.1",
   "description":"## CHANGELOG\r\n\r\n- Remove limit of 100 when searching repository code. !8671\r\n- Show error message when attempting to reopen an MR and there is an open MR for the same branch. !16447 (Akos Gyimesi)\r\n- Fix a bug where internal email pattern wasn't respected. !22516",
   "name":"new name",
   "created_at":"2019-01-03T01:55:18.203Z",
   "released_at":"2019-01-03T01:55:18.203Z",
   "author":{
      "id":1,
      "name":"Administrator",
      "username":"root",
      "state":"active",
      "avatar_url":"https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80\u0026d=identicon",
      "web_url":"https://gitlab.example.com/root"
   },
   "commit":{
      "id":"f8d3d94cbd347e924aa7b715845e439d00e80ca4",
      "short_id":"f8d3d94c",
      "title":"Initial commit",
      "created_at":"2019-01-03T01:53:28.000Z",
      "parent_ids":[

      ],
      "message":"Initial commit",
      "author_name":"Administrator",
      "author_email":"admin@example.com",
      "authored_date":"2019-01-03T01:53:28.000Z",
      "committer_name":"Administrator",
      "committer_email":"admin@example.com",
      "committed_date":"2019-01-03T01:53:28.000Z"
   },
   "commit_path":"/root/awesome-app/commit/588440f66559714280628a4f9799f0c4eb880a4a",
   "tag_path":"/root/awesome-app/-/tags/v0.11.1",
   "evidence_sha":"760d6cdfb0879c3ffedec13af470e0f71cf52c6cde4d",
   "assets":{
      "count":4,
      "sources":[
         {
            "format":"zip",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.zip"
         },
         {
            "format":"tar.gz",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.tar.gz"
         },
         {
            "format":"tar.bz2",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.tar.bz2"
         },
         {
            "format":"tar",
            "url":"https://gitlab.example.com/root/awesome-app/-/archive/v0.1/awesome-app-v0.1.tar"
         }
      ],
      "links":[

      ],
      "evidence_file_path":"https://gitlab.example.com/root/awesome-app/-/releases/v0.1/evidence.json"
   }
}
```

## 即将发布

`released_at` 属性设置为未来日期的发布，在 UI 中被标记为**即将发布**。

此外，如果通过 API 请求列出发布，对于每个将 `release_at` 属性设置为未来日期的发布，将返回一个附加属性 `upcoming_release`（设置为 true），作为响应的一部分。

## 历史发布

> 引入于 15.2 版本。

`released_at` 属性设置为过去日期的发布，在 UI 中被标记为**历史发布**。

此外，如果通过 API 请求列出发布，对于每个将 `release_at` 属性设置为过去日期的发布，将返回一个附加属性 `historical_release`（设置为 true），作为响应的一部分。
