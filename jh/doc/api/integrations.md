---
stage: Manage
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 集成 API **(FREE

通过这个 API 使您能够使用与极狐GitLab 集成的外部服务。

NOTE:
在极狐GitLab 14.4 中，`services` 端点被重命名为`integrations`。对集成 API 的调用可以通过 `/projects/:id/services` 和 `/projects/:id/integrations` 进行。
本文档中的示例引用的端点是 `/projects/:id/integrations`。

该 API 需要一个具有维护者或所有者角色的访问令牌。

## 所有可用的集成列表

> 引入于极狐GitLab 12.7。

获取所有可用的项目集成列表。

```plaintext
GET /projects/:id/integrations
```

响应示例：

```json
[
  {
    "id": 75,
    "title": "Jenkins CI",
    "slug": "jenkins",
    "created_at": "2019-11-20T11:20:25.297Z",
    "updated_at": "2019-11-20T12:24:37.498Z",
    "active": true,
    "commit_events": true,
    "push_events": true,
    "issues_events": true,
    "alert_events": true,
    "confidential_issues_events": true,
    "merge_requests_events": true,
    "tag_push_events": false,
    "deployment_events": false,
    "note_events": true,
    "confidential_note_events": true,
    "pipeline_events": true,
    "wiki_page_events": true,
    "job_events": true,
    "comment_on_event_enabled": true
  },
  {
    "id": 76,
    "title": "Alerts endpoint",
    "slug": "alerts",
    "created_at": "2019-11-20T11:20:25.297Z",
    "updated_at": "2019-11-20T12:24:37.498Z",
    "active": true,
    "commit_events": true,
    "push_events": true,
    "issues_events": true,
    "alert_events": true,
    "confidential_issues_events": true,
    "merge_requests_events": true,
    "tag_push_events": true,
    "deployment_events": false,
    "note_events": true,
    "confidential_note_events": true,
    "pipeline_events": true,
    "wiki_page_events": true,
    "job_events": true,
    "comment_on_event_enabled": true
  }
]
```

## 苹果应用商店

使用极狐GitLab 在苹果应用商店中构建和发布应用程序。

<!--See also the [Apple App Store integration documentation](../user/project/integrations/apple_app_store.md).-->

### 创建/编辑苹果应用商店集成

为项目设置苹果应用商店集成。

```plaintext
PUT /projects/:id/integrations/apple_app_store
```

参数：

| 参数                      | 类型     | 是否必需 | 描述                              |
|-------------------------|--------|------|---------------------------------|
| `app_store_issuer_id`   | string | true | 苹果应用商店 Connect Issuer ID        |
| `app_store_key_id`      | string | true | 苹果应用商店 Connect Key ID           |
| `app_store_private_key` | string | true | 苹果应用商店 Connect Private Key      |
| `app_store_protected_refs` | boolean | false | 仅在受保护的分支和标签上设置变量。默认为 `true`（启用） |


### 禁用苹果应用商店集成

禁用项目的苹果应用商店集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/apple_app_store
```

### 获取苹果应用商店设置

获取项目的苹果应用商店设置。

```plaintext
GET /projects/:id/integrations/apple_app_store
```

## Asana

将提交信息作为评论添加到 Asana 任务中。

参见 [Asana集成文档](../user/project/integrations/asana.md)。

### 创建/编辑 Asana 集成

为一个项目设置 Asana 集成。

```plaintext
PUT /projects/:id/integrations/asana
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `api_key` | string | true | 用户API令牌。用户必须具有访问任务的权限。所有评论都归于该用户。 |
| `restrict_to_branch` | string | false | 将自动检查以逗号分隔的分支列表。 保留空白用以包括所有分支。 |

### 禁用 Asana 集成

禁用项目的 Asana 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/asana
```

### 获取 Asana 集成设置

获取项目的 Asana 集成设置。

```plaintext
GET /projects/:id/integrations/asana
```

## Assembla

项目管理软件 (源码提交端点)。

### 创建/编辑 Assembla 集成

为项目设置 Assembla 集成。

```plaintext
PUT /projects/:id/integrations/assembla
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `token` | string | true | 身份验证令牌
| `subdomain` | string | false | 子域名设置 |

### 禁用 Assembla 集成

禁用项目的 Assembla 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/assembla
```

### 获取 Assembla 集成设置

获取项目的 Assembla 集成设置。

```plaintext
GET /projects/:id/integrations/assembla
```

## Atlassian Bamboo CI

持续集成和构建服务器。

### 创建/编辑 Atlassian Bamboo CI 集成

为项目设置 Atlassian Bamboo CI 集成。

> 使用 Atlassian Bamboo 运行 CI/CD 流水线。您必须在 Bamboo 中设置自动修订标签和仓库触发器。

```plaintext
PUT /projects/:id/integrations/bamboo
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `bamboo_url` | string | true | Bamboo 根 URL。例如，`https://bamboo.example.com`。 |
| `enable_ssl_verification` | boolean | false | 启用 SSL 验证。默认为 true（启用）。 |
| `build_key` | string | true | Bamboo 构建计划密钥 如 KEY。 |
| `username` | string | true | 具有 API 访问权限的用户(如果适用)。 |
| `password` | string | true | 用户密码。 |

### 禁用 Atlassian Bamboo CI 集成

禁用项目的 Atlassian Bamboo CI 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/bamboo
```

### 获取 Atlassian Bamboo CI 集成设置

获取项目的 Atlassian Bamboo CI 集成设置。

```plaintext
GET /projects/:id/integrations/bamboo
```

## Bugzilla

Bugzilla 议题跟踪器。

### 创建/编辑 Bugzilla 集成

为项目设置 Bugzilla 集成。

```plaintext
PUT /projects/:id/integrations/bugzilla
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `new_issue_url` | string | true |  新议题 URL |
| `issues_url` | string | true | 议题 URL |
| `project_url` | string | true | 项目 URL |

### 禁用 Bugzilla 集成

禁用项目的 Bugzilla 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/bugzilla
```

### 获取 Bugzilla 集成设置

获取项目的 Bugzilla 集成设置。

```plaintext
GET /projects/:id/integrations/bugzilla
```

## Buildkite

持续集成和部署。

### 创建/编辑 Buildkite 集成

为项目设置 Buildkite 集成。

```plaintext
PUT /projects/:id/integrations/buildkite
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `token` | string | true | Buildkite 项目的极狐GitLab 令牌。 |
| `project_url` | string | true | 流水线的 URL。例如，`https://buildkite.com/example/pipeline`。 |
| `enable_ssl_verification` | boolean | false | 已删除：这个参数没有影响，因为 SSL 验证总是启用的。 |
| `push_events` | boolean | false | 启用推送事件的通知。 |
| `merge_requests_events` | boolean | false | 启用合并请求事件的通知。 |
| `tag_push_events` | boolean | false | 启用标签推送事件的通知。 |

### 禁用 Buildkite 集成

禁用项目的 Buildkite 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/buildkite
```

### 获取 Buildkite 集成设置

获取项目的 Buildkite 集成设置。

```plaintext
GET /projects/:id/integrations/buildkite
```

## Campfire

向 Campfire Chat Room 发送有关推送事件的通知。
[新用户已无法注册 Campfire](https://basecamp.com/handbook/05-product-histories#campfire)。

### 创建/编辑 Campfire 集成

为项目设置 Campfire 集成。

```plaintext
PUT /projects/:id/integrations/campfire
```

参数：

| 参数 | 类型 | 是否必需 | 描述                                                                                 |
|---------------|---------|----------|---------------------------------------------------------------------------------------------|
| `token`       | string  | true     | Campfire 的 API 令牌。想要要找到它，请登录 Campfire 并选择 **My info**。                   |
| `subdomain`   | string  | false    | Campfire 的子域名。当您登录时，`https://` 和 `.campfirenow.com` 之间的文本。 |
| `room`        | string  | false    | Campfire 的房间。当你在一个房间里时，URL 的最后部分的文本。               |

### 禁用 Campfire 集成

禁用项目的 Campfire 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/campfire
```

### 获取 Campfire 集成设置

获取项目的 Campfire 集成设置。

```plaintext
GET /projects/:id/integrations/campfire
```

## ClickUp

> 引入于极狐GitLab 16.1。

ClickUp 议题跟踪器。

### 创建或编辑 ClickUp 集成

为项目设置 ClickUp 集成。

```plaintext
PUT /projects/:id/integrations/clickup
```

参数：

| 参数            | 类型     | 是否比需 | 描述     |
|---------------|--------|------|--------|
| `issues_url`  | string | true | 议题 URL |
| `project_url` | string | true | 项目 URL |

### 禁用 ClickUp 集成

为项目禁用 ClickUp 集成。重置集成设置。

```plaintext
DELETE /projects/:id/integrations/clickup
```

### 获取 ClickUp 集成设置

为项目获取 ClickUp 集成设置。

```plaintext
GET /projects/:id/integrations/clickup
```

## Datadog

Datadog 系统监控。

### 创建/编辑 Datadog 集成

为项目设置 Datadog 集成。

```plaintext
PUT /projects/:id/integrations/datadog
```

参数:

|     参数     |  类型  | 是否必需 | 描述                                                                     |
|:-----------------:|:------:|----------|------------------------------------------------------------------------|
| `api_key`         | string | true     | 用于与 Datadog 验证的 API 密钥。                                                |
| `api_url`         | string | false    | （高级）您的 Datadog 站点的完整 URL。                                              |
| `datadog_env`     | string | false    | 对于自助管理部署，为所有发送到 Datadog 的数据设置 env% 标签。                                 |
| `datadog_service` | string | false    | 在 Datadog 中，为来自此极狐GitLab 实例的所有数据打标签。当管理多个自助管理部署时很有用。                   |
| `datadog_site`    | string | false    | 发送数据的 Datadog 网站。要发送数据到欧盟的网站，请使用 `datadoghq.eu`。                       |
| `datadog_tags`    | string | false    | Datadog中的自定义标签。每行指定一个标签，格式为：`key:value\nkey2:value2`。引入于极狐GitLab 14.8。 |
| `archive_trace_events` | boolean | false    | 启用后，作业日志由 Datadog 收集并与流水线执行跟踪一起显示。引入于 15.3。                            |


<!-- | `archive_trace_events` | boolean | false    | When enabled, job logs are collected by Datadog and displayed along with pipeline execution traces ([introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/346339) in GitLab 14.7) | -->
<!-- TODO: uncomment the archive_trace_events field once :datadog_integration_logs_collection is rolled out. Rollout issue: https://gitlab.com/gitlab-org/gitlab/-/issues/346339 -->

### 禁用 Datadog 集成

禁用项目的 Datadog 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/datadog
```

### 获取 Datadog 集成设置

为项目获取 Datadog 集成设置。

```plaintext
GET /projects/:id/integrations/datadog
```

## Telegram

Telegram 聊天工具。

### 创建/编辑 Telegram 集成

为项目设置 Telegram 集成。

```plaintext
PUT /projects/:id/integrations/telegram
```

参数：

| 参数                           | 类型      | 是否必需 | 描述                                                             |
|------------------------------|---------|------|----------------------------------------------------------------|
| `token`                      | string  | true | Telegram bot 令牌。例如 `123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11` |
| `room`                       | string  | true | 目标聊天的唯一标识符或目标频道的用户名（格式为 `@channelusername`）                    |
| `push_events`                | boolean | true | 为推送事件启用通知                                                      |
| `issues_events`              | boolean | true | 为议题事件启用通知                                                      |
| `confidential_issues_events` | boolean | true | 为私密议题事件启用通知                                                    |
| `merge_requests_events`      | boolean | true | 为合并请求事件启用通知                                                    |
| `tag_push_events`            | boolean | true | 为标签推送事件启用通知                                                    |
| `note_events`                | boolean | true | 为备注事件启用通知                                                      |
| `confidential_note_events`   | boolean | true | 为私密备注事件启用通知                                                    |
| `pipeline_events`            | boolean | true | 为流水线事件启用通知                                                     |
| `wiki_page_events`           | boolean | true | 为 Wiki 页面事件启用通知                                                |

### 禁用 Telegram 集成

为项目禁用 Telegram 集成。重置集成设置。

```plaintext
DELETE /projects/:id/integrations/telegram
```

### 获取 Telegram 集成设置

为项目获取 Telegram 集成设置。

```plaintext
GET /projects/:id/integrations/telegram
```

## Unify Circuit

Unify Circuit RTC 和协作工具。
<!-- Unify Circuit RTC and collaboration tool. -->

### 创建/编辑 Unify Circuit 集成

为项目设置 Unify Circuit 集成。

```plaintext
PUT /projects/:id/integrations/unify-circuit
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `webhook` | string | true | Unify Circuit 的 webhook。例如，`https://circuit.com/rest/v2/webhooks/incoming/...`。 |
| `notify_only_broken_pipelines` | boolean | false | 发送流水线失败通知。 |
| `branches_to_be_notified` | string | false | 向其发送通知的分支。有效选项为 `all`、`default`、`protected` 和 `default_and_protected`。系统默认值为 `default`。 |
| `push_events` | boolean | false | 启用推送事件的通知。 |
| `issues_events` | boolean | false | 启用议题事件的通知。 |
| `confidential_issues_events` | boolean | false | 启用私密议题事件的通知。 |
| `merge_requests_events` | boolean | false | 启用合并请求事件的通知。 |
| `tag_push_events` | boolean | false | 启用标签推送事件的通知。 |
| `note_events` | boolean | false | 启用备注事件的通知。 |
| `confidential_note_events` | boolean | false | 启用私密备注事件的通知。 |
| `pipeline_events` | boolean | false | 启用流水线事件的通知。 |
| `wiki_page_events` | boolean | false | 启用 wiki 页面事件的通知。 |

### 禁用 Unify Circuit 集成

禁用项目的 Unify Circuit 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/unify-circuit
```

### 获取 Unify Circuit 集成设置

为项目获取 Unify Circuit 集成设置。

```plaintext
GET /projects/:id/integrations/unify-circuit
```

## Pumble

Pumble chat 工具。

### 创建/编辑 Pumble 集成

设置项目 Pumble 集成。

```plaintext
PUT /projects/:id/integrations/pumble
```

参数：

| 参数                             | 类型      | 是否必需  | 描述                                                                               |
|--------------------------------|---------|-------|----------------------------------------------------------------------------------|
| `webhook`                      | string  | true  | Pumble webhook。例如 `https://api.pumble.com/workspaces/x/...`                      |
| `branches_to_be_notified`      | string  | false | 发送通知的分支。有效值为 `all`、`default`、`protected` 和 `default_and_protected`。默认为 `default` |
| `confidential_issues_events`   | boolean | false | 启用机密议题事件通知                                                                       |
| `confidential_note_events`     | boolean | false | 启用机密备注事件通知                                                                       |
| `issues_events`                | boolean | false | 启用议题事件通知                                                                         |
| `merge_requests_events`        | boolean | false | 启用合并请求事件通知                                                                       |
| `note_events`                  | boolean | false | 启用备注事件通知                                                                         |
| `notify_only_broken_pipelines` | boolean | false | 发送失败流水线的通知                                                                       |
| `pipeline_events`              | boolean | false | 启用流水线事件通知                                                                        |
| `push_events`                  | boolean | false | 启用推送事件通知                                                                         |
| `tag_push_events`              | boolean | false | 启用标签推送事件通知                                                                       |
| `wiki_page_events`             | boolean | false | 启用 wiki 页面事件通知                                                                   |

### 禁用 Pumble 集成

禁用项目 Pumble 集成。保留集成设置。

```plaintext
DELETE /projects/:id/integrations/pumble
```

### 获取 Pumble 集成设置

获取项目 Pumble 集成设置。

```plaintext
GET /projects/:id/integrations/pumble
```

## Webex Teams

Webex Teams 协作工具。

### 创建/编辑 Webex Teams 集成

为项目设置 Webex Teams 集成。

```plaintext
PUT /projects/:id/integrations/webex-teams
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `webhook` | string | true | Webex Teams 的 webhook。例如，`https://api.ciscospark.com/v1/webhooks/incoming/...`。 |
| `notify_only_broken_pipelines` | boolean | false | 发送流水线失败通知。 |
| `branches_to_be_notified` | string | false | 有效选项为 `all`、`default`、`protected` 和 `default_and_protected`。系统默认值为 `default`。 |
| `push_events` | boolean | false | 启用推送事件的通知。 |
| `issues_events` | boolean | false | 启用议题事件的通知。 |
| `confidential_issues_events` | boolean | false | 启用私密议题事件的通知。 |
| `merge_requests_events` | boolean | false | 启用合并请求事件的通知。 |
| `tag_push_events` | boolean | false | 启用标签推送事件的通知。 |
| `note_events` | boolean | false | 启用备注事件的通知。 |
| `confidential_note_events` | boolean | false | 启用私密备注事件的通知。 |
| `pipeline_events` | boolean | false | 启用流水线事件的通知。 |
| `wiki_page_events` | boolean | false | 启用 wiki 页面事件的通知。 |

### 禁用 Webex Teams 集成

禁用项目的 Webex Teams 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/webex-teams
```

### 获取 Webex Teams 集成设置

为项目获取 Webex Teams 集成设置。

```plaintext
GET /projects/:id/integrations/webex-teams
```

## 自定义议题跟踪器

自定义议题跟踪器。

### 创建/编辑自定义议题跟踪器集成

为项目设置自定义问题跟踪集成。

```plaintext
PUT /projects/:id/integrations/custom-issue-tracker
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `new_issue_url` | string | true |  新的议题 URL |
| `issues_url` | string | true | 议题 URL |
| `project_url` | string | true | 项目 URL |

### 禁用自定义议题跟踪器集成

禁用项目的自定义问题跟踪集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/custom-issue-tracker
```

### 获取自定义议题跟踪器集成设置

获取项目的自定义问题跟踪器集成设置。

```plaintext
GET /projects/:id/integrations/custom-issue-tracker
```

## Discord

将项目事件的通知发送到一个 Discord 频道。

### 创建/编辑 Discord 集成

为项目设置 Discord 集成。

```plaintext
PUT /projects/:id/integrations/discord
```

参数：

| 参数 | 类型 | 是否必需 | 描述                                                                                   |
| --------- | ---- | -------- |--------------------------------------------------------------------------------------|
| `webhook` | string | true | Discord webhook。例如，`https://discord.com/api/webhooks/…`                              |
| `branches_to_be_notified` | string | false | 要发送通知的分支。有效选项为 `all`、 `default`、`protected` 和 `default_and_protected`。默认值为 `default` |
| `confidential_issues_events` | boolean | false | 启用私密议题事件的通知                                                                          |
| `confidential_note_events` | boolean | false | 启用私密备注事件的通知                                                                          |
| `issues_events` | boolean | false | 启用议题事件的通知                                                                            |
| `merge_requests_events` | boolean | false | 启用合并请求事件的通知                                                                          |
| `note_events` | boolean | false | 启用备注事件的通知                                                                            |
| `notify_only_broken_pipelines` | boolean | false | 发送失败流水线的通知                                                                           |
| `pipeline_events` | boolean | false | 启用流水线事件的通知                                                                           |
| `push_events` | boolean | false | 启用推送事件的通知                                                                            |
| `tag_push_events` | boolean | false | 启用标签推送事件的通知                                                                          |
| `wiki_page_events` | boolean | false | 启用 Wiki 页面事件的通知                                                                      |

### 禁用 Discord 集成

禁用项目的Discord集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/discord
```

### 获取 Discord 集成设置

获取项目的 Discord 集成设置。

```plaintext
GET /projects/:id/integrations/discord
```

## Drone CI

Drone 是用 Go 编写的一个建立在 Docker 上的持续集成平台。

### 创建/编辑 Drone CI 集成

为项目设置 Drone CI 集成。

```plaintext
PUT /projects/:id/integrations/drone-ci
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `token` | string | true | Drone CI 项目特定的访问令牌。 |
| `drone_url` | string | true | `http://drone.example.com`。 |
| `enable_ssl_verification` | boolean | false | 启用 SSL 验证。默认为 true（启用）。 |
| `push_events` | boolean | false | 启用推送事件的通知。 |
| `merge_requests_events` | boolean | false | 启用合并请求事件的通知。 |
| `tag_push_events` | boolean | false | 启用标签推送事件的通知。 |

### 禁用 Drone CI 集成

禁用项目的 Drone CI 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/drone-ci
```

### 获取 Drone CI 集成设置

获取项目的 Drone CI 集成设置。

```plaintext
GET /projects/:id/integrations/drone-ci
```

## 邮件推送

通过电子邮件将每次推送的提交和差异发送给收件人名单。

### 创建/编辑邮件推送集成

为项目设置邮件推送集成。

```plaintext
PUT /projects/:id/integrations/emails-on-push
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `recipients` | string | true | 用空格分隔的电子邮件 |
| `disable_diffs` | boolean | false | 禁用代码差异 |
| `send_from_committer_email` | boolean | false | 发送自提交者 |
| `push_events` | boolean | false | 启用推送事件的通知 |
| `tag_push_events` | boolean | false | 启用标签推送事件的通知 |
| `branches_to_be_notified` | string | false | 向其发送通知的分支。有效选项为 `all`、`default`、`protected` 和 `default_and_protected`。标记推送总是触发通知。默认值为 `all`。 |

### 禁用邮件推送集成

禁用项目的邮件推送集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/emails-on-push
```

### 获取邮件推送集成设置

获取项目的邮件推送集成设置。

```plaintext
GET /projects/:id/integrations/emails-on-push
```

## 研发工作流程管理 (EWM)

使用 IBM 研发工作流程管理（EWM）作为项目的议题跟踪器。

### 创建/编辑 EWM 集成

为项目设置 EWM 集成。

```plaintext
PUT /projects/:id/integrations/ewm
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `new_issue_url` | string | true | 在 EWM 中创建一个议题的 URL。 |
| `project_url`   | string | true | EWM 中项目的 URL。 |
| `issues_url`    | string | true | 在 EWM 中查看一个议题的 URL。必须包含`:id`。 |

### 禁用 EWM 集成

禁用项目的 EWM 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/ewm
```

### 获取 EWM 集成设置

获取项目的 EWM 集成设置。

```plaintext
GET /projects/:id/integrations/ewm
```

## Confluence 集成

> 引入于极狐GitLab 13.2。

将指向内部 wiki 的 URL 替换为指向 Confluence 云工作区 URL。

### 创建/编辑 Confluence 集成

为项目设置 Confluence 集成。

```plaintext
PUT /projects/:id/integrations/confluence
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `confluence_url` | string | true | 设在 atlassian.net 上的 Confluence 云工作区的 URL。  |

### 禁用 Confluence 集成

禁用项目的Confluence集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/confluence
```

### 获取 Confluence 集成设置

获取 Confluence 集成设置的项目。

```plaintext
GET /projects/:id/integrations/confluence
```

## Shimo 集成

> - 引入于极狐GitLab 14.5，[功能标志](../administration/feature_flags.md)为 `shimo_integration`。默认禁用。
> - 启用于 JiHuLab.com 于极狐GitLab 15.4。
> - 广泛可用于极狐GitLab 15.4。移除了功能标志 `shimo_integration`。

将指向内部 wiki 的链接替换为指向 Shimo 工作区的链接。

### 创建/编辑 Shimo 集成

设置项目 Shimo 集成。

```plaintext
PUT /projects/:id/integrations/shimo
```

参数：

| 参数                  | 类型     | 是否必需 | 描述            |
|---------------------|--------|------|---------------|
| `external_wiki_url` | string | true | Shimo 工作区 URL |

### 禁用 Shimo 集成

禁用项目 Shimo 集成。保留集成设置。

```plaintext
DELETE /projects/:id/integrations/shimo
```

## 外部 wiki

将内部 wiki 的 URL 替换为外部 wiki 的 URL。

### 创建/编辑 External wiki 集成

为项目设置外部wiki集成。

```plaintext
PUT /projects/:id/integrations/external-wiki
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `external_wiki_url` | string | true | 外部 wiki 的 URL。 |

### 禁用外部 wiki 集成

禁用项目的外部 wiki 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/external-wiki
```

### 获取外部 wiki 集成设置

获取项目的外部 wiki 集成设置。

```plaintext
GET /projects/:id/integrations/external-wiki
```

## GitHub **(PREMIUM)**

代码协作软件。

### 创建/编辑 GitHub 集成

为项目设置 GitHub 集成。

```plaintext
PUT /projects/:id/integrations/github
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `token` | string | true | 带有 `repo:status` OAuth 范围的 GitHub API 令牌。 |
| `repository_url` | string | true | GitHub 仓库 URL。 |
| `static_context` | boolean | false | 在[状态检查名称](../user/project/integrations/github.md#static-or-dynamic-status-check-names)中添加实例名称而不是分支。 |

### 禁用 GitHub 集成

禁用项目的 GitHub 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/github
```

### 获取 GitHub 集成设置

获取项目的 GitHub 集成设置。

```plaintext
GET /projects/:id/integrations/github
```

## Hangouts Chat

Google workspace 团队协作工具。

### 创建/编辑 Hangouts Chat 集成

为项目设置 Hangouts Chat 集成。

```plaintext
PUT /projects/:id/integrations/hangouts-chat
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `webhook` | string | true | Hangouts Chat的 webhook。例如，`https://chat.googleapis.com/v1/spaces...`。 |
| `notify_only_broken_pipelines` | boolean | false | 发送流水线失败通知。 |
| `notify_only_default_branch` | boolean | false | 已删除：该参数已被 `branches_to_be_notified` 取代。 |
| `branches_to_be_notified` | string | false | 要发送通知的分支。有效选项是 `all`、`default`、`protected` 和 `default_and_protected`。默认值是 `default`。 |
| `push_events` | boolean | false | 启用推送事件的通知。 |
| `issues_events` | boolean | false | 启用议题事件的通知。 |
| `confidential_issues_events` | boolean | false | 启用私密议题事件的通知。 |
| `merge_requests_events` | boolean | false | 启用合并请求事件的通知。 |
| `tag_push_events` | boolean | false | 启用标签推送事件的通知。 |
| `note_events` | boolean | false | 启用备注事件的通知。 |
| `confidential_note_events` | boolean | false | 启用私密备注事件的通知。 |
| `pipeline_events` | boolean | false | 启用流水线事件的通知。 |
| `wiki_page_events` | boolean | false | 启用 wiki 页面事件的通知。 |

### 禁用 Hangouts Chat 集成

禁用项目的 Hangouts Chat 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/hangouts-chat
```

### 获取 Hangouts Chat 集成设置

获取 Hangouts Chat 集成设置的项目。

```plaintext
GET /projects/:id/integrations/hangouts-chat
```

## Irker (IRC gateway)

在更新时，通过 irker 网关向收件人列表发送 IRC 信息。

<!--更多信息，请参见 [irker 集成文档](.../user/project/integrations/irker.md)。-->

### 创建/编辑 Irker (IRC gateway) 集成

为项目设置 Irker（IRC gateway）集成。

```plaintext
PUT /projects/:id/integrations/irker
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `recipients` | string | true | 用空格分隔的收件人/频道 |
| `default_irc_uri` | string | false | `irc://irc.network.net:6697/` |
| `server_host` | string | false | localhost |
| `server_port` | integer | false | 6659 |
| `colorize_messages` | boolean | false | 对信息进行着色 |

### 禁用 Irker (IRC gateway) 集成

禁用项目的 Irker（IRC gateway）集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/irker
```

### 获取 Irker (IRC gateway) 集成设置

获取 Irker(IRC gateway)集成设置的项目。

```plaintext
GET /projects/:id/integrations/irker
```

## Jira

Jira 议题跟踪器。

### 获取 Jira 集成设置

获取 Jira 集成设置的项目。

```plaintext
GET /projects/:id/integrations/jira
```

### 创建/编辑 Jira 集成

为项目设置 Jira 集成。

```plaintext
PUT /projects/:id/integrations/jira
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `url`           | string | yes | Jira 项目的 URL，该项目被链接到该 极狐GitLab 项目。例如，`https://jira.example.com`。 |
| `api_url`   | string | no | Jira 实例 API 的 URL。如果没有设置，则使用 Web URL值。例如，`https://jira-api.example.com`。 |
| `username`      | string | yes  | 创建的用户的用户名，以便与极狐GitLab/Jira 一起使用。|
| `password`      | string | yes  | 创建的用户的密码，以便与极狐GitLab/Jira 一起使用。|
| `active`        | boolean | no  | 激活或停用集成。默认为 false（停用）。 |
| `jira_issue_transition_automatic` | boolean | no | 启用自动议题转换。如果启用，将优先于`jira_issue_transition_id`。默认为 `false`。 |
| `jira_issue_transition_id` | string | no | 一个或多个自定义议题转换的 ID。如果 `jira_issue_transition_automatic` 被启用，则忽略。默认为一个空白字符串，这将禁用自定义转换。 |
| `commit_events` | boolean | false | 启用提交事件的通知。 |
| `merge_requests_events` | boolean | false | 启用合并请求事件的通知。 |
| `comment_on_event_enabled` | boolean | false | 在 Jira 议题中启用对每个极狐GitLab 事件（提交/合并请求）的评论。 |

### 禁用 Jira 集成

禁用项目的 Jira 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/jira
```

## Slack 指令

能够从 Slack 聊天实例中接收指令。

### 获取 Slack 指令集成设置

获取 Slack 指令集成设置，用于一个项目中。

```plaintext
GET /projects/:id/integrations/slack-slash-commands
```

响应示例：

```json
{
  "id": 4,
  "title": "Slack slash commands",
  "slug": "slack-slash-commands",
  "created_at": "2017-06-27T05:51:39-07:00",
  "updated_at": "2017-06-27T05:51:39-07:00",
  "active": true,
  "push_events": true,
  "issues_events": true,
  "confidential_issues_events": true,
  "merge_requests_events": true,
  "tag_push_events": true,
  "note_events": true,
  "job_events": true,
  "pipeline_events": true,
  "comment_on_event_enabled": false,
  "properties": {
    "token": "<your_access_token>"
  }
}
```

### 创建/编辑 Slack 指令集成

为一个项目设置 Slack 指令。

```plaintext
PUT /projects/:id/integrations/slack-slash-commands
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `token` | string | yes | Slack 访问令牌 |

### 禁用 Slack 指令集成

禁用项目的 Slack 指令集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/slack-slash-commands
```

## Mattermost 指令

能够从 Mattermost 聊天实例中接收指令。

### 获取 Mattermost 指令集成设置

获取项目的 Mattermost 指令集成设置。

```plaintext
GET /projects/:id/integrations/mattermost-slash-commands
```

### 创建/编辑 Mattermost 指令集成

为项目设置 Mattermost 指令。

```plaintext
PUT /projects/:id/integrations/mattermost-slash-commands
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `token` | string | yes | Mattermost 访问令牌 |

### 禁用 Mattermost 指令集成

禁用项目中 Mattermost 指令集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/mattermost-slash-commands
```

## Packagist

当提交或标签被推送到极狐GitLab 时，更新您在 Packagist（Composer 主仓库）的项目。

### 创建/编辑 Packagist 集成

为项目设置 Packagist 集成。

```plaintext
PUT /projects/:id/integrations/packagist
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `username` | string | yes | 一个 Packagist 账户的用户名。 |
| `token` | string | yes | Packagist 服务器的 API 令牌。 |
| `server` | boolean | no | Packagist 服务器的 URL。留空为默认。<https://packagist.org> |
| `push_events` | boolean | false | 启用推送事件的通知。 |
| `merge_requests_events` | boolean | false | 启用合并请求事件的通知。 |
| `tag_push_events` | boolean | false | 启用标签推送事件的通知。 |

### 禁用 Packagist 集成

禁用项目的Packagist集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/packagist
```

### 获取 Packagist 集成设置

获取项目的 Packagist 集成设置。

```plaintext
GET /projects/:id/integrations/packagist
```

## Pipeline-Emails

获取极狐GitLab CI/CD 流水线的电子邮件。

### 创建/编辑 Pipeline-Emails 集成

为一个项目设置 Pipeline-Emails 集成。

```plaintext
PUT /projects/:id/integrations/pipelines-email
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `recipients` | string | yes | 以逗号分隔的收件人电子邮件地址列表。 |
| `notify_only_broken_pipelines` | boolean | no | 只通知失败的流水线。 |
| `branches_to_be_notified` | string | false | 要发送通知的分支。有效选项是 `all`、`default`、`protected` 和 `default_and_protected`。默认值是 `default`。 |
| `notify_only_default_branch` | boolean | no | 只对默认分支发送通知。 |
| `pipeline_events` | boolean | false | 启用流水线事件的通知。 |

### 禁用 Pipeline-Emails 集成

禁用项目的 Pipeline-Emails 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/pipelines-email
```

### 获取 Pipeline-Emails 集成设置

为项目获取 Pipeline-Emails 集成设置。

```plaintext
GET /projects/:id/integrations/pipelines-email
```

## Pivotal Tracker

在 Pivotal Tracker 故事中添加提交信息作为评论。

<!--请参见[Pivotal Tracker集成文档](.../user/project/integrations/pivotal_tracker.md)。-->

### 创建/编辑 Pivotal Tracker 集成

为项目设置 Pivotal Tracker 集成。

```plaintext
PUT /projects/:id/integrations/pivotaltracker
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `token` | string | true | Pivotal Tracker 访问令牌。 |
| `restrict_to_branch` | boolean | false | 以逗号分隔的要自动检查的分支列表。留空以包括所有分支。 |

### 禁用 Pivotal Tracker 集成

禁用项目的Pivotal Tracker集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/pivotaltracker
```

### 获取 Pivotal Tracker 集成设置

获取 Pivotal Tracker 集成设置的项目。

```plaintext
GET /projects/:id/integrations/pivotaltracker
```

## 普罗米修斯(Prometheus)

普罗米修斯是一个强大的时间序列监测服务。

### 创建/编辑普罗米修斯集成

为项目设置普罗米修斯集成。

```plaintext
PUT /projects/:id/integrations/prometheus
```

参数:

| 参数 | 类型 | 是否必需 | 描述                                                                                 |
| --------- | ---- | -------- |------------------------------------------------------------------------------------|
| `api_url` | string | true | 普罗米修斯 API 基础 URL。例如，`http://prometheus.example.com/`。                              |
| `google_iap_audience_client_id` | string | false | IAP 安全资源的客户端 ID（例如 IAP_CLIENT_ID.apps.googleusercontent.com）。                      |
| `google_iap_service_account_json` | string | false | 您的服务账户的 `credentials.json` 文件，如 `{"type": "service_account", "project_id": ... }`。 |

### 禁用 Prometheus 集成

禁用项目的普罗米修斯集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/prometheus
```

### 获取 Prometheus 集成设置

获取普罗米修斯集成设置的项目。

```plaintext
GET /projects/:id/integrations/prometheus
```

## Pushover

Pushover 让您在 Android 设备、iPhone、iPad 和桌面上轻松获取实时通知。

### 创建/编辑 Pushover 集成

为项目设置 Pushover 集成。

```plaintext
PUT /projects/:id/integrations/pushover
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `api_key` | string | true | 您的申请密钥 |
| `user_key` | string | true | 您的用户密钥 |
| `priority` | string | true | 优先级 |
| `device` | string | false | 留空为所有活动设备 |
| `sound` | string | false | 通知的声音 |

### 禁用 Pushover 集成

禁用项目的 Pushover。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/pushover
```

### 获取 Pushover 集成设置

获取项目的 Pushover 集成设置。

```plaintext
GET /projects/:id/integrations/pushover
```

## Redmine

Redmine 议题跟踪器。

### 创建/编辑 Redmine 集成

为项目设置 Redmine 集成。

```plaintext
PUT /projects/:id/integrations/redmine
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `new_issue_url` | string | true | 新的议题 URL |
| `project_url` | string | true | 项目 URL |
| `issues_url` | string | true | 议题 URL |

### 禁用 Redmine 集成

禁用项目的 Redmine 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/redmine
```

### 获取 Redmine 集成设置

获取项目的 Redmine 集成设置。

```plaintext
GET /projects/:id/integrations/redmine
```

## Slack 通知

在 Slack 中接收事件通知。

### 创建/编辑 Slack 集成

为项目设置 Slack 集成。

```plaintext
PUT /projects/:id/integrations/slack
```

参数:

| 参数 | 类型 | 是否必需 | 描述                                                                                   |
| --------- | ---- | -------- |--------------------------------------------------------------------------------------|
| `webhook` | string | true | `https://hooks.slack.com/services/...`                                               |
| `username` | string | false | 用户名。                                                                                 |
| `channel` | string | false | 如果没有配置其他频道，则使用默认频道。                                                                  |
| `notify_only_broken_pipelines` | boolean | false | 发送流水线失败通知。                                                                           |
| `notify_only_default_branch` | boolean | false | 已删除：该参数已被 `branches_to_be_notified` 取代。                                              |
| `branches_to_be_notified` | string | false | 要发送通知的分支。有效选项是 `all`、`default`、`protected` 和 `default_and_protected`。默认值是 `default`。 |
| `alert_channel` | string | false | 接收告警事件通知的频道名称。                                                                       |
| `alert_events` | boolean | false | 启用告警事件的通知。                                                                           |
| `commit_events` | boolean | false | 启用提交事件的通知。 |
| `confidential_issue_channel` | string | false | 接收私密事件通知的频道名称。                                                                       |
| `confidential_issues_events` | boolean | false | 启用私密问题事件的通知。                                                                         |
| `confidential_note_channel` | string | false | 接收私密备注事件通知的频道名称。                                                                     |
| `confidential_note_events` | boolean | false | 启用私密备注事件的通知。                                                                         |
| `deployment_channel` | string | false | 接收部署事件通知的频道的名称。                                                                      |
| `deployment_events` | boolean | false | 启用部署事件的通知。                                                                           |
| `incident_channel` | string | false | 接收 incident 事件通知的频道名称。                                                               |
| `incidents_events` | boolean | false | 启用 incident 事件通知。                         |
| `issue_channel` | string | false | 接收事件通知的频道名称。                                                                         |
| `issues_events` | boolean | false | 启用议题事件的通知。                                                                           |
| `job_events` | boolean | false | 启用任务事件的通知。                                                                           |
| `merge_request_channel` | string | false | 接收合并请求事件通知的频道名称。                                                                     |
| `merge_requests_events` | boolean | false | 启用合并请求事件的通知。                                                                         |
| `note_channel` | string | false | 接收备注事件通知的频道名称。                                                                       |
| `note_events` | boolean | false | 启用备注事件的通知。                                                                           |
| `pipeline_channel` | string | false | 接收流水线事件通知的频道名称。                                                                      |
| `pipeline_events` | boolean | false | 启用流水线事件的通知。                                                                          |
| `push_channel` | string | false | 接收推送事件通知的频道名称。                                                                       |
| `push_events` | boolean | false | 启用推送事件的通知。                                                                           |
| `tag_push_channel` | string | false | 接收标签推送事件通知的频道名称。                                                                     |
| `tag_push_events` | boolean | false | 启用标签推送事件的通知。                                                                         |
| `wiki_page_channel` | string | false | 接收 wiki 页面事件通知的频道名称。                                                                 |
| `wiki_page_events` | boolean | false | 启用 wiki 页面事件的通知。                                                                     |

### 禁用 Slack 集成

禁用项目的 Slack 集成。 集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/slack
```

### 获取 Slack 集成设置

获取项目的 Slack 集成设置。

```plaintext
GET /projects/:id/integrations/slack
```

## Microsoft Teams

群组聊天软件。

### 创建/编辑 Microsoft Teams 集成

为项目设置 Microsoft Teams 集成。

```plaintext
PUT /projects/:id/integrations/microsoft-teams
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `webhook` | string | true | Microsoft 的 Teams webhook。例如，`https://outlook.office.com/webhook/...`。 |
| `notify_only_broken_pipelines` | boolean | false | 发送流水线失败通知。 |
| `notify_only_default_branch` | boolean | false | 已删除：该参数已被 `branches_to_be_notified` 取代。 |
| `branches_to_be_notified` | string | false | 要发送通知的分支。有效选项是 `all`、`default`、`protected` 和 `default_and_protected`。默认值是 `default`。 |
| `push_events` | boolean | false | 启用推送事件的通知。 |
| `issues_events` | boolean | false | 启用议题事件的通知。 |
| `confidential_issues_events` | boolean | false | 启用私密议题事件的通知。 |
| `merge_requests_events` | boolean | false | 启用合并请求事件的通知。 |
| `tag_push_events` | boolean | false | 启用标签推送事件的通知。 |
| `note_events` | boolean | false | 启用备注事件的通知。 |
| `confidential_note_events` | boolean | false | 启用私密备注事件的通知。 |
| `pipeline_events` | boolean | false | 启用流水线事件的通知。 |
| `wiki_page_events` | boolean | false | 启用 wiki 页面事件的通知。 |

### 禁用 Microsoft Teams 集成

禁用项目的 Microsoft Teams 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/microsoft-teams
```

### 获取 Microsoft Teams 集成设置

获取 Microsoft Teams 集成设置的项目。

```plaintext
GET /projects/:id/integrations/microsoft-teams
```

## Mattermost 通知

在 Mattermost 中接收事件通知。

### 创建/编辑 Mattermost 通知集成

为项目设置 Mattermost 通知集成。

```plaintext
PUT /projects/:id/integrations/mattermost
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `webhook` | string | true | Mattermost 的 webhook。例如，`http://mattermost_host/hooks/...`。 |
| `username` | string | false | 用户名。 |
| `channel` | string | false | 如果没有配置其他频道，则使用默认频道。 |
| `notify_only_broken_pipelines` | boolean | false | 发送流水线失败通知。 |
| `notify_only_default_branch` | boolean | false | 已删除：该参数已被 `branches_to_be_notified` 取代。 |
| `branches_to_be_notified` | string | false | 要发送通知的分支。有效选项是 `all`、`default`、`protected` 和 `default_and_protected`。默认值是 `default`。  |
| `push_events` | boolean | false | 启用推送事件的通知。 |
| `issues_events` | boolean | false | 启用议题事件的通知。 |
| `confidential_issues_events` | boolean | false | 启用私密议题事件的通知。 |
| `merge_requests_events` | boolean | false | 启用合并请求事件的通知。 |
| `tag_push_events` | boolean | false | 启用标签推送事件的通知。 |
| `note_events` | boolean | false | 启用备注事件的通知。 |
| `confidential_note_events` | boolean | false | 启用私密备注事件的通知。 |
| `pipeline_events` | boolean | false | 启用流水线事件的通知。 |
| `wiki_page_events` | boolean | false | 启用 wiki 页面事件的通知。 |
| `push_channel` | string | false | 接收推送事件通知的频道名称。 |
| `issue_channel` | string | false | 用于接收议题事件通知的频道的名称。 |
| `confidential_issue_channel` | string | false | 用于接收私密议题事件通知的频道的名称。 |
| `merge_request_channel` | string | false | 接收合并请求事件通知的频道名称。 |
| `note_channel` | string | false | 用于接收备注事件通知的频道的名称。 |
| `confidential_note_channel` | string | false | 用于接收私密备注事件通知的频道的名称。 |
| `tag_push_channel` | string | false | 用于接收标记推送事件通知的频道的名称。 |
| `pipeline_channel` | string | false | 用于接收流水线事件通知的频道的名称。 |
| `wiki_page_channel` | string | false | 用于接收 wiki 页面事件通知的频道的名称。 |

### 禁用 Mattermost notifications 集成

禁用项目的 Mattermost 通知集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/mattermost
```

### 获取 Mattermost notifications 集成设置

获取 Mattermost 通知集成设置的项目。 

```plaintext
GET /projects/:id/integrations/mattermost
```

## JetBrains TeamCity CI

一个持续集成和构建服务器。

### 创建/编辑 JetBrains TeamCity CI 集成

为项目设置JetBrains TeamCity CI集成。

> TeamCity 中的构建配置必须使用构建格式号 `%build.vcs.number%`。配置对所有分支的监控，以便合并请求构建。该设置在 VSC root 高级设置中。

```plaintext
PUT /projects/:id/integrations/teamcity
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `teamcity_url` | string | true | TeamCity 根 URL。例如, `https://teamcity.example.com`。 |
| `enable_ssl_verification` | boolean | false | 启用SSL验证。默认为true（启用）。 |
| `build_类型` | string | true | 构建配置 ID。 |
| `username` | string | true | 有权限触发手动构建的用户。 |
| `password` | string | true | 用户密码。 |
| `push_events` | boolean | false | 启用推送事件的通知。 |
| `merge_requests_events` | boolean | false | 启用合并请求事件的通知。 |

### 禁用 JetBrains TeamCity CI 集成

禁用 JetBrains TeamCity CI 集成的项目。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/teamcity
```

### 获取 JetBrains TeamCity CI 集成设置

获取 JetBrains TeamCity CI 集成设置的项目。

```plaintext
GET /projects/:id/integrations/teamcity
```

## Jenkins CI

一个持续集成和构建服务器。

### 创建/编辑 Jenkins CI 集成

为项目设置 Jenkins CI 集成。

```plaintext
PUT /projects/:id/integrations/jenkins
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `jenkins_url` | string | true | Jenkins URL 例如 `http://jenkins.example.com`。 |
| `enable_ssl_verification` | boolean | false | 启用SSL验证。默认为true（启用）。 |
| `project_name` | string | true | 一个 URL 友好的项目名称。例如：`my_project_name`。 |
| `username` | string | false | 用于与 Jenkins 服务器认证的用户名，如果服务器必须进行认证。 |
| `password` | string | false | 用于与 Jenkins 服务器认证的密码，如果服务器需要认证的话。 |
| `push_events` | boolean | false | 启用推送事件的通知。 |
| `merge_requests_events` | boolean | false | 启用合并请求事件的通知。 |
| `tag_push_events` | boolean | false | 启用标签推送事件的通知。 |

### 禁用 Jenkins CI 集成

禁用项目的 Jenkins CI 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/jenkins
```

### 获取 Jenkins CI 集成设置

获取项目的 Jenkins CI 集成设置。

```plaintext
GET /projects/:id/integrations/jenkins
```

## Jenkins CI (已弃用) 集成

一个持续集成和构建服务器。

NOTE:
在 13.0 版本中被移除。

### 创建/编辑 Jenkins CI (已弃用) 集成

为项目设置 Jenkins CI (已弃用) 集成。

```plaintext
PUT /projects/:id/integrations/jenkins-deprecated
```

参数：

- `project_url` (**必须**) - Jenkins 项目的URL，例如 `http://jenkins.example.com/job/my-project/`。
- `multiproject_enabled` (可选) - 多项目模式是在 Jenkins GitLab Hook 插件中配置的。
- `pass_unstable` (可选) - 不稳定的构建会被视为通过。

### 禁用 Jenkins CI (已弃用) 集成

禁用项目的Jenkins CI（已弃用）集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/jenkins-deprecated
```

### 获取 Jenkins CI (已弃用) 集成设置

获取 Jenkins CI (已弃用) 集成设置为一个项目。

```plaintext
GET /projects/:id/integrations/jenkins-deprecated
```

## MockCI

模拟一个外部 CI。<!--参见[`gitlab-org/gitlab-mock-ci-service`](https://gitlab.com/gitlab-org/gitlab-mock-ci-service)，了解配套的 MockCI 的例子。-->

这个集成只有在您的环境被设置为开发时才可用。

### 创建/编辑 MockCI 集成

为项目设置 MockCI 集成。

```plaintext
PUT /projects/:id/integrations/mock-ci
```

参数:

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `mock_service_url` | string | true | `http://localhost:4004`。 |
| `enable_ssl_verification` | boolean | false | 启用 SSL 验证。默认为true（启用）。 |

### 禁用 MockCI 集成

禁用项目的 MockCI 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/mock-ci
```

### 获取 MockCI 集成设置

获取项目的 MockCI 集成设置。

```plaintext
GET /projects/:id/integrations/mock-ci
```

## YouTrack

YouTrack 议题跟踪器。

### 创建/编辑 YouTrack 集成

为一个项目设置 YouTrack 集成。

```plaintext
PUT /projects/:id/integrations/youtrack
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `issues_url` | string | true | 议题 URL |
| `project_url` | string | true | 项目 URL |

### 禁用 YouTrack 集成

为项目禁用 YouTrack 集成。集成设置将被重置。

```plaintext
DELETE /projects/:id/integrations/youtrack
```

### 获取 YouTrack 集成设置

获取 YouTrack 集成设置的项目。

```plaintext
GET /projects/:id/integrations/youtrack
```
