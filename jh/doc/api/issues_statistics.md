---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 议题统计 API **(FREE)**

任何对[议题](../user/project/issues/index.md)统计 API 的请求都需要经过身份验证。

如果一个用户不是该项目的成员并且该项目为私有项目，则对该项目的 `GET` 请求结果将会是 `404` 状态码。

## 获取议题统计数据

获取认证用户可以访问的所有议题数。默认情况下，它只返回由当前用户创建的议题。如要获得所有议题，使用参数 `scope=all`。

```plaintext
GET /issues_statistics
GET /issues_statistics?labels=foo
GET /issues_statistics?labels=foo,bar
GET /issues_statistics?labels=foo,bar&state=opened
GET /issues_statistics?milestone=1.0.0
GET /issues_statistics?milestone=1.0.0&state=opened
GET /issues_statistics?iids[]=42&iids[]=43
GET /issues_statistics?author_id=5
GET /issues_statistics?assignee_id=5
GET /issues_statistics?my_reaction_emoji=star
GET /issues_statistics?search=foo&in=title
GET /issues_statistics?confidential=true
```

| 参数           | 类型             | 是否必需   | 描述                                                                                                                                         |
| ------------------- | ---------------- | ---------- | --------------------------------------------------------------------------------------------------------------------------------------------------- |
| `labels`            | string           | no         | 用逗号分割标记名称列表，议题必须包含所有标记才会被返回。 `None` 会列出所有不包含标记的议题。 `Any` 会列出所有至少含有一个标记的议题。 |
| `milestone`         | string           | no         | 里程碑标题。`None` 会列出所有不包含里程碑的议题。 `Any` 会列出所有被分配了里程碑的议题。|
| `scope`             | string           | no         | 返回给定范围的议题：`created_by_me`、`assigned_to_me` 或 `all`。默认为 `created_by_me`。 |
| `author_id`         | integer          | no         | 返回指定用户 `id` 创建的议题。与 `author_username` 相互排斥。可以与 `scope=all` 或 `scope=assigned_to_me` 结合使用。 |
| `author_username`   | string           | no         | 返回由指定 `username` 创建的议题。与 `author_id` 类似但是与 `author_id` 互斥。 |
| `assignee_id`       | integer          | no         | 返回指派给指定用户 `id` 的议题。与 `assignee_username` 相互排斥。`None` 返回未分配的议题。`Any` 返回所有有指派人的议题。 |
| `assignee_username` | string array     | no         | 返回指派给指定用户 `username` 的议题。与 `assignee_id` 类似但是与 `assignee_id` 相互排斥。在极狐GitLab 免费版中，`assignee_username` 数组应当只包含一个值，否则会返回一个参数无效的错误。 |
| `epic_id` **(PREMIUM)** | integer      | no         | 返回与给定史诗 ID 相关联的议题。`None` 返回未与史诗关联的议题。`Any` 返回所有有关联史诗的议题。 <!-- _([Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/46887) in GitLab 13.6)_ -->
| `my_reaction_emoji` | string           | no         | 返回验证的用户通过给定的`emoji` 反应的议题。`None` 返回没有给出反应的议题。`Any` 返回至少有一个给出反应的议题。 |
| `iids[]`            | integer array    | no         | 只返回具有给定 `iid` 的议题。|
| `search`            | string           | no         | 根据议题的 `title` 和 `description` 进行搜索。 |
| `in`                | string           | no         | 修改 `search` 参数的范围。`title`、`description`，或者用逗号连接为字符串。默认值为 `title,description`。             |
| `created_after`     | datetime         | no         | 返回在指定时间或之后创建的议题。预期为 ISO 8601 格式(`2019-03-15T08:00:00Z`)。 |
| `created_before`    | datetime         | no         | 返回在指定时间或之前创建的议题。预期为 ISO 8601 格式(`2019-03-15T08:00:00Z`)。 |
| `updated_after`     | datetime         | no         | 返回在给定时间或之后更新的议题。预期为 ISO 8601 格式(`2019-03-15T08:00:00Z`)。 |
| `updated_before`    | datetime         | no         | 返回在给定时间或之前更新的议题。预期为 ISO 8601 格式(`2019-03-15T08:00:00Z`)。 |
| `confidential`      | boolean          | no         | 过滤私密或公共议题。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/issues_statistics"
```

响应示例：

```json
{
  "statistics": {
    "counts": {
      "all": 20,
      "closed": 5,
      "opened": 15
    }
  }
}
```

## 获取群组议题统计数据

获取给定群组的议题数。

```plaintext
GET /groups/:id/issues_statistics
GET /groups/:id/issues_statistics?labels=foo
GET /groups/:id/issues_statistics?labels=foo,bar
GET /groups/:id/issues_statistics?labels=foo,bar&state=opened
GET /groups/:id/issues_statistics?milestone=1.0.0
GET /groups/:id/issues_statistics?milestone=1.0.0&state=opened
GET /groups/:id/issues_statistics?iids[]=42&iids[]=43
GET /groups/:id/issues_statistics?search=issue+title+or+description
GET /groups/:id/issues_statistics?author_id=5
GET /groups/:id/issues_statistics?assignee_id=5
GET /groups/:id/issues_statistics?my_reaction_emoji=star
GET /groups/:id/issues_statistics?confidential=true
```

| 参数           | 类型             | 是否必需   | 描述                                                                                                                   |
| ------------------- | ---------------- | ---------- | ----------------------------------------------------------------------------------------------------------------------------- |
| `id`                | integer/string   | yes        | 经过身份验证的用户拥有的群组 ID 或 [URL-encoded 路径](rest/index.md#namespaced-path-encoding)。 |
| `labels`            | string           | no         | 逗号分隔的标记名称列表，议题必须包含所有标记才能被返回。`None` 列出所有没有标记的议题。 `Any` 列出所有至少有一个标记的议题。 |
| `iids[]`            | integer array    | no         | 只返回具有给定 `iid` 的议题。 |
| `milestone`         | string           | no         | 里程碑标题。`None` 列出所有没有里程碑的议题。`Any` 会列出所有被分配了里程碑的议题。 |
| `scope`             | string           | no         | 返回给定范围的议题：`created_by_me`、`assigned_to_me` 或 `all`。 |
| `author_id`         | integer          | no         | 返回指定用户 `id` 创建的议题。与 `author_username` 相互排斥。可以与 `scope=all` 或 `scope=assigned_to_me` 结合使用。 |
| `author_username`   | string           | no         | 返回由指定 `username` 创建的议题。与 `author_id` 类似但是与 `author_id` 互斥。|
| `assignee_id`       | integer          | no         | 返回指派给指定用户 `id` 的议题。与 `assignee_username` 相互排斥。 `None` 返回未分配的议题。`Any` 返回所有有指派人的议题。 |
| `assignee_username` | string array     | no         | 返回指派给指定用户 `username` 的议题。与 `assignee_id` 类似但是与 `assignee_id` 相互排斥。在极狐GitLab 免费版中，`assignee_username` 数组应当只包含一个值，否则会返回一个参数无效的错误。 |
| `my_reaction_emoji` | string           | no         | 返回验证的用户通过给定的`emoji` 反应的议题。`None` 返回没有给出反应的议题。`Any` 返回至少有一个反应的议题。 |
| `search`            | string           | no         | 根据议题的 `title` 和 `description` 进行搜索。 |
| `created_after`     | datetime         | no         | 返回在指定时间或之后创建的议题。预期为 ISO 8601 格式(`2019-03-15T08:00:00Z`)。 |
| `created_before`    | datetime         | no         | 返回在指定时间或之前创建的议题。预期为 ISO 8601 格式(`2019-03-15T08:00:00Z`)。 |
| `updated_after`     | datetime         | no         | 返回在给定时间或之后更新的议题。预期为 ISO 8601 格式(`2019-03-15T08:00:00Z`)。 |
| `updated_before`    | datetime         | no         | 返回在给定时间或之前更新的议题。预期为 ISO 8601 格式(`2019-03-15T08:00:00Z`)。 |
| `confidential`      | boolean          | no         | 过滤私密或公共议题。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/4/issues_statistics"
```

响应示例:

```json
{
  "statistics": {
    "counts": {
      "all": 20,
      "closed": 5,
      "opened": 15
    }
  }
}
```

## 获取项目议题统计数据

获取给定项目的议题数。

```plaintext
GET /projects/:id/issues_statistics
GET /projects/:id/issues_statistics?labels=foo
GET /projects/:id/issues_statistics?labels=foo,bar
GET /projects/:id/issues_statistics?labels=foo,bar&state=opened
GET /projects/:id/issues_statistics?milestone=1.0.0
GET /projects/:id/issues_statistics?milestone=1.0.0&state=opened
GET /projects/:id/issues_statistics?iids[]=42&iids[]=43
GET /projects/:id/issues_statistics?search=issue+title+or+description
GET /projects/:id/issues_statistics?author_id=5
GET /projects/:id/issues_statistics?assignee_id=5
GET /projects/:id/issues_statistics?my_reaction_emoji=star
GET /projects/:id/issues_statistics?confidential=true
```

| 参数           | 类型             | 是否必需   | 描述                                                                                                                                         |
| ------------------- | ---------------- | ---------- | ----------------------------------------------------------------------------------------------------------------------------- |
| `id`                | integer/string   | yes        | 经过身份验证的用户拥有的项目ID 或 [URL-encoded 路径](rest/index.md#namespaced-path-encoding)。 |
| `iids[]`            | integer array    | no         | 只返回具有给定 `iid` 的议题。 |
| `labels`            | string           | no         | 逗号分隔的标记名称列表，议题必须包含所有标记才能被返回。`None` 列出所有没有标记的议题。 `Any` 列出所有至少有一个标记的议题。|
| `milestone`         | string           | no         | 里程碑标题。 `None` 列出所有没有里程碑的议题。 `Any` 会列出所有被分配了里程碑的议题。|
| `scope`             | string           | no         | 返回给定范围的议题：`created_by_me`、`assigned_to_me` 或 `all`。 |
| `author_id`         | integer          | no         | 返回指定用户 `id` 创建的问题。 与 `author_username` 相互排斥。可以与 `scope=all` 或 `scope=assigned_to_me` 结合使用。|
| `author_username`   | string           | no         | 返回由指定 `username` 创建的问题。 与 `author_id` 类似但是与 `author_id` 互斥。|
| `assignee_id`       | integer          | no         | 返回指派给指定用户 `id` 的议题。 与 `assignee_username` 相互排斥。 `None` 返回未分配的议题。`Any` 返回所有有指派人的议题。 |
| `assignee_username` | string array     | no         | 返回指派给指定用户 `username` 的议题。 与 `assignee_id` 类似但是与 `assignee_id` 相互排斥。在极狐GitLab 免费版中，`assignee_username` 数组应当只包含一个值，否则会返回一个参数无效的错误。 |
| `my_reaction_emoji` | string           | no         | 返回验证的用户通过给定的`emoji` 反应的议题。 `None` 返回没有给出反应的议题。`Any` 返回至少有一个反应的议题。 |
| `search`            | string           | no         | 根据议题的 `title` 和 `description` 进行搜索。 |
| `created_after`     | datetime         | no         | 返回在指定时间或之后创建的议题。预期为 ISO 8601 格式(`2019-03-15T08:00:00Z`)。 |
| `created_before`    | datetime         | no         | 返回在指定时间或之前创建的议题。预期为 ISO 8601 格式(`2019-03-15T08:00:00Z`)。 |
| `updated_after`     | datetime         | no         | 返回在给定时间或之后更新的议题。预期为 ISO 8601 格式(`2019-03-15T08:00:00Z`)。 |
| `updated_before`    | datetime         | no         | 返回在给定时间或之前更新的议题。预期为 ISO 8601 格式(`2019-03-15T08:00:00Z`)。 |
| `confidential`      | boolean          | no         | 过滤私密或公共议题。|

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/4/issues_statistics"
```

响应示例:

```json
{
  "statistics": {
    "counts": {
      "all": 20,
      "closed": 5,
      "opened": 15
    }
  }
}
```
