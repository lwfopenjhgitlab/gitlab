---
stage: Monitor
group: Observability
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 错误跟踪设置 API **(FREE)**

> 引入于 12.7 版本。

## 错误跟踪项目设置

项目设置 API 允许您为一个项目设置检索[错误跟踪](../operations/error_tracking.md)。仅适用于具备项目维护者权限的用户。

### 获取错误跟踪设置

```plaintext
GET /projects/:id/error_tracking/settings
```

| 参数 | 类型    | 是否必需 | 描述           |
| --------- | ------- | -------- | --------------------- |
| `id`      | integer | yes      | 经过身份验证的用户拥有的项目 ID 或[ URL 编码的路径](rest/index.md#namespaced-path-encoding) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/error_tracking/settings"
```

响应示例：

```json
{
  "active": true,
  "project_name": "sample sentry project",
  "sentry_external_url": "https://sentry.io/myawesomeproject/project",
  "api_url": "https://sentry.io/api/0/projects/myawesomeproject/project",
  "integrated": false
}
```

### 创建错误跟踪设置

> - 引入于极狐GitLab 15.10。

该 API 允许您为项目创建错误跟踪设置。仅适用于具有项目维护者角色的用户。

NOTE:
此 API 仅在与[集成错误跟踪](../operations/error_tracking.md#integrated-error-tracking)一起使用时可用。

```plaintext
PUT /projects/:id/error_tracking/settings
```

支持的参数：

| 参数           | 类型      | 是否必需 | 描述                                                                      |
|--------------|---------|------|-------------------------------------------------------------------------|
| `id`         | integer | yes  | 经过身份验证的用户拥有的项目 ID 或 [UR L编码的路径](rest/index.md#namespaced-path-encoding) |
| `active`     | boolean | yes  | 传递 `true` 以启用错误跟踪设置配置或传递 `false` 以禁用                                    |
| `integrated` | boolean | yes  | 传递 `true` 以启用集成错误跟踪后端。可用于 14.2 及更高版本                                    |

请求示例：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/error_tracking/settings?active=true&integrated=true"
```

响应示例：

```json
{
  "active": true,
  "project_name": null,
  "sentry_external_url": null,
  "api_url": null,
  "integrated": true
}
```

### 启用或禁用错误跟踪项目设置

API 允许您为一个项目启用或禁用错误追踪设置。仅适用于具备项目维护者权限的用户。

```plaintext
PATCH /projects/:id/error_tracking/settings
```

| 参数    | 类型    | 是否必需 | 描述           |
| ------------ | ------- | -------- | --------------------- |
| `id`         | integer | yes      | 经过身份验证的用户拥有的项目 ID 或[ URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `active`     | boolean | yes      | 通过 `true` 来启用已配置的错误追踪设置或者通过 `false` 禁用 |
| `integrated` | boolean | no       | 通过 `true` 来启用已配置的错误追踪后端，可用于 14.2 及以上版本 |

```shell
curl --request PATCH --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/error_tracking/settings?active=true"
```

响应示例：

```json
{
  "active": true,
  "project_name": "sample sentry project",
  "sentry_external_url": "https://sentry.io/myawesomeproject/project",
  "api_url": "https://sentry.io/api/0/projects/myawesomeproject/project",
  "integrated": false
}
```

## 客户端密钥错误追踪

> 引入于 14.3 版本。

集成错误追踪功能。仅适用于具备项目维护者权限的用户。

### 列出项目客户端密钥

```plaintext
GET /projects/:id/error_tracking/client_keys
```


| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id` | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[ URL 编码的路径](rest/index.md#namespaced-path-encoding)） |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/error_tracking/client_keys"
```

示例：

```json
[
  {
    "id": 1,
    "active": true,
    "public_key": "glet_aa77551d849c083f76d0bc545ed053a3",
    "sentry_dsn": "https://glet_aa77551d849c083f76d0bc545ed053a3@example.com/errortracking/api/v1/projects/5"
  },
  {
    "id": 3,
    "active": true,
    "public_key": "glet_0ff98b1d849c083f76d0bc545ed053a3",
    "sentry_dsn": "https://glet_aa77551d849c083f76d0bc545ed053a3@example.com/errortracking/api/v1/projects/5"
  }
]
```

### 创建一个客户端密钥


为项目创建一个新的客户端密钥。公钥参数自动生成。

```plaintext
POST /projects/:id/error_tracking/client_keys
```

| 参数  | 类型 | 是否必需 | 描述 |
| ---------  | ---- | -------- | ----------- |
| `id`       | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[ URL 编码的路径](rest/index.md#namespaced-path-encoding) |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --header "Content-Type: application/json" \
     "https://gitlab.example.com/api/v4/projects/5/error_tracking/client_keys"
```

示例：

```json
{
  "id": 3,
  "active": true,
  "public_key": "glet_0ff98b1d849c083f76d0bc545ed053a3",
  "sentry_dsn": "https://glet_aa77551d849c083f76d0bc545ed053a3@example.com/errortracking/api/v1/projects/5"
}
```

### 删除一个客户端密钥

从项目中移除一个客户端密钥。

```plaintext
DELETE /projects/:id/error_tracking/client_keys/:key_id
```

| 参数 | 类型 | 是否必需 | 描述                                                                         |
| --------- | ---- | -------- |----------------------------------------------------------------------------|
| `id`      | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[ URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `key_id`  | integer | yes | 客户端密钥 ID                                                                   |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/error_tracking/client_keys/13"
```
