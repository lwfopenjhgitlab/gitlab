---
stage: Govern
group: Compliance
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
type: reference, api
---

# 外部状态检查 API **(ULTIMATE)**

> - 引入于极狐GitLab 14.0，在 `:ff_external_status_checks` 功能标志后禁用。
> - 功能标志移除于极狐GitLab 14.1。

## 获取项目外部状态检查服务

您可以使用以下端点请求有关项目的外部状态检查服务的信息：

```plaintext
GET /projects/:id/external_status_checks
```

**参数：**

| 参数   | 类型      | 是否必需 | 描述                 |
|------|---------|------|--------------------|
| `id` | integer | yes  | 项目 ID|

```json
[
  {
    "id": 1,
    "name": "Compliance Tool",
    "project_id": 6,
    "external_url": "https://gitlab.com/example/compliance-tool",
    "protected_branches": [
      {
        "id": 14,
        "project_id": 6,
        "name": "master",
        "created_at": "2020-10-12T14:04:50.787Z",
        "updated_at": "2020-10-12T14:04:50.787Z",
        "code_owner_approval_required": false
      }
    ]
  }
]
```

## 创建外部状态检查服务

您可以使用以下端点为项目创建新的外部状态检查服务：

```plaintext
POST /projects/:id/external_status_checks
```

WARNING:
外部状态检查将有关所有适用合并请求的信息发送到定义的外部服务，其中包括私密合并请求。

| 参数                     | 类型               | 是否必需 | 描述                                             |
|------------------------|------------------|------|------------------------------------------------|
| `id`                   | integer          | yes  | 项目 ID                                          |
| `name`                 | string           | yes  | 展示外部状态检查服务名称                                   |
| `external_url`         | string           | yes  | 外部状态检查服务 URL |
| `protected_branch_ids` | `array<Integer>` | no   | 受保护分支的 ID，用于确定规则的范围 |

## 更新外部状态检查服务

您可以使用以下端点为项目更新现有外部状态检查：

```plaintext
PUT /projects/:id/external_status_checks/:check_id
```

| 参数                     | 类型               | 是否必需 | 描述                                             |
|------------------------|------------------|------|------------------------------------------------|
| `id`                   | integer          | yes  | 项目 ID                                          |
| `check_id`             | integer          | yes  | 外部状态检查服务 ID                                    |
| `name`                 | string           | no   | 展示外部状态检查服务名称                                   |
| `external_url`         | string           | no   | 外部状态检查服务 URL  |
| `protected_branch_ids` | `array<Integer>` | no   | 受保护分支的 ID，用于确定规则的范围 |

## 删除外部状态检查服务

您可以使用以下端点删除项目的外部状态检查：

```plaintext
DELETE /projects/:id/external_status_checks/:check_id
```

| 参数         | 类型      | 是否必需 | 描述                                              |
|------------|---------|------|-------------------------------------------------|
| `check_id` | integer | yes  | 外部状态检查服务 ID |
| `id`       | integer | yes  | 项目 ID                                           |


## 列出合并请求的状态检查

对于单个合并请求，列出适用于它的外部状态检查服务及其状态。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/status_checks
```

**参数：**

| 参数                  | 类型      | 是否必需 | 描述                          |
|---------------------|---------|------|-----------------------------|
| `id`                | integer | yes  | 项目 ID                       |
| `merge_request_iid` | integer | yes  | 合并请求 IID  |

```json
[
  {
    "id": 2,
    "name": "Service 1",
    "external_url": "https://gitlab.com/test-endpoint",
    "status": "passed"
  },
  {
    "id": 1,
    "name": "Service 2",
    "external_url": "https://gitlab.com/test-endpoint-2",
    "status": "pending"
  }
]
```

## 设置外部状态检查的状态

> - 引入于极狐GitLab 14.9，`passed` 状态表示传递外部状态检查。带有 `status_checks_add_status_field` [功能标志](../administration/feature_flags.md)。默认禁用。
> - 引入于极狐GitLab 14.9，`failed` 状态表示外部状态检查失败。带有 `status_checks_add_status_field` [功能标志](../administration/feature_flags.md)。默认禁用。
> - 传递检查的 `pass` 状态已废弃于极狐GitLab 14.9，用 `passed` 代替。
> - `failed` 和 `passed` 默认启用于极狐GitLab 15.0 并移除了功能标志。

对于单个合并请求，使用 API 通知极狐GitLab 合并请求已通过外部服务的检查。
要设置外部检查的状态，使用的个人访问令牌必须属于在合并请求的目标项目中至少具有 Developer 角色的用户。

以任何有权批准合并请求本身的用户身份执行此 API 调用。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/status_check_responses
```

**参数：**

| 参数                         | 类型      | 是否必需 | 描述                            |
|----------------------------|---------|------|-------------------------------|
| `id`                       | integer | yes  | 项目 ID                         |
| `merge_request_iid`        | integer | yes  | 合并请求 IID                      |
| `sha`                      | string  | yes  | 源分支 `HEAD` 的 SHA              |
| `external_status_check_id` | integer | yes  | 外部状态检查 ID                     |
| `status`                   | string  | no   | `passed`：传递状态；`failed`：传递状态失败 |

NOTE:
`sha` 必须是合并请求源分支 `HEAD` 的 SHA。

## 重试合并请求的失败状态检查

> 引入于极狐GitLab 15.7。

对于单个合并请求，重试指定的失败外部状态检查。
尽管合并请求没有改变，这个端点也会重新发送合并请求的当前状态到定义的外部服务。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/status_checks/:external_status_check_id/retry
```

**参数：**

| 参数                         | 类型      | 是否必需 | 描述                                             |
|----------------------------|---------|------|------------------------------------------------|
| `id`                       | integer | yes  | 项目 ID                              |
| `merge_request_iid`        | integer | yes  | 合并请求 IID                   |
| `external_status_check_id` | integer | yes  | 失败的外部状态检查 ID  |

## 响应

假设成功状态码为 202。

```json
{
    "message": "202 Accepted"
}
```

如果状态检查已经通过，状态码为 422。

```json
{
    "message": "External status check must be failed"
}
```

## 发送到外部服务的示例负载

```json
{
  "object_kind": "merge_request",
  "event_type": "merge_request",
  "user": {
    "id": 1,
    "name": "Administrator",
    "username": "root",
    "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "email": "[REDACTED]"
  },
  "project": {
    "id": 6,
    "name": "Flight",
    "description": "Ipsa minima est consequuntur quisquam.",
    "web_url": "http://example.com/flightjs/Flight",
    "avatar_url": null,
    "git_ssh_url": "ssh://example.com/flightjs/Flight.git",
    "git_http_url": "http://example.com/flightjs/Flight.git",
    "namespace": "Flightjs",
    "visibility_level": 20,
    "path_with_namespace": "flightjs/Flight",
    "default_branch": "master",
    "ci_config_path": null,
    "homepage": "http://example.com/flightjs/Flight",
    "url": "ssh://example.com/flightjs/Flight.git",
    "ssh_url": "ssh://example.com/flightjs/Flight.git",
    "http_url": "http://example.com/flightjs/Flight.git"
  },
  "object_attributes": {
    "assignee_id": null,
    "author_id": 1,
    "created_at": "2022-12-07 07:53:43 UTC",
    "description": "",
    "head_pipeline_id": 558,
    "id": 144,
    "iid": 4,
    "last_edited_at": null,
    "last_edited_by_id": null,
    "merge_commit_sha": null,
    "merge_error": null,
    "merge_params": {
      "force_remove_source_branch": "1"
    },
    "merge_status": "can_be_merged",
    "merge_user_id": null,
    "merge_when_pipeline_succeeds": false,
    "milestone_id": null,
    "source_branch": "root-master-patch-30152",
    "source_project_id": 6,
    "state_id": 1,
    "target_branch": "master",
    "target_project_id": 6,
    "time_estimate": 0,
    "title": "Update README.md",
    "updated_at": "2022-12-07 07:53:43 UTC",
    "updated_by_id": null,
    "url": "http://example.com/flightjs/Flight/-/merge_requests/4",
    "source": {
      "id": 6,
      "name": "Flight",
      "description": "Ipsa minima est consequuntur quisquam.",
      "web_url": "http://example.com/flightjs/Flight",
      "avatar_url": null,
      "git_ssh_url": "ssh://example.com/flightjs/Flight.git",
      "git_http_url": "http://example.com/flightjs/Flight.git",
      "namespace": "Flightjs",
      "visibility_level": 20,
      "path_with_namespace": "flightjs/Flight",
      "default_branch": "master",
      "ci_config_path": null,
      "homepage": "http://example.com/flightjs/Flight",
      "url": "ssh://example.com/flightjs/Flight.git",
      "ssh_url": "ssh://example.com/flightjs/Flight.git",
      "http_url": "http://example.com/flightjs/Flight.git"
    },
    "target": {
      "id": 6,
      "name": "Flight",
      "description": "Ipsa minima est consequuntur quisquam.",
      "web_url": "http://example.com/flightjs/Flight",
      "avatar_url": null,
      "git_ssh_url": "ssh://example.com/flightjs/Flight.git",
      "git_http_url": "http://example.com/flightjs/Flight.git",
      "namespace": "Flightjs",
      "visibility_level": 20,
      "path_with_namespace": "flightjs/Flight",
      "default_branch": "master",
      "ci_config_path": null,
      "homepage": "http://example.com/flightjs/Flight",
      "url": "ssh://example.com/flightjs/Flight.git",
      "ssh_url": "ssh://example.com/flightjs/Flight.git",
      "http_url": "http://example.com/flightjs/Flight.git"
    },
    "last_commit": {
      "id": "141be9714669a4c1ccaa013c6a7f3e462ff2a40f",
      "message": "Update README.md",
      "title": "Update README.md",
      "timestamp": "2022-12-07T07:52:11+00:00",
      "url": "http://example.com/flightjs/Flight/-/commit/141be9714669a4c1ccaa013c6a7f3e462ff2a40f",
      "author": {
        "name": "Administrator",
        "email": "admin@example.com"
      }
    },
    "work_in_progress": false,
    "total_time_spent": 0,
    "time_change": 0,
    "human_total_time_spent": null,
    "human_time_change": null,
    "human_time_estimate": null,
    "assignee_ids": [
    ],
    "reviewer_ids": [
    ],
    "labels": [
    ],
    "state": "opened",
    "blocking_discussions_resolved": true,
    "first_contribution": false,
    "detailed_merge_status": "mergeable"
  },
  "labels": [
  ],
  "changes": {
  },
  "repository": {
    "name": "Flight",
    "url": "ssh://example.com/flightjs/Flight.git",
    "description": "Ipsa minima est consequuntur quisquam.",
    "homepage": "http://example.com/flightjs/Flight"
  },
  "external_approval_rule": {
    "id": 1,
    "name": "QA",
    "external_url": "https://example.com/"
  }
}
```


## 相关主题

- [外部状态检查](../user/project/merge_requests/status_checks.md)
