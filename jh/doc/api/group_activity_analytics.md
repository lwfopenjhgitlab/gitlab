---
stage: Plan
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组活动分析 API **(PREMIUM)**

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/26460) -->引入于极狐GitLab 12.9。

## 获取群组近期创建议题的数量

```plaintext
GET /analytics/group_activity/issues_count
```

参数：

| 参数           | 类型     | 是否必需 | 描述   |
|--------------|--------|------|------|
| `group_path` | string | yes  | 群组路径 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/analytics/group_activity/issues_count?group_path=gitlab-org"
```

响应示例：

```json
{ "issues_count": 10 }
```

## 获取群组近期创建的合并请求的数量

```plaintext
GET /analytics/group_activity/merge_requests_count
```

参数：

| 参数           | 类型     | 是否必需 | 描述   |
|--------------|--------|------|------|
| `group_path` | string | yes  | 群组路径 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/analytics/group_activity/merge_requests_count?group_path=gitlab-org"
```

响应示例：

```json
{ "merge_requests_count": 10 }
```

## 获取近期加入群组的成员的数量

```plaintext
GET /analytics/group_activity/new_members_count
```

参数：

| 参数           | 类型     | 是否必需 | 描述   |
|--------------|--------|------|------|
| `group_path` | string | yes  | 群组路径 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/analytics/group_activity/new_members_count?group_path=gitlab-org"
```

响应示例：

```json
{ "new_members_count": 10 }
```
