---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 命名空间 API **(FREE)**

用户名和群组名称都在一个叫做[命名空间](../user/namespace/index.md)的特殊类别下。

您可能需要查看：

- [用户](users.md)
- [群组](groups.md)

此 API 使用[分页](rest/index.md#pagination)。

## 列出命名空间

获取经过身份验证的用户的命名空间列表。如果用户是管理员，将显示极狐GitLab 实例中所有命名空间的列表。

```plaintext
GET /namespaces
GET /namespaces?search=foobar
GET /namespaces?owned_only=true
```

| 参数           | 类型      | 是否必需 | 描述                                                 |
|--------------|---------|------|----------------------------------------------------|
| `search`     | string  | no   | 根据搜索条件返回用户有权查看的命名空间列表                              |
| `owned_only` | boolean | no   | 在极狐GitLab 14.2 及更高版本中，仅返回拥有的命名空间列表 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/namespaces"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "user1",
    "path": "user1",
    "kind": "user",
    "full_path": "user1",
    "parent_id": null,
    "avatar_url": "https://secure.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/user1",
    "billable_members_count": 1,
    "plan": "default",
    "trial_ends_on": null,
    "trial": false,
    "root_repository_size": 100
  },
  {
    "id": 2,
    "name": "group1",
    "path": "group1",
    "kind": "group",
    "full_path": "group1",
    "parent_id": null,
    "avatar_url": null,
    "web_url": "https://gitlab.example.com/groups/group1",
    "members_count_with_descendants": 2,
    "billable_members_count": 2,
    "plan": "default",
    "trial_ends_on": null,
    "trial": false,
    "root_repository_size": 100
  },
  {
    "id": 3,
    "name": "bar",
    "path": "bar",
    "kind": "group",
    "full_path": "foo/bar",
    "parent_id": 9,
    "avatar_url": null,
    "web_url": "https://gitlab.example.com/groups/foo/bar",
    "members_count_with_descendants": 5,
    "billable_members_count": 5,
    "plan": "default",
    "trial_ends_on": null,
    "trial": false,
    "root_repository_size": 100
  }
]
```

拥有者也可以看见和命名空间相关联的 `plan` 属性：

```json
[
  {
    "id": 1,
    "name": "user1",
    "plan": "silver",
    ...
  }
]
```

JihuLab.com 上的用户还可以看到 `max_seats_used`、`seats_in_use` 和 `max_seats_used_changed_at` 参数。
`max_seats_used` 是该群组拥有的最大用户数。`seats_in_use` 是当前正在使用的许可证席位数。`max_seats_used_changed_at` 展示 `max_seats_used` 值更新的日期。所有值都每天更新一次。

`max_seats_used` 和 `seats_in_use` 只对付费计划中的命名空间是非零的。

```json
[
  {
    "id": 1,
    "name": "user1",
    "billable_members_count": 2,
    "max_seats_used": 3,
    "max_seats_used_changed_at":"2023-02-13T12:00:02.000Z",
    "seats_in_use": 2,
    ...
  }
]
```

NOTE:
只有群组拥有者会以 `members_count_with_descendants`、`root_repository_size` 和 `plan` 呈现。

## 通过 ID 获取命名空间

通过 ID 获取命名空间。

```plaintext
GET /namespaces/:id
```

| 参数   | 类型             | 是否必需 | 描述                                                      |
|------|----------------|------|---------------------------------------------------------|
| `id` | integer/string | yes  | ID 或 [URL 编码的命名空间路径](rest/index.md#namespaced-path-encoding) |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/namespaces/2"
```

响应示例：

```json
{
  "id": 2,
  "name": "group1",
  "path": "group1",
  "kind": "group",
  "full_path": "group1",
  "parent_id": null,
  "avatar_url": null,
  "web_url": "https://gitlab.example.com/groups/group1",
  "members_count_with_descendants": 2,
  "billable_members_count": 2,
  "max_seats_used": 0,
  "seats_in_use": 0,
  "plan": "default",
  "trial_ends_on": null,
  "trial": false,
  "root_repository_size": 100
}
```

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/namespaces/group1"
```

响应示例：

```json
{
  "id": 2,
  "name": "group1",
  "path": "group1",
  "kind": "group",
  "full_path": "group1",
  "parent_id": null,
  "avatar_url": null,
  "web_url": "https://gitlab.example.com/groups/group1",
  "members_count_with_descendants": 2,
  "billable_members_count": 2,
  "max_seats_used": 0,
  "seats_in_use": 0,
  "plan": "default",
  "trial_ends_on": null,
  "trial": false,
  "root_repository_size": 100
}
```

## 获取命名空间的存在情况

通过路径获取命名空间的存在情况。建议一个不存在的新命名空间路径。

```plaintext
GET /namespaces/:namespace/exists
```

| 参数          | 类型      | 是否必需 | 描述                                                                     |
|-------------|---------|------|------------------------------------------------------------------------|
| `namespace` | string  | yes  | 命名空间的路径                                                                |
| `parent_id` | integer | no   | 父命名空间的 ID。如果未指定 ID，仅考虑顶级命名空间 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/namespaces/my-group/exists?parent_id=1"
```

响应示例：

```json
{
    "exists": true,
    "suggests": [
        "my-group1"
    ]
}
```
