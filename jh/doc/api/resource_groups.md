---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 资源组 API **(FREE)**

您可以阅读更多关于[使用资源组控制作业并发的内容](../ci/resource_groups/index.md)。

## 获取一个项目内所有的资源组

```plaintext
GET /projects/:id/resource_groups
```

| 参数 | 类型 | 是否必需 | 描述                                                                    |
|-----------|---------|----------|-----------------------------------------------------------------------|
| `id`      | integer/string     | yes      | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespacied-path-encoding) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/resource_groups"
```

响应示例：

```json
[
  {
    "id": 3,
    "key": "production",
    "process_mode": "unordered",
    "created_at": "2021-09-01T08:04:59.650Z",
    "updated_at": "2021-09-01T08:04:59.650Z"
  }
]
```

## 获取特定的资源组

```plaintext
GET /projects/:id/resource_groups/:key
```

| 参数 | 类型 | 是否必需 | 描述                                                                    |
|-----------|---------|----------|-----------------------------------------------------------------------|
| `id`      | integer/string     | yes      | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespacied-path-encoding) |
| `key`     | string  | yes      | 资源组的键值                                                                |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/resource_groups/production"
```

响应示例：

```json
{
  "id": 3,
  "key": "production",
  "process_mode": "unordered",
  "created_at": "2021-09-01T08:04:59.650Z",
  "updated_at": "2021-09-01T08:04:59.650Z"
}
```

## 列出特定资源组的未来作业

```plaintext
GET /projects/:id/resource_groups/:key/upcoming_jobs
```

| 参数   | 类型             | 是否必需 | 描述                                                            |
|------|----------------|------|---------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `key` | string         | yes  | 资源组的 key                                                      |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/50/resource_groups/production/upcoming_jobs"
```

响应示例：

```json
[
  {
    "id": 1154,
    "status": "waiting_for_resource",
    "stage": "deploy",
    "name": "deploy_to_production",
    "ref": "main",
    "tag": false,
    "coverage": null,
    "allow_failure": false,
    "created_at": "2022-09-28T09:57:04.590Z",
    "started_at": null,
    "finished_at": null,
    "duration": null,
    "queued_duration": null,
    "user": {
      "id": 1,
      "username": "john_smith",
      "name": "John Smith",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/2d691a4d0427ca8db6efc3924a6408ba?s=80\u0026d=identicon",
      "web_url": "http://localhost:3000/john_smith",
      "created_at": "2022-05-27T19:19:17.526Z",
      "bio": "",
      "location": null,
      "public_email": null,
      "skype": "",
      "linkedin": "",
      "twitter": "",
      "website_url": "",
      "organization": null,
      "job_title": "",
      "pronouns": null,
      "bot": false,
      "work_information": null,
      "followers": 0,
      "following": 0,
      "local_time": null
    },
    "commit": {
      "id": "3177f39064891bbbf5124b27850c339da331f02f",
      "short_id": "3177f390",
      "created_at": "2022-09-27T17:55:31.000+02:00",
      "parent_ids": [
        "18059e45a16eaaeaddf6fc0daf061481549a89df"
      ],
      "title": "List upcoming jobs",
      "message": "List upcoming jobs",
      "author_name": "Example User",
      "author_email": "user@example.com",
      "authored_date": "2022-09-27T17:55:31.000+02:00",
      "committer_name": "Example User",
      "committer_email": "user@example.com",
      "committed_date": "2022-09-27T17:55:31.000+02:00",
      "trailers": {},
      "web_url": "https://gitlab.example.com/test/gitlab/-/commit/3177f39064891bbbf5124b27850c339da331f02f"
    },
    "pipeline": {
      "id": 274,
      "iid": 9,
      "project_id": 50,
      "sha": "3177f39064891bbbf5124b27850c339da331f02f",
      "ref": "main",
      "status": "waiting_for_resource",
      "source": "web",
      "created_at": "2022-09-28T09:57:04.538Z",
      "updated_at": "2022-09-28T09:57:13.537Z",
      "web_url": "https://gitlab.example.com/test/gitlab/-/pipelines/274"
    },
    "web_url": "https://gitlab.example.com/test/gitlab/-/jobs/1154",
    "project": {
      "ci_job_token_scope_enabled": false
    }
  }
]
```

<a id="edit-an-existing-resource-group"></a>

## 编辑现有的资源组

更新现有资源组的属性。

如果资源组成功更新，则返回的状态码为 `200`。如果出现错误，则返回的状态代码为 `400`。


```plaintext
PUT /projects/:id/resource_groups/:key
```

| 参数 | 类型 | 是否必需 | 描述                                                                                                                  |
| --------------- | ------- | --------------------------------- |---------------------------------------------------------------------------------------------------------------------|
| `id`            | integer/string | yes                        | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespacied-path-encoding)                                                 |
| `key`           | string  | yes                               | 资源组的键值                                                                                                              |
| `process_mode`  | string  | no                                | 资源组的进程模式：`unordered`、 `oldest_first` 或 `newest_first`。请阅读[进程模式](../ci/resource_groups/index.md#process-modes)获取更多信息 |

```shell
curl --request PUT --data "process_mode=oldest_first" \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/resource_groups/production"
```

响应示例：

```json
{
  "id": 3,
  "key": "production",
  "process_mode": "oldest_first",
  "created_at": "2021-09-01T08:04:59.650Z",
  "updated_at": "2021-09-01T08:13:38.679Z"
}
```
