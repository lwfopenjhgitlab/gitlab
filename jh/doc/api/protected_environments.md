---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: concepts, howto
---

# 受保护环境 API **(PREMIUM)**

> 引入于极狐GitLab 12.8。

## 有效访问级别

`ProtectedEnvironments::DeployAccessLevel::ALLOWED_ACCESS_LEVELS` 方法中定义了访问级别。当前级别为：

```plaintext
30 => Developer access
40 => Maintainer access
60 => Admin access
```

<a id="group-inheritance-types"></a>

## 群组继承类型

群组继承允许部署访问级别和访问规则以考虑继承的群组成员身份。群组继承类型由 `ProtectedEnvironments::Authorizable::GROUP_INHERITANCE_TYPE` 定义。
可识别以下类型：

```plaintext
0 => Direct group membership only (default)
1 => All inherited groups
```

## 列出受保护环境

从项目中获取受保护环境的列表：

```plaintext
GET /projects/:id/protected_environments
```

| 参数   | 类型             | 是否必需 | 描述                                                            |
|------|----------------|------|---------------------------------------------------------------|
| `id` | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/protected_environments/"
```

响应示例：

```json
[
  {
    "name":"production",
    "deploy_access_levels":[
      {
        "id": 12,
        "access_level":40,
        "access_level_description":"Maintainers",
        "user_id":null,
        "group_id":null,
        "group_inheritance_type": 0
      }
    ],
    "required_approval_count": 0
  }
]
```

## 获取单个受保护环境

获取单个受保护环境：

```plaintext
GET /projects/:id/protected_environments/:name
```

| 参数     | 类型             | 是否必需 | 描述                                                            |
|--------|----------------|------|---------------------------------------------------------------|
| `id`   | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `name` | string         | yes  | 受保护环境的名称                |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/protected_environments/production"
```

响应示例：

```json
{
  "name":"production",
  "deploy_access_levels":[
    {
      "id": 12,
      "access_level": 40,
      "access_level_description": "Maintainers",
      "user_id": null,
      "group_id": null,
      "group_inheritance_type": 0
    }
  ],
  "required_approval_count": 0
}
```

## 保护单个环境

保护单个环境：

```plaintext
POST /projects/:id/protected_environments
```

| 参数                        | 类型             | 是否必需 | 描述                                                                                                        |
|---------------------------|----------------|------|-----------------------------------------------------------------------------------------------------------|
| `id`                      | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)                                             |
| `name`                    | string         | yes  | 环境名称                                                                                                      |
| `deploy_access_levels`    | array          | yes  | 允许部署的访问级别阵列，每个级别由哈希值描述                                                                                    |
| `required_approval_count` | integer        | no   | 部署到此环境所需的批准数量                                                                                             |
| `approval_rules`          | array          | no   | 允许批准的访问级别阵列，每个级别都由哈希值描述。详情请参见[多个批准规则](../ci/environments/deployment_approvals.md#multiple-approval-rules) |

`deploy_access_levels` 和 `approval_rules` 阵列中的元素应该是 `user_id`、`group_id` 或
`access_level`，并采用`{user_id: integer}`、`{group_id: integer}` 或
`{access_level: integer}` 的形式。（可选）您可以将每个的 `group_inheritance_type` 指定为[有效群组继承类型](#group-inheritance-types) 之一。

每个用户必须可以访问项目并且每个群组必须[分享该项目](../user/project/members/share_project_with_groups.md)。

```shell
curl --header 'Content-Type: application/json' --request POST \
     --data '{"name": "production", "deploy_access_levels": [{"group_id": 9899826}], "approval_rules": [{"group_id": 134}, {"group_id": 135, "required_approvals": 2}]}' \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/22034114/protected_environments"
```

响应示例：

```json
{
   "name": "production",
   "deploy_access_levels": [
      {
         "id": 12,
         "access_level": 40,
         "access_level_description": "protected-access-group",
         "user_id": null,
         "group_id": 9899826,
         "group_inheritance_type": 0
      }
   ],
   "required_approval_count": 0,
   "approval_rules": [
      {
         "id": 38,
         "user_id": null,
         "group_id": 134,
         "access_level": null,
         "access_level_description": "qa-group",
         "required_approvals": 1,
         "group_inheritance_type": 0
      },
      {
         "id": 39,
         "user_id": null,
         "group_id": 135,
         "access_level": null,
         "access_level_description": "security-group",
         "required_approvals": 2,
         "group_inheritance_type": 0
      }
   ]
}
```

## 更新受保护环境

> 引入于极狐GitLab 15.4。

更新单个环境。

```plaintext
PUT /projects/:id/protected_environments/:name
```

| 参数                        | 类型             | 是否必需 | 描述                                                                                                                          |
|---------------------------|----------------|------|-----------------------------------------------------------------------------------------------------------------------------|
| `id`                      | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)                                                               |
| `name`                    | string         | yes  | 环境名称                                                                                                                        |
| `deploy_access_levels`    | array          | no   | 允许部署的访问级别阵列，每个由哈希进行描述                                                                                                       |
| `required_approval_count` | integer        | no   | 要求部署到此环境的批准数量。是部署批准的一部分，暂不可用                                                                                                |
| `approval_rules`          | array          | no   | 允许批准的访问级别阵列，每个由哈希进行描述。详情请参见[多个批准规则](../ci/environments/deployment_approvals.md#multiple-approval-rules)|

`deploy_access_levels` 和 `approval_rules` 阵列中的元素应该是 `user_id`、`group_id` 或 `access_level`，且格式为 `{user_id: integer}`、`{group_id: integer}` 或 `{access_level: integer}`。（可选）您可以在每个上面将 `group_inheritance_type` 指定为[有效群组继承类型](#group-inheritance-types)。

更新：

- **`user_id`**：确保更新的用户可以访问项目。您还必须在对应哈希中传递 `deploy_access_level` 或 `approval_rule` 的 `id`。
- **`group_id`**：确保更新的群组[分享项目](../user/project/members/share_project_with_groups.md)。您还必须在对应哈希中传递 `deploy_access_level` 或 `approval_rule` 的 `id`。

删除：

- 您必须将 `_destroy` 组传递为 `true`。请参见下例。

### 示例：创建 `deploy_access_level` 记录

```shell
curl --header 'Content-Type: application/json' --request PUT \
     --data '{"deploy_access_levels": [{"group_id": 9899829, access_level: 40}], "required_approval_count": 1}' \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/22034114/protected_environments/production"
```

响应示例：

```json
{
  "name": "production",
  "deploy_access_levels": [
    {
      "id": 12,
      "access_level": 40,
      "access_level_description": "protected-access-group",
      "user_id": null,
      "group_id": 9899829,
      "group_inheritance_type": 1
    }
  ],
  "required_approval_count": 0
}
```

### 示例：更新 `deploy_access_level` 记录

```shell
curl --header 'Content-Type: application/json' --request PUT \
     --data '{"deploy_access_levels": [{"id": 12, "group_id": 22034120}], "required_approval_count": 2}' \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/22034114/protected_environments/production"
```

```json
{
   "name": "production",
   "deploy_access_levels": [
      {
         "id": 12,
         "access_level": 40,
         "access_level_description": "protected-access-group",
         "user_id": null,
         "group_id": 22034120,
         "group_inheritance_type": 0
      }
   ],
   "required_approval_count": 2
}
```

### 示例：删除 `deploy_access_level` 记录

```shell
curl --header 'Content-Type: application/json' --request PUT \
     --data '{"deploy_access_levels": [{"id": 12, "_destroy": true}], "required_approval_count": 0}' \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/22034114/protected_environments/production"
```

响应示例：

```json
{
   "name": "production",
   "deploy_access_levels": [],
   "required_approval_count": 0
}
```

### 示例：创建 `approval_rule` 记录

```shell
curl --header 'Content-Type: application/json' --request PUT \
     --data '{"approval_rules": [{"group_id": 134, "required_approvals": 1}]}' \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/22034114/protected_environments/production"
```

响应示例：

```json
{
   "name": "production",
   "approval_rules": [
      {
         "id": 38,
         "user_id": null,
         "group_id": 134,
         "access_level": null,
         "access_level_description": "qa-group",
         "required_approvals": 1,
         "group_inheritance_type": 0
      }
   ]
}
```

### 示例：更新 `approval_rule` 记录

```shell
curl --header 'Content-Type: application/json' --request PUT \
     --data '{"approval_rules": [{"id": 38, "group_id": 135, "required_approvals": 2}]}' \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/22034114/protected_environments/production"
```

```json
{
   "name": "production",
   "approval_rules": [
      {
         "id": 38,
         "user_id": null,
         "group_id": 135,
         "access_level": null,
         "access_level_description": "security-group",
         "required_approvals": 2,
         "group_inheritance_type": 0
      }
   ]
}
```

### 示例：删除 `approval_rule` 记录

```shell
curl --header 'Content-Type: application/json' --request PUT \
     --data '{"approval_rules": [{"id": 38, "_destroy": true}]}' \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/22034114/protected_environments/production"
```

响应示例：

```json
{
   "name": "production",
   "approval_rules": []
}
```

## 取消保护单个环境

取消保护特定的受保护的环境：

```plaintext
DELETE /projects/:id/protected_environments/:name
```

| 参数     | 类型             | 是否必需 | 描述                                                            |
|--------|----------------|------|---------------------------------------------------------------|
| `id`   | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `name` | string         | yes  | 受保护环境的名称               |


```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/protected_environments/staging"
```
