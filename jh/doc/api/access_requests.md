---
stage: Manage
group: Authentication and Authorization
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
---

# 群组和项目访问请求 API **(FREE)**

## 有效访问级别

访问级别定义于 `Gitlab::Access` 模块，且识别以下级别：

- No access（`0`）
- Minimal access（`5`）（<!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/220203)-->引入于极狐GitLab 13.5。）
- Guest（`10`）
- Reporter（`20`）
- Developer（`30`）
- Maintainer（`40`）
- Owner（`50`）。对 14.9 及更高版本中的项目有效。

## 为群组或项目列出访问请求

获取经过身份验证的用户可见的访问请求列表。

```plaintext
GET /groups/:id/access_requests
GET /projects/:id/access_requests
```

| 参数   | 类型             | 是否必需 | 描述                                                           |
|------|----------------|------|--------------------------------------------------------------|
| `id` | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)|

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/access_requests"
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/access_requests"
```

响应示例：

```json
[
 {
   "id": 1,
   "username": "raymond_smith",
   "name": "Raymond Smith",
   "state": "active",
   "created_at": "2012-10-22T14:13:35Z",
   "requested_at": "2012-10-22T14:13:35Z"
 },
 {
   "id": 2,
   "username": "john_doe",
   "name": "John Doe",
   "state": "active",
   "created_at": "2012-10-22T14:13:35Z",
   "requested_at": "2012-10-22T14:13:35Z"
 }
]
```

## 请求访问群组或项目

经过身份验证的用户请求访问群组或项目。

```plaintext
POST /groups/:id/access_requests
POST /projects/:id/access_requests
```

| 参数 | 类型             | 是否必需 | 描述                                                       |
| --------- |----------------|------|----------------------------------------------------------|
| `id`      | integer/string | yes  | ID 或 [URL 编码的群组或项目路径](rest/index.md#namespaced-path-encoding) |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/access_requests"
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/access_requests"
```

响应示例：

```json
{
  "id": 1,
  "username": "raymond_smith",
  "name": "Raymond Smith",
  "state": "active",
  "created_at": "2012-10-22T14:13:35Z",
  "requested_at": "2012-10-22T14:13:35Z"
}
```

## 批准访问请求

批准特定用户的访问请求。

```plaintext
PUT /groups/:id/access_requests/:user_id/approve
PUT /projects/:id/access_requests/:user_id/approve
```

| 参数             | 类型             | 是否必需 | 描述                                                          |
|----------------|----------------|------|-------------------------------------------------------------|
| `id`           | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `user_id`      | integer        | yes  | 访问请求者的用户 ID                                                 |
| `access_level` | integer        | no   | 有效的访问级别（默认为 `30`，开发者角色）                                     |

请求示例：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/access_requests/:user_id/approve?access_level=20"
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/access_requests/:user_id/approve?access_level=20"
```

响应示例：

```json
{
  "id": 1,
  "username": "raymond_smith",
  "name": "Raymond Smith",
  "state": "active",
  "created_at": "2012-10-22T14:13:35Z",
  "access_level": 20
}
```

## 拒绝访问请求

拒绝特定用户的访问请求。

```plaintext
DELETE /groups/:id/access_requests/:user_id
DELETE /projects/:id/access_requests/:user_id
```

| 参数 | 类型          | 是否必需| 描述 |
| -- | -------------- | -------- | ----------- |
| `id` | integer/string | yes      | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `user_id` | integer        | yes      | 访问请求者的用户 ID                                                                           |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/access_requests/:user_id"
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/access_requests/:user_id"
```
