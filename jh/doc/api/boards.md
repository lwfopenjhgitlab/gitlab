---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目议题板 API **(FREE)**

每个对[议题板](../user/project/issue_board.md)进行的 API 调用都必须经过授权。

如果用户不是私有项目的成员，对项目进行 `GET` 请求会出现 `404` 状态码。

## 列出项目议题板

列出特定项目的项目议题板。

```plaintext
GET /projects/:id/boards
```

| 参数 | 类型 | 是否必需 | 描述 |
| -- | ---- | -------- | ----------- |
| `id` | integer/string | yes | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/boards"
```

响应示例：

```json
[
  {
    "id" : 1,
    "name": "board1",
    "project": {
      "id": 5,
      "name": "Diaspora Project Site",
      "name_with_namespace": "Diaspora / Diaspora Project Site",
      "path": "diaspora-project-site",
      "path_with_namespace": "diaspora/diaspora-project-site",
      "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
      "web_url": "http://example.com/diaspora/diaspora-project-site"
    },
    "milestone":   {
      "id": 12,
      "title": "10.0"
    },
    "lists" : [
      {
        "id" : 1,
        "label" : {
          "name" : "Testing",
          "color" : "#F0AD4E",
          "description" : null
        },
        "position" : 1,
        "max_issue_count": 0,
        "max_issue_weight": 0,
        "limit_metric": null
      },
      {
        "id" : 2,
        "label" : {
          "name" : "Ready",
          "color" : "#FF0000",
          "description" : null
        },
        "position" : 2,
        "max_issue_count": 0,
        "max_issue_weight": 0,
        "limit_metric":  null
      },
      {
        "id" : 3,
        "label" : {
          "name" : "Production",
          "color" : "#FF5F00",
          "description" : null
        },
        "position" : 3,
        "max_issue_count": 0,
        "max_issue_weight": 0,
        "limit_metric":  null
      }
    ]
  }
]
```

项目中未激活或不存在的议题看板的另一个响应示例：

```json
[]
```

## 显示单独议题看板

获取单独项目议题看板。

```plaintext
GET /projects/:id/boards/:board_id
```

| 参数         | 类型             | 是否必需 | 描述                                                                                            |
|------------|----------------|------|-----------------------------------------------------------------------------------------------|
| `id`       | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 议题看板 ID                                                                                        |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/boards/1"
```

响应示例：

```json
  {
    "id": 1,
    "name": "project issue board",
    "project": {
      "id": 5,
      "name": "Diaspora Project Site",
      "name_with_namespace": "Diaspora / Diaspora Project Site",
      "path": "diaspora-project-site",
      "path_with_namespace": "diaspora/diaspora-project-site",
      "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
      "web_url": "http://example.com/diaspora/diaspora-project-site"
    },
    "milestone":   {
      "id": 12,
      "title": "10.0"
    },
    "lists" : [
      {
        "id" : 1,
        "label" : {
          "name" : "Testing",
          "color" : "#F0AD4E",
          "description" : null
        },
        "position" : 1,
        "max_issue_count": 0,
        "max_issue_weight": 0,
        "limit_metric":  null
      },
      {
        "id" : 2,
        "label" : {
          "name" : "Ready",
          "color" : "#FF0000",
          "description" : null
        },
        "position" : 2,
        "max_issue_count": 0,
        "max_issue_weight": 0,
        "limit_metric":  null
      },
      {
        "id" : 3,
        "label" : {
          "name" : "Production",
          "color" : "#FF5F00",
          "description" : null
        },
        "position" : 3,
        "max_issue_count": 0,
        "max_issue_weight": 0,
        "limit_metric":  null
      }
    ]
  }
```

## 创建议题看板

创建项目议题看板。

```plaintext
POST /projects/:id/boards
```

| 参数     | 类型             | 是否必需 | 描述                                                            |
|--------|----------------|------|---------------------------------------------------------------|
| `id`   | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `name` | string         | yes  | 新议题看板的名称                              |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/boards?name=newboard"
```

响应示例：

```json
  {
    "id": 1,
    "project": {
      "id": 5,
      "name": "Diaspora Project Site",
      "name_with_namespace": "Diaspora / Diaspora Project Site",
      "path": "diaspora-project-site",
      "path_with_namespace": "diaspora/diaspora-project-site",
      "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
      "web_url": "http://example.com/diaspora/diaspora-project-site"
    },
    "name": "newboard",
    "lists" : [],
    "group": null,
    "milestone": null,
    "assignee" : null,
    "labels" : [],
    "weight" : null
  }
```

## 更新议题看板

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/5954)-->引入于极狐GitLab 11.1。

更新项目议题看板。

```plaintext
PUT /projects/:id/boards/:board_id
```

| 参数                           | 类型             | 是否必需 | 描述                                                                                  |
|------------------------------|----------------|------|-------------------------------------------------------------------------------------|
| `id`                         | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)                       |
| `board_id`                   | integer        | yes  | 议题看板 ID                                                                              |
| `name`                       | string         | no   | 议题看板的新名称                                                                             |
| `assignee_id` **(PREMIUM)**  | integer        | no   | 议题看板范围内的指派人                                                                          |
| `milestone_id` **(PREMIUM)** | integer        | no   | 议题看板范围内的里程碑                                                                          |
| `labels` **(PREMIUM)**       | string         | no   | 议题看板范围内以逗号分隔的标记名称列表                                                                  |
| `weight` **(PREMIUM)**       | integer        | no   | 议题看板范围内的权重范围：0-9 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/boards/1?name=new_name&milestone_id=43&assignee_id=1&labels=Doing&weight=4"
```

响应示例：

```json
  {
    "id": 1,
    "project": {
      "id": 5,
      "name": "Diaspora Project Site",
      "name_with_namespace": "Diaspora / Diaspora Project Site",
      "path": "diaspora-project-site",
      "path_with_namespace": "diaspora/diaspora-project-site",
      "created_at": "2018-07-03T05:48:49.982Z",
      "default_branch": null,
      "tag_list": [], //deprecated, use `topics` instead
      "topics": [],
      "ssh_url_to_repo": "ssh://user@example.com/diaspora/diaspora-project-site.git",
      "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
      "web_url": "http://example.com/diaspora/diaspora-project-site",
      "readme_url": null,
      "avatar_url": null,
      "star_count": 0,
      "forks_count": 0,
      "last_activity_at": "2018-07-03T05:48:49.982Z"
    },
    "lists": [],
    "name": "new_name",
    "group": null,
    "milestone": {
      "id": 43,
      "iid": 1,
      "project_id": 15,
      "title": "Milestone 1",
      "description": "Milestone 1 desc",
      "state": "active",
      "created_at": "2018-07-03T06:36:42.618Z",
      "updated_at": "2018-07-03T06:36:42.618Z",
      "due_date": null,
      "start_date": null,
      "web_url": "http://example.com/root/board1/milestones/1"
    },
    "assignee": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://example.com/root"
    },
    "labels": [{
      "id": 10,
      "name": "Doing",
      "color": "#5CB85C",
      "description": null
    }],
    "weight": 4
  }
```

## 删除议题看板

删除项目议题看板。

```plaintext
DELETE /projects/:id/boards/:board_id
```

| 参数         | 类型             | 是否必需 | 描述                                                            |
|------------|----------------|------|---------------------------------------------------------------|
| `id`       | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 议题看板 ID                                             |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/boards/1"
```

## 列出项目议题看板中的议题看板列表

获取议题看板列表。
不包括 `open` 和 `closed` 的列表。

```plaintext
GET /projects/:id/boards/:board_id/lists
```

| 参数         | 类型             | 是否必需 | 描述                                                            |
|------------|----------------|------|---------------------------------------------------------------|
| `id`       | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 议题看板 ID                                              |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/boards/1/lists"
```

响应示例：

```json
[
  {
    "id" : 1,
    "label" : {
      "name" : "Testing",
      "color" : "#F0AD4E",
      "description" : null
    },
    "position" : 1,
    "max_issue_count": 0,
    "max_issue_weight": 0,
    "limit_metric":  null
  },
  {
    "id" : 2,
    "label" : {
      "name" : "Ready",
      "color" : "#FF0000",
      "description" : null
    },
    "position" : 2,
    "max_issue_count": 0,
    "max_issue_weight": 0,
    "limit_metric":  null
  },
  {
    "id" : 3,
    "label" : {
      "name" : "Production",
      "color" : "#FF5F00",
      "description" : null
    },
    "position" : 3,
    "max_issue_count": 0,
    "max_issue_weight": 0,
    "limit_metric":  null
  }
]
```

## 显示单独议题看板列表

获取单独议题看板列表。

```plaintext
GET /projects/:id/boards/:board_id/lists/:list_id
```

| 参数         | 类型             | 是否必需 | 描述                                                                                            |
|------------|----------------|------|-----------------------------------------------------------------------------------------------|
| `id`       | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 议题看板 ID                                                                                        |
| `list_id`  | integer        | yes  | 议题看板列表 ID                                                                                      |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/boards/1/lists/1"
```

响应示例：

```json
{
  "id" : 1,
  "label" : {
    "name" : "Testing",
    "color" : "#F0AD4E",
    "description" : null
  },
  "position" : 1,
  "max_issue_count": 0,
  "max_issue_weight": 0,
  "limit_metric":  null
}
```

## 创建议题看板列表

创建新的议题看板列表。

```plaintext
POST /projects/:id/boards/:board_id/lists
```

| 参数                           | 类型             | 是否必需 | 描述                                                            |
|------------------------------|----------------|------|---------------------------------------------------------------|
| `id`                         | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `board_id`                   | integer        | yes  | 议题看板 ID                                                        |
| `label_id`                   | integer        | no   | 标记 ID                                                         |
| `assignee_id` **(PREMIUM)**  | integer        | no   | 用户 ID                                                         |
| `milestone_id` **(PREMIUM)** | integer        | no   | 里程碑 ID                                          |

NOTE:
标记、指派人和里程碑参数是互斥的，也就是说，请求中只接受其中一个。
有关每种列表类型所需许可证的更多信息，请参见[议题看板文档](../user/project/issue_board.md)。

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/boards/1/lists?label_id=5"
```

响应示例：

```json
{
  "id" : 1,
  "label" : {
    "name" : "Testing",
    "color" : "#F0AD4E",
    "description" : null
  },
  "position" : 1,
  "max_issue_count": 0,
  "max_issue_weight": 0,
  "limit_metric":  null
}
```

## 在议题看板中重新排序列表

更新现存的议题看板列表。使用该调用改变列表的位置。

```plaintext
PUT /projects/:id/boards/:board_id/lists/:list_id
```

| 参数         | 类型             | 是否必需 | 描述                                                            |
|------------|----------------|------|---------------------------------------------------------------|
| `id`       | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 议题看板的 ID                                                       |
| `list_id`  | integer        | yes  | 议题看板列表的 ID                                                     |
| `position` | integer        | yes  | 列表的位置                                                         |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/boards/1/lists/1?position=2"
```

响应示例：

```json
{
  "id" : 1,
  "label" : {
    "name" : "Testing",
    "color" : "#F0AD4E",
    "description" : null
  },
  "position" : 1,
  "max_issue_count": 0,
  "max_issue_weight": 0,
  "limit_metric":  null
}
```

## 从议题看板删除板列表

只有管理员和项目拥有者可以删除板列表。

```plaintext
DELETE /projects/:id/boards/:board_id/lists/:list_id
```

| 参数         | 类型             | 是否必需 | 描述                                                            |
|------------|----------------|------|---------------------------------------------------------------|
| `id`       | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 议题看板的 ID                                                       |
| `list_id`  | integer        | yes  | 议题看板列表的 ID                                |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/boards/1/lists/1"
```
