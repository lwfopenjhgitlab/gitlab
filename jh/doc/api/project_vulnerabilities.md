---
stage: Govern
group: Threat Insights
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
type: reference, api
---

# 项目漏洞 API **(ULTIMATE)**

> 在极狐GitLab 12.5 中引入。

WARNING:
该 API 正在被废弃，所以是不稳定的。在不同的极狐GitLab 版本中，该 API 的响应 payload 是会有变更，或损坏的
响应的有效载荷可能会被改变或破坏。<!--请使用 [GraphQL API](graphql/reference/index.md#queryvulnerabilities) 来代替。-->

对漏洞的每一次 API 调用都必须通过[验证](rest/index.md#authentication)。

漏洞权限是从其项目中继承的。如果一个项目是私有的，而用户又不是该漏洞所属项目的成员，那么对该项目的请求就会返回 `404 Not Found` 状态码。

## 漏洞分页

API 结果是分页的，`GET` 请求默认一次返回 20 个结果。

阅读更多关于[分页](rest/index.md#pagination)的内容。

## 列出项目漏洞

列出一个项目的所有漏洞。

如果一个经过认证的用户没有权限使用项目安全仪表板。
对该项目漏洞的 `GET` 请求会收到 `403` 状态代码。

```plaintext
GET /projects/:id/vulnerabilities
```

| 参数 | 类型 | 是否必需 | 描述 |
| ------------- | -------------- | -------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`          | integer or string | yes      | ID 或[项目（经过认证的用户所有）的 URL 路径](rest/index.md#namespaced-path-encoding)                                                            |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/4/vulnerabilities"
```

响应示例：

```json
[
    {
        "author_id": 1,
        "confidence": "medium",
        "created_at": "2020-04-07T14:01:04.655Z",
        "description": null,
        "dismissed_at": null,
        "dismissed_by_id": null,
        "due_date": null,
        "finding": {
            "confidence": "medium",
            "created_at": "2020-04-07T14:01:04.630Z",
            "id": 103,
            "location_fingerprint": "228998b5db51d86d3b091939e2f5873ada0a14a1",
            "metadata_version": "2.0",
            "name": "Regular Expression Denial of Service in debug",
            "primary_identifier_id": 135,
            "project_fingerprint": "05e7cc9978ca495cf739a9f707ed34811e41c615",
            "project_id": 24,
            "raw_metadata": "{\"category\":\"dependency_scanning\",\"name\":\"Regular Expression Denial of Service\",\"message\":\"Regular Expression Denial of Service in debug\",\"description\":\"The debug module is vulnerable to regular expression denial of service when untrusted user input is passed into the `o` formatter. It takes around 50k characters to block for 2 seconds making this a low severity issue.\",\"cve\":\"yarn.lock:debug:gemnasium:37283ed4-0380-40d7-ada7-2d994afcc62a\",\"severity\":\"Unknown\",\"solution\":\"Upgrade to latest versions.\",\"scanner\":{\"id\":\"gemnasium\",\"name\":\"Gemnasium\"},\"location\":{\"file\":\"yarn.lock\",\"dependency\":{\"package\":{\"name\":\"debug\"},\"version\":\"1.0.5\"}},\"identifiers\":[{\"type\":\"gemnasium\",\"name\":\"Gemnasium-37283ed4-0380-40d7-ada7-2d994afcc62a\",\"value\":\"37283ed4-0380-40d7-ada7-2d994afcc62a\",\"url\":\"https://deps.sec.gitlab.com/packages/npm/debug/versions/1.0.5/advisories\"}],\"links\":[{\"url\":\"https://nodesecurity.io/advisories/534\"},{\"url\":\"https://github.com/visionmedia/debug/issues/501\"},{\"url\":\"https://github.com/visionmedia/debug/pull/504\"}],\"remediations\":[null]}",
            "report_type": "dependency_scanning",
            "scanner_id": 63,
            "severity": "low",
            "updated_at": "2020-04-07T14:01:04.664Z",
            "uuid": "f1d528ae-d0cc-47f6-a72f-936cec846ae7",
            "vulnerability_id": 103
        },
        "id": 103,
        "last_edited_at": null,
        "last_edited_by_id": null,
        "project": {
            "created_at": "2020-04-07T13:54:25.634Z",
            "description": "",
            "id": 24,
            "name": "security-reports",
            "name_with_namespace": "gitlab-org / security-reports",
            "path": "security-reports",
            "path_with_namespace": "gitlab-org/security-reports"
        },
        "project_default_branch": "master",
        "report_type": "dependency_scanning",
        "resolved_at": null,
        "resolved_by_id": null,
        "resolved_on_default_branch": false,
        "severity": "low",
        "start_date": null,
        "state": "detected",
        "title": "Regular Expression Denial of Service in debug",
        "updated_at": "2020-04-07T14:01:04.655Z",
        "updated_by_id": null
    }
]
```

## 创建新漏洞

创建一个新的漏洞。

如果一个经过认证的用户没有权限创建一个新的漏洞。
请求会收到 `403` 状态代码。

```plaintext
POST /projects/:id/vulnerabilities?finding_id=<your_finding_id>
```

| 参数 | 类型 | 是否必需 | 描述 |
| ------------------- | ----------------- | ---------- | -----------------------------------------------------------------------------------------------------------------------------|
| `id`                | integer or string | yes        | ID 或[项目的 URL 路径](rest/index.md#namespaced-path-encoding)  |
| `finding_id`        | integer or string | yes        | 已发现的漏洞 ID （新建漏洞由此创建） |

新创建漏洞的其他属性是继承了其源漏洞发现的结果，或使用以下默认值。

| 属性    | 值                                                 |
|--------------|-------------------------------------------------------|
| `author`     | 已认证的用户                                |
| `title`      | 漏洞发现的 `name` 属性      |
| `state`      | `opened`                                              |
| `severity`   | 漏洞发现的 `severity` 属性   |
| `confidence` | 漏洞发现的 `confidence` 属性 |

```shell
curl --header POST "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/vulnerabilities?finding_id=1"
```

响应示例：

```json
{
    "author_id": 1,
    "confidence": "medium",
    "created_at": "2020-04-07T14:01:04.655Z",
    "description": null,
    "dismissed_at": null,
    "dismissed_by_id": null,
    "due_date": null,
    "finding": {
        "confidence": "medium",
        "created_at": "2020-04-07T14:01:04.630Z",
        "id": 103,
        "location_fingerprint": "228998b5db51d86d3b091939e2f5873ada0a14a1",
        "metadata_version": "2.0",
        "name": "Regular Expression Denial of Service in debug",
        "primary_identifier_id": 135,
        "project_fingerprint": "05e7cc9978ca495cf739a9f707ed34811e41c615",
        "project_id": 24,
        "raw_metadata": "{\"category\":\"dependency_scanning\",\"name\":\"Regular Expression Denial of Service\",\"message\":\"Regular Expression Denial of Service in debug\",\"description\":\"The debug module is vulnerable to regular expression denial of service when untrusted user input is passed into the `o` formatter. It takes around 50k characters to block for 2 seconds making this a low severity issue.\",\"cve\":\"yarn.lock:debug:gemnasium:37283ed4-0380-40d7-ada7-2d994afcc62a\",\"severity\":\"Unknown\",\"solution\":\"Upgrade to latest versions.\",\"scanner\":{\"id\":\"gemnasium\",\"name\":\"Gemnasium\"},\"location\":{\"file\":\"yarn.lock\",\"dependency\":{\"package\":{\"name\":\"debug\"},\"version\":\"1.0.5\"}},\"identifiers\":[{\"type\":\"gemnasium\",\"name\":\"Gemnasium-37283ed4-0380-40d7-ada7-2d994afcc62a\",\"value\":\"37283ed4-0380-40d7-ada7-2d994afcc62a\",\"url\":\"https://deps.sec.gitlab.com/packages/npm/debug/versions/1.0.5/advisories\"}],\"links\":[{\"url\":\"https://nodesecurity.io/advisories/534\"},{\"url\":\"https://github.com/visionmedia/debug/issues/501\"},{\"url\":\"https://github.com/visionmedia/debug/pull/504\"}],\"remediations\":[null]}",
        "report_type": "dependency_scanning",
        "scanner_id": 63,
        "severity": "low",
        "updated_at": "2020-04-07T14:01:04.664Z",
        "uuid": "f1d528ae-d0cc-47f6-a72f-936cec846ae7",
        "vulnerability_id": 103
    },
    "id": 103,
    "last_edited_at": null,
    "last_edited_by_id": null,
    "project": {
        "created_at": "2020-04-07T13:54:25.634Z",
        "description": "",
        "id": 24,
        "name": "security-reports",
        "name_with_namespace": "gitlab-org / security-reports",
        "path": "security-reports",
        "path_with_namespace": "gitlab-org/security-reports"
    },
    "project_default_branch": "master",
    "report_type": "dependency_scanning",
    "resolved_at": null,
    "resolved_by_id": null,
    "resolved_on_default_branch": false,
    "severity": "low",
    "start_date": null,
    "state": "detected",
    "title": "Regular Expression Denial of Service in debug",
    "updated_at": "2020-04-07T14:01:04.655Z",
    "updated_by_id": null
}
```

### 错误

当选择用来创建漏洞的漏洞发现未找到，或者已经和另一个漏洞相关联时，就会报出这个错误：

```plaintext
A Vulnerability Finding is not found or already attached to a different Vulnerability
```

状态码：`400`

响应示例：

```json
{
  "message": {
    "base": [
      "finding is not found or is already attached to a vulnerability"
    ]
  }
}
```
