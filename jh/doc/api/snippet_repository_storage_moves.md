---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

# 代码片段仓库存储迁移 API **(FREE SELF)**

> 引入于 13.8 版本。

代码片段仓库可以在不同存储间迁移。此 API 可以帮助您[迁移到 Gitaly 集群](../administration/gitaly/index.md#migrate-to-gitaly-cluster)，例如：

当代码片段仓库迁移时，状态会随之变化。 `state` 的值如下：

- `initial`：创建了记录但后台任务还没有安排运行。
- `scheduled`：后台任务已经安排某一时间运行。
- `started`：代码片段仓库正在被拷贝到目标存储中。
- `replicated`：代码片段仓库已经迁移完成。
- `failed`：代码片段仓库迁移失败，或校验 checksum 失败。
- `finished`：代码片段仓库已经迁移完成，原始存储中的仓库已经被删除。
- `cleanup failed`：代码片段仓库已经迁移完成，但原始存储中的仓库没有被删除。

为了保证数据的完整性，代码片段在迁移期间被临时设置成只读状态。此时如果用户尝试推送新的提交，会收到 `The repository is temporarily read-only. Please try again later.` 的错误消息。

使用该 API 时需要进行[管理员身份验证](rest/index.md#authentication)。

对于其他仓库类型，请参照：

- [项目仓库存储迁移 API](project_repository_storage_moves.md)。
- [群组仓库存储迁移 API](group_repository_storage_moves.md)。

## 获取所有代码仓库存储迁移

```plaintext
GET /snippet_repository_storage_moves
```

因为 API 返回值采用[分页](rest/index.md#pagination)，`GET` 请求默认返回 20 条数据。

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/snippet_repository_storage_moves"
```

响应示例：

```json
[
  {
    "id": 1,
    "created_at": "2020-05-07T04:27:17.234Z",
    "state": "scheduled",
    "source_storage_name": "default",
    "destination_storage_name": "storage2",
    "snippet": {
      "id": 65,
      "title": "Test Snippet",
      "description": null,
      "visibility": "internal",
      "updated_at": "2020-12-01T11:15:50.385Z",
      "created_at": "2020-12-01T11:15:50.385Z",
      "project_id": null,
      "web_url": "https://gitlab.example.com/-/snippets/65",
      "raw_url": "https://gitlab.example.com/-/snippets/65/raw",
      "ssh_url_to_repo": "ssh://user@gitlab.example.com/snippets/65.git",
      "http_url_to_repo": "https://gitlab.example.com/snippets/65.git"
    }
  }
]
```

## 获取一个代码片段的所有仓库存储迁移

```plaintext
GET /snippets/:snippet_id/repository_storage_moves
```

因为 API 返回值采用[分页](rest/index.md#pagination)，`GET` 请求默认返回 20 条数据。

支持的参数:

| 参数                                | 类型             | 是否必需                             | 描述 |
| --------- | ---- | -------- | ----------- |
| `snippet_id` | integer | yes | 代码片段的 ID。 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/snippets/1/repository_storage_moves"
```

响应示例：

```json
[
  {
    "id": 1,
    "created_at": "2020-05-07T04:27:17.234Z",
    "state": "scheduled",
    "source_storage_name": "default",
    "destination_storage_name": "storage2",
    "snippet": {
      "id": 65,
      "title": "Test Snippet",
      "description": null,
      "visibility": "internal",
      "updated_at": "2020-12-01T11:15:50.385Z",
      "created_at": "2020-12-01T11:15:50.385Z",
      "project_id": null,
      "web_url": "https://gitlab.example.com/-/snippets/65",
      "raw_url": "https://gitlab.example.com/-/snippets/65/raw",
      "ssh_url_to_repo": "ssh://user@gitlab.example.com/snippets/65.git",
      "http_url_to_repo": "https://gitlab.example.com/snippets/65.git"
    }
  }
]
```

## 获取一个代码片段仓库迁移

```plaintext
GET /snippet_repository_storage_moves/:repository_storage_id
```

支持的参数：

| 参数                                | 类型             | 是否必需                             | 描述 |
| --------- | ---- | -------- | ----------- |
| `repository_storage_id` | integer | yes | 代码片段仓库的 ID。|

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/snippet_repository_storage_moves/1"
```

响应示例：

```json
{
  "id": 1,
  "created_at": "2020-05-07T04:27:17.234Z",
  "state": "scheduled",
  "source_storage_name": "default",
  "destination_storage_name": "storage2",
  "snippet": {
    "id": 65,
    "title": "Test Snippet",
    "description": null,
    "visibility": "internal",
    "updated_at": "2020-12-01T11:15:50.385Z",
    "created_at": "2020-12-01T11:15:50.385Z",
    "project_id": null,
    "web_url": "https://gitlab.example.com/-/snippets/65",
    "raw_url": "https://gitlab.example.com/-/snippets/65/raw",
    "ssh_url_to_repo": "ssh://user@gitlab.example.com/snippets/65.git",
    "http_url_to_repo": "https://gitlab.example.com/snippets/65.git"
  }
}
```

## 获取一个代码片段的一个仓库存储迁移

```plaintext
GET /snippets/:snippet_id/repository_storage_moves/:repository_storage_id
```

支持的参数：

| 参数                                | 类型             | 是否必需                             | 描述 |
| --------- | ---- | -------- | ----------- |
| `snippet_id` | integer | yes | 代码片段的 ID。 |
| `repository_storage_id` | integer | yes | 代码片段仓库迁移的 ID。|

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/snippets/1/repository_storage_moves/1"
```

响应示例：

```json
{
  "id": 1,
  "created_at": "2020-05-07T04:27:17.234Z",
  "state": "scheduled",
  "source_storage_name": "default",
  "destination_storage_name": "storage2",
  "snippet": {
    "id": 65,
    "title": "Test Snippet",
    "description": null,
    "visibility": "internal",
    "updated_at": "2020-12-01T11:15:50.385Z",
    "created_at": "2020-12-01T11:15:50.385Z",
    "project_id": null,
    "web_url": "https://gitlab.example.com/-/snippets/65",
    "raw_url": "https://gitlab.example.com/-/snippets/65/raw",
    "ssh_url_to_repo": "ssh://user@gitlab.example.com/snippets/65.git",
    "http_url_to_repo": "https://gitlab.example.com/snippets/65.git"
  }
}
```

## 为一段代码片段安排一个仓库存储迁移

> 引入于 13.8 版本。

```plaintext
POST /snippets/:snippet_id/repository_storage_moves
```

支持的参数:

| 参数                                | 类型             | 是否必需                             | 描述 |
| --------- | ---- | -------- | ----------- |
| `snippet_id` | integer | yes | 代码片段的 ID。|
| `destination_storage_name` | string | no | 目标存储分片的名称。在 13.5 及更高版本中，如何没有提供存储则会根据[存储的权重自动选择](../administration/repository_storage_paths.md#configure-where-new-repositories-are-stored)。|

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --header "Content-Type: application/json" \
     --data '{"destination_storage_name":"storage2"}' \
     "https://gitlab.example.com/api/v4/snippets/1/repository_storage_moves"
```

响应示例：

```json
{
  "id": 1,
  "created_at": "2020-05-07T04:27:17.234Z",
  "state": "scheduled",
  "source_storage_name": "default",
  "destination_storage_name": "storage2",
  "snippet": {
    "id": 65,
    "title": "Test Snippet",
    "description": null,
    "visibility": "internal",
    "updated_at": "2020-12-01T11:15:50.385Z",
    "created_at": "2020-12-01T11:15:50.385Z",
    "project_id": null,
    "web_url": "https://gitlab.example.com/-/snippets/65",
    "raw_url": "https://gitlab.example.com/-/snippets/65/raw",
    "ssh_url_to_repo": "ssh://user@gitlab.example.com/snippets/65.git",
    "http_url_to_repo": "https://gitlab.example.com/snippets/65.git"
  }
}
```

## 在存储分片上对所有代码片段安排仓库存储迁移

> 引入于 13.8 版本。

为存储在原始存储分片上的每一个代码片段安排仓库存储迁移。
此端点一次迁移所有代码片段。有关详细信息，请参阅[移动所有代码片段](../administration/operations/moving_repositories.md#move-all-snippets)。

```plaintext
POST /snippet_repository_storage_moves
```

支持的参数:

| 参数                                | 类型             | 是否必需                             | 描述 |
| --------- | ---- | -------- | ----------- |
| `source_storage_name` | string | yes | 原始存储分片的名称。|
| `destination_storage_name` | string | no | 目标存储分片的名称。如果没有提供则根据[存储权重自动选择](../administration/repository_storage_paths.md#configure-where-new-repositories-are-stored)。|

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --header "Content-Type: application/json" \
     --data '{"source_storage_name":"default"}' \
     "https://gitlab.example.com/api/v4/snippet_repository_storage_moves"
```

响应示例：

```json
{
  "message": "202 Accepted"
}
```
