---
stage: Fulfillment
group: Utilization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 许可证 **(FREE SELF)**

要与许可证端点交互，您需要将自己验证为管理员。

## 检索当前许可证信息

```plaintext
GET /license
```

```json
{
  "id": 2,
  "plan": "gold",
  "created_at": "2018-02-27T23:21:58.674Z",
  "starts_at": "2018-01-27",
  "expires_at": "2022-01-27",
  "historical_max": 300,
  "maximum_user_count": 300,
  "expired": false,
  "overage": 200,
  "user_limit": 100,
  "active_users": 300,
  "licensee": {
    "Name": "John Doe1"
  },
  "add_ons": {
    "GitLab_FileLocks": 1,
    "GitLab_Auditor_User": 1
  }
}
```

## 检索所有许可证信息

```plaintext
GET /licenses
```

```json
[
  {
    "id": 1,
    "plan": "silver",
    "created_at": "2018-02-27T23:21:58.674Z",
    "starts_at": "2018-01-27",
    "expires_at": "2022-01-27",
    "historical_max": 300,
    "maximum_user_count": 300,
    "expired": false,
    "overage": 200,
    "user_limit": 100,
    "licensee": {
      "Name": "John Doe1"
    },
    "add_ons": {
      "GitLab_FileLocks": 1,
      "GitLab_Auditor_User": 1
    }
  },
  {
    "id": 2,
    "plan": "gold",
    "created_at": "2018-02-27T23:21:58.674Z",
    "starts_at": "2018-01-27",
    "expires_at": "2022-01-27",
    "historical_max": 300,
    "maximum_user_count": 300,
    "expired": false,
    "overage": 200,
    "user_limit": 100,
    "licensee": {
      "Name": "Doe John"
    },
    "add_ons": {
      "GitLab_FileLocks": 1
    }
  }
]
```

  超额是计费用户数与许可用户数之间的差额。
  根据许可证是否过期，计算方式不同。

- 如果许可证已过期，则使用历史最大计费用户计数 (`historical_max`)。
- 如果许可证未过期，则使用当前计费用户计数。

返回：

- `200 OK`：响应包含 JSON 格式的许可证。如果没有许可证，则是空的 JSON 数组。
- `403 Forbidden`：如果当前用户不允许读取许可证。

## 检索单个许可证的信息

```plaintext
GET /license/:id
```

支持的参数：

| 参数   | 类型      | 是否必需 | 描述            |
|------|---------|------|---------------|
| `id` | integer | yes  | 极狐GitLab 许可证 ID |

返回以下状态码：

- `200 OK`：包含 JSON 格式的许可证的响应
- `404 Not Found`：所请求的许可证不存在
- `403 Forbidden`：当前用户无权读取许可证

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/license/:id"
```

响应示例：

```json
{
  "id": 1,
  "plan": "premium",
  "created_at": "2018-02-27T23:21:58.674Z",
  "starts_at": "2018-01-27",
  "expires_at": "2022-01-27",
  "historical_max": 300,
  "maximum_user_count": 300,
  "expired": false,
  "overage": 200,
  "user_limit": 100,
  "active_users": 50,
  "licensee": {
    "Name": "John Doe1"
  },
  "add_ons": {
    "GitLab_FileLocks": 1,
    "GitLab_Auditor_User": 1
  }
}
```

## 添加新许可证

```plaintext
POST /license
```

| 参数        | 类型     | 是否必需 | 描述   |
|-----------|--------|------|------|
| `license` | string | yes  | 许可证串 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/license?license=eyJkYXRhIjoiMHM5Q...S01Udz09XG4ifQ=="
```

响应示例：

```json
{
  "id": 1,
  "plan": "gold",
  "created_at": "2018-02-27T23:21:58.674Z",
  "starts_at": "2018-01-27",
  "expires_at": "2022-01-27",
  "historical_max": 300,
  "maximum_user_count": 300,
  "expired": false,
  "overage": 200,
  "user_limit": 100,
  "active_users": 300,
  "licensee": {
    "Name": "John Doe1"
  },
  "add_ons": {
    "GitLab_FileLocks": 1,
    "GitLab_Auditor_User": 1
  }
}
```

返回：

- 成功添加许可证：`201 Created`
- 无法添加许可证：`400 Bad Request`，并返回错误原因

## 删除许可证

```plaintext
DELETE /license/:id
```

| 参数   | 类型      | 是否必需 | 描述                               |
|------|---------|------|----------------------------------|
| `id` | integer | yes  | 极狐GitLab 许可证 ID|

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/license/:id"
```

响应示例：

```json
{
  "id": 2,
  "plan": "gold",
  "created_at": "2018-02-27T23:21:58.674Z",
  "starts_at": "2018-01-27",
  "expires_at": "2022-01-27",
  "historical_max": 300,
  "maximum_user_count": 300,
  "expired": false,
  "overage": 200,
  "user_limit": 100,
  "licensee": {
    "Name": "John Doe"
  },
  "add_ons": {
    "GitLab_FileLocks": 1,
    "GitLab_Auditor_User": 1
  }
}
```

返回：

- 成功删除许可证：`204 No Content`
- 当前用户无权删除许可证：`403 Forbidden`
- 无法找到要删除的许可证：`404 Not Found`

## 触发计费用户的重新计算

```plaintext
PUT /license/:id/refresh_billable_users
```

| 参数   | 类型      | 是否必需 | 描述            |
|------|---------|------|---------------|
| `id` | integer | yes  | 极狐GitLab 许可证 ID |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/license/:id/refresh_billable_users"
```

响应示例：

```json
{
  "success": true
}
```

返回以下内容：

- `202 Accepted`：如果刷新计费用户的请求成功启动。
- `403 Forbidden`：如果当前用户不允许刷新许可证的计费用户。
- `404 Not Found`：如果找不到许可证。

| 参数        | 类型      | 描述     |
|:----------|:--------|:-------|
| `success` | boolean | 请求是否成功 |
