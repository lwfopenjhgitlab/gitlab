---
stage: Create
group: IDE
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

# 群组仓库存储移动 API **(PREMIUM SELF)**

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/53016)-->引入于极狐GitLab 13.9。

群组仓库可以在存储间进行移动。此 API 可以帮助您迁移到 Gitaly 集群<!--[迁移到 Gitaly 集群](../administration/gitaly/index.md#migrate-to-gitaly-cluster)-->，例如迁移[群组 Wiki](../user/project/wiki/group.md)。

在处理群组仓库存储移动时，它们会转换不同的状态。`state` 的值为：

- `initial`：记录已创建，但尚未安排后台作业。
- `scheduled`：已安排后台作业。
- `started`：正在将群组仓库复制到目标存储。
- `replicated`：群组已被移动。
- `failed`：群组仓库复制失败，或校验和不匹配。
- `finished`：群组已被移动，源存储上的仓库已被删除。
- `cleanup failed`：群组已被移动，但源存储上的仓库无法删除。

为确保数据完整性，移动时群组被置于临时只读状态。在此期间，如果用户尝试推送新的提交，他们会收到以下信息：

```plaintext
The repository is temporarily read-only. Please try again later.
```

此 API 要求您[将自己授权](rest/index.md#authentication)为管理员。

API 也可以移动其他类型的仓库：

- 项目仓库存储移动 API<!--[项目仓库存储移动 API](project_repository_storage_moves.md)-->
- [代码片段仓库存储移动 API](snippet_repository_storage_moves.md)

## 检索所有群组仓库存储移动

```plaintext
GET /group_repository_storage_moves
```

默认情况下，`GET` 请求一次返回 20 个结果，因为 API 结果是[分页的](rest/index.md#pagination)。

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/group_repository_storage_moves"
```

响应示例：

```json
[
  {
    "id": 1,
    "created_at": "2020-05-07T04:27:17.234Z",
    "state": "scheduled",
    "source_storage_name": "default",
    "destination_storage_name": "storage2",
    "group": {
      "id": 283,
      "web_url": "https://gitlab.example.com/groups/testgroup",
      "name": "testgroup"
    }
  }
]
```

## 为单个群组检索所有仓库存储移动

您可以使用以下端点为单个群组检索所有仓库存储移动：

```plaintext
GET /groups/:group_id/repository_storage_moves
```

默认情况下，`GET` 请求一次返回 20 个结果，因为 API 结果是[分页的](rest/index.md#pagination)。

支持的参数：

| 参数         | 类型      | 是否必需 | 描述                  |
|------------|---------|------|---------------------|
| `group_id` | integer | yes  | 群组 ID |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/repository_storage_moves"
```

响应示例：

```json
[
  {
    "id": 1,
    "created_at": "2020-05-07T04:27:17.234Z",
    "state": "scheduled",
    "source_storage_name": "default",
    "destination_storage_name": "storage2",
    "group": {
      "id": 283,
      "web_url": "https://gitlab.example.com/groups/testgroup",
      "name": "testgroup"
    }
  }
]
```

## 获取单个群组仓库存储移动

在所有现有仓库存储移动中检索单个仓库存储移动，您可以使用以下端点：

```plaintext
GET /group_repository_storage_moves/:repository_storage_id
```

支持的参数：

| 参数                      | 类型      | 是否必需 | 描述                                                |
|-------------------------|---------|------|---------------------------------------------------|
| `repository_storage_id` | integer | yes  | 群组仓库存储移动 ID |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/group_repository_storage_moves/1"
```

响应示例：

```json
{
  "id": 1,
  "created_at": "2020-05-07T04:27:17.234Z",
  "state": "scheduled",
  "source_storage_name": "default",
  "destination_storage_name": "storage2",
  "group": {
    "id": 283,
    "web_url": "https://gitlab.example.com/groups/testgroup",
    "name": "testgroup"
  }
}
```

## 为群组获取单个仓库存储移动

您可以通过以下端点检索给定群组的特定仓库存储移动：

```plaintext
GET /groups/:group_id/repository_storage_moves/:repository_storage_id
```

支持的参数：

| 参数                      | 类型      | 是否必需 | 描述                                                |
|-------------------------|---------|------|---------------------------------------------------|
| `group_id`              | integer | yes  | 群组 ID                                             |
| `repository_storage_id` | integer | yes  | 群组仓库存储移动 ID  |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/repository_storage_moves/1"
```

响应示例：

```json
{
  "id": 1,
  "created_at": "2020-05-07T04:27:17.234Z",
  "state": "scheduled",
  "source_storage_name": "default",
  "destination_storage_name": "storage2",
  "group": {
    "id": 283,
    "web_url": "https://gitlab.example.com/groups/testgroup",
    "name": "testgroup"
  }
}
```

## 为群组计划仓库存储移动

为群组安排仓库存储移动。这个端点：

- 仅移动群组 Wiki 仓库。
- 不移动群组中项目的仓库。要安排项目移动，请使用[项目仓库存储移动](project_repository_storage_moves.md) API。

```plaintext
POST /groups/:group_id/repository_storage_moves
```

支持的参数：

| 参数                         | 类型      | 是否必需 | 描述                                                                                                                                                                                                                                                                                                                                                                                                |
|----------------------------|---------|------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `group_id`                 | integer | yes  | 群组 ID                                                                                                                                                                                                                                                                                                                                                                                             |
| `destination_storage_name` | string  | no   | 目标存储分片的名称。在极狐GitLab 13.5 及更高版本中，如未提供，则会[根据存储权重](../administration/repository_storage_paths.md#configure-where-new-repositories-are-stored)选择存储<!--在[GitLab 13.5 and later](https://gitlab.com/gitlab-org/gitaly/-/issues/3209), the storage is selected [based on storage weights](../administration/repository_storage_paths.md#configure-where-new-repositories-are-stored) if not provided.--> |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --header "Content-Type: application/json" \
     --data '{"destination_storage_name":"storage2"}' \
     "https://gitlab.example.com/api/v4/groups/1/repository_storage_moves"
```

响应示例：

```json
{
  "id": 1,
  "created_at": "2020-05-07T04:27:17.234Z",
  "state": "scheduled",
  "source_storage_name": "default",
  "destination_storage_name": "storage2",
  "group": {
    "id": 283,
    "web_url": "https://gitlab.example.com/groups/testgroup",
    "name": "testgroup"
  }
}
```

## 为所有存储分片上的群组计划仓库存储移动

为存储在源存储分片上的每个群组仓库计划仓库存储移动。此端点一次迁移所有群组。有关详细信息，请参阅[移动所有群组](../administration/operations/moving_repositories.md#move-all-groups)。

```plaintext
POST /group_repository_storage_moves
```

支持的参数：

| 参数                         | 类型     | 是否必需 | 描述                                                                                                                                                                                                    |
|----------------------------|--------|------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `source_storage_name`      | string | yes  | 源存储分片的名称                                                                                                                                                            |
| `destination_storage_name` | string | no   | 目标存储分片的名称。如未提供，则会[根据存储权重](../administration/repository_storage_paths.md#configure-where-new-repositories-are-stored)选择存储 |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --header "Content-Type: application/json" \
     --data '{"source_storage_name":"default"}' \
     "https://gitlab.example.com/api/v4/group_repository_storage_moves"
```

响应示例：

```json
{
  "message": "202 Accepted"
}
```
