---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# SAML API **(PREMIUM SAAS)**

> 引入于极狐GitLab 15.5。

访问 SAML 功能的 API。

## 获取群组的 SAML 身份

```plaintext
GET /groups/:id/saml/identities
```

获取群组的 SAML 身份。

支持的参数：

| 参数   | 类型             | 是否必需 | 描述                                                            |
|:-----|:---------------|:-----|:--------------------------------------------------------------|
| `id` | integer/string | Yes  | 群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |

如果成功，则返回 [`200`](rest/index.md#status-codes)和以下响应参数：

| 参数           | 类型     | 描述                 |
|--------------|--------|--------------------|
| `extern_uid` | string | 用户的外部 UID          |
| `user_id`    | string | 用户 ID |

请求示例：

```shell
curl --location --request GET "https://gitlab.example.com/api/v4/groups/33/saml/identities" --header "<PRIVATE-TOKEN>"
```

响应示例：

```json
[
    {
        "extern_uid": "4",
        "user_id": 48
    }
]
```

## 获取单个 SAML 身份

> 引入于极狐GitLab 16.1。

```plaintext
GET /groups/:id/saml/:uid
```

支持的参数：

| 参数    | 类型             | 是否必需 | 描述                                                         |
|-------|----------------|------|------------------------------------------------------------|
| `id`  | integer/string | yes  | 群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `uid` | string         | yes  | 用户的外部 UID                                                  |

请求示例：

```shell
curl --location --request GET "https://gitlab.example.com/api/v4/groups/33/saml/sydney_jones" --header "<PRIVATE TOKEN>"
```

响应示例：

```json
{
    "extern_uid": "4",
    "user_id": 48
}
```

## 更新 SAML 身份的 `extern_uid` 字段

更新 SAML 身份的 `extern_uid` 字段。

| SAML IdP 参数     | 极狐GitLab 字段  |
|-----------------|--------------|
| `id/externalId` | `extern_uid` |

```plaintext
PATCH /groups/:id/saml/:uid
```

支持的参数：

| 参数    | 类型     | 是否必需 | 描述                                                          |
|-------|--------|------|-------------------------------------------------------------|
| `id`      | integer/string | yes      | 群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `uid` | string | yes  | 用户的外部 UID                                                   |

请求示例：

```shell
curl --location --request PATCH "https://gitlab.example.com/api/v4/groups/33/saml/sydney_jones" \
--header "<PRIVATE TOKEN>" \
--form "extern_uid=sydney_jones_new" \
```
