---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目代码片段 **(FREE)**

<a id="snippet-visibility-level"></a>

## 代码片段可见性级别

极狐GitLab 中的代码片段（../api/project_snippets.md）可以是私有的、内部的或公共的。您可以使用代码片段中的 `visibility` 字段进行设置。

代码片段可见性级别的常量是：

- **`private`**：代码片段只对项目成员可见。                                                     
- **`internal`**：代码片段对所有经过身份验证的用户可见，除了[外部用户](../user/admin_area/external_users.md)。
- **`public`**：代码片段可以无授权访问。                                                       

NOTE:
从 2019 年 7 月起，新项目、群组和 JiHuLab.com 上的代码片段的 `Internal` 可见性设置都被禁用。使用 `Internal` 可见性设置的现有项目、群组和代码片段保留此设置。

## 列出代码片段

获取项目代码片段列表。

```plaintext
GET /projects/:id/snippets
```

参数：

| 参数   | 类型                | 是否必需 | 描述                                                            |
|------|-------------------|------|---------------------------------------------------------------|
| `id` | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |

## 单个代码片段

获取单个项目代码片段。

```plaintext
GET /projects/:id/snippets/:snippet_id
```

参数：

| 参数           | 类型                | 是否必需 | 描述                                                            |
|--------------|-------------------|------|---------------------------------------------------------------|
| `id`         | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `snippet_id` | integer           | yes  | 项目的代码片段的 ID                            |

```json
{
  "id": 1,
  "title": "test",
  "file_name": "add.rb",
  "description": "Ruby test snippet",
  "author": {
    "id": 1,
    "username": "john_smith",
    "email": "john@example.com",
    "name": "John Smith",
    "state": "active",
    "created_at": "2012-05-23T08:00:58Z"
  },
  "updated_at": "2012-06-28T10:52:04Z",
  "created_at": "2012-06-28T10:52:04Z",
  "project_id": 1,
  "web_url": "http://example.com/example/example/snippets/1",
  "raw_url": "http://example.com/example/example/snippets/1/raw"
}
```

## 创建新代码片段
创建新的项目代码片段。用户必需拥有创建新代码片段的相应权限。

```plaintext
POST /projects/:id/snippets
```

参数：

| 参数                | 类型                | 是否必需 | 描述                                                            |
|:------------------|:------------------|:-----|:--------------------------------------------------------------|
| `id`              | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `title`           | string            | yes  | 代码片段的标题                                                       |
| `file_name`       | string            | no   | 已废弃：使用 `files`。代码片段文件的名称                                      |
| `content`         | string            | no   | 已废弃：使用 `files`。代码片段的内容                                        |
| `description`     | string            | no   | 代码片段的描述                                                       |
| `visibility`      | string            | no   | 代码片段的[可见性](#snippet-visibility-level)                         |
| `files`           | array of hashes   | no   | 代码片段阵列                                                        |
| `files:file_path` | string            | yes  | 代码片段文件的文件路径                                                   |
| `files:content`   | string            | yes  | 代码片段文件的内容                                                     |

请求示例：

```shell
curl --request POST "https://gitlab.com/api/v4/projects/:id/snippets" \
     --header "PRIVATE-TOKEN: <your access token>" \
     --header "Content-Type: application/json" \
     -d @snippet.json
```

上面请求示例中使用的 `snippet.json`：

```json
{
  "title" : "Example Snippet Title",
  "description" : "More verbose snippet description",
  "visibility" : "private",
  "files": [
    {
      "file_path": "example.txt",
      "content" : "source code \n with multiple lines\n"
    }
  ]
}
```

## 更新代码片段

更新现存的项目代码片段。用户必需拥有更改现存代码片段的相应权限。

对包含多个文件的代码片段进行更新时必须使用 `files` 参数。

```plaintext
PUT /projects/:id/snippets/:snippet_id
```

参数：

| 参数                    | 类型                | 是否必需 | 描述                                                            |
|:----------------------|:------------------|:-----|:--------------------------------------------------------------|
| `id`                  | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `snippet_id`          | integer           | yes  | 项目的代码片段的 ID                                                   |
| `title`               | string            | no   | 代码片段的标题                                                       |
| `file_name`           | string            | no   | 已废弃：使用 `files`。代码片段文件的名称                                      |
| `content`             | string            | no   | 已废弃：使用 `files`。代码片段的内容                                        |
| `description`         | string            | no   | 代码片段的描述                                                       |
| `visibility`          | string            | no   | 代码片段的[可见性](#snippet-visibility-level)                         |
| `files`               | array of hashes   | no   | 代码片段文件阵列                                                      |
| `files:action`        | string            | yes  | 在文件上进行的操作类型：`create`、`update`、`delete` 或 `move`            |
| `files:file_path`     | string            | no   | 代码片段文件的文件路径                                                   |
| `files:previous_path` | string            | no   | 代码片段文件之前的路径                                                   |
| `files:content`       | string            | no   | 代码片段文件的内容                                                     |


请求示例：

```shell
curl --request PUT "https://gitlab.com/api/v4/projects/:id/snippets/:snippet_id" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     --header "Content-Type: application/json" \
     -d @snippet.json
```

上面请求示例中使用的 `snippet.json`：

```json
{
  "title" : "Updated Snippet Title",
  "description" : "More verbose snippet description",
  "visibility" : "private",
  "files": [
    {
      "action": "update",
      "file_path": "example.txt",
      "content" : "updated source code \n with multiple lines\n"
    }
  ]
}
```

## 删除代码片段

删除现存的项目代码片段。如果删除成功，则返回 `204 No Content` 状态码；如果未找到资源，则返回 `404`。

```plaintext
DELETE /projects/:id/snippets/:snippet_id
```

参数：

| 参数           | 类型                | 是否必需 | 描述                                                            |
|:-------------|:------------------|:-----|:--------------------------------------------------------------|
| `id`         | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `snippet_id` | integer           | yes  | 项目代码片段 ID                                                     |

请求示例：

```shell
curl --request DELETE "https://gitlab.com/api/v4/projects/:id/snippets/:snippet_id" \
     --header "PRIVATE-TOKEN: <your_access_token>"
```

## 代码片段内容

将原始项目代码片段作为纯文本返回。

```plaintext
GET /projects/:id/snippets/:snippet_id/raw
```

参数：

| 参数           | 类型                | 是否必需 | 描述                                                            |
|:-------------|:------------------|:-----|:--------------------------------------------------------------|
| `id`         | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `snippet_id` | integer           | yes  | 项目的代码片段 ID                              |

请求示例：

```shell
curl "https://gitlab.com/api/v4/projects/:id/snippets/:snippet_id/raw" \
     --header "PRIVATE-TOKEN: <your_access_token>"
```

## 代码片段仓库文件内容

将原始文件内容作为纯文本返回。

```plaintext
GET /projects/:id/snippets/:snippet_id/files/:ref/:file_path/raw
```

参数：

| 参数           | 类型                | 是否必需 | 描述                                                            |
|:-------------|:------------------|:-----|:--------------------------------------------------------------|
| `id`         | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `snippet_id` | integer           | yes  | 项目的代码片段 ID                                                    |
| `ref`        | string            | yes  | 分支、标签或提交的名称，例如：main                                           |
| `file_path`  | string            | yes  | URL 编码的文件路径。例如：`snippet%2Erb`                                   |

请求示例：

```shell
curl "https://gitlab.com/api/v4/projects/1/snippets/2/files/master/snippet%2Erb/raw" \
     --header "PRIVATE-TOKEN: <your_access_token>"
```

## 获取用户代理详细信息

只有拥有管理员权限的用户才可用。

```plaintext
GET /projects/:id/snippets/:snippet_id/user_agent_detail
```

| 参数           | 类型                | 是否必需 | 描述                                                            |
|--------------|-------------------|------|---------------------------------------------------------------|
| `id`         | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `snippet_id` | Integer           | yes  | 代码片段 ID                                          |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/snippets/2/user_agent_detail"
```

响应示例：

```json
{
  "user_agent": "AppleWebKit/537.36",
  "ip_address": "127.0.0.1",
  "akismet_submitted": false
}
```
