---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 版本 API **(FREE)**

NOTE:
我们建议您使用[元数据 API](metadata.md) 而不是版本 API。
它包含附加信息并与 GraphQL 元数据端点保持一致。
从极狐GitLab 15.5 开始，版本 API 是元数据 API 的镜像。

获取极狐GitLab 示例的版本信息。对已登录用户返回 `200 OK`。

```plaintext
GET /version
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/version"
```

## 响应示例

### 极狐GitLab 15.5 及更高版本

响应请参见[元数据 API](metadata.md)。

### 极狐GitLab 15.4 及更早版本

```json
{
  "version": "8.13.0-pre",
  "revision": "4e963fe"
}
```
