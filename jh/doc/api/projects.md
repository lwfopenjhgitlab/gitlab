---
stage: Data Stores
group: Tenant Scale
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目 API **(FREE)**

使用 REST API 与[项目](../user/project/index.md)交互。

<a id="project-visibility-level"></a>

## 项目可见性级别

极狐GitLab 中的项目可以是私有的、内部的或公开的。
可见性级别由项目中的**可见性**字段确定。

有关更多信息，请阅读[项目可见性](../user/public_access.md)。

响应中返回的字段根据经过身份验证的用户的[权限](../user/permissions.md)而有所不同。

## API v5 中移除的内容

以下参数已弃用，并计划在 API 的 v5 中移除：

- `tag_list`：改用 `topics` 参数。
- `marked_for_deletion_at`：改用 `marked_for_deletion_on` 参数。
  仅适用于[极狐GitLab 专业版或旗舰版](https://about.gitlab.cn/pricing/)。
-`approvals_before_merge`：改用[合并请求批准 API](merge_request_approvals.md)。
  仅适用于[极狐GitLab 专业版或旗舰版](https://about.gitlab.cn/pricing/)。

<a id="project-merge-method"></a>

## 项目合并方法

`merge_method` 可以使用以下选项：

- `merge`：为每次合并创建一个合并提交，如果没有冲突，则允许合并。
- `rebase_merge`：为每次合并创建一个合并提交，但只有在可以进行快进合并时才允许合并。您可以确保目标分支将在此合并请求构建和合并后构建。
- `ff`：不创建合并提交，所有合并都是快进的，只有当分支可以快进时才允许合并。

## 列出所有项目

> 响应中的 `_links.cluster_agents` 参数引入于 15.0。

获取经过身份验证的用户在极狐GitLab 中获取所有可见项目的列表。
未经身份验证访问时，仅使用简单字段返回公开项目。

```plaintext
GET /projects
```

| 参数                                          | 类型            | 是否必需 | 描述                                                                                                                                                                                                                                    |
|---------------------------------------------|----------|------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `archived`                                  | boolean  | **{dotted-circle}** No | 归档状态。                                                                                                                                                                                                                                 |
| `build_coverage_regex`                      | string   | **{dotted-circle}** No | 测试覆盖率解析（已废弃，计划删除）。                                                                                                                                                                                                                    |
| `id_after`                                  | integer  | **{dotted-circle}** No | ID 大于指定 ID 的项目。                                                                                                                                                                                                                       |
| `id_before`                                 | integer  | **{dotted-circle}** No | ID 小于指定 ID 的项目。                                                                                                                                                                                                                       |
| `imported`                                  | boolean  | **{dotted-circle}** No | 当前用户从外部系统导入的项目。                                                                                                                                                                                                                       |
| `last_activity_after`                       | datetime | **{dotted-circle}** No | 指定时间后具有最后活动的项目。格式：ISO 8601 (`YYYY-MM-DDTHH:MM:SSZ`)。                                                                                                                                                                                  |
| `last_activity_before`                      | datetime | **{dotted-circle}** No | 指定时间前具有最后活动的项目。格式：ISO 8601 (`YYYY-MM-DDTHH:MM:SSZ`)。                                                                                                                                                                                  |
| `membership`                                | boolean  | **{dotted-circle}** No | 当前用户所属的项目。                                                                                                                                                                                                                            |
| `min_access_level`                          | integer  | **{dotted-circle}** No | 当前用户最小[角色 (`access_level`)](members.md#roles)。                                                                                                                                                                                        |
| `order_by`                                  | string   | **{dotted-circle}** No | 返回按 `id`、`name`、`path`、`created_at`、`updated_at`、`last_activity_at` 或 `similarity` 字段排序的项目。`repository_size`、`storage_size`、`packages_size` 或 `wiki_size` 字段只允许管理员使用。`similarity`（引入于 14.1 版本）仅在搜索时可用，并且仅限于当前用户所属的项目。默认是`created_at`。 |
| `owned`                                     | boolean  | **{dotted-circle}** No | 当前用户明确拥有的项目。                                                                                                                                                                                                                          |
| `repository_checksum_failed` **(PREMIUM)**  | boolean  | **{dotted-circle}** No | 存储库校验和计算失败的项目。                                                                                                                                                                                                                        |
| `repository_storage`                        | string   | **{dotted-circle}** No | 存储在 `repository_storage` 上的项目。*（仅管理员）*                                                                                                                                                                                                |
| `search_namespaces`                         | boolean  | **{dotted-circle}** No | 匹配搜索条件时包括上级命名空间。默认为 `false`。                                                                                                                                                                                                          |
| `search`                                    | string   | **{dotted-circle}** No | 返回符合搜索条件的项目列表。                                                                                                                                                                                                                        |
| `simple`                                    | boolean  | **{dotted-circle}** No | 仅返回每个项目的有限字段。如果没有进行身份验证，则为空操作，只返回简单的字段。                                                                                                                                                                                               |
| `sort`                                      | string   | **{dotted-circle}** No | 返回按 `asc` 或 `desc` 顺序排序的项目。默认是 `desc`。                                                                                                                                                                                                |
| `starred`                                   | boolean  | **{dotted-circle}** No | 由当前用户加星标的项目。                                                                                                                                                                                                                          |
| `statistics`                                | boolean  | **{dotted-circle}** No | 包括项目统计。仅对至少具有报告者角色的用户可用。                                                                                                                                                                                                              |
| `topic`                                     | string   | **{dotted-circle}** No | 逗号分隔的主题名称。与所有给定主题匹配的项目。请参阅 `topics` 参数。                                                                                                                                                                                               |
| `topic_id`                                  | integer  | **{dotted-circle}** No | 具有由主题 ID 给出的指定主题的项目。                                                                                                                                                                                                                  |
| `visibility`                                | string   | **{dotted-circle}** No | 可见性 `public`、`internal` 或 `private`。                                                                                                                                                                                                  |
| `wiki_checksum_failed` **(PREMIUM)**        | boolean  | **{dotted-circle}** No | wiki 校验和计算失败的项目。                                                                                                                                                                                                                      |
| `with_custom_attributes`                    | boolean  | **{dotted-circle}** No | 在响应中包含[自定义属性](custom_attributes.md)。*（仅管理员）*                                                                                                                                                                                          |
| `with_issues_enabled`                       | boolean  | **{dotted-circle}** No | 启用了议题功能。                                                                                                                                                                                                                              |
| `with_merge_requests_enabled`               | boolean  | **{dotted-circle}** No | 启用了合并请求功能。                                                                                                                                                                                                                            |
| `with_programming_language`                 | string   | **{dotted-circle}** No | 使用给定编程语言的项目。                                                                                                                                                                                                                          |
| `updated_before`                            | datetime | **{dotted-circle}** No | 将结果限制为在指定时间之前最后更新的项目。格式：ISO 8601（`YYYY-MM-DDTHH:MM:SSZ`）。引入于 15.10。要使此过滤器生效，您还必须将 `updated_at` 提供为 `order_by` 参数。                                                                                                                       |
| `updated_after`                             | datetime | **{dotted-circle}** No | 将结果限制为在指定时间之后最后更新的项目。格式：ISO 8601（`YYYY-MM-DDTHH:MM:SSZ`）。引入于 15.10。要使此过滤器生效，您还必须将 `updated_at` 提供为 `order_by` 参数。                                                                                                                       |

此端点支持所选 `order_by` 选项的 [keyset 分页](rest/index.md#keyset-based-pagination)。

当 `simple=true` 或用户未经身份验证时，将返回如下内容：

请求示例：

```shell
curl --request GET "https://gitlab.example.com/api/v4/projects"
```

响应示例：

```json
[
  {
    "id": 4,
    "description": null,
    "name": "Diaspora Client",
    "name_with_namespace": "Diaspora / Diaspora Client",
    "path": "diaspora-client",
    "path_with_namespace": "diaspora/diaspora-client",
    "created_at": "2013-09-30T13:46:02Z",
    "default_branch": "main",
    "tag_list": [
      "example",
      "disapora client"
    ],
    "topics": [
      "example",
      "disapora client"
    ],
    "ssh_url_to_repo": "git@gitlab.example.com:diaspora/diaspora-client.git",
    "http_url_to_repo": "https://gitlab.example.com/diaspora/diaspora-client.git",
    "web_url": "https://gitlab.example.com/diaspora/diaspora-client",
    "avatar_url": "https://gitlab.example.com/uploads/project/avatar/4/uploads/avatar.png",
    "star_count": 0,
    "last_activity_at": "2013-09-30T13:46:02Z",
    "namespace": {
      "id": 2,
      "name": "Diaspora",
      "path": "diaspora",
      "kind": "group",
      "full_path": "diaspora",
      "parent_id": null,
      "avatar_url": null,
      "web_url": "https://gitlab.example.com/diaspora"
    }
  },
  {
    ...
  }
```

当用户通过身份验证且未设置 `simple` 时，将返回如下内容：

```json
[
  {
    "id": 4,
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    "description_html": "<p data-sourcepos=\"1:1-1:56\" dir=\"auto\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
    "name": "Diaspora Client",
    "name_with_namespace": "Diaspora / Diaspora Client",
    "path": "diaspora-client",
    "path_with_namespace": "diaspora/diaspora-client",
    "created_at": "2013-09-30T13:46:02Z",
    "updated_at": "2013-09-30T13:46:02Z",
    "default_branch": "main",
    "tag_list": [ //deprecated, use `topics` instead
      "example",
      "disapora client"
    ],
    "topics": [
      "example",
      "disapora client"
    ],
    "ssh_url_to_repo": "git@gitlab.example.com:diaspora/diaspora-client.git",
    "http_url_to_repo": "https://gitlab.example.com/diaspora/diaspora-client.git",
    "web_url": "https://gitlab.example.com/diaspora/diaspora-client",
    "readme_url": "https://gitlab.example.com/diaspora/diaspora-client/blob/master/README.md",
    "avatar_url": "https://gitlab.example.com/uploads/project/avatar/4/uploads/avatar.png",
    "forks_count": 0,
    "star_count": 0,
    "last_activity_at": "2022-06-24T17:11:26.841Z",
    "namespace": {
      "id": 3,
      "name": "Diaspora",
      "path": "diaspora",
      "kind": "group",
      "full_path": "diaspora",
      "parent_id": null,
      "avatar_url": "https://gitlab.example.com/uploads/project/avatar/6/uploads/avatar.png",
      "web_url": "https://gitlab.example.com/diaspora"
    },
    "container_registry_image_prefix": "registry.gitlab.example.com/diaspora/diaspora-client",
    "_links": {
      "self": "https://gitlab.example.com/api/v4/projects/4",
      "issues": "https://gitlab.example.com/api/v4/projects/4/issues",
      "merge_requests": "https://gitlab.example.com/api/v4/projects/4/merge_requests",
      "repo_branches": "https://gitlab.example.com/api/v4/projects/4/repository/branches",
      "labels": "https://gitlab.example.com/api/v4/projects/4/labels",
      "events": "https://gitlab.example.com/api/v4/projects/4/events",
      "members": "https://gitlab.example.com/api/v4/projects/4/members",
      "cluster_agents": "https://gitlab.example.com/api/v4/projects/4/cluster_agents"
    },
    "packages_enabled": true,
    "empty_repo": false,
    "archived": false,
    "visibility": "public",
    "resolve_outdated_diff_discussions": false,
    "container_expiration_policy": {
      "cadence": "1month",
      "enabled": true,
      "keep_n": 1,
      "older_than": "14d",
      "name_regex": "",
      "name_regex_keep": ".*-main",
      "next_run_at": "2022-06-25T17:11:26.865Z"
    },
    "issues_enabled": true,
    "merge_requests_enabled": true,
    "wiki_enabled": true,
    "jobs_enabled": true,
    "snippets_enabled": true,
    "container_registry_enabled": true,
    "service_desk_enabled": true,
    "can_create_merge_request_in": true,
    "issues_access_level": "enabled",
    "repository_access_level": "enabled",
    "merge_requests_access_level": "enabled",
    "forking_access_level": "enabled",
    "wiki_access_level": "enabled",
    "builds_access_level": "enabled",
    "snippets_access_level": "enabled",
    "pages_access_level": "enabled",
    "analytics_access_level": "enabled",
    "container_registry_access_level": "enabled",
    "security_and_compliance_access_level": "private",
    "emails_disabled": null,
    "shared_runners_enabled": true,
    "group_runners_enabled": true,
    "lfs_enabled": true,
    "creator_id": 1,
    "import_url": null,
    "import_type": null,
    "import_status": "none",
    "import_error": null,
    "open_issues_count": 0,
    "ci_default_git_depth": 20,
    "ci_forward_deployment_enabled": true,
    "ci_allow_fork_pipelines_to_run_in_parent_project": true,
    "ci_job_token_scope_enabled": false,
    "ci_separated_caches": true,
    "public_jobs": true,
    "build_timeout": 3600,
    "auto_cancel_pending_pipelines": "enabled",
    "ci_config_path": "",
    "shared_with_groups": [],
    "only_allow_merge_if_pipeline_succeeds": false,
    "allow_merge_on_skipped_pipeline": null,
    "restrict_user_defined_variables": false,
    "request_access_enabled": true,
    "only_allow_merge_if_all_discussions_are_resolved": false,
    "remove_source_branch_after_merge": true,
    "printing_merge_request_link_enabled": true,
    "merge_method": "merge",
    "squash_option": "default_off",
    "enforce_auth_checks_on_uploads": true,
    "suggestion_commit_message": null,
    "merge_commit_template": null,
    "squash_commit_template": null,
    "issue_branch_template": "gitlab/%{id}-%{title}",
    "auto_devops_enabled": false,
    "auto_devops_deploy_strategy": "continuous",
    "autoclose_referenced_issues": true,
    "keep_latest_artifact": true,
    "runner_token_expiration_interval": null,
    "external_authorization_classification_label": "",
    "requirements_enabled": false,
    "requirements_access_level": "enabled",
    "security_and_compliance_enabled": false,
    "compliance_frameworks": [],
    "permissions": {
      "project_access": null,
      "group_access": null
    }
  },
  {
    ...
  }
]
```


您可以通过[自定义属性](custom_attributes.md)过滤：

```plaintext
GET /projects?custom_attributes[key]=value&custom_attributes[other_key]=other_value
```

请求示例：

```shell
curl --globoff --request GET "https://gitlab.example.com/api/v4/projects?custom_attributes[location]=Antarctica&custom_attributes[role]=Developer"
```

### 分页限制

在 13.0 及更高版本，[offset-based 分页](rest/index.md#offset-based-pagination)限制为 50000 条记录。[Keyset 分页](rest/index.md#keyset-based-pagination)需要检索超出此限制的项目。

Keyset 分页仅支持 `order_by=id`。其他排序选项不可用。

<a id="list-user-projects"></a>

## 列出用户项目

> 响应中的 `_links.cluster_agents` 参数引入于 14.10 版本。

获取给定用户拥有的可见项目列表。未经身份验证访问时，仅返回公开项目。

NOTE:
仅返回用户（在 `user_id` 中指定）命名空间中的项目。不返回任何群组或子组中用户拥有的项目。如果配置文件设置为私有，则返回一个空列表。

此端点支持所选 `order_by` 选项的 [keyset 分页](rest/index.md#keyset-based-pagination)。

```plaintext
GET /users/:user_id/projects
```

| 参数                             | 类型       | 是否必需 | 描述                                                                                                              |
|--------------------------------|----------|------------------------|-----------------------------------------------------------------------------------------------------------------|
| `archived`                     | boolean  | **{dotted-circle}** No | 归档状态。                                                                                                           |
| `id_after`                     | integer  | **{dotted-circle}** No | ID 大于指定 ID 的项目。                                                                                                 |
| `id_before`                    | integer  | **{dotted-circle}** No | ID 小于指定 ID 的项目。                                                                                                 |
| `user_id`                      | string   | **{check-circle}** Yes | 用户的 ID 或用户名。                                                                                                    |
| `membership`                   | boolean  | **{dotted-circle}** No | 当前用户所属的项目。                                                                                                      |
| `min_access_level`             | integer  | **{dotted-circle}** No | 当前用户最低[角色 (`access_level`)](members.md#roles)。                                                                  |
| `order_by`                     | string   | **{dotted-circle}** No | 返回按 `id`、`name`、`path`、`created_at`、`updated_at` 或 `last_activity_at` 字段排序的项目。默认是`created_at`。                  |
| `owned`                        | boolean  | **{dotted-circle}** No | 当前用户明确拥有的项目。                                                                                                    |
| `search`                       | string   | **{dotted-circle}** No | 返回符合搜索条件的项目列表。                                                                                                  |
| `simple`                       | boolean  | **{dotted-circle}** No | 仅返回每个项目的有限字段。如果没有进行身份验证，则为空操作，只返回简单的字段。                                                                         |
| `sort`                         | string   | **{dotted-circle}** No | 返回按 `asc` 或 `desc` 顺序排序的项目。默认是 `desc`。                                                                          |
| `starred`                      | boolean  | **{dotted-circle}** No | 由当前用户加星标的项目。                                                                                                    |
| `statistics`                   | boolean  | **{dotted-circle}** No | 包括项目统计。仅对至少具有报告者角色的用户可用。                                                                                        |
| `visibility`                   | string   | **{dotted-circle}** No | 可见性 `public`、`internal` 或 `private`。                                                                            |
| `with_custom_attributes`       | boolean  | **{dotted-circle}** No | 在响应中包含[自定义属性](custom_attributes.md)。*（仅管理员）*                                                                    |
| `with_issues_enabled`          | boolean  | **{dotted-circle}** No | 启用了议题功能。                                                                                                        |
| `with_merge_requests_enabled`  | boolean  | **{dotted-circle}** No | 启用了合并请求功能。                                                                                                      |
| `with_programming_language`    | string   | **{dotted-circle}** No | 使用给定编程语言的项目。                                                                                                    |
| `updated_before`               | datetime | **{dotted-circle}** No | 将结果限制为在指定时间之前最后更新的项目。格式：ISO 8601（`YYYY-MM-DDTHH:MM:SSZ`）。引入于 15.10。要使此过滤器生效，您还必须将 `updated_at` 提供为 `order_by` 参数。 |
| `updated_after`                | datetime | **{dotted-circle}** No | 将结果限制为在指定时间之后最后更新的项目。格式：ISO 8601（`YYYY-MM-DDTHH:MM:SSZ`）。引入于 15.10。要使此过滤器生效，您还必须将 `updated_at` 提供为 `order_by` 参数。 |


```json
[
  {
    "id": 4,
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    "description_html": "<p data-sourcepos=\"1:1-1:56\" dir=\"auto\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
    "default_branch": "master",
    "visibility": "private",
    "ssh_url_to_repo": "git@example.com:diaspora/diaspora-client.git",
    "http_url_to_repo": "http://example.com/diaspora/diaspora-client.git",
    "web_url": "http://example.com/diaspora/diaspora-client",
    "readme_url": "http://example.com/diaspora/diaspora-client/blob/master/README.md",
    "tag_list": [ //deprecated, use `topics` instead
      "example",
      "disapora client"
    ],
    "topics": [
      "example",
      "disapora client"
    ],
    "owner": {
      "id": 3,
      "name": "Diaspora",
      "created_at": "2013-09-30T13:46:02Z"
    },
    "name": "Diaspora Client",
    "name_with_namespace": "Diaspora / Diaspora Client",
    "path": "diaspora-client",
    "path_with_namespace": "diaspora/diaspora-client",
    "issues_enabled": true,
    "open_issues_count": 1,
    "merge_requests_enabled": true,
    "jobs_enabled": true,
    "wiki_enabled": true,
    "snippets_enabled": false,
    "can_create_merge_request_in": true,
    "resolve_outdated_diff_discussions": false,
    "container_registry_enabled": false, // deprecated, use container_registry_access_level instead
    "container_registry_access_level": "disabled",
    "security_and_compliance_access_level": "disabled",
    "created_at": "2013-09-30T13:46:02Z",
    "updated_at": "2013-09-30T13:46:02Z",
    "last_activity_at": "2013-09-30T13:46:02Z",
    "creator_id": 3,
    "import_url": null,
    "import_type": null,
    "import_status": "none",
    "import_error": null,
    "namespace": {
      "id": 3,
      "name": "Diaspora",
      "path": "diaspora",
      "kind": "group",
      "full_path": "diaspora"
    },
    "import_status": "none",
    "archived": false,
    "avatar_url": "http://example.com/uploads/project/avatar/4/uploads/avatar.png",
    "shared_runners_enabled": true,
    "group_runners_enabled": true,
    "forks_count": 0,
    "star_count": 0,
    "runners_token": "b8547b1dc37721d05889db52fa2f02",
    "ci_default_git_depth": 50,
    "ci_forward_deployment_enabled": true,
    "ci_allow_fork_pipelines_to_run_in_parent_project": true,
    "ci_separated_caches": true,
    "public_jobs": true,
    "shared_with_groups": [],
    "only_allow_merge_if_pipeline_succeeds": false,
    "allow_merge_on_skipped_pipeline": false,
    "restrict_user_defined_variables": false,
    "only_allow_merge_if_all_discussions_are_resolved": false,
    "remove_source_branch_after_merge": false,
    "request_access_enabled": false,
    "merge_method": "merge",
    "squash_option": "default_on",
    "autoclose_referenced_issues": true,
    "enforce_auth_checks_on_uploads": true,
    "suggestion_commit_message": null,
    "merge_commit_template": null,
    "squash_commit_template": null,
    "issue_branch_template": "gitlab/%{id}-%{title}",
    "marked_for_deletion_at": "2020-04-03", // Deprecated and will be removed in API v5 in favor of marked_for_deletion_on
    "marked_for_deletion_on": "2020-04-03",
    "statistics": {
      "commit_count": 37,
      "storage_size": 1038090,
      "repository_size": 1038090,
      "wiki_size" : 0,
      "lfs_objects_size": 0,
      "job_artifacts_size": 0,
      "pipeline_artifacts_size": 0,
      "packages_size": 0,
      "snippets_size": 0,
      "uploads_size": 0
    },
    "container_registry_image_prefix": "registry.example.com/diaspora/diaspora-client",
    "_links": {
      "self": "http://example.com/api/v4/projects",
      "issues": "http://example.com/api/v4/projects/1/issues",
      "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
      "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
      "labels": "http://example.com/api/v4/projects/1/labels",
      "events": "http://example.com/api/v4/projects/1/events",
      "members": "http://example.com/api/v4/projects/1/members",
      "cluster_agents": "http://example.com/api/v4/projects/1/cluster_agents"
    }
  },
  {
    "id": 6,
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    "description_html": "<p data-sourcepos=\"1:1-1:56\" dir=\"auto\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
    "default_branch": "master",
    "visibility": "private",
    "ssh_url_to_repo": "git@example.com:brightbox/puppet.git",
    "http_url_to_repo": "http://example.com/brightbox/puppet.git",
    "web_url": "http://example.com/brightbox/puppet",
    "readme_url": "http://example.com/brightbox/puppet/blob/master/README.md",
    "tag_list": [ //deprecated, use `topics` instead
      "example",
      "puppet"
    ],
    "topics": [
      "example",
      "puppet"
    ],
    "owner": {
      "id": 4,
      "name": "Brightbox",
      "created_at": "2013-09-30T13:46:02Z"
    },
    "name": "Puppet",
    "name_with_namespace": "Brightbox / Puppet",
    "path": "puppet",
    "path_with_namespace": "brightbox/puppet",
    "issues_enabled": true,
    "open_issues_count": 1,
    "merge_requests_enabled": true,
    "jobs_enabled": true,
    "wiki_enabled": true,
    "snippets_enabled": false,
    "can_create_merge_request_in": true,
    "resolve_outdated_diff_discussions": false,
    "container_registry_enabled": false, // deprecated, use container_registry_access_level instead
    "container_registry_access_level": "disabled",
    "security_and_compliance_access_level": "disabled",
    "created_at": "2013-09-30T13:46:02Z",
    "updated_at": "2013-09-30T13:46:02Z",
    "last_activity_at": "2013-09-30T13:46:02Z",
    "creator_id": 3,
    "import_url": null,
    "import_type": null,
    "import_status": "none",
    "import_error": null,
    "namespace": {
      "id": 4,
      "name": "Brightbox",
      "path": "brightbox",
      "kind": "group",
      "full_path": "brightbox"
    },
    "import_status": "none",
    "import_error": null,
    "permissions": {
      "project_access": {
        "access_level": 10,
        "notification_level": 3
      },
      "group_access": {
        "access_level": 50,
        "notification_level": 3
      }
    },
    "archived": false,
    "avatar_url": null,
    "shared_runners_enabled": true,
    "group_runners_enabled": true,
    "forks_count": 0,
    "star_count": 0,
    "runners_token": "b8547b1dc37721d05889db52fa2f02",
    "ci_default_git_depth": 0,
    "ci_forward_deployment_enabled": true,
    "ci_allow_fork_pipelines_to_run_in_parent_project": true,
    "ci_separated_caches": true,
    "public_jobs": true,
    "shared_with_groups": [],
    "only_allow_merge_if_pipeline_succeeds": false,
    "allow_merge_on_skipped_pipeline": false,
    "restrict_user_defined_variables": false,
    "only_allow_merge_if_all_discussions_are_resolved": false,
    "remove_source_branch_after_merge": false,
    "request_access_enabled": false,
    "merge_method": "merge",
    "squash_option": "default_on",
    "auto_devops_enabled": true,
    "auto_devops_deploy_strategy": "continuous",
    "repository_storage": "default",
    "approvals_before_merge": 0, // Deprecated. Use merge request approvals API instead.
    "mirror": false,
    "mirror_user_id": 45,
    "mirror_trigger_builds": false,
    "only_mirror_protected_branches": false,
    "mirror_overwrites_diverged_branches": false,
    "external_authorization_classification_label": null,
    "packages_enabled": true,
    "service_desk_enabled": false,
    "service_desk_address": null,
    "autoclose_referenced_issues": true,
    "enforce_auth_checks_on_uploads": true,
    "suggestion_commit_message": null,
    "merge_commit_template": null,
    "squash_commit_template": null,
    "issue_branch_template": "gitlab/%{id}-%{title}",
    "statistics": {
      "commit_count": 12,
      "storage_size": 2066080,
      "repository_size": 2066080,
      "wiki_size" : 0,
      "lfs_objects_size": 0,
      "job_artifacts_size": 0,
      "pipeline_artifacts_size": 0,
      "packages_size": 0,
      "snippets_size": 0,
      "uploads_size": 0
    },
    "container_registry_image_prefix": "registry.example.com/brightbox/puppet",
    "_links": {
      "self": "http://example.com/api/v4/projects",
      "issues": "http://example.com/api/v4/projects/1/issues",
      "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
      "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
      "labels": "http://example.com/api/v4/projects/1/labels",
      "events": "http://example.com/api/v4/projects/1/events",
      "members": "http://example.com/api/v4/projects/1/members",
      "cluster_agents": "http://example.com/api/v4/projects/1/cluster_agents"
    }
  }
]
```


## 列出用户加星标的项目

> 响应中的参数 `_links.cluster_agents` 引入于 14.10 版本。

获取给定用户加星标的可见项目列表。未经身份验证访问时，仅返回公开项目。

```plaintext
GET /users/:user_id/starred_projects
```

| 参数                             | 类型            | 是否必需 | 描述                                                                                             |
|--------------------------------|---------|------------------------|------------------------------------------------------------------------------------------------|
| `user_id`                      | string  | **{check-circle}** Yes | 用户的 ID 或用户名。                                                                                   |
| `archived`                     | boolean  | **{dotted-circle}** No | 归档状态。                                                                                          |
| `membership`                   | boolean | **{dotted-circle}** No | 当前用户所属的项目。                                                                                     |
| `min_access_level`             | integer | **{dotted-circle}** No | 当前用户的最低[角色 (`access_level`)](members.md#roles)。                                                |
| `order_by`                     | string  | **{dotted-circle}** No | 返回按 `id`、`name`、`path`、`created_at`、`updated_at` 或 `last_activity_at` 字段排序的项目。默认是`created_at`。 |
| `owned`                        | boolean | **{dotted-circle}** No | 当前用户明确拥有的项目。                                                                                   |
| `search`                       | string  | **{dotted-circle}** No | 返回符合搜索条件的项目列表。                                                                                 |
| `simple`                       | boolean | **{dotted-circle}** No | 仅返回每个项目的有限字段。如果没有进行身份验证，则为空操作，只返回简单的字段。                                                        |
| `sort`                         | string   | **{dotted-circle}** No | 返回按 `asc` 或 `desc` 顺序排序的项目。默认是 `desc`。                                                         |
| `starred`                      | boolean  | **{dotted-circle}** No | 由当前用户加星标的项目。                                                                                   |
| `statistics`                   | boolean  | **{dotted-circle}** No | 包括项目统计。仅对对至少具有报告者角色的用户可用。                                                                      |
| `visibility`                   | string   | **{dotted-circle}** No | 可见性 `public`、`internal` 或 `private`。                                                           |
| `with_custom_attributes`       | boolean  | **{dotted-circle}** No | 在响应中包含[自定义属性](custom_attributes.md)。*（仅管理员）*                                                   |
| `with_issues_enabled`          | boolean  | **{dotted-circle}** No | 启用了议题功能。                                                                                       |
| `with_merge_requests_enabled`  | boolean  | **{dotted-circle}** No | 启用了合并请求功能。                                                                                     |
| `updated_before`               | datetime | **{dotted-circle}** No | 将结果限制为特定时间之前最后更新的项目。格式：ISO 8601（`YYYY-MM-DDTHH:MM:SSZ`）。引入于 15.10。                               |
| `updated_after`                | datetime | **{dotted-circle}** No | 将结果限制为特定时间之后最后更新的项目。格式：ISO 8601（`YYYY-MM-DDTHH:MM:SSZ`）。引入于 15.10。                               |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/5/starred_projects"
```

响应示例：


```json
[
  {
    "id": 4,
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    "description_html": "<p data-sourcepos=\"1:1-1:56\" dir=\"auto\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
    "default_branch": "master",
    "visibility": "private",
    "ssh_url_to_repo": "git@example.com:diaspora/diaspora-client.git",
    "http_url_to_repo": "http://example.com/diaspora/diaspora-client.git",
    "web_url": "http://example.com/diaspora/diaspora-client",
    "readme_url": "http://example.com/diaspora/diaspora-client/blob/master/README.md",
    "tag_list": [ //deprecated, use `topics` instead
      "example",
      "disapora client"
    ],
    "topics": [
      "example",
      "disapora client"
    ],
    "owner": {
      "id": 3,
      "name": "Diaspora",
      "created_at": "2013-09-30T13:46:02Z"
    },
    "name": "Diaspora Client",
    "name_with_namespace": "Diaspora / Diaspora Client",
    "path": "diaspora-client",
    "path_with_namespace": "diaspora/diaspora-client",
    "issues_enabled": true,
    "open_issues_count": 1,
    "merge_requests_enabled": true,
    "jobs_enabled": true,
    "wiki_enabled": true,
    "snippets_enabled": false,
    "can_create_merge_request_in": true,
    "resolve_outdated_diff_discussions": false,
    "container_registry_enabled": false, // deprecated, use container_registry_access_level instead
    "container_registry_access_level": "disabled",
    "security_and_compliance_access_level": "disabled",
    "created_at": "2013-09-30T13:46:02Z",
    "updated_at": "2013-09-30T13:46:02Z",
    "last_activity_at": "2013-09-30T13:46:02Z",
    "creator_id": 3,
    "namespace": {
      "id": 3,
      "name": "Diaspora",
      "path": "diaspora",
      "kind": "group",
      "full_path": "diaspora"
    },
    "import_status": "none",
    "archived": false,
    "avatar_url": "http://example.com/uploads/project/avatar/4/uploads/avatar.png",
    "shared_runners_enabled": true,
    "group_runners_enabled": true,
    "forks_count": 0,
    "star_count": 0,
    "runners_token": "b8547b1dc37721d05889db52fa2f02",
    "public_jobs": true,
    "shared_with_groups": [],
    "only_allow_merge_if_pipeline_succeeds": false,
    "allow_merge_on_skipped_pipeline": false,
    "restrict_user_defined_variables": false,
    "only_allow_merge_if_all_discussions_are_resolved": false,
    "remove_source_branch_after_merge": false,
    "request_access_enabled": false,
    "merge_method": "merge",
    "squash_option": "default_on",
    "autoclose_referenced_issues": true,
    "enforce_auth_checks_on_uploads": true,
    "suggestion_commit_message": null,
    "merge_commit_template": null,
    "squash_commit_template": null,
    "issue_branch_template": "gitlab/%{id}-%{title}",
    "statistics": {
      "commit_count": 37,
      "storage_size": 1038090,
      "repository_size": 1038090,
      "lfs_objects_size": 0,
      "job_artifacts_size": 0,
      "pipeline_artifacts_size": 0,
      "packages_size": 0,
      "snippets_size": 0,
      "uploads_size": 0
    },
    "container_registry_image_prefix": "registry.example.com/diaspora/diaspora-client",
    "_links": {
      "self": "http://example.com/api/v4/projects",
      "issues": "http://example.com/api/v4/projects/1/issues",
      "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
      "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
      "labels": "http://example.com/api/v4/projects/1/labels",
      "events": "http://example.com/api/v4/projects/1/events",
      "members": "http://example.com/api/v4/projects/1/members",
      "cluster_agents": "http://example.com/api/v4/projects/1/cluster_agents"
    }
  },
  {
    "id": 6,
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    "description_html": "<p data-sourcepos=\"1:1-1:56\" dir=\"auto\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
    "default_branch": "master",
    "visibility": "private",
    "ssh_url_to_repo": "git@example.com:brightbox/puppet.git",
    "http_url_to_repo": "http://example.com/brightbox/puppet.git",
    "web_url": "http://example.com/brightbox/puppet",
    "readme_url": "http://example.com/brightbox/puppet/blob/master/README.md",
    "tag_list": [ //deprecated, use `topics` instead
      "example",
      "puppet"
    ],
    "topics": [
      "example",
      "puppet"
    ],
    "owner": {
      "id": 4,
      "name": "Brightbox",
      "created_at": "2013-09-30T13:46:02Z"
    },
    "name": "Puppet",
    "name_with_namespace": "Brightbox / Puppet",
    "path": "puppet",
    "path_with_namespace": "brightbox/puppet",
    "issues_enabled": true,
    "open_issues_count": 1,
    "merge_requests_enabled": true,
    "jobs_enabled": true,
    "wiki_enabled": true,
    "snippets_enabled": false,
    "can_create_merge_request_in": true,
    "resolve_outdated_diff_discussions": false,
    "container_registry_enabled": false, // deprecated, use container_registry_access_level instead
    "container_registry_access_level": "disabled",
    "security_and_compliance_access_level": "disabled",
    "created_at": "2013-09-30T13:46:02Z",
    "updated_at": "2013-09-30T13:46:02Z",
    "last_activity_at": "2013-09-30T13:46:02Z",
    "creator_id": 3,
    "namespace": {
      "id": 4,
      "name": "Brightbox",
      "path": "brightbox",
      "kind": "group",
      "full_path": "brightbox"
    },
    "import_status": "none",
    "import_error": null,
    "permissions": {
      "project_access": {
        "access_level": 10,
        "notification_level": 3
      },
      "group_access": {
        "access_level": 50,
        "notification_level": 3
      }
    },
    "archived": false,
    "avatar_url": null,
    "shared_runners_enabled": true,
    "group_runners_enabled": true,
    "forks_count": 0,
    "star_count": 0,
    "runners_token": "b8547b1dc37721d05889db52fa2f02",
    "public_jobs": true,
    "shared_with_groups": [],
    "only_allow_merge_if_pipeline_succeeds": false,
    "allow_merge_on_skipped_pipeline": false,
    "restrict_user_defined_variables": false,
    "only_allow_merge_if_all_discussions_are_resolved": false,
    "remove_source_branch_after_merge": false,
    "request_access_enabled": false,
    "merge_method": "merge",
    "squash_option": "default_on",
    "auto_devops_enabled": true,
    "auto_devops_deploy_strategy": "continuous",
    "repository_storage": "default",
    "approvals_before_merge": 0, // Deprecated. Use merge request approvals API instead.
    "mirror": false,
    "mirror_user_id": 45,
    "mirror_trigger_builds": false,
    "only_mirror_protected_branches": false,
    "mirror_overwrites_diverged_branches": false,
    "external_authorization_classification_label": null,
    "packages_enabled": true,
    "service_desk_enabled": false,
    "service_desk_address": null,
    "autoclose_referenced_issues": true,
    "enforce_auth_checks_on_uploads": true,
    "suggestion_commit_message": null,
    "merge_commit_template": null,
    "squash_commit_template": null,
    "issue_branch_template": "gitlab/%{id}-%{title}",
    "statistics": {
      "commit_count": 12,
      "storage_size": 2066080,
      "repository_size": 2066080,
      "lfs_objects_size": 0,
      "job_artifacts_size": 0,
      "pipeline_artifacts_size": 0,
      "packages_size": 0,
      "snippets_size": 0,
      "uploads_size": 0
    },
    "container_registry_image_prefix": "registry.example.com/brightbox/puppet",
    "_links": {
      "self": "http://example.com/api/v4/projects",
      "issues": "http://example.com/api/v4/projects/1/issues",
      "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
      "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
      "labels": "http://example.com/api/v4/projects/1/labels",
      "events": "http://example.com/api/v4/projects/1/events",
      "members": "http://example.com/api/v4/projects/1/members",
      "cluster_agents": "http://example.com/api/v4/projects/1/cluster_agents"
    }
  }
]
```

<a id="get-single-project"></a>

## 获取单个项目

> 响应中的参数 `_links.cluster_agents` 引入于 14.10 版本。

获取特定项目。如果项目可公开访问，则无需身份验证即可访问此端点。

```plaintext
GET /projects/:id
```

| 参数                        | 类型            | 是否必需 | 描述                                                            |
|---------------------------|----------------|------------------------|---------------------------------------------------------------|
| `id`                      | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `license`                 | boolean        | **{dotted-circle}** No | 包括项目许可证数据。                                                    |
| `statistics`              | boolean  | **{dotted-circle}** No | 包括项目统计。仅对至少具有报告者角色的用户可用。                                      |
| `with_custom_attributes`  | boolean  | **{dotted-circle}** No | 在响应中包含[自定义属性](custom_attributes.md)。*（仅管理员）*                  |


```json
{
  "id": 3,
  "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  "description_html": "<p data-sourcepos=\"1:1-1:56\" dir=\"auto\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
  "default_branch": "master",
  "visibility": "private",
  "ssh_url_to_repo": "git@example.com:diaspora/diaspora-project-site.git",
  "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
  "web_url": "http://example.com/diaspora/diaspora-project-site",
  "readme_url": "http://example.com/diaspora/diaspora-project-site/blob/master/README.md",
  "tag_list": [ //deprecated, use `topics` instead
    "example",
    "disapora project"
  ],
  "topics": [
    "example",
    "disapora project"
  ],
  "owner": {
    "id": 3,
    "name": "Diaspora",
    "created_at": "2013-09-30T13:46:02Z"
  },
  "name": "Diaspora Project Site",
  "name_with_namespace": "Diaspora / Diaspora Project Site",
  "path": "diaspora-project-site",
  "path_with_namespace": "diaspora/diaspora-project-site",
  "issues_enabled": true,
  "open_issues_count": 1,
  "merge_requests_enabled": true,
  "jobs_enabled": true,
  "wiki_enabled": true,
  "snippets_enabled": false,
  "can_create_merge_request_in": true,
  "resolve_outdated_diff_discussions": false,
  "container_registry_enabled": false, // deprecated, use container_registry_access_level instead
  "container_registry_access_level": "disabled",
  "security_and_compliance_access_level": "disabled",
  "container_expiration_policy": {
    "cadence": "7d",
    "enabled": false,
    "keep_n": null,
    "older_than": null,
    "name_regex": null, // to be deprecated in GitLab 13.0 in favor of `name_regex_delete`
    "name_regex_delete": null,
    "name_regex_keep": null,
    "next_run_at": "2020-01-07T21:42:58.658Z"
  },
  "created_at": "2013-09-30T13:46:02Z",
  "updated_at": "2013-09-30T13:46:02Z",
  "last_activity_at": "2013-09-30T13:46:02Z",
  "creator_id": 3,
  "namespace": {
    "id": 3,
    "name": "Diaspora",
    "path": "diaspora",
    "kind": "group",
    "full_path": "diaspora",
    "avatar_url": "http://localhost:3000/uploads/group/avatar/3/foo.jpg",
    "web_url": "http://localhost:3000/groups/diaspora"
  },
  "import_url": null,
  "import_type": null,
  "import_status": "none",
  "import_error": null,
  "permissions": {
    "project_access": {
      "access_level": 10,
      "notification_level": 3
    },
    "group_access": {
      "access_level": 50,
      "notification_level": 3
    }
  },
  "archived": false,
  "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
  "license_url": "http://example.com/diaspora/diaspora-client/blob/master/LICENSE",
  "license": {
    "key": "lgpl-3.0",
    "name": "GNU Lesser General Public License v3.0",
    "nickname": "GNU LGPLv3",
    "html_url": "http://choosealicense.com/licenses/lgpl-3.0/",
    "source_url": "http://www.gnu.org/licenses/lgpl-3.0.txt"
  },
  "shared_runners_enabled": true,
  "group_runners_enabled": true,
  "forks_count": 0,
  "star_count": 0,
  "runners_token": "b8bc4a7a29eb76ea83cf79e4908c2b",
  "ci_default_git_depth": 50,
  "ci_forward_deployment_enabled": true,
  "ci_allow_fork_pipelines_to_run_in_parent_project": true,
  "ci_separated_caches": true,
  "public_jobs": true,
  "shared_with_groups": [
    {
      "group_id": 4,
      "group_name": "Twitter",
      "group_full_path": "twitter",
      "group_access_level": 30
    },
    {
      "group_id": 3,
      "group_name": "Gitlab Org",
      "group_full_path": "gitlab-org",
      "group_access_level": 10
    }
  ],
  "repository_storage": "default",
  "only_allow_merge_if_pipeline_succeeds": false,
  "allow_merge_on_skipped_pipeline": false,
  "restrict_user_defined_variables": false,
  "only_allow_merge_if_all_discussions_are_resolved": false,
  "remove_source_branch_after_merge": false,
  "printing_merge_requests_link_enabled": true,
  "request_access_enabled": false,
  "merge_method": "merge",
  "squash_option": "default_on",
  "auto_devops_enabled": true,
  "auto_devops_deploy_strategy": "continuous",
  "approvals_before_merge": 0, // Deprecated. Use merge request approvals API instead.
  "mirror": false,
  "mirror_user_id": 45,
  "mirror_trigger_builds": false,
  "only_mirror_protected_branches": false,
  "mirror_overwrites_diverged_branches": false,
  "external_authorization_classification_label": null,
  "packages_enabled": true,
  "service_desk_enabled": false,
  "service_desk_address": null,
  "autoclose_referenced_issues": true,
  "suggestion_commit_message": null,
  "enforce_auth_checks_on_uploads": true,
  "merge_commit_template": null,
  "squash_commit_template": null,
  "issue_branch_template": "gitlab/%{id}-%{title}",
  "marked_for_deletion_at": "2020-04-03", // Deprecated and will be removed in API v5 in favor of marked_for_deletion_on
  "marked_for_deletion_on": "2020-04-03",
  "compliance_frameworks": [ "sox" ],
  "statistics": {
    "commit_count": 37,
    "storage_size": 1038090,
    "repository_size": 1038090,
    "wiki_size" : 0,
    "lfs_objects_size": 0,
    "job_artifacts_size": 0,
    "pipeline_artifacts_size": 0,
    "packages_size": 0,
    "snippets_size": 0,
    "uploads_size": 0
  },
  "container_registry_image_prefix": "registry.example.com/diaspora/diaspora-client",
  "_links": {
    "self": "http://example.com/api/v4/projects",
    "issues": "http://example.com/api/v4/projects/1/issues",
    "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
    "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
    "labels": "http://example.com/api/v4/projects/1/labels",
    "events": "http://example.com/api/v4/projects/1/events",
    "members": "http://example.com/api/v4/projects/1/members",
    "cluster_agents": "http://example.com/api/v4/projects/1/cluster_agents"
  }
}
```

15.5 及更高版本的[极狐GitLab 旗舰版](https://about.gitlab.cn/pricing/)用户可以查看到 `only_allow_merge_if_all_status_checks_passed` 参数：

```json
{
  "id": 1,
  "project_id": 3,
  "only_allow_merge_if_all_status_checks_passed": false,
  ...
}
```

如果项目是派生项目，则 `forked_from_project` 字段会出现在响应中。
对于该字段，如果上游项目是私有的，则必须提供有效的身份验证令牌。
字段 `mr_default_target_self` 也会出现。如果此值为 `false`，则默认情况下所有合并请求将针对上游项目。

```json
{
   "id":3,

   ...

   "mr_default_target_self": false,
   "forked_from_project":{
      "id":13083,
      "description":"GitLab Community Edition",
      "name":"GitLab Community Edition",
      "name_with_namespace":"GitLab.org / GitLab Community Edition",
      "path":"gitlab-foss",
      "path_with_namespace":"gitlab-org/gitlab-foss",
      "created_at":"2013-09-26T06:02:36.000Z",
      "default_branch":"master",
      "tag_list":[], //deprecated, use `topics` instead
      "topics":[],
      "ssh_url_to_repo":"git@gitlab.com:gitlab-org/gitlab-foss.git",
      "http_url_to_repo":"https://gitlab.com/gitlab-org/gitlab-foss.git",
      "web_url":"https://gitlab.com/gitlab-org/gitlab-foss",
      "avatar_url":"https://gitlab.com/uploads/-/system/project/avatar/13083/logo-extra-whitespace.png",
      "license_url": "https://gitlab.com/gitlab-org/gitlab/-/blob/master/LICENSE",
      "license": {
        "key": "mit",
        "name": "MIT License",
        "nickname": null,
        "html_url": "http://choosealicense.com/licenses/mit/",
        "source_url": "https://opensource.org/licenses/MIT"
      },
      "star_count":3812,
      "forks_count":3561,
      "last_activity_at":"2018-01-02T11:40:26.570Z",
      "namespace": {
            "id": 72,
            "name": "GitLab.org",
            "path": "gitlab-org",
            "kind": "group",
            "full_path": "gitlab-org",
            "parent_id": null
      }
   }

   ...

}
```

<a id="templates-for-issues-and-merge-requests"></a>

### 议题和合并请求的模板 **(PREMIUM)**

> 引入于 13.10 版本。

[专业版或旗舰版](https://gitlab.cn/pricing/)的用户还可以查看 `issues_template` 和 `merge_requests_template` 参数来管理 [议题和合并请求描述模板](../user/project/description_templates.md)。

```json
{
  "id": 3,
  "issues_template": null,
  "merge_requests_template": null,
  ...
}
```

## 获取项目用户

获取项目的用户列表。

```plaintext
GET /projects/:id/users
```

| 参数            | 类型            | 是否必需 | 描述 |
|---------------|----------------|------------------------|-------------|
| `id`          | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `search`      | string         | **{dotted-circle}** No | 搜索特定用户。 |
| `skip_users`  | integer array  | **{dotted-circle}** No | 过滤掉具有指定 ID 的用户。 |

```json
[
  {
    "id": 1,
    "username": "john_smith",
    "name": "John Smith",
    "state": "active",
    "avatar_url": "http://localhost:3000/uploads/user/avatar/1/cd8.jpeg",
    "web_url": "http://localhost:3000/john_smith"
  },
  {
    "id": 2,
    "username": "jack_smith",
    "name": "Jack Smith",
    "state": "blocked",
    "avatar_url": "http://gravatar.com/../e32131cd8.jpeg",
    "web_url": "http://localhost:3000/jack_smith"
  }
]
```

## 列出项目的群组

获取此项目的上级组列表。

```plaintext
GET /projects/:id/groups
```

| 参数                        | 类型            | 是否必需 | 描述                                                            |
|---------------------------|-------------------|------------------------|---------------------------------------------------------------|
| `id`                      | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `search`                  | string            | **{dotted-circle}** No | 搜索特定群组。                                                       |
| `shared_min_access_level` | integer           | **{dotted-circle}** No | 至少具有此[角色 (`access_level`)](members.md#roles)的共享群组。         |
| `shared_visible_only`     | boolean           | **{dotted-circle}** No | 用户有权访问的共享群组。                                                |
| `skip_groups`             | array of integers | **{dotted-circle}** No | 跳过传递的群组 ID。                                                   |
| `with_shared`             | boolean           | **{dotted-circle}** No | 是否包括与该群组共享的项目。默认为 `false`。                                    |

```json
[
  {
    "id": 1,
    "name": "Foobar Group",
    "avatar_url": "http://localhost:3000/uploads/group/avatar/1/foo.jpg",
    "web_url": "http://localhost:3000/groups/foo-bar",
    "full_name": "Foobar Group",
    "full_path": "foo-bar"
  },
  {
    "id": 2,
    "name": "Shared Group",
    "avatar_url": "http://gitlab.example.com/uploads/group/avatar/1/bar.jpg",
    "web_url": "http://gitlab.example.com/groups/foo/bar",
    "full_name": "Shared Group",
    "full_path": "foo/shared"
  }
]
```

## 列出项目的可共享群组

获取可以与项目共享的群组列表。

```plaintext
GET /projects/:id/share_locations
```

| 参数       | 类型                | 是否必需                   | 描述                                                           |
|----------|-------------------|------------------------|--------------------------------------------------------------|
| `id`     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `search` | string            | **{dotted-circle}** No | 搜索特定群组                                                       |

```json
[
  {
    "id": 22,
    "web_url": "http://127.0.0.1:3000/groups/gitlab-org",
    "name": "Gitlab Org",
    "avatar_url": null,
    "full_name": "Gitlab Org",
    "full_path": "gitlab-org"
  },
  {
    "id": 25,
    "web_url": "http://127.0.0.1:3000/groups/gnuwget",
    "name": "Gnuwget",
    "avatar_url": null,
    "full_name": "Gnuwget",
    "full_path": "gnuwget"
  }
]
```

## 获取项目事件

请参阅[事件 API 文档](events.md#list-a-projects-visible-events)。

<a id="create-project"></a>

## 创建项目

> `operations_access_level` 移除于极狐GitLab 16.0。

创建一个由经过身份验证的用户拥有的新项目。

如果您的 HTTP 仓库不可公开访问，请将身份验证信息添加到 URL `https://username:password@gitlab.company.com/group/project.git`，其中 `password` 是启用了 `api` 范围 的公共访问密钥。

```plaintext
POST /projects
```

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your-token>" \
     --header "Content-Type: application/json" --data '{
        "name": "new_project", "description": "New Project", "path": "new_project",
        "namespace_id": "42", "initialize_with_readme": "true"}' \
     --url 'https://gitlab.example.com/api/v4/projects/'
```

| 参数                                                            | 类型            | 是否必需                                        | 描述                                                                                                                                                                                                                                                                                                                           |
|---------------------------------------------------------------|---------|---------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `name`                                                        | string  | **{check-circle}** Yes（如果没有提供 `path`）       | 新项目的名称。如果未提供，则等于路径。                                                                                                                                                                                                                                                                                                          |
| `path`                                                        | string  | **{check-circle}** Yes（如果没有提供 `name`）       | 新项目的仓库名称。如果未提供，则根据名称生成（生成为带有破折号的小写字母）。从 14.9 版本开始，路径不得以特殊字符开头或结尾，并且不得包含连续的特殊字符。                                                                                                                                                                                                                                              |
| `allow_merge_on_skipped_pipeline`                             | boolean | **{dotted-circle}** No                      | 设置合并请求是否可以与跳过的作业合并。                                                                                                                                                                                                                                                                                                          |
| `only_allow_merge_if_all_status_checks_passed` **(ULTIMATE)** | boolean | **{dotted-circle}** No                      | 指示应阻止合并请求的合并，除非所有状态检查都已通过。默认为 faulse。引入于极狐GitLab 15.5，默认禁用，功能标志为 `only_allow_merge_if_all_status_checks_passed`。                                                                                                                                                                                                             |
| `analytics_access_level`                                      | string  | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `approvals_before_merge` **(PREMIUM)**                        | integer | **{dotted-circle}** No                      | 默认情况下应有多少审核者批准合并请求。要配置审批规则，请参阅[合并请求审批 API](merge_request_approvals.md)。废弃于 16.0。                                                                                                                                                                                                                                             |
| `auto_cancel_pending_pipelines`                               | string  | **{dotted-circle}** No                      | 自动取消挂起的流水线。此操作在启用状态和禁用状态之间切换，不是布尔值。                                                                                                                                                                                                                                                                                          |
| `auto_devops_deploy_strategy`                                 | string  | **{dotted-circle}** No                      | Auto Deploy 策略（`continuous`、`manual` 或 `timed_incremental`）。                                                                                                                                                                                                                                                                 |
| `auto_devops_enabled`                                         | boolean | **{dotted-circle}** No                      | 为此项目启用 Auto DevOps。                                                                                                                                                                                                                                                                                                          |
| `autoclose_referenced_issues`                                 | boolean | **{dotted-circle}** No                      | 设置是否在默认分支上自动关闭引用的议题。                                                                                                                                                                                                                                                                                                         |
| `avatar`                                                      | mixed   | **{dotted-circle}** No                      | 项目头像的图像文件。                                                                                                                                                                                                                                                                                                                   |
| `build_git_strategy`                                          | string  | **{dotted-circle}** No                      | Git 策略。默认为 `fetch`。                                                                                                                                                                                                                                                                                                          |
| `build_timeout`                                               | integer | **{dotted-circle}** No                      | 作业可以运行的最长时间（以秒为单位）。                                                                                                                                                                                                                                                                                                          |
| `builds_access_level`                                         | string  | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `ci_config_path`                                              | string  | **{dotted-circle}** No                      | CI 配置文件的路径。                                                                                                                                                                                                                                                                                                                  |
| `container_expiration_policy_attributes`                      | hash    | **{dotted-circle}** No                      | 更新此项目的镜像清理策略。接受：`cadence`（字符串）、`keep_n`（整数）、`older_than`（字符串）、`name_regex`（字符串）、`name_regex_delete`（字符串）、`name_regex_keep`（字符串）、`enabled`（布尔值）。有关 `cadence`、`keep_n` 和 `older_than` 值的更多信息，请参阅 [Container Registry](../user/packages/container_registry/reduce_container_registry_storage.md#use-the-cleanup-policy-api) 文档。 |
| `container_registry_access_level`                             | string  | **{dotted-circle}** No                      | 将此项目的容器镜像库的可见性设置为 `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                          |
| `container_registry_enabled`                                  | boolean | **{dotted-circle}** No                      | *（已废弃）*为此项目启用容器镜像库。请改用 `container_registry_access_level`。                                                                                                                                                                                                                                                                    |
| `default_branch`                                              | string  | **{dotted-circle}** No                      | [默认分支](../user/project/repository/branches/default.md) 名称。 要求 `initialize_with_readme` 为 `true`。                                                                                                                                                                                                                             |
| `description`                                                 | string  | **{dotted-circle}** No                      | 简短的项目描述。                                                                                                                                                                                                                                                                                                                     |
| `emails_disabled`                                             | boolean | **{dotted-circle}** No                      | 禁用电子邮件通知。                                                                                                                                                                                                                                                                                                                    |
| `external_authorization_classification_label` **(PREMIUM)**   | string  | **{dotted-circle}** No                      | 项目的分类标记。                                                                                                                                                                                                                                                                                                                     |
| `forking_access_level`                                        | string  | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `group_with_project_templates_id` **(PREMIUM)**               | integer | **{dotted-circle}** No                      | 对于群组级自定义模板，指定所有自定义项目模板的来源组的 ID。为实例级模板留空。 要求 `use_custom_template` 为 true。                                                                                                                                                                                                                                                    |
| `import_url`                                                  | string  | **{dotted-circle}** No                      | 从中导入仓库的 URL。当 URL 值不为空时，您不能将 `initialize_with_readme` 设置为 `true`。这样做可能会导致以下错误：`not a git repository`。                                                                                                                                                                                                                        |
| `initialize_with_readme`                                      | boolean | **{dotted-circle}** No                      | 是否仅使用 README.md 文件创建 Git 仓库。默认为 `false`。如果此布尔为 `true`，则不得传递 `import_url` 或此端点的其他属性，这些属性指定仓库的替代内容。这样做可能会导致以下错误：`not a git repository`。                                                                                                                                                                                        |
| `issues_access_level`                                         | string  | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `issues_enabled`                                              | boolean | **{dotted-circle}** No                      | *（已废弃）*为此项目启用议题。请改用 `issues_access_level`。                                                                                                                                                                                                                                                                                   |
| `jobs_enabled`                                                | boolean | **{dotted-circle}** No                      | *（已废弃）*为此项目启用作业。请改用 `builds_access_level`。                                                                                                                                                                                                                                                                                   |
| `lfs_enabled`                                                 | boolean | **{dotted-circle}** No                      | 启用 LFS。                                                                                                                                                                                                                                                                                                                      |
| `merge_method`                                                | string  | **{dotted-circle}** No                      | 设置使用的[合并方法](#project-merge-method)。                                                                                                                                                                                                                                                                                          |
| `merge_pipelines_enabled`                                     | boolean | **{dotted-circle}** No                      | 启用或禁用合并流水线。                                                                                                                                                                                                                                                                                                                  |
| `merge_requests_access_level`                                 | string  | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `merge_requests_enabled`                                      | boolean | **{dotted-circle}** No                      | *（已废弃）*为此项目启用合并请求。请使用 `merge_requests_access_level` 代替。                                                                                                                                                                                                                                                                      |
| `merge_trains_enabled`                                        | boolean | **{dotted-circle}** No                      | 启用或禁用合并队列。                                                                                                                                                                                                                                                                                                                   |
| `mirror_trigger_builds` **(PREMIUM)**                         | boolean | **{dotted-circle}** No                      | 拉镜像触发构建。                                                                                                                                                                                                                                                                                                                     |
| `mirror` **(PREMIUM)**                                        | boolean | **{dotted-circle}** No                      | 在项目中启用拉镜像。                                                                                                                                                                                                                                                                                                                   |
| `namespace_id`                                                | integer | **{dotted-circle}** No                      | 新项目的命名空间（默认为当前用户的命名空间）。                                                                                                                                                                                                                                                                                                      |
| `only_allow_merge_if_all_discussions_are_resolved`            | boolean | **{dotted-circle}** No                      | 设置是否只有在所有讨论都解决后才能合并合并请求。                                                                                                                                                                                                                                                                                                     |
| `only_allow_merge_if_pipeline_succeeds`                       | boolean | **{dotted-circle}** No                      | 设置合并请求是否只能与成功的流水线合并。此设置在项目设置中被命名为[**流水线必须成功**](../user/project/merge_requests/merge_when_pipeline_succeeds.md#require-a-successful-pipeline-for-merge)。                                                                                                                                                                      |
| `packages_enabled`                                            | boolean | **{dotted-circle}** No                      | 启用或禁用软件包库功能。                                                                                                                                                                                                                                                                                                                 |
| `pages_access_level`                                          | string  | **{dotted-circle}** No                      | `disabled`、`private`、`enabled` 或 `public`。                                                                                                                                                                                                                                                                                   |
| `printing_merge_request_link_enabled`                         | boolean | **{dotted-circle}** No                      | 从命令行推送时显示创建/查看合并请求的链接。                                                                                                                                                                                                                                                                                                       |
| `public_builds`                                               | boolean | **{dotted-circle}** No                      | 如果为 `true`，非项目成员可以查看作业。                                                                                                                                                                                                                                                                                                      |
| `releases_access_level`                                       | string | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `environments_access_level`                                   | string  | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `feature_flags_access_level`                                  | string  | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `infrastructure_access_level`                                 | string  | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `monitor_access_level`                                        | string  | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `remove_source_branch_after_merge`                            | boolean | **{dotted-circle}** No                      | 默认为所有新的合并请求启用**删除源分支**选项。                                                                                                                                                                                                                                                                                                    |
| `repository_access_level`                                     | string  | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `repository_storage`                                          | string  | **{dotted-circle}** No                      | 仓库在哪个存储分片上。（仅限管理员）                                                                                                                                                                                                                                                                                                           |
| `request_access_enabled`                                      | boolean | **{dotted-circle}** No                      | 允许用户请求成员访问。                                                                                                                                                                                                                                                                                                                  |
| `requirements_access_level`                                   | string  | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `resolve_outdated_diff_discussions`                           | boolean | **{dotted-circle}** No                      | 自动解决合并请求差异讨论上通过推送更改的行。                                                                                                                                                                                                                                                                                                       |
| `security_and_compliance_access_level`                        | string  | **{dotted-circle}** No                      | （14.9 及更高版本）安全性和合规性访问级别。`disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                    |
| `shared_runners_enabled`                                      | boolean | **{dotted-circle}** No                      | 为此项目启用共享 Runner。                                                                                                                                                                                                                                                                                                             |
| `group_runners_enabled`                                       | boolean | **{dotted-circle}** No                     | 为项目启用群组 Runner。                                                                                                                                                                                                                                                                                                              |
| `snippets_access_level`                                       | string  | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `snippets_enabled`                                            | boolean | **{dotted-circle}** No                      | *（已废弃）*为此项目启用代码片段。请使用 `snippets_access_level` 代替。                                                                                                                                                                                                                                                                            |
| `squash_option`                                               | string  | **{dotted-circle}** No                      | `never`、`always`、`default_on` 或 `default_off`。                                                                                                                                                                                                                                                                               |
| `tag_list`                                                    | array   | **{dotted-circle}** No                      | （废弃于 14.0 版本）项目的标签列表；放置最终应分配给项目的标签数组。请使用 `topics` 代替。                                                                                                                                                                                                                                                                        |
| `template_name`                                               | string  | **{dotted-circle}** No                      | 当不使用 `use_custom_template`时，[内置项目模板](../user/project/index.md#create-a-project-from-a-built-in-template)的名称。当与 `use_custom_template` 一起使用时，自定义项目模板的名称。                                                                                                                                                                       |
| `template_project_id` **(PREMIUM)**                           | integer | **{dotted-circle}** No                      | 与 `use_custom_template` 一起使用时，自定义项目模板的项目 ID。使用项目 ID 优于使用 `template_name`，因为 `template_name` 可能不明确。                                                                                                                                                                                                                               |
| `topics`                                                      | array   | **{dotted-circle}** No                      | 项目的主题列表；放置最终应分配给项目主题数组。引入于 14.0 版本。                                                                                                                                                                                                                                                                                          |
| `use_custom_template` **(PREMIUM)**                           | boolean | **{dotted-circle}** No                      | 使用自定义[实例](../user/admin_area/custom_project_templates.md)或[群组](../user/group/custom_project_templates.md)（带有 `group_with_project_templates_id`）的项目模板。                                                                                                                                                                        |
| `visibility`                                                  | string  | **{dotted-circle}** No                      | 请参阅[项目可见性级别](#project-visibility-level)。                                                                                                                                                                                                                                                                                     |
| `wiki_access_level`                                           | string  | **{dotted-circle}** No                      | `disabled`、`private` 或 `enabled`。                                                                                                                                                                                                                                                                                            |
| `wiki_enabled`                                                | boolean | **{dotted-circle}** No                      | *（已废弃）*为此项目启用 wiki。使用 `wiki_access_level` 代替。                                                                                                                                                                                                                                                                                |

## 为用户创建项目

> `operations_access_level` 移除于极狐GitLab 16.0。

创建指定用户拥有的新项目。仅适用于管理员。

如果您的 HTTP 仓库不可公开访问，请将身份验证信息添加到 URL `https://username:password@gitlab.company.com/group/project.git`，其中 `password` 是带有已启用 `api` 范围的公共访问密钥。

```plaintext
POST /projects/user/:user_id
```

| 参数                                                            | 类型      | 是否必需 | 描述                                                                                                                                                     |
|---------------------------------------------------------------|---------|------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|
| `user_id`                                                     | integer | **{check-circle}** Yes | 项目所有者的用户 ID。                                                                                                                                           |
| `name`                                                        | string  | **{check-circle}** Yes | 新项目的名称。                                                                                                                                                |
| `allow_merge_on_skipped_pipeline`                             | boolean | **{dotted-circle}** No | 设置合并请求是否可以与跳过的作业合并。                                                                                                                                    |
| `only_allow_merge_if_all_status_checks_passed` **(ULTIMATE)** | boolean | **{dotted-circle}** No  | 指示应阻止合并请求的合并，除非所有状态检查都已通过。默认为 faulse。引入于极狐GitLab 15.5，默认禁用，功能标志为 `only_allow_merge_if_all_status_checks_passed`。                                       |
| `analytics_access_level`                                      | string  | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                      |
| `approvals_before_merge` **(PREMIUM)**                        | integer | **{dotted-circle}** No | 默认情况下应有多少审核者批准合并请求。废弃于 16.0。要配置审批规则，请参阅[合并请求审批 API](merge_request_approvals.md)。                                                                       |
| `auto_cancel_pending_pipelines`                               | string  | **{dotted-circle}** No | 自动取消挂起的流水线。此操作在启用状态和禁用状态之间切换，不是布尔值。                                                                                                                    |
| `auto_devops_deploy_strategy`                                 | string  | **{dotted-circle}** No | Auto Deploy 策略（`continuous`、`manual` 或 `timed_incremental`）。                                                                                           |
| `auto_devops_enabled`                                         | boolean | **{dotted-circle}** No | 为此项目启用 Auto DevOps。                                                                                                                                    |
| `autoclose_referenced_issues`                                 | boolean | **{dotted-circle}** No | 设置是否在默认分支上自动关闭引用的议题。                                                                                                                                   |
| `avatar`                                                      | mixed   | **{dotted-circle}** No | 项目头像的图像文件。                                                                                                                                             |
| `build_git_strategy`                                          | string  | **{dotted-circle}** No | Git 策略。默认为 `fetch`。                                                                                                                                    |
| `build_timeout`                                               | integer | **{dotted-circle}** No | 作业可以运行的最长时间（以秒为单位）。                                                                                                                                    |
| `builds_access_level`                                         | string  | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                      |
| `ci_config_path`                                              | string  | **{dotted-circle}** No | CI 配置文件的路径。                                                                                                                                            |
| `container_registry_access_level`                             | string  | **{dotted-circle}** No | 将此项目的容器镜像库的可见性设置为 `disabled`、`private` 或 `enabled`。                                                                                                    |
| `container_registry_enabled`                                  | boolean | **{dotted-circle}** No | *（已废弃）*为此项目启用容器镜像库。请改用 `container_registry_access_level`。                                                                                              |
| `default_branch`                                              | string  | **{dotted-circle}** No | [默认分支](../user/project/repository/branches/default.md) 名称。 要求 `initialize_with_readme` 为 `true`。                                                       |
| `description`                                                 | string  | **{dotted-circle}** No | 简短的项目描述。                                                                                                                                               |
| `emails_disabled`                                             | boolean | **{dotted-circle}** No | 禁用电子邮件通知。                                                                                                                                              |
| `enforce_auth_checks_on_uploads`                              | boolean | **{dotted-circle}** No | 在上传上执行[授权检查](../security/user_file_uploads.md#enable-authorization-checks-for-all-media-files)。                                                        |
| `external_authorization_classification_label` **(PREMIUM)**   | string  | **{dotted-circle}** No | 项目的分类标记。                                                                                                                                               |
| `forking_access_level`                                        | string  | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                      |
| `group_with_project_templates_id` **(PREMIUM)**               | integer | **{dotted-circle}** No | 对于群组级自定义模板，指定所有自定义项目模板的来源组的 ID。为实例级模板留空。 要求 `use_custom_template` 为 true。                                                                              |
| `import_url`                                                  | string  | **{dotted-circle}** No | 从中导入仓库的 URL。                                                                                                                                           |
| `initialize_with_readme`                                      | boolean | **{dotted-circle}** No | 默认为 `false`。                                                                                                                                           |
| `issues_access_level`                                         | string  | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                      |
| `issues_enabled`                                              | boolean | **{dotted-circle}** No | *（已废弃）*为此项目启用议题。请改用 `issues_access_level`。                                                                                                             |
| `jobs_enabled`                                                | boolean | **{dotted-circle}** No | *（已废弃）*为此项目启用作业。请改用 `builds_access_level`。                                                                                                             |
| `lfs_enabled`                                                 | boolean | **{dotted-circle}** No | 启用 LFS。                                                                                                                                                |
| `merge_commit_template`                                       | string  | **{dotted-circle}** No | 用于在合并请求中创建合并提交消息的[模板](../user/project/merge_requests/commit_templates.md)。引入于 14.5 版本。                                                                 |
| `merge_method`                                                | string  | **{dotted-circle}** No | 设置使用的[合并方法](#project-merge-method)。                                                                                                                    |
| `merge_requests_access_level`                                 | string  | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                      |
| `merge_requests_enabled`                                      | boolean | **{dotted-circle}** No | *（已废弃）*为此项目启用合并请求。请使用 `merge_requests_access_level` 代替。                                                                                                |
| `mirror_trigger_builds` **(PREMIUM)**                         | boolean | **{dotted-circle}** No | 拉镜像触发构建。                                                                                                                                               |
| `mirror` **(PREMIUM)**                                        | boolean | **{dotted-circle}** No | 在项目中启用拉镜像。                                                                                                                                             |
| `namespace_id`                                                | integer | **{dotted-circle}** No | 新项目的命名空间（默认为当前用户的命名空间）。                                                                                                                                |
| `only_allow_merge_if_all_discussions_are_resolved`            | boolean | **{dotted-circle}** No | 设置是否只有在所有讨论都解决后才能合并合并请求。                                                                                                                               |
| `only_allow_merge_if_pipeline_succeeds`                       | boolean | **{dotted-circle}** No | 设置合并请求是否只能与成功的作业合并。                                                                                                                                    |
| `packages_enabled`                                            | boolean | **{dotted-circle}** No | 启用或禁用软件包库功能。                                                                                                                                           |
| `pages_access_level`                                          | string  | **{dotted-circle}** No | `disabled`、`private`、`enabled` 或 `public`。                                                                                                             |
| `path`                                                        | string  | **{dotted-circle}** No | 新项目的自定义存储库名称。默认根据名称生成。                                                                                                                                 |
| `printing_merge_request_link_enabled`                         | boolean | **{dotted-circle}** No | 从命令行推送时显示创建/查看合并请求的链接。                                                                                                                                 |
| `public_builds`                                               | boolean | **{dotted-circle}** No | 如果为 `true`，非项目成员可以查看作业。                                                                                                                                |
| `releases_access_level`                                       | string  |  **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                      |
| `environments_access_level`                                   | string  | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                      |
| `feature_flags_access_level`                                  | string  | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                      |
| `infrastructure_access_level`                                 | string  | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                      |
| `monitor_access_level`                                        | string  | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                      |
| `remove_source_branch_after_merge`                            | boolean | **{dotted-circle}** No | 默认为所有新的合并请求启用**删除源分支**选项。                                                                                                                              |
| `repository_access_level`                                     | string  | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                      |
| `repository_storage`                                          | string  | **{dotted-circle}** No | 仓库在哪个存储分片上。（仅限管理员）                                                                                                                                     |
| `request_access_enabled`                                      | boolean | **{dotted-circle}** No | 允许用户请求成员访问。                                                                                                                                            |
| `requirements_access_level`                                   | string  | **{dotted-circle}** No | `disabled`、`private`、`enabled` 或 `public`。                                                                                                             |
| `resolve_outdated_diff_discussions`                           | boolean | **{dotted-circle}** No | 自动解决合并请求差异讨论上通过推送更改的行。                                                                                                                                 |
| `security_and_compliance_access_level`                        | string  | **{dotted-circle}** No | （14.9 及更高版本）安全性和合规性访问级别。`disabled`、`private` 或 `enabled`。                                                                                              |
| `shared_runners_enabled`                                      | boolean | **{dotted-circle}** No | 为此项目启用共享 Runner。                                                                                                                                       |
| `group_runners_enabled`                                       | boolean | **{dotted-circle}** No  | 启用项目的群组 Runner。                                                                                                                                        |
| `snippets_access_level`                                       | string  | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                      |
| `snippets_enabled`                                            | boolean | **{dotted-circle}** No | *（已废弃）*为此项目启用代码片段。请使用 `snippets_access_level` 代替。                                                                                                      |
| `issue_branch_template`                                       | string  | **{dotted-circle}** No  | 用于为[从议题创建分支](../user/project/merge_requests/creating_merge_requests.md#from-an-issue)的建议名称模板。引入于极狐GitLab 15.6。                                         |
| `squash_commit_template`                                      | string  | **{dotted-circle}** No | 用于在合并请求中创建压缩提交消息的[模板](../user/project/merge_requests/commit_templates.md)。（引入于 14.6 版本）                                                                |
| `squash_option`                                               | string  | **{dotted-circle}** No | `never`、`always`、`default_on` 或 `default_off`。                                                                                                         |
| `suggestion_commit_message`                                   | string  | **{dotted-circle}** No | 用于应用合并请求的提交消息[建议](../user/project/merge_requests/reviews/suggestions.md)。                                                                              |
| `tag_list`                                                    | array   | **{dotted-circle}** No | （废弃于 14.0 版本）项目的标签列表；放置最终应分配给项目的标签数组。请使用 `topics` 代替。                                                                                                  |
| `template_name`                                               | string  | **{dotted-circle}** No | 当不使用 `use_custom_template`时，[内置项目模板](../user/project/index.md#create-a-project-from-a-built-in-template)的名称。当与 `use_custom_template` 一起使用时，自定义项目模板的名称。 |
| `topics`                                                      | array   | **{dotted-circle}** No | 项目的主题列表。（引入于 14.0 版本）                                                                                                                                  |
| `use_custom_template` **(PREMIUM)**                           | boolean | **{dotted-circle}** No | 使用自定义[实例](../user/admin_area/custom_project_templates.md)或[群组](../user/group/custom_project_templates.md)（带有 `group_with_project_templates_id`）的项目模板。  |
| `visibility`                                                  | string  | **{dotted-circle}** No | 请参阅[项目可见性级别](#project-visibility-level)。                                                                                                               |
| `wiki_access_level`                                           | string  | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                      |
| `wiki_enabled`                                                | boolean | **{dotted-circle}** No | *（已废弃）*为此项目启用 wiki。使用 `wiki_access_level` 代替。                                                                                                          |

<a id="edit-project"></a>

## 编辑项目

> `operations_access_level` 移除于极狐GitLab 16.0。

更新现有项目。

如果您的 HTTP 仓库不可公开访问，请将身份验证信息添加到 URL `https://username:password@gitlab.company.com/group/project.git`，其中 `password` 是带有已启用 `api` 范围的公共访问密钥。

```plaintext
PUT /projects/:id
```

例如：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your-token>" \
     --url 'https://jihulab.com/api/v4/projects/<your-project-ID>' \
     --data "shared_runners_enabled=true" # to turn off: "shared_runners_enabled=false"
```

支持的参数：

| 参数                                                            | 类型                | 是否必需 | 描述                                                                                                                                                                                         |
|---------------------------------------------------------------|-------------------|------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                                                          | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。                                                                                                                              |
| `allow_merge_on_skipped_pipeline`                             | boolean           | **{dotted-circle}** No | 设置合并请求是否可以与跳过的作业合并。                                                                                                                                                                        |
| `allow_pipeline_trigger_approve_deployment` **(PREMIUM)**     | boolean        | **{dotted-circle}** No | 设置是否允许流水线触发器批准部署。                                                                                                                                                                          |
| `only_allow_merge_if_all_status_checks_passed` **(ULTIMATE)** | boolean           | **{dotted-circle}** No | 指示应阻止合并请求的合并，除非所有状态检查都已通过。默认为 faulse。<br/><br/>引入于极狐GitLab 15.5，默认禁用，功能标志为 `only_allow_merge_if_all_status_checks_passed`。功能标志默认启用于极狐GitLab 15.9。                                          |
| `analytics_access_level`                                      | string            | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                                                          |
| `approvals_before_merge` **(PREMIUM)**                        | integer           | **{dotted-circle}** No | 默认情况下应有多少审核者批准合并请求。废弃于 16.0。要配置审批规则，请参阅[合并请求审批 API](merge_request_approvals.md)。                                                                                                           |
| `auto_cancel_pending_pipelines`                               | string            | **{dotted-circle}** No | 自动取消挂起的流水线。此操作在启用状态和禁用状态之间切换，不是布尔值。                                                                                                                                                        |
| `auto_devops_deploy_strategy`                                 | string            | **{dotted-circle}** No | Auto Deploy 策略（`continuous`、`manual` 或 `timed_incremental`）。                                                                                                                               |
| `auto_devops_enabled`                                         | boolean           | **{dotted-circle}** No | 为此项目启用 Auto DevOps。                                                                                                                                                                        |
| `autoclose_referenced_issues`                                 | boolean           | **{dotted-circle}** No | 设置是否在默认分支上自动关闭引用的议题。                                                                                                                                                                       |
| `avatar`                                                      | mixed             | **{dotted-circle}** No | 项目头像的图像文件。                                                                                                                                                                                 |
| `build_git_strategy`                                          | string            | **{dotted-circle}** No | Git 策略。默认为 `fetch`。                                                                                                                                                                        |
| `build_timeout`                                               | integer           | **{dotted-circle}** No | 作业可以运行的最长时间（以秒为单位）。                                                                                                                                                                        |
| `builds_access_level`                                         | string            | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                                                          |
| `ci_config_path`                                              | string            | **{dotted-circle}** No | CI 配置文件的路径。                                                                                                                                                                                |
| `ci_default_git_depth`                                        | integer           | **{dotted-circle}** No | [浅克隆](../ci/pipelines/settings.md#limit-the-number-of-changes-fetched-during-clone)的默认修订数。                                                                                                 |
| `ci_forward_deployment_enabled`                               | boolean           | **{dotted-circle}** No | 启用或禁用[防止过期的部署作业](../ci/pipelines/settings.md#prevent-outdated-deployment-jobs)。                                                                                                            |
| `ci_allow_fork_pipelines_to_run_in_parent_project`            | boolean           |  **{dotted-circle}** No | 启用或禁用[在父项目中运行来自派生的合并请求的流水线](../ci/pipelines/merge_request_pipelines.md#run-pipelines-in-the-parent-project)。 _([在 GitLab 15.3 中引入](https://gitlab.com/gitlab-org/gitlab/-/issues/325189)。）_ |
| `ci_separated_caches`                                         | boolean           | **{dotted-circle}** No | 通过分支保护状态设置缓存是否应[分离](../ci/caching/index.md#cache-key-names)。                                                                                                                               |
| `container_expiration_policy_attributes`                      | hash              | **{dotted-circle}** No | 更新此项目的镜像清理策略。 接受：`cadence`（字符串）、`keep_n`（整数）、`older_than`（字符串）、`name_regex`（字符串）、`name_regex_delete`（字符串）、`name_regex_keep`（字符串）、`enabled`（布尔值）。                                           |
| `container_registry_access_level`                             | string            | **{dotted-circle}** No | 将此项目的容器镜像库的可见性设置为 `disabled`、`private` 或 `enabled`。                                                                                                                                        |
| `container_registry_enabled`                                  | boolean           | **{dotted-circle}** No | *（已废弃）*为此项目启用容器镜像库。请改用 `container_registry_access_level`。                                                                                                                                  |
| `default_branch`                                              | string            | **{dotted-circle}** No | [默认分支](../user/project/repository/branches/default.md)名称。                                                                                                                                  |
| `description`                                                 | string            | **{dotted-circle}** No | 简短的项目描述。                                                                                                                                                                                   |
| `emails_disabled`                                             | boolean           | **{dotted-circle}** No | 禁用电子邮件通知。                                                                                                                                                                                  |
| `enforce_auth_checks_on_uploads`                              | boolean           | **{dotted-circle}** No | 在上传上执行[授权检查](../security/user_file_uploads.md#enable-authorization-checks-for-all-media-files)。                                                                                            |
| `external_authorization_classification_label` **(PREMIUM)**   | string            | **{dotted-circle}** No | 项目的分类标记。                                                                                                                                                                                   |
| `forking_access_level`                                        | string            | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                                                          |
| `import_url`                                                  | string            | **{dotted-circle}** No | 从中导入仓库的 URL。                                                                                                                                                                               |
| `issues_access_level`                                         | string            | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                                                          |
| `issues_enabled`                                              | boolean           | **{dotted-circle}** No | *（已废弃）*为此项目启用议题。请改用 `issues_access_level`。                                                                                                                                                 |
| `issues_template` **(PREMIUM)**                               | string            | **{dotted-circle}** No | 议题的默认描述。使用 GFM 解析描述。请参阅[议题和合并请求的模板](#templates-for-issues-and-merge-requests)。                                                                                                             |
| `jobs_enabled`                                                | boolean           | **{dotted-circle}** No | *（已废弃）*为此项目启用作业。请改用 `builds_access_level`。                                                                                                                                                 |
| `keep_latest_artifact`                                        | boolean           | **{dotted-circle}** No | 禁用或启用为此项目保留最新产物的能力。                                                                                                                                                                        |
| `lfs_enabled`                                                 | boolean           | **{dotted-circle}** No | 启用 LFS。                                                                                                                                                                                    |
| `merge_commit_template`                                       | string            | **{dotted-circle}** No | 用于在合并请求中创建合并提交消息的[模板](../user/project/merge_requests/commit_templates.md)。引入于 14.5 版本。                                                                                                     |
| `merge_method`                                                | string            | **{dotted-circle}** No | 设置使用的[合并方法](#project-merge-method)。                                                                                                                                                        |
| `merge_pipelines_enabled`                                     | boolean           | **{dotted-circle}** No | 启用或禁用合并流水线。                                                                                                                                                                                |
| `merge_requests_access_level`                                 | string            | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                                                          |
| `merge_requests_enabled`                                      | boolean           | **{dotted-circle}** No | *（已废弃）*为此项目启用合并请求。请使用 `merge_requests_access_level` 代替。                                                                                                                                    |
| `merge_requests_template` **(PREMIUM)**                       | string            | **{dotted-circle}** No | 合并请求的默认描述。使用 GFM 解析描述。请参阅[议题和合并请求的模板](#templates-for-issues-and-merge-requests)。                                                                                                           |
| `merge_trains_enabled`                                        | boolean           | **{dotted-circle}** No | 启用或禁用合并队列。                                                                                                                                                                                 |
| `mirror_overwrites_diverged_branches` **(PREMIUM)**           | boolean           | **{dotted-circle}** No | 拉镜像覆盖分叉的分支。                                                                                                                                                                                |
| `mirror_trigger_builds` **(PREMIUM)**                         | boolean           | **{dotted-circle}** No | 拉镜像触发构建。                                                                                                                                                                                   |
| `mirror_user_id` **(PREMIUM)**                                | integer           | **{dotted-circle}** No | 负责围绕拉镜像事件的所有活动的用户。（仅限管理员）                                                                                                                                                                  |
| `mirror` **(PREMIUM)**                                        | boolean           | **{dotted-circle}** No | 在项目中启用拉镜像。                                                                                                                                                                                 |
| `mr_default_target_self`                                      | boolean           | **{dotted-circle}** No | 对于派生项目，将合并请求定位到该项目。如果为 `false`，则目标是上游项目。                                                                                                                                                   |
| `name`                                                        | string            | **{dotted-circle}** No | 项目的名称。                                                                                                                                                                                     |
| `only_allow_merge_if_all_discussions_are_resolved`            | boolean           | **{dotted-circle}** No | 设置是否只有在所有讨论都解决后才能合并合并请求。                                                                                                                                                                   |
| `only_allow_merge_if_pipeline_succeeds`                       | boolean           | **{dotted-circle}** No | 设置合并请求是否只能与成功的作业合并。                                                                                                                                                                        |
| `only_mirror_protected_branches` **(PREMIUM)**                | boolean           | **{dotted-circle}** No | 仅镜像受保护的分支。                                                                                                                                                                                 |
| `packages_enabled`                                            | boolean           | **{dotted-circle}** No | 启用或禁用软件包库功能。                                                                                                                                                                               |
| `pages_access_level`                                          | string            | **{dotted-circle}** No | `disabled`、`private`、`enabled` 或 `public`。                                                                                                                                                 |
| `path`                                                        | string            | **{dotted-circle}** No | 项目的自定义仓库名称。默认根据名称生成。                                                                                                                                                                       |
| `printing_merge_request_link_enabled`                         | boolean           | **{dotted-circle}** No | 从命令行推送时显示创建/查看合并请求的链接。                                                                                                                                                                     |
| `public_builds`                                               | boolean           | **{dotted-circle}** No | 如果为 `true`，非项目成员可以查看作业。                                                                                                                                                                    |
| `releases_access_level`                                       | string            | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                                                          |
| `environments_access_level`                                   | string            | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                                                          |
| `feature_flags_access_level`                                  | string            | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                                                          |
| `infrastructure_access_level`                                 | string            | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                                                          |
| `monitor_access_level`                                        | string            | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                                                          |
| `remove_source_branch_after_merge`                            | boolean           | **{dotted-circle}** No | 默认为所有新的合并请求启用**删除源分支**选项。                                                                                                                                                                  |
| `repository_access_level`                                     | string            | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                                                          |
| `repository_storage`                                          | string            | **{dotted-circle}** No | 仓库在哪个存储分片上。（仅限管理员）                                                                                                                                                                         |
| `request_access_enabled`                                      | boolean           | **{dotted-circle}** No | 允许用户请求成员访问。                                                                                                                                                                                |
| `requirements_access_level`                                   | string            | **{dotted-circle}** No | `disabled`、`private`、`enabled` 或 `public`。                                                                                                                                                 |
| `resolve_outdated_diff_discussions`                           | boolean           | **{dotted-circle}** No | 自动解决合并请求差异讨论上通过推送更改的行。                                                                                                                                                                     |
| `restrict_user_defined_variables`                             | boolean           | **{dotted-circle}** No | 仅允许具有维护者角色的用户在触发流水线时传递用户定义的变量。例如，当在 UI、API 或触发器令牌中触发流水线时。                                                                                                                                  |
| `security_and_compliance_access_level`                        | string            | **{dotted-circle}** No | （14.9 及更高版本）安全性和合规性访问级别。`disabled`、`private` 或 `enabled`。                                                                                                                                  |
| `service_desk_enabled`                                        | boolean           | **{dotted-circle}** No | 启用或禁用服务台功能。                                                                                                                                                                                |
| `shared_runners_enabled`                                      | boolean           | **{dotted-circle}** No | 为此项目启用共享 Runner。                                                                                                                                                                           |
| `group_runners_enabled`                                       | boolean           | **{dotted-circle}** No | 启用项目的群组 Runner。                                                                                                                                                                            |
| `snippets_access_level`                                       | string            | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                                                          |
| `snippets_enabled`                                            | boolean           | **{dotted-circle}** No | *（已废弃）* 为此项目启用代码片段。请使用 `snippets_access_level` 代替。                                                                                                                                         |
| `issue_branch_template`                                       | sstring           | **{dotted-circle}** No  | [从议题创建的分支](../user/project/merge_requests/creating_merge_requests.md#from-an-issue)的建议名称模板。引入于极狐GitLab 15.6。                                                                               |
| `squash_commit_template`                                      | string            | **{dotted-circle}** No | 用于在合并请求中创建压缩提交消息的[模板](../user/project/merge_requests/commit_templates.md)。（引入于 14.6 版本）                                                                                                    |
| `squash_option`                                               | string            | **{dotted-circle}** No | `never`、`always`、`default_on` 或 `default_off`。                                                                                                                                             |
| `suggestion_commit_message`                                   | string            | **{dotted-circle}** No | 用于应用合并请求建议的提交消息。                                                                                                                                                                           |
| `tag_list`                                                    | array             | **{dotted-circle}** No | （废弃于 14.0 版本）项目的标签列表；放置最终应分配给项目的标签数组。请使用 `topics` 代替。                                                                                                                                      |
| `topics`                                                      | array             | **{dotted-circle}** No | 项目的主题列表，将替换已添加到项目中的任何现有主题。引入于 14.0 版本。                                                                                                                                                     |
| `visibility`                                                  | string            | **{dotted-circle}** No | 请参阅[项目可见性级别](#project-visibility-level)。                                                                                                                                                   |
| `wiki_access_level`                                           | string            | **{dotted-circle}** No | `disabled`、`private` 或 `enabled`。                                                                                                                                                          |
| `wiki_enabled`                                                | boolean           | **{dotted-circle}** No | *（已废弃）* 为此项目启用 wiki。使用 `wiki_access_level` 代替。                                                                                                                                             |

## 派生项目

将项目派生到经过身份验证的用户的或提供的命名空间中。

项目的派生操作是异步的，并在后台作业中完成。请求立即返回。要确定项目的派生是否已完成，请查询新项目的 `import_status`。

```plaintext
POST /projects/:id/fork
```

| 参数                       | 类型            | 是否必需 | 描述 |
|--------------------------|----------------|------------------------|-------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `description`            | string         | **{dotted-circle}** No | 派生后分配给派生项目的描述。                 |
| `mr_default_target_self` | boolean | **{dotted-circle}** No | 对于派生项目，将合并请求定位到该项目。如果为 `false`，则目标是上游项目。 |
| `name`                   | string         | **{dotted-circle}** No | 派生后分配给派生项目的名称。                        |
| `namespace_id`           | integer        | **{dotted-circle}** No | 项目派生到的命名空间的 ID。                          |
| `namespace_path`         | string         | **{dotted-circle}** No | 项目派生到的命名空间的路径。                         |
| `namespace`              | integer or string | **{dotted-circle}** No | *（已废弃）*项目派生到的命名空间的 ID 或路径。    |
| `path`                   | string         | **{dotted-circle}** No | 派生后分配给派生项目的路径。                        |
| `visibility`             | string         | **{dotted-circle}** No | 派生后分配给派生项目的[可见性级别](#project-visibility-level)。 |

## 列出项目的派生

> 响应中的参数 `_links.cluster_agents` 引入于 14.10 版本。

列出调用用户可访问的与指定项目建立了派生关系的项目。

```plaintext
GET /projects/:id/forks
```

| 参数                            | 类型            | 是否必需 | 描述                                                                                             |
|-------------------------------|----------------|------------------------|------------------------------------------------------------------------------------------------|
| `id`                          | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。                                  |
| `membership`                  | boolean | **{dotted-circle}** No | 当前用户所属的项目。                                                                                     |
| `min_access_level`            | integer | **{dotted-circle}** No | 当前用户最低[角色 (`access_level`)](members.md#roles)。                                                 |
| `order_by`                    | string  | **{dotted-circle}** No | 返回按 `id`、`name`、`path`、`created_at`、`updated_at` 或 `last_activity_at` 字段排序的项目。默认是`created_at`。 |
| `owned`                       | boolean | **{dotted-circle}** No | 当前用户明确拥有的项目。                                                                                   |
| `search`                      | string  | **{dotted-circle}** No | 返回符合搜索条件的项目列表。                                                                                 |
| `simple`                      | boolean | **{dotted-circle}** No | 仅返回每个项目的有限字段。如果没有进行身份验证，则为空操作，只返回简单的字段。                                                        |
| `sort`                        | string   | **{dotted-circle}** No | 返回按 `asc` 或 `desc` 顺序排序的项目。默认是 `desc`。                                                         |
| `starred`                     | boolean  | **{dotted-circle}** No | 由当前用户加星标的项目。                                                                                   |
| `statistics`                  | boolean  | **{dotted-circle}** No | 包括项目统计。仅对至少具有报告者角色的用户可用。                                                                       |
| `visibility`                  | string   | **{dotted-circle}** No | 可见性 `public`、`internal` 或 `private`。                                                           |
| `with_custom_attributes`      | boolean  | **{dotted-circle}** No | 在响应中包含[自定义属性](custom_attributes.md)。*（仅管理员）*                                                   |
| `with_issues_enabled`         | boolean  | **{dotted-circle}** No | 启用了议题功能。                                                                                       |
| `with_merge_requests_enabled` | boolean  | **{dotted-circle}** No | 启用了合并请求功能。                                                                                     |
| `updated_before`              | datetime       | **{dotted-circle}** No | 将结果限制为在指定时间之前最后更新的项目。格式：ISO 8601 (`YYYY-MM-DDTHH:MM:SSZ`)。引入于 15.10。                           |
| `updated_after`               | datetime       | **{dotted-circle}** No | 将结果限制为在指定时间之后最后更新的项目。格式：ISO 8601 (`YYYY-MM-DDTHH:MM:SSZ`)。引入于 15.10。                           |


```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/forks"
```

响应示例：


```json
[
  {
    "id": 3,
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    "description_html": "<p data-sourcepos=\"1:1-1:56\" dir=\"auto\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
    "default_branch": "master",
    "visibility": "internal",
    "ssh_url_to_repo": "git@example.com:diaspora/diaspora-project-site.git",
    "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
    "web_url": "http://example.com/diaspora/diaspora-project-site",
    "readme_url": "http://example.com/diaspora/diaspora-project-site/blob/master/README.md",
    "tag_list": [ //deprecated, use `topics` instead
      "example",
      "disapora project"
    ],
    "topics": [
      "example",
      "disapora project"
    ],
    "name": "Diaspora Project Site",
    "name_with_namespace": "Diaspora / Diaspora Project Site",
    "path": "diaspora-project-site",
    "path_with_namespace": "diaspora/diaspora-project-site",
    "issues_enabled": true,
    "open_issues_count": 1,
    "merge_requests_enabled": true,
    "jobs_enabled": true,
    "wiki_enabled": true,
    "snippets_enabled": false,
    "can_create_merge_request_in": true,
    "resolve_outdated_diff_discussions": false,
    "container_registry_enabled": false, // deprecated, use container_registry_access_level instead
    "container_registry_access_level": "disabled",
    "security_and_compliance_access_level": "disabled",
    "created_at": "2013-09-30T13:46:02Z",
    "updated_at": "2013-09-30T13:46:02Z",
    "last_activity_at": "2013-09-30T13:46:02Z",
    "creator_id": 3,
    "namespace": {
      "id": 3,
      "name": "Diaspora",
      "path": "diaspora",
      "kind": "group",
      "full_path": "diaspora"
    },
    "import_status": "none",
    "archived": true,
    "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
    "shared_runners_enabled": true,
    "group_runners_enabled": true,
    "forks_count": 0,
    "star_count": 1,
    "public_jobs": true,
    "shared_with_groups": [],
    "only_allow_merge_if_pipeline_succeeds": false,
    "allow_merge_on_skipped_pipeline": false,
    "restrict_user_defined_variables": false,
    "only_allow_merge_if_all_discussions_are_resolved": false,
    "remove_source_branch_after_merge": false,
    "request_access_enabled": false,
    "merge_method": "merge",
    "squash_option": "default_on",
    "autoclose_referenced_issues": true,
    "enforce_auth_checks_on_uploads": true,
    "suggestion_commit_message": null,
    "merge_commit_template": null,
    "container_registry_image_prefix": "registry.example.com/diaspora/diaspora-project-site",
    "_links": {
      "self": "http://example.com/api/v4/projects",
      "issues": "http://example.com/api/v4/projects/1/issues",
      "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
      "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
      "labels": "http://example.com/api/v4/projects/1/labels",
      "events": "http://example.com/api/v4/projects/1/events",
      "members": "http://example.com/api/v4/projects/1/members",
      "cluster_agents": "http://example.com/api/v4/projects/1/cluster_agents"
    }
  }
]
```

## 星标一个项目

> > 响应中的参数 `_links.cluster_agents` 引入于 14.10 版本。

为给定项目加注星标。如果项目已加星标，则返回状态码 `304`。

```plaintext
POST /projects/:id/star
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------|----------------|------------------------|-------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/star"
```

响应示例：


```json
{
  "id": 3,
  "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  "description_html": "<p data-sourcepos=\"1:1-1:56\" dir=\"auto\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
  "default_branch": "master",
  "visibility": "internal",
  "ssh_url_to_repo": "git@example.com:diaspora/diaspora-project-site.git",
  "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
  "web_url": "http://example.com/diaspora/diaspora-project-site",
  "readme_url": "http://example.com/diaspora/diaspora-project-site/blob/master/README.md",
  "tag_list": [ //deprecated, use `topics` instead
    "example",
    "disapora project"
  ],
  "topics": [
    "example",
    "disapora project"
  ],
  "name": "Diaspora Project Site",
  "name_with_namespace": "Diaspora / Diaspora Project Site",
  "path": "diaspora-project-site",
  "path_with_namespace": "diaspora/diaspora-project-site",
  "issues_enabled": true,
  "open_issues_count": 1,
  "merge_requests_enabled": true,
  "jobs_enabled": true,
  "wiki_enabled": true,
  "snippets_enabled": false,
  "can_create_merge_request_in": true,
  "resolve_outdated_diff_discussions": false,
  "container_registry_enabled": false, // deprecated, use container_registry_access_level instead
  "container_registry_access_level": "disabled",
  "security_and_compliance_access_level": "disabled",
  "created_at": "2013-09-30T13:46:02Z",
  "updated_at": "2013-09-30T13:46:02Z",
  "last_activity_at": "2013-09-30T13:46:02Z",
  "creator_id": 3,
  "namespace": {
    "id": 3,
    "name": "Diaspora",
    "path": "diaspora",
    "kind": "group",
    "full_path": "diaspora"
  },
  "import_status": "none",
  "archived": true,
  "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
  "license_url": "http://example.com/diaspora/diaspora-client/blob/master/LICENSE",
  "license": {
    "key": "lgpl-3.0",
    "name": "GNU Lesser General Public License v3.0",
    "nickname": "GNU LGPLv3",
    "html_url": "http://choosealicense.com/licenses/lgpl-3.0/",
    "source_url": "http://www.gnu.org/licenses/lgpl-3.0.txt"
  },
  "shared_runners_enabled": true,
  "group_runners_enabled": true,
  "forks_count": 0,
  "star_count": 1,
  "public_jobs": true,
  "shared_with_groups": [],
  "only_allow_merge_if_pipeline_succeeds": false,
  "allow_merge_on_skipped_pipeline": false,
  "restrict_user_defined_variables": false,
  "only_allow_merge_if_all_discussions_are_resolved": false,
  "remove_source_branch_after_merge": false,
  "request_access_enabled": false,
  "merge_method": "merge",
  "squash_option": "default_on",
  "autoclose_referenced_issues": true,
  "enforce_auth_checks_on_uploads": true,
  "suggestion_commit_message": null,
  "merge_commit_template": null,
  "container_registry_image_prefix": "registry.example.com/diaspora/diaspora-project-site",
  "_links": {
    "self": "http://example.com/api/v4/projects",
    "issues": "http://example.com/api/v4/projects/1/issues",
    "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
    "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
    "labels": "http://example.com/api/v4/projects/1/labels",
    "events": "http://example.com/api/v4/projects/1/events",
    "members": "http://example.com/api/v4/projects/1/members",
    "cluster_agents": "http://example.com/api/v4/projects/1/cluster_agents"
  }
}
```

## 取消星标一个项目

> 响应中的参数 `_links.cluster_agents` 引入于 14.10 版本。

取消给定项目的星标。如果项目未加星标，则返回状态码 `304`。

```plaintext
POST /projects/:id/unstar
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------|----------------|------------------------|-------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/unstar"
```

响应示例：


```json
{
  "id": 3,
  "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  "description_html": "<p data-sourcepos=\"1:1-1:56\" dir=\"auto\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
  "default_branch": "master",
  "visibility": "internal",
  "ssh_url_to_repo": "git@example.com:diaspora/diaspora-project-site.git",
  "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
  "web_url": "http://example.com/diaspora/diaspora-project-site",
  "readme_url": "http://example.com/diaspora/diaspora-project-site/blob/master/README.md",
  "tag_list": [ //deprecated, use `topics` instead
    "example",
    "disapora project"
  ],
  "topics": [
    "example",
    "disapora project"
  ],
  "name": "Diaspora Project Site",
  "name_with_namespace": "Diaspora / Diaspora Project Site",
  "path": "diaspora-project-site",
  "path_with_namespace": "diaspora/diaspora-project-site",
  "issues_enabled": true,
  "open_issues_count": 1,
  "merge_requests_enabled": true,
  "jobs_enabled": true,
  "wiki_enabled": true,
  "snippets_enabled": false,
  "can_create_merge_request_in": true,
  "resolve_outdated_diff_discussions": false,
  "container_registry_enabled": false, // deprecated, use container_registry_access_level instead
  "container_registry_access_level": "disabled",
  "security_and_compliance_access_level": "disabled",
  "created_at": "2013-09-30T13:46:02Z",
  "updated_at": "2013-09-30T13:46:02Z",
  "last_activity_at": "2013-09-30T13:46:02Z",
  "creator_id": 3,
  "namespace": {
    "id": 3,
    "name": "Diaspora",
    "path": "diaspora",
    "kind": "group",
    "full_path": "diaspora"
  },
  "import_status": "none",
  "archived": true,
  "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
  "license_url": "http://example.com/diaspora/diaspora-client/blob/master/LICENSE",
  "license": {
    "key": "lgpl-3.0",
    "name": "GNU Lesser General Public License v3.0",
    "nickname": "GNU LGPLv3",
    "html_url": "http://choosealicense.com/licenses/lgpl-3.0/",
    "source_url": "http://www.gnu.org/licenses/lgpl-3.0.txt"
  },
  "shared_runners_enabled": true,
  "group_runners_enabled": true,
  "forks_count": 0,
  "star_count": 0,
  "public_jobs": true,
  "shared_with_groups": [],
  "only_allow_merge_if_pipeline_succeeds": false,
  "allow_merge_on_skipped_pipeline": false,
  "restrict_user_defined_variables": false,
  "only_allow_merge_if_all_discussions_are_resolved": false,
  "remove_source_branch_after_merge": false,
  "request_access_enabled": false,
  "merge_method": "merge",
  "squash_option": "default_on",
  "autoclose_referenced_issues": true,
  "enforce_auth_checks_on_uploads": true,
  "suggestion_commit_message": null,
  "merge_commit_template": null,
  "container_registry_image_prefix": "registry.example.com/diaspora/diaspora-project-site",
  "_links": {
    "self": "http://example.com/api/v4/projects",
    "issues": "http://example.com/api/v4/projects/1/issues",
    "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
    "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
    "labels": "http://example.com/api/v4/projects/1/labels",
    "events": "http://example.com/api/v4/projects/1/events",
    "members": "http://example.com/api/v4/projects/1/members",
    "cluster_agents": "http://example.com/api/v4/projects/1/cluster_agents"
  }
}
```

## 列出项目的星标人

列出为指定项目加注星标的用户。

```plaintext
GET /projects/:id/starrers
```

| 参数                        | 类型            | 是否必需 | 描述 |
|---------------------------|----------------|------------------------|-------------|
| `id`                      | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `search`                  | string         | **{dotted-circle}** No | 搜索特定用户。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/starrers"
```

响应示例：

```json
[
  {
    "starred_since": "2019-01-28T14:47:30.642Z",
    "user": {
        "id": 1,
        "username": "jane_smith",
        "name": "Jane Smith",
        "state": "active",
        "avatar_url": "http://localhost:3000/uploads/user/avatar/1/cd8.jpeg",
        "web_url": "http://localhost:3000/jane_smith"
    }
  },
  {
    "starred_since": "2018-01-02T11:40:26.570Z",
    "user": {
      "id": 2,
      "username": "janine_smith",
      "name": "Janine Smith",
      "state": "blocked",
      "avatar_url": "http://gravatar.com/../e32131cd8.jpeg",
      "web_url": "http://localhost:3000/janine_smith"
    }
  }
]
```

## 语言

获取项目中使用的语言的百分比值。

```plaintext
GET /projects/:id/languages
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------|----------------|------------------------|-------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/languages"
```

响应示例：

```json
{
  "Ruby": 66.69,
  "JavaScript": 22.98,
  "HTML": 7.91,
  "CoffeeScript": 2.42
}
```

## 归档一个项目

> 响应中的参数 `_links.cluster_agents` 引入于 14.10 版本。

如果用户是该项目的管理员或所有者，则归档该项目。此操作是幂等的，因此归档已归档的项目不会更改项目。

```plaintext
POST /projects/:id/archive
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------|----------------|------------------------|-------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/archive"
```

响应示例：


```json
{
  "id": 3,
  "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  "description_html": "<p data-sourcepos=\"1:1-1:56\" dir=\"auto\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
  "default_branch": "master",
  "visibility": "private",
  "ssh_url_to_repo": "git@example.com:diaspora/diaspora-project-site.git",
  "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
  "web_url": "http://example.com/diaspora/diaspora-project-site",
  "readme_url": "http://example.com/diaspora/diaspora-project-site/blob/master/README.md",
  "tag_list": [ //deprecated, use `topics` instead
    "example",
    "disapora project"
  ],
  "topics": [
    "example",
    "disapora project"
  ],
  "owner": {
    "id": 3,
    "name": "Diaspora",
    "created_at": "2013-09-30T13:46:02Z"
  },
  "name": "Diaspora Project Site",
  "name_with_namespace": "Diaspora / Diaspora Project Site",
  "path": "diaspora-project-site",
  "path_with_namespace": "diaspora/diaspora-project-site",
  "issues_enabled": true,
  "open_issues_count": 1,
  "merge_requests_enabled": true,
  "jobs_enabled": true,
  "wiki_enabled": true,
  "snippets_enabled": false,
  "can_create_merge_request_in": true,
  "resolve_outdated_diff_discussions": false,
  "container_registry_enabled": false, // deprecated, use container_registry_access_level instead
  "container_registry_access_level": "disabled",
  "security_and_compliance_access_level": "disabled",
  "created_at": "2013-09-30T13:46:02Z",
  "updated_at": "2013-09-30T13:46:02Z",
  "last_activity_at": "2013-09-30T13:46:02Z",
  "creator_id": 3,
  "namespace": {
    "id": 3,
    "name": "Diaspora",
    "path": "diaspora",
    "kind": "group",
    "full_path": "diaspora"
  },
  "import_status": "none",
  "import_error": null,
  "permissions": {
    "project_access": {
      "access_level": 10,
      "notification_level": 3
    },
    "group_access": {
      "access_level": 50,
      "notification_level": 3
    }
  },
  "archived": true,
  "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
  "license_url": "http://example.com/diaspora/diaspora-client/blob/master/LICENSE",
  "license": {
    "key": "lgpl-3.0",
    "name": "GNU Lesser General Public License v3.0",
    "nickname": "GNU LGPLv3",
    "html_url": "http://choosealicense.com/licenses/lgpl-3.0/",
    "source_url": "http://www.gnu.org/licenses/lgpl-3.0.txt"
  },
  "shared_runners_enabled": true,
  "group_runners_enabled": true,
  "forks_count": 0,
  "star_count": 0,
  "runners_token": "b8bc4a7a29eb76ea83cf79e4908c2b",
  "ci_default_git_depth": 50,
  "ci_forward_deployment_enabled": true,
  "ci_allow_fork_pipelines_to_run_in_parent_project": true,
  "ci_separated_caches": true,
  "public_jobs": true,
  "shared_with_groups": [],
  "only_allow_merge_if_pipeline_succeeds": false,
  "allow_merge_on_skipped_pipeline": false,
  "restrict_user_defined_variables": false,
  "only_allow_merge_if_all_discussions_are_resolved": false,
  "remove_source_branch_after_merge": false,
  "request_access_enabled": false,
  "merge_method": "merge",
  "squash_option": "default_on",
  "autoclose_referenced_issues": true,
  "enforce_auth_checks_on_uploads": true,
  "suggestion_commit_message": null,
  "merge_commit_template": null,
  "container_registry_image_prefix": "registry.example.com/diaspora/diaspora-project-site",
  "_links": {
    "self": "http://example.com/api/v4/projects",
    "issues": "http://example.com/api/v4/projects/1/issues",
    "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
    "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
    "labels": "http://example.com/api/v4/projects/1/labels",
    "events": "http://example.com/api/v4/projects/1/events",
    "members": "http://example.com/api/v4/projects/1/members",
    "cluster_agents": "http://example.com/api/v4/projects/1/cluster_agents"
  }
}
```

## 取消归档一个项目

> 响应中的参数 `_links.cluster_agents` 引入于 14.10 版本。

如果用户是该项目的管理员或所有者，则取消归档该项目。此操作是幂等的，因此取消归档未归档项目不会更改项目。

```plaintext
POST /projects/:id/unarchive
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------|----------------|------------------------|-------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/unarchive"
```

响应示例：


```json
{
  "id": 3,
  "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  "description_html": "<p data-sourcepos=\"1:1-1:56\" dir=\"auto\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
  "default_branch": "master",
  "visibility": "private",
  "ssh_url_to_repo": "git@example.com:diaspora/diaspora-project-site.git",
  "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
  "web_url": "http://example.com/diaspora/diaspora-project-site",
  "readme_url": "http://example.com/diaspora/diaspora-project-site/blob/master/README.md",
  "tag_list": [ //deprecated, use `topics` instead
    "example",
    "disapora project"
  ],
  "topics": [
    "example",
    "disapora project"
  ],
  "owner": {
    "id": 3,
    "name": "Diaspora",
    "created_at": "2013-09-30T13:46:02Z"
  },
  "name": "Diaspora Project Site",
  "name_with_namespace": "Diaspora / Diaspora Project Site",
  "path": "diaspora-project-site",
  "path_with_namespace": "diaspora/diaspora-project-site",
  "issues_enabled": true,
  "open_issues_count": 1,
  "merge_requests_enabled": true,
  "jobs_enabled": true,
  "wiki_enabled": true,
  "snippets_enabled": false,
  "can_create_merge_request_in": true,
  "resolve_outdated_diff_discussions": false,
  "container_registry_enabled": false, // deprecated, use container_registry_access_level instead
  "container_registry_access_level": "disabled",
  "security_and_compliance_access_level": "disabled",
  "created_at": "2013-09-30T13:46:02Z",
  "updated_at": "2013-09-30T13:46:02Z",
  "last_activity_at": "2013-09-30T13:46:02Z",
  "creator_id": 3,
  "namespace": {
    "id": 3,
    "name": "Diaspora",
    "path": "diaspora",
    "kind": "group",
    "full_path": "diaspora"
  },
  "import_status": "none",
  "import_error": null,
  "permissions": {
    "project_access": {
      "access_level": 10,
      "notification_level": 3
    },
    "group_access": {
      "access_level": 50,
      "notification_level": 3
    }
  },
  "archived": false,
  "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
  "license_url": "http://example.com/diaspora/diaspora-client/blob/master/LICENSE",
  "license": {
    "key": "lgpl-3.0",
    "name": "GNU Lesser General Public License v3.0",
    "nickname": "GNU LGPLv3",
    "html_url": "http://choosealicense.com/licenses/lgpl-3.0/",
    "source_url": "http://www.gnu.org/licenses/lgpl-3.0.txt"
  },
  "shared_runners_enabled": true,
  "group_runners_enabled": true,
  "forks_count": 0,
  "star_count": 0,
  "runners_token": "b8bc4a7a29eb76ea83cf79e4908c2b",
  "ci_default_git_depth": 50,
  "ci_forward_deployment_enabled": true,
  "ci_allow_fork_pipelines_to_run_in_parent_project": true,
  "ci_separated_caches": true,
  "public_jobs": true,
  "shared_with_groups": [],
  "only_allow_merge_if_pipeline_succeeds": false,
  "allow_merge_on_skipped_pipeline": false,
  "restrict_user_defined_variables": false,
  "only_allow_merge_if_all_discussions_are_resolved": false,
  "remove_source_branch_after_merge": false,
  "request_access_enabled": false,
  "merge_method": "merge",
  "squash_option": "default_on",
  "autoclose_referenced_issues": true,
  "enforce_auth_checks_on_uploads": true,
  "suggestion_commit_message": null,
  "merge_commit_template": null,
  "container_registry_image_prefix": "registry.example.com/diaspora/diaspora-project-site",
  "_links": {
    "self": "http://example.com/api/v4/projects",
    "issues": "http://example.com/api/v4/projects/1/issues",
    "merge_requests": "http://example.com/api/v4/projects/1/merge_requests",
    "repo_branches": "http://example.com/api/v4/projects/1/repository_branches",
    "labels": "http://example.com/api/v4/projects/1/labels",
    "events": "http://example.com/api/v4/projects/1/events",
    "members": "http://example.com/api/v4/projects/1/members",
    "cluster_agents": "http://example.com/api/v4/projects/1/cluster_agents"
  }
}
```

## 删除项目

<!--> The default behavior of [Delayed project deletion](https://gitlab.com/gitlab-org/gitlab/-/issues/32935) in GitLab 12.6 was changed to [Immediate deletion](https://gitlab.com/gitlab-org/gitlab/-/issues/220382) in GitLab 13.2.-->

此端点：

- 删除项目时包含删除所有关联资源（包括议题和合并请求）。
- 在 13.1 及更高版本，使用[专业版或旗舰版](https://about.gitlab.cn/pricing/)时，如果启用[延迟项目删除](../user/project/settings/index.md#delayed-project-deletion)，则应用。
- 在 15.1 及更高版本，使用[专业版或旗舰版](https://about.gitlab.cn/pricing/)时，如果项目已被标记为删除，则立即删除项目，并传递 `permanently_remove` 和 `full_path` 参数。
- 在 16.0 及更高版本，使用[专业版或旗舰版](https://about.gitlab.cn/pricing/)时，默认启用延迟项目删除。删除将在[默认删除延迟](../user/admin_area/settings/visibility_and_access_controls.md#deletion-protection)中指定的天数后进行。

WARNING:
在 15.9 中，从管理中心的删除保护设置中立即删除项目的选项已被弃用，并在 16.0 中移除。

```plaintext
DELETE /projects/:id
```

| 参数                                 | 类型            | 是否必需 | 描述                                                                                                                         |
|------------------------------------|----------------|------------------------|----------------------------------------------------------------------------------------------------------------------------|
| `id`                               | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。                                                              |
| `permanently_remove` **(PREMIUM)** | boolean/string    | no                     | 如果被标记为删除，则立即删除项目。引入于 15.11。                                                                                                |
| `full_path` **(PREMIUM)**          | string            | no                     | 与 `permanently_remove` 一起使用的项目的完整路径。引入于 15.11。要找到项目路径，请使用[获取单个项目](projects.md#get-single-project)中的 `path_with_namespace`。 |


## 恢复标记为删除的项目。 **(PREMIUM)**

> 引入于 12.6 版本。

恢复标记为删除的项目。

```plaintext
POST /projects/:id/restore
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------|----------------|------------------------|-------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

## 上传文件

> - 最大附件大小实施引入于极狐GitLab 13.11，[功能标志](../administration/feature_flags.md)为 `enforce_max_attachment_size_upload_api`。默认禁用。
> - 最大附件大小在 JiHuLab.com 中启用于极狐GitLab 13.11。
> - 广泛可用于极狐GitLab 15.10。移除功能标志 `enforce_max_attachment_size_upload_api`。

将文件上传到指定项目，用于议题或合并请求描述或评论。

```plaintext
POST /projects/:id/uploads
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------|----------------|------------------------|-------------|
| `file`    | string         | **{check-circle}** Yes | 要上传的文件。 |
| `id`      | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

要从文件系统上传文件，请使用 `--form` 参数。这会导致 cURL 使用标头 `Content-Type: multipart/form-data` 发布数据。`file=` 参数必须指向文件系统上的文件，并且前面有 `@`。例如：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --form "file=@dk.png" "https://gitlab.example.com/api/v4/projects/5/uploads"
```

返回的对象：

```json
{
  "alt": "dk",
  "url": "/uploads/66dbcd21ec5d24ed6ea225176098d52b/dk.png",
  "full_path": "/namespace1/project1/uploads/66dbcd21ec5d24ed6ea225176098d52b/dk.png",
  "markdown": "![dk](/uploads/66dbcd21ec5d24ed6ea225176098d52b/dk.png)"
}
```

返回的 `url` 是相对于项目路径的。返回的 `full_path` 是文件的绝对路径。在 Markdown 上下文中，当使用 `markdown` 中的格式时，链接会被展开。

<a id="max-attachment-size-enforcement"></a>

## 上传项目头像

将头像上传到指定项目。

```plaintext
PUT /projects/:id
```

| 参数                       | 类型            | 是否必需 | 描述 |
|--------------------------|----------------|------------------------|-------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `avatar`                 | string         | **{check-circle}** Yes | 要上传的文件。 |

要从文件系统上传头像，请使用 `--form` 参数。这会导致 cURL 使用标头 `Content-Type: multipart/form-data` 发布数据。`file=` 参数必须指向文件系统上的图像文件，并以`@` 开头。例如：

示例请求：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     --form "avatar=@dk.png" "https://gitlab.example.com/api/v4/projects/5"
```

返回的对象：

```json
{
  "avatar_url": "https://gitlab.example.com/uploads/-/system/project/avatar/2/dk.png"
}
```

## 移除项目头像

> 引入于极狐GitLab 15.4。

要移除项目头像，请在 `avatar` 参数中置空值。

请求示例：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     --data "avatar=" "https://gitlab.example.com/api/v4/projects/5"
```

<a id="share-project-with-group"></a>

## 与群组共享项目

允许与群组共享项目。

```plaintext
POST /projects/:id/share
```

| 参数              | 类型            | 是否必需 | 描述                                                            |
|-----------------|----------------|------------------------|---------------------------------------------------------------|
| `group_access`  | integer        | **{check-circle}** Yes | 授予群组的[角色 (`access_level`)](members.md#roles) 。                |
| `group_id`      | integer        | **{check-circle}** Yes | 要与之共享的群组的 ID。                                                 |
| `id`            | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `expires_at`    | string         | **{dotted-circle}** No | ISO 8601 格式的共享到期日期：2016-09-26                                 |

## 删除群组中的共享项目链接

从群组中取消共享项目。返回 `204` 并且没有成功的内容。

```plaintext
DELETE /projects/:id/share/:group_id
```

| 参数           | 类型            | 是否必需 | 描述 |
|--------------|----------------|------------------------|-------------|
| `group_id`   | integer        | **{check-circle}** Yes | 群组的 ID。 |
| `id`         | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/share/17"
```

## 导入项目成员

从另一个项目导入成员。

```plaintext
POST /projects/:id/import_project_members/:project_id
```

| 参数                   | 类型            | 是否必需 | 描述 |
|--------------|-------------------|------------------------|-------------|
| `id`         | integer or string | **{check-circle}** Yes | 接收成员的目标项目的 ID 或 [URL 编码路径](rest/index.md#namespaced-path-encoding)。 |
| `project_id` | integer or string | **{check-circle}** Yes | 要从中导入成员的源项目的 ID 或 [URL 编码路径](rest/index.md#namespaced-path-encoding)。 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/import_project_members/32"
```

返回：

- `200 OK`：成功。
- `404 Project Not Found`：如果目标或源项目不存在或请求者无法访问。
- `422 Unprocessable Entity`：如果项目成员的导入没有成功完成。

<a id="hooks"></a>

## Hooks

也称为 Project Hooks 和 Webhooks。 这些对于系统范围的 [System Hooks](system_hooks.md) 是不同的。

### 列出项目钩子

获取项目钩子列表。

```plaintext
GET /projects/:id/hooks
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------|----------------|------------------------|-------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

### 获取项目钩子

获取项目的特定钩子。

```plaintext
GET /projects/:id/hooks/:hook_id
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------|----------------|------------------------|---------------------------|
| `hook_id` | integer        | **{check-circle}** Yes | 项目钩子的 ID。 |
| `id`      | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

```json
{
  "id": 1,
  "url": "http://example.com/hook",
  "project_id": 3,
  "push_events": true,
  "push_events_branch_filter": "",
  "issues_events": true,
  "confidential_issues_events": true,
  "merge_requests_events": true,
  "tag_push_events": true,
  "note_events": true,
  "confidential_note_events": true,
  "job_events": true,
  "pipeline_events": true,
  "wiki_page_events": true,
  "deployment_events": true,
  "releases_events": true,
  "enable_ssl_verification": true,
  "repository_update_events": false,
  "alert_status": "executable",
  "disabled_until": null,
  "url_variables": [ ],
  "created_at": "2012-10-12T17:04:47Z"
}
```

### 添加项目钩子

向指定项目添加钩子。

```plaintext
POST /projects/:id/hooks
```

| 参数                           | 类型            | 是否必需 | 描述                                                            |
|------------------------------|----------------|------------------------|---------------------------------------------------------------|
| `id`                         | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `url`                        | string         | **{check-circle}** Yes | 钩子 URL。                                                       |
| `confidential_issues_events` | boolean        | **{dotted-circle}** No | 触发私密议题事件的钩子。                                                  |
| `confidential_note_events`   | boolean        | **{dotted-circle}** No | 触发私密备注事件的钩子。                                                  |
| `deployment_events`          | boolean        | **{dotted-circle}** No | 在部署事件上触发钩子。                                                   |
| `enable_ssl_verification`    | boolean        | **{dotted-circle}** No | 触发钩子时进行 SSL 验证。                                               |
| `issues_events`              | boolean        | **{dotted-circle}** No | 触发议题事件的钩子。                                                    |
| `job_events`                 | boolean        | **{dotted-circle}** No | 触发作业事件的钩子。                                                    |
| `merge_requests_events`      | boolean        | **{dotted-circle}** No | 触发合并请求事件的钩子。                                                  |
| `note_events`                | boolean        | **{dotted-circle}** No | 触发备注事件的钩子。                                                    |
| `pipeline_events`            | boolean        | **{dotted-circle}** No | 触发流水线事件的钩子。                                                   |
| `push_events_branch_filter`  | string         | **{dotted-circle}** No | 仅在匹配分支的推送事件上触发钩子。                                             |
| `push_events`                | boolean        | **{dotted-circle}** No | 触发推送事件的钩子。                                                    |
| `releases_events`            | boolean        | **{dotted-circle}** No | 触发发布事件的钩子。                                                    |
| `tag_push_events`            | boolean        | **{dotted-circle}** No | 触发标签推送事件的钩子。                                                  |
| `token`                      | string         | **{dotted-circle}** No | 用于验证接收到的有效载荷的 secret 令牌；响应中不返回令牌。                             |
| `wiki_page_events`           | boolean        | **{dotted-circle}** No | 触发 wiki 事件的钩子。                                                |

### 编辑项目钩子

编辑指定项目的钩子。

```plaintext
PUT /projects/:id/hooks/:hook_id
```

| 参数                   | 类型            | 是否必需 | 描述                                                                     |
|------------------------------|----------------|------------------------|------------------------------------------------------------------------|
| `hook_id`                    | integer        | **{check-circle}** Yes | 项目钩子 ID。                                                               |
| `id`                         | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。          |
| `url`                        | string         | **{check-circle}** Yes | 钩子 URL。                                                                |
| `confidential_issues_events` | boolean        | **{dotted-circle}** No | 触发私密议题事件的钩子。                                                           |
| `confidential_note_events`   | boolean        | **{dotted-circle}** No | 触发私密备注事件的钩子。                                                           |
| `confidential_note_events`   | boolean        | **{dotted-circle}** No | 触发私密备注事件的钩子。                                                           |
| `deployment_events`          | boolean        | **{dotted-circle}** No | 在部署事件上触发钩子。                                                            |
| `enable_ssl_verification`    | boolean        | **{dotted-circle}** No | 触发钩子时进行 SSL 验证。                                                        |
| `issues_events`              | boolean        | **{dotted-circle}** No | 触发议题事件的钩子。                                                             |
| `job_events`                 | boolean        | **{dotted-circle}** No | 触发作业事件的钩子。                                                             |
| `merge_requests_events`      | boolean        | **{dotted-circle}** No | 触发合并请求事件的钩子。                                                           |
| `note_events`                | boolean        | **{dotted-circle}** No | 触发备注事件的钩子。                                                             |
| `pipeline_events`            | boolean        | **{dotted-circle}** No | 触发流水线事件的钩子。                                                            |
| `push_events_branch_filter`  | string         | **{dotted-circle}** No | 仅在匹配分支的推送事件上触发钩子。                                                      |
| `push_events`                | boolean        | **{dotted-circle}** No | 触发推送事件的钩子。                                                             |
| `releases_events`            | boolean        | **{dotted-circle}** No | 触发发布事件的钩子。                                                             |
| `tag_push_events`            | boolean        | **{dotted-circle}** No | 触发标签推送事件的钩子。                                                           |
| `token`                      | string         | **{dotted-circle}** No | 用于验证接收到的有效载荷的 secret 令牌；不会在响应中返回。当您更改 webhook URL，secret 令牌将被重置并且不会保留。 |
| `wiki_page_events`           | boolean        | **{dotted-circle}** No | 触发 wiki 事件的钩子。                                                         |

### 删除项目钩子

从项目中删除钩子。这是一种幂等方法，可以多次调用。钩子可用或不可用。

```plaintext
DELETE /projects/:id/hooks/:hook_id
```

| 参数                       | 类型            | 是否必需 | 描述 |
|--------------------------|----------------|------------------------|-------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `hook_id`                | integer        | **{check-circle}** Yes | 项目钩子的 ID。 |

请注意，如果钩子可用或不可用，JSON 响应会有所不同。如果项目钩子在 JSON 响应中返回之前可用，或者返回空响应。

## 派生关系

允许修改现有项目之间的派生关系。
仅适用于项目所有者和管理员。

### 在现有项目之间创建派生的 from/to 关系

```plaintext
POST /projects/:id/fork/:forked_from_id
```

| 参数                   | 类型            | 是否必需 | 描述 |
|------------------|----------------|------------------------|-------------|
| `forked_from_id` | ID             | **{check-circle}** Yes | 派生出的项目的 ID。 |
| `id`             | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

### 从关系中删除现有的派生

```plaintext
DELETE /projects/:id/fork
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------|----------------|------------------------|-------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

## 按名称搜索项目

按名称搜索经过身份验证的用户可以访问的项目。如果项目可公开访问，则无需身份验证即可访问此端点。

```plaintext
GET /projects
```

| 参数                   | 类型            | 是否必需 | 描述 |
|------------|--------|------------------------|-------------|
| `search`   | string | **{check-circle}** Yes | 项目名称中包含的字符串。 |
| `order_by` | string | **{dotted-circle}** No | 返回按 `id`、`name`、`created_at` 或 `last_activity_at` 字段排序的请求。 |
| `sort`     | string | **{dotted-circle}** No | 返回按 `asc` 或 `desc` 顺序排序的请求。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects?search=test"
```

## 启动项目的例行维护

```plaintext
POST /projects/:id/housekeeping
```

| 参数                       | 类型            | 是否必需 | 描述                                                            |
|--------------------------|----------------|------------------------|---------------------------------------------------------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `task`                   | string           | **{dotted-circle}** No | 触发无法访问的对象的 `prune` 或触发 eager housekeeping 的 `eager`。          |


## 推送规则 **(PREMIUM)**

### 获取项目的推送规则

获取项目的[推送规则](../user/project/repository/push_rules.md)。

```plaintext
GET /projects/:id/push_rule
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------|----------------|------------------------|-------------|
| `id`      | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

```json
{
  "id": 1,
  "project_id": 3,
  "commit_message_regex": "Fixes \\d+\\..*",
  "commit_message_negative_regex": "ssh\\:\\/\\/",
  "branch_name_regex": "",
  "deny_delete_tag": false,
  "created_at": "2012-10-12T17:04:47Z",
  "member_check": false,
  "prevent_secrets": false,
  "author_email_regex": "",
  "file_name_regex": "",
  "max_file_size": 5,
  "commit_committer_check": false,
  "reject_unsigned_commits": false
}
```

### 添加项目推送规则

向指定项目添加推送规则。

```plaintext
POST /projects/:id/push_rule
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------------------------------------|----------------|------------------------|-------------|
| `id`                                    | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `author_email_regex`                    | string         | **{dotted-circle}** No | 所有提交作者的电子邮件必须与此匹配，例如 `@my-company.com$`。 |
| `branch_name_regex`                     | string         | **{dotted-circle}** No | 所有分支名称都必须与此匹配，例如 `(feature|hotfix)\/*`。 |
| `commit_committer_check`                | boolean        | **{dotted-circle}** No | 如果提交者电子邮件是他们自己经过验证的电子邮件之一，用户只能将提交推送到此仓库。 |
| `commit_message_negative_regex`         | string         | **{dotted-circle}** No | 不允许任何提交消息与此匹配，例如 `ssh\:\/\/`。 |
| `commit_message_regex`                  | string         | **{dotted-circle}** No | 所有提交消息都必须与此匹配，例如 `Fixed \d+\..*`。 |
| `deny_delete_tag`                       | boolean        | **{dotted-circle}** No | 拒绝删除标签。 |
| `file_name_regex`                       | string         | **{dotted-circle}** No | 所有提交的文件名必须**不** 与此匹配，例如 `(jar|exe)$`。 |
| `max_file_size`                         | integer        | **{dotted-circle}** No | 最大文件大小 (MB)。 |
| `member_check`                          | boolean        | **{dotted-circle}** No | 将作者（电子邮件）的提交限制为现有的极狐GitLab 用户。 |
| `prevent_secrets`                       | boolean        | **{dotted-circle}** No | 极狐GitLab 拒绝任何可能包含 secret 的文件。 |
| `reject_unsigned_commits`               | boolean        | **{dotted-circle}** No | 在未通过 GPG 签名时拒绝提交。 |

### 编辑项目推送规则

编辑指定项目的推送规则。

```plaintext
PUT /projects/:id/push_rule
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------------------------------------|----------------|------------------------|-------------|
| `id`                                    | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `author_email_regex`                    | string         | **{dotted-circle}** No | 所有提交作者的电子邮件必须与此匹配，例如 `@my-company.com$`。 |
| `branch_name_regex`                     | string         | **{dotted-circle}** No | 所有分支名称都必须与此匹配，例如 `(feature|hotfix)\/*`。 |
| `commit_committer_check`                | boolean        | **{dotted-circle}** No | 如果提交者电子邮件是他们自己经过验证的电子邮件之一，用户只能将提交推送到此仓库。 |
| `commit_message_negative_regex`         | string         | **{dotted-circle}** No | 不允许任何提交消息与此匹配，例如 `ssh\:\/\/`。 |
| `commit_message_regex`                  | string         | **{dotted-circle}** No | 所有提交消息都必须与此匹配，例如 `Fixed \d+\..*`。 |
| `deny_delete_tag`                       | boolean        | **{dotted-circle}** No | 拒绝删除标签。 |
| `file_name_regex`                       | string         | **{dotted-circle}** No | 所有提交的文件名必须**不**与此匹配，例如 `(jar|exe)$`。 |
| `max_file_size`                         | integer        | **{dotted-circle}** No | 最大文件大小 (MB)。 |
| `member_check`                          | boolean        | **{dotted-circle}** No | 将作者（电子邮件）的提交限制为现有的极狐GitLab 用户。 |
| `prevent_secrets`                       | boolean        | **{dotted-circle}** No | 极狐GitLab 拒绝任何可能包含 secret 的文件。 |
| `reject_unsigned_commits`               | boolean        | **{dotted-circle}** No | 在未通过 GPG 签名时拒绝提交。 |

### 删除项目推送规则

> 移动到专业版于 13.9 版本。

从项目中删除推送规则。这是一种幂等方法，可以多次调用，无论推送规则是否可用。

```plaintext
DELETE /projects/:id/push_rule
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------|----------------|------------------------|-------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

## 获取用户可以将项目转移到的群组

> 引入于极狐GitLab 15.4。

检索用户可以将项目转移到的群组列表。

```plaintext
GET /projects/:id/transfer_locations
```

| 参数       | 类型                | 是否必需                   | 描述                                                    |
|----------|-------------------|------------------------|-------------------------------------------------------|
| `id`     | integer or string | **{check-circle}** Yes | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `search` | string            | **{dotted-circle}** No | 搜索的群组的名称                                              |

请求示例：

```shell
curl --request GET "https://gitlab.example.com/api/v4/projects/1/transfer_locations"
```

响应示例：

```json
[
  {
    "id": 27,
    "web_url": "https://gitlab.example.com/groups/gitlab",
    "name": "GitLab",
    "avatar_url": null,
    "full_name": "GitLab",
    "full_path": "GitLab"
  },
  {
    "id": 31,
    "web_url": "https://gitlab.example.com/groups/foobar",
    "name": "FooBar",
    "avatar_url": null,
    "full_name": "FooBar",
    "full_path": "FooBar"
  }
]
```

## 将项目转移到新的命名空间

> 响应中的参数 `_links.cluster_agents` 引入于 14.10 版本。

有关转移项目的先决条件，请参阅[项目文档](../user/project/settings/index.md#transfer-a-project-to-another-namespace)。

```plaintext
PUT /projects/:id/transfer
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-------------|----------------|------------------------|-------------|
| `id`        | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `namespace` | integer or string | **{check-circle}** Yes | 命名空间的 ID 或路径。 |

请求示例：

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/transfer?namespace=14"
```

响应示例：


```json
  {
  "id": 7,
  "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  "description_html": "<p data-sourcepos=\"1:1-1:56\" dir=\"auto\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>",
  "name": "hello-world",
  "name_with_namespace": "cute-cats / hello-world",
  "path": "hello-world",
  "path_with_namespace": "cute-cats/hello-world",
  "created_at": "2020-10-15T16:25:22.415Z",
  "updated_at": "2020-10-15T16:25:22.415Z",
  "default_branch": "master",
  "tag_list": [], //deprecated, use `topics` instead
  "topics": [],
  "ssh_url_to_repo": "git@gitlab.example.com:cute-cats/hello-world.git",
  "http_url_to_repo": "https://gitlab.example.com/cute-cats/hello-world.git",
  "web_url": "https://gitlab.example.com/cute-cats/hello-world",
  "readme_url": "https://gitlab.example.com/cute-cats/hello-world/-/blob/master/README.md",
  "avatar_url": null,
  "forks_count": 0,
  "star_count": 0,
  "last_activity_at": "2020-10-15T16:25:22.415Z",
  "namespace": {
    "id": 18,
    "name": "cute-cats",
    "path": "cute-cats",
    "kind": "group",
    "full_path": "cute-cats",
    "parent_id": null,
    "avatar_url": null,
    "web_url": "https://gitlab.example.com/groups/cute-cats"
  },
  "container_registry_image_prefix": "registry.example.com/cute-cats/hello-world",
  "_links": {
    "self": "https://gitlab.example.com/api/v4/projects/7",
    "issues": "https://gitlab.example.com/api/v4/projects/7/issues",
    "merge_requests": "https://gitlab.example.com/api/v4/projects/7/merge_requests",
    "repo_branches": "https://gitlab.example.com/api/v4/projects/7/repository/branches",
    "labels": "https://gitlab.example.com/api/v4/projects/7/labels",
    "events": "https://gitlab.example.com/api/v4/projects/7/events",
    "members": "https://gitlab.example.com/api/v4/projects/7/members"
  },
  "packages_enabled": true,
  "empty_repo": false,
  "archived": false,
  "visibility": "private",
  "resolve_outdated_diff_discussions": false,
  "container_registry_enabled": true, // deprecated, use container_registry_access_level instead
  "container_registry_access_level": "enabled",
  "container_expiration_policy": {
    "cadence": "7d",
    "enabled": false,
    "keep_n": null,
    "older_than": null,
    "name_regex": null,
    "name_regex_keep": null,
    "next_run_at": "2020-10-22T16:25:22.746Z"
  },
  "issues_enabled": true,
  "merge_requests_enabled": true,
  "wiki_enabled": true,
  "jobs_enabled": true,
  "snippets_enabled": true,
  "service_desk_enabled": false,
  "service_desk_address": null,
  "can_create_merge_request_in": true,
  "issues_access_level": "enabled",
  "repository_access_level": "enabled",
  "merge_requests_access_level": "enabled",
  "forking_access_level": "enabled",
  "analytics_access_level": "enabled",
  "wiki_access_level": "enabled",
  "builds_access_level": "enabled",
  "snippets_access_level": "enabled",
  "pages_access_level": "enabled",
  "security_and_compliance_access_level": "enabled",
  "emails_disabled": null,
  "shared_runners_enabled": true,
  "group_runners_enabled": true,
  "lfs_enabled": true,
  "creator_id": 2,
  "import_status": "none",
  "open_issues_count": 0,
  "ci_default_git_depth": 50,
  "public_jobs": true,
  "build_timeout": 3600,
  "auto_cancel_pending_pipelines": "enabled",
  "ci_config_path": null,
  "shared_with_groups": [],
  "only_allow_merge_if_pipeline_succeeds": false,
  "allow_merge_on_skipped_pipeline": null,
  "restrict_user_defined_variables": false,
  "request_access_enabled": true,
  "only_allow_merge_if_all_discussions_are_resolved": false,
  "remove_source_branch_after_merge": true,
  "printing_merge_request_link_enabled": true,
  "merge_method": "merge",
  "squash_option": "default_on",
  "suggestion_commit_message": null,
  "merge_commit_template": null,
  "auto_devops_enabled": true,
  "auto_devops_deploy_strategy": "continuous",
  "autoclose_referenced_issues": true,
  "approvals_before_merge": 0, // Deprecated. Use merge request approvals API instead.
  "mirror": false,
  "compliance_frameworks": []
}
```

## 分支

在[分支](branches.md)文档中阅读更多内容。

## 项目导入/导出

在[项目导入/导出](project_import_export.md)文档中阅读更多信息。

## 项目成员

在[项目成员](members.md)文档中阅读更多信息。

## 项目漏洞

在[项目漏洞](project_vulnerabilities.md)文档中阅读更多信息。

## 获取项目的拉取镜像详细信息 **(PREMIUM)**

> 引入于极狐GitLab 15.6。

返回项目的拉取镜像的详细信息。

```plaintext
GET /projects/:id/mirror/pull
```

支持的参数：

| 参数   | 类型                | 是否必需                   | 描述                                                           |
|:-----|:------------------|:-----------------------|:-------------------------------------------------------------|
| `id` | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |

请求示例：

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/mirror/pull"
```

响应示例：

```json
{
  "id": 101486,
  "last_error": null,
  "last_successful_update_at": "2020-01-06T17:32:02.823Z",
  "last_update_at": "2020-01-06T17:32:02.823Z",
  "last_update_started_at": "2020-01-06T17:31:55.864Z",
  "update_status": "finished",
  "url": "https://*****:*****@gitlab.com/gitlab-org/security/gitlab.git"
}
```

<a id="configure-pull-mirroring-for-a-project"></a>

## 为项目配置拉取镜像 **(PREMIUM)**

> - 移动到专业版于极狐GitLab 13.9。
> - 字段 `mirror_branch_regex` 引入于极狐GitLab 15.8，[功能标志](../administration/feature_flags.md)为 `mirror_only_branches_match_regex`。默认禁用。
> - 默认启用于极狐GitLab 16.0。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下字段 `mirror_branch_regex` 可用。
要隐藏此功能，请让管理员禁用名为 `mirror_only_branches_match_regex` 的[功能标志](../administration/feature_flags.md)。
在 JiHuLab.com 上，此功能可用。

如果远端仓库可公开访问或通过 `username:token` 身份验证，请在[创建新项目](#create-project)或[更新现有项目](#edit-project)时使用 API 配置拉镜像。
如果您的 HTTP 仓库不可公开访问，您可以将身份验证信息添加到 URL：`https://username:token@gitlab.company.com/group/project.git`，其中 `token` 是启用了 API 范围的[个人访问令牌](../user/profile/personal_access_tokens.md)。

| 参数                               | 类型      | 是否必需 | 描述                                   |
|----------------------------------|---------|------------------------|--------------------------------------|
| `import_url`                     | string  | **{check-circle}** Yes | 被镜像的远端仓库的 URL（如果需要，使用 `user:token`）。 |
| `mirror`                         | boolean | **{check-circle}** Yes | 设置为 `true` 时启用项目上的拉镜像。               |
| `mirror_trigger_builds`          | boolean | **{dotted-circle}** No | 设置为 `true` 时触发镜像更新流水线。               |
| `only_mirror_protected_branches` | boolean | **{dotted-circle}** No | 当设置为 `true` 时，将镜像限制为仅受保护的分支。         |
| `mirror_branch_regex`            | string  | **{dotted-circle}** No | 包含正则表达式。仅镜像名称与正则表达式匹配的分支。需要禁用 `only_mirror_protected_branches`。      |

## 启动项目的拉镜像过程 **(PREMIUM)**

> 移动到专业版于 13.9 版本。

```plaintext
POST /projects/:id/mirror/pull
```

| 参数                   | 类型            | 是否必需 | 描述 |
|-----------|----------------|------------------------|-------------|
| `id`                     | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/mirror/pull"
```

## 项目徽章

在[项目徽章](project_badges.md)文档中阅读更多信息。

## 下载 Git 仓库的快照

此端点只能由管理员用户访问。

下载项目（或 wiki，如果需要）Git 仓库的快照。此快照始终采用未压缩的 tar 格式。

如果仓库损坏到 `git clone` 不起作用的程度，则快照可能允许检索一些数据。

```plaintext
GET /projects/:id/snapshot
```

| 参数                      | 类型            | 是否必需 | 描述 |
|-------------------------|----------------|------------------------|-------------|
| `id`                    | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `wiki`                  | boolean        | **{dotted-circle}** No | 是否下载 wiki，而不是项目、仓库。 |

## 获取仓库存储的路径

> 引入于 14.0 版本。

如果未使用 Gitaly Cluster，则获取指定项目的仓库存储路径。<!--如果使用 Gitaly Cluster，请参阅
[Praefect 生成的副本路径（极狐GitLab 15.0 及更高版本）](../administration/gitaly/index.md#praefect-generated-replica-paths-gitlab-150-and-later)。-->

仅适用于管理员。

```plaintext
GET /projects/:id/storage
```

| 参数                   | 类型            | 是否必需 | 描述 |
|--------------|----------------|------------------------|-------------|
| `id`         | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

```json
[
  {
    "project_id": 1,
    "disk_path": "@hashed/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b",
    "created_at": "2012-10-12T17:04:47Z",
    "repository_storage": "default"
  }
]
```
