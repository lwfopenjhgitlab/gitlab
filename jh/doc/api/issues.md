---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

<!--
# Issues API **(FREE)**

Interact with [GitLab Issues](../user/project/issues/index.md) using the REST API.

If a user is not a member of a private project, a `GET`
request on that project results in a `404` status code.
-->

# 议题 API **(FREE)**

通过 REST API 与[极狐GitLab 议题](../user/project/issues/index.md) 进行交互。

用户向私有群组发送 `GET` 请求时，如果不是该群组的成员会得到 `404` 状态码。

<!--
## Issues pagination

By default, `GET` requests return 20 results at a time because the API results
are paginated.

Read more on [pagination](rest/index.md#pagination).
-->

<a id="issues-pagination"></a>

## 议题分页

默认情况下，API 返回结果均经过分页机制处理，每次只会返回 20 条结果。

请阅读[分页](rest/index.md#pagination) 页面了解更多信息。

<!--
WARNING:
The `reference` attribute in responses is deprecated in favor of `references`.
[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/20354) in GitLab 12.6.

NOTE:
The `references.relative` attribute is relative to the group or project of the issue being requested.
When an issue is fetched from its project, the `relative` format is the same as the `short` format.
When requested across groups or projects, it's expected to be the same as the `full` format.
-->

WARNING:
响应中的 `reference` 属性已经被 `references` 取代。
引入于 12.6 版本。

NOTE:
`references.relative` 属性值和所请求的群组和项目有关。
如果是在某个项目中获取其中的议题，`relative` 的格式与 `short` 的格式相同。
如果是跨越多个群组或项目进行请求，其格式应该与 `full` 格式相同。

<a id="list-issues"></a>

## 列出议题

> - `due_date` 属性引入于 13.3 版本。
> - `weight` 属性在 13.9 版本被移动到专业版。
> - `due_date` 过滤器 `any`、`today` 和 `tomorrow` 引入于 14.8 版本。

<!--
Get all issues the authenticated user has access to. By default it
returns only issues created by the current user. To get all issues,
use parameter `scope=all`.
-->

获取经过身份验证的用户可以访问的所有议题。默认情况下只会返回当前用户创建的议题。
您可以使用参数 `scope=all` 来获取所有议题。

```plaintext
GET /issues
GET /issues?assignee_id=5
GET /issues?author_id=5
GET /issues?confidential=true
GET /issues?iids[]=42&iids[]=43
GET /issues?labels=foo
GET /issues?labels=foo,bar
GET /issues?labels=foo,bar&state=opened
GET /issues?milestone=1.0.0
GET /issues?milestone=1.0.0&state=opened
GET /issues?my_reaction_emoji=star
GET /issues?search=foo&in=title
GET /issues?state=closed
GET /issues?state=opened
```

| 参数 | 类型 | 是否必需 | 描述                                                                                                                                                                                                                    |
| ------------------- | ---------------- |------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `assignee_id`       | integer          | no   | 对于给定的用户 `id`，返回指派给这个用户的议题。与 `assignee_username` 冲突。使用 `None` 则返回未被指派的议题。使用 `Any` 则返回存在指派人的议题。                                                                                                                         |
| `assignee_username` | string array     | no   | 对于给定的用户名 `username`，返回指派给这个用户的议题。与 `assignee_id` 相似且与其冲突。在免费版中， `assignee_username` 数组只能包含单个成员，否则将报参数错误提示。                                                                                                            |
| `author_id`         | integer          | no   | 对于给定的用户 `id`，返回这个用户创建的议题。与 `author_username` 冲突。与 `scope=all` 或 `scope=assigned_to_me` 配合使用。                                                                                                                          |
| `author_username`   | string           | no   | 对于给定的用户名 `username`，返回这个用户创建的议题。与 `author_id` 相似且与其冲突。                                                                                                                                                                |
| `confidential`      | boolean          | no   | 筛选私密议题与公开议题。                                                                                                                                                                                                          |
| `created_after`     | datetime         | no   | 对于给定的时间戳，返回不早于该时间创建的议题。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）。                                                                                                                                                    |
| `created_before`    | datetime         | no   | 对于给定的时间戳，返回不晚于该时间创建的议题。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）。                                                                                                                                                    |
| `due_date`          | string           | no   | 返回没有截止日期、已经逾期、本周内逾期、本月内逾期或介于两周前和下个月之间逾期的议题。可接受的值：`0`（没有截止日期）、`any`、`today`、`tomorrow`、`overdue`、`week`、`month`、`next_month_and_previous_two_weeks`。                                                                   |
| `epic_id` **(PREMIUM)** | integer      | no   | 对于给定的史诗 ID，返回与这个史诗关联的议题。使用 `None` 则返回未与史诗关联的议题。使用 `Any` 则返回存在关联史诗的议题。引入于 13.6 版本。                                                                                                                                     |
| `health_status` **(ULTIMATE)**  |  string  | no   | 返回具有指定 `health_status` 的议题。引入于极狐GitLab 15.4。在极狐GitLab 15.5 及更高版本中，`None` 返回未分配健康状态的议题，`Any` 返回分配健康状态的议题。                                                                                                              |
| `iids[]`            | integer array    | no   | 返回包含给定 `iid` 的议题。                                                                                                                                                                                                     |
| `in`                | string           | no   | 修改 `search` 属性的范围。可以使用 `title`、`description` 或使用半角逗号对他们进行连接。默认值是 `title,description`。                                                                                                                                 |
| `issue_type`        | string           | no   | 筛选议题的类型，可选值为 `issue`、`incident` 或 `test_case`。引入于 13.12 版本。                                                                                                                                                           |
| `iteration_id` **(PREMIUM)** | integer | no   | 对于给定的迭代 ID，返回与这个迭代关联的议题。使用 `None` 则返回未与迭代关联的议题。使用 `Any` 则返回存在关联迭代的议题。引入于 13.6 版本。                                                                                                                                     |
| `iteration_title` **(PREMIUM)** | string | no   | 对于给定的迭代名称，返回与这个迭代关联的议题。与 `iteration_id` 相似且与其冲突。引入于 13.6 版本。                                                                                                                                                          |
| `labels`            | string           | no   | 对于给定的一系列以半角逗号分隔的标签，返回包含所有给定标签的议题。使用 `None` 则返回没有标签的议题。使用 `Any` 则返回包含至少一个标签的议题。使用 `No+Label`（已被弃用）则返回没有标签的议题。预定义名称对大小写不敏感。                                                                                             |
| `milestone`         | string           | no   | 里程碑名称。使用 `None` 则列出没有里程碑的议题。使用 `Any` 则列出存在关联里程碑的议题。`None` 及 `Any` 的用法将会在未来被弃用，请使用 `milestone_id` 替代。`milestone` 与 `milestone_id` 冲突。                                                                                  |
| `milestone_id`      | string           | no   | 对于给定的时间段（`None`、`Any`、`Upcoming` 或 `Started`），返回与该时间段里程碑相关联的议题。使用 `None` 则列出没有里程碑的议题。使用 `Any` 则列出存在关联里程碑的议题。使用 `Upcoming` 则列出与未开始里程碑相关联的议题。使用 `Started` 则列出与已开始里程碑相关联的议题。`milestone` 和 `milestone_id` 冲突。引入于 14.3 版本。 |
| `my_reaction_emoji` | string           | no   | 对于给定的 `emoji`，返回用户使用该表情回应的议题。使用 `None` 则返回没有使用表情回应的议题。使用 `Any` 则返回使用至少一个表情回应的议题。                                                                                                                                      |
| `non_archived`      | boolean          | no   | 返回未归档项目的议题。若使用 `false` 则同时返回已归档项目和未归档项目的议题。默认值是 `true`。引入于 13.0 版本。                                                                                                                                                   |
| `not`               | Hash             | no   | 对于给定的参数，返回不符合此参数约束的议题。可接受的值：`assignee_id`、`assignee_username`、`author_id`、`author_username`、`iids`、`iteration_id`、`iteration_title`、`labels`、`milestone`、`milestone_id` 和 `weight`。                                   |
| `order_by`          | string           | no   | 返回根据 `created_at`、`due_date`、`label_priority`、`milestone_due`、`popularity`、`priority`、`relative_position`、`title`、`updated_at` 或 `weight` 排序的议题。默认值是 `created_at`。                                                    |
| `scope`             | string           | no   | 返回满足范围 `created_by_me`、`assigned_to_me` 或 `all` 的议题。默认值是 `created_by_me`。                                                                                                                                             |
| `search`            | string           | no   | 根据 `title` 和 `description` 搜索议题。                                                                                                                                                                                      |
| `sort`              | string           | no   | 按照 `asc` 或 `desc` 的顺序返回议题。默认值是 `desc`。                                                                                                                                                                                |
| `state`             | string           | no   | 返回全部 `all` 议题或仅返回处于 `opened` 或 `closed` 状态的议题。                                                                                                                                                                        |
| `updated_after`     | datetime         | no   | 对于给定的时间戳，返回不早于该时间更新的议题。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）。                                                                                                                                                    |
| `updated_before`    | datetime         | no   | 对于给定的时间戳，返回不晚于该时间更新的议题。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）。                                                                                                                                                    |
| `weight` **(PREMIUM)** | integer       | no   | 返回权重为 `weight` 的议题。使用. `None` 则返回没有设置权重的议题。使用 `Any` 则返回设置了任意权重的议题。                                                                                                                                                    |
| `with_labels_details` | boolean        | no   | 若为 `true` 则返回更详尽的标签信息：`:name`、`:color`、`:description`、`:description_html`、`:text_color`。默认值是 `false`。`description_html` 属性引入于 12.7 版本。                                                                                |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/issues"
```

响应示例：

```json
[
  {
    "state" : "opened",
    "description" : "Ratione dolores corrupti mollitia soluta quia.",
    "author" : {
      "state" : "active",
      "id" : 18,
      "web_url" : "https://gitlab.example.com/eileen.lowe",
      "name" : "Alexandra Bashirian",
      "avatar_url" : null,
      "username" : "eileen.lowe"
    },
    "milestone" : {
      "project_id" : 1,
      "description" : "Ducimus nam enim ex consequatur cumque ratione.",
      "state" : "closed",
      "due_date" : null,
      "iid" : 2,
      "created_at" : "2016-01-04T15:31:39.996Z",
      "title" : "v4.0",
      "id" : 17,
      "updated_at" : "2016-01-04T15:31:39.996Z"
    },
    "project_id" : 1,
    "assignees" : [{
      "state" : "active",
      "id" : 1,
      "name" : "Administrator",
      "web_url" : "https://gitlab.example.com/root",
      "avatar_url" : null,
      "username" : "root"
    }],
    "assignee" : {
      "state" : "active",
      "id" : 1,
      "name" : "Administrator",
      "web_url" : "https://gitlab.example.com/root",
      "avatar_url" : null,
      "username" : "root"
    },
    "type" : "ISSUE",
    "updated_at" : "2016-01-04T15:31:51.081Z",
    "closed_at" : null,
    "closed_by" : null,
    "id" : 76,
    "title" : "Consequatur vero maxime deserunt laboriosam est voluptas dolorem.",
    "created_at" : "2016-01-04T15:31:51.081Z",
    "moved_to_id" : null,
    "iid" : 6,
    "labels" : ["foo", "bar"],
    "upvotes": 4,
    "downvotes": 0,
    "merge_requests_count": 0,
    "user_notes_count": 1,
    "due_date": "2016-07-22",
    "web_url": "http://gitlab.example.com/my-group/my-project/issues/6",
    "references": {
      "short": "#6",
      "relative": "my-group/my-project#6",
      "full": "my-group/my-project#6"
    },
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    },
    "has_tasks": true,
    "task_status": "10 of 15 tasks completed",
    "confidential": false,
    "discussion_locked": false,
    "issue_type": "issue",
    "severity": "UNKNOWN",
    "_links":{
      "self":"http://gitlab.example.com/api/v4/projects/1/issues/76",
      "notes":"http://gitlab.example.com/api/v4/projects/1/issues/76/notes",
      "award_emoji":"http://gitlab.example.com/api/v4/projects/1/issues/76/award_emoji",
      "project":"http://gitlab.example.com/api/v4/projects/1",
      "closed_as_duplicate_of": "http://gitlab.example.com/api/v4/projects/1/issues/75"
    },
    "task_completion_status":{
      "count":0,
      "completed_count":0
    }
  }
]
```

<!--
Issues created by users on GitLab Premium or higher include the `weight` property:
-->

使用专业版或更高级别创建，且包含权重 `weight` 的议题：

```json
[
   {
      "state" : "opened",
      "description" : "Ratione dolores corrupti mollitia soluta quia.",
      "weight": null,
      ...
   }
]
```

<!--
Issues created by users on GitLab Premium or higher include the `epic` property:
-->

使用专业版或更高级别创建，且包含关联史诗 `epic` 的议题：

```json
{
   "project_id" : 4,
   "description" : "Omnis vero earum sunt corporis dolor et placeat.",
   "epic_iid" : 5, //deprecated, use `iid` of the `epic` attribute
   "epic": {
     "id" : 42,
     "iid" : 5,
     "title": "My epic epic",
     "url" : "/groups/h5bp/-/epics/5",
     "group_id": 8
   },
   ...
}
```

<!--
Issues created by users on GitLab Premium or higher include the `iteration` property:
-->

使用专业版或更高级别创建，且包含关联迭代 `iteration` 的议题：

```json
{
  "iteration": {
    "id":90,
    "iid":4,
    "sequence":2,
    "group_id":162,
    "title":null,
    "description":null,
    "state":2,
    "created_at":"2022-03-14T05:21:11.929Z",
    "updated_at":"2022-03-14T05:21:11.929Z",
    "start_date":"2022-03-08",
    "due_date":"2022-03-14",
    "web_url":"https://gitlab.com/groups/my-group/-/iterations/90"
  }
  ...
}
```

<!--
Issues created by users on GitLab Ultimate include the `health_status` property:
-->

使用旗舰版创建，且包含健康状态 `health_status` 的议题：

```json
[
   {
      "state" : "opened",
      "description" : "Ratione dolores corrupti mollitia soluta quia.",
      "health_status": "on_track",
      ...
   }
]
```

<!--
WARNING:
The `assignee` column is deprecated. We now show it as a single-sized array `assignees` to conform
to the GitLab EE API.

WARNING:
The `epic_iid` attribute is deprecated and [scheduled for removal](https://gitlab.com/gitlab-org/gitlab/-/issues/35157) in API version 5.
Please use `iid` of the `epic` attribute instead.
-->

WARNING:
`assignee` 已经被弃用。我们已经转而使用 `assignees` 一维数组以维持不同版本 API 的一致性。

WARNING:
`epic_iid` 已经被弃用。我们将在第 5 版本的 API 中将其移除。
作为替代，请使用 `epic` 属性的 `iid`。

<!--
## List group issues

> The `due_date` property was [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/233420) in GitLab 13.3.
> The `weight` property moved to GitLab Premium in 13.9.
> The `due_date` filters `any`, `today`, and `tomorrow` were [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/78460) in GitLab 14.8.
-->

<a id="list-group-issues"></a>

## 列出群组议题

> - `due_date` 属性引入于 13.3 版本。
> - `weight` 属性在 13.9 版本被移动到专业版。
> - `due_date` 过滤器 `any`、`today` 和 `tomorrow` 引入于 14.8 版本。

<!--
Get a list of a group's issues.

If the group is private, credentials need to be provided for authorization.
The preferred way to do this, is by using [personal access tokens](../user/profile/personal_access_tokens.md).
-->

获取某个群组的议题。

如果这个群组是私有的，请求者必须提供有效的鉴权信息。
我们推荐您使用[个人访问令牌](../user/profile/personal_access_tokens.md)进行鉴权。

```plaintext
GET /groups/:id/issues
GET /groups/:id/issues?assignee_id=5
GET /groups/:id/issues?author_id=5
GET /groups/:id/issues?confidential=true
GET /groups/:id/issues?iids[]=42&iids[]=43
GET /groups/:id/issues?labels=foo
GET /groups/:id/issues?labels=foo,bar
GET /groups/:id/issues?labels=foo,bar&state=opened
GET /groups/:id/issues?milestone=1.0.0
GET /groups/:id/issues?milestone=1.0.0&state=opened
GET /groups/:id/issues?my_reaction_emoji=star
GET /groups/:id/issues?search=issue+title+or+description
GET /groups/:id/issues?state=closed
GET /groups/:id/issues?state=opened
```

| 参数 | 类型 | 是否必需 | 描述                                                                                                                                                        |
| ------------------- | ---------------- | ---------- |-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| `assignee_id`       | integer          | no         | 对于给定的用户 `id`，返回指派给这个用户的议题。与 `assignee_username` 冲突。使用 `None` 则返回未被指派的议题。使用 `Any` 则返回存在指派人的议题。                                                             |
| `assignee_username` | string array     | no         | 对于给定的用户名 `username`，返回指派给这个用户的议题。与 `assignee_id` 相似且与其冲突。在免费版中，`assignee_username` 数组只能包含单个成员，否则将报参数错误提示。                                                 |
| `author_id`         | integer          | no         | 对于给定的用户 `id`，返回这个用户创建的议题。与 `author_username` 冲突。与 `scope=all` 或 `scope=assigned_to_me` 配合使用。                                                              |
| `author_username`   | string           | no         | 对于给定的用户名 `username`，返回这个用户创建的议题。与 `author_id` 相似且与其冲突。                                                                                                    |
| `confidential`      | boolean          | no         | 筛选私密议题与公开议题。                                                                                                                                              |
| `created_after`     | datetime         | no         | 对于给定的时间戳，返回不早于该时间创建的议题。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                         |
| `created_before`    | datetime         | no         | 对于给定的时间戳，返回不晚于该时间创建的议题。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）                                                                                         |
| `due_date`          | string           | no         | 返回没有截止日期、已经逾期、本周内逾期、本月内逾期或介于两周前和下个月之间逾期的议题。可接受的值：`0`（没有截止日期）、`any`、`today`、`tomorrow`、`overdue`、`week`、`month`、`next_month_and_previous_two_weeks`。       |
| `epic_id` **(PREMIUM)** | integer      | no         | 对于给定的史诗 ID，返回与这个史诗关联的议题。使用 `None` 则返回未与史诗关联的议题。使用 `Any` 则返回存在关联史诗的议题。引入于 13.6 版本。                                                                         |
| `id`                | integer/string   | yes        | 经过身份验证的用户拥有的全局 ID 或 [经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)。                                                                                  |
| `iids[]`            | integer array    | no         | 返回包含给定 `iid` 的议题。                                                                                                                                         |
| `issue_type`        | string           | no         | 筛选议题的类型，可选值为 `issue`、`incident` 或 `test_case`。引入于 13.12 版本。                                                                                               |
| `iteration_id` **(PREMIUM)** | integer | no         | 对于给定的迭代 ID，返回与这个迭代关联的议题。使用 `None` 则返回未与迭代关联的议题。使用 `Any` 则返回存在关联迭代的议题。引入于 13.6 版本。                                                                         |
| `iteration_title` **(PREMIUM)** | string | no       | 对于给定的迭代名称，返回与这个迭代关联的议题。与 `iteration_id` 相似且与其冲突。引入于 13.6 版本。                                                                                              |
| `labels`            | string           | no         | 对于给定的一系列以半角逗号分隔的标签，返回包含所有给定标签的议题。使用 `None` 则返回没有标签的议题。使用 `Any` 则返回包含至少一个标签的议题。使用 `No+Label`（已被弃用）则返回没有标签的议题。预定义名称不区分大小写。                                  |
| `milestone`         | string           | no         | 里程碑名称。使用 `None` 则列出没有里程碑的议题。使用 `Any` 则列出存在关联里程碑的议题。                                                                                                       |
| `my_reaction_emoji` | string           | no         | 对于给定的 `emoji`，返回用户使用该表情回应的议题。使用 `None` 则返回没有使用表情回应的议题。使用 `Any` 则返回使用至少一个表情回应的议题。                                                                          |
| `non_archived`      | boolean          | no         | 返回未归档项目的议题。默认值是 `true`。引入于 12.8 版本。                                                                                                                       |
| `not`               | Hash             | no         | 对于给定的参数，返回不符合此参数约束的议题。可接受的值：`labels`、`milestone`、`author_id`、`author_username`、`assignee_id`、`assignee_username`、`my_reaction_emoji`、`search` 和 `in`。     |
| `order_by`          | string           | no         | 返回根据 `created_at`、`updated_at`、`priority`、`due_date`、`relative_position`、`label_priority`、`milestone_due`、`popularity` 或 `weight` 排序的议题。默认值是 `created_at`。 |
| `scope`             | string           | no         | 返回满足范围 `created_by_me`、`assigned_to_me` 或 `all` 的议题。默认值是 `all`。                                                                                           |
| `search`            | string           | no         | 根据 `title` 和 `description` 搜索群组议题。                                                                                                                        |
| `sort`              | string           | no         | 按照 `asc` 或 `desc` 的顺序返回议题。默认值是 `desc`。                                                                                                                    |
| `state`             | string           | no         | 返回全部 `all` 议题或仅返回处于 `opened` 或 `closed` 状态的议题。                                                                                                            |
| `updated_after`     | datetime         | no         | 对于给定的时间戳，返回不早于该时间更新的议题。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）。                                                                                        |
| `updated_before`    | datetime         | no         | 对于给定的时间戳，返回不晚于该时间更新的议题。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）。                                                                                        |
| `weight` **(PREMIUM)** | integer       | no         | 返回权重为 `weight` 的议题。使用. `None` 则返回没有设置权重的议题。使用 `Any` 则返回设置了任意权重的议题。                                                                                        |
| `with_labels_details` | boolean        | no         | 若为 `true` 则返回更详尽的标签信息：`:name`、`:color`、`:description`、`:description_html`、`:text_color`。默认值是 `false`。`description_html` 属性引入于 12.7 版本。                    |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/4/issues"
```

响应示例：

```json
[
  {
    "project_id" : 4,
    "milestone" : {
      "due_date" : null,
      "project_id" : 4,
      "state" : "closed",
      "description" : "Rerum est voluptatem provident consequuntur molestias similique ipsum dolor.",
      "iid" : 3,
      "id" : 11,
      "title" : "v3.0",
      "created_at" : "2016-01-04T15:31:39.788Z",
      "updated_at" : "2016-01-04T15:31:39.788Z"
    },
    "author" : {
      "state" : "active",
      "web_url" : "https://gitlab.example.com/root",
      "avatar_url" : null,
      "username" : "root",
      "id" : 1,
      "name" : "Administrator"
    },
    "description" : "Omnis vero earum sunt corporis dolor et placeat.",
    "state" : "closed",
    "iid" : 1,
    "assignees" : [{
      "avatar_url" : null,
      "web_url" : "https://gitlab.example.com/lennie",
      "state" : "active",
      "username" : "lennie",
      "id" : 9,
      "name" : "Dr. Luella Kovacek"
    }],
    "assignee" : {
      "avatar_url" : null,
      "web_url" : "https://gitlab.example.com/lennie",
      "state" : "active",
      "username" : "lennie",
      "id" : 9,
      "name" : "Dr. Luella Kovacek"
    },
    "type" : "ISSUE",
    "labels" : ["foo", "bar"],
    "upvotes": 4,
    "downvotes": 0,
    "merge_requests_count": 0,
    "id" : 41,
    "title" : "Ut commodi ullam eos dolores perferendis nihil sunt.",
    "updated_at" : "2016-01-04T15:31:46.176Z",
    "created_at" : "2016-01-04T15:31:46.176Z",
    "closed_at" : null,
    "closed_by" : null,
    "user_notes_count": 1,
    "due_date": null,
    "web_url": "http://gitlab.example.com/my-group/my-project/issues/1",
    "references": {
      "short": "#1",
      "relative": "my-project#1",
      "full": "my-group/my-project#1"
    },
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    },
    "has_tasks": true,
    "task_status": "10 of 15 tasks completed",
    "confidential": false,
    "discussion_locked": false,
    "issue_type": "issue",
    "severity": "UNKNOWN",
    "_links":{
      "self":"http://gitlab.example.com/api/v4/projects/4/issues/41",
      "notes":"http://gitlab.example.com/api/v4/projects/4/issues/41/notes",
      "award_emoji":"http://gitlab.example.com/api/v4/projects/4/issues/41/award_emoji",
      "project":"http://gitlab.example.com/api/v4/projects/4",
      "closed_as_duplicate_of": "http://gitlab.example.com/api/v4/projects/1/issues/75"
    },
    "task_completion_status":{
      "count":0,
      "completed_count":0
    }
  }
]
```

<!--
Issues created by users on GitLab Premium or higher include the `weight` property:
-->

使用专业版或更高级别创建，且包含权重 `weight` 的议题：

```json
[
   {
      "project_id" : 4,
      "description" : "Omnis vero earum sunt corporis dolor et placeat.",
      "weight": null,
      ...
   }
]
```

<!--
Issues created by users on GitLab Premium or higher include the `epic` property:
-->

使用专业版或更高级别创建，且包含关联史诗 `epic` 的议题：

```json
{
   "project_id" : 4,
   "description" : "Omnis vero earum sunt corporis dolor et placeat.",
   "epic_iid" : 5, //deprecated, use `iid` of the `epic` attribute
   "epic": {
     "id" : 42,
     "iid" : 5,
     "title": "My epic epic",
     "url" : "/groups/h5bp/-/epics/5",
     "group_id": 8
   },
   ...
}
```

<!--
Issues created by users on GitLab Ultimate include the `health_status` property:
-->

使用旗舰版创建，且包含健康状态 `health_status` 的议题：

```json
[
   {
      "project_id" : 4,
      "description" : "Omnis vero earum sunt corporis dolor et placeat.",
      "health_status": "at_risk",
      ...
   }
]
```

<!--
WARNING:
The `assignee` column is deprecated. We now show it as a single-sized array `assignees` to conform to the GitLab EE API.

WARNING:
The `epic_iid` attribute is deprecated and [scheduled for removal](https://gitlab.com/gitlab-org/gitlab/-/issues/35157) in API version 5.
Please use `iid` of the `epic` attribute instead.
-->

WARNING:
`assignee` 已经被弃用。我们已经转而使用 `assignees` 一维数组以维持不同版本 API 的一致性。

WARNING:
`epic_iid` 已经被弃用。我们将在第 5 版本的 API 中将其移除。
作为替代，请使用 `epic` 属性的 `iid`。

<!--
## List project issues

> The `due_date` property was [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/233420) in GitLab 13.3.
> The `weight` property moved to GitLab Premium in 13.9.
> The `due_date` filters `any`, `today`, and `tomorrow` were [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/78460) in GitLab 14.8.
-->

<a id="list-project-issues"></a>

## 列出项目议题

> - `due_date` 属性引入于 13.3 版本。
> - `weight` 属性在 13.9 版本被移动到专业版。
> - `due_date` 过滤器 `any`、`today` 和 `tomorro引入于 14.8 版本。

<!--
Get a list of a project's issues.

If the project is private, you need to provide credentials to authorize.
The preferred way to do this, is by using [personal access tokens](../user/profile/personal_access_tokens.md).
-->

列出一个项目的议题。

如果这个项目是私有的，请求者必须提供有效的鉴权信息。
我们推荐您使用[个人访问令牌](../user/profile/personal_access_tokens.md)进行鉴权。

```plaintext
GET /projects/:id/issues
GET /projects/:id/issues?assignee_id=5
GET /projects/:id/issues?author_id=5
GET /projects/:id/issues?confidential=true
GET /projects/:id/issues?iids[]=42&iids[]=43
GET /projects/:id/issues?labels=foo
GET /projects/:id/issues?labels=foo,bar
GET /projects/:id/issues?labels=foo,bar&state=opened
GET /projects/:id/issues?milestone=1.0.0
GET /projects/:id/issues?milestone=1.0.0&state=opened
GET /projects/:id/issues?my_reaction_emoji=star
GET /projects/:id/issues?search=issue+title+or+description
GET /projects/:id/issues?state=closed
GET /projects/:id/issues?state=opened
```

| 参数 | 类型 | 是否必需 | 描述                                                                                                                                                        |
| ------------------- | ---------------- | ---------- |-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| `assignee_id`       | integer          | no         | 对于给定的用户 `id`，返回指派给这个用户的议题。与 `assignee_username` 冲突。使用 `None` 则返回未被指派的议题。使用 `Any` 则返回存在指派人的议题。                                                             |
| `assignee_username` | string array     | no         | 对于给定的用户名 `username`，返回指派给这个用户的议题。与 `assignee_id` 相似且与其冲突。在免费版中， `assignee_username` 数组只能包含单个成员，否则将报参数错误提示。                                                |
| `author_id`         | integer          | no         | 对于给定的用户 `id`，返回这个用户创建的议题。与 `author_username` 冲突。与 `scope=all` 或 `scope=assigned_to_me` 配合使用。                                                              |
| `author_username`   | string           | no         | 对于给定的用户名 `username`，返回这个用户创建的议题。与 `author_id` 相似且与其冲突。                                                                                                    |
| `confidential`      | boolean          | no         | 筛选私密议题与公开议题。                                                                                                                                              |
| `created_after`     | datetime         | no         | 对于给定的时间戳，返回不早于该时间创建的议题。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）。                                                                                        |
| `created_before`    | datetime         | no         | 对于给定的时间戳，返回不晚于该时间创建的议题。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）。                                                                                        |
| `due_date`          | string           | no         | 返回没有截止日期、已经逾期、本周内逾期、本月内逾期或介于两周前和下个月之间逾期的议题。可接受的值：`0`（没有截止日期）、`any`、`today`、`tomorrow`、`overdue`、`week`、`month`、`next_month_and_previous_two_weeks`。       |
| `epic_id` **(PREMIUM)** | integer      | no         | 对于给定的史诗 ID，返回与这个史诗关联的议题。使用 `None` 则返回未与史诗关联的议题。使用 `Any` 则返回存在关联史诗的议题。引入于 13.6 版本。                                                                         |
| `id`                | integer/string   | yes        | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)。                                                                                   |
| `iids[]`            | integer array    | no         | 返回包含给定 `iid` 的议题。                                                                                                                                         |
| `issue_type`        | string           | no         | 筛选议题的类型，可选值为 `issue`、`incident` 或 `test_case`。引入于 13.12 版本。                                                                                               |
| `iteration_id` **(PREMIUM)** | integer | no         | 对于给定的迭代 ID，返回与这个迭代关联的议题。使用 `None` 则返回未与迭代关联的议题。使用 `Any` 则返回存在关联迭代的议题。引入于 13.6 版本。                                                                         |
| `iteration_title` **(PREMIUM)** | string | no       | 对于给定的迭代名称，返回与这个迭代关联的议题。与 `iteration_id` 相似且与其冲突。引入于 13.6 版本。                                                                                              |
| `labels`            | string           | no         | 对于给定的一系列以半角逗号分隔的标签，返回包含所有给定标签的议题。使用 `None` 则返回没有标签的议题。使用 `Any` 则返回包含至少一个标签的议题。使用 `No+Label`（已被弃用）则返回没有标签的议题。预定义名称不区分大小写。                                  |
| `milestone`         | string           | no         | 里程碑名称。使用 `None` 则列出没有里程碑的议题。使用 `Any` 则列出存在关联里程碑的议题。                                                                                                       |
| `my_reaction_emoji` | string           | no         | 对于给定的 `emoji`，返回用户使用该表情回应的议题。使用 `None` 则返回没有使用表情回应的议题。使用 `Any` 则返回使用至少一个表情回应的议题。                                                                          |
| `not`               | Hash             | no         | 对于给定的参数，返回不符合此参数约束的议题。可接受的值：`labels`、`milestone`、`author_id`、`author_username`、`assignee_id`、`assignee_username`、`my_reaction_emoji`、`search` 和 `in`。     |
| `order_by`          | string           | no         | 返回根据 `created_at`、`updated_at`、`priority`、`due_date`、`relative_position`、`label_priority`、`milestone_due`、`popularity` 或 `weight` 排序的议题。默认值是 `created_at`。 |
| `scope`             | string           | no         | 返回满足范围 `created_by_me`、`assigned_to_me` 或 `all` 的议题。默认值是 `all`。                                                                                           |
| `search`            | string           | no         | 根据 `title` 和 `description` 搜索项目议题。                                                                                                                        |
| `sort`              | string           | no         | 按照 `asc` 或 `desc` 的顺序返回议题。默认值是 `desc`。                                                                                                                    |
| `state`             | string           | no         | 返回全部 `all` 议题或仅返回处于 `opened` 或 `closed` 状态的议题。                                                                                                            |
| `updated_after`     | datetime         | no         | 对于给定的时间戳，返回不早于该时间更新的议题。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）。                                                                                        |
| `updated_before`    | datetime         | no         | 对于给定的时间戳，返回不晚于该时间更新的议题。时间戳应符合 ISO 8601 格式（`2019-03-15T08:00:00Z`）。                                                                                        |
| `weight` **(PREMIUM)** | integer       | no         | 返回权重为 `weight` 的议题。使用. `None` 则返回没有设置权重的议题。使用 `Any` 则返回设置了任意权重的议题。                                                                                        |
| `with_labels_details` | boolean        | no         | 若为 `true` 则返回更详尽的标签信息：`:name`、`:color`、`:description`、`:description_html`、`:text_color`。默认值是 `false`。`description_html` 引入于 12.7 版本。                      |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/4/issues"
```

响应示例：

```json
[
  {
    "project_id" : 4,
    "milestone" : {
      "due_date" : null,
      "project_id" : 4,
      "state" : "closed",
      "description" : "Rerum est voluptatem provident consequuntur molestias similique ipsum dolor.",
      "iid" : 3,
      "id" : 11,
      "title" : "v3.0",
      "created_at" : "2016-01-04T15:31:39.788Z",
      "updated_at" : "2016-01-04T15:31:39.788Z"
    },
    "author" : {
      "state" : "active",
      "web_url" : "https://gitlab.example.com/root",
      "avatar_url" : null,
      "username" : "root",
      "id" : 1,
      "name" : "Administrator"
    },
    "description" : "Omnis vero earum sunt corporis dolor et placeat.",
    "state" : "closed",
    "iid" : 1,
    "assignees" : [{
      "avatar_url" : null,
      "web_url" : "https://gitlab.example.com/lennie",
      "state" : "active",
      "username" : "lennie",
      "id" : 9,
      "name" : "Dr. Luella Kovacek"
    }],
    "assignee" : {
      "avatar_url" : null,
      "web_url" : "https://gitlab.example.com/lennie",
      "state" : "active",
      "username" : "lennie",
      "id" : 9,
      "name" : "Dr. Luella Kovacek"
    },
    "type" : "ISSUE",
    "labels" : ["foo", "bar"],
    "upvotes": 4,
    "downvotes": 0,
    "merge_requests_count": 0,
    "id" : 41,
    "title" : "Ut commodi ullam eos dolores perferendis nihil sunt.",
    "updated_at" : "2016-01-04T15:31:46.176Z",
    "created_at" : "2016-01-04T15:31:46.176Z",
    "closed_at" : "2016-01-05T15:31:46.176Z",
    "closed_by" : {
      "state" : "active",
      "web_url" : "https://gitlab.example.com/root",
      "avatar_url" : null,
      "username" : "root",
      "id" : 1,
      "name" : "Administrator"
    },
    "user_notes_count": 1,
    "due_date": "2016-07-22",
    "web_url": "http://gitlab.example.com/my-group/my-project/issues/1",
    "references": {
      "short": "#1",
      "relative": "#1",
      "full": "my-group/my-project#1"
    },
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    },
    "has_tasks": true,
    "task_status": "10 of 15 tasks completed",
    "confidential": false,
    "discussion_locked": false,
    "issue_type": "issue",
    "severity": "UNKNOWN",
    "_links":{
      "self":"http://gitlab.example.com/api/v4/projects/4/issues/41",
      "notes":"http://gitlab.example.com/api/v4/projects/4/issues/41/notes",
      "award_emoji":"http://gitlab.example.com/api/v4/projects/4/issues/41/award_emoji",
      "project":"http://gitlab.example.com/api/v4/projects/4",
      "closed_as_duplicate_of": "http://gitlab.example.com/api/v4/projects/1/issues/75"
    },
    "task_completion_status":{
      "count":0,
      "completed_count":0
    }
  }
]
```

<!--
Issues created by users on GitLab Premium or higher include the `weight` property:
-->

使用专业版或更高级别创建，且包含权重 `weight` 的议题：

```json
[
   {
      "project_id" : 4,
      "description" : "Omnis vero earum sunt corporis dolor et placeat.",
      "weight": null,
      ...
   }
]
```

<!--
Issues created by users on GitLab Premium or higher include the `epic` property:
-->

使用专业版或更高级别创建，且包含关联史诗 `epic` 的议题：

```json
{
   "project_id" : 4,
   "description" : "Omnis vero earum sunt corporis dolor et placeat.",
   "epic_iid" : 5, //deprecated, use `iid` of the `epic` attribute
   "epic": {
     "id" : 42,
     "iid" : 5,
     "title": "My epic epic",
     "url" : "/groups/h5bp/-/epics/5",
     "group_id": 8
   },
   ...
}
```

<!--
Issues created by users on GitLab Ultimate include the `health_status` property:
-->

使用旗舰版创建，且包含健康状态 `health_status` 的议题：

```json
[
   {
      "project_id" : 4,
      "description" : "Omnis vero earum sunt corporis dolor et placeat.",
      "health_status": "at_risk",
      ...
   }
]
```

<!--
WARNING:
The `assignee` column is deprecated. We now show it as a single-sized array `assignees` to conform to the GitLab EE API.

WARNING:
The `epic_iid` attribute is deprecated and [scheduled for removal](https://gitlab.com/gitlab-org/gitlab/-/issues/35157) in API version 5.
Please use `iid` of the `epic` attribute instead.
-->

WARNING:
`assignee` 已经被弃用。我们已经转而使用 `assignees` 一维数组以维持不同版本 API 的一致性。

WARNING:
`epic_iid` 已经被弃用。我们将在第 5 版本的 API 中将其移除。
作为替代，请使用 `epic` 属性的 `iid`。

<!--
## Single issue

Only for administrators. Get a single issue.

The preferred way to do this is by using [personal access tokens](../user/profile/personal_access_tokens.md).
-->

<a id="single-issue"></a>

## 单个议题

仅适用于管理员。获取单个议题。
我们推荐您使用[个人访问令牌](../user/profile/personal_access_tokens.md)进行鉴权。

```plaintext
GET /issues/:id
```

| 参数 | 类型 | 是否必需 | 描述 |
|-------------|---------|----------|--------------------------------------|
| `id`        | integer | yes      | 议题的 ID 号 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/issues/41"
```

Example response:

```json
{
  "id": 1,
  "milestone": {
    "due_date": null,
    "project_id": 4,
    "state": "closed",
    "description": "Rerum est voluptatem provident consequuntur molestias similique ipsum dolor.",
    "iid": 3,
    "id": 11,
    "title": "v3.0",
    "created_at": "2016-01-04T15:31:39.788Z",
    "updated_at": "2016-01-04T15:31:39.788Z",
    "closed_at": "2016-01-05T15:31:46.176Z"
  },
  "author": {
    "state": "active",
    "web_url": "https://gitlab.example.com/root",
    "avatar_url": null,
    "username": "root",
    "id": 1,
    "name": "Administrator"
  },
  "description": "Omnis vero earum sunt corporis dolor et placeat.",
  "state": "closed",
  "iid": 1,
  "assignees": [
    {
      "avatar_url": null,
      "web_url": "https://gitlab.example.com/lennie",
      "state": "active",
      "username": "lennie",
      "id": 9,
      "name": "Dr. Luella Kovacek"
    }
  ],
  "assignee": {
    "avatar_url": null,
    "web_url": "https://gitlab.example.com/lennie",
    "state": "active",
    "username": "lennie",
    "id": 9,
    "name": "Dr. Luella Kovacek"
  },
  "type": "ISSUE",
  "labels": [],
  "upvotes": 4,
  "downvotes": 0,
  "merge_requests_count": 0,
  "title": "Ut commodi ullam eos dolores perferendis nihil sunt.",
  "updated_at": "2016-01-04T15:31:46.176Z",
  "created_at": "2016-01-04T15:31:46.176Z",
  "closed_at": null,
  "closed_by": null,
  "subscribed": false,
  "user_notes_count": 1,
  "due_date": null,
  "web_url": "http://example.com/my-group/my-project/issues/1",
  "references": {
    "short": "#1",
    "relative": "#1",
    "full": "my-group/my-project#1"
  },
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "confidential": false,
  "discussion_locked": false,
  "issue_type": "issue",
  "severity": "UNKNOWN",
  "task_completion_status": {
    "count": 0,
    "completed_count": 0
  },
  "weight": null,
  "has_tasks": false,
  "_links": {
    "self": "http://gitlab.example:3000/api/v4/projects/1/issues/1",
    "notes": "http://gitlab.example:3000/api/v4/projects/1/issues/1/notes",
    "award_emoji": "http://gitlab.example:3000/api/v4/projects/1/issues/1/award_emoji",
    "project": "http://gitlab.example:3000/api/v4/projects/1",
    "closed_as_duplicate_of": "http://gitlab.example.com/api/v4/projects/1/issues/75"
  },
  "moved_to_id": null,
  "service_desk_reply_to": "service.desk@gitlab.com"
}
```

<!--
Issues created by users on GitLab Premium or higher include the `weight` property:
-->

使用专业版或更高级别创建，且包含权重 `weight` 的议题：

```json
{
   "project_id" : 4,
   "description" : "Omnis vero earum sunt corporis dolor et placeat.",
   "weight": null,
   ...
}
```

<!--
Issues created by users on GitLab Premium or higher include the `epic` property:
-->

使用专业版或更高级别创建，且包含关联史诗 `epic` 的议题：

```json
{
   "project_id" : 4,
   "description" : "Omnis vero earum sunt corporis dolor et placeat.",
   "epic": {
   "epic_iid" : 5, //deprecated, use `iid` of the `epic` attribute
   "epic": {
     "id" : 42,
     "iid" : 5,
     "title": "My epic epic",
     "url" : "/groups/h5bp/-/epics/5",
     "group_id": 8
   },
   ...
}
```

<!--
Users of [GitLab Ultimate](https://about.gitlab.com/pricing/) can also see the `health_status`
property:
-->

[旗舰版](https://gitlab.cn/pricing/)用户还可以看到健康状态 `health_status` 信息：

```json
[
   {
      "project_id" : 4,
      "description" : "Omnis vero earum sunt corporis dolor et placeat.",
      "health_status": "on_track",
      ...
   }
]
```

<!--
WARNING:
The `assignee` column is deprecated. We now show it as a single-sized array `assignees` to conform
to the GitLab EE API.

WARNING:
The `epic_iid` attribute is deprecated, and [scheduled for removal](https://gitlab.com/gitlab-org/gitlab/-/issues/35157) in API version 5.
Please use `iid` of the `epic` attribute instead.
-->

WARNING:
`assignee` 已经被弃用。我们已经转而使用 `assignees` 一维数组以维持不同版本 API 的一致性。

WARNING:
`epic_iid` 已经被弃用。我们将在第 5 版本的 API 中将其移除。
作为替代，请使用 `epic` 属性的 `iid`。

<!--
## Single project issue

Get a single project issue.

If the project is private or the issue is confidential, you need to provide credentials to authorize.
The preferred way to do this, is by using [personal access tokens](../user/profile/personal_access_tokens.md).
-->

<a id="single-project-issue"></a>

## 单个项目议题

获取单个项目议题。

如果这个项目是私有的或议题是私密的，请求者必须提供有效的鉴权信息。
我们推荐您使用[个人访问令牌](../user/profile/personal_access_tokens.md)进行鉴权。

```plaintext
GET /projects/:id/issues/:issue_iid
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-------------|---------|----------|------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                             |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/4/issues/41"
```

响应示例：

```json
{
  "project_id" : 4,
  "milestone" : {
    "due_date" : null,
    "project_id" : 4,
    "state" : "closed",
    "description" : "Rerum est voluptatem provident consequuntur molestias similique ipsum dolor.",
    "iid" : 3,
    "id" : 11,
    "title" : "v3.0",
    "created_at" : "2016-01-04T15:31:39.788Z",
    "updated_at" : "2016-01-04T15:31:39.788Z",
    "closed_at" : "2016-01-05T15:31:46.176Z"
  },
  "author" : {
    "state" : "active",
    "web_url" : "https://gitlab.example.com/root",
    "avatar_url" : null,
    "username" : "root",
    "id" : 1,
    "name" : "Administrator"
  },
  "description" : "Omnis vero earum sunt corporis dolor et placeat.",
  "state" : "closed",
  "iid" : 1,
  "assignees" : [{
    "avatar_url" : null,
    "web_url" : "https://gitlab.example.com/lennie",
    "state" : "active",
    "username" : "lennie",
    "id" : 9,
    "name" : "Dr. Luella Kovacek"
  }],
  "assignee" : {
    "avatar_url" : null,
    "web_url" : "https://gitlab.example.com/lennie",
    "state" : "active",
    "username" : "lennie",
    "id" : 9,
    "name" : "Dr. Luella Kovacek"
  },
  "type" : "ISSUE",
  "labels" : [],
  "upvotes": 4,
  "downvotes": 0,
  "merge_requests_count": 0,
  "id" : 41,
  "title" : "Ut commodi ullam eos dolores perferendis nihil sunt.",
  "updated_at" : "2016-01-04T15:31:46.176Z",
  "created_at" : "2016-01-04T15:31:46.176Z",
  "closed_at" : null,
  "closed_by" : null,
  "subscribed": false,
  "user_notes_count": 1,
  "due_date": null,
  "web_url": "http://gitlab.example.com/my-group/my-project/issues/1",
  "references": {
    "short": "#1",
    "relative": "#1",
    "full": "my-group/my-project#1"
  },
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "confidential": false,
  "discussion_locked": false,
  "issue_type": "issue",
  "severity": "UNKNOWN",
  "_links": {
    "self": "http://gitlab.example.com/api/v4/projects/1/issues/2",
    "notes": "http://gitlab.example.com/api/v4/projects/1/issues/2/notes",
    "award_emoji": "http://gitlab.example.com/api/v4/projects/1/issues/2/award_emoji",
    "project": "http://gitlab.example.com/api/v4/projects/1",
    "closed_as_duplicate_of": "http://gitlab.example.com/api/v4/projects/1/issues/75"
  },
  "task_completion_status":{
    "count":0,
    "completed_count":0
  }
}
```

<!--
Issues created by users on GitLab Premium or higher include the `weight` property:
-->

使用专业版或更高级别创建，且包含权重 `weight` 的议题：

```json
{
   "project_id" : 4,
   "description" : "Omnis vero earum sunt corporis dolor et placeat.",
   "weight": null,
   ...
}
```

<!--
Issues created by users on GitLab Premium or higher include the `epic` property:
-->

使用专业版或更高级别创建，且包含关联史诗 `epic` 的议题：

```json
{
   "project_id" : 4,
   "description" : "Omnis vero earum sunt corporis dolor et placeat.",
   "epic_iid" : 5, //deprecated, use `iid` of the `epic` attribute
   "epic": {
     "id" : 42,
     "iid" : 5,
     "title": "My epic epic",
     "url" : "/groups/h5bp/-/epics/5",
     "group_id": 8
   },
   ...
}
```

<!--
Users of [GitLab Ultimate](https://about.gitlab.com/pricing/) can also see the `health_status`
property:
-->

[旗舰版](https://gitlab.cn/pricing/)用户还可以看到健康状态 `health_status` 信息：

```json
[
   {
      "project_id" : 4,
      "description" : "Omnis vero earum sunt corporis dolor et placeat.",
      "health_status": "on_track",
      ...
   }
]
```

<!--
WARNING:
The `assignee` column is deprecated. We now show it as a single-sized array `assignees` to conform to the GitLab EE API.

WARNING:
The `epic_iid` attribute is deprecated and [scheduled for removal](https://gitlab.com/gitlab-org/gitlab/-/issues/35157) in API version 5.
Please use `iid` of the `epic` attribute instead.
-->

WARNING:
`assignee` 已经被弃用。我们已经转而使用 `assignees` 一维数组以维持不同版本 API 的一致性。

WARNING:
`epic_iid` 已经被弃用。我们将在第 5 版本的 API 中将其移除。
作为替代，请使用 `epic` 属性的 `iid`。


<!--
## New issue

> The `weight` property moved to GitLab Premium in 13.9.

Creates a new project issue.
-->

<a id="new-issue"></a>

## 新建议题

> `weight` 属性在 13.9 版本被移动到专业版。

创建一个新的项目议题。

```plaintext
POST /projects/:id/issues
```

| 参数 | 类型 | 是否必需 | 描述                                                                                             |
|-------------------------------------------|----------------|----------|------------------------------------------------------------------------------------------------|
| `assignee_id`                             | integer        | no       | 指派人的用户 ID，仅适用于免费版。                                                                             |
| `assignee_ids` **(PREMIUM)**              | integer array  | no       | 指派人的用户 ID。                                                                                     |
| `confidential`                            | boolean        | no       | 将议题设置为私密。默认值为 `false`。                                                                         |
| `created_at`                              | string         | no       | 议题的创建时间。使用 ISO 8601 格式，如 `2016-03-11T03:45:40Z`。需要管理员或项目/群组拥有者权限。                              |
| `description`                             | string         | no       | 议题描述。不能超过 1,048,576 个字符。                                                                       |
| `discussion_to_resolve`                   | string         | no       | 解决讨论的 ID。这将对议题描述进行自动填充并将对应讨论标记为已解决。和 `merge_request_to_resolve_discussions_of` 配合使用。           |
| `due_date`                                | string         | no       | 截止日期。使用 `YYYY-MM-DD` 格式，如 `2016-03-11`                                                         |
| `epic_id` **(PREMIUM)** | integer | no | 需要关联的史诗 ID。提供的值必须大于等于 0。                                                                       |
| `epic_iid` **(PREMIUM)** | integer | no | 需要关联的史诗 IID。提供的值必须大于等于 0。已被弃用，第五版 API 将会移除此属性。                                                 |
| `id`                                      | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)。                        |
| `iid`                                     | integer/string | no       | 项目议题内部 ID（需要管理员或项目拥有者权限）。                                                                      |
| `issue_type`                              | string         | no       | 议题类型，可以是 `issue`、`incident` 和 `test_case` 中的一个。默认值是 `issue`。                                   |
| `labels`                                  | string         | no       | 议题的标签名称，使用半角逗号分隔。                                                                              |
| `merge_request_to_resolve_discussions_of` | integer        | no       | 用于解决议题的合并请求 IID。这将对议题描述进行自动填充并将对应讨论标记为已解决。如果您同时提供描述或标题，这些值优先于默认值。                              |
| `milestone_id`                            | integer        | no       | 议题关联的里程碑全局 ID。要查看一个里程碑的 `milestone_id`，找到一个与该里程碑关联的议题并[使用 API](#single-project-issue) 获取议题的信息。 |
| `title`                                   | string         | yes      | 议题的标题。                                                                                         |
| `weight` **(PREMIUM)**                    | integer        | no       | 议题的权重。提供的值必须大于等于 0。                                                                            |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/4/issues?title=Issues%20with%20auth&labels=bug"
```

响应示例：

```json
{
  "project_id" : 4,
  "id" : 84,
  "created_at" : "2016-01-07T12:44:33.959Z",
  "iid" : 14,
  "title" : "Issues with auth",
  "state" : "opened",
  "assignees" : [],
  "assignee" : null,
  "type" : "ISSUE",
  "labels" : [
    "bug"
  ],
  "upvotes": 4,
  "downvotes": 0,
  "merge_requests_count": 0,
  "author" : {
    "name" : "Alexandra Bashirian",
    "avatar_url" : null,
    "state" : "active",
    "web_url" : "https://gitlab.example.com/eileen.lowe",
    "id" : 18,
    "username" : "eileen.lowe"
  },
  "description" : null,
  "updated_at" : "2016-01-07T12:44:33.959Z",
  "closed_at" : null,
  "closed_by" : null,
  "milestone" : null,
  "subscribed" : true,
  "user_notes_count": 0,
  "due_date": null,
  "web_url": "http://gitlab.example.com/my-group/my-project/issues/14",
  "references": {
    "short": "#14",
    "relative": "#14",
    "full": "my-group/my-project#14"
  },
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "confidential": false,
  "discussion_locked": false,
  "issue_type": "issue",
  "severity": "UNKNOWN",
  "_links": {
    "self": "http://gitlab.example.com/api/v4/projects/1/issues/2",
    "notes": "http://gitlab.example.com/api/v4/projects/1/issues/2/notes",
    "award_emoji": "http://gitlab.example.com/api/v4/projects/1/issues/2/award_emoji",
    "project": "http://gitlab.example.com/api/v4/projects/1",
    "closed_as_duplicate_of": "http://gitlab.example.com/api/v4/projects/1/issues/75"
  },
  "task_completion_status":{
    "count":0,
    "completed_count":0
  }
}
```

<!--
Issues created by users on GitLab Premium or higher include the `weight` property:
-->

使用专业版或更高级别创建，且包含权重 `weight` 的议题：

```json
{
   "project_id" : 4,
   "description" : null,
   "weight": null,
   ...
}
```

<!--
Issues created by users on GitLab Premium or higher include the `epic` property:
-->

使用专业版或更高级别创建，且包含关联史诗 `epic` 的议题：

```json
{
   "project_id" : 4,
   "description" : "Omnis vero earum sunt corporis dolor et placeat.",
   "epic_iid" : 5, //deprecated, use `iid` of the `epic` attribute
   "epic": {
     "id" : 42,
     "iid" : 5,
     "title": "My epic epic",
     "url" : "/groups/h5bp/-/epics/5",
     "group_id": 8
   },
   ...
}
```

<!--
Issues created by users on GitLab Ultimate include the `health_status` property:
-->

使用旗舰版创建，且包含健康状态 `health_status` 的议题：

```json
[
   {
      "project_id" : 4,
      "description" : "Omnis vero earum sunt corporis dolor et placeat.",
      "health_status": "on_track",
      ...
   }
]
```

<!--
WARNING:
The `assignee` column is deprecated. We now show it as a single-sized array `assignees` to conform to the GitLab EE API.

WARNING:
The `epic_iid` attribute is deprecated and [scheduled for removal](https://gitlab.com/gitlab-org/gitlab/-/issues/35157) in API version 5.
Please use `iid` of the `epic` attribute instead.
-->

WARNING:
`assignee` 已经被弃用。我们已经转而使用 `assignees` 一维数组以维持不同版本 API 的一致性。

WARNING:
`epic_iid` 已经被弃用。我们将在第 5 版本的 API 中将其移除。
作为替代，请使用 `epic` 属性的 `iid`。

<!--
## Rate limits

To help avoid abuse, users can be limited to a specific number of `Create` requests per minute.
See [Issues rate limits](../user/admin_area/settings/rate_limit_on_issues_creation.md).
-->

<a id="rate-limits"></a>

## 速率限制

为了避免滥用情况，用户每分钟可以调用的创建相关请求的次数将被受到限制。
阅读[议题速率限制](../user/admin_area/settings/rate_limit_on_issues_creation.md)页面了解更多信息。

<!--
## Edit issue

> The `weight` property moved to GitLab Premium in 13.9.
-->

<a id="edit-issue"></a>

## 编辑议题

> `weight` 属性在 13.9 版本被移动到专业版。

<!--
Updates an existing project issue. This call is also used to mark an issue as
closed.

At least one of the following parameters is required for the request to be successful:
-->

更新一个已有的项目的议题，或者将议题关闭。

下面这些参数，您需要至少提供一个：

- `:assignee_id`
- `:assignee_ids`
- `:confidential`
- `:created_at`
- `:description`
- `:discussion_locked`
- `:due_date`
- `:issue_type`
- `:labels`
- `:milestone_id`
- `:state_event`
- `:title`

```plaintext
PUT /projects/:id/issues/:issue_iid
```

| 参数 | 类型 | 是否必需 | 描述                                                                       |
|----------------|---------|----------|--------------------------------------------------------------------------|
| `add_labels`   | string  | no       | 需要给议题添加的标签，使用半角逗号分隔。                                                     |
| `assignee_ids` | integer array | no | 议题指派人的 ID。将此项设置为 `0` 或提供一个空值以取消所有已有指派。                                   |
| `confidential` | boolean | no       | 将议题设置为私密。                                                                |
| `description`  | string  | no       | 议题的描述。不能超过 1,048,576 个字符。                                                |
| `discussion_locked` | boolean | no  | 标记此议题讨论是否被锁定。只有项目成员可以往锁定的议题添加或编辑讨论。                                      |
| `due_date`     | string  | no       | 截止日期。使用格式 `YYYY-MM-DD`，如 `2016-03-11`。                                   |
| `epic_id` **(PREMIUM)** | integer | no | 需要关联的史诗 ID。提供的值必须大于等于 0。                                                 |
| `epic_iid` **(PREMIUM)** | integer | no | 需要关联的史诗 IID。提供的值必须大于等于 0。已被弃用，第五版 API 将会移除此属性。                           |
| `id`           | integer/string | yes | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)。  |
| `issue_iid`    | integer | yes      | 项目议题内部 ID（需要管理员或项目拥有者权限）。                                                |
| `issue_type`   | string  | no       | 议题类型，可以是 `issue`、`incident` 和 `test_case` 中的一个。                          |
| `labels`       | string  | no       | 议题的标签名称，使用半角逗号分隔。使用空串来删除所有标签。                                            |
| `milestone_id` | integer | no       | 议题关联的里程碑全局 ID。将此项设置为 `0` 或提供一个空值以移除里程碑。                                  |
| `remove_labels`| string  | no       | 需要给议题删除的标签，使用半角逗号分隔。                                                     |
| `state_event`  | string  | no       | 议题的状态。设置为 `close` 以关闭议题，设置为 `reopen` 以重新打开议题。                            |
| `title`        | string  | no       | 议题的标题。                                                                   |
| `updated_at`   | string  | no       | 议题的更新时间。使用 ISO 8601 格式，如 `2016-03-11T03:45:40Z`。需要管理员或项目/群组拥有者权限。不能提供空串。 |
| `weight` **(PREMIUM)** | integer | no | 议题的权重。提供的值必须大于等于 0。                                                      |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/4/issues/85?state_event=close"
```

Example response:

```json
{
  "created_at" : "2016-01-07T12:46:01.410Z",
  "author" : {
    "name" : "Alexandra Bashirian",
    "avatar_url" : null,
    "username" : "eileen.lowe",
    "id" : 18,
    "state" : "active",
    "web_url" : "https://gitlab.example.com/eileen.lowe"
  },
  "state" : "closed",
  "title" : "Issues with auth",
  "project_id" : 4,
  "description" : null,
  "updated_at" : "2016-01-07T12:55:16.213Z",
  "closed_at" : "2016-01-08T12:55:16.213Z",
  "closed_by" : {
    "state" : "active",
    "web_url" : "https://gitlab.example.com/root",
    "avatar_url" : null,
    "username" : "root",
    "id" : 1,
    "name" : "Administrator"
  },
  "iid" : 15,
  "labels" : [
    "bug"
  ],
  "upvotes": 4,
  "downvotes": 0,
  "merge_requests_count": 0,
  "id" : 85,
  "assignees" : [],
  "assignee" : null,
  "milestone" : null,
  "subscribed" : true,
  "user_notes_count": 0,
  "due_date": "2016-07-22",
  "web_url": "http://gitlab.example.com/my-group/my-project/issues/15",
  "references": {
    "short": "#15",
    "relative": "#15",
    "full": "my-group/my-project#15"
  },
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "confidential": false,
  "discussion_locked": false,
  "issue_type": "issue",
  "severity": "UNKNOWN",
  "_links": {
    "self": "http://gitlab.example.com/api/v4/projects/1/issues/2",
    "notes": "http://gitlab.example.com/api/v4/projects/1/issues/2/notes",
    "award_emoji": "http://gitlab.example.com/api/v4/projects/1/issues/2/award_emoji",
    "project": "http://gitlab.example.com/api/v4/projects/1",
    "closed_as_duplicate_of": "http://gitlab.example.com/api/v4/projects/1/issues/75"

  },
  "task_completion_status":{
    "count":0,
    "completed_count":0
  }
}
```

<!--
Issues created by users on GitLab Premium or higher include the `weight` property:
-->

使用专业版或更高级别创建，且包含权重 `weight` 的议题：

```json
{
   "project_id" : 4,
   "description" : null,
   "weight": null,
   ...
}
```

<!--
Issues created by users on GitLab Premium or higher include the `epic` property:
-->

使用专业版或更高级别创建，且包含关联史诗 `epic` 的议题：

```json
{
   "project_id" : 4,
   "description" : "Omnis vero earum sunt corporis dolor et placeat.",
   "epic_iid" : 5, //deprecated, use `iid` of the `epic` attribute
   "epic": {
     "id" : 42,
     "iid" : 5,
     "title": "My epic epic",
     "url" : "/groups/h5bp/-/epics/5",
     "group_id": 8
   },
   ...
}
```

<!--
Issues created by users on GitLab Ultimate include the `health_status` property:
-->

使用旗舰版创建，且包含健康状态 `health_status` 的议题：

```json
[
   {
      "project_id" : 4,
      "description" : "Omnis vero earum sunt corporis dolor et placeat.",
      "health_status": "on_track",
      ...
   }
]
```

<!--
WARNING:
The `epic_iid` attribute is deprecated and [scheduled for removal](https://gitlab.com/gitlab-org/gitlab/-/issues/35157) in API version 5.
Please use `iid` of the `epic` attribute instead.

WARNING:
`assignee` column is deprecated. We now show it as a single-sized array `assignees` to conform to the GitLab EE API.
-->

WARNING:
`epic_iid` 已经被弃用。我们将在第 5 版本的 API 中将其移除。
作为替代，请使用 `epic` 属性的 `iid`。

WARNING:
`assignee` 已经被弃用。我们已经转而使用 `assignees` 一维数组以维持不同版本 API 的一致性。

<!--
## Delete an issue

Only for administrators and project owners. Deletes an issue.
-->

<a id="delete-an-issue"></a>

## 删除议题

仅使用于管理员和项目拥有者。删除一个议题。

```plaintext
DELETE /projects/:id/issues/:issue_iid
```

| 参数 | 类型 | 是否必需 | 描述                                                                      |
|-------------|---------|----------|-------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)。 |
| `issue_iid` | integer | yes      | 项目议题的内部 ID。                                                             |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/4/issues/85"
```

<!--
## Reorder an issue

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/211864) in GitLab 13.2.

Reorders an issue, you can see the results when sorting issues manually
-->

<a id="reorder-an-issue"></a>

## 重排序议题

> 引入于 13.2 版本。

将一个议题进行重新排序，手动排序后的效果可直接肉眼观察到。

```plaintext
PUT /projects/:id/issues/:issue_iid/reorder
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-------------|---------|----------|------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                             |
| `move_after_id` | integer | no | 议题后面紧跟着的项目议题的全局 ID                                                     |
| `move_before_id` | integer | no | 议题前面紧跟着的项目议题的全局 ID                                                     |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/4/issues/85/reorder?move_after_id=51&move_before_id=92"
```

<!--
## Move an issue

Moves an issue to a different project. If the target project
is the source project or the user has insufficient permissions,
an error message with status code `400` is returned.

If a given label or milestone with the same name also exists in the target
project, it's then assigned to the issue being moved.
-->

<a id="move-an-issue"></a>

## 移动议题

将议题移至不同的项目。如果目标项目就是源项目或者用户权限不足，将返回一则错误信息和 `400` 状态码。

如果目标项目中也存在给定的标签或里程碑，他们会被自动关联到议题中。

```plaintext
POST /projects/:id/issues/:issue_iid/move
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-----------------|---------|----------|------------------------------------------------------------------------|
| `id`            | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid`     | integer | yes      | 项目议题的内部 ID                                                             |
| `to_project_id` | integer | yes      | 新项目的 ID                                                                |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" --form to_project_id=5 "https://gitlab.example.com/api/v4/projects/4/issues/85/move"
```

Example response:

```json
{
  "id": 92,
  "iid": 11,
  "project_id": 5,
  "title": "Sit voluptas tempora quisquam aut doloribus et.",
  "description": "Repellat voluptas quibusdam voluptatem exercitationem.",
  "state": "opened",
  "created_at": "2016-04-05T21:41:45.652Z",
  "updated_at": "2016-04-07T12:20:17.596Z",
  "closed_at": null,
  "closed_by": null,
  "labels": [],
  "upvotes": 4,
  "downvotes": 0,
  "merge_requests_count": 0,
  "milestone": null,
  "assignees": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "assignee": {
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  },
  "type" : "ISSUE",
  "author": {
    "name": "Kris Steuber",
    "username": "solon.cremin",
    "id": 10,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/7a190fecbaa68212a4b68aeb6e3acd10?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/solon.cremin"
  },
  "due_date": null,
  "web_url": "http://gitlab.example.com/my-group/my-project/issues/11",
  "references": {
    "short": "#11",
    "relative": "#11",
    "full": "my-group/my-project#11"
  },
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "confidential": false,
  "discussion_locked": false,
  "issue_type": "issue",
  "severity": "UNKNOWN",
  "_links": {
    "self": "http://gitlab.example.com/api/v4/projects/1/issues/2",
    "notes": "http://gitlab.example.com/api/v4/projects/1/issues/2/notes",
    "award_emoji": "http://gitlab.example.com/api/v4/projects/1/issues/2/award_emoji",
    "project": "http://gitlab.example.com/api/v4/projects/1",
    "closed_as_duplicate_of": "http://gitlab.example.com/api/v4/projects/1/issues/75"
  },
  "task_completion_status":{
    "count":0,
    "completed_count":0
  }
}
```

<!--
Issues created by users on GitLab Premium or higher include the `weight` property:
-->

使用专业版或更高级别创建，且包含权重 `weight` 的议题：

```json
{
  "project_id": 5,
  "description": "Repellat voluptas quibusdam voluptatem exercitationem.",
  "weight": null,
  ...
}
```

<!--
Issues created by users on GitLab Premium or higher include the `epic` property:
-->

使用专业版或更高级别创建，且包含关联史诗 `epic` 的议题：

```json
{
   "project_id" : 4,
   "description" : "Omnis vero earum sunt corporis dolor et placeat.",
   "epic_iid" : 5, //deprecated, use `iid` of the `epic` attribute
   "epic": {
     "id" : 42,
     "iid" : 5,
     "title": "My epic epic",
     "url" : "/groups/h5bp/-/epics/5",
     "group_id": 8
   },
   ...
}
```

<!--
Issues created by users on GitLab Ultimate include the `health_status` property:
-->

使用旗舰版创建，且包含健康状态 `health_status` 的议题：

```json
[
   {
      "project_id" : 4,
      "description" : "Omnis vero earum sunt corporis dolor et placeat.",
      "health_status": "on_track",
      ...
   }
]
```

<!--
WARNING:
The `assignee` column is deprecated. We now show it as a single-sized array `assignees` to conform to the GitLab EE API.

WARNING:
The `epic_iid` attribute is deprecated and [scheduled for removal](https://gitlab.com/gitlab-org/gitlab/-/issues/35157) in API version 5.
Please use `iid` of the `epic` attribute instead.
-->

WARNING:
`assignee` 已经被弃用。我们已经转而使用 `assignees` 一维数组以维持不同版本 API 的一致性。

WARNING:
`epic_iid` 已经被弃用。我们将在第 5 版本的 API 中将其移除。
作为替代，请使用 `epic` 属性的 `iid`。

<!--
## Clone an issue

Clone the issue to given project. If the user has insufficient permissions,
an error message with status code `400` is returned.

Copies as much data as possible as long as the target project contains equivalent labels, milestones,
and so on.
-->

<a id="clone-an-issue"></a>

## 克隆议题

将议题克隆至指定的项目。如果用户没有对应的权限，将返回一则错误信息和 `400` 状态码。

只要目标项目包含等效标准，如标记或里程碑，就尽可能多地复制数据。

```plaintext
POST /projects/:id/issues/:issue_iid/clone
```

| 参数 | 类型 | 是否必需 | 描述 |
| --------------- | -------------- | ---------------------- | --------------------------------- |
| `id`            | integer/string | **{check-circle}** Yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)。 |
| `issue_iid`     | integer        | **{check-circle}** Yes | 项目议题的内部 ID。 |
| `to_project_id` | integer        | **{check-circle}** Yes | 目标项目的 ID。 |
| `with_notes`    | boolean        | **{dotted-circle}** No | 是否连同[备注](notes.md)一同迁移。默认值为 `false`。 |

```shell
curl --request POST \
--header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/1/clone?with_notes=true&to_project_id=6"
```

响应示例：

```json
{
  "id":290,
  "iid":1,
  "project_id":143,
  "title":"foo",
  "description":"closed",
  "state":"opened",
  "created_at":"2021-09-14T22:24:11.696Z",
  "updated_at":"2021-09-14T22:24:11.696Z",
  "closed_at":null,
  "closed_by":null,
  "labels":[

  ],
  "milestone":null,
  "assignees":[
    {
      "id":179,
      "name":"John Doe2",
      "username":"john",
      "state":"active",
      "avatar_url":"https://www.gravatar.com/avatar/10fc7f102be8de7657fb4d80898bbfe3?s=80\u0026d=identicon",
      "web_url":"https://gitlab.example.com/john"
    }
  ],
  "author":{
    "id":179,
    "name":"John Doe2",
    "username":"john",
    "state":"active",
    "avatar_url":"https://www.gravatar.com/avatar/10fc7f102be8de7657fb4d80898bbfe3?s=80\u0026d=identicon",
    "web_url":"https://gitlab.example.com/john"
  },
  "type":"ISSUE",
  "assignee":{
    "id":179,
    "name":"John Doe2",
    "username":"john",
    "state":"active",
    "avatar_url":"https://www.gravatar.com/avatar/10fc7f102be8de7657fb4d80898bbfe3?s=80\u0026d=identicon",
    "web_url":"https://gitlab.example.com/john"
  },
  "user_notes_count":1,
  "merge_requests_count":0,
  "upvotes":0,
  "downvotes":0,
  "due_date":null,
  "confidential":false,
  "discussion_locked":null,
  "issue_type":"issue",
  "severity": "UNKNOWN",
  "web_url":"https://gitlab.example.com/namespace1/project2/-/issues/1",
  "time_stats":{
    "time_estimate":0,
    "total_time_spent":0,
    "human_time_estimate":null,
    "human_total_time_spent":null
  },
  "task_completion_status":{
    "count":0,
    "completed_count":0
  },
  "blocking_issues_count":0,
  "has_tasks":false,
  "_links":{
    "self":"https://gitlab.example.com/api/v4/projects/143/issues/1",
    "notes":"https://gitlab.example.com/api/v4/projects/143/issues/1/notes",
    "award_emoji":"https://gitlab.example.com/api/v4/projects/143/issues/1/award_emoji",
    "project":"https://gitlab.example.com/api/v4/projects/143",
    "closed_as_duplicate_of": "http://gitlab.example.com/api/v4/projects/1/issues/75"
  },
  "references":{
    "short":"#1",
    "relative":"#1",
    "full":"namespace1/project2#1"
  },
  "subscribed":true,
  "moved_to_id":null,
  "service_desk_reply_to":null
}
```

<!--
## Subscribe to an issue

Subscribes the authenticated user to an issue to receive notifications.
If the user is already subscribed to the issue, the status code `304`
is returned.
-->

<a id="subscribe-to-an-issue"></a>

## 订阅议题

经过身份验证的用户可以订阅议题以接收该议题的通知。
如果用户已经订阅了某个议题，将返回 `304` 状态码。

```plaintext
POST /projects/:id/issues/:issue_iid/subscribe
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-------------|---------|----------|------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                             |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/93/subscribe"
```

响应示例：

```json
{
  "id": 92,
  "iid": 11,
  "project_id": 5,
  "title": "Sit voluptas tempora quisquam aut doloribus et.",
  "description": "Repellat voluptas quibusdam voluptatem exercitationem.",
  "state": "opened",
  "created_at": "2016-04-05T21:41:45.652Z",
  "updated_at": "2016-04-07T12:20:17.596Z",
  "closed_at": null,
  "closed_by": null,
  "labels": [],
  "upvotes": 4,
  "downvotes": 0,
  "merge_requests_count": 0,
  "milestone": null,
  "assignees": [{
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  }],
  "assignee": {
    "name": "Miss Monserrate Beier",
    "username": "axel.block",
    "id": 12,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/axel.block"
  },
  "type" : "ISSUE",
  "author": {
    "name": "Kris Steuber",
    "username": "solon.cremin",
    "id": 10,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/7a190fecbaa68212a4b68aeb6e3acd10?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/solon.cremin"
  },
  "due_date": null,
  "web_url": "http://gitlab.example.com/my-group/my-project/issues/11",
  "references": {
    "short": "#11",
    "relative": "#11",
    "full": "my-group/my-project#11"
  },
  "time_stats": {
    "time_estimate": 0,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "confidential": false,
  "discussion_locked": false,
  "issue_type": "issue",
  "severity": "UNKNOWN",
  "_links": {
    "self": "http://gitlab.example.com/api/v4/projects/1/issues/2",
    "notes": "http://gitlab.example.com/api/v4/projects/1/issues/2/notes",
    "award_emoji": "http://gitlab.example.com/api/v4/projects/1/issues/2/award_emoji",
    "project": "http://gitlab.example.com/api/v4/projects/1",
    "closed_as_duplicate_of": "http://gitlab.example.com/api/v4/projects/1/issues/75"
  },
  "task_completion_status":{
    "count":0,
    "completed_count":0
  }
}
```

<!--
Issues created by users on GitLab Premium or higher include the `weight` property:
-->

使用专业版或更高级别创建，且包含权重 `weight` 的议题：

```json
{
  "project_id": 5,
  "description": "Repellat voluptas quibusdam voluptatem exercitationem.",
  "weight": null,
  ...
}
```

<!--
Issues created by users on GitLab Premium or higher include the `epic` property:
-->

使用专业版或更高级别创建，且包含关联史诗 `epic` 的议题：

```json
{
   "project_id" : 4,
   "description" : "Omnis vero earum sunt corporis dolor et placeat.",
   "epic_iid" : 5, //deprecated, use `iid` of the `epic` attribute
   "epic": {
     "id" : 42,
     "iid" : 5,
     "title": "My epic epic",
     "url" : "/groups/h5bp/-/epics/5",
     "group_id": 8
   },
   ...
}
```

<!--
Issues created by users on GitLab Ultimate include the `health_status` property:
-->

使用旗舰版创建，且包含健康状态 `health_status` 的议题：

```json
[
   {
      "project_id" : 4,
      "description" : "Omnis vero earum sunt corporis dolor et placeat.",
      "health_status": "on_track",
      ...
   }
]
```

<!--
WARNING:
The `assignee` column is deprecated. We now show it as a single-sized array `assignees` to conform to the GitLab EE API.

WARNING:
The `epic_iid` attribute is deprecated and [scheduled for removal](https://gitlab.com/gitlab-org/gitlab/-/issues/35157) in API version 5.
Please use `iid` of the `epic` attribute instead.
-->

WARNING:
`assignee` 已经被弃用。我们已经转而使用 `assignees` 一维数组以维持不同版本 API 的一致性。

WARNING:
`epic_iid` 已经被弃用。我们将在第 5 版本的 API 中将其移除。
作为替代，请使用 `epic` 属性的 `iid`。

<!--
## Unsubscribe from an issue

Unsubscribes the authenticated user from the issue to not receive notifications
from it. If the user is not subscribed to the issue, the
status code `304` is returned.
-->

<a id="unsubscribe-from-an-issue"></a>

## 取消订阅议题

经过身份验证的用户可以取消订阅议题以不再接收该议题的通知。
如果用户并没有订阅这个议题，将返回 `304` 状态码。

```plaintext
POST /projects/:id/issues/:issue_iid/unsubscribe
```

| 参数 | 类型 | 是否必需 | 描述                                                                      |
|-------------|---------|----------|-------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或 [经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                              |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/93/unsubscribe"
```

响应示例：

```json
{
  "id": 93,
  "iid": 12,
  "project_id": 5,
  "title": "Incidunt et rerum ea expedita iure quibusdam.",
  "description": "Et cumque architecto sed aut ipsam.",
  "state": "opened",
  "created_at": "2016-04-05T21:41:45.217Z",
  "updated_at": "2016-04-07T13:02:37.905Z",
  "labels": [],
  "upvotes": 4,
  "downvotes": 0,
  "merge_requests_count": 0,
  "milestone": null,
  "assignee": {
    "name": "Edwardo Grady",
    "username": "keyon",
    "id": 21,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/3e6f06a86cf27fa8b56f3f74f7615987?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/keyon"
  },
  "type" : "ISSUE",
  "closed_at": null,
  "closed_by": null,
  "author": {
    "name": "Vivian Hermann",
    "username": "orville",
    "id": 11,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/5224fd70153710e92fb8bcf79ac29d67?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/orville"
  },
  "subscribed": false,
  "due_date": null,
  "web_url": "http://gitlab.example.com/my-group/my-project/issues/12",
  "references": {
    "short": "#12",
    "relative": "#12",
    "full": "my-group/my-project#12"
  },
  "confidential": false,
  "discussion_locked": false,
  "issue_type": "issue",
  "severity": "UNKNOWN",
  "task_completion_status":{
    "count":0,
    "completed_count":0
  }
}
```

<!--
## Create a to-do item

Manually creates a to-do item for the current user on an issue. If
there already exists a to-do item for the user on that issue, status code `304` is
returned.
-->

<a id="create-a-to-do-item"></a>

## 创建待办事项

为当前用户基于某个议题手动创建待办事项。
如果这个议题已经是用户的待办事项，将返回 `304` 状态码。

```plaintext
POST /projects/:id/issues/:issue_iid/todo
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-------------|---------|----------|------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                             |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/93/todo"
```

响应示例：

```json
{
  "id": 112,
  "project": {
    "id": 5,
    "name": "GitLab CI/CD",
    "name_with_namespace": "GitLab Org / GitLab CI/CD",
    "path": "gitlab-ci",
    "path_with_namespace": "gitlab-org/gitlab-ci"
  },
  "author": {
    "name": "Administrator",
    "username": "root",
    "id": 1,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/root"
  },
  "action_name": "marked",
  "target_type": "Issue",
  "target": {
    "id": 93,
    "iid": 10,
    "project_id": 5,
    "title": "Vel voluptas atque dicta mollitia adipisci qui at.",
    "description": "Tempora laboriosam sint magni sed voluptas similique.",
    "state": "closed",
    "created_at": "2016-06-17T07:47:39.486Z",
    "updated_at": "2016-07-01T11:09:13.998Z",
    "labels": [],
    "milestone": {
      "id": 26,
      "iid": 1,
      "project_id": 5,
      "title": "v0.0",
      "description": "Accusantium nostrum rerum quae quia quis nesciunt suscipit id.",
      "state": "closed",
      "created_at": "2016-06-17T07:47:33.832Z",
      "updated_at": "2016-06-17T07:47:33.832Z",
      "due_date": null
    },
    "assignees": [{
      "name": "Jarret O'Keefe",
      "username": "francisca",
      "id": 14,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/a7fa515d53450023c83d62986d0658a8?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/francisca"
    }],
    "assignee": {
      "name": "Jarret O'Keefe",
      "username": "francisca",
      "id": 14,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/a7fa515d53450023c83d62986d0658a8?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/francisca"
    },
    "type" : "ISSUE",
    "author": {
      "name": "Maxie Medhurst",
      "username": "craig_rutherford",
      "id": 12,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/a0d477b3ea21970ce6ffcbb817b0b435?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/craig_rutherford"
    },
    "subscribed": true,
    "user_notes_count": 7,
    "upvotes": 0,
    "downvotes": 0,
    "merge_requests_count": 0,
    "due_date": null,
    "web_url": "http://gitlab.example.com/my-group/my-project/issues/10",
    "references": {
      "short": "#10",
      "relative": "#10",
      "full": "my-group/my-project#10"
    },
    "confidential": false,
    "discussion_locked": false,
    "issue_type": "issue",
    "severity": "UNKNOWN",
    "task_completion_status":{
      "count":0,
      "completed_count":0
    }
  },
  "target_url": "https://gitlab.example.com/gitlab-org/gitlab-ci/issues/10",
  "body": "Vel voluptas atque dicta mollitia adipisci qui at.",
  "state": "pending",
  "created_at": "2016-07-01T11:09:13.992Z"
}
```

<!--
WARNING:
The `assignee` column is deprecated. We now show it as a single-sized array `assignees` to conform to the GitLab EE API.
-->

WARNING:
`assignee` 已经被弃用。我们已经转而使用 `assignees` 一维数组以维持不同版本 API 的一致性。

<!--
## Promote an issue to an epic **(PREMIUM)**

Promotes an issue to an epic by adding a comment with the `/promote`
[quick action](../user/project/quick_actions.md).

To learn more about promoting issues to epics, visit [Manage epics](../user/group/epics/manage_epics.md#promote-an-issue-to-an-epic).
-->

<a id="promote-an-issue-to-an-epic"></a>

## 将议题升级为史诗 **(PREMIUM)**

通过添加 `/promote` [快速操作](../user/project/quick_actions.md)评论将议题升级为史诗。

要了解将议题升级为史诗的更多信息，请访问[管理史诗](../user/group/epics/manage_epics.md#promote-an-issue-to-an-epic) 页面。

```plaintext
POST /projects/:id/issues/:issue_iid/notes
```

<!--
Supported attributes:
-->

支持的属性：

| 参数 | 类型 | 是否必需 | 描述                                                                      |
| :---------- | :------------- | :------- |:------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)。 |
| `issue_iid` | integer        | yes      | 项目议题的内部 ID。                                                             |
| `body`      | String         | yes      | 记录的内容。必须将 `/promote` 置于单独一行的开头。                                         |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/notes?body=Lets%20promote%20this%20to%20an%20epic%0A%0A%2Fpromote"
```

响应示例：

```json
{
   "id":699,
   "type":null,
   "body":"Lets promote this to an epic",
   "attachment":null,
   "author": {
      "id":1,
      "name":"Alexandra Bashirian",
      "username":"eileen.lowe",
      "state":"active",
      "avatar_url":"https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url":"https://gitlab.example.com/eileen.lowe"
   },
   "created_at":"2020-12-03T12:27:17.844Z",
   "updated_at":"2020-12-03T12:27:17.844Z",
   "system":false,
   "noteable_id":461,
   "noteable_type":"Issue",
   "resolvable":false,
   "confidential":false,
   "noteable_iid":33,
   "commands_changes": {
      "promote_to_epic":true
   }
}
```

<!--
## Set a time estimate for an issue

Sets an estimated time of work for this issue.
-->

<a id="set-a-time-estimate-for-an-issue"></a>

## 为议题设置预计用时

为某个议题设置您预计需要在其所耗费的时间。

```plaintext
POST /projects/:id/issues/:issue_iid/time_estimate
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-------------|---------|----------|------------------------------------------------------------------------|
| `duration`  | string  | yes      | 预计用时，如 `3h30m`                                                         |
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                             |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/93/time_estimate?duration=3h30m"
```

响应示例：

```json
{
  "human_time_estimate": "3h 30m",
  "human_total_time_spent": null,
  "time_estimate": 12600,
  "total_time_spent": 0
}
```

<!--
## Reset the time estimate for an issue

Resets the estimated time for this issue to 0 seconds.
-->

<a id="reset-the-time-estimate-for-an-issue"></a>

## 为议题重置预计用时

将议题的预计用时重置为零。

```plaintext
POST /projects/:id/issues/:issue_iid/reset_time_estimate
```

| 参数 | 类型 | 是否必需 | 描述                                                                      |
|-------------|---------|----------|-------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或 [经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                              |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/93/reset_time_estimate"
```

响应示例：

```json
{
  "human_time_estimate": null,
  "human_total_time_spent": null,
  "time_estimate": 0,
  "total_time_spent": 0
}
```

<!--
## Add spent time for an issue

Adds spent time for this issue
-->

<a id="add-spent-time-for-an-issue"></a>

## 为议题添加已用时间

为议题添加您已经在其耗费的时间。

```plaintext
POST /projects/:id/issues/:issue_iid/add_spent_time
```

| 参数 | 类型 | 是否必需 | 描述                                                                      |
|-------------|---------|----------|-------------------------------------------------------------------------|
| `duration`  | string  | yes      | 所耗费的时间，如 `3h30m`                                                        |
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或 [经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                              |
| `summary`   | string  | no       | 简要描述这段时间是如何耗费的                                                          |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/93/add_spent_time?duration=1h"
```

响应示例：

```json
{
  "human_time_estimate": null,
  "human_total_time_spent": "1h",
  "time_estimate": 0,
  "total_time_spent": 3600
}
```

<!--
## Reset spent time for an issue

Resets the total spent time for this issue to 0 seconds.
-->

<a id="reset-spent-time-for-an-issue"></a>

## 为议题重置已用时间

将议题的已用时间重置为零。

```plaintext
POST /projects/:id/issues/:issue_iid/reset_spent_time
```

| 参数 | 类型 | 是否必需 | 描述                                                                      |
|-------------|---------|----------|-------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或 [经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                              |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/93/reset_spent_time"
```

响应示例：

```json
{
  "human_time_estimate": null,
  "human_total_time_spent": null,
  "time_estimate": 0,
  "total_time_spent": 0
}
```

<!--
## Get time tracking stats

If the project is private or the issue is confidential, you need to provide credentials to authorize.
The preferred way to do this, is by using [personal access tokens](../user/profile/personal_access_tokens.md).
-->

<a id="get-time-tracking-stats"></a>

## 获取工时统计信息

如果这个项目是私有的或议题是私密的，请求者必须提供有效的鉴权信息。
我们推荐您使用[个人访问令牌](../user/profile/personal_access_tokens.md)进行鉴权。

```plaintext
GET /projects/:id/issues/:issue_iid/time_stats
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-------------|---------|----------|------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                             |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/93/time_stats"
```

响应示例：

```json
{
  "human_time_estimate": "2h",
  "human_total_time_spent": "1h",
  "time_estimate": 7200,
  "total_time_spent": 3600
}
```

<!--
## List merge requests related to issue

Get all the merge requests that are related to the issue.

If the project is private or the issue is confidential, you need to provide credentials to authorize.
The preferred way to do this, is by using [personal access tokens](../user/profile/personal_access_tokens.md).
-->

<a id="list-merge-requests-related-to-issue"></a>

## 列出议题关联的合并请求

获取所有与给定议题相关联的合并请求。

如果这个项目是私有的或议题是私密的，请求者必须提供有效的鉴权信息。
我们推荐您使用[个人访问令牌](../user/profile/personal_access_tokens.md)进行鉴权。

```plaintext
GET /projects/:id/issues/:issue_iid/related_merge_requests
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-------------|---------|----------|------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                             |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/issues/11/related_merge_requests"
```

响应示例：

```json
[
  {
    "id": 29,
    "iid": 11,
    "project_id": 1,
    "title": "Provident eius eos blanditiis consequatur neque odit.",
    "description": "Ut consequatur ipsa aspernatur quisquam voluptatum fugit. Qui harum corporis quo fuga ut incidunt veritatis. Autem necessitatibus et harum occaecati nihil ea.\r\n\r\ntwitter/flight#8",
    "state": "opened",
    "created_at": "2018-09-18T14:36:15.510Z",
    "updated_at": "2018-09-19T07:45:13.089Z",
    "closed_by": null,
    "closed_at": null,
    "target_branch": "v2.x",
    "source_branch": "so_long_jquery",
    "user_notes_count": 9,
    "upvotes": 0,
    "downvotes": 0,
    "author": {
      "id": 14,
      "name": "Verna Hills",
      "username": "lawanda_reinger",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/de68a91aeab1cff563795fb98a0c2cc0?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/lawanda_reinger"
    },
    "assignee": {
      "id": 19,
      "name": "Jody Baumbach",
      "username": "felipa.kuvalis",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/6541fc75fc4e87e203529bd275fafd07?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/felipa.kuvalis"
    },
    "source_project_id": 1,
    "target_project_id": 1,
    "labels": [],
    "draft": false,
    "work_in_progress": false,
    "milestone": {
      "id": 27,
      "iid": 2,
      "project_id": 1,
      "title": "v1.0",
      "description": "Et tenetur voluptatem minima doloribus vero dignissimos vitae.",
      "state": "active",
      "created_at": "2018-09-18T14:35:44.353Z",
      "updated_at": "2018-09-18T14:35:44.353Z",
      "due_date": null,
      "start_date": null,
      "web_url": "https://gitlab.example.com/twitter/flight/milestones/2"
    },
    "merge_when_pipeline_succeeds": false,
    "merge_status": "cannot_be_merged",
    "sha": "3b7b528e9353295c1c125dad281ac5b5deae5f12",
    "merge_commit_sha": null,
    "squash_commit_sha": null,
    "discussion_locked": null,
    "should_remove_source_branch": null,
    "force_remove_source_branch": false,
    "reference": "!11",
    "web_url": "https://gitlab.example.com/twitter/flight/merge_requests/4",
    "references": {
      "short": "!4",
      "relative": "!4",
      "full": "twitter/flight!4"
    },
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    },
    "squash": false,
    "task_completion_status": {
      "count": 0,
      "completed_count": 0
    },
    "changes_count": "10",
    "latest_build_started_at": "2018-12-05T01:16:41.723Z",
    "latest_build_finished_at": "2018-12-05T02:35:54.046Z",
    "first_deployed_to_production_at": null,
    "pipeline": {
      "id": 38980952,
      "sha": "81c6a84c7aebd45a1ac2c654aa87f11e32338e0a",
      "ref": "test-branch",
      "status": "success",
      "web_url": "https://gitlab.com/gitlab-org/gitlab/pipelines/38980952"
    },
    "head_pipeline": {
      "id": 38980952,
      "sha": "81c6a84c7aebd45a1ac2c654aa87f11e32338e0a",
      "ref": "test-branch",
      "status": "success",
      "web_url": "https://gitlab.example.com/twitter/flight/pipelines/38980952",
      "before_sha": "3c738a37eb23cf4c0ed0d45d6ddde8aad4a8da51",
      "tag": false,
      "yaml_errors": null,
      "user": {
        "id": 19,
        "name": "Jody Baumbach",
        "username": "felipa.kuvalis",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/6541fc75fc4e87e203529bd275fafd07?s=80&d=identicon",
        "web_url": "https://gitlab.example.com/felipa.kuvalis"
      },
      "created_at": "2018-12-05T01:16:13.342Z",
      "updated_at": "2018-12-05T02:35:54.086Z",
      "started_at": "2018-12-05T01:16:41.723Z",
      "finished_at": "2018-12-05T02:35:54.046Z",
      "committed_at": null,
      "duration": 4436,
      "coverage": "46.68",
      "detailed_status": {
        "icon": "status_warning",
        "text": "passed",
        "label": "passed with warnings",
        "group": "success-with-warnings",
        "tooltip": "passed",
        "has_details": true,
        "details_path": "/twitter/flight/pipelines/38",
        "illustration": null,
        "favicon": "https://gitlab.example.com/assets/ci_favicons/favicon_status_success-8451333011eee8ce9f2ab25dc487fe24a8758c694827a582f17f42b0a90446a2.png"
      }
    },
    "diff_refs": {
      "base_sha": "d052d768f0126e8cddf80afd8b1eb07f406a3fcb",
      "head_sha": "81c6a84c7aebd45a1ac2c654aa87f11e32338e0a",
      "start_sha": "d052d768f0126e8cddf80afd8b1eb07f406a3fcb"
    },
    "merge_error": null,
    "user": {
      "can_merge": true
    }
  }
]
```

<!--
## List merge requests that close a particular issue on merge

Get all merge requests that close a particular issue when merged.

If the project is private or the issue is confidential, you need to provide credentials to authorize.
The preferred way to do this, is by using [personal access tokens](../user/profile/personal_access_tokens.md).
-->

<a id="list-merge-requests-that-close-a-particular-issue-on-merge"></a>

## 列出合并时关闭议题的合并请求

列出所有在合并的时候能关闭指定议题的合并请求。

如果这个项目是私有的或议题是私密的，请求者必须提供有效的鉴权信息。
我们推荐您使用[个人访问令牌](../user/profile/personal_access_tokens.md)进行鉴权。

```plaintext
GET /projects/:id/issues/:issue_iid/closed_by
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
| ----------- | ---------------| -------- |------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer        | yes      | 项目议题的内部 ID                                                             |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/issues/11/closed_by"
```

响应示例：

```json
[
  {
    "id": 6471,
    "iid": 6432,
    "project_id": 1,
    "title": "add a test for cgi lexer options",
    "description": "closes #11",
    "state": "opened",
    "created_at": "2017-04-06T18:33:34.168Z",
    "updated_at": "2017-04-09T20:10:24.983Z",
    "target_branch": "master",
    "source_branch": "feature.custom-highlighting",
    "upvotes": 0,
    "downvotes": 0,
    "author": {
      "name": "Administrator",
      "username": "root",
      "id": 1,
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/root"
    },
    "assignee": null,
    "source_project_id": 1,
    "target_project_id": 1,
    "closed_at": null,
    "closed_by": null,
    "labels": [],
    "draft": false,
    "work_in_progress": false,
    "milestone": null,
    "merge_when_pipeline_succeeds": false,
    "merge_status": "unchecked",
    "sha": "5a62481d563af92b8e32d735f2fa63b94e806835",
    "merge_commit_sha": null,
    "squash_commit_sha": null,
    "user_notes_count": 1,
    "should_remove_source_branch": null,
    "force_remove_source_branch": false,
    "web_url": "https://gitlab.example.com/gitlab-org/gitlab-test/merge_requests/6432",
    "reference": "!6432",
    "references": {
      "short": "!6432",
      "relative": "!6432",
      "full": "gitlab-org/gitlab-test!6432"
    },
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    }
  }
]
```

<!--
## Participants on issues

If the project is private or the issue is confidential, you need to provide credentials to authorize.
The preferred way to do this, is by using [personal access tokens](../user/profile/personal_access_tokens.md).
-->

<a id="participants-on-issues"></a>

## 议题参与人

如果这个项目是私有的或议题是私密的，请求者必须提供有效的鉴权信息。
我们推荐您使用[个人访问令牌](../user/profile/personal_access_tokens.md)进行鉴权。

```plaintext
GET /projects/:id/issues/:issue_iid/participants
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-------------|---------|----------|------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                             |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/93/participants"
```
响应示例：

```json
[
  {
    "id": 1,
    "name": "John Doe1",
    "username": "user1",
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/c922747a93b40d1ea88262bf1aebee62?s=80&d=identicon",
    "web_url": "http://gitlab.example.com/user1"
  },
  {
    "id": 5,
    "name": "John Doe5",
    "username": "user5",
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/4aea8cf834ed91844a2da4ff7ae6b491?s=80&d=identicon",
    "web_url": "http://gitlab.example.com/user5"
  }
]
```

<!--
## Comments on issues

Comments are done via the [notes](notes.md) resource.
-->

<a id="comments-on-issues"></a>

## 议题评论

评论相关操作由[备注](notes.md)相关接口实现。

<!--
## Get user agent details

Available only for administrators.
-->

<a id="get-user-agent-details"></a>

## 获取用户代理信息

仅适用于管理员。

```plaintext
GET /projects/:id/issues/:issue_iid/user_agent_detail
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-------------|---------|----------|------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                             |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/93/user_agent_detail"
```

响应示例：

```json
{
  "user_agent": "AppleWebKit/537.36",
  "ip_address": "127.0.0.1",
  "akismet_submitted": false
}
```

<!--
## List issue state events

To track which state was set, who did it, and when it happened, check out
[Resource state events API](resource_state_events.md#issues).
-->

<a id="list-issue-state-events"></a>

## 列出议题状态事件

要追踪议题当前状态、由谁设置、发生时间，可移步[资源状态事件 API](resource_state_events.md#issues)。

<!--
## Upload metric image

Available only for Incident issues.
-->

<a id="upload-metric-image"></a>

## 上传指标图像

仅适用于事件议题。

```plaintext
POST /projects/:id/issues/:issue_iid/metric_images
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-------------|---------|----------|------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                             |
| `file` | file | yes      | 需要上传的图像文件                                                              |
| `url` | string | no      | 事件详情的链接                                                                |
| `url_text` | string | no      | 图像或链接的描述                                                               |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" --form 'file=@/path/to/file.png' \
--form 'url=http://example.com' --form 'url_text=Example website' "https://gitlab.example.com/api/v4/projects/5/issues/93/metric_images"
```

响应示例：

```json
{
    "id": 23,
    "created_at": "2020-11-13T00:06:18.084Z",
    "filename": "file.png",
    "file_path": "/uploads/-/system/issuable_metric_image/file/23/file.png",
    "url": "http://example.com",
    "url_text": "Example website"
}
```

<!--
## List metric images

Available only for Incident issues.
-->

<a id="list-metric-images"></a>

## 列出指标图像

仅适用于事件议题。

```plaintext
GET /projects/:id/issues/:issue_iid/metric_images
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-------------|---------|----------|------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                             |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/93/metric_images"
```

响应示例：

```json
[
    {
        "id": 17,
        "created_at": "2020-11-12T20:07:58.156Z",
        "filename": "sample_2054",
        "file_path": "/uploads/-/system/issuable_metric_image/file/17/sample_2054.png",
        "url": "example.com/metric"
    },
    {
        "id": 18,
        "created_at": "2020-11-12T20:14:26.441Z",
        "filename": "sample_2054",
        "file_path": "/uploads/-/system/issuable_metric_image/file/18/sample_2054.png",
        "url": "example.com/metric"
    }
]
```

<!--
## Update metric image

Available only for Incident issues.
-->

<a id="update-metric-image"></a>

## 更新指标图像

仅适用于事件议题。

```plaintext
PUT /projects/:id/issues/:issue_iid/metric_images/:image_id
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-------------|---------|----------|------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                             |
| `image_id` | integer | yes      | 图像 ID                                                                  |
| `url` | string | no      | 事件详情的链接                                                                |
| `url_text` | string | no      | 图像或链接的描述                                                               |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" --request PUT  --form 'url=http://example.com' --form 'url_text=Example website' "https://gitlab.example.com/api/v4/projects/5/issues/93/metric_images/1"
```

响应示例：

```json
{
    "id": 23,
    "created_at": "2020-11-13T00:06:18.084Z",
    "filename": "file.png",
    "file_path": "/uploads/-/system/issuable_metric_image/file/23/file.png",
    "url": "http://example.com",
    "url_text": "Example website"
}
```

<!--
## Delete metric image

Available only for Incident issues.
-->

<a id="delete-metric-image"></a>

## 删除指标图像

仅适用于事件议题。

```plaintext
DELETE /projects/:id/issues/:issue_iid/metric_images/:image_id
```

| 参数 | 类型 | 是否必需 | 描述                                                                     |
|-------------|---------|----------|------------------------------------------------------------------------|
| `id`        | integer/string | yes      | 经过身份验证的用户拥有的全局 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer | yes      | 项目议题的内部 ID                                                             |
| `image_id` | integer | yes      | 图像 ID                                                                  |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" --request DELETE "https://gitlab.example.com/api/v4/projects/5/issues/93/metric_images/1"
```

可能返回的状态码包括：

- `204 No Content` 表示图像删除成功。
- `400 Bad Request` 表示图像无法被删除。
