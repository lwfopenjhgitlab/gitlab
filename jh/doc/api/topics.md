---
stage: Data Stores
group: Tenant Scale
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 主题 API **(FREE)**

> 引入于极狐GitLab 14.5。

使用 REST API 与项目主题进行交互。

## 获取主题列表

列出所有在极狐GitLab 实例中的项目主题，并按照与主题关联项目的数量进行排序。

```plaintext
GET /topics
```

支持参数:

| 参数 | 类型    | 是否必需 | 描述                    |
| ---------- | ------- | ---------------------- |-----------------------|
| `page`     | integer | **{dotted-circle}** No | 获取第几页的数据，默认是 `1`。     |
| `per_page` | integer | **{dotted-circle}** No | 指定一页返回数据的数量，默认是 `20`。 |
| `search`   | string  | **{dotted-circle}** No | 按照主题的 `name` 属性进行搜索。  |
| `without_projects` | boolean | **{dotted-circle}** No | 将结果限制为没有分配项目的主题。      |

请求示例:

```shell
curl "https://gitlab.example.com/api/v4/topics?search=git"
```

响应示例:

```json
[
  {
    "id": 1,
    "name": "gitlab",
    "title": "GitLab",
    "description": "GitLab is an open source end-to-end software development platform with built-in version control, issue tracking, code review, CI/CD, and more.",
    "total_projects_count": 1000,
    "avatar_url": "http://www.gravatar.com/avatar/a0d477b3ea21970ce6ffcbb817b0b435?s=80&d=identicon"
  },
  {
    "id": 3,
    "name": "git",
    "title": "Git",
    "description": "Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.",
    "total_projects_count": 900,
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon"
  },
  {
    "id": 2,
    "name": "git-lfs",
    "title": "Git LFS",
    "description": null,
    "total_projects_count": 300,
    "avatar_url": null
  }
]
```

## 获取一个主题

通过 ID 获取某一个主题。

```plaintext
GET /topics/:id
```

支持参数:

| 参数 | 类型    | 是否必需 | 描述                      |
| --------- | ------- | ---------------------- | ------------------- |
| `id`      | integer | **{check-circle}** Yes | 项目主题的 ID。 |

请求示例:

```shell
curl "https://gitlab.example.com/api/v4/topics/1"
```

响应示例:

```json
{
  "id": 1,
  "name": "gitlab",
  "title": "GitLab",
  "description": "GitLab is an open source end-to-end software development platform with built-in version control, issue tracking, code review, CI/CD, and more.",
  "total_projects_count": 1000,
  "avatar_url": "http://www.gravatar.com/avatar/a0d477b3ea21970ce6ffcbb817b0b435?s=80&d=identicon"
}
```

## 获取被指派某主题的项目列表

使用[项目 API](projects.md#list-all-projects) 来获取指派了某一个主题的所有项目列表。

```plaintext
GET /projects?topic=<topic_name>
```

## 创建一个项目主题

创建一个项目主题，只有管理员可以使用该API。

```plaintext
POST /topics
```

支持参数:

| 参数 | 类型    | 是否必需 | 描述                      |
| ------------- | ------- | ---------------------- | ----------- |
| `name`        | string  | **{check-circle}** Yes | Slug（名称） |
| `title`       | string  | **{check-circle}** Yes | 标题       |
| `avatar`      | file    | **{dotted-circle}** No | 头像      |
| `description` | string  | **{dotted-circle}** No | 描述 |

请求示例:

```shell
curl --request POST \
     --data "name=topic1&title=Topic 1" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/topics"
```

响应示例:

```json
{
  "id": 1,
  "name": "topic1",
  "title": "Topic 1",
  "description": null,
  "total_projects_count": 0,
  "avatar_url": null
}
```

## 更新一个项目主题

更新一个项目主题，只有管理员可以使用该 API。

```plaintext
PUT /topics/:id
```

支持参数:

| 参数 | 类型    | 是否必需 | 描述                      |
| ------------- | ------- | ---------------------- | ------------------- |
| `id`          | integer | **{check-circle}** Yes | 项目主题的 ID |
| `avatar`      | file    | **{dotted-circle}** No | 头像              |
| `description` | string  | **{dotted-circle}** No | 描述         |
| `name`        | string  | **{dotted-circle}** No | Slug（名称）       |
| `title`       | string  | **{dotted-circle}** No | 标题               |

请求示例:

```shell
curl --request PUT \
     --data "name=topic1" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/topics/1"
```

响应示例:

```json
{
  "id": 1,
  "name": "topic1",
  "title": "Topic 1",
  "description": null,
  "total_projects_count": 0,
  "avatar_url": null
}
```

### 更新项目主题的头像

为了从您的文件系统中上传头像文件，请使用 `--form` 参数。这个参数会通过 cURL 使用 HTTP 头 `Content-Type: multipart/form-data` 来上传数据。参数 `file=` 必须指向一个文件系统中的文件，从在最前面添加字符 `@`。比如：

```shell
curl --request PUT \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/topics/1" \
     --form "avatar=@/tmp/example.png"
```

### 删除项目主题的头像

> 引入于极狐GitLab 14.6。

通过对参数 `avatar` 设置为空值来删除项目主题的头像。

请求示例:

```shell
curl --request PUT \
     --data "avatar=" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/topics/1"
```

## 删除项目主题

> 引入于极狐GitLab 14.9。

您必须是管理员才能删除项目主题。当您删除项目主题时，同时也从项目中删除了该主题的指派关系。

```plaintext
DELETE /topics/:id
```

支持的参数:

| 参数 | 类型    | 是否必需 | 描述                      |
| ------------- | ------- | ---------------------- | ------------------- |
| `id`          | integer | **{check-circle}** Yes | 项目主题 ID |

请求示例:

```shell
curl --request DELETE \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/topics/1"
```

## 合并主题

> 引入于极狐GitLab 15.4。

您必须是管理员才能将源主题合并到目标主题中。
合并主题时，您删除源主题并将所有分配的项目移动到目标主题。

```plaintext
POST /topics/merge
```

支持的参数：

| 参数                | 类型      | 是否必需                   | 描述                                |
|-------------------|---------|------------------------|-----------------------------------|
| `source_topic_id` | integer | **{check-circle}** Yes | 源项目主题 ID                          |
| `target_topic_id` | integer | **{check-circle}** Yes | 目标项目主题 ID  |

请求示例：

```shell
curl --request POST \
     --data "source_topic_id=2&target_topic_id=1" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/topics/merge"
```

响应示例：

```json
{
  "id": 1,
  "name": "topic1",
  "title": "Topic 1",
  "description": null,
  "total_projects_count": 0,
  "avatar_url": null
}
```

