---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

<!--
# Commits API **(FREE)**

This API operates on [repository commits](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository). Read more about [GitLab-specific information](../user/project/repository/index.md#commit-changes-to-a-repository) for commits.
-->

# 提交 API **(FREE)**

本 API 对[仓库提交](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)进行操作。您可以阅读[极狐GitLab 特有信息](../user/project/repository/index.md#commit-changes-to-a-repository)了解更多资讯。

## 响应

此 API 响应中的某些日期字段是或者看起来是重复的：

- `created_at` 字段的存在仅仅是为了与其他极狐GitLab API 保持一致。
  它始终与 `committed_date` 字段相同。
- `committed_date` 和 `authored_date` 字段生成于不同来源，可能不相同。

## 列出仓库提交

> 通过作者搜索提交引入于极狐GitLab 15.10。

列出项目仓库中的提交信息。

```plaintext
GET /projects/:id/repository/commits
```

| 参数 | 类型             | 是否必需 | 描述                                                                                                                                  |
| --------- |----------------|------|-------------------------------------------------------------------------------------------------------------------------------------|
| `id`      | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)                                                         |
| `ref_name` | string         | no   | 仓库分支、标签或提交范围，如果不给定则默认为默认分支                                                                                                          |
| `since` | string         | no   | 对于给定的时间戳，返回不早于该时间创建的提交。时间戳应符合 ISO 8601 格式 `YYYY-MM-DDTHH:MM:SSZ`                                                                    |
| `until` | string         | no   | 对于给定的时间戳，返回不晚于该时间创建的提交。时间戳应符合 ISO 8601 格式 `YYYY-MM-DDTHH:MM:SSZ`                                                                    |
| `path` | string         | no   | 文件路径                                                                                                                                |
| `author` | string         | no   | 通过提交作者搜索提交                                                                                                                          |
| `all` | boolean        | no   | 获取仓库中的所有提交                                                                                                                          |
| `with_stats` | boolean        | no   | 在响应中包含每个提交的统计信息                                                                                                                     |
| `first_parent` | boolean        | no   | 对于合并提交仅关注第一个父提交                                                                                                                     |
| `order` | string         | no   | 按顺序列出提交。 可接受的值：`default`、[`topo`](https://git-scm.com/docs/git-log#Documentation/git-log.txt---topo-order)。默认值是 `default`，提交按时间倒序显示 |
| `trailers` | boolean        | no   | 解析并将为每个提交提供 [Git trailers](https://git-scm.com/docs/git-interpret-trailers) 信息                                                      |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/commits"
```

响应示例：

```json
[
  {
    "id": "ed899a2f4b50b4370feeea94676502b42383c746",
    "short_id": "ed899a2f4b5",
    "title": "Replace sanitize with escape once",
    "author_name": "Example User",
    "author_email": "user@example.com",
    "authored_date": "2021-09-20T11:50:22.001+00:00",
    "committer_name": "Administrator",
    "committer_email": "admin@example.com",
    "committed_date": "2021-09-20T11:50:22.001+00:00",
    "created_at": "2021-09-20T11:50:22.001+00:00",
    "message": "Replace sanitize with escape once",
    "parent_ids": [
      "6104942438c14ec7bd21c6cd5bd995272b3faff6"
    ],
    "web_url": "https://gitlab.example.com/janedoe/gitlab-foss/-/commit/ed899a2f4b50b4370feeea94676502b42383c746"
  },
  {
    "id": "6104942438c14ec7bd21c6cd5bd995272b3faff6",
    "short_id": "6104942438c",
    "title": "Sanitize for network graph",
    "author_name": "randx",
    "author_email": "user@example.com",
    "committer_name": "ExampleName",
    "committer_email": "user@example.com",
    "created_at": "2021-09-20T09:06:12.201+00:00",
    "message": "Sanitize for network graph",
    "parent_ids": [
      "ae1d9fb46aa2b07ee9836d49862ec4e2c46fbbba"
    ],
    "web_url": "https://gitlab.example.com/janedoe/gitlab-foss/-/commit/ed899a2f4b50b4370feeea94676502b42383c746"
  }
]
```


<a id="create-a-commit-with-multiple-files-and-actions"></a>

## 创建包含多个文件和操作的提交

```plaintext
POST /projects/:id/repository/commits
```

| 参数 | 类型             | 是否必需 | 描述 |
| --------- |----------------|------| ----------- |
| `id` | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `branch` | string         | yes  | 需要提交到的分支。要创建一个新的分支，您需要提供 `start_branch` 和 `start_sha` 二者之一，还可额外提供 `start_project` |
| `commit_message` | string         | yes  | 提交信息 |
| `start_branch` | string         | no   | 新创建的分支派生自的分支名称 |
| `start_sha` | string         | no   |  从中开始新分支的提交的 SHA     |
| `start_project` | integer/string | no   | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding)，此项目用被用于建立你的新分支。默认值为 `id` 的值 |
| `actions[]` | array          | yes  | 要做批处理操作的提交哈希数组。阅读下文了解更多信息 |
| `author_email` | string         | no   | 提交作者的邮箱 |
| `author_name` | string         | no   | 提交作者的名称 |
| `stats` | boolean        | no   | 包含提交状态。默认为 `true` |
| `force` | boolean        | no   | 若为 `true` 则会将目标分支使用基于 `start_branch` 或 `start_sha` 的新分支覆盖 |

| `actions[]` 参数 | 类型 | 是否必需 | 描述 |
| --------------------- | ---- | -------- | ----------- |
| `action` | string | yes | 需要进行的操作：`create`、`delete`、`move`、`update`、`chmod`|
| `file_path` | string | yes | 文件的完整路径。例如 `lib/class.rb` |
| `previous_path` | string | no | 被移动文件的原始完整路径。例如 `lib/class1.rb`。仅适用于 `move` 操作 |
| `content` | string | no | 文件内容，除了 `delete`、`chmod` 和 `move` 外其他操作必填。未指定 `content` 进行移动操作会保留现有文件内容，而为 `content` 填写任何其他值都会覆盖文件内容 |
| `encoding` | string | no | `text` 或者 `base64`。默认值是 `text` |
| `last_commit_id` | string | no | 最后一个已知的文件提交 ID。仅适用于`update`、`move` 和 `delete` 操作 |
| `execute_filemode` | boolean | no | 若为 `true/false` 则启用/禁用文件的可执行标志。仅适用于 `chmod` 操作 |

```shell
PAYLOAD=$(cat << 'JSON'
{
  "branch": "master",
  "commit_message": "some commit message",
  "actions": [
    {
      "action": "create",
      "file_path": "foo/bar",
      "content": "some content"
    },
    {
      "action": "delete",
      "file_path": "foo/bar2"
    },
    {
      "action": "move",
      "file_path": "foo/bar3",
      "previous_path": "foo/bar4",
      "content": "some content"
    },
    {
      "action": "update",
      "file_path": "foo/bar5",
      "content": "new content"
    },
    {
      "action": "chmod",
      "file_path": "foo/bar5",
      "execute_filemode": true
    }
  ]
}
JSON
)
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --header "Content-Type: application/json" \
     --data "$PAYLOAD" "https://gitlab.example.com/api/v4/projects/1/repository/commits"
```

响应示例：

```json
{
  "id": "ed899a2f4b50b4370feeea94676502b42383c746",
  "short_id": "ed899a2f4b5",
  "title": "some commit message",
  "author_name": "Example User",
  "author_email": "user@example.com",
  "committer_name": "Example User",
  "committer_email": "user@example.com",
  "created_at": "2016-09-20T09:26:24.000-07:00",
  "message": "some commit message",
  "parent_ids": [
    "ae1d9fb46aa2b07ee9836d49862ec4e2c46fbbba"
  ],
  "committed_date": "2016-09-20T09:26:24.000-07:00",
  "authored_date": "2016-09-20T09:26:24.000-07:00",
  "stats": {
    "additions": 2,
    "deletions": 2,
    "total": 4
  },
  "status": null,
  "web_url": "https://gitlab.example.com/janedoe/gitlab-foss/-/commit/ed899a2f4b50b4370feeea94676502b42383c746"
}
```

极狐GitLab 支持[表单编码](rest/index.md#encoding-api-parameters-of-array-and-hash-types)。下面就是一个使用表单编码的例子：

```shell
curl --request POST \
     --form "branch=master" \
     --form "commit_message=some commit message" \
     --form "start_branch=master" \
     --form "actions[][action]=create" \
     --form "actions[][file_path]=foo/bar" \
     --form "actions[][content]=</path/to/local.file" \
     --form "actions[][action]=delete" \
     --form "actions[][file_path]=foo/bar2" \
     --form "actions[][action]=move" \
     --form "actions[][file_path]=foo/bar3" \
     --form "actions[][previous_path]=foo/bar4" \
     --form "actions[][content]=</path/to/local1.file" \
     --form "actions[][action]=update" \
     --form "actions[][file_path]=foo/bar5" \
     --form "actions[][content]=</path/to/local2.file" \
     --form "actions[][action]=chmod" \
     --form "actions[][file_path]=foo/bar5" \
     --form "actions[][execute_filemode]=true" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/repository/commits"
```

<!--
## Get a single commit

Get a specific commit identified by the commit hash or name of a branch or tag.
-->

<a id="get-a-single-commit"></a>

## 获取一个提交

根据提交的哈希、分支或标签的名称获取一个提交。

```plaintext
GET /projects/:id/repository/commits/:sha
```

<!--
Parameters:
-->

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `sha` | string | yes | 提交的哈希、分支的名称或标签的名称 |
| `stats` | boolean | no | 包含提交状态。默认为 `true` |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/commits/master"
```

响应示例：

```json
{
  "id": "6104942438c14ec7bd21c6cd5bd995272b3faff6",
  "short_id": "6104942438c",
  "title": "Sanitize for network graph",
  "author_name": "randx",
  "author_email": "user@example.com",
  "committer_name": "Dmitriy",
  "committer_email": "user@example.com",
  "created_at": "2021-09-20T09:06:12.300+03:00",
  "message": "Sanitize for network graph",
  "committed_date": "2021-09-20T09:06:12.300+03:00",
  "authored_date": "2021-09-20T09:06:12.420+03:00",
  "parent_ids": [
    "ae1d9fb46aa2b07ee9836d49862ec4e2c46fbbba"
  ],
  "last_pipeline" : {
    "id": 8,
    "ref": "master",
    "sha": "2dc6aa325a317eda67812f05600bdf0fcdc70ab0",
    "status": "created"
  },
  "stats": {
    "additions": 15,
    "deletions": 10,
    "total": 25
  },
  "status": "running",
  "web_url": "https://gitlab.example.com/janedoe/gitlab-foss/-/commit/6104942438c14ec7bd21c6cd5bd995272b3faff6"
}
```

<!--
## Get references a commit is pushed to

Get all references (from branches or tags) a commit is pushed to.
The pagination parameters `page` and `per_page` can be used to restrict the list of references.
-->

<a id="get-references-a-commit-is-pushed-to"></a>

## 获取提交被推送到的引用

获取提交被推送到的所有引用（分支或标签）。
分页参数 `page` 和 `per_page` 可对引用列表进行限定。

```plaintext
GET /projects/:id/repository/commits/:sha/refs
```

<!--
Parameters:
-->

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `sha` | string | yes | 提交的哈希 |
| `type` | string | no | 类型。可接受的值：`branch`、`tag`、`all`。默认为 `all` |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/commits/5937ac0a7beb003549fc5fd26fc247adbce4a52e/refs?type=all"
```

响应示例：

```json
[
  {"type": "branch", "name": "'test'"},
  {"type": "branch", "name": "add-balsamiq-file"},
  {"type": "branch", "name": "wip"},
  {"type": "tag", "name": "v1.1.0"}
 ]

```

<!--
## Cherry-pick a commit

Cherry-picks a commit to a given branch.
-->

<a id="cherry-pick-a-commit"></a>

## 遴选提交

将一个提交遴选到给定的分支。

```plaintext
POST /projects/:id/repository/commits/:sha/cherry_pick
```

<!--
Parameters:
-->

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `sha` | string | yes | 提交的哈希 |
| `branch` | string | yes | 分支名称 |
| `dry_run` | boolean | no | 不进行实际的提交操作。默认为 `false`。引入于 13.3 版本 |
| `message` | string | no | 自定义提交信息。引入于 14.0 版本 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --form "branch=master" "https://gitlab.example.com/api/v4/projects/5/repository/commits/master/cherry_pick"
```

响应示例：

```json
{
  "id": "8b090c1b79a14f2bd9e8a738f717824ff53aebad",
  "short_id": "8b090c1b",
  "author_name": "Example User",
  "author_email": "user@example.com",
  "authored_date": "2016-12-12T20:10:39.000+01:00",
  "created_at": "2016-12-12T20:10:39.000+01:00",
  "committer_name": "Administrator",
  "committer_email": "admin@example.com",
  "committed_date": "2016-12-12T20:10:39.000+01:00",
  "title": "Feature added",
  "message": "Feature added\n\nSigned-off-by: Example User <user@example.com>\n",
  "parent_ids": [
    "a738f717824ff53aebad8b090c1b79a14f2bd9e8"
  ],
  "web_url": "https://gitlab.example.com/janedoe/gitlab-foss/-/commit/8b090c1b79a14f2bd9e8a738f717824ff53aebad"
}
```

<!--
In the event of a failed cherry-pick, the response provides context about
why:
-->

在遴选操作失败的时候，响应的信息会包含失败的原因：

```json
{
  "message": "Sorry, we cannot cherry-pick this commit automatically. This commit may already have been cherry-picked, or a more recent commit may have updated some of its content.",
  "error_code": "empty"
}
```

<!--
In this case, the cherry-pick failed because the changeset was empty and likely
indicates that the commit already exists in the target branch. The other
possible error code is `conflict`, which indicates that there was a merge
conflict.

When `dry_run` is enabled, the server attempts to apply the cherry-pick _but
not actually commit any resulting changes_. If the cherry-pick applies cleanly,
the API responds with `200 OK`:
-->

在这种情况下，遴选失败是因为变更集是空的，这可能表明目标分支中已经存在该提交。
另一个可能的错误代码是 `conflict`，表示存在合并冲突。

当启用 `dry_run` 时，服务器会尝试遴选，但不会提交任何由此产生的更改。
如果遴选可以完成，API 会响应 `200 OK`：

```json
{
  "dry_run": "success"
}
```

<!--
In the event of a failure, an error displays that is identical to a failure without
dry run.
-->

如果遴选失败，响应与未启用 `dry_run` 时的响应相同。

<!--
## Revert a commit

Reverts a commit in a given branch.
-->

<a id="revert-a-commit"></a>

## 回滚提交

在给定的分支上回滚一个指定的提交。

```plaintext
POST /projects/:id/repository/commits/:sha/revert
```

<!--
Parameters:
-->

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ----           | -------- | -----------                                                                     |
| `id`      | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `sha`     | string         | yes      | 需要回滚的提交哈希 |
| `branch`  | string         | yes      | 目标分支 |
| `dry_run` | boolean        | no       | 不实际进行提交操作。默认值是 `false`。引入于 13.3 版本 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --form "branch=master" \
     "https://gitlab.example.com/api/v4/projects/5/repository/commits/a738f717824ff53aebad8b090c1b79a14f2bd9e8/revert"
```

响应示例：

```json
{
  "id":"8b090c1b79a14f2bd9e8a738f717824ff53aebad",
  "short_id": "8b090c1b",
  "title":"Revert \"Feature added\"",
  "created_at":"2018-11-08T15:55:26.000Z",
  "parent_ids":["a738f717824ff53aebad8b090c1b79a14f2bd9e8"],
  "message":"Revert \"Feature added\"\n\nThis reverts commit a738f717824ff53aebad8b090c1b79a14f2bd9e8",
  "author_name":"Administrator",
  "author_email":"admin@example.com",
  "authored_date":"2018-11-08T15:55:26.000Z",
  "committer_name":"Administrator",
  "committer_email":"admin@example.com",
  "committed_date":"2018-11-08T15:55:26.000Z",
  "web_url": "https://gitlab.example.com/janedoe/gitlab-foss/-/commit/8b090c1b79a14f2bd9e8a738f717824ff53aebad"
}
```

<!--
In the event of a failed revert, the response provides context about why:
-->

在回滚操作失败的时候，响应的信息会包含失败的原因：

```json
{
  "message": "Sorry, we cannot revert this commit automatically. This commit may already have been reverted, or a more recent commit may have updated some of its content.",
  "error_code": "conflict"
}
```

<!--
In this case, the revert failed because the attempted revert generated a merge
conflict. The other possible error code is `empty`, which indicates that the
changeset was empty, likely due to the change having already been reverted.

When `dry_run` is enabled, the server attempts to apply the revert _but not
actually commit any resulting changes_. If the revert applies cleanly, the API
responds with `200 OK`:
-->

在这种情况下，回滚失败是因为变更导致了合并冲突。
另一个可能的错误代码是 `empty`，表示变更为空，这可能表明这个提交已经被回滚了。

当启用 `dry_run` 时，服务器会尝试回滚，但不会提交任何由此产生的更改。
如果回滚可以完成，API 会响应 `200 OK`：

```json
{
  "dry_run": "success"
}
```

<!--
In the event of a failure, an error displays that is identical to a failure without
dry run.
-->

如果回滚失败，响应与未启用 `dry_run` 时的响应相同。

<!--
## Get the diff of a commit

Get the diff of a commit in a project.
-->

<a id="get-the-diff-of-a-commit"></a>

## 获取提交变更内容

获取项目中某一个提交的变更内容。

```plaintext
GET /projects/:id/repository/commits/:sha/diff
```

<!--
Parameters:
-->

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `sha` | string | yes | 提交的哈希、分支的名称或标签的名称 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/commits/master/diff"
```

响应示例：

```json
[
  {
    "diff": "--- a/doc/update/5.4-to-6.0.md\n+++ b/doc/update/5.4-to-6.0.md\n@@ -71,6 +71,8 @@\n sudo -u git -H bundle exec rake migrate_keys RAILS_ENV=production\n sudo -u git -H bundle exec rake migrate_inline_notes RAILS_ENV=production\n \n+sudo -u git -H bundle exec rake gitlab:assets:compile RAILS_ENV=production\n+\n ```\n \n ### 6. Update config files",
    "new_path": "doc/update/5.4-to-6.0.md",
    "old_path": "doc/update/5.4-to-6.0.md",
    "a_mode": null,
    "b_mode": "100644",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  }
]
```

<!--
## Get the comments of a commit

Get the comments of a commit in a project.
-->

<a id="get-the-comments-of-a-commit"></a>

## 获取提交评论

获取项目中某一个提交的评论。

```plaintext
GET /projects/:id/repository/commits/:sha/comments
```

<!--
Parameters:
-->

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `sha` | string | yes | 提交的哈希、分支的名称或标签的名称 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/commits/master/comments"
```

响应示例：

```json
[
  {
    "note": "this code is really nice",
    "author": {
      "id": 11,
      "username": "admin",
      "email": "admin@local.host",
      "name": "Administrator",
      "state": "active",
      "created_at": "2014-03-06T08:17:35.000Z"
    }
  }
]
```

<!--
## Post comment to commit

Adds a comment to a commit.

To post a comment in a particular line of a particular file, you must specify
the full commit SHA, the `path`, the `line`, and `line_type` should be `new`.
-->

<a id="post-comment-to-commit"></a>

## 向提交添加评论

向提交添加一则评论。

要在特定文件的特定行中发表评论，您必须指定
完整的提交哈系、`path`、`line`，且 `line_type` 必须是 `new`。

<!--
The comment is added at the end of the last commit if at least one of the
cases below is valid:

- the `sha` is instead a branch or a tag and the `line` or `path` are invalid
- the `line` number is invalid (does not exist)
- the `path` is invalid (does not exist)

In any of the above cases, the response of `line`, `line_type` and `path` is
set to `null`.
-->

当下面的条件满足其一，则在最后一次提交的末尾添加注释：

- `sha` 是一个分支或标签且 `line` 或 `path` 无效
- `line` 号无效（即行号不存在）
- `path` 无效（即文件不存在）

在上述任何一种情况下，`line`、`line_type` 和 `path` 的响应都是 `null`。

有关评论合并请求的其他方法，请参阅备注 API 中的[创建新的合并请求备注](notes.md#create-new-merge-request-note)并在讨论 API 中[在合并请求差异中创建新的主题](discussions.md#create-a-new-thread-in-the-merge-request-diff)。

```plaintext
POST /projects/:id/repository/commits/:sha/comments
```

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `sha`       | string  | yes | 提交的哈希、分支的名称或标签的名称 |
| `note`      | string  | yes | 评论的内容 |
| `path`      | string  | no  | 文件相对仓库的路径 |
| `line`      | integer | no  | 评论应当放置的行号 |
| `line_type` | string  | no  | 代码行的类型。可接受 `new` 或 `old` 为值 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --form "note=Nice picture\!" --form "path=README.md" --form "line=11" --form "line_type=new" \
     "https://gitlab.example.com/api/v4/projects/17/repository/commits/18f3e63d05582537db6d183d9d557be09e1f90c8/comments"
```

响应示例：

```json
{
  "author" : {
    "web_url" : "https://gitlab.example.com/janedoe",
    "avatar_url" : "https://gitlab.example.com/uploads/user/avatar/28/jane-doe-400-400.png",
    "username" : "janedoe",
    "state" : "active",
    "name" : "Jane Doe",
    "id" : 28
  },
  "created_at" : "2016-01-19T09:44:55.600Z",
  "line_type" : "new",
  "path" : "README.md",
  "line" : 11,
  "note" : "Nice picture!"
}
```

<!--
## Get the discussions of a commit

Get the discussions of a commit in a project.
-->

<a id="get-the-discussions-of-a-commit"></a>

## 获取提交评论

获取项目中某一个提交的评论内容。

```plaintext
GET /projects/:id/repository/commits/:sha/discussions
```

<!--
Parameters:
-->

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `sha`     | string | yes | 提交的哈希、分支的名称或标签的名称 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/commits/4604744a1c64de00ff62e1e8a6766919923d2b41/discussions"
```

响应示例：

```json
[
  {
    "id": "4604744a1c64de00ff62e1e8a6766919923d2b41",
    "individual_note": true,
    "notes": [
      {
        "id": 334686748,
        "type": null,
        "body": "Nice piece of code!",
        "attachment": null,
        "author" : {
          "id" : 28,
          "name" : "Jane Doe",
          "username" : "janedoe",
          "web_url" : "https://gitlab.example.com/janedoe",
          "state" : "active",
          "avatar_url" : "https://gitlab.example.com/uploads/user/avatar/28/jane-doe-400-400.png"
        },
        "created_at": "2020-04-30T18:48:11.432Z",
        "updated_at": "2020-04-30T18:48:11.432Z",
        "system": false,
        "noteable_id": null,
        "noteable_type": "Commit",
        "resolvable": false,
        "confidential": null,
        "noteable_iid": null,
        "commands_changes": {}
      }
    ]
  }
]

```

<!--
## Commit status

This is the commit status API for use with GitLab.
-->

<a id="commit-status"></a>

## 提交状态

极狐GitLab 的提交状态 API。

<!--
### List the statuses of a commit

List the statuses of a commit in a project.
The pagination parameters `page` and `per_page` can be used to restrict the list of references.
-->

<a id="list-the-statuses-of-a-commit"></a>

### 列出提交的状态

列出项目提交的状态信息。
分页参数 `page` 和 `per_page` 可对状态信息进行限定。

```plaintext
GET /projects/:id/repository/commits/:sha/statuses
```

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `sha`     | string  | yes | 提交的哈希 |
| `ref`     | string  | no  | 仓库分支或标签名称，如果不给定则默认为默认分支 |
| `stage`   | string  | no  | 按照[构建阶段](../ci/yaml/index.md#stages)筛选，例如 `test` |
| `name`    | string  | no  | 按照[作业名称](../ci/yaml/index.md#job-keywords)筛选，例如 `bundler:audit` |
| `all`     | boolean | no  | 返回所有状态，而不仅仅是最新的状态 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/17/repository/commits/18f3e63d05582537db6d183d9d557be09e1f90c8/statuses"
```

响应示例：

```json
[
  ...

  {
    "status" : "pending",
    "created_at" : "2016-01-19T08:40:25.934Z",
    "started_at" : null,
    "name" : "bundler:audit",
    "allow_failure" : true,
    "author" : {
      "username" : "janedoe",
      "state" : "active",
      "web_url" : "https://gitlab.example.com/janedoe",
      "avatar_url" : "https://gitlab.example.com/uploads/user/avatar/28/jane-doe-400-400.png",
      "id" : 28,
      "name" : "Jane Doe"
    },
    "description" : null,
    "sha" : "18f3e63d05582537db6d183d9d557be09e1f90c8",
    "target_url" : "https://gitlab.example.com/janedoe/gitlab-foss/builds/91",
    "finished_at" : null,
    "id" : 91,
    "ref" : "master"
  },
  {
    "started_at" : null,
    "name" : "test",
    "allow_failure" : false,
    "status" : "pending",
    "created_at" : "2016-01-19T08:40:25.832Z",
    "target_url" : "https://gitlab.example.com/janedoe/gitlab-foss/builds/90",
    "id" : 90,
    "finished_at" : null,
    "ref" : "master",
    "sha" : "18f3e63d05582537db6d183d9d557be09e1f90c8",
    "author" : {
      "id" : 28,
      "name" : "Jane Doe",
      "username" : "janedoe",
      "web_url" : "https://gitlab.example.com/janedoe",
      "state" : "active",
      "avatar_url" : "https://gitlab.example.com/uploads/user/avatar/28/jane-doe-400-400.png"
    },
    "description" : null
  },

  ...
]
```

<a id="post-the-build-status-to-a-commit"></a>

### 设置提交的流水线状态

添加或更新提交的流水线状态。如果提交与合并请求相关联，API 调用必须以合并请求的源分支中的提交为目标。

```plaintext
POST /projects/:id/statuses/:sha
```

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或 [经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `sha`     | string  | yes   | 提交的哈希 |
| `state`   | string  | yes   | 状态值。可接受的值包含 `pending`、`running`、`success`、`failed` 和 `canceled`
| `ref`     | string  | no    | 状态指向的 `ref`（分支或标签） |
| `name` or `context` | string  | no | 将此状态与其他系统的状态区分开来的标签。默认值是 `default` |
| `target_url` |  string  | no  | 与此状态关联的目标 URL |
| `description` | string  | no  | 状态的简短描述 |
| `coverage` | float  | no    | 总代码覆盖率 |
| `pipeline_id` |  integer  | no  | 要设置状态的流水线 ID。在同一 SHA 上有多个流水线的情况下使用 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/17/statuses/18f3e63d05582537db6d183d9d557be09e1f90c8?state=success"
```

响应示例：

```json
{
  "author" : {
    "web_url" : "https://gitlab.example.com/janedoe",
    "name" : "Jane Doe",
    "avatar_url" : "https://gitlab.example.com/uploads/user/avatar/28/jane-doe-400-400.png",
    "username" : "janedoe",
    "state" : "active",
    "id" : 28
  },
  "name" : "default",
  "sha" : "18f3e63d05582537db6d183d9d557be09e1f90c8",
  "status" : "success",
  "coverage": 100.0,
  "description" : null,
  "id" : 93,
  "target_url" : null,
  "ref" : null,
  "started_at" : null,
  "created_at" : "2016-01-19T09:05:50.355Z",
  "allow_failure" : false,
  "finished_at" : "2016-01-19T09:05:50.365Z"
}
```

<!--
## List merge requests associated with a commit

Get a list of merge requests related to the specified commit.
-->

<a id="list-merge-requests-associated-with-a-commit"></a>

## 列出与提交关联的合并请求

返回有关最初引入特定提交的合并请求的信息。

```plaintext
GET /projects/:id/repository/commits/:sha/merge_requests
```

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `sha`     | string  | yes   | 提交的哈希 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/commits/af5b13261899fb2c0db30abdd0af8b07cb44fdc5/merge_requests"
```

响应示例：

```json
[
  {
    "id":45,
    "iid":1,
    "project_id":35,
    "title":"Add new file",
    "description":"",
    "state":"opened",
    "created_at":"2018-03-26T17:26:30.916Z",
    "updated_at":"2018-03-26T17:26:30.916Z",
    "target_branch":"master",
    "source_branch":"test-branch",
    "upvotes":0,
    "downvotes":0,
    "author" : {
      "web_url" : "https://gitlab.example.com/janedoe",
      "name" : "Jane Doe",
      "avatar_url" : "https://gitlab.example.com/uploads/user/avatar/28/jane-doe-400-400.png",
      "username" : "janedoe",
      "state" : "active",
      "id" : 28
    },
    "assignee":null,
    "source_project_id":35,
    "target_project_id":35,
    "labels":[ ],
    "draft":false,
    "work_in_progress":false,
    "milestone":null,
    "merge_when_pipeline_succeeds":false,
    "merge_status":"can_be_merged",
    "sha":"af5b13261899fb2c0db30abdd0af8b07cb44fdc5",
    "merge_commit_sha":null,
    "squash_commit_sha":null,
    "user_notes_count":0,
    "discussion_locked":null,
    "should_remove_source_branch":null,
    "force_remove_source_branch":false,
    "web_url":"https://gitlab.example.com/root/test-project/merge_requests/1",
    "time_stats":{
      "time_estimate":0,
      "total_time_spent":0,
      "human_time_estimate":null,
      "human_total_time_spent":null
    }
  }
]
```

<!--
## Get GPG signature of a commit

Get the [GPG signature from a commit](../user/project/repository/gpg_signed_commits/index.md),
if it is signed. For unsigned commits, it results in a 404 response.
-->

<a id="get-gpg-signature-of-a-commit"></a>

对于被签名的提交，您可以获取[其 GPG 签名](../user/project/repository/gpg_signed_commits/index.md)。
对于未被签名的提交，将返回 404 响应。

```plaintext
GET /projects/:id/repository/commits/:sha/signature
```

<!--
Parameters:
-->

参数：

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | yes | 经过身份验证的用户拥有的项目 ID 或[经过 URL 编码处理的路径](rest/index.md#namespaced-path-encoding) |
| `sha` | string | yes | 提交的哈希、分支的名称或标签的名称 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/repository/commits/da738facbc19eb2fc2cef57c49be0e6038570352/signature"
```

<!--
Example response if commit is GPG signed:
-->

对于被 GPG 签名的提交，示例响应如下：

```json
{
  "signature_type": "PGP",
  "verification_status": "verified",
  "gpg_key_id": 1,
  "gpg_key_primary_keyid": "8254AAB3FBD54AC9",
  "gpg_key_user_name": "John Doe",
  "gpg_key_user_email": "johndoe@example.com",
  "gpg_key_subkey_id": null,
  "commit_source": "gitaly"
}
```

<!--
Example response if commit is X.509 signed:
-->

对于被 X.509 签名的提交，示例响应如下：

```json
{
  "signature_type": "X509",
  "verification_status": "unverified",
  "x509_certificate": {
    "id": 1,
    "subject": "CN=gitlab@example.org,OU=Example,O=World",
    "subject_key_identifier": "BC:BC:BC:BC:BC:BC:BC:BC:BC:BC:BC:BC:BC:BC:BC:BC:BC:BC:BC:BC",
    "email": "gitlab@example.org",
    "serial_number": 278969561018901340486471282831158785578,
    "certificate_status": "good",
    "x509_issuer": {
      "id": 1,
      "subject": "CN=PKI,OU=Example,O=World",
      "subject_key_identifier": "AB:AB:AB:AB:AB:AB:AB:AB:AB:AB:AB:AB:AB:AB:AB:AB:AB:AB:AB:AB",
      "crl_url": "http://example.com/pki.crl"
    }
  },
  "commit_source": "gitaly"
}
```

<!--
Example response if commit is unsigned:
-->

对于未被签名的提交，示例响应如下：

```json
{
  "message": "404 GPG Signature Not Found"
}
```
