---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: concepts, howto
---

# 环境 API **(FREE)**

## 列举环境

获得指定项目的所有环境。

```plaintext
GET /projects/:id/environments
```

| 参数   | 类型     | 是否必需 | 描述                                                                         |
| --------- | ------- | -------- |----------------------------------------------------------------------------|
| `id`      | integer/string | yes      | 项目的 ID 或[ URL 编码的](rest/index.md#namespaced-path-encoding)路径                |
| `name`    | string  | no       | 使用该名称返回环境信息。和 `search` 参数互斥                                                |
| `search`  | string  | no       | 返回一组能匹配搜索条件的环境。和 `name` 参数互斥。长度必须为 3 个字符                                   |
| `states`  | string  | no       | 列出所有匹配特定状态的环境。可接受的状态值为：`available`， `stopping` 以及 `stopped`。如果没有给出状态值，返回所有环境 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/environments?name=review%2Ffix-foo"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "review/fix-foo",
    "slug": "review-fix-foo-dfjre3",
    "external_url": "https://review-fix-foo-dfjre3.gitlab.example.com",
    "state": "available",
    "tier": "development",
    "created_at": "2019-05-25T18:55:13.252Z",
    "updated_at": "2019-05-27T18:55:13.252Z",
    "enable_advanced_logs_querying": false,
    "logs_api_path": "/project/-/logs/k8s.json?environment_name=review%2Ffix-foo"
  }
]
```

## 获得特定环境

```plaintext
GET /projects/:id/environments/:environment_id
```

| 参数   | 类型     | 是否必需 | 描述                                                      |
|-----------|---------|----------|---------------------------------------------------------|
| `id`      | integer/string | yes      | 项目的 ID 或 [URL 编码的](rest/index.md#namespaced-path-encoding)路径 |
| `environment_id` | integer | yes | 环境的 ID                                                  |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/environments/1"
```

响应示例：

```json
{
  "id": 1,
  "name": "review/fix-foo",
  "slug": "review-fix-foo-dfjre3",
  "external_url": "https://review-fix-foo-dfjre3.gitlab.example.com",
  "state": "available",
  "tier": "development",
  "created_at": "2019-05-25T18:55:13.252Z",
  "updated_at": "2019-05-27T18:55:13.252Z",
  "enable_advanced_logs_querying": false,
  "logs_api_path": "/project/-/logs/k8s.json?environment_name=review%2Ffix-foo",
  "last_deployment": {
    "id": 100,
    "iid": 34,
    "ref": "fdroid",
    "sha": "416d8ea11849050d3d1f5104cf8cf51053e790ab",
    "created_at": "2019-03-25T18:55:13.252Z",
    "status": "success",
    "user": {
      "id": 1,
      "name": "Administrator",
      "state": "active",
      "username": "root",
      "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://localhost:3000/root"
    },
    "deployable": {
      "id": 710,
      "status": "success",
      "stage": "deploy",
      "name": "staging",
      "ref": "fdroid",
      "tag": false,
      "coverage": null,
      "created_at": "2019-03-25T18:55:13.215Z",
      "started_at": "2019-03-25T12:54:50.082Z",
      "finished_at": "2019-03-25T18:55:13.216Z",
      "duration": 21623.13423,
      "user": {
        "id": 1,
        "name": "Administrator",
        "username": "root",
        "state": "active",
        "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
        "web_url": "http://gitlab.dev/root",
        "created_at": "2015-12-21T13:14:24.077Z",
        "bio": null,
        "location": null,
        "public_email": "",
        "skype": "",
        "linkedin": "",
        "twitter": "",
        "website_url": "",
        "organization": null
      },
      "commit": {
        "id": "416d8ea11849050d3d1f5104cf8cf51053e790ab",
        "short_id": "416d8ea1",
        "created_at": "2016-01-02T15:39:18.000Z",
        "parent_ids": [
          "e9a4449c95c64358840902508fc827f1a2eab7df"
        ],
        "title": "Removed fabric to fix #40",
        "message": "Removed fabric to fix #40\n",
        "author_name": "Administrator",
        "author_email": "admin@example.com",
        "authored_date": "2016-01-02T15:39:18.000Z",
        "committer_name": "Administrator",
        "committer_email": "admin@example.com",
        "committed_date": "2016-01-02T15:39:18.000Z"
      },
      "pipeline": {
        "id": 34,
        "sha": "416d8ea11849050d3d1f5104cf8cf51053e790ab",
        "ref": "fdroid",
        "status": "success",
        "web_url": "http://localhost:3000/Commit451/lab-coat/pipelines/34"
      },
      "web_url": "http://localhost:3000/Commit451/lab-coat/-/jobs/710",
      "artifacts": [
        {
          "file_type": "trace",
          "size": 1305,
          "filename": "job.log",
          "file_format": null
        }
      ],
      "runner": null,
      "artifacts_expire_at": null
    }
  }
}
```

## 创建新环境

根据给定的名称和 `external_url` 创建一个新环境。

如果环境成功创建的话会返回 `201`，如果存在错误参数则返回 `400`。

```plaintext
POST /projects/:id/environments
```

| 参数   | 类型     | 是否必需 | 描述                                                                         |
| ------------- | ------- | -------- |----------------------------------------------------------------------------|
| `id`          | integer/string | yes      | 项目的 ID 或 [URL 编码的](rest/index.md#namespaced-path-encoding)路径                    |
| `name`        | string  | yes      | 环境名称                                                                       |
| `external_url` | string  | no     | 该环境的链接位置                                                                   |
| `tier`         | string         | no       | 新环境的层级。允许设置的值为 `production`， `staging`， `testing`， `development` 和 `other` |

```shell
curl --data "name=deploy&external_url=https://deploy.gitlab.example.com" \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/environments"
```

响应示例:

```json
{
  "id": 1,
  "name": "deploy",
  "slug": "deploy",
  "external_url": "https://deploy.gitlab.example.com",
  "state": "available",
  "tier": "production",
  "created_at": "2019-05-25T18:55:13.252Z",
  "updated_at": "2019-05-27T18:55:13.252Z"
}
```

## 更新现存环境

> 参数 `name` 移除于极狐GitLab 16.0。

更新现存环境的名称或 `external_url`。

如果环境成功更新返回 `200`。如果发生错误，将返回状态码 `400`。

```plaintext
PUT /projects/:id/environments/:environments_id
```

| 参数   | 类型     | 是否必需 | 描述                                                                      |
| --------------- | ------- | --------------------------------- |-------------------------------------------------------------------------|
| `id`            | integer/string | yes                               | 项目的 ID 或[ URL 编码的路径](rest/index.md#namespaced-path-encoding)            |
| `environment_id` | integer | yes | 环境 ID                                                                   |
| `external_url`  | string  | no                                | 新的 `external_url`                                                       |
| `tier`         | string         | no       | 新环境的层级。允许设置的值为 `production`、`staging`、`testing`、`development` 和 `other` |

```shell
curl --request PUT --data "external_url=https://staging.gitlab.example.com" \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/environments/1"
```

响应示例:

```json
{
  "id": 1,
  "name": "staging",
  "slug": "staging",
  "external_url": "https://staging.gitlab.example.com",
  "state": "available",
  "tier": "staging",
  "created_at": "2019-05-25T18:55:13.252Z",
  "updated_at": "2019-05-27T18:55:13.252Z"
}
```

## 删除环境

如果一个环境被成功删除 API 将返回 `204`，如果环境不存在则返回 `404`。

```plaintext
DELETE /projects/:id/environments/:environment_id
```


| 参数   | 类型     | 是否必需 | 描述                                                      |
| ----------- | -------------- | -------- |---------------------------------------------------------|
| `id` | integer/string | yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `environment_id` | integer | yes | 环境 ID                                                   |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/environments/1"
```

## 删除多个已停止的 Review App

> 引入于 14.2 版本。

该 API 为多个已经被[停止](../ci/environments/index.md#stopping-an-environment)和[在 Review App 文件夹内](../ci/review_apps/index.md)的环境计划删除。
实际删除是在它执行 1 周后进行的。
默认情况下，它只删除 30 天或更早的环境。您可以使用 `before` 参数更改此默认值。

```plaintext
DELETE /projects/:id/environments/review_apps
```

| 参数   | 类型     | 是否必需 | 描述           |
| --------- | ------- | -------- | -------------------- |
| `id`      | integer/string | yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)|
| `before`    | datetime | no | 可以删除环境的日期。默认为 30 天前。需要用 ISO 8601 格式（`YYYY-MM-DDTHH:MM:SSZ`） |
| `limit`     | integer | no | 要删除的最大环境数。默认为 100|
| `dry_run`   | boolean | no | 出于安全原因，默认为 `true`，即试运行一次，而不会执行实际删除。设为 `false` 会实际删除环境|

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/environments/review_apps"
```

响应示例:

```json
{
  "scheduled_entries": [
    {
      "id": 387,
      "name": "review/023f1bce01229c686a73",
      "slug": "review-023f1bce01-3uxznk",
      "external_url": null
    },
    {
      "id": 388,
      "name": "review/85d4c26a388348d3c4c0",
      "slug": "review-85d4c26a38-5giw1c",
      "external_url": null
    }
  ],
  "unprocessable_entries": []
}
```

## 停止环境

如果环境成功停止会返回 `200`，如果环境不存在则返回 `404`。

```plaintext
POST /projects/:id/environments/:environment_id/stop
```


| 参数   | 类型     | 是否必需 | 描述           |
|------------------|----------------|----------|----------------------------------------------------------------------------------------------------------------|
| `id`             | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `environment_id` | integer        | yes      | 环境 ID                                                                                      |
| `force`          | boolean        | no       | 强制环境停止而不执行 `on_stop` 动作                                                    |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/environments/1/stop"
```

响应示例：

```json
{
  "id": 1,
  "name": "deploy",
  "slug": "deploy",
  "external_url": "https://deploy.gitlab.example.com",
  "state": "stopped",
  "created_at": "2019-05-25T18:55:13.252Z",
  "updated_at": "2019-05-27T18:55:13.252Z"
}
```

## 停止旧环境

向指定日期之前最后修改或部署的所有环境发出停止请求。不包括受保护的环境。如果停止请求成功，则返回“ `200`；如果之前的日期无效，则返回 `400`。有关环境停止的确切时间的详细信息，请参阅[停止环境](../ci/environments/index.md#stopping-an-environment)。

```plaintext
POST /projects/:id/environments/stop_stale
```

| 参数       | 类型             | 是否必需 | 描述                                                                                                                                                                                             |
|----------|----------------|------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`     | integer/string | yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                                                                                                                |
| `before` | date           | yes  | 停止在指定日期之前修改或部署的环境。预计采用 ISO 8601 格式 (`2019-03-15T08:00:00Z`)。有效输入介于 10 年前和 1 周前之间 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/environments/stop_stale?before=10%2F10%2F2021"
```

响应示例：

```json
{
  "message": "Successfully requested stop for all stale environments"
}
```
