---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 功能标志用户列表 API **(FREE)**

> - 引入到极狐GitLab 专业版于 12.10。
> - <!--[Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/212318)-->移动到极狐GitLab 免费版于 13.5。

此 API 用于访问极狐GitLab 功能标志用户列表。

拥有至少开发者[角色](../user/permissions.md)的用户可以访问功能标志用户列表 API。

NOTE:
由于 API 结果进行了[分页](rest/index.md#pagination)，`GET` 请求一次返回 20 个结果。您可以更改这个值。

## 列出所有项目的功能标志用户列表

为请求的项目获取所有功能标志用户列表。

```plaintext
GET /projects/:id/feature_flags_user_lists
```

| 参数       | 类型             | 是否必需 | 描述                                                       |
|----------|----------------|------|----------------------------------------------------------|
| `id`     | integer/string | yes  | ID 或 [项目的 URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `search` | string         | no   | 返回匹配搜索标准的用户列表。                                           |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/feature_flags_user_lists"
```

响应示例：

```json
[
   {
      "name": "user_list",
      "user_xids": "user1,user2",
      "id": 1,
      "iid": 1,
      "project_id": 1,
      "created_at": "2020-02-04T08:13:51.423Z",
      "updated_at": "2020-02-04T08:13:51.423Z"
   },
   {
      "name": "test_users",
      "user_xids": "user3,user4,user5",
      "id": 2,
      "iid": 2,
      "project_id": 1,
      "created_at": "2020-02-04T08:13:10.507Z",
      "updated_at": "2020-02-04T08:13:10.507Z"
   }
]
```

## 创建功能标志用户列表

创建功能标志用户列表。

```plaintext
POST /projects/:id/feature_flags_user_lists
```

| 参数          | 类型             | 是否必需 | 描述                                                       |
|-------------|----------------|------|----------------------------------------------------------|
| `id`        | integer/string | yes  | ID 或 [项目的 URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `name`      | string         | yes  | 列表的名称。                                                   |
| `user_xids` | string         | yes  | 以逗号分隔的外部用户 ID 的列表。                                       |

```shell
curl "https://gitlab.example.com/api/v4/projects/1/feature_flags_user_lists" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     --header "Content-type: application/json" \
     --data @- << EOF
{
    "name": "my_user_list",
    "user_xids": "user1,user2,user3"
}
EOF
```

响应示例：

```json
{
   "name": "my_user_list",
   "user_xids": "user1,user2,user3",
   "id": 1,
   "iid": 1,
   "project_id": 1,
   "created_at": "2020-02-04T08:32:27.288Z",
   "updated_at": "2020-02-04T08:32:27.288Z"
}
```

## 获取功能标志用户列表

获取功能标志用户列表。

```plaintext
GET /projects/:id/feature_flags_user_lists/:iid
```

| 参数    | 类型             | 是否必需 | 描述                                                       |
|-------|----------------|------|----------------------------------------------------------|
| `id`  | integer/string | yes  | ID 或 [项目的 URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `iid` | integer/string | yes  | 项目的功能标志用户列表的内部 ID。                                       |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/feature_flags_user_lists/1"
```

响应示例：

```json
{
   "name": "my_user_list",
   "user_xids": "123,456",
   "id": 1,
   "iid": 1,
   "project_id": 1,
   "created_at": "2020-02-04T08:13:10.507Z",
   "updated_at": "2020-02-04T08:13:10.507Z"
}
```

## 更新功能标志用户列表

更新功能标志用户列表。

```plaintext
PUT /projects/:id/feature_flags_user_lists/:iid
```

| 参数          | 类型             | 是否必需 | 描述                                                       |
|-------------|----------------|------|----------------------------------------------------------|
| `id`        | integer/string | yes  | ID 或 [项目的 URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `iid`       | integer/string | yes  | 项目的功能标志用户列表的内部 ID。                                       |
| `name`      | string         | no   | 列表的名称。                                                   |
| `user_xids` | string         | no   | 以逗号分隔的外部用户 ID 的列表。                                       |

```shell
curl "https://gitlab.example.com/api/v4/projects/1/feature_flags_user_lists/1" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     --header "Content-type: application/json" \
     --request PUT \
     --data @- << EOF
{
    "user_xids": "user2,user3,user4"
}
EOF
```

响应示例：

```json
{
   "name": "my_user_list",
   "user_xids": "user2,user3,user4",
   "id": 1,
   "iid": 1,
   "project_id": 1,
   "created_at": "2020-02-04T08:32:27.288Z",
   "updated_at": "2020-02-05T09:33:17.179Z"
}
```

## 删除功能标志用户列表

删除功能标志用户列表。

```plaintext
DELETE /projects/:id/feature_flags_user_lists/:iid
```

| 参数    | 类型             | 是否必需 | 描述                                                      |
|-------|----------------|------|---------------------------------------------------------|
| `id`  | integer/string | yes  | ID 或[项目的 URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `iid` | integer/string | yes  | 项目的功能标志用户列表的内部 ID。                                      |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" --request DELETE "https://gitlab.example.com/api/v4/projects/1/feature_flags_user_lists/1"
```
