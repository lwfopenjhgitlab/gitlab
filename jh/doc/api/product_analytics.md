---
stage: Analyze
group: Product Analytics
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 产品分析 API **(ULTIMATE)**

> - 引入于极狐GitLab 15.4，[功能标志](../administration/feature_flags.md)为 `cube_api_proxy`。默认禁用。
> - `cube_api_proxy` 移除并替换为 `product_analytics_internal_preview` 于极狐GitLab 15.10。
> - `product_analytics_internal_preview` 替换为 `product_analytics_dashboards` 于极狐GitLab 15.11。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能不可用。要使其在每个项目或整个实例中可用，请要求管理员启用名为 `cube_api_proxy` 的[功能标志](../administration/feature_flags.md)。
在 JiHuLab.com 上，此功能不可用。
此功能尚未准备好用于生产用途。

NOTE:
请先使用 [API](settings.md) 定义 `cube_api_base_url` 和 `cube_api_key` 应用设置。

## 向 Cube 发送查询请求

生成可用于查询 Cube API 的访问令牌。例如：

```plaintext
POST /projects/:id/product_analytics/request/load
POST /projects/:id/product_analytics/request/dry-run
```

| 参数   | 类型      | 是否必需 | 描述                             |
|------|---------|------|--------------------------------|
| `id` | integer | yes  | 当前用户有读取权限的项目的 ID。              |
| `include_token` | boolean | no   | 是否在响应中包含访问令牌。（仅在 Funnel 生成时需要） |

### 请求主体

加载请求的主体必须是有效的 Cube 查询。

NOTE:
测量 `TrackedEvents` 时，您必须使用 `TrackedEvents.*` 作为 `dimensions` 和 `timeDimensions`。衡量 `Sessions` 时相同的规则也同样适用。

#### 跟踪事件示例

```json
{
  "query": {
    "measures": [
      "TrackedEvents.count"
    ],
    "timeDimensions": [
      {
        "dimension": "TrackedEvents.utcTime",
        "dateRange": "This week"
      }
    ],
    "order": [
      [
        "TrackedEvents.count",
        "desc"
      ],
      [
        "TrackedEvents.docPath",
        "desc"
      ],
      [
        "TrackedEvents.utcTime",
        "asc"
      ]
    ],
    "dimensions": [
      "TrackedEvents.docPath"
    ],
    "limit": 23
  },
  "queryType": "multi"
}
```

#### 会话示例

```json
{
  "query": {
    "measures": [
      "Sessions.count"
    ],
    "timeDimensions": [
      {
        "dimension": "Sessions.startAt",
        "granularity": "day"
      }
    ],
    "order": {
      "Sessions.startAt": "asc"
    },
    "limit": 100
  },
  "queryType": "multi"
}
```

## 向 Cube 发送元数据请求

返回 Analytics 数据的 Cube 元数据。例如：

```plaintext
GET /projects/:id/product_analytics/request/meta
```

| 参数   | 类型      | 是否必需 | 描述                                                            |
|------|---------|------|---------------------------------------------------------------|
| `id` | integer | yes  | 当前用户具有读取权限的项目的 ID。 |

## 列出项目的 Funnel

列出项目的所有 Funnel。例如：

```plaintext
GET /projects/:id/product_analytics/funnels
```

| 参数   | 类型      | 是否必需 | 描述                                                                                       |
|------|---------|------|------------------------------------------------------------------------------------------|
| `id` | integer | yes  | 当前用户具有开发者角色的项目的 ID。 |

