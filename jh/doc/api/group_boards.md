---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组议题看板 API **(FREE)**

每个对[群组议题看板](../user/project/issue_board.md#group-issue-boards)所进行的 API 调用都必须经过授权。

如果用户不是群组成员，且群组是私有的，`GET` 请求会返回 `404` 状态码。

## 列出群组中的所有群组议题看板

列出给定群组中的议题看板。

```plaintext
GET /groups/:id/boards
```

| 参数   | 类型             | 是否必需 | 描述                                                                                            |
|------|----------------|------|-----------------------------------------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/boards"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "group issue board",
    "group": {
      "id": 5,
      "name": "Documentcloud",
      "web_url": "http://example.com/groups/documentcloud"
    },
    "milestone":   {
      "id": 12,
      "title": "10.0"
    },
    "lists" : [
      {
        "id" : 1,
        "label" : {
          "name" : "Testing",
          "color" : "#F0AD4E",
          "description" : null
        },
        "position" : 1
      },
      {
        "id" : 2,
        "label" : {
          "name" : "Ready",
          "color" : "#FF0000",
          "description" : null
        },
        "position" : 2
      },
      {
        "id" : 3,
        "label" : {
          "name" : "Production",
          "color" : "#FF5F00",
          "description" : null
        },
        "position" : 3
      }
    ]
  }
]
```

[专业版及旗舰版](https://about.gitlab.cn/pricing/)的用户看见的参数不同，因为他们可以拥有多个群组看板。

响应示例：

```json
[
  {
    "id": 1,
    "name": "group issue board",
    "group": {
      "id": 5,
      "name": "Documentcloud",
      "web_url": "http://example.com/groups/documentcloud"
    },
    "milestone":   {
      "id": 12,
      "title": "10.0"
    },
    "lists" : [
      {
        "id" : 1,
        "label" : {
          "name" : "Testing",
          "color" : "#F0AD4E",
          "description" : null
        },
        "position" : 1
      },
      {
        "id" : 2,
        "label" : {
          "name" : "Ready",
          "color" : "#FF0000",
          "description" : null
        },
        "position" : 2
      },
      {
        "id" : 3,
        "label" : {
          "name" : "Production",
          "color" : "#FF5F00",
          "description" : null
        },
        "position" : 3
      }
    ]
  }
]
```

## 单个群组议题看板

获取单个群组议题看板。

```plaintext
GET /groups/:id/boards/:board_id
```

| 参数         | 类型             | 是否必需 | 描述                                                                                            |
|------------|----------------|------|-----------------------------------------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)|
| `board_id` | integer        | yes  | 议题看板 ID                                                                                        |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/boards/1"
```
响应示例：

```json
  {
    "id": 1,
    "name": "group issue board",
    "group": {
      "id": 5,
      "name": "Documentcloud",
      "web_url": "http://example.com/groups/documentcloud"
    },
    "milestone":   {
      "id": 12,
      "title": "10.0"
    },
    "lists" : [
      {
        "id" : 1,
        "label" : {
          "name" : "Testing",
          "color" : "#F0AD4E",
          "description" : null
        },
        "position" : 1
      },
      {
        "id" : 2,
        "label" : {
          "name" : "Ready",
          "color" : "#FF0000",
          "description" : null
        },
        "position" : 2
      },
      {
        "id" : 3,
        "label" : {
          "name" : "Production",
          "color" : "#FF5F00",
          "description" : null
        },
        "position" : 3
      }
    ]
  }
```

[专业版及旗舰版](https://about.gitlab.cn/pricing/)的用户看见的参数不同，因为他们可以拥有多个群组议题看板。

响应示例：

```json
  {
    "id": 1,
    "name": "group issue board",
    "group": {
      "id": 5,
      "name": "Documentcloud",
      "web_url": "http://example.com/groups/documentcloud"
    },
    "milestone":   {
      "id": 12,
      "title": "10.0"
    },
    "lists" : [
      {
        "id" : 1,
        "label" : {
          "name" : "Testing",
          "color" : "#F0AD4E",
          "description" : null
        },
        "position" : 1
      },
      {
        "id" : 2,
        "label" : {
          "name" : "Ready",
          "color" : "#FF0000",
          "description" : null
        },
        "position" : 2
      },
      {
        "id" : 3,
        "label" : {
          "name" : "Production",
          "color" : "#FF5F00",
          "description" : null
        },
        "position" : 3
      }
    ]
  }
```

## 创建群组议题看板 **(PREMIUM)**

创建群组议题看板。

```plaintext
POST /groups/:id/boards
```

| 参数     | 类型             | 是否必需 | 描述                                                            |
|--------|----------------|------|---------------------------------------------------------------|
| `id`   | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `name` | string         | yes  | 新议题看板的名称                               |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/boards?name=newboard"
```

响应示例：

```json
  {
    "id": 1,
    "name": "newboard",
    "project": null,
    "lists" : [],
    "group": {
      "id": 5,
      "name": "Documentcloud",
      "web_url": "http://example.com/groups/documentcloud"
    },
    "milestone": null,
    "assignee" : null,
    "labels" : [],
    "weight" : null
  }
```

## 更新群组议题看板

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/5954) -->引入于极狐GitLab 11.1。

更新群组议题看板。

```plaintext
PUT /groups/:id/boards/:board_id
```

| 参数                           | 类型             | 是否必需 | 描述                                                                                   |
|------------------------------|----------------|------|--------------------------------------------------------------------------------------|
| `id`                         | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                        |
| `board_id`                   | integer        | yes  | 议题看板 ID                                                                               |
| `name`                       | string         | no   | 议题看板的新名称                                                                              |
| `hide_backlog_list`          | boolean        | no   | 隐藏开放的列表                                                                              |
| `hide_closed_list`           | boolean        | no   | 隐藏关闭的列表                                                                              |
| `assignee_id` **(PREMIUM)**  | integer        | no   | 议题看板应该包含的指派人                                                                          |
| `milestone_id` **(PREMIUM)** | integer        | no   | 议题看板应该包含的里程碑                                                                          |
| `labels` **(PREMIUM)**       | string         | no   | 议题看板应该包含的用逗号分隔的标记名称列表                                                                 |
| `weight` **(PREMIUM)**       | integer        | no   | 议题看板应该包含的权重范围：0-9|

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/boards/1?name=new_name&milestone_id=44&assignee_id=1&labels=GroupLabel&weight=4"
```

响应示例：

```json
  {
    "id": 1,
    "project": null,
    "lists": [],
    "name": "new_name",
    "group": {
      "id": 5,
      "name": "Documentcloud",
      "web_url": "http://example.com/groups/documentcloud"
    },
    "milestone": {
      "id": 44,
      "iid": 1,
      "group_id": 5,
      "title": "Group Milestone",
      "description": "Group Milestone Desc",
      "state": "active",
      "created_at": "2018-07-03T07:15:19.271Z",
      "updated_at": "2018-07-03T07:15:19.271Z",
      "due_date": null,
      "start_date": null,
      "web_url": "http://example.com/groups/documentcloud/-/milestones/1"
    },
    "assignee": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://example.com/root"
    },
    "labels": [{
      "id": 11,
      "name": "GroupLabel",
      "color": "#428BCA",
      "description": ""
    }],
    "weight": 4
  }
```

## 删除群组议题看板 **(PREMIUM)**

删除群组议题看板。

```plaintext
DELETE /groups/:id/boards/:board_id
```

| 参数         | 类型             | 是否必需 | 描述                                                            |
|------------|----------------|------|---------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 议题看板 ID                                             |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/boards/1"
```

## 列出群组议题看板列表

获取议题看板列表。不包括 `open` 和 `closed` 列表。

```plaintext
GET /groups/:id/boards/:board_id/lists
```

| 参数         | 类型             | 是否必需 | 描述                                                           |
|------------|----------------|------|--------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 议题看板 ID                                             |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/boards/1/lists"
```

响应示例：

```json
[
  {
    "id" : 1,
    "label" : {
      "name" : "Testing",
      "color" : "#F0AD4E",
      "description" : null
    },
    "position" : 1
  },
  {
    "id" : 2,
    "label" : {
      "name" : "Ready",
      "color" : "#FF0000",
      "description" : null
    },
    "position" : 2
  },
  {
    "id" : 3,
    "label" : {
      "name" : "Production",
      "color" : "#FF5F00",
      "description" : null
    },
    "position" : 3
  }
]
```

## 单个群组议题看板列表

获取单个看板列表。

```plaintext
GET /groups/:id/boards/:board_id/lists/:list_id
```

| 参数         | 类型             | 是否必需 | 描述                                                            |
|------------|----------------|------|---------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 议题看板 ID                                                        |
| `list_id`  | integer        | yes  | 议题看板列表 ID                                    |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/boards/1/lists/1"
```

响应示例：

```json
{
  "id" : 1,
  "label" : {
    "name" : "Testing",
    "color" : "#F0AD4E",
    "description" : null
  },
  "position" : 1
}
```

## 新的群组议题看板列表

创建议题看板列表。

```plaintext
POST /groups/:id/boards/:board_id/lists
```

| 参数         | 类型             | 是否必需 | 描述                                                            |
|------------|----------------|------|---------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 议题看板 ID                                                        |
| `label_id` | integer        | yes  | 标记 ID                                              |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/4/boards/12/lists?milestone_id=7"
```

响应示例：

```json
{
  "id": 9,
  "label": null,
  "position": 0,
  "milestone": {
    "id": 7,
    "iid": 3,
    "group_id": 12,
    "title": "Milestone with due date",
    "description": "",
    "state": "active",
    "created_at": "2017-09-03T07:16:28.596Z",
    "updated_at": "2017-09-03T07:16:49.521Z",
    "due_date": null,
    "start_date": null,
    "web_url": "https://gitlab.example.com/groups/issue-reproduce/-/milestones/3"
  }
}
```

## 编辑群组议题看板列表

更新现存的议题看板列表。使用该调用改变列表的位置。

```plaintext
PUT /groups/:id/boards/:board_id/lists/:list_id
```

| 参数         | 类型             | 是否必需 | 描述                                                            |
|------------|----------------|------|---------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 议题看板 ID                                                       |
| `list_id`  | integer        | yes  | 议题看板列表 ID                                                     |
| `position` | integer        | yes  | 列表的位置                                                         |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/group/5/boards/1/lists/1?position=2"
```

响应示例：

```json
{
  "id" : 1,
  "label" : {
    "name" : "Testing",
    "color" : "#F0AD4E",
    "description" : null
  },
  "position" : 1
}
```

## 删除群组议题看板列表

只有管理员和群组所有者可以删除有问题的议题看板列表。

```plaintext
DELETE /groups/:id/boards/:board_id/lists/:list_id
```

| 参数         | 类型             | 是否必需 | 描述                                                            |
|------------|----------------|------|---------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `board_id` | integer        | yes  | 议题看板 ID                                                        |
| `list_id`  | integer        | yes  | 议题看板列表 ID                                  |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/boards/1/lists/1"
```
