---
stage: Verify
group: Pipeline Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组级别变量 API **(FREE)**

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/34519)-->引入于极狐GitLab 9.5。

## 列出群组变量

获取群组变量列表。

```plaintext
GET /groups/:id/variables
```

| 参数   | 类型             | 是否必需 | 描述                                                              |
|------|----------------|------|-----------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/variables"
```

```json
[
  {
    "key": "TEST_VARIABLE_1",
    "variable_type": "env_var",
    "value": "TEST_1",
    "protected": false,
    "masked": false,
    "raw": false,
    "environment_scope": "*"
  },
  {
    "key": "TEST_VARIABLE_2",
    "variable_type": "env_var",
    "value": "TEST_2",
    "protected": false,
    "masked": false,
    "raw": false,
    "environment_scope": "*"
  }
]
```

## 显示变量详细信息

获取群组特定变量的详细信息。

```plaintext
GET /groups/:id/variables/:key
```

| 参数    | 类型             | 是否必需 | 描述                                                              |
|-------|----------------|------|-----------------------------------------------------------------|
| `id`  | integer/string | yes  | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `key` | string         | yes  | 变量的 `key`                                        |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/variables/TEST_VARIABLE_1"
```

```json
{
  "key": "TEST_VARIABLE_1",
  "variable_type": "env_var",
  "value": "TEST_1",
  "protected": false,
  "masked": false,
  "raw": false,
  "environment_scope": "*"
}
```

## 创建变量

创建新变量。

```plaintext
POST /groups/:id/variables
```

| 参数                                | 类型              | 是否必需 | 描述                                                                                                           |
|-----------------------------------|-----------------|------|--------------------------------------------------------------------------------------------------------------|
| `id`                              | integer/string  | yes  | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                    |
| `key`                             | string          | yes  | 变量的 `key`；不能超过 255 个字符，只允许 `A-Z`、`a-z`、`0-9` 和 `_`                                                           |
| `value`                           | string          | yes  | 变量的 `value`                                                                                                  |
| `variable_type`                   | string          | no   | 变量的类型。可用类型为 `env_var`（默认）和 `file`                                                                            |
| `protected`                       | boolean         | no   | 变量是否受保护                                                                                                      |
| `masked`                          | boolean         | no   | 变量是否隐藏                                                                                                       |
| `raw`                             | boolean         | no   | 变量是否被视为原始字符串。默认值：`false`。当 `true` 时，值中的变量不会[展开](../ci/variables/index.md#prevent-cicd-variable-expansion)    |
| `environment_scope` **(PREMIUM)** | string          | no   | <!--[environment scope](../ci/environments/index.md#limit-the-environment-scope-of-a-cicd-variable)-->变量的环境范围 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/1/variables" --form "key=NEW_VARIABLE" --form "value=new value"
```

```json
{
  "key": "NEW_VARIABLE",
  "value": "new value",
  "variable_type": "env_var",
  "protected": false,
  "masked": false,
  "raw": false,
  "environment_scope": "*"
}
```

## 更新变量

更新群组变量。

```plaintext
PUT /groups/:id/variables/:key
```

| 参数                                | 类型             | 是否必需 | 描述                                                                                                         |
|-----------------------------------|----------------|------|------------------------------------------------------------------------------------------------------------|
| `id`                              | integer/string | yes  | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                            |
| `key`                             | string         | yes  | 变量的 `key`                                                                                                  |
| `value`                           | string         | yes  | 变量的 `value`                                                                                                |
| `variable_type`                   | string         | no   | 变量的类型。可用类型为 `env_var`（默认）和 `file`                                                                          |
| `protected`                       | boolean        | no   | 变量是否受保护                                                                                                    |
| `masked`                          | boolean        | no   | 变量是否隐藏                                                                                                     |
| `raw`                             | boolean        | no   | 变量是否被视为原始字符串。默认值：`false`。当 `true` 时，值中的变量不会[展开](../ci/variables/index.md#prevent-cicd-variable-expansion)                                                                                                    |
| `environment_scope` **(PREMIUM)** | string         | no   | <!--[environment scope](../ci/variables/index.md#limit-the-environment-scope-of-a-cicd-variable)-->变量的环境范围 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/1/variables/NEW_VARIABLE" --form "value=updated value"
```

```json
{
  "key": "NEW_VARIABLE",
  "value": "updated value",
  "variable_type": "env_var",
  "protected": true,
  "masked": true,
  "raw": true,
  "environment_scope": "*"
}
```

## 移除变量

移除群组的变量。

```plaintext
DELETE /groups/:id/variables/:key
```

| 参数    | 类型             | 是否必需 | 描述                                                              |
|-------|----------------|------|-----------------------------------------------------------------|
| `id`  | integer/string | yes  | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `key` | string         | yes  | 变量的 `key`                                      |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/1/variables/VARIABLE_1"
```
