---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 流水线触发令牌 API **(FREE)**

您可以从[通过 API 触发流水线](../ci/triggers/index.md)获取更多详情。

## 获取项目触发令牌列表

获取某项目的流水线触发器令牌列表。

```plaintext
GET /projects/:id/triggers
```

| 参数 | 类型    | 是否必需 | 描述         |
|-----------|---------|----------|---------------------|
| `id`      | integer/string | 是      | 经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding) |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/triggers"
```

```json
[
    {
        "id": 10,
        "description": "my trigger",
        "created_at": "2016-01-07T09:53:58.235Z",
        "last_used": null,
        "token": "6d056f63e50fe6f8c5f8f4aa10edb7",
        "updated_at": "2016-01-07T09:53:58.235Z",
        "owner": null
    }
]
```

被验证用户获取自己创建的触发令牌时，令牌会完全显示，如果触发令牌是其他用户创建时，令牌会只显示4个字符。

## 获取触发令牌详情

获取某项目的触发令牌详情。

```plaintext
GET /projects/:id/triggers/:trigger_id
```

| 参数 | 类型    | 是否必需 | 描述         |
|--------------|---------|----------|--------------------------|
| `id`         | integer/string | 是      | 经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding)      |
| `trigger_id` | integer | 是      | 触发器 ID          |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/triggers/5"
```

```json
{
    "id": 10,
    "description": "my trigger",
    "created_at": "2016-01-07T09:53:58.235Z",
    "last_used": null,
    "token": "6d056f63e50fe6f8c5f8f4aa10edb7",
    "updated_at": "2016-01-07T09:53:58.235Z",
    "owner": null
}
```

## 创建一个触发令牌

为某项目创建一个流水线触发器令牌。

```plaintext
POST /projects/:id/triggers
```

| 参数 | 类型    | 是否必需 | 描述         |
|---------------|---------|----------|--------------------------|
| `id`          | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding)      |
| `description` | string  | yes      | 触发器的名字         |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --form description="my description" "https://gitlab.example.com/api/v4/projects/1/triggers"
```

```json
{
    "id": 10,
    "description": "my trigger",
    "created_at": "2016-01-07T09:53:58.235Z",
    "last_used": null,
    "token": "6d056f63e50fe6f8c5f8f4aa10edb7",
    "updated_at": "2016-01-07T09:53:58.235Z",
    "owner": null
}
```

## 更新一个触发器令牌

为某项目更新流水线触发器令牌。

```plaintext
PUT /projects/:id/triggers/:trigger_id
```

| 参数 | 类型    | 是否必需 | 描述         |
|---------------|---------|----------|--------------------------|
| `id`          | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding)      |
| `trigger_id`  | integer | yes      | 触发器 ID           |
| `description` | string  | no       | 触发器的名字         |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     --form description="my description" "https://gitlab.example.com/api/v4/projects/1/triggers/10"
```

```json
{
    "id": 10,
    "description": "my trigger",
    "created_at": "2016-01-07T09:53:58.235Z",
    "last_used": null,
    "token": "6d056f63e50fe6f8c5f8f4aa10edb7",
    "updated_at": "2016-01-07T09:53:58.235Z",
    "owner": null
}
```

## 删除项目触发器令牌

为某项目删除流水线触发器令牌。

```plaintext
DELETE /projects/:id/triggers/:trigger_id
```

| 参数 | 类型    | 是否必需 | 描述         |
|----------------|---------|----------|--------------------------|
| `id`           | integer/string | yes      |  经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding)      |
| `trigger_id`   | integer | yes      | 触发器 ID           |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/triggers/5"
```

## 使用令牌触发一条流水线

使用[触发器令牌](../ci/triggers/index.md#create-a-trigger-token)或者经过校验的[CI/CD 任务令牌](../ci/jobs/ci_job_token.md)来触发一条流水线。

使用 CI/CD 作业令牌时，[触发的流水线涉及到多个项目](../ci/jobs/ci_job_token.md#trigger-a-multi-project-pipeline-by-using-a-cicd-job-token)。
校验请求的任务会和上游的流水线相关，用户可以在流水线图表中查看。

如果在某个作业中使用的是触发器令牌，则该任务不会和上游流水线关联。

```plaintext
POST /projects/:id/trigger/pipeline
```

支持的参数：

| 参数 | 类型             | 是否必需 | 描述                                                                            |
|:------------|:---------------|:-----------------------|:------------------------------------------------------------------------------|
| `id`        | integer/string | **{check-circle}** 是 | 经过身份验证的用户拥有的项目 ID 或者[经过 URL 编码的项目地址](rest/index.md#namespaced-path-encoding)。 |
| `ref`       | string         | **{check-circle}** 是 | 流水线运行的分支或标签。                                                                  |
| `token`     | string         | **{check-circle}** 是 | 触发器令牌或者 CI/CD 任务令牌。                                                           |
| `variables` | hash           | **{dotted-circle}** 否 | 包含流水线变量的键值字符串映射。例如：`{ VAR1: "value1", VAR2: "value2" }`。                      |

请求示例：

```shell
curl --request POST --form "variables[VAR1]=value1" --form "variables[VAR2]=value2" "https://gitlab.example.com/api/v4/projects/123/trigger/pipeline?token=2cb1840fb9dfc9fb0b7b1609cd29cb&ref=main"
```

响应示例：

```json
{
  "id": 257,
  "iid": 118,
  "project_id": 123,
  "sha": "91e2711a93e5d9e8dddfeb6d003b636b25bf6fc9",
  "ref": "main",
  "status": "created",
  "source": "trigger",
  "created_at": "2022-03-31T01:12:49.068Z",
  "updated_at": "2022-03-31T01:12:49.068Z",
  "web_url": "http://127.0.0.1:3000/test-group/test-project/-/pipelines/257",
  "before_sha": "0000000000000000000000000000000000000000",
  "tag": false,
  "yaml_errors": null,
  "user": {
    "id": 1,
    "username": "root",
    "name": "Administrator",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://127.0.0.1:3000/root"
  },
  "started_at": null,
  "finished_at": null,
  "committed_at": null,
  "duration": null,
  "queued_duration": null,
  "coverage": null,
  "detailed_status": {
    "icon": "status_created",
    "text": "created",
    "label": "created",
    "group": "created",
    "tooltip": "created",
    "has_details": true,
    "details_path": "/test-group/test-project/-/pipelines/257",
    "illustration": null,
    "favicon": "/assets/ci_favicons/favicon_status_created-4b975aa976d24e5a3ea7cd9a5713e6ce2cd9afd08b910415e96675de35f64955.png"
  }
}
```
