---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 备注 API **(FREE)**

备注是对以下内容进行的评论：

- 代码片段
- 议题
- 合并请求
- [史诗](../user/group/epics/index.md)

包括系统备注，它们是关于对象更改的备注（例如，指派人发生了更改，极狐GitLab 会发布系统备注）。

## 资源事件

> - 状态、里程碑和权重事件引入于极狐GitLab 13.3。 
> - 迭代事件引入于极狐GitLab 13.4。

有些系统备注不属于 API，被记录为单独事件：

- [资源标记事件](resource_label_events.md)
- [资源状态事件](resource_state_events.md)
- [资源里程碑事件](resource_milestone_events.md)
- [资源权重事件](resource_weight_events.md)
- [资源迭代事件](resource_iteration_events.md)

## 备注分页

默认情况下，`GET` 请求一次返回 20 个结果，因为 API 结果是分页的。

阅读更多关于[分页](rest/index.md#pagination)的内容。

## 速率限制

为了避免滥用，您可以为您的用户设置每分钟所能进行的 `Create` 请求的最大数量。
详情请参见[备注速率限制](../user/admin_area/settings/rate_limit_on_notes_creation.md)。

<a id="issues"></a>

## 议题

### 列出项目议题备注

获取单个议题的所有备注列表。

```plaintext
GET /projects/:id/issues/:issue_iid/notes
GET /projects/:id/issues/:issue_iid/notes?sort=asc&order_by=updated_at
```

| 参数          | 类型                | 是否必需 | 描述                                                                                         |
|-------------|-------------------|------|--------------------------------------------------------------------------------------------|
| `id`        | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)             
| `issue_iid` | integer           | yes  | 议题 IID                                                                      
| `sort`      | string            | no   |返回以 `asc` 或 `desc` 顺序排序的议题备注。默认为 `desc`                      
| `order_by`  | string            | no   |返回按照 `created_at` 或 `updated_at` 字段排序的议题备注。 默认为 `created_at` 


```json
[
  {
    "id": 302,
    "body": "closed",
    "attachment": null,
    "author": {
      "id": 1,
      "username": "pipin",
      "email": "admin@example.com",
      "name": "Pip",
      "state": "active",
      "created_at": "2013-09-30T13:46:01Z"
    },
    "created_at": "2013-10-02T09:22:45Z",
    "updated_at": "2013-10-02T10:22:45Z",
    "system": true,
    "noteable_id": 377,
    "noteable_type": "Issue",
    "project_id": 5,
    "noteable_iid": 377,
    "resolvable": false,
    "confidential": false,
    "internal": false
  },
  {
    "id": 305,
    "body": "Text of the comment\r\n",
    "attachment": null,
    "author": {
      "id": 1,
      "username": "pipin",
      "email": "admin@example.com",
      "name": "Pip",
      "state": "active",
      "created_at": "2013-09-30T13:46:01Z"
    },
    "created_at": "2013-10-02T09:56:03Z",
    "updated_at": "2013-10-02T09:56:03Z",
    "system": true,
    "noteable_id": 121,
    "noteable_type": "Issue",
    "project_id": 5,
    "noteable_iid": 121,
    "resolvable": false,
    "confidential": true,
    "internal": true
  }
]
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/notes"
```

### 获取单个议题备注

返回特定项目议题的单个备注。

```plaintext
GET /projects/:id/issues/:issue_iid/notes/:note_id
```

参数：

| 参数          | 类型                | 是否必需 | 描述                                                    |
|-------------|-------------------|------|-------------------------------------------------------|
| `id`        | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer           | yes  | 项目议题 IID                                              |
| `note_id`   | integer           | yes  | 议题备注 ID                           |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/notes/1"
```

### 创建新议题备注

为单个项目议题创建备注。

```plaintext
POST /projects/:id/issues/:issue_iid/notes
```

参数：

| 参数             | 类型                | 是否必需  | 描述                                                                                  |
|----------------|-------------------|-------|-------------------------------------------------------------------------------------|
| `id`           | integer or string | yes   | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)                          |
| `issue_iid`    | integer           | yes   | 议题 IID                                                                              |
| `body`         | string            | yes   | 备注内容。限制为 1,000,000 个字符                                                              |
| `confidential` | boolean           | no    | **废弃**：计划在 16.0 中移除并重命名为 `internal`。备注的私密标记。默认为 False                               |
| `created_at`   | string            | no    | 日期时间字符串，格式为 ISO 8601，且必需在 1970-01-01 之后。例如：`2016-03-11T03:45:40Z`（需要管理员或项目/群组拥有者权限） |
| `internal`     | boolean           | no    | 备注的内部标记。当两个参数都提交时，覆盖 `confidential`。默认为 False                                       |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/notes?body=note"
```

### 修改现存议题备注

修改议题的现存备注。

```plaintext
PUT /projects/:id/issues/:issue_iid/notes/:note_id
```

参数：

| 参数             | 类型                | 是否必需 | 描述                                                         |
|----------------|-------------------|------|------------------------------------------------------------|
| `id`           | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid`    | integer           | yes  | 议题 IID                                                     |
| `note_id`      | integer           | yes  | 备注 ID                                                      |
| `body`         | string            | no   | 备注内容。限制为 1,000,000 个字符                                     |
| `confidential` | boolean           | no   | **已废弃：** 计划在 16.0 中移除。备注的私密标记。默认为 False                    |


```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/notes/636?body=note"
```

### 删除议题备注

删除议题的现存备注。

```plaintext
DELETE /projects/:id/issues/:issue_iid/notes/:note_id
```

参数：

| 参数          | 类型                | 是否必需 | 描述                                                    |
|-------------|-------------------|------|-------------------------------------------------------|
| `id`        | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer           | yes  | 议题 IID                                                |
| `note_id`   | integer           | yes  | 备注 ID                                        |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/notes/636"
```

## 代码片段

代码片段备注 API 用于项目级别，而不是个人的代码片段。

### 列出所有代码片段备注

获取单个代码片段的所有备注列表。代码片段备注是用户可以发布到代码片段的评论。

```plaintext
GET /projects/:id/snippets/:snippet_id/notes
GET /projects/:id/snippets/:snippet_id/notes?sort=asc&order_by=updated_at
```

| 参数           | 类型                | 是否必需 | 描述                                                                                           |
|--------------|-------------------|------|----------------------------------------------------------------------------------------------|
| `id`         | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)               
| `snippet_id` | integer           | yes  | 项目代码片段 ID                                                                   
| `sort`       | string            | no   | 返回以 `asc` 或 `desc` 顺序排序的代码片段备注。默认为 `desc`                      
| `order_by`   | string            | no   | 返回按照 `created_at` 或 `updated_at` 字段排序的代码片段备注。默认为 `created_at` 

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/snippets/11/notes"
```

### 获取单个代码片段备注 

返回特定代码片段的单个备注。

```plaintext
GET /projects/:id/snippets/:snippet_id/notes/:note_id
```

参数：

| 参数           | 类型                | 是否必需 | 描述                                                    |
|--------------|-------------------|------|-------------------------------------------------------|
| `id`         | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `snippet_id` | integer           | yes  | 项目代码片段 ID                                             |
| `note_id`    | integer           | yes  | 代码片段备注 ID                          |

```json
{
  "id": 302,
  "body": "closed",
  "attachment": null,
  "author": {
    "id": 1,
    "username": "pipin",
    "email": "admin@example.com",
    "name": "Pip",
    "state": "active",
    "created_at": "2013-09-30T13:46:01Z"
  },
  "created_at": "2013-10-02T09:22:45Z",
  "updated_at": "2013-10-02T10:22:45Z",
  "system": true,
  "noteable_id": 377,
  "noteable_type": "Issue",
  "project_id": 5,
  "noteable_iid": 377,
  "resolvable": false,
  "confidential": false,
  "internal": false
}
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/snippets/11/notes/11"
```

### 创建新代码片段备注

为单个代码片段创建新备注。代码片段备注是用户对代码片段的评论。
如果您创建一个正文仅包含表情符号反应的备注，极狐GitLab 将返回此对象。

```plaintext
POST /projects/:id/snippets/:snippet_id/notes
```

参数：

| 参数           | 类型                | 是否必需 | 描述                                                                                                                      |
|--------------|-------------------|------|-------------------------------------------------------------------------------------------------------------------------|
| `id`         | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)                                                                   |
| `snippet_id` | integer           | yes  | 代码片段 ID                                                                                                                 |
| `body`       | string            | yes  | 备注内容。限制为 1,000,000 个字符串                                                                                                 |
| `created_at` | string            | no   | 日期时间字符串。格式为 ISO 8601。例如：`2016-03-11T03:45:40Z`（需要管理员或项目/群组拥有者权利） |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/snippet/11/notes?body=note"
```

### 修改现存代码片段备注

修改代码片段的现存备注。

```plaintext
PUT /projects/:id/snippets/:snippet_id/notes/:note_id
```

参数：

| 参数           | 类型                | 是否必需 | 描述                                                    |
|--------------|-------------------|------|-------------------------------------------------------|
| `id`         | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `snippet_id` | integer           | yes  | 代码片段 ID                                               |
| `note_id`    | integer           | yes  | 代码片段备注 ID                                             |
| `body`       | string            | yes  | 备注内容。限制为 1,000,000 个字符                                |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/snippets/11/notes/1659?body=note"
```

### 删除代码片段备注

删除代码片段的现存备注。

```plaintext
DELETE /projects/:id/snippets/:snippet_id/notes/:note_id
```

参数

| 参数           | 类型                | 是否必需 | 描述                                                    |
|--------------|-------------------|------|-------------------------------------------------------|
| `id`         | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `snippet_id` | integer           | yes  | 代码片段 ID                                               |
| `note_id`    | integer           | yes  | 备注 ID                                      |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/snippets/52/notes/1659"
```

<a id="merge-requests"></a>

## 合并请求

### 列出所有合并请求备注

获取单个合并请求的所有备注列表。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/notes
GET /projects/:id/merge_requests/:merge_request_iid/notes?sort=asc&order_by=updated_at
```

| 参数                  | 类型                | 是否必需 | 描述                                                                                                 |
|---------------------|-------------------|------|----------------------------------------------------------------------------------------------------|
| `id`                | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)                     
| `merge_request_iid` | integer           | yes  | 项目合并请求 IID                                                                  
| `sort`              | string            | no   | 返回以 `asc` 或 `desc` 顺序排序的合并请求备注。默认为 `desc`                      
| `order_by`          | string            | no   | 返回按照 `created_at` 或 `updated_at` 字段排序的合并请求备注。默认为 `created_at` 

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/notes"
```

### 获取单个合并请求备注

获取特定合并请求的单个备注。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/notes/:note_id
```

参数：

| 参数                  | 类型                | 是否必需 | 描述                                                   |
|---------------------|-------------------|------|------------------------------------------------------|
| `id`                | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer           | yes  | 项目合并请求 IID                                           |
| `note_id`           | integer           | yes  | 合并请求备注 ID                   |

```json
{
  "id": 301,
  "body": "Comment for MR",
  "attachment": null,
  "author": {
    "id": 1,
    "username": "pipin",
    "email": "admin@example.com",
    "name": "Pip",
    "state": "active",
    "created_at": "2013-09-30T13:46:01Z"
  },
  "created_at": "2013-10-02T08:57:14Z",
  "updated_at": "2013-10-02T08:57:14Z",
  "system": false,
  "noteable_id": 2,
  "noteable_type": "MergeRequest",
  "project_id": 5,
  "noteable_iid": 2,
  "resolvable": false,
  "confidential": false,
  "internal": false
}
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/notes/1"
```

<a id="create-new-merge-request-note"></a>

### 创建新合并请求备注

为单个合并请求创建新备注。备注不会附加到合并请求中的特定行。对于其他具有更精细控制的方法，请参阅提交 API 中的[向提交添加评论](commits.md#post-comment-to-commit)和讨论 API 中的[在合并请求的差异中创建主题](discussions.md#create-a-new-thread-in-the-merge-request-diff)。

如果您创建一个备注，其中正文仅包含表情符号反应，极狐GitLab 将返回此对象。

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/notes
```

参数：

| 参数                       | 类型                | 是否必需 | 描述                                                                                   |
|--------------------------|-------------------|------|--------------------------------------------------------------------------------------|
| `id`                     | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)                                |
| `merge_request_iid`      | integer           | yes  | 项目合并请求 IID                                                                           |
| `body`                   | string            | yes  | 备注内容。限制为 1,000,000 个字符                                                               |
| `created_at`             | string            | no   | 日期时间字符串。格式为 ISO 8601。例如：`2016-03-11T03:45:40Z`（需要管理员或项目/群组拥有者权限）                     |
| `merge_request_diff_head_sha` | string            | no   | `/merge` [快速操作](../user/project/quick_actions.md) 需要。head 提交的 SHA，确保在发送 API 请求后合并请求未更新 |

### 修改现存合并请求备注

修改合并请求的现存备注。

```plaintext
PUT /projects/:id/merge_requests/:merge_request_iid/notes/:note_id
```

参数：

| 参数                  | 类型                | 是否必需 | 描述                                                         |
|---------------------|-------------------|------|------------------------------------------------------------|
| `id`                | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer           | yes  | 项目合并请求 IID                                                 |
| `note_id`           | integer           | no   | 备注 ID                                                      |
| `body`              | string            | yes  | 备注内容。限制为 1,000,000 个字符                                     |
| `confidential`      | boolean           | no   | **已废弃：** 计划在 16.0 中移除。备注的私密标记。默认为 False                    |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/notes/1?body=note"
```

### 删除合并请求备注

删除合并请求的现存备注。

```plaintext
DELETE /projects/:id/merge_requests/:merge_request_iid/notes/:note_id
```

参数：

| 参数                  | 类型                | 是否必需 | 描述                                                    |
|---------------------|-------------------|------|-------------------------------------------------------|
| `id`                | integer or string | yes  | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer           | yes  | 合并请求 IID                                              |
| `note_id`           | integer           | yes  | 备注 ID                                       |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/7/notes/1602"
```

## 史诗 **(PREMIUM)**

### 列出所有史诗备注

获取单个史诗的所有备注列表。史诗备注是用户可以发布到史诗的评论。

NOTE:
史诗备注 API 使用史诗 ID 而不是史诗 IID。如果您使用史诗的 IID，极狐GitLab 将返回 404 错误或其他史诗的备注。它不同于[议题备注 API](#issues) 和[合并请求备注 API](#merge-requests)。

```plaintext
GET /groups/:id/epics/:epic_id/notes
GET /groups/:id/epics/:epic_id/notes?sort=asc&order_by=updated_at
```

| 参数         | 类型                | 是否必需 | 描述                                                                           |
|------------|-------------------|------|------------------------------------------------------------------------------|
| `id`       | integer or string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                        |
| `epic_id`  | integer           | yes  | 群组史诗 ID                                                                      |
| `sort`     | string            | no   | 返回以 `asc` 或 `desc` 顺序排序的史诗备注。默认为 `desc`                                      |
| `order_by` | string            | no   | 返回按照 `created_at` 或 `updated_at` 字段排序的史诗备注。默认为 `created_at` |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/epics/11/notes"
```

### 获取单个史诗备注

返回特定史诗的单个备注。

```plaintext
GET /groups/:id/epics/:epic_id/notes/:note_id
```

参数：

| 参数        | 类型                | 是否必需 | 描述                                                    |
|-----------|-------------------|------|-------------------------------------------------------|
| `id`      | integer or string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `epic_id` | integer           | yes  | 史诗 ID                                                 |
| `note_id` | integer           | yes  | 备注 ID                                        |

```json
{
  "id": 302,
  "body": "Epic note",
  "attachment": null,
  "author": {
    "id": 1,
    "username": "pipin",
    "email": "admin@example.com",
    "name": "Pip",
    "state": "active",
    "created_at": "2013-09-30T13:46:01Z"
  },
  "created_at": "2013-10-02T09:22:45Z",
  "updated_at": "2013-10-02T10:22:45Z",
  "system": true,
  "noteable_id": 11,
  "noteable_type": "Epic",
  "project_id": 5,
  "noteable_iid": 11,
  "resolvable": false,
  "confidential": false,
  "internal": false
}
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/epics/11/notes/1"
```

### 创建新史诗备注

为单个史诗创建新备注。史诗备注是用户可以发布到史诗的评论。
如果您创建一个正文仅包含表情符号反应的备注，极狐GitLab 将返回此对象。

```plaintext
POST /groups/:id/epics/:epic_id/notes
```

参数：

| 参数          | 类型                | 是否必需 | 描述                                                         |
|-------------|-------------------|------|------------------------------------------------------------|
| `body`      | string            | yes  | 备注内容。限制为 1,000,000 个字符                                     |
| `epic_id`   | integer           | yes  | 史诗 ID                                                      |
| `id`        | integer or string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `confidential` | boolean           | no   | **已弃用：** 计划在 16.0 中移除，并重命名为 `internal`。备注的私密标志。默认为 `false` |
| `internal`  | boolean  | no   | 备注的内部标记。提交两个参数时覆盖 `confidential`。默认为 `false`               |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/epics/11/notes?body=note"
```

### 修改现存史诗备注

修改史诗的现存备注。

```plaintext
PUT /groups/:id/epics/:epic_id/notes/:note_id
```

参数：

| 参数             | 类型                | 是否必需 | 描述                                                         |
|----------------|-------------------|------|------------------------------------------------------------|
| `id`           | integer or string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `epic_id`      | integer           | yes  | 史诗 ID                                                      |
| `note_id`      | integer           | yes  | 备注 ID                                                      |
| `body`         | string            | yes  | 备注内容。限制为 1,000,000 个字符                                     |
| `confidential` | boolean           | no   | **已废弃：** 计划在 16.0 中移除。备注的私密标记。默认为 False                    |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/epics/11/notes/1?body=note"
```

### 删除史诗备注

删除史诗的现存备注。

```plaintext
DELETE /groups/:id/epics/:epic_id/notes/:note_id
```

参数：

| 参数        | 类型                | 是否必需 | 描述                                                    |
|-----------|-------------------|------|-------------------------------------------------------|
| `id`      | integer or string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `epic_id` | integer           | yes  | 史诗 ID                                                 |
| `note_id` | integer           | yes  | 备注 ID                                       |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/epics/52/notes/1659"
```
