---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 计划限制 API **(FREE SELF)**

> 引入于极狐GitLab 13.10 版本。

计划限制 API 允许您对已经订阅的计划，维护应用程序的限制。

使用此 API 需要管理员权限。

## 获取当前计划的限制

在极狐GitLab 实例中获取当前计划的限制。

```plaintext
GET /application/plan_limits
```

| 参数                              | 类型     | 是否必需  | 描述 |
| --------------------------------- | ------- | -------- | ----------- |
| `plan_name`                       | string  | no       | 要查询限制的计划的名称，默认是：`default`。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/application/plan_limits"
```

响应示例：

```json
{
  "ci_pipeline_size": 0,
  "ci_active_jobs": 0,
  "ci_project_subscriptions": 2,
  "ci_pipeline_schedules": 10,
  "ci_needs_size_limit": 50,
  "ci_registered_group_runners": 1000,
  "ci_registered_project_runners": 1000,
  "conan_max_file_size": 3221225472,
  "enforcement_limit": 10000,
  "generic_packages_max_file_size": 5368709120,
  "helm_max_file_size": 5242880,
  "notification_limit": 10000,
  "maven_max_file_size": 3221225472,
  "npm_max_file_size": 524288000,
  "nuget_max_file_size": 524288000,
  "pypi_max_file_size": 3221225472,
  "terraform_module_max_file_size": 1073741824,
  "storage_size_limit": 15000
}
```

## 修改计划的限制

在极狐GitLab 实例中修改计划的限制。

```plaintext
PUT /application/plan_limits
```

| 参数                               | 类型    | 是否必需   | 描述                                     |
| --------------------------------- | ------- | -------- |----------------------------------------|
| `plan_name`                       | string  | yes      | 要更新的计划的名称。                             |
| `ci_pipeline_size`                | integer | no       | 单个流水线中作业的最大数量，引入于极狐GitLab 15.0。        |
| `ci_active_jobs`                  | integer | no       | 当前活跃流水线中的作业总数，引入于极狐GitLab 15.0。        |
| `ci_project_subscriptions`        | integer | no       | 项目的流水线订阅的最大数量，引入于极狐GitLab 15.0。        |
| `ci_pipeline_schedules`           | integer | no       | 最大流水线计划数量，引入于极狐GitLab 15.0。            |
| `ci_needs_size_limit`             | integer | no       | 作业能够拥有的 DAG 依赖项的最大数量，引入于极狐GitLab 15.0。 |
| `ci_registered_group_runners`     | integer | no       | 每个群组注册的 Runner 的最大数量，引入于极狐GitLab 15.0。 |
| `ci_registered_project_runners`   | integer | no       | 每个项目注册的 Runner 的最大数量，引入于极狐GitLab 15.0。 |
| `conan_max_file_size`             | integer | no       | 最大 Conan 包大小（以字节为单位）。                  |
| `enforcement_limit`               | integer | no       | 根命名空间限制实施的最大存储大小（以 MiB 为单位）。           |
| `generic_packages_max_file_size`  | integer | no       | 最大通用包大小（以字节为单位）。                       |
| `helm_max_file_size`              | integer | no       | 最大 Helm chart 文件大小（以字节为单位）。            |
| `maven_max_file_size`             | integer | no       | 最大 Maven 包大小（以字节为单位）。                  |
| `notification_limit`              | integer | no       | 根命名空间限制通知的最大存储大小（以 MiB 为单位）。           |
| `npm_max_file_size`               | integer | no       | 最大 Npm 包大小（以字节为单位）。                    |
| `nuget_max_file_size`             | integer | no       | 最大 NuGet 包大小（以字节为单位）。                  |
| `pypi_max_file_size`              | integer | no       | 最大 PyPI 包大小（以字节为单位）。                   |
| `terraform_module_max_file_size`  | integer | no       | 最大 Terraform 模块包大小（以字节为单位）。            |
| `storage_size_limit`              | integer | no       | 根命名空间中的最大存储空间大小 （以 MiB 为单位）。           |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/application/plan_limits?plan_name=default&conan_max_file_size=3221225472"
```

响应示例：

```json
{
  "ci_pipeline_size": 0,
  "ci_active_jobs": 0,
  "ci_project_subscriptions": 2,
  "ci_pipeline_schedules": 10,
  "ci_needs_size_limit": 50,
  "ci_registered_group_runners": 1000,
  "ci_registered_project_runners": 1000,
  "conan_max_file_size": 3221225472,
  "generic_packages_max_file_size": 5368709120,
  "helm_max_file_size": 5242880,
  "maven_max_file_size": 3221225472,
  "npm_max_file_size": 524288000,
  "nuget_max_file_size": 524288000,
  "pypi_max_file_size": 3221225472,
  "terraform_module_max_file_size": 1073741824
}
```
