---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 标记 API **(FREE)**

使用 REST API 和[标记](../user/project/labels.md)交互。

NOTE:
在 12.7 版本中添加 `description_html` 来响应 JSON。

## 列出标记

获取给定项目的所有标记。

默认情况下，这个请求一次返回 20 个结果，因为 API 结果是[分页](rest/index.md#pagination)的。

```plaintext
GET /projects/:id/labels
```

| 参数   | 类型           | 是否必需 | 描述                                                                    |
| ---------     | -------        | -------- |-----------------------------------------------------------------------|
| `id`          | integer/string | yes      | 可以被经过身份验证的用户访问的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `with_counts` | boolean        | no       | 是否包含议题和合并请求的数量。默认是 `false`。                                           |
| `include_ancestor_groups` | boolean | no | 包含上级群组，默认是 `true`。                                                    |
| `search` | string | no | 过滤标记使用的关键词。引入于 13.6 版本。                                               |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/labels?with_counts=true"
```

响应示例：

```json
[
  {
    "id" : 1,
    "name" : "bug",
    "color" : "#d9534f",
    "text_color" : "#FFFFFF",
    "description": "Bug reported by user",
    "description_html": "Bug reported by user",
    "open_issues_count": 1,
    "closed_issues_count": 0,
    "open_merge_requests_count": 1,
    "subscribed": false,
    "priority": 10,
    "is_project_label": true
  },
  {
    "id" : 4,
    "color" : "#d9534f",
    "text_color" : "#FFFFFF",
    "name" : "confirmed",
    "description": "Confirmed issue",
    "description_html": "Confirmed issue",
    "open_issues_count": 2,
    "closed_issues_count": 5,
    "open_merge_requests_count": 0,
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id" : 7,
    "name" : "critical",
    "color" : "#d9534f",
    "text_color" : "#FFFFFF",
    "description": "Critical issue. Need fix ASAP",
    "description_html": "Critical issue. Need fix ASAP",
    "open_issues_count": 1,
    "closed_issues_count": 3,
    "open_merge_requests_count": 1,
    "subscribed": false,
    "priority": null,
    "is_project_label": true
  },
  {
    "id" : 8,
    "name" : "documentation",
    "color" : "#f0ad4e",
    "text_color" : "#FFFFFF",
    "description": "Issue about documentation",
    "description_html": "Issue about documentation",
    "open_issues_count": 1,
    "closed_issues_count": 0,
    "open_merge_requests_count": 2,
    "subscribed": false,
    "priority": null,
    "is_project_label": false
  },
  {
    "id" : 9,
    "color" : "#5cb85c",
    "text_color" : "#FFFFFF",
    "name" : "enhancement",
    "description": "Enhancement proposal",
    "description_html": "Enhancement proposal",
    "open_issues_count": 1,
    "closed_issues_count": 0,
    "open_merge_requests_count": 1,
    "subscribed": true,
    "priority": null,
    "is_project_label": true
  }
]
```

## 获取项目单个标记

获取给定项目的单个标记。

```plaintext
GET /projects/:id/labels/:label_id
```

| 参数   | 类型           | 是否必需 | 描述                                                                    |
| ---------     | -------        | -------- |-----------------------------------------------------------------------|
| `id`          | integer or string | yes      | 可以被经过身份验证的用户访问的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `label_id` | integer or string | yes | 项目的 label 的 ID 或标题。                                                   |
| `include_ancestor_groups` | boolean | no | 是否包含上级群组，默认是 `true`。                                                  |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/labels/bug"
```

响应示例：

```json
{
  "id" : 1,
  "name" : "bug",
  "color" : "#d9534f",
  "text_color" : "#FFFFFF",
  "description": "Bug reported by user",
  "description_html": "Bug reported by user",
  "open_issues_count": 1,
  "closed_issues_count": 0,
  "open_merge_requests_count": 1,
  "subscribed": false,
  "priority": 10,
  "is_project_label": true
}
```

## 创建一个新的标记

为给定的存储库使用给定的名称和颜色创建一个新的标记。

```plaintext
POST /projects/:id/labels
```

| 参数   | 类型           | 是否必需 | 描述                                                                                                  |
| ------------- | ------- | -------- | ---------------------------- |
| `id`      | integer/string    | yes      | 可以被经过身份验证的用户访问的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `name`        | string  | yes      |  标记的名称     |
| `color`       | string  | yes      | 以 `#` 号开头，6位十六进制符号表示的标记颜色。(例如 #FFAABB) 或者 [CSS 颜色名称](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#Color_keywords)之一。|
| `description` | string  | no       | 标记的描述。 |
| `priority`    | integer | no       | 标记的优先级，必须大于等于0，或者使用 `null` 移除优先级。  |

```shell
curl --data "name=feature&color=#5843AD" --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/labels"
```

响应示例：

```json
{
  "id" : 10,
  "name" : "feature",
  "color" : "#5843AD",
  "text_color" : "#FFFFFF",
  "description":null,
  "description_html":null,
  "open_issues_count": 0,
  "closed_issues_count": 0,
  "open_merge_requests_count": 0,
  "subscribed": false,
  "priority": null,
    "is_project_label": true
}
```

## 删除一个标记

使用给定名称删除一个标记。

```plaintext
DELETE /projects/:id/labels/:label_id
```

| 参数   | 类型           | 是否必需 | 描述                                                                                                  |
| --------- | ------- | -------- | --------------------- |
| `id`            | integer or string | yes | 可以被经过身份验证的用户访问的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `label_id` | integer or string | yes | 群组标记的 ID 或标题。 |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/labels/bug"
```

NOTE:
一个旧的带 `name` 参数的端点 `DELETE /projects/:id/labels` 仍然可以使用，但是已经废弃了。

## 编辑一个已有的标记

使用新的名称或新的颜色更新一个已有的标记。要更新标记至少需要一个参数。 

```plaintext
PUT /projects/:id/labels/:label_id
```

| 参数   | 类型           | 是否必需 | 描述                                                                                                  |
| --------------- | ------- | --------------------------------- | -------------------------------  |
| `id`      | integer or string    | yes      | 可以被经过身份验证的用户访问的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `label_id` | integer or string | yes | 群组标记的 ID 或标题。 |
| `new_name`      | string  | yes if `color` is not provided    | 标记的新名称        |
| `color`         | string  | yes if `new_name` is not provided | 以 `#` 号开头，6 位十六进制符号表示的标记颜色。(例如#FFAABB) 或者 [CSS 颜色名称](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#Color_keywords)之一。 |
| `description`   | string  | no                                | 标记的新描述。 |
| `priority`    | integer | no       | 标记新的优先级。必须大于等于 0，或 `null` 来移除优先级。 |

```shell
curl --request PUT --data "new_name=docs&color=#8E44AD&description=Documentation" \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/labels/documentation"
```

响应示例：

```json
{
  "id" : 8,
  "name" : "docs",
  "color" : "#8E44AD",
  "text_color" : "#FFFFFF",
  "description": "Documentation",
  "description_html": "Documentation",
  "open_issues_count": 1,
  "closed_issues_count": 0,
  "open_merge_requests_count": 2,
  "subscribed": false,
  "priority": null,
  "is_project_label": true
}
```

NOTE:
一个旧的带 `name` 或 `label_id` 参数的端点 `PUT /projects/:id/labels` 仍然可用，但是已经废弃了。

## 将项目标记提升为群组标记

> - 在 12.3 版本里引入。
> - 在 13.6 及更高版本，提升一个项目标记会保留标记的 ID 然后把它变更为群组标记。
> - 在之前的版本，提升一个项目标记，会创建一个具有新 ID 的群组标记，然后删除旧的标记。

提升一个项目标记到群组标记，标记会保留它的 ID。

```plaintext
PUT /projects/:id/labels/:label_id/promote
```

| 参数   | 类型           | 是否必需 | 描述                                                                                                  |
| --------------- | ------- | --------------------------------- | -------------------------------  |
| `id`      | integer or string    | yes      | 可以被经过身份验证的用户访问的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `label_id` | integer or string | yes | 群组标记的 ID 或标题。 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/labels/documentation/promote"
```

响应示例：

```json
{
  "id" : 8,
  "name" : "documentation",
  "color" : "#8E44AD",
  "description": "Documentation",
  "description_html": "Documentation",
  "open_issues_count": 1,
  "closed_issues_count": 0,
  "open_merge_requests_count": 2,
  "subscribed": false
}
```

NOTE:
一个旧的带 `name` 参数的端点 `PUT /projects/:id/labels/promote` 仍然可用，但是已经废弃了。

## 订阅到一个标记

认证用户订阅标记来接收通知，如果用户已经订阅这个标记，那么会返回状态码 `304`。

```plaintext
POST /projects/:id/labels/:label_id/subscribe
```

| 参数   | 类型           | 是否必需 | 描述                                                                                                  |
| ---------- | ----------------- | -------- | ------------------------------------ |
| `id`      | integer or string    | yes      | 可以被经过身份验证的用户访问的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `label_id` | integer or string | yes      | 项目 label 的 ID 或标题 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/labels/1/subscribe"
```

响应示例：

```json
{
  "id" : 1,
  "name" : "bug",
  "color" : "#d9534f",
  "text_color" : "#FFFFFF",
  "description": "Bug reported by user",
  "description_html": "Bug reported by user",
  "open_issues_count": 1,
  "closed_issues_count": 0,
  "open_merge_requests_count": 1,
  "subscribed": true,
  "priority": null,
  "is_project_label": true
}
```

## 取消一个标记的订阅

认证用户取消对某个标记的订阅不再接收来自这个标记的通知。如果用户没有订阅到这个标记，那么会返回状态码 `304`。

```plaintext
POST /projects/:id/labels/:label_id/unsubscribe
```

| 参数   | 类型           | 是否必需 | 描述                                                                                                  |
| ---------- | ----------------- | -------- | ------------------------------------ |
| `id`      | integer or string    | yes      | 可以被经过身份验证的用户访问的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)|
| `label_id` | integer or string | yes      | 项目标记的 ID 或标题 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/labels/1/unsubscribe"
```
