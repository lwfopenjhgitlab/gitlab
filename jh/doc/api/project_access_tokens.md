---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目访问令牌 API **(FREE)**

您可以通过访问[项目访问令牌](../user/project/settings/project_access_tokens.md)了解更多内容。

## 列出项目访问令牌列表

<!--
> [引入](https://gitlab.com/gitlab-org/gitlab/-/issues/238991)于极狐GitLab 13.9。
!-->

获取[项目访问令牌](../user/project/settings/project_access_tokens.md)的列表。

```plaintext
GET projects/:id/access_tokens
```

| 参数 | 类型    | 是否必需 | 描述         |
|-----------|---------|----------|---------------------|
| `id` | integer or string | yes | ID 或者 [URL 编码中项目的路径](rest/index.md#namespaced-path-encoding。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/<project_id>/access_tokens"
```

```json
[
   {
      "user_id" : 141,
      "scopes" : [
         "api"
      ],
      "name" : "token",
      "expires_at" : "2021-01-31",
      "id" : 42,
      "active" : true,
      "created_at" : "2021-01-20T22:11:48.151Z",
      "revoked" : false,
      "access_level": 40
   }
]
```

## 获取项目的访问令牌

> 引入于极狐GitLab 14.10。

通过 ID 获取[项目访问令牌](../user/project/settings/project_access_tokens.md)。

```plaintext
GET projects/:id/access_tokens/:token_id
```

| 参数 | 类型    | 是否必需 | 描述         |
|-----------|---------|----------|---------------------|
| `id` | integer or string | yes | ID 或者 [项目的 URL 编码路径](rest/index.md#namespaced-path-encoding。 |
| `token_id` | integer or string | yes | 项目访问令牌的 ID。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/<project_id>/access_tokens/<token_id>"
```

```json
{
   "user_id" : 141,
   "scopes" : [
      "api"
   ],
   "name" : "token",
   "expires_at" : "2021-01-31",
   "id" : 42,
   "active" : true,
   "created_at" : "2021-01-20T22:11:48.151Z",
   "revoked" : false,
   "access_level": 40,
   "last_used_at": "2022-03-15T11:05:42.437Z"
}
```

## 创建项目访问令牌

> - 引入于极狐GitLab 13.10。
> - `token` 属性引入于极狐GitLab 13.10。

创建[项目访问令牌](../user/project/settings/project_access_tokens.md)。

当创建项目访问令牌时, 您设置的最高角色（访问权限）依赖于您在这个群组中是所有者还是维护者角色。例如，最高角色可设置如下： 

- 所有者 (`50`)， 如果您在这个项目中拥有所有者权限。
- 维护者 (`40`)，如果您在这个项目中拥有维护者权限。

在极狐GitLab 14.8 及更早版本中，项目访问令牌拥有维护者的最高角色。

```plaintext
POST projects/:id/access_tokens
```

| 参数 | 类型    | 是否必需 | 描述                                                                                                                                   |
|-----------|---------|----------|---------------------------------------------------------------------------------------------------------------------------------------|
| `id` | integer or string | yes | ID 或者 [项目的 URL 编码路径](rest/index.md#namespaced-path-encoding。                                                            |
| `name` | String | yes | 项目访问令牌的名称。                                 |
| `scopes` | `Array[String]` | yes | [权限范围列表](../user/project/settings/project_access_tokens.md#scopes-for-a-project-access-token)。                               |
| `access_level` | Integer | no | 访问权限。合法的值是 `10` (访客)，`20` (报告者)，`30` (开发者)，`40` (维护者)，and `50` (所有者)。默认是 `40`。 |
| `expires_at` | Date | no | 令牌过期时间为该日期的午夜（UTC）。|

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
--header "Content-Type:application/json" \
--data '{ "name":"test_token", "scopes":["api", "read_repository"], "expires_at":"2021-01-31", "access_level": 30 }' \
"https://gitlab.example.com/api/v4/projects/<project_id>/access_tokens"
```

```json
{
   "scopes" : [
      "api",
      "read_repository"
   ],
   "active" : true,
   "name" : "test",
   "revoked" : false,
   "created_at" : "2021-01-21T19:35:37.921Z",
   "user_id" : 166,
   "id" : 58,
   "expires_at" : "2021-01-31",
   "token" : "D4y...Wzr",
   "access_level": 30
}
```

## 轮换项目访问令牌

> 引入于极狐GitLab 16.0。

轮换项目访问令牌。撤销先前的令牌并创建一个在一周后过期的新令牌。

```plaintext
POST /projects/:id/access_tokens/:token_id/rotate
```

| 参数         | 类型                | 是否必需 | 描述                                                          |
|------------|-------------------|------|-------------------------------------------------------------|
| `id`       | integer or string | yes  | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `token_id` | integer or string | yes  | 项目访问令牌 ID                      |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/<project_id>/access_tokens/<token_id>/rotate"
```

### 响应

- `200: OK`：如果现有令牌已成功撤销并且新令牌已成功创建。
- `400: Bad Request`：如果没有成功轮换。
- `401: Unauthorized`：如果出现以下任一情况：
    - 用户无权访问具有指定 ID 的令牌。
    - 具有指定 ID 的令牌不存在。
- `404: Not Found`：如果用户是管理员但具有指定 ID 的令牌不存在。

## 撤销项目访问令牌

> 引入于极狐GitLab 13.9。

撤销[项目访问令牌](../user/project/settings/project_access_tokens.md)。

```plaintext
DELETE projects/:id/access_tokens/:token_id
```

| 参数       | 类型     |  是否必需 | 描述                 |
|-----------|---------|----------|---------------------|
| `id` | integer or string | yes | ID 或者 [URL 编码中项目的路径](rest/index.md#namespaced-path-encoding。 |
| `token_id` | integer or string | yes | 项目访问令牌的 ID。 |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/<project_id>/access_tokens/<token_id>"
```

### 响应

- `204: No Content`：撤销成功。
- `400 Bad Request` 或者 `404 Not Found`：撤销失败。
