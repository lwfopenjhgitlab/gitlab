---
stage: Verify
group: Pipeline Security
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
---

# 项目 CI/CD 作业令牌范围 API **(FREE)**

您可以阅读更多有关 [CI/CD 作业令牌](../ci/jobs/ci_job_token.md)的内容。

NOTE:
对 CI/CD 作业令牌范围 API 端点的所有请求都必须经过[身份验证](rest/index.md#authentication)。
经过身份验证的用户必须至少具有项目的维护者角色。

## 获取项目的 CI/CD 作业令牌访问设置

获取项目的 [CI/CD 作业令牌访问设置](../ci/jobs/ci_job_token.md#configure-cicd-job-token-access)（作业令牌范围）。

```plaintext
GET /projects/:id/job_token_scope
```

支持的参数：

| 参数 | 类型           | 是否必需               | 描述                                                                                                        |
|-----------|----------------|------------------------|-----------------------------------------------------------------------------------------------------------|
| `id`      | integer/string | **{check-circle}** Yes | 经过身份验证的用户拥有的项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |

如果成功，则返回 [`200`](rest/index.md#status-codes) 及以下响应参数：

| 参数          | 类型    | 描述                                                                                                                                                                    |
|:-------------------|:--------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `inbound_enabled`  | boolean | 表明其他项目中生成的 CI/CD 作业令牌是否有权访问该项目                                                                                                                                        |
| `outbound_enabled` | boolean | 表明此项目中生成的 CI/CD 作业令牌是否可以访问其他项目。已废弃且计划移除于 17.0<!--[Deprecated and planned for removal in GitLab 17.0 .](../update/removals.md#limit-ci_job_token-scope-is-disabled)--> |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/job_token_scope"
```

响应示例：

```json
{
  "inbound_enabled": true,
  "outbound_enabled": false
}
```

## 修正项目的 CI/CD 作业令牌访问设置

修正项目的[**允许使用 CI_作业令牌**设置访问此项目](../ci/jobs/ci_job_token.md#disable-the-job-token-scope-allowlist)（作业令牌范围）。

```plaintext
PATCH /projects/:id/job_token_scope
```

支持的参数：

| 参数 | 类型           | 是否必需               | 描述                                                                                                                              |
|-----------|----------------|------------------------|---------------------------------------------------------------------------------------------------------------------------------|
| `id`      | integer/string | **{check-circle}** Yes | 经过身份验证的用户拥有的项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                                                        |
| `enabled` | boolean        | **{check-circle}** Yes | 表明其他项目中生成的 CI/CD 作业令牌对该项目的访问受到限制|

如果成功，则返回 [`204`](rest/index.md#status-codes) 且无响应主体。

请求示例：

```shell
curl --request PATCH \
  --url "https://gitlab.example.com/api/v4/projects/1/job_token_scope" \
  --header 'PRIVATE-TOKEN: <your_access_token>' \
  --header 'Content-Type: application/json' \
  --data '{ "enabled": false }'
```

## 获取项目的 CI/CD 作业令牌入站许可名单

获取项目的 [CI/CD 作业令牌入站许可名单](../ci/jobs/ci_job_token.md#allow-access-to-your-project-with-a-job-token)（作业令牌范围）。

```plaintext
GET /projects/:id/job_token_scope/allowlist
```

支持的参数：

| 参数 | 类型           | 是否必需               | 描述                                                                                                        |
|-----------|----------------|------------------------|-----------------------------------------------------------------------------------------------------------|
| `id`      | integer/string | **{check-circle}** Yes | 经过身份验证的用户拥有的项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |

此端点支持[基于偏移量的分页](rest/index.md#offset-based-pagination)。

如果成功，则返回 [`200`](rest/index.md#status-codes) 以及每个项目带有限制字段的项目列表。

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/job_token_scope/allowlist"
```

响应示例：

```json
[
  {
    "id": 4,
    "description": null,
    "name": "Diaspora Client",
    "name_with_namespace": "Diaspora / Diaspora Client",
    "path": "diaspora-client",
    "path_with_namespace": "diaspora/diaspora-client",
    "created_at": "2013-09-30T13:46:02Z",
    "default_branch": "main",
    "tag_list": [
      "example",
      "disapora client"
    ],
    "topics": [
      "example",
      "disapora client"
    ],
    "ssh_url_to_repo": "git@gitlab.example.com:diaspora/diaspora-client.git",
    "http_url_to_repo": "https://gitlab.example.com/diaspora/diaspora-client.git",
    "web_url": "https://gitlab.example.com/diaspora/diaspora-client",
    "avatar_url": "https://gitlab.example.com/uploads/project/avatar/4/uploads/avatar.png",
    "star_count": 0,
    "last_activity_at": "2013-09-30T13:46:02Z",
    "namespace": {
      "id": 2,
      "name": "Diaspora",
      "path": "diaspora",
      "kind": "group",
      "full_path": "diaspora",
      "parent_id": null,
      "avatar_url": null,
      "web_url": "https://gitlab.example.com/diaspora"
    }
  },
  {
    ...
  }
```

## 创建新项目到项目的 CI/CD 作业令牌入站许可名单

将项目添加到项目的 [CI/CD 作业令牌入站许可名单](../ci/jobs/ci_job_token.md#allow-access-to-your-project-with-a-job-token)。

```plaintext
POST /projects/:id/job_token_scope/allowlist
```

支持的参数：

| 参数           | 类型           | 是否必需               | 描述                                                                                                 |
|---------------------|----------------|------------------------|----------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes | 经过身份验证的用户拥有的项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                           |
| `target_project_id` | integer        | **{check-circle}** Yes | 添加到 CI/CD 作业令牌入站许可名单中的项目的 ID |

如果成功，则返回 [`201`](rest/index.md#status-codes) 和以下响应参数：

| 参数           | 类型    | 描述                                                                                                    |
|:--------------------|:--------|:------------------------------------------------------------------------------------------------------|
| `source_project_id` | integer | 添加 CI/CD 作业令牌入站许可名单的项目的 ID |
| `target_project_id` | integer | 添加到源项目入站许可名单的项目的 ID                                                                                   |

请求示例：

```shell
curl --request PATCH \
  --url "https://gitlab.example.com/api/v4/projects/1/job_token_scope" \
  --header 'PRIVATE-TOKEN: <your_access_token>' \
  --header 'Content-Type: application/json' \
  --data '{ "target_project_id": 2 }'
```

响应示例：

```json
{
  "source_project_id": 1,
  "target_project_id": 2
}
```

## 从项目的 CI/CD 作业令牌入站许可名单中移除项目

从项目的 [CI/CD 作业令牌入站许可名单](../ci/jobs/ci_job_token.md#allow-access-to-your-project-with-a-job-token)中移除项目。

```plaintext
DELETE /projects/:id/job_token_scope/allowlist/:target_project_id
```

支持的参数：

| 参数           | 类型           | 是否必需               | 描述                                                                                                             |
|---------------------|----------------|------------------------|----------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | **{check-circle}** Yes | 经过身份验证的用户拥有的项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                                       |
| `target_project_id` | integer        | **{check-circle}** Yes | 从 CI/CD 作业令牌入站许可名单中移除的项目的 ID |

如果成功，则返回 [`204`](rest/index.md#status-codes) 且无响应主体。

请求示例：

```shell

curl --request DELETE \
  --url "https://gitlab.example.com/api/v4/projects/1/job_token_scope/allowlist/2" \
  --header 'PRIVATE-TOKEN: <your_access_token>' \
  --header 'Content-Type: application/json'
```
