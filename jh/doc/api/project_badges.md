---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目徽章 API **(FREE)**

## 占位符标记

[徽章](../user/project/badges.md)支持在链接和图片 URL 中实时替换的占位符。允许的占位符包括：

<!-- vale gitlab.Spelling = NO -->

- **%{project_path}**：替换为项目路径。
- **%{project_title}**：替换为项目标题。
- **%{project_name}**：替换为项目名称。
- **%{project_id}**：替换为项目 ID。
- **%{default_branch}**：替换为项目默认分支。
- **%{commit_sha}**：替换为项目最后一次提交的 SHA。

<!-- vale gitlab.Spelling = YES -->

## 获取项目徽章列表

获取项目及其群组徽章的列表。

```plaintext
GET /projects/:id/badges
```

| 参数       | 类型            | 是否必需 | 描述 |
| --------- | -------------- | ------- | --- |
| `id`      | integer/string | yes     | 经过身份验证的用户所拥有的项目的 ID 或[网址编码路径](rest/index.md#namespaced-path-encoding)。 |
| `name`    | string         | no      | 要返回的徽章的名称（区分大小写）。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/badges?name=Coverage"
```

响应示例：

```json
[
  {
    "name": "Coverage",
    "id": 1,
    "link_url": "http://example.com/ci_status.svg?project=%{project_path}&ref=%{default_branch}",
    "image_url": "https://shields.io/my/badge",
    "rendered_link_url": "http://example.com/ci_status.svg?project=example-org/example-project&ref=master",
    "rendered_image_url": "https://shields.io/my/badge",
    "kind": "project"
  },
  {
    "name": "Pipeline",
    "id": 2,
    "link_url": "http://example.com/ci_status.svg?project=%{project_path}&ref=%{default_branch}",
    "image_url": "https://shields.io/my/badge",
    "rendered_link_url": "http://example.com/ci_status.svg?project=example-org/example-project&ref=master",
    "rendered_image_url": "https://shields.io/my/badge",
    "kind": "group"
  }
]
```

## 获取单个项目徽章

获取单个项目徽章。

```plaintext
GET /projects/:id/badges/:badge_id
```

| 参数        | 类型            | 是否必需 | 描述 |
| ---------  | -------------- | ------- | ---- |
| `id`       | integer/string | yes     | 经过身份验证的用户所拥有的项目的 ID 或[网址编码路径](rest/index.md#namespaced-path-encoding) |
| `badge_id` | integer        | yes     | 徽章 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/badges/:badge_id"
```

响应示例：

```json
{
  "name": "Coverage",
  "id": 1,
  "link_url": "http://example.com/ci_status.svg?project=%{project_path}&ref=%{default_branch}",
  "image_url": "https://shields.io/my/badge",
  "rendered_link_url": "http://example.com/ci_status.svg?project=example-org/example-project&ref=master",
  "rendered_image_url": "https://shields.io/my/badge",
  "kind": "project"
}
```

## 添加项目徽章

添加项目徽章。

```plaintext
POST /projects/:id/badges
```

| 参数         | 类型            | 是否必需 | 描述 |
| ----------- | -------------- | ------- | ---- |
| `id`        | integer/string | yes     | 经过身份验证的用户所拥有的项目的 ID 或[网址编码路径](rest/index.md#namespaced-path-encoding) |
| `link_url`  | string         | yes     | 徽章链接网址 |
| `image_url` | string         | yes     | 徽章图片网址 |
| `name`      | string         | no      | 徽章名称 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --data "link_url=https://gitlab.com/gitlab-org/gitlab-foss/commits/master&image_url=https://shields.io/my/badge1&name=mybadge" \
     "https://gitlab.example.com/api/v4/projects/:id/badges"
```

响应示例：

```json
{
  "id": 1,
  "name": "mybadge",
  "link_url": "https://gitlab.com/gitlab-org/gitlab-foss/commits/master",
  "image_url": "https://shields.io/my/badge1",
  "rendered_link_url": "https://gitlab.com/gitlab-org/gitlab-foss/commits/master",
  "rendered_image_url": "https://shields.io/my/badge1",
  "kind": "project"
}
```

## 更新项目徽章

更新项目徽章。

```plaintext
PUT /projects/:id/badges/:badge_id
```

| 参数         | 类型            | 是否必需 | 描述 |
| ----------- | -------------- | ------- | ---- |
| `id`        | integer/string | yes     | 经过身份验证的用户所拥有的项目的 ID 或[网址编码路径](rest/index.md#namespaced-path-encoding) |
| `badge_id`  | integer        | yes     | 徽章 ID |
| `link_url`  | string         | no      | 徽章链接网址 |
| `image_url` | string         | no      | 徽章图片网址 |
| `name`      | string         | no      | 徽章名称 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/badges/:badge_id"
```

响应示例：

```json
{
  "id": 1,
  "name": "mybadge",
  "link_url": "https://gitlab.com/gitlab-org/gitlab-foss/commits/master",
  "image_url": "https://shields.io/my/badge",
  "rendered_link_url": "https://gitlab.com/gitlab-org/gitlab-foss/commits/master",
  "rendered_image_url": "https://shields.io/my/badge",
  "kind": "project"
}
```

## 移除项目徽章

移除项目徽章。此接口只能删除项目徽章。

```plaintext
DELETE /projects/:id/badges/:badge_id
```

| 参数        | 类型            | 是否必需 | 描述 |
| ---------- | -------------- | ------- | ---- |
| `id`       | integer/string | yes     | 经过身份验证的用户所拥有的项目的 ID 或[网址编码路径](rest/index.md#namespaced-path-encoding) |
| `badge_id` | integer        | yes     | 徽章 ID |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/badges/:badge_id"
```

## 预览项目徽章

返回解析占位符插值后的 `link_url` 和 `image_url` 最终网址样式。

```plaintext
GET /projects/:id/badges/render
```

| 参数         | 类型            | 是否必需 | 描述 |
| ----------- | -------------- | ------- | ---- |
| `id`        | integer/string | yes     | 经过身份验证的用户所拥有的项目的 ID 或[网址编码路径](rest/index.md#namespaced-path-encoding) |
| `link_url`  | string         | yes     | 徽章链接网址|
| `image_url` | string         | yes     | 徽章图片网址 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/:id/badges/render?link_url=http%3A%2F%2Fexample.com%2Fci_status.svg%3Fproject%3D%25%7Bproject_path%7D%26ref%3D%25%7Bdefault_branch%7D&image_url=https%3A%2F%2Fshields.io%2Fmy%2Fbadge"
```

响应示例：

```json
{
  "link_url": "http://example.com/ci_status.svg?project=%{project_path}&ref=%{default_branch}",
  "image_url": "https://shields.io/my/badge",
  "rendered_link_url": "http://example.com/ci_status.svg?project=example-org/example-project&ref=master",
  "rendered_image_url": "https://shields.io/my/badge"
}
```
