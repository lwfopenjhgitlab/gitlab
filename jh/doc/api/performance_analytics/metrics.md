---
stage: Plan
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference, api
---

# 效能分析指标 API **(PREMIUM)**

> 引入于 15.6 版本。

所有方法都至少需要报告者角色权限。

## 获取项目级别效能分析指标

获取项目级别效能分析指标。

```plaintext
GET /projects/:id/performance_analytics/metrics
```


| 参数                  | 类型             | 是否必需   | 描述 |
|:---------------------|:-----------------|:---------|:------------|
| `id`                 | integer/string   | 是       | 通过身份验证的用户可以使用 ID 或[项目 URL 编码路径](../index.md#namespaced-path-encoding)访问。 |
| `metric`             | string           | 是       | 指标名称：`commits`、`issues_created`、`issues_closed`、`merge_requests_created`、`merge_requests_approved`、`merge_requests_merged`、`merge_requests_closed` 或 `notes_created`。 |
| `start_date`         | string           | 否       | 开始的日期范围。ISO 8601 日期格式，例如 `2022-10-01`。 默认为 1 个月前。 |
| `end_date`           | string           | 否       | 结束日期范围。ISO 8601 日期格式，例如 `2022-10-01`。 默认为当前日期。 |
| `user_ids`           | array of integers| 否       | 根据特定用户 ID 过滤结果。 |
| `interval`           | string           | 否       | 时间间隔。`all`、`monthly` 或 `daily`。默认为 `daily`。 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/performance_analytics/metrics?metric=issues_created"
```

响应示例：

```json
[
  { "date": "2022-10-01", "value": 3 },
  { "date": "2022-10-02", "value": 6 },
  { "date": "2022-10-03", "value": 0 },
  { "date": "2022-10-04", "value": 0 },
  { "date": "2022-10-05", "value": 0 },
  { "date": "2022-10-06", "value": 0 },
  { "date": "2022-10-07", "value": 0 },
  { "date": "2022-10-08", "value": 4 }
]
```

## 获取群组级别效能分析指标

获取群组级别效能分析指标。

```plaintext
GET /groups/:id/performance_analytics/metrics
```


| 参数                  | 类型             | 是否必需   | 描述 |
|:---------------------|:-----------------|:---------|:------------|
| `id`                 | integer/string   | 是       | 通过身份验证的用户可以使用 ID 或[项目 URL 编码路径](../index.md#namespaced-path-encoding)访问。 |
| `metric`             | string           | 是       | 指标名称：`pushes`、`issues_created`、`issues_closed`、`merge_requests_created`、`merge_requests_approved`、`merge_requests_merged`、`merge_requests_closed` 或 `notes_created`。 |
| `start_date`         | string           | 否       | 开始的日期范围。ISO 8601 日期格式，例如 `2022-10-01`。 默认为 1 个月前。 |
| `end_date`           | string           | 否       | 结束日期范围。ISO 8601 日期格式，例如 `2022-10-01`。 默认为当前日期。 |
| `project_ids`        | array of integers| 否       | 根据特定项目 ID 过滤结果。 |
| `user_ids`           | array of integers| 否       | 根据特定用户 ID 过滤结果。 |
| `interval`           | string           | 否       | 时间间隔。`all`、`monthly` 或 `daily`。默认为 `daily`。 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/performance_analytics/metrics?metric=issues_created"
```

响应示例：

```json
[
  { "date": "2022-10-01", "value": 3 },
  { "date": "2022-10-02", "value": 6 },
  { "date": "2022-10-03", "value": 0 },
  { "date": "2022-10-04", "value": 0 },
  { "date": "2022-10-05", "value": 0 },
  { "date": "2022-10-06", "value": 0 },
  { "date": "2022-10-07", "value": 0 },
  { "date": "2022-10-08", "value": 4 }
]
```

## `value` 字段

以上项目级别和群组级别的接口，API 响应中 `value` 字段具有不同含义，具体要取决于提供的 `metric` 查询范围：

| `metric` 查询参数          | 响应中 `value` 的含义 |
| ------------------------- | --------------------- |
| `commits`                 | 默认分支的 commit 数量  |
| `pushes`                  | Git 推送数量 |
| `issues_created`          | 议题创建数量 |
| `issues_closed`           | 议题关闭数量 |
| `merge_requests_created`  | 合并请求创建数量 |
| `merge_requests_approved` | 合并请求审核通过数量 |
| `merge_requests_merged`   | 合并请求合并数量 |
| `merge_requests_closed`   | 合并请求关闭数量 |
| `notes_created`           | 评论创建数量 |
