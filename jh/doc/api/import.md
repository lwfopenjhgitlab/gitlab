---
stage: Manage
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---


# 导入 API **(FREE)**

使用导入 API 从 GitHub 或 Bitbucket Server 导入仓库。

<a id="import-repository-from-github"></a>

## 从 GitHub 导入仓库

> - 引入于极狐GitLab 15.8。如果 `target_namespace` 中指定的命名空间或群组名称不存在，则极狐GitLab 不再自动创建命名空间或群组。如果命名空间或群组名称被占用或 `target_namespace` 为空，则极狐GitLab 也不再使用用户的个人命名空间。
> - 要求维护者角色而不是开发者角色引入于极狐GitLab 16.0，并向后移植到 15.11.1 和 15.10.5。
> `optional_stages` 中的 `collaborators_import` key 引入于极狐GitLab 16.0。

使用 API 将您的项目从 GitHub 导入到极狐GitLab。

先决条件：

- [GitHub 导入先决条件](../user/project/import/github.md#prerequisites)。
- `target_namespace` 中设置的命名空间必须存在。
- 命名空间可以是您的命名空间，也可以是您在其中至少具有维护者角色的现存群组。

```plaintext
POST /import/github
```

| 参数 | 类型      | 是否必需 | 描述                                                                                      |
|------------|---------|------|-----------------------------------------------------------------------------------------|
| `personal_access_token`       | string  | 是    | GitHub 个人访问令牌                                                                           |
| `repo_id`   | integer | 是    | GitHub 仓库 ID                                                                            |
| `new_name`   | string  | 否    | 新仓库名称                                                                                   |
| `target_namespace`   | string  | 是    | 导入仓库的命名空间。支持子群组，比如 `/namespace/subgroup`。在极狐GitLab 15.8 及更高版本中，不可为空                     |
| `github_hostname`   | string  | 否    | 企业定制的 GitHub 访问地址。如果是 GitHub.com，则无需设置                                                  |
| `optional_stages`  | object  | 否    | [要导入的其他项目](../user/project/import/github.md#select-additional-items-to-import)。引入于 15.5 |

```shell
curl --request POST \
  --url "https://gitlab.example.com/api/v4/import/github" \
  --header "content-type: application/json" \
  --header "PRIVATE-TOKEN: <your_access_token>" \
  --data '{
    "personal_access_token": "aBc123abC12aBc123abC12abC123+_A/c123",
    "repo_id": "12345",
    "target_namespace": "group/subgroup",
    "new_name": "NEW-NAME",
    "github_hostname": "https://github.example.com",
    "optional_stages": {
      "single_endpoint_issue_events_import": true,
      "single_endpoint_notes_import": true,
      "attachments_import": true,
      "collaborators_import": true
    }
}'
```

以下 key 可用于 `optional_stages`：

- `single_endpoint_issue_events_import`：用于议题和拉取请求事件导入。
- `single_endpoint_notes_import`：用于替代和更彻底的评论导入。
- `attachments_import`：用于 Markdown 附件导入。
- `collaborators_import`，用于导入不是外部合作者的直接仓库合作者。


有关详细信息，请参阅[选择要导入的其他项目](../user/project/import/github.md#select-additional-items-to-import)。

响应示例：

```json
{
    "id": 27,
    "name": "my-repo",
    "full_path": "/root/my-repo",
    "full_name": "Administrator / my-repo"
}
```

### 使用群组访问令牌通过 API 导入公共项目

> 引入于极狐GitLab 15.7，项目在任何情况下都不会导入到 [bot 用户](../user/group/settings/group_access_tokens.md#bot-users-for-groups)的命名空间中。导入到 bot 用户命名空间的项目无法被具有有效令牌的用户删除，这存在安全风险。

当您使用群组访问令牌通过 API 将项目从 GitHub 导入到极狐GitLab 时：

- 极狐GitLab 项目继承了原项目的可见性设置。因此，如果原项目是公开的，则可以公开访问该项目。
- 如果 `path` 或 `target_namespace` 不存在，项目导入会失败。

### 取消 GitHub 项目导入

> 引入于极狐GitLab 15.5。

使用 API 取消正在进行的 GitHub 项目导入。

```plaintext
POST /import/github/cancel
```

| 参数           | 类型      | 是否必需 | 描述             |
|--------------|---------|------|----------------|
| `project_id` | integer | yes  | 极狐GitLab 项目 ID |

```shell
curl --request POST \
  --url "https://gitlab.example.com/api/v4/import/github/cancel" \
  --header "content-type: application/json" \
  --header "PRIVATE-TOKEN: <your_access_token>" \
  --data '{
    "project_id": 12345
}'
```

响应示例：

```json
{
    "id": 160,
    "name": "my-repo",
    "full_path": "/root/my-repo",
    "full_name": "Administrator / my-repo",
    "import_source": "source/source-repo",
    "import_status": "canceled",
    "human_import_status_name": "canceled",
    "provider_link": "/source/source-repo"
}
```

返回以下状态码：

- `200 OK`：正在取消项目导入
- `400 Bad Request`：无法取消项目导入
- `404 Not Found`：与 `project_id` 关联的项目不存在

### 将 GitHub gists 导入极狐GitLab 代码片段

> - 引入于极狐GitLab 15.8，[功能标志](../administration/feature_flags.md)为 `github_import_gists`。默认禁用。在 JiHuLab.com 上启用。
> - 在私有化部署版本上启用于极狐GitLab 15.10。
> - 广泛可用于极狐GitLab 15.11。移除 `github_import_gists` 功能标志。

您可以使用极狐GitLab API 将个人 GitHub gist（最多 10 个文件）导入到个人极狐GitLab 代码片段。
超过 10 个文件的 GitHub gist 将被跳过。您应该手动迁移这些 GitHub gist。

如果无法导入任何 gist，则会发送一封电子邮件，其中包含未导入的 gist 列表。

```plaintext
POST /import/github/gists
```

| 参数                      | 类型     | 是否必需 | 描述            |
|-------------------------|--------|------|---------------|
| `personal_access_token` | string | yes  | GitHub 个人访问令牌 |

```shell
curl --request POST \
  --url "https://gitlab.example.com/api/v4/import/github/gists" \
  --header "content-type: application/json" \
  --header "PRIVATE-TOKEN: <your_gitlab_access_token>" \
  --data '{
    "personal_access_token": "<your_github_personal_access_token>"
}'
```

返回以下状态码：

- `202 Accepted`：开始 gist 导入。
- `401 Unauthorized`：用户的 GitHub 个人访问令牌无效。
- `422 Unprocessable Entity`：gist 导入已经在进行中。
- `429 Too Many Requests`：用户已超出 GitHub 的速率限制。

<a id="import-repository-from-bitbucket-server"></a>

## 从 Bitbucket 服务器导入仓库

使用 API 将项目从 Bitbucket 服务器导入极狐GitLab。

NOTE:
Bitbucket 项目密钥仅被用来在 Bitbucket 中查找仓库。如果你想将仓库导入极狐GitLab 群组，那么您必须要指定 `target_namespace`。如果您没有指定 `target_namespace`，项目将会被导入您个人的用户命名空间下。

先决条件：

- 更多信息请参见[Bitbucket Server importer 先决条件](../user/project/import/bitbucket_server.md#import-your-bitbucket-repositories)。

```plaintext
POST /import/bitbucket_server
```

| 参数 | 类型           | 是否必需 | 描述                              |
|------------|---------|----------|---------------------|
| `bitbucket_server_url` | string | 是 | Bitbucket 服务器连接 |
| `bitbucket_server_username` | string | 是 | Bitbucket 服务器用户名 |
| `personal_access_token` | string | 是 | Bitbucket 服务器个人访问令牌/密码 |
| `bitbucket_server_project` | string | 是 | Bitbucket 项目密钥 |
| `bitbucket_server_repo` | string | 是 | Bitbucket 仓库名称 |
| `new_name` | string | 否 | 新仓库名称 |
| `target_namespace` | string | 否 | 项目导入的命名空间。支持子群组，比如 `/namespace/subgroup` |

```shell
curl --request POST \
  --url "https://gitlab.example.com/api/v4/import/bitbucket_server" \
  --header "content-type: application/json" \
  --header "PRIVATE-TOKEN: <your_access_token>" \
  --data '{
    "bitbucket_server_url": "http://bitbucket.example.com",
    "bitbucket_server_username": "root",
    "personal_access_token": "Nzk4MDcxODY4MDAyOiP8y410zF3tGAyLnHRv/E0+3xYs",
    "bitbucket_server_project": "NEW",
    "bitbucket_server_repo": "my-repo"
}'
```

<!--
## 群组和项目的自动导入 **(PREMIUM)**

更多关于用户、群组、项目自动导入的 API 调用信息，请查看[群组和项目的自动导入](../user/project/import/index.md#automate-group-and-project-import)。
-->

## 相关主题

- [通过直接传输 API 进行群组迁移](bulk_imports.md)
- [群组导入导出 API](group_import_export.md)
- [项目导入导出 API](project_import_export.md)
