---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
type: reference, api
---

# 项目统计数据 API **(FREE)**

每一次对[项目](../user/project/index.md)统计数据的 API 调用都必须通过认证。  
获取这些统计数据需要代码仓库的写入权限。  

这个 API 是统计通过 HTTP 克隆或拉取项目的次数，不包括通过 SSH 拉取项目的次数。  

## 获取过去 30 天的数据

```plaintext
GET /projects/:id/statistics
```

| 参数 | 类型 | 是否必需 | 描述 |
| ---------- | ------ | -------- | ----------- |
| `id`      | integer or string | yes      | ID 或[项目的 URL 路径](rest/index.md#namespaced-path-encoding) |

响应示例：

```json
{
  "fetches": {
    "total": 50,
    "days": [
      {
        "count": 10,
        "date": "2018-01-10"
      },
      {
        "count": 10,
        "date": "2018-01-09"
      },
      {
        "count": 10,
        "date": "2018-01-08"
      },
      {
        "count": 10,
        "date": "2018-01-07"
      },
      {
        "count": 10,
        "date": "2018-01-06"
      }
    ]
  }
}
```
