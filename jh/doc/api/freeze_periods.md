---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: concepts, howto
---

# 冻结周期 API **(FREE)**

> 引入于 13.0 版本。

您可以使用冻结周期 API 来操作极狐GitLab [冻结周期](../user/project/releases/index.md#prevent-unintentional-releases-by-setting-a-deploy-freeze)条目。

## 权限和安全

具有报告者[权限](../user/permissions.md)或更高权限的用户可以阅读冻结周期 API 端点。只有具有维护者角色的用户才能修改冻结周期。

## 冻结周期列表

冻结周期的分页列表，按 `created_at` 升序排序。

```plaintext
GET /projects/:id/freeze_periods
```

| 参数 | 类型 | 是否必需 | 描述 |
| ------------- | -------------- | -------- | ----------------------------------------------------------------------------------- |
| `id`          | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/19/freeze_periods"
```

响应示例：

```json
[
   {
      "id":1,
      "freeze_start":"0 23 * * 5",
      "freeze_end":"0 8 * * 1",
      "cron_timezone":"UTC",
      "created_at":"2020-05-15T17:03:35.702Z",
      "updated_at":"2020-05-15T17:06:41.566Z"
   }
]
```

## 通过 `freeze_period_id` 获取冻结周期

根据给定的 `freeze_period_id` 获取冻结周期。

```plaintext
GET /projects/:id/freeze_periods/:freeze_period_id
```

| 参数 | 类型                | 是否必需 | 描述 |
| ------------- |-------------------| -------- | ----------------------------------------------------------------------------------- |
| `id`          | integer or string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `freeze_period_id`    | integer           | yes      | 冻结周期的 ID。                                   |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/19/freeze_periods/1"
```

响应示例：

```json
{
   "id":1,
   "freeze_start":"0 23 * * 5",
   "freeze_end":"0 8 * * 1",
   "cron_timezone":"UTC",
   "created_at":"2020-05-15T17:03:35.702Z",
   "updated_at":"2020-05-15T17:06:41.566Z"
}
```

## 创建冻结周期

创建冻结周期。

```plaintext
POST /projects/:id/freeze_periods
```

| 参数 | 类型 | 是否必需 | 描述 |
| -------------------| --------------- | --------                    | ------------------------------------------------------------------------------------------------------------------------------ |
| `id`               | integer or string  | yes                         | 项目的 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding).                                              |
| `freeze_start`     | string          | yes                         | [定时任务](https://crontab.guru/)格式的冻结周期开始时间。                                                              |
| `freeze_end`       | string          | yes                         | [定时任务](https://crontab.guru/)格式的冻结周期结束时间。                                                               |
| `cron_timezone`    | string          | no                          | 定时任务字段的时区，如果没有提供，默认为 UTC。                                                               |

请求示例：

```shell
curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: <your_access_token>" \
     --data '{ "freeze_start": "0 23 * * 5", "freeze_end": "0 7 * * 1", "cron_timezone": "UTC" }' \
     --request POST "https://gitlab.example.com/api/v4/projects/19/freeze_periods"
```

响应示例：

```json
{
   "id":1,
   "freeze_start":"0 23 * * 5",
   "freeze_end":"0 7 * * 1",
   "cron_timezone":"UTC",
   "created_at":"2020-05-15T17:03:35.702Z",
   "updated_at":"2020-05-15T17:03:35.702Z"
}
```

## 更新冻结周期

根据给定的 `freeze_period_id` 更新冻结周期。

```plaintext
PUT /projects/:id/freeze_periods/:freeze_period_id
```

| 参数 | 类型 | 是否必需 | 描述 |
| ------------- | --------------- | -------- | ----------------------------------------------------------------------------------------------------------- |
| `id`          | integer or string  | yes      | 项目的 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding)。                         |
| `freeze_period_id`    | integer          | yes      | 冻结周期的 ID。                                                              |
| `freeze_start`     | string          | no                         | [定时任务](https://crontab.guru/) 格式的冻结周期开始时间。                                                              |
| `freeze_end`       | string          | no                         | [定时任务](https://crontab.guru/) 格式的冻结周期结束时间。                                                                |
| `cron_timezone`    | string          | no                          | 定时任务字段的时区。                                                               |

请求示例：

```shell
curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: <your_access_token>" \
     --data '{ "freeze_end": "0 8 * * 1" }' \
     --request PUT "https://gitlab.example.com/api/v4/projects/19/freeze_periods/1"
```

响应示例：

```json
{
   "id":1,
   "freeze_start":"0 23 * * 5",
   "freeze_end":"0 8 * * 1",
   "cron_timezone":"UTC",
   "created_at":"2020-05-15T17:03:35.702Z",
   "updated_at":"2020-05-15T17:06:41.566Z"
}
```

## 删除冻结周期

根据给定的 `freeze_period_id` 删除冻结周期。

```plaintext
DELETE /projects/:id/freeze_periods/:freeze_period_id
```

| 参数 | 类型                | 是否必需 | 描述 |
| ------------- |-------------------| -------- | ----------------------------------------------------------------------------------- |
| `id`          | integer or string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `freeze_period_id`    | integer           | yes      | 冻结周期的 ID。                                     |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/19/freeze_periods/1"

```
