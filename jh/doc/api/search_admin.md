---
stage: Data Stores
group: Global Search
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 搜索管理 API API **(PREMIUM SELF)**

> 引入于极狐GitLab 16.1。

搜索管理 API 返回有关[高级搜索迁移](../integration/advanced_search/elasticsearch.md#advanced-search-migrations)的信息。

您必须具有管理员权限才能使用此 API。

## 列出所有高级搜索迁移

获取极狐GitLab 实例的所有高级搜索迁移的列表。

```plaintext
GET /admin/search/migrations
```

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/admin/search/migrations"
```

响应示例：

```json
[
  {
    "version": 20230427555555,
    "name": "BackfillHiddenOnMergeRequests",
    "started_at": "2023-05-12T01:35:05.469+00:00",
    "completed_at": "2023-05-12T01:36:06.432+00:00",
    "completed": true,
    "obsolete": false,
    "migration_state": {}
  },
  {
    "version": 20230428500000,
    "name": "AddSuffixProjectInWikiRid",
    "started_at": "2023-05-04T18:59:43.542+00:00",
    "completed_at": "2023-05-04T18:59:43.542+00:00",
    "completed": false,
    "obsolete": false,
    "migration_state": {
      "pause_indexing": true,
      "slice": 1,
      "task_id": null,
      "max_slices": 5,
      "retry_attempt": 0
    }
  },
  {
    "version": 20230503064300,
    "name": "BackfillProjectPermissionsInBlobsUsingPermutations",
    "started_at": "2023-05-03T16:04:44.074+00:00",
    "completed_at": "2023-05-03T16:04:44.074+00:00",
    "completed": true,
    "obsolete": false,
    "migration_state": {
      "permutation_idx": 8,
      "documents_remaining": 5,
      "task_id": "I2_LXc-xQlOeu-KmjYpM8g:172820",
      "documents_remaining_for_permutation": 0
    }
  }
]
```

## 获得高级搜索迁移

通过提供迁移版本或名称来获取单个高级搜索迁移。

```plaintext
GET /admin/search/mirations/:version_or_name
```

参数：

| 参数                | 类型             | 是否必需 | 描述       |
|-------------------|----------------|------|----------|
| `version_or_name` | integer/string | Yes  | 迁移的版本或名称 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/admin/search/mirations/20230503064300"
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/admin/search/mirations/BackfillProjectPermissionsInBlobsUsingPermutations"
```

如果成功，则返回 [`200`](rest/index.md#status-codes) 和以下响应参数：

| 参数                | 类型       | 描述                     |
|:------------------|:---------|:-----------------------|
| `version`         | integer  | 迁移的版本                  |
| `name`            | string   | 迁移的名称                  |
| `started_at`      | datetime | 迁移的开始时间                |
| `completed_at`    | datetime | 迁移的完成时间                |
| `completed`       | boolean  | 如果为 `true`，则迁移完成       |
| `obsolete`        | boolean  | 如果为 `true`，则迁移已过期      |
| `migration_state` | object   | 存储的迁移状态 |

响应示例：

```json
{
  "version": 20230503064300,
  "name": "BackfillProjectPermissionsInBlobsUsingPermutations",
  "started_at": "2023-05-03T16:04:44.074+00:00",
  "completed_at": "2023-05-03T16:04:44.074+00:00",
  "completed": true,
  "obsolete": false,
  "migration_state": {
    "permutation_idx": 8,
    "documents_remaining": 5,
    "task_id": "I2_LXc-xQlOeu-KmjYpM8g:172820",
    "documents_remaining_for_permutation": 0
  }
}
```
