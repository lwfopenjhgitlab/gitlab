---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
type: reference, api
---

# 项目别名 API **(PREMIUM SELF)**

所有的方法都需要管理员权限。

## 列出所有的项目别名

获取所有的项目别名：

```plaintext
GET /project_aliases
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/project_aliases"
```

响应示例：

```json
[
  {
    "id": 1,
    "project_id": 1,
    "name": "gitlab-foss"
  },
  {
    "id": 2,
    "project_id": 2,
    "name": "gitlab"
  }
]
```

## 获取项目别名的详细信息

获取项目别名的详细信息：

```plaintext
GET /project_aliases/:name
```

| 参数 | 类型    | 是否必需 | 描述         |
|-----------|--------|----------|-----------------------|
| `name`    | string | **{check-circle}** Yes      | 别名的名字 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/project_aliases/gitlab"
```

响应样例:

```json
{
  "id": 1,
  "project_id": 1,
  "name": "gitlab"
}
```

## 创建项目别名

为某项目添加一个别名。成功会返回 `201 Created`。失败则会按照错误的不同来返回错误码，例如：别名已经存在，返回 `400 Bad Request`：

```plaintext
POST /project_aliases
```

| 参数 | 类型    | 是否必需 | 描述      |
|--------------|----------------|----------|---------|
| `project_id` | integer or string | **{check-circle}** Yes      | 项目路径 ID   |
| `name`       | string         | **{check-circle}** Yes      | 别名且必须唯一 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/project_aliases" --form "project_id=1" --form "name=gitlab"
```

或

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/project_aliases" --form "project_id=gitlab-org/gitlab" --form "name=gitlab"
```

响应示例：

```json
{
  "id": 1,
  "project_id": 1,
  "name": "gitlab"
}
```

## 删除项目别名

删除项目别名，成功返回 `204 No Content`, 如果没有该别名，则返回`404 Not Found`：

```plaintext
DELETE /project_aliases/:name
```

| 参数 | 类型    | 是否必需 | 描述         |
|-----------|--------|----------|-----------------------|
| `name`    | string | **{check-circle}** Yes      | 别名的名字 |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/project_aliases/gitlab"
```
