---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: concepts, howto
---

# 部署 API **(FREE)**

## 获取项目部署列表

获取项目的部署列表。

```plaintext
GET /projects/:id/deployments
```

| 参数 | 类型 | 是否必需 | 描述                                                                                        |
|------------------|----------------|------|-------------------------------------------------------------------------------------------|
| `id`             | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding)。                       |
| `order_by`       | string         | no   | 返回按 `id`、 `iid`、 `created_at`、 `updated_at`、`finished_at` 或 `ref` 字段之一排序的部署。默认按照 `id` 排序。 |
| `sort`           | string         | no   | 返回按照 `asc` 或 `desc` 顺序排序的部署。 默认顺序为 `asc`。                                                 |
| `updated_after`  | datetime       | no   | 返回在指定日期后更新的部署。需要按照 ISO 8601 格式传递参数： (`2019-03-15T08:00:00Z`)。                             |
| `updated_before` | datetime       | no   | 返回在指定日期前更新的部署。需要按照 ISO 8601 格式传递参数： (`2019-03-15T08:00:00Z`)。                             |
| `finished_after` | datetime       | no   | 返回指定日期后完成的部署。预计采用 ISO 8601 格式 (`2019-03-15T08:00:00Z`)。                                   |
| `finished_before`| datetime       | no   | 返回指定日期前完成的部署。预计采用 ISO 8601 格式 (`2019-03-15T08:00:00Z`)。                                   |
| `environment`    | string         | no   | 用于过滤部署的 [环境名称](../ci/environments/index.md)。                                              |
| `status`         | string         | no   | 部署的状态，用于过滤部署，可选值为 `created`、 `running`、 `success`、 `failed`、 `canceled` 或 `blocked`。      

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/deployments"
```

NOTE:
使用 `finished_before` 或 `finished_after` 时，您应该将 `order_by` 指定为 `finished_at` 且将 `status` 指定为 `success`。


响应示例：

```json
[
  {
    "created_at": "2016-08-11T07:36:40.222Z",
    "updated_at": "2016-08-11T07:38:12.414Z",
    "status": "created",
    "deployable": {
      "commit": {
        "author_email": "admin@example.com",
        "author_name": "Administrator",
        "created_at": "2016-08-11T09:36:01.000+02:00",
        "id": "99d03678b90d914dbb1b109132516d71a4a03ea8",
        "message": "Merge branch 'new-title' into 'main'\r\n\r\nUpdate README\r\n\r\n\r\n\r\nSee merge request !1",
        "short_id": "99d03678",
        "title": "Merge branch 'new-title' into 'main'\r"
      },
      "coverage": null,
      "created_at": "2016-08-11T07:36:27.357Z",
      "finished_at": "2016-08-11T07:36:39.851Z",
      "id": 657,
      "name": "deploy",
      "ref": "main",
      "runner": null,
      "stage": "deploy",
      "started_at": null,
      "status": "success",
      "tag": false,
      "project": {
        "ci_job_token_scope_enabled": false
      },
      "user": {
        "id": 1,
        "name": "Administrator",
        "username": "root",
        "state": "active",
        "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
        "web_url": "http://gitlab.dev/root",
        "created_at": "2015-12-21T13:14:24.077Z",
        "bio": null,
        "location": null,
        "public_email": "",
        "skype": "",
        "linkedin": "",
        "twitter": "",
        "website_url": "",
        "organization": ""
      },
      "pipeline": {
        "created_at": "2016-08-11T02:12:10.222Z",
        "id": 36,
        "ref": "main",
        "sha": "99d03678b90d914dbb1b109132516d71a4a03ea8",
        "status": "success",
        "updated_at": "2016-08-11T02:12:10.222Z",
        "web_url": "http://gitlab.dev/root/project/pipelines/12"
      }
    },
    "environment": {
      "external_url": "https://about.gitlab.com",
      "id": 9,
      "name": "production"
    },
    "id": 41,
    "iid": 1,
    "ref": "main",
    "sha": "99d03678b90d914dbb1b109132516d71a4a03ea8",
    "user": {
      "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "id": 1,
      "name": "Administrator",
      "state": "active",
      "username": "root",
      "web_url": "http://localhost:3000/root"
    }
  },
  {
    "created_at": "2016-08-11T11:32:35.444Z",
    "updated_at": "2016-08-11T11:34:01.123Z",
    "status": "created",
    "deployable": {
      "commit": {
        "author_email": "admin@example.com",
        "author_name": "Administrator",
        "created_at": "2016-08-11T13:28:26.000+02:00",
        "id": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
        "message": "Merge branch 'rename-readme' into 'main'\r\n\r\nRename README\r\n\r\n\r\n\r\nSee merge request !2",
        "short_id": "a91957a8",
        "title": "Merge branch 'rename-readme' into 'main'\r"
      },
      "coverage": null,
      "created_at": "2016-08-11T11:32:24.456Z",
      "finished_at": "2016-08-11T11:32:35.145Z",
      "id": 664,
      "name": "deploy",
      "ref": "main",
      "runner": null,
      "stage": "deploy",
      "started_at": null,
      "status": "success",
      "tag": false,
      "project": {
        "ci_job_token_scope_enabled": false
      },
      "user": {
        "id": 1,
        "name": "Administrator",
        "username": "root",
        "state": "active",
        "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
        "web_url": "http://gitlab.dev/root",
        "created_at": "2015-12-21T13:14:24.077Z",
        "bio": null,
        "location": null,
        "public_email": "",
        "skype": "",
        "linkedin": "",
        "twitter": "",
        "website_url": "",
        "organization": ""
      },
      "pipeline": {
        "created_at": "2016-08-11T07:43:52.143Z",
        "id": 37,
        "ref": "main",
        "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
        "status": "success",
        "updated_at": "2016-08-11T07:43:52.143Z",
        "web_url": "http://gitlab.dev/root/project/pipelines/13"
      }
    },
    "environment": {
      "external_url": "https://about.gitlab.com",
      "id": 9,
      "name": "production"
    },
    "id": 42,
    "iid": 2,
    "ref": "main",
    "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
    "user": {
      "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "id": 1,
      "name": "Administrator",
      "state": "active",
      "username": "root",
      "web_url": "http://localhost:3000/root"
    }
  }
]
```

## 获得一个指定的部署

```plaintext
GET /projects/:id/deployments/:deployment_id
```

| 参数 | 类型 | 是否必需 | 描述 |
|-----------|---------|----------|------------------------------------------------------------------|
| `id`      | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `deployment_id` | integer | yes      | 部署的 ID                                                           |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/deployments/1"
```

响应示例：

```json
{
  "id": 42,
  "iid": 2,
  "ref": "main",
  "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
  "created_at": "2016-08-11T11:32:35.444Z",
  "updated_at": "2016-08-11T11:34:01.123Z",
  "status": "success",
  "user": {
    "name": "Administrator",
    "username": "root",
    "id": 1,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://localhost:3000/root"
  },
  "environment": {
    "id": 9,
    "name": "production",
    "external_url": "https://about.gitlab.com"
  },
  "deployable": {
    "id": 664,
    "status": "success",
    "stage": "deploy",
    "name": "deploy",
    "ref": "main",
    "tag": false,
    "coverage": null,
    "created_at": "2016-08-11T11:32:24.456Z",
    "started_at": null,
    "finished_at": "2016-08-11T11:32:35.145Z",
    "project": {
      "ci_job_token_scope_enabled": false
    },
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.dev/root",
      "created_at": "2015-12-21T13:14:24.077Z",
      "bio": null,
      "location": null,
      "skype": "",
      "linkedin": "",
      "twitter": "",
      "website_url": "",
      "organization": ""
    },
    "commit": {
      "id": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
      "short_id": "a91957a8",
      "title": "Merge branch 'rename-readme' into 'main'\r",
      "author_name": "Administrator",
      "author_email": "admin@example.com",
      "created_at": "2016-08-11T13:28:26.000+02:00",
      "message": "Merge branch 'rename-readme' into 'main'\r\n\r\nRename README\r\n\r\n\r\n\r\nSee merge request !2"
    },
    "pipeline": {
      "created_at": "2016-08-11T07:43:52.143Z",
      "id": 42,
      "ref": "main",
      "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
      "status": "success",
      "updated_at": "2016-08-11T07:43:52.143Z",
      "web_url": "http://gitlab.dev/root/project/pipelines/5"
    },
    "runner": null
  }
}
```
配置[统一审批设置](../ci/environments/deployment_approvals.md#unified-approval-setting-deprecated)时，用户在极狐Gitlab 专业版或旗舰版创建的部署包含 `approvals` 和 `pending_approval_count` 参数：

```json
{
  "status": "created",
  "pending_approval_count": 0,
  "approvals": [
    {
      "user": {
        "id": 49,
        "username": "project_6_bot",
        "name": "****",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/e83ac685f68ea07553ad3054c738c709?s=80&d=identicon",
        "web_url": "http://localhost:3000/project_6_bot"
      },
      "status": "approved",
      "created_at": "2022-02-24T20:22:30.097Z",
      "comment": "Looks good to me"
    }
  ],
  ...
}
```

当[多重审批规则](../ci/environments/deployment_approvals.md#multiple-approval-rules)被配置时, 用户在极狐Gitlab 专业版或旗舰版创建的部署包含 `approval_summary` 参数：

```json
{
  "approval_summary": {
    "rules": [
      {
        "user_id": null,
        "group_id": 134,
        "access_level": null,
        "access_level_description": "qa-group",
        "required_approvals": 1,
        "deployment_approvals": []
      },
      {
        "user_id": null,
        "group_id": 135,
        "access_level": null,
        "access_level_description": "security-group",
        "required_approvals": 2,
        "deployment_approvals": [
          {
            "user": {
              "id": 100,
              "username": "security-user-1",
              "name": "security user-1",
              "state": "active",
              "avatar_url": "https://www.gravatar.com/avatar/e130fcd3a1681f41a3de69d10841afa9?s=80&d=identicon",
              "web_url": "http://localhost:3000/security-user-1"
            },
            "status": "approved",
            "created_at": "2022-04-11T03:37:03.058Z",
            "comment": null
          }
        ]
      }
    ]
  }
  ...
}
```

## 创建一个部署

```plaintext
POST /projects/:id/deployments
```

| 参数 | 类型 | 是否必需 | 描述 |
|---------------|----------------|----------|---------------------------------------------------------------------------------|
| `id`          | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。             |
| `environment` | string         | yes      | 要为其创建部署的[环境名称](../ci/environments/index.md)。                                   |
| `sha`         | string         | yes      | 已部署的提交的 SHA。                                                                    |
| `ref`         | string         | yes      | 已部署的分支或标签的名字。                                                                   |
| `tag`         | boolean        | yes      | 一个布尔值，表明被部署的 ref 是一个标签 (`true`) 还是不是一个标签 (`false`)。                             |
| `status`      | string         | no       | 用于过滤部署的部署状态值。可选值为 `created`、 `running`、 `success`、 `failed`、 或 `canceled` 其中之一。 |

```shell
curl --data "environment=production&sha=a91957a858320c0e17f3a0eca7cfacbff50ea29a&ref=main&tag=false&status=success" \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/deployments"
```

响应示例：

```json
{
  "id": 42,
  "iid": 2,
  "ref": "main",
  "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
  "created_at": "2016-08-11T11:32:35.444Z",
  "status": "success",
  "user": {
    "name": "Administrator",
    "username": "root",
    "id": 1,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://localhost:3000/root"
  },
  "environment": {
    "id": 9,
    "name": "production",
    "external_url": "https://about.gitlab.com"
  },
  "deployable": null
}
```

用户在极狐GitLab 专业版或旗舰版上创建的部署包括 `approvals` 和 `pending_approval_count` 参数：

```json
{
  "status": "created",
  "pending_approval_count": 0,
  "approvals": [],
  ...
}
```

## 更新部署

```plaintext
PUT /projects/:id/deployments/:deployment_id
```

| 参数 | 类型 | 是否必需 | 描述                                                              |
|------------------|----------------|------|-----------------------------------------------------------------|
| `id`             | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `deployment_id`  | integer        | yes  | 待更新的部署的 ID                                                      |
| `status`         | string         | yes  | 部署的新状态。可选值为 `running`、 `success`、 `failed`、 或 `canceled`。   |

```shell
curl --request PUT --data "status=success" --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/deployments/42"
```

响应示例：

```json
{
  "id": 42,
  "iid": 2,
  "ref": "main",
  "sha": "a91957a858320c0e17f3a0eca7cfacbff50ea29a",
  "created_at": "2016-08-11T11:32:35.444Z",
  "status": "success",
  "user": {
    "name": "Administrator",
    "username": "root",
    "id": 1,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://localhost:3000/root"
  },
  "environment": {
    "id": 9,
    "name": "production",
    "external_url": "https://about.gitlab.com"
  },
  "deployable": null
}
```

用户在极狐GitLab 专业版或旗舰版上创建的部署包括 `approvals` 和 `pending_approval_count` 参数：

```json
{
  "status": "created",
  "pending_approval_count": 0,
  "approvals": [
    {
      "user": {
        "id": 49,
        "username": "project_6_bot",
        "name": "****",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/e83ac685f68ea07553ad3054c738c709?s=80&d=identicon",
        "web_url": "http://localhost:3000/project_6_bot"
      },
      "status": "approved",
      "created_at": "2022-02-24T20:22:30.097Z",
      "comment": "Looks good to me"
    }
  ],
  ...
}
```

## 删除特定部署

删除当前不是环境的最新部署或处于 `running` 状态的特定部署。

```plaintext
DELETE /projects/:id/deployments/:deployment_id
```

| 参数              | 类型             | 是否必需 | 描述                                                           |
|-----------------|----------------|------|--------------------------------------------------------------|
| `id`            | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `deployment_id` | integer        | yes  | 部署 ID                                      |

```shell
curl --request "DELETE" --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/deployments/1"
```

响应示例：

```json
{ "message": "204 Deployment destroyed" }
```

```json
{ "message": "403 Forbidden" }
```

```json
{ "message": "400 Cannot destroy running deployment" }
```

```json
{ "message": "400 Deployment currently deployed to environment" }
```

## 获取与部署关联的合并请求列表

> 引入于极狐Gitlab 12.7 版本。

NOTE:
并非所有部署都可以与合并请求相关联。
若想了解详细信息，请查看
[跟踪部署到环境中的合并请求](../ci/environments/index.md#track-newly-included-merge-requests-per-deployment)。

此 API 检索给定部署附带的合并请求列表:

```plaintext
GET /projects/:id/deployments/:deployment_id/merge_requests
```

它支持与[合并请求 API](merge_requests.md#list-merge-requests) 相同的参数，并使用相同的格式返回响应：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/deployments/42/merge_requests"
```

## 批准或拒绝被阻止的部署 **(PREMIUM)**

> - 引入于极狐Gitlab 14.7，[功能标记](../administration/feature_flags.md)为 `deployment_approvals`。默认禁用。
> - 在极狐Gitlab 14.8 中，功能标记已删除。

若想了解更多信息，请阅读[部署审批](../ci/environments/deployment_approvals.md)。

```plaintext
POST /projects/:id/deployments/:deployment_id/approval
```

| 参数 | 类型 | 是否必需 | 描述 |
|-----------------|----------------|----------|------------------------------------------------------------------------------------------------------|
| `id`            | integer/string | yes      | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。                                    |
| `deployment_id` | integer        | yes      | 部署的 ID。                                                                                              |
| `status`        | string         | yes      | 审批的状态 (`approved` 或 `rejected`)。                                                                     |
| `comment`       | string         | no       | 批准的评论。                                                                                               |
| `represented_as`| string         | no       | 当用户属于[多个审批规则](../ci/environments/deployment_approvals.md#multiple-approval-rules)时，用于审批的用户/组/角色的名称。 |

```shell
curl --data "status=approved&comment=Looks good to me&represented_as=security" \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/deployments/1/approval"
```

响应示例：

```json
{
  "user": {
    "id": 100,
    "username": "security-user-1",
    "name": "security user-1",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/e130fcd3a1681f41a3de69d10841afa9?s=80&d=identicon",
    "web_url": "http://localhost:3000/security-user-1"
  },
  "status": "approved",
  "created_at": "2022-02-24T20:22:30.097Z",
  "comment":"Looks good to me"
}
```
