---
stage: Plan
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference, api
---

# DevOps 研究和评估 (DORA) 关键指标 API **(ULTIMATE)**

> - 引入于 13.10 版本。
> - 在 14.0 版本中，旧的键/值对 `{ "<date>" => "<value>" }` 已经从参数中删除。
> - 在 14.9 版本中，引入了 `time_to_restore_service`。

<!--You can also retrieve [DORA metrics](../../user/analytics/dora_metrics.md) with the [GraphQL API](../../api/graphql/reference/index.md).-->

所有方法都至少需要报告者角色权限。

## 获取项目级别 DORA 指标

获取项目级别 DORA 指标。

```plaintext
GET /projects/:id/dora/metrics
```

| 参数                  | 类型           | 是否必需 | 描述                                                                                                                   |
|---------------------|--------        |------|----------------------------------------------------------------------------------------------------------------------|
| `id`                | integer/string | 是    | 通过身份验证用户可以使用 ID 或[项目 URL 编码路径](../rest/index.md#namespaced-path-encoding)访问。                                              |
| `metric`            | string         | 是    | `deployment_frequency`、`lead_time_for_changes`、`time_to_restore_service` 或 `change_failure_rate` 之一。                 |
| `start_date`        | string         | 否    | 开始的日期范围。ISO 8601 日期格式，例如 `2021-03-01`。 默认为 3 个月前。                                                                    |
| `end_date`          | string         | 否    | 结束日期范围。ISO 8601 日期格式，例如 `2021-03-01`。 默认为当前日期。                                                                       |
| `interval`          | string         | 否    | 时间间隔。`all`、`monthly` 或 `daily`。默认为 `daily`。                                                                          |
| `environment_tiers` | array of strings   | 否    |  [环境级别](../../ci/environments/index.md#deployment-tier-of-environments)。默认为 `production`。                 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/dora/metrics?metric=deployment_frequency"
```

响应示例：

```json
[
  { "date": "2021-03-01", "value": 3 },
  { "date": "2021-03-02", "value": 6 },
  { "date": "2021-03-03", "value": 0 },
  { "date": "2021-03-04", "value": 0 },
  { "date": "2021-03-05", "value": 0 },
  { "date": "2021-03-06", "value": 0 },
  { "date": "2021-03-07", "value": 0 },
  { "date": "2021-03-08", "value": 4 }
]
```

## 获取群组级别 DORA 指标

> 引入于 13.10 版本。

获取群组级别 DORA 指标。

```plaintext
GET /groups/:id/dora/metrics
```

| 参数          | 类型           | 是否必需 | 描述                                                                                                                                                                                         |
|--------------      |--------        |----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`               | integer/string | 是      | 通过身份验证用户可以使用 ID 或[项目 URL 编码路径](../rest/index.md#namespaced-path-encoding)访问。                                                                        |
| `metric`           | string         | 是      | `deployment_frequency`、`lead_time_for_changes`、`time_to_restore_service` 或 `change_failure_rate` 之一。                                                                                         |
| `start_date`       | string         | 否      | 开始的日期范围。ISO 8601 日期格式，例如`2021-03-01`。 默认为 3 个月前。                                                                                                  |
| `end_date`         | string         | 否      | 结束日期范围。ISO 8601 日期格式，例如`2021-03-01`。 默认为当前日期。                                                                                                  |
| `interval`         | string         | 否      | 时间间隔。`all`，`monthly` 或 `daily`。默认为 `daily`。                                                                                                                                        |
| `environment_tiers` | array of strings   | 否    |  [环境级别](../../ci/environments/index.md#deployment-tier-of-environments)。默认为 `production`。                 |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/dora/metrics?metric=deployment_frequency"
```

响应示例：

```json
[
  { "date": "2021-03-01", "value": 3 },
  { "date": "2021-03-02", "value": 6 },
  { "date": "2021-03-03", "value": 0 },
  { "date": "2021-03-04", "value": 0 },
  { "date": "2021-03-05", "value": 0 },
  { "date": "2021-03-06", "value": 0 },
  { "date": "2021-03-07", "value": 0 },
  { "date": "2021-03-08", "value": 4 }
]
```

## `value` 字段

以上项目级别和群组级别的接口，API 响应中 `value` 字段具有不同含义，具体要取决于提供的 `metric` 查询范围：

| `metric` 查询参数。 | 响应中 `value` 的含义。                                                                                                                           |
|:------------------------ |:-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `deployment_frequency`   | API 返回该时间段内成功部署的总数。                                                                                                 |
| `lead_time_for_changes`  | 合并请求 (MR) 的合并与该时间段内部署的所有 MR 的 MR 提交部署之间的中位数秒数。 |
| `time_to_restore_service`  | 事件在该时间段内打开的中位秒数。仅适用于生产环境。                                         |
| `change_failure_rate`  | 事件数除以时间段内的部署数。仅适用于生产环境。                              |

NOTE:
API 通过计算每日中值的中值返回 `monthly` 和 `all` 间隔。这可能会导致返回的数据略有不准确。
