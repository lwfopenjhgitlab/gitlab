---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 实例集群 API（基于证书）（已废弃） **(FREE SELF)**

> - <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/36001) -->引入于极狐GitLab 13.2。
> - <!--[Deprecated](https://gitlab.com/groups/gitlab-org/configure/-/epics/8)-->废弃于极狐GitLab 14.5。

WARNING:
此功能废弃于极狐GitLab 14.5。<!--[deprecated](https://gitlab.com/groups/gitlab-org/configure/-/epics/8) -->

使用实例级 Kubernetes 集群<!--[instance-level Kubernetes clusters](../user/instance/clusters/index.md),-->，您可以将 Kubernetes 集群连接到极狐GitLab 实例，并在您实例的所有项目中使用相同的集群。

用户需要管理员权限才能使用这些端点。

## 列出实例集群

返回实例集群列表。

```plaintext
GET /admin/clusters
```

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/admin/clusters"
```

响应示例：

```json
[
  {
    "id": 9,
    "name": "cluster-1",
    "created_at": "2020-07-14T18:36:10.440Z",
    "managed": true,
    "enabled": true,
    "domain": null,
    "provider_type": "user",
    "platform_type": "kubernetes",
    "environment_scope": "*",
    "cluster_type": "instance_type",
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/root"
    },
    "platform_kubernetes": {
      "api_url": "https://example.com",
      "namespace": null,
      "authorization_type": "rbac",
      "ca_cert":"-----BEGIN CERTIFICATE-----IxMDM1MV0ZDJkZjM...-----END CERTIFICATE-----"
    },
    "provider_gcp": null,
    "management_project": null
  },
  {
    "id": 10,
    "name": "cluster-2",
    "created_at": "2020-07-14T18:39:05.383Z",
    "domain": null,
    "provider_type": "user",
    "platform_type": "kubernetes",
    "environment_scope": "staging",
    "cluster_type": "instance_type",
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "https://gitlab.example.com/root"
    },
    "platform_kubernetes": {
      "api_url": "https://example.com",
      "namespace": null,
      "authorization_type": "rbac",
      "ca_cert":"-----BEGIN CERTIFICATE-----LzEtMCadtaLGxcsGAZjM...-----END CERTIFICATE-----"
    },
    "provider_gcp": null,
    "management_project": null
  },
  {
    "id": 11,
    "name": "cluster-3",
    ...
  }
]
```

## 获取单个实例集群

返回单个实例集群。

参数：

| 参数           | 类型      | 是否必需 | 描述                   |
|--------------|---------|------|----------------------|
| `cluster_id` | integer | yes  | 集群 ID  |

```plaintext
GET /admin/clusters/:cluster_id
```

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/admin/clusters/9"
```

响应示例：

```json
{
  "id": 9,
  "name": "cluster-1",
  "created_at": "2020-07-14T18:36:10.440Z",
  "managed": true,
  "enabled": true,
  "domain": null,
  "provider_type": "user",
  "platform_type": "kubernetes",
  "environment_scope": "*",
  "cluster_type": "instance_type",
  "user": {
    "id": 1,
    "name": "Administrator",
    "username": "root",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/root"
  },
  "platform_kubernetes": {
    "api_url": "https://example.com",
    "namespace": null,
    "authorization_type": "rbac",
    "ca_cert":"-----BEGIN CERTIFICATE-----IxMDM1MV0ZDJkZjM...-----END CERTIFICATE-----"
  },
  "provider_gcp": null,
  "management_project": null
}
```

<a id="add-existing-instance-cluster"></a>

## 添加现存实例集群

添加现存 Kubernetes 实例集群。

```plaintext
POST /admin/clusters/add
```

参数：

| 参数                                                   | 类型      | 是否必需 | 描述                                                                        |
|------------------------------------------------------|---------|------|---------------------------------------------------------------------------|
| `name`                                               | string  | yes  | 集群名称                                                                      |
| `domain`                                             | string  | no   | 集群的[基础域](../user/project/clusters/gitlab_managed_clusters.md#base-domain) |
| `environment_scope`                                  | string  | no   | 集群的关联环境。默认为 `*`                                                           |
| `management_project_id`                              | integer | no   | 集群[管理项目](../user/clusters/management_project.md)的 ID                      |
| `enabled`                                            | boolean | no   | 集群是否有效。默认为 `true`                                                         |
| `managed`                                            | boolean | no   | 极狐GitLab 是否管理该集群的命名空间和服务账户。默认为 `true`                                     |
| `platform_kubernetes_attributes[api_url]`            | string  | yes  | 访问 Kubernetes API 的 URL                                                   |
| `platform_kubernetes_attributes[token]`              | string  | yes  | 向 Kubernetes 鉴权的令牌                                                        |
| `platform_kubernetes_attributes[ca_cert]`            | string  | no   | TLS 证书。如果 API 使用自签名 TLS 证书，则必填                                            |
| `platform_kubernetes_attributes[namespace]`          | string  | no   | 与项目关联的唯一命名空间                                                              |
| `platform_kubernetes_attributes[authorization_type]` | string  | no   | 集群授权类型：`rbac`、`abac` 或 `unknown_authorization`。默认为 `rbac`                 |

请求示例：

```shell
curl --header "Private-Token:<your_access_token>" "http://gitlab.example.com/api/v4/admin/clusters/add" \
-H "Accept:application/json" \
-H "Content-Type:application/json" \
-X POST --data '{"name":"cluster-3", "environment_scope":"production", "platform_kubernetes_attributes":{"api_url":"https://example.com", "token":"12345",  "ca_cert":"-----BEGIN CERTIFICATE-----qpoeiXXZafCM0ZDJkZjM...-----END CERTIFICATE-----"}}'
```

响应示例：

```json
{
  "id": 11,
  "name": "cluster-3",
  "created_at": "2020-07-14T18:42:50.805Z",
  "managed": true,
  "enabled": true,
  "domain": null,
  "provider_type": "user",
  "platform_type": "kubernetes",
  "environment_scope": "production",
  "cluster_type": "instance_type",
  "user": {
    "id": 1,
    "name": "Administrator",
    "username": "root",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "http://gitlab.example.com:3000/root"
  },
  "platform_kubernetes": {
    "api_url": "https://example.com",
    "namespace": null,
    "authorization_type": "rbac",
    "ca_cert":"-----BEGIN CERTIFICATE-----qpoeiXXZafCM0ZDJkZjM...-----END CERTIFICATE-----"
  },
  "provider_gcp": null,
  "management_project": null
}
```

## 编辑实例集群

更新现存实例集群。

```plaintext
PUT /admin/clusters/:cluster_id
```

参数：

| 参数                                          | 类型      | 是否必需 | 描述                                                                        |
|---------------------------------------------|---------|------|---------------------------------------------------------------------------|
| `cluster_id`                                | integer | yes  | 集群 ID                                                                     |
| `name`                                      | string  | no   | 集群名称                                                                      |
| `domain`                                    | string  | no   | 集群的[基础域](../user/project/clusters/gitlab_managed_clusters.md#base-domain) |
| `environment_scope`                         | string  | no   | 集群关联的环境                                                                   |
| `management_project_id`                     | integer | no   | 集群[管理项目](../user/clusters/management_project.md)的 ID                      |
| `enabled`                                   | boolean | no   | 集群是否有效                                                                    |
| `managed`                                   | boolean | no   | 极狐GitLab 是否管理该集群的命名空间和服务账户                                                |
| `platform_kubernetes_attributes[api_url]`   | string  | no   | 访问 Kubernetes API 的 URL                                                   |
| `platform_kubernetes_attributes[token]`     | string  | no   | 向 Kubernetes 鉴权的令牌                                                        |
| `platform_kubernetes_attributes[ca_cert]`   | string  | no   | TLS 证书。如果 API 使用自签名 TLS 证书，则必填                                            |
| `platform_kubernetes_attributes[namespace]` | string  | no   | 与项目关联的唯一命名空间                                                              |

NOTE:
只有当集群通过添加现存 Kubernetes 集群<!--[添加现存 Kubernetes 集群](../user/project/clusters/add_existing_cluster.md)-->选项或[添加现存实例集群](#add-existing-instance-cluster)端点进行添加，才能更新 `name`、`api_url`、`ca_cert` 和 `token`。

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "http://gitlab.example.com/api/v4/admin/clusters/9" \
-H "Content-Type:application/json" \
-X PUT --data '{"name":"update-cluster-name", "platform_kubernetes_attributes":{"api_url":"https://new-example.com","token":"new-token"}}'
```

响应示例：

```json
{
  "id": 9,
  "name": "update-cluster-name",
  "created_at": "2020-07-14T18:36:10.440Z",
  "managed": true,
  "enabled": true,
  "domain": null,
  "provider_type": "user",
  "platform_type": "kubernetes",
  "environment_scope": "*",
  "cluster_type": "instance_type",
  "user": {
    "id": 1,
    "name": "Administrator",
    "username": "root",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/root"
  },
  "platform_kubernetes": {
    "api_url": "https://new-example.com",
    "namespace": null,
    "authorization_type": "rbac",
    "ca_cert":"-----BEGIN CERTIFICATE-----IxMDM1MV0ZDJkZjM...-----END CERTIFICATE-----"
  },
  "provider_gcp": null,
  "management_project": null,
  "project": null
}
```

## 删除实例集群

删除现有实例集群。不移除连接的 Kubernetes 集群中的现有资源。

```plaintext
DELETE /admin/clusters/:cluster_id
```

参数：

| 参数           | 类型      | 是否必需 | 描述                   |
|--------------|---------|------|----------------------|
| `cluster_id` | integer | yes  | 集群 ID |

请求示例：

```shell
curl --request DELETE --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/admin/clusters/11"
```
