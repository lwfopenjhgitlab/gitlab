---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 用户 API **(FREE)**


本文档包含有关用户 API 的 API 调用、参数和响应的信息。

有关更新用户事件时间戳的用户活动的信息，请参阅[获取用户活动](#get-user-activities)。

## 列出用户

获取用户列表。

此功能使用分页参数 `page` 和 `per_page` 限制用户列表。

### 对于非管理员用户

```plaintext
GET /users
```

```json
[
  {
    "id": 1,
    "username": "john_smith",
    "name": "John Smith",
    "state": "active",
    "avatar_url": "http://localhost:3000/uploads/user/avatar/1/cd8.jpeg",
    "web_url": "http://localhost:3000/john_smith"
  },
  {
    "id": 2,
    "username": "jack_smith",
    "name": "Jack Smith",
    "state": "blocked",
    "avatar_url": "http://gravatar.com/../e32131cd8.jpeg",
    "web_url": "http://localhost:3000/jack_smith"
  }
]
```

您还可以使用 `?search=` 按姓名、用户名或公共电子邮件搜索用户。例如，`/users?search=John`。当您搜索：

- 公共电子邮件，您必须使用完整的电子邮件地址才能完全匹配。搜索可能返回部分匹配。例如，如果您搜索电子邮件 `on@example.com`，则搜索可以同时返回 `on@example.com` 和 `jon@example.com`。
- 姓名或用户名，您不必完全匹配，因为是模糊搜索。

此外，您可以通过用户名查找用户：

```plaintext
GET /users?username=:username
```

例如：

```plaintext
GET /users?username=jack_smith
```

NOTE:
用户名称搜索区分大小写。

此外，您可以根据状态 `blocked` 和 `active` 过滤用户。
不支持 `active=false` 或 `blocked=false`。

```plaintext
GET /users?active=true
```

```plaintext
GET /users?blocked=true
```

此外，您只能使用 `external=true` 搜索外部用户。
不支持`external=false`。

```plaintext
GET /users?external=true
```

极狐GitLab 支持机器人用户，例如[警报机器人](../operations/incident_management/integrations.md)或[支持机器人](../user/project/service_desk.md#support-bot-user)。
您可以使用 `exclude_internal=true` 参数从用户列表中排除以下类型的内部用户<!--[内部用户](../development/internal_users.md#internal-users)-->（引入于 13.4）。

- 警报机器人
- 支持机器人

但是，此操作不排除[项目的机器人用户](../user/project/settings/project_access_tokens.md#bot-users-for-projects)或[群组的机器人用户](../user/group/settings/group_access_tokens.md#bot-users-for-groups)。

```plaintext
GET /users?exclude_internal=true
```

此外，如果您要从用户列表中排除外部用户，您可以使用 `exclude_external=true` 参数。

```plaintext
GET /users?exclude_external=true
```

为排除[项目的机器人用户](../user/project/settings/project_access_tokens.md#bot-users-for-projects)和[群组的机器人用户](../user/group/settings/group_access_tokens.md#bot-users-for-groups)，您可以使用 `without_project_bots=true` 参数。

```plaintext
GET /users?without_project_bots=true
```

### 对于管理员 **(FREE SELF)**

> - 响应中的 `namespace_id` 字段引入于极狐GitLab 14.10。
> - 响应中的 `created_by` 字段引入于极狐GitLab 15.6。
> - 响应中的 `scim_identities` 字段引入于极狐GitLab 16.1。

```plaintext
GET /users
```

| 参数                               | 类型      | 是否必需 | 描述                                                                        |
|----------------------------------|---------|------|---------------------------------------------------------------------------|
| `order_by`                       | string  | no   | 返回按照 `id`、`name`、`username`、`created_at` 或 `updated_at` 字段排序的用户。 默认为 `id` |
| `sort`                           | string  | no   | 返回按照 `asc` 或 `desc` 顺序排序的用户。默认为 `desc`                                    |
| `two_factor`                     | string  | no   | 根据双重身份验证过滤用户。过滤值为 `enabled` 或 `disabled`。默认返回所有用户                         |
| `without_projects`               | boolean | no   | 过滤没有项目的用户。默认为 `false`，表明返回所有用户，不论其有没有项目                                   |
| `admins`                         | boolean | no   | 仅返回管理员。默认为 `false`                                                        |
| `saml_provider_id` **(PREMIUM)** | number  | no   | 仅返回使用特定 SAML 提供者 ID 创建的用户。如果未包括，则返回所有用户                                   |
| `skip_ldap` **(PREMIUM)**        | boolean | no     | 跳过 LDAP 用户                                                                |


```json
[
  {
    "id": 1,
    "username": "john_smith",
    "email": "john@example.com",
    "name": "John Smith",
    "state": "active",
    "avatar_url": "http://localhost:3000/uploads/user/avatar/1/index.jpg",
    "web_url": "http://localhost:3000/john_smith",
    "created_at": "2012-05-23T08:00:58Z",
    "is_admin": false,
    "bio": "",
    "location": null,
    "skype": "",
    "linkedin": "",
    "twitter": "",
    "discord": "",
    "website_url": "",
    "organization": "",
    "job_title": "",
    "last_sign_in_at": "2012-06-01T11:41:01Z",
    "confirmed_at": "2012-05-23T09:05:22Z",
    "theme_id": 1,
    "last_activity_on": "2012-05-23",
    "color_scheme_id": 2,
    "projects_limit": 100,
    "current_sign_in_at": "2012-06-02T06:36:55Z",
    "note": "DMCA Request: 2018-11-05 | DMCA Violation | Abuse | https://gitlab.zendesk.com/agent/tickets/123",
    "identities": [
      {"provider": "github", "extern_uid": "2435223452345"},
      {"provider": "bitbucket", "extern_uid": "john.smith"},
      {"provider": "google_oauth2", "extern_uid": "8776128412476123468721346"}
    ],
    "can_create_group": true,
    "can_create_project": true,
    "two_factor_enabled": true,
    "external": false,
    "private_profile": false,
    "current_sign_in_ip": "196.165.1.102",
    "last_sign_in_ip": "172.127.2.22",
    "namespace_id": 1,
    "created_by": null
  },
  {
    "id": 2,
    "username": "jack_smith",
    "email": "jack@example.com",
    "name": "Jack Smith",
    "state": "blocked",
    "avatar_url": "http://localhost:3000/uploads/user/avatar/2/index.jpg",
    "web_url": "http://localhost:3000/jack_smith",
    "created_at": "2012-05-23T08:01:01Z",
    "is_admin": false,
    "bio": "",
    "location": null,
    "skype": "",
    "linkedin": "",
    "twitter": "",
    "discord": "",
    "website_url": "",
    "organization": "",
    "job_title": "",
    "last_sign_in_at": null,
    "confirmed_at": "2012-05-30T16:53:06.148Z",
    "theme_id": 1,
    "last_activity_on": "2012-05-23",
    "color_scheme_id": 3,
    "projects_limit": 100,
    "current_sign_in_at": "2014-03-19T17:54:13Z",
    "identities": [],
    "can_create_group": true,
    "can_create_project": true,
    "two_factor_enabled": true,
    "external": false,
    "private_profile": false,
    "current_sign_in_ip": "10.165.1.102",
    "last_sign_in_ip": "172.127.2.22",
    "namespace_id": 2,
    "created_by": null
  }
]
```

[极狐GitLab 专业版及旗舰版](https://gitlab.cn/pricing/)的用户也能看见 `shared_runners_minutes_limit`、`extra_shared_runners_minutes_limit`、`is_auditor` 和 `using_license_seat` 参数。

```json
[
  {
    "id": 1,
    ...
    "shared_runners_minutes_limit": 133,
    "extra_shared_runners_minutes_limit": 133,
    "is_auditor": false,
    "using_license_seat": true
    ...
  }
]
```

[极狐GitLab 专业版及旗舰版](https://gitlab.cn/pricing/)的用户也能看见 `group_saml` 提供者选项和 `provisioned_by_group_id` 参数：

```json
[
  {
    "id": 1,
    ...
    "identities": [
      {"provider": "github", "extern_uid": "2435223452345"},
      {"provider": "bitbucket", "extern_uid": "john.smith"},
      {"provider": "google_oauth2", "extern_uid": "8776128412476123468721346"},
      {"provider": "group_saml", "extern_uid": "123789", "saml_provider_id": 10}
    ],
    "provisioned_by_group_id": 123789
    ...
  }
]
```

您还可以使用 `?search=` 按姓名、用户名或电子邮件搜索用户。例如，`/users?search=John`。当您搜索：

- 电子邮件，您必须使用完整的电子邮件地址才能进行完全匹配。作为管理员，您可以搜索公共和私人电子邮件地址。
- 姓名或用户名，您不必完全匹配，因为是模糊搜索。

您可以使用外部 UID 和提供者查找用户：

```plaintext
GET /users?extern_uid=:extern_uid&provider=:provider
```

例如：

```plaintext
GET /users?extern_uid=1234567&provider=github
```

您可以使用创建日期时间范围查找用户：

```plaintext
GET /users?created_before=2001-01-02T00:00:00.060Z&created_after=1999-01-02T00:00:00.060
```

您可以使用 `/users?without_projects=true` 搜索没有项目的用户：

您可以使用以下内容通过[自定义属性](custom_attributes.md)进行过滤：

```plaintext
GET /users?custom_attributes[key]=value&custom_attributes[other_key]=other_value
```

您可以使用以下内容在响应中包括用户的[自定义属性](custom_attributes.md)：

```plaintext
GET /users?with_custom_attributes=true
```

您可以使用 `created_by` 参数来查看是否创建了用户帐户：

- [由管理员手动创建](../user/profile/account/create_accounts.md#create-users-in-admin-area)。
- 创建为[项目机器人用户](../user/project/settings/project_access_tokens.md#bot-users-for-projects)。

如果返回值为 `null`，则该帐户是由自己注册帐户的用户创建的。

## 单个用户

获取单个用户。

### 对于用户

```plaintext
GET /users/:id
```

参数：

| 参数   | 类型      | 是否必需 | 描述              |
|------|---------|------|-----------------|
| `id` | integer | yes  | 用户 ID |

```json
{
  "id": 1,
  "username": "john_smith",
  "name": "John Smith",
  "state": "active",
  "avatar_url": "http://localhost:3000/uploads/user/avatar/1/cd8.jpeg",
  "web_url": "http://localhost:3000/john_smith",
  "created_at": "2012-05-23T08:00:58Z",
  "bio": "",
  "bot": false,
  "location": null,
  "public_email": "john@example.com",
  "skype": "",
  "linkedin": "",
  "twitter": "",
  "discord": "",
  "website_url": "",
  "organization": "",
  "job_title": "Operations Specialist",
  "pronouns": "he/him",
  "work_information": null,
  "followers": 1,
  "following": 1,
  "local_time": "3:38 PM",
  "is_followed": false
}
```

### 对于管理员 **(FREE SELF)**

> - 响应中的 `namespace_id` 字段引入于极狐GitLab 14.10。
> - 响应中的 `created_by` 字段引入于极狐GitLab 15.6。

```plaintext
GET /users/:id
```

参数：

| 参数   | 类型      | 是否必需 | 描述              |
|------|---------|------|-----------------|
| `id` | integer | yes  | 用户 ID|

响应示例：

```json
{
  "id": 1,
  "username": "john_smith",
  "email": "john@example.com",
  "name": "John Smith",
  "state": "active",
  "avatar_url": "http://localhost:3000/uploads/user/avatar/1/index.jpg",
  "web_url": "http://localhost:3000/john_smith",
  "created_at": "2012-05-23T08:00:58Z",
  "is_admin": false,
  "bio": "",
  "location": null,
  "public_email": "john@example.com",
  "skype": "",
  "linkedin": "",
  "twitter": "",
  "discord": "",
  "website_url": "",
  "organization": "",
  "job_title": "Operations Specialist",
  "pronouns": "he/him",
  "work_information": null,
  "followers": 1,
  "following": 1,
  "local_time": "3:38 PM",
  "last_sign_in_at": "2012-06-01T11:41:01Z",
  "confirmed_at": "2012-05-23T09:05:22Z",
  "theme_id": 1,
  "last_activity_on": "2012-05-23",
  "color_scheme_id": 2,
  "projects_limit": 100,
  "current_sign_in_at": "2012-06-02T06:36:55Z",
  "note": "DMCA Request: 2018-11-05 | DMCA Violation | Abuse | https://gitlab.zendesk.com/agent/tickets/123",
  "identities": [
    {"provider": "github", "extern_uid": "2435223452345"},
    {"provider": "bitbucket", "extern_uid": "john.smith"},
    {"provider": "google_oauth2", "extern_uid": "8776128412476123468721346"}
  ],
  "can_create_group": true,
  "can_create_project": true,
  "two_factor_enabled": true,
  "external": false,
  "private_profile": false,
  "commit_email": "john-codes@example.com",
  "current_sign_in_ip": "196.165.1.102",
  "last_sign_in_ip": "172.127.2.22",
  "plan": "gold",
  "trial": true,
  "sign_in_count": 1337,
  "namespace_id": 1,
  "created_by": null
}
```

NOTE:
`plan` 和 `trial` 参数仅在收费版本中可用。

[极狐GitLab 专业版及旗舰版](https://about.gitlab.cn/pricing/)的用户也能看见 `shared_runners_minutes_limit`、`is_auditor` 和 `extra_shared_runners_minutes_limit` 参数。

```json
{
  "id": 1,
  "username": "john_smith",
  "is_auditor": false,
  "shared_runners_minutes_limit": 133,
  "extra_shared_runners_minutes_limit": 133,
  ...
}
```

[极狐GitLab 专业版及旗舰版](https://about.gitlab.cn/pricing/)的用户也能看见 `group_saml` 选项和 `provisioned_by_group_id` 参数：

```json
{
  "id": 1,
  "username": "john_smith",
  "shared_runners_minutes_limit": 133,
  "extra_shared_runners_minutes_limit": 133,
  "identities": [
    {"provider": "github", "extern_uid": "2435223452345"},
    {"provider": "bitbucket", "extern_uid": "john.smith"},
    {"provider": "google_oauth2", "extern_uid": "8776128412476123468721346"},
    {"provider": "group_saml", "extern_uid": "123789", "saml_provider_id": 10}
  ],
  "provisioned_by_group_id": 123789
  ...
}
```

[JiHuLab.com 专业版或旗舰版](https://about.gitlab.cn/pricing/)的用户也可以查看 `scim_identities` 参数：

```json
{
  ...
  "extra_shared_runners_minutes_limit": null,
  "scim_identities": [
      {"extern_uid": "2435223452345", "group_id": "3", "active": true},
      {"extern_uid": "john.smith", "group_id": "42", "active": false}
    ]
  ...
}
```

管理员可以使用 `created_by` 参数查看是否创建了用户帐户：

- [由管理员手动创建](../user/profile/account/create_accounts.md#create-users-in-admin-area)。
- 创建为[项目机器人用户](../user/project/settings/project_access_tokens.md#bot-users-for-projects)。

如果返回值为 `null`，则该帐户是由自己注册帐户的用户创建的。

您可以使用以下内容在响应中包括用户的[自定义属性](custom_attributes.md)：

```plaintext
GET /users/:id?with_custom_attributes=true
```

## 用户创建 **(FREE SELF)**

> - 响应中的 `namespace_id` 字段引入于极狐GitLab 14.10。
> - 创建审计员用户功能引入于极狐GitLab 15.3。

创建新用户。请注意，只有管理员可以创建新用户。
必须指定 `password`、`reset_password` 或 `force_random_password`。
如果 `reset_password` 和 `force_random_password` 都是 `false`，那么 `password` 是必需的。

`force_random_password` 和 `reset_password` 优先级高于 `password`。此外，`reset_password` 和 `force_random_password` 可以一起使用。

NOTE:
从 12.1 起，`private_profile` 默认为 `false`。
从 15.8 起，`private_profile` 默认为[此](../user/admin_area/settings/account_and_limit_settings.md#set-profiles-of-new-users-to-private-by-default)设置确定的值。

NOTE:
从 13.2 起，`bio` 默认为 `""`，而不是 `null`。

```plaintext
POST /users
```

参数：

| 参数                                                  | 是否必需 | 描述                                                                                                                                       |
|:----------------------------------------------------|:-----|:-----------------------------------------------------------------------------------------------------------------------------------------|
| `admin`                                             | No   | 用户为管理员。有效值为 `true` 或 `false`，默认为 `false`                                                                                                 |
| `auditor` **(PREMIUM)**                             | No   | 用户是审计员。有效值为 `true` 或 `false`，默认为 `false`。引入于 15.3                                                                                        |
| `avatar`                                            | No   | 用户头像的图片文件                                                                                                                                |
| `bio`                                               | No   | 用户介绍                                                                                                                                     |
| `can_create_group`                                  | No   | 用户可以创建群组 - `true` 或 `false`                                                                                                              |
| `color_scheme_id`                                   | No   | 用户的文件查看器的色彩方案（更多信息，请参见[用户个人设置文档](../user/profile/preferences.md#syntax-highlighting-theme)）                                              |
| `email`                                             | Yes  | 电子邮件                                                                                                                                     |
| `extern_uid`                                        | No   | 外部 UID                                                                                                                                   |
| `external`                                          | No   | 将用户标记为外部 - 真或假（默认）                                                                                                                       |
| `extra_shared_runners_minutes_limit` **(PREMIUM)**  | No   | 仅可由管理员设置。此用户的额外计算单元                                                                                                                      |
| `force_random_password`                             | No   | 将用户密码设置为随意值 - 真或假（默认）                                                                                                                    |
| `group_id_for_saml`                                 | No   | 配置 SAML 的群组的 ID                                                                                                                          |
| `linkedin`                                          | No   | 领英                                                                                                                                       |
| `location`                                          | No   | 用户位置                                                                                                                                     |
| `name`                                              | Yes  | 名称                                                                                                                                       |
| `note`                                              | No   | 该用户的管理员备注                                                                                                                                |
| `organization`                                      | No   | 机构名称                                                                                                                                     |
| `password`                                          | No   | 密码                                                                                                                                       |
| `private_profile`                                   | No   | 用户的配置文件是私有的 - 真或假，默认值由[此](../user/admin_area/settings/account_and_limit_settings.md#set-profiles-of-new-users-to-private-by-default)设置决定 |
| `projects_limit`                                    | No   | 用户可以创建的项目数量                                                                                                                              |
| `provider`                                          | No   | 外部提供者名称                                                                                                                                  |
| `reset_password`                                    | No   | 发送用户密码重置链接 - 真或假（默认）                                                                                                                     |
| `shared_runners_minutes_limit` **(PREMIUM)**        | No   | 仅可由管理员设置。该用户的最大月度计算单元。可以是 `nil`（默认；继承系统默认）、`0`（无限制）或 `> 0`                                                                               |
| `skip_confirmation`                                 | No   | 跳过确认 - 真或假（默认）                                                                                                                           |
| `skype`                                             | No   | Skype ID                                                                                                                                 |
| `theme_id`                                          | No   | 用户的极狐GitLab 主题（详情信息，请参见[用户个人设置文档](../user/profile/preferences.md#navigation-theme)）                                                      |
| `twitter`                                           | No   | Twitter 账户                                                                                                                               |
| `discord`                                           | No   | Discord 账户                                                                                                                               |
| `username`                                          | Yes  | 用户名称                                                                                                                                     |
| `view_diffs_file_by_file`                           | No   | 表明用户每页只能看见一个文件差异的标志                                                                                                                      |
| `website_url`                                       | No   | 网页 URL                                                                                                                                   |

## 用户修改 **(FREE SELF)**

> - 响应中的 `namespace_id` 字段引入于极狐GitLab 14.10。
> - 修改审计员用户功能引入于极狐GitLab 15.3。

修改现存用户。只有管理员可以修改用户的属性。 

`email` 字段是用户的主要电子邮件地址。您只能将此字段更改为已为该用户添加的次要电子邮件地址。要为同一用户添加更多电子邮件地址，请使用[添加电子邮件功能](#add-email)。

```plaintext
PUT /users/:id
```

参数：

| 参数                                                 | 是否必需 | 描述                                                                                          |
|:---------------------------------------------------|:-----|:--------------------------------------------------------------------------------------------|
| `admin`                                            | No   | 用户为管理员，有效值为 `true` 或 `false`，默认为 `false`                                                    |
| `auditor` **(PREMIUM)**                            | No   | 用户为审计员，有效值为 `true` 或 `false`，默认为 `false`。引入于 15.3（默认）                                       |
| `avatar`                                           | No   | 用户头像的图片文件                                                                                   |
| `bio`                                              | No   | 用户介绍                                                                                        |
| `can_create_group`                                 | No   | 用户可以创建群组 - 真或假                                                                              |
| `color_scheme_id`                                  | No   | 用户的文件查看器的色彩方案（详细信息，请参见[用户个人设置文档](../user/profile/preferences.md#syntax-highlighting-theme)） |
| `commit_email`                                     | No   | 用户的提交电子邮件。设置为 `_private` 以使用私人提交电子邮件。引入于 15.5                                               |
| `email`                                            | No   | 电子邮件                                                                                        |
| `extern_uid`                                       | No   | 外部 UID                                                                                      |
| `external`                                         | No   | 将用户标记为外部 - 真或假（默认）                                                                          |
| `extra_shared_runners_minutes_limit` **(PREMIUM)** | No   | 仅可由管理员设置。此用户的额外计算单元                                                                         |
| `group_id_for_saml`                                | No   | 配置 SAML 的群组的 ID                                                                             |
| `id`                                               | Yes  | 用户 ID                                                                                       |
| `linkedin`                                         | No   | 领英                                                                                          |
| `location`                                         | No   | 用户位置                                                                                        |
| `name`                                             | No   | 名称                                                                                          |
| `note`                                             | No   | 该用户的管理员备注                                                                                   |
| `organization`                                     | No   | 机构名称                                                                                        |
| `password`                                         | No   | 密码                                                                                          |
| `private_profile`                                  | No   | 用户的配置文件是私有的 - 真或假                                                                           |
| `projects_limit`                                   | No   | 限制每个用户可以创建的项目                                                                               |
| `pronouns`                                         | No   | 代词                                                                                          |
| `provider`                                         | No   | 外部供应者名称                                                                                     |
| `public_email`                                     | No   | 用户的公共电子邮件（必须已经过验证）                                                                          |
| `shared_runners_minutes_limit` **(PREMIUM)**       | No   | 仅可由管理员设置。该用户的最大月度计算单元。可以是 `nil`（默认；继承系统默认）、`0`（无限制）或 `> 0`                                  |
| `skip_reconfirmation`                              | No   | 跳过重新确认 - 真或假（默认）                                                                            |
| `skype`                                            | No   | Skype ID                                                                                    |
| `theme_id`                                         | No   | 用户的极狐GitLab 主题（参见[用户个人设置文档](../user/profile/preferences.md#navigation-theme)）               |
| `twitter`                                          | No   | Twitter 账户                                                                                  |
| `discord`                                           |No   | Discord 账户                                                                                  |
| `username`                                         | No   | 用户名                                                                                         |
| `view_diffs_file_by_file`                          | No   | 表明用户每页只能看见一个文件差异的标志                                                                         |
| `website_url`                                      | No   | 网址 URL                                                                                      |

在密码更新时，用户必须在下次登录时更改密码。
注意，目前这个方法只返回一个 `404` 错误，即使在 `409` （冲突）更合适的情况下。
例如，将电子邮件地址重命名为某个现有地址时。

## 删除用户的鉴权身份 **(FREE SELF)**

使用与该身份关联的提供者名称删除用户的鉴权身份。仅管理员可用。

```plaintext
DELETE /users/:id/identities/:provider
```

参数

| 参数         | 类型      | 是否必需 | 描述      |
|------------|---------|------|---------|
| `id`       | integer | yes  | 用户 ID   |
| `provider` | string  | yes  | 外部提供者名称 |

## 用户删除 **(FREE SELF)**

删除用户。仅管理员可用。
如果操作成功，则返回 `204 No Content` 状态码，如果找不到资源，则返回 `404`；如果无法软删除用户，则返回 `409`。

```plaintext
DELETE /users/:id
```

参数：

| 参数            | 类型      | 是否必需 | 描述                                                                                                                                                                                                                                                           |
|---------------|---------|------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`          | integer | yes  | 用户 ID                                                                                                                                                                                                                                                        |
| `hard_delete` | boolean | no   | 如果为真，则通常[移至 Ghost 用户](../user/profile/account/delete_account.md#associated-records)的贡献以及由该用户单独拥有的群组将被删除|

## 列出当前用户

获取当前用户。

### 对于非管理员用户

获取经过身份验证的用户。

```plaintext
GET /user
```

```json
{
  "id": 1,
  "username": "john_smith",
  "email": "john@example.com",
  "name": "John Smith",
  "state": "active",
  "avatar_url": "http://localhost:3000/uploads/user/avatar/1/index.jpg",
  "web_url": "http://localhost:3000/john_smith",
  "created_at": "2012-05-23T08:00:58Z",
  "bio": "",
  "location": null,
  "public_email": "john@example.com",
  "skype": "",
  "linkedin": "",
  "twitter": "",
  "discord": "",
  "website_url": "",
  "organization": "",
  "job_title": "",
  "pronouns": "he/him",
  "bot": false,
  "work_information": null,
  "followers": 0,
  "following": 0,
  "local_time": "3:38 PM",
  "last_sign_in_at": "2012-06-01T11:41:01Z",
  "confirmed_at": "2012-05-23T09:05:22Z",
  "theme_id": 1,
  "last_activity_on": "2012-05-23",
  "color_scheme_id": 2,
  "projects_limit": 100,
  "current_sign_in_at": "2012-06-02T06:36:55Z",
  "identities": [
    {"provider": "github", "extern_uid": "2435223452345"},
    {"provider": "bitbucket", "extern_uid": "john_smith"},
    {"provider": "google_oauth2", "extern_uid": "8776128412476123468721346"}
  ],
  "can_create_group": true,
  "can_create_project": true,
  "two_factor_enabled": true,
  "external": false,
  "private_profile": false,
  "commit_email": "admin@example.com",
}
```

[极狐GitLab 专业版及旗舰版](https://about.gitlab.cn/pricing/)的用户也能查看到 `shared_runners_minutes_limit` 和 `extra_shared_runners_minutes_limit` 参数。

### 对于管理员 **(FREE SELF)**

> - 响应中的 `namespace_id` 字段引入于极狐GitLab 14.10。
> - 响应中的 `created_by` 字段引入于极狐GitLab 15.6。

```plaintext
GET /user
```

参数：

| 参数     | 类型      | 是否必需 | 描述               |
|--------|---------|------|------------------|
| `sudo` | integer | no   | 在其位置上进行调用的用户的 ID |

```json
{
  "id": 1,
  "username": "john_smith",
  "email": "john@example.com",
  "name": "John Smith",
  "state": "active",
  "avatar_url": "http://localhost:3000/uploads/user/avatar/1/index.jpg",
  "web_url": "http://localhost:3000/john_smith",
  "created_at": "2012-05-23T08:00:58Z",
  "is_admin": true,
  "bio": "",
  "location": null,
  "public_email": "john@example.com",
  "skype": "",
  "linkedin": "",
  "twitter": "",
  "discord": "",
  "website_url": "",
  "organization": "",
  "job_title": "",
  "last_sign_in_at": "2012-06-01T11:41:01Z",
  "confirmed_at": "2012-05-23T09:05:22Z",
  "theme_id": 1,
  "last_activity_on": "2012-05-23",
  "color_scheme_id": 2,
  "projects_limit": 100,
  "current_sign_in_at": "2012-06-02T06:36:55Z",
  "identities": [
    {"provider": "github", "extern_uid": "2435223452345"},
    {"provider": "bitbucket", "extern_uid": "john_smith"},
    {"provider": "google_oauth2", "extern_uid": "8776128412476123468721346"}
  ],
  "can_create_group": true,
  "can_create_project": true,
  "two_factor_enabled": true,
  "external": false,
  "private_profile": false,
  "commit_email": "john-codes@example.com",
  "current_sign_in_ip": "196.165.1.102",
  "last_sign_in_ip": "172.127.2.22",
  "namespace_id": 1,
  "created_by": null,
  "note": null
}
```

[极狐GitLab 专业版及旗舰版](https://about.gitlab.cn/pricing/)的用户也能看见以下参数：

- `shared_runners_minutes_limit`
- `extra_shared_runners_minutes_limit`
- `is_auditor`
- `provisioned_by_group_id`
- `using_license_seat`

## 用户状态

获取经过身份验证的用户的状态。

```plaintext
GET /user/status
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/user/status"
```

响应示例：

```json
{
  "emoji":"coffee",
  "availability":"busy",
  "message":"I crave coffee :coffee:",
  "message_html": "I crave coffee <gl-emoji title=\"hot beverage\" data-name=\"coffee\" data-unicode-version=\"4.0\">☕</gl-emoji>",
  "clear_status_at": null
}
```

## 获取用户状态

获取用户状态。此端点可无鉴权访问。

```plaintext
GET /users/:id_or_username/status
```

| 参数               | 类型     | 是否必需 | 描述                                              |
|------------------|--------|------|-------------------------------------------------|
| `id_or_username` | string | yes  | 要获取状态的用户的 ID 或名称|

```shell
curl "https://gitlab.example.com/users/janedoe/status"
```

响应示例：

```json
{
  "emoji":"coffee",
  "availability":"busy",
  "message":"I crave coffee :coffee:",
  "message_html": "I crave coffee <gl-emoji title=\"hot beverage\" data-name=\"coffee\" data-unicode-version=\"4.0\">☕</gl-emoji>",
  "clear_status_at": null
}
```

## 设置用户状态

设置当前用户的状态。

```plaintext
PUT /user/status
PATCH /user/status
```

| 参数                   | 类型     | 是否必需 | 描述                                                                                                                                            |
|----------------------|--------|------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| `emoji`              | string | no   | 用作状态的表情符号的名称。如果省略，则使用 `speech_balloon`。表情符号名称可以是 [Gemojione index](https://github.com/bonusly/gemojione/blob/master/config/index.json) 中指定的名称 |
| `message`            | string | no   | 设置为状态的消息。也可以包含表情符号代码，不能超过 100 个字符                                                                                                             |
| `clear_status_after` | string | no   | 一段时间后自动清理状态。允许的值为：`30_minutes`、`3_hours`、`8_hours`、`1_day`、`3_days`、`7_days` 和 `30_days`                                                      

`PUT` 和 `PATCH` 的区别。

使用 `PUT` 时，任何未传递的参数都将设置为 `null` 并因此被清除。使用 `PATCH` 时，任何未传递的参数都将被忽略。显式传递 `null` 以清除字段。

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" --data "clear_status_after=1_day" --data "emoji=coffee" \
     --data "message=I crave coffee" "https://gitlab.example.com/api/v4/user/status"
```

响应示例：

```json
{
  "emoji":"coffee",
  "message":"I crave coffee",
  "message_html": "I crave coffee",
  "clear_status_at":"2021-02-15T10:49:01.311Z"
}
```

## 获取用户设置

获取经过身份验证的用户的个人设置列表。

```plaintext
GET /user/preferences
```

响应示例：

```json
{
  "id": 1,
  "user_id": 1
  "view_diffs_file_by_file": true,
  "show_whitespace_in_diffs": false,
  "pass_user_identities_to_ci_jwt": false
}
```

参数：

- **无**

## 用户设置修改

更新当前用户的设置。

```plaintext
PUT /user/preferences
```

```json
{
  "id": 1,
  "user_id": 1
  "view_diffs_file_by_file": true,
  "show_whitespace_in_diffs": false,
  "pass_user_identities_to_ci_jwt": false
}
```

参数：

| 参数                         | 是否必需 | 描述                                                                                                                                                                                                                                                                                                                                                         |
|:---------------------------|:-----|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `view_diffs_file_by_file`  | Yes  | 表明用户每页只能看见一处文件差异的标志                                                                                                                                                                                                                                                                                                                                        |
| `show_whitespace_in_diffs` | Yes  | 表明用户在差异中看见空白更改的标志                                                                                                                                                                                                                                                                                                                                          |
| `pass_user_identities_to_ci_jwt` | Yes      | 指示用户将其外部身份作为 CI 信息传递的标志。此参数不包含足够的信息来识别或授权外部系统中的用户。该参数是极狐GitLab 内部的，不得传递给第三方服务 |


## 用户关注

### 关注和取消关注用户

关注用户。

```plaintext
POST /users/:id/follow
```

取消关注用户。

```plaintext
POST /users/:id/unfollow
```

| 参数   | 类型      | 是否必需 | 描述                               |
|------|---------|------|----------------------------------|
| `id` | integer | yes  | 要关注的用户的 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/users/3/follow"
```

响应示例：

```json
{
  "id": 1,
  "username": "john_smith",
  "name": "John Smith",
  "state": "active",
  "avatar_url": "http://localhost:3000/uploads/user/avatar/1/cd8.jpeg",
  "web_url": "http://localhost:3000/john_smith"
}
```

### 粉丝和关注

获取用户粉丝。

```plaintext
GET /users/:id/followers
```

获取被关注的用户的列表。

```plaintext
GET /users/:id/following
```

| 参数   | 类型      | 是否必需 | 描述                               |
|------|---------|------|----------------------------------|
| `id` | integer | yes  | 要关注的用户的 ID  |

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>"  "https://gitlab.example.com/users/3/followers"
```

响应示例：

```json
[
  {
    "id": 2,
    "name": "Lennie Donnelly",
    "username": "evette.kilback",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/7955171a55ac4997ed81e5976287890a?s=80&d=identicon",
    "web_url": "http://127.0.0.1:3000/evette.kilback"
  },
  {
    "id": 4,
    "name": "Serena Bradtke",
    "username": "cammy",
    "state": "active",
    "avatar_url": "https://www.gravatar.com/avatar/a2daad869a7b60d3090b7b9bef4baf57?s=80&d=identicon",
    "web_url": "http://127.0.0.1:3000/cammy"
  }
]
```

## 用户数量

获取经过身份验证的用户的数量（和右上角菜单中的一样）。

| 参数                                | 类型     | 描述                                           |
|-----------------------------------|--------|----------------------------------------------|
| `assigned_issues`                 | number | 开放并分配到当前用户的议题数量。添加于 14.2                     |
| `assigned_merge_requests`         | number | 生效且分配到当前用户的合并请求的数量。添加于 13.8                  |
| `merge_requests`                  | number | 废弃于 13.8。相当于 `assigned_merge_requests` 并被其代替 |
| `review_requested_merge_requests` | number | 当前用户被请求评审的合并请求的数量。添加于 13.8                   |
| `todos`                           | number | 当前用户的待办事项数量。添加于 14.2                         |

```plaintext
GET /user_counts
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/user_counts"
```

响应示例：

```json
{
  "merge_requests": 4,
  "assigned_issues": 15,
  "assigned_merge_requests": 11,
  "review_requested_merge_requests": 0,
  "todos": 1
}
```

## 创建服务账户用户 **(PREMIUM)**

> 创建服务账户的功能引入于极狐GitLab 16.1。

使用自动生成的电子邮件地址和用户名创建服务账户用户。

```plaintext
POST /service_accounts
```

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/service_accounts"
```

## 列出用户项目

请参见[用户项目列表](projects.md#list-user-projects)。

## 列出用户的关联数量

获取指定用户以下方面的数量列表

- 项目
- 群组
- 议题
- 合并请求

管理员可以查询任何用户的数据，非管理员只能查询自己的数据。

```plaintext
GET /users/:id/associations_count
```

参数：

| 参数   | 类型      | 是否必需 | 描述              |
|------|---------|------|-----------------|
| `id` | integer | yes  | 用户 ID |

响应示例：

```json
{
  "groups_count": 2,
  "projects_count": 3,
  "issues_count": 8,
  "merge_requests_count": 5
}
```

## 列出 SSH 密钥

获取经过身份验证的用户的 SSH 密钥列表。

```plaintext
GET /user/keys
```

```json
[
  {
    "id": 1,
    "title": "Public key",
    "key": "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAIEAiPWx6WM4lhHNedGfBpPJNPpZ7yKu+dnn1SJejgt4596k6YjzGGphH2TUxwKzxcKDKKezwkpfnxPkSMkuEspGRt/aZZ9wa++Oi7Qkr8prgHc4soW6NUlfDzpvZK2H5E7eQaSeP3SAwGmQKUFHCddNaP0L+hM7zhFNzjFvpaMgJw0=",
    "created_at": "2014-08-01T14:47:39.080Z"
  },
  {
    "id": 3,
    "title": "Another Public key",
    "key": "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAIEAiPWx6WM4lhHNedGfBpPJNPpZ7yKu+dnn1SJejgt4596k6YjzGGphH2TUxwKzxcKDKKezwkpfnxPkSMkuEspGRt/aZZ9wa++Oi7Qkr8prgHc4soW6NUlfDzpvZK2H5E7eQaSeP3SAwGmQKUFHCddNaP0L+hM7zhFNzjFvpaMgJw0=",
    "created_at": "2014-08-01T14:47:39.080Z"
  }
]
```

参数：

- **无**

## 列出用户的 SSH 密钥

获取特定用户的 SSH 密钥列表。

```plaintext
GET /users/:id_or_username/keys
```

| 参数               | 类型     | 是否必需 | 描述                       |
|------------------|--------|------|--------------------------|
| `id_or_username` | string | yes  | 要为其获取 SSH 密钥的用户的 ID 或用户名 |

## 单个 SSH 密钥

获取单个密钥。

```plaintext
GET /user/keys/:key_id
```

参数：

| 参数       | 类型     | 是否必需 | 描述                   |
|----------|--------|------|----------------------|
| `key_id` | string | yes  | SSH 密钥 ID |

```json
{
  "id": 1,
  "title": "Public key",
  "key": "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAIEAiPWx6WM4lhHNedGfBpPJNPpZ7yKu+dnn1SJejgt4596k6YjzGGphH2TUxwKzxcKDKKezwkpfnxPkSMkuEspGRt/aZZ9wa++Oi7Qkr8prgHc4soW6NUlfDzpvZK2H5E7eQaSeP3SAwGmQKUFHCddNaP0L+hM7zhFNzjFvpaMgJw0=",
  "created_at": "2014-08-01T14:47:39.080Z"
}
```

## 特定用户的单个 SSH 密钥

> 引入于极狐GitLab 14.9。

获取特定用户的单个密钥。

```plaintext
GET /users/:id/keys/:key_id
```

参数：

| 参数       | 类型      | 是否必需 | 描述                         |
|----------|---------|------|----------------------------|
| `id`     | integer | yes  | 特定用户的 ID  |
| `key_id` | integer | yes  | SSH 密钥 ID                  |

```json
{
  "id": 1,
  "title": "Public key",
  "key": "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAIEAiPWx6WM4lhHNedGfBpPJNPpZ7yKu+dnn1SJejgt4596k6YjzGGphH2TUxwKzxcKDKKezwkpfnxPkSMkuEspGRt/aZZ9wa++Oi7Qkr8prgHc4soW6NUlfDzpvZK2H5E7eQaSeP3SAwGmQKUFHCddNaP0L+hM7zhFNzjFvpaMgJw0=",
  "created_at": "2014-08-01T14:47:39.080Z"
}
```

## 添加 SSH 密钥

> `usage_type` 参数引入于极狐GitLab 15.7。

创建经过身份验证的用户拥有的新密钥。

```plaintext
POST /user/keys
```

参数：

| 参数           | 类型     | 是否必需 | 描述                                                  |
|--------------|--------|------|-----------------------------------------------------|
| `title`      | string | yes  | 新 SSH 密钥的标题                                         |
| `key`        | string | yes  | 新 SSH 密钥                                            |
| `expires_at` | string | no   | SSH 密钥的过期日期，格式为 ISO 8601（`YYYY-MM-DDTHH:MM:SSZ`） |
| `usage_type` | string | no   | SSH 密钥的使用范围：`auth`、`signing` 或 `auth_and_signing`。默认值：`auth_and_signing`      |

```json
{
  "title": "ABC",
  "key": "ssh-dss AAAAB3NzaC1kc3MAAACBAMLrhYgI3atfrSD6KDas1b/3n6R/HP+bLaHHX6oh+L1vg31mdUqK0Ac/NjZoQunavoyzqdPYhFz9zzOezCrZKjuJDS3NRK9rspvjgM0xYR4d47oNZbdZbwkI4cTv/gcMlquRy0OvpfIvJtjtaJWMwTLtM5VhRusRuUlpH99UUVeXAAAAFQCVyX+92hBEjInEKL0v13c/egDCTQAAAIEAvFdWGq0ccOPbw4f/F8LpZqvWDydAcpXHV3thwb7WkFfppvm4SZte0zds1FJ+Hr8Xzzc5zMHe6J4Nlay/rP4ewmIW7iFKNBEYb/yWa+ceLrs+TfR672TaAgO6o7iSRofEq5YLdwgrwkMmIawa21FrZ2D9SPao/IwvENzk/xcHu7YAAACAQFXQH6HQnxOrw4dqf0NqeKy1tfIPxYYUZhPJfo9O0AmBW2S36pD2l14kS89fvz6Y1g8gN/FwFnRncMzlLY/hX70FSc/3hKBSbH6C6j8hwlgFKfizav21eS358JJz93leOakJZnGb8XlWvz1UJbwCsnR2VEY8Dz90uIk1l/UqHkA= loic@call",
  "expires_at": "2016-01-21T00:00:00.000Z",
  "usage_type": "auth"
}
```

成功时返回状态为 `201 Created` 的创建密钥。如果发生错误时会返回 `400 Bad Request`，并带有错误消息：

```json
{
  "message": {
    "fingerprint": [
      "has already been taken"
    ],
    "key": [
      "has already been taken"
    ]
  }
}
```

## 为用户添加 SSH 密钥 **(FREE SELF)**

> `usage_type` 参数引入于极狐GitLab 15.7。

创建特定用户拥有的新密钥。仅管理员可用。

```plaintext
POST /users/:id/keys
```

参数：

| 参数           | 类型      | 是否必需 | 描述                                                  |
|--------------|---------|------|-----------------------------------------------------|
| `id`         | integer | yes  | 特定用户的 ID                                            |
| `title`      | string  | yes  | 新 SSH 密钥的标题                                         |
| `key`        | string  | yes  | 新 SSH 密钥                                            |
| `expires_at` | string  | no   | SSH 密钥的过期日期，格式为 ISO 8601（`YYYY-MM-DDTHH:MM:SSZ`） |
| `usage_type` | string  | no   | SSH 密钥的使用范围：`auth`、`signing` 或 `auth_and_signing`。默认值：`auth_and_signing`    |

```json
{
  "title": "ABC",
  "key": "ssh-dss AAAAB3NzaC1kc3MAAACBAMLrhYgI3atfrSD6KDas1b/3n6R/HP+bLaHHX6oh+L1vg31mdUqK0Ac/NjZoQunavoyzqdPYhFz9zzOezCrZKjuJDS3NRK9rspvjgM0xYR4d47oNZbdZbwkI4cTv/gcMlquRy0OvpfIvJtjtaJWMwTLtM5VhRusRuUlpH99UUVeXAAAAFQCVyX+92hBEjInEKL0v13c/egDCTQAAAIEAvFdWGq0ccOPbw4f/F8LpZqvWDydAcpXHV3thwb7WkFfppvm4SZte0zds1FJ+Hr8Xzzc5zMHe6J4Nlay/rP4ewmIW7iFKNBEYb/yWa+ceLrs+TfR672TaAgO6o7iSRofEq5YLdwgrwkMmIawa21FrZ2D9SPao/IwvENzk/xcHu7YAAACAQFXQH6HQnxOrw4dqf0NqeKy1tfIPxYYUZhPJfo9O0AmBW2S36pD2l14kS89fvz6Y1g8gN/FwFnRncMzlLY/hX70FSc/3hKBSbH6C6j8hwlgFKfizav21eS358JJz93leOakJZnGb8XlWvz1UJbwCsnR2VEY8Dz90uIk1l/UqHkA= loic@call",
  "expires_at": "2016-01-21T00:00:00.000Z",
  "usage_type": "auth"
}
```

成功时返回状态为 `201 Created` 的已创建密钥。
如果发生错误，则返回 `400 Bad Request`，并附有解释错误的消息：

```json
{
  "message": {
    "fingerprint": [
      "has already been taken"
    ],
    "key": [
      "has already been taken"
    ]
  }
}
```

NOTE:
也会添加[审计实例事件](../administration/audit_events.md#instance-events)中描述的审计事件**(PREMIUM)**。

## 删除当前用户的 SSH 密钥

删除经过身份验证的用户拥有的密钥。
如果成功，返回 `204 No Content` 状态码；如未找到资源，返回 `404`。

```plaintext
DELETE /user/keys/:key_id
```

参数：

| 参数       | 类型      | 是否必需 | 描述        |
|----------|---------|------|-----------|
| `key_id` | integer | yes  | SSH 密钥 ID |

## 删除特定用户的 SSH 密钥 **(FREE SELF)**

删除特定用户拥有的密钥。仅管理员可用。

```plaintext
DELETE /users/:id/keys/:key_id
```

参数：

| 参数       | 类型      | 是否必需 | 描述                         |
|----------|---------|------|----------------------------|
| `id`     | integer | yes  | 特定用户的 ID |
| `key_id` | integer | yes  | SSH 密钥 ID                  |

## 列出所有 GPG 密钥

获取经过身份验证的用户的 GPG 密钥的列表。

```plaintext
GET /user/gpg_keys
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/user/gpg_keys"
```

响应示例：

```json
[
    {
        "id": 1,
        "key": "-----BEGIN PGP PUBLIC KEY BLOCK-----\r\n\r\nxsBNBFVjnlIBCACibzXOLCiZiL2oyzYUaTOCkYnSUhymg3pdbfKtd4mpBa58xKBj\r\nt1pTHVpw3Sk03wmzhM/Ndlt1AV2YhLv++83WKr+gAHFYFiCV/tnY8bx3HqvVoy8O\r\nCfxWhw4QZK7+oYzVmJj8ZJm3ZjOC4pzuegNWlNLCUdZDx9OKlHVXLCX1iUbjdYWa\r\nqKV6tdV8hZolkbyjedQgrpvoWyeSHHpwHF7yk4gNJWMMI5rpcssL7i6mMXb/sDzO\r\nVaAtU5wiVducsOa01InRFf7QSTxoAm6Xy0PGv/k48M6xCALa9nY+BzlOv47jUT57\r\nvilf4Szy9dKD0v9S0mQ+IHB+gNukWrnwtXx5ABEBAAHNFm5hbWUgKGNvbW1lbnQp\r\nIDxlbUBpbD7CwHUEEwECACkFAlVjnlIJEINgJNgv009/AhsDAhkBBgsJCAcDAgYV\r\nCAIJCgsEFgIDAQAAxqMIAFBHuBA8P1v8DtHonIK8Lx2qU23t8Mh68HBIkSjk2H7/\r\noO2cDWCw50jZ9D91PXOOyMPvBWV2IE3tARzCvnNGtzEFRtpIEtZ0cuctxeIF1id5\r\ncrfzdMDsmZyRHAOoZ9VtuD6mzj0ybQWMACb7eIHjZDCee3Slh3TVrLy06YRdq2I4\r\nbjMOPePtK5xnIpHGpAXkB3IONxyITpSLKsA4hCeP7gVvm7r7TuQg1ygiUBlWbBYn\r\niE5ROzqZjG1s7dQNZK/riiU2umGqGuwAb2IPvNiyuGR3cIgRE4llXH/rLuUlspAp\r\no4nlxaz65VucmNbN1aMbDXLJVSqR1DuE00vEsL1AItI=\r\n=XQoy\r\n-----END PGP PUBLIC KEY BLOCK-----",
        "created_at": "2017-09-05T09:17:46.264Z"
    }
]
```

## 获取特定 GPG 密钥

获取经过身份验证的用户的特定 GPG 密钥。

```plaintext
GET /user/gpg_keys/:key_id
```

参数：

| 参数       | 类型      | 是否必需 | 描述                       |
|----------|---------|------|--------------------------|
| `key_id` | integer | yes  | GPG 密钥 ID  |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/user/gpg_keys/1"
```

响应示例：

```json
  {
      "id": 1,
      "key": "-----BEGIN PGP PUBLIC KEY BLOCK-----\r\n\r\nxsBNBFVjnlIBCACibzXOLCiZiL2oyzYUaTOCkYnSUhymg3pdbfKtd4mpBa58xKBj\r\nt1pTHVpw3Sk03wmzhM/Ndlt1AV2YhLv++83WKr+gAHFYFiCV/tnY8bx3HqvVoy8O\r\nCfxWhw4QZK7+oYzVmJj8ZJm3ZjOC4pzuegNWlNLCUdZDx9OKlHVXLCX1iUbjdYWa\r\nqKV6tdV8hZolkbyjedQgrpvoWyeSHHpwHF7yk4gNJWMMI5rpcssL7i6mMXb/sDzO\r\nVaAtU5wiVducsOa01InRFf7QSTxoAm6Xy0PGv/k48M6xCALa9nY+BzlOv47jUT57\r\nvilf4Szy9dKD0v9S0mQ+IHB+gNukWrnwtXx5ABEBAAHNFm5hbWUgKGNvbW1lbnQp\r\nIDxlbUBpbD7CwHUEEwECACkFAlVjnlIJEINgJNgv009/AhsDAhkBBgsJCAcDAgYV\r\nCAIJCgsEFgIDAQAAxqMIAFBHuBA8P1v8DtHonIK8Lx2qU23t8Mh68HBIkSjk2H7/\r\noO2cDWCw50jZ9D91PXOOyMPvBWV2IE3tARzCvnNGtzEFRtpIEtZ0cuctxeIF1id5\r\ncrfzdMDsmZyRHAOoZ9VtuD6mzj0ybQWMACb7eIHjZDCee3Slh3TVrLy06YRdq2I4\r\nbjMOPePtK5xnIpHGpAXkB3IONxyITpSLKsA4hCeP7gVvm7r7TuQg1ygiUBlWbBYn\r\niE5ROzqZjG1s7dQNZK/riiU2umGqGuwAb2IPvNiyuGR3cIgRE4llXH/rLuUlspAp\r\no4nlxaz65VucmNbN1aMbDXLJVSqR1DuE00vEsL1AItI=\r\n=XQoy\r\n-----END PGP PUBLIC KEY BLOCK-----",
      "created_at": "2017-09-05T09:17:46.264Z"
  }
```

## 添加 GPG 密钥

创建经过身份验证的用户拥有的新 GPG 密钥。

```plaintext
POST /user/gpg_keys
```

参数：

| 参数    | 类型     | 是否必需 | 描述        |
|-------|--------|------|-----------|
| `key` | string | yes  | 新的 GPG 密钥 |

```shell

export KEY="$( gpg --armor --export <your_gpg_key_id>)"

curl --data-urlencode "key=-----BEGIN PGP PUBLIC KEY BLOCK-----
> xsBNBFVjnlIBCACibzXOLCiZiL2oyzYUaTOCkYnSUhymg3pdbfKtd4mpBa58xKBj
> t1pTHVpw3Sk03wmzhM/Ndlt1AV2YhLv++83WKr+gAHFYFiCV/tnY8bx3HqvVoy8O
> CfxWhw4QZK7+oYzVmJj8ZJm3ZjOC4pzuegNWlNLCUdZDx9OKlHVXLCX1iUbjdYWa
> qKV6tdV8hZolkbyjedQgrpvoWyeSHHpwHF7yk4gNJWMMI5rpcssL7i6mMXb/sDzO
> VaAtU5wiVducsOa01InRFf7QSTxoAm6Xy0PGv/k48M6xCALa9nY+BzlOv47jUT57
> vilf4Szy9dKD0v9S0mQ+IHB+gNukWrnwtXx5ABEBAAHNFm5hbWUgKGNvbW1lbnQp
> IDxlbUBpbD7CwHUEEwECACkFAlVjnlIJEINgJNgv009/AhsDAhkBBgsJCAcDAgYV
> CAIJCgsEFgIDAQAAxqMIAFBHuBA8P1v8DtHonIK8Lx2qU23t8Mh68HBIkSjk2H7/
> oO2cDWCw50jZ9D91PXOOyMPvBWV2IE3tARzCvnNGtzEFRtpIEtZ0cuctxeIF1id5
> crfzdMDsmZyRHAOoZ9VtuD6mzj0ybQWMACb7eIHjZDCee3Slh3TVrLy06YRdq2I4
> bjMOPePtK5xnIpHGpAXkB3IONxyITpSLKsA4hCeP7gVvm7r7TuQg1ygiUBlWbBYn
> iE5ROzqZjG1s7dQNZK/riiU2umGqGuwAb2IPvNiyuGR3cIgRE4llXH/rLuUlspAp
> o4nlxaz65VucmNbN1aMbDXLJVSqR1DuE00vEsL1AItI=
> =XQoy
> -----END PGP PUBLIC KEY BLOCK-----" \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/user/gpg_keys"
```

响应示例：

```json
[
  {
    "id": 1,
    "key": "-----BEGIN PGP PUBLIC KEY BLOCK-----\nxsBNBFVjnlIBCACibzXOLCiZiL2oyzYUaTOCkYnSUhymg3pdbfKtd4mpBa58xKBj\nt1pTHVpw3Sk03wmzhM/Ndlt1AV2YhLv++83WKr+gAHFYFiCV/tnY8bx3HqvVoy8O\nCfxWhw4QZK7+oYzVmJj8ZJm3ZjOC4pzuegNWlNLCUdZDx9OKlHVXLCX1iUbjdYWa\nqKV6tdV8hZolkbyjedQgrpvoWyeSHHpwHF7yk4gNJWMMI5rpcssL7i6mMXb/sDzO\nVaAtU5wiVducsOa01InRFf7QSTxoAm6Xy0PGv/k48M6xCALa9nY+BzlOv47jUT57\nvilf4Szy9dKD0v9S0mQ+IHB+gNukWrnwtXx5ABEBAAHNFm5hbWUgKGNvbW1lbnQp\nIDxlbUBpbD7CwHUEEwECACkFAlVjnlIJEINgJNgv009/AhsDAhkBBgsJCAcDAgYV\nCAIJCgsEFgIDAQAAxqMIAFBHuBA8P1v8DtHonIK8Lx2qU23t8Mh68HBIkSjk2H7/\noO2cDWCw50jZ9D91PXOOyMPvBWV2IE3tARzCvnNGtzEFRtpIEtZ0cuctxeIF1id5\ncrfzdMDsmZyRHAOoZ9VtuD6mzj0ybQWMACb7eIHjZDCee3Slh3TVrLy06YRdq2I4\nbjMOPePtK5xnIpHGpAXkB3IONxyITpSLKsA4hCeP7gVvm7r7TuQg1ygiUBlWbBYn\niE5ROzqZjG1s7dQNZK/riiU2umGqGuwAb2IPvNiyuGR3cIgRE4llXH/rLuUlspAp\no4nlxaz65VucmNbN1aMbDXLJVSqR1DuE00vEsL1AItI=\n=XQoy\n-----END PGP PUBLIC KEY BLOCK-----",
    "created_at": "2017-09-05T09:17:46.264Z"
  }
]
```

## 删除 GPG 密钥

删除当前经过身份验证的用户拥有的 GPG 密钥。

```plaintext
DELETE /user/gpg_keys/:key_id
```

参数：

| 参数       | 类型      | 是否必需 | 描述                       |
|----------|---------|------|--------------------------|
| `key_id` | integer | yes  | GPG 密钥 ID |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/user/gpg_keys/1"
```

如果成功，则返回 `204 No Content`；如果无法找到密钥，则返回 `404 Not Found`。

## 列出特定用户的所有 GPG 密钥

获取特定用户的 GPG 密钥的列表。此端点可以未授权访问。

```plaintext
GET /users/:id/gpg_keys
```

参数：

| 参数   | 类型      | 是否必需 | 描述                |
|------|---------|------|-------------------|
| `id` | integer | yes  | 用户 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/2/gpg_keys"
```

响应示例：

```json
[
    {
        "id": 1,
        "key": "-----BEGIN PGP PUBLIC KEY BLOCK-----\r\n\r\nxsBNBFVjnlIBCACibzXOLCiZiL2oyzYUaTOCkYnSUhymg3pdbfKtd4mpBa58xKBj\r\nt1pTHVpw3Sk03wmzhM/Ndlt1AV2YhLv++83WKr+gAHFYFiCV/tnY8bx3HqvVoy8O\r\nCfxWhw4QZK7+oYzVmJj8ZJm3ZjOC4pzuegNWlNLCUdZDx9OKlHVXLCX1iUbjdYWa\r\nqKV6tdV8hZolkbyjedQgrpvoWyeSHHpwHF7yk4gNJWMMI5rpcssL7i6mMXb/sDzO\r\nVaAtU5wiVducsOa01InRFf7QSTxoAm6Xy0PGv/k48M6xCALa9nY+BzlOv47jUT57\r\nvilf4Szy9dKD0v9S0mQ+IHB+gNukWrnwtXx5ABEBAAHNFm5hbWUgKGNvbW1lbnQp\r\nIDxlbUBpbD7CwHUEEwECACkFAlVjnlIJEINgJNgv009/AhsDAhkBBgsJCAcDAgYV\r\nCAIJCgsEFgIDAQAAxqMIAFBHuBA8P1v8DtHonIK8Lx2qU23t8Mh68HBIkSjk2H7/\r\noO2cDWCw50jZ9D91PXOOyMPvBWV2IE3tARzCvnNGtzEFRtpIEtZ0cuctxeIF1id5\r\ncrfzdMDsmZyRHAOoZ9VtuD6mzj0ybQWMACb7eIHjZDCee3Slh3TVrLy06YRdq2I4\r\nbjMOPePtK5xnIpHGpAXkB3IONxyITpSLKsA4hCeP7gVvm7r7TuQg1ygiUBlWbBYn\r\niE5ROzqZjG1s7dQNZK/riiU2umGqGuwAb2IPvNiyuGR3cIgRE4llXH/rLuUlspAp\r\no4nlxaz65VucmNbN1aMbDXLJVSqR1DuE00vEsL1AItI=\r\n=XQoy\r\n-----END PGP PUBLIC KEY BLOCK-----",
        "created_at": "2017-09-05T09:17:46.264Z"
    }
]
```

## 获取特定用户的特定 GPG 密钥

获取特定用户的特定 GPG 密钥。引入于 13.5。此端点可以无管理员授权即可访问。

```plaintext
GET /users/:id/gpg_keys/:key_id
```

参数：

| 参数       | 类型      | 是否必需 | 描述                       |
|----------|---------|------|--------------------------|
| `id`     | integer | yes  | 用户 ID        |
| `key_id` | integer | yes  | GPG 密钥 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/2/gpg_keys/1"
```

响应示例：

```json
  {
      "id": 1,
      "key": "-----BEGIN PGP PUBLIC KEY BLOCK-----\r\n\r\nxsBNBFVjnlIBCACibzXOLCiZiL2oyzYUaTOCkYnSUhymg3pdbfKtd4mpBa58xKBj\r\nt1pTHVpw3Sk03wmzhM/Ndlt1AV2YhLv++83WKr+gAHFYFiCV/tnY8bx3HqvVoy8O\r\nCfxWhw4QZK7+oYzVmJj8ZJm3ZjOC4pzuegNWlNLCUdZDx9OKlHVXLCX1iUbjdYWa\r\nqKV6tdV8hZolkbyjedQgrpvoWyeSHHpwHF7yk4gNJWMMI5rpcssL7i6mMXb/sDzO\r\nVaAtU5wiVducsOa01InRFf7QSTxoAm6Xy0PGv/k48M6xCALa9nY+BzlOv47jUT57\r\nvilf4Szy9dKD0v9S0mQ+IHB+gNukWrnwtXx5ABEBAAHNFm5hbWUgKGNvbW1lbnQp\r\nIDxlbUBpbD7CwHUEEwECACkFAlVjnlIJEINgJNgv009/AhsDAhkBBgsJCAcDAgYV\r\nCAIJCgsEFgIDAQAAxqMIAFBHuBA8P1v8DtHonIK8Lx2qU23t8Mh68HBIkSjk2H7/\r\noO2cDWCw50jZ9D91PXOOyMPvBWV2IE3tARzCvnNGtzEFRtpIEtZ0cuctxeIF1id5\r\ncrfzdMDsmZyRHAOoZ9VtuD6mzj0ybQWMACb7eIHjZDCee3Slh3TVrLy06YRdq2I4\r\nbjMOPePtK5xnIpHGpAXkB3IONxyITpSLKsA4hCeP7gVvm7r7TuQg1ygiUBlWbBYn\r\niE5ROzqZjG1s7dQNZK/riiU2umGqGuwAb2IPvNiyuGR3cIgRE4llXH/rLuUlspAp\r\no4nlxaz65VucmNbN1aMbDXLJVSqR1DuE00vEsL1AItI=\r\n=XQoy\r\n-----END PGP PUBLIC KEY BLOCK-----",
      "created_at": "2017-09-05T09:17:46.264Z"
  }
```

## 为特定用户添加 GPG 密钥 **(FREE SELF)**

创建特定用户拥有的新 GPG 密钥。仅管理员可用。

```plaintext
POST /users/:id/gpg_keys
```

参数：

| 参数       | 类型      | 是否必需 | 描述                       |
|----------|---------|------|--------------------------|
| `id`     | integer | yes  | 用户 ID                    |
| `key_id` | integer | yes  | GPG 密钥 ID |

```shell
curl --data-urlencode "key=-----BEGIN PGP PUBLIC KEY BLOCK-----
> xsBNBFVjnlIBCACibzXOLCiZiL2oyzYUaTOCkYnSUhymg3pdbfKtd4mpBa58xKBj
> t1pTHVpw3Sk03wmzhM/Ndlt1AV2YhLv++83WKr+gAHFYFiCV/tnY8bx3HqvVoy8O
> CfxWhw4QZK7+oYzVmJj8ZJm3ZjOC4pzuegNWlNLCUdZDx9OKlHVXLCX1iUbjdYWa
> qKV6tdV8hZolkbyjedQgrpvoWyeSHHpwHF7yk4gNJWMMI5rpcssL7i6mMXb/sDzO
> VaAtU5wiVducsOa01InRFf7QSTxoAm6Xy0PGv/k48M6xCALa9nY+BzlOv47jUT57
> vilf4Szy9dKD0v9S0mQ+IHB+gNukWrnwtXx5ABEBAAHNFm5hbWUgKGNvbW1lbnQp
> IDxlbUBpbD7CwHUEEwECACkFAlVjnlIJEINgJNgv009/AhsDAhkBBgsJCAcDAgYV
> CAIJCgsEFgIDAQAAxqMIAFBHuBA8P1v8DtHonIK8Lx2qU23t8Mh68HBIkSjk2H7/
> oO2cDWCw50jZ9D91PXOOyMPvBWV2IE3tARzCvnNGtzEFRtpIEtZ0cuctxeIF1id5
> crfzdMDsmZyRHAOoZ9VtuD6mzj0ybQWMACb7eIHjZDCee3Slh3TVrLy06YRdq2I4
> bjMOPePtK5xnIpHGpAXkB3IONxyITpSLKsA4hCeP7gVvm7r7TuQg1ygiUBlWbBYn
> iE5ROzqZjG1s7dQNZK/riiU2umGqGuwAb2IPvNiyuGR3cIgRE4llXH/rLuUlspAp
> o4nlxaz65VucmNbN1aMbDXLJVSqR1DuE00vEsL1AItI=
> =XQoy
> -----END PGP PUBLIC KEY BLOCK-----" \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/2/gpg_keys"
```

响应示例：

```json
[
  {
    "id": 1,
    "key": "-----BEGIN PGP PUBLIC KEY BLOCK-----\nxsBNBFVjnlIBCACibzXOLCiZiL2oyzYUaTOCkYnSUhymg3pdbfKtd4mpBa58xKBj\nt1pTHVpw3Sk03wmzhM/Ndlt1AV2YhLv++83WKr+gAHFYFiCV/tnY8bx3HqvVoy8O\nCfxWhw4QZK7+oYzVmJj8ZJm3ZjOC4pzuegNWlNLCUdZDx9OKlHVXLCX1iUbjdYWa\nqKV6tdV8hZolkbyjedQgrpvoWyeSHHpwHF7yk4gNJWMMI5rpcssL7i6mMXb/sDzO\nVaAtU5wiVducsOa01InRFf7QSTxoAm6Xy0PGv/k48M6xCALa9nY+BzlOv47jUT57\nvilf4Szy9dKD0v9S0mQ+IHB+gNukWrnwtXx5ABEBAAHNFm5hbWUgKGNvbW1lbnQp\nIDxlbUBpbD7CwHUEEwECACkFAlVjnlIJEINgJNgv009/AhsDAhkBBgsJCAcDAgYV\nCAIJCgsEFgIDAQAAxqMIAFBHuBA8P1v8DtHonIK8Lx2qU23t8Mh68HBIkSjk2H7/\noO2cDWCw50jZ9D91PXOOyMPvBWV2IE3tARzCvnNGtzEFRtpIEtZ0cuctxeIF1id5\ncrfzdMDsmZyRHAOoZ9VtuD6mzj0ybQWMACb7eIHjZDCee3Slh3TVrLy06YRdq2I4\nbjMOPePtK5xnIpHGpAXkB3IONxyITpSLKsA4hCeP7gVvm7r7TuQg1ygiUBlWbBYn\niE5ROzqZjG1s7dQNZK/riiU2umGqGuwAb2IPvNiyuGR3cIgRE4llXH/rLuUlspAp\no4nlxaz65VucmNbN1aMbDXLJVSqR1DuE00vEsL1AItI=\n=XQoy\n-----END PGP PUBLIC KEY BLOCK-----",
    "created_at": "2017-09-05T09:17:46.264Z"
  }
]
```

## 删除特定用户的 GPG 密钥 **(FREE SELF)**

删除特定用户的 GPG 密钥。仅对管理员可用。

```plaintext
DELETE /users/:id/gpg_keys/:key_id
```

参数：

| 参数       | 类型      | 是否必需 | 描述                       |
|----------|---------|------|--------------------------|
| `id`     | integer | yes  | 用户 ID                    |
| `key_id` | integer | yes  | GPG 密钥 ID |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/2/gpg_keys/1"
```

## 列出电子邮件

获取经过身份验证的用户电子邮件的列表。

NOTE:
此端点当前不返回主要电子邮件地址。

```plaintext
GET /user/emails
```

```json
[
  {
    "id": 1,
    "email": "email@example.com",
    "confirmed_at" : "2021-03-26T19:07:56.248Z"
  },
  {
    "id": 3,
    "email": "email2@example.com",
    "confirmed_at" : null
  }
]
```

参数：

- **无**

## 列出用户的电子邮件 **(FREE SELF)**

获取特定用户电子邮件的列表。仅管理员可用。

NOTE:
此端点不返回主要电子邮件地址。

```plaintext
GET /users/:id/emails
```

参数：

| 参数   | 类型      | 是否必需 | 描述                         |
|------|---------|------|----------------------------|
| `id` | integer | yes  | 特定用户的 ID |

## 单个电子邮件

获取单个电子邮件。

```plaintext
GET /user/emails/:email_id
```

参数：

| 参数         | 类型      | 是否必需 | 描述      |
|------------|---------|------|---------|
| `email_id` | integer | yes  | 电子邮件 ID |

```json
{
  "id": 1,
  "email": "email@example.com",
  "confirmed_at" : "2021-03-26T19:07:56.248Z"
}
```

<a id="add-email"></a>"

## 添加电子邮件

创建经过身份验证的用户拥有的新电子邮件。

```plaintext
POST /user/emails
```

参数：

| 参数      | 类型     | 是否必需 | 描述     |
|---------|--------|------|--------|
| `email` | string | yes  | 电子邮件地址 |

```json
{
  "id": 4,
  "email": "email@example.com",
  "confirmed_at" : "2021-03-26T19:07:56.248Z"
}
```

如果成功，则返回创建的电子邮件，并带有状态码 `201 Created`。如果出现错误，则返回 `400 Bad Request` 和带有错误原因的消息：

```json
{
  "message": {
    "email": [
      "has already been taken"
    ]
  }
}
```

## 为用户添加电子邮件 **(FREE SELF)**

创建特定用户拥有的新电子邮件。仅管理员可用。

```plaintext
POST /users/:id/emails
```

参数：

| 参数                  | 类型      | 是否必需 | 描述                                   |
|---------------------|---------|------|--------------------------------------|
| `id`                | string  | yes  | 特定用户的 ID                             |
| `email`             | string  | yes  | 电子邮件地址                               |
| `skip_confirmation` | boolean | no   | 跳过确认并假设电子邮件已经过验证 - 真或假（默认） |

## 删除当前用户的电子邮件地址

删除经过身份验证的用户拥有的电子邮件地址。

如果成功，则返回 `204 No Content` 状态码；如果未找到资源，则返回 `404`。

无法删除主要电子邮件地址。

```plaintext
DELETE /user/emails/:email_id
```

参数：

| 参数         | 类型      | 是否必需 | 描述      |
|------------|---------|------|---------|
| `email_id` | integer | yes  | 电子邮件 ID |

## 删除特定用户的电子邮件 **(FREE SELF)**

先决条件：

- 您必须是私有化部署的极狐GitLab 实例的管理员。

删除特定用户拥有的电子邮件地址。无法删除主要电子邮件地址。

```plaintext
DELETE /users/:id/emails/:email_id
```

参数：

| 参数         | 类型      | 是否必需 | 描述                         |
|------------|---------|------|----------------------------|
| `id`       | integer | yes  | 特定用户的 ID |
| `email_id` | integer | yes  | 电子邮件 ID                    |

## 禁用用户 **(FREE SELF)**

禁用特定用户。仅管理员可用。

```plaintext
POST /users/:id/block
```

参数：

| 参数   | 类型      | 是否必需 | 描述                         |
|------|---------|------|----------------------------|
| `id` | integer | yes  | 特定用户的 ID |

返回：

- 成功：`201 OK`
- 无法找到用户：`404 User Not Found`
- 禁用用户：`403 Forbidden`
  - 用户通过 LDAP 被禁用
  - 内部用户

## 取消禁用用户 **(FREE SELF)**

取消禁用特定用户。仅管理员可用。

```plaintext
POST /users/:id/unblock
```

参数：

| 参数   | 类型      | 是否必需 | 描述                         |
|------|---------|------|----------------------------|
| `id` | integer | yes  | 特定用户的 ID |

如果成功，则返回 `201 OK`；如果无法找到用户，则返回 `404 User Not Found`；如果取消禁用通过 LDAP 同步禁用的用户，则返回 `403 Forbidden`。

## 冻结用户 **(FREE SELF)**

> 引入于极狐GitLab 12.4。

冻结特定用户。仅管理员可用。

```plaintext
POST /users/:id/deactivate
```

参数：

| 参数   | 类型      | 是否必需 | 描述                         |
|------|---------|------|----------------------------|
| `id` | integer | yes  | 特定用户的 ID |

返回：

- 成功：`201 OK` 
- 无法找到用户：`404 User Not Found`
- 冻结以下用户：`403 Forbidden`：
  - 被管理员或被 LDAP 同步禁用的用户
  - 非[休眠](../user/admin_area/moderate_users.md#automatically-deactivate-dormant-users)的用户
  - 内部的用户

## 激活用户 **(FREE SELF)**

> 引入于极狐GitLab 12.4。

激活特定用户。仅管理员可用。

```plaintext
POST /users/:id/activate
```

参数：

| 参数   | 类型      | 是否必需 | 描述                         |
|------|---------|------|----------------------------|
| `id` | integer | yes  | 特定用户的 ID |

返回：

- 成功：`201 OK`
- 无法找到用户：`404 User Not Found`
- 用户因为被管理员或 LDAP 同步禁用而无法激活：`403 Forbidden` 

## 封禁用户 **(FREE SELF)**

> 引入于极狐GitLab 14.3。

封禁特定用户。仅管理员可用。

```plaintext
POST /users/:id/ban
```

参数：

- `id` （必需）- 特定用户的 ID

返回：

- 成功：`201 OK`
- 无法找到用户：`404 User Not Found`
- 试图封禁未激活的用户：`403 Forbidden`

## 解禁用户 **(FREE SELF)**

> 引入于极狐GitLab 14.3。

解禁特定用户。仅管理员可用。

```plaintext
POST /users/:id/unban
```

参数：

- `id`（必需）- 特定用户的 ID 

返回：

- 成功：`201 OK`
- 无法找到用户：`404 User Not Found` 
- 试图解禁未封禁的用户：`403 Forbidden`

## 获取用户贡献事件

详情请参见[事件 API 文档](events.md#get-user-contribution-events)。

## 获取用户的所有模拟令牌 **(FREE SELF)**

需要管理员访问权限。
 
检索用户的每个模拟令牌。使用分页参数 `page` 和 `per_page` 来限制模拟令牌列表。

```plaintext
GET /users/:user_id/impersonation_tokens
```

参数：

| 参数        | 类型      | 是否必需 | 描述                                  |
|-----------|---------|------|-------------------------------------|
| `user_id` | integer | yes  | 用户 ID                               |
| `state`   | string  | no   | 根据状态过滤令牌（`all`、`active`、`inactive`） |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/42/impersonation_tokens"
```

响应示例：

```json
[
   {
      "active" : true,
      "user_id" : 2,
      "scopes" : [
         "api"
      ],
      "revoked" : false,
      "name" : "mytoken",
      "id" : 2,
      "created_at" : "2017-03-17T17:18:09.283Z",
      "impersonation" : true,
      "expires_at" : "2017-04-04"
   },
   {
      "active" : false,
      "user_id" : 2,
      "scopes" : [
         "read_user"
      ],
      "revoked" : true,
      "name" : "mytoken2",
      "created_at" : "2017-03-17T17:19:28.697Z",
      "id" : 3,
      "impersonation" : true,
      "expires_at" : "2017-04-14"
   }
]
```

## 批准用户 **(FREE SELF)**

> 引入于极狐GitLab 13.7。

批注特定用户。仅管理员可用。

```plaintext
POST /users/:id/approve
```

参数：

| 参数   | 类型      | 是否必需 | 描述                         |
|------|---------|------|----------------------------|
| `id` | integer | yes  | 特定用户的 ID  |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/42/approve"
```

返回：

- 成功：`201 Created`
- 无法找到用户：`404 User Not Found`
- 用户因为被管理员或 LDAP 同步禁用而无法获得批准：`403 Forbidden`
- 用户被冻结：`409 Conflict`

响应示例：

```json
{ "message": "Success" }
```

```json
{ "message": "404 User Not Found" }
```

```json
{ "message": "The user you are trying to approve is not pending approval" }
```

## 拒绝用户 **(FREE SELF)**

> 引入于极狐GitLab 14.3。

拒绝[待批准](../user/admin_area/moderate_users.md#users-pending-approval)的特定用户。仅管理员可用。

```plaintext
POST /users/:id/reject
```

参数：

- `id`（必需）- 特定用户的 ID

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/42/reject"
```

返回：

- 成功：`200 OK`
- 未鉴权为管理员：`403 Forbidden`
- 无法找到用户：`404 User Not Found`
- 用户未待批准：`409 Conflict`

响应示例：

```json
{ "message": "Success" }
```

```json
{ "message": "404 User Not Found" }
```

```json
{ "message": "User does not have a pending request" }
```

## 获取用户的模拟令牌 **(FREE SELF)**

> 需要管理员权限。

显示用户的模拟令牌。

```plaintext
GET /users/:user_id/impersonation_tokens/:impersonation_token_id
```

参数：

| 参数                       | 类型      | 是否必需 | 描述      |
|--------------------------|---------|------|---------|
| `user_id`                | integer | yes  | 用户 ID   |
| `impersonation_token_id` | integer | yes  | 模拟令牌 ID |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/42/impersonation_tokens/2"
```

响应示例：

```json
{
   "active" : true,
   "user_id" : 2,
   "scopes" : [
      "api"
   ],
   "revoked" : false,
   "name" : "mytoken",
   "id" : 2,
   "created_at" : "2017-03-17T17:18:09.283Z",
   "impersonation" : true,
   "expires_at" : "2017-04-04"
}
```

<a id="create-an-impersonation-token"></a>

## 创建模拟令牌 **(FREE SELF)**

需要管理员访问权限。令牌值仅返回一次，请正确保存，因为只能获取一次。

创建新的模拟令牌。仅管理员可用。
您只能创建模拟令牌来模拟用户并执行 API 调用和 Git 读写。用户在他们的个人资料设置页面中看不到这些令牌。

```plaintext
POST /users/:user_id/impersonation_tokens
```

| 参数           | 类型      | 是否必需 | 描述                              |
|--------------|---------|------|---------------------------------|
| `user_id`    | integer | yes  | 用户 ID                           |
| `name`       | string  | yes  | 模拟令牌的名称                         |
| `expires_at` | date    | no   | 模拟令牌的过期日期，格式为 ISO（`YYYY-MM-DD`） |
| `scopes`     | array   | yes  | 模拟令牌的范围数组（`api`、`read_user`）    |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --data "name=mytoken" --data "expires_at=2017-04-04" \
     --data "scopes[]=api" "https://gitlab.example.com/api/v4/users/42/impersonation_tokens"
```

响应示例：

```json
{
   "id" : 2,
   "revoked" : false,
   "user_id" : 2,
   "scopes" : [
      "api"
   ],
   "token" : "EsMo-vhKfXGwX9RKrwiy",
   "active" : true,
   "impersonation" : true,
   "name" : "mytoken",
   "created_at" : "2017-03-17T17:18:09.283Z",
   "expires_at" : "2017-04-04"
}
```

## 撤回模拟令牌 **(FREE SELF)**

需要管理员访问权限。

撤回模拟令牌。

```plaintext
DELETE /users/:user_id/impersonation_tokens/:impersonation_token_id
```

参数：

| 参数                       | 类型      | 是否必需 | 描述                                 |
|--------------------------|---------|------|------------------------------------|
| `user_id`                | integer | yes  | 用户 ID                   |
| `impersonation_token_id` | integer | yes  | 模拟令牌 ID  |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/42/impersonation_tokens/1"
```

<a id="create-a-personal-access-token"></a>

## 创建个人访问令牌 **(FREE SELF)**

> - 引入于极狐GitLab 13.6。
> - 功能标志移动于极狐GitLab 13.8。

使用此 API 创建新的个人访问令牌。令牌值仅返回一次，请正确保存，因为只能获取一次。此 API 仅供极狐GitLab 管理员使用。

```plaintext
POST /users/:user_id/personal_access_tokens
```

| 参数           | 类型      | 是否必需 | 描述                                                                                                                            |
|--------------|---------|------|-------------------------------------------------------------------------------------------------------------------------------|
| `user_id`    | integer | yes  | 用户 ID                                                                                                                         |
| `name`       | string  | yes  | 个人访问令牌名称                                                                                                                      |
| `expires_at` | date    | no   | 个人模拟令牌的过期日期，格式为 ISO（`YYYY-MM-DD`）                                                                                             |
| `scopes`     | array   | yes  | 个人访问令牌的范围数组。可能的值请参见[个人访问令牌范围](../user/profile/personal_access_tokens.md#personal-access-token-scopes)|

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --data "name=mytoken" --data "expires_at=2017-04-04" \
     --data "scopes[]=api" "https://gitlab.example.com/api/v4/users/42/personal_access_tokens"
```

响应示例：

```json
{
    "id": 3,
    "name": "mytoken",
    "revoked": false,
    "created_at": "2020-10-14T11:58:53.526Z",
    "scopes": [
        "api"
    ],
    "user_id": 42,
    "active": true,
    "expires_at": "2020-12-31",
    "token": "ggbfKkC4n-Lujy8jwCR2"
}
```

<a id="get-user-activities"></a>

## 获取用户活动 **(FREE SELF)**

先决条件：

- 您必须是管理员。

获取所有用户的最后活动日期，从最旧到最新排序。

更新用户事件时间戳的活动（`last_activity_on` 和 `current_sign_in_at`）是：

- Git HTTP/SSH 活动（例如克隆、推送）
- 用户登录极狐GitLab
- 用户访问与仪表板、项目、议题和合并请求相关的页面（引入于 11.8）
- 使用 API 的用户
- 使用 GraphQL API 的用户

默认情况下，显示过去 6 个月内所有用户的活动，但可以使用 `from` 参数进行修改。

```plaintext
GET /user/activities
```

参数：

| 参数     | 类型     | 是否必需 | 描述                                               |
|--------|--------|------|--------------------------------------------------|
| `from` | string | no   | 日期字符串，格式为：`YEAR-MM-DD`。例如：`2016-03-11`。默认为 6 个月前 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/user/activities"
```

响应示例：

```json
[
  {
    "username": "user1",
    "last_activity_on": "2015-12-14",
    "last_activity_at": "2015-12-14"
  },
  {
    "username": "user2",
    "last_activity_on": "2015-12-15",
    "last_activity_at": "2015-12-15"
  },
  {
    "username": "user3",
    "last_activity_on": "2015-12-16",
    "last_activity_at": "2015-12-16"
  }
]
```

`last_activity_at` 已废弃，使用 `last_activity_on` 代替。

## 用户成员资格 **(FREE SELF)**

> 引入于极狐GitLab 12.8。

先决条件：

- 您必须是管理员。

列出用户所属的所有项目和群组。
返回成员资格的 `source_id`、`source_name`、`source_type` 和 `access_level`。
源可以是 `Namespace`（代表一个群组）或 `Project` 类型。响应仅代表直接成员资格。不包括继承的成员资格，例如子群组中的成员资格。
访问级别由整数值表示。有关更多详细信息，请阅读[访问级别值](access_requests.md#valid-access-levels)。

```plaintext
GET /users/:id/memberships
```

参数：

| 参数     | 类型      | 是否必需 | 描述                                     |
|--------|---------|------|----------------------------------------|
| `id`   | integer | yes  | 特定用户的 ID                               |
| `type` | string  | no   | 按照类型过滤成员资格。可以是 `Project` 或 `Namespace` |

返回：

- 成功：`200 OK`
- 无法找到用户：`404 User Not Found` 
- 不被管理员请求：`403 Forbidden` 
- 不支持请求类型：`400 Bad Request`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/:user_id/memberships"
```

响应示例：

```json
[
  {
    "source_id": 1,
    "source_name": "Project one",
    "source_type": "Project",
    "access_level": "20"
  },
  {
    "source_id": 3,
    "source_name": "Group three",
    "source_type": "Namespace",
    "access_level": "20"
  }
]
```

## 禁用双重身份验证 **(FREE SELF)**

> 引入于极狐GitLab 15.2。

先决条件：

- 您必须是管理员。

为指定用户禁用双重身份验证 (2FA)。

管理员不能为自己的用户帐户或使用 API 的其他管理员禁用 2FA。<!--相反，他们可以[使用 Rails 控制台](../security/two_factor_authentication.md#for-a-single-user)禁用管理员的 2FA。-->

```plaintext
PATCH /users/:id/disable_two_factor
```

参数：

| 参数   | 类型      | 是否必需 | 描述                |
|------|---------|------|-------------------|
| `id` | integer | yes  | 用户 ID  |

```shell
curl --request PATCH --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/users/1/disable_two_factor"
```

返回：

- `204 No content`：成功
- `400 Bad request`：特定用户未开启双重身份验证
- `403 Forbidden`：未鉴权为管理员 
- `404 User Not Found`：无法找到用户

<a id="create-a-runner"></a>

## 创建 Runner **(FREE)**

创建链接到当前用户的 Runner。

先决条件：

- 您必须是管理员或具有目标命名空间或项目的所有者角色。
- 对于 `instance_type`，您必须是极狐GitLab 实例的管理员。

请务必复制或保存响应中的 `token`，因为该值无法再次检索。

```plaintext
POST /user/runners
```

|参数            | 类型           | 是否必需         | 描述                                                                                               |
|----------------------|--------------|--------------|--------------------------------------------------------------------------------------------------|
| `runner_type`        | string       | yes          | 指定 Runner 的范围； `instance_type`、`group_type` 或 `project_type`                                     |
| `group_id`           | integer      | no           | 在其中创建 Runner 的群组的 ID。如果 `runner_type` 为 `group_type`，则必填                                         |
| `project_id`         | integer      | no           | 在其中创建 Runner 的项目的 ID。如果 `runner_type` 为 `project_type`，则必填                                       |
| `description`        | string       | no           | Runner 描述                                                                                        |
| `paused`             | boolean      | no           | 指定 Runner 是否应该忽略新作业                                                                              |
| `locked`             | boolean      | no           | 指定是否应为当前项目锁定 Runner                                                                              |
| `run_untagged`       | boolean      | no           | 指定 Runner 是否应处理未打标签的作业                                                                           |
| `tag_list`           | string array | no           | Runner 标签列表                                                                                      |
| `access_level`       | string       | no           | Runner 的访问级别：`not_protected` 或 `ref_protected`                                                   |
| `maximum_timeout`    | integer      | no           | 限制 Runner 可以运行作业的时间量（以秒为单位）的最大超时时间                                                               |
| `maintenance_note`   | string       | no           | Runner 的自由格式的维护说明（1024 个字符）                  |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --data "runner_type=instance_type" \
     "https://gitlab.example.com/api/v4/user/runners"
```

响应示例：

```json
{
    "id": 9171,
    "token": "glrt-kyahzxLaj4Dc1jQf4xjX",
    "token_expires_at": null
}
```
