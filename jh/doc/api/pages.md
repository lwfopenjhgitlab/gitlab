---
stage: Plan
group: Knowledge
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Pages API **(FREE)**

管理[极狐GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) 的端点。

您必须开启极狐GitLab Pages 功能才能使用这些端点。详情请参见[管理](../administration/pages/index.md)和[使用](../user/project/pages/index.md)此功能。

## 取消发布 Pages

先决条件：

- 您必须对实例具管理员访问权限。

移除 Pages。

```plaintext
DELETE /projects/:id/pages
```

| 参数   | 类型             | 是否必需 | 描述                                                            |
|------|----------------|------|---------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |


```shell
curl --request 'DELETE' --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/2/pages"
```
