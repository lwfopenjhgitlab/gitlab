---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组集群 API（基于证书）（已废弃） **(FREE)**

> - <!--[Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/30213) -->引入于极狐GitLab 12.1。
> - <!--[Deprecated](https://gitlab.com/groups/gitlab-org/configure/-/epics/8) -->废弃于极狐GitLab 14.5。

WARNING:
<!--[deprecated](https://gitlab.com/groups/gitlab-org/configure/-/epics/8) -->此功能废弃于极狐GitLab 14.5。

类似于[项目级别](../user/project/clusters/index.md)和实例级别<!--[实例级别](../user/instance/clusters/index.md)-->的 Kubernetes 集群，群组级别的 Kubernetes 集群允许您将 Kubernetes 集群连接到您的群组，使您能够在多个项目中使用相同的集群。

用户至少需要该群组的维护者角色才能使用这些端点。

## 列出群组集群

返回群组集群列表。

```plaintext
GET /groups/:id/clusters
```

参数：

| 参数   | 类型             | 是否必需 | 描述                                                    |
|------|----------------|------|-------------------------------------------------------|
| `id` | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/groups/26/clusters"
```

响应示例：

```json
[
  {
    "id":18,
    "name":"cluster-1",
    "domain":"example.com",
    "created_at":"2019-01-02T20:18:12.563Z",
    "managed": true,
    "enabled": true,
    "provider_type":"user",
    "platform_type":"kubernetes",
    "environment_scope":"*",
    "cluster_type":"group_type",
    "user":
    {
      "id":1,
      "name":"Administrator",
      "username":"root",
      "state":"active",
      "avatar_url":"https://www.gravatar.com/avatar/4249f4df72b..",
      "web_url":"https://gitlab.example.com/root"
    },
    "platform_kubernetes":
    {
      "api_url":"https://104.197.68.152",
      "authorization_type":"rbac",
      "ca_cert":"-----BEGIN CERTIFICATE-----\r\nhFiK1L61owwDQYJKoZIhvcNAQELBQAw\r\nLzEtMCsGA1UEAxMkZDA1YzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM4ZDBj\r\nMB4XDTE4MTIyNzIwMDM1MVoXDTIzMTIyNjIxMDM1MVowLzEtMCsGA1UEAxMkZDA1\r\nYzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM.......-----END CERTIFICATE-----"
    },
    "management_project":
    {
      "id":2,
      "description":null,
      "name":"project2",
      "name_with_namespace":"John Doe8 / project2",
      "path":"project2",
      "path_with_namespace":"namespace2/project2",
      "created_at":"2019-10-11T02:55:54.138Z"
    }
  },
  {
    "id":19,
    "name":"cluster-2",
    ...
  }
]
```

## 获取单个群组集群

获取单个群组集群。

```plaintext
GET /groups/:id/clusters/:cluster_id
```

参数：

| 参数           | 类型             | 是否必需 | 描述                                                    |
|--------------|----------------|------|-------------------------------------------------------|
| `id`         | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `cluster_id` | integer        | yes  | 集群 ID                                 |

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/groups/26/clusters/18"
```

响应示例：

```json
{
  "id":18,
  "name":"cluster-1",
  "domain":"example.com",
  "created_at":"2019-01-02T20:18:12.563Z",
  "managed": true,
  "enabled": true,
  "provider_type":"user",
  "platform_type":"kubernetes",
  "environment_scope":"*",
  "cluster_type":"group_type",
  "user":
  {
    "id":1,
    "name":"Administrator",
    "username":"root",
    "state":"active",
    "avatar_url":"https://www.gravatar.com/avatar/4249f4df72b..",
    "web_url":"https://gitlab.example.com/root"
  },
  "platform_kubernetes":
  {
    "api_url":"https://104.197.68.152",
    "authorization_type":"rbac",
    "ca_cert":"-----BEGIN CERTIFICATE-----\r\nhFiK1L61owwDQYJKoZIhvcNAQELBQAw\r\nLzEtMCsGA1UEAxMkZDA1YzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM4ZDBj\r\nMB4XDTE4MTIyNzIwMDM1MVoXDTIzMTIyNjIxMDM1MVowLzEtMCsGA1UEAxMkZDA1\r\nYzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM.......-----END CERTIFICATE-----"
  },
  "management_project":
  {
    "id":2,
    "description":null,
    "name":"project2",
    "name_with_namespace":"John Doe8 / project2",
    "path":"project2",
    "path_with_namespace":"namespace2/project2",
    "created_at":"2019-10-11T02:55:54.138Z"
  },
  "group":
  {
    "id":26,
    "name":"group-with-clusters-api",
    "web_url":"https://gitlab.example.com/group-with-clusters-api"
  }
}
```

<a id="add-existing-cluster-to-group"></a>

## 向群组添加现存集群

向群组添加现存 Kubernetes 集群。

```plaintext
POST /groups/:id/clusters/user
```

参数：

| 参数                                                   | 类型             | 是否必需 | 描述                                                               |
|------------------------------------------------------|----------------|------|------------------------------------------------------------------|
| `id`                                                 | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)            |
| `name`                                               | string         | yes  | 集群名称                                                             |
| `domain`                                             | string         | no   | 集群的[基础域](../user/group/clusters/index.md#base-domain)            |
| `management_project_id`                              | integer        | no   | 集群的管理项目<!--[管理项目](../user/clusters/management_project.md)-->的 ID |
| `enabled`                                            | boolean        | no   | 集群是否有效。默认为 `true`                                                |
| `managed`                                            | boolean        | no   | 极狐GitLab 是否管理该集群的命名空间和服务账号，默认为 `true`                            |
| `platform_kubernetes_attributes[api_url]`            | string         | yes  | 访问 Kubernetes API 的 URL                                          |
| `platform_kubernetes_attributes[token]`              | string         | yes  | 向 Kubernetes 鉴权的令牌                                               |
| `platform_kubernetes_attributes[ca_cert]`            | string         | no   | TLS 证书。如果 API 使用自签名 TLS 证书，则必填                                   |
| `platform_kubernetes_attributes[authorization_type]` | string         | no   | 集群鉴权类型：`rbac`、`abac` 或 `unknown_authorization`。默认为 `rbac`        |
| `environment_scope`                                  | string         | no   | 集群的关联环境。默认为 `*` **(PREMIUM)**                                    |

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/groups/26/clusters/user" \
-H "Accept: application/json" \
-H "Content-Type:application/json" \
--request POST --data '{"name":"cluster-5", "platform_kubernetes_attributes":{"api_url":"https://35.111.51.20","token":"12345","ca_cert":"-----BEGIN CERTIFICATE-----\r\nhFiK1L61owwDQYJKoZIhvcNAQELBQAw\r\nLzEtMCsGA1UEAxMkZDA1YzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM4ZDBj\r\nMB4XDTE4MTIyNzIwMDM1MVoXDTIzMTIyNjIxMDM1MVowLzEtMCsGA1UEAxMkZDA1\r\nYzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM.......-----END CERTIFICATE-----"}}'
```

响应示例：

```json
{
  "id":24,
  "name":"cluster-5",
  "created_at":"2019-01-03T21:53:40.610Z",
  "managed": true,
  "enabled": true,
  "provider_type":"user",
  "platform_type":"kubernetes",
  "environment_scope":"*",
  "cluster_type":"group_type",
  "user":
  {
    "id":1,
    "name":"Administrator",
    "username":"root",
    "state":"active",
    "avatar_url":"https://www.gravatar.com/avatar/4249f4df72b..",
    "web_url":"https://gitlab.example.com/root"
  },
  "platform_kubernetes":
  {
    "api_url":"https://35.111.51.20",
    "authorization_type":"rbac",
    "ca_cert":"-----BEGIN CERTIFICATE-----\r\nhFiK1L61owwDQYJKoZIhvcNAQELBQAw\r\nLzEtMCsGA1UEAxMkZDA1YzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM4ZDBj\r\nMB4XDTE4MTIyNzIwMDM1MVoXDTIzMTIyNjIxMDM1MVowLzEtMCsGA1UEAxMkZDA1\r\nYzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM.......-----END CERTIFICATE-----"
  },
  "management_project":null,
  "group":
  {
    "id":26,
    "name":"group-with-clusters-api",
    "web_url":"https://gitlab.example.com/root/group-with-clusters-api"
  }
}
```

## 编辑群组集群

更新现存的群组集群。

```plaintext
PUT /groups/:id/clusters/:cluster_id
```

参数：

| 参数                                        | 类型             | 是否必需 | 描述                                                               |
|-------------------------------------------|----------------|------|------------------------------------------------------------------|
| `id`                                      | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)            |
| `cluster_id`                              | integer        | yes  | 集群 ID                                                            |
| `name`                                    | string         | no   | 集群名称                                                             |
| `domain`                                  | string         | no   | 集群的[基础域](../user/group/clusters/index.md#base-domain)            |
| `management_project_id`                   | integer        | no   | 集群的管理项目<!--[管理项目](../user/clusters/management_project.md)-->的 ID |
| `enabled`                                 | boolean        | no   | 集群是否有效                                                           |
| `managed`                                 | boolean        | no   | 极狐GitLab 是否管理该集群的命名空间和服务账号                                       |
| `platform_kubernetes_attributes[api_url]` | string         | no   | 访问 Kubernetes API 的 URL                                          |
| `platform_kubernetes_attributes[token]`   | string         | no   | 向 Kubernetes 鉴权的令牌                                               |
| `platform_kubernetes_attributes[ca_cert]` | string         | no   | TLS 证书。如果 API 使用自签名 TLS 证书，则必填                                   |
| `environment_scope`                       | string         | no   | 集群的相关环境 **(PREMIUM)**                                            |

NOTE:
只有集群通过"添加现存 Kubernetes 集群"<!--["Add existing Kubernetes cluster"](../user/project/clusters/add_existing_cluster.md)-->选项或["向群组添加现存集群"](#add-existing-cluster-to-group)端点进行添加，`name`、`api_url`、`ca_cert` 和 `token` 才能更新。

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/groups/26/clusters/24" \
-H "Content-Type:application/json" \
--request PUT --data '{"name":"new-cluster-name","domain":"new-domain.com","platform_kubernetes_attributes":{"api_url":"https://10.10.101.1:6433"}}'
```

响应示例：

```json
{
  "id":24,
  "name":"new-cluster-name",
  "domain":"new-domain.com",
  "created_at":"2019-01-03T21:53:40.610Z",
  "managed": true,
  "enabled": true,
  "provider_type":"user",
  "platform_type":"kubernetes",
  "environment_scope":"*",
  "cluster_type":"group_type",
  "user":
  {
    "id":1,
    "name":"Administrator",
    "username":"root",
    "state":"active",
    "avatar_url":"https://www.gravatar.com/avatar/4249f4df72b..",
    "web_url":"https://gitlab.example.com/root"
  },
  "platform_kubernetes":
  {
    "api_url":"https://new-api-url.com",
    "authorization_type":"rbac",
    "ca_cert":null
  },
  "management_project":
  {
    "id":2,
    "description":null,
    "name":"project2",
    "name_with_namespace":"John Doe8 / project2",
    "path":"project2",
    "path_with_namespace":"namespace2/project2",
    "created_at":"2019-10-11T02:55:54.138Z"
  },
  "group":
  {
    "id":26,
    "name":"group-with-clusters-api",
    "web_url":"https://gitlab.example.com/group-with-clusters-api"
  }
}
```

## 删除群组集群

删除现存群组集群。不在连接的 Kubernetes 集群中移除现存资源。

```plaintext
DELETE /groups/:id/clusters/:cluster_id
```

参数：

| 参数           | 类型             | 是否必需 | 描述                                                    |
|--------------|----------------|------|-------------------------------------------------------|
| `id`         | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `cluster_id` | integer        | yes  | 集群 ID                                 |

请求示例：

```shell
curl --request DELETE --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/groups/26/clusters/23"
```
