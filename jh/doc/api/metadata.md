---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 元数据 API **(FREE)**

> - 引入于极狐GitLab 15.2。
> - `enterprise` 引入于极狐GitLab 15.6。

检索此极狐GitLab 实例的元数据信息。

```plaintext
GET /metadata
```

响应体参数：

| 参数                | 类型             | 描述                                                      |
|:------------------|:---------------|:--------------------------------------------------------|
| `version`         | string         | 极狐GitLab 实例的版本                                          |
| `revision`        | string         | 极狐GitLab 实例的修订版                                         |
| `kas`             | object         | 关于 Kubernetes (KAS) 的极狐GitLab 代理服务器的元数据                 |
| `kas.enabled`     | boolean        | 表明 KAS 是否启用                                             |
| `kas.externalUrl` | string or null | 代理用来与 KAS 通信的 URL。如果 `kas.enabled` 为 `false`，此值为 `null` |
| `kas.version`     | string or null | KAS 版本。如果 `kas.enabled` 为 `false`，此值为 `null`            |
| `enterprise`  | boolean        | 展示是否是收费版本                                               |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/metadata"
```

响应示例：

```json
{
  "version": "15.2-pre",
  "revision": "c401a659d0c",
  "kas": {
    "enabled": true,
    "externalUrl": "grpc://gitlab.example.com:8150",
    "version": "15.0.0"
  },
  "enterprise": true
}
```
