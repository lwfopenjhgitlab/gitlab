---
stage: Plan
group: Product Planning
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 史诗 API **(PREMIUM)**

<!--
> - 引入于[极狐GitLab 旗舰版](https://about.gitlab.cn/pricing/) 10.2。
> - 单层史诗在 12.8 版本移动到[专业版](https://about.gitlab.cn/pricing/)。
-->

每个对史诗议题的 API 调用都必须经过认证。

如果一个用户不是某个群组的成员，并且该群组是私有的，对该群组的 `GET` 请求
的请求会返回 `404` 的状态码。

史诗仅在[专业版及以上](https://about.gitlab.cn/pricing/)中可用。
如果史诗功能不可用，会返回 `403` 状态码。

## 史诗议题 API

[史诗议题 API](epic_issues.md) 允许您与史诗相关的议题互动。

## 整合里程碑日期

因为开始日期和到期日期可以动态地从相关的议题里程碑中获取。
当用户有编辑权限时，会显示额外的字段。这包括两个布尔值 `start_date_is_fixed` 和 `due_date_is_fixed`，以及四个日期字段 `start_date_fixed`、
`start_date_from_inherited_source`、`due_date_fixed`、`due_date_from_inherited_source`。

- `end_date` 已被废弃，改为 `due_date`。
- `start_date_from_milestones` 已被废弃，改为 `start_date_from_inherited_source`。
- `due_date_from_milestones` 已被废弃，改为 `due_date_from_inherited_source`。

## 史诗分页

默认情况下，`GET` 请求一次返回 20 个结果，因为 API 结果是分页的。

阅读更多关于[分页](rest/index.md#pagination)的内容。

WARNING:
在 12.6 及更高版本中。
返回中的 `reference` 属性已被弃用，改为 `references`。

NOTE:
`references.relative` 是相对于史诗被请求的群组而言的。当一个史诗从它的源群组获取时，`relative` 格式与 `short` 格式相同。当史诗被跨群组请求时，`relative` 格式与 `full` 格式被看作相同。

## 列出一个群组的史诗

> 返回中的 `parent_iid` 和 `_links[parent]` 引入于 14.6 版本。

获取所请求的群组及其子群组的所有史诗。

```plaintext
GET /groups/:id/epics
GET /groups/:id/epics?author_id=5
GET /groups/:id/epics?labels=bug,reproduced
GET /groups/:id/epics?state=opened
```

| 参数       | 类型             | 是否必需 | 描述                                                       |
| ------------------- | ---------------- | ---------- | --------------------------------------------------------------------------------------------------------------------------- |
| `id`                | integer/string   | yes        | 认证用户拥有的 ID 或[群组的 URL 编码路径](rest/index.md#namespaced-path-encoding)。              |
| `author_id`         | integer          | no         | 返回由给定用户 `id` 创建的史诗。                                                                                |
| `author_username`   | string           | no         | 返回由给定的 `username` 用户创建的史诗。在 14.7 及更高版本中可用。 |
| `labels`            | string           | no         | 返回与逗号分隔的标签名称列表相匹配的史诗。可以使用史诗群组或父群组中的标签名称。 |
| `with_labels_details` | boolean        | no         | 如果 `true`，响应会返回标签字段中每个标签的更多细节：`:name`、`:color`、`:description`、`:description_html`、`:text_color`。默认为 `false`。 |
| `order_by`          | string           | no         | 返回按照 `created_at`、`updated_at` 或 `title` 字段排序的史诗。默认是 `created_at`。                            |
| `sort`              | string           | no         | 返回按 `asc` 或 `desc` 顺序排序的史诗。默认为`desc`。                                                           |
| `search`            | string           | no         | 根据其 `title` 和 `description` 搜索史诗。                                                                      |
| `state`             | string           | no         | 根据史诗的 `state` 搜索，可能的过滤器：`opened`、`closed` 和 `all`，默认为 `all`。                        |
| `created_after`     | datetime         | no         | 返回在给定时间或之后创建的史诗。预期为 ISO 8601格式 (`2019-03-15T08:00:00Z`)。 |
| `created_before`    | datetime         | no         | 返回在给定时间或之前创建的史诗。预期为 ISO 8601格式 (`2019-03-15T08:00:00Z`)。 |
| `updated_after`     | datetime         | no         | 返回史诗在给定时间或之后的更新。预期为 ISO 8601格式 (`2019-03-15T08:00:00Z`)。 |
| `updated_before`    | datetime         | no         | 返回史诗在给定时间或之前的更新。预期为 ISO 8601格式  (`2019-03-15T08:00:00Z`)。 |
| `include_ancestor_groups` | boolean    | no         | 包括所请求群组祖先的史诗。默认是 `false`。                                                      |
| `include_descendant_groups` | boolean  | no         | 包括所请求群组后代的史诗。默认值是  `true`。                                                     |
| `my_reaction_emoji` | string           | no         | 返回史诗被认证的用户对给定的表情符号的反应。`None` 返回没有反应的史诗。`Any` 返回至少有一个反应的史诗。在 13.0 及更高版本中可用。 |
| `not` | Hash | no | 返回与提供的参数不匹配的史诗。接受 `author_id`、`author_username`（14.7 及更高版本）和 `labels`（14.6 及更高版本）。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/epics"
```

响应示例：

```json
[
  {
  "id": 29,
  "iid": 4,
  "group_id": 7,
  "parent_id": 23,
  "parent_iid": 3,
  "title": "Accusamus iste et ullam ratione voluptatem omnis debitis dolor est.",
  "description": "Molestias dolorem eos vitae expedita impedit necessitatibus quo voluptatum.",
  "state": "opened",
  "confidential": "false",
  "web_url": "http://gitlab.example.com/groups/test/-/epics/4",
  "reference": "&4",
  "references": {
    "short": "&4",
    "relative": "&4",
    "full": "test&4"
  },
  "author": {
    "id": 10,
    "name": "Lu Mayer",
    "username": "kam",
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/018729e129a6f31c80a6327a30196823?s=80&d=identicon",
    "web_url": "http://gitlab.example.com/kam"
  },
  "start_date": null,
  "start_date_is_fixed": false,
  "start_date_fixed": null,
  "start_date_from_milestones": null,       //deprecated in favor of start_date_from_inherited_source
  "start_date_from_inherited_source": null,
  "end_date": "2018-07-31",                 //deprecated in favor of due_date
  "due_date": "2018-07-31",
  "due_date_is_fixed": false,
  "due_date_fixed": null,
  "due_date_from_milestones": "2018-07-31", //deprecated in favor of start_date_from_inherited_source
  "due_date_from_inherited_source": "2018-07-31",
  "created_at": "2018-07-17T13:36:22.770Z",
  "updated_at": "2018-07-18T12:22:05.239Z",
  "closed_at": "2018-08-18T12:22:05.239Z",
  "labels": [],
  "upvotes": 4,
  "downvotes": 0,
  "color": "#1068bf",
  "_links":{
      "self": "http://gitlab.example.com/api/v4/groups/7/epics/4",
      "epic_issues": "http://gitlab.example.com/api/v4/groups/7/epics/4/issues",
      "group":"http://gitlab.example.com/api/v4/groups/7",
      "parent":"http://gitlab.example.com/api/v4/groups/7/epics/3"
  }
  },
  {
  "id": 50,
  "iid": 35,
  "group_id": 17,
  "parent_id": 19,
  "parent_iid": 1,
  "title": "Accusamus iste et ullam ratione voluptatem omnis debitis dolor est.",
  "description": "Molestias dolorem eos vitae expedita impedit necessitatibus quo voluptatum.",
  "state": "opened",
  "web_url": "http://gitlab.example.com/groups/test/sample/-/epics/35",
  "reference": "&4",
  "references": {
    "short": "&4",
    "relative": "sample&4",
    "full": "test/sample&4"
  },
  "author": {
    "id": 10,
    "name": "Lu Mayer",
    "username": "kam",
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/018729e129a6f31c80a6327a30196823?s=80&d=identicon",
    "web_url": "http://gitlab.example.com/kam"
  },
  "start_date": null,
  "start_date_is_fixed": false,
  "start_date_fixed": null,
  "start_date_from_milestones": null,       //deprecated in favor of start_date_from_inherited_source
  "start_date_from_inherited_source": null,
  "end_date": "2018-07-31",                 //deprecated in favor of due_date
  "due_date": "2018-07-31",
  "due_date_is_fixed": false,
  "due_date_fixed": null,
  "due_date_from_milestones": "2018-07-31", //deprecated in favor of start_date_from_inherited_source
  "due_date_from_inherited_source": "2018-07-31",
  "created_at": "2018-07-17T13:36:22.770Z",
  "updated_at": "2018-07-18T12:22:05.239Z",
  "closed_at": "2018-08-18T12:22:05.239Z",
  "labels": [],
  "upvotes": 4,
  "downvotes": 0,
  "color": "#1068bf",
  "_links":{
      "self": "http://gitlab.example.com/api/v4/groups/17/epics/35",
      "epic_issues": "http://gitlab.example.com/api/v4/groups/17/epics/35/issues",
      "group":"http://gitlab.example.com/api/v4/groups/17",
      "parent":"http://gitlab.example.com/api/v4/groups/17/epics/1"
  }
  }
]
```

## 单个史诗

> 响应中的 `parent_iid` 和 `_links[parent]` 引入于 14.6 版本。

获取单个史诗。

```plaintext
GET /groups/:id/epics/:epic_iid
```

| 参数       | 类型             | 是否必需 | 描述                                                       |
| ------------------- | ---------------- | ---------- | ---------------------------------------------------------------------------------------|
| `id`                | integer/string   | yes        | 认证用户拥有的 ID 或[群组的 URL 编码路径](rest/index.md#namespaced-path-encoding)。               |
| `epic_iid`          | integer/string   | yes        | 史诗的内部 ID。  |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/epics/5"
```

响应示例：

```json
{
  "id": 30,
  "iid": 5,
  "group_id": 7,
  "parent_id": null,
  "parent_iid": null,
  "title": "Ea cupiditate dolores ut vero consequatur quasi veniam voluptatem et non.",
  "description": "Molestias dolorem eos vitae expedita impedit necessitatibus quo voluptatum.",
  "state": "opened",
  "web_url": "http://gitlab.example.com/groups/test/-/epics/5",
  "reference": "&5",
  "references": {
    "short": "&5",
    "relative": "&5",
    "full": "test&5"
  },
  "author":{
    "id": 7,
    "name": "Pamella Huel",
    "username": "arnita",
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/a2f5c6fcef64c9c69cb8779cb292be1b?s=80&d=identicon",
    "web_url": "http://gitlab.example.com/arnita"
  },
  "start_date": null,
  "start_date_is_fixed": false,
  "start_date_fixed": null,
  "start_date_from_milestones": null,       //deprecated in favor of start_date_from_inherited_source
  "start_date_from_inherited_source": null,
  "end_date": "2018-07-31",                 //deprecated in favor of due_date
  "due_date": "2018-07-31",
  "due_date_is_fixed": false,
  "due_date_fixed": null,
  "due_date_from_milestones": "2018-07-31", //deprecated in favor of start_date_from_inherited_source
  "due_date_from_inherited_source": "2018-07-31",
  "created_at": "2018-07-17T13:36:22.770Z",
  "updated_at": "2018-07-18T12:22:05.239Z",
  "closed_at": "2018-08-18T12:22:05.239Z",
  "labels": [],
  "upvotes": 4,
  "downvotes": 0,
  "color": "#1068bf",
  "subscribed": true,
  "_links":{
      "self": "http://gitlab.example.com/api/v4/groups/7/epics/5",
      "epic_issues": "http://gitlab.example.com/api/v4/groups/7/epics/5/issues",
      "group":"http://gitlab.example.com/api/v4/groups/7",
      "parent": null
  }
}
```

## 创建新史诗

> 响应中的 `parent_iid` 和 `_links[parent]` 引入于 14.6 版本。

创建一个新的史诗。

NOTE:
`start_date` 和 `end_date` 不应该被直接指派，因为它们现在代表了复合值。您可以通过 `*_is_fixed` 和 `*_fixed` 字段来代替。

```plaintext
POST /groups/:id/epics
```

| 参数       | 类型             | 是否必需 | 描述                                                       |
| ------------------- | ---------------- | ---------- | ---------------------------------------------------------------------------------------|
| `id`                | integer/string   | yes        | 认证用户拥有的 ID 或[群组的URL编码路径](rest/index.md#namespaced-path-encoding)。               |
| `title`             | string           | yes        | 史诗的标题。 |
| `labels`            | string           | no         | 以逗号分隔的标签列表。 |
| `description`       | string           | no         | 关于史诗的描述。限于 1,048,576 个字符。  |
| `color`             | string           | no         | 史诗的颜色。引入于 14.8 版本，在一个名为 `epic_highlight_color` 的功能标志后面(默认为禁用)。 |
| `confidential`      | boolean          | no         | 史诗是否应该是保密的。 |
| `created_at`        | string           | no         | 史诗的创建时间。日期时间字符串，ISO 8601格式，例如 `2016-03-11T03:45:40Z`。需要管理员或项目/群组所有者权限，引入于 13.5 版本。 |
| `start_date_is_fixed` | boolean        | no         | 开始日期是来自于 `start_date_fixed` 还是来自于里程碑。 |
| `start_date_fixed`  | string           | no         | 史诗的固定开始日期。 |
| `due_date_is_fixed` | boolean          | no         | 截止日期是来自于 `due_date_fixed` 还是来自于里程碑。 |
| `due_date_fixed`    | string           | no         | 史诗的固定到期日。 |
| `parent_id`         | integer/string   | no         | 父史诗的 ID。 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/epics?title=Epic&description=Epic%20description&parent_id=29"
```

响应示例：

```json
{
  "id": 33,
  "iid": 6,
  "group_id": 7,
  "parent_id": 29,
  "parent_iid": 4,
  "title": "Epic",
  "description": "Epic description",
  "state": "opened",
  "confidential": "false",
  "web_url": "http://gitlab.example.com/groups/test/-/epics/6",
  "reference": "&6",
  "references": {
    "short": "&6",
    "relative": "&6",
    "full": "test&6"
  },
  "author": {
    "name" : "Alexandra Bashirian",
    "avatar_url" : null,
    "state" : "active",
    "web_url" : "https://gitlab.example.com/eileen.lowe",
    "id" : 18,
    "username" : "eileen.lowe"
  },
  "start_date": null,
  "start_date_is_fixed": false,
  "start_date_fixed": null,
  "start_date_from_milestones": null,       //deprecated in favor of start_date_from_inherited_source
  "start_date_from_inherited_source": null,
  "end_date": "2018-07-31",                 //deprecated in favor of due_date
  "due_date": "2018-07-31",
  "due_date_is_fixed": false,
  "due_date_fixed": null,
  "due_date_from_milestones": "2018-07-31", //deprecated in favor of start_date_from_inherited_source
  "due_date_from_inherited_source": "2018-07-31",
  "created_at": "2018-07-17T13:36:22.770Z",
  "updated_at": "2018-07-18T12:22:05.239Z",
  "closed_at": "2018-08-18T12:22:05.239Z",
  "labels": [],
  "upvotes": 4,
  "downvotes": 0,
  "color": "#1068bf",
  "_links":{
    "self": "http://gitlab.example.com/api/v4/groups/7/epics/6",
    "epic_issues": "http://gitlab.example.com/api/v4/groups/7/epics/6/issues",
    "group":"http://gitlab.example.com/api/v4/groups/7",
    "parent": "http://gitlab.example.com/api/v4/groups/7/epics/4"
  }
}
```

## 更新史诗

> 响应中的 `parent_iid` 和 `_links[parent]` 引入于 14.6 版本。

更新一个史诗。

NOTE:
`start_date` 和 `end_date` 不应该被直接指派，因为它们现在代表了复合值。你可以通过 `*_is_fixed` 和 `*_fixed`字段来代替。

```plaintext
PUT /groups/:id/epics/:epic_iid
```

| 参数       | 类型             | 是否必需 | 描述                                                       |
| ------------------- | ---------------- | ---------- | ---------------------------------------------------------------------------------------|
| `id`                | integer/string   | yes        | 认证用户拥有的 ID 或[群组的 URL 编码路径](rest/index.md#namespaced-path-encoding)。               |
| `epic_iid`          | integer/string   | yes        | 史诗的内部 ID。  |
| `add_labels`        | string           | no         | 逗号分隔的标签名称，以添加到一个议题中。 |
| `confidential`      | boolean          | no         | 史诗是否应该是保密的。 |
| `description`       | string           | no         | 史诗关于史诗的描述。限于 1,048,576 个字符。 |
| `due_date_fixed`    | string           | no         | 史诗的固定到期日。 |
| `due_date_is_fixed` | boolean          | no         | 截止日期是来自于 `due_date_fixed` 还是来自于里程碑。 |
| `labels`            | string           | no         | 以逗号分隔的标签名称，用于一个议题。设置为空字符串可以取消所有标签的分配。 |
| `parent_id`         | integer/string   | no         | 父史诗的 ID。在 14.6 及更高版本中可用。 |
| `remove_labels`     | string           | no         | 用逗号分隔的标签名称，以从一个议题中删除。 |
| `start_date_fixed`  | string           | no         | 史诗的固定开始日期。 |
| `start_date_is_fixed` | boolean        | no         | 开始日期是否应来自于 `start_date_fixed` 或来自于里程碑。 |
| `state_event`       | string           | no         | 史诗的状态事件。设置 `close` 来关闭史诗，设置 `reopen` 来重新打开史诗。 |
| `title`             | string           | no         | 史诗的标题。 |
| `updated_at`        | string           | no         | 史诗更新的时间。日期时间字符串，ISO 8601 格式，例如 `2016-03-11T03:45:40Z`。需要管理员或项目/群组所有者权限，引入于 13.5 版本。 |
| `color`             | string           | no         | 史诗的颜色。引入于 14.8 版本，在一个名为 `epic_highlight_color` 的功能标志后面(默认为禁用)。 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/epics/5?title=New%20Title&parent_id=29"
```

响应示例：

```json
{
  "id": 33,
  "iid": 6,
  "group_id": 7,
  "parent_id": 29,
  "parent_iid": 4,
  "title": "New Title",
  "description": "Epic description",
  "state": "opened",
  "confidential": "false",
  "web_url": "http://gitlab.example.com/groups/test/-/epics/6",
  "reference": "&6",
  "references": {
    "short": "&6",
    "relative": "&6",
    "full": "test&6"
  },
  "author": {
    "name" : "Alexandra Bashirian",
    "avatar_url" : null,
    "state" : "active",
    "web_url" : "https://gitlab.example.com/eileen.lowe",
    "id" : 18,
    "username" : "eileen.lowe"
  },
  "start_date": null,
  "start_date_is_fixed": false,
  "start_date_fixed": null,
  "start_date_from_milestones": null,       //deprecated in favor of start_date_from_inherited_source
  "start_date_from_inherited_source": null,
  "end_date": "2018-07-31",                 //deprecated in favor of due_date
  "due_date": "2018-07-31",
  "due_date_is_fixed": false,
  "due_date_fixed": null,
  "due_date_from_milestones": "2018-07-31", //deprecated in favor of start_date_from_inherited_source
  "due_date_from_inherited_source": "2018-07-31",
  "created_at": "2018-07-17T13:36:22.770Z",
  "updated_at": "2018-07-18T12:22:05.239Z",
  "closed_at": "2018-08-18T12:22:05.239Z",
  "labels": [],
  "upvotes": 4,
  "downvotes": 0,
  "color": "#1068bf"
}
```

## 删除史诗

删除一个史诗。

```plaintext
DELETE /groups/:id/epics/:epic_iid
```

| 参数       | 类型             | 是否必需 | 描述                                                       |
| ------------------- | ---------------- | ---------- | ---------------------------------------------------------------------------------------|
| `id`                | integer/string   | yes        | 认证用户拥有的 ID 或[群组的 URL 编码路径](rest/index.md#namespaced-path-encoding)。               |
| `epic_iid`          | integer/string   | yes        | 史诗的内部 ID。  |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/epics/5"
```

## 创建一个待办事项

在史诗上为当前用户手动创建一个待办事项。如果该用户在史诗上已经有一个待办事项，则返回状态码 `304`。

```plaintext
POST /groups/:id/epics/:epic_iid/todo
```

| 参数       | 类型             | 是否必需 | 描述                                                       |
|-------------|---------|----------|--------------------------------------|
| `id`        | integer/string | yes   | 认证用户拥有的 ID 或[群组的 URL编码路径](rest/index.md#namespaced-path-encoding)。 |
| `epic_iid` | integer | yes          | 史诗的内部 ID。 |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/epics/5/todo"
```

响应示例：

```json
{
  "id": 112,
  "group": {
    "id": 1,
    "name": "Gitlab",
    "path": "gitlab",
    "kind": "group",
    "full_path": "base/gitlab",
    "parent_id": null
  },
  "author": {
    "name": "Administrator",
    "username": "root",
    "id": 1,
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    "web_url": "https://gitlab.example.com/root"
  },
  "action_name": "marked",
  "target_type": "epic",
  "target": {
    "id": 30,
    "iid": 5,
    "group_id": 1,
    "title": "Ea cupiditate dolores ut vero consequatur quasi veniam voluptatem et non.",
    "description": "Molestias dolorem eos vitae expedita impedit necessitatibus quo voluptatum.",
    "author":{
      "id": 7,
      "name": "Pamella Huel",
      "username": "arnita",
      "state": "active",
      "avatar_url": "http://www.gravatar.com/avatar/a2f5c6fcef64c9c69cb8779cb292be1b?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/arnita"
    },
    "web_url": "http://gitlab.example.com/groups/test/-/epics/5",
    "reference": "&5",
    "references": {
      "short": "&5",
      "relative": "&5",
      "full": "test&5"
    },
    "start_date": null,
    "end_date": null,
    "created_at": "2018-01-21T06:21:13.165Z",
    "updated_at": "2018-01-22T12:41:41.166Z",
    "closed_at": "2018-08-18T12:22:05.239Z"
  },
  "target_url": "https://gitlab.example.com/groups/epics/5",
  "body": "Vel voluptas atque dicta mollitia adipisci qui at.",
  "state": "pending",
  "created_at": "2016-07-01T11:09:13.992Z"
}
```
