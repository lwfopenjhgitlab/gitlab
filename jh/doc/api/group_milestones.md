---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组里程碑 API **(FREE)**

通过 REST API 使用群组[里程碑](../user/project/milestones/index.md)。
您也可以阅读[项目里程碑 API](milestones.md) 页面。

## 列出群组里程碑

返回群组里程碑列表。

```plaintext
GET /groups/:id/milestones
GET /groups/:id/milestones?iids[]=42
GET /groups/:id/milestones?iids[]=42&iids[]=43
GET /groups/:id/milestones?state=active
GET /groups/:id/milestones?state=closed
GET /groups/:id/milestones?title=1.0
GET /groups/:id/milestones?search=version
```

参数：

| 参数                          | 类型             | 是否必需 | 描述                                                                                                        |
|-----------------------------|----------------|------|-----------------------------------------------------------------------------------------------------------|
| `id`                        | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)                                   |
| `iids[]`                    | integer array  | no   | 仅返回带有给定 `iid` 的里程碑（注意：如果 `include_parent_milestones` 设置为 `true`，则忽略)                                      |
| `state`                     | string         | no   | 仅返回 `active` 或 `closed` 里程碑                                                                               |
| `title`                     | string         | no   | 仅返回带有给定 `title` 的里程碑                                                                                      |
| `search`                    | string         | no   | 仅返回标题或描述匹配提供的资源串的里程碑                                                                                      |
| `include_parent_milestones` | boolean        | no   | 包括父群组及其祖先群组的里程碑，<!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/196066)-->引入于极狐GitLab 13.4 |
| `updated_before`            | datetime | no | 仅返回给定日期时间之前更新的里程碑。预计格式为 ISO 8601（`2019-03-15T08:00:00Z`）。引入于 15.10                                        |
| `updated_after`             | datetime | no | 仅返回给定日期时间之后更新的里程碑。预计格式为 ISO 8601（`2019-03-15T08:00:00Z`）。引入于 15.10                                        |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/milestones"
```

响应示例

```json
[
  {
    "id": 12,
    "iid": 3,
    "group_id": 16,
    "title": "10.0",
    "description": "Version",
    "due_date": "2013-11-29",
    "start_date": "2013-11-10",
    "state": "active",
    "updated_at": "2013-10-02T09:24:18Z",
    "created_at": "2013-10-02T09:24:18Z",
    "expired": false,
    "web_url": "https://gitlab.com/groups/gitlab-org/-/milestones/42"
  }
]
```

## 获取单个里程碑

获取单个群组里程碑。

```plaintext
GET /groups/:id/milestones/:milestone_id
```

参数：

| 参数             | 类型             | 是否必需 | 描述                                                            |
|----------------|----------------|------|---------------------------------------------------------------|
| `id`           | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `milestone_id` | integer        | yes  | 群组里程碑 ID                               |

## 创建新的里程碑

创建新的群组里程碑。

```plaintext
POST /groups/:id/milestones
```

参数：

| 参数            | 类型             | 是否必需 | 描述                                                            |
|---------------|----------------|------|---------------------------------------------------------------|
| `id`          | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `title`       | string         | yes  | 里程碑的标题                                                        |
| `description` | string         | no   | 里程碑的描述                                                        |
| `due_date`    | date           | no   | 里程碑的截止日期，格式为 ISO 8601（`YYYY-MM-DD`）                           |
| `start_date`  | date           | no   | 里程碑的开始日期，格式为 ISO 8601（`YYYY-MM-DD`）                           |

## 编辑里程碑

更新现存的群组里程碑。

```plaintext
PUT /groups/:id/milestones/:milestone_id
```

参数：

| 参数             | 类型             | 是否必需 | 描述                                                            |
|----------------|----------------|------|---------------------------------------------------------------|
| `id`           | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `milestone_id` | integer        | yes  | 群组里程碑 ID                                                      |
| `title`        | string         | no   | 里程碑的标题                                                        |
| `description`  | string         | no   | 里程碑的描述                                                        |
| `due_date`     | date           | no   | 里程碑的截止日期，格式为 ISO 8601（`YYYY-MM-DD`）                           |
| `start_date`   | date           | no   | 里程碑的开始日期，格式为 ISO 8601（`YYYY-MM-DD`）                           |
| `state_event`  | string         | no   | 里程碑的状态事件 _（`close` 或 `activate`）_                             |

## 删除群组里程碑

只有群组的开发者角色才能进行这个操作。

```plaintext
DELETE /groups/:id/milestones/:milestone_id
```

参数

| 参数             | 类型             | 是否必需 | 描述                                                            |
|----------------|----------------|------|---------------------------------------------------------------|
| `id`           | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `milestone_id` | integer        | yes  | 群组里程碑 ID                             |

## 获取分配给单个里程碑的所有议题

获取分配给单个群组里程碑的所有议题。

```plaintext
GET /groups/:id/milestones/:milestone_id/issues
```

参数：

| 参数             | 类型             | 是否必需 | 描述                                                            |
|----------------|----------------|------|---------------------------------------------------------------|
| `id`           | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `milestone_id` | integer        | yes  | 群组里程碑 ID                                  |

目前，此 API 端点不返回子群组的议题。
如果您想获得所有里程碑的议题，您可以使用[列出议题 API](issues.md#list-issues) 并筛选特定的里程碑（例如，`GET /issues?milestone=1.0.0&state=opened`）。

## 获取分配给单个里程碑的所有合并请求

获取分配给单个群组里程碑的所有合并请求。

```plaintext
GET /groups/:id/milestones/:milestone_id/merge_requests
```

参数：

| 参数             | 类型             | 是否必需 | 描述                                                            |
|----------------|----------------|------|---------------------------------------------------------------|
| `id`           | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `milestone_id` | integer        | yes  | 群组里程碑 ID                                 |

## 获取单个里程碑的所有燃尽图事件 **(PREMIUM)**

> - <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/4737)-->引入于极狐GitLab 12.1。
> - 移动到极狐GitLab 专业版于 13.9。

获取单个里程碑的所有燃尽图事件。

```plaintext
GET /groups/:id/milestones/:milestone_id/burndown_events
```

参数：

| 参数             | 类型             | 是否必需 | 描述                                                            |
|----------------|----------------|------|---------------------------------------------------------------|
| `id`           | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `milestone_id` | integer        | yes  | 群组里程碑 ID                              |
