---
stage: Package
group: Package Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Conan API **(FREE)**

本文是 Conan 软件包<!--[Conan Packages](../../user/packages/conan_repository/index.md)--> 的 API 文档。

WARNING:
此 API 由 [Conan 软件包管理器客户端](https://docs.conan.io/en/latest/)使用并且通常不适合手动使用。

<!--有关如何从极狐GitLab 软件包库上传和安装 Conan 包的说明，请参阅 [Conan 软件包库文档](../../user/packages/conan_repository/index.md)。-->

NOTE:
这些端点不遵守标准的 API 身份验证方法。
有关如何传递证书的详细信息，请参阅每个路由。未来可能会删除未记录的身份验证方法。

NOTE:
Conan 库不符合 FIPS，并且在启用 FIPS 模式<!--[FIPS 模式](../../development/fips_compliance.md)-->时被禁用。
这些端点都将返回 "404 Not Found"。

## 路由前缀

有两组相同的路由，每个路由在不同的范围内发出请求：

- 使用实例级前缀在整个极狐GitLab 实例范围内发出请求。
- 使用项目级前缀在单个项目范围内提出请求。

本文档中的示例均使用实例级前缀。

### 实例级

```plaintext
/packages/conan/v1
```

使用实例级路由时，请注意 Conan Recipe 的命名限制。<!--[naming
restriction](../../user/packages/conan_repository/index.md#package-recipe-naming-convention-for-instance-remotes)-->

### 项目级

```plaintext
 /projects/:id/packages/conan/v1`
```

| 参数   | 类型     | 是否必需 | 描述          |
|------|--------|------|-------------|
| `id` | string | yes  | 项目 ID 或完整路径 |

## Ping

> 引入于极狐GitLab 12.2。

Ping 极狐GitLab Conan 仓库以验证可用性：

```plaintext
GET <route-prefix>/ping
```

```shell
curl "https://gitlab.example.com/api/v4/packages/conan/v1/ping"
```

响应示例：

```json
""
```

## 搜索

> 引入于极狐GitLab 12.4。

通过名称搜索 Conan 软件包实例：

```plaintext
GET <route-prefix>/conans/search
```

| 参数  | 类型     | 是否必需 | 描述                  |
|-----|--------|------|---------------------|
| `q` | string | yes  | 查询搜索。您可以将 `*` 用作通配符 |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/packages/conan/v1/conans/search?q=Hello*"
```

响应示例：

```json
{
  "results": [
    "Hello/0.1@foo+conan_test_prod/beta",
    "Hello/0.1@foo+conan_test_prod/stable",
    "Hello/0.2@foo+conan_test_prod/beta",
    "Hello/0.3@foo+conan_test_prod/beta",
    "Hello/0.1@foo+conan-reference-test/stable",
    "HelloWorld/0.1@baz+conan-reference-test/beta"
    "hello-world/0.4@buz+conan-test/alpha"
  ]
}
```

<a id="authenticate"></a>

## 身份验证

> 引入于极狐GitLab 12.2。

返回 Bearer Header 中 Conan 请求的 JWT：

```shell
"Authorization: Bearer <token>
```

Conan 软件包管理器客户端自动使用此令牌。

```plaintext
GET <route-prefix>/users/authenticate
```

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/packages/conan/v1/users/authenticate"
```

响应示例：

```shell
eyJhbGciOiJIUzI1NiIiheR5cCI6IkpXVCJ9.eyJhY2Nlc3NfdG9rZW4iOjMyMTQyMzAsqaVzZXJfaWQiOjQwNTkyNTQsImp0aSI6IjdlNzBiZTNjLWFlNWQtNDEyOC1hMmIyLWZiOThhZWM0MWM2OSIsImlhd3r1MTYxNjYyMzQzNSwibmJmIjoxNjE2NjIzNDMwLCJleHAiOjE2MTY2MjcwMzV9.QF0Q3ZIB2GW5zNKyMSIe0HIFOITjEsZEioR-27Rtu7E
```

## 检查证书

> 引入于极狐GitLab 12.4。

检查基本鉴权证书或从 [`/authenticate`](#authenticate) 生成的 Conan JWT 的有效性。

```plaintext
GET <route-prefix>/users/check_credentials
```

```shell
curl --header "Authorization: Bearer <authenticate_token>" "https://gitlab.example.com/api/v4/packages/conan/v1/users/check_credentials
```

响应示例：

```shell
ok
```

## Recipe 快照

> 引入于极狐GitLab 12.5。

返回指定 Conan Recipe 的 Recipe 文件的快照。快照是一个带有关联的 md5 哈希的文件名的列表。

```plaintext
GET <route-prefix>/conans/:package_name/:package_version/:package_username/:package_channel
```

| 参数                        | 类型     | 是否必需 | 描述                                  |
|---------------------------|--------|------|-------------------------------------|
| `package_name`            | string | yes  | 软件包名称                               |
| `package_version`         | string | yes  | 软件包版本                               |
| `package_username`        | string | yes  | 软件包的 Conan 用户名。此参数是以 `+` 分隔的项目的完整路径 |
| `package_channel`         | string | yes  | 软件包信道                               |

```shell
curl --header "Authorization: Bearer <authenticate_token>" "https://gitlab.example.com/api/v4/packages/conan/v1/conans/my-package/1.0/my-group+my-project/stable"
```

响应示例：

```json
{
  "conan_sources.tgz": "eadf19b33f4c3c7e113faabf26e76277",
  "conanfile.py": "25e55b96a28f81a14ba8e8a8c99eeace",
  "conanmanifest.txt": "5b6fd77a2ba14303ce4cdb08c87e82ab"
}
```

## 软件包快照

> 引入于极狐GitLab 12.5。

返回带有 Conan 引用的指定 Conan Recipe 的软件包文件的快照。快照是一个带有关联的 md5 哈希的文件名的列表。

```plaintext
GET <route-prefix>/conans/:package_name/:package_version/:package_username/:package_channel/packages/:conan_package_reference
```

| 参数                        | 类型     | 是否必需 | 描述                                  |
|---------------------------|--------|------|-------------------------------------|
| `package_name`            | string | yes  | 软件包名称                               |
| `package_version`         | string | yes  | 软件包版本                               |
| `package_username`        | string | yes  | 软件包的 Conan 用户名。此参数是以 `+` 分隔的项目的完整路径 |
| `package_channel`         | string | yes  | 软件包信道                               |
| `conan_package_reference` | string | yes  | Conan 软件包的引用哈希。Conan 生成此值           |

```shell
curl --header "Authorization: Bearer <authenticate_token>" "https://gitlab.example.com/api/v4/packages/conan/v1/conans/my-package/1.0/my-group+my-project/stable/packages/103f6067a947f366ef91fc1b7da351c588d1827f"
```

响应示例：

```json
{
  "conan_package.tgz": "749b29bdf72587081ca03ec033ee59dc",
  "conaninfo.txt": "32859d737fe84e6a7ccfa4d64dc0d1f2",
  "conanmanifest.txt": "a86b398e813bd9aa111485a9054a2301"
}
```

<a id="recipe-manifest"></a>

## Recipe Manifest

> 引入于极狐GitLab 12.5。

Manifest 是带有关联的下载 URL 的 Recipe 文件名。

```plaintext
GET <route-prefix>/conans/:package_name/:package_version/:package_username/:package_channel/digest
```

| 参数                        | 类型     | 是否必需 | 描述                                  |
|---------------------------|--------|------|-------------------------------------|
| `package_name`            | string | yes  | 软件包的名称                              |
| `package_version`         | string | yes  | 软件包的版本                              |
| `package_username`        | string | yes  | 软件包的 Conan 用户名。此参数是以 `+` 分隔的项目的完整路径 |
| `package_channel`         | string | yes  | 软件包的信道                              |


```shell
curl --header "Authorization: Bearer <authenticate_token>" "https://gitlab.example.com/api/v4/packages/conan/v1/conans/my-package/1.0/my-group+my-project/stable/digest"
```

响应示例：

```json
{
  "conan_sources.tgz": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/export/conan_sources.tgz",
  "conanfile.py": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/export/conanfile.py",
  "conanmanifest.txt": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/export/conanmanifest.txt"
}
```

响应中的 URL 所拥有的路由前缀与请求这些 URL 的路由前缀相同。如果您使用项目级路由进行请求，返回的 URL 包含 `/projects/:id`。

## Package Manifest

> 引入于极狐GitLab 12.5。

Manifest 是包含相关下载 URL 的包文件名列表。

```plaintext
GET <route-prefix>/conans/:package_name/:package_version/:package_username/:package_channel/packages/:conan_package_reference/digest
```

| 参数                        | 类型     | 是否必需 | 描述                                  |
|---------------------------|--------|------|-------------------------------------|
| `package_name`            | string | yes  | 软件包的名称                              |
| `package_version`         | string | yes  | 软件包的版本                              |
| `package_username`        | string | yes  | 软件包的 Conan 用户名。此参数是以 `+` 分隔的项目的完整路径 |
| `package_channel`         | string | yes  | 软件包的信道                              |
| `conan_package_reference` | string | yes  | Conan 软件包的引用哈希。Conan 生成此值           |

```shell
curl --header "Authorization: Bearer <authenticate_token>" "https://gitlab.example.com/api/v4/packages/conan/v1/conans/my-package/1.0/my-group+my-project/stable/packages/103f6067a947f366ef91fc1b7da351c588d1827f/digest"
```

响应示例：

```json
{
  "conan_package.tgz": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/packages/103f6067a947f366ef91fc1b7da351c588d1827f/0/conan_package.tgz",
  "conaninfo.txt": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/packages/103f6067a947f366ef91fc1b7da351c588d1827f/0/conaninfo.txt",
  "conanmanifest.txt": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/packages/103f6067a947f366ef91fc1b7da351c588d1827f/0/conanmanifest.txt"
}
```

响应中的 URL 所拥有的路由前缀与请求这些 URL 的路由前缀相同。如果您使用项目级路由进行请求，返回的 URL 包含 `/projects/:id`。 

<a id="recipe-download-urls"></a>

## Recipe 下载 URL

> 引入于极狐GitLab 12.5。

Recipe 下载 URL 返回 Recipe 文件名列表及其关联的下载 URL。
此参数与 [Recipe Manifest](#recipe-manifest) 端点的有效负载相同。

```plaintext
GET <route-prefix>/conans/:package_name/:package_version/:package_username/:package_channel/download_urls
```

| 参数                        | 类型     | 是否必需 | 描述                                  |
|---------------------------|--------|------|-------------------------------------|
| `package_name`            | string | yes  | 软件包的名称                              |
| `package_version`         | string | yes  | 软件包的版本                              |
| `package_username`        | string | yes  | 软件包的 Conan 用户名。此参数是以 `+` 分隔的项目的完整路径 |
| `package_channel`         | string | yes  | 软件包的信道                              |


```shell
curl --header "Authorization: Bearer <authenticate_token>" "https://gitlab.example.com/api/v4/packages/conan/v1/conans/my-package/1.0/my-group+my-project/stable/digest"
```

响应示例：

```json
{
  "conan_sources.tgz": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/export/conan_sources.tgz",
  "conanfile.py": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/export/conanfile.py",
  "conanmanifest.txt": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/export/conanmanifest.txt"
}
```

响应中的 URL 所拥有的路由前缀与请求这些 URL 的路由前缀相同。如果您使用项目级路由进行请求，返回的 URL 包含 `/projects/:id`。

<a id="package-download-urls"></a>

## 软件包下载 URL

> 引入于极狐GitLab 12.5。

软件包下载 URL 返回软件包文件名列表及其关联的下载 URL。
此参数与 [Package Manifest](#recipe-manifest) 端点的有效负载相同。

```plaintext
GET <route-prefix>/conans/:package_name/:package_version/:package_username/:package_channel/packages/:conan_package_reference/download_urls
```

| 参数                        | 类型     | 是否必需 | 描述                                  |
|---------------------------|--------|------|-------------------------------------|
| `package_name`            | string | yes  | 软件包的名称                              |
| `package_version`         | string | yes  | 软件包的版本                              |
| `package_username`        | string | yes  | 软件包的 Conan 用户名。此参数是以 `+` 分隔的项目的完整路径 |
| `package_channel`         | string | yes  | 软件包的信道                              |
| `conan_package_reference` | string | yes  | Conan 软件包的引用哈希。Conan 生成此值           |

```shell
curl --header "Authorization: Bearer <authenticate_token>" "https://gitlab.example.com/api/v4/packages/conan/v1/conans/my-package/1.0/my-group+my-project/stable/packages/103f6067a947f366ef91fc1b7da351c588d1827f/download_urls"
```

响应示例：

```json
{
  "conan_package.tgz": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/packages/103f6067a947f366ef91fc1b7da351c588d1827f/0/conan_package.tgz",
  "conaninfo.txt": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/packages/103f6067a947f366ef91fc1b7da351c588d1827f/0/conaninfo.txt",
  "conanmanifest.txt": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/packages/103f6067a947f366ef91fc1b7da351c588d1827f/0/conanmanifest.txt"
}
```

响应中的 URL 所拥有的路由前缀与请求这些 URL 的路由前缀相同。如果您使用项目级路由进行请求，返回的 URL 包含 `/projects/:id`。

<a id="recipe-upload-urls"></a>

## Recipe 上传 URL

> 引入于极狐GitLab 12.5。

特定 Recipe 文件名和文件大小的列表，将返回上传每个文件的 URL 列表。

```plaintext
POST <route-prefix>/conans/:package_name/:package_version/:package_username/:package_channel/upload_urls
```

| 参数                 | 类型     | 是否必需 | 描述                                  |
|--------------------|--------|------|-------------------------------------|
| `package_name`     | string | yes  | 软件包的名称                              |
| `package_version`  | string | yes  | 软件包的版本                              |
| `package_username` | string | yes  | 软件包的 Conan 用户名。此参数是以 `+` 分隔的项目的完整路径 |
| `package_channel`  | string | yes  | 软件包的信道                              |

请求 JSON 负载示例：

```json
{
  "conanfile.py": 410,
  "conanmanifest.txt": 130
}
```

```shell
curl --request POST \
     --header "Authorization: Bearer <authenticate_token>" \
     --header "Content-Type: application/json" \
     --data '{"conanfile.py":410,"conanmanifest.txt":130}' \
     "https://gitlab.example.com/api/v4/packages/conan/v1/conans/my-package/1.0/my-group+my-project/stable/upload_urls"
```

响应示例：

```json
{
  "conanfile.py": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/export/conanfile.py",
  "conanmanifest.txt": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/export/conanmanifest.txt"
}
```

响应中的 URL 所拥有的路由前缀与请求这些 URL 的路由前缀相同。如果您使用项目级路由进行请求，返回的 URL 包含 `/projects/:id`。

<a id="package-upload-urls"></a>

## 软件包上传 URL

> 引入于极狐GitLab 12.5。

特定软件包文件名和文件大小的列表，将返回上传每个文件的 URL 列表。

```plaintext
POST <route-prefix>/conans/:package_name/:package_version/:package_username/:package_channel/packages/:conan_package_reference/upload_urls
```

| 参数                        | 类型     | 是否必需 | 描述                                  |
|---------------------------|--------|------|-------------------------------------|
| `package_name`            | string | yes  | 软件包的名称                              |
| `package_version`         | string | yes  | 软件包的版本                              |
| `package_username`        | string | yes  | 软件包的 Conan 用户名。此参数是以 `+` 分隔的项目的完整路径 |
| `package_channel`         | string | yes  | 软件包的信道                              |
| `conan_package_reference` | string | yes  | Conan 软件包的引用哈希。Conan 生成此值           |

请求 JSON 负载示例：

```json
{
  "conan_package.tgz": 5412,
  "conanmanifest.txt": 130,
  "conaninfo.txt": 210
  }
```

```shell
curl --request POST \
     --header "Authorization: Bearer <authenticate_token>" \
     --header "Content-Type: application/json" \
     --data '{"conan_package.tgz":5412,"conanmanifest.txt":130,"conaninfo.txt":210}'
     "https://gitlab.example.com/api/v4/packages/conan/v1/conans/my-package/1.0/my-group+my-project/stable/packages/103f6067a947f366ef91fc1b7da351c588d1827f/upload_urls"
```

响应示例：

```json
{
  "conan_package.tgz": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/package/103f6067a947f366ef91fc1b7da351c588d1827f/0/conan_package.tgz",
  "conanmanifest.txt": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/package/103f6067a947f366ef91fc1b7da351c588d1827f/0/conanmanifest.txt",
  "conaninfo.txt": "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/package/103f6067a947f366ef91fc1b7da351c588d1827f/0/conaninfo.txt"
}
```

响应中的 URL 所拥有的路由前缀与请求这些 URL 的路由前缀相同。如果您使用项目级路由进行请求，返回的 URL 包含 `/projects/:id`。

## 下载 Recipe 文件

> 引入于极狐GitLab 12.6。

将 Recipe 文件下载到软件包库。您必须使用 [Recipe 下载 URL 端点](#recipe-download-urls)返回的下载 URL。


```plaintext
PUT packages/conan/v1/files/:package_name/:package_version/:package_username/:package_channel/:recipe_revision/export/:file_name
```

| 参数                 | 类型     | 是否必需 | 描述                                             |
|--------------------|--------|------|------------------------------------------------|
| `package_name`     | string | yes  | 软件包的名称                                         |
| `package_version`  | string | yes  | 软件包的版本                                         |
| `package_username` | string | yes  | 软件包的 Conan 用户名。此参数是以 `+` 分隔的项目的完整路径            |
| `package_channel`  | string | yes  | 软件包的信道                                         |
| `recipe_revision`  | string | yes  | Recipe 的修改。极狐GitLab 不支持 Conan 修改，所以一直使用默认值 `0` |
| `file_name`        | string | yes  | 请求文件的名称和文件扩展名                                  |

```shell
curl --header "Authorization: Bearer <authenticate_token>" "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/export/conanfile.py"
```

您也可以使用以下内容将输出写入到文件中：

```shell
curl --header "Authorization: Bearer <authenticate_token>" "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/export/conanfile.py" >> conanfile.py
```

此例写入到当前目录中的 `conanfile.py`。

## 上传 Recipe 文件

> 引入于极狐GitLab 12.6。

将 Recipe 文件上传到软件包库。您必须使用 [Recipe 上传 URL 端点](#recipe-upload-urls)返回的上传 URL。

```plaintext
GET packages/conan/v1/files/:package_name/:package_version/:package_username/:package_channel/:recipe_revision/export/:file_name
```

| 参数                 | 类型     | 是否必需 | 描述                                             |
|--------------------|--------|------|------------------------------------------------|
| `package_name`     | string | yes  | 软件包的名称                                         |
| `package_version`  | string | yes  | 软件包的版本                                         |
| `package_username` | string | yes  | 软件包的 Conan 用户名。此参数是以 `+` 分隔的项目的完整路径            |
| `package_channel`  | string | yes  | 软件包的信道                                         |
| `recipe_revision`  | string | yes  | Recipe 的修改。极狐GitLab 不支持 Conan 修改，所以一直使用默认值 `0` |
| `file_name`        | string | yes  | 请求文件的名称和文件扩展名                                  |

在请求主体中提供文件上下文：

```shell
curl --request PUT \
     --user <username>:<personal_access_token> \
     --upload-file path/to/conanfile.py \
     "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/export/conanfile.py"
```

## 下载软件包文件

> 引入于极狐GitLab 12.6。

将软件包文件下载到软件包库。您必须使用[软件包下载 URL 端点](#package-download-urls)返回的下载 URL。


```plaintext
GET packages/conan/v1/files/:package_name/:package_version/:package_username/:package_channel/:recipe_revision/package/:conan_package_reference/:package_revision/:file_name
```

| 参数                        | 类型     | 是否必需 | 描述                                             |
|---------------------------|--------|------|------------------------------------------------|
| `package_name`            | string | yes  | 软件包的名称                                         |
| `package_version`         | string | yes  | 软件包的版本                                         |
| `package_username`        | string | yes  | 软件包的 Conan 用户名。此参数是以 `+` 分隔的项目的完整路径            |
| `package_channel`         | string | yes  | 软件包的信道                                         |
| `recipe_revision`         | string | yes  | Recipe 的修改。极狐GitLab 不支持 Conan 修改，所以一直使用默认值 `0` |
| `conan_package_reference` | string | yes  | Conan 软件包的引用哈希。Conan 生成此值                      |
| `package_revision`        | string | yes  | 软件包的修改。极狐GitLab 不支持 Conan 修改，所以一直使用默认值 `0`     |
| `file_name`               | string | yes  | 请求文件的名称和文件扩展名                                  |

```shell
curl --header "Authorization: Bearer <authenticate_token>" "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/packages/103f6067a947f366ef91fc1b7da351c588d1827f/0/conaninfo.txt"
```

您也可以使用以下内容将输出写入到文件中：

```shell
curl --header "Authorization: Bearer <authenticate_token>" "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/packages/103f6067a947f366ef91fc1b7da351c588d1827f/0/conaninfo.txt" >> conaninfo.txt
```

此例写入到当前目录中的 `conaninfo.txt`。

## 上传软件包文件

> 引入于极狐GitLab 12.6。

将软件包文件上传到软件包库。您必须使用[软件包上传 URL 端点](#package-upload-urls)返回的上传 URL。

```plaintext
PUT packages/conan/v1/files/:package_name/:package_version/:package_username/:package_channel/:recipe_revision/package/:conan_package_reference/:package_revision/:file_name
```

| 参数                        | 类型     | 是否必需 | 描述                                             |
|---------------------------|--------|------|------------------------------------------------|
| `package_name`            | string | yes  | 软件包的名称                                         |
| `package_version`         | string | yes  | 软件包的版本                                         |
| `package_username`        | string | yes  | 软件包的 Conan 用户名。此参数是以 `+` 分隔的项目的完整路径            |
| `package_channel`         | string | yes  | 软件包的信道                                         |
| `recipe_revision`         | string | yes  | Recipe 的修改。极狐GitLab 不支持 Conan 修改，所以一直使用默认值 `0` |
| `conan_package_reference` | string | yes  | Conan 软件包的引用哈希。Conan 生成此值                      |
| `package_revision`        | string | yes  | 软件包的修改。极狐GitLab 不支持 Conan 修改，所以一直使用默认值 `0`     |
| `file_name`               | string | yes  | 请求文件的名称和文件扩展名                                  |

在请求主体中提供文件上下文：

```shell
curl --request PUT \
     --user <username>:<personal_access_token> \
     --upload-file path/to/conaninfo.txt \
     "https://gitlab.example.com/api/v4/packages/conan/v1/files/my-package/1.0/my-group+my-project/stable/0/package/103f6067a947f366ef91fc1b7da351c588d1827f/0/conaninfo.txt"
```

## 删除软件包（删除 Conan Recipe）

> 引入于极狐GitLab 12.5。

从库中删除 Conan Recipe 和软件包文件：

```plaintext
DELETE <route-prefix>/conans/:package_name/:package_version/:package_username/:package_channel
```

| 参数                 | 类型     | 是否必需 | 描述                                  |
|--------------------|--------|------|-------------------------------------|
| `package_name`     | string | yes  | 软件包的名称                              |
| `package_version`  | string | yes  | 软件包的版本                              |
| `package_username` | string | yes  | 软件包的 Conan 用户名。此参数是以 `+` 分隔的项目的完整路径 |
| `package_channel`  | string | yes  | 软件包的信道                              |

```shell
curl --request DELETE --header "Authorization: Bearer <authenticate_token>" "https://gitlab.example.com/api/v4/packages/conan/v1/conans/my-package/1.0/my-group+my-project/stable"
```

响应示例：

```json
{
  "id": 1,
  "project_id": 123,
  "created_at": "2020-08-19T13:17:28.655Z",
  "updated_at": "2020-08-19T13:17:28.655Z",
  "name": "my-package",
  "version": "1.0",
  "package_type": "conan",
  "creator_id": null,
  "status": "default"
}
```
