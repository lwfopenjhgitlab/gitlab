---
stage: Package
group: Package Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Maven API **(FREE)**

本文是 [Maven 软件包](../../user/packages/maven_repository/index.md)的 API 文档。

WARNING:
此 API 由 [Maven 包管理器客户端](https://maven.apache.org/)使用并且通常不适合手动使用。

有关如何从极狐GitLab 软件包库上传和安装 Maven 包的说明，请参阅 [Maven 软件包库文档](../../user/packages/maven_repository/index.md)。

NOTE:
这些端点不遵守标准的 API 身份验证方法。
有关支持的 Header 和令牌类型的详细信息，请参阅 [Maven 软件包库文档](../../user/packages/maven_repository/index.md)。将来可能会删除未记录的身份验证方法。

## 下载实例级的软件包文件

下载 Maven 软件包文件：

```plaintext
GET packages/maven/*path/:file_name
```

| 参数          | 类型     | 是否必需 | 描述                                                                            |
|-------------|--------|------|-------------------------------------------------------------------------------|
| `path`      | string | yes  | Maven 软件包路径，格式为 `<groupId>/<artifactId>/<version>`。使用 `/` 替换 `groupId` 中的 `.` |
| `file_name` | string | yes  | Maven 软件包的名称                                                                  |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/packages/maven/foo/bar/mypkg/1.0-SNAPSHOT/mypkg-1.0-SNAPSHOT.jar"
```

将输出写入文件：

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/packages/maven/foo/bar/mypkg/1.0-SNAPSHOT/mypkg-1.0-SNAPSHOT.jar" >> mypkg-1.0-SNAPSHOT.jar
```

将下载文件写入到当前目录中的 `mypkg-1.0-SNAPSHOT.jar`。

## 下载群组级的软件包文件

下载 Maven 软件包文件：

```plaintext
GET groups/:id/-/packages/maven/*path/:file_name
```

| 参数          | 类型     | 是否必需 | 描述                                                                            |
|-------------|--------|------|-------------------------------------------------------------------------------|
| `path`      | string | yes  | Maven 软件包路径，格式为 `<groupId>/<artifactId>/<version>`。使用 `/` 替换 `groupId` 中的 `.` |
| `file_name` | string | yes  | Maven 软件包文件的名称                                                                |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/groups/1/-/packages/maven/foo/bar/mypkg/1.0-SNAPSHOT/mypkg-1.0-SNAPSHOT.jar"
```

将输出写入到文件：

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/groups/1/-/packages/maven/foo/bar/mypkg/1.0-SNAPSHOT/mypkg-1.0-SNAPSHOT.jar" >> mypkg-1.0-SNAPSHOT.jar
```

将下载文件写入到当前目录中的 `mypkg-1.0-SNAPSHOT.jar`。

## 下载项目级的软件包文件

下载 Maven 软件包文件：

```plaintext
GET projects/:id/packages/maven/*path/:file_name
```

| 参数          | 类型     | 是否必需 | 描述                                                                                                                   |
|-------------|--------|------|----------------------------------------------------------------------------------------------------------------------|
| `path`      | string | yes  | Maven 软件包路径，格式为 `<groupId>/<artifactId>/<version>`。使用 `/` 替换 `groupId` 中的 `.` |
| `file_name` | string | yes  | Maven 软件包文件的名称                                                                                  |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/maven/foo/bar/mypkg/1.0-SNAPSHOT/mypkg-1.0-SNAPSHOT.jar"
```

将输出写入到文件：

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/maven/foo/bar/mypkg/1.0-SNAPSHOT/mypkg-1.0-SNAPSHOT.jar" >> mypkg-1.0-SNAPSHOT.jar
```

将下载文件写入到当前目录中的 `mypkg-1.0-SNAPSHOT.jar`。

## 上传软件包文件

上传 Maven 软件包文件：

```plaintext
PUT projects/:id/packages/maven/*path/:file_name
```

| 参数          | 类型     | 是否必需 | 描述                                                                                                                   |
|-------------|--------|------|----------------------------------------------------------------------------------------------------------------------|
| `path`      | string | yes  | Maven 软件包路径，格式为 `<groupId>/<artifactId>/<version>`。使用 `/` 替换 `groupId` 中的 `.` |
| `file_name` | string | yes  | Maven 软件包文件的名称                                                                                 |

```shell
curl --request PUT \
     --upload-file path/to/mypkg-1.0-SNAPSHOT.pom \
     --header "Private-Token: <personal_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/packages/maven/foo/bar/mypkg/1.0-SNAPSHOT/mypkg-1.0-SNAPSHOT.pom"
```
