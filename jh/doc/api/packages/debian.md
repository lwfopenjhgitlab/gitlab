---
stage: Package
group: Package Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Debian API **(FREE SELF)**

> - Debian API 引入于极狐GitLab 13.5。
> - Debian 群组 API 引入于极狐GitLab 14.2。
> - [部署在功能标志后](../../user/feature_flags.md)，默认禁用。

本文档是 Debian 的 API 文档<!--[Debian](../../user/packages/debian_repository/index.md)-->。

WARNING:
此 API 由 Debian 相关的软件包客户端使用，例如 [dput](https://manpages.debian.org/stable/dput-ng/dput.1.en.html)
和 [apt-get](https://manpages.debian.org/stable/apt/apt-get.8.en.html)，并且通常不适合手动使用。此 API 正在开发中，由于功能有限，尚未准备好用于生产用途。

<!--For instructions on how to upload and install Debian packages from the GitLab
package registry, see the [Debian registry documentation](../../user/packages/debian_repository/index.md).-->

NOTE:
这些端点不遵守标准的 API 身份验证方法。
<!--See the [Debian registry documentation](../../user/packages/debian_repository/index.md)
for details on which headers and token types are supported. Undocumented authentication methods might be removed in the future.-->

## 启用 Debian API

Debian API 在默认禁用的功能标志后。
[有权访问极狐GitLab Rails 控制台的极狐GitLab 管理员](../../administration/feature_flags.md)可以启用。<!--要启用它，请按照[启用 Debian API](../../user/packages/debian_repository/index.md#enable-the-debian-api) 中的说明进行操作。-->

## 启用 Debian 群组 API

Debian 群组 API 在默认禁用的功能标志后。
[有权访问极狐GitLab Rails 控制台的极狐GitLab 管理员](../../administration/feature_flags.md)可以启用。<!--To enable it, follow the instructions in
[Enable the Debian group API](../../user/packages/debian_repository/index.md#enable-the-debian-group-api).-->

<!--
### Authenticate to the Debian Package Repositories

See [Authenticate to the Debian Package Repositories](../../user/packages/debian_repository/index.md#authenticate-to-the-debian-package-repositories).
-->

## 上传包文件

> - 引入于极狐GitLab 14.0。
> - 使用显式分发和组件上传引入于极狐GitLab 15.9。

上传 Debian 包文件：

```plaintext
PUT projects/:id/packages/debian/:file_name
```

| 参数          | 类型     | 是否必需 | 描述                                             |
|-------------|--------|------|------------------------------------------------|
| `id`        | string | 是    | 项目的 ID 或完整路径                                   |
| `file_name` | string | 是    | Debian 包文件的名称                                  |
| `distribution` |string | 否    | 分发代号或套件。与 `component` 一起使用，用于使用显式分发和组件进行上传。    |
| `component` |string | 否    | 软件包文件组件。与 `distribution` 一起使用，用于使用显式分发和组件进行上传。 |

```shell
curl --request PUT \
     --user "<username>:<personal_access_token>" \
     --upload-file path/to/mypkg.deb \
     "https://gitlab.example.com/api/v4/projects/1/packages/debian/mypkg.deb"
```

使用显式分发和组件上传：

```shell
curl --request PUT \
  --user "<username>:<personal_access_token>" \
  --upload-file  /path/to/myother.deb \
  "https://gitlab.example.com/api/v4/projects/1/packages/debian/myother.deb?distribution=sid&component=main"
```

## 下载包

> 引入于极狐GitLab 14.2。

下载包文件。

```plaintext
GET projects/:id/packages/debian/pool/:distribution/:letter/:package_name/:package_version/:file_name
```

| 参数                | 类型     | 是否必需 | 描述                                             |
|-------------------|--------|------|------------------------------------------------|
| `distribution`    | string | yes  | Debian 发行版的代号或套件                               |
| `letter`          | string | yes  | Debian 分类（首字母或库首字母）|
| `package_name`    | string | yes  | 源包的名称                                          |
| `package_version` | string | yes  | 源包的版本                                          |
| `file_name`       | string | yes  | 文件名称                                           |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/debian/pool/my-distro/a/my-pkg/1.0.0/example_1.0.0~alpha2_amd64.deb"
```

将输出写入到文件中：

```shell
curl --header "Private-Token: <personal_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/packages/debian/pool/my-distro/a/my-pkg/1.0.0/example_1.0.0~alpha2_amd64.deb" \
     --remote-name
```

将使用当前目录中的远程文件名写入下载的文件。

## 路由前缀

所描述的其余端点是两组相同的路由，每个路由都在不同的范围内发出请求：

- 使用项目级前缀在单个项目范围内提出请求。
- 使用群组级前缀在单个群组的范围内发出请求。

本文档中的示例均使用项目级前缀。

### 项目级

```plaintext
 /projects/:id/packages/debian`
```

| 参数   | 类型     | 是否必需 | 描述            |
|------|--------|------|---------------|
| `id` | string | yes  | 项目 ID 或完整项目路径 |

### 群组级

```plaintext
 /groups/:id/-/packages/debian`
```

| 参数   | 类型     | 是否必需 | 描述            |
|------|--------|------|---------------|
| `id` | string | yes  | 项目 ID 或完整群组路径 |

## 下载发行版发布文件

> 引入于极狐GitLab 14.1。

下载 Debian 发行版文件。

```plaintext
GET <route-prefix>/dists/*distribution/Release
```

| 参数             | 类型     | 是否必需 | 描述                                                |
|----------------|--------|------|---------------------------------------------------|
| `distribution` | string | yes  | Debian 发行版的代号或套件 |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/Release"
```

将输出写入到文件中：

```shell
curl --header "Private-Token: <personal_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/Release" \
     --remote-name
```

将使用当前目录中的远程文件名写入下载的文件。

## 下载签名发行版发布文件

> 引入于极狐GitLab 14.1。

下载签名 Debian 发行版文件。

```plaintext
GET <route-prefix>/dists/*distribution/InRelease
```

| 参数             | 类型     | 是否必需 | 描述                                            |
|----------------|--------|------|-----------------------------------------------|
| `distribution` | string | yes  | Debian 发行版的代号或套件 |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/InRelease"
```

将输出写入到文件：

```shell
curl --header "Private-Token: <personal_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/InRelease" \
     --remote-name
```

将使用当前目录中的远程文件名写入下载的文件。

## 下载发布文件签名

> 引入于极狐GitLab 14.2。

下载 Debian 发布文件签名。

```plaintext
GET <route-prefix>/dists/*distribution/Release.gpg
```

| 参数             | 类型     | 是否必需 | 描述                                            |
|----------------|--------|------|-----------------------------------------------|
| `distribution` | string | yes  | Debian 发行版的代号或套件 |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/Release.gpg"
```

将输出写入文件：

```shell
curl --header "Private-Token: <personal_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/Release.gpg" \
     --remote-name
```

将使用当前目录中的远程文件名写入下载的文件。

## 下载软件包索引

> 引入于极狐GitLab 14.2。

下载软件包索引。

```plaintext
GET <route-prefix>/dists/*distribution/:component/binary-:architecture/Packages
```

| 参数                | 类型     | 是否必需 | 描述                                             |
|-------------------|--------|------|------------------------------------------------|
| `distribution`    | string | yes  | Debian 发行版的代号或套件                               |
| `component`          | string | yes  | 发行版组件名称|
| `architecture`    | string | yes  | 发行版架构类型                                          |


```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/main/binary-amd64/Packages"
```

将输出写入文件中：

```shell
curl --header "Private-Token: <personal_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/main/binary-amd64/Packages" \
     --remote-name
```

将使用当前目录中的远端文件名写入下载的文件中。

## 按哈希下载包索引

> 引入于极狐GitLab 15.4。

按哈希下载包索引。

```plaintext
GET <route-prefix>/dists/*distribution/:component/debian-installer/binary-:architecture/Packages
```

| 参数             | 类型     | 是否必需 | 描述                 |
|----------------|--------|------|--------------------|
| `distribution` | string | yes  | Debian 发行版的代码名称或套件 |
| `component`    | string | yes  | 发行版组件名称            |
| `architecture` | string | yes  | 发行版架构类型            |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/main/debian-installer/binary-amd64/Packages"
```

将输出写入文件：

```shell
curl --header "Private-Token: <personal_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/main/debian-installer/binary-amd64/Packages" \
     --remote-name
```

将使用当前目录中的远端文件名写入下载的文件中。

## 下载 Debian Installer 软件包索引

> 引入于极狐GitLab 15.4。

下载 Debian Installer 软件包索引。

```plaintext
GET <route-prefix>/dists/*distribution/:component/debian-installer/binary-:architecture/Packages
```

| 参数             | 类型     | 是否必需 | 描述                        |
|----------------|--------|------|---------------------------|
| `distribution` | string | yes  | Debian 发行版的代码名称或套件        |
| `component`    | string | yes  | 发行版组件名称                   |
| `architecture` | string | yes  | 发行版架构类型 |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/main/debian-installer/binary-amd64/Packages"
```

将输出写入文件：

```shell
curl --header "Private-Token: <personal_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/main/debian-installer/binary-amd64/Packages" \
     --remote-name
```

将使用当前目录中的远端文件名写入下载的文件。

## 按哈希下载 Debian Installer 软件包索引

> 引入于极狐GitLab 15.4。

按哈希下载 Debian Installer 软件包索引

```plaintext
GET <route-prefix>/dists/*distribution/:component/debian-installer/binary-:architecture/by-hash/SHA256/:file_sha256
```

| 参数             | 类型     | 是否必需 | 描述 |
|----------------|--------|--| ----------- |
| `distribution` | string | yes | Debian 发行版的代码名称或套件|
| `component`    | string | yes | 发行版组件名称 |
| `architecture` | string | yes | 发行版架构类型 |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/main/debian-installer/binary-amd64/by-hash/SHA256/66a045b452102c59d840ec097d59d9467e13a3f34f6494e539ffd32c1bb35f18"
```

将输出写入文件：

```shell
curl --header "Private-Token: <personal_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/main/debian-installer/binary-amd64/by-hash/SHA256/66a045b452102c59d840ec097d59d9467e13a3f34f6494e539ffd32c1bb35f18" \
     --remote-name
```

将使用当前目录中的远端文件名写入下载的文件。

## 下载源包索引

> 引入于极狐GitLab 15.4。

下载源包索引。

```plaintext
GET <route-prefix>/dists/*distribution/:component/source/Sources
```

| 参数             | 类型     | 是否必需 | 描述                 |
|----------------|--------|------|--------------------|
| `distribution` | string | yes  | Debian 发行版的代码名称或套件 |
| `component`    | string | yes  | 发行版组件名称            |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/main/source/Sources"
```

将输出写入文件：

```shell
curl --header "Private-Token: <personal_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/main/source/Sources" \
     --remote-name
```

将使用当前目录中的远端文件名写入下载的文件。

## 按哈希下载源包索引

> 引入于极狐GitLab 15.4。

按哈希下载源包索引。

```plaintext
GET <route-prefix>/dists/*distribution/:component/source/by-hash/SHA256/:file_sha256
```

| 参数             | 类型     | 是否必需 | 描述          |
|----------------|--------|------|-------------|
| `distribution` | string | yes  | 发行版的代码名称或套件 |
| `component`    | string | yes  | 发行版组件名称     |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/main/source/by-hash/SHA256/66a045b452102c59d840ec097d59d9467e13a3f34f6494e539ffd32c1bb35f18"
```

将输出写入文件：

```shell
curl --header "Private-Token: <personal_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/packages/debian/dists/my-distro/main/source/by-hash/SHA256/66a045b452102c59d840ec097d59d9467e13a3f34f6494e539ffd32c1bb35f18" \
     --remote-name
```

将使用当前目录中的远端文件名写入下载的文件。
