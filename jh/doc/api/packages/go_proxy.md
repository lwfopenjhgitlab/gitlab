---
stage: Package
group: Package Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Go 代理 API **(FREE SELF)**

本文是 [Go 软件包](../../user/packages/go_proxy/index.md)的 API 文档。
此 API 位于默认禁用的功能标志后面。有权访问极狐GitLab Rails 控制台的极狐GitLab 管理员可以为您的极狐GitLab 实例[启用](../../administration/feature_flags.md)此 API。

WARNING:
[Go 客户端](https://maven.apache.org/)使用此 API 并且通常不适合手动使用。

有关如何使用 Go 代理的说明，请参阅 [Go 代理包文档](../../user/packages/go_proxy/index.md)。

NOTE:
这些端点不遵守标准的 API 身份验证方法。
有关支持的 Header 和令牌类型的详细信息，请参阅 [Go 代理包文档](../../user/packages/go_proxy/index.md)。将来可能会删除未记录的身份验证方法。

## 列表

> 引入于极狐GitLab 13.1。

获取特定 Go 模块的所有打标签的版本。

```plaintext
GET projects/:id/packages/go/:module_name/@v/list
```

| 参数          | 类型     | 是否必需 | 描述                                                                            |
|-------------|--------|------|-------------------------------------------------------------------------------|
| `id`       | string | yes  | 项目的 ID 或完整路径 |
| `module_name` | string | yes  | Go 模块的名称|

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/go/my-go-module/@v/list"
```

输出示例：

```shell
"v1.0.0\nv1.0.1\nv1.3.8\n2.0.0\n2.1.0\n3.0.0"
```

## 版本元数据

> 引入于极狐GitLab 13.1。

获取特定 Go 模块的所有打标签的版本：

```plaintext
GET projects/:id/packages/go/:module_name/@v/:module_version.info
```


| 参数          | 类型     | 是否必需 | 描述                                                                            |
|-------------|--------|------|-------------------------------------------------------------------------------|
| `id`       | string | yes  | 项目的 ID 或完整路径 |
| `module_name` | string | yes  | Go 模块的名称|
| `module_version` | string | yes | Go 模块的版本 |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/go/my-go-module/@v/1.0.0.info"
```

输出示例：

```json
{
  "Version": "v1.0.0",
  "Time": "1617822312 -0600"
}
```

## 下载模块文件

> 引入于极狐GitLab 13.1。

获取 `.mod` 模块文件：

```plaintext
GET projects/:id/packages/go/:module_name/@v/:module_version.mod
```

| 参数          | 类型     | 是否必需 | 描述                                                                            |
|-------------|--------|------|-------------------------------------------------------------------------------|
| `id`       | string | yes  | 项目的 ID 或完整路径 |
| `module_name` | string | yes  | Go 模块的名称|
| `module_version` | string | yes | Go 模块的版本 |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/go/my-go-module/@v/1.0.0.mod"
```

写入文件：

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/go/my-go-module/@v/1.0.0.mod" >> foo.mod
```

写入当前目录中的 `foo.mod`。

## 下载模块来源

> 引入于极狐GitLab 13.1。

获取模块来源的 `.zip`：

```plaintext
GET projects/:id/packages/go/:module_name/@v/:module_version.zip
```


| 参数          | 类型     | 是否必需 | 描述                                                                            |
|-------------|--------|------|-------------------------------------------------------------------------------|
| `id`       | string | yes  | 项目的 ID 或完整路径 |
| `module_name` | string | yes  | Go 模块的名称|
| `module_version` | string | yes | Go 模块的版本 |

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/go/my-go-module/@v/1.0.0.zip"
```

写入文件：

```shell
curl --header "Private-Token: <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/go/my-go-module/@v/1.0.0.zip" >> foo.zip
```

写入当前目录中的 `foo.zip`。
