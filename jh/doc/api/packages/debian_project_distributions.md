---
stage: Package
group: Package Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Debian 项目发行版 API **(FREE SELF)**

> - 引入于极狐GitLab 13.5。
> - [部署在功能标志后](../../user/feature_flags.md)，默认禁用。

这是 Debian 项目发行版 API 的参考文档。此 API 在默认禁用的功能标志后。要使用此 API，您必须[启用它](#enable-the-debian-group-api)。

WARNING:
此 API 正在开发中，未准备好用于生产用途。

<!--For more information about working with Debian packages, see the
[Debian package registry documentation](../../user/packages/debian_repository/index.md).-->

<a id="enable-the-debian-group-api"></a>

## 启用 Debian API

Debian API 位于默认禁用的功能标志后。
[有权访问极狐GitLab Rails 控制台的极狐GitLab 管理员](../../administration/feature_flags.md)可以启用。<!--要启用它，请按照中的说明进行操作
[启用 Debian 组 API](../../user/packages/debian_repository/index.md#enable-the-debian-group-api)。-->

<!--
## Authenticate to the Debian distributions APIs

See [Authenticate to the Debian distributions APIs](../../user/packages/debian_repository/index.md#authenticate-to-the-debian-distributions-apis).
-->

## 列出项目中的所有 Debian 发行版

列出特定项目中的 Debian 发行版。

```plaintext
GET /projects/:id/debian_distributions
```

| 参数         | 类型             | 是否必需 | 描述                                                       |
|------------|----------------|------|----------------------------------------------------------|
| `id`       | integer/string | yes  | ID or [URL 编码的项目路径](../rest/index.md#namespaced-path-encoding)|
| `codename` | string         | no   | 使用特定 `codename` 过滤                                       |
| `suite`    | string         | no   | 使用特定 `suite` 过滤                                          |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/debian_distributions"
```

响应示例：

```json
[
  {
    "id": 1,
    "codename": "sid",
    "suite": null,
    "origin": null,
    "label": null,
    "version": null,
    "description": null,
    "valid_time_duration_seconds": null,
    "components": [
      "main"
    ],
    "architectures": [
      "all",
      "amd64"
    ]
  }
]
```

## 单个 Debian 项目发行版

获取单个 Debian 项目发行版。

```plaintext
GET /projects/:id/debian_distributions/:codename
```

| 参数         | 类型             | 是否必需 | 描述                                                               |
|------------|----------------|------|------------------------------------------------------------------|
| `id`       | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](../rest/index.md#namespaced-path-encoding) |
| `codename` | integer         | yes  | 发行版的 `codename`                                                  |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/debian_distributions/unstable"
```

响应示例：

```json
{
  "id": 1,
  "codename": "sid",
  "suite": null,
  "origin": null,
  "label": null,
  "version": null,
  "description": null,
  "valid_time_duration_seconds": null,
  "components": [
    "main"
  ],
  "architectures": [
    "all",
    "amd64"
  ]
}
```

## 单个 Debian 项目发行版密钥

获取单个 Debian 项目发行版密钥。

```plaintext
GET /projects/:id/debian_distributions/:codename/key.asc
```

| 参数         | 类型             | 是否必需 | 描述                                                               |
|------------|----------------|------|------------------------------------------------------------------|
| `id`       | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](../rest/index.md#namespaced-path-encoding) |
| `codename` | integer        | yes  | 发行版的 `codename`                                                  |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/debian_distributions/unstable/key.asc"
```

响应示例：

```plaintext
-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: Alice's OpenPGP certificate
Comment: https://www.ietf.org/id/draft-bre-openpgp-samples-01.html

mDMEXEcE6RYJKwYBBAHaRw8BAQdArjWwk3FAqyiFbFBKT4TzXcVBqPTB3gmzlC/U
b7O1u120JkFsaWNlIExvdmVsYWNlIDxhbGljZUBvcGVucGdwLmV4YW1wbGU+iJAE
ExYIADgCGwMFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AWIQTrhbtfozp14V6UTmPy
MVUMT0fjjgUCXaWfOgAKCRDyMVUMT0fjjukrAPoDnHBSogOmsHOsd9qGsiZpgRnO
dypvbm+QtXZqth9rvwD9HcDC0tC+PHAsO7OTh1S1TC9RiJsvawAfCPaQZoed8gK4
OARcRwTpEgorBgEEAZdVAQUBAQdAQv8GIa2rSTzgqbXCpDDYMiKRVitCsy203x3s
E9+eviIDAQgHiHgEGBYIACAWIQTrhbtfozp14V6UTmPyMVUMT0fjjgUCXEcE6QIb
DAAKCRDyMVUMT0fjjlnQAQDFHUs6TIcxrNTtEZFjUFm1M0PJ1Dng/cDW4xN80fsn
0QEA22Kr7VkCjeAEC08VSTeV+QFsmz55/lntWkwYWhmvOgE=
=iIGO
-----END PGP PUBLIC KEY BLOCK-----
```

## 创建 Debian 项目发行版

创建 Debian 项目发行版。

```plaintext
POST /projects/:id/debian_distributions
```

| 参数                            | 类型             | 是否必需 | 描述                                                               |
|-------------------------------|----------------|------|------------------------------------------------------------------|
| `id`                          | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](../rest/index.md#namespaced-path-encoding) |
| `codename`                    | string         | yes  | Debian 发行版的代号                                                    |
| `suite`                       | string         | no   | 新的 Debian 发行版的套件                                                 |
| `origin`                      | string         | no   | 新的 Debian 发行版的来源                                                 |
| `label`                       | string         | no   | 新的 Debian 发行版的标记                                                 |
| `version`                     | string         | no   | 新的 Debian 发行版的版本                                                 |
| `description`                 | string         | no   | 新的 Debian 发行版的描述                                                 |
| `valid_time_duration_seconds` | integer        | no   | 新的 Debian 发行版的有效时长（秒）                                            |
| `components`                  | architectures  | no   | 新的 Debian 发行版的组件列表                                               |
| `architectures`               | architectures  | no   | 新的 Debian 发行版的架构列表                                               |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/debian_distributions?codename=sid"
```

响应示例：

```json
{
  "id": 1,
  "codename": "sid",
  "suite": null,
  "origin": null,
  "label": null,
  "version": null,
  "description": null,
  "valid_time_duration_seconds": null,
  "components": [
    "main"
  ],
  "architectures": [
    "all",
    "amd64"
  ]
}
```

## 更新 Debian 项目发行版

更新 Debian 项目发行版。

```plaintext
PUT /projects/:id/debian_distributions/:codename
```

| 参数                            | 类型             | 是否必需 | 描述                                                               |
|-------------------------------|----------------|------|------------------------------------------------------------------|
| `id`                          | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](../rest/index.md#namespaced-path-encoding) |
| `codename`                    | string         | yes  | Debian 发行版的新代号                                                   |
| `suite`                       | string         | no   | Debian 发行版的新套件                                                   |
| `origin`                      | string         | no   | Debian 发行版的新来源                                                   |
| `label`                       | string         | no   | Debian 发行版的新标记                                                   |
| `version`                     | string         | no   | Debian 发行版的新版本                                                   |
| `description`                 | string         | no   | Debian 发行版的新描述                                                   |
| `valid_time_duration_seconds` | integer        | no   | Debian 发行版的新的有效时长（秒）                                             |
| `components`                  | architectures  | no   | Debian 发行版的新的组件列表                                                |
| `architectures`               | architectures  | no   | Debian 发行版的新的架构列表                                                |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/debian_distributions/unstable?suite=new-suite&valid_time_duration_seconds=604800"
```

响应示例：

```json
{
  "id": 1,
  "codename": "sid",
  "suite": "new-suite",
  "origin": null,
  "label": null,
  "version": null,
  "description": null,
  "valid_time_duration_seconds": 604800,
  "components": [
    "main"
  ],
  "architectures": [
    "all",
    "amd64"
  ]
}
```

## 删除 Debian 项目发行版

删除 Debian 项目发行版。

```plaintext
DELETE /projects/:id/debian_distributions/:codename
```

| 参数         | 类型             | 是否必需 | 描述                                                               |
|------------|----------------|------|------------------------------------------------------------------|
| `id`       | integer/string | yes  | 授权用户拥有的 ID 或 [URL 编码的项目路径](../rest/index.md#namespaced-path-encoding) |
| `codename` | integer        | yes  | Debian 发行版的代号                                                    |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/debian_distributions/unstable"
```
