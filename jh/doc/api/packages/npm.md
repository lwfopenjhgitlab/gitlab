---
stage: Package
group: Package Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.example/handbook/product/ux/technical-writing/#assignments
---

# npm API **(FREE)**

本文是 [npm 软件包的](../../user/packages/npm_registry/index.md) API 文档。

WARNING:
此 API 由 [npm 包管理器客户端](https://docs.npmjs.com/)使用并且通常不适合手动使用。

有关如何从极狐GitLab 软件包库上传和安装 npm 包的说明，请参阅 [npm 软件包库文档](../../user/packages/npm_registry/index.md)。

NOTE:
这些端点不遵守标准的 API 身份验证方法。
有关支持的 Header 和令牌类型的详细信息，请参阅 [npm 软件包库文档](../../user/packages/npm_registry/index.md)。将来可能会删除未记录的身份验证方法。

## 下载软件包

下载 npm 软件包。此 URL 由[元数据端点](#metadata)提供。

```plaintext
GET projects/:id/packages/npm/:package_name/-/:file_name
```

| 参数             | 类型     | 是否必需 | 描述                                    |
|----------------|--------| -------- |---------------------------------------|
| `id`           | string | yes      | 项目的 ID 或完整路径                          |
| `package_name` | string | yes      | 软件包的名称                                |
| `file_name`    | string | yes      | 软件包文件的名称 |

```shell
curl --header "Authorization: Bearer <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/npm/@myscope/my-pkg/-/@my-scope/my-pkg-0.0.1.tgz"
```

将输出写入到文件：

```shell
curl --header "Authorization: Bearer <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/npm/@myscope/my-pkg/-/@my-scope/my-pkg-0.0.1.tgz" >> @myscope/my-pkg-0.0.1.tgz
```

将下载文件写入到当前目录中的 `@myscope/my-pkg-0.0.1.tgz`。

## 上传软件包文件

上传软件包。

```plaintext
PUT projects/:id/packages/npm/:package_name
```

| 参数    | 类型   | 是否必需 | 描述           |
|----------------|--------|------|--------------|
| `id`           | string | yes  | 项目的 ID 或完整路径 |
| `package_name` | string | yes  | 软件包的名称       |
| `versions`     | string | yes  | 软件包版本信息      |

```shell
curl --request PUT
     --header "Content-Type: application/json"
     --data @./path/to/metadata/file.json
     --header "Authorization: Bearer <personal_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/packages/npm/@myscope%2fmy-pkg"
```

元数据文件内容由 npm 生成，类似以下内容：

```json
{
    "_attachments": {
        "@myscope/my-pkg-1.3.7.tgz": {
            "content_type": "application/octet-stream",
            "data": "H4sIAAAAAAAAE+1TQUvDMBjdeb/iI4edZEldV2dPwhARPIjiyXlI26zN1iYhSeeK7L+bNJtednMg4l4OKe+9PF7DF0XzNS0ZVmEfr4wUgxODEJLEMRzjPRJyCYPJNCFRlCTE+dzH1PvJqYscQ2ss1a7KT3PCv8DX/kfwMQRAgjYMpYBuIoIzKtwy6MILG6YNl8Jr0XgyvgpswUyuubJ75TGMDuSaUcsKyDooa1C6De6G8t7GRcG2br4CGxKME3wDR1hmrLexvJKwQLdaS52CkOAFMIrlfMlZsUAwGgHbcgsRcid3fdqade9SFz7u9a1naGsrqX3gHbcPNINDyydWcmN1By+W19x2oU7NcyZMfwn3z/PAqTaruanmUix5+V3UXVKq9yEoRZW1yqQYl9zWNBvnssFUcbyJsdJyxXJrcHQdz8gsTg6PzGChGty3H+6Gvz0BZ5xxxn/FJ1EDRNIACAAA",
            "length": 354
        }
    },
    "_id": "@myscope/my-pkg",
    "description": "Package created by me",
    "dist-tags": {
        "latest": "1.3.7"
    },
    "name": "@myscope/my-pkg",
    "readme": "ERROR: No README data found!",
    "versions": {
        "1.3.7": {
            "_id": "@myscope/my-pkg@1.3.7",
            "_nodeVersion": "12.18.4",
            "_npmVersion": "6.14.6",
            "author": {
                "name": "GitLab Package Registry Utility"
            },
            "description": "Package created by me",
            "dist": {
                "integrity": "sha512-loy16p+Dtw2S43lBmD3Nye+t+Vwv7Tbhv143UN2mwcjaHJyBfGZdNCTXnma3gJCUSE/AR4FPGWEyCOOTJ+ev9g==",
                "shasum": "4a9dbd94ca6093feda03d909f3d7e6bd89d9d4bf",
                "tarball": "https://gitlab.example.com/api/v4/projects/1/packages/npm/@myscope/my-pkg/-/@myscope/my-pkg-1.3.7.tgz"
            },
            "keywords": [],
            "license": "ISC",
            "main": "index.js",
            "name": "@myscope/my-pkg",
            "publishConfig": {
                "@myscope:registry": "https://gitlab.example.com/api/v4/projects/1/packages/npm"
            },
            "readme": "ERROR: No README data found!",
            "scripts": {
                "test": "echo \"Error: no test specified\" && exit 1"
            },
            "version": "1.3.7"
        }
    }
}
```

## 路由前缀

剩余的路由中有两组相同的路由，每个路由都在不同的范围内发出请求：

- 使用实例级前缀在整个实例范围内发出请求。
- 使用项目级前缀在单个项目范围内提出请求。
- 使用群组级前缀在群组范围内发出请求。

本文档中的示例均使用项目级前缀。

### 实例级

```plaintext
 /packages/npm`
```

| 参数 | 类型   | 是否必需 | 描述          |
| --------- | ------ | -------- |-------------|
| `id`      | string | yes      | 群组 ID 或完整路径 |

### 项目级

```plaintext
 /projects/:id/packages/npm`
```

| 参数 | 类型   | 是否必需 | 描述          |
| --------- | ------ | -------- |-------------|
| `id`      | string | yes      | 项目 ID 或完整路径 |

### 群组级

> - 引入于极狐GitLab 16.0，[功能标志](../../administration/feature_flags.md)为 `npm_group_level_endpoints`。默认禁用。
> - 普遍可用于极狐GitLab 16.1。移除功能标志 `npm_group_level_endpoints`。

```plaintext
 /groups/:id/-/packages/npm`
```

| 参数 | 类型   | 是否必需 | 描述          |
| --------- | ------ | -------- |-------------|
| `id`      | string | yes      | 群组 ID 或完整路径 |

<a id="metadata"></a>

## 元数据

返回特定软件包的元数据。

```plaintext
GET <route-prefix>/:package_name
```

| 参数      | 类型   | 是否必需 | 描述    |
| -------------- | ------ | -------- |-------|
| `package_name` | string | yes      | 软件包名称 |

```shell
curl --header "Authorization: Bearer <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/npm/@myscope/my-pkg"
```

响应示例：

```json
{
  "name": "@myscope/my-pkg",
  "versions": {
    "0.0.2": {
      "name": "@myscope/my-pkg",
      "version": "0.0.1",
      "dist": {
        "shasum": "93abb605b1110c0e3cca0a5b805e5cb01ac4ca9b",
        "tarball": "https://gitlab.example.com/api/v4/projects/1/packages/npm/@myscope/my-pkg/-/@myscope/my-pkg-0.0.1.tgz"
      }
    }
  },
  "dist-tags": {
    "latest": "0.0.1"
  }
}
```

响应中的 URL 所拥有的路由前缀与请求这些 URL 的路由前缀相同。如果您使用实例级路由进行请求，返回的 URL 包含 `/api/v4/packages/npm`。

## Dist-Tags

### 列出标签

> 引入于极狐GitLab 12.7。

列出软件包的 dist-tags。

```plaintext
GET <route-prefix>/-/package/:package_name/dist-tags
```

| 参数      | 类型   | 是否必需 | 描述    |
| -------------- | ------ | -------- |-------|
| `package_name` | string | yes      | 软件包名称 |

```shell
curl --header "Authorization: Bearer <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/npm/-/package/@myscope/my-pkg/dist-tags"
```

响应示例：

```json
{
  "latest": "2.1.1",
  "stable": "1.0.0"
}
```

响应中的 URL 所拥有的路由前缀与请求这些 URL 的路由前缀相同。如果您使用实例级路由进行请求，返回的 URL 包含 `/api/v4/packages/npm`。

### 创建或更新标签

> 引入于极狐GitLab 12.7。

创建或更新 dist-tag。

```plaintext
PUT <route-prefix>/-/package/:package_name/dist-tags/:tag
```

| 参数      | 类型   | 是否必需 | 描述       |
| -------------- | ------ | -------- |----------|
| `package_name` | string | yes      | 软件包的名称   |
| `tag`          | string | yes      | 创建或更新的标签 |
| `version`      | string | yes      | 要打标签的版本  |

```shell
curl --request PUT --header "Authorization: Bearer <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/npm/-/package/@myscope/my-pkg/dist-tags/stable"
```

此端点响应成功，返回 `204 No Content`。

### 删除标签

> 引入于极狐GitLab 12.7。

删除 dist-tag。

```plaintext
DELETE <route-prefix>/-/package/:package_name/dist-tags/:tag
```

| 参数      | 类型     | 是否必需 | 描述        |
| -------------- |--------| -------- |-----------|
| `package_name` | string | yes      | 软件包的名称    |
| `tag`          | string | yes      | 要创建或更新的标签 |

```shell
curl --request DELETE --header "Authorization: Bearer <personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/npm/-/package/@myscope/my-pkg/dist-tags/stable"
```

此端点响应成功，返回 `204 No Content`。
