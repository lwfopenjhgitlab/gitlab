---
stage: Package
group: Package Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# PyPI API **(FREE)**

本文是 [PyPI 软件包](../../user/packages/pypi_repository/index.md)的 API 文档。

WARNING:
此 API 由 [PyPI 软件包管理器客户端](https://pypi.org/)使用并且通常不适合手动使用。

有关如何从极狐GitLab 软件包库上传和安装 PyPI 包的说明，请参阅 [PyPI 软件包库文档](../../user/packages/pypi_repository/index.md)。

NOTE:
这些端点不遵守标准的 API 身份验证方法。
有关支持的 Header 和令牌类型的详细信息，请参阅 [PyPI 软件包库文档](../../user/packages/pypi_repository/index.md)。将来可能会删除未记录的身份验证方法。

NOTE:
当启用 FIPS 模式时，推荐使用 [Twine 3.4.2](https://twine.readthedocs.io/en/stable/changelog.html?highlight=FIPS#id28) 或更高版本。

## 从群组下载软件包文件

> 引入于极狐GitLab 13.12。

下载 PyPI 包文件。 [简单 API](#group-level-simple-api-entry-point) 通常提供此 URL。

```plaintext
GET groups/:id/-/packages/pypi/files/:sha256/:file_identifier
```

| 参数                | 类型     | 是否必需 | 描述                     |
|-------------------|--------|------|------------------------|
| `id`              | string | yes  | 群组的 ID 或完整路径           |
| `sha256`          | string | yes  | PyPI 软件包文件的 sha256 校验和 |
| `file_identifier` | string | yes  | PyPI 软件包文件的名称          |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/groups/1/-/packages/pypi/files/5y57017232013c8ac80647f4ca153k3726f6cba62d055cd747844ed95b3c65ff/my.pypi.package-0.0.1.tar.gz"
```

将输出写入文件：

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/groups/1/-/packages/pypi/files/5y57017232013c8ac80647f4ca153k3726f6cba62d055cd747844ed95b3c65ff/my.pypi.package-0.0.1.tar.gz" >> my.pypi.package-0.0.1.tar.gz
```

将下载文件写入当前目录的 `my.pypi.package-0.0.1.tar.gz`。

## 群组级的简单 API 索引

> 引入于极狐GitLab 15.1。

将群组中的软件包列表返回为 HTML 文件：

```plaintext
GET groups/:id/-/packages/pypi/simple
```

| 参数   | 类型     | 是否必需 | 描述           |
|------|--------|------|--------------|
| `id` | string | yes  | 群组的 ID 或完整路径 |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/groups/1/-/packages/pypi/simple"
```

响应示例：

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Links for Group</title>
  </head>
  <body>
    <h1>Links for Group</h1>
    <a href="https://gitlab.example.com/api/v4/groups/1/-/packages/pypi/simple/my-pypi-package" data-requires-python="">my.pypi.package</a><br><a href="https://gitlab.example.com/api/v4/groups/1/-/packages/pypi/simple/package-2" data-requires-python="3.8">package_2</a><br>
  </body>
</html>
```

将输出写入文件：

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/groups/1/-/packages/pypi/simple" >> simple_index.html
```

将下载文件写入当前目录的 `simple_index.html`。

<a id="group-level-simple-api-entry-point"></a>

## 群组级别的简单 API 条目点

> 引入于极狐GitLab 13.12。

将软件包描述符返回为 HTML 文件：

```plaintext
GET groups/:id/-/packages/pypi/simple/:package_name
```

| 参数             | 类型     | 是否必需 | 描述           |
|----------------|--------|------|--------------|
| `id`           | string | yes  | 群组的 ID 或完整路径 |
| `package_name` | string | yes  | 软件包名称        |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/groups/1/-/packages/pypi/simple/my.pypi.package"
```

响应示例：

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Links for my.pypi.package</title>
  </head>
  <body>
    <h1>Links for my.pypi.package</h1>
    <a href="https://gitlab.example.com/api/v4/groups/1/-/packages/pypi/files/5y57017232013c8ac80647f4ca153k3726f6cba62d055cd747844ed95b3c65ff/my.pypi.package-0.0.1-py3-none-any.whl#sha256=5y57017232013c8ac80647f4ca153k3726f6cba62d055cd747844ed95b3c65ff" data-requires-python="&gt;=3.6">my.pypi.package-0.0.1-py3-none-any.whl</a><br><a href="https://gitlab.example.com/api/v4/groups/1/-/packages/pypi/files/9s9w01b0bcd52b709ec052084e33a5517ffca96f7728ddd9f8866a30cdf76f2/my.pypi.package-0.0.1.tar.gz#sha256=9s9w011b0bcd52b709ec052084e33a5517ffca96f7728ddd9f8866a30cdf76f2" data-requires-python="&gt;=3.6">my.pypi.package-0.0.1.tar.gz</a><br>
  </body>
</html>
```

将输出写入文件：

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/groups/1/-/packages/pypi/simple/my.pypi.package" >> simple.html
```

将下载文件写入当前目录的 `simple.html`。

## 从项目下载软件包文件

> 引入于极狐GitLab 12.10。

下载 PyPI 软件包文件。[简单的 API](#project-level-simple-api-entry-point) 通常提供此 URL。

```plaintext
GET projects/:id/packages/pypi/files/:sha256/:file_identifier
```

| 参数                | 类型     | 是否必需 | 描述                    |
|-------------------|--------|------|-----------------------|
| `id`              | string | yes  | 项目的 ID 或完整路径          |
| `sha256`          | string | yes  | PyPI 软件包文件 sha256 校验和 |
| `file_identifier` | string | yes  | PyPI 软件包文件名           |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/pypi/files/5y57017232013c8ac80647f4ca153k3726f6cba62d055cd747844ed95b3c65ff/my.pypi.package-0.0.1.tar.gz"
```

将输出写入文件：

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/pypi/files/5y57017232013c8ac80647f4ca153k3726f6cba62d055cd747844ed95b3c65ff/my.pypi.package-0.0.1.tar.gz" >> my.pypi.package-0.0.1.tar.gz
```

将下载文件写入当前目录的 `my.pypi.package-0.0.1.tar.gz`。

## 项目级的简单 API 索引

> 引入于极狐GitLab 15.1。

将项目中的软件包列表返回为 HTML 文件：

```plaintext
GET projects/:id/packages/pypi/simple
```

| 参数   | 类型     | 是否必需 | 描述           |
|------|--------|------|--------------|
| `id` | string | yes  | 项目的 ID 或完整路径 |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/pypi/simple"
```

响应示例：

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Links for Project</title>
  </head>
  <body>
    <h1>Links for Project</h1>
    <a href="https://gitlab.example.com/api/v4/projects/1/packages/pypi/simple/my-pypi-package" data-requires-python="">my.pypi.package</a><br><a href="https://gitlab.example.com/api/v4/projects/1/packages/pypi/simple/package-2" data-requires-python="3.8">package_2</a><br>
  </body>
</html>
```

将输出写入文件：

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/pypi/simple" >> simple_index.html
```

将下载文件写入当前目录的 `simple_index.html`。

<a id="project-level-simple-api-entry-point"></a>

## 项目级的简单 API 条目点

> 引入于极狐GitLab 12.10。

将软件包描述符返回为 HTML 文件：

```plaintext
GET projects/:id/packages/pypi/simple/:package_name
```

| 参数             | 类型     | 是否必需 | 描述           |
|----------------|--------|------|--------------|
| `id`           | string | yes  | 项目的 ID 或完整路径 |
| `package_name` | string | yes  | 软件包名称        |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/pypi/simple/my.pypi.package"
```

响应示例：

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Links for my.pypi.package</title>
  </head>
  <body>
    <h1>Links for my.pypi.package</h1>
    <a href="https://gitlab.example.com/api/v4/projects/1/packages/pypi/files/5y57017232013c8ac80647f4ca153k3726f6cba62d055cd747844ed95b3c65ff/my.pypi.package-0.0.1-py3-none-any.whl#sha256=5y57017232013c8ac80647f4ca153k3726f6cba62d055cd747844ed95b3c65ff" data-requires-python="&gt;=3.6">my.pypi.package-0.0.1-py3-none-any.whl</a><br><a href="https://gitlab.example.com/api/v4/projects/1/packages/pypi/files/9s9w01b0bcd52b709ec052084e33a5517ffca96f7728ddd9f8866a30cdf76f2/my.pypi.package-0.0.1.tar.gz#sha256=9s9w011b0bcd52b709ec052084e33a5517ffca96f7728ddd9f8866a30cdf76f2" data-requires-python="&gt;=3.6">my.pypi.package-0.0.1.tar.gz</a><br>
  </body>
</html>
```

将输出写入文件：

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/pypi/simple/my.pypi.package" >> simple.html
```

将下载文件写入当前目录的 `simple.html`。

## 上传软件包

上传 PyPI 软件包：

```plaintext
PUT projects/:id/packages/pypi
```

| 参数                | 类型     | 是否必需 | 描述           |
|-------------------|--------|------|--------------|
| `id`              | string | yes  | 项目的 ID 或完整路径 |
| `requires_python` | string | no   | 必需的 PyPI 版本  |

```shell
curl --request POST \
     --form 'content=@path/to/my.pypi.package-0.0.1.tar.gz' \
     --form 'name=my.pypi.package'
     --form 'version=1.3.7'
     --user <username>:<personal_access_token> \
     "https://gitlab.example.com/api/v4/projects/1/packages/pypi"
```
