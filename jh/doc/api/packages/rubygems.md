---
stage: Package
group: Package Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Ruby gems API **(FREE SELF)**

本文是 [Ruby gems](../../user/packages/rubygems_registry/index.md) 的 API 文档。

WARNING:
此 API 由 [Ruby gems 和 Bundler 包管理器客户端](https://maven.apache.org/)使用并且通常不适合手动使用。此 API 在研发中，由于功能有限，尚未准备好用于生产用途。

有关如何从极狐GitLab 软件包库上传和安装 gems 的说明，请参阅 [Ruby gems 库文档](../../user/packages/rubygems_registry/index.md)。

NOTE:
这些端点不遵守标准的 API 身份验证方法。
有关支持的 Header 和令牌类型的详细信息，请参阅 [Ruby gems 库文档](../../user/packages/rubygems_registry/index.md)。将来可能会删除未记录的身份验证方法。

## 启用 Ruby gems API

极狐GitLab 的 Ruby gems API 位于默认禁用的功能标志后面。有权访问极狐GitLab Rails 控制台的管理员可以为您的实例启用此 API。

启用：

```ruby
Feature.enable(:rubygem_packages)
```

禁用：

```ruby
Feature.disable(:rubygem_packages)
```

为特定项目启用或禁用：

```ruby
Feature.enable(:rubygem_packages, Project.find(1))
Feature.disable(:rubygem_packages, Project.find(2))
```

## 下载 gem 文件

> 引入于极狐GitLab 13.10。

下载 gem：

```plaintext
GET projects/:id/packages/rubygems/gems/:file_name
```

| 参数          | 类型     | 是否必需 | 描述           |
|-------------|--------|------|--------------|
| `id`        | string | yes  | 项目的 ID 或完整路径 |
| `file_name` | string | yes  | `.gem` 文件的名称 |

```shell
curl --header "Authorization:<personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/rubygems/gems/my_gem-1.0.0.gem"
```

将输出写入文件：

```shell
curl --header "Authorization:<personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/rubygems/gems/my_gem-1.0.0.gem" >> my_gem-1.0.0.gem
```

将下载文件写入当前目录中的 `my_gem-1.0.0.gem`。

## 获取依赖项列表

> 引入于极狐GitLab 13.10。

为 gem 列表获取依赖项列表：

```plaintext
GET projects/:id/packages/rubygems/api/v1/dependencies
```

| 参数     | 类型     | 是否必需 | 描述                                               |
|--------|--------|------|--------------------------------------------------|
| `id`   | string | yes  | 项目的 ID 或完整路径                                     |
| `gems` | string | no   | 以逗号分隔的为其获取依赖项的 gem 列表 |

```shell
curl --header "Authorization:<personal_access_token>" "https://gitlab.example.com/api/v4/projects/1/packages/rubygems/api/v1/dependencies?gems=my_gem,foo"
```

此端点为请求的 gem 的所有版本返回一个经过编组的哈希数组。由于响应已编组，您可以将其存储在文件中。如果安装了 Ruby，则可以使用以下
Ruby 命令读取响应。为此，您必须[在 `~/.gem/credentials` 中设置证书](../../user/packages/rubygems_registry/index.md#authenticate-with-a-personal-access-token-or-deploy-token)：

```shell
$ ruby -ropen-uri -rpp -e \
  'pp Marshal.load(open("https://gitlab.example.com/api/v4/projects/1/packages/rubygems/api/v1/dependencies?gems=my_gem,rails,foo"))'

[{:name=>"my_gem", :number=>"0.0.1", :platform=>"ruby", :dependencies=>[]},
 {:name=>"my_gem",
  :number=>"0.0.3",
  :platform=>"ruby",
  :dependencies=>
   [["dependency_1", "~> 1.2.3"],
    ["dependency_2", "= 3.0.0"],
    ["dependency_3", ">= 1.0.0"],
    ["dependency_4", ">= 0"]]},
 {:name=>"my_gem",
  :number=>"0.0.2",
  :platform=>"ruby",
  :dependencies=>
   [["dependency_1", "~> 1.2.3"],
    ["dependency_2", "= 3.0.0"],
    ["dependency_3", ">= 1.0.0"],
    ["dependency_4", ">= 0"]]},
 {:name=>"foo",
  :number=>"0.0.2",
  :platform=>"ruby",
  :dependencies=>
    ["dependency_2", "= 3.0.0"],
    ["dependency_4", ">= 0"]]}]
```

将下载文件写入到当前目录中的 `mypkg-1.0-SNAPSHOT.jar`。

## 上传 gem

> 引入于极狐GitLab 13.11。

上传 gem：

```plaintext
POST projects/:id/packages/rubygems/api/v1/gems
```

| 参数   | 类型     | 是否必需 | 描述           |
|------|--------|------|--------------|
| `id` | string | yes  | 项目的 ID 或完整路径 |

```shell
curl --request POST \
     --upload-file path/to/my_gem_file.gem \
     --header "Authorization:<personal_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/packages/rubygems/api/v1/gems"
```
