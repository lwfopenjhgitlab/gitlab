---
stage: Package
group: Package Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Terraform Module Registry API **(FREE)**

本文是 [Terraform Module Registry](../../user/packages/terraform_module_registry/index.md)的 API 文档。

<!--WARNING:
This API is used by the [terraform cli](https://www.terraform.io/)
and is generally not meant for manual consumption. Undocumented authentication methods might be removed in the future.-->

有关如何从极狐GitLab Terraform Module Registry 上传和安装 Terraform Module 的说明，请参阅 [Terraform Module Registry 文档](../../user/packages/terraform_module_registry/index.md)。

## 列出特定 Module 的可用版本

获取特定 Module 的可用版本。

```plaintext
GET packages/terraform/modules/v1/:module_namespace/:module_name/:module_system/versions
```

| 参数                 | 类型     | 是否必需 | 描述                                                              |
|--------------------|--------|------|-----------------------------------------------------------------|
| `module_namespace` | string | yes  | Terraform Module 项目或子群组所属的顶级群组（命名空间）                            |
| `module_name`      | string | yes  | Module 名称                                                       |
| `module_system`    | string | yes  | Module 系统或[提供者](https://www.terraform.io/registry/providers)的名称 |

```shell
curl --header "Authorization: Bearer <personal_access_token>" "https://gitlab.example.com/api/v4/packages/terraform/modules/v1/group/hello-world/local/versions"
```

响应示例：

```json
{
  "modules": [
    {
      "versions": [
        {
          "version": "1.0.0",
          "submodules": [],
          "root": {
            "dependencies": [],
            "providers": [
              {
                "name": "local",
                "version":""
              }
            ]
          }
        },
        {
          "version": "0.9.3",
          "submodules": [],
          "root": {
            "dependencies": [],
            "providers": [
              {
                "name": "local",
                "version":""
              }
            ]
          }
        }
      ],
      "source": "https://gitlab.example.com/group/hello-world"
    }
  ]
}
```

## 特定 Module 的最新版本

获取特定 Module 的最新版本信息。

```plaintext
GET packages/terraform/modules/v1/:module_namespace/:module_name/:module_system
```

| 参数                 | 类型    | 是否必需 | 描述                                                              |
|--------------------|-------|------|-----------------------------------------------------------------|
| `module_namespace` | string | yes  | Terraform Module 项目所属的群组                                        |
| `module_name`      | string | yes  | Module 名称                                                       |
| `module_system`    | string | yes  | Module 系统或[提供者](https://www.terraform.io/registry/providers)的名称 |

```shell
curl --header "Authorization: Bearer <personal_access_token>" "https://gitlab.example.com/api/v4/packages/terraform/modules/v1/group/hello-world/local"
```

响应示例：

```json
{
  "name": "hello-world/local",
  "provider": "local",
  "providers": [
    "local"
  ],
  "root": {
    "dependencies": []
  },
  "source": "https://gitlab.example.com/group/hello-world",
  "submodules": [],
  "version": "1.0.0",
  "versions": [
    "1.0.0"
  ]
}
```

## 获取特定 Module 的特定版本

获取特定 Module 的特定版本信息。

```plaintext
GET packages/terraform/modules/v1/:module_namespace/:module_name/:module_system/1.0.0
```

| 参数                 | 类型     | 是否必需 | 描述                                                              |
|--------------------|--------|------|-----------------------------------------------------------------|
| `module_namespace` | string | yes  | Terraform Module 项目所属的群组                                        |
| `module_name`      | string | yes  | Module 名称                                                       |
| `module_system`    | string | yes  | Module 系统或[提供者](https://www.terraform.io/registry/providers)的名称 |

```shell
curl --header "Authorization: Bearer <personal_access_token>" "https://gitlab.example.com/api/v4/packages/terraform/modules/v1/group/hello-world/local/1.0.0"
```

响应示例：

```json
{
  "name": "hello-world/local",
  "provider": "local",
  "providers": [
    "local"
  ],
  "root": {
    "dependencies": []
  },
  "source": "https://gitlab.example.com/group/hello-world",
  "submodules": [],
  "version": "1.0.0",
  "versions": [
    "1.0.0"
  ]
}
```

## 获取下载最新 Module 版本的 URL

在 `X-Terraform-Get` Header 中获取最新 Module 版本的下载 URL。

```plaintext
GET packages/terraform/modules/v1/:module_namespace/:module_name/:module_system/download
```

| 参数                 | 类型     | 是否必需 | 描述                                                              |
|--------------------|--------|------|-----------------------------------------------------------------|
| `module_namespace` | string | yes  | Terraform Module 项目所属的群组                                        |
| `module_name`      | string | yes  | Module 名称                                                       |
| `module_system`    | string | yes  | Module 系统或[提供者](https://www.terraform.io/registry/providers)的名称 |

```shell
curl --header "Authorization: Bearer <personal_access_token>" "https://gitlab.example.com/api/v4/packages/terraform/modules/v1/group/hello-world/local/download"
```

响应示例：

```plaintext
HTTP/1.1 204 No Content
Content-Length: 0
X-Terraform-Get: /api/v4/packages/terraform/modules/v1/group/hello-world/local/1.0.0/file?token=&archive=tgz
```

在底层，此 API 端点跳转到 `packages/terraform/modules/v1/:module_namespace/:module_name/:module_system/:module_version/download`。

## 获取下载特定 Module 版本的 URL

在 `X-Terraform-Get` Header 中获取下载特定 Module 版本的 URL。

```plaintext
GET packages/terraform/modules/v1/:module_namespace/:module_name/:module_system/:module_version/download
```

| 参数                 | 类型     | 是否必需 | 描述                                                              |
|--------------------|--------|------|-----------------------------------------------------------------|
| `module_namespace` | string | yes  | Terraform Module 项目所属的群组                                        |
| `module_name`      | string | yes  | Module 名称                                                       |
| `module_system`    | string | yes  | Module 系统或[提供者](https://www.terraform.io/registry/providers)的名称 |
| `module_version`   | string | yes  | 要下载的特定 Module 版本         |

```shell
curl --header "Authorization: Bearer <personal_access_token>" "https://gitlab.example.com/api/v4/packages/terraform/modules/v1/group/hello-world/local/1.0.0/download"
```

响应示例：

```plaintext
HTTP/1.1 204 No Content
Content-Length: 0
X-Terraform-Get: /api/v4/packages/terraform/modules/v1/group/hello-world/local/1.0.0/file?token=&archive=tgz
```

## 下载 Module

```plaintext
GET packages/terraform/modules/v1/:module_namespace/:module_name/:module_system/:module_version/file
```

| 参数                 | 类型     | 是否必需 | 描述                                                              |
|--------------------|--------|------|-----------------------------------------------------------------|
| `module_namespace` | string | yes  | Terraform Module 项目所属的群组                                        |
| `module_name`      | string | yes  | Module 名称                                                       |
| `module_system`    | string | yes  | Module 系统或[提供者](https://www.terraform.io/registry/providers)的名称 |
| `module_version`   | string | yes  | 要下载的特定 Module 版本                            |

```shell
curl --header "Authorization: Bearer <personal_access_token>" "https://gitlab.example.com/api/v4/packages/terraform/modules/v1/group/hello-world/local/1.0.0/file"
```

将输出写入文件：

```shell
curl --header "Authorization: Bearer <personal_access_token>" "https://gitlab.example.com/api/v4/packages/terraform/modules/v1/group/hello-world/local/1.0.0/file" --output hello-world-local.tgz
```

