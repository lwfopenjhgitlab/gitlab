---
stage: Package
group: Package Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Helm API **(FREE)**

这是 [Helm](../../user/packages/helm_repository/index.md) 的 API 文档。

WARNING:
此 API 为 Helm 相关的包客户端所用，诸如 [Helm](https://helm.sh/) 和 [`helm-push`](https://github.com/chartmuseum/helm-push/#readme)，通常不用于手动操作。

关于如何从极狐GitLab 软件包库上传以及安装 Helm 包的指导，请查看 [Helm 包仓库文档](../../user/packages/helm_repository/index.md)。

NOTE:
这些端点并不遵从标准的 API 认证方法。查看 [Helm 包仓库文档](../../user/packages/helm_repository/index.md)以了解支持哪些 Header 和令牌类型。将来可能会删除未记录的身份验证方法。

## Download chart 索引

> 引入于极狐GitLab 14.1 版本。

下载 Chart 索引：

```plaintext
GET projects/:id/packages/helm/:channel/index.yaml
```

| 参数 | 类型   | 是否必需 | 描述 |
| --------- | ------ | -------- | ----------- |
| `id`      | string | 是      | 项目 ID 或全路径。 |
| `channel` | string | 是      | Helm 仓库通道。 |

```shell
curl --user <username>:<personal_access_token> \
     "https://gitlab.example.com/api/v4/projects/1/packages/helm/stable/index.yaml"
```

将输出写到文件中：

```shell
curl --user <username>:<personal_access_token> \
     "https://gitlab.example.com/api/v4/projects/1/packages/helm/stable/index.yaml" \
     --remote-name
```

## 下载 chart

> 引入于极狐GitLab 14.0 版本。

下载 chart：

```plaintext
GET projects/:id/packages/helm/:channel/charts/:file_name.tgz
```

| 参数 | 类型   | 是否必需 | 描述 |
| ----------- | ------ | -------- | ----------- |
| `id`        | string | 是      | 项目 ID 或全路径。 |
| `channel`   | string | 是      | Helm 仓库通道。 |
| `file_name` | string | 是      | Chart 文件名称。 |

```shell
curl --user <username>:<personal_access_token> \
     "https://gitlab.example.com/api/v4/projects/1/packages/helm/stable/charts/mychart.tgz" \
     --remote-name
```

## 上传 chart

> 引入于极狐GitLab 14.1 版本。

上传 chart：

```plaintext
POST projects/:id/packages/helm/api/:channel/charts
```

| 参数 | 类型   | 是否必需 | 描述 |
| --------- | ------ | -------- | ----------- |
| `id`      | string | 是      | 项目 ID 或全路径。 |
| `channel` | string | 是      | Helm 仓库通道。 |
| `chart`   | file   | 是      | Chart (如 `multipart/form-data`)。 |

```shell
curl --request POST \
     --form 'chart=@mychart.tgz' \
     --user <username>:<personal_access_token> \
     "https://gitlab.example.com/api/v4/projects/1/packages/helm/api/stable/charts"
```
