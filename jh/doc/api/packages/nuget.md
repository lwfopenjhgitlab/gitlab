---
stage: Package
group: Package Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about..example/handbook/product/ux/technical-writing/#assignments
---

# NuGet API **(FREE)**

本文是 NuGet 软件包<!--[NuGet 软件包](../../user/packages/nuget_repository/index.md)-->的 API 文档。

WARNING:
此 API 由 [NuGet 包管理器客户端](https://www.nuget.org/)使用并且通常不适合手动使用。

有关如何从极狐GitLab 软件包库上传和安装 NuGet 包的说明，请参阅 NuGet 软件包库文档。<!--[NuGet 软件包库文档](../../user/packages/nuget_repository/index.md)。-->

NOTE:
这些端点不遵守标准的 API 身份验证方法。
<!--See the [NuGet package registry documentation](../../user/packages/nuget_repository/index.md)
for details on which headers and token types are supported. Undocumented authentication methods might be removed in the future.-->

## 软件包索引

> 引入于极狐GitLab 12.8。

返回特定软件包的索引，包括可用版本的列表。

```plaintext
GET projects/:id/packages/nuget/download/:package_name/index
```

| 参数             | 类型     | 是否必需 | 描述           |
|----------------|--------|------|--------------|
| `id`           | string | yes  | 项目的 ID 或完整路径 |
| `package_name` | string | yes  | 软件包的名称       |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/nuget/download/MyNuGetPkg/index"
```

响应示例：

```json
{
  "versions": [
    "1.3.0.17"
  ]
}
```

## 下载软件包文件

> 引入于极狐GitLab 12.8。

下载 NuGet 软件包文件。[元数据服务](#metadata-service)提供此 URL。

```plaintext
GET projects/:id/packages/nuget/download/:package_name/:package_version/:package_filename
```

| 参数                 | 类型   | 是否必需 | 描述         |
|--------------------| ------ | -------- |------------|
| `id`               | string | yes      | 项目的 ID 或完整路径 |
| `package_name`     | string | yes      | 软件包名称      |
| `package_version`  | string | yes      | 软件包版本      |
| `package_filename` | string | yes      | 文件名称       |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/nuget/download/MyNuGetPkg/1.3.0.17/mynugetpkg.1.3.0.17.nupkg"
```

将输出写入文件：

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/nuget/download/MyNuGetPkg/1.3.0.17/mynugetpkg.1.3.0.17.nupkg" > MyNuGetPkg.1.3.0.17.nupkg
```

将下载文件写入当前目录中的 `MyNuGetPkg.1.3.0.17.nupkg`。

## 上传软件包文件

> 引入于极狐GitLab 12.8。

上传 NuGet 软件包文件：

```plaintext
PUT projects/:id/packages/nuget
```

| 参数         | 类型   | 是否必需 | 描述           |
| ----------------- | ------ | -------- |--------------|
| `id`              | string | yes      | 项目的 ID 或完整路径 |
| `package_name`    | string | yes      | 软件包名称        |
| `package_version` | string | yes      | 软件包版本        |
| `package_filename`| string | yes      | 文件名称         |

```shell
curl --request PUT \
     --form 'package=@path/to/mynugetpkg.1.3.0.17.nupkg' \
     --user <username>:<personal_access_token> \
     "https://gitlab.example.com/api/v4/projects/1/packages/nuget/"
```

## 上传符号软件包文件

> 引入于极狐GitLab 12.8。

上传 NuGet 符号软件包文件（`.snupkg`）：

```plaintext
PUT projects/:id/packages/nuget/symbolpackage
```

| 参数         | 类型   | 是否必需 | 描述           |
| ----------------- | ------ | -------- |--------------|
| `id`              | string | yes      | 项目的 ID 或完整路径 |
| `package_name`    | string | yes      | 软件包名称        |
| `package_version` | string | yes      | 软件包版本        |
| `package_filename`| string | yes      | 文件名称         |

```shell
curl --request PUT \
     --form 'package=@path/to/mynugetpkg.1.3.0.17.snupkg' \
     --user <username>:<personal_access_token> \
     "https://gitlab.example.com/api/v4/projects/1/packages/nuget/symbolpackage"
```

## 路由前缀

剩余的路由中有两组相同的路由，每个路由都在不同的范围内发出请求：

- 使用群组级前缀在群组范围内发出请求。
- 使用项目级前缀在单个项目范围内发出请求。

本文档中的示例均使用项目级前缀。

### 群组级

```plaintext
 /groups/:id/-/packages/nuget`
```

| 参数 | 类型   | 是否必需 | 描述           |
| --------- | ------ | -------- |--------------|
| `id`      | string | yes      | 群组的 ID 或完整路径 |

### 项目级

```plaintext
 /projects/:id/packages/nuget`
```

| 参数 | 类型   | 是否必需 | 描述           |
| --------- | ------ | -------- |--------------|
| `id`      | string | yes      | 项目的 ID 或完整路径 |

## 服务索引

> - 在极狐GitLab 16.1 中变更为公开。


返回可用 API 资源的列表。不强制进行身份验证：

```plaintext
GET <route-prefix>/index
```

请求示例：

```shell
curl "https://gitlab.example.com/api/v4/projects/1/packages/nuget/index"
```

响应示例：

```json
{
  "version": "3.0.0",
  "resources": [
    {
      "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/query",
      "@type": "SearchQueryService",
      "comment": "Filter and search for packages by keyword."
    },
    {
      "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/query",
      "@type": "SearchQueryService/3.0.0-beta",
      "comment": "Filter and search for packages by keyword."
    },
    {
      "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/query",
      "@type": "SearchQueryService/3.0.0-rc",
      "comment": "Filter and search for packages by keyword."
    },
    {
      "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/metadata",
      "@type": "RegistrationsBaseUrl",
      "comment": "Get package metadata."
    },
    {
      "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/metadata",
      "@type": "RegistrationsBaseUrl/3.0.0-beta",
      "comment": "Get package metadata."
    },
    {
      "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/metadata",
      "@type": "RegistrationsBaseUrl/3.0.0-rc",
      "comment": "Get package metadata."
    },
    {
      "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/download",
      "@type": "PackageBaseAddress/3.0.0",
      "comment": "Get package content (.nupkg)."
    },
    {
      "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget",
      "@type": "PackagePublish/2.0.0",
      "comment": "Push and delete (or unlist) packages."
    },
    {
      "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/symbolpackage",
      "@type": "SymbolPackagePublish/4.9.0",
      "comment": "Push symbol packages."
    }
  ]
}
```

响应中的 URL 所拥有的路由前缀与请求这些 URL 的路由前缀相同。如果您使用群组级路由进行请求，返回的 URL 包含 `/groups/:id/-`。

<a id="metadata-service"></a>

## 元数据服务

> 引入于极狐GitLab 12.8。

返回软件包的元数据：

```plaintext
GET <route-prefix>/metadata/:package_name/index
```

| 参数      | 类型   | 是否必需 | 描述    |
| -------------- | ------ | -------- |-------|
| `package_name` | string | yes      | 软件包名称 |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/nuget/metadata/MyNuGetPkg/index"
```

响应示例：

```json
{
  "count": 1,
  "items": [
    {
      "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/metadata/MyNuGetPkg/1.3.0.17.json",
      "lower": "1.3.0.17",
      "upper": "1.3.0.17",
      "count": 1,
      "items": [
        {
          "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/metadata/MyNuGetPkg/1.3.0.17.json",
          "packageContent": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/download/MyNuGetPkg/1.3.0.17/helloworld.1.3.0.17.nupkg",
          "catalogEntry": {
            "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/metadata/MyNuGetPkg/1.3.0.17.json",
            "authors": "Author1, Author2",
            "dependencyGroups": [],
            "id": "MyNuGetPkg",
            "version": "1.3.0.17",
            "tags": "",
            "packageContent": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/download/MyNuGetPkg/1.3.0.17/helloworld.1.3.0.17.nupkg",
            "summary": "Summary of the package",
            "published": "2023-05-08T17:23:25Z",
          }
        }
      ]
    }
  ]
}
```

## 版本元数据服务

> 引入于极狐GitLab 12.8。

返回特定软件包版本的元数据：

```plaintext
GET <route-prefix>/metadata/:package_name/:package_version
```

| 参数         | 类型   | 是否必需 | 描述    |
| ----------------- | ------ |------|-------|
| `package_name`    | string | yes  | 软件包名称 |
| `package_version` | string | yes  | 软件包版本 |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/nuget/metadata/MyNuGetPkg/1.3.0.17"
```

响应示例：

```json
{
  "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/metadata/MyNuGetPkg/1.3.0.17.json",
  "packageContent": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/download/MyNuGetPkg/1.3.0.17/helloworld.1.3.0.17.nupkg",
  "catalogEntry": {
    "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/metadata/MyNuGetPkg/1.3.0.17.json",
    "authors": "Author1, Author2",
    "dependencyGroups": [],
    "id": "MyNuGetPkg",
    "version": "1.3.0.17",
    "tags": "",
    "packageContent": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/download/MyNuGetPkg/1.3.0.17/helloworld.1.3.0.17.nupkg",
    "summary": "Summary of the package",
    "published": "2023-05-08T17:23:25Z",
  }
}
```

## 搜索服务

> 引入于极狐GitLab 12.8。

给定查询，在仓库中搜索 NuGet 软件包：

```plaintext
GET <route-prefix>/query
```

| 参数    | 类型      | 是否必需 | 描述                                                  |
| ------------ |---------| -------- |-----------------------------------------------------|
| `q`          | string  | yes      | 搜索查询                                                |
| `skip`       | integer | no       | 跳过的结果数量                                             |
| `take`       | integer | no       | 返回的结果数量                                             |
| `prerelease` | boolean | no       | 包含预发布版本。如果未提供值，则默认为 `true` |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/nuget/query?q=MyNuGet"
```

响应示例：

```json
{
  "totalHits": 1,
  "data": [
    {
      "@type": "Package",
      "authors": "Author1, Author2",
      "id": "MyNuGetPkg",
      "title": "MyNuGetPkg",
      "summary": "Summary of the package",
      "totalDownloads": 0,
      "verified": true,
      "version": "1.3.0.17",
      "versions": [
        {
          "@id": "https://gitlab.example.com/api/v4/projects/1/packages/nuget/metadata/MyNuGetPkg/1.3.0.17.json",
          "version": "1.3.0.17",
          "downloads": 0
        }
      ],
      "tags": ""
    }
  ]
}
```

