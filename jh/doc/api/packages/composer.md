---
stage: Package
group: Package Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Composer API **(FREE)**

本文是 Composer Package<!--[Composer Package](../../user/packages/composer_repository/index.md)--> 的 API 文档。

WARNING:
[Composer 包管理器客户端](https://getcomposer.org/)使用此 API 并且它通常不适合手动使用。

<!--For instructions on how to upload and install Composer packages from the GitLab
package registry, see the [Composer package registry documentation](../../user/packages/composer_repository/index.md).-->

NOTE:
这些端点不遵守标准的 API 身份验证方法。
<!--See the [Composer package registry documentation](../../user/packages/composer_repository/index.md)
for details on which headers and token types are supported. Undocumented authentication methods might be removed in the future.-->

<a id="base-repository-request"></a>

## 基础仓库请求

返回用于请求单个包的仓库 URL 模板：

```plaintext
GET group/:id/-/packages/composer/packages
```

| 参数   | 类型     | 是否必需 | 描述           |
|------|--------|------|--------------|
| `id` | string | yes  | 群组的 ID 或完整路径 |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/group/1/-/packages/composer/packages"
```

响应示例：

```json
{
  "packages": [],
  "metadata-url": "/api/v4/group/1/-/packages/composer/p2/%package%.json",
  "provider-includes": {
    "p/%hash%.json": {
      "sha256": "082df4a5035f8725a12i4a3d2da5e6aaa966d06843d0a5c6d499313810427bd6"
    }
  },
  "providers-url": "/api/v4/group/1/-/packages/composer/%package%$%hash%.json"
}
```

Composer V1 和 V2 使用此端点。要查看 V2 特定的响应，请包含 Composer `User-Agent` Header。建议使用 Composer V2 而不是 V1。

```shell
curl --user <username>:<personal_access_token> \
     --header "User-Agent: Composer/2" \
     "https://gitlab.example.com/api/v4/group/1/-/packages/composer/packages"
```

响应示例：

```json
{
  "packages": [],
  "metadata-url": "/api/v4/group/1/-/packages/composer/p2/%package%.json"
}
```

<a id="v1-packages-list"></a>

## V1 包列表

给定 V1 提供者 SHA，返回仓库中的包列表。建议使用 Composer V2 而不是 V1。

```plaintext
GET group/:id/-/packages/composer/p/:sha
```

| 参数    | 类型     | 是否必需 | 描述                                                    |
|-------|--------|------|-------------------------------------------------------|
| `id`  | string | yes  | 群组的 ID 或完整路径                                          |
| `sha` | string | yes  | 提供者 SHA，由 Composer [基本请求](#base-repository-request)提供 |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/group/1/-/packages/composer/p/082df4a5035f8725a12i4a3d2da5e6aaa966d06843d0a5c6d499313810427bd6"
```

响应示例：

```json
{
  "providers": {
    "my-org/my-composer-package": {
      "sha256": "5c873497cdaa82eda35af5de24b789be92dfb6510baf117c42f03899c166b6e7"
    }
  }
}
```

<a id="v1-package-metadata"></a>

## V1 包元数据

返回给定包的版本和元数据列表。建议使用 Composer V2 而不是 V1。

```plaintext
GET group/:id/-/packages/composer/:package_name$:sha
```

请注意 URL 中的 `$` 符号。提出请求时，您可能需要符号 `%24` 的 URL 编码的版本。请参考表后面的例子：


| 参数             | 类型     | 是否必需 | 描述                                                                                   |
|----------------|--------|------|--------------------------------------------------------------------------------------|
| `id`           | string | yes  | 群组的 ID 或完整路径                                                                         |
| `package_name` | string | yes  | 包名称                                                                                  |
| `sha`          | string | yes  | 包的 SHA 摘要，由 [V1 包列表](#v1-packages-list)提供|

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/group/1/-/packages/composer/my-org/my-composer-package%245c873497cdaa82eda35af5de24b789be92dfb6510baf117c42f03899c166b6e7"
```

响应示例：

```json
{
  "packages": {
    "my-org/my-composer-package": {
      "1.0.0": {
        "name": "my-org/my-composer-package",
        "type": "library",
        "license": "GPL-3.0-only",
        "version": "1.0.0",
        "dist": {
          "type": "zip",
          "url": "https://gitlab.example.com/api/v4/projects/1/packages/composer/archives/my-org/my-composer-package.zip?sha=673594f85a55fe3c0eb45df7bd2fa9d95a1601ab",
          "reference": "673594f85a55fe3c0eb45df7bd2fa9d95a1601ab",
          "shasum": ""
        },
        "source": {
          "type": "git",
          "url": "https://gitlab.example.com/my-org/my-composer-package.git",
          "reference": "673594f85a55fe3c0eb45df7bd2fa9d95a1601ab"
        },
        "uid": 1234567
      },
      "2.0.0": {
        "name": "my-org/my-composer-package",
        "type": "library",
        "license": "GPL-3.0-only",
        "version": "2.0.0",
        "dist": {
          "type": "zip",
          "url": "https://gitlab.example.com/api/v4/projects/1/packages/composer/archives/my-org/my-composer-package.zip?sha=445394f85a55fe3c0eb45df7bd2fa9d95a1601ab",
          "reference": "445394f85a55fe3c0eb45df7bd2fa9d95a1601ab",
          "shasum": ""
        },
        "source": {
          "type": "git",
          "url": "https://gitlab.example.com/my-org/my-composer-package.git",
          "reference": "445394f85a55fe3c0eb45df7bd2fa9d95a1601ab"
        },
        "uid": 1234567
      }
    }
  }
}
```

<a id="v2-package-metadata"></a>

## V2 包元数据

返回给定包的版本和元数据列表：

```plaintext
GET group/:id/-/packages/composer/p2/:package_name
```

| 参数             | 类型     | 是否必需 | 描述           |
|----------------|--------|------|--------------|
| `id`           | string | yes  | 群组的 ID 或完整路径 |
| `package_name` | string | yes  | 包名称          |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/group/1/-/packages/composer/p2/my-org/my-composer-package"
```

响应示例：

```json
{
  "packages": {
    "my-org/my-composer-package": {
      "1.0.0": {
        "name": "my-org/my-composer-package",
        "type": "library",
        "license": "GPL-3.0-only",
        "version": "1.0.0",
        "dist": {
          "type": "zip",
          "url": "https://gitlab.example.com/api/v4/projects/1/packages/composer/archives/my-org/my-composer-package.zip?sha=673594f85a55fe3c0eb45df7bd2fa9d95a1601ab",
          "reference": "673594f85a55fe3c0eb45df7bd2fa9d95a1601ab",
          "shasum": ""
        },
        "source": {
          "type": "git",
          "url": "https://gitlab.example.com/my-org/my-composer-package.git",
          "reference": "673594f85a55fe3c0eb45df7bd2fa9d95a1601ab"
        },
        "uid": 1234567
      },
      "2.0.0": {
        "name": "my-org/my-composer-package",
        "type": "library",
        "license": "GPL-3.0-only",
        "version": "2.0.0",
        "dist": {
          "type": "zip",
          "url": "https://gitlab.example.com/api/v4/projects/1/packages/composer/archives/my-org/my-composer-package.zip?sha=445394f85a55fe3c0eb45df7bd2fa9d95a1601ab",
          "reference": "445394f85a55fe3c0eb45df7bd2fa9d95a1601ab",
          "shasum": ""
        },
        "source": {
          "type": "git",
          "url": "https://gitlab.example.com/my-org/my-composer-package.git",
          "reference": "445394f85a55fe3c0eb45df7bd2fa9d95a1601ab"
        },
        "uid": 1234567
      }
    }
  }
}
```

## 创建包

从 Git 标签或分支创建 Composer 包。

```plaintext
POST projects/:id/packages/composer
```

| 参数       | 类型     | 是否必需 | 描述           |
|----------|--------|------|--------------|
| `id`     | string | yes  | 群组的 ID 或完整路径 |
| `tag`    | string | no   | 目标包的标签名称     |
| `branch` | string | no   | 目标包的分支名称     |

```shell
curl --request POST --user <username>:<personal_access_token> \
     --data tag=v1.0.0 "https://gitlab.example.com/api/v4/projects/1/packages/composer"
```

响应示例：

```json
{
  "message": "201 Created"
}
```

## 下载包归档

> 对此端点的授权引入于极狐GitLab 14.10。

下载 Composer 包。此 URL 在 [v1](#v1-package-metadata) 或 [v2 包元数据](#v2-package-metadata)响应中提供。
请求中必须包含 `.zip` 文件扩展名。

```plaintext
GET projects/:id/packages/composer/archives/:package_name
```

| 参数             | 类型     | 是否必需 | 描述                                                       |
|----------------|--------|------|----------------------------------------------------------|
| `id`           | string | yes  | 群组的 ID 或完整路径                                             |
| `package_name` | string | yes  | 包名称                                                      |
| `sha`          | string | yes  | 请求包版本的目标 SHA |

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/composer/archives/my-org/my-composer-package.zip?sha=673594f85a55fe3c0eb45df7bd2fa9d95a1601ab"
```

将输出写入到文件中：

```shell
curl --user <username>:<personal_access_token> "https://gitlab.example.com/api/v4/projects/1/packages/composer/archives/my-org/my-composer-package.zip?sha=673594f85a55fe3c0eb45df7bd2fa9d95a1601ab" >> package.tar.gz
```

将下载文件写入到当前路径的 `package.tar.gz` 中。

NOTE:
在 14.10 及更高版本中，此端点需要授权。在 14.9 及更早的版本中，可无限制访问。
