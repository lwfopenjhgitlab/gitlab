---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
type: reference, api
---

# 仓库 API **(FREE)**

## 列出仓库树

> 迭代带有数字 (`?page=2`) 的结果页面废弃于极狐GitLab 14.3。

获取项目中仓库文件和目录的列表。如果仓库可以公开访问，此端点无需身份验证即可访问。

此命令提供与 `git ls-tree` 命令基本相同的功能。详情请参见 Git 内部文档中的[树对象](https://git-scm.com/book/en/v2/Git-Internals-Git-Objects/#_tree_objects)。

WARNING:
此端点在 15.0 中变更为[基于键集的分页](index.md#keyset-based-pagination)，迭代带有数字 (`?page=2`) 的结果页面已废弃。

```plaintext
GET /projects/:id/repository/tree
```

支持的参数：

| 参数           | 类型                | 是否必需 | 描述                                                            |
|:-------------|:------------------|:-----|:--------------------------------------------------------------|
| `id`         | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `path`       | string            | no   | 仓库内的路径。用于获取子目录的内容                                             |
| `ref`        | string            | no   | 仓库分支名称或标签名称。如未给定，则为默认分支名称                                     |
| `recursive`  | boolean           | no   | 用于获取递归树的布尔值（默认为 False）                                        |
| `per_page`   | integer           | no   | 每页展示的结果数量。如未指定，默认为 `20`。[了解更多分页的内容](index.md#pagination)      |
| `pagination` | string            | no   | 如果设置为 `keyset`，则使用[键集分页方法](index.md#keyset-based-pagination)  |
| `page_token` | string            | no   | 获取下一页的树记录 ID。仅与键集分页一起使用                                       |

```json
[
  {
    "id": "a1e8f8d745cc87e3a9248358d9352bb7f9a0aeba",
    "name": "html",
    "type": "tree",
    "path": "files/html",
    "mode": "040000"
  },
  {
    "id": "4535904260b1082e14f867f7a24fd8c21495bde3",
    "name": "images",
    "type": "tree",
    "path": "files/images",
    "mode": "040000"
  },
  {
    "id": "31405c5ddef582c5a9b7a85230413ff90e2fe720",
    "name": "js",
    "type": "tree",
    "path": "files/js",
    "mode": "040000"
  },
  {
    "id": "cc71111cfad871212dc99572599a568bfe1e7e00",
    "name": "lfs",
    "type": "tree",
    "path": "files/lfs",
    "mode": "040000"
  },
  {
    "id": "fd581c619bf59cfdfa9c8282377bb09c2f897520",
    "name": "markdown",
    "type": "tree",
    "path": "files/markdown",
    "mode": "040000"
  },
  {
    "id": "23ea4d11a4bdd960ee5320c5cb65b5b3fdbc60db",
    "name": "ruby",
    "type": "tree",
    "path": "files/ruby",
    "mode": "040000"
  },
  {
    "id": "7d70e02340bac451f281cecf0a980907974bd8be",
    "name": "whitespace",
    "type": "blob",
    "path": "files/whitespace",
    "mode": "100644"
  }
]
```

<a id="get-a-blob-from-repository">

## 从仓库获取 Blob 

允许您接收仓库中 Blob 的信息，例如大小和内容。 Blob 内容采用 Base64 编码。如果仓库可公开访问，则无需身份验证就可以访问此端点。

```plaintext
GET /projects/:id/repository/blobs/:sha
```

支持的参数：

| 参数    | 类型                | 是否必需 | 描述                                                            |
|:------|:------------------|:-----|:--------------------------------------------------------------|
| `id`  | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `sha` | string            | yes  | Blob SHA                                                      |

## 原始 Blob 内容

通过 Blob SHA 获取 Blob 的原始文件内容。如果仓库可公开访问，则无需身份验证就可以访问此端点。

```plaintext
GET /projects/:id/repository/blobs/:sha/raw
```

支持的参数：

| 参数    | 类型                | 是否必需 | 描述                                                            |
|:------|:------------------|:-----|:--------------------------------------------------------------|
| `id`  | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `sha` | string            | yes  | Blob SHA                                                 |

## 获取文件归档

> - 对[包括 Git LFS Blob](../topics/git/lfs/index.md#lfs-objects-in-project-archives) 的支持引入于极狐GitLab 13.5。
> - 对下载子文件夹的支持引入于极狐GitLab 14.4。

获取仓库的存档。如果仓库可公开访问，则无需进行身份验证就可以访问此端点。

对于 JiHuLab.com 用户，此端点的速率限制阈值为每分钟 5 个请求。

```plaintext
GET /projects/:id/repository/archive[.format]
```

`format` 是归档格式的可选后缀。默认为 `tar.gz`。例如，指定 `archive.zip` 将发送 ZIP 格式的归档。
可用选项为：

- `bz2`
- `tar`
- `tar.bz2`
- `tar.gz`
- `tb2`
- `tbz`
- `tbz2`
- `zip`

支持的参数：

| 参数     | 类型                | 是否必需 | 描述                                                            |
|:-------|:------------------|:-----|:--------------------------------------------------------------|
| `id`   | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `sha`  | string            | no   | 要下载的提交 SHA。可以使用标签、分支引用或 SHA。如果未指定，则默认为默认分支的尖端                 |
| `path` | string            | no   | 要下载的仓库的子路径。如果字符串为空，则默认为整个仓库                             |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.com/api/v4/projects/<project_id>/repository/archive?sha=<commit_sha>&path=<path>"
```

## 比较分支、标签或提交

如果仓库可公开访问，则无需身份验证就可以访问此端点。
<!--如果达到 [Diff限制](../development/diffs.md#diff-limits)，则 Diffs 可以有一个空的 diff 字符串。-->

```plaintext
GET /projects/:id/repository/compare
```

支持的参数：

| 参数                | 类型                | 是否必需 | 描述                                                                                                                                                                                                                                                        |
|:------------------|:------------------|:-----|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`              | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding)                                                                                                                                                                                             |
| `from`            | string            | yes  | 提交 SHA 或分支名称                                                                                                                                                                                                                                              |
| `to`              | string            | yes  | 提交 SHA 或分支名称                                                                                                                                                                                                                                              |
| `from_project_id` | integer           | no   | 进行比较的 ID                                                                                                                                                                                                                                                  |
| `straight`        | boolean           | no   | 比较方法，`true` 用于直接比较 `from` 和 `to` (`from`..`to`)，`false` 使用合并基进行比较 (`from`...`to`)'。默认为 `false` |

```plaintext
GET /projects/:id/repository/compare?from=master&to=feature
```

响应示例：

```json
{
  "commit": {
    "id": "12d65c8dd2b2676fa3ac47d955accc085a37a9c1",
    "short_id": "12d65c8dd2b",
    "title": "JS fix",
    "author_name": "Example User",
    "author_email": "user@example.com",
    "created_at": "2014-02-27T10:27:00+02:00"
  },
  "commits": [{
    "id": "12d65c8dd2b2676fa3ac47d955accc085a37a9c1",
    "short_id": "12d65c8dd2b",
    "title": "JS fix",
    "author_name": "Example User",
    "author_email": "user@example.com",
    "created_at": "2014-02-27T10:27:00+02:00"
  }],
  "diffs": [{
    "old_path": "files/js/application.js",
    "new_path": "files/js/application.js",
    "a_mode": null,
    "b_mode": "100644",
    "diff": "--- a/files/js/application.js\n+++ b/files/js/application.js\n@@ -24,8 +24,10 @@\n //= require g.raphael-min\n //= require g.bar-min\n //= require branch-graph\n-//= require highlightjs.min\n-//= require ace/ace\n //= require_tree .\n //= require d3\n //= require underscore\n+\n+function fix() { \n+  alert(\"Fixed\")\n+}",
    "new_file": false,
    "renamed_file": false,
    "deleted_file": false
  }],
  "compare_timeout": false,
  "compare_same_ref": false,
  "web_url": "https://gitlab.example.com/thedude/gitlab-foss/-/compare/ae73cb07c9eeaf35924a10f713b364d32b2dd34f...0b4bc9a49b562e85de7cc9e834518ea6828729b9"
}
```

## 贡献者

> - 参数 `additions` 和 `deletions` 废弃于极狐GitLab 13.4，因为它们一直返回 `0`。
> - 参数 `additions` 和 `deletions` 移除于极狐GitLab 14.0。

获取仓库贡献者列表。如果仓库可公开访问，则无需进行身份验证就可以访问此端点。

```plaintext
GET /projects/:id/repository/contributors
```

支持的参数：

| 参数         | 类型                | 是否必需 | 描述                                                               |
|:-----------|:------------------|:-----|:-----------------------------------------------------------------|
| `id`       | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding)    |
| `order_by` | string            | no   | 返回按照 `name`、`email` 或 `commits`（按提交日期排序）字段进行排序的贡献者。默认为 `commits` |
| `sort`     | string            | no   | 返回按照 `asc` 或 `desc` 顺序排序的贡献者。默认为 `asc`                           |

响应示例：

```json
[{
  "name": "Example User",
  "email": "example@example.com",
  "commits": 117,
  "additions": 0,
  "deletions": 0
}, {
  "name": "Sample User",
  "email": "sample@example.com",
  "commits": 33,
  "additions": 0,
  "deletions": 0
}]
```

## 合并基

获取 2 个或更多 refs 的共同祖先，例如提交 SHA、分支名称或标签。

```plaintext
GET /projects/:id/repository/merge_base
```

| 参数         | 类型                | 是否必需 | 描述                                                     |
|:-----------|:------------------|:-----|:-------------------------------------------------------|
| `id`       | integer or string | yes  | 项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `refs`| array            | yes    | 找到共同祖先的 refs，接受多个 refs                                 |

以下为请求示例，为了便于阅读，引用已被截断：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
  "https://gitlab.example.com/api/v4/projects/5/repository/merge_base?refs[]=304d257d&refs[]=0031876f"
```

响应示例：

```json
{
  "id": "1a0b36b3cdad1d2ee32457c102a8c0b7056fa863",
  "short_id": "1a0b36b3",
  "title": "Initial commit",
  "created_at": "2014-02-27T08:03:18.000Z",
  "parent_ids": [],
  "message": "Initial commit\n",
  "author_name": "Example User",
  "author_email": "user@example.com",
  "authored_date": "2014-02-27T08:03:18.000Z",
  "committer_name": "Example User",
  "committer_email": "user@example.com",
  "committed_date": "2014-02-27T08:03:18.000Z"
}
```

## 向更新日志文件添加更新日志数据

> - 引入于极狐GitLab 13.9。
> - 提交范围限制引入于极狐GitLab 15.1，[功能标记](../administration/feature_flags.md)为 `changelog_commits_limitation`。默认启用。

根据仓库中的提交生成更新日志数据。

给定一个[语义版本](https://semver.org/)和一系列提交，极狐GitLab 为所有使用特定的 [Git Trailer](https://git-scm.com/docs/git-interpret-trailers) 的提交生成更新日志。

极狐GitLab 向项目的 Git 仓库中的更新日志文件添加一个新的 Markdown 格式的内容，且输出格式可以定制。

```plaintext
POST /projects/:id/repository/changelog
```

### 支持的参数

更新日志支持以下参数：

| 参数            | 类型       | 是否必需 | 描述                                                                                                               |
|:--------------|:---------|:-----|:-----------------------------------------------------------------------------------------------------------------|
| `version`     | string   | yes  | 为其生成更新日志的版本。格式必需遵循[语义版本控制](https://semver.org/)                                                                  |
| `from`        | string   | no   | 标记更新日志中提交范围开始的提交 SHA。此提交本身不包含在列表中                                                                                |
| `to`          | string   | no   | 标记更新日志中提交范围结束的提交 SHA。此提交*包含*在更新日志中。默认为 `branch` 参数中指定的分支。最高为  15000 个提交，除非禁用 `changelog_commits_limitation` 功能标志 |
| `date`        | datetime | no   | 发布的日期和时间，默认为当前时间                                                                                                 |
| `branch`      | string   | no   | 提交更新日志更改的分支，默认为项目的默认分支                                                                                           |
| `trailer`     | string   | no   | 用于包含提交的 Git Trailer，默认为 `Changelog`。区分大小写，`Example` 不匹配 `example` 或 `eXaMpLE`                                    |
| `config_file` | string   | no   | 项目 Git 仓库中更新日志配置文件的路径。默认为 `.gitlab/changelog_config.yml`                                                         |
| `file`        | string   | no   | 更改提交到的文件，默认为 `CHANGELOG.md`                                                                                      |
| `message`     | string   | no   | 提交更改时使用的提交信息，默认为 `Add changelog for version X`，其中 X 是 `version` 参数的值                                             |

### `from` 参数要求

如果未指定 `from` 参数，极狐GitLab 使用 `version` 参数中指定的版本之前的最后一个稳定版本的 Git 标签。
极狐GitLab 从标签名称中提取版本号，Git 标签名称必须遵循特定的格式。默认情况下，极狐GitLab 考虑使用这些格式的标签：

- `vX.Y.Z`
- `X.Y.Z`

其中 `X.Y.Z` 是[语义版本控制](https://semver.org/)之后的版本。例如，有一个项目的标签如下：

- v1.0.0-pre1
- v1.0.0
- v1.1.0
- v2.0.0

如果 `version` 参数是 `2.1.0`，极狐GitLab 使用标签 `v2.0.0`。而当版本是 `1.1.1` 或 `1.2.0` 时，极狐GitLab 使用标签 v1.1.0。从未使用过标签 `v1.0.0-pre1`，因为预发布标签被忽略。

如果未指定 `from` 且未找到要使用的标签，则 API 会产生错误。
要解决此类错误，您必须明确指定 `from` 参数的值。

### 示例

这些示例使用 [cURL](https://curl.se/) 进行 HTTP 请求。
示例命令使用以下值：

- **项目 ID**：42
- **位置**：托管在 JiHuLab.com 上
- **示例 API 令牌**：`token`

此命令为版本 `1.0.0` 生成更新日志。

提交范围：

- 从上一次发布的标签开始。
- 以目标分支上的最后一次提交结束。默认目标分支是项目的默认分支。

如果最后一个标签是 `v0.9.0` 并且默认分支是 `main`，则此示例中包含的提交范围是 `v0.9.0..main`：

```shell
curl --request POST --header "PRIVATE-TOKEN: token" \
  --data "version=1.0.0" "https://gitlab.com/api/v4/projects/42/repository/changelog"
```

要在不同的分支上生成数据，请指定 `branch` 参数。这个命令从 `foo` 分支生成数据：

```shell
curl --request POST --header "PRIVATE-TOKEN: token" \
  --data "version=1.0.0&branch=foo" "https://gitlab.com/api/v4/projects/42/repository/changelog"
```

要使用其他 Trailer，请使用 `trailer` 参数：

```shell
curl --request POST --header "PRIVATE-TOKEN: token" \
  --data "version=1.0.0&trailer=Type" "https://gitlab.com/api/v4/projects/42/repository/changelog"
```

要在其他文件上存储结果，请使用 `file` 参数：

```shell
curl --request POST --header "PRIVATE-TOKEN: token" \
  --data "version=1.0.0&file=NEWS" "https://gitlab.com/api/v4/projects/42/repository/changelog"
```

## 生成更新日志数据

> 引入于极狐GitLab 14.6。

根据仓库中的提交生成更新日志数据，无需将它们提交到更新日志文件中。

与 `POST /projects/:id/repository/changelog` 的工作方式完全一样，除了更新日志数据不专属于任何更新日志文件。

```plaintext
GET /projects/:id/repository/changelog
```

支持的参数：

| 参数            | 类型       | 是否必需 | 描述                                                        |
|:--------------|:---------|:-----|:----------------------------------------------------------|
| `version`     | string   | yes  | 为其生成更新日志的版本。格式必需遵循[语义版本控制](https://semver.org/)           |
| `from`        | string   | no   | 用于生成更新日志的提交（作为 SHA）范围的开始。此提交本身不包含在列表中                     |
| `to`          | string   | no   | 用于更新日志的提交（作为 SHA）范围的结束。此提交*包含*在列表中。默认为 `branch` 参数中指定的分支  |
| `date`        | datetime | no   | 发布的日期和时间，格式为 ISO 8601。例如：`2016-03-11T03:45:40Z`。默认为当前时间   |
| `trailer`     | string   | no   | 用于包括提交的 Git Trailer，默认为 `Changelog`                       |
| `config_file` | string   | no   | 项目 Git 仓库中的更新日志配置文件的路径。默认为 `.gitlab/changelog_config.yml` |

```shell
curl --header "PRIVATE-TOKEN: token" \
  "https://gitlab.com/api/v4/projects/42/repository/changelog?version=1.0.0"
```

响应示例，添加了换行符以提高可读性：

```json
{
  "notes": "## 1.0.0 (2021-11-17)\n\n### feature (2 changes)\n\n-
[Title 2](namespace13/project13@ad608eb642124f5b3944ac0ac772fecaf570a6bf)
([merge request](namespace13/project13!2))\n-
[Title 1](namespace13/project13@3c6b80ff7034fa0d585314e1571cc780596ce3c8)
([merge request](namespace13/project13!1))\n"
}
```

## 相关主题

- [更新日志](../user/project/changelogs.md)
- [更新日志条目](../development/changelog.md)

