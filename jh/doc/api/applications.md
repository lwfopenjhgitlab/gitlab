---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 应用程序 API **(FREE)**

应用程序 API 在实例范围的 OAuth 应用程序上运行，用于：

- [使用极狐GitLab 作为身份验证的提供方](../integration/oauth_provider.md)。
- [允许用户访问极狐GitLab 资源](oauth2.md)。

应用程序 API 不能用于管理组应用程序或单个用户的应用程序。

NOTE:
只有管理员用户可以使用应用程序 API。

## 创建应用程序

通过发送 JSON 来创建应用程序。

如果请求成功，则返回 `200`。

```plaintext
POST /applications
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
|:---------------|:--------|:---------|:---------------------------------|
| `name`         | string  | yes      | 应用程序的名称。         |
| `redirect_uri` | string  | yes      | 应用程序的重定向 URI。 |
| `scopes`       | string  | yes      | 应用程序的作用域。您可以通过使用空格分隔每个作用域来指定多个作用域。 |
| `confidential` | boolean | no       | 该应用程序用于客户端 secret 可以被保密存放的地方。移动应用程序和网页应用程序被视为非机密。如果未提供，则默认为 `true`。 |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --data "name=MyApplication&redirect_uri=http://redirect.uri&scopes=api read_user email" \
     "https://gitlab.example.com/api/v4/applications"
```

响应示例：

```json
{
    "id":1,
    "application_id": "5832fc6e14300a0d962240a8144466eef4ee93ef0d218477e55f11cf12fc3737",
    "application_name": "MyApplication",
    "secret": "ee1dd64b6adc89cf7e2c23099301ccc2c61b441064e9324d963c46902a85ec34",
    "callback_url": "http://redirect.uri",
    "confidential": true
}
```

## 列出所有应用程序

列出所有已注册的应用程序。

```plaintext
GET /applications
```

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/applications"
```

响应示例：

```json
[
    {
        "id":1,
        "application_id": "5832fc6e14300a0d962240a8144466eef4ee93ef0d218477e55f11cf12fc3737",
        "application_name": "MyApplication",
        "callback_url": "http://redirect.uri",
        "confidential": true
    }
]
```

NOTE:
此 API 不公开 `secret` 值。

## 删除应用程序

删除特定应用程序。

如果请求成功，则返回 `204`。

```plaintext
DELETE /applications/:id
```

参数：

| 参数 | 类型 | 是否必需 | 描述 |
|:----------|:--------|:---------|:----------------------------------------------------|
| `id`      | integer | yes      | 应用程序的 ID（不是 `application_id`）。 |


请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/applications/:id"
```
