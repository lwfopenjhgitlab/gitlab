---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 头像 API **(FREE)**

> 引入于 11.0 版本。

<a href="#get-a-single-avatar-url"></a>

## 获取一个单独的头像的 URL

通过用户的邮箱地址来获取单个[头像](../user/profile/index.md#access-your-user-settings)的资源地址。

假设:

- 提供的公共邮箱地址没有找到关联的用户，则会返回外部的头像服务的结果。
- 对外的可见性是被限制的，那么在没有授权的情况下请求会返回 `403 Forbidden`。

NOTE:
这个 API 可以在没有授权的情况下访问。

```plaintext
GET /avatar?email=admin@example.com
```

参数:

| 参数    | 类型    | 是否必需 | 描述                                                                                              |
| :------ | :------ | :------- | :------------------------------------------------------------------------------------------------ |
| `email` | string  | yes      | 该用户的公共邮箱地址。                                                                            |
| `size`  | integer | no       | 单一的像素尺寸（因为图像是一个正方形)。仅用于在 Gravatar 或配置好的 Libravatar 服务器中查询头像。 |

请求示例：

```shell
curl "https://gitlab.example.com/api/v4/avatar?email=admin@example.com&size=32"
```

响应示例：

```json
{
  "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=64&d=identicon"
}
```
