---
stage: Data Stores
group: Global Search
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 搜索 API **(FREE)**

> 功能标志 `search_filter_by_confidential` 移除于极狐GitLab 13.6。

搜索的每个 API 调用都必须经过授权。

<a id="additional-scopes"></a>

## 其他范围 **(PREMIUM)**

如果[启用了 Elasticsearch](../integration/advanced_search/elasticsearch.md)，[高级搜索 API](#advanced-search-api) 和[群组搜索 API](#group-search-api) 可以使用其他范围：
`blobs`、`commits`、`notes` 和 `wiki_blobs`。

<a id="advanced-search-api"></a>

## 高级搜索 API

在指定范围内跨极狐GitLab 实例全局搜索表达式。
响应取决于请求的范围。

```plaintext
GET /search
```

| 参数             | 类型      | 是否必需 | 描述                                                                                                                                                        |
|----------------|---------|------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| `scope`        | string  | yes  | 搜索范围。值包括 `projects`、`issues`、`merge_requests`、`milestones`、`snippet_titles` 和 `users`。[其他范围](#additional-scopes)：`blobs`、`commits`、`notes` 和 `wiki_blobs` |
| `search`       | string  | yes  | 搜索查询                                                                                                                                                      |
| `state`        | string  | no   | 按状态过滤。支持 `issues` 和 `merge_requests`；忽略其他范围                                                                                                               |
| `confidential` | boolean | no   | 按机密性过滤。支持 `issues` 范围；忽略其他范围                                                                                                                              |
| `order_by`     | string  | no   | 允许的值仅为 `created_at`。如未设置，结果或按照 `created_at` 的降序排序进行基本搜索，或按最相关的文档排序（对于高级搜索）                                                                                |
| `sort`         | string  | no   | 允许的值仅为 `asc` 或 `desc`。如未设置，结果或按照 `created_at` 的降序排序进行基本搜索，或按最相关的文档排序（对于高级搜索）                                                                              |


### 范围： `projects`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/search?scope=projects&search=flight"
```

响应示例：

```json
[
  {
    "id": 6,
    "description": "Nobis sed ipsam vero quod cupiditate veritatis hic.",
    "name": "Flight",
    "name_with_namespace": "Twitter / Flight",
    "path": "flight",
    "path_with_namespace": "twitter/flight",
    "created_at": "2017-09-05T07:58:01.621Z",
    "default_branch": "master",
    "tag_list":[], //deprecated, use `topics` instead
    "topics":[],
    "ssh_url_to_repo": "ssh://jarka@localhost:2222/twitter/flight.git",
    "http_url_to_repo": "http://localhost:3000/twitter/flight.git",
    "web_url": "http://localhost:3000/twitter/flight",
    "readme_url": "http://localhost:3000/twitter/flight/-/blob/master/README.md",
    "avatar_url": null,
    "star_count": 0,
    "forks_count": 0,
    "last_activity_at": "2018-01-31T09:56:30.902Z"
  }
]
```

### 范围：`issues`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/search?scope=issues&search=file"
```

响应示例：

```json
[
  {
    "id": 83,
    "iid": 1,
    "project_id": 12,
    "title": "Add file",
    "description": "Add first file",
    "state": "opened",
    "created_at": "2018-01-24T06:02:15.514Z",
    "updated_at": "2018-02-06T12:36:23.263Z",
    "closed_at": null,
    "labels":[],
    "milestone": null,
    "assignees": [{
      "id": 20,
      "name": "Ceola Deckow",
      "username": "sammy.collier",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/c23d85a4f50e0ea76ab739156c639231?s=80&d=identicon",
      "web_url": "http://localhost:3000/sammy.collier"
    }],
    "author": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://localhost:3000/root"
    },
    "assignee": {
      "id": 20,
      "name": "Ceola Deckow",
      "username": "sammy.collier",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/c23d85a4f50e0ea76ab739156c639231?s=80&d=identicon",
      "web_url": "http://localhost:3000/sammy.collier"
    },
    "user_notes_count": 0,
    "upvotes": 0,
    "downvotes": 0,
    "due_date": null,
    "confidential": false,
    "discussion_locked": null,
    "web_url": "http://localhost:3000/h5bp/7bp/subgroup-prj/issues/1",
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    }
  }
]
```

NOTE:
`assignee` 列已废弃，其显示为单一大小的阵列 `assignees` 以符合极狐GitLab 付费版的 API。

### 范围：`merge_requests`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/search?scope=merge_requests&search=file"
```

响应示例：

```json
[
  {
    "id": 56,
    "iid": 8,
    "project_id": 6,
    "title": "Add first file",
    "description": "This is a test MR to add file",
    "state": "opened",
    "created_at": "2018-01-22T14:21:50.830Z",
    "updated_at": "2018-02-06T12:40:33.295Z",
    "target_branch": "master",
    "source_branch": "jaja-test",
    "upvotes": 0,
    "downvotes": 0,
    "author": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://localhost:3000/root"
    },
    "assignee": {
      "id": 5,
      "name": "Jacquelyn Kutch",
      "username": "abigail",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/3138c66095ee4bd11a508c2f7f7772da?s=80&d=identicon",
      "web_url": "http://localhost:3000/abigail"
    },
    "source_project_id": 6,
    "target_project_id": 6,
    "labels": [
      "ruby",
      "tests"
    ],
    "draft": false,
    "work_in_progress": false,
    "milestone": {
      "id": 13,
      "iid": 3,
      "project_id": 6,
      "title": "v2.0",
      "description": "Qui aut qui eos dolor beatae itaque tempore molestiae.",
      "state": "active",
      "created_at": "2017-09-05T07:58:29.099Z",
      "updated_at": "2017-09-05T07:58:29.099Z",
      "due_date": null,
      "start_date": null
    },
    "merge_when_pipeline_succeeds": false,
    "merge_status": "can_be_merged",
    "sha": "78765a2d5e0a43585945c58e61ba2f822e4d090b",
    "merge_commit_sha": null,
    "squash_commit_sha": null,
    "user_notes_count": 0,
    "discussion_locked": null,
    "should_remove_source_branch": null,
    "force_remove_source_branch": true,
    "web_url": "http://localhost:3000/twitter/flight/merge_requests/8",
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    }
  }
]
```

### 范围：`milestones`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/search?scope=milestones&search=release"
```

响应示例：

```json
[
  {
    "id": 44,
    "iid": 1,
    "project_id": 12,
    "title": "next release",
    "description": "Next release milestone",
    "state": "active",
    "created_at": "2018-02-06T12:43:39.271Z",
    "updated_at": "2018-02-06T12:44:01.298Z",
    "due_date": "2018-04-18",
    "start_date": "2018-02-04"
  }
]
```

### 范围：`snippet_titles`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/search?scope=snippet_titles&search=sample"
```

响应示例：

```json
[
  {
    "id": 50,
    "title": "Sample file",
    "file_name": "file.rb",
    "description": "Simple ruby file",
    "author": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://localhost:3000/root"
    },
    "updated_at": "2018-02-06T12:49:29.104Z",
    "created_at": "2017-11-28T08:20:18.071Z",
    "project_id": 9,
    "web_url": "http://localhost:3000/root/jira-test/snippets/50"
  }
]
```

### 范围：`wiki_blobs` **(PREMIUM)**

> 移动到专业版于 13.9。

仅当启用 Elasticsearch 时，此范围才可用。<!--This scope is available only if [Elasticsearch](../integration/advanced_search/elasticsearch.md) is enabled.-->

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/search?scope=wiki_blobs&search=bye"
```

响应示例：

```json

[
  {
    "basename": "home",
    "data": "hello\n\nand bye\n\nend",
    "path": "home.md",
    "filename": "home.md",
    "id": null,
    "ref": "master",
    "startline": 5,
    "project_id": 6
  }
]
```

NOTE:
`filename` 已废弃，转用 `path` 代替。两者都返回仓库内文件的完整路径，但是未来 `filename` 仅仅是文件名而不是完整路径。

### 范围：提交 **(PREMIUM)**

> 移动到专业版于 13.9。

仅当启用 Elasticsearch 时，此范围才可用。<!--This scope is available only if [Elasticsearch](../integration/advanced_search/elasticsearch.md) is enabled.-->

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/search?scope=commits&search=bye"
```

响应示例：

```json

[
  {
  "id": "4109c2d872d5fdb1ed057400d103766aaea97f98",
  "short_id": "4109c2d8",
  "title": "goodbye $.browser",
  "created_at": "2013-02-18T22:02:54.000Z",
  "parent_ids": [
    "59d05353ab575bcc2aa958fe1782e93297de64c9"
  ],
  "message": "goodbye $.browser\n",
  "author_name": "angus croll",
  "author_email": "anguscroll@gmail.com",
  "authored_date": "2013-02-18T22:02:54.000Z",
  "committer_name": "angus croll",
  "committer_email": "anguscroll@gmail.com",
  "committed_date": "2013-02-18T22:02:54.000Z",
  "project_id": 6
  }
]
```

### 范围：blobs **(PREMIUM)**

> 移动到专业版于 13.9。

仅当启用 Elasticsearch 时，此范围才可用。<!--This scope is available only if [Elasticsearch](../integration/advanced_search/elasticsearch.md) is enabled.-->

此范围中可用的过滤器为：

- 文件名
- 路径
- 扩展

要使用过滤器，请将其包含在您的查询中。例如：`a query filename:some_name*`。

您可以使用通配符 (`*`) 进行全局匹配。

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/search?scope=blobs&search=installation"
```

响应示例：

```json

[
  {
    "basename": "README",
    "data": "```\n\n## Installation\n\nQuick start using the [pre-built",
    "path": "README.md",
    "filename": "README.md",
    "id": null,
    "ref": "master",
    "startline": 46,
    "project_id": 6
  }
]
```

NOTE:
`filename` 已废弃，转用 `path` 代替。两者都返回仓库内文件的完整路径，但是未来 `filename` 仅仅是文件名而不是完整路径。

### 范围：备注 **(PREMIUM)**

> 移动到专业版于 13.9。

仅当启用 Elasticsearch 时，此范围才可用。<!--This scope is available only if [Elasticsearch](../integration/advanced_search/elasticsearch.md) is enabled.-->

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/6/search?scope=notes&search=maxime"
```

响应示例：

```json
[
  {
    "id": 191,
    "body": "Harum maxime consequuntur et et deleniti assumenda facilis.",
    "attachment": null,
    "author": {
      "id": 23,
      "name": "User 1",
      "username": "user1",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/111d68d06e2d317b5a59c2c6c5bad808?s=80&d=identicon",
      "web_url": "http://localhost:3000/user1"
    },
    "created_at": "2017-09-05T08:01:32.068Z",
    "updated_at": "2017-09-05T08:01:32.068Z",
    "system": false,
    "noteable_id": 22,
    "noteable_type": "Issue",
    "project_id": 6,
    "noteable_iid": 2
  }
]
```

### 范围：用户

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/search?scope=users&search=doe"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "John Doe1",
    "username": "user1",
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/c922747a93b40d1ea88262bf1aebee62?s=80&d=identicon",
    "web_url": "http://localhost/user1"
  }
]
```

<a id="group-search-api"></a>

## 群组搜索 API

在特定群组中搜索表达式。

如果用户不是群组成员并且该群组是私有的，则对该群组的 `GET` 请求会导致 `404 Not Found` 状态码。

```plaintext
GET /groups/:id/search
```

| 参数             | 类型             | 是否必需 | 描述                                                                                                                                       |
|----------------|----------------|------|------------------------------------------------------------------------------------------------------------------------------------------|
| `id`           | integer/string | yes  | 经过身份验证的用户拥有的群组 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                                                                     |
| `scope`        | string         | yes  | 搜索范围。值包括 `issues`、`merge_requests`、`milestones`、`projects` 和 `users`。[其他范围](#additional-scopes)：`blobs`、`commits`、`notes` 和 `wiki_blobs` |
| `search`       | string         | yes  | 搜索查询                                                                                                                                     |
| `state`        | string         | no   | 按状态过滤。支持 `issues` 和 `merge_requests`；忽略其他范围                                                                                              |
| `confidential` | boolean        | no   | 按机密性过滤。支持 `issues` 范围；忽略其他范围                                                                                                             |
| `order_by`     | string         | no   | 允许的值仅为 `created_at`。如未设置，结果或按照 `created_at` 的降序排序进行基本搜索，或按最相关的文档排序（对于高级搜索）                                                               |
| `sort`         | string         | no   | 允许的值仅为 `asc` 或 `desc`。如未设置，结果或按照 `created_at` 的降序排序进行基本搜索，或按最相关的文档排序（对于高级搜索）                                                             |

响应取决于请求的范围。

### 范围：`projects`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/3/search?scope=projects&search=flight"
```

响应示例：


```json
[
  {
    "id": 6,
    "description": "Nobis sed ipsam vero quod cupiditate veritatis hic.",
    "name": "Flight",
    "name_with_namespace": "Twitter / Flight",
    "path": "flight",
    "path_with_namespace": "twitter/flight",
    "created_at": "2017-09-05T07:58:01.621Z",
    "default_branch": "master",
    "tag_list":[], //deprecated, use `topics` instead
    "topics":[],
    "ssh_url_to_repo": "ssh://jarka@localhost:2222/twitter/flight.git",
    "http_url_to_repo": "http://localhost:3000/twitter/flight.git",
    "web_url": "http://localhost:3000/twitter/flight",
    "readme_url": "http://localhost:3000/twitter/flight/-/blob/master/README.md",
    "avatar_url": null,
    "star_count": 0,
    "forks_count": 0,
    "last_activity_at": "2018-01-31T09:56:30.902Z"
  }
]
```

### 范围：`issues`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/3/search?scope=issues&search=file"
```

响应示例：

```json
[
  {
    "id": 83,
    "iid": 1,
    "project_id": 12,
    "title": "Add file",
    "description": "Add first file",
    "state": "opened",
    "created_at": "2018-01-24T06:02:15.514Z",
    "updated_at": "2018-02-06T12:36:23.263Z",
    "closed_at": null,
    "labels":[],
    "milestone": null,
    "assignees": [{
      "id": 20,
      "name": "Ceola Deckow",
      "username": "sammy.collier",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/c23d85a4f50e0ea76ab739156c639231?s=80&d=identicon",
      "web_url": "http://localhost:3000/sammy.collier"
    }],
    "author": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://localhost:3000/root"
    },
    "assignee": {
      "id": 20,
      "name": "Ceola Deckow",
      "username": "sammy.collier",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/c23d85a4f50e0ea76ab739156c639231?s=80&d=identicon",
      "web_url": "http://localhost:3000/sammy.collier"
    },
    "user_notes_count": 0,
    "upvotes": 0,
    "downvotes": 0,
    "due_date": null,
    "confidential": false,
    "discussion_locked": null,
    "web_url": "http://localhost:3000/h5bp/7bp/subgroup-prj/issues/1",
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    }
  }
]
```

NOTE:
`assignee` 列已废弃，现在将其展示为单一大小的阵列 `assignees` 以符合极狐GitLab 付费版的 API。

### Scope: `merge_requests`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/3/search?scope=merge_requests&search=file"
```

响应示例：

```json
[
  {
    "id": 56,
    "iid": 8,
    "project_id": 6,
    "title": "Add first file",
    "description": "This is a test MR to add file",
    "state": "opened",
    "created_at": "2018-01-22T14:21:50.830Z",
    "updated_at": "2018-02-06T12:40:33.295Z",
    "target_branch": "master",
    "source_branch": "jaja-test",
    "upvotes": 0,
    "downvotes": 0,
    "author": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://localhost:3000/root"
    },
    "assignee": {
      "id": 5,
      "name": "Jacquelyn Kutch",
      "username": "abigail",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/3138c66095ee4bd11a508c2f7f7772da?s=80&d=identicon",
      "web_url": "http://localhost:3000/abigail"
    },
    "source_project_id": 6,
    "target_project_id": 6,
    "labels": [
      "ruby",
      "tests"
    ],
    "draft": false,
    "work_in_progress": false,
    "milestone": {
      "id": 13,
      "iid": 3,
      "project_id": 6,
      "title": "v2.0",
      "description": "Qui aut qui eos dolor beatae itaque tempore molestiae.",
      "state": "active",
      "created_at": "2017-09-05T07:58:29.099Z",
      "updated_at": "2017-09-05T07:58:29.099Z",
      "due_date": null,
      "start_date": null
    },
    "merge_when_pipeline_succeeds": false,
    "merge_status": "can_be_merged",
    "sha": "78765a2d5e0a43585945c58e61ba2f822e4d090b",
    "merge_commit_sha": null,
    "squash_commit_sha": null,
    "user_notes_count": 0,
    "discussion_locked": null,
    "should_remove_source_branch": null,
    "force_remove_source_branch": true,
    "web_url": "http://localhost:3000/twitter/flight/merge_requests/8",
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    }
  }
]
```

### 范围：`milestones`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/3/search?scope=milestones&search=release"
```

响应示例：

```json
[
  {
    "id": 44,
    "iid": 1,
    "project_id": 12,
    "title": "next release",
    "description": "Next release milestone",
    "state": "active",
    "created_at": "2018-02-06T12:43:39.271Z",
    "updated_at": "2018-02-06T12:44:01.298Z",
    "due_date": "2018-04-18",
    "start_date": "2018-02-04"
  }
]
```

### 范围：`wiki_blobs` **(PREMIUM)**

> 移动到极狐GitLab 专业版于 13.9。

仅当启用 Elasticsearch 时，此范围才可用。<!--This scope is available only if [Elasticsearch](../integration/advanced_search/elasticsearch.md) is enabled.-->

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/6/search?scope=wiki_blobs&search=bye"
```

响应示例：

```json

[
  {
    "basename": "home",
    "data": "hello\n\nand bye\n\nend",
    "path": "home.md",
    "filename": "home.md",
    "id": null,
    "ref": "master",
    "startline": 5,
    "project_id": 6
  }
]
```

NOTE:
`filename` 已废弃，转用 `path` 代替。两者都返回仓库内文件的完整路径，但是未来 `filename` 仅仅是文件名而不是完整路径。

### 范围：`commits` **(PREMIUM)**

> 移动到极狐GitLab 专业版于 13.9。

仅当启用 Elasticsearch 时，此范围才可用。<!--This scope is available only if [Elasticsearch](../integration/advanced_search/elasticsearch.md) is enabled.-->

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/6/search?scope=commits&search=bye"
```

响应示例：

```json

[
  {
  "id": "4109c2d872d5fdb1ed057400d103766aaea97f98",
  "short_id": "4109c2d8",
  "title": "goodbye $.browser",
  "created_at": "2013-02-18T22:02:54.000Z",
  "parent_ids": [
    "59d05353ab575bcc2aa958fe1782e93297de64c9"
  ],
  "message": "goodbye $.browser\n",
  "author_name": "angus croll",
  "author_email": "anguscroll@gmail.com",
  "authored_date": "2013-02-18T22:02:54.000Z",
  "committer_name": "angus croll",
  "committer_email": "anguscroll@gmail.com",
  "committed_date": "2013-02-18T22:02:54.000Z",
  "project_id": 6
  }
]
```

### 范围：`blobs` **(PREMIUM)**

> 移动到极狐GitLab 专业版于 13.9。

仅当启用 Elasticsearch 时，此范围才可用。<!--This scope is available only if [Elasticsearch](../integration/advanced_search/elasticsearch.md) is enabled.-->

此范围中可用的过滤器为：

- 文件名
- 路径
- 扩展

要使用过滤器，请将其包含在您的查询中。例如：`a query filename:some_name*`。

您可以使用通配符 (`*`) 进行全局匹配。

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/6/search?scope=blobs&search=installation"
```

响应示例：

```json

[
  {
    "basename": "README",
    "data": "```\n\n## Installation\n\nQuick start using the [pre-built",
    "path": "README.md",
    "filename": "README.md",
    "id": null,
    "ref": "master",
    "startline": 46,
    "project_id": 6
  }
]
```

NOTE:
`filename` 已废弃，转用 `path` 代替。两者都返回仓库内文件的完整路径，但是未来 `filename` 仅仅是文件名而不是完整路径。

### 范围：`notes` **(PREMIUM)**

> 移动到极狐GitLab 专业版于 13.9。

仅当启用 Elasticsearch 时，此范围才可用。<!--This scope is available only if [Elasticsearch](../integration/advanced_search/elasticsearch.md) is enabled.-->

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/6/search?scope=notes&search=maxime"
```

响应示例：

```json
[
  {
    "id": 191,
    "body": "Harum maxime consequuntur et et deleniti assumenda facilis.",
    "attachment": null,
    "author": {
      "id": 23,
      "name": "User 1",
      "username": "user1",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/111d68d06e2d317b5a59c2c6c5bad808?s=80&d=identicon",
      "web_url": "http://localhost:3000/user1"
    },
    "created_at": "2017-09-05T08:01:32.068Z",
    "updated_at": "2017-09-05T08:01:32.068Z",
    "system": false,
    "noteable_id": 22,
    "noteable_type": "Issue",
    "project_id": 6,
    "noteable_iid": 2
  }
]
```

### 范围：`users`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/3/search?scope=users&search=doe"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "John Doe1",
    "username": "user1",
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/c922747a93b40d1ea88262bf1aebee62?s=80&d=identicon",
    "web_url": "http://localhost/user1"
  }
]
```

## 项目搜索 API

在特定项目内搜索表达式。

如果用户不是项目成员并且项目是私有的，对项目进行的 `GET` 请求会返回 `404` 状态码。

```plaintext
GET /projects/:id/search
```

| 参数             | 类型             | 是否必需 | 描述                                                                                               |
|----------------|----------------|------|--------------------------------------------------------------------------------------------------|
| `id`           | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                                    |
| `scope`        | string         | yes  | 搜索范围，值包括 `blobs`、`commits`、`issues`、`merge_requests`、`milestones`、`notes`、`users` 和 `wiki_blobs` |
| `search`       | string         | yes  | 搜索查询                                                                                             |
| `ref`          | string         | no   | 要搜索的仓库分支或标签的名称。默认使用项目的默认分支。只适用于以下范围：`commits`、`blobs` 和 `wiki_blobs`                             |
| `state`        | string         | no   | 按状态过滤。支持 `issues` 和 `merge_requests`；忽略其他范围                                                      |
| `confidential` | boolean        | no   | 按机密性过滤。支持 `issues` 范围；忽略其他范围                                                                     |
| `order_by`     | string         | no   | 允许的值仅为 `created_at`。如未设置，结果或按照 `created_at` 的降序排序进行基本搜索，或按最相关的文档排序（对于高级搜索）                       |
| `sort`         | string         | no   | 允许的值仅为 `asc` 或 `desc`。如未设置，结果或按照 `created_at` 的降序排序进行基本搜索，或按最相关的文档排序（对于高级搜索）                     |


响应取决于请求的范围。

### 范围：`issues`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/12/search?scope=issues&search=file"
```

响应示例：

```json
[
  {
    "id": 83,
    "iid": 1,
    "project_id": 12,
    "title": "Add file",
    "description": "Add first file",
    "state": "opened",
    "created_at": "2018-01-24T06:02:15.514Z",
    "updated_at": "2018-02-06T12:36:23.263Z",
    "closed_at": null,
    "labels":[],
    "milestone": null,
    "assignees": [{
      "id": 20,
      "name": "Ceola Deckow",
      "username": "sammy.collier",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/c23d85a4f50e0ea76ab739156c639231?s=80&d=identicon",
      "web_url": "http://localhost:3000/sammy.collier"
    }],
    "author": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://localhost:3000/root"
    },
    "assignee": {
      "id": 20,
      "name": "Ceola Deckow",
      "username": "sammy.collier",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/c23d85a4f50e0ea76ab739156c639231?s=80&d=identicon",
      "web_url": "http://localhost:3000/sammy.collier"
    },
    "user_notes_count": 0,
    "upvotes": 0,
    "downvotes": 0,
    "due_date": null,
    "confidential": false,
    "discussion_locked": null,
    "web_url": "http://localhost:3000/h5bp/7bp/subgroup-prj/issues/1",
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    }
  }
]
```

NOTE:
`assignee` 列已废弃，现在我们将其显示为单一大小的阵列 `assignees` 以符合极狐GitLab 付费版的 API。

### 范围：`merge_requests`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/6/search?scope=merge_requests&search=file"
```

响应示例：

```json
[
  {
    "id": 56,
    "iid": 8,
    "project_id": 6,
    "title": "Add first file",
    "description": "This is a test MR to add file",
    "state": "opened",
    "created_at": "2018-01-22T14:21:50.830Z",
    "updated_at": "2018-02-06T12:40:33.295Z",
    "target_branch": "master",
    "source_branch": "jaja-test",
    "upvotes": 0,
    "downvotes": 0,
    "author": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://localhost:3000/root"
    },
    "assignee": {
      "id": 5,
      "name": "Jacquelyn Kutch",
      "username": "abigail",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/3138c66095ee4bd11a508c2f7f7772da?s=80&d=identicon",
      "web_url": "http://localhost:3000/abigail"
    },
    "source_project_id": 6,
    "target_project_id": 6,
    "labels": [
      "ruby",
      "tests"
    ],
    "draft": false,
    "work_in_progress": false,
    "milestone": {
      "id": 13,
      "iid": 3,
      "project_id": 6,
      "title": "v2.0",
      "description": "Qui aut qui eos dolor beatae itaque tempore molestiae.",
      "state": "active",
      "created_at": "2017-09-05T07:58:29.099Z",
      "updated_at": "2017-09-05T07:58:29.099Z",
      "due_date": null,
      "start_date": null
    },
    "merge_when_pipeline_succeeds": false,
    "merge_status": "can_be_merged",
    "sha": "78765a2d5e0a43585945c58e61ba2f822e4d090b",
    "merge_commit_sha": null,
    "squash_commit_sha": null,
    "user_notes_count": 0,
    "discussion_locked": null,
    "should_remove_source_branch": null,
    "force_remove_source_branch": true,
    "web_url": "http://localhost:3000/twitter/flight/merge_requests/8",
    "time_stats": {
      "time_estimate": 0,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    }
  }
]
```

### 范围：`milestones`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/12/search?scope=milestones&search=release"
```

响应示例：

```json
[
  {
    "id": 44,
    "iid": 1,
    "project_id": 12,
    "title": "next release",
    "description": "Next release milestone",
    "state": "active",
    "created_at": "2018-02-06T12:43:39.271Z",
    "updated_at": "2018-02-06T12:44:01.298Z",
    "due_date": "2018-04-18",
    "start_date": "2018-02-04"
  }
]
```

### 范围：`notes` **(PREMIUM)**

> 移动到极狐GitLab 专业版于 13.9。

仅当启用 Elasticsearch 时，此范围才可用。<!--This scope is available only if [Elasticsearch](../integration/advanced_search/elasticsearch.md) is enabled.-->

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/6/search?scope=notes&search=maxime"
```

响应示例：

```json
[
  {
    "id": 191,
    "body": "Harum maxime consequuntur et et deleniti assumenda facilis.",
    "attachment": null,
    "author": {
      "id": 23,
      "name": "User 1",
      "username": "user1",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/111d68d06e2d317b5a59c2c6c5bad808?s=80&d=identicon",
      "web_url": "http://localhost:3000/user1"
    },
    "created_at": "2017-09-05T08:01:32.068Z",
    "updated_at": "2017-09-05T08:01:32.068Z",
    "system": false,
    "noteable_id": 22,
    "noteable_type": "Issue",
    "project_id": 6,
    "noteable_iid": 2
  }
]
```

### 范围：`wiki_blobs` **(PREMIUM)**

> 移动到极狐GitLab 专业版于 13.9。

仅当启用 Elasticsearch 时，此范围才可用。<!--This scope is available only if [Elasticsearch](../integration/advanced_search/elasticsearch.md) is enabled.-->

此范围中可用的过滤器为：

- 文件名
- 路径
- 扩展

要使用过滤器，请将其包含在您的查询中。例如：`a query filename:some_name*`。

您可以使用通配符 (`*`) 进行全局匹配。

对文件名和内容执行 Wiki Blob 搜索。搜索结果：

- 在文件名中找到的搜索结果显示在内容中找到的结果之前。
- 可能包含同一 Blob 的多个匹配项，因为在文件名和内容中都可能找到搜索字符串，或者可能在内容中出现多次。

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/6/search?scope=wiki_blobs&search=bye"
```

响应示例：

```json

[
  {
    "basename": "home",
    "data": "hello\n\nand bye\n\nend",
    "path": "home.md",
    "filename": "home.md",
    "id": null,
    "ref": "master",
    "startline": 5,
    "project_id": 6
  }
]
```

NOTE:
`filename` 已废弃，转用 `path` 代替。两者都返回仓库内文件的完整路径，但是未来 `filename` 仅仅是文件名而不是完整路径。

### 范围：`commits` **(PREMIUM)**

> 移动到极狐GitLab 专业版于 13.9。

仅当启用 Elasticsearch 时，此范围才可用。<!--This scope is available only if [Elasticsearch](../integration/advanced_search/elasticsearch.md) is enabled.-->

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/6/search?scope=commits&search=bye"
```

响应示例：

```json

[
  {
  "id": "4109c2d872d5fdb1ed057400d103766aaea97f98",
  "short_id": "4109c2d8",
  "title": "goodbye $.browser",
  "created_at": "2013-02-18T22:02:54.000Z",
  "parent_ids": [
    "59d05353ab575bcc2aa958fe1782e93297de64c9"
  ],
  "message": "goodbye $.browser\n",
  "author_name": "angus croll",
  "author_email": "anguscroll@gmail.com",
  "authored_date": "2013-02-18T22:02:54.000Z",
  "committer_name": "angus croll",
  "committer_email": "anguscroll@gmail.com",
  "committed_date": "2013-02-18T22:02:54.000Z",
  "project_id": 6
  }
]
```

### 范围：`blobs` **(PREMIUM)**

> 移动到极狐GitLab 专业版于 13.9。

仅当启用 Elasticsearch 时，此范围才可用。<!--This scope is available only if [Elasticsearch](../integration/advanced_search/elasticsearch.md) is enabled.-->

此范围中可用的过滤器为：

- 文件名
- 路径
- 扩展

要使用过滤器，请将其包含在您的查询中。例如：`a query filename:some_name*`。

您可以使用通配符 (`*`) 进行全局匹配。

对文件名和内容执行 Blob 搜索。搜索结果：

- 在文件名中找到的搜索结果显示在内容中找到的结果之前。
- 可能包含同一 Blob 的多个匹配项，因为在文件名和内容中都可能找到搜索字符串，或者可能在内容中出现多次。

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" https://gitlab.example.com/api/v4/projects/6/search?scope=blobs&search=keyword%20filename:*.py
```

响应示例：

```json

[
  {
    "basename": "README",
    "data": "```\n\n## Installation\n\nQuick start using the [pre-built",
    "path": "README.md",
    "filename": "README.md",
    "id": null,
    "ref": "master",
    "startline": 46,
    "project_id": 6
  }
]
```

NOTE:
`filename` 已废弃，转用 `path` 代替。两者都返回仓库内文件的完整路径，但是未来 `filename` 仅仅是文件名而不是完整路径。

### Scope: `users`

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/6/search?scope=users&search=doe"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "John Doe1",
    "username": "user1",
    "state": "active",
    "avatar_url": "http://www.gravatar.com/avatar/c922747a93b40d1ea88262bf1aebee62?s=80&d=identicon",
    "web_url": "http://localhost/user1"
  }
]
```
