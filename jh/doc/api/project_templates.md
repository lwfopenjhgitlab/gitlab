---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目模板 API **(FREE)**

这个 API 是获得以下特定项目模板的终端。

- [Dockerfile 模板](templates/dockerfiles.md)
- [Gitignore 模板](templates/gitignores.md)
- [极狐GitLab CI/CD 配置模板](templates/gitlab_ci_ymls.md)
- [开源许可模板](templates/licenses.md)
- [议题和合并请求模板](../user/project/description_templates.md)

这些模板计划在 API 的第 5 个版本中移除。

除了实例级的通用模板外，特定项目的模板也可以从这个 API 端点获得。

对[群组级文件模板](../user/group/manage.md#group-file-templates)也支持。**(PREMIUM)**

## 获取所有特定类型的模板

```plaintext
GET /projects/:id/templates/:type
```

| 参数 | 类型 | 是否必需 | 描述 |
| ---------- | ------ | -------- | ----------- |
| `id`      | integer or string | **{check-circle}** Yes       | ID 或[项目的 URL 路径](rest/index.md#namespaced-path-encoding) |
| `type`     | string | **{check-circle}** Yes  | 模板类型，接受的值：`dockerfiles`、`gitignores`、`gitlab_ci_ymls`、`licenses`、`issues`、`merge_requests` |

响应示例：

```json
[
  {
    "key": "epl-1.0",
    "name": "Eclipse Public License 1.0"
  },
  {
    "key": "lgpl-3.0",
    "name": "GNU Lesser General Public License v3.0"
  },
  {
    "key": "unlicense",
    "name": "The Unlicense"
  },
  {
    "key": "agpl-3.0",
    "name": "GNU Affero General Public License v3.0"
  },
  {
    "key": "gpl-3.0",
    "name": "GNU General Public License v3.0"
  },
  {
    "key": "bsd-3-clause",
    "name": "BSD 3-clause \"New\" or \"Revised\" License"
  },
  {
    "key": "lgpl-2.1",
    "name": "GNU Lesser General Public License v2.1"
  },
  {
    "key": "mit",
    "name": "MIT License"
  },
  {
    "key": "apache-2.0",
    "name": "Apache License 2.0"
  },
  {
    "key": "bsd-2-clause",
    "name": "BSD 2-clause \"Simplified\" License"
  },
  {
    "key": "mpl-2.0",
    "name": "Mozilla Public License 2.0"
  },
  {
    "key": "gpl-2.0",
    "name": "GNU General Public License v2.0"
  }
]
```

## 获取特定类型的某一个模板

```plaintext
GET /projects/:id/templates/:type/:name
```

| 参数 | 类型 | 是否必需 | 描述                                                                                                          |
| ---------- | ------ | -------- |-------------------------------------------------------------------------------------------------------------|
| `id`      | integer or string | **{check-circle}** Yes       | ID 或[项目的 URL 路径](rest/index.md#namespaced-path-encoding)                                                         |
| `type`     | string | **{check-circle}** Yes | 模板类型，接受的值：`dockerfiles`、`gitignores`、`gitlab_ci_ymls`、`licenses`、`issues` 或 `merge_requests`                |
| `name`     | string | **{check-circle}** Yes       | 项目的键值，可从终端获取                                                                                                |
| `source_template_project_id`   | integer |  **{dotted-circle}** No       | 存储特定模板的项目 ID，多个项目模板存在重名情况时有用。如果多个模板重名，并且没有指定 `source_template_project_id`，则会根据 `closest ancestor` 原则返回最匹配的结果 |
| `project`  | string |  **{dotted-circle}** No        | 项目名称，在模板中扩展占位符时使用。只影响许可证                                                                                    |
| `fullname` | string |  **{dotted-circle}** No        | 版权所有人的全名，在模板中扩展占位符时使用。只影响许可证                                                                                |

响应示例(Dockerfile)：

```json
{
  "name": "Binary",
  "content": "# This file is a template, and might need editing before it works on your project.\n# This Dockerfile installs a compiled binary into a bare system.\n# You must either commit your compiled binary into source control (not recommended)\n# or build the binary first as part of a CI/CD pipeline.\n\nFROM buildpack-deps:buster\n\nWORKDIR /usr/local/bin\n\n# Change `app` to whatever your binary is called\nAdd app .\nCMD [\"./app\"]\n"
}
```

响应示例(license)：

```json
{
  "key": "mit",
  "name": "MIT License",
  "nickname": null,
  "popular": true,
  "html_url": "http://choosealicense.com/licenses/mit/",
  "source_url": "https://opensource.org/licenses/MIT",
  "description": "A short and simple permissive license with conditions only requiring preservation of copyright and license notices. Licensed works, modifications, and larger works may be distributed under different terms and without source code.",
  "conditions": [
    "include-copyright"
  ],
  "permissions": [
    "commercial-use",
    "modifications",
    "distribution",
    "private-use"
  ],
  "limitations": [
    "liability",
    "warranty"
  ],
  "content": "MIT License\n\nCopyright (c) 2018 [fullname]\n\nPermission is hereby granted, free of charge, to any person obtaining a copy\nof this software and associated documentation files (the \"Software\"), to deal\nin the Software without restriction, including without limitation the rights\nto use, copy, modify, merge, publish, distribute, sublicense, and/or sell\ncopies of the Software, and to permit persons to whom the Software is\nfurnished to do so, subject to the following conditions:\n\nThe above copyright notice and this permission notice shall be included in all\ncopies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\nIMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\nFITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\nAUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\nLIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\nOUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\nSOFTWARE.\n"
}
```
