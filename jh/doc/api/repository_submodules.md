---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 仓库子模块 API **(FREE)**

## 在仓库中更新现存子模块引用

在某些工作流程中，尤其是自动化工作流程中，可以更新子模块引用以让使用它的其他项目保持最新。
此端点允许您在特定分支上更新 [Git 子模块](https://git-scm.com/book/en/v2/Git-Tools-Submodules)的引用。

```plaintext
PUT /projects/:id/repository/submodules/:submodule
```

| 参数               | 类型             | 是否必需 | 描述                                                              |
|------------------|----------------|------|-----------------------------------------------------------------|
| `id`             | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding)   |
| `submodule`      | string         | yes  | URL 编码的子模块的完整路径。例如：`lib%2Fclass%2Erb`                           |
| `branch`         | string         | yes  | 要提交到的分支的名称                                                      |
| `commit_sha`     | string         | yes  | 将子模块更新到的完整提交 SHA                                                |
| `commit_message` | string         | no   | 提交信息。如未提供信息，则设置为默认值 |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/submodules/lib%2Fmodules%2Fexample" \
--data "branch=master&commit_sha=3ddec28ea23acc5caa5d8331a6ecb2a65fc03e88&commit_message=Update submodule reference"
```

响应示例：

```json
{
  "id": "ed899a2f4b50b4370feeea94676502b42383c746",
  "short_id": "ed899a2f4b5",
  "title": "Updated submodule example_submodule with oid 3ddec28ea23acc5caa5d8331a6ecb2a65fc03e88",
  "author_name": "Dmitriy Zaporozhets",
  "author_email": "dzaporozhets@sphereconsultinginc.com",
  "committer_name": "Dmitriy Zaporozhets",
  "committer_email": "dzaporozhets@sphereconsultinginc.com",
  "created_at": "2018-09-20T09:26:24.000-07:00",
  "message": "Updated submodule example_submodule with oid 3ddec28ea23acc5caa5d8331a6ecb2a65fc03e88",
  "parent_ids": [
    "ae1d9fb46aa2b07ee9836d49862ec4e2c46fbbba"
  ],
  "committed_date": "2018-09-20T09:26:24.000-07:00",
  "authored_date": "2018-09-20T09:26:24.000-07:00",
  "status": null
}
```
