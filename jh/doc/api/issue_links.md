---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 议题链接 API **(FREE)**

> 简单的 "relates to" 关系移动到极狐GitLab 13.4（免费版）。

## 列出议题关系

获取给定议题的[链接议题](../user/project/issues/related_issues.md)列表，按关系创建日期时间排序（升序）。根据用户授权过滤议题。

```plaintext
GET /projects/:id/issues/:issue_iid/links
```

参数：

| 参数          | 类型             | 是否必需 | 描述                                                            |
|-------------|----------------|------|---------------------------------------------------------------|
| `id`        | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer        | yes  | 项目议题的内部 ID                               |

```json
[
  {
    "id" : 84,
    "iid" : 14,
    "issue_link_id": 1,
    "project_id" : 4,
    "created_at" : "2016-01-07T12:44:33.959Z",
    "title" : "Issues with auth",
    "state" : "opened",
    "assignees" : [],
    "assignee" : null,
    "labels" : [
      "bug"
    ],
    "author" : {
      "name" : "Alexandra Bashirian",
      "avatar_url" : null,
      "state" : "active",
      "web_url" : "https://gitlab.example.com/eileen.lowe",
      "id" : 18,
      "username" : "eileen.lowe"
    },
    "description" : null,
    "updated_at" : "2016-01-07T12:44:33.959Z",
    "milestone" : null,
    "user_notes_count": 0,
    "due_date": null,
    "web_url": "http://example.com/example/example/issues/14",
    "confidential": false,
    "weight": null,
    "link_type": "relates_to",
    "link_created_at": "2016-01-07T12:44:33.959Z",
    "link_updated_at": "2016-01-07T12:44:33.959Z"
  }
]
```

## 获取议题链接

> 引入于 15.1。

获取议题链接的详细信息。

```plaintext
GET /projects/:id/issues/:issue_iid/links/:issue_link_id
```

支持的参数：

| 参数              | 类型             | 是否必需                   | 描述                                                   |
|-----------------|----------------|------------------------|------------------------------------------------------|
| `id`            | integer/string | **{check-circle}** Yes | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)|
| `issue_iid`     | integer        | **{check-circle}** Yes | 项目议题的内部 ID                                           |
| `issue_link_id` | integer/string | **{check-circle}** Yes | 议题关系 ID                      |

响应主体参数：

| 参数             | 类型     | 描述                                            |
|:---------------|:-------|:----------------------------------------------|
| `source_issue` | object | 关系的源议题的详细信息                                   |
| `target_issue` | object | 关系的目标议题的详细信息                                  |
| `link_type`    | string | 关系的类型：`relates_to`、`blocks` 或 `is_blocked_by` |

请求示例：

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/84/issues/14/links/1"
```

响应示例：

```json
{
  "source_issue" : {
    "id" : 83,
    "iid" : 11,
    "project_id" : 4,
    "created_at" : "2016-01-07T12:44:33.959Z",
    "title" : "Issues with auth",
    "state" : "opened",
    "assignees" : [],
    "assignee" : null,
    "labels" : [
      "bug"
    ],
    "author" : {
      "name" : "Alexandra Bashirian",
      "avatar_url" : null,
      "state" : "active",
      "web_url" : "https://gitlab.example.com/eileen.lowe",
      "id" : 18,
      "username" : "eileen.lowe"
    },
    "description" : null,
    "updated_at" : "2016-01-07T12:44:33.959Z",
    "milestone" : null,
    "subscribed" : true,
    "user_notes_count": 0,
    "due_date": null,
    "web_url": "http://example.com/example/example/issues/11",
    "confidential": false,
    "weight": null
  },
  "target_issue" : {
    "id" : 84,
    "iid" : 14,
    "project_id" : 4,
    "created_at" : "2016-01-07T12:44:33.959Z",
    "title" : "Issues with auth",
    "state" : "opened",
    "assignees" : [],
    "assignee" : null,
    "labels" : [
      "bug"
    ],
    "author" : {
      "name" : "Alexandra Bashirian",
      "avatar_url" : null,
      "state" : "active",
      "web_url" : "https://gitlab.example.com/eileen.lowe",
      "id" : 18,
      "username" : "eileen.lowe"
    },
    "description" : null,
    "updated_at" : "2016-01-07T12:44:33.959Z",
    "milestone" : null,
    "subscribed" : true,
    "user_notes_count": 0,
    "due_date": null,
    "web_url": "http://example.com/example/example/issues/14",
    "confidential": false,
    "weight": null
  },
  "link_type": "relates_to"
}
```

## 创建议题链接

在两个议题之间建立双向关系。必须允许用户更新这两个议题。

```plaintext
POST /projects/:id/issues/:issue_iid/links
```

| 参数                  | 类型             | 是否必需 | 描述                                                            |
|---------------------|----------------|------|---------------------------------------------------------------|
| `id`                | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid`         | integer        | yes  | 项目议题的内部 ID                                                    |
| `target_project_id` | integer/string | yes  | 目标项目的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding)  |
| `target_issue_iid`  | integer/string | yes  | 目标项目议题的内部 ID                                                  |
| `link_type`         | string         | no   | 关系类型：`relates_to`、`blocks`、`is_blocked_by`，默认为 `relates_to`   |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/4/issues/1/links?target_project_id=5&target_issue_iid=1"
```

响应示例：

```json
{
  "source_issue" : {
    "id" : 83,
    "iid" : 11,
    "project_id" : 4,
    "created_at" : "2016-01-07T12:44:33.959Z",
    "title" : "Issues with auth",
    "state" : "opened",
    "assignees" : [],
    "assignee" : null,
    "labels" : [
      "bug"
    ],
    "author" : {
      "name" : "Alexandra Bashirian",
      "avatar_url" : null,
      "state" : "active",
      "web_url" : "https://gitlab.example.com/eileen.lowe",
      "id" : 18,
      "username" : "eileen.lowe"
    },
    "description" : null,
    "updated_at" : "2016-01-07T12:44:33.959Z",
    "milestone" : null,
    "subscribed" : true,
    "user_notes_count": 0,
    "due_date": null,
    "web_url": "http://example.com/example/example/issues/11",
    "confidential": false,
    "weight": null
  },
  "target_issue" : {
    "id" : 84,
    "iid" : 14,
    "project_id" : 4,
    "created_at" : "2016-01-07T12:44:33.959Z",
    "title" : "Issues with auth",
    "state" : "opened",
    "assignees" : [],
    "assignee" : null,
    "labels" : [
      "bug"
    ],
    "author" : {
      "name" : "Alexandra Bashirian",
      "avatar_url" : null,
      "state" : "active",
      "web_url" : "https://gitlab.example.com/eileen.lowe",
      "id" : 18,
      "username" : "eileen.lowe"
    },
    "description" : null,
    "updated_at" : "2016-01-07T12:44:33.959Z",
    "milestone" : null,
    "subscribed" : true,
    "user_notes_count": 0,
    "due_date": null,
    "web_url": "http://example.com/example/example/issues/14",
    "confidential": false,
    "weight": null
  },
  "link_type": "relates_to"
}
```

## 删除议题链接

删除议题链接，进而移除双向关系。

```plaintext
DELETE /projects/:id/issues/:issue_iid/links/:issue_link_id
```

| 参数              | 类型             | 是否必需 | 描述                                                            |
|-----------------|----------------|------|---------------------------------------------------------------|
| `id`            | integer/string | yes  | 经过身份验证的用户拥有的 ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid`     | integer        | yes  | 项目议题的内部 ID                                                    |
| `issue_link_id` | integer/string | yes  | 议题关系 ID                                                       |
| `link_type`     | string         | no   | 关系类型：`relates_to`、`blocks`、`is_blocked_by`，默认为 `relates_to`   |

```json
{
  "source_issue" : {
    "id" : 83,
    "iid" : 11,
    "project_id" : 4,
    "created_at" : "2016-01-07T12:44:33.959Z",
    "title" : "Issues with auth",
    "state" : "opened",
    "assignees" : [],
    "assignee" : null,
    "labels" : [
      "bug"
    ],
    "author" : {
      "name" : "Alexandra Bashirian",
      "avatar_url" : null,
      "state" : "active",
      "web_url" : "https://gitlab.example.com/eileen.lowe",
      "id" : 18,
      "username" : "eileen.lowe"
    },
    "description" : null,
    "updated_at" : "2016-01-07T12:44:33.959Z",
    "milestone" : null,
    "subscribed" : true,
    "user_notes_count": 0,
    "due_date": null,
    "web_url": "http://example.com/example/example/issues/11",
    "confidential": false,
    "weight": null
  },
  "target_issue" : {
    "id" : 84,
    "iid" : 14,
    "project_id" : 4,
    "created_at" : "2016-01-07T12:44:33.959Z",
    "title" : "Issues with auth",
    "state" : "opened",
    "assignees" : [],
    "assignee" : null,
    "labels" : [
      "bug"
    ],
    "author" : {
      "name" : "Alexandra Bashirian",
      "avatar_url" : null,
      "state" : "active",
      "web_url" : "https://gitlab.example.com/eileen.lowe",
      "id" : 18,
      "username" : "eileen.lowe"
    },
    "description" : null,
    "updated_at" : "2016-01-07T12:44:33.959Z",
    "milestone" : null,
    "subscribed" : true,
    "user_notes_count": 0,
    "due_date": null,
    "web_url": "http://example.com/example/example/issues/14",
    "confidential": false,
    "weight": null
  },
  "link_type": "relates_to"
}
```
