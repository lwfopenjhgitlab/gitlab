---
stage: Package
group: Container Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 依赖代理 API **(FREE)**

<a href="#purge-the-dependency-proxy-for-a-group"></a>

## 清理群组内的依赖代理

> - 在极狐GitLab 12.10 版本中引入。
> - 在极狐GitLab 13.6 版本中从专业版转移至免费版可用。

安排群组内 manifests 和 blobs 的清理工作。这个 API 端点需要相应群组的所有者角色。

```plaintext
DELETE /groups/:id/dependency_proxy/cache
```

| 参数 | 类型 | 是否必需 | 描述 |
| --------- | ---- | -------- | ----------- |
| `id`      | integer/string | 是 | 群组 ID 或者 [URL 编码后的群组路径](rest/index.md#namespaced-path-encoding)。 |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/5/dependency_proxy/cache"
```
