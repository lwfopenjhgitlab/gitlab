---
stage: Manage
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 通过直接传输 API 进行群组和项目迁移 **(FREE)**

> - 引入于极狐GitLab 14.1。
> - 项目迁移引入于极狐GitLab 15.11。

通过直接传输 API 进行群组迁移，您可以启动并查看通过[直接传输群组迁移](../user/group/import/index.md#migrate-groups-by-direct-transfer-recommended)发起的迁移进度。

WARNING:
使用此 API 迁移项目处于 Beta <!--[Beta](../policy/experiment-beta-support.md#beta) -->状态。此功能未准备好用于生产。

## 先决条件

关于通过直接传输 API 迁移群组的先决条件信息，请参见[通过直接传输迁移群组](../user/group/import/index.md#prerequisites)。

## 开启新群组或项目迁移

> - 引入于极狐GitLab 14.2。
> - `project_entity` 源类型引入于极狐GitLab 15.11。

使用此端点开启新群组或项目迁移。指定：

- `entities[group_entity]`：迁移群组
- `entities[project_entity]`：迁移项目（Beta）

```plaintext
POST /bulk_imports
```

| 参数 | 类型    | 是否必需 | 描述                                                                                      |
| --------------------------------- | ------ |------|-----------------------------------------------------------------------------------------|
| `configuration`                   | Hash   | yes  | 极狐GitLab 实例配置                                                                           |
| `configuration[url]`              | String | yes  | 极狐GitLab 实例网址                                                                           |
| `configuration[access_token]`     | String | yes  | 极狐GitLab 实例的访问令牌                                                                        |
| `entities`                        | Array  | yes  | 要导入的实体列表                                                                                |
| `entities[source_type]`           | String | yes  | 源实体类型，有效值为 `group_entity`（极狐GitLab 14.2 及更高版本）和 `project_entity` (极狐GitLab 15.11 及更高版本) |
| `entities[source_full_path]`      | String | yes  | 要导入的实体完整路径                                                                              |
| `entities[destination_name]`      | String | yes  | 实体目标 slug。已废弃，使用 destination_slug 代替                                                    |
| `entities[destination_slug]`      | String | yes  | 实体目标 slug                                                                               |
| `entities[destination_namespace]` | String | yes  | 实体目标命名空间                                                                                |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/bulk_imports" \
  --header "Content-Type: application/json" \
  --data '{
    "configuration": {
      "url": "http://gitlab.example/",
      "access_token": "access_token"
    },
    "entities": [
      {
        "source_full_path": "source/full/path",
        "source_type": "group_entity",
        "destination_slug": "destination_slug",
        "destination_namespace": "destination/namespace/path"
      }
    ]
  }'
```

```json
{ "id": 1, "status": "created", "source_type": "gitlab", "created_at": "2021-06-18T09:45:55.358Z", "updated_at": "2021-06-18T09:46:27.003Z" }
```

## 列出所有群组或项目迁移

```plaintext
GET /bulk_imports
```

| 参数 | 类型    | 是否必需 | 描述                                            |
|:-----------|:--------|:---------|:----------------------------------------------|
| `per_page` | integer | no       | 每页要返回的记录数                                     |
| `page`     | integer | no       | 要检索的页面                                        |
| `sort`     | string  | no       | 返回按创建日期升序 `asc` 或降序 `desc` 排列的记录。默认为降序 `desc` |
| `status`   | string  | no       | 导入状态                                          |

状态可以是以下之一：

- `created`
- `started`
- `finished`
- `failed`

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/bulk_imports?per_page=2&page=1"
```

```json
[
    {
        "id": 1,
        "status": "finished",
        "source_type": "gitlab",
        "created_at": "2021-06-18T09:45:55.358Z",
        "updated_at": "2021-06-18T09:46:27.003Z"
    },
    {
        "id": 2,
        "status": "started",
        "source_type": "gitlab",
        "created_at": "2021-06-18T09:47:36.581Z",
        "updated_at": "2021-06-18T09:47:58.286Z"
    }
]
```

## 列出所有群组或项目迁移的实体

```plaintext
GET /bulk_imports/entities
```

| 参数 | 类型    | 是否必需 | 描述                                            |
|:-----------|:--------|:---------|:----------------------------------------------|
| `per_page` | integer | no       | 每页要返回的记录数                                     |
| `page`     | integer | no       | 要检索的页面                                        |
| `sort`     | string  | no       | 返回按创建日期升序 `asc` 或降序 `desc` 排列的记录。默认为降序 `desc` |
| `status`   | string  | no       | 导入状态                                          |

状态可以是以下之一：

- `created`
- `started`
- `finished`
- `failed`

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/bulk_imports/entities?per_page=2&page=1&status=started"
```

```json
[
  {
    "id": 1,
    "bulk_import_id": 1,
    "status": "finished",
    "source_full_path": "source_group",
    "destination_slug": "destination_slug",
    "destination_namespace": "destination_path",
    "parent_id": null,
    "namespace_id": 1,
    "project_id": null,
    "created_at": "2021-06-18T09:47:37.390Z",
    "updated_at": "2021-06-18T09:47:51.867Z",
    "failures": []
  },
  {
    "id": 2,
    "bulk_import_id": 2,
    "status": "failed",
    "source_full_path": "another_group",
    "destination_slug": "another_slug",
    "destination_namespace": "another_namespace",
    "parent_id": null,
    "namespace_id": null,
    "project_id": null,
    "created_at": "2021-06-24T10:40:20.110Z",
    "updated_at": "2021-06-24T10:40:46.590Z",
    "failures": [
      {
        "relation": "group",
        "step": "extractor",
        "exception_message": "Error!",
        "exception_class": "Exception",
        "correlation_id_value": "dfcf583058ed4508e4c7c617bd7f0edd",
        "created_at": "2021-06-24T10:40:46.495Z",
        "pipeline_class": "BulkImports::Groups::Pipelines::GroupPipeline",
        "pipeline_step": "extractor"
      }
    ]
  }
]
```

## 获取群组或项目迁移详细信息

```plaintext
GET /bulk_imports/:id
```

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/bulk_imports/1"
```

```json
{
  "id": 1,
  "status": "finished",
  "source_type": "gitlab",
  "created_at": "2021-06-18T09:45:55.358Z",
  "updated_at": "2021-06-18T09:46:27.003Z"
}
```

## 列出群组或项目迁移实体

```plaintext
GET /bulk_imports/:id/entities
```

| 参数 | 类型    | 是否必需 | 描述                                            |
|:-----------|:--------|:---------|:----------------------------------------------|
| `per_page` | integer | no       | 每页要返回的记录数                                     |
| `page`     | integer | no       | 要检索的页面                                        |
| `sort`     | string  | no       | 返回按创建日期升序 `asc` 或降序 `desc` 排列的记录。默认为降序 `desc` |
| `status`   | string  | no       | 导入状态                                          |

状态可以是以下之一：

- `created`
- `started`
- `finished`
- `failed`

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/bulk_imports/1/entities?per_page=2&page=1&status=finished"
```

```json
[
    {
        "id": 1,
        "status": "finished",
        "source_type": "gitlab",
        "created_at": "2021-06-18T09:45:55.358Z",
        "updated_at": "2021-06-18T09:46:27.003Z"
    }
]
```

## 获取群组或项目迁移实体详细信息

```plaintext
GET /bulk_imports/:id/entities/:entity_id
```

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/bulk_imports/1/entities/2"
```

```json
{
  "id": 1,
  "status": "finished",
  "source_type": "gitlab",
  "created_at": "2021-06-18T09:45:55.358Z",
  "updated_at": "2021-06-18T09:46:27.003Z"
}
```
