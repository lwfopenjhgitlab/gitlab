---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Sidekiq 队列管理 API **(FREE SELF)**

> 引入于 12.9 版本。

从匹配给定元数据<!--[元数据](../development/logging.md#logging-context-metadata-through-rails-or-grape-requests)-->的 Sidekiq 队列中删除作业。

响应有三个字段:

1. `deleted_jobs` - 请求删除的作业数。
1. `queue_size` - 处理请求后队列的剩余大小。
1. `completed` - 请求是否能够及时处理整个队列。如果不能，使用相同的参数重试可能会删除更多的作业（包括发出第一个请求后添加的作业）。

此 API 接口仅对管理员可用。

```plaintext
DELETE /admin/sidekiq/queues/:queue_name
```

| 参数           | 类型   | 是否必需 | 描述                                                                                                                                   |
|---------------------|--------|----------|----------------------------------------------------------------------------------------------------------------------------------------------|
| `queue_name`        | string | yes      | 要从其中删除作业的队列的名称。                                                                                                    |
| `user`              | string | no       | 发起此作业的用户名。                                                                                              |
| `project`           | string | no       | 发起此作业的项目的完成路径。                                                                              |
| `root_namespace`    | string | no       | 项目的根明命名空间。                                                                                                            |
| `subscription_plan` | string | no       | 根命名空间的订阅计划（仅限于 SaaS）。                                                                                |
| `caller_id`         | string | no       | 发起此作业的源头（例如：`ProjectsController#create`、`/api/:version/projects/:id`、`PostReceive`）。 |
| `feature_category`  | string | no       | 后台作业的功能类别（例如：`team_planning` 或者 `code_review`）。                                                  |
| `worker_class`      | string | no       | 后端 worker 的类名（例如：`PostReceive` 或者 `MergeWorker`）。                                                         |

除了 `queue_name` 之外，至少需要一个属性。

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/admin/sidekiq/queues/authorized_projects?user=root"
```

返回示例：

```json
{
  "completed": true,
  "deleted_jobs": 7,
  "queue_size": 14
}
```
