---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
---

# 合并请求批准 API **(PREMIUM)**

> - 使用 `/approvals` 端点更改批准配置废弃于极狐GitLab 12.3。
> - 端点`/approvals` 移除于极狐GitLab 16.0。

配置项目中的[所有合并请求的批准](../user/project/merge_requests/approvals/index.md)。必需为所有端点进行授权。

## 项目级别的合并请求批准

### 获取配置

> - 移动到极狐GitLab 专业版于 13.9。
> - `approvers` 和 `approver_groups` 字段废弃于极狐GitLab 12.3 并经常返回空值。使用[项目级别批准规则](#get-project-level-rules)访问此信息。

您可以使用以下端点请求项目批准配置的信息：

```plaintext
GET /projects/:id/approvals
```

支持的参数：

| 参数   | 类型                | 是否必需 | 描述                                                    |
|------|-------------------|------|-------------------------------------------------------|
| `id` | integer or string | **{check-circle}** Yes   | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |

```json
{
  "approvers": [], // Deprecated in GitLab 12.3, always returns empty
  "approver_groups": [], // Deprecated in GitLab 12.3, always returns empty
  "approvals_before_merge": 2,
  "reset_approvals_on_push": true,
  "selective_code_owner_removals": false,
  "disable_overriding_approvers_per_merge_request": false,
  "merge_requests_author_approval": true,
  "merge_requests_disable_committers_approval": false,
  "require_password_to_approve": true
}
```

### 更改配置

> 移动到极狐GitLab 专业版于 13.9。

如果有相关权限，您可以使用以下端点更改批准配置：

```plaintext
POST /projects/:id/approvals
```

支持的参数：

| 参数                                               | 类型                | 是否必需                      | 描述                                                           |
|--------------------------------------------------|-------------------|---------------------------|--------------------------------------------------------------|
| `id`                                             | integer or string | **{check-circle}** Yes    | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)   |
| `approvals_before_merge`（已废弃）                    | integer           | **{dotted-circle}** No    | 合并 MR 之前需要多少批准。废弃于 12.3                                      |
| `disable_overriding_approvers_per_merge_request` | boolean           | **{dotted-circle}** No    | 允许或阻止覆盖每个合并请求的批准者                                            |
| `merge_requests_author_approval`                 | boolean           | **{dotted-circle}** No    | 允许或阻止作者批准自己的合并请求。`true` 表示作者可以批准自己的合并请求                      |
| `merge_requests_disable_committers_approval`     | boolean           | **{dotted-circle}** No    | 允许或阻止提交者批准自己的合并请求                                            |
| `require_password_to_approve`                    | boolean           | **{dotted-circle}** No    | 要求批准者在添加批准前输入密码进行授权                                          |
| `reset_approvals_on_push`                        | boolean           | **{dotted-circle}** No    | 重置新推送的批准                                                     |
| `selective_code_owner_removals`                  | boolean           | **{dotted-circle}** No    | 如果他们的文件发生更改，则重置代码所有者的批准。仅当禁用 `reset_approvals_on_push` 时才能启用 |

```json
{
  "approvals_before_merge": 2,
  "reset_approvals_on_push": true,
  "selective_code_owner_removals": false,
  "disable_overriding_approvers_per_merge_request": false,
  "merge_requests_author_approval": false,
  "merge_requests_disable_committers_approval": false,
  "require_password_to_approve": true
}
```

<a id="get-project-level-rules"></a>"

### 获取项目级别的规则

> - 移动到极狐GitLab 专业版于 13.9。
> - 分页支持引入于极狐GitLab 15.3，[功能标志](../administration/feature_flags.md)为 `approval_rules_pagination`。默认启用。
> - `applies_to_all_protected_branches` 特性引入于极狐GitLab 15.3。
> - 分页支持普遍可用于极狐GitLab 15.7。功能标志 `approval_rules_pagination` 已移除。
> - `usernames` 参数引入于极狐GitLab 15.8。

您可以使用以下端点请求有关项目批准规则的信息：

```plaintext
GET /projects/:id/approval_rules
```

使用 `page` 和 `per_page` [分页](rest/index.md#offset-based-pagination)参数限制批准规则列表。

支持的参数：

| 参数   | 类型                | 是否必需 | 描述                                                    |
|------|-------------------|------|-------------------------------------------------------|
| `id` | integer or string | **{check-circle}** Yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |

```json
[
  {
    "id": 1,
    "name": "security",
    "rule_type": "regular",
    "eligible_approvers": [
      {
        "id": 5,
        "name": "John Doe",
        "username": "jdoe",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
        "web_url": "http://localhost/jdoe"
      },
      {
        "id": 50,
        "name": "Group Member 1",
        "username": "group_member_1",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
        "web_url": "http://localhost/group_member_1"
      }
    ],
    "approvals_required": 3,
    "users": [
      {
        "id": 5,
        "name": "John Doe",
        "username": "jdoe",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
        "web_url": "http://localhost/jdoe"
      }
    ],
    "groups": [
      {
        "id": 5,
        "name": "group1",
        "path": "group1",
        "description": "",
        "visibility": "public",
        "lfs_enabled": false,
        "avatar_url": null,
        "web_url": "http://localhost/groups/group1",
        "request_access_enabled": false,
        "full_name": "group1",
        "full_path": "group1",
        "parent_id": null,
        "ldap_cn": null,
        "ldap_access": null
      }
    ],
    "protected_branches": [
      {
        "id": 1,
        "name": "master",
        "push_access_levels": [
          {
            "access_level": 30,
            "access_level_description": "Developers + Maintainers"
          }
        ],
        "merge_access_levels": [
          {
            "access_level": 30,
            "access_level_description": "Developers + Maintainers"
          }
        ],
        "unprotect_access_levels": [
          {
            "access_level": 40,
            "access_level_description": "Maintainers"
          }
        ],
        "code_owner_approval_required": "false"
      }
    ],
    "contains_hidden_groups": false
  }
]
```

### 获取单个项目级别的规则

> - 引入于极狐GitLab 13.7。
> - `applies_to_all_protected_branches` 属性引入于极狐GitLab 15.3。
<!--> - `usernames` 参数引入于极狐GitLab 1x.x。-->

您可以使用以下端点请求有关单个项目批准规则的信息：

```plaintext
GET /projects/:id/approval_rules/:approval_rule_id
```

支持的参数：

| 参数                 | 类型                | 是否必需 | 描述                                                    |
|--------------------|-------------------|------|-------------------------------------------------------|
| `id`               | integer or string | **{check-circle}** Yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `approval_rule_id` | integer           | **{check-circle}** Yes  | 批准规则 ID                             |

```json
{
  "id": 1,
  "name": "security",
  "rule_type": "regular",
  "eligible_approvers": [
    {
      "id": 5,
      "name": "John Doe",
      "username": "jdoe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/jdoe"
    },
    {
      "id": 50,
      "name": "Group Member 1",
      "username": "group_member_1",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/group_member_1"
    }
  ],
  "approvals_required": 3,
  "users": [
    {
      "id": 5,
      "name": "John Doe",
      "username": "jdoe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/jdoe"
    }
  ],
  "groups": [
    {
      "id": 5,
      "name": "group1",
      "path": "group1",
      "description": "",
      "visibility": "public",
      "lfs_enabled": false,
      "avatar_url": null,
      "web_url": "http://localhost/groups/group1",
      "request_access_enabled": false,
      "full_name": "group1",
      "full_path": "group1",
      "parent_id": null,
      "ldap_cn": null,
      "ldap_access": null
    }
  ],
  "applies_to_all_protected_branches": false,
  "protected_branches": [
    {
      "id": 1,
      "name": "master",
      "push_access_levels": [
        {
          "access_level": 30,
          "access_level_description": "Developers + Maintainers"
        }
      ],
      "merge_access_levels": [
        {
          "access_level": 30,
          "access_level_description": "Developers + Maintainers"
        }
      ],
      "unprotect_access_levels": [
        {
          "access_level": 40,
          "access_level_description": "Maintainers"
        }
      ],
      "code_owner_approval_required": "false"
    }
  ],
  "contains_hidden_groups": false
}
```

### 创建项目级别的规则

> - 移动到极狐GitLab 专业版于 13.9。
> - 漏洞检查功能移除于极狐GitLab 15.0。
> - `applies_to_all_protected_branches` 属性引入于极狐GitLab 15.3。
<!-- > - `usernames` property was [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/) in GitLab 1x.x.-->

您可以使用以下端点创建项目批准规则。

```plaintext
POST /projects/:id/approval_rules
```

支持的参数：

| 参数                                    | 类型                | 是否必需                   | 描述                                                                                                                                                                                                       |
|---------------------------------------|-------------------|------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                                  | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                                                                                                                                             |
| `name`                                | string            | **{check-circle}** Yes | 批准规则名称                                                                                                                                                                                                   |
| `approvals_required`                  | integer           | **{check-circle}** Yes | 此规则所需要的批准数量                                                                                                                                                                                              |
| `applies_to_all_protected_branches`   | boolean           | **{dotted-circle}** No | 规则是否应用于所有受保护的分支。如果设置为 `true`，`protected_branch_ids` 的值将被忽略。默认为 `false`。引入于极狐itLab 15.3                                                                                                                   |
| `rule_type`                           | string            | **{dotted-circle}** No | 规则类型。`any_approver` 是预配置的默认规则，且 `approvals_required` 为 `0`。其他的规则是 `regular`                                                                                                                              
| `user_ids`                            | Array             | **{dotted-circle}** No | 作为批准者的用户的 ID                                                                                                                                                                                             |
| `group_ids`                           | Array             | **{dotted-circle}** No | 作为批准者的群组的 ID                                                                                                                                                                                             |
| `protected_branch_ids`                | Array             | **{dotted-circle}** No | 应用规则的受保护分支的 ID。要识别 ID，请[使用 API](protected_branches.md#list-protected-branches)                                                                                                                           |
| `report_type`                         | string            | **{dotted-circle}** No | 当规则类型为 `report_approver` 时所需的报告类型。支持的报告类型为 `license_scanning`（废弃于 15.9）<!--[(废弃于 15.9)](../update/deprecations.md#license-check-and-the-policies-tab-on-the-license-compliance-page)-->和 `code_coverage` |
| `usernames`                           | string array      | **{dotted-circle}** No | 规则的用户名                                                                                                                                                                                                   |

```json
{
  "id": 1,
  "name": "security",
  "rule_type": "regular",
  "eligible_approvers": [
    {
      "id": 2,
      "name": "John Doe",
      "username": "jdoe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/jdoe"
    },
    {
      "id": 50,
      "name": "Group Member 1",
      "username": "group_member_1",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/group_member_1"
    }
  ],
  "approvals_required": 1,
  "users": [
    {
      "id": 2,
      "name": "John Doe",
      "username": "jdoe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/jdoe"
    }
  ],
  "groups": [
    {
      "id": 5,
      "name": "group1",
      "path": "group1",
      "description": "",
      "visibility": "public",
      "lfs_enabled": false,
      "avatar_url": null,
      "web_url": "http://localhost/groups/group1",
      "request_access_enabled": false,
      "full_name": "group1",
      "full_path": "group1",
      "parent_id": null,
      "ldap_cn": null,
      "ldap_access": null
    }
  ],
  "protected_branches": [
    {
      "id": 1,
      "name": "master",
      "push_access_levels": [
        {
          "access_level": 30,
          "access_level_description": "Developers + Maintainers"
        }
      ],
      "merge_access_levels": [
        {
          "access_level": 30,
          "access_level_description": "Developers + Maintainers"
        }
      ],
      "unprotect_access_levels": [
        {
          "access_level": 40,
          "access_level_description": "Maintainers"
        }
      ],
      "code_owner_approval_required": "false"
    }
  ],
  "contains_hidden_groups": false
}
```
<!--
You can increase the default number of 0 required approvers like this:

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
  --header 'Content-Type: application/json' \
  --data '{"name": "Any name", "rule_type": "any_approver", "approvals_required": 2}'
```
-->
您可以创建额外的针对于特定用户的规则：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
  --header 'Content-Type: application/json' \
  --data '{"name": "Name of your rule", "approvals_required": 3, "user_ids": [123, 456, 789]}' \
  https://gitlab.example.com/api/v4/projects/<project_id>/approval_rules
```

### 更新项目级别的规则

> - 移动到极狐GitLab 专业版于 13.9。
> - 漏洞检查功能移除于极狐GitLab 15.0。
> - `applies_to_all_protected_branches` 属性引入于极狐GitLab 15.3。
<!-- > - `usernames` property was [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/) in GitLab 1x.x.-->

您可以使用以下端点更新项目批准规则：

```plaintext
PUT /projects/:id/approval_rules/:approval_rule_id
```

**重要：** 不在 `users`/`groups` 参数中的批准者和群组会被**移除**。

支持的参数：

| 参数                                  | 类型                | 是否必需 | 描述                                                                                     |
|-------------------------------------|-------------------|------|----------------------------------------------------------------------------------------|
| `id`                                | integer or string | **{check-circle}** Yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                             |
| `approval_rule_id`                  | integer           | **{check-circle}** Yes  | 批准规则 ID                                                                                |
| `name`                              | string            | **{check-circle}** Yes  | 批准规则名称                                                                                 |
| `approvals_required`                | integer           | **{check-circle}** Yes  | 此规则所需批准的数量                                                                             |
| `applies_to_all_protected_branches` |  boolean    | **{dotted-circle}** No  | 规则是否应用到所有的受保护分支。如果设置为 `true`，忽略 `protected_branch_ids` 的值。默认为 `false`。引入于极狐GitLab 15.3 |
| `user_ids`                          | Array             | **{dotted-circle}** No   | 作为批准者的用户的 ID                                                                           |
| `group_ids`                         | Array             | **{dotted-circle}** No   | 作为批准者的群组的 ID                                                                           |
| `protected_branch_ids`              | Array             | **{dotted-circle}** No   | 应用规则的受保护分支的 ID。要识别 ID，请[使用 API](protected_branches.md#list-protected-branches)         |
| `remove_hidden_groups`              | boolean              |  **{dotted-circle}** No    | 是否应该移除隐藏群组                                                                             |
| `usernames`                         | string array           | **{dotted-circle}** No   | 规则的用户名称                                                                                |

```json
{
  "id": 1,
  "name": "security",
  "rule_type": "regular",
  "eligible_approvers": [
    {
      "id": 2,
      "name": "John Doe",
      "username": "jdoe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/jdoe"
    },
    {
      "id": 50,
      "name": "Group Member 1",
      "username": "group_member_1",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/group_member_1"
    }
  ],
  "approvals_required": 1,
  "users": [
    {
      "id": 2,
      "name": "John Doe",
      "username": "jdoe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/jdoe"
    }
  ],
  "groups": [
    {
      "id": 5,
      "name": "group1",
      "path": "group1",
      "description": "",
      "visibility": "public",
      "lfs_enabled": false,
      "avatar_url": null,
      "web_url": "http://localhost/groups/group1",
      "request_access_enabled": false,
      "full_name": "group1",
      "full_path": "group1",
      "parent_id": null,
      "ldap_cn": null,
      "ldap_access": null
    }
  ],
  "protected_branches": [
    {
      "id": 1,
      "name": "master",
      "push_access_levels": [
        {
          "access_level": 30,
          "access_level_description": "Developers + Maintainers"
        }
      ],
      "merge_access_levels": [
        {
          "access_level": 30,
          "access_level_description": "Developers + Maintainers"
        }
      ],
      "unprotect_access_levels": [
        {
          "access_level": 40,
          "access_level_description": "Maintainers"
        }
      ],
      "code_owner_approval_required": "false"
    }
  ],
  "contains_hidden_groups": false
}
```

### 删除项目级别的规则

> - 移动到极狐GitLab 专业版于 13.9

您可以使用以下端点删除项目批准规则：

```plaintext
DELETE /projects/:id/approval_rules/:approval_rule_id
```

支持的参数：

| 参数                 | 类型                | 是否必需 | 描述                                                    |
|--------------------|-------------------|------|-------------------------------------------------------|
| `id`               | integer or string | **{check-circle}** Yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `approval_rule_id` | integer           | **{check-circle}** Yes  | 批准规则 ID                              

## 合并请求级别的合并请求批准

对特定合并请求所进行的批准配置。必须对所有端点进行授权。

### 获取配置

> 移动到极狐GitLab 专业版于 13.9。

您可以使用以下端点请求有关合并请求批准状态的信息：

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/approvals
```

支持的参数：

| 参数                  | 类型                | 是否必需 | 描述                                                    |
|---------------------|-------------------|------|-------------------------------------------------------|
| `id`                | integer or string | **{check-circle}** Yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer           | **{check-circle}** Yes  | 合并请求 IID                                         |

```json
{
  "id": 5,
  "iid": 5,
  "project_id": 1,
  "title": "Approvals API",
  "description": "Test",
  "state": "opened",
  "created_at": "2016-06-08T00:19:52.638Z",
  "updated_at": "2016-06-08T21:20:42.470Z",
  "merge_status": "cannot_be_merged",
  "approvals_required": 2,
  "approvals_left": 1,
  "approved_by": [
    {
      "user": {
        "name": "Administrator",
        "username": "root",
        "id": 1,
        "state": "active",
        "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80\u0026d=identicon",
        "web_url": "http://localhost:3000/root"
      }
    }
  ]
}
```

<!--- start_remove The following content will be removed on remove_date: '2023-08-17' -->

### 更改批准配置（移除）

> - 端点 `/approvals` 废弃于极狐GitLab 13.10。
> - 端点 `approvals` 禁用于极狐GitLab 16.0，[功能标志](../administration/feature_flags.md)为 `remove_deprecated_approvals`。默认禁用。

端点 `POST /projects/:id/merge_requests/:merge_request_iid/approvals` 在 13。10 中已弃用，并在 16.0 中删除。要更改合并请求所需的批准，请使用本页面中的[创建合并请求级别规则](#create-merge-request-level-rule)中所描述的 `/approval_rules` 端点。
<!--- end_remove -->

### 获取合并请求的批准状态

> - 移动到极狐GitLab 专业版于 13.9。

您可以使用以下端点请求有关合并请求批准状态的信息：

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/approval_state
```

如果为合并请求创建合并请求级别规则，`approval_rules_overwritten` 为 `true`；如果没创建，则为 `false`。

这包括有关已批准用户(`approved_by`)以及规则是否已被批准 (`approved`)的其他信息。

支持的参数：

| 参数                  | 类型                | 是否必需 | 描述                                                    |
|---------------------|-------------------|------|-------------------------------------------------------|
| `id`                | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer           | **{check-circle}** Yes | 合并请求 IID                                          |

```json
{
  "approval_rules_overwritten": true,
  "rules": [
    {
      "id": 1,
      "name": "Ruby",
      "rule_type": "regular",
      "eligible_approvers": [
        {
          "id": 4,
          "name": "John Doe",
          "username": "jdoe",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
          "web_url": "http://localhost/jdoe"
        }
      ],
      "approvals_required": 2,
      "users": [
        {
          "id": 4,
          "name": "John Doe",
          "username": "jdoe",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
          "web_url": "http://localhost/jdoe"
        }
      ],
      "groups": [],
      "contains_hidden_groups": false,
      "approved_by": [
        {
          "id": 4,
          "name": "John Doe",
          "username": "jdoe",
          "state": "active",
          "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
          "web_url": "http://localhost/jdoe"
        }
      ],
      "source_rule": null,
      "approved": true,
      "overridden": false
    }
  ]
}
```

### 获取合并请求级别规则

> - 移动到极狐GitLab 专业版于 13.9。
> - 分页支持引入于极狐GitLab 15.3，[功能标志](../administration/feature_flags.md)为 `approval_rules_pagination`。默认启用。
> - 分页支持普遍可用于极狐GitLab 15.7。功能标志 `approval_rules_pagination` 已移除。


您可以使用以下端点请求有关合并请求批准规则的信息：

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/approval_rules
```

使用 `page` 和 `per_page` [分页](rest/index.md#offset-based-pagination)参数限制批准规则列表。

支持的参数：

| 参数                  | 类型                | 是否必需 | 描述                                                    |
|---------------------|-------------------|------|-------------------------------------------------------|
| `id`                | integer or string | **{check-circle}** Yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer           | **{check-circle}** Yes  | 合并请求 IID                                           |

```json
[
  {
    "id": 1,
    "name": "security",
    "rule_type": "regular",
    "eligible_approvers": [
      {
        "id": 5,
        "name": "John Doe",
        "username": "jdoe",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
        "web_url": "http://localhost/jdoe"
      },
      {
        "id": 50,
        "name": "Group Member 1",
        "username": "group_member_1",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
        "web_url": "http://localhost/group_member_1"
      }
    ],
    "approvals_required": 3,
    "source_rule": null,
    "users": [
      {
        "id": 5,
        "name": "John Doe",
        "username": "jdoe",
        "state": "active",
        "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
        "web_url": "http://localhost/jdoe"
      }
    ],
    "groups": [
      {
        "id": 5,
        "name": "group1",
        "path": "group1",
        "description": "",
        "visibility": "public",
        "lfs_enabled": false,
        "avatar_url": null,
        "web_url": "http://localhost/groups/group1",
        "request_access_enabled": false,
        "full_name": "group1",
        "full_path": "group1",
        "parent_id": null,
        "ldap_cn": null,
        "ldap_access": null
      }
    ],
    "contains_hidden_groups": false,
    "overridden": false
  }
]
```

### 获取单个合并请求级别规则

> 引入于极狐GitLab 14.10。

您可以使用以下端点请求有关单个合并请求批准规则的信息：

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/approval_rules/:approval_rule_id
```

**参数：**

| 参数                  | 类型                | 是否必需 | 描述                                                     |
|---------------------|-------------------|------|--------------------------------------------------------|
| `id`                | integer or string | **{check-circle}** Yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer           | **{check-circle}** Yes  | 合并请求 IID                                               |
| `approval_rule_id`  | integer           | **{check-circle}** Yes  | 批准规则 ID                            |

```json
{
  "id": 1,
  "name": "security",
  "rule_type": "regular",
  "eligible_approvers": [
    {
      "id": 5,
      "name": "John Doe",
      "username": "jdoe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/jdoe"
    },
    {
      "id": 50,
      "name": "Group Member 1",
      "username": "group_member_1",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/group_member_1"
    }
  ],
  "approvals_required": 3,
  "source_rule": null,
  "users": [
    {
      "id": 5,
      "name": "John Doe",
      "username": "jdoe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/jdoe"
    }
  ],
  "groups": [
    {
      "id": 5,
      "name": "group1",
      "path": "group1",
      "description": "",
      "visibility": "public",
      "lfs_enabled": false,
      "avatar_url": null,
      "web_url": "http://localhost/groups/group1",
      "request_access_enabled": false,
      "full_name": "group1",
      "full_path": "group1",
      "parent_id": null,
      "ldap_cn": null,
      "ldap_access": null
    }
  ],
  "contains_hidden_groups": false,
  "overridden": false

```

<a id="create-merge-request-level-rule"></a>

### 创建合并请求级别规则

> - 移动到极狐GitLab 专业版于 13.9。

您可以使用以下端点创建合并请求批准规则：

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/approval_rules
```

支持的参数：

| 参数                         | 类型                | 是否必需 | 描述                                                         |
|----------------------------|-------------------|------|------------------------------------------------------------|
| `id`                       | integer or string | **{check-circle}** Yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid`        | integer           | **{check-circle}** Yes  | 合并请求 IID                                                   |
| `name`                     | string            | **{check-circle}** Yes  | 批准规则名称                                                     |
| `approvals_required`       | integer           | **{check-circle}** Yes  | 此规则所需的批准数量                                                 |
| `approval_project_rule_id` | integer           | **{dotted-circle}** No   | 项目级别的批准规则 ID                                               |
| `user_ids`                 | Array             | **{dotted-circle}** No   | 作为批准者的用户的 ID                                               |
| `group_ids`                | Array             | **{dotted-circle}** No   | 作为批准者的群组的 ID                                               |
| `usernames`                | string array      | **{dotted-circle}** No   | 规则的用户名称                                                    |

**重要：** 如果设置 `approval_project_rule_id`，会复制项目级别规则的 `name`、`users` 和 `groups`。使用 `approvals_required` 指定的那个。

```json
{
  "id": 1,
  "name": "security",
  "rule_type": "regular",
  "eligible_approvers": [
    {
      "id": 2,
      "name": "John Doe",
      "username": "jdoe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/jdoe"
    },
    {
      "id": 50,
      "name": "Group Member 1",
      "username": "group_member_1",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/group_member_1"
    }
  ],
  "approvals_required": 1,
  "source_rule": null,
  "users": [
    {
      "id": 2,
      "name": "John Doe",
      "username": "jdoe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/jdoe"
    }
  ],
  "groups": [
    {
      "id": 5,
      "name": "group1",
      "path": "group1",
      "description": "",
      "visibility": "public",
      "lfs_enabled": false,
      "avatar_url": null,
      "web_url": "http://localhost/groups/group1",
      "request_access_enabled": false,
      "full_name": "group1",
      "full_path": "group1",
      "parent_id": null,
      "ldap_cn": null,
      "ldap_access": null
    }
  ],
  "contains_hidden_groups": false,
  "overridden": false
}
```

### 更新合并请求级别规则

> - 移动到极狐GitLab 专业版于 13.9。

您可以使用以下端点更新合并请求批准规则：

```plaintext
PUT /projects/:id/merge_requests/:merge_request_iid/approval_rules/:approval_rule_id
```

**重要：** 不在 `users`/`groups` 参数中的批准者和群组会被**移除**

**重要：** 不允许更新 `report_approver` 或 `code_owner` 规则。这些是系统生成的规则。

支持的参数：

| 参数                   | 类型                | 是否必需                   | 描述                                                         |
|----------------------|-------------------|------------------------|------------------------------------------------------------|
| `id`                 | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid`  | integer           | **{check-circle}** Yes | 合并请求 IID                                                   |
| `approval_rule_id`   | integer           | **{check-circle}** Yes | 批准规则 ID                                                    |
| `name`               | string            | **{check-circle}** No | 批准规则名称                                                     |
| `approvals_required` | integer           | **{check-circle}** No  | 此规则所需的批准的数量                                                |
| `user_ids`           | Array             | **{check-circle}** No  | 作为批准者的用户的 ID                                               |
| `group_ids`          | Array             | **{check-circle}** No  | 作为批准者的群组的 ID                                               |
| `remove_hidden_groups`     | boolean           | **{dotted-circle}** No | 隐藏群组是否应该移除                                                 |
| `usernames`  | string array      | **{dotted-circle}** No | 规则的用户名称                                                    |

```json
{
  "id": 1,
  "name": "security",
  "rule_type": "regular",
  "eligible_approvers": [
    {
      "id": 2,
      "name": "John Doe",
      "username": "jdoe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/jdoe"
    },
    {
      "id": 50,
      "name": "Group Member 1",
      "username": "group_member_1",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/group_member_1"
    }
  ],
  "approvals_required": 1,
  "source_rule": null,
  "users": [
    {
      "id": 2,
      "name": "John Doe",
      "username": "jdoe",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/0?s=80&d=identicon",
      "web_url": "http://localhost/jdoe"
    }
  ],
  "groups": [
    {
      "id": 5,
      "name": "group1",
      "path": "group1",
      "description": "",
      "visibility": "public",
      "lfs_enabled": false,
      "avatar_url": null,
      "web_url": "http://localhost/groups/group1",
      "request_access_enabled": false,
      "full_name": "group1",
      "full_path": "group1",
      "parent_id": null,
      "ldap_cn": null,
      "ldap_access": null
    }
  ],
  "contains_hidden_groups": false,
  "overridden": false
}
```

### 删除合并请求级别的规则

> - 移动到极狐GitLab 专业版于 13.9。

您可以使用以下端点删除合并请求批准规则：

```plaintext
DELETE /projects/:id/merge_requests/:merge_request_iid/approval_rules/:approval_rule_id
```

**重要：** 不允许删除 `report_approver` 或 `code_owner` 规则。这些是系统生成的规则。

支持的参数：

| 参数                  | 类型                | 是否必需 | 描述                                                    |
|---------------------|-------------------|------|-------------------------------------------------------|
| `id`                | integer or string | **{check-circle}** Yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer           | **{check-circle}** Yes  | 合并请求 IID                                              |
| `approval_rule_id`  | integer           | **{check-circle}** Yes  | 批准规则 ID                            |

## 批准合并请求

> 移动到极狐GitLab 专业版于 13.9。

如果有相关权限，您可以使用以下端点批准合并请求：

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/approve
```

支持的参数：

| 参数                  | 类型                | 是否必需                   | 描述                                                                                                                            |
|---------------------|-------------------|------------------------|-------------------------------------------------------------------------------------------------------------------------------|
| `id`                | integer or string | **{check-circle}** Yes | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                                                                         |
| `merge_request_iid` | integer           | **{check-circle}** Yes | 合并请求 IID                                                                                                                      |
| `sha`               | string            | **{check-circle}** no  | 合并请求的 `HEAD`                                                                                                                  |
| `approval_password` | string            | **{check-circle}** no  | 当前用户的密码。如果在项目设置中启用了[**需要用户密码以批准**](../user/project/merge_requests/approvals/settings.md#require-user-password-to-approve)，则必填 |

`sha` 参数与[接受合并请求](merge_requests.md#merge-a-merge-request)的工作方式相同：如果通过，则必须要与要添加的批准的合并请求的当前 HEAD 匹配。如果不匹配，则响应代码为 `409`。

```json
{
  "id": 5,
  "iid": 5,
  "project_id": 1,
  "title": "Approvals API",
  "description": "Test",
  "state": "opened",
  "created_at": "2016-06-08T00:19:52.638Z",
  "updated_at": "2016-06-09T21:32:14.105Z",
  "merge_status": "can_be_merged",
  "approvals_required": 2,
  "approvals_left": 0,
  "approved_by": [
    {
      "user": {
        "name": "Administrator",
        "username": "root",
        "id": 1,
        "state": "active",
        "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80\u0026d=identicon",
        "web_url": "http://localhost:3000/root"
      }
    },
    {
      "user": {
        "name": "Nico Cartwright",
        "username": "ryley",
        "id": 2,
        "state": "active",
        "avatar_url": "http://www.gravatar.com/avatar/cf7ad14b34162a76d593e3affca2adca?s=80\u0026d=identicon",
        "web_url": "http://localhost:3000/ryley"
      }
    }
  ]
}
```

## 取消批准合并请求

> 移动到极狐GitLab 专业版于 13.9。

如果您批准了合并请求，您可以使用以下端点取消批准：

```plaintext
POST /projects/:id/merge_requests/:merge_request_iid/unapprove
```

支持的参数：

| 参数                  | 类型                | 是否必需 | 描述                                                    |
|---------------------|-------------------|------|-------------------------------------------------------|
| `id`                | integer or string | **{check-circle}** Yes  | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer           | **{check-circle}** Yes  | 合并请求的 IID                          |

## 重置合并请求的批准

清除合并请求的所有批准。

仅适用于基于项目或群组令牌的[机器人用户](../user/project/settings/project_access_tokens.md#bot-users-for-projects)。没有机器人权限的用户会收到 `401 Unauthorized` 响应。

```plaintext
PUT /projects/:id/merge_requests/:merge_request_iid/reset_approvals
```

| 参数                  | 类型                | 是否必需                   | 描述                                                                     |
|---------------------|-------------------|------------------------|------------------------------------------------------------------------|
| `id`                | integer or string | **{check-circle}** Yes | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer           | **{check-circle}** Yes | 合并请求的内部 ID                                      |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/76/merge_requests/1/reset_approvals"
```
