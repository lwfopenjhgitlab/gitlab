---
stage: Plan
group: Knowledge
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目级别 WIKI API **(FREE)**

> - 在 14.9 版本中添加了 `encoding` 字段。
> - 在 14.9 版本中添加了 `render_html` 参数。
> - 在 14.9 版本中添加了 `version` 参数。

项目 [wikis](../user/project/wiki/index.md) API 仅在 APIv4 中可用。
[群组 wikis](group_wikis.md) API 也可用。


## 列出 wiki 页面

获取给定项目的所有 wiki 页面。

```plaintext
GET /projects/:id/wikis
```

| 参数 | 类型    | 是否必需 | 描述                                                      |
| --------- | ------- | -------- |---------------------------------------------------------|
| `id`      | integer/string    | 是      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `with_content`      | boolean    | 否      | 包括页面的内容                                                 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/wikis?with_content=1"
```

响应示例：

```json
[
  {
    "content" : "Here is an instruction how to deploy this project.",
    "format" : "markdown",
    "slug" : "deploy",
    "title" : "deploy",
    "encoding": "UTF-8"
  },
  {
    "content" : "Our development process is described here.",
    "format" : "markdown",
    "slug" : "development",
    "title" : "development",
    "encoding": "UTF-8"
  },{
    "content" : "*  [Deploy](deploy)\n*  [Development](development)",
    "format" : "markdown",
    "slug" : "home",
    "title" : "home",
    "encoding": "UTF-8"
  }
]
```

## 获取 wiki 页面

获取一个给定项目的 wiki 页面。

```plaintext
GET /projects/:id/wikis/:slug
```

| 参数 | 类型    | 是否必需 | 描述                                                      |
| --------- | ------- | -------- |---------------------------------------------------------|
| `id`      | integer/string    | 是      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `slug` | string  | 是       | wiki 页面的 URL 编码的 slug（唯一字符串），例如 `dir%2Fpage_name`       |
| `render_html`      | boolean    | 否      | 返回 wiki 页面的渲染 HTML                                      |
| `version`      | string    | 否      | wiki 页面版本 SHA                                           |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/wikis/home"
```

响应示例：

```json
{
  "content" : "home page",
  "format" : "markdown",
  "slug" : "home",
  "title" : "home",
  "encoding": "UTF-8"
}
```

## 建立一个新的 wiki 页面

使用给定的标题、slug 和内容为给定的存储库创建一个新的 wiki 页面。

```plaintext
POST /projects/:id/wikis
```

| 参数 | 类型    | 是否必需 | 描述                                                         |
| ------------- | ------- | -------- |------------------------------------------------------------|
| `id`      | integer/string    | 是      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)    |
| `content`       | string  | 是      | wiki 页面的内容                                                 |
| `title`        | string  | 是      | wiki 页面的标题                                                 |
| `format` | string  | 否       | wiki 页面的格式。 可用格式有：`markdown`（默认）、`rdoc`、`asciidoc` 和 `org` |

```shell
curl --data "format=rdoc&title=Hello&content=Hello world" \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/wikis"
```

响应示例：

```json
{
  "content" : "Hello world",
  "format" : "markdown",
  "slug" : "Hello",
  "title" : "Hello",
  "encoding": "UTF-8"
}
```

## 编辑一个已存在 wiki 页面

更新现有的 wiki 页面。 更新 wiki 页面至少需要一个参数。

```plaintext
PUT /projects/:id/wikis/:slug
```

| 参数 | 类型    | 是否必需 | 描述                                                         |
| --------------- | ------- | --------------------------------- |------------------------------------------------------------|
| `id`      | integer/string    | 是      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)    |
| `content`       | string  | 是 (如果没有提供 `title` )     | wiki 页面的内容                                                 |
| `title`        | string  | 是 (如果没提供 `content` )      | wiki 页面的标题                                                 |
| `format` | string  | 否       | wiki 页面的格式。 可用格式有：`markdown`（默认）、`rdoc`、`asciidoc` 和 `org` |
| `slug` | string  | 是       | wiki 页面的 URL 编码的 slug（唯一字符串），例如 `dir%2Fpage_name`          |

```shell
curl --request PUT --data "format=rdoc&content=documentation&title=Docs" \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/wikis/foo"
```

响应示例：

```json
{
  "content" : "documentation",
  "format" : "markdown",
  "slug" : "Docs",
  "title" : "Docs",
  "encoding": "UTF-8"
}
```

## 删除一个 wiki 页面

删除带有给定 slug 的 wiki 页面。

```plaintext
DELETE /projects/:id/wikis/:slug
```

| 参数 | 类型    | 是否必需 | 描述                                                      |
| --------- | ------- | -------- |---------------------------------------------------------|
| `id`      | integer/string    | 是      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `slug` | string  | 是       | wiki 页面的 URL 编码的 slug（唯一字符串），例如 `dir%2Fpage_name`       |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/wikis/foo"
```

成功后，HTTP 状态代码为`204`，并且不需要 JSON 响应。

## 将附件上传到 wiki 仓库

将文件上传到 wiki 仓库中的附件文件夹。附件文件夹名是 `upload` 。

```plaintext
POST /projects/:id/wikis/attachments
```

| 参数 | 类型    | 是否必需 | 描述                                                      |
| ------------- | ------- | -------- |---------------------------------------------------------|
| `id`      | integer/string    | 是      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `file` | string | 是 | 要上传的附件                                                  |
| `branch` | string | 否 | 分支的名称。 默认为 wiki 仓库默认分支                                  |

要从文件系统上传文件，请使用 `--form` 参数。 这样 cURL 将使用标头 `Content-Type: multipart/form-data` 发布数据。通过 `@`，`file=` 参数必须指向您的文件系统上的一个文件并且在前面。例如：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --form "file=@dk.png" "https://gitlab.example.com/api/v4/projects/1/wikis/attachments"
```

响应示例：

```json
{
  "file_name" : "dk.png",
  "file_path" : "uploads/6a061c4cf9f1c28cb22c384b4b8d4e3c/dk.png",
  "branch" : "master",
  "link" : {
    "url" : "uploads/6a061c4cf9f1c28cb22c384b4b8d4e3c/dk.png",
    "markdown" : "![dk](uploads/6a061c4cf9f1c28cb22c384b4b8d4e3c/dk.png)"
  }
}
```
