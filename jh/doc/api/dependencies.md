---
stage: Secure
group: Composition Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 依赖项 API **(ULTIMATE)**

WARNING:
此 API 当前处于实验<!--[实验](../policy/experiment-beta-support.md#experiment)-->阶段，可能不稳定。响应负载可能会受到极狐GitLab 版本发布带来的更改或者损坏的影响。

> - 在 14.4 版本中引入分页。

对该端点的每一次调用均需要身份验证。若要执行此调用，用户需要拥有读取仓库的权限。
若要查看响应中的漏洞，用户需要拥有读取[项目安全仪表盘](../user/application_security/security_dashboard/index.md)的权限。

<a id="list-project-dependencies"></a>

## 获取项目依赖项列表

获取项目依赖列表。此 API 部分镜像了依赖列表的功能。
只能为 Gemnasium 支持的[语言和包管理器](../user/application_security/dependency_scanning/index.md#supported-languages-and-package-managers)生成该列表。

```plaintext
GET /projects/:id/dependencies
GET /projects/:id/dependencies?package_manager=maven
GET /projects/:id/dependencies?package_manager=yarn,bundler
```

| 参数                | 类型             | 是否必需 | 描述                                                                                                                                             |
|-------------------|----------------|------|------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`              | integer/string | 是    | 项目 ID 或[项目的 URL 编码路径](rest/index.md#namespaced-path-encoding)。                                                                                     |
| `package_manager` | string array   | 否    | 返回属于指定包管理器的依赖项。有效值包括：`bundler`、`composer`、`conan`、`go`、`gradle`、`maven`、`npm`、`nuget`、`pip`、`pipenv`、`pnpm`、`yarn`、`sbt` 或 `setuptools`。 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/4/dependencies"
```

响应示例：

```json
[
  {
    "name": "rails",
    "version": "5.0.1",
    "package_manager": "bundler",
    "dependency_file_path": "Gemfile.lock",
    "vulnerabilities": [
      {
        "name": "DDoS",
        "severity": "unknown",
        "id": 144827,
        "url": "https://gitlab.example.com/group/project/-/security/vulnerabilities/144827"
      }
    ],
    "licenses": [
      {
        "name": "MIT",
        "url": "https://opensource.org/licenses/MIT"
      }
    ]
  },
  {
    "name": "hanami",
    "version": "1.3.1",
    "package_manager": "bundler",
    "dependency_file_path": "Gemfile.lock",
    "vulnerabilities": [],
    "licenses": [
      {
        "name": "MIT",
        "url": "https://opensource.org/licenses/MIT"
      }
    ]
  }
]
```

## 依赖分页

默认情况下，由于 API 返回的结果是分页的，`GET` 请求一次返回 20 个结果。

了解更多相关信息，请阅读[分页](rest/index.md#pagination)。
