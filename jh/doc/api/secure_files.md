---
stage: Verify
group: Pipeline Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference, api
---

# 项目级别安全文件 API **(FREE)**

> - 引入于极狐GitLab 14.8，[功能标志](../administration/feature_flags.md)为 `ci_secure_files`。默认禁用。
> - 普遍可用于极狐GitLab 15.7。移除 `ci_secure_files` 功能标志。

<!--
This feature is part of [Mobile DevOps](../ci/mobile_devops.md) developed by [GitLab Incubation Engineering](https://about.gitlab.com/handbook/engineering/incubation/).

The feature is still in development, but you can:

- [Request a feature](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/feedback/-/issues/new?issuable_template=feature_request).
- [Report a bug](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/feedback/-/issues/new?issuable_template=report_bug).
- [Share feedback](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/feedback/-/issues/new?issuable_template=general_feedback).
-->
您可以安全地存储多达 100 个文件作为安全文件在 CI/CD 流水线中使用。这些文件安全地存储在项目仓库之外，不受版本控制。在这些文件中存储敏感信息是安全的。安全文件支持纯文本和二进制文件类型，但必须不能超过 5 MB。

## 列出项目安全文件

获取项目中的安全文件列表。

```plaintext
GET /projects/:project_id/secure_files
```

支持的参数：

| 参数           | 类型             | 是否必需                   | 描述                                                            |
|--------------|----------------|------------------------|---------------------------------------------------------------|
| `project_id` | integer/string | **{check-circle}** Yes | 授权用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/secure_files"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "myfile.jks",
    "checksum": "16630b189ab34b2e3504f4758e1054d2e478deda510b2b08cc0ef38d12e80aac",
    "checksum_algorithm": "sha256",
    "created_at": "2022-02-22T22:22:22.222Z",
    "expires_at": null,
    "metadata": null
  },
  {
    "id": 2,
    "name": "myfile.cer",
    "checksum": "16630b189ab34b2e3504f4758e1054d2e478deda510b2b08cc0ef38d12e80aa2",
    "checksum_algorithm": "sha256",
    "created_at": "2022-02-22T22:22:22.222Z",
    "expires_at": "2023-09-21T14:55:59.000Z",
    "metadata": {
      "id":"75949910542696343243264405377658443914",
      "issuer": {
        "C":"US",
        "O":"Apple Inc.",
        "CN":"Apple Worldwide Developer Relations Certification Authority",
        "OU":"G3"
      },
      "subject": {
        "C":"US",
        "O":"Organization Name",
        "CN":"Apple Distribution: Organization Name (ABC123XYZ)",
        "OU":"ABC123XYZ",
        "UID":"ABC123XYZ"
      },
      "expires_at":"2023-09-21T14:55:59.000Z"
    }
  }
]
```

## 显示安全文件详细信息

获取项目中特定安全文件的详细信息。

```plaintext
GET /projects/:project_id/secure_files/:id
```

支持的参数：

| 参数           | 类型             | 是否必需                   | 描述                                                            |
|--------------|----------------|------------------------|---------------------------------------------------------------|
| `project_id` | integer/string | **{check-circle}** Yes | 授权用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `id`         | integer        | **{check-circle}** Yes | 安全文件的 `id`                               |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/secure_files/1"
```

响应示例：

```json
{
  "id": 1,
  "name": "myfile.jks",
  "checksum": "16630b189ab34b2e3504f4758e1054d2e478deda510b2b08cc0ef38d12e80aac",
  "checksum_algorithm": "sha256",
  "created_at": "2022-02-22T22:22:22.222Z",
  "expires_at": null,
  "metadata": null
}
```

## 创建安全文件

创建新的安全文件。

```plaintext
POST /projects/:project_id/secure_files
```

支持的参数：

| 参数           | 类型             | 是否必需                   | 描述                                                           |
|--------------|----------------|------------------------|--------------------------------------------------------------|
| `project_id` | integer/string | **{check-circle}** Yes | 授权用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `name`       | string         | **{check-circle}** Yes | 上传的文件的 `name`。文件名在项目中必须唯一                                    |
| `file`       | file           | **{check-circle}** Yes | 上传的 `file`（限制为 5 MB）                                         |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/1/secure_files"  --form "name=myfile.jks" --form "file=@/path/to/file/myfile.jks"
```

响应示例：

```json
{
  "id": 1,
  "name": "myfile.jks",
  "checksum": "16630b189ab34b2e3504f4758e1054d2e478deda510b2b08cc0ef38d12e80aac",
  "checksum_algorithm": "sha256",
  "created_at": "2022-02-22T22:22:22.222Z",
  "expires_at": null,
  "metadata": null
}
```

## 下载安全文件

下载项目安全文件的内容。

```plaintext
GET /projects/:project_id/secure_files/:id/download
```

支持的参数：

| 参数           | 类型             | 是否必需                   | 描述                                                            |
|--------------|----------------|------------------------|---------------------------------------------------------------|
| `project_id` | integer/string | **{check-circle}** Yes | 授权用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `id`         | integer        | **{check-circle}** Yes | 安全文件的 `id`                                 |

请求示例：

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" https://gitlab.example.com/api/v4/projects/1/secure_files/1/download --output myfile.jks
```

## 移除安全文件

移除项目的安全文件。

```plaintext
DELETE /projects/:project_id/secure_files/:id
```

支持的参数：

| 参数           | 类型             | 是否必需                   | 描述                                                            |
|--------------|----------------|------------------------|---------------------------------------------------------------|
| `project_id` | integer/string | **{check-circle}** Yes | 授权用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `id`         | integer        | **{check-circle}** Yes | 安全文件的 `id`                               |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/secure_files/1"
```
