---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 成员角色 API **(ULTIMATE)**

> - 引入于极狐GitLab 15.4。部署在[功能标志](../administration/feature_flags.md) `customizable_roles` 后，默认禁用。
> - 默认启用于极狐GitLab 15.9。

## 列出群组的所有成员角色

获取经过身份验证的用户可查看的群组成员角色列表。

```plaintext
GET /groups/:id/member_roles
```

| 参数   | 类型             | 是否必需 | 描述                                                                                                       |
|------|----------------|------|----------------------------------------------------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |

如果成功，则返回 [`200`](rest/index.md#status-codes) 及以下响应参数：

| 参数                     | 类型      | 描述            |
|:-----------------------|:--------|:--------------|
| `[].id`                | integer | 成员角色的 ID      |
| `[].group_id`          | integer | 成员角色所属的群组的 ID |
| `[].base_access_level` | integer | 成员角色的基本访问级别   |
| `[].read_code`         | boolean | 读取代码的权限       |

请求示例：

```shell
curl --header "Authorization: Bearer <your_access_token>" "https://gitlab.example.com/api/v4/groups/:id/member_roles"
```

响应示例：

```json
[
  {
    "id": 2,
    "group_id": 84,
    "base_access_level": 10,
    "read_code": true
  },
  {
    "id": 3,
    "group_id": 84,
    "base_access_level": 10,
    "read_code": false
  }
]
```

## 向群组添加成员角色

向群组添加成员角色

```plaintext
POST /groups/:id/member_roles
```

要向群组添加成员角色，该群组必须处于 root 级别（没有父群组）。

| 参数                  | 类型             | 是否必需 | 描述                                                                       |
|---------------------|----------------|------|--------------------------------------------------------------------------|
| `id`                | integer/string | yes  | 经过身份验证的用户拥有的群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `base_access_level` | integer        | yes  | 配置角色的基本访问级别                                                              |
| `read_code`         | boolean        | no   | 读取代码的权限                                                                  |

如果成功，则返回 [`201`](rest/index.md#status-codes) 及以下参数：

| 参数                  | 类型      | 描述            |
|:--------------------|:--------|:--------------|
| `id`                | integer | 成员角色的 ID      |
| `group_id`          | integer | 成员角色所属的群组的 ID |
| `base_access_level` | integer | 成员角色的基本访问级别   |
| `read_code`         | boolean | 读取代码的权限       |

请求示例：

```shell
 curl --request POST --header "Content-Type: application/json" --header "Authorization: Bearer $YOUR_ACCESS_TOKEN" --data '{"base_access_level" : 10, "read_code" : true}' "https://example.gitlab.com/api/v4/groups/:id/member_roles"
```

响应示例：

```json
{
  "id": 3,
  "group_id": 84,
  "base_access_level": 10,
  "read_code": true
}
```

### 移除群组的成员角色

移除群组的成员角色。

```plaintext
DELETE /groups/:id/member_roles/:member_role_id
```

| 参数               | 类型             | 是否必需 | 描述                                                                       |
|------------------|----------------|------|--------------------------------------------------------------------------|
| `id`             | integer/string | yes  | 经过身份验证的用户拥有的群组的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `member_role_id` | integer        | yes  | 成员角色的 ID                                              |

如果成功，则返回 [`204`](rest/index.md#status-codes) 及空响应。

请求示例：

```shell
curl --request DELETE --header "Content-Type: application/json" --header "Authorization: Bearer $YOUR_ACCESS_TOKEN" "https://example.gitlab.com/api/v4/groups/:group_id/member_roles/:member_role_id"
```
