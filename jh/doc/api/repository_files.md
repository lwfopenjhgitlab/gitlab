---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
type: reference, api
---

# 仓库文件 API **(FREE)**

您可以使用此 API 获取、创建、更新和删除仓库中的文件。
您还可以为此 API [配置速率限制](../user/admin_area/settings/files_api_rate_limits.md)。

## 私人访问令牌的可用范围

下表描述了使用[个人访问令牌](../user/profile/personal_access_tokens.md)的不同的可用范围。

| 范围                | 描述          |
|-------------------|-------------|
| `read_repository` | 允许仓库文件的阅读权限 |
| `api`             | 允许仓库文件的读写权限 |

<a id="get-file-from-repository"></a>

## 从仓库获取文件

> 响应中的 `execute_filemode` 字段引入于极狐GitLab 14.10。

允许您接收仓库中文件的信息，例如名称、大小和内容。文件内容采用 Base64 编码。如果仓库可公开访问，则可以无需身份验证访问此端点。

```plaintext
GET /projects/:id/repository/files/:file_path
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/13083/repository/files/app%2Fmodels%2Fkey%2Erb?ref=master"
```

| 参数          | 类型                | 是否必需 | 描述                                                           |
|-------------|-------------------|------|--------------------------------------------------------------|
| `id`        | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `file_path` | string            | yes  | URL 编码的新文件的完整路径。例如：`lib%2Fclass%2Erb`                        |
| `ref`       | string            | yes  | 分支、标签或提交的名称                                      |

响应示例：

```json
{
  "file_name": "key.rb",
  "file_path": "app/models/key.rb",
  "size": 1476,
  "encoding": "base64",
  "content": "IyA9PSBTY2hlbWEgSW5mb3...",
  "content_sha256": "4c294617b60715c1d218e61164a3abd4808a4284cbc30e6728a01ad9aada4481",
  "ref": "master",
  "blob_id": "79f7bbd25901e8334750839545a9bd021f0e4c83",
  "commit_id": "d5a3ff139356ce33e37e73add446f16869741b50",
  "last_commit_id": "570e7b2abdd848b95f2f578043fc23bd6f6fd24d",
  "execute_filemode": false
}
```

NOTE:
`blob_id` 是 Blob SHA，请参见[仓库-从仓库获取 Blob](repositories.md#get-a-blob-from-repository)。

除 `GET` 方法外，您还可以使用 `HEAD` 仅获取文件元数据。

```plaintext
HEAD /projects/:id/repository/files/:file_path
```

```shell
curl --head --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/13083/repository/files/app%2Fmodels%2Fkey%2Erb?ref=master"
```

响应示例：

```plaintext
HTTP/1.1 200 OK
...
X-Gitlab-Blob-Id: 79f7bbd25901e8334750839545a9bd021f0e4c83
X-Gitlab-Commit-Id: d5a3ff139356ce33e37e73add446f16869741b50
X-Gitlab-Content-Sha256: 4c294617b60715c1d218e61164a3abd4808a4284cbc30e6728a01ad9aada4481
X-Gitlab-Encoding: base64
X-Gitlab-File-Name: key.rb
X-Gitlab-File-Path: app/models/key.rb
X-Gitlab-Last-Commit-Id: 570e7b2abdd848b95f2f578043fc23bd6f6fd24d
X-Gitlab-Ref: master
X-Gitlab-Size: 1476
X-Gitlab-Execute-Filemode: false
...
```

## 从仓库获取文件 Blame 

允许您接收 Blame 信息。每个 Blame 范围都包含行和对应的提交信息。

```plaintext
GET /projects/:id/repository/files/:file_path/blame
```

| 参数             | 类型                | 是否必需 | 描述                                                           |
|----------------|-------------------|------|--------------------------------------------------------------|
| `id`           | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `file_path`    | string            | yes  | URL 编码的新文件的完整路径。例如：`lib%2Fclass%2Erb`                        |
| `ref`          | string            | yes  | 分支、标签或提交的名称                                                  |
| `range`        | hash              | no   | Blame 范围                                                     |
| `range[start]` | integer           | yes  | 要 Blame 的范围的第一行                                              |
| `range[end]`   | integer           | yes  | 要 Blame 的范围的最后一行                                             |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/13083/repository/files/path%2Fto%2Ffile.rb/blame?ref=master"
```

响应示例：

```json
[
  {
    "commit": {
      "id": "d42409d56517157c48bf3bd97d3f75974dde19fb",
      "message": "Add feature\n\nalso fix bug\n",
      "parent_ids": [
        "cc6e14f9328fa6d7b5a0d3c30dc2002a3f2a3822"
      ],
      "authored_date": "2015-12-18T08:12:22.000Z",
      "author_name": "John Doe",
      "author_email": "john.doe@example.com",
      "committed_date": "2015-12-18T08:12:22.000Z",
      "committer_name": "John Doe",
      "committer_email": "john.doe@example.com"
    },
    "lines": [
      "require 'fileutils'",
      "require 'open3'",
      ""
    ]
  },
  ...
]
```

NOTE:
`HEAD` 方法像[从仓库获取文件](repository_files.md#get-file-from-repository)中一样，仅返回文件元数据。

```shell
curl --head --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/13083/repository/files/path%2Fto%2Ffile.rb/blame?ref=master"
```

响应示例：

```plaintext
HTTP/1.1 200 OK
...
X-Gitlab-Blob-Id: 79f7bbd25901e8334750839545a9bd021f0e4c83
X-Gitlab-Commit-Id: d5a3ff139356ce33e37e73add446f16869741b50
X-Gitlab-Content-Sha256: 4c294617b60715c1d218e61164a3abd4808a4284cbc30e6728a01ad9aada4481
X-Gitlab-Encoding: base64
X-Gitlab-File-Name: file.rb
X-Gitlab-File-Path: path/to/file.rb
X-Gitlab-Last-Commit-Id: 570e7b2abdd848b95f2f578043fc23bd6f6fd24d
X-Gitlab-Ref: master
X-Gitlab-Size: 1476
X-Gitlab-Execute-Filemode: false
...
```

### 示例

要获取 Blame 范围，请使用文件的起始和结束行号码指定 `range[start]` 和 `range[end]` 参数。

```shell
curl --head --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/13083/repository/files/path%2Fto%2Ffile.rb/blame?ref=master&range[start]=1&range[end]=2"
```

响应示例：

```json
[
  {
    "commit": {
      "id": "d42409d56517157c48bf3bd97d3f75974dde19fb",
      "message": "Add feature\n\nalso fix bug\n",
      "parent_ids": [
        "cc6e14f9328fa6d7b5a0d3c30dc2002a3f2a3822"
      ],
      "authored_date": "2015-12-18T08:12:22.000Z",
      "author_name": "John Doe",
      "author_email": "john.doe@example.com",
      "committed_date": "2015-12-18T08:12:22.000Z",
      "committer_name": "John Doe",
      "committer_email": "john.doe@example.com"
    },
    "lines": [
      "require 'fileutils'",
      "require 'open3'"
    ]
  },
  ...
]
```

## 从仓库获取原始文件

```plaintext
GET /projects/:id/repository/files/:file_path/raw
```

| 参数          | 类型                | 是否必需 | 描述                                                           |
|-------------|-------------------|------|--------------------------------------------------------------|
| `id`        | integer or string | yes  | 经过身份验证的用户拥有项目的 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `file_path` | string            | yes  | URL 编码的新文件的完整路径。例如：`lib%2Fclass%2Erb`                        |
| `ref`       | string            | yes  | 分支、标签或提交的名称。默认为项目的 `HEAD`                     |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/13083/repository/files/app%2Fmodels%2Fkey%2Erb/raw?ref=master"
```

NOTE:
类似[从仓库获取文件](repository_files.md#get-file-from-repository)，您可以使用 `HEAD` 仅获取文件元数据。

## 在仓库中创建新文件

> `execute_filemode` 参数引入于极狐GitLab 14.10。

允许您创建单个文件。关于使用单个请求创建多个文件的更多内容，请参见[提交 API](commits.md#create-a-commit-with-multiple-files-and-actions)。

```plaintext
POST /projects/:id/repository/files/:file_path
```

| 参数                 | 类型                | 是否必需 | 描述                                                         |
|--------------------|-------------------|------|------------------------------------------------------------|
| `id`               | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `file_path`        | string            | yes  | URL 编码的新文件的完整路径。例如：`lib%2Fclass%2Erb`                      |
| `branch`           | string            | yes  | 要创建的新分支的名称。提交添加到了此分支                                       |
| `start_branch`     | string            | no   | 从其中创建新分支的基础分支的名称                                           |
| `encoding`         | string            | no   | 将编码变更为 `base64`。默认为 `text`                                 |
| `author_email`     | string            | no   | 提交作者的电子邮件地址                                                |
| `author_name`      | string            | no   | 提交作者的名字                                                    |
| `content`          | string            | yes  | 文件内容                                                       |
| `commit_message`   | string            | yes  | 提交信息                                                       |
| `execute_filemode` | boolean           | no   | 在文件上启用或禁用 `execute` 标记，可以是 `true` 或 `false`                |

```shell
curl --request POST --header 'PRIVATE-TOKEN: <your_access_token>' \
     --header "Content-Type: application/json" \
     --data '{"branch": "master", "author_email": "author@example.com", "author_name": "Firstname Lastname",
               "content": "some content", "commit_message": "create a new file"}' \
     "https://gitlab.example.com/api/v4/projects/13083/repository/files/app%2Fproject%2Erb"
```

响应示例：

```json
{
  "file_path": "app/project.rb",
  "branch": "master"
}
```

## 在仓库中更新现存文件

> `execute_filemode` 参数引入于极狐GitLab 14.10。

允许您创建单个文件。关于使用单个请求更新多个文件的更多内容，请参见[提交 API](commits.md#create-a-commit-with-multiple-files-and-actions)。

```plaintext
PUT /projects/:id/repository/files/:file_path
```

| 参数                 | 类型                | 是否必需 | 描述                                                          |
|--------------------|-------------------|------|-------------------------------------------------------------|
| `id`               | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `file_path`        | string            | yes  | URL 编码的新文件的完整路径。例如：`lib%2Fclass%2Erb`                       |
| `branch`           | string            | yes  | 要创建的新分支的名称。提交被添加到此分支                                        |
| `start_branch`     | string            | no   | 从其中创建新分支的基础分支的名称                                            |
| `encoding`         | string            | no   | 将编码变更为 `base64`。默认为 `text`                                  |
| `author_email`     | string            | no   | 提交作者的电子邮件地址                                                 |
| `author_name`      | string            | no   | 提交作者的名字                                                     |
| `content`          | string            | yes  | 文件内容                                                        |
| `commit_message`   | string            | yes  | 提交信息                                                        |
| `last_commit_id`   | string            | no   | 上一个已知的文件提交 ID                                               |
| `execute_filemode` | boolean           | no   | 在文件上启用或禁用 `execute` 标记。可以是 `true` 或 `false`                 |

```shell
curl --request PUT --header 'PRIVATE-TOKEN: <your_access_token>' \
     --header "Content-Type: application/json" \
     --data '{"branch": "master", "author_email": "author@example.com", "author_name": "Firstname Lastname",
       "content": "some content", "commit_message": "update file"}' \
     "https://gitlab.example.com/api/v4/projects/13083/repository/files/app%2Fproject%2Erb"
```

响应示例：

```json
{
  "file_path": "app/project.rb",
  "branch": "master"
}
```

如果提交因任何原因失败，我们将返回 400 错误，并带有非特定的错误信息。提交失败的可能原因包括：

- 包含 `file_path` 的 `/../` （尝试目录遍历）
- 新文件内容与当前文件内容相同。也就是说，用户试图进行空提交
- 在文件编辑过程中，通过 Git 推送更新了分支

极狐GitLab Shell 有布尔返回代码，防止极狐GitLab 指定错误。

## 在仓库中删除现存文件

允许您删除单个文件。关于使用单个请求删除多个文件的更多内容，请参见[提交 API](commits.md#create-a-commit-with-multiple-files-and-actions)。

```plaintext
DELETE /projects/:id/repository/files/:file_path
```

| 参数               | 类型                | 是否必需 | 描述                                                           |
|------------------|-------------------|------|--------------------------------------------------------------|
| `id`             | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](index.md#namespaced-path-encoding) |
| `file_path`      | string            | yes  | URL 编码的新文件的完整路径。例如：`lib%2Fclass%2Erb`                        |
| `branch`         | string            | yes  | 要创建的新分支的名称。提交添加到此分支                                          |
| `start_branch`   | string            | no   | 从中创建新分支的基础分支的名称                                              |
| `author_email`   | string            | no   | 提交作者的电子邮件地址                                                  |
| `author_name`    | string            | no   | 提交作者的名字                                                      |
| `commit_message` | string            | yes  | 提交信息                                                         |
| `last_commit_id` | string            | no   | 上一个已知的文件提交 ID                                                |

```shell
curl --request DELETE --header 'PRIVATE-TOKEN: <your_access_token>' \
     --header "Content-Type: application/json" \
     --data '{"branch": "master", "author_email": "author@example.com", "author_name": "Firstname Lastname",
       "commit_message": "delete file"}' \
     "https://gitlab.example.com/api/v4/projects/13083/repository/files/app%2Fproject%2Erb"
```
