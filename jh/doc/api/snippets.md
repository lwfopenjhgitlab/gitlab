---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 代码片段 API **(FREE)**

操作在[代码片段](../user/snippets.md)上的代码片段 API。存在[项目代码片段](project_snippets.md)和[在存储间移动代码片段](snippet_repository_storage_moves.md)的相关 API。

<a id="snippet-visibility-level"></a>

## 代码片段可见性级别

极狐GitLab 中的代码片段可以是私有的、内部的或公共的。
您可以使用代码段中的 `visibility` 字段进行设置。

代码片段可见性级别的有效值为：

| 可见性        | 描述                                                                 |
|:-----------|:-------------------------------------------------------------------|
| `private`  | 代码片段仅对代码片段创建者可见                                                    |
| `internal` | 代码片段对[外部用户](../user/admin_area/external_users.md)之外的任意经过身份验证的用户可见 |
| `public`   | 代码片段可以不经授权访问                                                       |

## 列出用户的所有代码片段

获取当前用户的代码片段列表。

```plaintext
GET /snippets
```

参数：

| 参数               | 类型       | 是否必需 | 描述                                                   |
|------------------|----------|------|------------------------------------------------------|
| `per_page`       | integer  | no   | 每页返回的代码片段的数量                                         |
| `page`           | integer  | no   | 要检索的页面                                               |
| `created_after`  | datetime | no   | 返回给定时间后创建的代码片段。预计格式为 ISO 8601（`2019-03-15T08:00:00Z`） |
| `created_before` | datetime | no   | 返回给定时间前创建的代码片段。预计格式为 ISO 8601（`2019-03-15T08:00:00Z`） |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/snippets"
```

响应示例：

```json
[
    {
        "id": 42,
        "title": "Voluptatem iure ut qui aut et consequatur quaerat.",
        "file_name": "mclaughlin.rb",
        "description": null,
        "visibility": "internal",
        "author": {
            "id": 22,
            "name": "User 0",
            "username": "user0",
            "state": "active",
            "avatar_url": "https://www.gravatar.com/avatar/52e4ce24a915fb7e51e1ad3b57f4b00a?s=80&d=identicon",
            "web_url": "http://example.com/user0"
        },
        "updated_at": "2018-09-18T01:12:26.383Z",
        "created_at": "2018-09-18T01:12:26.383Z",
        "project_id": null,
        "web_url": "http://example.com/snippets/42",
        "raw_url": "http://example.com/snippets/42/raw"
    },
    {
        "id": 41,
        "title": "Ut praesentium non et atque.",
        "file_name": "ondrickaemard.rb",
        "description": null,
        "visibility": "internal",
        "author": {
            "id": 22,
            "name": "User 0",
            "username": "user0",
            "state": "active",
            "avatar_url": "https://www.gravatar.com/avatar/52e4ce24a915fb7e51e1ad3b57f4b00a?s=80&d=identicon",
            "web_url": "http://example.com/user0"
        },
        "updated_at": "2018-09-18T01:12:26.360Z",
        "created_at": "2018-09-18T01:12:26.360Z",
        "project_id": 1,
        "web_url": "http://example.com/gitlab-org/gitlab-test/snippets/41",
        "raw_url": "http://example.com/gitlab-org/gitlab-test/snippets/41/raw"
    }
]
```

## 获取单个代码片段

获取单个代码片段。

```plaintext
GET /snippets/:id
```

参数：

| 参数   | 类型      | 是否必需 | 描述                                   |
|:-----|:--------|:-----|:-------------------------------------|
| `id` | integer | yes  | 要检索的代码片段的 ID                         |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/snippets/1"
```

响应示例：

```json
{
  "id": 1,
  "title": "test",
  "file_name": "add.rb",
  "description": "Ruby test snippet",
  "visibility": "private",
  "author": {
    "id": 1,
    "username": "john_smith",
    "email": "john@example.com",
    "name": "John Smith",
    "state": "active",
    "created_at": "2012-05-23T08:00:58Z"
  },
  "expires_at": null,
  "updated_at": "2012-06-28T10:52:04Z",
  "created_at": "2012-06-28T10:52:04Z",
  "project_id": null,
  "web_url": "http://example.com/snippets/1",
  "raw_url": "http://example.com/snippets/1/raw"
}
```

## 单个代码片段内容

获取单个代码片段的原始内容。

```plaintext
GET /snippets/:id/raw
```

参数：

| 参数   | 类型      | 是否必需 | 描述                                   |
|:-----|:--------|:-----|:-------------------------------------|
| `id` | integer | yes  | 要检索的代码片段的 ID |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/snippets/1/raw"
```

响应示例：

```plaintext
Hello World snippet
```

## 代码片段仓库文件内容

将原始文件内容返回为纯文本。

```plaintext
GET /snippets/:id/files/:ref/:file_path/raw
```

参数：

| 参数          | 类型      | 是否必需 | 描述           |
|:------------|:--------|:-----|:-------------|
| `id`        | integer | yes  | 要检索的代码片段的 ID |
| `ref`       | string  | yes  | 对标签、分支或提交的引用 |
| `file_path` | string  | yes  | URL 编码的文件路径  |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/snippets/1/files/master/snippet%2Erb/raw"
```

响应示例：

```plaintext
Hello World snippet
```

## 创建新的代码片段

创建新的代码片段。

NOTE:
用户必须有权限创建新的代码片段。

```plaintext
POST /snippets
```

参数：

| 参数                | 类型              | 是否必需 | 描述                                    |
|:------------------|:----------------|:-----|:--------------------------------------|
| `title`           | string          | yes  | 代码片段标题                                |
| `file_name`       | string          | no   | 已废弃：使用 `files` 代替。代码片段文件的名称           |
| `content`         | string          | no   | 已废弃：使用 `files` 代替。代码片段的内容             |
| `description`     | string          | no   | 代码片段描述                                |
| `visibility`      | string          | no   | 代码片段的[可见性](#snippet-visibility-level) |
| `files`           | array of hashes | no   | 代码片段文件的数组                             |
| `files:file_path` | string          | yes  | 代码片段文件的路径                             |
| `files:content`   | string          | yes  | 代码片段文件内容                              |

请求示例：

```shell
curl --request POST "https://gitlab.example.com/api/v4/snippets" \
     --header 'Content-Type: application/json' \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     -d @snippet.json
```

上面请求示例中使用的 `snippet.json`：

```json
{
  "title": "This is a snippet",
  "description": "Hello World snippet",
  "visibility": "internal",
  "files": [
    {
      "content": "Hello world",
      "file_path": "test.txt"
    }
  ]
}
```

响应示例：

```json
{
  "id": 1,
  "title": "This is a snippet",
  "description": "Hello World snippet",
  "visibility": "internal",
  "author": {
    "id": 1,
    "username": "john_smith",
    "email": "john@example.com",
    "name": "John Smith",
    "state": "active",
    "created_at": "2012-05-23T08:00:58Z"
  },
  "expires_at": null,
  "updated_at": "2012-06-28T10:52:04Z",
  "created_at": "2012-06-28T10:52:04Z",
  "project_id": null,
  "web_url": "http://example.com/snippets/1",
  "raw_url": "http://example.com/snippets/1/raw",
  "ssh_url_to_repo": "ssh://git@gitlab.example.com:snippets/1.git",
  "http_url_to_repo": "https://gitlab.example.com/snippets/1.git",
  "file_name": "test.txt",
  "files": [
    {
      "path": "text.txt",
      "raw_url": "https://gitlab.example.com/-/snippets/1/raw/master/renamed.md"
    }
  ]
}
```

## 更新代码片段

更新现存的代码片段。

NOTE:
用户必须有权限更新现存的代码片段。

```plaintext
PUT /snippets/:id
```

参数：

| 参数                    | 类型              | 是否必需      | 描述                                             |
|:----------------------|:----------------|:----------|:-----------------------------------------------|
| `id`                  | integer         | yes       | 要更新的代码片段的 ID                                   |
| `title`               | string          | no        | 代码片段标题                                         |
| `file_name`           | string          | no        | 已废弃：使用 `files` 代替。代码片段文件的名称                    |
| `content`             | string          | no        | 已废弃：使用 `files` 代替。代码片段文件的内容                    |
| `description`         | string          | no        | 代码片段的描述                                        |
| `visibility`          | string          | no        | 代码片段的[可见性](#snippet-visibility-level)          |
| `files`               | array of hashes | sometimes | 代码片段文件的数组。使用多个文件更新代码片段时必需                      |
| `files:action`        | string          | yes       | 对文件进行的操作类型：`create`、`update`、`delete` 或 `move` |
| `files:file_path`     | string          | no        | 代码片段文件的路径                                      |
| `files:previous_path` | string          | no        | 代码片段文件的以前路径                                    |
| `files:content`       | string          | no        | 代码片段文件的内容                                      |

请求示例：

```shell
curl --request PUT "https://gitlab.example.com/api/v4/snippets/1" \
     --header 'Content-Type: application/json' \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     -d @snippet.json
```

上面请求示例中使用的 `snippet.json`：

```json
{
  "title": "foo",
  "files": [
    {
      "action": "move",
      "previous_path": "test.txt",
      "file_path": "renamed.md"
    }
  ]
}
```

响应示例：

```json
{
  "id": 1,
  "title": "test",
  "description": "description of snippet",
  "visibility": "internal",
  "author": {
    "id": 1,
    "username": "john_smith",
    "email": "john@example.com",
    "name": "John Smith",
    "state": "active",
    "created_at": "2012-05-23T08:00:58Z"
  },
  "expires_at": null,
  "updated_at": "2012-06-28T10:52:04Z",
  "created_at": "2012-06-28T10:52:04Z",
  "project_id": null,
  "web_url": "http://example.com/snippets/1",
  "raw_url": "http://example.com/snippets/1/raw",
  "ssh_url_to_repo": "ssh://git@gitlab.example.com:snippets/1.git",
  "http_url_to_repo": "https://gitlab.example.com/snippets/1.git",
  "file_name": "renamed.md",
  "files": [
    {
      "path": "renamed.md",
      "raw_url": "https://gitlab.example.com/-/snippets/1/raw/master/renamed.md"
    }
  ]
}
```

## 删除代码片段

删除现存的代码片段。

```plaintext
DELETE /snippets/:id
```

参数：

| 参数   | 类型      | 是否必需 | 描述                                 |
|:-----|:--------|:-----|:-----------------------------------|
| `id` | integer | yes  | 要删除的代码片段的 ID |

请求示例：

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/snippets/1"
```

以下是可能的返回代码：

| 代码    | 描述         |
|:------|:-----------|
| `204` | 删除成功，未返回数据 |
| `404` | 未发现代码片段    |

## 列出所有公共的代码片段

列出所有公共的代码片段。

```plaintext
GET /snippets/public
```

参数：

| 参数               | 类型       | 是否必需 | 描述                                                    |
|------------------|----------|------|-------------------------------------------------------|
| `per_page`       | integer  | no   | 每页返回的代码片段的数量                                          |
| `page`           | integer  | no   | 要检索的页面                                                |
| `created_after`  | datetime | no   | 返回给定时间后创建的代码片段。预计格式为 ISO 8601（`2019-03-15T08:00:00Z`） |
| `created_before` | datetime | no   | 返回给定时间前创建的代码片段。预计格式为 ISO 8601（`2019-03-15T08:00:00Z`） |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/snippets/public?per_page=2&page=1"
```

响应示例：

```json
[
    {
        "author": {
            "avatar_url": "http://www.gravatar.com/avatar/edaf55a9e363ea263e3b981d09e0f7f7?s=80&d=identicon",
            "id": 12,
            "name": "Libby Rolfson",
            "state": "active",
            "username": "elton_wehner",
            "web_url": "http://example.com/elton_wehner"
        },
        "created_at": "2016-11-25T16:53:34.504Z",
        "file_name": "oconnerrice.rb",
        "id": 49,
        "title": "Ratione cupiditate et laborum temporibus.",
        "updated_at": "2016-11-25T16:53:34.504Z",
        "project_id": null,
        "web_url": "http://example.com/snippets/49",
        "raw_url": "http://example.com/snippets/49/raw"
    },
    {
        "author": {
            "avatar_url": "http://www.gravatar.com/avatar/36583b28626de71061e6e5a77972c3bd?s=80&d=identicon",
            "id": 16,
            "name": "Llewellyn Flatley",
            "state": "active",
            "username": "adaline",
            "web_url": "http://example.com/adaline"
        },
        "created_at": "2016-11-25T16:53:34.479Z",
        "file_name": "muellershields.rb",
        "id": 48,
        "title": "Minus similique nesciunt vel fugiat qui ullam sunt.",
        "updated_at": "2016-11-25T16:53:34.479Z",
        "project_id": null,
        "web_url": "http://example.com/snippets/48",
        "raw_url": "http://example.com/snippets/49/raw",
        "visibility": "public"
    }
]
```

## 获取用户代理详细信息

NOTE:
仅对管理员可用。

```plaintext
GET /snippets/:id/user_agent_detail
```

| 参数   | 类型      | 是否必需 | 描述                  |
|:-----|:--------|:-----|:--------------------|
| `id` | integer | yes  | 代码片段 ID |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/snippets/1/user_agent_detail"
```

响应示例：

```json
{
  "user_agent": "AppleWebKit/537.36",
  "ip_address": "127.0.0.1",
  "akismet_submitted": false
}
```
