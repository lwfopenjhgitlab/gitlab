---
stage: Verify
group: Pipeline Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 作业产物 API **(FREE)**

<a id="get-job-artifacts"></a>

## 获取作业产物

> 在[极狐GitLab 专业版](https://about.gitlab.cn/pricing/) 9.5 中引入 `CI_JOB_TOKEN` 在产物下载 API 中的使用。

获取项目的作业产物的压缩存档文件。

```plaintext
GET /projects/:id/jobs/:job_id/artifacts
```

| 参数   | 类型           | 是否必需 | 描述                                                                                                                                                                                                 |
|-------------|----------------|----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`        | 整型/字符串 | 是      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。                                                                                                                               |
| `job_id`    | 整型        | 是      | 作业 ID。                                                                                                                                                                                             |
| `job_token` **(PREMIUM)** | 字符串 | 否 | 与多项目流水线的[触发器](../ci/jobs/ci_job_token.md#download-an-artifact-from-a-different-pipeline)一起使用。它应该只在 `.gitlab-ci.yml` 文件定义的 CI/CD 作业进行调用。它的值始终是 `$CI_JOB_TOKEN`。当使用此令牌时，与 `$CI_JOB_TOKEN` 相关的作业必须运行。 |

使用带有 `PRIVATE-TOKEN` 的标头请求示例：

```shell
curl --location --output artifacts.zip --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/jobs/42/artifacts"
```

在 `.gitlab-ci.yml` 里面使用 [`script` 定义](../ci/yaml/index.md#script)**(PREMIUM)**，您可以使用：

- 带有极狐GitLab 提供的 `CI_JOB_TOKEN` 变量的 `JOB-TOKEN` 标头。
  示例，以下作业下载 ID 为 42 的作业的产物。该命令用单引号括起来，因为它包含一个冒号 (`:`)：

  ```yaml
  artifact_download:
    stage: test
    script:
      - 'curl --location --output artifacts.zip --header "JOB-TOKEN: $CI_JOB_TOKEN" "https://gitlab.example.com/api/v4/projects/1/jobs/42/artifacts"'
  ```

- 或者带有极狐GitLab 提供的 `CI_JOB_TOKEN` 变量的 `job_token` 属性。
  示例，以下作业下载 ID 为 42 的作业的产物。

  ```yaml
  artifact_download:
    stage: test
    script:
      - 'curl --location --output artifacts.zip "https://gitlab.example.com/api/v4/projects/1/jobs/42/artifacts?job_token=$CI_JOB_TOKEN"'
  ```

可能的响应状态代码：

| 状态    | 描述                     |
|-----------|---------------------------------|
| 200       | 返回产物文件。      |
| 404       | 未找到构建或没有产物。|

## 下载产物归档文件

> 在[极狐GitLab 专业版](https://about.gitlab.cn/pricing/) 9.5 中引入 `CI_JOB_TOKEN` 在产物下载 API 中的使用。

从给定参考名称和作业的最新**成功**流水线下载产物归档文件，前提是作业成功完成。这与[获取作业的产物](#get-job-artifacts)相同，但通过定义作业的名称而不是其 ID。

NOTE:
如果一个流水线是其他[子流水线的父级](../ci/pipelines/downstream_pipelines.md#parent-child-pipelines)，则按从父级到子级的层次顺序搜索产物。
例如，如果父级流水线和子流水线都有同名的作业，则返回父级流水线中的产物。

```plaintext
GET /projects/:id/jobs/artifacts/:ref_name/download?job=name
```

参数：

| 参数   | 类型           | 是否必需 | 描述                                                                                                                                                                                                  |
|-------------|----------------|----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`        | 整型/字符串 | 是      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。                                                                                                                                |
| `ref_name`  | 字符串         | 是      | 仓库中分支或者标签名称。不支持 HEAD 或 SHA 引用。                                                                                                                                                                      |
| `job`       | 字符串         | 是      | 作业名称。                                                                                                                                                                                               |
| `job_token` **(PREMIUM)** | 字符串 | 否 | 与多项目流水线的[触发器](../ci/jobs/ci_job_token.md#download-an-artifact-from-a-different-pipeline)一起使用。它应该只在 `.gitlab-ci.yml` 文件定义的 CI/CD 作业进行调用。它的值始终是 `$CI_JOB_TOKEN`。当使用这个令牌时，与 `$CI_JOB_TOKEN` 相关的作业必须运行。 |

使用 `PRIVATE-TOKEN` 标头的请求示例：

```shell
curl --location --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/jobs/artifacts/main/download?job=test"
```

在 `.gitlab-ci.yml` 里面使用 [script 定义](../ci/yaml/index.md#script)**(PREMIUM)**，您可以使用：

- 带有极狐GitLab 提供的 `CI_JOB_TOKEN` 变量的 `JOB-TOKEN` 标头。
  示例，以下 test 作业下载的是 main 分支的产物。该命令用单引号括起来，因为它包含一个冒号 (`:`)：

  ```yaml
  artifact_download:
    stage: test
    script:
      - 'curl --location --output artifacts.zip --header "JOB-TOKEN: $CI_JOB_TOKEN" "https://gitlab.example.com/api/v4/projects/$CI_PROJECT_ID/jobs/artifacts/main/download?job=test"'
  ```

- 或者带有极狐GitLab 提供的 `CI_JOB_TOKEN` 变量的 `job_token` 属性。
  示例，以下 test 作业下载的是 main 分支的产物。

  ```yaml
  artifact_download:
    stage: test
    script:
      - 'curl --location --output artifacts.zip "https://gitlab.example.com/api/v4/projects/$CI_PROJECT_ID/jobs/artifacts/main/download?job=test&job_token=$CI_JOB_TOKEN"'
  ```

可能的响应状态代码：

| 状态    | 描述                     |
|-----------|---------------------------------|
| 200       | 返回产物文件。      |
| 404       | 未找到构建或没有产物。|

## 按作业 ID 下载单个产物文件

> - 引入于 13.10 版本。
> - 在[极狐GitLab 专业版](https://about.gitlab.cn/pricing/) 13.10 中引入 `CI_JOB_TOKEN` 在产物下载 API 中的使用。

从作业的产物压缩存档中下载具有指定 ID 的作业的单个产物文件。该文件从存档中提取并流式传输到客户端。

```plaintext
GET /projects/:id/jobs/:job_id/artifacts/*artifact_path
```

参数：

| 参数   | 类型           | 是否必需 | 描述                                                                                                  |
|-----------------|----------------|----------|------------------------------------------------------------------------------------------------------------------|
| `id`            | 整型/字符串 | 是      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `job_id`        | 整型        | 是      | 作业唯一标识。                                                                                       |
| `artifact_path` | 字符串         | 是      | 产物存档中文件的路径。                                                                    |
| `job_token` **(PREMIUM)** | 字符串 | 否     | 与多项目流水线的[触发器](../ci/jobs/ci_job_token.md#download-an-artifact-from-a-different-pipeline)一起使用。它应该只在 `.gitlab-ci.yml` 文件定义的 CI/CD 作业进行调用。它的值始终是 `$CI_JOB_TOKEN`。当使用这个令牌时，与 `$CI_JOB_TOKEN` 相关的作业必须运行。 |

请求示例:

```shell
curl --location --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/jobs/5/artifacts/some/release/file.pdf"
```

可能的响应状态代码：

| 状态    | 描述                          |
|-----------|--------------------------------------|
| 200       | 发送单个产物文件。         |
| 400       | 提供的路径无效。                |
| 404       | 未找到构建或没有文件/产物。 |

## 从特定标签或分支下载单个产物文件

> - 引入于 13.10 版本。
> - 在[极狐GitLab 专业版](https://about.gitlab.cn/pricing/) 13.10 中引入 `CI_JOB_TOKEN` 在产物下载 API 中的使用。

从作业的产物存档中为给定参考名称的最新**成功**流水线的特定作业中下载单个产物文件。该文件从存档中提取并流式传输到客户端。

产物文件提供了比 [CSV 导出](../user/application_security/vulnerability_report/index.md#export-vulnerability-details)中可用的更多的详细信息。

在 13.5 及更高版本中，[父级流水线和子流水线](../ci/pipelines/downstream_pipelines.md#parent-child-pipelines)的产物按从父到子的层次顺序进行搜索。例如，如果父级流水线和子流水线都有同名的作业，则返回父级流水线中的产物。

```plaintext
GET /projects/:id/jobs/artifacts/:ref_name/raw/*artifact_path?job=name
```

参数：

| 参数   | 类型           | 是否必需 | 描述                                                                                                  |
|-----------------|----------------|----------|--------------------------------------------------------------------------------------------------------------|
| `id`            | 整型/字符串 | 是      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `ref_name`      | 字符串         | 是      | 仓库的分支名称或标签名称。不支持`HEAD` 或 `SHA`引用。                             |
| `artifact_path` | 字符串         | 是      | 产物存档中文件的路径。                                                                |
| `job`           | 字符串         | 是      | 作业名称。                                                                                         |
| `job_token` **(PREMIUM)** | 字符串 | 否     | 与多项目流水线的[触发器](../ci/jobs/ci_job_token.md#download-an-artifact-from-a-different-pipeline)一起使用。它应该只在 `.gitlab-ci.yml` 文件定义的 CI/CD 作业进行调用。它的值始终是 `$CI_JOB_TOKEN`。当使用这个令牌时，与 `$CI_JOB_TOKEN` 相关的作业必须运行。 |

请求示例：

```shell
curl --location --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/jobs/artifacts/main/raw/some/release/file.pdf?job=pdf"
```

可能的响应状态代码：

| 状态    | 描述                          |
|-----------|--------------------------------------|
| 200       | 发送单个产物文件。         |
| 400       | 提供的路径无效。                |
| 404       | 未找到构建或没有文件/产物。 |

## 保留产物

防止产物被过期设置删除。

```plaintext
POST /projects/:id/jobs/:job_id/artifacts/keep
```

参数：

| 参数   | 类型           | 是否必需 | 描述                                                                                                  |
|-----------|----------------|----------|--------------------------------------------------------------------------------------------------------------|
| `id`      | 整型/字符串 | 是      | 经过身份验证的用户拥有的项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `job_id`  | 整型        | 是      | 作业 ID。                                                                                                |

请求示例:

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/jobs/1/artifacts/keep"
```

响应示例:

```json
{
  "commit": {
    "author_email": "admin@example.com",
    "author_name": "Administrator",
    "created_at": "2015-12-24T16:51:14.000+01:00",
    "id": "0ff3ae198f8601a285adcf5c0fff204ee6fba5fd",
    "message": "Test the CI integration.",
    "short_id": "0ff3ae19",
    "title": "Test the CI integration."
  },
  "coverage": null,
  "allow_failure": false,
  "download_url": null,
  "id": 42,
  "name": "rubocop",
  "ref": "main",
  "artifacts": [],
  "runner": null,
  "stage": "test",
  "created_at": "2016-01-11T10:13:33.506Z",
  "started_at": "2016-01-11T10:13:33.506Z",
  "finished_at": "2016-01-11T10:15:10.506Z",
  "duration": 97.0,
  "status": "failed",
  "failure_reason": "script_failure",
  "tag": false,
  "web_url": "https://example.com/foo/bar/-/jobs/42",
  "user": null
}
```

## 删除作业产物

> 引入于极狐GitLab 13.10。

删除作业产物。

先决条件：

- 至少具有项目的维护者角色。

```plaintext
DELETE /projects/:id/jobs/:job_id/artifacts
```

| 参数   | 类型           | 是否必需 | 描述                                                                                                  |
|-----------|----------------|----------|-----------------------------------------------------------------------------|
| `id`      | 整型/字符串 | 是      | ID 或 [URL 编码的项目路径](rest/index.md#namespaced-path-encoding) |
| `job_id`  | 整型        | 是      | 作业 ID。                                                                |

请求示例:

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/jobs/1/artifacts"
```

NOTE:
至少需要维护者角色才能删除产物。

如果产物被成功删除，则返回带有 `204 No Content` 状态的响应代码。

## 删除项目产物

> - 在极狐GitLab 14.7 中引入，带有一个名为 `bulk_expire_project_artifacts` 的[标志](../administration/feature_flags.md)。在私有化部署版和 Jihulab.com 上默认启用。
> - 在极狐GitLab 14.10 中删除了功能标志。

删除可以删除的项目的产物。

默认情况下，[来自每个 ref 的最近成功流水线的产物被保留](../ci/jobs/job_artifacts.md#keep-artifacts-from-most-recent-successful-jobs)。

```plaintext
DELETE /projects/:id/artifacts
```

| 参数   | 类型           | 是否必需 | 描述                                                                                                  |
|-----------|----------------|----------|-----------------------------------------------------------------------------|
| `id`      | 整型/字符串 | 是      | ID 或 [URL编码的项目路径](rest/index.md#namespaced-path-encoding) |

请求示例:

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/artifacts"
```

NOTE:
至少维护者权限才能删除产物。

安排 worker 将所有可以删除的产物的到期时间更新到当前时间。返回带有状态 `202 Accepted` 的响应代码。
