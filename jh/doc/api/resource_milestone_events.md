---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 资源里程碑事件 API **(FREE)**

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/31720) -->引入于极狐GitLab 13.1。

资源[里程碑](../user/project/milestones/index.md)事件跟踪发生在极狐GitLab [议题](../user/project/issues/index.md)和[合并请求](../user/project/merge_requests/index.md)。

使用它们来跟踪添加或删除了哪个里程碑，是谁做的，以及何时发生的。

## 议题

### 列出项目议题里程碑事件

获取单个议题的所有里程碑事件的列表。

```plaintext
GET /projects/:id/issues/:issue_iid/resource_milestone_events
```

| 参数 | 类型 | 是否必需 | 描述                                                           |
| ----------- | -------------- | -------- |--------------------------------------------------------------|
| `id`        | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid` | integer        | yes      | 议题的 IID                                                      |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/resource_milestone_events"
```

响应示例：

```json
[
  {
    "id": 142,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-20T13:38:20.077Z",
    "resource_type": "Issue",
    "resource_id": 253,
    "milestone":   {
      "id": 61,
      "iid": 9,
      "project_id": 7,
      "title": "v1.2",
      "description": "Ipsum Lorem",
      "state": "active",
      "created_at": "2020-01-27T05:07:12.573Z",
      "updated_at": "2020-01-27T05:07:12.573Z",
      "due_date": null,
      "start_date": null,
      "web_url": "http://gitlab.example.com:3000/group/project/-/milestones/9"
    },
    "action": "add"
  },
  {
    "id": 143,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-21T14:38:20.077Z",
    "resource_type": "Issue",
    "resource_id": 253,
    "milestone":   {
      "id": 61,
      "iid": 9,
      "project_id": 7,
      "title": "v1.2",
      "description": "Ipsum Lorem",
      "state": "active",
      "created_at": "2020-01-27T05:07:12.573Z",
      "updated_at": "2020-01-27T05:07:12.573Z",
      "due_date": null,
      "start_date": null,
      "web_url": "http://gitlab.example.com:3000/group/project/-/milestones/9"
    },
    "action": "remove"
  }
]
```

### 获取单个议题里程碑事件

返回特定项目议题的单个里程碑事件。

```plaintext
GET /projects/:id/issues/:issue_iid/resource_milestone_events/:resource_milestone_event_id
```

参数：

| 参数 | 类型 | 是否必需 | 描述                                                             |
| ----------------------------- | -------------- | -------- |----------------------------------------------------------------|
| `id`                          | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `issue_iid`                   | integer        | yes      | 议题的 IID                                                        |
| `resource_milestone_event_id` | integer        | yes      | 里程碑事件的 ID                                                      |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/resource_milestone_events/1"
```

## 合并请求

### 列出项目内合并请求里程碑事件

获取单个合并请求的所有里程碑事件的列表。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/resource_milestone_events
```

| 参数 | 类型 | 是否必需 | 描述                                                            |
| ------------------- | -------------- | -------- |---------------------------------------------------------------|
| `id`                | integer/string | yes      | 项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid` | integer        | yes      | 合并请求的 IID                                                     |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/resource_milestone_events"
```

响应示例：

```json
[
  {
    "id": 142,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-20T13:38:20.077Z",
    "resource_type": "MergeRequest",
    "resource_id": 142,
    "milestone":   {
      "id": 61,
      "iid": 9,
      "project_id": 7,
      "title": "v1.2",
      "description": "Ipsum Lorem",
      "state": "active",
      "created_at": "2020-01-27T05:07:12.573Z",
      "updated_at": "2020-01-27T05:07:12.573Z",
      "due_date": null,
      "start_date": null,
      "web_url": "http://gitlab.example.com:3000/group/project/-/milestones/9"
    },
    "action": "add"
  },
  {
    "id": 143,
    "user": {
      "id": 1,
      "name": "Administrator",
      "username": "root",
      "state": "active",
      "avatar_url": "https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
      "web_url": "http://gitlab.example.com/root"
    },
    "created_at": "2018-08-21T14:38:20.077Z",
    "resource_type": "MergeRequest",
    "resource_id": 142,
    "milestone":   {
      "id": 61,
      "iid": 9,
      "project_id": 7,
      "title": "v1.2",
      "description": "Ipsum Lorem",
      "state": "active",
      "created_at": "2020-01-27T05:07:12.573Z",
      "updated_at": "2020-01-27T05:07:12.573Z",
      "due_date": null,
      "start_date": null,
      "web_url": "http://gitlab.example.com:3000/group/project/-/milestones/9"
    },
    "action": "remove"
  }
]
```

### 获取单个合并请求里程碑事件

返回特定项目合并请求的单个里程碑事件。

```plaintext
GET /projects/:id/merge_requests/:merge_request_iid/resource_milestone_events/:resource_milestone_event_id
```

参数：

| 参数 | 类型 | 是否必需 | 描述                                                             |
| ----------------------------- | -------------- | -------- |----------------------------------------------------------------|
| `id`                          | integer/string | yes      | 项目的 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `merge_request_iid`           | integer        | yes      | 合并请求的 IID                                                      |
| `resource_milestone_event_id` | integer        | yes      | 里程碑事件的 ID                                                      |

请求示例：

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/merge_requests/11/resource_milestone_events/120"
```
