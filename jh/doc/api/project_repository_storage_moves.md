---
stage: Systems
group: Gitaly
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目仓库存储移动 API **(FREE SELF)**

> 引入于极狐GitLab 13.0。

项目仓库（包括 Wiki 和设计仓库）可以在存储之间移动。此 API 在[迁移到 Gitaly 集群](../administration/gitaly/index.md#migrate-to-gitaly-cluster)等场景下很有用。

在项目仓库存储移动的过程中，它们会转换为不同的状态。`state` 的值为：

- `initial`：记录已创建，但尚未安排后台作业。
- `scheduled`：已安排后台作业。
- `started`：项目仓库被复制到目的仓库。
- `replicated`：项目被移动。
- `failed`：无法拷贝项目仓库或校验和不匹配。
- `finished`：项目被移动或源存储上的仓库已被删除。
- `cleanup failed`：项目已被移动但源存储上的仓库无法被删除。

为确保数据完整性，项目在移动时被置于临时只读状态。在此期间，如果用户尝试推送新的提交，会收到 `The repository is temporarily
read-only. Please try again later.` 消息。

此 API 要求您[将您授权](rest/index.md#authentication)为管理员。

其他仓库类型，请参见：

- [代码片段仓库存储移动 API](snippet_repository_storage_moves.md)
- [群组仓库存储移动 API](group_repository_storage_moves.md)

## 检索所有项目仓库存储移动

```plaintext
GET /project_repository_storage_moves
```

默认情况下，`GET` 请求一次返回 20 个结果，因为 API 结果是[分页的](rest/index.md#pagination)。

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/project_repository_storage_moves"
```

响应示例：

```json
[
  {
    "id": 1,
    "created_at": "2020-05-07T04:27:17.234Z",
    "state": "scheduled",
    "source_storage_name": "default",
    "destination_storage_name": "storage2",
    "project": {
      "id": 1,
      "description": null,
      "name": "project1",
      "name_with_namespace": "John Doe2 / project1",
      "path": "project1",
      "path_with_namespace": "namespace1/project1",
      "created_at": "2020-05-07T04:27:17.016Z"
    }
  }
]
```

## 检索所有项目的仓库存储移动

```plaintext
GET /projects/:project_id/repository_storage_moves
```

默认情况下，`GET` 请求一次返回 20 个结果，因为 API 结果是[分页的](rest/index.md#pagination)。

参数：

| 参数           | 类型      | 是否必需 | 描述                   |
|--------------|---------|------|----------------------|
| `project_id` | integer | yes  | 项目 ID  |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/repository_storage_moves"
```

响应示例：

```json
[
  {
    "id": 1,
    "created_at": "2020-05-07T04:27:17.234Z",
    "state": "scheduled",
    "source_storage_name": "default",
    "destination_storage_name": "storage2",
    "project": {
      "id": 1,
      "description": null,
      "name": "project1",
      "name_with_namespace": "John Doe2 / project1",
      "path": "project1",
      "path_with_namespace": "namespace1/project1",
      "created_at": "2020-05-07T04:27:17.016Z"
    }
  }
]
```

## 获取单个项目仓库存储移动

```plaintext
GET /project_repository_storage_moves/:repository_storage_id
```

参数：

| 参数                      | 类型      | 是否必需 | 描述                                                 |
|-------------------------|---------|------|----------------------------------------------------|
| `repository_storage_id` | integer | yes  | 项目仓库存储移动 ID |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/project_repository_storage_moves/1"
```

响应示例：

```json
{
  "id": 1,
  "created_at": "2020-05-07T04:27:17.234Z",
  "state": "scheduled",
  "source_storage_name": "default",
  "destination_storage_name": "storage2",
  "project": {
    "id": 1,
    "description": null,
    "name": "project1",
    "name_with_namespace": "John Doe2 / project1",
    "path": "project1",
    "path_with_namespace": "namespace1/project1",
    "created_at": "2020-05-07T04:27:17.016Z"
  }
}
```

## 获取项目的单个仓库存储移动

```plaintext
GET /projects/:project_id/repository_storage_moves/:repository_storage_id
```

参数：

| 参数                      | 类型      | 是否必需 | 描述                                                 |
|-------------------------|---------|------|----------------------------------------------------|
| `project_id`            | integer | yes  | 项目 ID                                              |
| `repository_storage_id` | integer | yes  | 项目仓库存储移动 ID  |

请求示例：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/repository_storage_moves/1"
```

响应示例：

```json
{
  "id": 1,
  "created_at": "2020-05-07T04:27:17.234Z",
  "state": "scheduled",
  "source_storage_name": "default",
  "destination_storage_name": "storage2",
  "project": {
    "id": 1,
    "description": null,
    "name": "project1",
    "name_with_namespace": "John Doe2 / project1",
    "path": "project1",
    "path_with_namespace": "namespace1/project1",
    "created_at": "2020-05-07T04:27:17.016Z"
  }
}
```

## 为项目计划仓库存储移动

> - 引入于极狐GitLab 13.1。
> - 引入于极狐GitLab 13.3，成功移动和完整性检查后，原始仓库会自动移除。

WARNING:
在极狐GitLab 13.3 之前，仓库移动更像是仓库副本，因为原始仓库未从原始存储磁盘位置删除，并且必须手动清理。

```plaintext
POST /projects/:project_id/repository_storage_moves
```

参数：

| 参数                         | 类型      | 是否必需 | 描述                                                                                                                                      |
|----------------------------|---------|------|-----------------------------------------------------------------------------------------------------------------------------------------|
| `project_id`               | integer | yes  | 项目 ID                                                                                                                                   |
| `destination_storage_name` | string  | no   | 目标存储分片的名称。在 13.5 及更高版本中，如未提供存储，则基于[存储权重自动](../administration/repository_storage_paths.md#configure-where-new-repositories-are-stored)选择 |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --header "Content-Type: application/json" \
     --data '{"destination_storage_name":"storage2"}' \
     "https://gitlab.example.com/api/v4/projects/1/repository_storage_moves"
```

响应示例：

```json
{
  "id": 1,
  "created_at": "2020-05-07T04:27:17.234Z",
  "state": "scheduled",
  "source_storage_name": "default",
  "destination_storage_name": "storage2",
  "project": {
    "id": 1,
    "description": null,
    "name": "project1",
    "name_with_namespace": "John Doe2 / project1",
    "path": "project1",
    "path_with_namespace": "namespace1/project1",
    "created_at": "2020-05-07T04:27:17.016Z"
  }
}
```

## 为所有存储分片上的项目计划仓库存储移动

> 引入于极狐GitLab 13.7。

为存储在源存储分片上的每个项目仓库安排仓库存储移动。
此端点一次性迁移所有项目。有关详细信息，请参阅[移动所有项目](../administration/operations/moving_repositories.md#move-all-projects)。

```plaintext
POST /project_repository_storage_moves
```

参数：

| 参数                         | 类型     | 是否必需 | 描述                                                                                                                        |
|----------------------------|--------|------|---------------------------------------------------------------------------------------------------------------------------|
| `source_storage_name`      | string | yes  | 源存储分片的名称                                                                                                                  |
| `destination_storage_name` | string | no   | 目标存储分片的名称。如未提供存储，则基于[存储权重自动](../administration/repository_storage_paths.md#configure-where-new-repositories-are-stored)选择 |

请求示例：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" --header "Content-Type: application/json" \
     --data '{"source_storage_name":"default"}' \
     "https://gitlab.example.com/api/v4/project_repository_storage_moves"
```

响应示例：

```json
{
  "message": "202 Accepted"
}
```
