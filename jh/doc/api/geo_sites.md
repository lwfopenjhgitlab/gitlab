---
stage: Systems
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Geo 站点 API **(PREMIUM SELF)**

> 引入于极狐GitLab 16.0。

使用 Geo 站点 API 管理 Geo 站点端点。

先决条件：

- 您必须是管理员。

## 创建新 Geo 站点

创建新 Geo 站点。

```plaintext
POST /geo_sites
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/geo_sites" \
     --request POST \
     -d "name=himynameissomething" \
     -d "url=https://another-node.example.com/"
```

| 参数                                    | 类型      | 是否必需 | 描述                                                                           |
|---------------------------------------|---------|------|------------------------------------------------------------------------------|
| `primary`                             | boolean | no   | 指定此站点是否应为主要站点。默认为假                                                           |
| `enabled`                             | boolean | no   | 指示 Geo 站点是否已启用的标志。默认为真                                                       |
| `name`                                | string  | yes  | Geo 站点的唯一标识符。如果在 `gitlab.rb` 中设置，则必须匹配 `geo_node_name`，否则必须匹配 `external_url` |
| `url`                                 | string  | yes  | Geo 站点的面向用户的 URL                                                             |
| `internal_url`                        | string  | no   | 次要站点应该使用其进行联系的主要站点上定义的 URL。如果未设置，则返回 `url`                                   |
| `files_max_capacity`                  | integer | no   | 控制此次要站点的 LFS/附件回填的最大并发数。默认为 10                                               |
| `repos_max_capacity`                  | integer | no   | 控制此次要站点的仓库回填的最大并发数。默认为 25                                                    |
| `verification_max_capacity`           | integer | no   | 控制此站点的仓库验证的最大并发数。默认为 100                                                     |
| `container_repositories_max_capacity` | integer | no   | 控制此站点的容器仓库同步的最大并发数。默认为 10                                                    |
| `sync_object_storage`                 | boolean | no   | 指示次要 Geo 站点是否应在对象存储中复制 blob 的标志。默认为假                                         |
| `selective_sync_type`                 | string  | no   | 将同步限制为特定的群组或分片。有效值：`"namespaces"`、`"shards"` 或 `null`                        |
| `selective_sync_shards`               | array   | no   | 如果 `selective_sync_type` == `shards`，则为同步项目的仓库存储                             |
| `selective_sync_namespace_ids`        | array   | no   | 如果 `selective_sync_type` == `namespaces`，则为应同步的群组的 ID                        |
| `minimum_reverification_interval`     | integer | no   | 仓库验证有效的时间间隔（以天为单位）。一旦过期，则进行重新验证。在次要站点上设置时无效                                  |


响应示例：

```json
{
  "id": 3,
  "name": "Test Site 1",
  "url": "https://secondary.example.com/",
  "internal_url": "https://secondary.example.com/",
  "primary": false,
  "enabled": true,
  "current": false,
  "files_max_capacity": 10,
  "repos_max_capacity": 25,
  "verification_max_capacity": 100,
  "container_repositories_max_capacity": 10,
  "selective_sync_type": "namespaces",
  "selective_sync_shards": [],
  "selective_sync_namespace_ids": [1, 25],
  "minimum_reverification_interval": 7,
  "sync_object_storage": false,
  "web_edit_url": "https://primary.example.com/admin/geo/sites/3/edit",
  "web_geo_replication_details_url": "https://secondary.example.com/admin/geo/sites/3/replication/lfs_objects",
  "_links": {
     "self": "https://primary.example.com/api/v4/geo_sites/3",
     "status": "https://primary.example.com/api/v4/geo_sites/3/status",
     "repair": "https://primary.example.com/api/v4/geo_sites/3/repair"
  }
}
```

## 检索所有 Geo 站点相关的配置

```plaintext
GET /geo_sites
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/geo_sites"
```

响应示例：

```json
[
  {
    "id": 1,
    "name": "us-site",
    "url": "https://primary.example.com/",
    "internal_url": "https://internal.example.com/",
    "primary": true,
    "enabled": true,
    "current": true,
    "files_max_capacity": 10,
    "repos_max_capacity": 25,
    "verification_max_capacity": 100,
    "container_repositories_max_capacity": 10,
    "selective_sync_type": "namespaces",
    "selective_sync_shards": [],
    "selective_sync_namespace_ids": [1, 25],
    "minimum_reverification_interval": 7,
    "web_edit_url": "https://primary.example.com/admin/geo/sites/1/edit",
    "_links": {
      "self": "https://primary.example.com/api/v4/geo_sites/1",
      "status":"https://primary.example.com/api/v4/geo_sites/1/status",
      "repair":"https://primary.example.com/api/v4/geo_sites/1/repair"
    }
  },
  {
    "id": 2,
    "name": "cn-site",
    "url": "https://secondary.example.com/",
    "internal_url": "https://secondary.example.com/",
    "primary": false,
    "enabled": true,
    "current": false,
    "files_max_capacity": 10,
    "repos_max_capacity": 25,
    "verification_max_capacity": 100,
    "container_repositories_max_capacity": 10,
    "selective_sync_type": "namespaces",
    "selective_sync_shards": [],
    "selective_sync_namespace_ids": [1, 25],
    "minimum_reverification_interval": 7,
    "sync_object_storage": true,
    "web_edit_url": "https://primary.example.com/admin/geo/sites/2/edit",
    "web_geo_replication_details_url": "https://secondary.example.com/admin/geo/sites/2/replication/lfs_objects",
    "_links": {
      "self":"https://primary.example.com/api/v4/geo_sites/2",
      "status":"https://primary.example.com/api/v4/geo_sites/2/status",
      "repair":"https://primary.example.com/api/v4/geo_sites/2/repair"
    }
  }
]
```

## 检索特定 Geo 站点相关的配置

```plaintext
GET /geo_sites/:id
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/geo_sites/1"
```

响应示例：

```json
{
  "id": 1,
  "name": "us-site",
  "url": "https://primary.example.com/",
  "internal_url": "https://primary.example.com/",
  "primary": true,
  "enabled": true,
  "current": true,
  "files_max_capacity": 10,
  "repos_max_capacity": 25,
  "verification_max_capacity": 100,
  "container_repositories_max_capacity": 10,
  "selective_sync_type": "namespaces",
  "selective_sync_shards": [],
  "selective_sync_namespace_ids": [1, 25],
  "minimum_reverification_interval": 7,
  "web_edit_url": "https://primary.example.com/admin/geo/sites/1/edit",
  "_links": {
    "self": "https://primary.example.com/api/v4/geo_sites/1",
    "status":"https://primary.example.com/api/v4/geo_sites/1/status",
    "repair":"https://primary.example.com/api/v4/geo_sites/1/repair"
  }
}
```

## 编辑 Geo 站点

更新现有 Geo 站点的设置。

```plaintext
PUT /geo_sites/:id
```

| 参数                                    | 类型      | 是否必需 | 描述                                                                                                                                                     |
|---------------------------------------|---------|------|--------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                                  | integer | yes  | Geo 站点 ID                                                                                                                                              |
| `enabled`                             | boolean | no   | 指示 Geo 站点是否已启用的标志                                                                                                                                      |
| `name`                                | string  | no   | Geo 站点的唯一标识符。如果在 `gitlab.rb` 中设置，则必须匹配 `geo_node_name`，否则必须匹配 `external_url`                                                                           |
| `url`                                 | string  | no   | Geo 站点的面向用户的 URL                                                                                                                                       |
| `internal_url`                        | string  | no   | 次要站点应该使用其进行联系的主要站点上定义的 URL。如果未设置，则返回 `url`                                                                                                             |
| `files_max_capacity`                  | integer | no   | 控制此次要站点的 LFS/附件回填的最大并发数                                                                                                                                |
| `repos_max_capacity`                  | integer | no   | 控制此次要站点的仓库回填的最大并发数                                                                                                                                     |
| `verification_max_capacity`           | integer | no   | 控制此站点的验证的最大并发数                                                                                                                                         |
| `container_repositories_max_capacity` | integer | no   | 控制此站点的容器仓库同步的最大并发数                                                                                                                                     |
| `selective_sync_type`                 | string  | no   | 将同步限制为特定的群组或分片。有效值：`"namespaces"`、`"shards"` 或 `null`                                                                                                  |
| `selective_sync_shards`               | array   | no   | 如果 `selective_sync_type` == `shards`，则为同步项目的仓库存储                                                                                                       |
| `selective_sync_namespace_ids`        | array   | no   | 如果 `selective_sync_type` == `namespaces`，则为应同步的群组的 ID                                                                                                  |
| `minimum_reverification_interval`     | integer | no   | 仓库验证有效的时间间隔（以天为单位）。一旦过期，则进行重新验证。在次要站点上设置时无效 |

响应示例：

```json
{
  "id": 1,
  "name": "us-site",
  "url": "https://primary.example.com/",
  "internal_url": "https://internal.example.com/",
  "primary": true,
  "enabled": true,
  "current": true,
  "files_max_capacity": 10,
  "repos_max_capacity": 25,
  "verification_max_capacity": 100,
  "container_repositories_max_capacity": 10,
  "selective_sync_type": "namespaces",
  "selective_sync_shards": [],
  "selective_sync_namespace_ids": [1, 25],
  "minimum_reverification_interval": 7,
  "web_edit_url": "https://primary.example.com/admin/geo/sites/1/edit",
  "_links": {
    "self": "https://primary.example.com/api/v4/geo_sites/1",
    "status": "https://primary.example.com/api/v4/geo_sites/1/status",
    "repair": "https://primary.example.com/api/v4/geo_sites/1/repair"
  }
}

```

## 删除 Geo 站点

移除 Geo 站点。

```plaintext
DELETE /geo_sites/:id
```

| 参数   | 类型      | 是否必需 | 描述                    |
|------|---------|------|-----------------------|
| `id` | integer | yes  | Geo 站点 ID |

## 修复主要 Geo 站点

修复主要 Geo 站点的 OAuth 身份验证。

```plaintext
POST /geo_sites/:id/repair
```

响应示例：

```json
{
  "id": 1,
  "name": "us-site",
  "url": "https://primary.example.com/",
  "internal_url": "https://primary.example.com/",
  "primary": true,
  "enabled": true,
  "current": true,
  "files_max_capacity": 10,
  "repos_max_capacity": 25,
  "verification_max_capacity": 100,
  "container_repositories_max_capacity": 10,
  "web_edit_url": "https://primary.example.com/admin/geo/sites/1/edit",
  "_links": {
    "self": "https://primary.example.com/api/v4/geo_sites/1",
    "status":"https://primary.example.com/api/v4/geo_sites/1/status",
    "repair":"https://primary.example.com/api/v4/geo_sites/1/repair"
  }
}
```

## 检索所有 Geo 站点相关的状态

```plaintext
GET /geo_sites/status
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/geo_sites/status"
```

响应示例：

```json
[
  {
    "geo_node_id": 1,
    "repository_verification_enabled": true,
    "repositories_replication_enabled": null,
    "repositories_synced_count": null,
    "repositories_failed_count": null,
    "wikis_synced_count": null,
    "wikis_failed_count": null,
    "repositories_verified_count": null,
    "repositories_verification_failed_count": null,
    "repositories_verification_total_count": null,
    "wikis_verified_count": null,
    "wikis_verification_failed_count": null,
    "wikis_verification_total_count": null,
    "job_artifacts_synced_missing_on_primary_count": null,
    "repositories_checksummed_count": 19,
    "repositories_checksum_failed_count": 0,
    "repositories_checksum_mismatch_count": null,
    "repositories_checksum_total_count": 19,
    "wikis_checksummed_count": 0,
    "wikis_checksum_failed_count": 0,
    "wikis_checksum_mismatch_count": null,
    "wikis_checksum_total_count": 19,
    "repositories_retrying_verification_count": null,
    "wikis_retrying_verification_count": null,
    "projects_count": 19,
    "container_repositories_replication_enabled": null,
    "design_repositories_replication_enabled": null,
    "design_repositories_count": null,
    "design_repositories_synced_count": null,
    "design_repositories_failed_count": null,
    "lfs_objects_count": 0,
    "lfs_objects_checksum_total_count": 0,
    "lfs_objects_checksummed_count": 0,
    "lfs_objects_checksum_failed_count": 0,
    "lfs_objects_synced_count": null,
    "lfs_objects_failed_count": null,
    "lfs_objects_registry_count": null,
    "lfs_objects_verification_total_count": null,
    "lfs_objects_verified_count": null,
    "lfs_objects_verification_failed_count": null,
    "merge_request_diffs_count": 0,
    "merge_request_diffs_checksum_total_count": 0,
    "merge_request_diffs_checksummed_count": 0,
    "merge_request_diffs_checksum_failed_count": 0,
    "merge_request_diffs_synced_count": null,
    "merge_request_diffs_failed_count": null,
    "merge_request_diffs_registry_count": null,
    "merge_request_diffs_verification_total_count": null,
    "merge_request_diffs_verified_count": null,
    "merge_request_diffs_verification_failed_count": null,
    "package_files_count": 25,
    "package_files_checksum_total_count": 25,
    "package_files_checksummed_count": 25,
    "package_files_checksum_failed_count": 0,
    "package_files_synced_count": null,
    "package_files_failed_count": null,
    "package_files_registry_count": null,
    "package_files_verification_total_count": null,
    "package_files_verified_count": null,
    "package_files_verification_failed_count": null,
    "terraform_state_versions_count": 18,
    "terraform_state_versions_checksum_total_count": 18,
    "terraform_state_versions_checksummed_count": 18,
    "terraform_state_versions_checksum_failed_count": 0,
    "terraform_state_versions_synced_count": null,
    "terraform_state_versions_failed_count": null,
    "terraform_state_versions_registry_count": null,
    "terraform_state_versions_verification_total_count": null,
    "terraform_state_versions_verified_count": null,
    "terraform_state_versions_verification_failed_count": null,
    "snippet_repositories_count": 20,
    "snippet_repositories_checksum_total_count": 20,
    "snippet_repositories_checksummed_count": 20,
    "snippet_repositories_checksum_failed_count": 0,
    "snippet_repositories_synced_count": null,
    "snippet_repositories_failed_count": null,
    "snippet_repositories_registry_count": null,
    "snippet_repositories_verification_total_count": null,
    "snippet_repositories_verified_count": null,
    "snippet_repositories_verification_failed_count": null,
    "group_wiki_repositories_count": 0,
    "group_wiki_repositories_checksum_total_count": null,
    "group_wiki_repositories_checksummed_count": null,
    "group_wiki_repositories_checksum_failed_count": null,
    "group_wiki_repositories_synced_count": null,
    "group_wiki_repositories_failed_count": null,
    "group_wiki_repositories_registry_count": null,
    "group_wiki_repositories_verification_total_count": null,
    "group_wiki_repositories_verified_count": null,
    "group_wiki_repositories_verification_failed_count": null,
    "pipeline_artifacts_count": 0,
    "pipeline_artifacts_checksum_total_count": 0,
    "pipeline_artifacts_checksummed_count": 0,
    "pipeline_artifacts_checksum_failed_count": 0,
    "pipeline_artifacts_synced_count": null,
    "pipeline_artifacts_failed_count": null,
    "pipeline_artifacts_registry_count": null,
    "pipeline_artifacts_verification_total_count": null,
    "pipeline_artifacts_verified_count": null,
    "pipeline_artifacts_verification_failed_count": null,
    "pages_deployments_count": 0,
    "pages_deployments_checksum_total_count": 0,
    "pages_deployments_checksummed_count": 0,
    "pages_deployments_checksum_failed_count": 0,
    "pages_deployments_synced_count": null,
    "pages_deployments_failed_count": null,
    "pages_deployments_registry_count": null,
    "pages_deployments_verification_total_count": null,
    "pages_deployments_verified_count": null,
    "pages_deployments_verification_failed_count": null,
    "uploads_count": 51,
    "uploads_checksum_total_count": 51,
    "uploads_checksummed_count": 51,
    "uploads_checksum_failed_count": 0,
    "uploads_synced_count": null,
    "uploads_failed_count": null,
    "uploads_registry_count": null,
    "uploads_verification_total_count": null,
    "uploads_verified_count": null,
    "uploads_verification_failed_count": null,
    "job_artifacts_count": 205,
    "job_artifacts_checksum_total_count": 205,
    "job_artifacts_checksummed_count": 205,
    "job_artifacts_checksum_failed_count": 0,
    "job_artifacts_synced_count": null,
    "job_artifacts_failed_count": null,
    "job_artifacts_registry_count": null,
    "job_artifacts_verification_total_count": null,
    "job_artifacts_verified_count": null,
    "job_artifacts_verification_failed_count": null,
    "ci_secure_files_count": 0,
    "ci_secure_files_checksum_total_count": 0,
    "ci_secure_files_checksummed_count": 0,
    "ci_secure_files_checksum_failed_count": 0,
    "ci_secure_files_synced_count": null,
    "ci_secure_files_failed_count": null,
    "ci_secure_files_registry_count": null,
    "ci_secure_files_verification_total_count": null,
    "ci_secure_files_verified_count": null,
    "ci_secure_files_verification_failed_count": null,
    "container_repositories_count": 0,
    "container_repositories_checksum_total_count": 0,
    "container_repositories_checksummed_count": 0,
    "container_repositories_checksum_failed_count": 0,
    "container_repositories_synced_count": null,
    "container_repositories_failed_count": null,
    "container_repositories_registry_count": null,
    "container_repositories_verification_total_count": null,
    "container_repositories_verified_count": null,
    "container_repositories_verification_failed_count": null,
    "dependency_proxy_blobs_count": 0,
    "dependency_proxy_blobs_checksum_total_count": 0,
    "dependency_proxy_blobs_checksummed_count": 0,
    "dependency_proxy_blobs_checksum_failed_count": 0,
    "dependency_proxy_blobs_synced_count": null,
    "dependency_proxy_blobs_failed_count": null,
    "dependency_proxy_blobs_registry_count": null,
    "dependency_proxy_blobs_verification_total_count": null,
    "dependency_proxy_blobs_verified_count": null,
    "dependency_proxy_blobs_verification_failed_count": null,
    "dependency_proxy_manifests_count": 0,
    "dependency_proxy_manifests_checksum_total_count": 0,
    "dependency_proxy_manifests_checksummed_count": 0,
    "dependency_proxy_manifests_checksum_failed_count": 0,
    "dependency_proxy_manifests_synced_count": null,
    "dependency_proxy_manifests_failed_count": null,
    "dependency_proxy_manifests_registry_count": null,
    "dependency_proxy_manifests_verification_total_count": null,
    "dependency_proxy_manifests_verified_count": null,
    "dependency_proxy_manifests_verification_failed_count": null,
    "project_wiki_repositories_count": 19,
    "project_wiki_repositories_checksum_total_count": 19,
    "project_wiki_repositories_checksummed_count": 19,
    "project_wiki_repositories_checksum_failed_count": 0,
    "project_wiki_repositories_synced_count": null,
    "project_wiki_repositories_failed_count": null,
    "project_wiki_repositories_registry_count": null,
    "project_wiki_repositories_verification_total_count": null,
    "project_wiki_repositories_verified_count": null,
    "project_wiki_repositories_verification_failed_count": null,
    "git_fetch_event_count_weekly": null,
    "git_push_event_count_weekly": null,
    "proxy_remote_requests_event_count_weekly": null,
    "proxy_local_requests_event_count_weekly": null,
    "repositories_synced_in_percentage": "0.00%",
    "repositories_checksummed_in_percentage": "100.00%",
    "repositories_verified_in_percentage": "0.00%",
    "repositories_checked_in_percentage": "0.00%",
    "wikis_synced_in_percentage": "0.00%",
    "wikis_checksummed_in_percentage": "0.00%",
    "wikis_verified_in_percentage": "0.00%",
    "replication_slots_used_in_percentage": "100.00%",
    "design_repositories_synced_in_percentage": "0.00%",
    "lfs_objects_synced_in_percentage": "0.00%",
    "lfs_objects_verified_in_percentage": "0.00%",
    "merge_request_diffs_synced_in_percentage": "0.00%",
    "merge_request_diffs_verified_in_percentage": "0.00%",
    "package_files_synced_in_percentage": "0.00%",
    "package_files_verified_in_percentage": "0.00%",
    "terraform_state_versions_synced_in_percentage": "0.00%",
    "terraform_state_versions_verified_in_percentage": "0.00%",
    "snippet_repositories_synced_in_percentage": "0.00%",
    "snippet_repositories_verified_in_percentage": "0.00%",
    "group_wiki_repositories_synced_in_percentage": "0.00%",
    "group_wiki_repositories_verified_in_percentage": "0.00%",
    "pipeline_artifacts_synced_in_percentage": "0.00%",
    "pipeline_artifacts_verified_in_percentage": "0.00%",
    "pages_deployments_synced_in_percentage": "0.00%",
    "pages_deployments_verified_in_percentage": "0.00%",
    "uploads_synced_in_percentage": "0.00%",
    "uploads_verified_in_percentage": "0.00%",
    "job_artifacts_synced_in_percentage": "0.00%",
    "job_artifacts_verified_in_percentage": "0.00%",
    "ci_secure_files_synced_in_percentage": "0.00%",
    "ci_secure_files_verified_in_percentage": "0.00%",
    "container_repositories_synced_in_percentage": "0.00%",
    "container_repositories_verified_in_percentage": "0.00%",
    "dependency_proxy_blobs_synced_in_percentage": "0.00%",
    "dependency_proxy_blobs_verified_in_percentage": "0.00%",
    "dependency_proxy_manifests_synced_in_percentage": "0.00%",
    "dependency_proxy_manifests_verified_in_percentage": "0.00%",
    "project_wiki_repositories_synced_in_percentage": "0.00%",
    "project_wiki_repositories_verified_in_percentage": "0.00%",
    "repositories_count": 19,
    "wikis_count": 19,
    "replication_slots_count": 1,
    "replication_slots_used_count": 1,
    "healthy": true,
    "health": "Healthy",
    "health_status": "Healthy",
    "missing_oauth_application": false,
    "db_replication_lag_seconds": null,
    "replication_slots_max_retained_wal_bytes": 0,
    "repositories_checked_count": null,
    "repositories_checked_failed_count": null,
    "last_event_id": 357,
    "last_event_timestamp": 1683127088,
    "cursor_last_event_id": null,
    "cursor_last_event_timestamp": 0,
    "last_successful_status_check_timestamp": 1683129788,
    "version": "16.0.0-pre",
    "revision": "129eb954664",
    "selective_sync_type": null,
    "namespaces": [],
    "updated_at": "2023-05-03T16:03:10.117Z",
    "storage_shards_match": true,
    "_links": {
      "self": "https://primary.example.com/api/v4/geo_sites/1/status",
      "site": "https://primary.example.com/api/v4/geo_sites/1"
    }
  },
  {
    "geo_node_id": 2,
    "repository_verification_enabled": true,
    "repositories_replication_enabled": true,
    "repositories_synced_count": 18,
    "repositories_failed_count": 0,
    "wikis_synced_count": 18,
    "wikis_failed_count": 0,
    "repositories_verified_count": 0,
    "repositories_verification_failed_count": 0,
    "repositories_verification_total_count": 19,
    "wikis_verified_count": 0,
    "wikis_verification_failed_count": 0,
    "wikis_verification_total_count": 19,
    "job_artifacts_synced_missing_on_primary_count": null,
    "repositories_checksummed_count": null,
    "repositories_checksum_failed_count": null,
    "repositories_checksum_mismatch_count": 0,
    "repositories_checksum_total_count": null,
    "wikis_checksummed_count": null,
    "wikis_checksum_failed_count": null,
    "wikis_checksum_mismatch_count": 0,
    "wikis_checksum_total_count": null,
    "repositories_retrying_verification_count": 0,
    "wikis_retrying_verification_count": 0,
    "projects_count": 19,
    "container_repositories_replication_enabled": null,
    "design_repositories_replication_enabled": true,
    "design_repositories_count": 0,
    "design_repositories_synced_count": 0,
    "design_repositories_failed_count": 0,
    "lfs_objects_count": 0,
    "lfs_objects_checksum_total_count": null,
    "lfs_objects_checksummed_count": null,
    "lfs_objects_checksum_failed_count": null,
    "lfs_objects_synced_count": 0,
    "lfs_objects_failed_count": 0,
    "lfs_objects_registry_count": 0,
    "lfs_objects_verification_total_count": 0,
    "lfs_objects_verified_count": 0,
    "lfs_objects_verification_failed_count": 0,
    "merge_request_diffs_count": 0,
    "merge_request_diffs_checksum_total_count": null,
    "merge_request_diffs_checksummed_count": null,
    "merge_request_diffs_checksum_failed_count": null,
    "merge_request_diffs_synced_count": 0,
    "merge_request_diffs_failed_count": 0,
    "merge_request_diffs_registry_count": 0,
    "merge_request_diffs_verification_total_count": 0,
    "merge_request_diffs_verified_count": 0,
    "merge_request_diffs_verification_failed_count": 0,
    "package_files_count": 25,
    "package_files_checksum_total_count": null,
    "package_files_checksummed_count": null,
    "package_files_checksum_failed_count": null,
    "package_files_synced_count": 1,
    "package_files_failed_count": 24,
    "package_files_registry_count": 25,
    "package_files_verification_total_count": 1,
    "package_files_verified_count": 1,
    "package_files_verification_failed_count": 0,
    "terraform_state_versions_count": 18,
    "terraform_state_versions_checksum_total_count": null,
    "terraform_state_versions_checksummed_count": null,
    "terraform_state_versions_checksum_failed_count": null,
    "terraform_state_versions_synced_count": 0,
    "terraform_state_versions_failed_count": 0,
    "terraform_state_versions_registry_count": 18,
    "terraform_state_versions_verification_total_count": 0,
    "terraform_state_versions_verified_count": 0,
    "terraform_state_versions_verification_failed_count": 0,
    "snippet_repositories_count": 20,
    "snippet_repositories_checksum_total_count": null,
    "snippet_repositories_checksummed_count": null,
    "snippet_repositories_checksum_failed_count": null,
    "snippet_repositories_synced_count": 20,
    "snippet_repositories_failed_count": 0,
    "snippet_repositories_registry_count": 20,
    "snippet_repositories_verification_total_count": 20,
    "snippet_repositories_verified_count": 20,
    "snippet_repositories_verification_failed_count": 0,
    "group_wiki_repositories_count": 0,
    "group_wiki_repositories_checksum_total_count": null,
    "group_wiki_repositories_checksummed_count": null,
    "group_wiki_repositories_checksum_failed_count": null,
    "group_wiki_repositories_synced_count": 0,
    "group_wiki_repositories_failed_count": 0,
    "group_wiki_repositories_registry_count": 0,
    "group_wiki_repositories_verification_total_count": null,
    "group_wiki_repositories_verified_count": null,
    "group_wiki_repositories_verification_failed_count": null,
    "pipeline_artifacts_count": 0,
    "pipeline_artifacts_checksum_total_count": null,
    "pipeline_artifacts_checksummed_count": null,
    "pipeline_artifacts_checksum_failed_count": null,
    "pipeline_artifacts_synced_count": 0,
    "pipeline_artifacts_failed_count": 0,
    "pipeline_artifacts_registry_count": 0,
    "pipeline_artifacts_verification_total_count": 0,
    "pipeline_artifacts_verified_count": 0,
    "pipeline_artifacts_verification_failed_count": 0,
    "pages_deployments_count": 0,
    "pages_deployments_checksum_total_count": null,
    "pages_deployments_checksummed_count": null,
    "pages_deployments_checksum_failed_count": null,
    "pages_deployments_synced_count": 0,
    "pages_deployments_failed_count": 0,
    "pages_deployments_registry_count": 0,
    "pages_deployments_verification_total_count": 0,
    "pages_deployments_verified_count": 0,
    "pages_deployments_verification_failed_count": 0,
    "uploads_count": 51,
    "uploads_checksum_total_count": null,
    "uploads_checksummed_count": null,
    "uploads_checksum_failed_count": null,
    "uploads_synced_count": 0,
    "uploads_failed_count": 1,
    "uploads_registry_count": 51,
    "uploads_verification_total_count": 0,
    "uploads_verified_count": 0,
    "uploads_verification_failed_count": 0,
    "job_artifacts_count": 0,
    "job_artifacts_checksum_total_count": null,
    "job_artifacts_checksummed_count": null,
    "job_artifacts_checksum_failed_count": null,
    "job_artifacts_synced_count": 0,
    "job_artifacts_failed_count": 0,
    "job_artifacts_registry_count": 0,
    "job_artifacts_verification_total_count": 0,
    "job_artifacts_verified_count": 0,
    "job_artifacts_verification_failed_count": 0,
    "ci_secure_files_count": 0,
    "ci_secure_files_checksum_total_count": null,
    "ci_secure_files_checksummed_count": null,
    "ci_secure_files_checksum_failed_count": null,
    "ci_secure_files_synced_count": 0,
    "ci_secure_files_failed_count": 0,
    "ci_secure_files_registry_count": 0,
    "ci_secure_files_verification_total_count": 0,
    "ci_secure_files_verified_count": 0,
    "ci_secure_files_verification_failed_count": 0,
    "container_repositories_count": null,
    "container_repositories_checksum_total_count": null,
    "container_repositories_checksummed_count": null,
    "container_repositories_checksum_failed_count": null,
    "container_repositories_synced_count": null,
    "container_repositories_failed_count": null,
    "container_repositories_registry_count": null,
    "container_repositories_verification_total_count": null,
    "container_repositories_verified_count": null,
    "container_repositories_verification_failed_count": null,
    "dependency_proxy_blobs_count": 0,
    "dependency_proxy_blobs_checksum_total_count": null,
    "dependency_proxy_blobs_checksummed_count": null,
    "dependency_proxy_blobs_checksum_failed_count": null,
    "dependency_proxy_blobs_synced_count": 0,
    "dependency_proxy_blobs_failed_count": 0,
    "dependency_proxy_blobs_registry_count": 0,
    "dependency_proxy_blobs_verification_total_count": 0,
    "dependency_proxy_blobs_verified_count": 0,
    "dependency_proxy_blobs_verification_failed_count": 0,
    "dependency_proxy_manifests_count": 0,
    "dependency_proxy_manifests_checksum_total_count": null,
    "dependency_proxy_manifests_checksummed_count": null,
    "dependency_proxy_manifests_checksum_failed_count": null,
    "dependency_proxy_manifests_synced_count": 0,
    "dependency_proxy_manifests_failed_count": 0,
    "dependency_proxy_manifests_registry_count": 0,
    "dependency_proxy_manifests_verification_total_count": 0,
    "dependency_proxy_manifests_verified_count": 0,
    "dependency_proxy_manifests_verification_failed_count": 0,
    "project_wiki_repositories_count": 19,
    "project_wiki_repositories_checksum_total_count": null,
    "project_wiki_repositories_checksummed_count": null,
    "project_wiki_repositories_checksum_failed_count": null,
    "project_wiki_repositories_synced_count": 19,
    "project_wiki_repositories_failed_count": 0,
    "project_wiki_repositories_registry_count": 19,
    "project_wiki_repositories_verification_total_count": 19,
    "project_wiki_repositories_verified_count": 19,
    "project_wiki_repositories_verification_failed_count": 0,
    "git_fetch_event_count_weekly": null,
    "git_push_event_count_weekly": null,
    "proxy_remote_requests_event_count_weekly": null,
    "proxy_local_requests_event_count_weekly": null,
    "repositories_synced_in_percentage": "94.74%",
    "repositories_checksummed_in_percentage": "0.00%",
    "repositories_verified_in_percentage": "0.00%",
    "repositories_checked_in_percentage": "0.00%",
    "wikis_synced_in_percentage": "94.74%",
    "wikis_checksummed_in_percentage": "0.00%",
    "wikis_verified_in_percentage": "0.00%",
    "replication_slots_used_in_percentage": "0.00%",
    "design_repositories_synced_in_percentage": "0.00%",
    "lfs_objects_synced_in_percentage": "0.00%",
    "lfs_objects_verified_in_percentage": "0.00%",
    "merge_request_diffs_synced_in_percentage": "0.00%",
    "merge_request_diffs_verified_in_percentage": "0.00%",
    "package_files_synced_in_percentage": "4.00%",
    "package_files_verified_in_percentage": "4.00%",
    "terraform_state_versions_synced_in_percentage": "0.00%",
    "terraform_state_versions_verified_in_percentage": "0.00%",
    "snippet_repositories_synced_in_percentage": "100.00%",
    "snippet_repositories_verified_in_percentage": "100.00%",
    "group_wiki_repositories_synced_in_percentage": "0.00%",
    "group_wiki_repositories_verified_in_percentage": "0.00%",
    "pipeline_artifacts_synced_in_percentage": "0.00%",
    "pipeline_artifacts_verified_in_percentage": "0.00%",
    "pages_deployments_synced_in_percentage": "0.00%",
    "pages_deployments_verified_in_percentage": "0.00%",
    "uploads_synced_in_percentage": "0.00%",
    "uploads_verified_in_percentage": "0.00%",
    "job_artifacts_synced_in_percentage": "0.00%",
    "job_artifacts_verified_in_percentage": "0.00%",
    "ci_secure_files_synced_in_percentage": "0.00%",
    "ci_secure_files_verified_in_percentage": "0.00%",
    "container_repositories_synced_in_percentage": "0.00%",
    "container_repositories_verified_in_percentage": "0.00%",
    "dependency_proxy_blobs_synced_in_percentage": "0.00%",
    "dependency_proxy_blobs_verified_in_percentage": "0.00%",
    "dependency_proxy_manifests_synced_in_percentage": "0.00%",
    "dependency_proxy_manifests_verified_in_percentage": "0.00%",
    "project_wiki_repositories_synced_in_percentage": "100.00%",
    "project_wiki_repositories_verified_in_percentage": "100.00%",
    "repositories_count": 19,
    "wikis_count": 19,
    "replication_slots_count": null,
    "replication_slots_used_count": null,
    "healthy": false,
    "health": "An existing tracking database cannot be reused..",
    "health_status": "Unhealthy",
    "missing_oauth_application": false,
    "db_replication_lag_seconds": 0,
    "replication_slots_max_retained_wal_bytes": null,
    "repositories_checked_count": null,
    "repositories_checked_failed_count": null,
    "last_event_id": 357,
    "last_event_timestamp": 1683127088,
    "cursor_last_event_id": 357,
    "cursor_last_event_timestamp": 1683127088,
    "last_successful_status_check_timestamp": 1683127146,
    "version": "16.0.0-pre",
    "revision": "129eb954664",
    "selective_sync_type": "",
    "namespaces": [],
    "updated_at": "2023-05-03T15:19:06.174Z",
    "storage_shards_match": true,
    "_links": {
      "self": "https://primary.example.com/api/v4/geo_sites/2/status",
      "site": "https://primary.example.com/api/v4/geo_sites/2"
    }
  }
]
```

## 检索特定 Geo 站点相关的状态

```plaintext
GET /geo_sites/:id/status
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/geo_sites/2/status"
```

响应示例：

```json
  {
  "geo_node_id": 2,
  "repository_verification_enabled": true,
  "repositories_replication_enabled": true,
  "repositories_synced_count": 18,
  "repositories_failed_count": 0,
  "wikis_synced_count": 18,
  "wikis_failed_count": 0,
  "repositories_verified_count": 0,
  "repositories_verification_failed_count": 0,
  "repositories_verification_total_count": 19,
  "wikis_verified_count": 0,
  "wikis_verification_failed_count": 0,
  "wikis_verification_total_count": 19,
  "job_artifacts_synced_missing_on_primary_count": null,
  "repositories_checksummed_count": null,
  "repositories_checksum_failed_count": null,
  "repositories_checksum_mismatch_count": 0,
  "repositories_checksum_total_count": null,
  "wikis_checksummed_count": null,
  "wikis_checksum_failed_count": null,
  "wikis_checksum_mismatch_count": 0,
  "wikis_checksum_total_count": null,
  "repositories_retrying_verification_count": 0,
  "wikis_retrying_verification_count": 0,
  "projects_count": 19,
  "container_repositories_replication_enabled": null,
  "design_repositories_replication_enabled": true,
  "design_repositories_count": 0,
  "design_repositories_synced_count": 0,
  "design_repositories_failed_count": 0,
  "lfs_objects_count": 0,
  "lfs_objects_checksum_total_count": null,
  "lfs_objects_checksummed_count": null,
  "lfs_objects_checksum_failed_count": null,
  "lfs_objects_synced_count": 0,
  "lfs_objects_failed_count": 0,
  "lfs_objects_registry_count": 0,
  "lfs_objects_verification_total_count": 0,
  "lfs_objects_verified_count": 0,
  "lfs_objects_verification_failed_count": 0,
  "merge_request_diffs_count": 0,
  "merge_request_diffs_checksum_total_count": null,
  "merge_request_diffs_checksummed_count": null,
  "merge_request_diffs_checksum_failed_count": null,
  "merge_request_diffs_synced_count": 0,
  "merge_request_diffs_failed_count": 0,
  "merge_request_diffs_registry_count": 0,
  "merge_request_diffs_verification_total_count": 0,
  "merge_request_diffs_verified_count": 0,
  "merge_request_diffs_verification_failed_count": 0,
  "package_files_count": 25,
  "package_files_checksum_total_count": null,
  "package_files_checksummed_count": null,
  "package_files_checksum_failed_count": null,
  "package_files_synced_count": 1,
  "package_files_failed_count": 24,
  "package_files_registry_count": 25,
  "package_files_verification_total_count": 1,
  "package_files_verified_count": 1,
  "package_files_verification_failed_count": 0,
  "terraform_state_versions_count": 18,
  "terraform_state_versions_checksum_total_count": null,
  "terraform_state_versions_checksummed_count": null,
  "terraform_state_versions_checksum_failed_count": null,
  "terraform_state_versions_synced_count": 0,
  "terraform_state_versions_failed_count": 0,
  "terraform_state_versions_registry_count": 18,
  "terraform_state_versions_verification_total_count": 0,
  "terraform_state_versions_verified_count": 0,
  "terraform_state_versions_verification_failed_count": 0,
  "snippet_repositories_count": 20,
  "snippet_repositories_checksum_total_count": null,
  "snippet_repositories_checksummed_count": null,
  "snippet_repositories_checksum_failed_count": null,
  "snippet_repositories_synced_count": 20,
  "snippet_repositories_failed_count": 0,
  "snippet_repositories_registry_count": 20,
  "snippet_repositories_verification_total_count": 20,
  "snippet_repositories_verified_count": 20,
  "snippet_repositories_verification_failed_count": 0,
  "group_wiki_repositories_count": 0,
  "group_wiki_repositories_checksum_total_count": null,
  "group_wiki_repositories_checksummed_count": null,
  "group_wiki_repositories_checksum_failed_count": null,
  "group_wiki_repositories_synced_count": 0,
  "group_wiki_repositories_failed_count": 0,
  "group_wiki_repositories_registry_count": 0,
  "group_wiki_repositories_verification_total_count": null,
  "group_wiki_repositories_verified_count": null,
  "group_wiki_repositories_verification_failed_count": null,
  "pipeline_artifacts_count": 0,
  "pipeline_artifacts_checksum_total_count": null,
  "pipeline_artifacts_checksummed_count": null,
  "pipeline_artifacts_checksum_failed_count": null,
  "pipeline_artifacts_synced_count": 0,
  "pipeline_artifacts_failed_count": 0,
  "pipeline_artifacts_registry_count": 0,
  "pipeline_artifacts_verification_total_count": 0,
  "pipeline_artifacts_verified_count": 0,
  "pipeline_artifacts_verification_failed_count": 0,
  "pages_deployments_count": 0,
  "pages_deployments_checksum_total_count": null,
  "pages_deployments_checksummed_count": null,
  "pages_deployments_checksum_failed_count": null,
  "pages_deployments_synced_count": 0,
  "pages_deployments_failed_count": 0,
  "pages_deployments_registry_count": 0,
  "pages_deployments_verification_total_count": 0,
  "pages_deployments_verified_count": 0,
  "pages_deployments_verification_failed_count": 0,
  "uploads_count": 51,
  "uploads_checksum_total_count": null,
  "uploads_checksummed_count": null,
  "uploads_checksum_failed_count": null,
  "uploads_synced_count": 0,
  "uploads_failed_count": 1,
  "uploads_registry_count": 51,
  "uploads_verification_total_count": 0,
  "uploads_verified_count": 0,
  "uploads_verification_failed_count": 0,
  "job_artifacts_count": 0,
  "job_artifacts_checksum_total_count": null,
  "job_artifacts_checksummed_count": null,
  "job_artifacts_checksum_failed_count": null,
  "job_artifacts_synced_count": 0,
  "job_artifacts_failed_count": 0,
  "job_artifacts_registry_count": 0,
  "job_artifacts_verification_total_count": 0,
  "job_artifacts_verified_count": 0,
  "job_artifacts_verification_failed_count": 0,
  "ci_secure_files_count": 0,
  "ci_secure_files_checksum_total_count": null,
  "ci_secure_files_checksummed_count": null,
  "ci_secure_files_checksum_failed_count": null,
  "ci_secure_files_synced_count": 0,
  "ci_secure_files_failed_count": 0,
  "ci_secure_files_registry_count": 0,
  "ci_secure_files_verification_total_count": 0,
  "ci_secure_files_verified_count": 0,
  "ci_secure_files_verification_failed_count": 0,
  "container_repositories_count": null,
  "container_repositories_checksum_total_count": null,
  "container_repositories_checksummed_count": null,
  "container_repositories_checksum_failed_count": null,
  "container_repositories_synced_count": null,
  "container_repositories_failed_count": null,
  "container_repositories_registry_count": null,
  "container_repositories_verification_total_count": null,
  "container_repositories_verified_count": null,
  "container_repositories_verification_failed_count": null,
  "dependency_proxy_blobs_count": 0,
  "dependency_proxy_blobs_checksum_total_count": null,
  "dependency_proxy_blobs_checksummed_count": null,
  "dependency_proxy_blobs_checksum_failed_count": null,
  "dependency_proxy_blobs_synced_count": 0,
  "dependency_proxy_blobs_failed_count": 0,
  "dependency_proxy_blobs_registry_count": 0,
  "dependency_proxy_blobs_verification_total_count": 0,
  "dependency_proxy_blobs_verified_count": 0,
  "dependency_proxy_blobs_verification_failed_count": 0,
  "dependency_proxy_manifests_count": 0,
  "dependency_proxy_manifests_checksum_total_count": null,
  "dependency_proxy_manifests_checksummed_count": null,
  "dependency_proxy_manifests_checksum_failed_count": null,
  "dependency_proxy_manifests_synced_count": 0,
  "dependency_proxy_manifests_failed_count": 0,
  "dependency_proxy_manifests_registry_count": 0,
  "dependency_proxy_manifests_verification_total_count": 0,
  "dependency_proxy_manifests_verified_count": 0,
  "dependency_proxy_manifests_verification_failed_count": 0,
  "project_wiki_repositories_count": 19,
  "project_wiki_repositories_checksum_total_count": null,
  "project_wiki_repositories_checksummed_count": null,
  "project_wiki_repositories_checksum_failed_count": null,
  "project_wiki_repositories_synced_count": 19,
  "project_wiki_repositories_failed_count": 0,
  "project_wiki_repositories_registry_count": 19,
  "project_wiki_repositories_verification_total_count": 19,
  "project_wiki_repositories_verified_count": 19,
  "project_wiki_repositories_verification_failed_count": 0,
  "git_fetch_event_count_weekly": null,
  "git_push_event_count_weekly": null,
  "proxy_remote_requests_event_count_weekly": null,
  "proxy_local_requests_event_count_weekly": null,
  "repositories_synced_in_percentage": "94.74%",
  "repositories_checksummed_in_percentage": "0.00%",
  "repositories_verified_in_percentage": "0.00%",
  "repositories_checked_in_percentage": "0.00%",
  "wikis_synced_in_percentage": "94.74%",
  "wikis_checksummed_in_percentage": "0.00%",
  "wikis_verified_in_percentage": "0.00%",
  "replication_slots_used_in_percentage": "0.00%",
  "design_repositories_synced_in_percentage": "0.00%",
  "lfs_objects_synced_in_percentage": "0.00%",
  "lfs_objects_verified_in_percentage": "0.00%",
  "merge_request_diffs_synced_in_percentage": "0.00%",
  "merge_request_diffs_verified_in_percentage": "0.00%",
  "package_files_synced_in_percentage": "4.00%",
  "package_files_verified_in_percentage": "4.00%",
  "terraform_state_versions_synced_in_percentage": "0.00%",
  "terraform_state_versions_verified_in_percentage": "0.00%",
  "snippet_repositories_synced_in_percentage": "100.00%",
  "snippet_repositories_verified_in_percentage": "100.00%",
  "group_wiki_repositories_synced_in_percentage": "0.00%",
  "group_wiki_repositories_verified_in_percentage": "0.00%",
  "pipeline_artifacts_synced_in_percentage": "0.00%",
  "pipeline_artifacts_verified_in_percentage": "0.00%",
  "pages_deployments_synced_in_percentage": "0.00%",
  "pages_deployments_verified_in_percentage": "0.00%",
  "uploads_synced_in_percentage": "0.00%",
  "uploads_verified_in_percentage": "0.00%",
  "job_artifacts_synced_in_percentage": "0.00%",
  "job_artifacts_verified_in_percentage": "0.00%",
  "ci_secure_files_synced_in_percentage": "0.00%",
  "ci_secure_files_verified_in_percentage": "0.00%",
  "container_repositories_synced_in_percentage": "0.00%",
  "container_repositories_verified_in_percentage": "0.00%",
  "dependency_proxy_blobs_synced_in_percentage": "0.00%",
  "dependency_proxy_blobs_verified_in_percentage": "0.00%",
  "dependency_proxy_manifests_synced_in_percentage": "0.00%",
  "dependency_proxy_manifests_verified_in_percentage": "0.00%",
  "project_wiki_repositories_synced_in_percentage": "100.00%",
  "project_wiki_repositories_verified_in_percentage": "100.00%",
  "repositories_count": 19,
  "wikis_count": 19,
  "replication_slots_count": null,
  "replication_slots_used_count": null,
  "healthy": false,
  "health": "An existing tracking database cannot be reused..",
  "health_status": "Unhealthy",
  "missing_oauth_application": false,
  "db_replication_lag_seconds": 0,
  "replication_slots_max_retained_wal_bytes": null,
  "repositories_checked_count": null,
  "repositories_checked_failed_count": null,
  "last_event_id": 357,
  "last_event_timestamp": 1683127088,
  "cursor_last_event_id": 357,
  "cursor_last_event_timestamp": 1683127088,
  "last_successful_status_check_timestamp": 1683127146,
  "version": "16.0.0-pre",
  "revision": "129eb954664",
  "selective_sync_type": "",
  "namespaces": [],
  "updated_at": "2023-05-03T15:19:06.174Z",
  "storage_shards_match": true,
  "_links": {
    "self": "https://primary.example.com/api/v4/geo_sites/2/status",
    "site": "https://primary.example.com/api/v4/geo_sites/2"
  }
}
```

NOTE:
`health_status` 参数只能处于"健康"或"不健康"状态，而 `health` 参数可以为空、"不健康"或包含实际的错误消息。

## 检索在当前站点上发生的项目同步或验证失败

仅适用于次要站点。

```plaintext
GET /geo_sites/current/failures
```

| 参数             | 类型     | 是否必需 | 描述                                              |
|----------------|--------|------|-------------------------------------------------|
| `type`         | string | no   | 失败对象的类型（`repository`/`wiki`）                    |
| `failure_type` | string | no   | 失败类型（`sync`/`checksum_mismatch`/`verification`） |

此端点使用[分页](rest/index.md#pagination)。

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://primary.example.com/api/v4/geo_sites/current/failures"
```

响应示例：

```json
[
  {
    "project_id": 3,
    "last_repository_synced_at": "2017-10-31 14:25:55 UTC",
    "last_repository_successful_sync_at": "2017-10-31 14:26:04 UTC",
    "last_wiki_synced_at": "2017-10-31 14:26:04 UTC",
    "last_wiki_successful_sync_at": "2017-10-31 14:26:11 UTC",
    "repository_retry_count": null,
    "wiki_retry_count": 1,
    "last_repository_sync_failure": null,
    "last_wiki_sync_failure": "Error syncing Wiki repository",
    "last_repository_verification_failure": "",
    "last_wiki_verification_failure": "",
    "repository_verification_checksum_sha": "da39a3ee5e6b4b0d32e5bfef9a601890afd80709",
    "wiki_verification_checksum_sha": "da39a3ee5e6b4b0d3255bfef9ef0189aafd80709",
    "repository_checksum_mismatch": false,
    "wiki_checksum_mismatch": false
  }
]
```
