---
stage: Manage
group: Import and Integrate
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组关系导出 API **(FREE)**

> 引入于极狐GitLab 13.12。

群组关系导出 API 将群组的部分结构导出为每个顶级关系（例如里程碑、看板和标签）的单独文件。

群组关系导出 API 主要用于[直接传输群组迁移](../user/group/import/index.md#migrate-groups-by-direct-transfer-recommended)，不能与[群组导入和导出 API](group_import_export.md) 一起使用。

## 计划新导出

开始新的群组关系导出。

```plaintext
POST /groups/:id/export_relations
```

| 参数   | 类型             | 是否必需 | 描述                                                         |
|------|----------------|------|------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的群组 ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/export_relations"
```

```json
{
  "message": "202 Accepted"
}
```

## 导出状态

查看关系导出状态。

```plaintext
GET /groups/:id/export_relations/status
```

| 参数   | 类型             | 是否必需 | 描述                                                         |
|------|----------------|------|------------------------------------------------------------|
| `id` | integer/string | yes  | 经过身份验证的用户拥有的群组 ID |

```shell
curl --request GET --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/1/export_relations/status"
```

状态可以为：

- `0`: `started`
- `1`: `finished`
- `-1`: `failed`

- `0` - `started`
- `1` - `finished`
- `-1` - `failed`

```json
[
  {
    "relation": "badges",
    "status": 1,
    "error": null,
    "updated_at": "2021-05-04T11:25:20.423Z"
  },
  {
    "relation": "boards",
    "status": 1,
    "error": null,
    "updated_at": "2021-05-04T11:25:20.085Z"
  }
]
```

## 导出下载

下载完成的关系导出。

```plaintext
GET /groups/:id/export_relations/download
```

| 参数         | 类型             | 是否必需 | 描述                                                             |
|------------|----------------|------|----------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户拥有的群组 ID                                                   |
| `relation` | string         | yes  | 要下载的群组顶级关系的名称 |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" --remote-header-name \
     --remote-name "https://gitlab.example.com/api/v4/groups/1/export_relations/download?relation=labels"
```

```shell
ls labels.ndjson.gz
labels.ndjson.gz
```

## 相关主题

- [项目关系导出 API](project_relations_export.md)
