---
stage: Plan
group: Knowledge
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
type: reference, api
---

# 群组 Wiki API **(PREMIUM)**

> - <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/212199) -->引入于极狐GitLab 13.5。
> - `encoding` 字段<!--[added](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/81150)-->添加于极狐GitLab 14.9。
> - `render_html` 属性<!--[added](https://gitlab.com/gitlab-org/gitlab/-/issues/336792)-->添加于极狐GitLab 14.9。
> - `version` 属性<!--[added](https://gitlab.com/gitlab-org/gitlab/-/issues/336792)-->添加于极狐GitLab 14.9。

[群组 Wiki](../user/project/wiki/group.md) API 只在 APIv4 中可用。

[项目 Wiki](wikis.md) API 也可用。

## 列出 Wiki 页面

列出给定群组的所有 Wiki 页面。

```plaintext
GET /groups/:id/wikis
```

| 参数             | 类型             | 是否必需 | 描述                                                    |
|----------------|----------------|------|-------------------------------------------------------|
| `id`           | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `with_content` | boolean        | no   | 包括页面内容                                                |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/wikis?with_content=1"
```

响应示例：

```json
[
  {
    "content" : "Here is an instruction how to deploy this project.",
    "format" : "markdown",
    "slug" : "deploy",
    "title" : "deploy",
    "encoding": "UTF-8"
  },
  {
    "content" : "Our development process is described here.",
    "format" : "markdown",
    "slug" : "development",
    "title" : "development",
    "encoding": "UTF-8"
  },{
    "content" : "*  [Deploy](deploy)\n*  [Development](development)",
    "format" : "markdown",
    "slug" : "home",
    "title" : "home",
    "encoding": "UTF-8"
  }
]
```

## 获取 Wiki 页面

获取特定群组的 Wiki 页面。

```plaintext
GET /groups/:id/wikis/:slug
```

| 参数            | 类型             | 是否必需 | 描述                                                    |
|---------------|----------------|------|-------------------------------------------------------|
| `id`          | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `slug`        | string         | yes  | URL 编码的 Wiki 页面的标识串（唯一字符串），例如 `dir%2Fpage_name`       |
| `render_html` | boolean        | no   | 返回 Wiki 页面的渲染 HTML                                    |
| `version`     | string         | no   | Wiki 页面版本 SHA                                         |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/wikis/home"
```

响应示例：

```json
{
  "content" : "home page",
  "format" : "markdown",
  "slug" : "home",
  "title" : "home",
  "encoding": "UTF-8"
}
```

## 创建新的 Wiki 页面

使用给定标题、标识串和内容，为特定仓库创建新的 Wiki 页面。

```plaintext
POST /projects/:id/wikis
```

| 参数        | 类型             | 是否必需 | 描述                                                        |
|-----------|----------------|------|-----------------------------------------------------------|
| `id`      | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)     |
| `content` | string         | yes  | Wiki 页面的内容                                                |
| `title`   | string         | yes  | Wiki 页面的标题                                                |
| `format`  | string         | no   | Wiki 页面的格式。可用格式为：`markdown`（默认）、`rdoc`、`asciidoc` 和 `org` |

```shell
curl --data "format=rdoc&title=Hello&content=Hello world" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/1/wikis"
```

响应示例：

```json
{
  "content" : "Hello world",
  "format" : "markdown",
  "slug" : "Hello",
  "title" : "Hello",
  "encoding": "UTF-8"
}
```

## 编辑现存 Wiki 页面

更新现存 Wiki 页面。至少需要一个参数以更新 Wiki 页面。

```plaintext
PUT /groups/:id/wikis/:slug
```

| 参数        | 类型             | 是否必需                             | 描述                                                        |
|-----------|----------------|----------------------------------|-----------------------------------------------------------|
| `id`      | integer/string | yes                              | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding)     |
| `content` | string         | yes if `title` is not provided   | Wiki 页面的内容                                                |
| `title`   | string         | yes if `content` is not provided | Wiki 页面的标题                                                |
| `format`  | string         | no                               | Wiki 页面的格式。可用格式为：`markdown`（默认）、`rdoc`、`asciidoc` 和 `org` |
| `slug`    | string         | yes                              | URL 编码的 Wiki 页面的标识串（唯一字符串）。例如：`dir%2Fpage_name`             |

```shell
curl --request PUT --data "format=rdoc&content=documentation&title=Docs" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/1/wikis/foo"
```

响应示例：

```json
{
  "content" : "documentation",
  "format" : "markdown",
  "slug" : "Docs",
  "title" : "Docs",
  "encoding": "UTF-8"
}
```

## 删除 Wiki 页面

删除特定标识串的 Wiki 页面。

```plaintext
DELETE /groups/:id/wikis/:slug
```

| 参数     | 类型             | 是否必需 | 描述                                                    |
|--------|----------------|------|-------------------------------------------------------|
| `id`   | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `slug` | string         | yes  | URL 编码的 Wiki 页面的标识串（唯一字符串）。例如：`dir%2Fpage_name`       |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/1/wikis/foo"
```

成功后，HTTP 状态代码为 `204`，并且不需要 JSON 响应。

## 向 Wiki 仓库上传附件

将文件上传到 Wiki 仓库中的附件文件夹。附件文件夹为 `uploads`。

```plaintext
POST /groups/:id/wikis/attachments
```

| 参数       | 类型             | 是否必需 | 描述                                                    |
|----------|----------------|------|-------------------------------------------------------|
| `id`     | integer/string | yes  | ID 或 [URL 编码的群组路径](rest/index.md#namespaced-path-encoding) |
| `file`   | string         | yes  | 要上传的附件                                                |
| `branch` | string         | no   | 分支名称。默认为 Wiki 仓库的默认分支                            |

要从文件系统上传文件，请使用 `--form` 参数。cURL 使用标头 `Content-Type: multipart/form-data` 发布数据。
`file=` 参数必须指向您的文件系统上的文件，并且前面添加 `@`。例如：

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     --form "file=@dk.png" "https://gitlab.example.com/api/v4/groups/1/wikis/attachments"
```

响应示例：

```json
{
  "file_name" : "dk.png",
  "file_path" : "uploads/6a061c4cf9f1c28cb22c384b4b8d4e3c/dk.png",
  "branch" : "master",
  "link" : {
    "url" : "uploads/6a061c4cf9f1c28cb22c384b4b8d4e3c/dk.png",
    "markdown" : "![dk](uploads/6a061c4cf9f1c28cb22c384b4b8d4e3c/dk.png)"
  }
}
```
