---
stage: Verify
group: Pipeline Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 实例级 CI/CD 变量 API **(FREE SELF)**

> - <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/14108) -->引入于极狐GitLab 13.0。
> - <!--[Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/218249) -->功能标志移除于极狐GitLab 13.2。

## 列出所有实例变量

获取所有实例级变量的列表。

```plaintext
GET /admin/ci/variables
```

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/admin/ci/variables"
```

```json
[
  {
    "key": "TEST_VARIABLE_1",
    "variable_type": "env_var",
    "value": "TEST_1",
    "protected": false,
    "masked": false,
    "raw": false
  },
  {
    "key": "TEST_VARIABLE_2",
    "variable_type": "env_var",
    "value": "TEST_2",
    "protected": false,
    "masked": false,
    "raw": false
  }
]
```

## 显示实例变量详情

获取特定实例级变量的详情。

```plaintext
GET /admin/ci/variables/:key
```

| 参数    | 类型     | 是否必需 | 描述                      |
|-------|--------|------|-------------------------|
| `key` | string | yes  | 变量的 `key`  |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/admin/ci/variables/TEST_VARIABLE_1"
```

```json
{
  "key": "TEST_VARIABLE_1",
  "variable_type": "env_var",
  "value": "TEST_1",
  "protected": false,
  "masked": false,
  "raw": false
}
```

## 创建实例变量

创建新的实例级变量。

<!--[In GitLab 13.1 and later](https://gitlab.com/gitlab-org/gitlab/-/issues/216097), -->在极狐GitLab 13.1 及更高版本中，可以更改允许的实例级变量的最大数量。

```plaintext
POST /admin/ci/variables
```

| 参数              | 类型      | 是否必需 | 描述                                                                                                                                       |
|-----------------|---------|------|------------------------------------------------------------------------------------------------------------------------------------------|
| `key`           | string  | yes  | 变量的 `key`。最大 255 个字符。只能是 `A-Z`、`a-z`、`0-9` 或 `_`                                                                                         |
| `value`         | string  | yes  | 变量的 `value`。在极狐GitLab 13.3 及更高版本中，最大允许 10,000 个字符<!--([GitLab 13.3 and later](https://gitlab.com/gitlab-org/gitlab/-/issues/220028)).--> |
| `variable_type` | string  | no   | 变量的类型。可用类型为：`env_var`（默认）和 `file`                                                                                                        |
| `protected`     | boolean | no   | 变量是否受保护                                                                                                                                  |
| `masked`        | boolean | no   | 变量是否隐藏                                                                                                                                   |
| `raw`           | boolean | no   | 变量是否可扩展                                                                                                                                  |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/admin/ci/variables" --form "key=NEW_VARIABLE" --form "value=new value"
```

```json
{
  "key": "NEW_VARIABLE",
  "value": "new value",
  "variable_type": "env_var",
  "protected": false,
  "masked": false,
  "raw": false
}
```

## 更新实例变量

更新实例级变量。

```plaintext
PUT /admin/ci/variables/:key
```

| 参数              | 类型      | 是否必需 | 描述                                                                                                                                       |
|-----------------|---------|------|------------------------------------------------------------------------------------------------------------------------------------------|
| `key`           | string  | yes  | 变量的 `key`                                                                                                                                |
| `value`         | string  | yes  | 变量的 `value`。在极狐GitLab 13.3 及更高版本中，最大允许 10,000 个字符<!--([GitLab 13.3 and later](https://gitlab.com/gitlab-org/gitlab/-/issues/220028)).--> |
| `variable_type` | string  | no   | 变量的类型。可用类型为：`env_var`（默认）和 `file`                                                                                                        |
| `protected`     | boolean | no   | 变量是否受保护                                                                                                                                  |
| `masked`        | boolean | no   | 变量是否隐藏                                                                                                                                   |
| `raw`           | boolean | no   | 变量是否可扩展                                                                                                                                  |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/admin/ci/variables/NEW_VARIABLE" --form "value=updated value"
```

```json
{
  "key": "NEW_VARIABLE",
  "value": "updated value",
  "variable_type": "env_var",
  "protected": true,
  "masked": true,
  "raw": true
}
```

## 移除实例变量

移除实例级变量。

```plaintext
DELETE /admin/ci/variables/:key
```

| 参数    | 类型     | 是否必需 | 描述                      |
|-------|--------|------|-------------------------|
| `key` | string | yes  | 变量的 `key` |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/admin/ci/variables/VARIABLE_1"
```
