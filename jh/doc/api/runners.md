---
stage: Verify
group: Runner
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Runner API **(FREE)**

[分页](rest/index.md#pagination)可用于以下 API 端点（默认返回 20 项）：

```plaintext
GET /runners
GET /runners/all
GET /runners/:id/jobs
GET /projects/:id/runners
GET /groups/:id/runners
```

<a id="registration-and-authentication-tokens"></a>

## 注册和身份验证令牌

将 Runner 连接到极狐GitLab 时，需要考虑两个令牌。

| 令牌   | 描述                                                                                                                                                                                                                                                                                    |
|------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 注册令牌 | 用于[注册 Runner](https://docs.gitlab.cn/runner/register/) 的令牌。可以通过[极狐GitLab 获取](../ci/runners/index.md)                                                                                                                                                                                  |
| 身份验证令牌 | 用于使用极狐GitLab 实例对 Runner 进行身份验证的令牌。[注册 Runner](https://docs.gitlab.cn/runner/register/) 或当手动[注册 Runner](#register-a-new-runner) 时通过 Runner API 或[重置身份验证令牌](#reset-runners-authentication-token-by-using-the-runner-id)时自动获取。您也可以使用[创建 Runner](users.md#create-a-runner) API 方法获取身份验证令牌 |

以下是如何在 Runner 注册中使用这两个令牌的示例：

1. 您使用注册令牌通过极狐GitLab API 注册 Runner，以及返回身份验证令牌。
1. 您使用该身份验证令牌并将其添加到 [Runner 的配置文件](https://docs.gitlab.cn/runner/commands/#configuration-file)：

   ```toml
   [[runners]]
     token = "<authentication_token>"
   ```

极狐GitLab 和 Runner 进行了连接。

## 列出拥有的 Runner

获取用户可用的 Runner 列表。

```plaintext
GET /runners
GET /runners?scope=active
GET /runners?type=project_type
GET /runners?status=online
GET /runners?paused=true
GET /runners?tag_list=tag1,tag2
```

| 参数         | 类型           | 是否必需 | 描述                                                                                                     |
|------------|--------------|------|--------------------------------------------------------------------------------------------------------|
| `scope`    | string       | no   | 已废弃：使用 `type` 或 `status`。要返回的 Runner 的范围：`active`、`paused`、`online` 或 `offline`。如未提供，则展示所有 Runner      |
| `type`     | string       | no   | 要返回的 Runner 的类型：`instance_type`、`group_type` 或 `project_type`                                          |
| `status`   | string       | no   | 要返回的 Runner 的状态：`online`、`offline`、`stale` 或 `never_contacted`。`active` 和 `paused` 废弃于 14.8 并将移除于 16.0 |
| `paused`   | boolean      | no   | 是否仅包括接收或忽略新作业的 Runner                                                                                  |
| `tag_list` | string array | no   | Runner 标签列表                                                                                            |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/runners"
```

NOTE:
`status` 查询参数中的 `active` 和 `paused` 值废弃于 14.8，并将移除于 16.0。其被 `paused`查询参数代替。

NOTE:
响应中的 `active` 参数废弃于 14.8，并将移除于 16.0。被 `paused` 参数代替。

响应示例：

```json
[
    {
        "active": true,
        "paused": false,
        "description": "test-1-20150125",
        "id": 6,
        "ip_address": "127.0.0.1",
        "is_shared": false,
        "runner_type": "project_type",
        "name": null,
        "online": true,
        "status": "online"
    },
    {
        "active": true,
        "paused": false,
        "description": "test-2-20150125",
        "id": 8,
        "ip_address": "127.0.0.1",
        "is_shared": false,
        "runner_type": "group_type",
        "name": null,
        "online": false,
        "status": "offline"
    }
]
```

## 列出所有 Runner **(FREE SELF)**

获取极狐GitLab 实例中所有 Runner 的列表（项目 Runner 和共享 Runner）。仅具有管理员访问权限的用户可以访问。

```plaintext
GET /runners/all
GET /runners/all?scope=online
GET /runners/all?type=project_type
GET /runners/all?status=online
GET /runners/all?paused=true
GET /runners/all?tag_list=tag1,tag2
```

| 参数         | 类型           | 是否必需 | 描述                                                                                                                    |
|------------|--------------|------|-----------------------------------------------------------------------------------------------------------------------|
| `scope`    | string       | no   | 已废弃：使用 `type` 或 `status`。要返回的 Runner 的范围：`specific`、`shared`、`active`、`paused`、`online` 或 `offline`。如未提供，则显示所有 Runner |
| `type`     | string       | no   | 要返回的 Runner 的类型：`instance_type`、`group_type` 或 `project_type`                                                         |
| `status`   | string       | no   | 要返回的 Runner 的状态：`online`、`offline`、`stale` 或 `never_contacted`。`active` 和 `paused` 已废弃于 14.8，将移除于 16.0                |
| `paused`   | boolean      | no   | 是否仅包含接受或忽略新作业的 Runner                                                                                                 |
| `tag_list` | string array | no   | Runner 标签列表                                                                                                           |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/runners/all"
```

NOTE:
`status` 查询参数中的 `active` 和 `paused` 值已废弃于 14.8，并将移除于 16.0。被 `paused` 查询参数代替。

NOTE:
响应中的 `active` 参数废弃于 14.8，并将移除于 16.0。被 `paused` 参数代替。

响应示例：

```json
[
    {
        "active": true,
        "paused": false,
        "description": "shared-runner-1",
        "id": 1,
        "ip_address": "127.0.0.1",
        "is_shared": true,
        "runner_type": "instance_type",
        "name": null,
        "online": true,
        "status": "online"
    },
    {
        "active": true,
        "paused": false,
        "description": "shared-runner-2",
        "id": 3,
        "ip_address": "127.0.0.1",
        "is_shared": true,
        "runner_type": "instance_type",
        "name": null,
        "online": false,
        "status": "offline"
    },
    {
        "active": true,
        "paused": false,
        "description": "test-1-20150125",
        "id": 6,
        "ip_address": "127.0.0.1",
        "is_shared": false,
        "runner_type": "project_type",
        "name": null,
        "online": true,
        "status": "paused"
    },
    {
        "active": true,
        "paused": false,
        "description": "test-2-20150125",
        "id": 8,
        "ip_address": "127.0.0.1",
        "is_shared": false,
        "runner_type": "group_type",
        "name": null,
        "online": false,
        "status": "offline"
    }
]
```

要查看前 20 Runner 后的其他 Runner，请使用[分页](rest/index.md#pagination)。

## 获取 Runner 的详细信息

获取 Runner 的详细信息。

至少需要维护者角色才能在项目和群组级别获取 Runner 的详细信息。

通过此端点的实例级别的 Runner 的详细信息对所有经过身份验证的用户可用。

```plaintext
GET /runners/:id
```

| 参数   | 类型      | 是否必需 | 描述                 |
|------|---------|------|--------------------|
| `id` | integer | yes  | Runner ID  |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/runners/6"
```

NOTE:
响应中的 `token` 参数废弃于 12.10，并移除于 13.0。

NOTE:
响应中的 `active` 参数废弃于 14.8，并将移除于 16.0。被 `paused` 参数代替。

响应示例：

```json
{
    "active": true,
    "paused": false,
    "architecture": null,
    "description": "test-1-20150125",
    "id": 6,
    "ip_address": "127.0.0.1",
    "is_shared": false,
    "runner_type": "project_type",
    "contacted_at": "2016-01-25T16:39:48.066Z",
    "name": null,
    "online": true,
    "status": "online",
    "platform": null,
    "projects": [
        {
            "id": 1,
            "name": "GitLab Community Edition",
            "name_with_namespace": "GitLab.org / GitLab Community Edition",
            "path": "gitlab-foss",
            "path_with_namespace": "gitlab-org/gitlab-foss"
        }
    ],
    "revision": null,
    "tag_list": [
        "ruby",
        "mysql"
    ],
    "version": null,
    "access_level": "ref_protected",
    "maximum_timeout": 3600
}
```

## 更新 Runner 的详细信息

更新 Runner 的详细信息。

```plaintext
PUT /runners/:id
```

| 参数                | 类型      | 是否必需 | 描述                                             |
|-------------------|---------|------|------------------------------------------------|
| `id`              | integer | yes  | Runner ID                                      |
| `description`     | string  | no   | Runner 描述                                      |
| `active`          | boolean | no   | 已废弃：使用 `paused` 代替。表明是否允许 Runner 接收作业的标记       |
| `paused`          | boolean | no   | 指定 Runner 是否应该忽略新作业的标记                         |
| `tag_list`        | array   | no   | Runner 标签列表                                    |
| `run_untagged`    | boolean | no   | 指定 Runner 可以执行未打标签的作业的标记                       |
| `locked`          | boolean | no   | 指定 Runner 已锁定的标记                               |
| `access_level`    | string  | no   | Runner 的访问级别：`not_protected` 或 `ref_protected` |
| `maximum_timeout` | integer | no   | 限制 Runner 可以运行作业的时间量（以秒为单位）的最大超时时间             |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/runners/6" \
     --form "description=test-1-20150125-test" --form "tag_list=ruby,mysql,tag1,tag2"
```

NOTE:
响应中的 `token` 参数废弃于 12.10，并且移除于 13.0。

NOTE:
`active` 查询参数废弃于 14.8，并将移除于 16.0。被 `paused` 参数代替。

响应示例：

```json
{
    "active": true,
    "architecture": null,
    "description": "test-1-20150125-test",
    "id": 6,
    "ip_address": "127.0.0.1",
    "is_shared": false,
    "runner_type": "group_type",
    "contacted_at": "2016-01-25T16:39:48.066Z",
    "name": null,
    "online": true,
    "status": "online",
    "platform": null,
    "projects": [
        {
            "id": 1,
            "name": "GitLab Community Edition",
            "name_with_namespace": "GitLab.org / GitLab Community Edition",
            "path": "gitlab-foss",
            "path_with_namespace": "gitlab-org/gitlab-foss"
        }
    ],
    "revision": null,
    "tag_list": [
        "ruby",
        "mysql",
        "tag1",
        "tag2"
    ],
    "version": null,
    "access_level": "ref_protected",
    "maximum_timeout": null
}
```

### 暂停 Runner

暂停 Runner。

```plaintext
PUT --form "paused=true" /runners/:runner_id

# --or--

# Deprecated: removal planned in 16.0
PUT --form "active=false" /runners/:runner_id
```

| 参数          | 类型      | 是否必需 | 描述                 |
|-------------|---------|------|--------------------|
| `runner_id` | integer | yes  | Runner ID  |

```shell
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     --form "paused=true"  "https://gitlab.example.com/api/v4/runners/6"

# --or--

# Deprecated: removal planned in 16.0
curl --request PUT --header "PRIVATE-TOKEN: <your_access_token>" \
     --form "active=false"  "https://gitlab.example.com/api/v4/runners/6"
```

NOTE:
`active` 形式参数废弃于 14.8，并将移除于 16.0。被 `paused` 参数代替。

## 列出 Runner 的作业

> 引入于极狐GitLab 10.3。

列出正在处理或已由指定 Runner 处理的作业。作业列表限制为用户至少具有报告者角色的项目。

```plaintext
GET /runners/:id/jobs
```

| 参数         | 类型      | 是否必需 | 描述                                                              |
|------------|---------|------|-----------------------------------------------------------------|
| `id`       | integer | yes  | Runner ID                                                       |
| `status`   | string  | no   | 作业状态：`running`、`success`、`failed` 或 `canceled`                  |
| `order_by` | string  | no   | 作业按照 `id` 进行排序                                                  |
| `sort`     | string  | no   | 按照 `asc` 或 `desc` 的顺序对作业进行排序（默认为 `desc`）。也指定 `order_by`，包括 `id` |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/runners/1/jobs?status=running"
```

响应示例：

```json
[
    {
        "id": 2,
        "ip_address": "127.0.0.1",
        "status": "running",
        "stage": "test",
        "name": "test",
        "ref": "master",
        "tag": false,
        "coverage": null,
        "created_at": "2017-11-16T08:50:29.000Z",
        "started_at": "2017-11-16T08:51:29.000Z",
        "finished_at": "2017-11-16T08:53:29.000Z",
        "duration": 120,
        "user": {
            "id": 1,
            "name": "John Doe2",
            "username": "user2",
            "state": "active",
            "avatar_url": "http://www.gravatar.com/avatar/c922747a93b40d1ea88262bf1aebee62?s=80&d=identicon",
            "web_url": "http://localhost/user2",
            "created_at": "2017-11-16T18:38:46.000Z",
            "bio": null,
            "location": null,
            "public_email": "",
            "skype": "",
            "linkedin": "",
            "twitter": "",
            "website_url": "",
            "organization": null
        },
        "commit": {
            "id": "97de212e80737a608d939f648d959671fb0a0142",
            "short_id": "97de212e",
            "title": "Update configuration\r",
            "created_at": "2017-11-16T08:50:28.000Z",
            "parent_ids": [
                "1b12f15a11fc6e62177bef08f47bc7b5ce50b141",
                "498214de67004b1da3d820901307bed2a68a8ef6"
            ],
            "message": "See merge request !123",
            "author_name": "John Doe2",
            "author_email": "user2@example.org",
            "authored_date": "2017-11-16T08:50:27.000Z",
            "committer_name": "John Doe2",
            "committer_email": "user2@example.org",
            "committed_date": "2017-11-16T08:50:27.000Z"
        },
        "pipeline": {
            "id": 2,
            "sha": "97de212e80737a608d939f648d959671fb0a0142",
            "ref": "master",
            "status": "running"
        },
        "project": {
            "id": 1,
            "description": null,
            "name": "project1",
            "name_with_namespace": "John Doe2 / project1",
            "path": "project1",
            "path_with_namespace": "namespace1/project1",
            "created_at": "2017-11-16T18:38:46.620Z"
        }
    }
]
```

## 列出项目的 Runner

列出项目中所有可用的 Runner，包括祖先群组中和[任何允许的共享 Runner](../ci/runners/runners_scope.md#enable-shared-runners-for-a-project)。

```plaintext
GET /projects/:id/runners
GET /projects/:id/runners?scope=active
GET /projects/:id/runners?type=project_type
GET /projects/:id/runners/all?status=online
GET /projects/:id/runners/all?paused=true
GET /projects/:id/runners?tag_list=tag1,tag2
```

| 参数         | 类型             | 是否必需 | 描述                                                                                                      |
|------------|----------------|------|---------------------------------------------------------------------------------------------------------|
| `id`       | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding)                                      |
| `scope`    | string         | no   | 已废弃：使用 `type` 或 `status` 代替。要返回的 Runner 的范围：`active`、`paused`、`online` 或 `offline`。如未提供，则显示所有 Runner    |
| `type`     | string         | no   | 要返回的 Runner 的类型：`instance_type`、`group_type` 或 `project_type`                                           |
| `status`   | string         | no   | 要返回的 Runner 的状态：`online`、`offline`、`stale` 或 `never_contacted`。`active` 和 `paused` 已废弃于 14.8，并将移除于 16.0 |
| `paused`   | boolean        | no   | 是否仅包含接受或拒绝新作业的 Runner                                                                                   |
| `tag_list` | string array   | no   | Runner 标签列表                                                                                             |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/9/runners"
```

NOTE:
在 `status` 查询参数中的 `active` 和 `paused` 值废弃于 14.8，并将移除于 16.0。被 `paused` 查询参数代替。

NOTE:
响应中的 `active` 参数废弃于 14.8，并将移除于 16.0。被 `paused` 参数代替。

响应示例：

```json
[
    {
        "active": true,
        "paused": false,
        "description": "test-2-20150125",
        "id": 8,
        "ip_address": "127.0.0.1",
        "is_shared": false,
        "runner_type": "project_type",
        "name": null,
        "online": false,
        "status": "offline"
    },
    {
        "active": true,
        "paused": false,
        "description": "development_runner",
        "id": 5,
        "ip_address": "127.0.0.1",
        "is_shared": true,
        "runner_type": "instance_type",
        "name": null,
        "online": true,
        "status": "online"
    }
]
```

## 在项目中启用 Runner

在项目中启用可用的项目 Runner。

```plaintext
POST /projects/:id/runners
```

| 参数          | 类型             | 是否必需 | 描述                                                            |
|-------------|----------------|------|---------------------------------------------------------------|
| `id`        | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `runner_id` | integer        | yes  | Runner ID                                            |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/9/runners" \
     --form "runner_id=9"
```

响应示例：

```json
{
    "active": true,
    "description": "test-2016-02-01",
    "id": 9,
    "ip_address": "127.0.0.1",
    "is_shared": false,
    "runner_type": "project_type",
    "name": null,
    "online": true,
    "status": "online"
}
```

## 在项目中禁用 Runner

在项目中禁用项目 Runner。仅当项目不是与特定 Runner 关联的唯一项目时才可用。
这种情况下会返回错误。使用[删除 Runner](#delete-a-runner) 的调用。

```plaintext
DELETE /projects/:id/runners/:runner_id
```

| 参数          | 类型             | 是否必需 | 描述                                                            |
|-------------|----------------|------|---------------------------------------------------------------|
| `id`        | integer/string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `runner_id` | integer        | yes  | Runner ID                                            |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/9/runners/9"
```

## 列出群组的 Runner

列出群组及其祖先群组中的所有可用 Runner，包括[任何允许的共享 Runner](../ci/runners/runners_scope.md#enable-shared-runners-for-a-group)。

```plaintext
GET /groups/:id/runners
GET /groups/:id/runners?type=group_type
GET /groups/:id/runners/all?status=online
GET /groups/:id/runners/all?paused=true
GET /groups/:id/runners?tag_list=tag1,tag2
```

| 参数         | 类型           | 是否必需 | 描述                                                                                                     |
|------------|--------------|------|--------------------------------------------------------------------------------------------------------|
| `id`       | integer      | yes  | 经过身份验证的用户拥有的群组 ID                                                                                      |
| `type`     | string       | no   | 要返回的 Runner 的类型：`instance_type`、`group_type` 或 `project_type`。`project_type` 废弃于 15.0                  |
| `status`   | string       | no   | 要返回的 Runner 的状态：`online`、`offline`、`stale` 或 `never_contacted`。`active` 和 `paused` 废弃于 14.8，并将移除于 16.0 |
| `paused`   | boolean      | no   | 是否仅包括创建或忽略新作业的 Runner                                                                                  |
| `tag_list` | string array | no   | Runner 的标签列表                                                                                           |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/groups/9/runners"
```

NOTE:
`status` 查询参数中的 `active` 和 `paused` 值废弃于 14.8，并将移除于 16.0。被 `paused` 查询参数代替。

NOTE:
响应中的 `active` 参数废弃于 14.8，并将移除于 16.0。被 `paused` 参数替代。

响应示例：

```json
[
  {
    "id": 3,
    "description": "Shared",
    "ip_address": "127.0.0.1",
    "active": true,
    "paused": false,
    "is_shared": true,
    "runner_type": "instance_type",
    "name": "gitlab-runner",
    "online": null,
    "status": "never_contacted"
  },
  {
    "id": 6,
    "description": "Test",
    "ip_address": "127.0.0.1",
    "active": true,
    "paused": false,
    "is_shared": true,
    "runner_type": "instance_type",
    "name": "gitlab-runner",
    "online": false,
    "status": "offline"
  },
  {
    "id": 8,
    "description": "Test 2",
    "ip_address": "127.0.0.1",
    "active": true,
    "paused": false,
    "is_shared": false,
    "runner_type": "group_type",
    "name": "gitlab-runner",
    "online": null,
    "status": "never_contacted"
  }
]
```

<a id="register-a-new-runner"></a>

## 注册新 Runner

为实例注册新 Runner。

```plaintext
POST /runners
```

| 参数                 | 类型           | 是否必需 | 描述                                                                                                                              |
|--------------------|--------------|------|---------------------------------------------------------------------------------------------------------------------------------|
| `token`            | string       | yes  | [注册令牌](#registration-and-authentication-tokens)                                                                                 |
| `description`      | string       | no   | Runner 描述                                                                                                                       |
| `info`             | hash         | no   | Runner 的元数据。可以包括 `name`、`version`、`revision`、`platform` 和 `architecture`，但是在 UI 的管理中心中仅显示 `version`、`platform` 和 `architecture` |
| `active`           | boolean      | no   | 已废弃：使用 `paused` 代替。指定 Runner 是否接收作业                                                                                             |
| `paused`           | boolean      | no   | 指定 Runner 是否应该忽略新作业                                                                                                             |
| `locked`           | boolean      | no   | 指定 Runner 是否应该为当前项目锁定                                                                                                           |
| `run_untagged`     | boolean      | no   | 指定 Runner 是否应该处理未打标签的作业                                                                                                         |
| `tag_list`         | string array | no   | Runner 标签列表                                                                                                                     |
| `access_level`     | string       | no   | Runner 的访问级别：`not_protected` 或 `ref_protected`                                                                                  |
| `maximum_timeout`  | integer      | no   | 限制 Runner 可以运行作业的时间量（以秒为单位）的最大超时时间                                                                                              |
| `maintainer_note`  | string       | no   | 已废弃，请参见 `maintenance_note`                                                                                                      |
| `maintenance_note` | string       | no   | Runner（1024 位）的自由格式的维护说明                                                                                                        |

```shell
curl --request POST "https://gitlab.example.com/api/v4/runners" \
     --form "token=<registration_token>" --form "description=test-1-20150125-test" \
     --form "tag_list=ruby,mysql,tag1,tag2"
```

响应：

| 状态    | 描述            |
|-------|---------------|
| 201   | 新建 Runner     |
| 403   | Runner 注册令牌无效 |
| 410   | Runner 注册禁用   |

响应示例：

```json
{
    "id": 12345,
    "token": "6337ff461c94fd3fa32ba3b1ff4125",
    "token_expires_at": "2021-09-27T21:05:03.203Z"
}
```

## 删除 Runner

删除 Runner 有两种方式：

- 指定 Runner ID
- 指定 Runner 的身份验证令牌

### 通过 ID 删除 Runner

要通过 ID 删除 Runner，请通过 Runner ID 使用您的访问令牌：

```plaintext
DELETE /runners/:id
```

| 参数   | 类型      | 是否必需 | 描述                                                                                                  |
|------|---------|------|-----------------------------------------------------------------------------------------------------|
| `id` | integer | yes  | Runner ID。UI 中此 ID 在 **设置 > CI/CD** 下可见。展开 **Runner**，在 **移除 Runner** 按键下是 ID，ID 前面有 "#" 符号，例如 `#6` |

```shell
curl --request DELETE --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/runners/6"
```

### 通过身份验证令牌删除 Runner

使用身份验证令牌删除 Runner：

```plaintext
DELETE /runners
```

| 参数      | 类型     | 是否必需 | 描述                                                     |
|---------|--------|------|--------------------------------------------------------|
| `token` | string | yes  | Runner 的[身份验证令牌](#registration-and-authentication-tokens)|

```shell
curl --request DELETE "https://gitlab.example.com/api/v4/runners" \
     --form "token=<authentication_token>"
```

响应：

| 状态  | 描述                     |
|-----|------------------------|
| 204 | 删除了 Runner|

## 为注册 Runner 验证身份验证

为注册 Runner 验证身份验证证书。

```plaintext
POST /runners/verify
```

| 参数      | 类型     | 是否必需 | 描述                                                      |
|---------|--------|------|---------------------------------------------------------|
| `token` | string | yes  | Runner 的[身份验证令牌](#registration-and-authentication-tokens) |
| `system_id`  | string | no   | Runner 的系统标识符。如果 `token` 以 `glrt-` 开头，则此参数是必需的          |

```shell
curl --request POST "https://gitlab.example.com/api/v4/runners/verify" \
     --form "token=<authentication_token>"
```

响应：

| 状态  | 描述   |
|-----|------|
| 200 | 证书有效 |
| 403 | 证书无效 |

响应示例：

```json
{
    "id": 12345,
    "token": "glrt-6337ff461c94fd3fa32ba3b1ff4125",
    "token_expires_at": "2021-09-27T21:05:03.203Z"
}
```

## 重置实例的 Runner 注册令牌（已废弃）

> - 引入于极狐GitLab 14.3。
> - 废弃于极狐GitLab 15.7。

为极狐GitLab 实例重置 Runner 注册令牌。

```plaintext
POST /runners/reset_registration_token
```

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/runners/reset_registration_token"
```

## 重置项目的 Runner 注册令牌（已废弃）

> - 引入于极狐GitLab 14.3。
> - 废弃于极狐GitLab 15.7。

WARNING:
此功能废弃于极狐GitLab 15.7，并计划于 17.0 移除。这是一个重大变更。

为项目重置 Runner 注册令牌。

```plaintext
POST /projects/:id/runners/reset_registration_token
```

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/9/runners/reset_registration_token"
```

## 重置群组的 Runner 注册令牌（已废弃）

> - 引入于极狐GitLab 14.3。
> - 废弃于极狐GitLab 15.7。

WARNING:
此功能废弃于极狐GitLab 15.7，并计划于 17.0 移除。这是一个重大变更。

为群组重置 Runner 的注册令牌。

```plaintext
POST /groups/:id/runners/reset_registration_token
```

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/9/runners/reset_registration_token"
```

<a id="reset-runners-authentication-token-by-using-the-runner-id"></a>

## 使用 Runner ID 重置 Runner 的身份验证令牌

使用 Runner ID 重置 Runner 的身份验证令牌。

```plaintext
POST /runners/:id/reset_authentication_token
```

| 参数   | 类型      | 是否必需 | 描述                 |
|------|---------|------|--------------------|
| `id` | integer | yes  | Runner ID |

```shell
curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/runners/1/reset_authentication_token"
```

响应示例：

```json
{
    "token": "6337ff461c94fd3fa32ba3b1ff4125",
    "token_expires_at": "2021-09-27T21:05:03.203Z"
}
```

## 使用当前令牌重置 Runner 的身份验证令牌

使用当前令牌的值作为输入重置 Runner 的身份验证令牌。

```plaintext
POST /runners/reset_authentication_token
```

| 参数      | 类型     | 是否必需 | 描述             |
|---------|--------|------|----------------|
| `token` | string | yes  | Runner 的身份验证令牌 |

```shell
curl --request POST --form "token=<current token>" \
     "https://gitlab.example.com/api/v4/runners/reset_authentication_token"
```

响应示例：

```json
{
    "token": "6337ff461c94fd3fa32ba3b1ff4125",
    "token_expires_at": "2021-09-27T21:05:03.203Z"
}
```

