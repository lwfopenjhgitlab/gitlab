---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 项目集群 API (certificate-based) (deprecated) **(FREE)**

> - 引入于极狐GitLab 11.7。
> - 废弃于极狐GitLab 14.5。

WARNING:
此功能废弃于极狐GitLab 14.5。

至少需要维护者角色才能使用这些端点。

## 列出项目集群

返回项目集群列表。

```plaintext
GET /projects/:id/clusters
```

参数：

| 参数   | 类型                | 是否必需 | 描述                                                                                                      |
|------|-------------------|------|---------------------------------------------------------------------------------------------------------|
| `id` | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/26/clusters"
```

响应示例：

```json
[
  {
    "id":18,
    "name":"cluster-1",
    "domain":"example.com",
    "created_at":"2019-01-02T20:18:12.563Z",
    "managed": true,
    "enabled": true,
    "provider_type":"user",
    "platform_type":"kubernetes",
    "environment_scope":"*",
    "cluster_type":"project_type",
    "user":
    {
      "id":1,
      "name":"Administrator",
      "username":"root",
      "state":"active",
      "avatar_url":"https://www.gravatar.com/avatar/4249f4df72b..",
      "web_url":"https://gitlab.example.com/root"
    },
    "platform_kubernetes":
    {
      "api_url":"https://104.197.68.152",
      "namespace":"cluster-1-namespace",
      "authorization_type":"rbac",
      "ca_cert":"-----BEGIN CERTIFICATE-----\r\nhFiK1L61owwDQYJKoZIhvcNAQELBQAw\r\nLzEtMCsGA1UEAxMkZDA1YzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM4ZDBj\r\nMB4XDTE4MTIyNzIwMDM1MVoXDTIzMTIyNjIxMDM1MVowLzEtMCsGA1UEAxMkZDA1\r\nYzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM.......-----END CERTIFICATE-----"
    },
    "management_project":
    {
      "id":2,
      "description":null,
      "name":"project2",
      "name_with_namespace":"John Doe8 / project2",
      "path":"project2",
      "path_with_namespace":"namespace2/project2",
      "created_at":"2019-10-11T02:55:54.138Z"
    }
  },
  {
    "id":19,
    "name":"cluster-2",
    ...
  }
]
```

## 获取单个项目集群

获取单个项目集群。

```plaintext
GET /projects/:id/clusters/:cluster_id
```

参数：

| 参数           | 类型                | 是否必需 | 描述                                                                      |
|--------------|-------------------|------|-------------------------------------------------------------------------|
| `id`         | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `cluster_id` | integer           | yes  | 集群 ID                                                    |

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/26/clusters/18"
```

响应示例：

```json
{
  "id":18,
  "name":"cluster-1",
  "domain":"example.com",
  "created_at":"2019-01-02T20:18:12.563Z",
  "managed": true,
  "enabled": true,
  "provider_type":"user",
  "platform_type":"kubernetes",
  "environment_scope":"*",
  "cluster_type":"project_type",
  "user":
  {
    "id":1,
    "name":"Administrator",
    "username":"root",
    "state":"active",
    "avatar_url":"https://www.gravatar.com/avatar/4249f4df72b..",
    "web_url":"https://gitlab.example.com/root"
  },
  "platform_kubernetes":
  {
    "api_url":"https://104.197.68.152",
    "namespace":"cluster-1-namespace",
    "authorization_type":"rbac",
    "ca_cert":"-----BEGIN CERTIFICATE-----\r\nhFiK1L61owwDQYJKoZIhvcNAQELBQAw\r\nLzEtMCsGA1UEAxMkZDA1YzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM4ZDBj\r\nMB4XDTE4MTIyNzIwMDM1MVoXDTIzMTIyNjIxMDM1MVowLzEtMCsGA1UEAxMkZDA1\r\nYzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM.......-----END CERTIFICATE-----"
  },
  "management_project":
  {
    "id":2,
    "description":null,
    "name":"project2",
    "name_with_namespace":"John Doe8 / project2",
    "path":"project2",
    "path_with_namespace":"namespace2/project2",
    "created_at":"2019-10-11T02:55:54.138Z"
  },
  "project":
  {
    "id":26,
    "description":"",
    "name":"project-with-clusters-api",
    "name_with_namespace":"Administrator / project-with-clusters-api",
    "path":"project-with-clusters-api",
    "path_with_namespace":"root/project-with-clusters-api",
    "created_at":"2019-01-02T20:13:32.600Z",
    "default_branch":null,
    "tag_list":[], //deprecated, use `topics` instead
    "topics":[],
    "ssh_url_to_repo":"ssh://gitlab.example.com/root/project-with-clusters-api.git",
    "http_url_to_repo":"https://gitlab.example.com/root/project-with-clusters-api.git",
    "web_url":"https://gitlab.example.com/root/project-with-clusters-api",
    "readme_url":null,
    "avatar_url":null,
    "star_count":0,
    "forks_count":0,
    "last_activity_at":"2019-01-02T20:13:32.600Z",
    "namespace":
    {
      "id":1,
      "name":"root",
      "path":"root",
      "kind":"user",
      "full_path":"root",
      "parent_id":null
    }
  }
}
```

<a id="add-existing-cluster-to-project"></a>

## 向项目添加现有集群

向项目添加现有 Kubernetes 集群。

```plaintext
POST /projects/:id/clusters/user
```

参数：

| 参数                                                   | 类型                | 是否必需 | 描述                                                                      |
|------------------------------------------------------|-------------------|------|-------------------------------------------------------------------------|
| `id`                                                 | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `name`                                               | string            | yes  | 集群名称                                                                    |
| `domain`                                             | string            | no   | 集群的[基础域名](../user/project/clusters/gitlab_managed_clusters.md#base-domain) |
| `management_project_id`                              | integer           | no   | 集群的管理项目<!--[管理项目](../user/clusters/management_project.md)--> ID         |
| `enabled`                                            | boolean           | no   | 表明集群是否活跃。默认为 `true`                                                     |
| `managed`                                            | boolean           | no   | 确定极狐GitLab 是否管理此集群的命名空间和服务帐户。默认为 `true`                                 |
| `platform_kubernetes_attributes[api_url]`            | string            | yes  | 访问 Kubernetes API 的 URL                                                 |
| `platform_kubernetes_attributes[token]`              | string            | yes  | 针对 Kubernetes 进行身份验证的令牌                                                 |
| `platform_kubernetes_attributes[ca_cert]`            | string            | no   | TLS 证书。如果 API 使用自签名 TLS 证书，则为必需                                         |
| `platform_kubernetes_attributes[namespace]`          | string            | no   | 与项目相关的唯一命名空间                                                            |
| `platform_kubernetes_attributes[authorization_type]` | string            | no   | 集群授权类型：`rbac`、`abac` 或 `unknown_authorization`。默认为 `rbac`               |
| `environment_scope`                                  | string            | no   | 集群的关联环境。默认为 `*` **(PREMIUM)** |

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/26/clusters/user" \
-H "Accept: application/json" \
-H "Content-Type:application/json" \
-X POST --data '{"name":"cluster-5", "platform_kubernetes_attributes":{"api_url":"https://35.111.51.20","token":"12345","namespace":"cluster-5-namespace","ca_cert":"-----BEGIN CERTIFICATE-----\r\nhFiK1L61owwDQYJKoZIhvcNAQELBQAw\r\nLzEtMCsGA1UEAxMkZDA1YzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM4ZDBj\r\nMB4XDTE4MTIyNzIwMDM1MVoXDTIzMTIyNjIxMDM1MVowLzEtMCsGA1UEAxMkZDA1\r\nYzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM.......-----END CERTIFICATE-----"}}'
```

响应示例：

```json
{
  "id":24,
  "name":"cluster-5",
  "created_at":"2019-01-03T21:53:40.610Z",
  "managed": true,
  "enabled": true,
  "provider_type":"user",
  "platform_type":"kubernetes",
  "environment_scope":"*",
  "cluster_type":"project_type",
  "user":
  {
    "id":1,
    "name":"Administrator",
    "username":"root",
    "state":"active",
    "avatar_url":"https://www.gravatar.com/avatar/4249f4df72b..",
    "web_url":"https://gitlab.example.com/root"
  },
  "platform_kubernetes":
  {
    "api_url":"https://35.111.51.20",
    "namespace":"cluster-5-namespace",
    "authorization_type":"rbac",
    "ca_cert":"-----BEGIN CERTIFICATE-----\r\nhFiK1L61owwDQYJKoZIhvcNAQELBQAw\r\nLzEtMCsGA1UEAxMkZDA1YzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM4ZDBj\r\nMB4XDTE4MTIyNzIwMDM1MVoXDTIzMTIyNjIxMDM1MVowLzEtMCsGA1UEAxMkZDA1\r\nYzQ1YjctNzdiMS00NDY0LThjNmEtMTQ0ZDJkZjM.......-----END CERTIFICATE-----"
  },
  "management_project":null,
  "project":
  {
    "id":26,
    "description":"",
    "name":"project-with-clusters-api",
    "name_with_namespace":"Administrator / project-with-clusters-api",
    "path":"project-with-clusters-api",
    "path_with_namespace":"root/project-with-clusters-api",
    "created_at":"2019-01-02T20:13:32.600Z",
    "default_branch":null,
    "tag_list":[], //deprecated, use `topics` instead
    "topics":[],
    "ssh_url_to_repo":"ssh:://gitlab.example.com/root/project-with-clusters-api.git",
    "http_url_to_repo":"https://gitlab.example.com/root/project-with-clusters-api.git",
    "web_url":"https://gitlab.example.com/root/project-with-clusters-api",
    "readme_url":null,
    "avatar_url":null,
    "star_count":0,
    "forks_count":0,
    "last_activity_at":"2019-01-02T20:13:32.600Z",
    "namespace":
    {
      "id":1,
      "name":"root",
      "path":"root",
      "kind":"user",
      "full_path":"root",
      "parent_id":null
    }
  }
}
```

## 编辑项目集群

更新现有项目集群。

```plaintext
PUT /projects/:id/clusters/:cluster_id
```

参数：

| 参数                                          | 类型                | 是否必需 | 描述                                                                                                                  |
|---------------------------------------------|-------------------|------|---------------------------------------------------------------------------------------------------------------------|
| `id`                                        | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的路径](rest/index.md#namespaced-path-encoding) |
| `cluster_id`                                | integer           | yes  | 集群 ID                                                                                               |
| `name`                                      | string            | no   | 集群名称                                                                                            |
| `domain`                                    | string            | no   | 集群的[基础域名](../user/project/clusters/gitlab_managed_clusters.md#base-domain)                   |
| `management_project_id`                     | integer           | no   | 集群的管理项目<!--[管理项目](../user/clusters/management_project.md)--> ID                          |
| `enabled`                                   | boolean           | no   | 表明集群是否活跃。默认为 `true`                                                                              |
| `managed`                                   | boolean           | no   | 确定极狐GitLab 是否管理此集群的命名空间和服务帐户                                       |
| `platform_kubernetes_attributes[api_url]`   | string            | no   | 访问 Kubernetes API 的 URL                                                                                |
| `platform_kubernetes_attributes[token]`     | string            | no   | 针对 Kubernetes 进行身份验证的令牌                                                                        |
| `platform_kubernetes_attributes[ca_cert]`   | string            | no   | TLS 证书。如果 API 使用自签名 TLS 证书，则为必需                                           |
| `platform_kubernetes_attributes[namespace]` | string            | no   | 与项目相关的唯一命名空间                                                                        |
| `environment_scope`                         | string            | no   | 集群的关联环境                                                                          |

NOTE:
只有当集群通过“添加现有 Kubernetes 集群”选项或通过["向项目添加现有集群"](#add-existing-cluster-to-project)端点添加时，才能更新 `name`、`api_url`、`ca_cert` 和 `token`。

请求示例：

```shell
curl --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/26/clusters/24" \
-H "Content-Type:application/json" \
-X PUT --data '{"name":"new-cluster-name","domain":"new-domain.com","api_url":"https://new-api-url.com"}'
```

响应示例：

```json
{
  "id":24,
  "name":"new-cluster-name",
  "domain":"new-domain.com",
  "created_at":"2019-01-03T21:53:40.610Z",
  "managed": true,
  "enabled": true,
  "provider_type":"user",
  "platform_type":"kubernetes",
  "environment_scope":"*",
  "cluster_type":"project_type",
  "user":
  {
    "id":1,
    "name":"Administrator",
    "username":"root",
    "state":"active",
    "avatar_url":"https://www.gravatar.com/avatar/4249f4df72b..",
    "web_url":"https://gitlab.example.com/root"
  },
  "platform_kubernetes":
  {
    "api_url":"https://new-api-url.com",
    "namespace":"cluster-5-namespace",
    "authorization_type":"rbac",
    "ca_cert":null
  },
  "management_project":
  {
    "id":2,
    "description":null,
    "name":"project2",
    "name_with_namespace":"John Doe8 / project2",
    "path":"project2",
    "path_with_namespace":"namespace2/project2",
    "created_at":"2019-10-11T02:55:54.138Z"
  },
  "project":
  {
    "id":26,
    "description":"",
    "name":"project-with-clusters-api",
    "name_with_namespace":"Administrator / project-with-clusters-api",
    "path":"project-with-clusters-api",
    "path_with_namespace":"root/project-with-clusters-api",
    "created_at":"2019-01-02T20:13:32.600Z",
    "default_branch":null,
    "tag_list":[], //deprecated, use `topics` instead
    "topics":[],
    "ssh_url_to_repo":"ssh:://gitlab.example.com/root/project-with-clusters-api.git",
    "http_url_to_repo":"https://gitlab.example.com/root/project-with-clusters-api.git",
    "web_url":"https://gitlab.example.com/root/project-with-clusters-api",
    "readme_url":null,
    "avatar_url":null,
    "star_count":0,
    "forks_count":0,
    "last_activity_at":"2019-01-02T20:13:32.600Z",
    "namespace":
    {
      "id":1,
      "name":"root",
      "path":"root",
      "kind":"user",
      "full_path":"root",
      "parent_id":null
    }
  }
}
```

## 删除项目集群

删除现有的项目集群。不删除连接的 Kubernetes 集群中的现有资源。

```plaintext
DELETE /projects/:id/clusters/:cluster_id
```

参数：

| 参数           | 类型                | 是否必需 | 描述                                                                      |
|--------------|-------------------|------|-------------------------------------------------------------------------|
| `id`         | integer or string | yes  | 经过身份验证的用户拥有的项目 ID 或 [URL 编码的项目](rest/index.md#namespaced-path-encoding) |
| `cluster_id` | integer           | yes  | 集群 ID                                                                   |

请求示例：

```shell
curl --request DELETE --header "Private-Token: <your_access_token>" "https://gitlab.example.com/api/v4/projects/26/clusters/23"
```
