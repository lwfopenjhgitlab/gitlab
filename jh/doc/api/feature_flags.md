---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 功能标志 API **(FREE)**

> - <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/9566)-->引入于极狐GitLab 专业版 12.5。
> - <!--[Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/212318)-->移动到极狐GitLab 免费版 13.5。

用于访问[极狐GitLab 功能标志](../operations/feature_flags.md)资源的 API。

拥有至少开发者[角色](../user/permissions.md)的用户可以访问功能标志 API。

## 功能标志分页

由于 API 结果进行了[分页](rest/index.md#pagination)，`GET` 请求一次默认返回 20 个结果。

## 项目的列表功能标志

获取所有请求项目的功能标志。

```plaintext
GET /projects/:id/feature_flags
```

| 参数      | 类型             | 是否必需 | 描述                                                      |
|---------|----------------|----|---------------------------------------------------------|
| `id`    | integer/string | yes | ID 或[项目的 URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `scope` | string         | no | 功能标志的状态：`enabled` 或 `disabled`。                         |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/feature_flags"
```

响应示例：

```json
[
   {
      "name":"merge_train",
      "description":"This feature is about merge train",
      "active": true,
      "version": "new_version_flag",
      "created_at":"2019-11-04T08:13:51.423Z",
      "updated_at":"2019-11-04T08:13:51.423Z",
      "scopes":[],
      "strategies": [
        {
          "id": 1,
          "name": "userWithId",
          "parameters": {
            "userIds": "user1"
          },
          "scopes": [
            {
              "id": 1,
              "environment_scope": "production"
            }
          ]
        }
      ]
   },
   {
      "name":"new_live_trace",
      "description":"This is a new live trace feature",
      "active": true,
      "version": "new_version_flag",
      "created_at":"2019-11-04T08:13:10.507Z",
      "updated_at":"2019-11-04T08:13:10.507Z",
      "scopes":[],
      "strategies": [
        {
          "id": 2,
          "name": "default",
          "parameters": {},
          "scopes": [
            {
              "id": 2,
              "environment_scope": "staging"
            }
          ]
        }
      ]
   }
]
```

## 获取单个功能标志

获取单个功能标志。

```plaintext
GET /projects/:id/feature_flags/:feature_flag_name
```

| 参数                  | 类型             | 是否必需 | 描述                                                      |
|---------------------|----------------|----|---------------------------------------------------------|
| `id`                | integer/string | yes | ID 或[项目的 URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `feature_flag_name` | string         | yes | 功能标志的名称。                                                |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/feature_flags/awesome_feature"
```

响应示例：

```json
{
  "name": "awesome_feature",
  "description": null,
  "active": true,
  "version": "new_version_flag",
  "created_at": "2020-05-13T19:56:33.119Z",
  "updated_at": "2020-05-13T19:56:33.119Z",
  "scopes": [],
  "strategies": [
    {
      "id": 36,
      "name": "default",
      "parameters": {},
      "scopes": [
        {
          "id": 37,
          "environment_scope": "production"
        }
      ]
    }
  ]
}
```

## 创建功能标志

创建功能标志。

```plaintext
POST /projects/:id/feature_flags
```

| 参数                                    | 类型             | 是否必需 | 描述                                                                                                                                                                                                                                                                            |
|---------------------------------------|----------------|----|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `id`                                  | integer/string | yes | ID 或[项目的 URL 编码的路径](rest/index.md#namespaced-path-encoding)。                                                                                                                                                                                                                  |
| `name`                                | string         | yes | 功能标志的名称。                                                                                                                                                                                                                                                                      |
| `version`                             | string         | yes | 功能标志的版本。必须是 `new_version_flag`。您可以不填写以创建遗留功能标志。                                                                                                                                                                                                                               |
| `description`                         | string         | no | 功能标志的描述。                                                                                                                                                                                                                                                                      |
| `active`                              | boolean        | no | 标志的活跃状态。默认为 "true"。<!--[Supported](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38350)-->支持于极狐GitLab 13.3 及更高版本。                                                                                                                                                     |
| `strategies`                          | JSON           | no | 功能标志[策略](../operations/feature_flags.md#feature-flag-strategies)。                                                                                                                                                                                                             |
| `strategies:name`                     | JSON           | no | 标志名称。可以是 `default`、`gradualRolloutUserId`、`userWithId` 或 `gitlabUserList`。<!--[GitLab 13.5](https://gitlab.com/gitlab-org/gitlab/-/issues/36380)-->在极狐GitLab 13.5 及更高版本中，可以是 [`flexibleRollout`](https://docs.getunleash.io/user_guide/activation_strategy/#gradual-rollout)。 |
| `strategies:parameters`               | JSON           | no | 策略参数。                                                                                                                                                                                                                                                                         |
| `strategies:scopes`                   | JSON           | no | 策略的范围。                                                                                                                                                                                                                                                                        |
| `strategies:scopes:environment_scope` | string         | no | 范围的环境范围。                                                                                                                                                                                                                                                                      |

```shell
curl "https://gitlab.example.com/api/v4/projects/1/feature_flags" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     --header "Content-type: application/json" \
     --data @- << EOF
{
  "name": "awesome_feature",
  "version": "new_version_flag",
  "strategies": [{ "name": "default", "parameters": {}, "scopes": [{ "environment_scope": "production" }] }]
}
EOF
```

响应示例：

```json
{
  "name": "awesome_feature",
  "description": null,
  "active": true,
  "version": "new_version_flag",
  "created_at": "2020-05-13T19:56:33.119Z",
  "updated_at": "2020-05-13T19:56:33.119Z",
  "scopes": [],
  "strategies": [
    {
      "id": 36,
      "name": "default",
      "parameters": {},
      "scopes": [
        {
          "id": 37,
          "environment_scope": "production"
        }
      ]
    }
  ]
}
```

## 更新功能标志

更新功能标志。

```plaintext
PUT /projects/:id/feature_flags/:feature_flag_name
```

| 参数                                    | 类型             | 是否必需 | 描述                                                                                                              |
|---------------------------------------|----------------|------|-----------------------------------------------------------------------------------------------------------------|
| `id`                                  | integer/string | yes  | ID 或[项目的 URL 编码的路径](rest/index.md#namespaced-path-encoding)。                                                    |
| `feature_flag_name`                   | string         | yes  | 功能标志的当前名称。                                                                                                      |
| `description`                         | string         | no   | 功能标志的描述。                                                                                                        |
| `active`                              | boolean        | no   | 标志的活跃状态。<!--[Supported](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38350)-->支持于极狐GitLab 13.3 及更高版本。  |
| `name`                                | string         | no   | 功能标志的新名称。<!--[Supported](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38350)-->支持于极狐GitLab 13.3 及更高版本。 |
| `strategies`                          | JSON           | no   | 功能标志[策略](../operations/feature_flags.md#feature-flag-strategies)。                                               |
| `strategies:id`                       | JSON           | no   | 功能标志策略 ID。                                                                                                      |
| `strategies:name`                     | JSON           | no   | 策略名称。                                                                                                           |
| `strategies:_destroy`                 | boolean        | no   | 为真时删除策略。                                                                                                        |
| `strategies:parameters`               | JSON           | no   | 策略参数。                                                                                                           |
| `strategies:scopes`                   | JSON           | no   | 策略的范围。                                                                                                          |
| `strategies:scopes:id`                | JSON           | no   | 环境范围 ID。                                                                                                        |
| `strategies:scopes:environment_scope` | string         | no   | 范围的环境范围。                                                                                                        |
| `strategies:scopes:_destroy`          | boolean        | no   | 为真时删除范围。                                                                                                        |

```shell
curl "https://gitlab.example.com/api/v4/projects/1/feature_flags/awesome_feature" \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     --header "Content-type: application/json" \
     --data @- << EOF
{
  "strategies": [{ "name": "gradualRolloutUserId", "parameters": { "groupId": "default", "percentage": "25" }, "scopes": [{ "environment_scope": "staging" }] }]
}
EOF
```

响应示例：

```json
{
  "name": "awesome_feature",
  "description": null,
  "active": true,
  "version": "new_version_flag",
  "created_at": "2020-05-13T20:10:32.891Z",
  "updated_at": "2020-05-13T20:10:32.891Z",
  "scopes": [],
  "strategies": [
    {
      "id": 38,
      "name": "gradualRolloutUserId",
      "parameters": {
        "groupId": "default",
        "percentage": "25"
      },
      "scopes": [
        {
          "id": 40,
          "environment_scope": "staging"
        }
      ]
    },
    {
      "id": 37,
      "name": "default",
      "parameters": {},
      "scopes": [
        {
          "id": 39,
          "environment_scope": "production"
        }
      ]
    }
  ]
}
```

## 删除功能标志

删除功能标志。

```plaintext
DELETE /projects/:id/feature_flags/:feature_flag_name
```

| 参数                  | 类型             | 是否必需 | 描述                                                      |
|---------------------|----------------|------|---------------------------------------------------------|
| `id`                | integer/string | yes  | ID 或[项目的 URL 编码的路径](rest/index.md#namespaced-path-encoding)。 |
| `feature_flag_name` | string         | yes  | 功能标志的名称。                                                |

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" --request DELETE "https://gitlab.example.com/api/v4/projects/1/feature_flags/awesome_feature"
```
