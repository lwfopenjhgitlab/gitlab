---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 对实验、Beta 和一般可用（GA）功能的支持 **(PREMIUM)**

某些极狐GitLab 功能作为实验阶段功能或 Beta 阶段功能发布，提供不完全的技术支持。
所有其他功能均被视为一般可用（GA）。

<a id="experiment"></a>

## 实验阶段

对于具有 “实验”、“Alpha” 或任何类似名称的功能，不提供技术支持。

- 尚未准备好用于生产。
- 没有可用的技术支持。
- 可能不稳定或有性能问题。
- 可能随时删除。
- 可能会发生数据丢失。
- 文档可能不存在或只是博客格式。
- 用户界面显示实验状态。
- 用户体验不完整，可能只有快速操作访问。
- 具有默认情况下打开的功能标志。
- 具有默认情况下关闭的切换开关。
- 未在发版说明中宣布。
- 团队收集反馈问题。

## Beta

对于 Beta 阶段的功能，提供有限技术支持。

- 可能尚未准备好用于生产。
- 在商业合理的基础上提供技术支持。
- 可能不稳定，并可能导致性能和稳定性问题。
- 配置和依赖不太可能改变。
- 特性和功能不太可能改变。但是，重大更改可能会在主要版本之外发生，与一般可用阶段功能相比，变更的通知更少。
- 数据丢失的可能性不大。
- 文档标记 Beta 阶段。
- 用户界面显示 Beta 状态。
- 用户体验已完成或接近完成。
- 具有默认情况下打开的功能标志。
- 具有默认情况下关闭的切换开关。
- 可以在包括 Beta 阶段功能的发版说明中宣布。

<a id="generally-available-ga"></a>

## 一般可用（GA）

功能达到一般可用阶段，表示它们通过了生产环境准备审查，并且：

- 可用于任何规模的生产环境。
- 文档更完备，提供技术支持。
- UX 完整且符合极狐GitLab 设计标准。

<!--
## Never internal

Features are never internal (GitLab team-members) only.
Our [mission is "everyone can contribute"](https://about.gitlab.com/company/mission/), and that is only possible if people outside the company can try a feature.
We get higher quality (more diverse) feedback if people from different organizations try something.
We've also learned that internal only as a state slows us down more than it speeds us up.
Release the experiment instead of testing internally or waiting for the feature to be in a Beta state.
The experimental features are only shown when people/organizations opt-in to experiments, we are allowed to make mistakes here and literally experiment.

## All features are in production

All features that are available on GitLab.com are considered "in production."
Because all Experiment, Beta, and Generally Available features are available on GitLab.com, they are all considered to be in production.
-->
