---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
comments: false
---

# 功能分支 **(FREE)**

- 团队的高效并行工作流程
- 开发分支中的每个功能
- 保持更改隔离
- 考虑议题的一对一关联
- 频繁推送分支到服务器
  - 提示：推送分支是您正在进行的编码的简单备份。

## 功能分支工作流示例

1. 创建一个名为 `squash_some_bugs` 的新功能分支
1. 编辑 `bugs.rb` 并删除所有错误。
1. 提交
1. 推送

```shell
git checkout -b squash_some_bugs
# Edit `bugs.rb`
git status
git add bugs.rb
git commit -m 'Fix some buggy code'
git push origin squash_some_bugs
```
