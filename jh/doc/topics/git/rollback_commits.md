---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
comments: false
---

# 回滚提交 **(FREE)**

## 撤销提交

- 撤消上次提交，将所有内容放回暂存区域：

  ```shell
  git reset --soft HEAD^
  ```

- 添加文件并更改消息：

  ```shell
  git commit --amend -m "New Message"
  ```

- 撤消最后的提交并删除更改：

  ```shell
  git reset --hard HEAD^
  ```

- 与上一个相同，但是撤销两次提交：

  ```shell
  git reset --hard HEAD^^
  ```

**推送后请勿重置（reset）**

## 重置工作流

1. 再次编辑文件 `edit_this_file.rb`
1. 查看状态
1. 添加并提交错误信息
1. 查看日志
1. 修改提交
1. 查看日志
1. 软复位
1. 查看日志
1. 拉取更新
1. 推送更改

## 命令

```shell
# Change file edit_this_file.rb
git status
git commit -am "kjkfjkg"
git log
git commit --amend -m "New comment added"
git log
git reset --soft HEAD^
git log
git pull origin master
git push origin master
```

## 注意

- `git revert` 与 `git reset` 的区别
- 重置删除提交，将恢复删除更改但保留提交
- 支持把还原撤销，还原变得更安全

```shell
# Changed file
git commit -am "bug introduced"
git revert HEAD
# New commit created reverting changes
# Now we want to re apply the reverted commit
git log # take hash from the revert commit
git revert <rev commit hash>
# reverted commit is back (new commit created again)
```
