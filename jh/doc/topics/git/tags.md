---
redirect_to: '../../user/project/repository/tags/index.md'
remove_date: '2023-05-27'
---

此文档已移动到[另一个位置](../../user/project/repository/tags/index.md)。

<!-- This redirect file can be deleted after <2023-05-27>. -->
<!-- Redirects that point to other docs in the same project expire in three months. -->
<!-- Redirects that point to docs in a different project or site (for example, link is not relative and starts with `https:`) expire in one year. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/redirects.html -->
