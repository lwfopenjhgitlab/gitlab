---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
comments: false
---

# Git Log **(FREE)**

Git Log 列出提交历史。允许搜索和过滤。

- 启动日志：

  ```shell
  git log
  ```

- 获取记录的 set 数：

  ```shell
  git log -n 2
  ```

- 按作者搜索提交。允许用户名或正则表达式。

  ```shell
  git log --author="user_name"
  ```

- 按评论信息搜索：

  ```shell
  git log --grep="<pattern>"
  ```

- 按日期搜索：

  ```shell
  git log --since=1.month.ago --until=3.weeks.ago
  ```

## Git Log 工作流

1. 切换到工作区目录
1. 克隆多 runner 项目
1. 更改为项目目录
1. 按作者搜索
1. 按日期搜索
1. 结合

## 命令

```shell
cd ~/workspace
git clone git@gitlab.com:gitlab-org/gitlab-runner.git
cd gitlab-runner
git log --author="Travis"
git log --since=1.month.ago --until=3.weeks.ago
git log --since=1.month.ago --until=1.day.ago --author="Travis"
```
