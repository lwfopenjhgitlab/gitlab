---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
description: 'This article describes how to install Git on macOS, Ubuntu Linux and Windows.'
---

# 安装 Git **(FREE)**

要开始为极狐GitLab 项目做出贡献，您必须在您的计算机上安装适当的 Git 客户端。关于[安装 Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) 的信息可以在 Git 官方网站上找到。

## 支持的操作系统

Git 适用于以下操作系统：

- [macOS](#macos)
- [Ubuntu Linux](#ubuntu-linux)
- [Microsoft Windows](#windows)

### macOS

macOS 提供了一个 Git 版本。您可以使用这个版本，或者通过从项目网站下载最新版本的 Git 在 macOS 上安装它。我们建议使用 [Homebrew](https://brew.sh/index.html) 安装 Git。使用 Homebrew，您可以访问大量库和应用程序，并为您管理它们的依赖关系。

先决条件：

- Homebrew 和 Xcode 的 15 GB 可用磁盘空间。
- 用于任何其它开发库的额外磁盘空间。

在 macOS 上安装 Git：

1. 打开终端并安装 XCode 命令行工具：

   ```shell
   xcode-select --install
   ```

   或者，您可以通过 macOS App Store 安装整个 [XCode](https://developer.apple.com/xcode/) 包。

1. 选择 **安装**，下载并安装 XCode 命令行工具。
1. 按照 [Homebrew 官方安装说明](https://brew.sh/index.html)安装 Homebrew。
1. 通过从终端运行 `brew install git` 来安装 Git。
1. 在终端中，验证 Git 是否在您的计算机上运行：

   ```shell
   git --version
   ```

### Ubuntu Linux

在 Ubuntu 和其他 Linux 操作系统上，使用内置的包管理器安装 Git：

1. 打开终端并运行以下命令，从官方维护的包存档中安装最新的 Git：

   ```shell
   sudo apt-add-repository ppa:git-core/ppa
   sudo apt-get update
   sudo apt-get install git
   ```

1. 要验证 Git 是否可以在您的计算机上运行，请运行：

   ```shell
   git --version
   ```

### Windows

前往 [Git 网站](https://git-scm.com/)，然后下载并安装适用于 Windows 的 Git。

## 安装 Git 后

在您的计算机上成功安装 Git 后，请阅读[向极狐GitLab 添加 SSH 密钥](../../../user/ssh.md)。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
