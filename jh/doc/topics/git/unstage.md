---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
comments: false
---

# 取消暂存 **(FREE)**

- 要从暂存区删除文件，请使用重置 HEAD，其中 HEAD 是当前分支的最后一次提交。这将取消暂存文件但保留修改。

  ```shell
  git reset HEAD <file>
  ```

- 要将文件恢复到我们可以使用的更改之前的状态：

  ```shell
  git checkout -- <file>
  ```

- 要从磁盘和仓库中删除文件，请使用 `git rm`。要删除目录，请使用 `-r` 标志：

  ```shell
  git rm '*.txt'
  git rm -r <dirname>
  ```

- 如果我们想从仓库中删除一个文件但将其保留在磁盘上，假设我们忘记将它添加到我们的 `.gitignore` 文件中，然后使用 `--cache`：

  ```shell
  git rm <filename> --cache
  ```
