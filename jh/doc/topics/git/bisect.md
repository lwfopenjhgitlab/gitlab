---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
comments: false
---

# Bisect **(FREE)**

- 查找引入了 bug 的提交
- 通过消除流程工作
- 指定一个已知的好和坏的 revision 来开始

## Bisect 工作流示例

1. 开始 bisect 流程
1. 输入错误的 revision（通常是最新的提交）
1. 输入一个好的 revision（提交/分支）
1. 运行代码查看 bug 是否还存在
1. 告诉 bisect 结果
1. 重复前两步，直到找到有问题的提交

## 设置

```shell
  mkdir bisect-ex
  cd bisect-ex
  touch index.html
  git add -A
  git commit -m "starting out"
  vi index.html
  # Add all good
  git add -A
  git commit -m "second commit"
  vi index.html
  # Add all good 2
  git add -A
  git commit -m "third commit"
  vi index.html
```

```shell
  # Add all good 3
  git add -A
  git commit -m "fourth commit"
  vi index.html
  # This looks bad
  git add -A
  git commit -m "fifth commit"
  vi index.html
  # Really bad
  git add -A
  git commit -m "sixth commit"
  vi index.html
  # again just bad
  git add -A
  git commit -m "seventh commit"
```

## 命令

```shell
  git bisect start
  # Test your code
  git bisect bad
  git bisect next
  # Say yes to the warning
  # Test
  git bisect good
  # Test
  git bisect bad
  # Test
  git bisect good
  # done
  git bisect reset
```
