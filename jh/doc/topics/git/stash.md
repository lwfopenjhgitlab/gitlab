---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
comments: false
---

# Git Stash **(FREE)**

我们使用 `git stash` 来存储我们的更改，当它们还没有准备好提交时，但我们必须更改到不同的分支。

- 隐藏（stash）：

  ```shell
  git stash save
  # or
  git stash
  # or with a message
  git stash save "this is a message to display on the list"
  ```

- 应用隐藏（stash），继续处理它：

  ```shell
  git stash apply
  # or apply a specific one from out stack
  git stash apply stash@{3}
  ```

- 每次保存一个存储时，它都会被堆叠，所以通过使用 `list`，可以看到所有的存储。

  ```shell
  git stash list
  # or for more information (log methods)
  git stash list --stat
  ```

- 要清理堆栈，请手动删除：

  ```shell
  # drop top stash
  git stash drop
  # or
  git stash drop <name>
  # to clear all history we can use
  git stash clear
  ```

- 应用并删除一个命令：

  ```shell
  git stash pop
  ```

- 如果我们遇到冲突，请重置或提交我们的更改。
- 通过 `pop` 引发的冲突之后不会丢弃存储。

## Git Stash 工作流示例

1. 修改文件
1. 暂存文件
1. 隐藏（Stash）文件
1. 查看我们的存储清单
1. 通过状态确认没有待处理的更改
1. 用 pop 申请
1. 查看列表以确认更改

```shell
# Modify edit_this_file.rb file
git add .

git stash save "Saving changes from edit this file"

git stash list
git status

git stash pop
git stash list
git status
```
