---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page，see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: how-tos
---

# 在功能分支上开发 **(FREE)**

我们鼓励使用最小可行修改 (MVC)。但是，可行的修改并不是总是小的。在这种情况下，建立一个专用的功能分支很有用。
人们可以贡献 MR 到功能分支而不影响默认分支<!--[默认分支](../../user/project/repository/branches/default.md)-->的功能。

一旦完成开发分支的工作，功能分支就可以最终合并到默认分支。

每当一个 MVC 需要多个 MR 时，极狐Gitlab 就会经常执行这个过程。

## 使用案例

许多成员于每月贡献代码。在这种情况下，提交 MR 到功能分支会比默认分支<!--[默认分支](../../user/project/repository/branches/default.md)-->更高效。

在这种情况下，功能分支可能为 `release-X-Y`。假设分支 `release-X-Y` 已经存在，您可以针对该分支建立一个 MR，通过以下步骤：

1. 切换到默认分支<!--[默认分支](../../user/project/repository/branches/default.md)-->（这里是 `main`）：

   ```shell
   git checkout main
   ```

2. 确保更新代码库至最新版本：

   ```shell
   git fetch
   git pull
   ```

3. 切换到功能分支：

   ```shell
   git checkout release-x-y
   ```

4. 基于功能分支（`release-x-y`）创建一个新分支（`test-branch`）：

   ```shell
   git checkout -b test-branch release-x-y
   ```

   您现在处于分支 `test-branch`。

5. 在分支 `test-branch` 上修改。
6. 修改完成后，暂存到 `test-branch`：

   ```shell
   git add .
   ```

7. 提交您的修改：

   ```shell
   git commit -m "Some good reason"
   ```

8. 推送您的修改到代码库：

   ```shell
   git push --set-upstream origin test-branch
   ```

9. 在浏览器中打开代码库。<!--在这里，代码库是 `about-gitlab-cn`，访问路径是 `https://jihulab.com/jihulab/marketing/digital-experience/about-gitlab-cn`-->
   
   <!--
   如果需要，请登录到极狐 GitLab。您应该可以看到 **新建合并请求** 按钮。

   ![Create merge request](img/create_merge_request_v13_1.png)-->

10. 在点击 **新建合并请求** 之后，在页面中，您可以选择 **源分支** 和 **目标分支**。<!--在屏幕截图中, 我们选择了 `test-branch` 作为源分支， `release-13-0` 作为目标分支。-->
	
	<!--
   ![Modify branches](img/modify_branches_v13_1.png)-->    

11. 选择完源分支和目标分支后，点击 **Compare branches and continue**。您会看到一个类似以下的条目：

	   ```plaintext
	   New Merge Request
	
	   从 test-branch 到 release-13-0
	   ```

       一个类似这样的条目，确认了合并请求的目的。

12. 在 **New Merge Request** 页面上添加任何额外的修改，然后点击 **Create 合并请求**。

13. 在新的合并请求中， 找到 **Request to merge**。 页面上显示了一个类似以下的条目:

	   ```plaintext
	   请求合并 test-branch 入 release-13-0
	   ```
		
      这确认了你建立的这个 MR 将会合并到一个特殊分支，而不是默认分支<!--[默认分支](../../user/project/repository/branches/default.md)-->。

14. 像其它 MR 一样处理修改。
15. 当您的 MR 通过之后，且有人合并了这个 MR，就能确认您的工作已经被集成到了这个功能分支中。
当这个功能分支准备好后，它就能合并到默认分支<!--[默认分支](../../user/project/repository/branches/default.md)-->。
