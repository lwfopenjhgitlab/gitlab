---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
comments: false
---

# Git Add **(FREE)**

将内容添加到索引或暂存区域。

- 添加文件列表：

  ```shell
  git add <files>
  ```

- 添加所有文件，包括已删除的文件：

  ```shell
  git add -A
  ```

- 在当前目录中添加所有文本文件：

  ```shell
  git add *.txt
  ```

- 在项目中添加所有文本文件：

  ```shell
  git add "*.txt*"
  ```

- 添加目录中的所有文件：

  ```shell
  git add views/layouts/
  ```
