---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
comments: false
---

# 合并冲突 **(FREE)**

- 经常发生
- 学习解决冲突很难
- 熟能生巧
- 修复冲突后强制推送。小心处理！

## 合并冲突工作流示例

1. 检出一个新分支并编辑 `conflicts.rb`。添加 “Line4” 和 “Line5”。
1. 提交并推送。
1. 查看 `main` 并编辑 `conflicts.rb`。在 “Line3” 下方添加 “Line6” 和 “Line7”。
1. 提交并推送到 `main`。
1. 创建一个合并请求并观察它失败。
1. 使用 `main` 重新定义我们的新分支。
1. 修复 `conflicts.rb` 文件的冲突。
1. 暂存文件并继续变基。
1. 强制推送更改。
1. 最后继续合并请求。

```shell
git checkout -b conflicts_branch

# vi conflicts.rb
# Add 'Line4' and 'Line5'

git commit -am "add line4 and line5"
git push origin conflicts_branch

git checkout main

# vi conflicts.rb
# Add 'Line6' and 'Line7'
git commit -am "add line6 and line7"
git push origin main
```

在 Web UI 上创建合并请求，并显示冲突警告。

```shell
git checkout conflicts_branch
git fetch
git rebase main

# Fix conflicts by editing the files.

git add conflicts.rb
# No need to commit this file

git rebase --continue

# Remember that we have rewritten our commit history so we
# need to force push so that our remote branch is restructured
git push origin conflicts_branch -f
```

## 注意

- 何时使用 `git merge` 以及何时使用 `git rebase`
- 使用 `main` 更新分支时变基
- 将更改从功能分支带到 `main` 分支时合并
- 参考：<https://www.atlassian.com/git/tutorials/merging-vs-rebasing>