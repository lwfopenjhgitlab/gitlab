---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
comments: false
---

# Subtree **(FREE)**

- 当有嵌套仓库时使用。
- 依赖数量太大时不推荐。
- 对于这些情况，我们需要一个依赖控制系统。
- 命令非常长，因此需要别名。

## Subtree 别名

- 添加：`git subtree add --prefix <target-folder> <url> <branch> --squash`
- 拉取：`git subtree pull --prefix <target-folder> <url> <branch> --squash`
- 推送：`git subtree add --prefix <target-folder> <url> <branch>`
- 扩展：`git config alias.sbp 'subtree pull --prefix st /
  git@gitlab.com:balameb/subtree-nested-example.git master --squash'`

```shell
  # Add an alias
  # Add
  git config alias.sba 'subtree add --prefix st /
  git@gitlab.com:balameb/subtree-nested-example.git master --squash'
  # Pull
  git config alias.sbpl 'subtree pull --prefix st /
  git@gitlab.com:balameb/subtree-nested-example.git master --squash'
  # Push
  git config alias.sbph 'subtree push --prefix st /
  git@gitlab.com:balameb/subtree-nested-example.git master'

  # Adding this subtree adds a st dir with a readme
  git sba
  vi st/README.md
  # Edit file
  git status shows differences

```

```shell
  # Adding, or committing won't change the sub repo at remote
  # even if we push
  git add -A
  git commit -m "Adding to subtree readme"

  # Push to subtree repo
  git sbph
  # now we can check our remote sub repo
```
