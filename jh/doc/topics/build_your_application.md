---
stage: 
group: 
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 构建您的应用 **(FREE)**

将您的源代码添加到仓库，创建合并请求以检入代码，并使用 CI/CD 生成您的应用程序。在您的应用程序中包含包并将其输出到各种环境。

- [仓库](../user/project/repository/index.md)
- [合并请求](../user/project/merge_requests/index.md)
- [CI/CD](../ci/index.md)
- [软件包与镜像库](../user/packages/index.md)

<!--
- [Application infrastructure](../user/infrastructure/index.md)
-->
