---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: index
---

# Mono **(FREE)**

[Mono](https://jihulab.com/gitlab-cn/mono-client) 是适配极狐GitLab 的多仓库管理工具。Mono 以 Git 为基础构建，可以在需要时轻松整合管理多个代码仓库。

## 快速入门

以下内容可以帮助您开始使用 Mono：

- [如何安装 Mono 与基本工作流程](getting-started.md)
- [Mono 命令行选项](command-line-options.md)
