---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
description: 'This article describes how to install Git on macOS, Ubuntu Linux and Windows.'
---

# 快速入门 **(FREE)**


本文介绍如何安装 Mono 工具，并提供一个基本工作流示例。

## 安装准备

准备好包含 manifest 文件（默认为 `default.xml`）的项目，作为 mono 工具的初始化项目。您可以在 manifest 文件中配置要通过 mono 管理的多代码仓库，示例如下：

```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>

  <remote  name="origin" fetch=".." />
  <default revision="main"
           remote="origin"
           sync-j="4" />
  <project path="local-test-mono/repo1" name="repo1" groups="group1" />
  <project path="local-test-mono/repo2" name="repo2" groups="group2" />
</manifest>
```

## 安装步骤

安装 mono 工具并完成初始化的步骤如下：

1. 使用以下命令，在当前目录下载 mono 安装脚本：

    ```
    mkdir -p ~/.bin
    PATH="${HOME}/.bin:${PATH}"
    curl https://jihulab.com/gitlab-cn/mono-client/-/raw/main-jh/mono?inline=false > ~/.bin/mono
    chmod a+rx ~/.bin/mono
    ```
    
1. 使用 [init 命令](../mono/command-line-options.md#init)，安装并初始化 mono 工具。假设初始化项目为 JiHuLab.com 上的 `example-group/root-project` 项目，则初始化命令为：
    
    ```
    mono init -u git@jihulab.com:example-group/root-project.git
    ```          

1. 使用 [sync 命令](../mono/command-line-options.md#sync)，下载初始化项目的 manifest 文件中指定的仓库：

    ```
    mono sync
    ```


1. 下载成功后，使用 info 命令可以查看本地项目的信息，例如：

	```
	$ mono info
	Manifest branch: main
	Manifest merge branch: refs/heads/main
	Manifest groups: default,platform-linux
	----------------------------
	Project: test-mono/repo1
	Mount path: /home/Deer/test-mono/local-test-mono/repo1
	Current revision: f20311818185d6e82e5cf6b9758f2fab37409af1
	Manifest revision: main
	Local Branches: 0
	----------------------------
	Project: test-mono/repo2
	Mount path: /home/Deer/test-mono/local-test-mono/repo2
	Current revision: 3695cbd74d2bf4fdc49d91380494dba25e78338f
	Manifest revision: main
	Local Branches: 0
	----------------------------
	```

## 基本工作流

安装完成后，使用 Mono 工具与代码库交互的基本工作流如下：

1. 使用 [start 命令](../mono/command-line-options.md#start)，在本地创建主题分支，例如在项目 `local-test-mono/repo1` 中创建分支 `feature30`：

    ```
    cd local-test-mono/repo1
    mono start feature30 .
    ```

1. 进行代码变更后，使用以下命令暂存代码变更：

    ```
    git add
    ```

1. 使用 [status 命令](../mono/command-line-options.md#status)和 [diff 命令](../mono/command-line-options.md#diff)，分别查看文件变更状态和差异：

    ```
    mono status
    mono diff
    ```

1. 使用以下命令，提交代码变更：

    ```
    git commit -m 'commit message'    
    ```
    
1. 使用 [sync 命令](../mono/command-line-options.md#sync)拉取远端代码，检测是否有代码冲突；使用 [upload 命令](../mono/command-line-options.md#upload) 推送代码，并在极狐GitLab 上自动创建合并请求。

    ```
    mono sync
    mono upload
    ```
