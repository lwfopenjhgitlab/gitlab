---
stage: 
group: 
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 部署并发布您的应用 **(FREE)**

在内部或向公众部署您的应用程序。使用标志以增量方式发布功能。

## 部署

将您的应用程序被部署到其最终的目标基础架构，部署是软件交付过程的步骤。

### 使用 Auto DevOps 进行部署

[Auto DevOps](autodevops/index.md) 是一个基于 CI/CD 的自动化工作流程，支持整个软件供应链：使用极狐GitLab CI/CD 构建、测试、lint、打包、部署、保护和监控应用程序，它提供了一组即用型模板，可服务于绝大多数用例。

[Auto Deploy](autodevops/stages.md#auto-deploy) 是专门用于使用极狐GitLab CI/CD 进行软件部署的 DevOps 阶段。

### 将应用程序部署到 Kubernetes 集群

通过极狐GitLab 和 Kubernetes 之间的广泛集成，您可以使用[极狐GitLab 代理](../user/clusters/agent/install/index.md)，将应用程序安全地部署到 Kubernetes 集群。

#### GitOps 部署

使用[用于 Kubernets 的极狐GitLab 代理](../user/clusters/agent/install/index.md)，您可以执行[基于拉取的 Kubernetes manifests 部署](../user/clusters/agent/gitops.md)，提供了一种可扩展、安全且云原生的方法来管理 Kubernetes 部署。

#### 从极狐GitLab CI/CD 部署到 Kubernetes

使用[极狐GitLab Kubernetes 代理](../user/clusters/agent/install/index.md)，您可以从极狐GitLab CI/CD 执行[基于推送的部署](../user/clusters/agent/ci_cd_workflow.md)。该代理在极狐GitLab 和您的 Kubernetes 集群之间提供安全可靠的连接。

<!--
### Deploy to AWS with GitLab CI/CD

GitLab provides Docker images that you can use to run AWS commands from GitLab CI/CD, and a template to
facilitate [deployment to AWS](../ci/cloud_deployment). Moreover, Auto Deploy has built-in support
for EC2 and ECS deployments.
-->

### 使用极狐GitLab CI/CD 进行一般软件部署

您可以使用极狐GitLab CI/CD 来定位极狐GitLab Runner 可访问的任何类型的基础架构。[用户和预定义环境变量](../ci/variables/index.md) 和 CI/CD 模板支持设置大量部署策略。

## 环境

要跟踪您的部署并深入了解您的基础架构，我们建议将它们连接到[一个极狐GitLab 环境](../ci/environments/index.md)。

## 发布

使用极狐GitLab [发布](../user/project/releases/index.md)，来规划、构建和交付您的应用程序。

### 功能标志

使用[功能标志](../operations/feature_flags.md)，来控制和战略性地推出应用程序部署。

<!--
## Deploy to Google Cloud

GitLab [Cloud Seed](../cloud_seed/index.md) is an open-source Incubation Engineering program that
enables you to set up deployment credentials and deploy your application to Google Cloud Run with minimal friction.
-->
