---
stage: 
group: 
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 计划和跟踪工作 **(FREE)**

通过创建需求、议题和史诗来规划您的工作。使用里程碑安排工作并跟踪团队的时间。了解如何通过快速操作节省时间，了解极狐GitLab 如何渲染 Markdown 文本，了解如何使用 Git 与极狐GitLab 进行交互。

## 基本工作流功能

每个人都需要日常使用的计划功能。

- [键盘快捷键](../user/shortcuts.md)
- [Markdown](../user/markdown.md)
- [快速操作](../user/project/quick_actions.md)
- [待办事项列表](../user/todos.md)
- [使用 Git](../topics/git/index.md)

## 团队计划

作为一个团队完成工作。

- [评论和主题](../user/discussions/index.md)
- [客户关系（CRM）](../user/crm/index.md)
  - [联系人](../user/crm/index.md#contacts)
  - [组织](../user/crm/index.md#organizations)
- [议题](../user/project/issues/index.md)
- [迭代](../user/group/iterations/index.md)
- [标记](../user/project/labels.md)
- [里程碑](../user/project/milestones/index.md)
- [需求](../user/project/requirements/index.md)
- [Tasks](../user/tasks.md)
- [时间跟踪](../user/project/time_tracking.md)
- [Wikis](../user/project/wiki/index.md)

## 组合管理

跨团队协调您的工作。

- [史诗](../user/group/epics/index.md)
  - [多级史诗](../user/group/epics/manage_epics.md#multi-level-child-epics)
  - [史诗看板](../user/group/epics/epic_boards.md)
  - [查看健康状态](../user/project/issues/managing_issues.md#health-status)
- [路线图](../user/group/roadmap/index.md)
- [规划层次结构](../user/group/planning_hierarchy/index.md)

