---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Auto DevOps 要求 **(FREE)**

在启用 [Auto DevOps](index.md) 之前，我们建议您为部署做好准备。或者，您可以使用 Auto DevOps 来构建和测试您的应用，然后再配置部署。

要准备部署：

1. 定义[部署策略](#auto-devops-部署策略)。
1. 准备[基础域名](#auto-devops-基础域名)。
1. 定义要将其部署到的位置：

   1. [Kubernetes](#auto-devops-requirements-for-kubernetes).
   1. [Bare metal](#auto-devops-requirements-for-bare-metal).

完成后：

1. [启用 Auto DevOps](index.md#启用或禁用-auto-devops)。

<a id="auto-devops-deployment-strategy"></a>

## Auto DevOps 部署策略

使用 Auto DevOps 部署应用时，请选择最适合您需求的持续部署策略：

| 部署策略 | 设置 | 方法 |
|--|--|--|
| **持续部署到生产环境** | 启用 [Auto Deploy](stages.md#auto-deploy) 并将默认分支持续部署到生产环境。 | 持续部署到生产环境。 |
| **使用定时增量部署持续部署到生产环境** | 将 `INCREMENTAL_ROLLOUT_MODE` 变量设置为 `timed`。 | 持续部署到生产环境，部署之间有 5 分钟的延迟。 |
| **自动部署到 staging 环境，手动部署到生产环境** | 将 `STAGING_ENABLED` 设置为`1`，将 `INCREMENTAL_ROLLOUT_MODE` 设置为 `manual`。 | 默认分支持续部署到 staging 并持续交付到生产环境。 |

您可以在启用 Auto DevOps 或更高版本时选择部署方法：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏，选择 **设置 > CI/CD**。
1. 展开 **Auto DevOps**。
1. 选择部署策略。
1. 选择 **保存修改**。

NOTE:
使用蓝绿部署技术将停机时间和风险降至最低。

<a id="auto-devops-base-domain"></a>

## Auto DevOps 基础域名

Auto DevOps 基础域名需要使用 [Auto Review Apps](stages.md#auto-review-apps)、[Auto Deploy](stages.md#auto-deploy) 和 [Auto Monitoring](stages.md#auto-monitoring)。

要定义基础域名，可以：

- 在项目、群组或实例级别：转到您的集群设置并添加到那里。
- 在项目或群组级别：将其添加为环境变量：`KUBE_INGRESS_BASE_DOMAIN`。
- 在实例级别：转到 **主菜单 > 管理员 > 设置 > CI/CD > 持续集成和交付** 并添加到那里。

基础域名变量 `KUBE_INGRESS_BASE_DOMAIN` 遵循与其他环境变量相同的优先顺序。

如果您未在项目和组中指定基础域名，Auto DevOps 将使用实例范围的 **Auto DevOps 域名**。

Auto DevOps 需要与基础域名匹配的通配符 DNS A 记录。对于 `example.com` 的基础域名，您需要一个 DNS 条目，例如：

```plaintext
*.example.com   3600     A     1.2.3.4
```

在这种情况下，部署的应用从 `example.com` 提供服务，而 `1.2.3.4` 是负载均衡器的 IP 地址，通常是 NGINX（[参见要求](requirements.md)）。设置 DNS 记录超出了本文档的范围；请咨询您的 DNS 提供商以获取信息。

或者，您可以使用免费的公共服务，如 [nip.io](https://nip.io)，无需任何配置即可提供自动通配符 DNS。对于 [nip.io](https://nip.io)，将 Auto DevOps 基域设置为 `1.2.3.4.nip.io`。

完成设置后，所有请求都会到达负载均衡器，负载均衡器将请求路由到运行应用程序的 Kubernetes pod。

## Auto DevOps requirements for Kubernetes

要充分利用 Kubernetes 的 Auto DevOps，您需要：

- **Kubernetes**（[Auto Review Apps](stages.md#auto-review-apps)、[Auto Deploy](stages.md#auto-deploy) 和 [Auto Monitoring](stages.md#auto-monitoring)）

  要启用部署，您需要：

  1. 用于您的项目的 Kubernetes 1.12+ 集群。对于 Kubernetes 1.16+ 集群，您必须对 [Auto Deploy for Kubernetes 1.16+](stages.md#kubernetes-116) 进行额外配置。
  1. 对于外部 HTTP 流量，需要一个 Ingress 控制器。对于常规部署，任何 Ingress 控制器都应该可以工作，但从 14.0 版本开始，金丝雀部署需要 NGINX Ingress。您可以通过 GitLab 集群管理项目模板，或使用 [`ingress-nginx`](https://github.com/kubernetes/ingress-nginx/tree/master/charts/ingress-nginx) Helm chart。

     NOTE:
     要在使用 Prometheus 集群集成时显示指标，您必须[启用 Prometheus 指标](https://github.com/kubernetes/ingress-nginx/tree/master/charts/ingress-nginx#prometheus-metrics)。

     使用自定义 chart 进行部署时，您还必须使用 Prometheus [注释](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/)要被 Prometheus 抓取的 Ingress manifest，使用 `prometheus.io/scrape: "true"` 和`prometheus.io/port: "10254"`。

     NOTE:
     如果您的集群安装在裸金属上，请参阅[裸金属的 Auto DevOps 要求](#auto-devops-requirements-for-bare-metal)。

- **基础域名**（[Auto Review Apps](stages.md#auto-review-apps)、[Auto Deploy](stages.md#auto-deploy) 和 [Auto Monitoring](stages.md#auto-monitoring)）

  您必须 [指定 Auto DevOps 基础域名](#auto-devops-基础域名)，您的所有 Auto DevOps 应用都使用它。此域名必须配置通配符 DNS。

- **GitLab Runner**（所有 stages）

  您的 runner 必须配置为运行 Docker，通常使用 Docker 或 Kubernetes executor，并启用特权模式。
  Runner 不需要安装在 Kubernetes 集群中，但 Kubernetes executor 易于使用并自动扩展。 您也可以使用 [Docker Machine](https://docs.gitlab.com/runner/executors/docker_machine.html) 将基于 Docker 的 runner 配置为自动缩放。

  Runner 应注册为整个实例的共享 runner，或分配给特定项目的特定 runner。

- **Prometheus**（[Auto Monitoring](stages.md#auto-monitoring)）

  要启用 Auto Monitoring，您需要在集群内部或外部安装 Prometheus，并配置为抓取您的 Kubernetes 集群。如果您已经配置了极狐GitLab 与 Kubernetes 的集成，则可以通过启用 Prometheus 集群集成来指示极狐GitLab 查询集群内 Prometheus。

  必须为项目激活 Prometheus 集成，或者在群组或实例级别激活。

  要获取响应指标（除了系统指标），您必须配置 Prometheus 监控 NGINX。

- **cert-manager**（可选，用于 TLS/HTTPS）

  要为您的应用启用 HTTPS 端点，您可以 [安装 cert-manager](https://cert-manager.io/docs/installation/supported-releases/)，这是一个有助于颁发证书的原生 Kubernetes 证书管理控制器。在集群上安装 cert-manager 会发出 [Let's Encrypt](https://letsencrypt.org/) 证书，并确保证书有效且是最新的。

如果您没有配置 Kubernetes 或 Prometheus，则 [Auto Review Apps](stages.md#auto-review-apps)、[Auto Deploy](stages.md#auto-deploy) 和 [Auto Monitoring](stages.md#auto-monitoring) 被跳过。

在满足所有要求后，您可以[启用 Auto DevOps](index.md#启用或禁用-auto-devops)。

<!--
## Auto DevOps requirements for Amazon ECS

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/208132) in GitLab 13.0.

You can choose to target [AWS ECS](../../ci/cloud_deployment/index.md) as a deployment platform instead of using Kubernetes.

To get started on Auto DevOps to AWS ECS, you must add a specific CI/CD variable.
To do so, follow these steps:

1. In GitLab, on the top bar, select **Menu > Projects** and find your project.
1. On the left sidebar, select **Settings > CI/CD**.
1. Expand **Auto DevOps**.
1. Specify which AWS platform to target during the Auto DevOps deployment
   by adding the `AUTO_DEVOPS_PLATFORM_TARGET` variable with one of the following values:
   - `FARGATE` if the service you're targeting must be of launch type FARGATE.
   - `ECS` if you're not enforcing any launch type check when deploying to ECS.

When you trigger a pipeline, if you have Auto DevOps enabled and if you have correctly
[entered AWS credentials as variables](../../ci/cloud_deployment/index.md#deploy-your-application-to-the-aws-elastic-container-service-ecs),
your application is deployed to AWS ECS.

If you have both a valid `AUTO_DEVOPS_PLATFORM_TARGET` variable and a Kubernetes cluster tied to your project,
only the deployment to Kubernetes runs.

WARNING:
Setting the `AUTO_DEVOPS_PLATFORM_TARGET` variable to `ECS` triggers jobs
defined in the [`Jobs/Deploy/ECS.gitlab-ci.yml` template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Deploy/ECS.gitlab-ci.yml).
However, it's not recommended to [include](../../ci/yaml/index.md#includetemplate)
it on its own. This template is designed to be used with Auto DevOps only. It may change
unexpectedly causing your pipeline to fail if included on its own. Also, the job
names within this template may also change. Do not override these jobs' names in your
own pipeline, as the override stops working when the name changes.

## Auto DevOps requirements for Amazon EC2

[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/216008) in GitLab 13.6.

You can target [AWS EC2](../../ci/cloud_deployment/index.md)
as a deployment platform instead of Kubernetes. To use Auto DevOps with AWS EC2, you must add a
specific CI/CD variable.

For more details, see [Custom build job for Auto DevOps](../../ci/cloud_deployment/index.md#custom-build-job-for-auto-devops)
for deployments to AWS EC2.
-->

## Auto DevOps requirements for bare metal

根据 [Kubernetes Ingress-NGINX 文档](https://kubernetes.github.io/ingress-nginx/deploy/baremetal/)：

> 在传统的云环境中，网络负载均衡器可按需使用，单个 Kubernetes manifest 足以为 NGINX Ingress 控制器与外部客户端提供单点联系，并间接与集群内运行的任何应用联系。
裸金属环境缺乏这种资源，需要稍微不同的设置来为外部消费者提供相同类型的访问。

上面链接的文档解释了这个问题并提出了可能的解决方案，例如：

- 通过 [MetalLB](https://github.com/metallb/metallb)。
- 通过 [PorterLB](https://github.com/kubesphere/porterlb)。
