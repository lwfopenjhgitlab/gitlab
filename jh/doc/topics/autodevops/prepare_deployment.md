---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 准备 Auto DevOps 进行部署 **(FREE)**

如果您在未设置基本域名和部署策略的情况下启用 Auto DevOps，极狐GitLab 将无法直接部署您的应用程序。因此，我们建议您在启用 Auto DevOps 之前准备好它们。

## 部署策略

使用 Auto DevOps 部署应用程序时，请选择最适合您需求的[持续部署策略](../../ci/introduction/index.md)：

| 部署策略 | 设置 | 方法 |
|--|--|--|
| **持续部署到生产** | 启用 [Auto Deploy](stages.md#auto-deploy) 并将默认分支持续部署到生产环境。 | 持续部署到生产。 |
| **使用定时增量部署持续部署到生产环境** | 将 [`INCREMENTAL_ROLLOUT_MODE`](customize.md#timed-incremental-rollout-to-production) 变量设置为 `timed`。 | 持续部署到生产环境，两次部署之间有 5 分钟的延迟。 |
| **自动部署到 staging，手动部署到生产** | 将 [`STAGING_ENABLED`](customize.md#deploy-policy-for-staging-and-production-environments) 设置为 `1`，将 [`INCREMENTAL_ROLLOUT_MODE`](customize.md#incremental-rollout-to-production) 设置为 `manual`。 | 默认分支持续部署到 staging 并持续交付到生产环境。 |

您可以在启用 Auto DevOps 或更高版本时选择部署方法：

1. 在极狐GitLab 中，转到项目的 **设置 > CI/CD > Auto DevOps**。
1. 选择部署策略。
1. 选择 **保存修改**。

NOTE:
使用[蓝绿部署](../../ci/environments/incremental_rollouts.md#blue-green-deployment)技术来最大限度地减少停机时间和风险。

## Auto DevOps 基础域名

[Auto Review Apps](stages.md#auto-review-apps)、[Auto Deploy](stages.md#auto-deploy) 和 [Auto Monitoring](stages.md#auto-monitoring) 要求使用 Auto DevOps 基础域名。

要定义基础域名：

- 在项目、群组或实例级别：转到您的集群设置并将其添加到那里。
- 在项目或群组级别：将其添加为环境变量：`KUBE_INGRESS_BASE_DOMAIN`。
- 在实例级别：转到 **菜单 > 管理员 > 设置 > CI/CD > 持续集成和交付** 并将其添加到那里。

基础域名变量 `KUBE_INGRESS_BASE_DOMAIN` 遵循与其它环境变量相同的优先顺序。

如果您未在项目和群组中指定基础域名，Auto DevOps 将使用实例范围的 **Auto DevOps 域名**。

Auto DevOps 要求通配符 DNS `A` 记录匹配基础域名。对于例如 `example.com` 的基础域名，您需要一个 DNS 条目，例如：

```plaintext
*.example.com   3600     A     1.2.3.4
```

在这种情况下，部署的应用程序从 `example.com` 提供服务，而 `1.2.3.4` 是负载均衡器的 IP 地址，通常是 NGINX（[参见要求](requirements.md)）。
设置 DNS 记录超出了本文档的范围；请咨询您的 DNS 提供商以获取信息。

或者，您可以使用免费的公开服务，如 [nip.io](https://nip.io)，无需任何配置即可提供自动通配符 DNS。对于 [nip.io](https://nip.io)，将 Auto DevOps 基础域名设置为 `1.2.3.4.nip.io`。

完成设置后，所有请求都会到达负载均衡器，它将请求路由到运行应用程序的 Kubernetes pod。
