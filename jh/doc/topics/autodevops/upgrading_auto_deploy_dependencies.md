---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 为较新的 Auto Deploy 依赖项升级部署 **(FREE)**

[Auto Deploy](stages.md#auto-deploy) 是一项将您的应用程序部署到 Kubernetes 集群的功能。它由几个依赖项组成：

- [Auto Deploy 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml)是一组使用 `auto-deploy-image` 的流水线作业和脚本。
- `auto-deploy-image` 是与 Kubernetes 集群通信的执行镜像。
- `auto-deploy-app chart` 是用于部署应用程序的 Helm chart。

`auto-deploy-image` 和 `auto-deploy-app` chart 使用[语义版本控制](https://semver.org/)。
默认情况下，您的 Auto DevOps 项目会继续使用稳定且非破坏性的版本。
但是，这些依赖项可以在极狐GitLab 的主要版本中升级，但需要您升级部署的重大变更。

本指南说明如何使用更新或不同主要版本的 Auto Deploy 依赖项升级您的部署。

<a id="verify-dependency-versions"></a>

## 验证依赖项版本

检查当前版本的过程因您使用的模板而异。首先验证正在使用哪个模板：

- 对于私有化部署版实例，正在使用与极狐GitLab 包捆绑的[稳定 Auto Deploy 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml)。
- 如果满足以下**一个**条件，则使用 [JiHuLab.com 中稳定的 Auto Deploy 模板]：
  - 您的 Auto DevOps 项目没有 `.gitlab-ci.yml` 文件。
  - 您的 Auto DevOps 项目有一个 `.gitlab-ci.yml` 并[包含](../../ci/yaml/index.md#includetemplate) `Auto-DevOps.gitlab-ci.yml` 模板。
- 如果同时满足以下**两个**条件，则使用[最新的 Auto Deploy 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.latest.gitlab-ci.yml)：
  - 您的 Auto DevOps 项目有一个 `.gitlab-ci.yml` 文件并[包含](../../ci/yaml/index.md#includetemplate) `Auto-DevOps.gitlab-ci.yml` 模板。
  - 项目还包括[最新的 Auto Deploy 模板](#early-adopters)。

如果您知道正在使用什么模板：

- `auto-deploy-image` 版本在模板中（例如 `auto-deploy-image:v1.0.3`）。
- `auto-deploy-app` chart 版本位于 auto-deploy-image 仓库中（例如 `version: 1.0.3`）。

## 兼容性

下表解释了极狐GitLab 和 Auto Deploy 依赖项之间的版本兼容性：

| 极狐GitLab 版本   | `auto-deploy-image` 版本 | 备注 |
|------------------|-----------------------------|-------|
| v10.0 到 v14.0  | v0.1.0 到 v2.0.0            | v0 和 v1 auto-deploy-image 后向兼容 |
| v13.4 及更高 | v2.0.0 及更高             | v2 auto-deploy-image 包含重大更改，如[升级指南](#upgrade-deployments-to-the-v2-auto-deploy-image)中所述。 |

您可以在 [Auto Deploy 稳定模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml)中找到当前稳定版本的 auto-deploy-image。

<a id="upgrade-guide"></a>

## 升级指南

使用 Auto DevOps 的项目必须使用极狐GitLab 管理的未修改 chart。
[自定义 chart](customize.md#custom-helm-chart) 不受支持。

### 将部署升级到 v1 `auto-deploy-image`

v1 chart 向后兼容 v0 chart，因此无需更改配置。

<a id="upgrade-deployments-to-the-v2-auto-deploy-image"></a>

### 将部署升级到 v2 `auto-deploy-image`

v2 自动部署镜像包含多个依赖项和架构更改。
如果您的 Auto DevOps 项目有一个使用 v1 `auto-deploy-image` 部署的活动环境，请继续执行以下升级指南。否则，您可以跳过此过程。

#### Kubernetes 1.16+

v2 auto-deploy-image 放弃了对 Kubernetes 1.15 及更低版本的支持。如果您需要升级 Kubernetes 集群，请按照云提供商的说明进行操作。

#### Helm v3

`auto-deploy-image` 使用 Helm 二进制文件来操作发布。
以前，`auto-deploy-image` 使用 Helm v2，它在集群中使用 Tiller。
在 v2 `auto-deploy-image` 中，它使用了不再需要 Tiller 的 Helm v3。

如果您的 Auto DevOps 项目具有使用 v1 `auto-deploy-image` 部署的活动环境，请使用以下步骤升级到使用 Helm v3 的 v2 `auto-deploy-image`：

1. 包含 [Helm 2to3 迁移 CI/CD 模板](https://jihulab.com/gitlab-cn/gitlab/-/raw/master/lib/gitlab/ci/templates/Jobs/Helm-2to3.gitlab-ci.yml)：

   - 如果您在 JiHuLab.com，或 14.0.1 及更高版本上，则此模板已包含在 Auto DevOps 中。
   - 在其它版本的上，您可以修改 `.gitlab-ci.yml` 以包含模板：

     ```yaml
     include:
       - template: Auto-DevOps.gitlab-ci.yml
       - remote: https://gitlab.com/gitlab-org/gitlab/-/raw/master/lib/gitlab/ci/templates/Jobs/Helm-2to3.gitlab-ci.yml
     ```

1. 设置以下 CI/CD 变量：

   - `MIGRATE_HELM_2TO3` 为 `true`。如果此变量不存在，则迁移作业不会运行。
   - `AUTO_DEVOPS_FORCE_DEPLOY_V2` 为 `1`.
   - **可选：**`BACKUP_HELM2_RELEASES` 为 `1`。如果您设置此变量，迁移作业会在名为 `helm-2-release-backups` 的作业产物中保存 1 周的备份。如果您在准备好之前意外删除了 Helm v2 版本，您可以使用 `kubectl apply -f $backup` 从 Kubernetes manifest 文件中恢复此备份。

     WARNING:
     *如果您有公开流水线，请不要使用它*。此产物可以包含 secret，并且任何可以看到您的作业的用户都可以看到。

1. 运行流水线并触发 `<environment-name>:helm-2to3:migrate` 作业。
1. 像往常一样部署您的环境。此部署使用 Helm v3。
1. 如果部署成功，可以安全运行 `<environment-name>:helm-2to3:cleanup`。这会从命名空间中删除所有 Helm v2 发布数据。
1. 移除 `MIGRATE_HELM_2TO3` CI/CD 变量或将其设置为 `false`。您可以使用[环境范围](../../ci/environments/index.md#scope-environments-with-specs)，一次完成一个环境。

#### 集群内 PostgreSQL Channel 2

v2 auto-deploy-image 放弃了对[老式集群内 PostgreSQL](upgrading_postgresql.md) 的支持。
如果您的 Kubernetes 集群仍然依赖它，请使用 [v1 auto-deploy-image](#use-a-specific-version-of-auto-deploy-dependencies) [升级和迁移您的数据](upgrading_postgresql.md)。

#### 金丝雀部署和增量部署的流量路由更改

Auto Deploy 支持高级部署策略，例如[金丝雀部署](customize.md#deploy-policy-for-canary-environments)和[增量部署](../../ci/environments/incremental_rollouts.md)。

以前，`auto-deploy-image` 创建了一项服务，通过更改副本比率来平衡不稳定和稳定之间的流量。在 v2 `auto-deploy-image` 中，使用 [Canary Ingress](https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#canary) 控制流量。

获取更多详细信息，查看 [v2 `auto-deploy-app` chart resource architecture](#v2-chart-resource-architecture)。

如果您的 Auto DevOps 项目在使用 v1 `auto-deploy-image` 部署的 `production` 环境中具有活动的 `canary` 或 `rollout` 版本，请使用以下步骤升级到 v2：

1. 验证您的项目是[使用 v1 `auto-deploy-image`](#verify-dependency-versions)。如果没有，[指定版本](#use-a-specific-version-of-auto-deploy-dependencies)。
1. 如果您正在部署 `canary` 或 `rollout` 部署，请先将其提升为 `production` 并删除不稳定的。
1. 验证您的项目是[使用 v2 `auto-deploy-image`](#verify-dependency-versions)。如果没有，[指定版本](#use-a-specific-version-of-auto-deploy-dependencies)。
1. 在 CI/CD 设置中，添加一个值为 `true` 的 `AUTO_DEVOPS_FORCE_DEPLOY_V2` CI/CD 变量。
1. 创建一个新流水线并运行 `production` 作业，使用 v2 `auto-deploy-app chart` 更新资源架构。
1. 移除 `AUTO_DEVOPS_FORCE_DEPLOY_V2` 变量。

<a id="use-a-specific-version-of-auto-deploy-dependencies"></a>

### 使用特定版本的 Auto Deploy 依赖项

要使用特定版本的 Auto Deploy 依赖项，请指定包含[所需版本的 `auto-deploy-image` 和 `auto-deploy-app`](#verify-dependency-versions) 的先前 Auto Deploy 稳定模板。

例如，如果模板捆绑在 13.3 中，请将 `.gitlab-ci.yml` 更改为：

```yaml
include:
  - template: Auto-DevOps.gitlab-ci.yml
  - remote: https://jihulab.com/gitlab-cn/gitlab/-/raw/v13.3.0-ee/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml
```

<!--
Alternatively, you can use the [v13.12 Auto DevOps templates archive](https://gitlab.com/hfyngvason/auto-devops-v13-12).
-->

### 忽略警告并继续部署

如果您确定新的 chart 版本可以安全部署，您可以添加 `AUTO_DEVOPS_FORCE_DEPLOY_V<major-version-number>` [CI/CD 变量](customize.md#build-and-deployment)来强制继续部署。

例如，如果您想在以前使用 `v0.17.0` chart 的部署上部署 `v2.0.0` chart，请添加 `AUTO_DEVOPS_FORCE_DEPLOY_V2`。

<a id="early-adopters"></a>

## 早期采用者

如果您想使用最新的 Beta 版或不稳定版本的 `auto-deploy-image`，请将最新的 Auto Deploy 模板包含到您的 `.gitlab-ci.yml` 中：

```yaml
include:
  - template: Auto-DevOps.gitlab-ci.yml
  - template: Jobs/Deploy.latest.gitlab-ci.yml
```

WARNING:
使用 Beta 或不稳定的 `auto-deploy-image` 可能会对您的环境造成不可恢复的损害。不要用重要的项目或环境对其进行测试。

## `auto-deploy-app` chart 的资源架构

### v0 和 v1 chart 资源架构

```mermaid
graph TD;
subgraph gl-managed-app
Z[Nginx Ingress]
end
Z[Nginx Ingress] --> A(Ingress);
Z[Nginx Ingress] --> B(Ingress);
subgraph stg namespace
B[Ingress] --> H(...);
end
subgraph prd namespace
A[Ingress] --> D(Service);
D[Service] --> E(Deployment:Pods:app:stable);
D[Service] --> F(Deployment:Pods:app:canary);
D[Service] --> I(Deployment:Pods:app:rollout);
E(Deployment:Pods:app:stable)---id1[(Pods:Postgres)]
F(Deployment:Pods:app:canary)---id1[(Pods:Postgres)]
I(Deployment:Pods:app:rollout)---id1[(Pods:Postgres)]
end
```

<a id="v2-chart-resource-architecture"></a>

### v2 chart 资源架构

```mermaid
graph TD;
subgraph gl-managed-app
Z[Nginx Ingress]
end
Z[Nginx Ingress] --> A(Ingress);
Z[Nginx Ingress] --> B(Ingress);
Z[Nginx Ingress] --> |If canary is present or incremental rollout/|J(Canary Ingress);
subgraph stg namespace
B[Ingress] --> H(...);
end
subgraph prd namespace
subgraph stable track
A[Ingress] --> D[Service];
D[Service] --> E(Deployment:Pods:app:stable);
end
subgraph canary track
J(Canary Ingress) --> K[Service]
K[Service] --> F(Deployment:Pods:app:canary);
end
E(Deployment:Pods:app:stable)---id1[(Pods:Postgres)]
F(Deployment:Pods:app:canary)---id1[(Pods:Postgres)]
end
```

## 故障排除

### 主要版本不匹配警告

如果部署的 chart 的主要版本与前一个不同，则可能无法正确部署新 chart。这可能是由于架构更改。如果发生这种情况，部署作业将失败并显示类似于以下内容的消息：

```plaintext
*************************************************************************************
                                   [WARNING]
Detected a major version difference between the chart that is currently deploying (auto-deploy-app-v0.7.0), and the previously deployed chart (auto-deploy-app-v1.0.0).
A new major version might not be backward compatible with the current release (production). The deployment could fail or be stuck in an unrecoverable status.
...
```

要清除此错误消息并恢复部署，您必须执行以下操作之一：

- 手动[升级 chart 版本](#upgrade-guide)。
- [使用特定 chart 版本](#use-a-specific-version-of-auto-deploy-dependencies)。

### 错误：`missing key "app.kubernetes.io/managed-by": must be set to "Helm"`

如果您的集群有一个使用 v1 `auto-deploy-image` 的部署，您可能会遇到以下错误：

- `Error: rendered manifests contain a resource that already exists. Unable to continue with install: Secret "production-postgresql" in namespace "<project-name>-production" exists and cannot be imported into the current release: invalid ownership metadata; label validation error: missing key "app.kubernetes.io/managed-by": must be set to "Helm"; annotation validation error: missing key "meta.helm.sh/release-name": must be set to "production-postgresql"; annotation validation error: missing key "meta.helm.sh/release-namespace": must be set to "<project-name>-production"`

这是因为之前的部署是使用 Helm2 部署的，与 Helm3 不兼容。
要解决此问题，请按照[升级指南](#upgrade-deployments-to-the-v2-auto-deploy-image)。
