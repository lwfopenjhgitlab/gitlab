---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 自定义 Auto DevOps **(FREE)**

虽然 [Auto DevOps](index.md) 提供了很好的默认设置来帮助您入门，但您几乎可以自定义所有内容以满足您的需求。Auto DevOps 提供从自定义 [buildpacks](#custom-buildpacks) 到 [Dockerfiles](#custom-dockerfile) 和 [Helm charts](#custom-helm-chart) 的所有内容。您甚至可以将完整的 [CI/CD 配置](#customizing-gitlab-ciyml)复制到您的项目中，启用 staging 和金丝雀部署<!--，[使用 API 管理 Auto DevOps](customize.md#extend-auto-devops-with-the-api) -->等。

<a id="custom-buildpacks"></a>

## 自定义构建包

如果您的项目的自动构建包检测失败，或者您需要对构建进行更多控制，您可以自定义用于构建的构建包。

### 使用 Cloud Native Buildpacks 自定义构建包

指定以下二者之一：

- 具有任何 [`pack` 的 URI 规范格式](https://buildpacks.io/docs/app-developer-guide/specify-buildpacks/)的 CI/CD 变量 `BUILDPACK_URL`。
- 一个 [`project.toml` 项目描述符](https://buildpacks.io/docs/app-developer-guide/using-project-descriptor/)，其中包含您想要包含的构建包。

### 使用 Herokuish 的自定义构建包

指定以下二者之一：

- CI/CD 变量 `BUILDPACK_URL`。
- 项目根目录下的 `.buildpacks` 文件，每行包含一个 buildpack URL。

构建包 URL 可以指向 Git 仓库 URL 或 tarball URL。
对于 Git 仓库，您可以通过将 `#<ref>` 附加到 Git 仓库 URL 来指向特定的 Git 引用（例如提交 SHA、标签名称或分支名称）。
例如：

- 标签`v142`：`https://github.com/heroku/heroku-buildpack-ruby.git#v142`。
- 分支`mybranch`：`https://github.com/heroku/heroku-buildpack-ruby.git#mybranch`。
- 提交 SHA `f97d8a8ab49`：`https://github.com/heroku/heroku-buildpack-ruby.git#f97d8a8ab49`。

### 多个构建包

Auto DevOps 不完全支持使用多个构建包，因为 Auto Test 不能使用 `.buildpacks` 文件。后端用于解析 `.buildpacks` 文件的构建包 [heroku-buildpack-multi](https://github.com/heroku/heroku-buildpack-multi/) 没有提供必要的命令 `bin/test-compile` 和 `bin/test`。

如果您的目标是仅使用单个自定义构建包，则应提供项目 CI/CD 变量`BUILDPACK_URL`。

<a id="custom-dockerfile"></a>

## 自定义 `Dockerfile`

> 对 `DOCKERFILE_PATH` 的支持引入于 13.2 版本。

如果您的项目在项目仓库的根目录中有一个 `Dockerfile`，Auto DevOps 会基于 Dockerfile 构建一个 Docker 镜像，而不是使用构建包。
这可以更快并产生更小的镜像，特别是如果您的 Dockerfile 基于 [Alpine](https://hub.docker.com/_/alpine/)。

如果您设置 `DOCKERFILE_PATH` CI/CD 变量，Auto Build 会在那里寻找 Dockerfile。

## 将参数传递给 `docker build`

可以使用 `AUTO_DEVOPS_BUILD_IMAGE_EXTRA_ARGS` 项目 CI/CD 变量将参数传递给 `docker build` 命令。例如，要基于 `ruby:alpine` 而不是默认的 `ruby:latest` 构建 Docker 镜像：

1. 将 `AUTO_DEVOPS_BUILD_IMAGE_EXTRA_ARGS` 设置为 `--build-arg=RUBY_VERSION=alpine`。
1. 将以下内容添加到自定义 `Dockerfile` 中：

   ```dockerfile
   ARG RUBY_VERSION=latest
   FROM ruby:$RUBY_VERSION

   # ... put your stuff here
   ```

如果您需要传递复杂的值，例如换行符和空格，请使用 Base64 编码。由于 Auto DevOps 使用参数的方式，这些未编码的复杂值可能会导致转义问题。

WARNING:
如果可能，请避免将 secret 作为 Docker 构建参数传递，因为它们可能会保留在您的镜像中。有关详细信息，请参阅[这个关于 secret 最佳实践的讨论](https://github.com/moby/moby/issues/13490)。

<a id="custom-container-image"></a>

## 自定义容器镜像

默认情况下，[Auto Deploy](stages.md#auto-deploy) 部署由 [Auto Build](stages.md#auto-build) 构建并推送到极狐GitLab 容器镜像库的容器镜像。
您可以通过定义特定变量来覆盖：

| 条目 | 默认值 | 可以由以下特定变量覆盖 |
| ----- | -----   | -----    |
| 镜像路径 | `$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG`（分支流水线）。`$CI_REGISTRY_IMAGE`（标签流水线）。 | `$CI_APPLICATION_REPOSITORY` |
| 镜像标签 | `$CI_COMMIT_SHA`（分支流水线）。`$CI_COMMIT_TAG`（标签流水线）。 | `$CI_APPLICATION_TAG` |

这些变量也会影响 Auto Build 和 Auto Container Scanning。如果您不想构建镜像并将其推送到`$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG`，请考虑仅包含 `Jobs/Deploy.gitlab-ci.yml`，或[禁用 `build` 作业](#disable-jobs)。

如果您使用 Auto Container Scanning 并为 `$CI_APPLICATION_REPOSITORY` 设置一个值，那么您还应该更新 `$CS_DEFAULT_BRANCH_IMAGE`。有关详细信息，请参阅[设置默认分支镜像](../../user/application_security/container_scanning/index.md#setting-the-default-branch-image)。

以下是您的 `.gitlab-ci.yml` 中的示例设置：

```yaml
variables:
  CI_APPLICATION_REPOSITORY: <your-image-repository>
  CI_APPLICATION_TAG: <the-tag>
```

<!--
## Extend Auto DevOps with the API

You can extend and manage your Auto DevOps configuration with GitLab APIs:

- [Settings that can be accessed with API calls](../../api/settings.md#list-of-settings-that-can-be-accessed-via-api-calls),
  which include `auto_devops_enabled`, to enable Auto DevOps on projects by default.
- [Creating a new project](../../api/projects.md#create-project).
- [Editing groups](../../api/groups.md#update-group).
- [Editing projects](../../api/projects.md#edit-project).
-->

## 将 CI/CD 变量转发到构建环境

可以使用 `AUTO_DEVOPS_BUILD_IMAGE_FORWARDED_CI_VARIABLES` CI/CD 变量将 CI/CD 变量转发到构建环境。转发的变量应在逗号分隔列表中按名称指定。例如，要转发变量 `CI_COMMIT_SHA` 和 `CI_ENVIRONMENT_NAME`，请将 `AUTO_DEVOPS_BUILD_IMAGE_FORWARDED_CI_VARIABLES` 设置为 `CI_COMMIT_SHA,CI_ENVIRONMENT_NAME`。

- 使用构建包时，转发的变量会自动作为环境变量使用。
- 使用 `Dockerfile` 时，需要执行以下附加步骤：

  1. 通过将以下代码添加到文件顶部来激活实验性 `Dockerfile` 语法：

     ```dockerfile
     # syntax = docker/dockerfile:experimental
     ```

  1. 要在 `Dockerfile` 中的任何 `RUN $COMMAND` 中提供 secret，请在运行 `$COMMAND` 之前挂载 secret 文件并获取它：

     ```dockerfile
     RUN --mount=type=secret,id=auto-devops-build-secrets . /run/secrets/auto-devops-build-secrets && $COMMAND
     ```

当设置 `AUTO_DEVOPS_BUILD_IMAGE_FORWARDED_CI_VARIABLES` 时，Auto DevOps 启用实验性 [Docker BuildKit](https://docs.docker.com/develop/develop-images/build_enhancements/) 功能以使用 `--secret` 标志。

<a id="custom-helm-chart"></a>

## 自定义 Helm Chart

Auto DevOps 使用 [Helm](https://helm.sh/) 将您的应用程序部署到 Kubernetes。您可以通过将 chart 绑定到项目仓库或指定项目 CI/CD 变量来覆盖使用的 Helm chart：

- **捆绑的 chart** - 如果您的项目有一个 `./chart` 目录，其中包含 `Chart.yaml` 文件，Auto DevOps 会检测到该 chart 并使用它而不是默认 chart，使您能够准确控制应用程序的部署方式。
- **项目变量** - 使用要使用的自定义 chart 的 URL 创建一个[项目 CI/CD 变量](../../ci/variables/index.md) `AUTO_DEVOPS_CHART`，或创建两个项目变量：`AUTO_DEVOPS_CHART_REPOSITORY` 包含自定义 chart 仓库的 URL，`AUTO_DEVOPS_CHART` 包含 chart 路径。

## 自定义 Helm Chart 的值

您可以通过以下任一方式覆盖默认 Helm chart 的 `values.yaml` 文件中的默认值：

- 将名为 `.gitlab/auto-deploy-values.yaml` 的文件添加到您的仓库中，如果找到，该文件会自动使用。
- 将具有不同名称或路径的文件添加到仓库，并使用路径和名称设置 `HELM_UPGRADE_VALUES_FILE` [CI/CD 变量](#cicd-variables)。

某些值不能被上述选项覆盖。像 `replicaCount` 这样的设置应该被 `REPLICAS` [构建和部署](#build-and-deployment) CI/CD 变量覆盖。

NOTE:
对于 12.5 及更早版本，使用 `HELM_UPGRADE_EXTRA_ARGS` 变量通过将 `HELM_UPGRADE_EXTRA_ARGS` 设置为 `--values <my-values.yaml>` 来覆盖默认 chart 值。

## 自定义 `helm upgrade` 命令

您可以通过使用 `HELM_UPGRADE_EXTRA_ARGS` CI/CD 变量将选项传递给命令，来自定义 auto-deploy-image 中使用的 `helm upgrade` 命令。
例如，将 `HELM_UPGRADE_EXTRA_ARGS` 的值设置为 `--no-hooks`，在执行命令时禁用 pre-upgrade 和 post-upgrade 钩子。

有关选项的完整列表，请参阅[官方文档](https://helm.sh/docs/helm/helm_upgrade/)。

## 分环境自定义 Helm chart

您可以通过将 CI/CD 变量限定为所需环境来指定每个环境使用的自定义 Helm chart。请参阅[限制 CI/CD 变量的环境范围](../../ci/variables/index.md#limit-the-environment-scope-of-a-cicd-variable)。

<a id="customize-gitlab-ciyml"></a>

## 自定义 `.gitlab-ci.yml`

Auto DevOps 是完全可定制的，因为 [Auto DevOps 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml)只是 [`.gitlab-ci.yml`](../../ci/yaml/index.md) 文件的实现，并且仅使用可用于任何 `.gitlab-ci.yml` 实现的功能。

要修改 Auto DevOps 使用的 CI/CD 流水线，[`include` 模板](../../ci/yaml/index.md#includetemplate)，并根据需要添加 `.gitlab-ci.yml` 文件到您的仓库的根目录，其中包含以下内容：

```yaml
include:
  - template: Auto-DevOps.gitlab-ci.yml
```

添加您的更改，您的添加将使用 [`include`](../../ci/yaml/index.md#include) 描述的行为，与 [Auto DevOps 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml)合并。

如果需要专门删除部分文件，也可以复制粘贴 [Auto DevOps 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml)的内容到您的项目中，并根据需要进行编辑。

<!--
## Use multiple Kubernetes clusters

See [Multiple Kubernetes clusters for Auto DevOps](multiple_clusters_auto_devops.md).
-->

## 自定义 Kubernetes 命名空间

在 14.5 及之前的版本，您可以使用 `environment:kubernetes:namespace` 为环境指定命名空间。
但是，此功能随基于证书的集成功能废弃。

您现在应该使用 `KUBE_NAMESPACE` 环境变量并[限制它可用于的环境](../../ci/environments/index.md#scope-environments-with-specs)。

## 使用 Auto DevOps 组件

如果您只需要 Auto DevOps 提供的功能的子集，您可以将各个 Auto DevOps 作业包含到您自己的 `.gitlab-ci.yml` 中。每个组件作业都依赖于应该在包含模板的 `.gitlab-ci.yml` 中定义的阶段。

例如，要使用 [Auto Build](stages.md#auto-build)，您可以在 `.gitlab-ci.yml` 中添加以下内容：

```yaml
stages:
  - build

include:
  - template: Jobs/Build.gitlab-ci.yml
```

有关可用作业的信息，请参阅 [Auto DevOps 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml)。

WARNING:
从 13.0 版本开始，Auto DevOps 模板使用 [`only`](../../ci/yaml/index.md#only--except) 或 [`except`](../../ci/yaml/index.md#only--except) 语法已切换到 [`rules`](../../ci/yaml/index.md#rules) 语法。
如果您的 `.gitlab-ci.yml` 扩展了这些 Auto DevOps 模板并覆盖了 `only` 或 `except` 关键字，则必须迁移模板以使用 [`rules`](../../ci/yaml/index.md#rules) 语法。

<!--
For users who cannot migrate just yet, you can alternatively pin your templates to
the [GitLab 12.10 based templates](https://gitlab.com/gitlab-org/auto-devops-v12-10).
-->

## 使用托管在本地 Docker 镜像库中的镜像

您可以配置许多 Auto DevOps 作业在离线环境中运行：

1. 将所需的 Auto DevOps Docker 镜像从 Docker Hub 和 `registry.gitlab.com` 复制到其本地镜像库。
1. 镜像托管并在本地镜像库中可用后，编辑 `.gitlab-ci.yml` 指向本地托管的镜像。例如：

   ```yaml
   include:
     - template: Auto-DevOps.gitlab-ci.yml

   variables:
     REGISTRY_URL: "registry.gitlab.example"

   build:
     image: "$REGISTRY_URL/docker/auto-build-image:v0.6.0"
     services:
       - name: "$REGISTRY_URL/greg/docker/docker:20.10.16-dind"
         command: ['--tls=false', '--host=tcp://0.0.0.0:2375']
   ```

<a id="postgresql-database-support"></a>

## PostgreSQL 数据库支持

为了支持需要数据库的应用程序，默认配置了 [PostgreSQL](https://www.postgresql.org/)。访问数据库的凭据是预先配置的，但可以通过设置关联的 [CI/CD 变量](#cicd-variables)来自定义。您可以使用这些凭据来定义 `DATABASE_URL`：

```yaml
postgres://user:password@postgres-host:postgres-port/postgres-database
```

<!--
### 升级 PostgresSQL

WARNING:
在  13.0 版本中，控制默认配置 PostgreSQL 的 CI/CD 变量 `AUTO_DEVOPS_POSTGRES_CHANNEL` 已更改为 `2`。要继续使用旧的 PostgreSQL，请将 `AUTO_DEVOPS_POSTGRES_CHANNEL` 变量设置为 `1`。

The version of the chart used to provision PostgreSQL:

- Is 8.2.1 in GitLab 13.0 and later, but can be set back to 0.7.1 if needed.
- Can be set to from 0.7.1 to 8.2.1 in GitLab 12.9 and 12.10.
- Is 0.7.1 in GitLab 12.8 and earlier.

GitLab encourages users to [migrate their database](upgrading_postgresql.md)
to the newer PostgreSQL.
-->

### 自定义 PostgreSQL Helm Chart 的值

> 引入到 auto-deploy-image v2 于 13.8 版本。

要设置自定义值，请执行以下操作之一：

- 将名为 `.gitlab/auto-deploy-postgres-values.yaml` 的文件添加到您的仓库。如果找到，将自动使用此文件。该文件默认用于 PostgreSQL Helm 升级。
- 将具有不同名称或路径的文件添加到仓库，并使用路径和名称设置 `POSTGRES_HELM_UPGRADE_VALUES_FILE` [环境变量](#database)。
- 设置 `POSTGRES_HELM_UPGRADE_EXTRA_ARGS` [环境变量](#database)。

### 使用外部 PostgreSQL 数据库 providers

虽然 Auto DevOps 为生产环境的 PostgreSQL 容器提供了开箱即用的支持，但对于某些用例，它可能不够安全或有弹性，您可能希望使用外部托管 provider（例如 AWS 关系数据库 服务）用于 PostgreSQL。

您必须在项目的 CI/CD 设置中为 `POSTGRES_ENABLED` 和 `DATABASE_URL` 定义环境范围的 CI/CD 变量：

1. 使用环境范围 [CI/CD 变量](../../ci/environments/index.md#scope-environments-with-specs)将禁用所需环境的内置 PostgreSQL 安装。对于这个用例，很可能只有 `production` 必须添加到此列表中。Review Apps 和 staging 的内置 PostgreSQL 设置即可。

   ![Auto Metrics](img/disable_postgres.png)

1. 将 `DATABASE_URL` 变量定义为可用于您的应用程序的环境范围变量。这应该是以下格式的 URL：

   ```yaml
   postgres://user:password@postgres-host:postgres-port/postgres-database
   ```

您必须确保您的 Kubernetes 集群可以通过网络访问托管 PostgreSQL 的任何位置。

<a id="cicd-variables"></a>

## CI/CD 变量

以下变量可用于设置 Auto DevOps 域名、提供自定义 Helm chart 或扩展您的应用程序。PostgreSQL 也可以自定义，您可以使用[自定义构建包](#custom-buildpacks)。

<a id="build-and-deployment"></a>

### 构建和部署

下表列出了与构建和部署应用程序相关的 CI/CD 变量。

| **CI/CD 变量**                      | **描述**                    |
|-----------------------------------------|------------------------------------|
| `ADDITIONAL_HOSTS`                      | 以逗号分隔的列表指定的完全限定域名，添加到 Ingress 主机。 |
| `<ENVIRONMENT>_ADDITIONAL_HOSTS`        | 对于特定环境，将添加到 Ingress 主机的完全限定域名指定为逗号分隔列表。优先于 `ADDITIONAL_HOSTS`。 |
| `AUTO_BUILD_IMAGE_VERSION`              | 自定义用于 `build` 作业的镜像版本。 |
| `AUTO_DEPLOY_IMAGE_VERSION`            | 自定义用于 Kubernetes 部署作业的镜像版本。 |
| `AUTO_DEVOPS_ATOMIC_RELEASE`            | 从 13.0 版本开始，Auto DevOps 默认使用 [`--atomic`](https://v2.helm.sh/docs/helm/#options-43) 进行 Helm 部署。将此变量设置为 `false` 可以禁用 `--atomic`。 |
| `AUTO_DEVOPS_BUILD_IMAGE_CNB_ENABLED`   | 设置为 `false` 可以使用 Herokuish 而不是 Cloud Native Buildpacks 和 Auto Build。 |
| `AUTO_DEVOPS_BUILD_IMAGE_CNB_BUILDER`   | 使用 Cloud Native Buildpacks 构建时使用的构建器。默认构建器是 `heroku/buildpacks:18`。 |
| `AUTO_DEVOPS_BUILD_IMAGE_EXTRA_ARGS`    | 要传递给 `docker build` 命令的额外参数。请注意，使用引号不会单词分割。 |
| `AUTO_DEVOPS_BUILD_IMAGE_FORWARDED_CI_VARIABLES` | 要转发到构建环境（buildpack 构建器或 `docker build`）的 CI/CD 变量名称的逗号分隔列表。 |
| `AUTO_DEVOPS_BUILD_IMAGE_CNB_PORT`      | 在 15.0 及更高版本中，生成的 Docker 镜像公开的端口。设置为 `false` 可以防止暴露任何端口。默认为 `5000`。 |
| `AUTO_DEVOPS_CHART`                     | 用于部署您的应用程序的 Helm Chart。默认为极狐GitLab 提供。 |
| `AUTO_DEVOPS_CHART_REPOSITORY`          | 用于搜索 chart 的 Helm Chart 仓库。默认为 `https://charts.gitlab.cn`。 |
| `AUTO_DEVOPS_CHART_REPOSITORY_NAME`     | 用于设置 Helm 仓库的名称。默认为 `gitlab`。 |
| `AUTO_DEVOPS_CHART_REPOSITORY_USERNAME` | 用于设置连接到 Helm 仓库的用户名。默认为无凭据。还要设置 `AUTO_DEVOPS_CHART_REPOSITORY_PASSWORD`。 |
| `AUTO_DEVOPS_CHART_REPOSITORY_PASSWORD` | 用于设置连接到 Helm 仓库的用户名。默认为无凭据。还要设置 `AUTO_DEVOPS_CHART_REPOSITORY_USERNAME`。 |
| `AUTO_DEVOPS_CHART_REPOSITORY_PASS_CREDENTIALS` | 从 14.2 版本开始，设置为非空值，在 chart 产物位于与仓库不同的主机上时，启用将 Helm 仓库凭据转发到 chart 服务器。 |
| `AUTO_DEVOPS_DEPLOY_DEBUG`              | 从 13.1 版本开始，如果存在此变量，Helm 会输出调试日志。 |
| `AUTO_DEVOPS_ALLOW_TO_FORCE_DEPLOY_V<N>` | 从 auto-deploy-image v1.0.0 开始，如果存在此变量，则会强制部署新的主要版本的 chart。 |
| `BUILDPACK_URL`                         | 构建包的完整 URL。[必须指向 Pack 或 Herokuish 支持的 URL](#custom-buildpacks)。 |
| `CANARY_ENABLED`                        | 用于定义[金丝雀环境的部署策略](#deploy-policy-for-canary-environments)。 |
| `BUILDPACK_VOLUMES`                     | 指定一个或多个要挂载的 Buildpack 卷。使用管道符 `|` 作为列表分隔符。 |
| `CANARY_PRODUCTION_REPLICAS`            | 在生产环境中，[金丝雀部署](../../user/project/canary_deployments.md)的金丝雀副本数。优先于 `CANARY_REPLICAS`。默认为 1。 |
| `CANARY_REPLICAS`                       | [金丝雀部署] (../../user/project/canary_deployments.md)的金丝雀副本数。默认为 1。 |
| `CI_APPLICATION_REPOSITORY`             | 正在构建或部署的容器镜像仓库，`$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG`。有关更多详细信息，请阅读[自定义容器镜像](#custom-container-image)。 |
| `CI_APPLICATION_TAG`                    | 正在构建或部署的容器镜像的标签，`$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG`。有关更多详细信息，请阅读[自定义容器镜像](#custom-container-image)。 |
| `DAST_AUTO_DEPLOY_IMAGE_VERSION`        | 在默认分支上自定义用于 DAST 部署的镜像版本。通常应该与 `AUTO_DEPLOY_IMAGE_VERSION` 相同。 |
| `DOCKERFILE_PATH`                       | 从 13.2 版本开始，允许覆盖[构建阶段的默认 Dockerfile 路径](#custom-dockerfile)。 |
| `HELM_RELEASE_NAME`                     | 从 12.1 版本开始，允许覆盖 `helm` 发布名称。可用于在将多个项目部署到单个命名空间时分配唯一的发布名称。 |
| `HELM_UPGRADE_VALUES_FILE`              | 从 12.6 版本开始，允许覆盖 `helm upgrade` 值文件。默认为 `.gitlab/auto-deploy-values.yaml`。 |
| `HELM_UPGRADE_EXTRA_ARGS`               | 部署应用程序时允许在 `helm upgrade` 命令中使用额外选项。请注意，使用引号不会阻止单词分割。 |
| `INCREMENTAL_ROLLOUT_MODE`              | 如果存在，可用于为生产环境启用应用程序的增量部署。对于手动部署作业设置为 `manual`，对于自动部署作业设置为 `timed`，每个部署有 5 分钟的延迟。 |
| `K8S_SECRET_*`                          | Auto DevOps 将任何前缀为 `K8S_SECRET_` 的变量作为环境变量提供给已部署的应用程序。 |
| `KUBE_CONTEXT`                          | 从 14.5 版本开始，可用于从 `KUBECONFIG` 中选择要使用的上下文。当 `KUBE_CONTEXT` 为空时，使用 `KUBECONFIG` 中的默认上下文（如果有）。与 Kubernetes 代理一起使用时必须选择上下文。 |
| `KUBE_INGRESS_BASE_DOMAIN`              | 可用于为每个集群设置一个域名。 |
| `KUBE_NAMESPACE`                        | 用于部署的命名空间。使用基于证书的集群时，不应直接覆盖此值。 |
| `KUBECONFIG`                            | 用于部署的 kubeconfig。用户提供的值优先于极狐GitLab 提供的值。 |
| `PRODUCTION_REPLICAS`                   | 要在生产环境中部署的副本数。优先于 `REPLICAS` 并默认为 1。对于零停机升级，设置为 2 或更大。 |
| `REPLICAS`                              | 要部署的副本数。默认为 1。更改此变量而不是[更改](#customize-values-for-helm-chart) `replicaCount`。 |
| `ROLLOUT_RESOURCE_TYPE`                 | 允许在使用自定义 Helm chart 时指定正在部署的资源类型。默认值为 `deployment`。 |
| `ROLLOUT_STATUS_DISABLED`               | 从 12.0 版本开始，用于禁用发布状态检查，因为它不支持所有资源类型，例如，`cronjob`。 |
| `STAGING_ENABLED`                       | 用于定义 [staging 和生产环境的部署策略](#deploy-policy-for-staging-and-production-environments)。 |
| `TRACE`                                 | 设置为任何值以使 Helm 命令产生详细的输出。您可以使用此设置来帮助诊断 Auto DevOps 部署问题。 |

NOTE:
使用[项目 CI/CD 变量](../../ci/variables/index.md)设置副本变量后，您可以通过重新部署来扩展应用程序。

WARNING:
您不应该直接使用 Kubernetes 扩展您的应用程序。这可能会导致 Helm 未检测到更改而造成混淆，并且使用 Auto DevOps 进行后续部署可以撤消您的更改。

<a id="database"></a>

### 数据库

下表列出了与数据库相关的 CI/CD 变量。

| **CI/CD 变量**                            | **描述**                    |
|-----------------------------------------|------------------------------------|
| `DB_INITIALIZE`                         | 用于指定要运行的命令，初始化应用程序的 PostgreSQL 数据库。在应用程序 pod 内运行。 |
| `DB_MIGRATE`                            | 用于指定要运行从而迁移应用程序的 PostgreSQL 数据库的命令。在应用程序 pod 内运行。 |
| `POSTGRES_ENABLED`                      | 是否启用 PostgreSQL。默认为 `true`。设置为 `false` 可以禁用 PostgreSQL 的自动部署。 |
| `POSTGRES_USER`                         | PostgreSQL 用户。默认为 `user`。将其设置为使用自定义用户名。 |
| `POSTGRES_PASSWORD`                     | PostgreSQL 密码。默认为 `testing-password`。将其设置为使用自定义密码。 |
| `POSTGRES_DB`                           | PostgreSQL 数据库名称。默认为 [`$CI_ENVIRONMENT_SLUG`](../../ci/variables/index.md#predefined-cicd-variables) 的值。将其设置为使用自定义数据库名称。 |
| `POSTGRES_VERSION`                      | 要使用的 [`postgres` Docker 镜像](https://hub.docker.com/_/postgres) 的标记。从 13.0（之前为 9.6.2）版本开始，测试和部署默认为 9.6.16。如果 `AUTO_DEVOPS_POSTGRES_CHANNEL` 设置为 `1`，则部署使用默认版本 `9.6.2`。 |
| `POSTGRES_HELM_UPGRADE_VALUES_FILE`     | 在 13.8 及更高版本中，当使用 auto-deploy-image v2 时，此变量允许覆盖 PostgreSQL 的 `helm upgrade` 值文件。默认为 `.gitlab/auto-deploy-postgres-values.yaml`。 |
| `POSTGRES_HELM_UPGRADE_EXTRA_ARGS`      | 在 13.8 及更高版本中，当使用 auto-deploy-image v2 时，此变量允许在部署应用程序时在 `helm upgrade` 命令中使用额外的 PostgreSQL 选项。请注意，使用引号不会阻止单词分割。 |

<a id="disable-jobs"></a>

### 禁用作业

下表列出了用于禁用作业的变量。

| **作业名称**                           | **CI/CD 变量**               | **使用的版本**    | **描述** |
|----------------------------------------|---------------------------------|-----------------------|-----------------|
| `.fuzz_base`                           | `COVFUZZ_DISABLED`              | 13.2+ | <!--[Read more](../../user/application_security/coverage_fuzzing/) about how `.fuzz_base` provide capability for your own jobs.-->如果该变量存在，则不会创建您的作业。 |
| `apifuzzer_fuzz`                       | `API_FUZZING_DISABLED`          | 13.3+ | 如果变量存在，则不会创建作业。 |
| `build`                                | `BUILD_DISABLED`                |                       | 如果变量存在，则不会创建作业。 |
| `build_artifact`                       | `BUILD_DISABLED`                |                       | 如果变量存在，则不会创建作业。 |
| `bandit-sast`                          | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `brakeman-sast`                        | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `canary`                               | `CANARY_ENABLED`                |                       | 如果变量存在，则会创建此手动作业。 |
| `cluster_image_scanning`               | `CLUSTER_IMAGE_SCANNING_DISABLED` |                     | 如果变量存在，则不会创建作业。 |
| `code_intelligence`                    | `CODE_INTELLIGENCE_DISABLED`    | 13.6+      | 如果变量存在，则不会创建作业。 |
| `code_quality`                         | `CODE_QUALITY_DISABLED`         |                       | 如果变量存在，则不会创建作业。 |
| `container_scanning`                   | `CONTAINER_SCANNING_DISABLED`   |                       | 如果变量存在，则不会创建作业。 |
| `dast`                                 | `DAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `dast_environment_deploy`              | `DAST_DISABLED_FOR_DEFAULT_BRANCH` 或 `DAST_DISABLED` |  12.4 | 如果存在任一变量，则不会创建作业。 |
| `dependency_scanning`                  | `DEPENDENCY_SCANNING_DISABLED`  |                       | 如果变量存在，则不会创建作业。 |
| `eslint-sast`                          | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `flawfinder-sast`                      | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `gemnasium-dependency_scanning`        | `DEPENDENCY_SCANNING_DISABLED`  |                       | 如果变量存在，则不会创建作业。 |
| `gemnasium-maven-dependency_scanning`  | `DEPENDENCY_SCANNING_DISABLED`  |                       | 如果变量存在，则不会创建作业。 |
| `gemnasium-python-dependency_scanning` | `DEPENDENCY_SCANNING_DISABLED`  |                       | 如果变量存在，则不会创建作业。 |
| `gosec-sast`                           | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `kubesec-sast`                         | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `license_management`                   | `LICENSE_MANAGEMENT_DISABLED`   | 12.7 及更早版本 | 如果变量存在，则不会创建作业。<!--Job deprecated [from GitLab 12.8](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/22773)--> |
| `license_scanning`                     | `LICENSE_MANAGEMENT_DISABLED`   | 12.8+ | 如果变量存在，则不会创建作业。 |
| `load_performance`                     | `LOAD_PERFORMANCE_DISABLED`     | 13.2+      | 如果变量存在，则不会创建作业。 |
| `nodejs-scan-sast`                     | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `performance`                          | `PERFORMANCE_DISABLED`          | 13.12 及更早版本 | 浏览器性能。如果变量存在，则不会创建作业。由 `browser_performance` 取代。 |
| `browser_performance`                  | `BROWSER_PERFORMANCE_DISABLED`  | 14.0+      | 浏览器性能。如果变量存在，则不会创建作业。取代 `performance`。 |
| `phpcs-security-audit-sast`            | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `pmd-apex-sast`                        | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `review`                               | `REVIEW_DISABLED`               |                       | 如果变量存在，则不会创建作业。 |
| `review:stop`                          | `REVIEW_DISABLED`               |                       | 手动作业。如果变量存在，则不会创建作业。 |
| `sast`                                 | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `sast:container`                       | `CONTAINER_SCANNING_DISABLED`   |                       | 如果变量存在，则不会创建作业。 |
| `secret_detection`                     | `SECRET_DETECTION_DISABLED`     | 13.1+      | 如果变量存在，则不会创建作业。 |
| `secret_detection_default_branch`      | `SECRET_DETECTION_DISABLED`     | 13.2+ | 如果变量存在，则不会创建作业。 |
| `security-code-scan-sast`              | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `secrets-sast`                         | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `sobelaw-sast`                         | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `stop_dast_environment`                | `DAST_DISABLED_FOR_DEFAULT_BRANCH` 或 `DAST_DISABLED` | 12.4+ | 如果存在任一变量，则不会创建作业。 |
| `spotbugs-sast`                        | `SAST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `test`                                 | `TEST_DISABLED`                 |                       | 如果变量存在，则不会创建作业。 |
| `staging`                              | `STAGING_ENABLED`               |                       | 如果变量存在，则创建作业。 |
| `stop_review`                          | `REVIEW_DISABLED`               |                       | 如果变量存在，则不会创建作业。 |

### 应用程序 secret 变量

一些应用程序需要定义已部署的应用程序可以访问的 secret 变量。Auto DevOps 检测以 `K8S_SECRET_` 开头的 CI/CD 变量，并将这些前缀变量作为环境变量提供给已部署的应用程序。

要配置您的应用程序变量：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏，选择 **设置 > CI/CD**。
1. 展开 **变量**。
1. 创建一个 CI/CD 变量，确保密钥以 `K8S_SECRET_` 为前缀。例如，您可以使用键 `K8S_SECRET_RAILS_MASTER_KEY` 创建一个变量。

1. 通过手动创建新流水线或将代码更改推送到极狐GitLab 来运行 Auto DevOps 流水线。

Auto DevOps 流水线使用您的应用程序 secret 变量来填入 Kubernetes secret。这个 secret 在每个环境中都是唯一的。
部署应用程序时，secret 作为环境变量加载到运行应用程序的容器中。按照上面的示例，您可以在下面看到包含 `RAILS_MASTER_KEY` 变量的 secret。

```shell
$ kubectl get secret production-secret -n minimal-ruby-app-54 -o yaml

apiVersion: v1
data:
  RAILS_MASTER_KEY: MTIzNC10ZXN0
kind: Secret
metadata:
  creationTimestamp: 2018-12-20T01:48:26Z
  name: production-secret
  namespace: minimal-ruby-app-54
  resourceVersion: "429422"
  selfLink: /api/v1/namespaces/minimal-ruby-app-54/secrets/production-secret
  uid: 57ac2bfd-03f9-11e9-b812-42010a9400e4
type: Opaque
```

环境变量在 Kubernetes pod 中通常被认为是不可变的。
如果您在不更改任何代码的情况下更新应用程序 secret，然后手动创建新流水线，则任何正在运行的应用程序 pod 都不会收到更新的 secret。要更新 secret，请执行以下任一操作：

- 向极狐GitLab 推送代码更新，强制 Kubernetes 部署重新创建 pod。
- 手动删除正在运行的 pod，使 Kubernetes 创建具有更新 secret 的新 pod。

由于当前 Auto DevOps 脚本环境的限制，当前不支持具有多行值的变量。

### 高级副本变量设置

除了上面提到的用于生产环境的两个副本相关变量外，您还可以将其他变量用于不同的环境。

名为 `track` 的 Kubernetes 的标签、极狐GitLab CI/CD 环境名称和副本环境变量组合成 `TRACK_ENV_REPLICAS` 格式，使您能够定义自己的变量来扩展 pod 的副本：

- `TRACK`：Helm Chart 应用定义中`track` [Kubernetes 标签](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/) 的大写值。如果未设置，则不考虑变量名。
- `ENV`：部署作业的大写环境名称，在`.gitlab-ci.yml`中设置。

在下面的示例中，环境的名称是 `qa`，它部署了 `foo`，会产生一个名为 `FOO_QA_REPLICAS` 的环境变量：

```yaml
QA testing:
  stage: deploy
  environment:
    name: qa
  script:
    - deploy foo
```

引用的 `foo` 也必须在应用程序的 Helm chart 中定义，例如：

```yaml
replicaCount: 1
image:
  repository: gitlab.example.com/group/project
  tag: stable
  pullPolicy: Always
  secrets:
    - name: gitlab-registry
application:
  track: foo
  tier: web
service:
  enabled: true
  name: web
  type: ClusterIP
  url: http://my.host.com/
  externalPort: 5000
  internalPort: 5000
```

<a id="deploy-policy-for-staging-and-production-environments"></a>

### 为 staging 和生产环境部署策略

NOTE:
您也可以在[项目设置](requirements.md#auto-devops-deployment-strategy)中进行设置。

Auto DevOps 的正常行为是使用持续部署，每次在默认分支上运行新流水线时自动推送到生产环境。但是，在某些情况下，您可能希望使用 staging 环境并手动部署到生产环境。对于这种情况，引入了 `STAGING_ENABLED` CI/CD 变量。

如果您使用非空值定义 `STAGING_ENABLED`，则系统会自动将应用程序部署到 `staging` 环境，并在您准备手动部署到生产环境时为您创建 `production_manual` 作业。

<a id="deploy-policy-for-canary-environments"></a>

### 金丝雀环境部署策略 **(PREMIUM)**

在将任何更改部署到生产环境之前，您可以使用[金丝雀环境](../../user/project/canary_deployments.md)。

如果您使用非空值定义 `CANARY_ENABLED`，则会创建两个手动作业：

- `canary` - 将应用程序部署到金丝雀环境。
- `production_manual` - 手动将应用程序部署到生产环境。

<a id="incremental-rollout-to-production"></a>

### 增量部署到生产 **(PREMIUM)**

NOTE:
您也可以在[项目设置](requirements.md#auto-devops-deployment-strategy)中进行设置。

当您准备好将应用程序的新版本部署到生产环境时，您可能希望使用增量部署来仅用最新代码替换几个 pod，检查应用程序的行为，然后再手动将部署增加到 100%。

如果在您的项目中将 `INCREMENTAL_ROLLOUT_MODE` 设置为 `manual`，则创建 4 个不同的[手动作业](../../ci/pipelines/index.md#add-manual-interaction-to-your-pipeline)：

1. `rollout 10%`
1. `rollout 25%`
1. `rollout 50%`
1. `rollout 100%`

该百分比基于 `REPLICAS` CI/CD 变量，并定义了您希望部署的 pod 数量。如果值为 `10`，并且您运行 `10%` 部署作业，则有 `1` 个新 pod 和 `9` 个旧 pod。

要启动作业，请选择作业名称旁边的运行图标 (**{play}**)。您不需要从 `10%` 到 `100%`，您可以跳到任何您想要的作业。
您还可以通过在达到 `100%` 之前运行较低百分比的作业来缩容。一旦达到 `100%`，就无法缩容，必须使用环境页面中的回滚按钮重新部署旧版本来回滚。

在下面，您可以看到如果定义了 rollout 或 staging 变量，流水线将如何显示。

无 `INCREMENTAL_ROLLOUT_MODE` 和 `STAGING_ENABLED`：

![Staging and rollout disabled](img/rollout_staging_disabled.png)

无 `INCREMENTAL_ROLLOUT_MODE`，有 `STAGING_ENABLED`：

![Staging enabled](img/staging_enabled.png)

`INCREMENTAL_ROLLOUT_MODE` 设置为 `manual`，无 `STAGING_ENABLED`：

![Rollout enabled](img/rollout_enabled.png)

`INCREMENTAL_ROLLOUT_MODE` 设置为 `manual`，有 `STAGING_ENABLED`：

![Rollout and staging enabled](img/rollout_staging_enabled.png)

WARNING:
此配置已弃用，并计划在将来删除。

<a id="timed-incremental-rollout-to-production"></a>

### 定时增量部署到生产 **(PREMIUM)**

NOTE:
您也可以在[项目设置](requirements.md#auto-devops-deployment-strategy)中进行设置。

此配置基于[增量部署到生产](#incremental-rollout-to-production)。

一切都以相同的方式运行，以下除外：

- 要启用它，请将 `INCREMENTAL_ROLLOUT_MODE` CI/CD 变量设置为 `timed`。
- 代替标准的 `production` 作业，创建以下作业，每个作业之间有 5 分钟的延迟：

  1. `timed rollout 10%`
  1. `timed rollout 25%`
  1. `timed rollout 50%`
  1. `timed rollout 100%`

## Auto DevOps banner

未启用 Auto DevOps 时，对新项目具有维护者或更高权限的用户会显示以下 Auto DevOps banner：

![Auto DevOps banner](img/autodevops_banner_v12_6.png)

可以通过以下方式禁用 banner：

- 用户自己关闭。
- 一个项目，明确[禁用 Auto DevOps](index.md#enable-or-disable-auto-devops)。
- 在一个完整的极狐GitLab 实例：
  - 由管理员在 Rails 控制台中运行以下命令：

    ```ruby
    Feature.enable(:auto_devops_banner_disabled)
    ```

  - 通过带有管理员访问令牌的 REST API：

    ```shell
    curl --data "value=true" --header "PRIVATE-TOKEN: <personal_access_token>" "https://gitlab.example.com/api/v4/features/auto_devops_banner_disabled"
    ```
