---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Auto DevOps 的 Stages **(FREE)**

以下部分描述了 [Auto DevOps](index.md) 的各个 stage。
仔细阅读它们，了解每一个是如何工作的。

## Auto Build

NOTE:
如果 Docker in Docker 不适用于您的 GitLab Runners，则不支持自动构建，例如在 OpenShift 集群中。<!--The OpenShift support in GitLab is tracked [in a dedicated epic](https://gitlab.com/groups/gitlab-org/-/epics/2068).-->

Auto Build 使用现有的 `Dockerfile` 或 Heroku buildpacks 创建应用的构建。生成的 Docker 镜像被推送到 [Container Registry](../../user/packages/container_registry/index.md)，并使用提交 SHA 或 tag 进行标记。

### 使用 Dockerfile 自动构建

如果项目的仓库在其根目录中包含 `Dockerfile`，则 Auto Build 使用 `docker build` 来创建 Docker 镜像。

如果您还使用 Auto Review Apps 和 Auto Deploy，并且您选择提供自己的 `Dockerfile`，则必须：

- 将您的应用公开到端口 `5000`，因为默认 Helm chart 假定此端口可用。
- 通过自定义 Auto Deploy Helm chart<!--[自定义 Auto Deploy Helm chart](customize.md#custom-helm-chart)--> 覆盖默认值。

### 使用 Cloud Native Buildpacks 自动构建

> - 默认情况下使用 Cloud Native Buildpacks 自动构建引入于 14.0 版本。

Auto Build 使用项目的 `Dockerfile`（如果存在）构建应用。如果不存在 `Dockerfile`，Auto Build 会使用 [Cloud Native Buildpacks](https://buildpacks.io) 来构建您的应用，检测应用程序并将其构建到 Docker 镜像中。该功能使用 [`pack` 命令](https://github.com/buildpacks/pack)。
默认的 [builder](https://buildpacks.io/docs/concepts/components/builder/) 是 `heroku/buildpacks:18`，但可以使用 CI/CD 变量 `AUTO_DEVOPS_BUILD_IMAGE_CNB_BUILDER` 选择不同的 builder。

每个 buildpack 都需要您项目的仓库包含某些文件，以便 Auto Build 成功构建您的应用。该结构特定于您选择的 builder 和 buildpack。
例如，当使用 Heroku 的 builder（默认）时，应用的根目录必须包含适合应用程序语言的文件：

- 对于 Python 项目，`Pipfile` 或 `requirements.txt` 文件。
- 对于 Ruby 项目，一个 `Gemfile` 或 `Gemfile.lock` 文件。

对于其他语言和框架的要求，请阅读 [Heroku buildpacks 文档](https://devcenter.heroku.com/articles/buildpacks#officially-supported-buildpacks)。

NOTE:
Auto Test 仍然使用 Herokuish，因为测试套件检测还不是 Cloud Native Buildpack 规范的一部分。<!--For more information, see
[this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/212689).-->

#### 将卷挂载到构建容器中

> - 引入于 14.2 版本。
> - 多卷支持（或 `auto-build-image` v1.6.0）引入于 14.6 版本。

变量 `BUILDPACK_VOLUMES` 可用于将卷挂载定义传递给 `pack` 命令。使用 `--volume` 参数将挂载传递给 `pack build`。
每个卷定义都可以包含 `build pack` 提供的任何功能，例如主机路径、目标路径、卷是否可写以及一个或多个卷选项。

使用管道 `|` 字符传递多个卷。
列表中的每个项目都使用单独的 `--volume` 参数传递给 `build back`。

在下面的例子中，三个卷被挂载在容器中，分别是 `/etc/foo`、`/opt/foo` 和 `/var/opt/foo`：

```yaml
buildjob:
  variables:
    BUILDPACK_VOLUMES: /mnt/1:/etc/foo:ro|/mnt/2:/opt/foo:ro|/mnt/3:/var/opt/foo:rw
```

在 [`pack build` 文档](https://buildpacks.io/docs/tools/pack/cli/pack_build/) 中阅读有关定义卷的更多信息。

### Auto Build using Herokuish

> 在 14.0 版本中替换为 Cloud Native Buildpacks。

在 14.0 版本之前，[Herokuish](https://github.com/gliderlabs/herokuish) 是没有 `Dockerfile` 的项目的默认构建方法。Herokuish 仍然可以通过设置 CI/CD 变量 `AUTO_DEVOPS_BUILD_IMAGE_CNB_ENABLED` 为 `false` 来使用。

NOTE:
如果尽管项目满足 buildpack 要求，但 Auto Build 仍然失败，请设置项目 CI/CD 变量 `TRACE=true` 以启用详细日志记录，这可能有助于您进行故障排查。

### 从 Herokuish 迁移到 Cloud Native Buildpacks

使用 Cloud Native Buildpacks 构建支持使用与 Herokuish 构建相同的选项，但有以下注意事项：

- buildpack 必须是 Cloud Native Buildpack。Heroku buildpack 可以使用 Heroku 的 [`cnb-shim`] (https://github.com/heroku/cnb-shim) 转换为 Cloud Native Buildpack。
- `BUILDACK_URL` 必须采用 [`pack` 支持的格式](https://buildpacks.io/docs/app-developer-guide/specify-buildpacks/)。
- `/bin/herokuish` 命令在构建的镜像中不存在，并且不再需要（也不可能）以 `/bin/herokuish procfile exec` 为前缀的命令。相反，自定义命令应以 `/cnb/lifecycle/launcher` 为前缀，以接收正确的执行环境。

## Auto Test

自动测试通过分析您的项目，使用 [Herokuish](https://github.com/gliderlabs/herokuish) 和 [Heroku buildpacks](https://devcenter.heroku.com/articles/buildpacks) 为您的应用运行适当的测试来检测语言和框架。系统会自动检测多种语言和框架，但如果未检测到您的语言，您可以创建一个自定义 buildpack。

自动测试使用您在应用中已有的 tests。如果没有 tests，则由您来添加它们。

<!-- vale gitlab.Spelling = NO -->

NOTE:
并非所有 [Auto Build](#auto-build) 支持的 buildpack 都受 Auto Test 支持。
Auto Test 使用 [Herokuish](https://gitlab.com/gitlab-org/gitlab/-/issues/212689)，*不是* Cloud Native Buildpacks，并且仅使用实现 [Testpack API](https://devcenter.heroku.com/articles/testpack-api)。

<!-- vale gitlab.Spelling = YES -->

### 当前支持的语言

请注意，并非所有构建包都支持自动测试，因为它是一个相对较新的增强功能。Heroku 的所有[官方支持的语言](https://devcenter.heroku.com/articles/heroku-ci#supported-languages)都支持自动测试。Heroku 的 Herokuish buildpack 支持的语言都支持 Auto Test，但值得注意的是 multi-buildpack 不支持。

支持的构建包是：

```plaintext
- heroku-buildpack-multi
- heroku-buildpack-ruby
- heroku-buildpack-nodejs
- heroku-buildpack-clojure
- heroku-buildpack-python
- heroku-buildpack-java
- heroku-buildpack-gradle
- heroku-buildpack-scala
- heroku-buildpack-play
- heroku-buildpack-php
- heroku-buildpack-go
- buildpack-nginx
```

如果您的应用程序需要不在上述列表中的 buildpack，您可能需要使用自定义 buildpack<!--[自定义 buildpack](customize.md#custom-buildpacks)-->。

<a id="auto-code-quality"></a>

## Auto Code Quality

自动代码质量使用 Code Quality 镜像对当前代码运行静态分析和其他代码检查。创建报告后，会作为产物上传，您可以稍后下载并检出。合并请求部件还显示任何源分支和目标分支之间的差异<!--[源分支和目标分支之间的差异](../../user/project/merge_requests/code_quality.md)-->。

## Auto SAST

静态应用程序安全测试 (SAST) 对当前代码运行静态分析，并检查潜在的安全问题。Auto SAST stage 需要 GitLab Runner 11.5 或更高版本。

创建报告后，它会作为产物上传，您可以稍后下载并检出。合并请求部件还显示[旗舰版](https://about.gitlab.cn/pricing/)许可证上的任何安全警告。

<!--
To learn more about [how SAST works](../../user/application_security/sast/index.md),
see the documentation.
-->

## Auto Secret Detection

Secret Detection 使用 Secret Detection Docker 镜像对当前代码运行 Secret Detection，并检查泄露的 secret。自动 secret 检测需要 GitLab Runner 11.5 或更高版本。

创建报告后，它会作为产物上传，您可以稍后下载和评估。合并请求部件还显示 [旗舰版](https://about.gitlab.cn/pricing/) 许可证上的任何安全警告。

<!--
To learn more, see [Secret Detection](../../user/application_security/secret_detection/index.md).
-->

## Auto Dependency Scanning **(ULTIMATE)**

Dependency Scanning 对项目的依赖项进行分析并检查潜在的安全问题。
在 [旗舰版](https://about.gitlab.cn/pricing/) 以外的许可证上跳过自动依赖扫描阶段，需要 GitLab Runner 11.5 或更高版本。

创建报告后，它会作为产物上传，您可以稍后下载并检出。合并请求部件显示检测到的任何安全警告。

<!--
To learn more about
[Dependency Scanning](../../user/application_security/dependency_scanning/index.md),
see the documentation.
-->

## Auto License Compliance **(ULTIMATE)**

License Compliance 使用 License Compliance Docker 镜像来搜索项目依赖项并获取其许可证。在[旗舰版](https://about.gitlab.cn/pricing/)以外的许可证上，跳过 Auto License Compliance stage。

创建报告后，它会作为产物上传，您可以稍后下载并检出。合并请求显示任何检测到的许可证。

<!--
To learn more about
[License Compliance](../../user/compliance/license_compliance/index.md), see the
documentation.
-->

## Auto Container Scanning

容器的漏洞静态分析使用 [Trivy](https://aquasecurity.github.io/trivy/latest/) 来检查 Docker 镜像中的潜在安全问题。在[旗舰版](https://about.gitlab.cn/pricing/)以外的许可证上跳过 Auto Container Scanning stage。

创建报告后，它会作为产物上传，您可以稍后下载并检出。合并请求显示任何检测到的安全问题。

<!--
To learn more about
[Container Scanning](../../user/application_security/container_scanning/index.md),
see the documentation.
-->

## Auto Review Apps

这是一个可选步骤，因为许多项目没有可用的 Kubernetes 集群。如果不满足[要求](requirements.md)，作业将被静默跳过。

[Review Apps](../../ci/review_apps/index.md) 是基于分支代码的临时应用环境，因此开发人员、设计师、QA、产品经理和其他 review 人员可以实际查看代码更改并与之交互，作为 review 过程的一部分。Auto Review Apps 为每个分支创建一个 Review App。

Auto Review Apps 仅将您的应用部署到您的 Kubernetes 集群。如果没有可用的集群，则不会发生部署。

Review App 具有基于项目 ID、分支或标签名称、唯一编号和 Auto DevOps 基础域名的组合的唯一 URL，例如 `13083-review-project-branch-123456.example.com`。合并请求部件显示一个指向 Review App 的链接，以便于发现。当分支或标签被删除时，例如在合并一个合并请求后，Review App 也会被删除。

使用 Helm 的 auto-deploy-app chart 部署 Review App，您可以对其进行自定义。应用部署到环境的 Kubernetes 命名空间中。

<!--
In GitLab 11.4 and later, [local Tiller](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/22036) is
used. Previous versions of GitLab had a Tiller installed in the project
namespace.
-->

WARNING:
您的应用*不应该*在 Helm 之外进行操作（直接使用 Kubernetes）。
这可能会导致 Helm 无法检测到更改的混淆，并且使用 Auto DevOps 进行后续部署可以撤消您的更改。此外，如果您更改了某些内容并想通过再次部署来撤消它，Helm 可能一开始就没有检测到任何更改，因此不会意识到它需要重新应用旧配置。

## Auto DAST **(ULTIMATE)**

动态应用安全测试 (DAST) 使用流行的开源工具 [OWASP ZAProxy](https://github.com/zaproxy/zaproxy) 来分析当前代码并检查潜在的安全问题。Auto DAST 阶段在[旗舰版](https://about.gitlab.cn/pricing/)以外的许可证上被跳过。

- 在您的默认分支上，DAST 会扫描专门为此目的部署的应用，除非您[覆盖目标分支](#覆盖-dast-目标)。该应用在 DAST 运行后被删除。
- 在功能分支上，DAST 扫描 [review app](#auto-review-apps)。

DAST 扫描完成后，任何安全警告都会显示在 Security Dashboard 和合并请求部件上。

<!--
To learn more about
[Dynamic Application Security Testing](../../user/application_security/dast/index.md),
see the documentation.
-->

### 覆盖 DAST 目标

要使用自定义目标而不是自动部署的 Review App，请将 `DAST_WEBSITE` CI/CD 变量设置为 DAST 扫描的 URL。

WARNING:
如果启用了 DAST Full Scan，强烈建议**不要**将 `DAST_WEBSITE` 设置为任何 staging 或生产环境。DAST Full Scan 主动攻击目标，这可能会导致您的应用瘫痪并导致数据丢失或损坏。

### 禁用 Auto DAST

您可以禁用 DAST：

- 通过将 `DAST_DISABLED` CI/CD 变量设置为 `"true"` 在所有分支上。
- 仅在默认分支上通过将 `DAST_DISABLED_FOR_DEFAULT_BRANCH` 变量设置为 `"true"`。
- 通过将 `REVIEW_DISABLED` 变量设置为 `"true"` 仅在功能分支上。这也会禁用 Review App。

## Auto Browser Performance Testing **(PREMIUM)**

Auto Browser Performance Testing 使用 [Sitespeed.io 容器](https://hub.docker.com/r/sitespeedio/sitespeed.io/) 测量网页的浏览器性能，为每个页面创建包含整体性能得分的 JSON 报告，并将报告作为产物上传。默认情况下，它会测试您的 Review 和 Production 环境的根页面。如果要测试其他 URL，请将路径添加到根目录中名为 `.gitlab-urls.txt` 的文件中，每行一个文件。例如：

```plaintext
/
/features
/direction
```

源分支和目标分支之间的任何浏览器性能差异也显示在合并请求部件中。

## Auto Load Performance Testing **(PREMIUM)**

Auto Load Performance Testing 使用 [k6 容器](https://hub.docker.com/r/loadimpact/k6/) 测量应用的服务器性能，创建包含几个关键结果指标的 JSON 报告，并上传报告作为产物。

需要一些初始设置，需要编写针对您的特定应用量身定制的 [k6](https://k6.io/) 测试。还需要配置测试，以便它可以通过 CI/CD 变量获取环境的动态 URL。

源分支和目标分支之间的任何负载性能测试结果差异也显示在合并请求部件中。

## Auto Deploy

Auto Deploy 是 Auto DevOps 的可选步骤。如果不满足[要求](requirements.md)，则跳过该作业。

分支或合并请求合并到项目的默认分支后，Auto Deploy 会将应用部署到 Kubernetes 集群中的 `production` 环境中，命名空间基于项目名称和唯一的项目 ID，例如 `project-4321`。

Auto Deploy 默认不包括部署到 staging 或 canary 环境，如果您想启用它们，[Auto DevOps 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml)包含这些任务的作业定义。

您可以使用 CI/CD 变量自动扩展您的 pod 副本，并将自定义参数应用于 Auto DevOps `helm upgrade` 命令。这是自定义 Auto Deploy Helm chart 的简单方法。

Helm 使用 auto-deploy-app chart 将应用部署到环境的 Kubernetes 命名空间。

在 11.4 及更高版本中，使用 [local Tiller](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/22036)。以前的版本在项目命名空间中安装了 Tiller。

WARNING:
您的应用*不应该*在 Helm 之外进行操作（直接使用 Kubernetes）。这可能会导致 Helm 无法检测到更改的混淆，并且使用 Auto DevOps 进行后续部署可以撤消您的更改。此外，如果您更改了某些内容并想通过再次部署来撤消它，Helm 可能一开始就没有检测到任何更改，因此不会意识到它需要重新应用旧配置。

WARNING:
14.0 版本更新 Auto Deploy 模板。由于 v2 `auto-deploy-image` 的重大更改，这可能会导致 Auto DevOps 项目意外失败。<!--Follow [the upgrade guide](upgrading_auto_deploy_dependencies.md#upgrade-guide)
to upgrade your environments before upgrading to GitLab 14.0.-->

### GitLab deploy tokens

GitLab Deploy Tokens 在启用 Auto DevOps 时为内部和私有项目创建，并保存 Auto DevOps 设置。您可以使用部署令牌永久访问镜像库。手动撤销 GitLab deploy tokens 后，它不会自动创建。

如果找不到 GitLab deploy tokens，则使用 `CI_REGISTRY_PASSWORD`。

NOTE:
`CI_REGISTRY_PASSWORD` 仅在部署期间有效。Kubernetes 可以在部署期间成功拉取容器镜像，但如果必须再次拉取镜像，例如在 pod eviction 之后，Kubernetes 无法这样做，因为它会尝试使用 `CI_REGISTRY_PASSWORD` 获取镜像。

### Kubernetes 1.16+

WARNING:
在 13.0 版本中，`deploymentApiVersion` 设置的默认值已从 `extensions/v1beta` 更改为 `apps/v1`。

在 Kubernetes 1.16 及更高版本中，删除了许多 [API](https://kubernetes.io/blog/2019/07/18/api-deprecations-in-1-16/)，包括对 `Deployment` 的支持 `extensions/v1beta1` 版本。

要在 Kubernetes 1.16+ 集群上使用 Auto Deploy：

1. 如果您是首次在 13.0 或更高版本中部署应用程序，则无需进行任何配置。

1. 在 12.10 及更早版本中，在 `.gitlab/auto-deploy-values.yaml` 文件中设置以下内容：

   ```yaml
   deploymentApiVersion: apps/v1
   ```

1. 如果您安装了集群内 PostgreSQL 数据库，并且 `AUTO_DEVOPS_POSTGRES_CHANNEL` 设置为 `1`，需要升级 PostgreSQL<!--, follow the [guide to upgrade
   PostgreSQL](upgrading_postgresql.md)-->。

1. 如果您是第一次部署应用并使用 12.9 或 12.10 版本，请将 `AUTO_DEVOPS_POSTGRES_CHANNEL` 设置为 `2`。

<!--
WARNING:
In GitLab 12.9 and 12.10, opting into
`AUTO_DEVOPS_POSTGRES_CHANNEL` version `2` deletes the version `1` PostgreSQL
database. Follow the [guide to upgrading PostgreSQL](upgrading_postgresql.md)
to back up and restore your database before opting into version `2` (On
GitLab 13.0, an additional CI/CD variable is required to trigger the database
deletion).
-->

### Migrations

您可以通过分别设置项目 CI/CD 变量 `DB_INITIALIZE` 和 `DB_MIGRATE` 来配置 PostgreSQL 的数据库初始化和迁移，在应用 pod 中运行。

如果存在，`DB_INITIALIZE` 作为 Helm post-install hook，在应用程序 pod 中作为 shell 命令运行。由于某些应用在没有成功的数据库初始化步骤的情况下无法运行，系统在没有应用部署的情况下部署第一个版本，而只有数据库初始化步骤。数据库初始化完成后，系统会正常部署应用部署的第二个版本。

请注意，post-install hook 意味着如果任何部署成功，此后不会处理 `DB_INITIALIZE`。

如果存在，`DB_MIGRATE` 作为 Helm pre-upgrade hook 在应用 pod 中作为 shell 命令运行。

例如，在使用 [Cloud Native Buildpacks](#使用-cloud-native-buildpacks-自动构建) 构建的镜像中的 Rails 应用程序中：

- `DB_INITIALIZE` 可以设置为 `RAILS_ENV=production /cnb/lifecycle/launcher bin/rails db:setup`
- `DB_MIGRATE` 可以设置为 `RAILS_ENV=production /cnb/lifecycle/launcher bin/rails db:migrate`

除非您的仓库包含 `Dockerfile`，否则您的镜像是使用 Cloud Native Buildpacks 构建的，并且您必须在这些镜像中运行前缀为 `/cnb/lifecycle/launcher` 的命令（或使用 [Herokuish](#auto-build-using-herokuish)，为 `/bin/herokuish procfile exec` )，复制应用运行的环境。

<!--
### 升级 auto-deploy-app Chart

You can upgrade the auto-deploy-app chart by following the [upgrade guide](upgrading_auto_deploy_dependencies.md).

### Workers

Some web applications must run extra deployments for "worker processes". For
example, Rails applications commonly use separate worker processes
to run background tasks like sending emails.

The [default Helm chart](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/-/tree/master/assets/auto-deploy-app)
used in Auto Deploy
[has support for running worker processes](https://gitlab.com/gitlab-org/charts/auto-deploy-app/-/merge_requests/9).

To run a worker, you must ensure the worker can respond to
the standard health checks, which expect a successful HTTP response on port
`5000`. For [Sidekiq](https://github.com/mperham/sidekiq), you can use
the [`sidekiq_alive` gem](https://rubygems.org/gems/sidekiq_alive).

To work with Sidekiq, you must also ensure your deployments have
access to a Redis instance. Auto DevOps doesn't deploy this instance for you, so
you must:

- Maintain your own Redis instance.
- Set a CI/CD variable `K8S_SECRET_REDIS_URL`, which is the URL of this instance,
  to ensure it's passed into your deployments.

After configuring your worker to respond to health checks, run a Sidekiq
worker for your Rails application. You can enable workers by setting the
following in the [`.gitlab/auto-deploy-values.yaml` file](customize.md#customize-values-for-helm-chart):

```yaml
workers:
  sidekiq:
    replicaCount: 1
    command:
      - /cnb/lifecycle/launcher
      - sidekiq
    preStopCommand:
      - /cnb/lifecycle/launcher
      - sidekiqctl
      - quiet
    terminationGracePeriodSeconds: 60
```

### Running commands in the container

Applications built with [Auto Build](#auto-build) using Herokuish, the default
unless your repository contains [a custom Dockerfile](#auto-build-using-a-dockerfile),
may require commands to be wrapped as follows:

```shell
/bin/herokuish procfile exec $COMMAND
```

Some of the reasons you may need to wrap commands:

- Attaching using `kubectl exec`.
- Using the GitLab [Web Terminal](../../ci/environments/index.md#web-terminals-deprecated).

For example, to start a Rails console from the application root directory, run:

```shell
/bin/herokuish procfile exec bin/rails c
```

When using Cloud Native Buildpacks, instead of `/bin/herokuish procfile exec`, use

```shell
/cnb/lifecycle/launcher $COMMAND
```
-->

## Auto Monitoring

应用部署后，Auto Monitoring 可帮助您立即监控应用的服务器和响应指标。Auto Monitoring 使用 Prometheus 直接从 Kubernetes 检索系统指标，例如 CPU 和内存使用情况，并从 NGINX 服务器检索响应指标，例如 HTTP 错误率、延迟和吞吐量。

指标包括：

- **响应指标：**延迟、吞吐量、错误率
- **系统指标：**CPU 利用率、内存利用率

要使用自动监控：

1. [安装和配置 Auto DevOps 要求](requirements.md)。
1. [启用 Auto DevOps](index.md#启用或禁用-auto-devops)，如果您还没有这样做的话。
1. 在左侧边栏中，选择 **CI/CD > 流水线**。
1. 选择 **运行流水线**。
1. 流水线成功完成后，打开[已部署环境的监控仪表板](../../ci/environments/index.md#监控环境)，查看已部署应用的指标。要查看整个 Kubernetes 集群的指标，请在左侧栏中选择 **监控 > 指标**。

![Auto Metrics](img/auto_monitoring.png)

## Auto Code Intelligence

GitLab Code Intelligence 添加了交互式开发环境 (IDE) 常见的代码导航功能，包括类型签名、符号文档和转到定义。它由 [LSIF](https://lsif.dev/) 提供支持，仅可用于使用 Go 语言的 Auto DevOps 项目。

<!--
随着更多 LSIF 索引器可用，GitLab 计划增加对更多语言的支持。
You can follow the [code intelligence epic](https://gitlab.com/groups/gitlab-org/-/epics/4212)
for updates.
-->

此 stage 默认启用。您可以通过添加 `CODE_INTELLIGENCE_DISABLED` CI/CD 变量来禁用它。 <!--Read more about
[disabling Auto DevOps jobs](../../topics/autodevops/customize.md#disable-jobs).-->
