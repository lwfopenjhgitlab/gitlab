---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Auto DevOps **(FREE)**

> - 对 GitLab Agent 的支持引入于 14.5 版本。

GitLab Auto DevOps 是一组预配置的功能和集成，它们协同工作以支持您的软件交付过程。

Auto DevOps 功能和集成：

- 检测您的代码的语言。
- 构建和测试您的应用程序。
- 衡量代码质量。
- 扫描漏洞和安全漏洞。
- 检查许可问题。
- 实时监控。
- 部署您的应用程序。

## Auto DevOps 功能

基于 DevOps [stages](stages.md)，使用 Auto DevOps：

**构建您的应用：**

- [Auto Build](stages.md#auto-build)
- [Auto Dependency Scanning](stages.md#auto-dependency-scanning)

**测试您的应用：**

- [Auto Test](stages.md#auto-test)
- [Auto Browser Performance Testing](stages.md#auto-browser-performance-testing)
- [Auto Code Intelligence](stages.md#auto-code-intelligence)
- [Auto Code Quality](stages.md#auto-code-quality)
- [Auto Container Scanning](stages.md#auto-container-scanning)
- [Auto License Compliance](stages.md#auto-license-compliance)

**部署您的应用：**

- [Auto Review Apps](stages.md#auto-review-apps)
- [Auto Deploy](stages.md#auto-deploy)

**监控您的应用：**

- [Auto Monitoring](stages.md#auto-monitoring)

**保护您的应用：**

- [Auto Dynamic Application Security Testing (DAST)](stages.md#auto-dast)
- [Auto Static Application Security Testing (SAST)](stages.md#auto-sast)
- [Auto Secret Detection](stages.md#auto-secret-detection)

### 工作原理

Auto DevOps 检测您的代码语言并使用 [CI/CD 模板](https://jihulab.com/gitlab-cn/gitlab/-/tree/master/lib/gitlab/ci/templates) 创建和运行默认流水线。您需要做到只是[启用](#启用或禁用-auto-devops)它。

Auto DevOps 从构建和测试您的应用开始。然后，根据您的[预定义部署配置](requirements.md)，创建必要的作业，将您的应用部署到 staging 和/或生产环境。它还设置了 [Review Apps](stages.md#auto-review-apps)，以便您可以预览每个分支的更改。

请注意，您不需要预先设置部署。Auto DevOps 仍会构建和测试您的应用。您可以稍后定义部署。

Auto DevOps 避免了手动创建整个流水线的麻烦。
保持简单并促进迭代方法：首先发布您的应用程序，然后再探索自定义<!--[customizations](customize.md)-->。
<!--You can also [manage Auto DevOps with APIs](customize.md#extend-auto-devops-with-the-api).-->

在工作流程中使用 Auto DevOps 的优势是：

- 一致性：始终从默认模板开始。
- 简单性：首先使用默认设置创建您的流水线，稍后进行迭代。
- 生产力：在短时间内部署多个应用程序。
- 高效率：快速完成工作。

### 与应用平台和 PaaS 的比较

Auto DevOps 提供通常包含在应用程序平台或平台即服务 (PaaS) 中的功能。

受 [Heroku](https://www.heroku.com/) 的启发，Auto DevOps 在多个方面超越了它：

- Auto DevOps 适用于任何 Kubernetes 集群。
- 没有额外费用。
- 您可以使用自己托管的集群或任何公共云上的集群。
- Auto DevOps 提供了一个渐进式的毕业路径。如果您需要自定义<!--[customize](customize.md)-->，请从更改模板开始并从那里发展。

## 开始使用 Auto DevOps

要开始使用，您只需[启用 Auto DevOps](#启用或禁用-auto-devops)。
这足以运行 Auto DevOps 流水线来构建和测试您的应用。

如果您想构建、测试和部署您的应用：

1. 查看[部署要求](requirements.md).
1. [启用 Auto DevOps](#启用或禁用-auto-devops).

<!--
1. Follow the [quick start guide](#quick-start).
-->

由于 Auto DevOps 依赖于许多组件，因此请熟悉：

- Continuous methodologies
- [Docker](https://docs.docker.com)
- GitLab Runner

部署到 Kubernetes 集群时，请确保您熟悉：

- [Kubernetes](https://kubernetes.io/docs/home/)
- [Helm](https://helm.sh/docs/)
- [Prometheus](https://prometheus.io/docs/introduction/overview/)

<a id="enable-or-disable-auto-devops"></a>

### 启用或禁用 Auto DevOps

根据您的实例类型，您可以在以下级别启用或禁用 Auto DevOps：

| 实例类型       | [项目](#项目级别) | [群组](#群组级别) | [实例](#实例级别) (管理中心)  |
|---------------------|------------------------|------------------------|------------------------|
| SaaS         | **{check-circle}** Yes | **{check-circle}** Yes | **{dotted-circle}** No |
| 自助管理版 | **{check-circle}** Yes | **{check-circle}** Yes | **{check-circle}** Yes |

在启用 Auto DevOps 之前，请考虑[准备好部署](requirements.md)。否则，Auto DevOps 可以构建和测试您的应用，但无法部署它。

#### 项目级别

要将 Auto DevOps 用于单个项目，您可以逐个项目启用它。如果您打算将其用于更多项目，您可以为[群组](#群组级别)或[实例](#实例级别)启用它。这样可以节省您一一启用的时间。

只有项目维护者可以在项目级别启用或禁用 Auto DevOps。

在启用 Auto DevOps 之前，请确保您的项目不存在 `.gitlab-ci.yml`。如果存在，您的 CI/CD 配置优先于 Auto DevOps 流水线。

要为项目启用 Auto DevOps：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏，选择 **设置 > CI/CD**。
1. 展开 **Auto DevOps**。
1. 选中 **默认为 Auto DevOps 流水线** 复选框。
1. 可选但推荐。添加 [基础域名](requirements.md#auto-devops-基础域名)。
1. 可选但推荐。选择 [部署策略](requirements.md#auto-devops-部署策略)。
1. 选择 **保存修改**。

极狐GitLab 在默认分支上触发 Auto DevOps 流水线。

要禁用它，请按照相同的过程并清除 **默认为 Auto DevOps 流水线** 复选框。

#### 群组级别

在群组级别启用 Auto DevOps 时，该群组中的子组和项目会继承配置。这样可以通过批量启用而不是为每个子组或项目单独启用来节省您的时间。

为组启用后，您仍然可以为不想使用的子组和项目禁用 Auto DevOps。

只有管理员和群组所有者可以在群组级别启用或禁用 Auto DevOps。

为群组启用 Auto DevOps：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的组。
1. 在左侧边栏，选择 **设置 > CI/CD**。
1. 展开 **Auto DevOps**。
1. 选中 **默认为 Auto DevOps 流水线** 复选框。
1. 选择 **保存修改**。

要在群组级别禁用 Auto DevOps，请遵循相同的过程并清除 **默认为 Auto DevOps 流水线** 复选框。

在群组级别启用 Auto DevOps 后，您可以为属于该组的任何项目触发 Auto DevOps 流水线。 为此：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 确保项目不包含 `.gitlab-ci.yml`文件。
1. 在左侧边栏中，选择 **CI/CD > 流水线**。
1. 要触发 Auto DevOps 流水线，请选择 **运行流水线**。

#### 实例级别 **(FREE SELF)**

通过在实例级别启用 Auto DevOps，在该实例中创建的所有项目都将启用。当您想为所有项目默认运行 Auto DevOps 时，这很方便。您仍然可以为不想运行的群组和项目单独禁用 Auto DevOps。

只有管理员可以在实例级别启用或禁用 Auto DevOps。

即使对某个实例禁用，群组所有者和项目维护者仍然可以在群组和项目级别启用 Auto DevOps。

要为您的实例启用 Auto DevOps：

1. 在顶部栏上，选择 **菜单 > 管理员**。
1. 在左侧边栏，选择 **设置 > CI/CD**。
1. 展开 **Auto DevOps**。
1. 选中 **默认为 Auto DevOps 流水线** 复选框。
1. 可选。添加 Auto DevOps [基础域名](requirements.md#auto-devops-基础域名)。
1. 选择 **保存修改**。

启用后会尝试在每个项目中运行 Auto DevOps 流水线。如果流水线在特定项目中失败，将自行禁用。实例管理员可以在 [Auto DevOps 设置](../../user/admin_area/settings/continuous_integration.md#auto-devops)中更改此设置。

如果 [CI/CD 配置文件](../../ci/yaml/index.md) 存在，它保持不变并且 Auto DevOps 不会影响它。

要在实例级别禁用 Auto DevOps，请遵循相同的流程并清除 **默认为 Auto DevOps 流水线** 复选框。

<!--
### Quick start

To guide your through the process of setting up Auto DevOps to deploy to a Kubernetes cluster on
Google Kubernetes Engine (GKE), see the [quick start guide](quick_start_guide.md).

You can also follow the quick start for the general steps, but deploy to
[AWS ECS](requirements.md#auto-devops-requirements-for-amazon-ecs) instead.

If you're a self-managed user, before deploying to GKE, a GitLab administrator needs to:

1. Configure the [Google OAuth 2.0 OmniAuth Provider](../../integration/google.md).
1. Configure a cluster on GKE.
-->

## 更新极狐GitLab 时升级 Auto DevOps 依赖项

更新极狐GitLab 时，您可能需要升级 Auto DevOps 依赖项，匹配您的极狐GitLab 新版本：

- 升级 Auto DevOps 资源<!--[升级 Auto DevOps 资源](upgrading_auto_deploy_dependencies.md)-->：
   - 自动 DevOps 模板。
   - 自动部署模板。
   - 自动部署镜像。
   - Helm。
   - Kubernetes。
   - 环境变量。
- 升级 PostgreSQL<!--[升级 PostgreSQL](upgrading_postgresql.md)-->。

## 限制

### 私有 registry 支持

我们不能保证您可以将私有容器镜像库与 Auto DevOps 一起使用。

我们强烈建议您使用带有 Auto DevOps 的 GitLab Container Registry 来简化配置并防止任何不可预见的问题。

### 在代理后面安装应用

极狐GitLab 与 Helm 的集成不支持在代理后面安装应用。

为此，请在运行时将代理设置注入安装 pod。
例如，您可以使用 [`PodPreset`](https://v1-19.docs.kubernetes.io/docs/concepts/workloads/pods/podpreset/)：

```yaml
apiVersion: settings.k8s.io/v1alpha1
kind: PodPreset
metadata:
  name: gitlab-managed-apps-default-proxy
  namespace: gitlab-managed-apps
spec:
  env:
    - name: http_proxy
      value: "PUT_YOUR_HTTP_PROXY_HERE"
    - name: https_proxy
      value: "PUT_YOUR_HTTPS_PROXY_HERE"
```

## 故障排查

查看[故障排查文档](troubleshooting.md)。
