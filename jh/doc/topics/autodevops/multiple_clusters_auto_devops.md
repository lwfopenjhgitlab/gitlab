---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 多 Kubernetes 集群的 Auto DevOps **(FREE)**

使用 Auto DevOps 时，您可以将不同的环境部署到不同的 Kubernetes 集群，因为它们之间存在 1:1 连接。

Auto DevOps 使用的[部署作业模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml)定义了 3 个环境名称：

- `review/`（每个以 `review/` 开头的环境）
- `staging`
- `production`

这些环境使用 [Auto Deploy](stages.md#auto-deploy) 与作业绑定，因此除了环境范围外，它们必须具有不同的部署域名。
您必须[根据环境](../../ci/variables/index.md#limit-the-environment-scope-of-a-cicd-variable)为上述每个变量定义一个单独的 `KUBE_INGRESS_BASE_DOMAIN` 变量。

下表是如何配置三个不同集群的示例：

| 集群名称 | 集群环境范围 | `KUBE_INGRESS_BASE_DOMAIN` 变量值 | 变量环境范围 | 备注 |
|--------------|---------------------------|-------------------------------------------|----------------------------|---|
| review       | `review/*`                | `review.example.com`                      | `review/*`                 | 运行所有 [Review Apps](../../ci/review_apps/index.md) 的集群。`*` 是通配符，由以 `review/` 开头的每个环境名称使用。 |
| staging      | `staging`                 | `staging.example.com`                     | `staging`                  | 可选。运行 staging 环境部署的 staging 集群。您必须[首先启用它](customize.md#deploy-policy-for-staging-and-production-environments)。 |
| production   | `production`              | `example.com`                             | `production`               | 运行生产环境部署的生产集群。您可以使用[增量部署](customize.md#incremental-rollout-to-production)。 |

要为每个环境添加不同的集群：

1. 导航到您项目的 **基础设施 > Kubernetes 集群**。
1. 创建具有各自环境范围的 Kubernetes 集群，如上表所述。
1. 创建集群后，导航到每个集群并安装 Ingress。等待分配 Ingress IP 地址。
1. 确保您已使用指定的 Auto DevOps 域名[配置了您的 DNS](requirements.md#auto-devops-base-domain)。
1. 通过 **基础设施 > Kubernetes 集群** 导航到每个集群的页面，并根据其 Ingress IP 地址添加域名。

完成配置后，通过创建合并请求来测试您的设置。
验证您的应用程序是否在具有 `review/*` 环境范围的 Kubernetes 集群中，部署为 Review 应用程序。同样，检查其它环境。

检查活动 Kubernetes 集群时不考虑集群环境范围。要使多集群设置与 Auto DevOps 一起使用，请创建一个将**集群环境范围**设置为 `*` 的备用集群，不需要新的集群，您可以使用任何已添加的集群。
