---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Auto DevOps 故障排查 **(FREE)**

本文档页面中的信息描述了使用 Auto DevOps 时的常见错误以及任何可用的解决方法。

## 跟踪 Helm 命令

将 CI/CD 变量 `TRACE` 设置为任何值，以使 Helm 命令产生详细的输出。您可以使用此输出来诊断 Auto DevOps 部署问题。

您可以通过更改高级 Auto DevOps 配置变量来解决 Auto DevOps 部署的一些问题。阅读有关[自定义 Auto DevOps CI/CD 变量](customize.md#cicd-variables)的更多信息。

## 无法选择 buildpack

自动构建和自动测试可能无法检测到您的语言或框架，并出现以下错误：

```plaintext
Step 5/11 : RUN /bin/herokuish buildpack build
 ---> Running in eb468cd46085
    -----> Unable to select a buildpack
The command '/bin/sh -c /bin/herokuish buildpack build' returned a non-zero code: 1
```

以下是可能的原因：

- 您的应用可能缺少 buildpack 正在寻找的关键文件。Ruby 应用需要正确检测 `Gemfile`，即使没有 `Gemfile` 也可以编写 Ruby 应用。
- 您的应用可能不存在 buildpack。尝试指定自定义 buildpack<!--[自定义 buildpack](customize.md#custom-buildpacks)-->。

## Pipeline that extends Auto DevOps with only / except fails

如果您的流水线失败并显示以下消息：

```plaintext
Found errors in your .gitlab-ci.yml:

  jobs:test config key may not be used with `rules`: only
```

当包含作业的规则配置被 `only` 或 `except` 语法覆盖时，会出现此错误。
要解决此问题，您必须：

- 将您的 `only/except` 语法转换为规则。
- （临时）将您的模板固定到基于 12.10 的模板。

<!--
## 未能创建 Kubernetes 命名空间

如果 GitLab 无法为您的项目创建 Kubernetes 命名空间和服务帐户，则 Auto Deploy 会失败。有关调试此问题的帮助，请参阅 [对失败的部署作业进行故障排除](../../user/project/clusters/deploy_to_cluster.md#troubleshooting)。
-->

<a id="detected-an-existing-postgresql-database"></a>

## 检测到现有的 PostgreSQL 数据库

升级到 13.0 版本后，使用 Auto DevOps 进行部署时可能会遇到此消息：

```plaintext
Detected an existing PostgreSQL database installed on the
deprecated channel 1, but the current channel is set to 2. The default
channel changed to 2 in of GitLab 13.0.
[...]
```

默认情况下，Auto DevOps 会在您的应用旁边安装一个集群内 PostgreSQL 数据库。13.0 版本中更改了默认安装方法，升级现有数据库需要用户参与。 两种安装方法是：

- **channel 1（已弃用）：**将数据库作为关联 Helm chart 的依赖项拉入。仅支持最高 1.15 版的 Kubernetes 版本。
- **channel 2（当前）：**将数据库安装为独立的 Helm chart。将集群内数据库功能与 Kubernetes 1.16 及更高版本一起使用时需要。

如果您收到此错误，您可以执行以下操作之一：

- 您可以*安全地*忽略警告并通过将 `AUTO_DEVOPS_POSTGRES_CHANNEL` 设置为 `1` 并重新部署来继续使用 channel 1 PostgreSQL 数据库。

- 您可以通过将 `AUTO_DEVOPS_POSTGRES_DELETE_V1` 设置为非空值并重新部署来删除 channel 1 PostgreSQL 数据库并安装新的 channel 2 数据库。

  WARNING:
  删除 channel 1 PostgreSQL 数据库将永久删除现有 channel 1 数据库及其所有数据。<!--有关备份和升级数据库的更多信息，请参阅 [升级 PostgreSQL](upgrading_postgresql.md)。-->

- 如果不使用集群内数据库，可以将 `POSTGRES_ENABLED` 设置为 `false` 并重新部署。此选项与*自定义 chart 的用户特别相关，没有 chart 内 PostgreSQL 依赖项*。数据库自动检测基于您发布的 `postgresql.enabled` Helm 值。这个值是基于 `POSTGRES_ENABLED` CI/CD 变量设置的，并由 Helm 持久化，无论您的 chart 是否使用该变量。

WARNING:
将 `POSTGRES_ENABLED` 设置为 `false` 会永久删除您环境中任何现有的 channel 1 数据库。

## Error: unable to recognize "": no matches for kind "Deployment" in version "extensions/v1beta1"

将 Kubernetes 集群升级到 [v1.16+](stages.md#kubernetes-116) 后，使用 Auto DevOps 进行部署时可能会遇到此消息：

```plaintext
UPGRADE FAILED
Error: failed decoding reader into objects: unable to recognize "": no matches for kind "Deployment" in version "extensions/v1beta1"
```

如果您当前在环境命名空间上的部署是使用 Kubernetes v1.16+ 中不存在的已弃用/删除的 API 部署的，则可能会发生这种情况。例如，如果您的集群内 PostgreSQL 以传统方式安装，则资源是通过 `extensions/v1beta1` API 创建的。但是，在 v1.16 中，deployment 资源已移至 `app/v1` API。

要恢复这些过时的资源，您必须通过将旧 API 映射到较新的 API 来转换当前部署。有一个名为 [`mapkubeapis`](https://github.com/hickeyma/helm-mapkubeapis) 的帮助工具可以解决这个问题。按照以下步骤在 Auto DevOps 中使用该工具：

1. 修改您的`.gitlab-ci.yml`：

   ```yaml
   include:
     - template: Auto-DevOps.gitlab-ci.yml
     - remote: https://gitlab.com/shinya.maeda/ci-templates/-/raw/master/map-deprecated-api.gitlab-ci.yml

   variables:
     HELM_VERSION_FOR_MAPKUBEAPIS: "v2" # If you're using auto-depoy-image v2 or above, please specify "v3".
   ```

1. 运行作业 `<environment-name>:map-deprecated-api`。确保此作业成功，然后再进行下一步。您应该看到类似于以下输出的内容：

   ```shell
   2020/10/06 07:20:49 Found deprecated or removed Kubernetes API:
   "apiVersion: extensions/v1beta1
   kind: Deployment"
   Supported API equivalent:
   "apiVersion: apps/v1
   kind: Deployment"
   ```

1. 将你的 `.gitlab-ci.yml` 恢复到以前的版本。您不再需要包含补充模板 `map-deprecated-api`。

1. 像往常一样继续部署。

## Error: error initializing: Looks like "https://kubernetes-charts.storage.googleapis.com" is not a valid chart repository or cannot be reached

如[在 CNCF 官方博客文章中所宣布](https://www.cncf.io/blog/2020/10/07/important-reminder-for-all-helm-users-stable-incubator-repos-are-deprecated-and-all-images-are-changing-location/)，stable Helm chart repository 已于 2020 年 11 月 13 日弃用并删除。
在该日期之后，您可能会遇到此错误。

一些功能依赖于 stable chart。为了减轻影响，我们将它们更改为使用新的官方仓库或 GitLab 维护的 Helm Stable Archive 仓库。
Auto Deploy 包含一个示例修复。

在 Auto Deploy 中，`auto-deploy-image` 的 `v1.0.6+` 不再将已弃用的 stable 仓库添加到 `helm` 命令。如果您使用自定义 chart 并且它依赖于已弃用的 stable 仓库，请指定较旧的 `auto-deploy-image`，如下例所示：

```yaml
include:
  - template: Auto-DevOps.gitlab-ci.yml

.auto-deploy:
  image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:v1.0.5"
```

请记住，当 stable 仓库被删除时，此方法将停止工作，因此您最终必须修复您的自定义 chart。

要修复您的自定义 chart：

1. 在您的 chart 目录中，更新 `requirements.yaml` 文件中的 `repository` 值：

   ```yaml
   repository: "https://kubernetes-charts.storage.googleapis.com/"
   ```

   改为：

   ```yaml
   repository: "https://charts.helm.sh/stable"
   ```

1. 在您的 chart 目录中，使用与 Auto DevOps 相同的 Helm 主版本运行 `helm dep update .`。
1. 提交对 `requirements.yaml` 文件的更改。
1. 如果您之前有一个 `requirements.lock` 文件，请将更改提交到该文件。如果您之前在 chart 中没有 `requirements.lock` 文件，则无需提交新文件。该文件是可选的，但如果存在，它用于验证下载的依赖项的完整性。

<!--
You can find more information in
[issue #263778, "Migrate PostgreSQL from stable Helm repository"](https://gitlab.com/gitlab-org/gitlab/-/issues/263778).
-->

## Error: release .... failed: timed out waiting for the condition

开始使用 Auto DevOps 时，您可能会在首次部署应用时遇到此错误：

```plaintext
INSTALL FAILED
PURGING CHART
Error: release staging failed: timed out waiting for the condition
```

这很可能是由于在部署过程中尝试的 liveness（或 readiness）探测失败造成的。默认情况下，这些探测器在端口 5000 上针对已部署应用的根页面运行。如果您的应用程序未配置为在根页面提供任何服务，或者配置为在 5000 以外的特定端口上运行，则此检查失败。

如果失败，您应该在相关 Kubernetes 命名空间的事件中看到这些失败，类似于以下示例：

```plaintext
LAST SEEN   TYPE      REASON                   OBJECT                                            MESSAGE
3m20s       Warning   Unhealthy                pod/staging-85db88dcb6-rxd6g                      Readiness probe failed: Get http://10.192.0.6:5000/: dial tcp 10.192.0.6:5000: connect: connection refused
3m32s       Warning   Unhealthy                pod/staging-85db88dcb6-rxd6g                      Liveness probe failed: Get http://10.192.0.6:5000/: dial tcp 10.192.0.6:5000: connect: connection refused
```

要更改用于 liveness 检查的端口，请将自定义值传递给 Auto DevOps 使用的 Helm chart<!--[自定义值传递给 Auto DevOps 使用的 Helm 图表](customize.md#customize-values-for-helm-chart)-->：

1. 在仓库的根目录下创建一个名为 `.gitlab/auto-deploy-values.yaml` 的目录和文件。

1. 使用以下内容填充文件，将端口值替换为应用配置使用的实际端口号：

   ```yaml
   service:
     internalPort: <port_value>
     externalPort: <port_value>
   ```

1. 提交您的更改。

提交更改后，后续探测应使用新定义的端口。
被探测的页面也可以通过以相同的方式覆盖 `livenessProbe.path` 和 `readinessProbe.path` 值（显示在默认的 `values.yaml` 文件中）来更改。
