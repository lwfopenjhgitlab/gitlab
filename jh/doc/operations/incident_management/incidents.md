---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 事件 **(FREE)**

事件是需要紧急恢复的服务中断。
事件在事件管理工作流程中至关重要。
使用极狐GitLab 对事件进行分类、响应和补救。

## 事件列表

当您[查看事件列表](manage_incidents.md#view-incidents-list)时，它包含以下内容：

- 要过滤事件，请选择事件列表上方的 **打开**、**已关闭** 或 **全部**。
- **搜索**：搜索事件标题和描述或[过滤列表](#filter-the-incidents-list)。
- **严重性**：特定事件的严重性，可以是以下值之一：
  - **{severity-critical}** **严重 - S1**
  - **{severity-high}** **高 - S2**
  - **{severity-medium}** **中 - S3**
  - **{severity-low}** **低 - S4**
  - **{severity-unknown}** **未知**

  13.4 版本中引入了事件详情页面上的[编辑事件严重性](#change-severity)功能：

- **事件**：事件的描述，包含事件最有意义的数据。
- **状态**：事件的状态，可以是以下值之一：

  - **已触发**
  - **已确认**
  - **已解决**

  在专业版或旗舰中，此字段还关联到事件的 [on-call 升级](paging.md#escalating-an-incident)。

- **创建日期**：创建事件的时间。此字段使用 `X time ago` 的标准 pattern，但根据用户的区域设置，由日期/时间提示工具支持。
- **指派人**：指派给事件的用户。
- **已发布**：如果事件发布到状态页面<!--[状态页面](status_page.md)-->，则显示绿色复选标记 (**{check-circle}**)。

事件列表显示按事件创建日期排序的事件（引入于 13.3 版本。）
要查看列是否可排序，请将鼠标指向标题。可排序的列会在列名称旁边显示一个箭头。

### 对事件列表进行排序

> 引入于 13.3 版本：默认情况下，事件按创建日期排序。

事件列表显示按事件创建日期排序的事件，首先显示最新的。

要按另一列排序或更改排序顺序，请选择该列。

您可以排序的列：

- 严重性
- 状态
- SLA 时间
- 已发布

### 过滤事件列表

要按作者或指派人过滤事件列表，请在搜索框中输入这些值。

## 事件详情

> 引入于 13.4 版本。

至少拥有访客权限的用户可以查看事件详情页面。导航到项目侧栏中的 **监控 > 事件**，然后从列表中选择一个事件。

当您对事件采取任何这些操作时，系统会记录系统备注，并将其显示在事件详情视图中：

- 更新事件的严重性（引入于 13.5 版本。）

<!--
For live examples of GitLab incidents, visit the `tanuki-inc` project's
[incident list page](https://gitlab.com/gitlab-examples/ops/incident-setup/everyone/tanuki-inc/-/incidents).
Select any incident in the list to display its incident details page.
-->

<a id="summary"></a>

### 摘要

事件摘要部分提供有关事件的关键详细信息和议题模板的内容（如果适用）。事件顶部的高亮栏从左到右显示：

- 原始警报的链接。
- 警报开始时间。
- 事件计数。

在高亮条下方，系统显示一个摘要，其中包括以下字段：

- 开始时间
- 严重性
- `full_query`
- 监控工具

可以使用[极狐GitLab Flavored Markdown](../../user/markdown.md) 进一步自定义事件摘要。如果事件是[从警报创建的](../metrics/alerts.md#trigger-actions-from-alerts)，它为事件提供了 Markdown，那么 Markdown 将附加到摘要中。如果为项目配置了事件模板，则模板内容附加在末尾。

评论显示在主题中，但可以[通过切换最近更新视图](#recent-updates-view)，按时间顺序显示。

当您对事件进行更改时，极狐GitLab 会创建系统备注并将其显示在摘要下方。

### 指标 **(PREMIUM)**

> 引入于 13.8 版本。

在许多情况下，事件与指标相关联。您可以在**指标**选项卡中，上传指标图表的屏幕截图：

![Incident Metrics tab](img/incident_metrics_tab_v13_8.png)

上传图像时，您可以将图像与文本或指向原始图表的链接相关联。

![Text link modal](img/incident_metrics_tab_text_link_modal_v14_9.png)

如果添加链接，您可以通过单击上传图像上方的超链接访问原始图表。

### 警报详情

事件在单独的选项卡中显示关联警报的详细信息。要填充此选项卡，必须使用关联警报创建事件。从警报自动创建的事件已填充此字段。

![Incident alert details](img/incident_alert_details_v13_4.png)

### 时间线事件

> 引入于 15.2 版本，[功能标志](../../administration/feature_flags.md)为 `incident_timeline`。在 SaaS 版上启用，在私有化部署版上禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，询问管理员[启用功能标志](../../administration/feature_flags.md) `incident_timeline`。在 SaaS 上，此功能可用。

事件时间表是事件记录的重要组成部分，向管理者和外部人士提供了事件期间发生的事情，以及为解决该事件而采取的步骤的高级别概述。

#### 查看时间线事件

时间线事件按日期和时间的升序列出。它们按日期分组，并按发生时间的升序排列：

![Incident timeline events list](img/timeline_events_v15_1.png)

要查看时间线：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **监控 > 事件**。
1. 选择一个事件。
1. 选择 **时间线** 选项卡。

#### 创建时间线事件

使用表单手动创建时间线事件。

先决条件：

- 您必须至少具有项目的开发者角色。

要创建时间线事件：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **监控 > 事件**。
1. 选择一个事件。
1. 选择 **时间线** 选项卡。
1. 选择 **添加新的时间线事件**。
1. 填写必填字段。
1. 选择 **保存** 或 **保存并添加另一个事件**。

#### 删除时间线事件

您还可以删除时间线事件。

先决条件：

- 您必须至少具有项目的开发者角色。

要删除时间线事件：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **监控 > 事件**。
1. 选择一个事件。
1. 选择 **时间线** 选项卡。
1. 在时间线事件的右侧，选择 **更多操作** (**{ellipsis_v}**)，然后选择 **删除**。
1. 要确认，请选择 **删除事件**。

<a id="recent-updates-view"></a>

### 最近更新视图 **(PREMIUM)**

> 引入于 13.5 版本。

要查看事件的最新更新，请在评论栏上选择 **打开最近的更新视图** (**{history}**)。评论按时间顺序显示，从最新到最旧。

### SLA 倒计时 **(PREMIUM)**

> 引入于 13.5 版本。

您可以对事件启用 SLA（Service Level Agreement）倒计时器，跟踪您与客户持有的 SLA。创建事件时，计时器会自动启动，并显示 SLA 期限到期之前的剩余时间。计时器也每 15 分钟动态更新一次，因此您无需刷新页面即可查看剩余时间。
配置定时器：

先决条件：

- 您必须至少具有项目的维护者角色。

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 导航至 **设置 > 监控**。
1. 滚动到 **事件** 并选择 **展开**，然后选择 **事件设置** 选项卡。
1. 选择 **启用距 SLA 时间倒计时器**。
1. 以 15 分钟为增量设置时间限制。
1. 选择 **保存修改**。

启用 SLA 倒计时器后，**距 SLA 时间:** 属性将显示为事件列表中的一列，并显示为新创建事件的字段。如果事件在 SLA 结束之前没有关闭，系统会在事件中添加一个 `missed::SLA` 标记。

<!--
## Related topics

- [Create an incident](manage_incidents.md#create-an-incident)
- [Create an incident automatically](../metrics/alerts.md#trigger-actions-from-alerts)
  whenever an alert is triggered
- [View incidents list](manage_incidents.md#view-incidents-list)
- [Assign to a user](manage_incidents.md#assign-to-a-user)
- [Change incident severity](manage_incidents.md#change-severity)
- [Change incident status](manage_incidents.md#change-status)
- [Change escalation policy](manage_incidents.md#change-escalation-policy)
- [Embed metrics](manage_incidents.md#embed-metrics)
- [Close an incident](manage_incidents.md#close-an-incident)
- [Automatically close incidents via recovery alerts](manage_incidents.md#automatically-close-incidents-via-recovery-alerts)
- [Add a to-do item](../../user/todos.md#create-a-to-do-item)
- [Add labels](../../user/project/labels.md)
- [Assign a milestone](../../user/project/milestones/index.md)
- [Make an incident confidential](../../user/project/issues/confidential_issues.md)
- [Set a due date](../../user/project/issues/due_dates.md)
- [Toggle notifications](../../user/profile/notifications.md#edit-notification-settings-for-issues-merge-requests-and-epics)
- [Track spent time](../../user/project/time_tracking.md)
- [Add a Zoom meeting to an incident](../../user/project/issues/associate_zoom_meeting.md) the same
  way you add it to an issue.
- [Linked resources in incidents](linked_resources.md)
- Create incidents and receive incident notifications [directly from Slack](slack.md).
- Use the [Issues API](../../api/issues.md) to interact with incidents.
-->