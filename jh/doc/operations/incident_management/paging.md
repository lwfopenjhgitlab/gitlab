---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 呼叫和通知 **(FREE)**

当出现新的警报或事件时，重要的是要立即通知响应者，以便他们可以对问题进行分类和响应。响应者可以使用本页描述的方法接收通知。

## Slack 通知

> 引入于 13.1 版本。

可以使用 Slack 通知服务，通过 Slack 通知响应者，您可以针对新警报和新事件进行配置。配置后，响应者通过 Slack 收到**单个**呼叫。要在您的移动设备上设置 Slack 通知，请确保在您的手机上为 Slack 应用程序启用通知，这样您就不会错过任何一个呼叫。

<a id="email-notifications-for-alerts"></a>

## 警报的电子邮件通知

电子邮件通知在项目中可用于已触发警报。具有 **所有者** 或 **维护者** 角色的项目成员可以选择接收新警报的单个电子邮件通知。

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 监控**。
1. 展开 **警报**。
1. 在 **警报设置** 选项卡上，选中 **向所有者和维护者发送一封电子邮件通知以获取新警报** 复选框。
1. 选择 **保存修改**。

[更新警报状态](alerts.md#update-an-alerts-status)，管理警报的电子邮件通知。

<a id="paging"></a>

## 呼叫 **(PREMIUM)**

在配置了[升级策略](escalation_policies.md)的项目中，可以通过电子邮件自动呼叫待命响应者有关严重问题的信息。

<a id="escalating-an-alert"></a>

### 升级警报

当警报被触发时，它会立即升级到待命的响应者。
对于项目升级策略中的每个升级规则，指定的待命响应者会在规则触发时收到一封电子邮件。您可以通过[更新警报状态](alerts.md#update-an-alerts-status)，来响应呼叫或停止警报升级。

### 升级事件

> - 引入于 14.9 版本，功能标志为 `incident_escalations`。默认禁用。
> - 在 SaaS 版和私有化部署版上启用于 14.10 版本。
> - 功能标志 `incident_escalations` 删除于 15.1 版本。

对于每个单独的事件，呼叫待命响应者是可选的。

要开始升级事件，请[设置事件的升级策略](manage_incidents.md#change-escalation-policy)。

对于每个升级规则，指定的待命响应者会在规则触发时收到一封电子邮件。您可以通过[更新事件状态](manage_incidents.md#change-incident-status)，或将事件的升级策略改回**无升级策略**，来响应呼叫或停止事件升级。

在 15.1 及更早版本中，[从警报创建的事件](alerts.md#create-an-incident-from-an-alert)不支持独立升级。在 15.2 及更高版本中，所有事件都可以独立升级。
