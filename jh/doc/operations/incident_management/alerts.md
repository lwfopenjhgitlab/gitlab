---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 警报 **(FREE)**

警报是事件管理工作流程中的关键实体，它们代表可能表明服务中断事件，系统提供了用于分类的列表视图和详细视图，以便更深入地调查所发生的事情。

<a id="alert-list"></a>

## 警报列表

至少具有开发者角色的用户可以在项目侧栏中的 **监控 > 警报** 访问警报列表。警报列表显示按开始时间排序的警报，但您可以通过单击警报列表中的标题来更改排序顺序。

警报列表显示以下信息：

![Alert List](img/alert_list_v13_1.png)

- **搜索**：警报列表支持对标题、描述、监控工具和服务字段进行简单的自由文本搜索。（引入于 13.1 版本。）
- **严重性**：警报的当前重要性以及它应该得到多少关注。有关所有状态的列表，请阅读[警报管理严重性](#alert-severity)。
- **开始时间**：警报触发的时间。此字段使用 `X time ago` 的标准 pattern，但根据用户的区域设置，由日期/时间提示工具细化支持。
- **警报描述**：警报的描述，它试图捕获最有意义的数据。
- **事件计数**：警报触发的次数。
- **议题**：已为警报创建的事件议题的链接。
- **状态**：警报的当前状态：
  - **已触发**：调查尚未开始。
  - **已确认**：有人正在积极调查该问题。
  - **已解决**：无需进一步工作。
  - **已忽略**：未对警报采取任何措施。

<a id="alert-severity"></a>

## 警报严重性

每个级别的警报都包含一个形状独特且颜色编码的图标，可帮助您识别特定警报的严重性。这些严重性图标可帮助您立即确定应优先调查哪些警报：

![Alert Management Severity System](img/alert_management_severity_v13_0.png)

警报包含以下图标之一：

<!-- vale gitlab.SubstitutionWarning = NO -->

| 严重性 | 图标                    | 颜色（十六进制） |
|----------|-------------------------|---------------------|
| Critical | **{severity-critical}** | `#8b2615`           |
| High     | **{severity-high}**     | `#c0341d`           |
| Medium   | **{severity-medium}**   | `#fca429`           |
| Low      | **{severity-low}**      | `#fdbc60`           |
| Info     | **{severity-info}**     | `#418cd8`           |
| Unknown  | **{severity-unknown}**  | `#bababa`           |

<!-- vale gitlab.SubstitutionWarning = YES -->

<a id="alert-details-page"></a>

## 警报详情页面

通过访问[警报列表](alerts.md)，并从列表中选择警报，导航到警报详情视图。您至少需要开发者角色才能访问警报。

警报提供 **概览** 和 **警报详情** 选项卡，为您提供所需的适量信息。

### 警报详情选项卡

**警报详情**选项卡有两个部分。顶部提供了关键详细信息的简短列表，例如严重性、开始时间、事件数量和原始监控工具。第二部分显示完整的警报负载。

### 指标选项卡

> - 引入于 13.2 版本。
> - 变更于 14.10 版本。在 14.9 及更早版本，此选项卡显示来自 Prometheus 的警报的指标图表。

在许多情况下，警报与指标相关联。您可以在**指标**选项卡中，上传指标图表的屏幕截图：

- 选择 **上传**，然后从您的文件浏览器中选择一个图像。
- 从您的文件浏览器中拖动一个文件并将其放入拖放区。

上传图像时，您可以向图像添加文本并将其链接到原始图形。

![Text link modal](img/incident_metrics_tab_text_link_modal_v14_9.png)

如果添加链接，它会显示在上传的图像上方。

### 活动订阅选项卡

> 引入于 13.1 版本。

**活动订阅**选项卡是警报上的活动日志。当您对警报采取措施时，将被记录为系统备注，为您提供警报调查和指派历史的线性时间线。

以下操作会产生系统备注：

- [更新警报的状态](#update-an-alerts-status)
- [根据警报创建事件](manage_incidents.md#from-an-alert)
- [指派警报到用户](#assign-an-alert)
- [将警报升级到待命响应者](paging.md#escalating-an-alert)

![Alert Details Activity Feed](img/alert_detail_activity_feed_v13_5.png)

## 警报操作

极狐GitLab 中有不同的操作可用于帮助分类和响应警报。

<a id="change-an-alerts-status"></a>

### 更改警报状态

您可以更改警报的状态。

可用的状态是：

- 已触发（新警报的默认值）
- 已确认
- 已解决

先决条件：

- 您必须至少具有开发者角色。

要更改警报的状态：

- 从[警报列表](#alert-list)：

  1. 在 **状态** 列中，在警报旁边，选择状态下拉列表。
  1. 选择一个状态。

- 从[警报详情页](#alert-details-page)：

  1. 在右侧栏中，选择 **编辑**。
  1. 选择一个状态。

要在[启用电子邮件通知](paging.md#email-notifications-for-alerts)的项目中停止警报重复的电子邮件通知，请将警报的状态从 **已触发** 更改为其他状态。

#### 通过关闭关联的事件来解决警报

先决条件：

- 您必须至少具有报告者角色。

当您[关闭关联到警报的事件](manage_incidents.md#close-an-incident)时，极狐GitLab [将警报的状态更改为](#change-an-alerts-status)**已解决**。
然后，您会被记入警报的状态更改。

#### 作为 on-call 响应者 **(PREMIUM)**

On-call 响应者可以通过更改警报状态，来响应[警报页面](paging.md#escalating-an-alert)。

更改状态具有以下效果：

- **已确认**：根据项目的[升级策略](escalation_policies.md)，限制 on-call 呼叫。
- **已解决**：使警报的所有 on-call 呼叫静默。
- 从**已解决**到**已触发**：重新开始警报升级。

在 15.1 及更早版本，更改[与事件关联的警报](#from-an-alert)的状态也会更改事件状态。在 15.2 及更高版本，事件状态是独立的，不会随着警报状态的变化而变化。

### 指派警报

在共享警报所有权的大型团队中，可能很难跟踪谁在调查和处理警报。指派警报功能通过明确哪个用户拥有警报来简化协作和委派。极狐GitLab 中的每个警报仅支持一个指派人。

指派警报：

1. 显示当前警报列表：

   1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
   1. 在左侧边栏中，选择 **监控 > 警报**。

1. 选择您想指派的警报并显示其详细信息。

   ![Alert Details View Assignees](img/alert_details_assignees_v13_1.png)

1. 如果右侧边栏未展开，请选择 **展开侧边栏** (**{chevron-double-lg-right}**) 将其展开。

1. 在右侧栏中，找到 **指派人**，然后选择 **编辑**。从列表中，选择要指派给警报的用户。极狐GitLab 将为指派的用户创建一个[待办事项](../../user/todos.md)。

在完成调查或修复警报后，用户可以从警报中取消指派自己。要删除指派人，请选择 **指派人** 下拉列表旁边的 **编辑**，并从指派人列表中清除用户，或选择 **取消指派**。

### 从警报创建待办事项

> 引入于 13.1 版本。

您可以从警报中为自己手动创建一个[待办事项](../../user/todos.md)，稍后在您的**待办事项列表**中查看它。

要添加待办事项，请在右侧栏中选择**添加待办事项**。

