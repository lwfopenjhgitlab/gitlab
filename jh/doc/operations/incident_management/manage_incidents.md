---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 管理事件 **(FREE)**

此页面收集了与您使用[事件](incidents.md)相关的所有操作的说明。

## 创建事件

您可以手动或自动创建事件。

### 从事件列表创建

> - 移动到免费版于 13.3 版本。
> - 权限角色变更为从访客到报告者，于 14.5 版本。
> - 自动应用 `incident` 标签删除于 14.8 版本。

先决条件：

- 您必须至少拥有项目的报告者角色。

从事件列表创建事件：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **监控 > 事件**。
1. 选择 **创建事件**。

### 从议题列表创建

先决条件：

- 您必须至少拥有项目的报告者角色。

从议题列表创建事件：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **议题 > 列表**，然后选择 **新建议题**。
1. 从 **类型** 下拉列表中，选择 **事件**。页面上仅提供与事件相关的字段。
1. 选择 **创建议题**。

<a id="from-an-alert"></a>

### 从警报创建

查看[警报](alerts.md)时创建事件议题。事件描述是从警报中自动获取填入的。

先决条件：

- 您必须至少拥有项目的开发者角色。

从警报创建事件：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **监控 > 警报**。
1. 选择您想要的警报。
1. 选择 **创建事件**。

创建事件后，要从警报中查看它，请选择 **查看事件**。

当您[关闭关联了警报的事件](#close-an-incident)时，极狐GitLab [更改警报的状态](alerts.md#change-an-alerts-status)为 **已解决**。然后，您会被记入警报的状态更改。

<a id="using-the-pagerduty-webhook"></a>

### 当警报被触发时自动创建 **(ULTIMATE)**

在项目设置中，您可以打开在触发警报时[自动创建事件](../metrics/alerts.md#trigger-actions-from-alerts)。

<a id="automatically-when-an-alert-is-triggered"></a>

### 使用 PagerDuty webhook

> - 引入于 13.3 版本。
> - [PagerDuty V3 Webhook](https://support.pagerduty.com/docs/webhooks) 的支持引入于 15.7 版本。

您可以使用 PagerDuty 设置一个 Webhook，以自动为每个 PagerDuty 事件创建一个极狐GitLab 事件。此配置要求您在 PagerDuty 和极狐GitLab 中进行更改。

先决条件：

- 您必须至少拥有项目的维护者角色。

要使用 PagerDuty 设置 Webhook：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 监控**
1. 展开 **事件**。
1. 选择 **PagerDuty 集成** 选项卡。
1. 打开 **启用** 开关。
1. 选择 **保存集成**。
1. 复制 **Webhook URL** 的值以供后续步骤使用。
1. 要将 Webhook URL 添加到 PagerDuty webhook 集成，请按照 [PagerDuty 文档](https://support.pagerduty.com/docs/webhooks#manage-v3-webhook-subscriptions)中描述的步骤操作。

要确认集成成功，请从 PagerDuty 触发测试事件，检查是否从该事件创建了极狐GitLab 事件。

## 查看事件列表

查看[事件列表](incidents.md#incidents-list)：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **监控 > 事件**。

要查看事件的[详细信息页面](incidents.md#incident-details)，请从列表中选择它。

### 谁可以查看事件

您是否可以查看事件取决于[项目可见性级别](../../user/public_access.md)和事件的私密状态：

- 公开项目和非私密事件：您不必成为该项目的成员。
- 私有项目和非私密事件：您必须至少具有该项目的来宾角色。
- 私密事件（无论项目可见性如何）：您必须至少具有该项目的报告者角色。

## 指派给用户

将事件分配给积极响应的用户。

先决条件：

- 您必须至少具有该项目的报告者角色。

分配用户：

1. 在事件中，在右侧边栏的 **指派人** 旁边，选择 **编辑**。
1. 从下拉列表中，选择一个或[多个用户](../../user/project/issues/multiple_assignees_for_issues.md)添加为 **指派人**。
1. 选择下拉列表之外的任何区域。

<a id="change-severity"></a>

## 更改严重性

> 在事件详情页面编辑严重性功能引入于 13.4 版本。

请参阅[事件列表](incidents.md#incidents-list)，了解可用的严重级别的完整描述。

先决条件：

- 您必须至少具有该项目的报告者角色。

要更改事件的严重性：

1. 在事件中，在右侧边栏的 **严重性** 旁边，选择 **编辑**。
1. 从下拉列表中选择新的严重性。

您还可以使用 `/severity` [快速操作](../../user/project/quick_actions.md)，更改严重性。

<a id="change-incident-status"></a>

## 更改状态

> - 引入于 14.9 版本，[功能标志](../../administration/feature_flags.md)为 `incident_escalations`。默认禁用。
> - 在 SaaS 和私有化部署版上启用于 14.10 版本。
> - 功能标志 `incident_escalations` 删除于 15.1 版本。

先决条件：

- 您必须至少拥有项目的开发者角色。

要更改事件的状态：

1. 在事件中，在右侧边栏的 **状态** 旁边，选择 **编辑**。
1. 从下拉列表中选择新的严重性。

**已触发**是新事件的默认状态。

### 作为 on-call 响应者 **(PREMIUM)**

On-call 响应者可以通过更改状态来响应[事件呼叫](paging.md#escalating-an-incident)。

更改状态具有以下效果：

- **已确认**：根据项目的[升级策略](escalation_policies.md)，限制 on-call 呼叫。
- **已解决**：使事件的所有 on-call 呼叫静默。
- 从**已解决**到**已触发**：重新开始事件升级。

在 15.1 及更早版本，更改[从警报创建的事件](#from-an-alert)的状态也会更改警报状态。在 15.2 及更高版本，警报状态是独立的，不会随着事件状态的变化而变化。

<a id="change-escalation-policy"></a>

### 更改升级策略 **(PREMIUM)**

先决条件：

- 您必须至少具有该项目的开发者角色。

要更改事件的升级策略：

1. 在事件中，在右侧栏的 **升级策略** 旁边，选择 **编辑**。
1. 从下拉列表中选择升级策略。

默认情况下，新事件没有选择升级策略。

选择升级策略[将事件状态](#change-status)更改为**已触发**，并开始[将事件升级给 on-call 响应者](paging.md#escalating-an-incident)。

在 15.1 及更早版本，更改[从警报创建的事件](#from-an-alert)的状态也会更改警报状态。在 15.2 及更高版本，警报状态是独立的，不会随着事件状态的变化而变化。

## 嵌入指标

您可以在任何使用[极狐GitLab Markdown](../../user/markdown.md) 的地方嵌入指标，例如描述、议题评论和合并请求。嵌入指标可帮助您在讨论事件或性能问题时共享它们。
您可以通过复制并粘贴到指标仪表盘的链接<!--[复制并粘贴到指标仪表盘的链接](../metrics/embed.md#embedding-gitlab-managed-kubernetes-metrics)-->，将仪表盘直接输出到极狐GitLab 中的任何议题、合并请求、史诗或任何其它 Markdown 文本字段。

您可以在事件和议题模板中嵌入极狐GitLab 的指标和 Grafana 指标。

## 关闭事件

先决条件：

- 您必须至少具有该项目的报告者角色。

要关闭事件，请在右上角选择 **关闭事件**。

当您关闭关联到[警报](alerts.md)的事件时，关联的警报的状态更改为 **已解决**。然后，您会被记入警报的状态更改。

<a id="automatically-close-incidents-via-recovery-alerts"></a>

### 通过恢复警报自动关闭事件

> 对 HTTP 集成的支持引入于 13.4 版本。

您可以启用当极狐GitLab 从 HTTP 或 Prometheus webhook 收到恢复警报时，自动关闭事件的功能。

先决条件：

- 您必须至少具有该项目的维护者角色。

配置方法：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 监控**。
1. 展开 **事件** 部分。
1. 选择 **自动关闭关联事件** 复选框。
1. 选择 **保存更改**。

当极狐GitLab 收到恢复警报时，会关闭相关事件。此操作被记录为事件的系统备注，表明它已被极狐GitLab 警报机器人自动关闭。

<!--
## Other actions

Because incidents in GitLab are built on top of [issues](../../user/project/issues/index.md),
they have the following actions in common:

- [Add a to-do item](../../user/todos.md#create-a-to-do-item)
- [Add labels](../../user/project/labels.md#assign-and-unassign-labels)
- [Assign a milestone](../../user/project/milestones/index.md#assign-a-milestone-to-an-issue-or-merge-request)
- [Make an incident confidential](../../user/project/issues/confidential_issues.md)
- [Set a due date](../../user/project/issues/due_dates.md)
- [Toggle notifications](../../user/profile/notifications.md#edit-notification-settings-for-issues-merge-requests-and-epics)
- [Track time spent](../../user/project/time_tracking.md)
-->
