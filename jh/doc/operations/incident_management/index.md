---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 事件管理 **(FREE)**

> 引入于 13.0 版本。

事件管理使开发人员能够轻松分类和查看其应用程序生成的警报和事件。通过显示正在开发代码的警报和事件，可以提高效率和意识。查看以下部分了解更多信息：

- [集成您的监控工具](integrations.md)。
- 管理[待命计划](oncall_schedules.md)并接收触发警报的[通知](paging.md)。
- 对[警报](alerts.md)和[事件](incidents.md)进行分类。
- 使用状态页面<!--[状态页面](status_page.md)-->通知利益相关者。
