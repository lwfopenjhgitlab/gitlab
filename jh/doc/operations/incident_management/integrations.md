---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 集成 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/13203) in GitLab 12.4.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/42640) from GitLab Ultimate to GitLab Free in 12.8.
-->

极狐GitLab 可以通过 webhook 接收器接受来自任何来源的警报。[警报通知](alerts.md)可以对 on-call [触发呼叫](paging.md#paging)，或用于[创建事件](manage_incidents.md#from-an-alert)。

<a id="integrations-list"></a>

## 集成列表

> 引入于 13.5 版本。

您需要至少拥有维护者角色，可以通过导航到项目侧边栏菜单中的 **设置 > 监控**，并展开 **警报** 部分来查看配置的警报集成列表。该列表显示集成名称、类型和状态（启用或禁用）：

![Current Integrations](img/integrations_list_v13_5.png)

<a id="configuration"></a>

## 配置

极狐GitLab 可以通过您配置的 HTTP 端点接收警报。

### 单个 HTTP 端点

在极狐GitLab 项目中，启用 HTTP 端点会激活接收 JSON 格式的警报负载。您始终可以根据需要[自定义有效负载](#customize-the-alert-payload-outside-of-gitlab)。

1. 以具有项目维护者角色的用户身份登录极狐GitLab。
1. 在您的项目中导航到 **设置 > 监控**。
1. 展开 **警报** 部分，然后在 **选择集成类型** 下拉菜单中，选择 **HTTP 端点**。
1. 切换 **启用** 警报设置。保存集成后，**查看凭据**选项卡中提供了 Webhook 配置的 URL 和授权密钥。您还必须在外部服务中输入 URL 和授权密钥。

<a id="http-endpoints"></a>

### HTTP 端点 **(PREMIUM)**

> 引入于 13.6 版本。

在[专业版](https://gitlab.cn/pricing/)中，您可以创建多个唯一的 HTTP 端点，接收来自任何外部源的 JSON 格式的警报，并且您可以[自定义有效负载](#customize-the-alert-payload-outside-of-gitlab)。

1. 以具有项目维护者角色的用户身份登录极狐GitLab。
1. 在您的项目中导航到 **设置 > 监控**。
1. 展开 **警报** 部分。
1. 对于您要创建的每个端点：

   1. 选择**添加新集成**。
   1. 在 **选择集成类型** 下拉菜单中，选择 **HTTP 端点**。
   1. 命名集成。
   1. 切换 **启用** 警报设置。保存集成后，用于 webhook 配置的 **URL** 和 **授权密钥** 在 **查看凭据** 选项卡中可用。您还必须在外部服务中输入 URL 和授权密钥。
   1. 可选。要将监控工具警报中的字段映射到极狐GitLab 字段，请输入示例有效负载并选择 **解析有效负载并进行自定义映射**。需要有效的 JSON。如果您更新示例负载，您还必须重新映射字段。

   1. 可选。如果您提供了有效的示例负载，请选择 **负载警报键** 中的每个值，[映射到**极狐GitLab 警报键**](#map-fields-in-custom-alerts)。
   1. 要保存集成，请选择 **保存集成**。如果需要，您可以在创建集成后，从集成的 **发送测试警报** 选项卡发送测试警报。

新的 HTTP 端点显示在[集成列表](#integrations-list)中。
您可以通过选择集成列表右侧的 **{settings}** 设置图标来编辑集成。

<a id="map-fields-in-custom-alerts"></a>

#### 在自定义警报中映射字段

> 引入于 13.10 版本。

您可以将监控工具的警报格式与极狐GitLab 警报集成。要在[警报列表](alerts.md)和[警报详情页](alerts.md#alert-details-page)中显示正确的信息，请在[创建 HTTP 端点](#http-endpoints)时，将警报的字段映射到极狐GitLab 字段：

![Alert Management List](img/custom_alert_mapping_v13_11.png)

<a id="customize-the-alert-payload-outside-of-gitlab"></a>

## 在极狐GitLab 之外自定义警报有效负载

对于没有[自定义映射](#map-fields-in-custom-alerts)的 HTTP 端点，您可以通过发送以下参数来自定义有效负载。所有字段都是可选的。如果传入警报不包含 `Title` 字段的值，则将应用默认值 `New: Alert`。

| 属性                  | 类型            | 描述 |
| ------------------------- | --------------- | ----------- |
| `title`                   | String          | 警报的标题。 |
| `description`             | String          | 问题的高级别摘要。 |
| `start_time`              | DateTime        | 警报的时间。如果没有提供，则使用当前时间。 |
| `end_time`                | DateTime        | 警报的解决时间。如果提供，则解决警报。 |
| `service`                 | String          | 受影响的服务。 |
| `monitoring_tool`         | String          | 关联监控工具的名称。 |
| `hosts`                   | String or Array | 一个或多个主机，即此事件在何处发生。 |
| `severity`                | String          | 警报的严重性，不区分大小写。可以是以下之一：`critical`、`high`、`medium`、`low`、`info`、`unknown`。如果缺少或值不在此列表中，则默认为 `critical`。 |
| `fingerprint`             | String or Array | 警报的唯一标识符，可用于对同一警报的出现进行分组。 |
| `gitlab_environment_name` | String          | 关联极狐GitLab [环境](../../ci/environments/index.md)的名称。<!--[在仪表板上显示警报](../../user/operations_dashboard/index.md#adding-a-project-to-the-dashboard)-->在仪表板上显示警报是必需的。 |

您还可以将自定义字段添加到警报的有效负载中。额外参数的值不仅限于原始类型（如字符串或数字），还可以是嵌套的 JSON 对象。例如：

```json
{ "foo": { "bar": { "baz": 42 } } }
```

NOTE:
确保您的请求小于[有效负载应用程序限制](../../administration/instance_limits.md#generic-alert-json-payloads)。

### 示例请求体

示例负载：

```json
{
  "title": "Incident title",
  "description": "Short description of the incident",
  "start_time": "2019-09-12T06:00:55Z",
  "service": "service affected",
  "monitoring_tool": "value",
  "hosts": "value",
  "severity": "high",
  "fingerprint": "d19381d4e8ebca87b55cda6e8eee7385",
  "foo": {
    "bar": {
      "baz": 42
    }
  }
}
```

<a id="prometheus-endpoint"></a>

### Prometheus 端点

先决条件：

- 您必须至少具有该项目的维护者角色。

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 监控**。
1. 展开 **警报** 部分，然后选择 **添加新的集成**。
1. 从 **选择集成类型** 下拉列表中，选择 **Prometheus**。
1. 打开 **启用** 开关。
1. 输入 **Prometheus API 基本 URL**。
   您应该输入一个占位 URL。使用此字段的功能废弃并计划删除于 16.0 版本。
1. 选择 **保存集成**。

Webhook 配置的 URL 和授权密钥在 **查看凭据** 页签中可用。

在您的外部服务中输入 URL 和授权密钥。
您还可以从集成的 [**发送测试警报**](#triggering-test-alerts) 页签发送测试警报。

#### 将集成凭据添加到 Prometheus Alertmanager

要向极狐GitLab 发送 Prometheus 警报通知，请将 URL 和授权密钥从您的 [Prometheus 集成](#prometheus-endpoint) 复制到 [`webhook_configs`](https://prometheus.io/docs/alerting/latest/configuration/#webhook_config) Prometheus Alertmanager 配置的部分：

```yaml
receivers:
  - name: gitlab
    webhook_configs:
      - http_config:
          authorization:
            type: Bearer
            credentials: 1234567890abdcdefg
        send_resolved: true
        url: http://IP_ADDRESS:PORT/root/manual_prometheus/prometheus/alerts/notify.json
        # Rest of configuration omitted
        # ...
```

#### 预期的请求属性

警报应针对 Prometheus [webhook 接收器](https://prometheus.io/docs/alerting/latest/configuration/#webhook_config)进行格式化。

必需的顶级属性：

- `alerts`
- `commonAnnotations`
- `commonLabels`
- `externalURL`
- `groupKey`
- `groupLabels`
- `receiver`
- `status`
- `version`

从 Prometheus 负载中的 `alerts`，为数组中的每个项目创建一个极狐GitLab 警报。
您可以更改下面列出的嵌套参数来配置极狐GitLab 警报。

| 属性                                                                  | 类型     | 是否必需 | 描述                          |
| -------------------------------------------------------------------------- | -------- | -------- | ------------------------------------ |
| `annotations/title`、`annotations/summary` 或 `labels/alertname` 之一   | String   | Yes      | 警报的标题。             |
| `startsAt`                                                                 | DateTime | Yes      | 警报的开始时间。         |
| `annotations/description`                                                  | String   | No       | 议题的高级摘要。 |
| `annotations/gitlab_incident_markdown`                                     | String   | No       | [极狐GitLab Flavored Markdown](../../user/markdown.md) 将附加到从警报创建的任何事件中。 |
| `annotations/runbook`                                                      | String   | No       | 链接到有关如何管理此警报的文档或说明。 |
| `endsAt`                                                                   | DateTime | No       | 警报的解决时间。    |
| `generatorUrl` 中的 `g0.expr` 查询参数                                | String   | No       | 关联指标的查询。          |
| `labels/gitlab_environment_name`                                           | String   | No       | 关联的极狐GitLab [环境](../../ci/environments/index.md)的名称。需要[在仪表板上显示警报](../../user/operations_dashboard/index.md#adding-a-project-to-the-dashboard)。 |
| `labels/severity`                                                          | String   | No       | 警报的严重性。应该是 [Prometheus 严重性选项](#prometheus-severity-options)之一。如果缺少或值不在此列表中，则默认为 `critical`。 |
| `status`                                                                   | String   | No       | Prometheus 中警报的状态。如果值为 `resolved`，则警报已解决。 |
| `annotations/gitlab_y_label`、`annotations/title`、`annotations/summary` 或 `labels/alertname` 之一 | String | No | 在[极狐GitLab Flavored Markdown](../../user/markdown.md) 中嵌入此警报的指标时使用的 Y 轴标签。 |

[警报详细信息页面](alerts.md#alert-details-page)上提供了 `annotations` 下包含的其他属性。忽略任何其他属性。

属性不限于原始类型（如字符串或数字），还可以是嵌套的 JSON 对象。例如：

```json
{
    "target": {
        "user": {
            "id": 42
        }
    }
}
```

NOTE:
确保您的请求小于负载应用限制。

#### Prometheus 严重性选项

> 引入于 13.9 版本。

来自 Prometheus 的警报，可以为[警报严重性](../incident_management/alerts.md#alert-severity)提供任何不区分大小写的后续值：

- **严重**：`critical`、`s1`、`p1`、`emergency`、`fatal`
- **高**：`high`、`s2`、`p2`、`major`、`page`
- **中**：`medium`、`s3`、`p3`、`error`、`alert`
- **低**：`low`、`s4`、`p4`、`warn`、`warning`
- **消息**：`info`、`s5`、`p5`、`debug`、`information`、`notice`

如果值缺失或不在此列表中，则严重性默认为 `critical`。

#### Prometheus 警报示例

警报规则示例：

```yaml
groups:
- name: example
  rules:
  - alert: ServiceDown
    expr: up == 0
    for: 5m
    labels:
      severity: high
    annotations:
      title: "Example title"
      runbook: "http://example.com/my-alert-runbook"
      description: "Service has been down for more than 5 minutes."
      gitlab_y_label: "y-axis label"
      foo:
        bar:
          baz: 42
```

请求负载示例：

```json
{
  "version" : "4",
  "groupKey": null,
  "status": "firing",
  "receiver": "",
  "groupLabels": {},
  "commonLabels": {},
  "commonAnnotations": {},
  "externalURL": "",
  "alerts": [{
    "startsAt": "2022-010-30T11:22:40Z",
    "generatorURL": "http://host?g0.expr=up",
    "endsAt": null,
    "status": "firing",
    "labels": {
      "gitlab_environment_name": "production",
      "severity": "high"
    },
    "annotations": {
      "title": "Example title",
      "runbook": "http://example.com/my-alert-runbook",
      "description": "Service has been down for more than 5 minutes.",
      "gitlab_y_label": "y-axis label",
      "foo": {
        "bar": {
          "baz": 42
        }
      }
    }
  }]
}
```


## 授权

接受以下授权方式：

- 承载授权 header（Bearer authorization header）
- 基本认证（Basic authentication）

配置警报集成时可以找到 `<authorization_key>` 和 `<url>` 值。

### 承载授权 header

授权密钥可用作 Bearer 令牌：

```shell
curl --request POST \
  --data '{"title": "Incident title"}' \
  --header "Authorization: Bearer <authorization_key>" \
  --header "Content-Type: application/json" \
  <url>
```

### 基本认证

授权密钥可以用作 `password`。`username` 留空时：

- 用户名：`<blank>`
- 密码：`<authorization_key>`

```shell
curl --request POST \
  --data '{"title": "Incident title"}' \
  --header "Authorization: Basic <base_64_encoded_credentials>" \
  --header "Content-Type: application/json" \
  <url>
```

基本认证也可以直接在 URL 中与凭据一起使用：

```shell
curl --request POST \
  --data '{"title": "Incident title"}' \
  --header "Content-Type: application/json" \
  <username:password@url>
```

WARNING:
在 URL 中使用您的授权密钥是不安全的，因为它在服务器日志中可见。如果您的工具支持，我们建议使用上述标题选项之一。

## 返回体

> 引入于 14.5 版本。

JSON 响应正文包含在请求中创建的任何警报的列表：

```json
[
  {
    "iid": 1,
    "title": "Incident title"
  },
  {
    "iid": 2,
    "title": "Second Incident title"
  }
]
```

成功的响应返回一个 `200` 响应码。

## 触发测试警报

> 引入于 13.2 版本。

在项目维护者或所有者配置集成后，您可以触发测试警报以确认您的集成正常工作。

1. 以至少具有开发者角色的用户身份登录。
1. 在您的项目中导航到 **设置 > 监控**。
1. 选择 **警报**，展开该部分。
1. 在[列表](#integrations-list)中选择集成右侧的 **{settings}** 设置图标。
1. 选择 **发送测试警报** 选项卡将其打开。
1. 在有效负载字段中输入测试有效负载（需要有效的 JSON）。
1. 选择 **发送**。

极狐GitLab 根据测试结果显示错误或成功消息。

## 相同警报的自动分组 **(PREMIUM)**

> 引入于 13.2 版本。

在 13.2 及更高版本中，极狐GitLab 根据其有效负载对警报进行分组。当传入警报包含与另一个警报相同的有效负载（不包括 `start_time` 和 `hosts` 属性）时，系统将这些警报组合在一起，并在[警报管理列表](incidents.md)和详细信息页面上显示一个计数器。

如果现有警报已经处于 `resolved`，系统会创建一个新警报。

![Alert Management List](img/alert_list_v13_1.png)

## 恢复警报

> 引入于 13.4 版本。

当 HTTP 端点接收到带有警报集结束时间的有效负载时，系统中的警报将自动解决。对于没有[自定义映射](#map-fields-in-custom-alerts)的 HTTP 端点，预期的字段是 `end_time`。使用自定义映射，您可以选择预期的字段。

当警报解决时，您还可以配置关联的[事件自动关闭](../incident_management/manage_incidents.md#automatically-close-incidents-via-recovery-alerts)。

## 关联到您的 Opsgenie 警报 **(PREMIUM)**

> 引入于 13.2 版本。

WARNING:
我们正在通过 [HTTP 端点集成](#single-http-endpoint)，与 Opsgenie 和其它警报工具进行更深入的集成，以便您可以在极狐GitLab 界面中查看警报。之前从极狐GitLab 警报列表直接链接到 Opsgenie 警报的功能在 13.8 及更高版本中已废弃。

您可以使用极狐GitLab 与 [Opsgenie](https://www.atlassian.com/software/opsgenie) 的集成来监控警报。

如果启用 Opsgenie 集成，则不能同时激活其它极狐GitLab 警报服务。

要启用 Opsgenie 集成：

1. 以具有维护者或所有者角色的用户身份登录。
1. 导航到 **监控 > 警报**。
1. 在 **集成** 选择框中，选择 **Opsgenie**。
1. 选择 **启用** 切换。
1. 在 **API URL** 字段中，输入 Opsgenie 集成的基本 URL，例如 `https://app.opsgenie.com/alert/list`。
1. 选择 **保存修改**。

启用集成后，导航到 **监控 > 警报** 处的警报列表页面，然后选择 **查看 Opsgenie 中的警报**。
