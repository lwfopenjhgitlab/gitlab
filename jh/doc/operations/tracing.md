---
stage: Monitor
group: Monitor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Jaeger 跟踪 **(FREE SELF)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/7903) in GitLab 11.5.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/42645) from GitLab Ultimate to GitLab Free in 13.5.
-->

> - 废弃于 14.7 版本

WARNING:
此功能正处于其生命周期结束的过程中。它已在 14.7 版本中弃用，并计划在 15.0 版本中删除。

跟踪提供对已部署应用程序的性能和健康状况的洞察，跟踪处理给定请求的每个功能或微服务。跟踪可以很容易地理解请求的端到端流程，无论您使用的是单体系统还是分布式系统。

## 安装 Jaeger

[Jaeger](https://www.jaegertracing.io/) 是一个开源的端到端分布式跟踪系统，用于监控和故障排除基于微服务的分布式系统。要了解有关安装 Jaeger 的更多信息，请阅读官方[入门文档](https://www.jaegertracing.io/docs/latest/getting-started/)。

同时查看：

- [多合一 Docker 镜像](https://www.jaegertracing.io/docs/latest/getting-started/#all-in-one)。
- 部署选项：
  - [Kubernetes](https://github.com/jaegertracing/jaeger-kubernetes)。
  - [OpenShift](https://github.com/jaegertracing/jaeger-openshift)。

## 链接到 Jaeger

极狐GitLab 提供了一种从项目中打开 Jaeger UI 的简单方法：

1. [设置 Jaeger](https://www.jaegertracing.io) 并使用[客户端库](https://www.jaegertracing.io/docs/latest/client-libraries/)之一配置您的应用程序。
1. 导航到项目的 **设置 > 监控** 并提供 Jaeger URL。
1. 单击 **保存修改**，使更改生效。
1. 您现在可以访问项目侧栏中的 **监控 > 跟踪**，系统会将您重定向到配置的 Jaeger URL。
