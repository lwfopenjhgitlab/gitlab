---
stage: Monitor
group: Monitor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 错误跟踪 **(FREE)**

错误跟踪允许开发人员轻松发现和查看他们的应用程序可能生成的错误。
通过在代码正在开发的地方显示错误信息，可以提高效率和意识。

## 错误跟踪工作原理

要使错误跟踪起作用，您需要以下两个部分：

- **您使用 Sentry SDK 的应用程序：**当错误发生时，Sentry SDK 会捕获有关它的信息并通过网络将其发送到后端。后端存储有关所有错误的信息。

- **错误跟踪后端：**后端可以是极狐GitLab 本身或 Sentry。当它是极狐GitLab 时，我们将其命名为*集成错误跟踪*，因为您不需要设置单独的后端。它已经是产品的一部分。

  - 要使用极狐GitLab 后端，请参阅[集成错误跟踪](#集成错误跟踪)。
  - 要使用 Sentry 作为后端，请参阅 [Sentry 错误跟踪](#sentry-错误跟踪)。

  无论您选择哪种后端，[错误跟踪 UI](#错误跟踪列表) 都是一样的。

## Sentry 错误跟踪

[Sentry](https://sentry.io/) 是一个开源的错误跟踪系统。极狐GitLab 允许管理员将 Sentry 连接到 GitLab，允许用户查看极狐GitLab 中的 Sentry 错误列表。

### 部署 Sentry

你可以注册云托管的 [Sentry](https://sentry.io)，部署自己的 [on-premise 实例](https://github.com/getsentry/onpremise/)。<!--, or use GitLab to [install Sentry to a Kubernetes cluster](../user/clusters/applications.md#install-sentry-using-gitlab-cicd).-->

### 启用 Sentry

极狐GitLab 提供了一种将 Sentry 连接到您的项目的简单方法。您至少需要维护者权限才能启用 Sentry 集成。

1. 注册 Sentry.io 或[部署您自己的](#部署-sentry) Sentry 实例。
1. [创建](https://docs.sentry.io/product/sentry-basics/guides/integrate-frontend/create-new-project/)一个新的 Sentry 项目。对于您要集成的每个极狐GitLab 项目，我们建议您创建一个新的 Sentry 项目。
1. 查找或生成 [Sentry 身份验证令牌](https://docs.sentry.io/api/auth/#auth-tokens)。对于 Sentry 的 SaaS 版本，您可以在 [https://sentry.io/api/](https://sentry.io/api/) 找到或生成身份验证令牌。确保至少为令牌提供以下范围权限：`project:read`、`event:read` 和 `event:write`（用于解析事件）。
1. 在极狐GitLab，启用错误跟踪：
   1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
   1. 在左侧边栏中，选择 **监控 > 错误跟踪**。
   1. 选择 **启用错误跟踪**。
1. 在极狐GitLab，确保错误跟踪已启用：
   1. 在左侧边栏中，选择 **设置 > 监控**。
   1. 展开 **错误跟踪**。
   1. 确保选中 **启用** 复选框。
1. 在 **Sentry API URL** 框中，输入您的 Sentry 主机名。例如，输入`https://sentry.example.com`。对于 Sentry 的 SaaS 版本，主机名是 `https://sentry.io`。
1. 在 **验证令牌** 框中，输入您之前生成的令牌。
1. 要测试与 Sentry 的连接并填充 **项目** 下拉列表，请选择 **连接**。
1. 从 **项目** 列表中，选择一个 Sentry 项目链接到您的极狐GitLab 项目。
1. 选择 **保存修改**。

您现在可以访问项目侧栏中的 **监控 > 错误跟踪**，[查看 Sentry 错误列表](#错误跟踪列表)。

### 启用极狐GitLab 议题链接

您可能还希望按照 [Sentry 文档](https://docs.sentry.io/product/integrations/gitlab/)中的步骤启用 Sentry 的极狐GitLab 集成。

### 启用 GitLab Runner

要使用 Sentry 配置 GitLab Runner，您必须将 `sentry_dsn` 的值添加到 GitLab Runner 的 `config.toml` 配置文件中<!--，如 [GitLab Runner 高级配置](https://docs.gitlab.com/runner/configuration/advanced-configuration.html)-->。
在设置 Sentry 时，如果系统要求您选择项目类型，请选择 **Go**。

如果您在 GitLab Runner 日志中看到以下错误，则应在 **Sentry.io > Project Settings > Client Keys (DSN) > Show deprecated DSN** 中指定已弃用的 DSN。

```plaintext
ERROR: Sentry failure builds=0 error=raven: dsn missing private key
```

## 错误跟踪列表

至少拥有报告者权限的用户可以在项目侧边栏中的 **监控 > 错误跟踪** 中找到错误跟踪列表。
在这里，您可以按标题或状态（忽略、已解决或未解决之一）过滤错误，并按频率、首次出现或上次出现的降序排序。默认情况下，错误列表按上次出现排序并过滤展示未解决的错误。

![Error Tracking list](img/error_tracking_list_v12_6.png)

## 错误详情

从错误列表中，用户可以通过单击任何错误的标题，导航到错误详细信息页面。

这个页面有：

- Sentry Issue 的链接。
- 如果 Sentry Issue 的第一个版本上的 Sentry [发布 ID/版本](https://docs.sentry.io/product/releases/?platform=javascript#configure-sdk) 与您的 GitLab 托管项目中提交（commit）的 SHA 匹配，则指向提交的链接。
- 有关该问题的其他详细信息，包括完整的堆栈跟踪。
- <!--In [GitLab 12.7 and newer](https://gitlab.com/gitlab-org/gitlab/-/issues/36246), -->显示语言和紧急程度。

默认情况下，会显示一个 **创建议题** 按钮：

![Error Details without Issue Link](img/error_details_v12_7.png)

如果您已从错误中创建极狐GitLab 议题，则 **创建议题** 按钮将变为 **查看议题** 按钮，并且指向极狐GitLab 议题的链接将显示在错误详细信息部分中。

## 对错误采取行动

您可以从 GitLab UI 中对 Sentry 错误采取措施。

### 忽略错误

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/39665) in GitLab 12.7.
-->

在[错误详情](#错误详情)页面中，您可以通过单击页面顶部附近的 **忽略** 按钮忽略 Sentry 错误。

忽略错误会阻止它出现在[错误跟踪列表](#错误跟踪列表)中，并使在 Sentry 中设置的通知静音。

### 解决错误

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/39825) in GitLab 12.7.
-->

在[错误详情](#错误详情)页面中，您可以通过单击页面顶部附近的 **解决** 按钮来解决 Sentry 错误。

将错误标记为已解决表示该错误已停止触发事件。如果极狐GitLab 议题与错误相关联，则议题将关闭。

如果发生另一个事件，则错误将恢复为未解决。

<a id="integrated-error-tracking"></a>

## 集成错误跟踪

> - 引入于 14.4 版本
> - 禁用于 14.9 版本，功能标志为 `integrated_error_tracking`。默认禁用。

FLAG:
此功能默认不可用，要在私有化部署版上启用，需询问管理员启用名为 `integrated_error_tracking` 的[功能标志](../administration/feature_flags.md)。该功能尚未准备好用于生产用途。
在 SaaS 版上不可用。

WARNING:
打开集成错误跟踪可能会影响性能，具体取决于您的错误率。

集成错误跟踪是 Sentry 后端的轻量级替代方案。
您仍然在应用程序中使用 Sentry SDK。但是您不需要部署 Sentry 或设置云托管的 Sentry。相反，您使用极狐GitLab 作为它的后端。

Sentry 后端会自动为您创建的每个项目分配一个数据源名称 (DSN)。
极狐GitLab 也是如此。您应该能够在极狐GitLab 错误跟踪设置中为您的项目找到 DSN。通过使用极狐GitLab 提供的 DSN，您的应用程序连接到极狐GitLab，报告错误。
这些错误存储在极狐GitLab 数据库中并由 GitLab UI 呈现，与 Sentry 集成的方式相同。

### 项目设置

您可以在 **设置 > 监控 > 错误跟踪** 找到功能配置。

#### 如何启用

1. 选择 **GitLab** 作为您项目的错误跟踪后端：

    ![Error Tracking Settings](img/error_tracking_setting_v14_3.png)

1. 选择 **保存修改**。页面重新加载后，您应该会看到一个带有 DSN 字符串的文本字段，复制它。

    ![Error Tracking Settings DSN](img/error_tracking_setting_dsn_v14_4.png)

1. 从上一步中获取 DSN 并使用它配置您的 Sentry SDK。错误现在报告给极狐GitLab 收集器，并且在 [GitLab UI](#错误跟踪列表) 中可见。

#### 管理 DSN

当您启用该功能时，您会收到一个 DSN。它包括用于身份验证的哈希。此哈希是客户端密钥。极狐GitLab 使用客户端密钥来验证从应用程序到极狐GitLab 后端的错误跟踪请求。

在某些情况下，您可能想要创建一个新的客户端密钥并删除一个现有的。
<!--You can do so by managing client keys with the [error tracking API](../api/error_tracking.md).-->

#### 限制

集成错误跟踪功能是使用 Sentry SDK for Ruby on Rails 构建和测试的。
不保证支持其他语言和框架。<!--For up-to-date information, see the
[compatibility issue](https://gitlab.com/gitlab-org/gitlab/-/issues/340178).-->
