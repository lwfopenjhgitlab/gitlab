---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 功能标志 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/7433) in GitLab 11.4.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/212318) from GitLab Premium to GitLab Free in 13.5.
-->

使用功能标志，您可以将应用程序的新功能小批量部署到生产环境中。
您可以为部分用户打开和关闭功能，帮助您实现持续交付。
功能标志有助于降低风险，允许您进行受控测试，并将功能交付与客户发布分开。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an example of feature flags in action, see [GitLab for Deploys, Feature Flags, and Error Tracking](https://www.youtube.com/embed/5tw2p6lwXxo).
-->

<!--
NOTE:
The Feature Flags GitLab offer as a feature (described in this document) is not the same method
used for the [development of GitLab](../development/feature_flags/index.md).
-->

## 工作原理

极狐GitLab 使用 [Unleash](https://github.com/Unleash/unleash)，一种功能切换服务。

通过在极狐GitLab 中启用或禁用标志，您的应用程序可以确定启用或禁用哪些功能。

您可以在极狐GitLab 中创建功能标志并使用应用程序中的 API 来获取功能标志列表及其状态。应用程序必须配置为与极狐GitLab 通信，因此开发人员可以使用兼容的客户端库并[在您的应用程序中集成功能标志](#将功能标志与您的应用程序集成)。

## 创建功能标志

要创建和启用功能标志：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 功能标志**。
1. 选择 **新建功能标志**。
1. 输入名称。以字母开头且仅包含小写字母、数字、下划线（`_`）或破折号（`-`），且不以破折号（`-`）或下划线（`_`）结尾。
1. 可选。输入说明（最多 255 个字符）。
1. 添加功能标志[**策略**](#功能标志策略)，定义应如何应用标志。对于每个策略，包括 **类型**（默认为 [**所有用户**](#所有用户)）和 **环境**（默认为所有环境）。
1. 选择 **创建功能标志**。

要更改这些设置，请在列表中的任何功能标志旁边，选择 **编辑** (**{pencil}**)。

## 最大功能标志数

> 引入于 13.5 版本。

自助管理实例上每个项目的最大功能标志数为 200。<!--对于 GitLab SaaS，最大数量由 [tier](https://about.gitlab.com/pricing/) 确定：-->

<!--
| Tier     | Feature flags per project (SaaS) | Feature flags per project (self-managed) |
|----------|----------------------------------|------------------------------------------|
| Free     | 50                               | 200                                      |
| Premium  | 150                              | 200                                      |
| Ultimate | 200                              | 200                                      |
-->

<a id="feature-flag-strategies"></a>

## 功能标志策略

> - 引入于 13.0 版本。
> - 部署在功能标志后默认禁用。
> - 改为默认启用于 13.2 版本。
> - 推荐生产使用。

<!--
> - It's enabled on GitLab.com.
-->

您可以跨多个环境应用功能标志策略，而无需多次定义策略。

极狐GitLab 功能标志使用 [Unleash](https://docs.getunleash.io/) 作为功能标志引擎。在 Unleash 中，[策略](https://docs.getunleash.io/reference/activation-strategies)用于细粒度的功能标志控制。极狐GitLab 功能标志可以有多种策略，支持的策略有：

- [所有用户](#所有用户)
- [用户百分比](#用户百分比)
- [用户 ID](#用户-id)
- [用户列表](#用户列表)

可以在[创建功能标志](#创建功能标志)时将策略添加到功能标志，或者在创建后通过导航到 **部署 > 功能标志** 并选择 **编辑** （**{pencil}**）来编辑现有功能标志。

### 所有用户

为所有用户启用该功能，它使用标准（`默认`）Unleash 激活[策略](https://docs.getunleash.io/reference/activation-strategies#standard)。

### 百分比上线

> 引入于 13.5 版本。

为一定比例的页面视图启用该功能，并具有可配置的行为一致性。这种一致性也称为粘性。它使用逐步上线 (`flexibleRollout`) Unleash 激活[策略](https://docs.getunleash.io/reference/activation-strategies#gradual-rollout)。

您可以将一致性配置为基于：

- **用户 ID**：每个用户 ID 都有一致的行为，忽略会话 ID。
- **会话 ID**：每个会话 ID 都有一致的行为，忽略用户 ID。
- **随机**：不保证一致的行为。该功能会随机启用选定百分比的页面浏览量。用户 ID 和会话 ID 将被忽略。
- **可用 ID**：根据用户的状态尝试一致的行为：
  - 如果用户已登录，则根据用户 ID 使行为保持一致。
  - 如果用户是匿名的，则根据会话 ID 使行为保持一致。
  - 如果没有用户 ID 或会话 ID，则随机启用选定百分比的页面视图的功能。

例如，将基于 **可用 ID** 的值设置为 15%，以便为 15% 的页面浏览量启用该功能。对于经过身份验证的用户，基于他们的用户 ID。对于具有会话 ID 的匿名用户，将基于他们的会话 ID，因为他们没有用户 ID。如果未提供会话 ID，则返回随机。

上线百分比可以从 0% 到 100%。

根据用户 ID 选择一致性的功能与 [用户百分比](#用户百分比) 推出功能相同。

WARNING:
选择 **随机** 将为个别用户提供不一致的应用程序行为。

### 用户百分比

为一定比例的经过身份验证的用户启用该功能。它使用 Unleash 激活策略 [`gradualRolloutUserId`](https://docs.getunleash.io/reference/activation-strategies#gradual-rollout)。

例如，将值设置为 15% 可为 15% 的经过身份验证的用户启用该功能。

上线百分比可以从 0% 到 100%。

粘性（同一用户的一致应用行为）对登录用户有保证，但对匿名用户则不保证。

请注意，具有基于 **用户 ID** 的一致性的[上线百分比](#百分比上线)具有相同的行为。我们建议使用百分比部署，因为它比用户百分比更灵活。

WARNING:
如果选择了用户百分比策略，则**必须**为 Unleash 客户端提供一个用户 ID 才能启用该功能。请参阅下面的 [Ruby 示例](#ruby-应用示例)。

### 用户 ID

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/8240) in GitLab 12.2.
> - [Updated](https://gitlab.com/gitlab-org/gitlab/-/issues/34363) to be defined per environment in GitLab 12.6.
-->

为目标用户列表启用该功能。它是使用 Unleash UserIDs (`userWithId`) 激活[策略](https://docs.getunleash.io/reference/activation-strategies#userids)。

以逗号分隔的值列表形式输入用户 ID（例如，`user@example.com、user2@example.com` 或 `username1,username2,username3` 等）。请注意，用户 ID 是应用程序用户的标识符。他们不需要是极狐GitLab 用户。

WARNING:
Unleash 客户端**必须**被赋予一个用户 ID，以便为目标用户启用该功能。请参阅下面的 [Ruby 示例](#ruby-应用示例)。

<a id="user-list"></a>

### 用户列表

> 引入于 13.1 版本。

为[在功能标志 UI](#创建用户列表) 中或使用功能标志用户列表 API <!--[功能标志用户列表 API](../api/feature_flag_user_lists.md)-->创建的用户列表启用功能。
与[用户 ID](#用户-id) 类似，它使用 Unleash UsersID (`userWithId`) 激活[策略](https://docs.getunleash.io/user_guide/activation_strategy#userids)。

不可能*禁用*用户列表成员的功能，但您可以通过为不包含排除用户的用户列表，启用功能来实现相同的效果。

例如：

- `Full-user-list` = `User1A, User1B, User2A, User2B, User3A, User3B, ...`
- `Full-user-list-excluding-B-users` = `User1A, User2A, User3A, ...`

#### 创建用户列表

> - 引入于 13.3 版本。
> - 更新于 14.0 版本。

要创建用户列表：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 功能标志**。
1. 选择 **查看用户列表**
1. 选择 **新建用户列表**。
1. 输入列表名称。
1. 选择 **创建**。

您可以通过选择列表旁边的 **编辑** (**{pencil}**) 来查看列表的用户 ID。
查看列表时，您可以通过选择 **编辑** (**{pencil}**) 对其进行重命名。

#### 添加用户到用户列表

> 引入于 13.3 版本。

要将用户添加到用户列表：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 功能标志**。
1. 选择要添加用户的列表旁边的 **编辑** (**{pencil}**)。
1. 选择 **添加用户**。
1. 以逗号分隔的值列表的形式输入用户 ID。 例如，`user@example.com、user2@example.com` 或`username1,username2,username3` 等。
1. 选择 **添加**。

#### 从用户列表删除用户

> 引入于 13.3 版本。

从用户列表中删除用户：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 功能标志**。
1. 选择要更改的列表旁边的 **编辑** (**{pencil}**)。
1. 选择要删除的 ID 旁边的 **删除** (**{remove}**)。

## 搜索代码引用 **(PREMIUM)**

> 引入于 14.4 版本。

搜索您的项目并在您的代码中找到任何功能标志的引用，以便您在需要删除功能标志时清理它。

要搜索功能标志的代码引用：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 功能标志**。
1. 编辑要删除的功能标志。
1. 选择 **更多操作** (**{ellipsis_v}**)。
1. 选择 **搜索代码引用**。

## 禁用特定环境的功能标志

在 13.0 及更早版本中，禁用特定环境的功能标志：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 功能标志**。
1. 对于要禁用的功能标志，选择 **编辑** (**{pencil}**)。
1. 禁用标志：

    - 在 13.0 及更早版本中：滑动环境的状态开关。或者，要删除环境 spec，请在右侧选择 **删除 (X)**。
    - 在 13.1 及更高版本中：对于它适用的每个策略，在 **环境** 下，删除环境。

1. 选择 **保存修改**。

## 为所有环境禁用功能标志

要为所有环境禁用功能标志：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 功能标志**。
1. 对于要禁用的功能标志，将状态切换开关滑动到 **已禁用**。

功能标志显示在 **已禁用** 选项卡上。

## 将功能标志与您的应用程序集成

要在您的应用程序中使用功能标志，请从极狐GitLab 获取访问凭证。
然后使用客户端库准备您的应用程序。

### 获取访问凭证

要获取您的应用程序与极狐GitLab 通信所需的访问凭据：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 功能标志**。
1. 选择 **配置** 查看以下内容：
   - **API URL**：客户端（应用程序）连接以获取功能标志列表的 URL。
   - **实例 ID**：授权检索功能标志的唯一令牌。
   - **应用程序名称**：应用程序运行的*环境*名称（不是应用程序本身的名称）。

     例如，如果应用程序为生产服务器运行，**应用程序名称** 可以是 `production` 或类似名称。该值用于环境 spec 评估。

请注意，这些字段的含义可能会随着时间而改变。例如，我们不确定 **实例 ID** 是分配给 **环境** 的单个令牌还是多个令牌。此外，**应用程序名称** 可以描述应用程序版本而不是运行环境。

### 选择客户端库

极狐GitLab 实现了一个与 Unleash 客户端兼容的后端。

使用 Unleash 客户端，开发人员可以在应用程序代码中定义标志的默认值。
如果提供的配置文件中不存在标志，则每个功能标志评估都可以表达所需的结果。

Unleash 目前[为各种语言和框架提供了许多 SDK](https://github.com/Unleash/unleash#client-implementations)。

<!--
### Feature flags API information

For API content, see:

- [Feature Flags API](../api/feature_flags.md)
- [Feature Flag Specs API](../api/feature_flag_specs.md) (Deprecated and [scheduled for removal](https://gitlab.com/gitlab-org/gitlab/-/issues/213369) in GitLab 14.0.)
- [Feature Flag User Lists API](../api/feature_flag_user_lists.md)
- [Legacy Feature Flags API](../api/feature_flags_legacy.md)
-->

### Go 应用示例

以下是如何在 Go 应用程序中集成功能标志的示例：

```go
package main

import (
    "io"
    "log"
    "net/http"

    "github.com/Unleash/unleash-client-go/v3"
)

type metricsInterface struct {
}

func init() {
    unleash.Initialize(
        unleash.WithUrl("https://gitlab.com/api/v4/feature_flags/unleash/42"),
        unleash.WithInstanceId("29QmjsW6KngPR5JNPMWx"),
        unleash.WithAppName("production"), // Set to the running environment of your application
        unleash.WithListener(&metricsInterface{}),
    )
}

func helloServer(w http.ResponseWriter, req *http.Request) {
    if unleash.IsEnabled("my_feature_name") {
        io.WriteString(w, "Feature enabled\n")
    } else {
        io.WriteString(w, "hello, world!\n")
    }
}

func main() {
    http.HandleFunc("/", helloServer)
    log.Fatal(http.ListenAndServe(":8080", nil))
}
```

### Ruby 应用示例

下面是如何在 Ruby 应用程序中集成功能标志的示例。

Unleash 客户端获得一个用户 ID，用于 **百分比上线 (登录用户)** 上线策略或 **目标用户** 列表。

```ruby
#!/usr/bin/env ruby

require 'unleash'
require 'unleash/context'

unleash = Unleash::Client.new({
  url: 'http://gitlab.com/api/v4/feature_flags/unleash/42',
  app_name: 'production', # Set to the running environment of your application
  instance_id: '29QmjsW6KngPR5JNPMWx'
})

unleash_context = Unleash::Context.new
# Replace "123" with the ID of an authenticated user.
# Note that the context's user ID must be a string:
# https://unleash.github.io/docs/unleash_context
unleash_context.user_id = "123"

if unleash.is_enabled?("my_feature_name", unleash_context)
  puts "Feature enabled"
else
  puts "hello, world!"
end
```

### Unleash 代理示例

从 [Unleash Proxy](https://docs.getunleash.io/reference/unleash-proxy) 0.2 版开始，代理与功能标志兼容。要运行 Docker 容器以连接到项目的功能标志，请运行以下命令：

```shell
docker run \
  -e UNLEASH_PROXY_SECRETS=<secret> \
  -e UNLEASH_URL=<project feature flags URL> \
  -e UNLEASH_INSTANCE_ID=<project feature flags instance ID> \
  -e UNLEASH_APP_NAME=<project environment> \
  -e UNLEASH_API_TOKEN=<tokenNotUsed> \
  -p 3000:3000 \
  unleashorg/unleash-proxy
```

| 变量                   | 值                                                                                                                                |
| --------------------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| `UNLEASH_PROXY_SECRETS`      | 用于配置 [Unleash Proxy 客户端](https://docs.getunleash.io/reference/unleash-proxy#how-to-connect-to-the-proxy) 的共享密钥。 |
| `UNLEASH_URL`         | 您项目的 API URL。有关更多详细信息，请阅读[获取访问凭证](#获取访问凭证)。 |
| `UNLEASH_INSTANCE_ID` | 您的项目的实例 ID。有关更多详细信息，请阅读[获取访问凭证](#获取访问凭证)。 |
| `UNLEASH_APP_NAME`    | 应用程序运行环境的名称。有关更多详细信息，请阅读[获取访问凭证](#获取访问凭证)。 |
| `UNLEASH_API_TOKEN`   | 需要启动 Unleash 代理，但不用于连接到极狐GitLab。可以设置为任何值。 |

使用 Unleash 代理时存在一个限制，其中每个代理实例只能为 `UNLEASH_APP_NAME` 中命名的环境请求标志。Proxy 代表客户端将其发送到极狐GitLab，客户端无法覆盖它。

## 功能标志关联议题 **(PREMIUM)**

> - 引入于 13.2 版本。
> - 功能标志移除于 13.5 版本。
> - 在议题中显示相关功能标志，引入于 14.1 版本

您可以将相关议题链接到功能标志。 在 **相关议题** 部分，单击 `+` 按钮并输入议题参考编号或议题的完整 URL。
然后议题出现在相关的功能标志中，反之亦然。

此功能类似于[链接议题](../user/project/issues/related_issues.md)功能。

## 性能因素

一般来说，功能标志可以在任何应用程序中使用，但是，如果它是一个大型应用程序，则可能需要提前进行额外配置。
本节介绍性能因素，帮助您的组织确定在使用该功能之前需要完成的工作。
在深入了解详细信息之前，请阅读[工作原理](#工作原理)部分。

### 应用程序节点中支持的最大客户端数

极狐GitLab 尽可能多地接受客户端请求，直到达到[速率限制](../security/rate_limits.md)。
目前，Feature Flag API 属于 SaaS 特定限制中的 **未经身份验证的流量（来自给定 IP 地址）**，因此限制为 **500 个请求/分钟**。

请注意，轮询速率可在 SDK 中配置。假设所有客户端都从同一个 IP 请求：

- 每分钟请求一次 ... 可以支持 500 个客户端。
- 每 15 秒请求一次 ... 可以支持 125 个客户端。

对于寻求更具可扩展性的解决方案的应用程序，我们建议使用 [Unleash Proxy](#unleash-代理示例)。
此代理服务器位于服务器和客户端之间。它代表客户端组向服务器请求，因此可以大大减少出站请求的数量。

<!--
There is also an [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/295472) to give more
capacity to the current rate limit.
-->

### 从网络错误中恢复

一般来说，[Unleash 客户端](https://github.com/Unleash/unleash#unleash-sdks)在服务器返回错误码时有一个回退机制。
例如，`unleash-ruby-client` 从本地备份中读取标志数据，以便应用程序可以在当前状态下继续运行。

请阅读 SDK 项目中的文档以获取更多信息。

### 私有化部署实例

在功能方面， SaaS 和私有化部署实例没有区别。

在可扩展性方面，取决于实例的规范。
例如，SaaS 在 HA 架构上运行，因此它可以同时处理大量请求，但是，在低规格机器上运行的私有化部署实例不能期望相同的结果。

<!--
Please see [Reference architectures](../administration/reference_architectures/index.md)
for more information.
-->
