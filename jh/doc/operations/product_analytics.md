---
stage: Growth
group: Product Intelligence
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 产品分析 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/225167) in GitLab 13.3.
> - It's deployed behind a feature flag, disabled by default.
> - It's disabled on GitLab.com.
> - It's able to be enabled or disabled per-project.
> - It's not recommended for production use.
> - To use it in GitLab self-managed instances, ask a GitLab administrator to enable it.
-->

> - 部署在功能标志后，默认禁用
> - 可以为每个项目启用或禁用
> - 不推荐生产使用
> - 要在自助管理实例上使用，询问极狐GitLab 管理员启用

极狐GitLab 允许您从规划中的应用程序到获得反馈。反馈不仅仅是可观察性，还包括了解人们如何使用您的产品。
Product Analytics 使用从您的应用程序发送的事件来了解他们如何使用它。
它基于最好的开源事件跟踪器 [Snowplow](https://github.com/snowplow/snowplow)。使用产品分析，您可以在极狐GitLab 中接收和分析 Snowplow 数据。

## 启用或禁用产品分析

产品分析正在开发中，尚未准备好用于生产。它部署在**默认禁用**的功能标志后面。[有权访问 GitLab Rails 控制台的管理员](../administration/feature_flags.md) 可以为您的实例启用它。可以为每个项目启用或禁用产品分析。

启用：

```ruby
# Instance-wide
Feature.enable(:product_analytics)
# or by project
Feature.enable(:product_analytics, Project.find(<project ID>))
```

禁用：

```ruby
# Instance-wide
Feature.disable(:product_analytics)
# or by project
Feature.disable(:product_analytics, Project.find(<project ID>))
```

## 访问产品分析

为产品分析启用功能标志后，您可以访问用户界面：

1. 以至少具有报告者角色的用户身份登录极狐GitLab。
1. 导航到 **监控 > 产品分析**。

用户界面包含：

- 显示最近事件和总数的事件页面。
- 发送示例事件的测试页面。
- 包含要在您的应用程序中实现的代码的设置页面。

## 产品分析的速率限制

虽然产品分析正在开发中，但它的速率限制为每个项目 **100 个事件/分钟**。此限制可防止数据库中的事件表增长过快。

## 产品分析的数据存储

产品分析存储事件在极狐GitLab 数据库中。

WARNING:
这种数据存储是实验性的，很可能会在未来的开发过程中移除这些数据。

## 事件收集

事件由 Rails 收集器收集，允许极狐GitLab 快速发布功能。由于可扩展性问题，计划切换到一个单独的应用程序，例如 snowplow-go-collector，用于事件收集。
