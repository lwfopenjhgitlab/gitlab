---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用软件包升级极狐GitLab **(FREE SELF)**

您可以使用极狐GitLab 软件包将极狐GitLab 升级到新版本。

## 先决条件

<!--
- Decide when to upgrade by viewing the [supported upgrade paths](../index.md#upgrade-paths).
  You can't directly skip major versions (for example, go from 10.3 to 12.7 in one step).
- If you are upgrading from a non-package installation to a GitLab package installation, see
  [Upgrading from a non-package installation to a GitLab package installation](https://docs.gitlab.com/omnibus/update/convert_to_omnibus.html).
-->

- 确保所有后台迁移<!--[后台迁移](../index.md#checking-for-background-migrations-before-upgrading)-->已完全完成。在后台迁移完成之前升级可能会导致数据损坏。我们建议在主要版本和次要版本之间执行的升级次数，每周不超过一次，以便有时间完成后台迁移。
- 在升级应用程序服务器之前，必须将 Gitaly 服务器升级到较新的版本，可以防止应用服务器上的 gRPC 客户端发送旧 Gitaly 版本不支持的 RPC。

## 停机时间

- 对于单节点安装实例，极狐GitLab 在升级过程中对用户不可用。用户的网络浏览器显示 `Deploy in progress` 消息或 `502` 错误。

<!--
- 对于多节点安装实例，请参阅如何执行[零停机升级](../zero_downtime.md)。

## 特定版本的更改

升级版本可能需要一些手动干预。有关更多信息，请检查您要升级到的版本：

- [极狐GitLab 15](https://docs.gitlab.cn/omnibus/update/gitlab_15_changes.html)
-->

## 升级前备份

在安装较新的极狐GitLab 版本之前备份极狐GitLab 数据库。您可以通过在 `/etc/gitlab/skip-auto-backup` 创建一个空文件来跳过此自动数据库备份：

```shell
sudo touch /etc/gitlab/skip-auto-backup
```

尽管如此，强烈建议您自己维护完整的最新备份<!--[备份](../../raketasks/backup_restore.md)-->。

<a id="upgrade-using-the-official-repositories"></a>

## 使用官方仓库升级

所有极狐GitLab 软件包都发布到极狐GitLab [软件包服务器](https://packages.gitlab.cn/)，包含 `gitlab-jh` 仓库。

### 使用官方仓库升级到最新版本

如果您定期升级极狐GitLab，例如每月一次，您可以使用包管理器升级到最新版本。

要升级到最新的极狐GitLab 版本：

```shell
# Ubuntu/Debian
sudo apt update && sudo apt install gitlab-jh

# RHEL/CentOS 7
sudo yum install gitlab-jh
```

<!--
NOTE:
For the GitLab Community Edition, replace `gitlab-ee` with
`gitlab-ce`.
-->

### 使用官方仓库升级到特定版本

Linux 软件包管理器默认安装最新可用版本的软件包来进行安装和升级。<!--Upgrading directly to the latest major
version can be problematic for older GitLab versions that require a multi-stage
[upgrade path](../index.md#upgrade-paths). An upgrade path can span multiple
versions, so you must specify the specific GitLab package with each upgrade.-->

在软件包管理器的安装或升级命令中指定极狐GitLab 版本号：

1. 识别已安装软件包的版本号：

   ```shell
   # Ubuntu/Debian
   sudo apt-cache madison gitlab-jh

   # RHEL/CentOS 7
   yum --showduplicates list gitlab-jh
   ```

1. 使用以下命令之一安装特定的 `gitlab-jh` 包，并将 `<version>` 替换为您在上一步中找到的受支持版本：

   ```shell
   # Ubuntu/Debian
   sudo apt install gitlab-jh=<version>

   # RHEL/CentOS 6 and 7
   yum install gitlab-jh-<version>
   ```

<!--
NOTE:
For the GitLab Community Edition, replace `gitlab-ee` with
`gitlab-ce`.
-->

## 使用手动下载的包进行升级

NOTE:
建议使用[软件包库](#upgrade-using-the-official-repositories)，而不是手动安装。

如果由于某种原因，您不使用官方仓库，则可以下载软件包并手动安装。此方法可用于第一次安装极狐GitLab 或更新它。

下载并安装极狐GitLab：

1. 访问软件包的[官方仓库](#upgrade-using-the-official-repositories)。
1. 通过搜索您要安装的版本（例如 14.1.8）过滤列表。单个版本可能存在多个包，每个支持的发行版和架构都有一个。 文件名旁边是指示分布的标签，因为文件名可能相同。
1. 找到您要安装的软件包版本，然后从列表中选择文件名。
1. 选择右上角的 **下载**，下载软件包。
1. 下载软件包后，使用以下命令之一安装它，并将 `<package_name>` 替换为您下载的包名称：

   ```shell
   # Debian/Ubuntu
   dpkg -i <package_name>

   # CentOS/RHEL
   rpm -Uvh <package_name>
   ```

<!--
NOTE:
For the GitLab Community Edition, replace `gitlab-ee` with
`gitlab-ce`.
-->

## 故障排除

### 获取极狐GitLab 安装的状态

```shell
sudo gitlab-ctl status
sudo gitlab-rake gitlab:check SANITIZE=true
```

- 查看有关使用 `gitlab-ctl` 执行[维护任务](https://docs.gitlab.cn/omnibus/maintenance/index.html)的信息。
- 查看有关使用 `gitlab-rake` 执行[检查配置](../../administration/raketasks/maintenance.md#check-gitlab-configuration)的信息。

<!--
### RPM 'package is already installed' error

If you are using RPM and you are upgrading from GitLab Community Edition to GitLab Enterprise Edition you may get an error like this:

```shell
package gitlab-7.5.2_omnibus.5.2.1.ci-1.el7.x86_64 (which is newer than gitlab-7.5.2_ee.omnibus.5.2.1.ci-1.el7.x86_64) is already installed
```

You can override this version check with the `--oldpackage` option:

```shell
sudo rpm -Uvh --oldpackage gitlab-7.5.2_ee.omnibus.5.2.1.ci-1.el7.x86_64.rpm
```

### Package obsoleted by installed package

CE and EE packages are marked as obsoleting and replacing each other so that both aren't installed and running at the same time.

If you are using local RPM files to switch from CE to EE or vice versa, use `rpm` for installing the package rather than `yum`. If you try to use yum, then you may get an error like this:

```plaintext
Cannot install package gitlab-ee-11.8.3-ee.0.el6.x86_64. It is obsoleted by installed package gitlab-ce-11.8.3-ce.0.el6.x86_64
```

To avoid this issue, either:

- Use the same instructions provided in the
  [Upgrade using a manually-downloaded package](#upgrade-using-a-manually-downloaded-package) section.
- Temporarily disable this checking in yum by adding `--setopt=obsoletes=0` to the options given to the command.

### 500 error when accessing Project > Settings > Repository

This error occurs when GitLab is converted from CE > EE > CE, and then back to EE.
When viewing a project's repository settings, you can view this error in the logs:

```shell
Processing by Projects::Settings::RepositoryController#show as HTML
  Parameters: {"namespace_id"=>"<namespace_id>", "project_id"=>"<project_id>"}
Completed 500 Internal Server Error in 62ms (ActiveRecord: 4.7ms | Elasticsearch: 0.0ms | Allocations: 14583)

NoMethodError (undefined method `commit_message_negative_regex' for #<PushRule:0x00007fbddf4229b8>
Did you mean?  commit_message_regex_change):
```

This error is caused by an EE feature being added to a CE instance on the initial move to EE.
After the instance is moved back to CE and then is upgraded to EE again, the
`push_rules` table already exists in the database. Therefore, a migration is
unable to add the `commit_message_regex_change` column.

This results in the [backport migration of EE tables](https://gitlab.com/gitlab-org/gitlab/-/blob/cf00e431024018ddd82158f8a9210f113d0f4dbc/db/migrate/20190402150158_backport_enterprise_schema.rb#L1619) not working correctly.
The backport migration assumes that certain tables in the database do not exist when running CE.

To fix this issue:

1. Start a database console:

   In GitLab 14.2 and later:

   ```shell
   sudo gitlab-rails dbconsole --database main
   ```

   In GitLab 14.1 and earlier:

   ```shell
   sudo gitlab-rails dbconsole
   ```

1. Manually add the missing `commit_message_negative_regex` column:

   ```sql
   ALTER TABLE push_rules ADD COLUMN commit_message_negative_regex VARCHAR;

   # Exit psql
   \q
   ```

1. Restart GitLab:

   ```shell
   sudo gitlab-ctl restart
   ```

### Error `Failed to connect to the internal GitLab API` on a separate GitLab Pages server

Please see [GitLab Pages troubleshooting](../../administration/pages/index.md#failed-to-connect-to-the-internal-gitlab-api).
-->

### 运行 `apt-get update` 出现错误 `An error occurred during the signature verification` 

要更新极狐GitLab 软件包服务器的 GPG 密钥，请运行：

```shell
curl --silent "https://packages.gitlab.cn/repository/raw/gpg/public.gpg.key" | apt-key add -
apt-get update
```
