---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 降级 **(FREE SELF)**

本节包含有关如何恢复到早期版本的软件包的一般信息。

WARNING:
您必须至少在要降级到的版本下创建数据库备份。理想情况下，您手头应该有一个完整备份存档<!--[完整备份存档](../../raketasks/backup_restore.md)-->。

下面的示例演示了在次要版本和补丁版本之间降级时的降级过程（例如，从 14.9.3 到 14.9.1）。

<!--
在主要版本之间降级时，请考虑升级到要降级的主要版本时发生的[特定版本更改](index.md#version-specific-changes)。
-->

步骤包括：

- 停止极狐GitLab
- 删除当前软件包
- 安装旧软件包
- 重新配置极狐GitLab
- 恢复数据库备份
- 启动极狐GitLab

操作步骤：

1. 停止极狐GitLab 并删除当前包：

   ```shell
   # If running Puma
   sudo gitlab-ctl stop puma

   # Stop sidekiq
   sudo gitlab-ctl stop sidekiq

   # If on Ubuntu: remove the current package
   sudo dpkg -r gitlab-jh

   # If on Centos: remove the current package
   sudo yum remove gitlab-jh
   ```

1. 确定要降级到的极狐GitLab 版本：

   ```shell
   # Ubuntu
   sudo apt-cache madison gitlab-jh

   # CentOS:
   sudo yum --showduplicates list gitlab-jh
   ```

1. 将极狐GitLab 降级到所需版本（例如，降级到极狐GitLab 14.9.1）：

   ```shell
   # Ubuntu
   sudo apt install gitlab-jh=14.9.1-jh.0

   # CentOS:
   sudo yum install gitlab-jh-14.9.1-jh.0.el7
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

1. 恢复极狐GitLab <!--[Restore GitLab](../../raketasks/backup_restore.md#restore-for-omnibus-gitlab-installations)-->数据库，完成降级。
