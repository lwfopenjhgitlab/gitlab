---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 如何更新极狐GitLab 文档

任何人都可以为极狐GitLab 文档做出贡献！您可以在以下情况下创建文档合并请求：

- 您在现有文档中发现错误或其他改进空间。
- 您希望翻译尚未翻译的文档，可以帮助极狐GitLab 用户或管理员完成他们的工作。

<!--
If you are working on a feature or enhancement, use the
[feature workflow process described in the GitLab Handbook](https://about.gitlab.com/handbook/engineering/ux/technical-writing/workflow/#for-a-product-change).
-->

## 如何更新文档

如果您不是极狐GitLab 团队成员，或者没有相关仓库的开发人员角色，要更新极狐GitLab 文档：

<!--
1. Select an issue you'd like to work on.
   - You don't need an issue to open a merge request.
   - For a Hackathon, in the issue, in a comment, mention the person who opened the issue and ask for the issue to be assigned to you.
     To be fair to other contributors, if you see someone has already asked to work on the issue, choose another issue.
     If you are looking for issues to work on and don't see any that suit you, you can always fix [Vale](testing.md#vale) issues.
-->

1. 访问[极狐GitLab 仓库](https://jihulab.com/gitlab-cn/gitlab)。
1. 在右上角，选择 **派生**，制作仓库的副本。
1. 在您的派生项目中，通过转至 `\jh\doc` 目录找到文档页面。
1. 如果您了解 Git，请进行更改并打开合并请求。
   如果没有，请按照以下步骤操作：
   1. 在右上角，选择 **编辑**，进行更改，然后 **保存**。
   1. 从左侧菜单中，选择 **合并请求**。
   1. 对于源分支，选择您的派生项目和分支。如果您没有创建分支，请选择 `main-jh`。
      对于目标分支，选择[极狐GitLab 仓库](https://jihulab.com/gitlab-cn/gitlab)的 `main-jh` 分支。
   1. 对于提交消息，不要以句点结尾。
   1. 选择 **提交更改**，合并请求开放。
   1. 选择 **文档** 模板。在描述中，写下更改的简短摘要和相关议题的链接（如果有）。


如果您在页面上工作时需要帮助，请查看[翻译前必读](styleguide/index.md)。

<!--
- The .
- The [Word list](styleguide/word_list.md)
- The [Markdown Guide](https://about.gitlab.com/handbook/markdown-guide/).
-->

### 寻求帮助

如果您有以下情况，请向 Technical Writer 团队寻求帮助：

- 需要帮助选择正确的文档位置。
- 想要讨论翻译想法。
- 需要其它帮助。

如果需要紧急帮助，请直接在议题或合并请求中指派 Technical Writer。如果需要非紧急帮助，请在议题或合并请求中 ping Technical Writer。

<!--
确定可以帮助您的人：

1. Locate the Technical Writer for the relevant
   [DevOps stage group](https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments).
1. Either:
   - If urgent help is required, directly assign the Technical Writer in the issue or in the merge request.
   - If non-urgent help is required, ping the Technical Writer in the issue or merge request.

If you are a member of the GitLab Slack workspace, you can request help in `#docs`.
-->

<!--
## Documentation labels

When you author an issue or merge request, you must add these labels:

- A [type label](../contributing/issue_workflow.md#type-labels).
- A [stage label](../contributing/issue_workflow.md#stage-labels) and [group label](../contributing/issue_workflow.md#group-labels).
  For example, `~devops::create` and `~group::source code`.
- A `~documentation` [specialization label](../contributing/issue_workflow.md#specialization-labels).

A member of the Technical Writing team adds these labels:

- A [documentation scoped label](../../user/project/labels.md#scoped-labels) with the
  `docs::` prefix. For example, `~docs::improvement`.
- The [`~Technical Writing` team label](../contributing/issue_workflow.md#team-labels).
- A type label: either `~"type::feature"` or `~"type::maintenance"`.
-->


### 审核和合并

任何对相关项目具有维护者角色的人都可以合并文档更改。维护者必须努力确保内容：

- 清晰且足够容易让目标受众访问并理解。
- 符合相关规范和指南<!--Meets the [Documentation Guidelines](index.md) and [Style Guide](styleguide/index.md).-->

<!--
If the author or reviewer has any questions, they can mention the writer who is assigned to the relevant
[DevOps stage group](https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments).
-->

该过程涉及以下步骤：

- 第一审核人。由代码审核人或其他合适的同事进行审核，以确保准确性、清晰度和完整性。对于没有实质性内容更改的小修复，可以跳过此步骤。
- Technical Writer（可选）。如果在合并合并请求前未完成审核，则必须安排在合并后。 仅在需要紧急合并时才安排合并后审核。请求一个：
  - 合并前审核，将 MR 指派给 Technical Writer。<!--listed for the applicable
    [DevOps stage group](https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments).-->
  - 合并后审核，查看[合并后审核文档](#合并后审核)。
- 维护者。对于合并请求，维护者：
  - 可以随时要求上述任何审核。
  - 在 Technical Writer 审核之前或之后进行审核。

<!--
  - 确保设置了给定的发布里程碑。
  - Ensure the appropriate labels are applied, including any required to pick a merge request into
    a release.
  - Ensure that, if there has not been a Technical Writer review completed or scheduled, they
    [create the required issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Doc%20Review), assign it to the Technical Writer of the given stage group,
    and link it from the merge request.

The process is reflected in the **Documentation**
[merge request template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/merge_request_templates/Documentation.md).
-->

<!--
## Other ways to help

If you have ideas for further documentation resources please
[create an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Documentation)
using the Documentation template.
-->

## 合并后审核

如果在合并之前没有指派给 Technical Writer 进行审核，则必须由开发人员或维护人员在合并后立即安排审核。为此，使用 [Doc Review 描述模板](https://jihulab.com/gitlab-cn/gitlab/-/issues/new?issuable_template=Doc%20Review) 创建一个议题，并链接引入了文档更改的且已合并的合并请求。

<!--
Circumstances in which a regular pre-merge Technical Writer review might be skipped include:

- There is a short amount of time left before the milestone release. If fewer than three
 days are remaining, seek a post-merge review and ping the writer via Slack to ensure the review is
  completed as soon as possible.
- 更改的规模很小，并且您非常确信该功能的早期用户可以轻松使用所编写的文档。
-->

请记住：

- 在极狐GitLab，我们像对待代码一样对待文档。与代码一样，必须审核文档以确保质量。
- 如果尽快合并同一 MR 的代码很重要，您可以请求合并后由 Technical Writer 审核文档。在这种情况下，原始 MR 的作者可以在后续 MR 中解决 Technical Writer 提供的反馈。
- Technical Writer 还可以帮助决定，是否可以在没有审核的情况下合并文档，审核将在合并后不久进行。

<!--
- Documentation forms part of the GitLab [definition of done](../contributing/merge_request_workflow.md#definition-of-done).
- That pre-merge Technical Writer reviews should be most common when the code is complete well in
  advance of a milestone release and for larger documentation changes.
-->

<!--
### Before merging

Ensure the following if skipping an initial Technical Writer review:

- [Product badges](styleguide/index.md#product-tier-badges) are applied.
- The GitLab [version](styleguide/index.md#gitlab-versions) that
  introduced the feature is included.
- Changes to headings don't affect in-app hyperlinks.
- Specific [user permissions](../../user/permissions.md) are documented.
- New documents are linked from higher-level indexes, for discoverability.
- The style guide is followed:
  - For [directories and files](styleguide/index.md#work-with-directories-and-files).
  - For [images](styleguide/index.md#images).

Merge requests that change the location of documentation must always be reviewed by a Technical
Writer before merging.
-->
