## Mirror share

Mirror 是极狐GitLab 付费版的一个功能，用来同步获取远端仓库的数据，设置之后会定期自动更新。

### Mirror 使用简介

在新建的项目中使用 Mirror 功能：

1. 在极狐GitLab 中新建一个空的项目，进入该项目。
1. 点击侧边栏中的 **设置 > 仓库**。 
1. 在页面中选择 **镜像仓库**。
1. 在表单中输入 Git 仓库 URL，例如 `https://github.com/rails/rails.git`。镜像方向选择 **拉取**。如果是私密项目，还需要选择验证方式，选择 **密码** 或 **SSH 公钥**。
1. 点击 **镜像仓库**。

稍后整个仓库将会被同步过来，同步时间的长短取决于项目的大小。

设置好之后，点击镜像仓库，仓库就会开始同步了，过一会，整个仓库就会被同步过来，时间长短取决于项目大小。

### Mirror 的代码原理

在为 project 设置 mirror 之后，会发送请求到 `Projects/MirrorsController` 中。


```ruby
# ee/app/controllers/ee/projects/mirrors_controller.rb

override :update
def update
  esult = ::Projects::UpdateService.new(project, current_user, safe_mirror_params).execute
  ...
end
```

然后代码运行来到了 `Projects/UpdateService` 中。

```ruby
# ee/app/service/ee/projects/update_service.rb
override :execute
def execute
  ...

  if result[:status] == :success
    ...
    project.import_state.force_import_job! if params[:mirror].present? && project.mirror?
    ...
  end

  result
end
```

在 project 的数据更新完毕后，由于我们为此 project 设置了 mirror 设置，所以 project 的 mirror 字段会被更新成 true。所以 `project.mirror?` 方法会返回 true。`mirror?` 方法是 `mirror` 方法的一个 `alias_method`。所以代码会调用 `project.import_state.force_import_job!` 来进行 mirror job 的入队操作。

```ruby
# ee/app/modles/ee/project_import_state.rb
def force_import_job!
  return if mirror_update_due? || updating_mirror?

  set_next_execution_to_now(prioritized: true)
  reset_retry_count if hard_failed?

  save!

  UpdateAllMirrorsWorker.perform_async
end
```

在执行 `UpdateAllMirrorsWorker` 之前，代码做了两个判断 `mirror_update_due?` 和 `updating_mirror?`。

```ruby
# ee/app/models/ee/project_import_state.rb
def mirror_update_due?
  return false unless project_eligible_for_mirroring?
  return false unless next_execution_timestamp?
  return false if hard_failed?
  return false if updating_mirror?

  next_execution_timestamp <= Time.current
end
```

- `project_eligible_for_mirroring?` 是用来判断此 project 是否开启了 mirror 并且仓库存在，并且此 project 没有被 archived 或删除。
- `next_execution_timestamp?` 是判断此 project 的 import_state 的 `next_execution_timestamp` 字段是否有值，此字段代表下次 mirror 程序执行的时间。
- `hard_failed?` 是用来判断此 project 的 mirror 失败次数是否已经达到了上限（14次）。
- `updating_mirror?` 用来判断 mirror 程序是否正在执行中。

符合了以上这些判断条件后，最后检查此 project 的 import_state 的下次执行时间字段 `next_execution_timestamp` 是否小于等于当前时间。

所以一个 project 需要满足以下条件之一，才会在 `force_import_job!` 中执行 mirror 操作：

- project 开启了 mirror 但是仓库是空的。
- project 的 import_state 的 `next_execution_timestamp` 字段为 `nil`。
- project mirror 的重试次数已经超过上限。
- project 的 mirror 程序正在执行。
- project 的 import_state 的 `next_execution_timestamp` 大于当前时间。

然后程序使用 `set_next_execution_to_now` 方法来更新此 project 的 import_state 的 `next_execution_timestamp` 字段。并且如果重试次数已经达到上限，就将重试次数重置为 0，然后保存记录，并调用 `UpdateAllMirrorsWorker`。

```ruby
# ee/app/workers/update_all_mirrors_worker.rb
def perform
  return if Gitlab::Database.read_only?

  scheduled = 0
  with_lease do
    scheduled = schedule_mirrors!

    if scheduled > 0
      # Wait for all ProjectImportScheduleWorker jobs to be picked up
      deadline = Time.current + SCHEDULE_WAIT_TIMEOUT
      sleep 1 while pending_project_import_scheduling? && Time.current < deadline
    end
  end

  # If we didn't get the lease, or no updates were scheduled, exit early
  return unless scheduled > 0

  # Wait to give some jobs a chance to complete
  sleep(RESCHEDULE_WAIT)

  # If there's capacity left now (some jobs completed),
  # reschedule this job to enqueue more work.
  #
  # This is in addition to the regular (cron-like) scheduling of this job.
  UpdateAllMirrorsWorker.perform_async if Gitlab::Mirror.reschedule_immediately?
end
```

在 `UpdateAllMirrorsWorker` 的 perform 方法中，schedule 的操作是在 `with_lease` 方法中进行的。

```ruby
# ee/app/workers/update_all_mirrors_worker.rb
def with_lease
  if lease_uuid = try_obtain_lease
    yield
  end

  lease_uuid
ensure
  cancel_lease(lease_uuid) if lease_uuid
end

def try_obtain_lease
  ::Gitlab::ExclusiveLease.new(LEASE_KEY, timeout: LEASE_TIMEOUT).try_obtain
end

def cancel_lease(uuid)
  ::Gitlab::ExclusiveLease.cancel(LEASE_KEY, uuid)
end
```

在 `with_lease` 这个方法中，`try_obtain_lease` 方法会初始化一个 `Gitlab::ExclusiveLease` 实例，传入一个常量 `LEASE_KEY` 也就是 `update_all_mirrors` 和一个过期时间，并调用此实例的 `try_obtain` 方法，这个方法会在 Redis 中写入一对 key value，其中 key 就是 `LEASE_KEY` 加上一个前缀得到的。然后执行 `yield` 中的代码，最终调用 `cancel_lease` 方法删除之前使用 `try_obtain` 写入到 Redis 中的 key。

也就是说在 `UpdateAllMirrorsWorker` 中，同一时间只能有一个 with_lease 中的代码块在执行。如果再调用 with_lease 方法，会因为 Redis 中的 key 已经存在而使 `try_obtain_lease` 返回 false。

在 `with_lease` 的代码块中，会调用 `schedule_mirrors!` 方法。

```ruby
# ee/app/workers/update_all_mirrors_worker.rb
def schedule_mirrors!
  ...
  ::Gitlab::Mirror.reset_scheduling

  capacity = Gitlab::Mirror.available_capacity

  ...
  now = Time.current
  last = nil
  scheduled = 0

  ...
  last = Time.utc(2020, 3, 28) if Gitlab.com?

  while capacity > 0
    batch_size = [capacity * 2, 500].min
    projects = pull_mirrors_batch(freeze_at: now, batch_size: batch_size, offset_at: last).to_a
    break if projects.empty?

    projects_to_schedule = projects.lazy.select(&:mirror?).take(capacity).force
    capacity -= projects_to_schedule.size

    schedule_projects_in_batch(projects_to_schedule)

    scheduled += projects_to_schedule.length

    # If fewer than `batch_size` projects were returned, we don't need to query again
    break if projects.length < batch_size

    last = projects.last.import_state.next_execution_timestamp
  end

  scheduled
end
```

在 `schedule_mirrors!`  方法中，首先调用 `::Gitlab::Mirror.reset_scheduling` 方法，清空 Redis 中记录的 schedule 的任务的数量。然后获取当前 mirror 任务的剩余容量。剩余容量是总容量减去已占用容量得到的，总容量通过 `Gitlab::CurrentSettings.mirror_max_capacity` 设置。

如果剩余容量大于 0，则通过 `pull_mirrors_batch` 获取到需要更新的 project, 这些 project 符合以下条件：

- 没有被 archived。
- 没有被删除。
- import_state 的 `next_execution_timestamp` 小于当前时间。
- 所在的 namespace 有使用 mirror pull 的权限。

然后拿取剩余容量大小数量的 projects，进行 schedule 操作，并将容量的值置为减去 schedule 的 projects 的数量，注意这边用的是 wihile 循环，说明如果剩余容量在减去 schedule 的 projects 的数量后如果仍然大于 0，将会继续执行 projects 的 schedule 操作。

通过调用 `schedule_projects_in_batch` 方法来 schedule 一个 project。

```ruby
# ee/app/workers/update_all_mirrors_worker.rb
def schedule_projects_in_batch(projects)
  return if projects.empty?

  # projects were materialized at this stage
  ::Gitlab::Mirror.track_scheduling(projects.map(&:id))

  ProjectImportScheduleWorker.bulk_perform_async_with_contexts(
    projects,
    arguments_proc: -> (project) { project.id },
    context_proc: -> (project) { { project: project } }
  )
end
```

在此方法中，会调用 `Gitlab::Mirror.track_scheduling` 方法来记录 schedule 的 project 的 ID，记录在 Redis 的 set 中。记录这些 schedule 的 project 的 ID 是为了确保这些 project 尽量在 schedule 完毕再执行下一批需要 mirror 的 project。在 `with_lease` 中会判断 Redis 中这个 set 的容量是否大于 0，如果不大于 0，可以进行下一批 project 的 schedule；如果大于 0，就等到 `deadline` 再进行下一批 project 的 schedule。 然后调用 `ProjectImportScheduleWorker` 将这些 project 进行 schedule。这里使用了批量执行 worker 的方式。

在 `ProjectImportScheduleWorker` 中，首先会调用 `::Gitlab::Mirror.untrack_scheduling(project_id)` 将此 project 的 ID 从 Redis 的 set 中删除。然后调用此 project 的 import_state 的 `schedule` 方法。

```ruby
# ee/app/workers/project_import_schedule_worker.rb
def perform(project_id)
  ::Gitlab::Mirror.untrack_scheduling(project_id)

  return if Gitlab::Database.read_only?

  project = Project.with_route.with_import_state.with_namespace.find_by_id(project_id)

  with_context(project: project) do
    unless project&.import_state
      log_extra_metadata_on_done(:mirroring_skipped, "No import state found for #{project_id}")
      next
    end

    project.import_state.schedule
  end
end
```

在 `app/models/project_import_state.rb` 中，使用了 `state_machine` 这个 gem，这个 gem 可以定义一些状态，并且定义在状态转换的时候需要执行的操作。

```ruby
# app/models/project_import_state.rb
after_transition [:none, :finished, :failed] => :scheduled do |state, _|
  state.run_after_commit do
    job_id = project.add_import_job

    if job_id
      correlation_id = Labkit::Correlation::CorrelationId.current_or_new_id
      update(jid: job_id, correlation_id_value: correlation_id)
    end
  end
end
```

在状态转换到 `scheduled` 之后，会调用这个 project 的 `add_import_job` 方法。

```ruby
# ee/app/models/ee/project.rb
override :add_import_job
def add_import_job
  return if gitlab_custom_project_template_import?

  # Historically this was intended ensure `super` is only called
  # when a project is imported(usually on project creation only) so `repository_exists?`
  # check was added so that it does not stop mirroring if later on mirroring option is added to the project.
  return super if import? && !repository_exists?

  if mirror?
    ::Gitlab::Metrics.add_event(:mirrors_scheduled)

    job_id = if ::Feature.enabled?(:delay_for_repository_update_mirror, self)
               # Delay won't be necessary when https://gitlab.com/gitlab-org/gitlab/-/issues/364799 is resolved
               RepositoryUpdateMirrorWorker.perform_in(1.minute, self.id)
             else
               RepositoryUpdateMirrorWorker.perform_async(self.id)
             end

    log_import_activity(job_id, type: :mirror)

    job_id
  end
end
```

在 `ee/app/models/ee/project.rb` 中，最终调用 `RepositoryUpdateMirrorWorker` 来执行 mirror 仓库的更新。


```ruby
# ee/app/workers/repository_update_mirror_worker.rb
def perform(project_id)
  ...

  result = Projects::UpdateMirrorService.new(project, @current_user).execute
  raise UpdateError, result[:message] if result[:status] == :error
  ...
end
```

在 `RepositoryUpdateMirrorWorker` 中，调用 `Projects::UpdateMirrorService` 来更新 project。

```ruby
# ee/app/services/projects/update_mirror_service.rb
 def execute
  ...

  checksum_before = project.repository.checksum

  update_tags do
    project.fetch_mirror(forced: true, check_tags_changed: true)
  end

  update_branches

  ...
  update_lfs_objects if project.repository.checksum != checksum_before

  ...
  run_housekeeping

  success
rescue Gitlab::Shell::Error, Gitlab::Git::BaseError, UpdateError => e
  error(e.message)
end
```

在 `Projects::UpdateMirrorService` 中，调用 project 的 fetch_mirror 方法来更新仓库（这个方法实际上是调用通过 GRPC 调用 gitaly 执行 pull 操作来更新仓库），然后在这个 service 中，会更新分支信息，LFS 存储等，然后返回成功。

以上就是 mirror 的全部流程。

那么 mirror 又是如何自动更新的呢。在 `ee/lib/gitlab/mirror.rb` 中，会注册一个 Sidekiq 的 cron job。

```ruby
# ee/lib/gitlab/mirror.rb
def configure_cron_job!
  destroy_cron_job!
  return if Gitlab::Geo.connected? && Gitlab::Geo.secondary?

  Sidekiq::Cron::Job.create(
    name: 'update_all_mirrors_worker',
    cron: SCHEDULER_CRON,
    class: 'UpdateAllMirrorsWorker'
  )
end
```
 
这个 Sidekiq 的 cron job 会每分钟就执行一次 `UpdateAllMirrorsWorker`，查找 import_state 中 `next_execution_timestamp` 字段小于当前时间且拥有 mirror pull 功能的 project，并把它们 schedule。

```ruby
# config/initializers/sidekiq.rb
Gitlab.ee do
  Gitlab::Mirror.configure_cron_job!

  Gitlab::Geo.configure_cron_jobs!
end
```

这个 Sidekiq cron job 会在初始化 Sidekiq 的时候就注册。
