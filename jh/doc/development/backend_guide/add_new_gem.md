# 如何向 JH 添加一个新的 Gem

## 背景

`Gemfile` 用来管理项目的依赖，一般位于项目的根目录。

Upstream 的 `Gemfile` 文件位于 GitLab 的根目录下。

JH 的依赖继承自 Upstream，并在其基础上需要额外的依赖，又因为 JH 的代码都放置于 `jh` 目录下，所以用 `jh/Gemfile` 文件来管理极狐GitLab 的依赖。

所以在运行 `bundle install` 命令之前，首先需要配置 JH `Gemfile` 路径：

```sh
bundle config set --local gemfile 'jh/Gemfile'
```

以上命令会在 `.bundle/config` 中写入 `BUNDLE_GEMFILE: "jh/Gemfile"`，来让 bundle 找到 JH 的 `Gemfile` 文件。

## 为 JH 新加一个 Gem

下面以添加 `ruby-debug-ide` 为例，展示具体的操作步骤。

1. 在 `jh/Gemfile` 的末尾添加新的 Gem。

    ```text
    gem 'ruby-debug-ide', '~> 0.7.3', require: false, group: [:development]
    ```

2. 安装 Gem 并更新 `jh/Gemfile.lock`。

    ```sh
    bundle install
    ```

3. 生成 JH 最新的 checksum 文件。

   ```sh
   bundle exec rake bundle_checksum:jh
   ./jh/bin/merge_checksum
   ```

4. 提交更改。
