---
stage: none
group: unassigned
---

# 极狐GitLab 测试开发指南

本文档旨在介绍极狐GitLab 自动化测试的相关指南和最佳实践。

更多未尽事项请参考 [GitLab 测试开发指南](https://docs.gitlab.com/ee/development/testing_guide/) 。

## 前端测试开发和风格指南

关于前端测试规范，以及如何避免和上游测试冲突。请查看[前端测试开发和风格指南](frontend_testing.md) 。

