---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab 许可和兼容性

[极狐GitLab](https://jihulab.com/gitlab-cn/gitlab/-/tree/main-jh/jh) 采用 "[The JiHu Edition License](https://jihulab.com/gitlab-cn/gitlab/-/blob/main-jh/jh/LICENSE)" 进行许可，其中包含更多限制说明。

## 自动化测试

为了遵守我们使用的库的许可条款，我们必须确保在添加新 gem 时检查兼容的许可证。为了自动化这个过程，我们使用了 Pivotal 的 [license_finder](https://github.com/pivotal/LicenseFinder) gem。每次推送新提交时它都会运行，并验证捆绑包中的所有 gem 和节点模块是否使用与极狐GitLab 的许可证不冲突的许可证。

但是，自动化测试存在一些限制。未通过 Bundler、npm 或 Yarn 包含的 CSS、JavaScript 或 Ruby 库（例如那些手动复制到我们的 `vendor` 目录中的源代码树中的库），必须手动且独立地进行验证。每次使用此类库时都要小心，因为自动化测试不会从其中捕获有问题的许可证。

有些 gem 可能不会在他们的 `gemspec` 文件中包含他们的许可信息，并且一些 node modules 可能不会在它们的 `package.json` 文件中包含许可信息。License Finder 无法检测到这些，必须手动验证。

### License Finder 命令

License Finder 提供了一些基本命令，您需要这些命令来管理许可证检测。

要验证检查是否通过，和/或查看导致检查失败的依赖项：

```shell
bundle exec license_finder
```

要将新许可证列入白名单：

```shell
license_finder permitted_licenses add MIT
```

要将新许可证列入黑名单：

```shell
license_finder restricted_licenses add Unlicense
```

如果没有自动检测到依赖项的许可证，则通知 License Finder：

```shell
license_finder licenses add my_unknown_dependency MIT
```

对于上述所有内容，请包含 `--why "Reason"` 和 `--who "My Name"`，以便 `decisions.yml` 文件可以跟踪何时、为什么以及谁批准了依赖项。

[License Finder README](https://github.com/pivotal/LicenseFinder) 中提供了有关 gem 及其命令如何工作的更多详细信息。

## 加密密钥

如果您的许可证是在 Customers Portal 或 License App 的本地开发或 staging 环境中创建的，则需要设置一个名为 `GITLAB_LICENSE_MODE` 且值为 `test` 的环境变量，来使用正确的解密密钥。

这些项目默认设置为使用测试许可证加密密钥。

<!--
## Additional information

Please see the [Open Source](https://about.gitlab.com/handbook/engineering/open-source/#using-open-source-libraries) page for more information on licensing.
-->
