---
stage: none
group: unassigned
---

# 极狐GitLab 开发流程指南

本文档旨在介绍极狐GitLab 开发过程中一些流程相关的内容。

## CI Job: JH 同步 Upstream 代码

为了和 Upstream 行为保持一致，极狐GitLab Repo 定期会同步 Upstream 代码。请查看[sync](sync.md)。

## CI Job: 检测 MR 中只包含 jh/ 目录的代码修改

为了避免和 Upstream 代码分叉，极狐GitLab Repo 只允许 `jh/` 目录的修改。请查看[only_jh](only_jh.md)。

## CI Job: 监控 Upstream Feature Flag 的变动

为了避免被 Upstream Feature Flag 改变极狐GitLab 的行为，需要监控 Feature Flag 的变动。请查看[watch-upstream-feature-flag](watch_upstream_feature_flag.md)。

## 处理 Upstream 失败的测试

因为极狐GitLab 修改了 GitLab 的某些行为，导致某些 Upstream 测试失败，需要特殊处理 Upstream 测试。请查看[skip-upstream-test](skip_upstream_test.md)。
