---
stage: none
group: unassigned
---

### CI Job: JH 同步 Upstream 代码

GitLab 上游仓库（即：Upstream repo）的 master 分支 通过 [`pull mirroring`](https://docs.gitlab.com/ee/user/project/repository/mirror/pull.html) 实时镜像到了 JH repo 的 master 分支。
JH repo 的 master 分支 被 `自动定时同步` 到了它的默认分支 main-jh，这样极狐GitLab 就有了 GitLab CE/EE 的所有最新代码。

在进行 `自动定时同步` 时，通过 [code-sync project](https://jihulab.com/gitlab-cn/code-sync/-/pipeline_schedules) 从以下项目的 master 分支 merge 进项目的 main-jh 分支:

|项目| 同步频率 |
|---|------|
|GitLab| every 3 hours in daytime |
|GitLab chart| every 5 hours |
|cng-images| every 4 hours |
|omnibus-gitlab| every 4 hours |
|runner| every 5 hours |

以下是 [同步脚本](https://jihulab.com/gitlab-cn/code-sync/-/blob/main-jh/.gitlab-ci.yml#L60) ，每个项目会有一个 job 进行同步，通过 `extends .sync` job 和自定义的变量完成代码合并:

```shell
runner:
  extends:
    - .sync
  variables:
    REPOSITORY: https://dummy:${JH_RUNNER_SYNC_TOKEN}@jihulab.com/gitlab-cn/gitlab-runner.git
    MERGE_FROM: main
    MERGE_INTO: main-jh
    USER_EMAIL: project9153_bot@example.com
    USER_NAME: JH_RUNNER_SYNC_TOKEN

omnibus-gitlab:
  extends:
    - .sync
  variables:
    REPOSITORY: https://dummy:${JH_OMNIBUS_GITLAB_SYNC_TOKEN}@jihulab.com/gitlab-cn/omnibus-gitlab.git
    MERGE_FROM: master
    MERGE_INTO: main-jh
    USER_EMAIL: project9157_bot@example.com
    USER_NAME: JH_OMNIBUS_GITLAB_SYNC_TOKEN

.sync:
  stage: sync
  variables:
    MERGE_FROM: master
    MERGE_INTO: main-jh
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "schedule" && $JH_SYNC_SCHEDULE == $CI_JOB_NAME'
    - when: manual
      allow_failure: true
  before_script:
    - git clone --filter=tree:0 "${REPOSITORY}" "${CI_JOB_NAME}"
    - cd ${CI_JOB_NAME}
    - git config user.email "${USER_EMAIL}"
    - git config user.name "${USER_NAME}"
    - git checkout "${MERGE_INTO}"
    - git merge "origin/${MERGE_FROM}"
  script:
    - git push origin "${MERGE_INTO}"
```

另外因为极狐GitLab 项目需要生成极狐自己的 Gemfile，所以相比其他的 project 有额外的动作。 `bundle_install_script`会为极狐配置 bundle config，指定 Gemfile 的路径为 `jh/Gemfile`。


```shell
gitlab:
  extends:
    - .sync
  image: registry.jihulab.com/gitlab-cn/gitlab-build-images:ruby-2.7.patched-golang-1.16-git-2.31-lfs-2.9-chrome-89-node-14.15-yarn-1.22-postgresql-11-graphicsmagick-1.3.36
  variables:
    REPOSITORY: https://dummy:${JH_SYNC_TOKEN}@jihulab.com/gitlab-cn/gitlab.git
    MERGE_FROM: master
    MERGE_INTO: main-jh
    USER_EMAIL: project23966655_bot2@example.com
    USER_NAME: JH_SYNC_TOKEN
  cache:
    key: vendor-ruby-v2
    paths:
      - vendor/ruby
  script:
    - apt-get update
    - apt-get --assume-yes install jq
    - ./jh/bin/build_packagejson
    - yarn install # Update yarn.lock based on new package.json
    - git commit -am  "Update package.json and yarn.lock" || true
    - mv ../vendor/ruby vendor/ || true
    - ./jh/bin/merge_checksum
    - ./jh/bin/adjust_gemfile_lock
    - . scripts/utils.sh
    - bundle_install_script
    - git commit -am "Update jh/Gemfile.lock" || true
    - !reference [.sync, script]
    - mv vendor ../
```
