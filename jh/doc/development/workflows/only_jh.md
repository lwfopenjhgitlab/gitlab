---
stage: none
group: unassigned
---

### CI Job: 检测 MR 中只包含 jh/ 目录的代码修改

极狐GitLab 的源码都统一放置在极狐GitLab 仓库（即：JH repo）中的 `jh` 目录下。类似于 GitLab EE 是对 CE 的扩展一样，极狐GitLab 是对 GitLab CE/EE 的扩展。

为了避免 JH repo 中 CE/EE 部分的代码和 Upstream repo 分叉，在 JH repo 中创建的 MR（即：JH MR）只能向 main-jh 分支中的 `jh` 目录提交代码，如需修改`jh` 目录之外的代码，则需要在 Upstream repo 中创建 MR（Upstream MR）来完成。

在 JH 的 Pipeline 有专门 job 检查 `只改动了 jh/ 目录下的代码`，该 job 只在 JH MR 里面运行，巧妙的通过 git diff 命令检测代码改动，[代码如下](https://jihulab.com/gitlab-cn/gitlab/-/blob/main-jh/jh/.gitlab/ci/only-jh.gitlab-ci.yml):

```shell
only-jh-check:
  stage: test
  extends:
    - .minimal-job
  rules:
    - if: $CI_MERGE_REQUEST_IID
  script:
    - git fetch origin ${CI_DEFAULT_BRANCH}
    - git diff --name-only origin/${CI_DEFAULT_BRANCH}... -- jh > jh-changes
    - git diff --name-only origin/${CI_DEFAULT_BRANCH}... > all-changes
    - non_jh_changes=$(diff jh-changes all-changes | sed 's/> /* /' | grep '* ') || true
    - |
      if test -n "${non_jh_changes}"; then
        echo -e "\e[31mChanges in the following files should be brought to upstream:\e[0m"
        echo "${non_jh_changes}"
        exit $(echo "${non_jh_changes}" | wc -l - | cut -f1 -d' ')
      fi

```
