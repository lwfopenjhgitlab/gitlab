---
stage: none
group: unassigned
---

### CI Job: 监控 Upstream Feature Flag 的变动

极狐GitLab 是基于 Upstream GitLab 的扩展，在 Upstream GitLab 中有大量的 Feature Flag 来控制软件的行为。在某些时候，Upstream 会对 Feature Flag 进行修改，比如设置为默认打开，或者默认关闭，一定程度上就会改变极狐GitLab 的行为，所以极狐GitLab 需要监控 Upstream Feature Flag 的变化。

在极狐GitLab 的 Pipeline 中使用 `watch-upstream-feature-flags` job, 在每次 code sync 时，通过 `git diff` 判定 upstream 是否改动了 `config/feature_flags` 文件夹。

当发生变化后，处理流程如下：
1. CI Job 发送 slack 消息通知 Dev/SRE 发现变更。
2. CI Job 会在极狐GitLab 项目中创建 Issue 记录变更，例如 [#1245](https://jihulab.com/gitlab-cn/gitlab/-/issues/1245) 。
3. Feature Flag 会影响产品的行为，但我们只需要关注影响极狐运营策略的 Feature Flag，比如 `影响免费用户创建 projects 的数量`，如果有不确定极狐的运营策略，可以找SRE或者产品确认。其他的 Feature Flag 变动只要同步就好。
4. Dev 需要在 issue 里把变动的 Feature Flag 简单描述下内容，把相关 MR 或者 issue 列出来，可以参考这个 https://jihulab.com/gitlab-cn/gitlab/-/issues/1301。
5. 如果 Dev 对变动的 Feature Flag 在 jihulab.com 的开关状态不明确，可以联系 SRE 同学查询。

[代码如下](https://jihulab.com/gitlab-cn/gitlab/-/blob/main-jh/jh/.gitlab/ci/watch-upstream-feature-flags.gitlab-ci.yml):

```shell
watch-upstream-feature-flags:
  stage: test
  allow_failure: true
  extends:
    - .minimal-job
  rules:
    - if: $GITLAB_USER_NAME == "JH_SYNC_TOKEN" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    - latest_commit=`git rev-parse --short HEAD`
    - echo "latest commit is $latest_commit"
    - latest_changes=`git diff $latest_commit^ $latest_commit --name-only`
    - echo "latest_changes is $latest_changes"
    - |-
        if [[ $latest_changes == *"config/feature_flags"* ]];
        then
            echo "Upstream feature flags changed! Change commit is $latest_commit"
            feature_flag_changes=$(git diff $latest_commit^ $latest_commit -- config/feature_flags)
            echo "feature_flag_changes is $feature_flag_changes"
            title="Feature flag changes detected"
            datetime=`TZ=UTC-8 date '+%F %T'`
            curl -X POST --header 'Content-Type:application/json' --header "PRIVATE-TOKEN:$JIHU_REPORTER_TOKEN" --data "{\"confidential\":\"true\",\"title\":\"$title $datetime\",\"description\":\"New Change in commit $latest_commit   \n see detail here, $CI_JOB_URL\n\",\"labels\":\"featureflag::changed\"}" https://jihulab.com/api/v4/projects/13953/issues
            curl -X POST \
              -H 'Content-type:application/json' \
              --data "{\"blocks\":[{\"type\":\"header\",\"text\":{\"type\":\"plain_text\",\"text\":\"$title\"}},{\"type\":\"section\",\"fields\":[{\"type\":\"mrkdwn\",\"text\":\"*Job:* <$CI_JOB_URL|#$CI_JOB_ID>\"},{\"type\":\"mrkdwn\",\"text\":\"*Issue:* <https://jihulab.com/gitlab-cn/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=featureflag%3A%3Achanged|link>\"}]}]}" \
              --url $FEATURE_FLAG_CHANNEL_URL
        else
            echo "Upstream feature flag not changed"
            curl -X POST -H "Content-Type:application/json" --data "{\"text\":\"Upstream feature flag not changed, $CI_JOB_URL\"}" \
              --url $FEATURE_FLAG_CHANNEL_URL
        fi
```
