# 极狐GitLab 开发指南

本文档旨在了解为极狐GitLab 贡献代码所需的流程和技术信息。

## 开始使用

- [搭建开发环境](env_setup/index.md)

## 代码贡献指南

- [极狐GitLab 代码贡献指南](contributing/index.md)
  - [代码开发约定以及风格指南](contributing/style_guides.md)

## 后端指南

- [极狐GitLab 后端开发指南](backend_guide/index.md)

## 前端指南

- [极狐GitLab 前端开发指南](frontend_guide/index.md)
  - [架构相关](frontend_guide/architecture.md)
  - [如何在极狐GitLab 里进行开发](frontend_guide/how_to_develop.md)

## 文档中心指南

- [极狐GitLab 文档中心指南](documentation/index.md)

## 翻译指南

- [极狐GitLab 翻译字符串指南](i18n/index.md)

## 流程指南

- [极狐GitLab 流程指南](workflows/index.md)
