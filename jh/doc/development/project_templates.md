---
stage: Manage
group: Workspace
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
---

# 贡献内置项目模板

本页面介绍如何贡献[内置项目模板](https://docs.gitlab.cn/jh/user/project/working_with_projects#create-a-project-from-a-built-in-template)。

贡献内置项目模板，您必须完成以下任务：

1. [创建一个用于评审的项目模板](#create-a-project-template-for-review)
1. [向极狐GitLab SVG 仓库添加模板 SVG 图标](#add-the-template-svg-icon-to-gitlab-svgs)
1. [创建一个包含项目模板详细信息的合并请求](#create-a-merge-request-with-vendor-details)


<a id="create-a-project-template-for-review"></a>

## 创建一个用于评审的项目模板

1. 创建一个公共项目。
1. 添加您想在模板中使用的项目内容。不要包括不必要的资源或依赖。
1. 当项目可以进行评审时，[创建议题](https://jihulab.com/gitlab-cn/gitlab/-/issues)并附上项目链接。在您的议题中需要提及相关的后端研发经理和产品经理，向其告知模板功能。
1. 评审通过后，后端研发经理会根据项目类别将您的项目添加到 `project-templates` 或 `pages` 群组。

<a id="add-the-template-svg-icon-to-gitlab-svgs"></a>

## 向极狐GitLab SVG 仓库添加模板 SVG 图标

如果项目模板拥有 SVG 图标，您必须在使用项目模板详细信息创建合并请求之前将其添加到[极狐SVG 仓库](https://jihulab.com/gitlab-cn/gitlab-svgs#adding-icons-or-illustrations)中。

<a id="create-a-merge-request-with-vendor-details"></a>

## 创建一个包含项目模板详细信息的合并请求

在极狐GitLab 实施项目模板之前，您必须<!--在包括项目 Vendor 详细信息的[`gitlab-org/gitlab`](https://gitlab.com/gitlab-org/gitlab)中-->[创建合并请求](https://docs.gitlab.cn/jh/user/project/merge_requests/creating_merge_requests)。

1. [导出项目](https://docs.gitlab.cn/jh/user/project/settings/import_export#export-a-project-and-its-data)并将文件保存为 `<name>.tar.gz`，其中 `<name>` 是项目名称的简写。

1. 在 `gitlab-cn/gitlab` 中，创建并检出新分支。
1. 编辑以下文件以包括项目模板：
     - 将 `<name>.tar.gz` 添加到 `jh/vendor/project_templates` 目录中。
     - 在 `jh/lib/jh/gitlab/project_template.rb` 中，在 `localized_jh_templates_table` 方法中添加模板的详细信息。
        在以下示例中，项目名称简写为 `dongtai_iast`：

        ```ruby
        ::Gitlab::ProjectTemplate.new('dongtai_iast', 'DongTai IAST', s_('JH|A demo project showing how DongTai IAST integrates into DevOps for detecting vulnerabilities'), 'https://jihulab.com/gitlab-cn/project-templates/dongtai-iast', 'illustrations/jh/logos/dongtai.svg')
        ```

        如果项目没有 SVG 图标，则不要包括 `, 'illustrations/jh/logos/dongtai.svg'`。

     - 在 `jh/spec/support/helpers/project_template_test_helper.rb` 中，在 `all_templates` 的方法中附加模板名称的简写。
     - 在 `jh/app/assets/javascripts/projects/default_project_templates.js` 中，添加模板的详细信息。例如：

        ```javascript
        dongtai_iast: {
          text: s__('JH|ProjectTemplates|DongTai IAST'),
          icon: '.template-option .icon-dongtai_iast',
        },
        ```

        如果项目没有 SVG 图标，使用 `.icon-gitlab_logo` 代替。

1. 提交所有更改并推送到分支上以更新合并请求。



## 改进现存模板

要更新现存内置项目模板：

1. 在 `project-templates` 和 `pages` 群组相关的项目中创建合并请求。
1. 在上述合并请求被接受后，您可以：
   - [创建议题](https://jihulab.com/gitlab-cn/gitlab/-/issues)以请求更新模板。
   - 或[创建一个包含项目模板详细信息的合并请求](#create-a-merge-request-with-vendor-details)以更新模版。
