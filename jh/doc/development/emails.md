---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 在开发中使用电子邮件

## 确保与邮件程序 Sidekiq 作业的兼容性

每当在 `ActionMailer` 上调用 `deliver_later` 时，Sidekiq 作业就会排队。
如果需要添加或删除邮件程序参数，确保向后和向前兼容很重要。<!--Adhere to the Sidekiq steps for
[changing the arguments for a worker](sidekiq/compatibility_across_updates.md#changing-the-arguments-for-a-worker)-->

新的邮件程序方法或新的邮件程序也同样适用。<!--If you introduce either,
follow the steps for [adding new workers](sidekiq/compatibility_across_updates.md#adding-new-workers).-->

在以下来自 [`NotificationService`](https://jihulab.com/gitlab-cn/gitlab/-/blob/33ccb22e4fc271dbaac94b003a7a1a2915a13441/app/services/notification_service.rb#L74) 的示例中，在此邮件程序的定义中添加或删除参数可能会在所有 Rails 和 Sidekiq 节点拥有更新的代码之前在部署期间引发问题。

```ruby
mailer.unknown_sign_in_email(user, ip, time).deliver_later
```

## 已发送电子邮件

要查看您的开发实例中"发送"的渲染电子邮件，请访问 [`/rails/letter_opener`](http://localhost:3000/rails/letter_opener)。

<!--[S/MIME signed](../administration/smime_signing_email.md) -->S/MIME 签名的电子邮件当前无法使用 `letter_opener` 预览。

## 邮件程序预览

Rails 提供了一种使用示例数据以 HTML 和纯文本形式预览邮件程序模板的方法。

预览位于 [`app/mailers/previews`](https://jihulab.com/gitlab-cn/gitlab-foss/tree/master/app/mailers/previews) 中，可以在 [`/rails/mailers`](http://localhost:3000/rails/mailers) 中查看。

有关更多信息，请参阅 [Rails 指南](https://guides.rubyonrails.org/action_mailer_basics.html#previewing-emails)。

## 收到的电子邮件

1. 进入极狐GitLab 安装目录。

2. 在 `config/gitlab.yml` 中找到 `incoming_email` 部分，启用该功能并填写您的特定 IMAP 服务器和电子邮件账户的详细信息：

   以下为 Gmail / Google Apps 的配置，假设邮箱为 `gitlab-incoming@gmail.com`：

   ```yaml
   incoming_email:
     enabled: true

     # The email address including the %{key} placeholder that will be replaced to reference the
     # item being replied to. This %{key} should be included in its entirety within the email
     # address and not replaced by another value.
     # For example: emailaddress+%{key}@gmail.com.
     # The placeholder must appear in the "user" part of the address (before the `@`). It can be omitted but some features,
     # including Service Desk, may not work properly.
     address: "gitlab-incoming+%{key}@gmail.com"

     # Email account username
     # With third party providers, this is usually the full email address.
     # With self-hosted email servers, this is usually the user part of the email address.
     user: "gitlab-incoming@gmail.com"
     # Email account password
     password: "[REDACTED]"

     # IMAP server host
     host: "imap.gmail.com"
     # IMAP server port
     port: 993
     # Whether the IMAP server uses SSL
     ssl: true
     # Whether the IMAP server uses StartTLS
     start_tls: false

     # The mailbox where incoming mail will end up. Usually "inbox".
     mailbox: "inbox"
     # The IDLE command timeout.
     idle_timeout: 60

     # Whether to expunge (permanently remove) messages from the mailbox when they are marked as deleted after delivery
     expunge_deleted: false
   ```

   如前所述，`+` 之后的部分将被忽略，并且此消息将发送到 `gitlab-incoming@gmail.com`。

1. 在确保安装了正确版本的 MailRoom 之前，请阅读 [MailRoom Gem 更新](#mailroom-gem-updates)了解更多详细信息。综上所述，您需要暂时将 `Gemfile` 中的 `gitlab-mail_room` 版本更新为最新的 `gitlab-mail_room` 并运行 `bundle install`。**不要提交**此更改，因为它是临时解决方法。

1. 在极狐GitLab 根目录中运行以下命令以启动 `mail_room`：

   ```shell
   bundle exec mail_room -q -c config/mail_room.yml
   ```

1. 验证所有配置是否正确：

   ```shell
   bundle exec rake gitlab:incoming_email:check RAILS_ENV=development
   ```

1. 现在应该可以通过电子邮件回复。

## 电子邮件命名空间

从极狐GitLab 13.10 开始，我们支持电子邮件处理程序地址的新格式。这样做是为了支持各种类型的邮箱。

如果您需要新电子邮件处理程序功能，请遵循以下电子邮件密钥格式规则：

- Action 总是在最后，用 `-` 分隔。例如 `-issue` 或 `-merge-request`。
- 如果您的功能与项目相关，则密钥以项目标识符开头（项目路径 slug 和项目 ID），用 `-` 分隔。例如，`gitlab-org-gitlab-foss-20`。
- 可以在项目标识符和 Action 之间添加其他信息，例如作者的令牌，用 `-` 分隔。例如，`gitlab-org-gitlab-foss-20-Author_Token12345678-issue`。
- 在 `lib/gitlab/email/handler.rb` 中注册处理程序。

有效电子邮件密钥示例：

- `gitlab-org-gitlab-foss-20-Author_Token12345678-issue`（创建新议题）
- `gitlab-org-gitlab-foss-20-Author_Token12345678-merge-request`（创建新合并请求）
- `1234567890abcdef1234567890abcdef-unsubscribe`（退订对话）
- `1234567890abcdef1234567890abcdef`（回复对话）

`-issue-` 在极狐GitLab 中用作服务台功能的处理程序。

### 旧格式

尽管我们继续支持旧格式，但任何新功能都不应使用旧格式。
以下是电子邮件处理程序唯一有效的旧格式：

- `path/to/project+namespace`
- `path/to/project+namespace+action`
- `namespace`
- `namespace+action`

在极狐GitLab 中，服务台功能的处理程序是 `path/to/project`。

<a id="mailroom-gem-updates"></a>

### MailRoom Gem 更新

我们使用 `gitlab-mail_room`<!--[`gitlab-mail_room`](https://gitlab.com/gitlab-org/gitlab-mail_room)-->，它是 [`MailRoom`](https://github.com/tpitale/mail_room/) 的派生，以确保我们可以在必要时快速更新 Gem。我们尝试尽快将更改上传到上游并让两个项目同步。

更新 MailRoom：

1. 在极狐GitLab Rails 中更新 `Gemfile`。
1. 更新 Helm Chart 配置。

---

[返回开发文档](index.md)
