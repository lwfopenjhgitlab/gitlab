---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
comments: false
description: Read through the GitLab installation methods.
type: index
---

# 安装方法

在以下可用安装方式中，根据您的平台选择安装极狐GitLab 的方式。

| 安装方式                                            | 描述 | 何时选择 |
|----------------------------------------------------------------|-------------|----------------|
| [Linux 安装包](https://docs.gitlab.cn/omnibus/installation/) | 官方的 deb/rpm 安装包（也被称作 Omnibus GitLab）包含极狐GitLab 和依赖的组件，包括 PostgreSQL、Redis 和 Sidekiq。 | 入门的推荐方式。Linux 安装包是成熟的和可扩展的。<!--，现在用于 jihulab.com。如果您需要额外的灵活性和弹性，我们建议您按照[参考架构文档](../administration/reference_architectures/index.md)中的说明部署极狐 GitLab。--> |
| [Helm charts](https://docs.gitlab.cn/charts/)                 | 用于在 Kubernetes 上安装极狐GitLab 及其所有组件的云原生 Helm chart。 | 在 Kubernetes 上安装极狐GitLab 时，您需要注意一些权衡：<br/>- 管理和故障排查需要 Kubernetes 知识。<br/>- 对于较小的安装，它可能会更昂贵。默认安装需要比单节点 Linux 包部署更多的资源，因为大多数服务都是以冗余方式部署的。<br/>- 有一些功能需要注意的限制。<!--[需要注意的限制](https://docs.gitlab.cn/charts/#limitations).--><br/><br/> 如果您的基础设施是基于 Kubernetes 构建的并且您熟悉它的工作原理，请使用此方法。管理方法、可观察性和一些概念与传统部署不同。 |
| [Docker](https://docs.gitlab.cn/jh/install/docker.html)              | Docker 容器化的极狐GitLab 软件包。 | 如果您熟悉 Docker，请使用此方法。 |

<!--
## Cloud providers

You can install GitLab on several cloud providers.

| Cloud provider                                                | Description |
|---------------------------------------------------------------|-------------|
| [AWS (HA)](aws/index.md)                                      | Install GitLab on AWS using the community AMIs provided by GitLab. |
| [Google Cloud Platform (GCP)](google_cloud_platform/index.md) | Install GitLab on a VM in GCP. |
| [Azure](azure/index.md)                                       | Install GitLab from Azure Marketplace. |
-->

### 不支持的 Linux 发行版和类 Unix 操作系统

- Arch Linux
- Fedora
- FreeBSD
- Gentoo
- macOS

对于极狐GitLab 安装于以上操作系统，存在可能性，但不提供支持。获取更多信息，请参考[安装指导](https://gitlab.cn/install/)。

<!--
See [OS versions that are no longer supported](../administration/package_information/supported_os.md#os-versions-that-are-no-longer-supported) for Omnibus installs page
for a list of supported and unsupported OS versions as well as the last support GitLab version for that OS.
-->

## Microsoft Windows

极狐GitLab 基于 Linux 操作系统开发，**不**支持在 Microsoft Windows 操作系统上运行，也没有在未来支持的计划，此时请考虑使用虚拟机运行极狐 GitLab。
