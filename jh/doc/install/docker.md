---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab Docker 镜像 **(FREE SELF)**

极狐GitLab Docker 镜像是极狐GitLab 的整体镜像，在单个容器中运行所有必要的服务。 

<!--
Find the GitLab official Docker image at:

- [GitLab Docker image in Docker Hub](https://hub.docker.com/r/gitlab/gitlab-ee/)
-->

Docker 镜像不包括邮件传输代理 (MTA)。推荐的解决方案是添加在单独容器中运行的 MTA（例如 Postfix 或 Sendmail）。作为另一种选择，您可以直接在极狐GitLab 容器中安装 MTA，但这会增加维护开销，因为您可能需要在每次升级或重新启动后重新安装 MTA。

<!--
In the following examples, if you want to use the latest RC image, use
`gitlab/gitlab-ee:rc` instead.
-->

您不应在 Kubernetes 中部署极狐GitLab Docker 镜像，因为它会造成单点故障。<!--If you want to deploy GitLab in Kubernetes, the
[GitLab Helm Chart](https://docs.gitlab.com/charts/) or [GitLab Operator](https://docs.gitlab.com/operator/)
should be used instead.-->

WARNING:
Docker for Windows 不受官方支持。存在卷权限的已知问题，以及潜在的其他未知问题。

<!--
If you are trying to run on Docker
for Windows, see the [getting help page](https://about.gitlab.com/get-help/) for links
to community resources (such as IRC or forums) to seek help from other users.
-->

## 先决条件

Docker 是必需的。查看[官方安装文档](https://docs.docker.com/get-docker/)。

<a id="set-up-the-volumes-location"></a>

## 设置卷位置

在设置其他所有内容之前，请配置一个新的环境变量 `$GITLAB_HOME`，指向配置、日志和数据文件所在的目录。
确保该目录存在并且已授予适当的权限。

对于 Linux 用户，将路径设置为 `/srv/gitlab`：

```shell
export GITLAB_HOME=/srv/gitlab
```

对于 macOS 用户，使用用户的 `$HOME/gitlab` 目录：

```shell
export GITLAB_HOME=$HOME/gitlab
```

`GITLAB_HOME` 环境变量应该附加到您的 shell 的配置文件中，以便它应用于所有未来的终端会话：

- Bash：`~/.bash_profile`
- ZSH：`~/.zshrc`

极狐GitLab 容器使用主机装载的卷来存储持久数据：

| 本地位置       | 容器位置 | 使用                                       |
|----------------------|--------------------|---------------------------------------------|
| `$GITLAB_HOME/data`  | `/var/opt/gitlab`  | 用于存储应用程序数据。               |
| `$GITLAB_HOME/logs`  | `/var/log/gitlab`  | 用于存储日志。                           |
| `$GITLAB_HOME/config`| `/etc/gitlab`      | 用于存储极狐GitLab 配置文件。 |

## 安装

极狐GitLab Docker 镜像可以通过多种方式运行：

- [使用 Docker Engine](#使用-docker-engine-安装极狐gitlab)
- [使用 Docker Compose](#使用-docker-compose-安装极狐gitlab)
- [使用 Docker swarm 模式](#使用-docker-swarm-模式安装极狐gitlab)

<a id="install-gitlab-using-docker-engine"></a>

### 使用 Docker Engine 安装极狐GitLab

您可以微调这些目录以满足您的要求。
一旦设置了 `GITLAB_HOME` 变量，您就可以运行镜像：

```shell
sudo docker run --detach \
  --hostname gitlab.example.com \
  --publish 443:443 --publish 80:80 --publish 22:22 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  --shm-size 256m \
  registry.gitlab.cn/omnibus/gitlab-jh:latest
```

这将下载并启动极狐GitLab 容器，并发布访问 SSH、HTTP 和 HTTPS 所需的端口。所有极狐GitLab 数据将存储在 `$GITLAB_HOME` 的子目录中。系统重启后，容器将自动 `restart`。

如果您使用的是 SELinux，请改为运行以下命令：

```shell
sudo docker run --detach \
  --hostname gitlab.example.com \
  --publish 443:443 --publish 80:80 --publish 22:22 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab:Z \
  --volume $GITLAB_HOME/logs:/var/log/gitlab:Z \
  --volume $GITLAB_HOME/data:/var/opt/gitlab:Z \
  --shm-size 256m \
  registry.gitlab.cn/omnibus/gitlab-jh:latest
```

这将确保 Docker 进程有足够的权限在挂载的卷中创建配置文件。

如果您使用 Kerberos 集成<!--[Kerberos 集成](../integration/kerberos.md)--> **(PREMIUM ONLY)**，您还必须发布您的 Kerberos 端口（例如，`--publish 8443:8443`）。否则会阻止使用 Kerberos 进行 Git 操作。

初始化过程可能需要很长时间。 您可以通过以下方式跟踪此过程：

```shell
sudo docker logs -f gitlab
```

启动容器后，您可以访问 `gitlab.example.com`（如果您在 macOS 上使用 boot2docker，则可以访问 `http://192.168.59.103`）。Docker 容器开始响应查询可能需要一段时间。

访问极狐GitLab URL，并使用用户名 root 和来自以下命令的密码登录：

```shell
sudo docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password
```

NOTE:
密码文件将在 24 小时后的第一次重新配置运行中自动删除。

<a id="install-gitlab-using-docker-compose"></a>

### 使用 Docker Compose 安装极狐GitLab

使用 [Docker Compose](https://docs.docker.com/compose/)，您可以轻松配置、安装和升级基于 Docker 的极狐GitLab 安装实例：

1. [安装 Docker Compose](https://docs.docker.com/compose/install/)。
1. 创建一个 `docker-compose.yml` 文件：

   ```yaml
   version: '3.6'
   services:
     web:
       image: 'registry.gitlab.cn/omnibus/gitlab-jh:latest'
       restart: always
       hostname: 'gitlab.example.com'
       environment:
         GITLAB_OMNIBUS_CONFIG: |
           external_url 'https://gitlab.example.com'
           # Add any other gitlab.rb configuration here, each on its own line
       ports:
         - '80:80'
         - '443:443'
         - '22:22'
       volumes:
         - '$GITLAB_HOME/config:/etc/gitlab'
         - '$GITLAB_HOME/logs:/var/log/gitlab'
         - '$GITLAB_HOME/data:/var/opt/gitlab'
       shm_size: '256m'
   ```

1. 确保您在与 `docker-compose.yml` 相同的目录下并启动极狐GitLab：

   ```shell
   docker compose up -d
   ```

NOTE:
阅读[预配置 Docker 容器](#pre-configure-docker-container)部分，了解 `GITLAB_OMNIBUS_CONFIG` 变量是如何工作的。

下面是另一个 `docker-compose.yml` 示例，其中极狐GitLab 在自定义 HTTP 和 SSH 端口上运行。注意 `GITLAB_OMNIBUS_CONFIG` 变量如何匹配 `ports` 部分：

```yaml
version: '3.6'
services:
  web:
    image: 'registry.gitlab.cn/omnibus/gitlab-jh:latest'
    restart: always
    hostname: 'gitlab.example.com'
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'http://gitlab.example.com:8929'
        gitlab_rails['gitlab_shell_ssh_port'] = 2224
    ports:
      - '8929:8929'
      - '2224:22'
    volumes:
      - '$GITLAB_HOME/config:/etc/gitlab'
      - '$GITLAB_HOME/logs:/var/log/gitlab'
      - '$GITLAB_HOME/data:/var/opt/gitlab'
    shm_size: '256m'
```

这与使用 `--publish 8929:8929 --publish 2224:22` 相同。

### 使用 Docker swarm 模式安装极狐GitLab

使用 [Docker swarm 模式](https://docs.docker.com/engine/swarm/)，您可以轻松地在 swarm 集群中配置和部署基于 Docker 的极狐GitLab 安装实例。

在 swarm 模式下，您可以利用 [Docker secret](https://docs.docker.com/engine/swarm/secrets/) 和 [Docker 配置](https://docs.docker.com/engine/swarm/configs/) 以高效安全地部署您的极狐GitLab 实例。
Secrets 可用于安全地传递您的初始 root 密码，而无需将其作为环境变量公开。
配置可以帮助您保持极狐GitLab 镜像尽可能通用。

这是一个使用 secret 和配置将具有四个 runner 的极狐GitLab 部署为 [stack](https://docs.docker.com/get-started/swarm-deploy/#describe-apps-using-stack-files) 的示例：

1. [设置 Docker swarm](https://docs.docker.com/engine/swarm/swarm-tutorial/)。
1. 创建一个 `docker-compose.yml` 文件：

   ```yaml
   version: "3.6"
   services:
     gitlab:
       image: registry.gitlab.cn/omnibus/gitlab-jh:latest
       ports:
         - "22:22"
         - "80:80"
         - "443:443"
       volumes:
         - $GITLAB_HOME/data:/var/opt/gitlab
         - $GITLAB_HOME/logs:/var/log/gitlab
         - $GITLAB_HOME/config:/etc/gitlab
       shm_size: '256m'
       environment:
         GITLAB_OMNIBUS_CONFIG: "from_file('/omnibus_config.rb')"
       configs:
         - source: gitlab
           target: /omnibus_config.rb
       secrets:
         - gitlab_root_password
     gitlab-runner:
       image: gitlab/gitlab-runner:alpine
       deploy:
         mode: replicated
         replicas: 4
   configs:
     gitlab:
       file: ./gitlab.rb
   secrets:
     gitlab_root_password:
       file: ./root_password.txt
   ```

   为简单起见，省略了 `network` 配置。
   更多信息可以参考官方的[撰写文件参考](https://docs.docker.com/compose/compose-file/)。

1. 创建 `gitlab.rb` 文件：

   ```ruby
   external_url 'https://my.domain.com/'
   gitlab_rails['initial_root_password'] = File.read('/run/secrets/gitlab_root_password').gsub("\n", "")
   ```

1. 创建 `root_password.txt` 文件：

   ```plaintext
   MySuperSecretAndSecurePassw0rd!
   ```

1. 确保您与 `docker-compose.yml` 位于同一目录中并运行：

   ```shell
   docker stack deploy --compose-file docker-compose.yml mystack
   ```

## 配置

容器使用官方的 Omnibus GitLab 包，所以所有的配置都在唯一的配置文件 `/etc/gitlab/gitlab.rb` 中完成。

要访问极狐GitLab 配置文件，您可以在正在运行的容器的上下文中启动 shell 会话。这将允许您浏览所有目录并使用您喜欢的文本编辑器：

```shell
sudo docker exec -it gitlab /bin/bash
```

您也可以只编辑`/etc/gitlab/gitlab.rb`：

```shell
sudo docker exec -it gitlab editor /etc/gitlab/gitlab.rb
```

打开 `/etc/gitlab/gitlab.rb` 后，请确保将 `external_url` 设置为指向有效 URL。

要从极狐GitLab 接收电子邮件，您必须配置 [SMTP 设置](https://docs.gitlab.cn/omnibus/settings/smtp.html)，因为极狐GitLab Docker 镜像没有安装 SMTP 服务器。您可能还对[启用 HTTPS](https://docs.gitlab.cn/omnibus/settings/ssl/index.html) 感兴趣。

完成所需的所有更改后，您需要重新启动容器以重新配置极狐GitLab：

```shell
sudo docker restart gitlab
```

每当容器启动时，极狐GitLab 都会重新配置自身。
有关配置极狐GitLab 的更多选项，请查看[配置文档](https://docs.gitlab.cn/omnibus/settings/configuration.html)。

<a id="pre-configure-docker-container"></a>

### 预配置 Docker 容器

您可以通过将环境变量 `GITLAB_OMNIBUS_CONFIG` 添加到 Docker 运行命令，来预配置 GitLab Docker 镜像。此变量可以包含任何 `gitlab.rb` 设置，并在加载容器的 `gitlab.rb` 文件之前进行评估。允许您配置外部 GitLab URL，并从 [Omnibus GitLab 模板](https://jihulab.com/gitlab-cn/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template)进行数据库配置或任何其他选项。
`GITLAB_OMNIBUS_CONFIG` 中包含的设置不会写入 `gitlab.rb` 配置文件，而是在加载时进行评估。

这是一个设置外部 URL 并在启动容器时启用 LFS 的示例：

```shell
sudo docker run --detach \
  --hostname gitlab.example.com \
  --env GITLAB_OMNIBUS_CONFIG="external_url 'http://my.domain.com/'; gitlab_rails['lfs_enabled'] = true;" \
  --publish 443:443 --publish 80:80 --publish 22:22 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  --shm-size 256m \
  registry.gitlab.cn/omnibus/gitlab-jh:latest
```

请注意，每次执行 `docker run` 命令时，都需要提供 `GITLAB_OMNIBUS_CONFIG` 选项。`GITLAB_OMNIBUS_CONFIG` 的内容在后续运行之间*不*保留。

<a id="use-tagged-versions-of-gitlab"></a>

### 使用标签版本的极狐GitLab

提供极狐GitLab Docker 镜像的标记版本。

<!--
To see all available tags see:

- [GitLab CE tags](https://hub.docker.com/r/gitlab/gitlab-ce/tags/)
- [GitLab EE tags](https://hub.docker.com/r/gitlab/gitlab-ee/tags/)
-->

要使用特定的标记版本，请将 `registry.gitlab.cn/omnibus/gitlab-jh:latest` 替换为您要运行的极狐GitLab 版本，例如 `registry.gitlab.cn/omnibus/gitlab-jh:14.5.0`。

### 在公共 IP 地址上运行极狐GitLab

您可以通过修改 `--publish` 标志让 Docker 使用您的 IP 地址并将所有流量转发到极狐GitLab 容器。

要在 IP `198.51.100.1` 上公开极狐GitLab：

```shell
sudo docker run --detach \
  --hostname gitlab.example.com \
  --publish 198.51.100.1:443:443 \
  --publish 198.51.100.1:80:80 \
  --publish 198.51.100.1:22:22 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  --shm-size 256m \
  registry.gitlab.cn/omnibus/gitlab-jh:latest
```

然后，您可以通过 `http://198.51.100.1/` 和 `https://198.51.100.1/` 访问您的极狐GitLab 实例。

### 在不同的端口上公开极狐GitLab

极狐GitLab 会占用容器内的某些端口<!--[某些端口](../administration/package_information/defaults.md)-->。

如果您想使用与 `80` (HTTP) 或 `443` (HTTPS) 不同的主机端口，需要在 `docker run` 命令中添加一个单独的 `--publish` 指令。

例如，要在主机的端口 `8929` 上公开 Web 界面，并在端口 `2289` 上公开 SSH 服务：

1. 使用以下 `docker run` 命令：

   ```shell
   sudo docker run --detach \
     --hostname gitlab.example.com \
     --publish 8929:8929 --publish 2289:22 \
     --name gitlab \
     --restart always \
     --volume $GITLAB_HOME/config:/etc/gitlab \
     --volume $GITLAB_HOME/logs:/var/log/gitlab \
     --volume $GITLAB_HOME/data:/var/opt/gitlab \
     --shm-size 256m \     
     registry.gitlab.cn/omnibus/gitlab-jh:latest
   ```

   NOTE:
   发布端口的格式是 `hostPort:containerPort`。在 Docker 的文档中阅读有关[公开传入端口](https://docs.docker.com/engine/reference/run/#/expose-incoming-ports)的更多信息。

1. 进入正在运行的容器：

   ```shell
   sudo docker exec -it gitlab /bin/bash
   ```

1. 用您的编辑器打开 `/etc/gitlab/gitlab.rb` 并设置 `external_url`：

   ```ruby
   # For HTTP
   external_url "http://gitlab.example.com:8929"

   or

   # For HTTPS (notice the https)
   external_url "https://gitlab.example.com:8929"
   ```

   此 URL 中指定的端口必须与 Docker 发布到主机的端口匹配。此外，如果 NGINX 监听端口没有在 `nginx['listen_port']` 中明确设置，它将从 `external_url` 中拉取。有关更多信息，请参阅 [NGINX 文档](https://docs.gitlab.cn/omnibus/settings/nginx.html)。

1. 设置 `gitlab_shell_ssh_port`：

   ```ruby
   gitlab_rails['gitlab_shell_ssh_port'] = 2289
   ```

1. 最后，重新配置极狐GitLab：

   ```shell
   gitlab-ctl reconfigure
   ```

按照上面的示例，您将能够从 Web 浏览器在 `<hostIP>:8929` 下访问极狐GitLab，并在端口 `2289` 下使用 SSH 进行推送。

可以在 [Docker compose](#install-gitlab-using-docker-compose) 部分找到使用不同端口的 `docker-compose.yml` 示例。

### 配置多个数据库连接

在 16.0 版本中，极狐GitLab 默认使用两个指向同一个 PostgreSQL 数据库的数据库连接。

如果出于任何原因您希望切换回单一数据库连接：

1. 编辑容器内的 `/etc/gitlab/gitlab.rb`：

   ```shell
   sudo docker exec -it gitlab editor /etc/gitlab/gitlab.rb
   ```

1. 添加以下行：

   ```ruby
   gitlab_rails['databases']['ci']['enable'] = false
   ```

1. 重启容器：

```shell
sudo docker restart gitlab
```

## 升级

在大多数情况下，升级极狐GitLab 就像下载最新的 Docker [标签镜像](#use-tagged-versions-of-gitlab)一样简单。

### 使用 Docker Engine 升级极狐GitLab

要升级[使用 Docker Engine 安装](#install-gitlab-using-docker-engine)的极狐GitLab：

1. 进行[备份](#back-up-gitlab)，作为最低要求，备份[数据库](#create-a-database-backup)和极狐GitLab secrets 文件。
1. 停止正在运行的容器：

   ```shell
   sudo docker stop gitlab
   ```

1. 移除现有容器：

   ```shell
   sudo docker rm gitlab
   ```

1. 拉取新镜像。例如，最新的极狐GitLab 镜像：

   ```shell
   sudo docker pull registry.gitlab.cn/omnibus/gitlab-jh:latest
   ```

1. 确保 `GITLAB_HOME` 环境变量是[已定义的](#set-up-the-volumes-location)：

   ```shell
   echo $GITLAB_HOME
   ```

1. 使用[先前指定的](#install-gitlab-using-docker-engine)选项再次创建容器：

   ```shell
   sudo docker run --detach \
   --hostname gitlab.example.com \
   --publish 443:443 --publish 80:80 --publish 22:22 \
   --name gitlab \
   --restart always \
   --volume $GITLAB_HOME/config:/etc/gitlab \
   --volume $GITLAB_HOME/logs:/var/log/gitlab \
   --volume $GITLAB_HOME/data:/var/opt/gitlab \
   --shm-size 256m \   
   registry.gitlab.cn/omnibus/gitlab-jh:latest
   ```

在第一次运行时，极狐GitLab 将重新配置并升级自身。

<!--
Refer to the GitLab [Update recommendations](../policy/maintenance.md#upgrade-recommendations)
when upgrading between major versions.
-->

### 使用 Docker compose 升级极狐GitLab

要升级[使用 Docker Compose 安装](#使用-docker-compose-安装极狐gitlab)的极狐GitLab：

1. 进行[备份](#备份极狐gitlab)，作为最低要求，备份[数据库](#create-a-database-backup)和极狐GitLab  secrets 文件。
1. 下载最新版本并升级您的极狐GitLab 实例：

   ```shell
   docker compose pull
   docker compose up -d
   ```

   如果您使用[标签](#使用标签版本的极狐gitlab)代替，则需要先编辑 `docker-compose.yml`。

<!--
### Convert Community Edition to Enterprise Edition

You can convert an existing Docker-based GitLab Community Edition (CE) container
to a GitLab [Enterprise Edition](https://about.gitlab.com/pricing/) (EE) container
using the same approach as [updating the version](#update).

We recommend you convert from the same version of CE to EE (for example, CE 14.1 to EE 14.1).
This is not explicitly necessary, and any standard upgrade (for example, CE 14.0 to EE 14.1) should work.
The following steps assume that you are upgrading the same version.

1. Take a [backup](#back-up-gitlab).
1. Stop the current CE container, and remove or rename it.
1. To create a new container with GitLab EE,
   replace `ce` with `ee` in your `docker run` command or `docker-compose.yml` file.
   However, reuse the CE container name, port and file mappings, and version.
-->

<!--
### Downgrade GitLab

To downgrade GitLab after an upgrade:

1. Follow the upgrade procedure, but [specify the tag for the original version of GitLab](#use-tagged-versions-of-gitlab)
   instead of `latest`.

1. Restore the [database backup you made](#create-a-database-backup) as part of the upgrade.

   - Restoring is required to back out database data and schema changes (migrations) made as part of the upgrade.
   - GitLab backups must be restored to the exact same version and edition.
   - [Follow the restore steps for Docker images](../raketasks/restore_gitlab.md#restore-for-docker-image-and-gitlab-helm-chart-installations), including
     stopping Puma and Sidekiq. Only the database must be restored, so add
     `SKIP=artifacts,repositories,registry,uploads,builds,pages,lfs,packages,terraform_state`
     to the `gitlab-backup restore` command line arguments.
-->

<a id="back-up-gitlab"></a>

## 备份极狐GitLab

您可以使用以下命令创建极狐GitLab 备份：

```shell
docker exec -t <container name> gitlab-backup create
```

<!--
Read more on how to [back up and restore GitLab](../raketasks/backup_restore.md).
-->

NOTE:
如果配置完全通过 `GITLAB_OMNIBUS_CONFIG` 环境变量提供（根据 ["预配置 Docker 容器"](#预配置-docker-容器) 步骤），则意味着没有直接在 `gitlab.rb` 配置文件中设置，则不需要备份 `gitlab.rb` 文件。

WARNING:
从备份中恢复极狐GitLab 时，需要备份极狐GitLab secrets 文件，可以避免复杂的步骤。Secrets 文件存储在容器内的 `/etc/gitlab/gitlab-secrets.json`，或容器主机上的 `$GITLAB_HOME/config/gitlab-secrets.json`。

<a id="create-a-database-backup"></a>

### 创建数据库备份

如果遇到问题，需要数据库备份来回滚极狐GitLab 升级。

```shell
docker exec -t <container name> gitlab-backup create SKIP=artifacts,repositories,registry,uploads,builds,pages,lfs,packages,terraform_state
```

备份被写入 `/var/opt/gitlab/backups`，它应该在 [Docker 挂载的卷](#set-up-the-volumes-location)上。

<!--
## Installing GitLab Community Edition

[GitLab CE Docker image](https://hub.docker.com/r/gitlab/gitlab-ce/)

To install the Community Edition, replace `ee` with `ce` in the commands on this
page.
-->

## 故障排查

如果您在使用 Omnibus GitLab 和 Docker 时遇到问题，以下信息将有所帮助。

### 诊断潜在问题

读取容器日志：

```shell
sudo docker logs gitlab
```

进入正在运行的容器：

```shell
sudo docker exec -it gitlab /bin/bash
```

从容器内，您可以像 [Omnibus 安装示例](https://jihulab.com/gitlab-cn/omnibus-gitlab/blob/master/README.md)一样管理极狐GitLab 容器。

### 500 Internal Error

更新 Docker 镜像时，您可能会遇到所有路径都显示 `500` 页面的问题。如果发生这种情况，请重新启动容器以尝试纠正问题：

```shell
sudo docker restart gitlab
```

### 权限问题

从旧的极狐GitLab Docker 镜像更新时，您可能会遇到权限问题。当之前镜像中的用户未正确保留时，就会发生这种情况。存在修复所有文件权限的脚本。

要修复您的容器，请执行 `update-permissions` 并在之后重新启动容器：

```shell
sudo docker exec gitlab update-permissions
sudo docker restart gitlab
```

### Windows/Mac: `Error executing action run on resource ruby_block[directory resource: /data/GitLab]`

在 Windows 或 Mac 上将 Docker Toolbox 与 VirtualBox 一起使用并使用 Docker 卷时，会发生此错误。`/c/Users` 卷挂载为 VirtualBox 共享文件夹，不支持所有 POSIX 文件系统功能。
不重新挂载就无法更改目录所有权和权限，并且极狐GitLab 失败。

我们的建议是切换到为您的平台使用本机 Docker 安装，而不是使用 Docker Toolbox。

如果您不能使用本机 Docker 安装（Windows 10 家庭版或 Windows 7/8），那么另一种解决方案是为 Docker 工具箱的 boot2docker 设置 NFS 挂载而不是 VirtualBox 共享。

### Linux ACL 问题

如果您在 Docker 主机上使用文件 ACL，`docker` 组需要对卷的完全访问权限才能让极狐GitLab 工作：

```shell
getfacl $GITLAB_HOME

# file: $GITLAB_HOME
# owner: XXXX
# group: XXXX
user::rwx
group::rwx
group:docker:rwx
mask::rwx
default:user::rwx
default:group::rwx
default:group:docker:rwx
default:mask::rwx
default:other::r-x
```

如果这些不正确，请将它们设置为：

```shell
sudo setfacl -mR default:group:docker:rwx $GITLAB_HOME
```

默认组是`docker`。如果您更改了组，请务必更新您的命令。

### `/dev/shm` mount 在 Docker 容器中没有足够的空间

极狐GitLab 在 `/-/metrics` 带有 Prometheus 指标端点，用于公开有关极狐GitLab 运行状况和性能的各种统计信息。为此所需的文件被写入临时文件系统（如 `/run` 或 `/dev/shm`）。

默认情况下，Docker 分配 64MB 给共享内存目录（挂载在`/dev/shm`）。这不足以保存所有生成的 Prometheus 指标相关文件，并将生成如下错误日志：

```plaintext
writing value to /dev/shm/gitlab/sidekiq/gauge_all_sidekiq_0-1.db failed with unmapped file
writing value to /dev/shm/gitlab/sidekiq/gauge_all_sidekiq_0-1.db failed with unmapped file
writing value to /dev/shm/gitlab/sidekiq/gauge_all_sidekiq_0-1.db failed with unmapped file
writing value to /dev/shm/gitlab/sidekiq/histogram_sidekiq_0-0.db failed with unmapped file
writing value to /dev/shm/gitlab/sidekiq/histogram_sidekiq_0-0.db failed with unmapped file
writing value to /dev/shm/gitlab/sidekiq/histogram_sidekiq_0-0.db failed with unmapped file
writing value to /dev/shm/gitlab/sidekiq/histogram_sidekiq_0-0.db failed with unmapped file
```

除了从管理页面禁用 Prometheus Metrics 之外，解决此问题的推荐解决方案是将共享内存的大小增加到至少 256MB。
如果使用 `docker run`，可以通过传递标志 `--shm-size 256m` 来完成。
如果使用 `docker-compose.yml` 文件，`shm_size` 键可以用于此目的。

### 由于 `json-file`，Docker 容器耗尽了空间

Docker的[默认日志驱动是`json-file`](https://docs.docker.com/config/containers/logging/configure/#configure-the-default-logging-driver)，默认不执行日志轮换。由于缺乏轮换，`json-file` 驱动程序存储的日志文件可能会为生成大量输出的容器消耗大量磁盘空间。这会导致磁盘空间耗尽。要解决此问题，请在可用时使用 [journald](https://docs.docker.com/config/containers/logging/journald/) 作为日志记录驱动程序，或[其他支持的驱动程序](https://docs.docker.com/config/containers/logging/configure/#supported-logging-drivers)与本地轮换支持。

### 启动 Docker 时出现缓冲区溢出错误

如果您收到此缓冲区溢出错误，您应该清除 `/var/log/gitlab` 中的旧日志文件：

```plaintext
buffer overflow detected : terminated
xargs: tail: terminated by signal 6
```

删除旧的日志文件有助于修复错误，并确保实例的干净启动。

### ThreadError 不允许操作，无法创建线程

```plaintext
can't create Thread: Operation not permitted
```

在[不支持新的 clone3 函数的主机](https://github.com/moby/moby/issues/42680)上，运行使用较新的 `glibc` 版本构建的容器时，会发生此错误。在 16.0 及更高版本中，容器镜像包含使用较新的 `glibc` 构建的 Ubuntu 22.04 Linux 软件包。

此问题已在 [Docker 20.10.10](https://github.com/moby/moby/pull/42836) 等较新的容器运行时工具得到解决。

要解决此问题，请将 Docker 更新到版本 20.10.10 或更高版本。
