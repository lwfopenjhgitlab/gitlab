---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
comments: false
description: Read through the GitLab installation methods.
type: index
---

# 安装 **(FREE SELF)**

您可以在大多数 GNU/Linux 发行版、多个云提供商和 Kubernetes 集群中安装极狐GitLab。

为了获得最佳体验，您应该平衡性能、可靠性、管理成本（备份、升级和故障排除）与托管成本。

要开始安装，首先[选择您的安装方法](install_methods.md)。

如果您已经有一个正在运行的实例，请了解如何[配置它](next_steps.md)。