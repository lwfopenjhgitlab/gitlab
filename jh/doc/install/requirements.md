---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 极狐GitLab 安装系统要求 **(FREE SELF)**

此页面包含有关安装和使用极狐GitLab 所需的最低要求的信息。

## 硬件要求

### 存储

<!--The necessary hard drive space largely depends on the size of the repositories you want to store in GitLab but as a *rule of thumb* you should have at least as much free space as all your repositories combined take up.

If you want to be flexible about growing your hard drive space in the future consider mounting it using [logical volume management (LVM)](https://en.wikipedia.org/wiki/Logical_volume_management) so you can add more hard drives when you need them.

Apart from a local hard drive you can also mount a volume that supports the network file system (NFS) protocol. This volume might be located on a file server, a network attached storage (NAS) device, a storage area network (SAN) or on an Amazon Web Services (AWS) Elastic Block Store (EBS) volume.

If you have enough RAM and a recent CPU the speed of GitLab is mainly limited by hard drive seek times. Having a fast drive (7200 RPM and up) or a solid state drive (SSD) improves the responsiveness of GitLab.

NOTE:
Since file system performance may affect the overall performance of GitLab,
[we don't recommend using cloud-based file systems for storage](../administration/nfs.md#avoid-using-cloud-based-file-systems).-->

必要的硬盘空间在很大程度上取决于您想在极狐GitLab 中存储的仓库的大小，但作为*准则*，您应该至少拥有与所有仓库组合占用的空间一样多的可用空间。

Omnibus GitLab 软件包需要大约 2.5 GB 的存储空间用于安装。

如果您想在未来灵活增加硬盘空间，请考虑使用逻辑卷管理（LVM）进行挂载，以便您在需要更多硬盘空间时进行添加。

除了本地硬盘，您也可以挂载支持网络文件系统（NFS）协议的卷。卷可能位于文件服务器、网络附加存储（NAS）设备、存储区域网络（SAN）或云提供商的弹性块存储卷上。

如果您有足够的 RAM 和较新的 CPU，极狐GitLab 的速度主要受硬盘驱动器寻道时间的限制。使用高转速（7200 转及以上）或固态驱动器（SSD），可提高极狐GitLab 的响应能力。

NOTE:
由于文件系统性能可能会影响极狐GitLab 的整体性能，我们不建议使用基于云的文件系统进行存储<!--[我们不建议使用基于云的文件系统进行存储](../administration/nfs.md#avoid-using-cloud-based-file-systems)-->。

NOTE:
Git 仓库的 NFS 存储已废弃。


### CPU

<!--CPU requirements are dependent on the number of users and expected workload. Your exact needs may be more, depending on your workload. Your workload is influenced by factors such as - but not limited to - how active your users are, how much automation you use, mirroring, and repository/change size.

The following is the recommended minimum CPU hardware guidance for a handful of example GitLab user base sizes.

- **4 cores** is the **recommended** minimum number of cores and supports up to 500 users
- 8 cores supports up to 1000 users
- More users? Consult the [reference architectures page](../administration/reference_architectures/index.md)-->

CPU 需求取决于用户数量和预期的工作负载，确切需求更多地取决于您的工作负载。您的工作负载受多重因素影响，不限于您的用户活跃程度、您使用的自动化程度、镜像、制品库大小和变更大小。

以下是针对部分用户数量群体，推荐的最低 CPU 硬件要求。

- **4 核** 是推荐的最小核数，支持多达 500 名用户
- 8 核支持多达 1000 名用户

<!--
- 需要支持更多用户？请查看[参考架构文档](../administration/reference_architectures/index.md)。
-->

<a id="memory"></a>

### 内存

<!--Memory requirements are dependent on the number of users and expected workload. Your exact needs may be more, depending on your workload. Your workload is influenced by factors such as - but not limited to - how active your users are, how much automation you use, mirroring, and repository/change size.

The following is the recommended minimum Memory hardware guidance for a handful of example GitLab user base sizes.

- **4GB RAM** is the **required** minimum memory size and supports up to 500 users
  - Our [Memory Team](https://about.gitlab.com/handbook/engineering/development/enablement/memory/) is working to reduce the memory requirement.
- 8GB RAM supports up to 1000 users
- More users? Consult the [reference architectures page](../administration/reference_architectures/index.md)

In addition to the above, we generally recommend having at least 2GB of swap on your server,
even if you currently have enough available RAM. Having swap helps to reduce the chance of errors occurring
if your available memory changes. We also recommend configuring the kernel's swappiness setting
to a low value like `10` to make the most of your RAM while still having the swap
available when needed.-->

内存需求取决于用户数量和预期的工作负载，确切需求更多地取决于您的工作负载。您的工作负载受多重因素影响，不限于您的用户活跃程度、您使用的自动化程度、镜像、制品库大小和变更大小。

以下是针对部分用户数量群体，推荐的最低内存硬件要求。

- **4GB RAM** 是**必需的**最小内存，支持多达 500 名用户
- 8GB RAM 支持多达 1000 名用户

<!--
- 需要支持更多用户？请查看[参考架构文档](../administration/reference_architectures/index.md)。
-->

除了上述需求之外，我们通常建议您的服务器上至少有 2GB 的 swap 存储空间，即使您已有足够可用的 RAM。如果您的可用内存发生变化，swap 可帮助您减小错误发生的概率。我们也建议您将内核的 swappiness 设置为低值，例如 10，在充分利用 RAM 的同时，使 swap 在需要时可用。

NOTE:
尽管不期望出现会降低性能的过大的 swapping，但它是针对内存不足情况的极其重要的最后手段。在出现意外的系统负载期间，例如同一主机上的操作系统更新或其他服务，峰值内存负载峰值可能远高于平均水平。拥有大量 swapping 有助于避免 Linux OOM killer 不安全地终止潜在的关键进程，例如 PostgreSQL，这可能会产生灾难性的后果。

## 数据库

<!--PostgreSQL is the only supported database, which is bundled with the Omnibus GitLab package.
You can also use an [external PostgreSQL database](https://docs.gitlab.com/omnibus/settings/database.html#using-a-non-packaged-postgresql-database-management-server).
Support for MySQL was removed in GitLab 12.1. Existing users using GitLab with
MySQL/MariaDB are advised to [migrate to PostgreSQL](../update/mysql_to_postgresql.md) before upgrading.-->

PostgreSQL 是唯一支持的数据库，捆绑在 Omnibus GitLab 软件包中。您也可以使用[外部 PostgreSQL 数据库](https://docs.gitlab.cn/omnibus/settings/database.html#使用非打包的-postgresql-数据库管理服务器)。

<a id="postgresql-requirements"></a>

### PostgreSQL 要求

<!--
The server running PostgreSQL should have _at least_ 5-10 GB of storage
available, though the exact requirements [depend on the number of users](../administration/reference_architectures/index.md).

We highly recommend using the minimum PostgreSQL versions (as specified in
the following table) as these were used for development and testing:

| GitLab version | Minimum PostgreSQL version |
|----------------|----------------------------|
| 10.0           | 9.6                        |
| 13.0           | 11                         |

You must also ensure the following extensions are loaded into every
GitLab database. [Read more about this requirement, and troubleshooting](postgresql_extensions.md).

| Extension    | Minimum GitLab version |
| ------------ | ---------------------- |
| `pg_trgm`    | 8.6                    |
| `btree_gist` | 13.1                   |
| `plpgsql`    | 11.7                   |
-->

运行 PostgreSQL 的服务器应至少具有 5-10 GB的存储空间，具体需求根据用户数量决定<!--[根据用户数量决定](../administration/reference_architectures/index.md)-->。

我们强烈建议使用下表所述的最小 PostgreSQL 版本，已经过开发和测试验证。

| 极狐GitLab 版本 | 最小 PostgreSQL 版本 |
|----------------|----------------------------|
| 13.0           | 11                         |
| 14.0           | 12.7                         |
| 15.0           | 12.10                       |
| 16.0  | 13.6                       |

您也必须确保下表所述的扩展已加载到每个 GitLab 数据库。<!--[获取更多关于该需求的信息，以及故障排查](postgresql_extensions.md)。-->

| 扩展   | 最小 GitLab 版本 |
| ------------ | ---------------------- |
| `pg_trgm`    | 8.6                    |
| `btree_gist` | 13.1                   |
| `plpgsql`    | 11.7                   |

<!--
NOTE:
Support for [PostgreSQL 9.6 and 10 was removed in GitLab 13.0](https://about.gitlab.com/releases/2020/05/22/gitlab-13-0-released/#postgresql-11-is-now-the-minimum-required-version-to-install-gitlab) so that GitLab can benefit from PostgreSQL 11 improvements, such as partitioning. For the schedule of transitioning to PostgreSQL 12, see [the related epic](https://gitlab.com/groups/gitlab-org/-/epics/2184).
-->

已知以下托管 PostgreSQL 服务不兼容，不应使用：

| 版本 | 托管服务                                       |
|----------------|-------------------------------------------------------|
| 14.4+          | Amazon Aurora |

#### 使用 GitLab Geo 的额外需求

<!--
If you're using [GitLab Geo](../administration/geo/index.md), we strongly
recommend running Omnibus GitLab-managed instances, as we actively develop and
test based on those. We try to be compatible with most external (not managed by
Omnibus GitLab) databases (for example, [AWS Relational Database Service (RDS)](https://aws.amazon.com/rds/)),
but we can't guarantee compatibility.
-->

如果您使用 GitLab Geo<!--[GitLab Geo](../administration/geo/index.md)-->，我们强烈建议您运行由 Omnibus GitLab 管理的数据库实例，我们的开发和测试也基于该类型的实例。我们尝试与大多数外部数据库兼容（例如 [AWS Relational Database Service (RDS)](https://aws.amazon.com/rds/)），但我们不能保证兼容性。

#### 操作系统语言环境兼容性和静默索引损坏

`glibc` 中语言环境数据的更改意味着 PostgreSQL 数据库文件在不同操作系统版本之间不完全兼容。

为避免索引损坏，在以下情况，检查语言环境兼容性：

- 在服务器之间移动二进制 PostgreSQL 数据。
- 升级您的 Linux 发行版。
- 更新或更改第三方容器镜像。

#### Gitaly 集群数据库需求

[在 Gitaly 集群文档中阅读更多内容](../administration/gitaly/praefect.md)。

#### 独立使用 GitLab 数据库

<!--
Databases created or used for GitLab, Geo, Gitaly Cluster, or other components should be for the
exclusive use of GitLab. Do not make direct changes to the database, schemas, users, or other
properties except when following procedures in the GitLab documentation or following the directions
of GitLab Support or other GitLab engineers.

- The main GitLab application currently uses three schemas:

  - The default `public` schema
  - `gitlab_partitions_static` (automatically created)
  - `gitlab_partitions_dynamic` (automatically created)

  No other schemas should be manually created.

- GitLab may create new schemas as part of Rails database migrations. This happens when performing
  a GitLab upgrade. The GitLab database account requires access to do this.

- GitLab creates and modifies tables during the upgrade process, and also as part of normal
  operations to manage partitioned tables.

- You should not modify the GitLab schema (for example, adding triggers or modifying tables).
  Database migrations are tested against the schema definition in the GitLab code base. GitLab
  version upgrades may fail if the schema is modified.
-->

为极狐GitLab、Geo、Gitaly 集群或其他组件创建或使用的数据库，应该作为极狐GitLab 的专属独立使用。除非遵循官方文档或技术支持的指示，不要直接更改数据库、schema、用户或其它属性。

* 极狐GitLab 主应用目前使用三个 schema：

	* 默认的 `public` schema
	* `gitlab_partitions_static`（自动创建）
	* `gitlab_partitions_dynamic`（自动创建）

	不应手动创建其它 schema。
	
* 作为 Rails 数据库迁移的一部分，极狐GitLab 主应用可能会创建新的 schema。当进行 GitLab 升级时，会发生这种情况。GitLab 数据库账户需要访问权限，才能执行此操作。

* 在升级过程中，会创建和修改表，是一种管理分区表的正常操作。

* 您不应修改 GitLab schema（例如添加触发器或修改表）。数据库迁移已基于现有代码库中的 schema 定义进行了测试。如果 schema 被修改，版本升级可能会失败。
	
<a id="puma-settings"></a>
	
## Puma 设置

Puma 的推荐设置取决于它运行的基础设施。Linux 安装包默认为推荐的 Puma 设置。无论安装方式为何，您都可以调整 Puma 设置。

如果您正在使用 Linux 安装包，查看 [Puma 设置](https://docs.gitlab.cn/jh/administration/operations/puma.html)文档，获取更改 Puma 设置的说明。

如果您正在使用 Helm chart，查看 [`webservice` chart](https://docs.gitlab.cn/charts/charts/gitlab/webservice/index.html)。

### Puma workers

<!--
The recommended number of workers is calculated as the highest of the following:

- `2`
- Number of CPU cores - 1

For example a node with 4 cores should be configured with 3 Puma workers.

You can increase the number of Puma workers, providing enough CPU and memory capacity is available.
A higher number of Puma workers usually helps to reduce the response time of the application
and increase the ability to handle parallel requests. You must perform testing to verify the
optimal settings for your infrastructure.
-->

推荐的 workers 数量应取以下两者之间的最大值：

- `2`
- CPU 和内存资源可用性的组合（查看如何为 [Linux 安装包](https://jihulab.com/gitlab-cn/omnibus-gitlab/-/blob/ef9facdc927e7389db6a5e0655414ba8318c7b8a/files/gitlab-cookbooks/gitlab/libraries/puma.rb#L31-46)自动配置）。

以下述场景为例：

- 具有 2 个内核/8 GB 内存的节点应配置 **2 个 Puma worker**。

  计算方法为：

  ```plaintext
  The highest number from
  2
  And
  [
  the lowest number from
    - number of cores: 2
    - memory limit: (8 - 1.5) = 6
  ]
  ```

  因此，2 和 2 中的最高值是 2。

- 一个 4 核/4 GB 内存的节点应该配置 **2 个 Puma worker**。

  ```plaintext
  The highest number from
  2
  And
  [
  the lowest number from
    - number of cores: 4
    - memory limit: (4 - 1.5) = 2.5
  ]
  ```

 因此，2 和 2 中的最高值是 2。

- 一个 4 核/8 GB 内存的节点应该配置 **4 个 Puma worker**。

  ```plaintext
  The highest number from
  2
  And
  [
  the lowest number from
    - number of cores: 4
    - memory limit: (8 - 1.5) = 6.5
  ]
  ```

  因此，2 和 4 中的最高值是 4。

您可以增加 Puma workers 的数量，提供充足的 CPU 和内存容量。更多的 Puma workers 通常有助于减少应用的响应时间，提高处理并行请求的能力。您必须进行测试，以验证基础架构的最佳设置。

### Puma threads

<!--
The recommended number of threads is dependent on several factors, including total memory, and use
of [legacy Rugged code](../administration/gitaly/index.md#direct-access-to-git-in-gitlab).

- If the operating system has a maximum 2 GB of memory, the recommended number of threads is `1`.
  A higher value results in excess swapping, and decrease performance.
- If legacy Rugged code is in use, the recommended number of threads is `1`.
- In all other cases, the recommended number of threads is `4`. We don't recommend setting this
higher, due to how [Ruby MRI multi-threading](https://en.wikipedia.org/wiki/Global_interpreter_lock)
works.
-->

推荐的 threads 数量取决于多重因素，包括总内存，和 legacy Rugged 代码<!--[legacy Rugged 代码](../administration/gitaly/index.md#direct-access-to-git-in-gitlab)-->。

* 如果操作系统最多有 2 GB 内存，推荐的 threads 数量为 `1`。较高的值会导致过度交换并降低性能。
* 如果 legacy Rugged 代码正在使用，推荐的 threads 数量为 `1`。
* 在所有其它情况下，推荐的 threads 数量为 `4`。我们不推荐设置更高的数值。

### 每个 Puma worker 的最大内存

默认情况下，每个 Puma worker 将被限制为 1.2 GB 的内存。
此设置可以调整，如果您需要增加 Puma Worker 的数量应该考虑。

## Redis

Redis 存储所有用户会话和后台任务队列。
对 Redis 的要求如下：

- 16.0 及更高版本需要 Redis 6.0。
- 不支持 Redis 集群模式。必须使用 Redis Standalone。
- Redis 的存储要求极低，平均每个用户约为 25 kB。

## Sidekiq

Sidekiq 使用多线程进程处理后台作业。
该进程从整个 Rails 堆栈（200MB+）开始，随着时间推移，会由于内存泄漏而不断增长。
在非常活跃的服务器（10000 个计费用户）上，Sidekiq 进程可以使用 1GB 以上的内存。

## Prometheus and its exporters

[Prometheus](https://prometheus.io) 及其相关 exporters 默认开启，开启对极狐GitLab 的深度监控。在默认设置下，这些进程消耗大约 200 MB 的内存。

<!--
如果您想禁用 Prometheus 和关联的 exporters，或想阅读更多相关信息，请查看 [Prometheus 文档](../administration/monitoring/prometheus/index.md)。-->

## GitLab Runner

<!--
We strongly advise against installing GitLab Runner on the same machine you plan
to install GitLab on. Depending on how you decide to configure GitLab Runner and
what tools you use to exercise your application in the CI environment, GitLab
Runner can consume significant amount of available memory.

Memory consumption calculations, that are available above, won't be valid if
you decide to run GitLab Runner and the GitLab Rails application on the same
machine.

It's also not safe to install everything on a single machine, because of the
[security reasons](https://docs.gitlab.com/runner/security/), especially when you plan to use shell executor with GitLab
Runner.

We recommend using a separate machine for each GitLab Runner, if you plan to
use the CI features.
The GitLab Runner server requirements depend on:

- The type of [executor](https://docs.gitlab.com/runner/executors/) you configured on GitLab Runner.
- Resources required to run build jobs.
- Job concurrency settings.

Since the nature of the jobs varies for each use case, you need to experiment by adjusting the job concurrency to get the optimum setting.

For reference, the GitLab.com Build Cloud [auto-scaling runner for Linux](../ci/runners/build_cloud/linux_build_cloud.md) is configured so that a **single job** runs in a **single instance** with:

- 1 vCPU.
- 3.75 GB of RAM.
-->

我们强烈建议您，不要在计划安装极狐GitLab 的机器上，同时安装 GitLab Runner。GitLab Runner 会消耗大量内存，取决于您决定如何配置 GitLab Runner，以及在 CI 环境中调试应用所使用的的工具。

如果您决定在同一台机器上运行 GitLab Runner 和 GitLab Rails 应用程序，上方计算的内存消耗值无效。

由于安全原因<!--[安全原因](https://docs.gitlab.cn/runner/security/)-->，在一台机器上安装所有组件并不安全，尤其是当您计划使用 shell executor 时。

如果您计划使用 CI 特性，我们建议每个 GitLab Runner 使用单独的机器。GitLab Runner 服务器的需求依赖以下因素：

* 您在 GitLab Runner 中配置的 executor<!--[executor](https://docs.gitlab.cn/runner/executors/)--> 类型。
* 运行 build job 所需的资源。
* Job 的并发设置。

由于每个 Job 都不同，您需要试验调整 Job 的并发性，以获得最佳设置。

<a id="supported-web-browsers"></a>

## 支持的 Web 浏览器

<!--
WARNING:
With GitLab 13.0 (May 2020) we have removed official support for Internet Explorer 11.

GitLab supports the following web browsers:

- [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/new/)
- [Google Chrome](https://www.google.com/chrome/)
- [Chromium](https://www.chromium.org/getting-involved/dev-channel)
- [Apple Safari](https://www.apple.com/safari/)
- [Microsoft Edge](https://www.microsoft.com/en-us/edge)

For the listed web browsers, GitLab supports:

- The current and previous major versions of browsers.
- The current minor version of a supported major version.

NOTE:
We don't support running GitLab with JavaScript disabled in the browser and have no plans of supporting that
in the future because we have features such as Issue Boards which require JavaScript extensively.
-->

WARNING:
我们已移除对 Internet Explorer 11 的官方支持。

极狐GitLab 支持以下 Web 浏览器：

- [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/new/)
- [Google Chrome](https://www.google.com/chrome/)
- [Chromium](https://www.chromium.org/getting-involved/dev-channel)
- [Apple Safari](https://www.apple.com/safari/)
- [Microsoft Edge](https://www.microsoft.com/en-us/edge?form=MA13FJ)

对于以上列出的 Web 浏览器，极狐GitLab 支持：

* 当前和以前的主要浏览器版本。
* 支持的主要版本的当前小版本。

NOTE:
我们不支持在浏览器中禁用 JavaScript 的情况下运行极狐GitLab，在未来也没有计划支持，因为我们有例如议题看板等广泛需要 JavaScript 的功能。

## 安全

安装后，请务必阅读并遵循[维护安全的极狐GitLab 安装](../security/index.md)的指导。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
