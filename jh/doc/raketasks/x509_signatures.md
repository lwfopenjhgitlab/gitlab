---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# X.509 签名 **(FREE SELF)**

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/122159)-->引入于极狐GitLab 12.10。

当[使用 X.509 签署提交](../user/project/repository/x509_signed_commits/index.md)时，信任锚点可能会改变，存储在数据库中的签名一定会被更新。

## 更新所有 X.509 签名

这个任务循环遍历所有 X.509 签署的提交，并基于当前证书存储库更新验证。

更新所有 X.509 签名，请运行：

**Omnibus 安装：**

```shell
sudo gitlab-rake gitlab:x509:update_signatures
```

**源代码安装：**

```shell
sudo -u git -H bundle exec rake gitlab:x509:update_signatures RAILS_ENV=production
```
