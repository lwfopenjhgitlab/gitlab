---
stage: Monitor
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 生成示例 Prometheus 数据 **(FREE SELF)**

此命令在一系列时间间隔内针对特定环境的每个指标运行 Prometheus 查询：

- 30 分钟
- 3 小时
- 8 小时
- 24 小时
- 72 小时
- 7 天

每个查询的结果作为由指标的 `identifier` 命名的 YAML 文件存储在 `sample_metrics` 目录下。
当设置完环境变量 `USE_SAMPLE_METRICS` 后，
Prometheus API 查询被重新路由到 `Projects::Environments::SampleMetricsController`，
如果它在 `sample_metrics` 目录中，那么它会加载相应的数据集。

此命令需要安装了可用 Prometheus 的环境的 ID。

## 示例

以下示例展示如何运行 Rake 任务：

```shell
bundle exec rake gitlab:generate_sample_prometheus_data[21]
```

