---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 用户管理 **(FREE SELF)**

极狐GitLab 提供 Rake 任务用于用户管理。

## 将用户添加为所有项目的开发者

将用户添加为所有项目的开发者，请运行：

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:import:user_to_projects[username@domain.tld]

# installation from source
bundle exec rake gitlab:import:user_to_projects[username@domain.tld] RAILS_ENV=production
```

## 向所有项目添加所有用户

向所有项目添加所有用户，请运行：

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:import:all_users_to_all_projects

# installation from source
bundle exec rake gitlab:import:all_users_to_all_projects RAILS_ENV=production
```

管理员被添加为维护者。

## 将用户添加为所有群组的开发者

将用户添加为所有群组的开发者，请运行：

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:import:user_to_groups[username@domain.tld]

# installation from source
bundle exec rake gitlab:import:user_to_groups[username@domain.tld] RAILS_ENV=production
```

## 向所有群组添加所有用户

向所有群组添加所有用户，请运行：

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:import:all_users_to_all_groups

# installation from source
bundle exec rake gitlab:import:all_users_to_all_groups RAILS_ENV=production
```

管理员被添加为所有者，这样他们就能够向群组添加其他成员。

## 在给定群组中将所有用户更新为 `project_limit:0` 和 `can_create_group: false`

在给定群组中将所有用户更新为 `project_limit:0` 和 `can_create_group: false`，请运行：

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:user_management:disable_project_and_group_creation\[:group_id\]

# installation from source
bundle exec rake gitlab:user_management:disable_project_and_group_creation\[:group_id\] RAILS_ENV=production
```

根据给定的限制，更新给定群组、子群组和群组命名空间中的项目中的所有用户。

## 控制计费用户的数量

如果启用了该项设置，会控制新用户的创建，除非管理员清除这个选项。
默认为 `false`：

```plaintext
block_auto_created_users: false
```

## 为所有用户禁用双重身份验证

这个任务禁用用户的双重身份验证（2FA）设置。 
当极狐GitLab `config/secrets.yml` 文件丢失且用户无法登录时，这很有用。

为所有用户禁用双重身份验证，请运行：

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:two_factor:disable_for_all_users

# installation from source
bundle exec rake gitlab:two_factor:disable_for_all_users RAILS_ENV=production
```

## 轮换双重身份验证加密密钥

极狐GitLab 在加密的数据库列中存储双重身份验证（2FA）所需的 Secret 数据。
这个数据的加密密钥是 `otp_key_base`，并且存储在 `config/secrets.yml` 中。

如果文件发生泄漏，但是单独的 2FA Secret 没有泄漏，可以使用加密密钥重新加密这些 Secret。这允许您更改泄露的密钥，无需强制所有用户更改 2FA 详细信息。

轮换双重身份验证加密密钥：

1. 在 `config/secrets.yml` 文件中查询旧的密钥，但是**请确保您运行在生产环境中**。您所感兴趣的行如下所示：

   ```yaml
   production:
     otp_key_base: fffffffffffffffffffffffffffffffffffffffffffffff
   ```

1. 生成新的 Secret：

   ```shell
   # omnibus-gitlab
   sudo gitlab-rake secret

   # installation from source
   bundle exec rake secret RAILS_ENV=production
   ```

1. 停止极狐GitLab 服务器，备份现存 Secret 文件，更新数据库：

   ```shell
   # omnibus-gitlab
   sudo gitlab-ctl stop
   sudo cp config/secrets.yml config/secrets.yml.bak
   sudo gitlab-rake gitlab:two_factor:rotate_key:apply filename=backup.csv old_key=<old key> new_key=<new key>

   # installation from source
   sudo /etc/init.d/gitlab stop
   cp config/secrets.yml config/secrets.yml.bak
   bundle exec rake gitlab:two_factor:rotate_key:apply filename=backup.csv old_key=<old key> new_key=<new key> RAILS_ENV=production
   ```

   `<old key>` 值可以从 `config/secrets.yml`（`<new key>` 是之前生成的）中读取。用户 2FA Secret 的**加密的**值被写到特定的 `filename` 中。如果发生错误，您可以使用它进行回滚。

1. 更改 `config/secrets.yml`，将 `otp_key_base` 设置为 `<new key>` 并重启。 
   请确保您运行在**生产**环境。

   ```shell
   # omnibus-gitlab
   sudo gitlab-ctl start

   # installation from source
   sudo /etc/init.d/gitlab start
   ```

如果发生问题（例如 `old_key` 使用了错误的值），您可以恢复您的 `config/secrets.yml` 备份并回滚更改：

```shell
# omnibus-gitlab
sudo gitlab-ctl stop
sudo gitlab-rake gitlab:two_factor:rotate_key:rollback filename=backup.csv
sudo cp config/secrets.yml.bak config/secrets.yml
sudo gitlab-ctl start

# installation from source
sudo /etc/init.d/gitlab start
bundle exec rake gitlab:two_factor:rotate_key:rollback filename=backup.csv RAILS_ENV=production
cp config/secrets.yml.bak config/secrets.yml
sudo /etc/init.d/gitlab start

```

## 相关主题

- [重置用户密码](../security/reset_user_password.md#use-a-rake-task)
