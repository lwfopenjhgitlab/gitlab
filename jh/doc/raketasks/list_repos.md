---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 列举仓库目录 **(FREE SELF)**

您可以打印磁盘上所有由极狐GitLab 管理的 Git 仓库列表。

打印列表，您需要运行以下命令：

```shell
# Omnibus
sudo gitlab-rake gitlab:list_repos

# Source
cd /home/git/gitlab
sudo -u git -H bundle exec rake gitlab:list_repos RAILS_ENV=production
```

结果使用极狐GitLab Rails 应用程序的默认顺序。

## 限制搜索结果

要仅列出最近活动的项目，请使用 `SINCE` 环境变量传递日期。
您指定的时间由 Rails [`TimeZone#parse` 函数](https://api.rubyonrails.org/classes/ActiveSupport/TimeZone.html#method-i-parse)解析。

```shell
# Omnibus
sudo gitlab-rake gitlab:list_repos SINCE='Sep 1 2015'

# Source
cd /home/git/gitlab
sudo -u git -H bundle exec rake gitlab:list_repos RAILS_ENV=production SINCE='Sep 1 2015'
```

