---
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 迁移到版本化代码片段 **(FREE SELF)**

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/215861)-->引入于极狐GitLab 13.0。

在极狐GitLab 13.0 中，[极狐GitLab 代码片段由 Git 仓库支持](../user/snippets.md#versioned-snippets)。
代码片段内容存储在仓库中，用户可以直接通过 Git 进行更新。

尽管如此，现有的极狐GitLab 代码片段必须迁移到这个新功能。
对于每个代码片段：


- 创建一个新的仓库。
- 使用代码片段文件名在仓库中创建一个文件。
- 将代码片段提交到仓库。

  当极狐GitLab 实例升级到 13.0 或更高版本时，极狐GitLab 通过后台迁移<!--[后台迁移](../development/database/background_migrations.md)-->执行此迁移。
  但是，如果任何代码片段迁移失败，则必须单独迁移它们。
  以下 Rake 任务有助于该过程。

## 向 Git 迁移指定代码片段

如果您想迁移一系列代码片段，请按如下所述运行任务。

对于 Omnibus 安装，请运行：

```shell
sudo gitlab-rake gitlab:snippets:migrate SNIPPET_IDS=1,2,3,4
```

对于源代码安装，请运行：

```shell
bundle exec rake gitlab:snippets:migrate SNIPPET_IDS=1,2,3,4
```

迁移过程中支持的 ID 数量有默认限制 (100)。您可以使用环境变量 `LIMIT` 来修改此限制。

```shell
sudo gitlab-rake gitlab:snippets:migrate SNIPPET_IDS=1,2,3,4 LIMIT=50
```

对于源代码安装，请运行：

```shell
bundle exec rake gitlab:snippets:migrate SNIPPET_IDS=1,2,3,4 LIMIT=50
```

## 显示是否运行代码片段后台迁移

如果您想检查代码片段后台迁移的状态，即其是否正在运行，您都可以使用以下任务。

对于 Omnibus 安装，请运行：

```shell
sudo gitlab-rake gitlab:snippets:migration_status
```

对于源代码安装，请运行：

```shell
bundle exec rake gitlab:snippets:migration_status RAILS_ENV=production
```

## 列举非迁移代码片段

通过以下任务，您可以获得所有尚未迁移或迁移失败的代码片段的 ID。

对于 Omnibus 安装，请运行：

```shell
sudo gitlab-rake gitlab:snippets:list_non_migrated
```

对于源代码安装，请运行：

```shell
bundle exec rake gitlab:snippets:list_non_migrated RAILS_ENV=production
```

由于未迁移代码片段的数量可能很大，我们默认限制返回 100 个 ID。您可以使用环境变量 `LIMIT` 修改此限制。

```shell
sudo gitlab-rake gitlab:snippets:list_non_migrated LIMIT=200
```

对于源代码安装，请运行：

```shell
bundle exec rake gitlab:snippets:list_non_migrated RAILS_ENV=production LIMIT=200
```

