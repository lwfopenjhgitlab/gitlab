---
redirect_to: 'index.md'
remove_date: '2022-05-24'
---

本文档移动到了[其他位置](index.md)。

<!-- This redirect file can be deleted after <2022-05-24>. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/#move-or-rename-a-page -->

