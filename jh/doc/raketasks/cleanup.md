---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 清理 **(FREE SELF)**

极狐GitLab 提供 Rake 任务以清理极狐GitLab 实例。

## 移除未引用的 LFS 文件

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/36628)-->引入于极狐GitLab 12.10。

WARNING:
为完成所有后台迁移，不要在极狐GitLab 升级的 12 个小时内运行，否则会导致数据丢失。

当您从仓库历史移除 LFS 文件，它们会变成孤立文件，继续消耗磁盘空间。
通过这个 Rake 任务，您可以从数据库移除不活跃的引用，从而允许 LFS 文件的垃圾回收。

例如：

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:cleanup:orphan_lfs_file_references PROJECT_PATH="gitlab-org/gitlab-foss"

# installation from source
bundle exec rake gitlab:cleanup:orphan_lfs_file_references RAILS_ENV=production PROJECT_PATH="gitlab-org/gitlab-foss"
```

您也可以使用 `PROJECT_ID` 而不是 `PROJECT_PATH` 来指定项目。
例如：

```shell
$ sudo gitlab-rake gitlab:cleanup:orphan_lfs_file_references PROJECT_ID="13083"

I, [2019-12-13T16:35:31.764962 #82356]  INFO -- :  Looking for orphan LFS files for project GitLab Org / GitLab Foss
I, [2019-12-13T16:35:31.923659 #82356]  INFO -- :  Removed invalid references: 12
```

默认情况下，这个任务不会删除任何东西，只会展示它能够删除的文件引用的数量。如果您想删除引用，请使用 `DRY_RUN=false` 运行命令。
您也可以使用 `LIMIT={number}` 参数限制删除的引用的数量。

请注意，这个 Rake 仅将引用移除到 LFS 文件。未引用的 LFS 文件稍后将进行垃圾回收（一天一次）。
如果您需要立即进行，请运行 `rake gitlab:cleanup:orphan_lfs_files`。

### 立即移除未引用的 LFS 文件

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/36628)-->引入于极狐GitLab 12.10。

未引用的 LFS 文件会被每天移除一次，您也可以立即移除。例如：

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:cleanup:orphan_lfs_files

# installation from source
bundle exec rake gitlab:cleanup:orphan_lfs_files
```

示例输出：

```shell
$ sudo gitlab-rake gitlab:cleanup:orphan_lfs_files
I, [2020-01-08T20:51:17.148765 #43765]  INFO -- : Removed unreferenced LFS files: 12
```

## 清理项目上传文件

清理不在极狐GitLab 数据库中的项目上传文件。

### 从文件系统清理项目上传文件

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/20863)-->引入于极狐GitLab 11.2。

您可以清理不在极狐GitLab 数据库中的本地项目上传文件。
如果无法找到其项目，任务会试图修复文件，否则它会将文件移动到 lost+found 目录中。

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:cleanup:project_uploads

# installation from source
bundle exec rake gitlab:cleanup:project_uploads RAILS_ENV=production
```

示例输出：

```shell
$ sudo gitlab-rake gitlab:cleanup:project_uploads

I, [2018-07-27T12:08:27.671559 #89817]  INFO -- : Looking for orphaned project uploads to clean up. Dry run...
D, [2018-07-27T12:08:28.293568 #89817] DEBUG -- : Processing batch of 500 project upload file paths, starting with /opt/gitlab/embedded/service/gitlab-rails/public/uploads/test.out
I, [2018-07-27T12:08:28.689869 #89817]  INFO -- : Can move to lost and found /opt/gitlab/embedded/service/gitlab-rails/public/uploads/test.out -> /opt/gitlab/embedded/service/gitlab-rails/public/uploads/-/project-lost-found/test.out
I, [2018-07-27T12:08:28.755624 #89817]  INFO -- : Can fix /opt/gitlab/embedded/service/gitlab-rails/public/uploads/foo/bar/89a0f7b0b97008a4a18cedccfdcd93fb/foo.txt -> /opt/gitlab/embedded/service/gitlab-rails/public/uploads/qux/foo/bar/89a0f7b0b97008a4a18cedccfdcd93fb/foo.txt
I, [2018-07-27T12:08:28.760257 #89817]  INFO -- : Can move to lost and found /opt/gitlab/embedded/service/gitlab-rails/public/uploads/foo/bar/1dd6f0f7eefd2acc4c2233f89a0f7b0b/image.png -> /opt/gitlab/embedded/service/gitlab-rails/public/uploads/-/project-lost-found/foo/bar/1dd6f0f7eefd2acc4c2233f89a0f7b0b/image.png
I, [2018-07-27T12:08:28.764470 #89817]  INFO -- : To cleanup these files run this command with DRY_RUN=false

$ sudo gitlab-rake gitlab:cleanup:project_uploads DRY_RUN=false
I, [2018-07-27T12:08:32.944414 #89936]  INFO -- : Looking for orphaned project uploads to clean up...
D, [2018-07-27T12:08:33.293568 #89817] DEBUG -- : Processing batch of 500 project upload file paths, starting with /opt/gitlab/embedded/service/gitlab-rails/public/uploads/test.out
I, [2018-07-27T12:08:33.689869 #89817]  INFO -- : Did move to lost and found /opt/gitlab/embedded/service/gitlab-rails/public/uploads/test.out -> /opt/gitlab/embedded/service/gitlab-rails/public/uploads/-/project-lost-found/test.out
I, [2018-07-27T12:08:33.755624 #89817]  INFO -- : Did fix /opt/gitlab/embedded/service/gitlab-rails/public/uploads/foo/bar/89a0f7b0b97008a4a18cedccfdcd93fb/foo.txt -> /opt/gitlab/embedded/service/gitlab-rails/public/uploads/qux/foo/bar/89a0f7b0b97008a4a18cedccfdcd93fb/foo.txt
I, [2018-07-27T12:08:33.760257 #89817]  INFO -- : Did move to lost and found /opt/gitlab/embedded/service/gitlab-rails/public/uploads/foo/bar/1dd6f0f7eefd2acc4c2233f89a0f7b0b/image.png -> /opt/gitlab/embedded/service/gitlab-rails/public/uploads/-/project-lost-found/foo/bar/1dd6f0f7eefd2acc4c2233f89a0f7b0b/image.png
```

### 从对象存储清理项目上传文件

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/20918)-->引入于极狐GitLab 11.2。

将不在极狐GitLab 数据库中的对象上传文件移动到 lost+found 目录中。

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:cleanup:remote_upload_files

# installation from source
bundle exec rake gitlab:cleanup:remote_upload_files RAILS_ENV=production
```

示例输出：

```shell
$ sudo gitlab-rake gitlab:cleanup:remote_upload_files

I, [2018-08-02T10:26:13.995978 #45011]  INFO -- : Looking for orphaned remote uploads to remove. Dry run...
I, [2018-08-02T10:26:14.120400 #45011]  INFO -- : Can be moved to lost and found: @hashed/6b/DSC_6152.JPG
I, [2018-08-02T10:26:14.120482 #45011]  INFO -- : Can be moved to lost and found: @hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451/711491b29d3eb08837798c4909e2aa4d/DSC00314.jpg
I, [2018-08-02T10:26:14.120634 #45011]  INFO -- : To cleanup these files run this command with DRY_RUN=false
```

```shell
$ sudo gitlab-rake gitlab:cleanup:remote_upload_files DRY_RUN=false

I, [2018-08-02T10:26:47.598424 #45087]  INFO -- : Looking for orphaned remote uploads to remove...
I, [2018-08-02T10:26:47.753131 #45087]  INFO -- : Moved to lost and found: @hashed/6b/DSC_6152.JPG -> lost_and_found/@hashed/6b/DSC_6152.JPG
I, [2018-08-02T10:26:47.764356 #45087]  INFO -- : Moved to lost and found: @hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451/711491b29d3eb08837798c4909e2aa4d/DSC00314.jpg -> lost_and_found/@hashed/79/02/7902699be42c8a8e46fbbb4501726517e86b22c56a189f7625a6da49081b2451/711491b29d3eb08837798c4909e2aa4d/DSC00314.jpg
```

<a id="remove-orphan-artifact-files"></a>

## 移除 Orphan 产物文件

> - <!--[Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/29681)-->引入于极狐GitLab 12.1。
> - <!--[`ionice` support fixed](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28023)-->`ionice` 支持修复于极狐GitLab 12.10。

<!--
NOTE:
这些命令对存储在[对象存储](../administration/object_storage.md)中的产物不起作用。
-->

当您发现磁盘上有多余的作业产物文件和/或目录，您可以运行：

```shell
sudo gitlab-rake gitlab:cleanup:orphan_job_artifact_files
```

这个命令：

- 扫描整个产物文件夹。
- 检查哪个文件在数据库中还有记录。
- 如果没有发现数据库记录，文件和目录会从磁盘中删除。

默认情况下，这个任务不会删除任何内容，仅展示它可以删除的内容。
如果您想删除文件，您可以使用 `DRY_RUN=false` 运行命令：

```shell
sudo gitlab-rake gitlab:cleanup:orphan_job_artifact_files DRY_RUN=false
```

您可以使用 `LIMIT`（默认为 `100`）限制删除的文件的数量：

```shell
sudo gitlab-rake gitlab:cleanup:orphan_job_artifact_files LIMIT=100
```

如果运行上面的命令，则最多会从磁盘删除 100 个文件。您可以使用它删除少量文件，用作测试。

假设 `DEBUG=1` 展示每个被检测为孤立文件的文件的完整路径。

如果安装了 `ionice`，任务使用它确保命令不会在磁盘上引发太多负载。您可以使用 `NICENESS` 配置优先级别。
以下是有效级别，您也可以使用 `man 1 ionice` 进行确定。

- `0` 或 `None`
- `1` 或 `Realtime`
- `2` 或 `Best-effort`（默认）
- `3` 或 `Idle`

## 移除过期的 ActiveSession 查找密钥

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/30668)-->引入于极狐GitLab 12.2。

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:cleanup:sessions:active_sessions_lookup_keys

# installation from source
bundle exec rake gitlab:cleanup:sessions:active_sessions_lookup_keys RAILS_ENV=production
```

## 容器镜像库垃圾回收

容器镜像库会占用很多磁盘空间。
为清理未使用的层，镜像库包括[垃圾回收命令](../administration/packages/container_registry.md#container-registry-garbage-collection)。
