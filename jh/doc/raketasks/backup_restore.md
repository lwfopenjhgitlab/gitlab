---
stage: Enablement
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 备份和恢复极狐GitLab **(FREE SELF)**

极狐GitLab 提供了用于备份和恢复极狐GitLab 实例的 Rake 任务。

应用程序数据备份会创建一个包含数据库、所有仓库和所有附件的归档文件。

恢复备份与创建备份的极狐GitLab 必须拥有**完全相同的版本和类型**。
[将项目从一台服务器迁移到另一台服务器](#migrate-to-a-new-server)的最佳方式是备份和恢复。

WARNING:
极狐GitLab 不会备份未存储在文件系统上的项目。如有需要，且您使用对象存储<!--[对象存储](../administration/object_storage.md)-->，一定要使用您的对象存储提供者启用备份。

## 要求

为了能够进行备份和恢复，请确保您系统已安装 Rsync。
如果您安装了极狐GitLab：

- *如果您使用 Omnibus 软件包*，则无需额外操作。
- *如果您使用源代码安装*，您需要确定是否安装了 `rsync`。例如：

  ```shell
  # Debian/Ubuntu
  sudo apt-get install rsync

  # RHEL/CentOS
  sudo yum install rsync
  ```

<a id="backup-timestamp"></a>

## 备份时间戳

备份归档保存在 `backup_path` 中，该路径在
`config/gitlab.yml` 文件中指定。文件名是 `[TIMESTAMP]_gitlab_backup.tar`，
其中 `TIMESTAMP` 标识每个备份的创建时间和极狐GitLab 的版本。恢复极狐GitLab 需要时间戳，也可以使用多个备份。

例如，如果备份名称是 `1493107454_2018_04_25_10.6.4-ce_gitlab_backup.tar`，时间戳为 `1493107454_2018_04_25_10.6.4-ce`。

<a id="back-up-gitlab"></a>

## 备份极狐GitLab

极狐GitLab 提供了一个命令行界面来备份您的整个实例，包含：

- 数据库
- 附件
- Git 仓库数据
- CI/CD 作业输出日志
- CI/CD 作业产物
- LFS 对象
- Terraform 状态（<!--[introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/331806)-->引入于极狐GitLab 14.7）
- 容器镜像库镜像
- 极狐GitLab 页面内容
- 包（<!--[introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/332006)-->引入于极狐GitLab 14.7）
- 代码片段
- 群组 Wiki<!--[群组 Wiki](../user/project/wiki/group.md)-->

备份不包括：

- [Mattermost 数据](https://docs.mattermost.com/administration/config-settings.html#file-storage)
- Redis（和 Sidekiq 作业）

WARNING:
极狐GitLab 不备份任何配置文件（`/etc/gitlab`）、TLS 密钥和证书或系统文件。强烈建议您阅读有关[存储配置文件](#storing-configuration-files)的信息。

WARNING:
当您的安装使用 PgBouncer，无论是出于性能原因还是将其与 Patroni 集群一起使用，备份命令都需要[附加参数](#back-up-and-restore-for-installations-using-pgbouncer)。
根据您的极狐GitLab 版本，如果您已使用 Omnibus 软件包安装了极狐GitLab，请使用以下命令：

- 极狐GitLab 12.2 及更高版本：

  ```shell
  sudo gitlab-backup create
  ```

- 极狐GitLab 12.1 及更早版本：

  ```shell
  gitlab-rake gitlab:backup:create
  ```

如果您使用源代码安装极狐GitLab，请使用以下命令：

```shell
sudo -u git -H bundle exec rake gitlab:backup:create RAILS_ENV=production
```

如果您在 Docker 容器中运行极狐GitLab，请基于您安装的极狐GitLab 版本从主机运行备份：

- 极狐GitLab 12.2 及更高版本：

  ```shell
  docker exec -t <container name> gitlab-backup create
  ```

- 极狐GitLab 12.1 及更早版本：

  ```shell
  docker exec -t <container name> gitlab-rake gitlab:backup:create
  ```

如果您在 Kubernetes 集群上使用[极狐GitLab Helm Chart](https://jihulab.com/gitlab-cn/charts/gitlab)，
您可以通过使用 `kubectl` 运行极狐GitLab 工具箱 Pod 上的 `backup-utility` 脚本来运行备份任务。
详情请参见[图表备份文档](https://docs.gitlab.cn/charts/backup-restore/backup.html)。

与 Kubernetes 案例类似，如果您已将极狐GitLab 集群扩展到使用多个应用服务器，您应该选择一个指定的节点（不是弹性伸缩的方式) 用于运行备份 Rake 任务。因为备份 Rake
任务与主 Rails 应用程序紧密耦合，通常是您运行 Puma 或 Sidekiq 的节点。

示例输出：

```plaintext
Dumping database tables:
- Dumping table events... [DONE]
- Dumping table issues... [DONE]
- Dumping table keys... [DONE]
- Dumping table merge_requests... [DONE]
- Dumping table milestones... [DONE]
- Dumping table namespaces... [DONE]
- Dumping table notes... [DONE]
- Dumping table projects... [DONE]
- Dumping table protected_branches... [DONE]
- Dumping table schema_migrations... [DONE]
- Dumping table services... [DONE]
- Dumping table snippets... [DONE]
- Dumping table taggings... [DONE]
- Dumping table tags... [DONE]
- Dumping table users... [DONE]
- Dumping table users_projects... [DONE]
- Dumping table web_hooks... [DONE]
- Dumping table wikis... [DONE]
Dumping repositories:
- Dumping repository abcd... [DONE]
Creating backup archive: $TIMESTAMP_gitlab_backup.tar [DONE]
Deleting tmp directories...[DONE]
Deleting old backups... [SKIPPING]
```

<a id="storing-configuration-files"></a>

### 存储配置文件

极狐GitLab 提供的[备份 Rake 任务](#back-up-gitlab)*不会*存储您的配置文件。主要原因是您的数据库包含用于双重身份验证的加密信息和 CI/CD *安全变量*在内的条目。
将加密信息作为密钥存储在同一位置破坏了加密的目的。

WARNING:
Secret 文件对于保存数据库加密密钥至关重要。

您必须**至少**备份：

对于 Omnibus：

- `/etc/gitlab/gitlab-secrets.json`
- `/etc/gitlab/gitlab.rb`

对于源代码安装：

- `/home/git/gitlab/config/secrets.yml`
- `/home/git/gitlab/config/gitlab.yml`

对于 Docker 安装<!-- [Docker 安装](https://docs.gitlab.cn/omnibus/docker/)-->，您必须备份存储配置文件的卷。如果您根据文档创建了极狐GitLab 容器，它应该在 `/srv/gitlab/config` 目录中。

对于在 Kubernetes 集群上进行[极狐GitLab Helm Chart 安装](https://jihulab.com/gitlab-cn/charts/gitlab)，
您必须遵循[备份 Secret](https://docs.gitlab.cn/charts/backup-restore/backup.html#backup-the-secrets) 的要求。

如果您必须执行完整的机器恢复，您可能还需要备份 TLS 密钥和证书（`/etc/gitlab/ssl`、`/etc/gitlab/trusted-certs`），以及您的
[SSH 主机密钥](https://superuser.com/questions/532040/copy-ssh-keys-from-one-server-to-another-server/532079#532079)，
以避免中间人攻击警告。

如果您使用 Omnibus GitLab，请查看其他信息以[备份您的配置](https://docs.gitlab.cn/omnibus/settings/backups.html)。

万一 Secret 文件丢失，请参见[故障排查部分](#when-the-secrets-file-is-lost)。

### 备份选项

极狐GitLab 提供的用于备份实例的命令行工具可以接受更多选项。

#### 备份策略选项

默认备份策略本质上使用 Linux 命令 `tar` 和 `gzip` 将数据从各自的数据位置传输到备份。
在大多数情况下都没有问题，但是当数据快速变化时可能会引发问题。

当 `tar` 正在读取数据时数据发生更改，可能会发生错误 `file changed as we read it`，
并导致备份失败。为了解决这个问题，8.17
引入了一种称为 `copy` 的新备份策略。策略在调用 `tar` 和 `gzip` 之前将数据文件复制到临时位置，
避免发生错误。

副作用是备份过程需要额外的 1X 磁盘空间。该过程尽最大努力清理每个阶段的临时文件，所以问题不会复杂化，但对于大型安装来说可能是一个相当大的变化。
这就是 `copy` 策略在 8.17 中不是默认策略的原因。

要使用 `copy` 策略而不是默认的流式传输策略，请在 Rake 任务命令中指定 `STRATEGY=copy`。
例如：

```shell
sudo gitlab-backup create STRATEGY=copy
```

极狐GitLab 12.1 及更早版本的用户应该使用 `gitlab-rake gitlab:backup:create`。

<a id="backup-filename"></a>

#### 备份文件名

WARNING:
如果您使用自定义备份文件名，您不能[限制备份的有效期](#limit-backup-lifetime-for-local-files-prune-old-backups)。

默认情况下，会根据[备份时间戳](#backup-timestamp)中的规范创建备份文件。
但是，您可以通过设置 `BACKUP` 环境变量覆盖文件名的 `[TIMESTAMP]` 部分。
例如：

```shell
sudo gitlab-backup create BACKUP=dump
```

极狐GitLab 12.1 及更早版本的用户应该用 `gitlab-rake gitlab:backup:create`。

生成的文件名为 `dump_gitlab_backup.tar`。这对使用 rsync 和增量备份的系统很有用，并会带来相当快的传输速度。

#### 确定可以传输归档

为确保生成的归档可以通过 rsync 传输，您可以设置 `GZIP_RSYNCABLE=yes`
选项。这会将 `--rsyncable` 选项设置为 `gzip`，这仅在结合设置[备份文件名选项](#backup-filename)时有用。

请注意，我们不保证 `gzip` 中的 `--rsyncable` 选项在所有发行版上都可用。
要验证它在您的发行版中是否可用，请运行
`gzip --help` 或查阅手册。

```shell
sudo gitlab-backup create BACKUP=dump GZIP_RSYNCABLE=yes
```

极狐GitLab 12.1 及更早版本的用户应该使用 `gitlab-rake gitlab:backup:create`。

<a id="excluding-specific-directories-from-the-backup"></a>

#### 从备份中排除特定目录

您可以通过添加环境变量 `SKIP` 从备份中排除特定目录，其值是以下选项以逗号分隔的列表：

- `db`（数据库）
- `uploads`（附件）
- `builds`（CI 作业输出日志）
- `artifacts`（CI 作业产物）
- `lfs`（LFS 对象）
- `terraform_state`（Terraform 状态）
- `registry`（容器数据库镜像）
- `pages`（Pages 内容）
- `repositories`（Git 仓库数据）
- `packages`（软件包）

所有 Wiki 都作为 `repositories` 组的一部分进行备份。备份期间将跳过不存在的 Wiki。

NOTE:
在[备份和恢复 Helm Chart](https://docs.gitlab.cn/charts/architecture/backup-restore.html) 时，多了一个额外选项 `packages`，指的是极狐GitLab 包镜像库<!--[包镜像库](../user/packages/package_registry/index.md)-->管理的任何包。
详情请参见[命令行参数](https://docs.gitlab.cn/charts/architecture/backup-restore.html#command-line-arguments)。

所有 Wiki 都作为 `repositories` 组的一部分进行备份。不存在的 Wiki
会在备份期间跳过。

对于 Omnibus GitLab 软件包：

```shell
sudo gitlab-backup create SKIP=db,uploads
```

极狐GitLab 12.1 及更早版本的用户应该使用 `gitlab-rake gitlab:backup:create`。

对于源代码安装：

```shell
sudo -u git -H bundle exec rake gitlab:backup:create SKIP=db,uploads RAILS_ENV=production
```

<a id="skipping-tar-creation"></a>

#### 跳过 Tar 创建

创建备份的最后一部分是生成一个包含所有部分的 `.tar` 文件。在某些情况下（例如，如果备份被其他备份软件获取），创建 `.tar` 文件可能会浪费精力，甚至有害。因此您可以通过将 `tar` 添加到 `SKIP` 环境变量来跳过此步骤。

将 `tar` 添加到 `SKIP` 变量会将包含备份的文件和目录留在用于中间文件的目录中。
创建新备份时，这些文件会被覆盖，因此您应该确保别处有备份，因为您在系统上只能有一个备份。

对于 Omnibus GitLab 软件包：

```shell
sudo gitlab-backup create SKIP=tar
```

对于源代码安装：

```shell
sudo -u git -H bundle exec rake gitlab:backup:create SKIP=tar RAILS_ENV=production
```

#### 在恢复中禁用提示

从备份进行还原时，还原脚本可能会在还原之前要求确认。
如果您想禁用这些提示，您可以将 `GITLAB_ASSUME_YES` 设置为 `1`。

对于 Omnibus GitLab 软件包：

```shell
sudo GITLAB_ASSUME_YES=1 gitlab-backup restore
```

对于源代码安装：

```shell
sudo -u git -H GITLAB_ASSUME_YES=1 bundle exec rake gitlab:backup:restore RAILS_ENV=production
```

#### 并发备份 Git 仓库

> - <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/37158)-->引入于极狐GitLab 13.3。
> - <!--[Concurrent restore introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/69330)-->并发恢复引入于极狐GitLab 14.3。

使用多个仓库存储<!--[多个仓库存储](../administration/repository_storage_paths.md)-->时，
可以并发备份或恢复仓库，以帮助充分利用 CPU 时间。
以下变量可用于修改 Rake 任务的默认行为：

- `GITLAB_BACKUP_MAX_CONCURRENCY`：。
  默认为逻辑 CPU 的数量（在极狐GitLab 14.1 及更早版本，默认为 `1`）。
- `GITLAB_BACKUP_MAX_STORAGE_CONCURRENCY`：每个存储上同时要备份的最大项目数。
  允许在存储中传播仓库备份。
  默认为 `2`（在极狐 GitLab 14.1 及更早版本中，
  默认为 `1`）。

例如，对于带有 4 个仓库存储的 Omnibus GitLab 安装：

```shell
sudo gitlab-backup create GITLAB_BACKUP_MAX_CONCURRENCY=4 GITLAB_BACKUP_MAX_STORAGE_CONCURRENCY=1
```

例如，对于源代码安装：

```shell
sudo -u git -H bundle exec rake gitlab:backup:create GITLAB_BACKUP_MAX_CONCURRENCY=4 GITLAB_BACKUP_MAX_STORAGE_CONCURRENCY=1
```

#### 增量仓库备份

> - 引入于极狐GitLab 14.9，<!--[标签](../administration/feature_flags.md) -->标签为 `incremental_repository_backup`。默认禁用。
> - <!--[Enabled on self-managed](https://gitlab.com/gitlab-org/gitlab/-/issues/355945)-->启用于私有化部署版本于极狐GitLab 14.10。
> - <!--`PREVIOUS_BACKUP` option [introduced](https://gitlab.com/gitlab-org/gitaly/-/issues/4184)-->`PREVIOUS_BACKUP` 选项引入于极狐GitLab 15.0。

标记：
在私有化部署的极狐GitLab 上，默认情况下此功能可用。要隐藏该功能，请让管理员禁用名为 `incremental_repository_backup` 的功能标志<!--[禁用名为 `incremental_repository_backup` 的功能标志](../administration/feature_flags.md)-->。
在 jihulab.com 上，此功能不可用。

增量备份比完整备份更快，因为它们仅将上次备份之后的更改打包到备份包中。
必须有一个现有备份才能从以下位置创建增量备份：

- 在极狐GitLab 14.9 和 14.10 中，使用 `BACKUP=<timestamp_of_backup>` 选项选择要使用的备份。选中的之前的备份会被覆盖。
- 在极狐GitLab 15.0 及更高版本中，请使用 `PREVIOUS_BACKUP=<timestamp_of_backup>` 选项选择要使用的备份。默认情况下，
  按照[备份时间戳](#backup-timestamp)中的规定创建备份文件。您可以通过设置 [`BACKUP` 环境变量](#backup-filename)覆盖文件名的 `[TIMESTAMP]` 部分。

创建增量备份，请运行：

```shell
sudo gitlab-backup create INCREMENTAL=yes PREVIOUS_BACKUP=<timestamp_of_backup>
```

也可以使用 `SKIP=tar` 从[未解压备份](#skipping-tar-creation)创建增量备份：

```shell
sudo gitlab-backup create INCREMENTAL=yes SKIP=tar
```

#### 备份特定仓库存储

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/86896)-->引入于极狐GitLab 15.0。

使用[多个仓库存储](../administration/repository_storage_paths.md)时，
可以使用 `REPOSITORIES_STORAGES` 选项单独备份特定仓库存储的仓库。
该选项接受以逗号分隔的存储名称列表。

例如，对于 Omnibus GitLab 安装：

```shell
sudo gitlab-backup create REPOSITORIES_STORAGES=storage1,storage2
```

例如，对于源代码安装：

```shell
sudo -u git -H bundle exec rake gitlab:backup:create REPOSITORIES_STORAGES=storage1,storage2
```

<a id="uploading-backups-to-a-remote-cloud-storage"></a>

#### 上传备份到远端（云）存储

您可以让备份脚本上传（使用[Fog 库](http://fog.io/)）
它创建的 `.tar` 文件。在以下示例中，我们使用 Amazon S3
进行存储，但 Fog 还允许您使用[其他存储提供者](http://fog.io/storage/)。
极狐GitLab 还为 AWS、谷歌、OpenStack Swift、Rackspace 和阿里云[导入云驱动程序](https://jihulab.com/gitlab-cn/gitlab/-/blob/da46c9655962df7d49caef0e2b9f6bbe88462a02/Gemfile#L113)。
本地驱动程序[也可用](#uploading-to-locally-mounted-shares)。

<!--[阅读更多通过极狐GitLab 使用对象存储的内容](../administration/object_storage.md)。-->

##### 使用 Amazon S3

对于 Omnibus GitLab 软件包：

1. 将以下内容添加到 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['backup_upload_connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-west-1',
     'aws_access_key_id' => 'AKIAKIAKI',
     'aws_secret_access_key' => 'secret123'
     # If using an IAM Profile, don't configure aws_access_key_id & aws_secret_access_key
     # 'use_iam_profile' => true
   }
   gitlab_rails['backup_upload_remote_directory'] = 'my.s3.bucket'
   ```

1. <!--[重新配置极狐GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)-->
   重新配置极狐GitLab 以使更改生效。

##### S3 加密桶

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/64765)-->引入于极狐GitLab 14.3。

AWS 支持这些[服务器端加密的模式](https://docs.aws.amazon.com/AmazonS3/latest/userguide/serv-side-encryption.html)：

- Amazon S3 托管密钥 (SSE-S3)
- 存储在 AWS 密钥管理服务(SSE-KMS)中的客户主密钥（CMK）
- 客户提供的密钥 (SSE-C)

在极狐GitLab 中使用您选择的模式。每种模式的配置方法都有相似之处，但略有不同。

###### SSE-S3

要启用 SSE-S3，请在备份存储选项中将 `server_side_encryption` 设置为 `AES256`。例如，在 Omnibus GitLab 中：

```ruby
gitlab_rails['backup_upload_storage_options'] = {
  'server_side_encryption' => 'AES256'
}
```

###### SSE-KMS

要启用 SSE-KMS，您需要[通过亚马逊资源名称（ARN）的 `arn:aws:kms:region:acct-id:key/key-id` 格式的 KMS 密钥](https://docs.aws.amazon.com/AmazonS3/latest/userguide/UsingKMSEncryption.html)。在 `backup_upload_storage_options` 配置下，请：

- 将 `server_side_encryption` 设置为 `aws:kms`。
- 将 `server_side_encryption_kms_key_id` 设置为密钥的 ARN。

例如，在 Omnibus GitLab 中：

```ruby
gitlab_rails['backup_upload_storage_options'] = {
  'server_side_encryption' => 'aws:kms',
  'server_side_encryption_kms_key_id' => 'arn:aws:<YOUR KMS KEY ID>:'
}
```

###### SSE-C

SSE-C 要求您设置以下加密选项：

- `backup_encryption`：AES256。
- `backup_encryption_key`：未编码的 32 字节（256 位）密钥。如果不是 32 字节，则上传失败。

例如，在 Omnibus GitLab 中：

```ruby
gitlab_rails['backup_encryption'] = 'AES256'
gitlab_rails['backup_encryption_key'] = '<YOUR 32-BYTE KEY HERE>'
```

如果密钥包含二进制字符且无法以 UTF-8 编码，
则使用 `GITLAB_BACKUP_ENCRYPTION_KEY` 环境变量指定密钥。
例如：

```ruby
gitlab_rails['env'] = { 'GITLAB_BACKUP_ENCRYPTION_KEY' => "\xDE\xAD\xBE\xEF" * 8 }
```

##### Digital Ocean Spaces

此示例可用于 Amsterdam (AMS3) 中的存储桶：

1. 将以下内容添加到 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['backup_upload_connection'] = {
     'provider' => 'AWS',
     'region' => 'ams3',
     'aws_access_key_id' => 'AKIAKIAKI',
     'aws_secret_access_key' => 'secret123',
     'endpoint'              => 'https://ams3.digitaloceanspaces.com'
   }
   gitlab_rails['backup_upload_remote_directory'] = 'my.s3.bucket'
   ```

1. [重新配置极狐GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)，以使更改生效。

如果您在使用 Digital Ocean Spaces 时看到 `400 Bad Request` 错误消息，
可能是因为使用了备份加密。因为 Digital Ocean Spaces 不支持加密、删除或评论包含 `gitlab_rails['backup_encryption']` 的行。

##### 其他 S3 提供者

并非所有 S3 提供者都与 Fog 库完全兼容。例如，
如果您在上传后看到 `411 Length Required` 错误消息，
您可能需要将 `aws_signature_version` 的值从默认值降为`2`。<!--[由于这个问题](https://github.com/fog/fog-aws/issues/428)。-->

对于源代码安装：

1. 编辑 `home/git/gitlab/config/gitlab.yml`：

   ```yaml
     backup:
       # snip
       upload:
         # Fog storage connection settings, see http://fog.io/storage/ .
         connection:
           provider: AWS
           region: eu-west-1
           aws_access_key_id: AKIAKIAKI
           aws_secret_access_key: 'secret123'
           # If using an IAM Profile, leave aws_access_key_id & aws_secret_access_key empty
           # ie. aws_access_key_id: ''
           # use_iam_profile: 'true'
         # The remote 'directory' to store your backups. For S3, this would be the bucket name.
         remote_directory: 'my.s3.bucket'
         # Specifies Amazon S3 storage class to use for backups, this is optional
         # storage_class: 'STANDARD'
         #
         # Turns on AWS Server-Side Encryption with Amazon Customer-Provided Encryption Keys for backups, this is optional
         #   'encryption' must be set in order for this to have any effect.
         #   'encryption_key' should be set to the 256-bit encryption key for Amazon S3 to use to encrypt or decrypt.
         #   To avoid storing the key on disk, the key can also be specified via the `GITLAB_BACKUP_ENCRYPTION_KEY`  your data.
         # encryption: 'AES256'
         # encryption_key: '<key>'
         #
         #
         # Turns on AWS Server-Side Encryption with Amazon S3-Managed keys (optional)
         # https://docs.aws.amazon.com/AmazonS3/latest/userguide/serv-side-encryption.html
         # For SSE-S3, set 'server_side_encryption' to 'AES256'.
         # For SS3-KMS, set 'server_side_encryption' to 'aws:kms'. Set
         # 'server_side_encryption_kms_key_id' to the ARN of customer master key.
         # storage_options:
         #   server_side_encryption: 'aws:kms'
         #   server_side_encryption_kms_key_id: 'arn:aws:kms:YOUR-KEY-ID-HERE'
   ```

1. [重启极狐GitLab](../administration/restart_gitlab.md#installations-from-source)，以使更改生效。

如果您要将备份上传到 S3，您应该创建一个新的具有受限访问权限的 IAM 用户。要仅为上传备份授予上传用户访问权限，请创建以下 IAM 配置文件，用您的存储桶的名称替换 `my.s3.bucket`：

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1412062044000",
      "Effect": "Allow",
      "Action": [
        "s3:AbortMultipartUpload",
        "s3:GetBucketAcl",
        "s3:GetBucketLocation",
        "s3:GetObject",
        "s3:GetObjectAcl",
        "s3:ListBucketMultipartUploads",
        "s3:PutObject",
        "s3:PutObjectAcl"
      ],
      "Resource": [
        "arn:aws:s3:::my.s3.bucket/*"
      ]
    },
    {
      "Sid": "Stmt1412062097000",
      "Effect": "Allow",
      "Action": [
        "s3:GetBucketLocation",
        "s3:ListAllMyBuckets"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Sid": "Stmt1412062128000",
      "Effect": "Allow",
      "Action": [
        "s3:ListBucket"
      ],
      "Resource": [
        "arn:aws:s3:::my.s3.bucket"
      ]
    }
  ]
}
```

##### 使用谷歌云存储

要使用谷歌云存储保存备份，您必须先从谷歌控制台创建访问密钥：

1. 访问[谷歌存储设置页面](https://console.cloud.google.com/storage/settings)。
1. 选择 **Interoperability**，然后创建访问密钥。
1. 记下 **Access Key** 和 **Secret** 并在以下配置中进行替换。
1. 在存储桶高级设置中确保选中访问控制选项
   **Set object-level and bucket-level permissions**。
1. 确保您已经创建了一个存储桶。

对于 Omnibus GitLab 软件包：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['backup_upload_connection'] = {
     'provider' => 'Google',
     'google_storage_access_key_id' => 'Access Key',
     'google_storage_secret_access_key' => 'Secret',

     ## If you have CNAME buckets (foo.example.com), you might run into SSL issues
     ## when uploading backups ("hostname foo.example.com.storage.googleapis.com
     ## does not match the server certificate"). In that case, uncomnent the following
     ## setting. See: https://github.com/fog/fog/issues/2834
     #'path_style' => true
   }
   gitlab_rails['backup_upload_remote_directory'] = 'my.google.bucket'
   ```

1. [重新配置极狐GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)，以使更改生效。

对于源代码安装：

1. 编辑 `home/git/gitlab/config/gitlab.yml`：

   ```yaml
     backup:
       upload:
         connection:
           provider: 'Google'
           google_storage_access_key_id: 'Access Key'
           google_storage_secret_access_key: 'Secret'
         remote_directory: 'my.google.bucket'
   ```

1. [重启极狐GitLab](../administration/restart_gitlab.md#installations-from-source)，以使更改生效。

##### 使用 Azure Blob Storage

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/25877)-->引入于极狐GitLab 13.4。

对于 Omnibus GitLab 软件包：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['backup_upload_connection'] = {
    'provider' => 'AzureRM',
    'azure_storage_account_name' => '<AZURE STORAGE ACCOUNT NAME>',
    'azure_storage_access_key' => '<AZURE STORAGE ACCESS KEY>',
    'azure_storage_domain' => 'blob.core.windows.net', # Optional
   }
   gitlab_rails['backup_upload_remote_directory'] = '<AZURE BLOB CONTAINER>'
   ```

1. [重新配置极狐GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)，以使更改生效。

对于源代码安装：

1. 编辑 `home/git/gitlab/config/gitlab.yml`：

   ```yaml
     backup:
       upload:
         connection:
           provider: 'AzureRM'
           azure_storage_account_name: '<AZURE STORAGE ACCOUNT NAME>'
           azure_storage_access_key: '<AZURE STORAGE ACCESS KEY>'
         remote_directory: '<AZURE BLOB CONTAINER>'
   ```

1. [重新启动极狐GitLab](../administration/restart_gitlab.md#installations-from-source)，以使更改生效。

详情请参见 [Azure 参数表](../administration/object_storage.md#azure-blob-storage)。

##### 为备份指定自定义目录

此选项仅适用于远端存储。如果要对备份进行分组，您可以传递 `DIRECTORY` 环境变量：

```shell
sudo gitlab-backup create DIRECTORY=daily
sudo gitlab-backup create DIRECTORY=weekly
```

极狐GitLab 12.1 及更早版本的用户应该使用 `gitlab-rake gitlab:backup:create`。

#### 跳过将备份上传到远端存储

如果您已将极狐GitLab 配置为[在远端存储中上传备份](#uploading-backups-to-a-remote-cloud-storage)，您可以使用 `SKIP=remote` 选项跳过将备份上传到远端存储。

对于 Omnibus GitLab 软件包：

```shell
sudo gitlab-backup create SKIP=remote
```

对于源代码安装：

```shell
sudo -u git -H bundle exec rake gitlab:backup:create SKIP=remote RAILS_ENV=production
```

<a id="uploading-to-locally-mounted-shares"></a>

#### 上传到本地挂载共享

您还可以使用 Fog [`Local`](https://github.com/fog/fog-local#usage)
存储提供者将备份发送到挂载共享（例如，`NFS`、`CIFS` 或
`SMB`) 。`local_root` 键指向的目录*必须*
由挂载（使用 `CIFS` 和 `SMB` 的 `git` 用户的 `uid=` 进行挂载）时的 `git` 用户或者您执行备份任务的用户（对于 Omnibus 软件包，是 `git` 用户）所拥有。

*必须*设置 `local_root` 键和 `backup_upload_remote_directory`。
这是复制备份的目标挂载目录中的子目录，
如果不存在，您需要进行创建。如果复制 Tarball 的目标目录是您的挂载目录的根目录，请使用 `.`。

因为文件系统性能可能会影响极狐GitLab 的整体性能，极狐GitLab 不推荐使用基于云的存储文件系统<!--[极狐GitLab 不推荐使用基于云的存储文件系统](../administration/nfs.md#avoid-using-cloud-based-file-systems)-->。

对于 Omnibus GitLab 软件包：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['backup_upload_connection'] = {
     :provider => 'Local',
     :local_root => '/mnt/backups'
   }

   # The directory inside the mounted folder to copy backups to
   # Use '.' to store them in the root directory
   gitlab_rails['backup_upload_remote_directory'] = 'gitlab_backups'
   ```

1. [重新配置极狐GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)，以使更改生效。

对于源代码安装：

1. 编辑 `home/git/gitlab/config/gitlab.yml`：

   ```yaml
   backup:
     upload:
       # Fog storage connection settings, see http://fog.io/storage/ .
       connection:
         provider: Local
         local_root: '/mnt/backups'
       # The directory inside the mounted folder to copy backups to
       # Use '.' to store them in the root directory
       remote_directory: 'gitlab_backups'
   ```

1. [重启极狐GitLab](../administration/restart_gitlab.md#installations-from-source)，以使更改生效。

#### 备份归档权限

极狐GitLab 创建的备份归档 (`1393513186_2014_02_27_gitlab_backup.tar`)
默认拥有所有者/群组 `git`/`git` 和 0600 权限。
旨在避免其他系统用户读取极狐GitLab 数据。如果备份归档需要拥有不同的权限，您可以使用 `archive_permissions` 设置。

对于 Omnibus GitLab 软件包：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['backup_archive_permissions'] = 0644 # Makes the backup archives world-readable
   ```

1. [重新配置极狐GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)，以使更改生效。

对于源代码安装：

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   backup:
     archive_permissions: 0644 # Makes the backup archives world-readable
   ```

1. [重启极狐GitLab](../administration/restart_gitlab.md#installations-from-source)，以使更改生效。

#### 配置 Cron 进行每日备份

WARNING:
以下 Cron 作业不[备份您的极狐GitLab 配置文件](#storing-configuration-files)或 [SSH 主机密钥](https://superuser.com/questions/532040/copy-ssh-keys-from-one-server-to-another-server/532079#532079)。

您可以调度备份仓库和极狐GitLab 元数据的 Cron 作业。

对于 Omnibus GitLab 软件包：

1. 为 `root` 用户编辑 Crontab：

   ```shell
   sudo su -
   crontab -e
   ```

1. 添加以下内容，每天上午 2 点调度备份：

   ```plaintext
   0 2 * * * /opt/gitlab/bin/gitlab-backup create CRON=1
   ```

极狐GitLab 12.1 及更早版本的用户应该使用 `gitlab-rake gitlab:backup:create`。

对于源代码安装：

1. 为 `git` 用户编辑 Crontab：

   ```shell
   sudo -u git crontab -e
   ```

1. 在底部添加以下内容：

   ```plaintext
   # Create a full backup of the GitLab repositories and SQL database every day at 2am
   0 2 * * * cd /home/git/gitlab && PATH=/usr/local/bin:/usr/bin:/bin bundle exec rake gitlab:backup:create RAILS_ENV=production CRON=1
   ```

如果没有任何错误，`CRON=1` 环境设置指示备份脚本隐藏所有进度输出。
我们建议您这样做，以减少 Cron 垃圾邮件。
在解决备份问题时，用 `--trace` 替换 `CRON=1` 以进行详细记录。

<a id="limit-backup-lifetime-for-local-files-prune-old-backups"></a>

### 限制本地文件的备份生存期（删除旧备份）

WARNING:
如果您为您的备份使用[自定义文件名](#backup-filename)，本部分中描述的过程将不起作用。

为防止定期备份用尽磁盘空间，您可能需要为备份设置生存时间。下次备份任务运行时，备份时间大于 `backup_keep_time` 的备份会被删除。

此配置选项仅管理本地文件。 极狐GitLab 不会删除存储在第三方[对象存储](#uploading-backups-to-a-remote-cloud-storage)中的旧文件，因为用户可能没有列出和删除文件的权限。
建议您为您的对象存储配置合适的保留策略
（例如，[AWS S3](https://docs.aws.amazon.com/AmazonS3/latest/user-guide/create-lifecycle.html)）。

对于 Omnibus GitLab 软件包：

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   ## Limit backup lifetime to 7 days - 604800 seconds
   gitlab_rails['backup_keep_time'] = 604800
   ```

1. [重新配置极狐GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)，以使更改生效。

对于源代码安装：

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   backup:
     ## Limit backup lifetime to 7 days - 604800 seconds
     keep_time: 604800
   ```

1. [重启极狐GitLab](../administration/restart_gitlab.md#installations-from-source)，以使更改生效。

<a id="restore-gitlab"></a>

## 恢复极狐GitLab

极狐GitLab 提供了命令行界面来恢复您的整个安装。它灵活简便，可以满足您的需求。

[恢复先决条件部分](#restore-prerequisites)包括一些关键信息。在尝试在生产环境中执行之前，请务必至少阅读并测试一次完整的恢复过程。

恢复备份和创建备份的极狐GitLab 必须拥有*完全相同的版本和类型*。

如果您的备份与当前安装的版本不同，则必须在恢复备份之前降级您的极狐GitLab 安装<!--[降级您的极狐GitLab 安装](../update/package/downgrade.md)-->。

<a id="restore-prerequisites"></a>

### 恢复先决条件

您需要先安装有效的极狐GitLab，然后才能执行恢复操作。这是因为执行恢复操作（`git`）的系统用户通常不允许创建或删除导入数据所需的 SQL 数据库
（`gitlabhq_production`）。所有现有数据会被删除
(SQL) 或移动到单独的目录（例如仓库和上传中）中。

要恢复备份，您必须恢复 `/etc/gitlab/gitlab-secrets.json`
（对于 Omnibus 包）或 `/home/git/gitlab/.secret`（对于源代码安装）。此文件包含数据库加密密钥、CI/CD 变量<!--[CI/CD 变量](../ci/variables/index.md)-->，以及
用于双重身份验证<!--[双重身份验证](../user/profile/account/two_factor_authentication.md)-->的变量。如果您无法恢复此加密密钥文件以及应用程序数据备份，启用双重身份验证的用户和极狐GitLab Runner
就无法访问您的极狐GitLab 服务器。

您可能还想恢复以前的 `/etc/gitlab/gitlab.rb`（对于 Omnibus 软件包）
或 `/home/git/gitlab/config/gitlab.yml`（对于源代码安装）和任何 TLS 密钥、证书（`/etc/gitlab/ssl`、`/etc/gitlab/trusted-certs`），或
[SSH 主机密钥](https://superuser.com/questions/532040/copy-ssh-keys-from-one-server-to-another-server/532079#532079)。

从极狐GitLab 12.9 开始，如果发现未解压的备份（如使用
`SKIP=tar`)，并且未使用 `BACKUP=<timestamp>` 选择任何备份，则使用未解压的备份。

根据您的情况，您可能希望使用一个或多个以下选项运行恢复命令：

- `BACKUP=timestamp_of_backup`：如果存在多个备份，则需要。
  阅读[备份时间戳的内容](#backup-timestamp)。
- `force=yes`：不询问是否应该重新生成 authorized_keys 文件，
  并假设为删除的数据库表告警、启用 "写入 authorized_keys 文件" 设置和更新 LDAP 提供者都回复 'yes'。

如果要还原到作为挂载点的目录，则必须确保这些目录在还原操作之前是空的。
否则，极狐GitLab 会尝试在还原新数据之前移动这些目录，导致错误。

阅读更多关于配置 NFS 挂载<!--[配置 NFS 挂载](../administration/nfs.md)-->的内容。

### 为 Omnibus GitLab 安装还原

此过程假定：

- 您已经安装了与创建备份所用的极狐GitLab **完全相同的版本和类型**。
- 您至少运行了一次 `sudo gitlab-ctl reconfigure`。
- 极狐GitLab 正在运行。如果没有，请使用 `sudo gitlab-ctl start` 进行启动。

首先确保您的备份 tar 文件位于
`gitlab.rb` 配置 `gitlab_rails['backup_path']` 中描述的备份目录中。默认是
`/var/opt/gitlab/backups`。备份文件需要由 `git` 用户拥有。

```shell
sudo cp 11493107454_2018_04_25_10.6.4-ce_gitlab_backup.tar /var/opt/gitlab/backups/
sudo chown git:git /var/opt/gitlab/backups/11493107454_2018_04_25_10.6.4-ce_gitlab_backup.tar
```

停止与数据库进行连接的进程。让极狐GitLab 的其余部分运行：

```shell
sudo gitlab-ctl stop puma
sudo gitlab-ctl stop sidekiq
# Verify
sudo gitlab-ctl status
```

接下来，指定您想恢复的备份的时间戳，恢复备份：

```shell
# This command will overwrite the contents of your GitLab database!
sudo gitlab-backup restore BACKUP=11493107454_2018_04_25_10.6.4-ce
```

极狐GitLab 12.1 及更早版本的用户应该使用 `gitlab-rake gitlab:backup:restore`。
或许会出现一些[已知非阻塞的错误消息](#restoring-database-backup-using-omnibus-packages-outputs-warnings)。

WARNING:
`gitlab-rake gitlab:backup:restore` 没有在您的镜像库目录中设置正确的文件系统权限。
在极狐GitLab 12.2 或更高版本中，您可以使用 `gitlab-backup restore` 避免这个问题。

如果备份 tar 文件与安装的极狐GitLab 版本不匹配，恢复命令会中止并出现错误信息。您需要安装正确的极狐GitLab 版本，然后重试。

WARNING:
如果您的安装使用 PgBouncer，无论是出于性能原因还是将其与 Patroni 集群一起使用，恢复命令都需要[附加参数](#back-up-and-restore-for-installations-using-pgbouncer)。

接下来，如有必要，[像上文提到的那样](#restore-prerequisites)恢复 `/etc/gitlab/gitlab-secrets.json`。

重新配置、重启并检查<!--[检查](../administration/raketasks/maintenance.md#check-gitlab-configuration)-->极狐GitLab：

```shell
sudo gitlab-ctl reconfigure
sudo gitlab-ctl restart
sudo gitlab-rake gitlab:check SANITIZE=true
```

在极狐GitLab 13.1 及更高版本中，确认数据库值可以被解密<!--[数据库值可以被解密](../administration/raketasks/check.md#verify-database-values-can-be-decrypted-using-the-current-secrets)-->，特别是恢复 `/etc/gitlab/gitlab-secrets.json`，或者恢复目标是另外一个服务器的时候。

```shell
sudo gitlab-rake gitlab:doctor:secrets
```

为双重保险，您可以在上传文件上进行完整性检查<!--[在上传文件上进行完整性检查](../administration/raketasks/check.md#uploaded-files-integrity)-->：

```shell
sudo gitlab-rake gitlab:artifacts:check
sudo gitlab-rake gitlab:lfs:check
sudo gitlab-rake gitlab:uploads:check
```

### 为 Docker 镜像和极狐GitLab Helm Chart 安装进行恢复


对于在 Kubernetes 集群上使用 Docker 镜像或极狐GitLab Helm Chart 进行的极狐GitLab 安装，
恢复任务期望恢复目录为空。但是，使用 Docker 和 Kubernetes 卷挂载，可能会在卷根目录创建系统级别的目录，例如 Linux 系统中的 `lost+found` 目录。
这些目录通常由 `root` 拥有，可能会导致因恢复 Rake 任务以 `git` 用户身份运行而带来的访问权限错误。
要恢复极狐GitLab 安装，用户必须保证恢复目标目录为空。

对于这两种安装类型，备份 Tarball 必须在备份位置（默认位置是 `/var/opt/gitlab/backups`）中可用。

对于 Docker 安装，可以从主机运行恢复任务：

```shell
# Stop the processes that are connected to the database
docker exec -it <name of container> gitlab-ctl stop puma
docker exec -it <name of container> gitlab-ctl stop sidekiq

# Verify that the processes are all down before continuing
docker exec -it <name of container> gitlab-ctl status

# Run the restore. NOTE: "_gitlab_backup.tar" is omitted from the name
docker exec -it <name of container> gitlab-backup restore BACKUP=11493107454_2018_04_25_10.6.4-ce

# Restart the GitLab container
docker restart <name of container>

# Check GitLab
docker exec -it <name of container> gitlab-rake gitlab:check SANITIZE=true
```

极狐GitLab 12.1 及更早版本应该使用 `gitlab-rake gitlab:backup:create`。

WARNING:
`gitlab-rake gitlab:backup:restore` 没有在镜像库目录中设置正确的文件系统权限。
在极狐GitLab 12.2 及更高版本中，您可以使用 `gitlab-backup restore` 避免这个问题。

极狐GitLab Helm Chart 使用了不同的流程，详情请参见[恢复极狐GitLab Helm Chart 安装](https://jihulab.com/gitlab-cn/charts/gitlab/blob/master/doc/backup-restore/restore.md)。

### 为源代码安装进行恢复

首先，确保您的备份 Tar 文件在 `gitlab.yml` 配置中描述的备份目录中。

```yaml
## Backup settings
backup:
  path: "tmp/backups"   # Relative paths are relative to Rails.root (default: tmp/backups/)
```

默认为 `/home/git/gitlab/tmp/backups`，且需要由 `git` 用户拥有。现在，您可以开始进行备份了。

```shell
# Stop processes that are connected to the database
sudo service gitlab stop

sudo -u git -H bundle exec rake gitlab:backup:restore RAILS_ENV=production
```

示例输出：

```plaintext
Unpacking backup... [DONE]
Restoring database tables:
-- create_table("events", {:force=>true})
   -> 0.2231s
[...]
- Loading fixture events...[DONE]
- Loading fixture issues...[DONE]
- Loading fixture keys...[SKIPPING]
- Loading fixture merge_requests...[DONE]
- Loading fixture milestones...[DONE]
- Loading fixture namespaces...[DONE]
- Loading fixture notes...[DONE]
- Loading fixture projects...[DONE]
- Loading fixture protected_branches...[SKIPPING]
- Loading fixture schema_migrations...[DONE]
- Loading fixture services...[SKIPPING]
- Loading fixture snippets...[SKIPPING]
- Loading fixture taggings...[SKIPPING]
- Loading fixture tags...[SKIPPING]
- Loading fixture users...[DONE]
- Loading fixture users_projects...[DONE]
- Loading fixture web_hooks...[SKIPPING]
- Loading fixture wikis...[SKIPPING]
Restoring repositories:
- Restoring repository abcd... [DONE]
- Object pool 1 ...
Deleting tmp directories...[DONE]
```

接下来，如有必要，[像上文提到的那样](#restore-prerequisites)恢复 `/home/git/gitlab/.secret`。

重启极狐GitLab：

```shell
sudo service gitlab restart
```

### 从备份恢复一个或几个项目或群组

虽然用于恢复极狐GitLab 实例的 Rake 任务不支持恢复单个项目或群组，您可以将您的备份恢复为单独且临时的极狐GitLab 实例，然后从中导出您的项目或群组：

1. [安装新的极狐GitLab](../install/index.md) 实例，其版本需要与您想恢复的备份实例的版本相同。
1. [恢复备份](#restore-gitlab)到新实例，然后
   导出您的[项目](../user/project/settings/import_export.md)或[群组](../user/group/settings/import_export.md)。
   请务必阅读导出功能文档的**重要说明**，以了解导出和没导出的内容。
1. 导出完成后，访问旧实例，然后导出。
1. 导入完您想要的项目或群组后，您就可以删除新的临时的极狐GitLab 实例。

<!--A feature request to provide direct restore of individual projects or groups
is being discussed in [issue #17517](https://gitlab.com/gitlab-org/gitlab/-/issues/17517).-->

### 恢复选项


极狐GitLab 提供的从备份恢复的命令行工具可以接受更多选项。

#### 排除恢复的任务

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/19347)-->引入与极狐GitLab 14.10。

您可以通过添加环境变量 `SKIP` 来排除恢复时的特定任务，其值是以下选项以逗号分隔的列表：

- `db`（数据库）
- `uploads`（附件）
- `builds`（CI 作业输出日志）
- `artifacts`（CI 作业产物）
- `lfs`（LFS 对象）
- `terraform_state`（Terraform 状态）
- `registry`（容器镜像库镜像）
- `pages`（Pages 内容）
- `repositories`（Git 仓库数据）
- `packages`（软件包）

对于 Omnibus GitLab 软件包：

```shell
sudo gitlab-backup restore BACKUP=timestamp_of_backup SKIP=db,uploads
```

对于源代码安装：

```shell
sudo -u git -H bundle exec rake gitlab:backup:restore BACKUP=timestamp_of_backup SKIP=db,uploads RAILS_ENV=production
```

#### 恢复特定仓库存储

> <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/86896)-->引入于极狐GitLab 15.0。

使用[多个仓库存储](../administration/repository_storage_paths.md)时，可以使用 `REPOSITORIES_STORAGES` 选项单独备份
特定仓库存储中的仓库。该选项接受以逗号分隔的存储名称列表。

例如，对于 Omnibus GitLab 安装：

```shell
sudo gitlab-backup restore BACKUP=timestamp_of_backup REPOSITORIES_STORAGES=storage1,storage2
```

例如，对于源代码安装：

```shell
sudo -u git -H bundle exec rake gitlab:backup:restore BACKUP=timestamp_of_backup REPOSITORIES_STORAGES=storage1,storage2
```

## 替代备份策略

如果您的极狐GitLab 实例包含大量 Git 仓库数据，您可能会发现极狐GitLab 备份脚本太慢了。如果您的极狐GitLab 实例有很多派生项目，定期备份任务也会复制所有项目的 Git 数据。
在这些情况下，请考虑使用文件系统快照作为备份策略的一部分。

示例：Amazon Elastic Block Store（EBS）

> 使用托管在 Amazon AWS 上的 Omnibus GitLab 的极狐GitLab 服务器。
> 包含 ext4 文件系统的 EBS 驱动挂载在 `/var/opt/gitlab` 中。
> 在这种情况下，您可以通过拍摄 EBS 快照来备份应用程序。
> 备份包括所有仓库、上传和 PostgreSQL 数据。

示例：Logical Volume Manager（LVM）快照 + rsync

> 使用 Omnibus GitLab 的极狐GitLab 服务器，LVM 逻辑卷挂载在 `/var/opt/gitlab`。
> 使用 rsync 复制 `/var/opt/gitlab` 目录并不可靠，因为 rsync 在运行时会更改太多文件。
> 我们创建了临时 LVM 快照，而不是 rsync-ing `/var/opt/gitlab`，我们将其作为只读文件系统挂载到 `/mnt/gitlab_backup`。
> 现在我们有了运行时间更长的 rsync 作业，它在远端服务器上创建一个一致的副本。
> 副本包括所有仓库、上传和 PostgreSQL 数据。

如果您在虚拟化服务器上运行极狐GitLab，您还可以创建整个极狐GitLab 服务器的 VM 快照。但是对于虚拟机快照来说，要求您关闭服务器并不罕见，这限制了此解决方案的实际使用。

### 分别备份仓库数据

首先，确保您在[跳过仓库](#excluding-specific-directories-from-the-backup)的同时备份现存的极狐GitLab 数据：

```shell
# for Omnibus GitLab package installations
sudo gitlab-backup create SKIP=repositories

# for installations from source:
sudo -u git -H bundle exec rake gitlab:backup:create SKIP=repositories RAILS_ENV=production
```

对于手动备份磁盘上的 Git 仓库数据，有多种可能的策略：

- 使用快照，例如之前的 Amazon EBS 驱动快照示例，或 LVM 快照 + rsync。
- 使用极狐GitLab Geo<!--[极狐GitLab Geo](../administration/geo/index.md) --> 并依赖 Geo 辅助站点上的仓库数据。
- [防止写入和复制 Git 仓库数据](#prevent-writes-and-copy-the-git-repository-data)。
- [通过将仓库标记为只读（实验性）来创建在线备份](#online-backup-through-marking-repositories-as-read-only-experimental)。

<a id="prevent-writes-and-copy-the-git-repository-data"></a>

#### 防止写入和复制 Git 仓库数据

必须以一致的方式复制 Git 仓库。在并发写入操作期间不应该进行复制，因为这可能会导致不一致或损坏问题。

为了防止写入 Git 仓库数据，有两种可能的方法：

- 使用[维护模式](../administration/maintenance_mode/index.md)替换只读状态中的极狐GitLab。
- 通过在备份仓库之前停止所有 Gitaly 服务来创建显式停机时间：

  ```shell
  sudo gitlab-ctl stop gitaly
  # execute git data copy step
  sudo gitlab-ctl start gitaly
  ```

您可以使用任何方法复制 Git 仓库数据，只要阻止对正在复制的数据进行写入（以防止不一致和损坏问题）。出于优先和安全考虑，推荐的方法是：

1. 将 `rsync` 与归档模式、删除和校验和选项一起使用，例如：

   ```shell
   rsync -aR --delete --checksum source destination # be extra safe with the order as it will delete existing data if inverted
   ```

1. 使用 `tar` 管道将整个仓库的目录复制到另一个服务器或位置。<!--[`tar` 管道将整个仓库的目录复制到另一个服务器或位置](../administration/operations/moving_repositories.md#tar-pipe-to-another-server)-->

1. 使用 `sftp`、`scp`、`cp` 或任何其他复制方法。

<a id="online-backup-through-marking-repositories-as-read-only-experimental"></a>

#### 通过将仓库标记为只读进行在线备份（实验性功能）

一种无需实例范围停机即可备份仓库的方法是在复制底层数据时以编程方式将项目标记为只读。

这样做有几个可能的缺点：

- 仓库在一段时间内是只读的，该时间段随仓库的大小而变化。
- 由于将每个项目标记为只读，备份需要更长的时间才能完成，这可能会导致不一致。例如，
  与备份的最后一个项目相比，备份的第一个项目的最后可用数据可能存在日期差异。
- 派生网络应该是完全只读的，同时备份内部项目以防止对池仓库进行更改。

这是一个**实验性的**脚本，试图在 Geo 团队 Runbook 项目<!--[Geo 团队 Runbook 项目](https://jihulab.com/gitlab-cn/geo-team/runbooks/-/tree/main/experimental-online-backup-through-rsync)-->中进行自动化。

<a id="back-up-and-restore-for-installations-using-pgbouncer"></a>

## 为使用 PgBouncer 的安装进行备份和恢复

不要通过 PgBouncer 连接备份或恢复极狐GitLab。这些任务必须[绕过 PgBouncer 并直接连接到 PostgreSQL 主数据库节点](#bypassing-pgbouncer)，否则它们会导致极狐GitLab 中断。

当通过 PgBouncer 进行极狐GitLab 备份或恢复任务时，会显示以下错误消息：

```ruby
ActiveRecord::StatementInvalid: PG::UndefinedTable
```

每次运行极狐GitLab 备份时，极狐GitLab 都会生成 500 个错误，缺失表格的错误会由 PostgreSQL 进行记录：
<!--[由 PostgreSQL 进行记录](../administration/logs.md#postgresql-logs)：-->

```plaintext
ERROR: relation "tablename" does not exist at character 123
```

<!--This happens because the task uses `pg_dump`, which [sets a null search
path and explicitly includes the schema in every SQL query](https://gitlab.com/gitlab-org/gitlab/-/issues/23211)
to address [CVE-2018-1058](https://www.postgresql.org/about/news/postgresql-103-968-9512-9417-and-9322-released-1834/).-->

由于在事务池模式下通过 PgBouncer 重用连接，PostgreSQL 无法搜索默认的 `public` 模式。因此，清除搜索路径会出现表和列失踪的情况发生。

<a id="bypassing-pgbouncer"></a>

### 绕过 PgBouncer

有两种方法可以解决这个问题：

1. 为备份任务[使用环境变量覆盖数据库设置](#environment-variable-overrides)。
1. 重新配置节点，[直接连接到 PostgreSQL 主数据库节点](../administration/postgresql/pgbouncer.md#procedure-for-bypassing-pgbouncer)。

<a id="environment-variable-overrides"></a>

#### 环境变量覆盖

默认情况下，极狐GitLab 使用存储在配置文件（`database.yml`）中的数据库配置。但是，您可以通过设置以 `GITLAB_BACKUP_` 为前缀的环境变量，为备份和恢复任务覆盖数据库设置：

- `GITLAB_BACKUP_PGHOST`
- `GITLAB_BACKUP_PGUSER`
- `GITLAB_BACKUP_PGPORT`
- `GITLAB_BACKUP_PGPASSWORD`
- `GITLAB_BACKUP_PGSSLMODE`
- `GITLAB_BACKUP_PGSSLKEY`
- `GITLAB_BACKUP_PGSSLCERT`
- `GITLAB_BACKUP_PGSSLROOTCERT`
- `GITLAB_BACKUP_PGSSLCRL`
- `GITLAB_BACKUP_PGSSLCOMPRESSION`

例如，使用 Omnibus 软件包覆盖数据库主机和端口以使用 192.168.1.10
和端口 5432：

```shell
sudo GITLAB_BACKUP_PGHOST=192.168.1.10 GITLAB_BACKUP_PGPORT=5432 /opt/gitlab/bin/gitlab-backup create
```

有关参数的更多内容，请参见[PostgreSQL 文档](https://www.postgresql.org/docs/12/libpq-envars.html)。

<a id="migrate-to-a-new-server"></a>

## 迁移到新服务器

<!-- some details borrowed from GitLab.com move from Azure to GCP detailed at https://gitlab.com/gitlab-com/migration/-/blob/master/.gitlab/issue_templates/failover.md -->

您可以使用极狐GitLab 进行备份和恢复，将您的实例迁移到新的服务器上。本部分简要介绍在服务器上运行极狐GitLab 部署的典型过程。
如果您正在运行极狐GitLab Geo，替代做法是计划故障的 Geo 灾难复原<!--[计划故障的 Geo 灾难复原](../administration/geo/disaster_recovery/planned_failover.md)-->。

WARNING:
避免新旧服务器不协调的数据处理，其中多个服务器可以同时连接并处理相同的数据。例如，当使用[接收电子邮件](../administration/incoming_email.md)，如果两个极狐GitLab 实例同时处理电子邮件，那么这两个实例都会丢失一些数据。
其他服务也可能出现此类问题，例如[非打包数据库](https://docs.gitlab.cn/omnibus/settings/database.html#using-a-non-packaged-postgresql-database-management-server)、
未打包的 Redis 实例，或未打包的 Sidekiq。

先决条件：

- 迁移之前，使用[广播消息横幅](../user/admin_area/broadcast_messages.md)通知您的用户即将要进行的定期维护。
- 确保您的备份是完整且最新的。创建完整的系统级备份，或拍摄迁移中涉及的所有服务器的快照，以防破坏性命令
  （如 `rm`）运行不正确。

### 准备新服务器

准备新服务器：

1. 从旧服务器复制
   [SSH 主机密钥](https://superuser.com/questions/532040/copy-ssh-keys-from-one-server-to-another-server/532079#532079)，
   避免中间人攻击警告。
   <!--示例步骤请参见[手动复制主站点的 SSH 主机密钥](../administration/geo/replication/configuration.md#step-2-manually-replicate-the-primary-sites-ssh-host-keys)。-->
1. [安装并配置极狐GitLab](https://about.gitlab.cn/install)，
   [接收电子邮件](../administration/incoming_email.md)除外：
    1. 安装极狐GitLab。
    1. 通过将 `/etc/gitlab` 文件从旧服务器复制到新服务器进行配置，并根据需要进行更新。
       详情请参见
       [Omnibus 配置备份和恢复说明](https://docs.gitlab.cn/omnibus/settings/backups.html)。
    1. 如果适用，禁用[传入电子邮件](../administration/incoming_email.md)。
    1. 在备份和恢复后的初始启动时阻止新的 CI/CD 作业启动。
       编辑 `/etc/gitlab/gitlab.rb` 并设置以下内容：

       ```ruby
       nginx['custom_gitlab_server_config'] = "location /api/v4/jobs/request {\n deny all;\n return 503;\n}\n"
       ```

    1. 重新配置极狐GitLab：

       ```shell
       sudo gitlab-ctl reconfigure
       ```

1. 停止极狐GitLab，以防不必要和无意的数据处理：

   ```shell
   sudo gitlab-ctl stop
   ```

1. 配置新服务器以允许接收 Redis 数据库和极狐GitLab 备份文件：

   ```shell
   sudo rm -f /var/opt/gitlab/redis/dump.rdb
   sudo chown <your-linux-username> /var/opt/gitlab/redis /var/opt/gitlab/backups
   ```

### 从旧服务器准备和传输内容

1. 确保您拥有旧服务器的最新系统级备份或快照。
1. 如果您的极狐GitLab 版本支持的话，启用[维护模式](../administration/maintenance_mode/index.md)。
1. 组织启用新的 CI/CD 作业：
    1. 编辑 `/etc/gitlab/gitlab.rb` 并设置以下内容：

       ```ruby
       nginx['custom_gitlab_server_config'] = "location /api/v4/jobs/request {\n deny all;\n return 503;\n}\n"
       ```

    1. 重新配置极狐GitLab：

       ```shell
       sudo gitlab-ctl reconfigure
       ```

1. 禁用定期后台作业：
    1. 在顶部栏上，选择 **菜单 > 管理员**。
    1. 在左侧边栏上，选择 **监控 > 后台作业**。
    1. 在 Sidekiq 仪表板下，选择 **Cron** 选项卡，然后选择 **禁用所有**。
1. 等待当前正在运行的 CI/CD 作业完成，否则尚未完成的作业可能会丢失。
   要查看当前正在运行的作业，请在左侧边栏中选择 **概览 > 作业**，
   然后选择 **运行**。
1. 等待 Sidekiq 作业完成：
    1. 在左侧边栏上，选择 **监控 > 后台作业**。
    1. 在 Sidekiq 仪表板下，选择 **队列**，然后选择 **实时轮询**。
       等待 **忙碌** 和 **排队** 的值降为 0。
       这些队列包含您的用户已提交的作业。在完成之前关闭可能会导致作业丢失。
       记下 Sidekiq 仪表板中显示的数字，以进行迁移后验证。
1. 将 Redis 数据库刷到磁盘，并停止除迁移所需服务以外的极狐GitLab 服务：

   ```shell
   sudo /opt/gitlab/embedded/bin/redis-cli -s /var/opt/gitlab/redis/redis.socket save && sudo gitlab-ctl stop && sudo gitlab-ctl start postgresql && sudo gitlab-ctl start gitaly
   ```

1. 创建极狐GitLab 备份：

   ```shell
   sudo gitlab-backup create
   ```

1. 将以下内容添加到 `/etc/gitlab/gitlab.rb` 底部，禁用以下极狐GitLab 服务并阻止意外重启：

   ```ruby
   alertmanager['enable'] = false
   gitlab_exporter['enable'] = false
   gitlab_pages['enable'] = false
   gitlab_workhorse['enable'] = false
   grafana['enable'] = false
   logrotate['enable'] = false
   gitlab_rails['incoming_email_enabled'] = false
   nginx['enable'] = false
   node_exporter['enable'] = false
   postgres_exporter['enable'] = false
   postgresql['enable'] = false
   prometheus['enable'] = false
   puma['enable'] = false
   redis['enable'] = false
   redis_exporter['enable'] = false
   registry['enable'] = false
   sidekiq['enable'] = false
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

1. 验证一切都已停止，并确认没有正在运行的服务：

   ```shell
   sudo gitlab-ctl status
   ```

1. 将 Redis 数据库和极狐GitLab 备份传输到新服务器：

   ```shell
   sudo scp /var/opt/gitlab/redis/dump.rdb <your-linux-username>@new-server:/var/opt/gitlab/redis
   sudo scp /var/opt/gitlab/backups/your-backup.tar <your-linux-username>@new-server:/var/opt/gitlab/backups
   ```

### 在新服务器上恢复数据

1. 恢复合适的文件系统权限：

   ```shell
   sudo chown gitlab-redis /var/opt/gitlab/redis
   sudo chown gitlab-redis:gitlab-redis /var/opt/gitlab/redis/dump.rdb
   sudo chown git:root /var/opt/gitlab/backups
   sudo chown git:git /var/opt/gitlab/backups/your-backup.tar
   ```

1. [恢复极狐GitLab 备份](#restore-gitlab).
1. 验证 Redis 数据库是否恢复正确：
    1. 在顶部栏上，选择 **菜单 > 管理员**。
    1. 在左侧边栏上，选择 **监控 > 后台作业**。
    1. 在 Sidekiq 仪表板下，验证数字与旧服务器上显示的内容相匹配。
    1. 在 Sidekiq 仪表板下，选择 **Cron**，然后选择 **启用全部**
       重新启用定期后台作业。
1. 测试极狐GitLab 实例上的只读操作是否按预期工作。例如，浏览项目仓库文件、合并请求和议题。
1. 如果之前启用了[维护模式](../administration/maintenance_mode/index.md)，将其禁用。
1. 测试极狐GitLab 实例是否按预期工作。
1. 如果适用，重新启用[接收电子邮件](../administration/incoming_email.md)并测试它是否按预期工作。
1. 更新您的 DNS 或负载均衡器以指向新服务器。
1. 通过删除您之前添加的自定义 NGINX 配置来取消阻止新的 CI/CD 作业启动：

   ```ruby
   # The following line must be removed
   nginx['custom_gitlab_server_config'] = "location /api/v4/jobs/request {\n deny all;\n return 503;\n}\n"
   ```

1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

1. 移除计划维护[广播消息横幅](../user/admin_area/broadcast_messages.md)。

<!--## Additional notes

This documentation is for GitLab Community and Enterprise Edition. We back up
GitLab.com and ensure your data is secure. You can't, however, use these
methods to export or back up your data yourself from GitLab.com.

Issues are stored in the database, and can't be stored in Git itself.

To migrate your repositories from one server to another with an up-to-date
version of GitLab, use the [import Rake task](import.md) to do a mass import of
the repository. If you do an import Rake task rather than a backup restore,
you get all of your repositories, but no other data.-->

## 故障排除

以下是您可能遇到的问题及相应解决方法。

<a id="restoring-database-backup-using-omnibus-packages-outputs-warnings"></a>

### 使用 Omnibus 包恢复数据库备份输出警告

如果您进行备份恢复操作，您可能会遇到以下警告信息：

```plaintext
ERROR: must be owner of extension pg_trgm
ERROR: must be owner of extension btree_gist
ERROR: must be owner of extension plpgsql
WARNING:  no privileges could be revoked for "public" (two occurrences)
WARNING:  no privileges were granted for "public" (two occurrences)
```

请注意，尽管有这些警告消息，但备份已成功恢复。

Rake 任务以 `gitlab` 用户身份运行，该用户没有访问数据库的超级用户权限。
启动还原时，它也作为 `gitlab` 用户运行，但它也尝试更改无权访问的对象。
这些对象对数据库备份或恢复没有影响，但会显示告警信息。

详情请参见：

- PostgreSQL 议题跟踪：
    - [不是超级用户](https://www.postgresql.org/message-id/201110220712.30886.adrian.klaver@gmail.com)。
    - [拥有不同拥有者](https://www.postgresql.org/message-id/2039.1177339749@sss.pgh.pa.us)。

- Stack Overflow：[错误](https://stackoverflow.com/questions/4368789/error-must-be-owner-of-language-plpgsql)。

<a id="when-the-secrets-file-is-lost"></a>

### Secret 文件丢失

如果您没有[备份 Secret 文件](#storing-configuration-files)，您必须完成几个步骤才能让极狐GitLab 再次正常工作。

Secret 文件负责存储包含所需敏感信息的列的加密密钥。
如果密钥丢失，极狐GitLab 将无法解密这些列，也无法访问以下项目：

- CI/CD 变量<!--[CI/CD 变量](../ci/variables/index.md)-->
- Kubernetes / GCP 集成<!--[Kubernetes / GCP 集成](../user/infrastructure/clusters/index.md)-->
- 自定义 Pages 域<!--[自定义 Pages 域](../user/project/pages/custom_domains_ssl_tls_certification/index.md)-->
- 项目错误跟踪<!--[项目错误跟踪](../operations/error_tracking.md)-->
- Runner 鉴权<!--[Runner 鉴权](../ci/runners/index.md)-->
- 项目镜像<!--[项目镜像](../user/project/repository/mirror/index.md)-->
- 网络钩子<!--[网络钩子](../user/project/integrations/webhooks.md)-->

在 CI/CD 变量和 Runner 鉴权等过程中，您可能会遇到：

- 作业卡住
- 500 个错误

在这种情况下，您必须重置 CI/CD 变量和 Runner 鉴权的所有令牌，后面会进行详细介绍。
重置令牌后，您应该可以访问您的项目，作业会再次运行。

使用以下部分中的信息需要您自担风险。

#### 验证所有值都可以解密

您可以确定您的数据库是否包含无法通过使用 Rake 任务<!-- [Rake 任务](../administration/raketasks/check.md#verify-database-values-can-be-decrypted-using-the-current-secrets)-->解密的值。

#### 备份

您必须直接修改极狐GitLab 数据以解决 Secret 文件丢失的问题。

WARNING:
在进行任何更改之前，请创建完整的数据库备份。

#### 禁用用户双重身份验证鉴权（2FA）

启用 2FA 的用户无法登录极狐GitLab。在这种情况下，您必须为所有人禁用 2FA，之后用户必须重新激活 2FA。
<!--[为所有人禁用 2FA](../security/two_factor_authentication.md#disable-2fa-for-everyone)-->

#### 重启 CI/CD 变量

1. 进入数据库控制台：

   对于 Omnibus GitLab 14.1 及更早版本：

   ```shell
   sudo gitlab-rails dbconsole
   ```

   对于 Omnibus GitLab 14.2 及更高版本：

   ```shell
   sudo gitlab-rails dbconsole --database main
   ```

   对于源代码安装极狐GitLab 14.1 及更早版本：

   ```shell
   sudo -u git -H bundle exec rails dbconsole -e production
   ```

   对于源代码安装极狐GitLab 14.2 及更高版本：

   ```shell
   sudo -u git -H bundle exec rails dbconsole -e production --database main
   ```

1. 检查 `ci_group_variables` 和 `ci_variables` 表：

   ```sql
   SELECT * FROM public."ci_group_variables";
   SELECT * FROM public."ci_variables";
   ```

   这些是您需要删除的变量。

1. 丢弃表：

   ```sql
   DELETE FROM ci_group_variables;
   DELETE FROM ci_variables;
   ```

1. 如果您知道要从中删除变量的特定群组或项目，您可以在您的 `DELETE` 中包含一个 `WHERE` 语句来指定它：

   ```sql
   DELETE FROM ci_group_variables WHERE group_id = <GROUPID>;
   DELETE FROM ci_variables WHERE project_id = <PROJECTID>;
   ```

您可能需要重新配置或重启极狐GitLab，以使更改生效。

#### 重启 Runner 注册令牌

1. 进入数据库控制台：

   对于 Omnibus GitLab 14.1 及更早版本：

   ```shell
   sudo gitlab-rails dbconsole
   ```

   对于 Omnibus GitLab 14.2 及更高版本：

   ```shell
   sudo gitlab-rails dbconsole --database main
   ```

   对于源代码安装极狐GitLab 14.1 及更早版本：

   ```shell
   sudo -u git -H bundle exec rails dbconsole -e production
   ```

   对于源代码安装极狐GitLab 14.2 及更高版本：

   ```shell
   sudo -u git -H bundle exec rails dbconsole -e production --database main
   ```

1. 清理项目、群组和整个实例的所有令牌：

   WARNING:
   最终的 `UPDATE` 操作阻止 Runner 接收新作业。您必须注册新的 Runner。

   ```sql
   -- Clear project tokens
   UPDATE projects SET runners_token = null, runners_token_encrypted = null;
   -- Clear group tokens
   UPDATE namespaces SET runners_token = null, runners_token_encrypted = null;
   -- Clear instance tokens
   UPDATE application_settings SET runners_registration_token_encrypted = null;
   -- Clear key used for JWT authentication
   -- This may break the $CI_JWT_TOKEN job variable:
   -- https://gitlab.com/gitlab-org/gitlab/-/issues/325965
   UPDATE application_settings SET encrypted_ci_jwt_signing_key = null;
   -- Clear runner tokens
   UPDATE ci_runners SET token = null, token_encrypted = null;
   ```

#### 重置待处理的流水线作业

1. 进入数据库控制台：

   对于 Omnibus GitLab 14.1 及更早版本：

   ```shell
   sudo gitlab-rails dbconsole
   ```

   对于 Omnibus GitLab 14.2 及更高版本：

   ```shell
   sudo gitlab-rails dbconsole --database main
   ```

   对于源代码安装极狐GitLab 14.1 及更早版本：

   ```shell
   sudo -u git -H bundle exec rails dbconsole -e production
   ```

   对于源代码安装极狐GitLab 14.2 及更高版本：

   ```shell
   sudo -u git -H bundle exec rails dbconsole -e production --database main
   ```

1. 清理待处理作业的所有令牌：

   ```sql
   -- Clear build tokens
   UPDATE ci_builds SET token = null, token_encrypted = null;
   ```

可以对其余功能采用类似的策略。通过删除无法解密的数据，极狐GitLab 可以继续运行，并且手动替换丢失的数据。

#### 修复项目集成

如果您丢失了 Secret，[项目的集成设置页面](../user/project/integrations/index.md)可能会显示 `500` 错误消息。

修复方法是截断 `web_hooks` 表：

1. 进入数据库控制台：

   对于 Omnibus GitLab 14.1 及更早版本：

   ```shell
   sudo gitlab-rails dbconsole
   ```

   对于 Omnibus GitLab 14.2 及更高版本：

   ```shell
   sudo gitlab-rails dbconsole --database main
   ```

   对于源代码安装GitLab 14.1 及更早版本：

   ```shell
   sudo -u git -H bundle exec rails dbconsole -e production
   ```

   对于源代码安装GitLab 14.2 及更高版本：

   ```shell
   sudo -u git -H bundle exec rails dbconsole -e production --database main
   ```

1. 截断表：

   ```sql
   -- truncate web_hooks table
   TRUNCATE web_hooks CASCADE;
   ```

### 从备份还原后容器镜像库推送失败

如果您使用[容器镜像库](../user/packages/container_registry/index.md)，恢复镜像库数据后，在 Omnibus GitLab 实例上恢复备份后，向镜像库推送可能会失败。

这些失败在镜像库日志中提到了权限问题，类似于：

```plaintext
level=error
msg="response completed with error"
err.code=unknown
err.detail="filesystem: mkdir /var/opt/gitlab/gitlab-rails/shared/registry/docker/registry/v2/repositories/...: permission denied"
err.message="unknown error"
```

此问题是由以非特权用户 `git` 运行的恢复引起的，无法在恢复过程中为镜像库文件分配正确的所有权。
<!--([issue #62759](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/62759 "Incorrect permissions on registry filesystem after restore")).-->

让您的镜像库再次工作：

```shell
sudo chown -R registry:registry /var/opt/gitlab/gitlab-rails/shared/registry/docker
```

如果您更改了镜像库的默认文件系统位置，请针对您的自定义位置运行 `chown`，
而不是 `/var/opt/gitlab/gitlab-rails/shared/registry/docker`。

### 备份完成失败，显示 Gzip 错误

运行备份时，您可能会收到 Gzip 错误消息：

```shell
sudo /opt/gitlab/bin/gitlab-backup create
...
Dumping ...
...
gzip: stdout: Input/output error

Backup failed
```

如果发生这种情况，请检查以下内容：

- 确认有足够的磁盘空间用于 Gzip 操作。
- 如果正在使用 NFS，请检查是否设置了挂载选项 `timeout`。
  默认值为 `600`，将其更改为较小的值会导致此错误。

### 仓库备份和恢复的 `gitaly-backup`

> - <!--[Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/333034)-->引入于极狐GitLab 14.2。
> - 在功能标志后部署<!--[在功能标志后部署](../user/feature_flags.md)-->，默认启用。
> - <!--[Generally available](https://gitlab.com/gitlab-org/gitlab/-/issues/333034)-->广泛可用于极狐GitLab 14.10。移除功能标志 `gitaly_backup`。<!--[功能标志 `gitaly_backup`](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/83254) removed.-->

备份 Rake 任务使用 `gitaly-backup` 二进制文件从 Gitaly 创建和恢复仓库备份。
`gitaly-backup` 取代了之前从极狐GitLab 直接调用 Gitaly 上的 RPC 的备份方法。

备份 Rake 任务必须能够找到此可执行文件。在大多数情况下，您不需要更改二进制文件的路径，因为它应该可以在默认路径 `/opt/gitlab/embedded/bin/gitaly-backup` 下正常工作。
如果您有特定原因要更改路径，可以在 Omnibus GitLab 包中进行配置：

1. 将以下内容添加到 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['backup_gitaly_backup_path'] = '/path/to/gitaly-backup'
   ```

1. [重新配置极狐GitLab](../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)，以使更改生效。

