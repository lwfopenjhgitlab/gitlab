---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 导入裸仓库（已废弃） **(FREE SELF)**

WARNING:
导入裸仓库的 Rake 任务废弃于 15.8 版本，将于 16.0 版本删除。

`gitlab:import:repos` Rake 任务的替代方案包括：

- 使用[导出文件](../user/project/settings/import_export.md)，或[直接传输](../user/group/import/index.md#migrate-groups-by-direct-transfer-recommended)迁移代码仓库。
- [通过 URL 导入](../user/project/import/repo_by_url.md)代码仓库。
- 导入[来自其他源头的代码仓库](../user/project/import/index.md)。

Rake 任务可用于将裸仓库导入极狐GitLab 实例。
从现有极狐GitLab 实例迁移时，为保留用户及其命名空间的所有权，迁移项目使用：

- [直接传输](../user/group/import/index.md#migrate-groups-by-direct-transfer-recommended)。
- [导出文件](../user/project/settings/import_export.md)。

导入代码仓库时：

- 项目的所有者是第一个管理员。
- 根据需要创建群组，包括子组。
- 群组的所有者是第一个管理员。
- 现有项目被跳过。
- 可以跳过哈希存储中的项目。详情请参见[从哈希存储导入裸仓库](#importing-bare-repositories-from-hashed-storage)。
- 现有 Git 仓库从磁盘移动过来（从原始路径中删除）。

将裸仓库导入到极狐GitLab 实例：

1. 创建一个新文件夹以从中导入您的 Git 仓库。
   您还可以将项目导入（子）组的命名空间，而不是管理员的命名空间。为此，请创建子文件夹并将这些子文件夹的所有权和读/写/执行权限授予
   `git` 用户及其群组：

   ```shell
   sudo -u git mkdir -p /var/opt/gitlab/git-data/repository-import-$(date "+%Y-%m-%d")/<optional_groupname>/<optional_subgroup>
   ```

1. 将您的裸仓库复制到这个新创建的文件夹中。请注意：

    - 在任何子文件夹中找到的任何 `.git` 仓库都会作为项目被导入。
    - 根据需要创建群组，可以是嵌套文件夹。

   例如，如果我们将仓库复制到 `/var/opt/gitlab/git-data/repository-import-2020-08-22`，并且仓库 `A` 需要在群组 `G1` 和 `G2` 下，它必须在这些文件夹下创建：
   `/var/opt/gitlab/git-data/repository-import-2020-08-22/G1/G2/A.git`。

   ```shell
   sudo cp -r /old/git/foo.git /var/opt/gitlab/git-data/repository-import-$(date "+%Y-%m-%d")/<optional_groupname>/<optional_subgroup>

   # Do this once when you are done copying git repositories
   sudo chown -R git:git /var/opt/gitlab/git-data/repository-import-$(date "+%Y-%m-%d")
   ```

   `foo.git` 需要由 `git` 用户和 `git` 用户组拥有。

   如果您从源代码安装，请将 `/var/opt/gitlab/` 替换为 `/home/git`。

1. 根据您的安装类型，运行以下命令：

    - Omnibus 安装

   ```shell
   sudo gitlab-rake gitlab:import:repos["/var/opt/gitlab/git-data/repository-import-$(date "+%Y-%m-%d")"]
   ```

   -源代码安装。在运行此命令之前，您需要更改到安装极狐GitLab 的目录：

   ```shell
   cd /home/git/gitlab
   sudo -u git -H bundle exec rake gitlab:import:repos["/var/opt/gitlab/git-data/repository-import-$(date "+%Y-%m-%d")"] RAILS_ENV=production
   ```

## 示例输出

```plaintext
Processing /var/opt/gitlab/git-data/repository-import-1/a/b/c/blah.git
 * Using namespace: a/b/c
 * Created blah (a/b/c/blah)
 * Skipping repo  /var/opt/gitlab/git-data/repository-import-1/a/b/c/blah.wiki.git
Processing /var/opt/gitlab/git-data/repository-import-1/abcd.git
 * Created abcd (abcd.git)
Processing /var/opt/gitlab/git-data/repository-import-1/group/xyz.git
 * Using namespace: group (2)
 * Created xyz (group/xyz.git)
 * Skipping repo /var/opt/gitlab/git-data/repository-import-1/@shared/a/b/abcd.git
[...]
```

<a id="importing-bare-repositories-from-hashed-storage"></a>

## 从哈希存储导入裸仓库

遗留存储中的项目具有镜像极狐GitLab 中的完整项目路径的目录结构，包括它们的命名空间结构。
裸仓库导入器使用这些信息，将项目导入到适当的位置。每个项目及其父命名空间都进行了有意义的命名。

但是，哈希存储中项目的目录结构不包含此信息。出于多种原因，这是有益的，尤其是在提高性能和保护数据完整性方面。详情请参见[仓库存储类型](../administration/repository_storage_types.md)。

可导入的仓库取决于极狐GitLab 的版本。

### 极狐GitLab 10.3 及更早版本

不支持从哈希存储导入裸仓库。

### 极狐GitLab 10.4 及更高版本

为了支持从散列存储导入裸仓库，极狐GitLab 10.4 及更高版本将完整项目路径存储在每个仓库内部中 Git 仓库配置文件内的一个特殊位置。位置格式如下：

```ini
[gitlab]
  fullpath = gitlab-org/gitlab
```

但是，现有仓库未迁移以包含此路径。

如果在极狐GitLab 10.4 及更高版本中发生以下事件，则可以导入裸仓库：

- 创建
- 迁移到哈希存储
- 重命名
- 转移到另一个命名空间
- 祖先更名
- 祖先转移到另一个命名空间

  如果以下所有关于仓库的情况都属实，那么极狐GitLab 10.4 **不能**将裸仓库导入到 11.6 中：

- 仓库是在极狐GitLab 10.3 或更早版本中创建的。
- 在极狐GitLab 10.4 到 11.6 中，仓库没有被重命名、转移或迁移到[哈希存储](../administration/repository_storage_types.md#hashed-storage)。
- 在极狐GitLab 10.4 到 11.6 中，仓库的祖先命名空间没有被重命名或转移。

<!--[In GitLab 11.6](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/41776) and later, all
bare repositories are importable.

To manually migrate repositories yourself (for GitLab 10.4 to GitLab 11.6), you can use the
[Rails console](../administration/operations/rails_console.md#starting-a-rails-console-session)
to do so. In a Rails console session, run the following to migrate a project:
-->

```ruby
project = Project.find_by_full_path('gitlab-org/gitlab')
project.set_full_path
```

在 Rails 控制台会话中，运行以下命令来迁移所有命名空间的项目（如果命名空间中有几千个项目，这可能需要一段时间）：

```ruby
namespace = Namespace.find_by_full_path('gitlab-org')
namespace.send(:write_projects_repository_config)
```

