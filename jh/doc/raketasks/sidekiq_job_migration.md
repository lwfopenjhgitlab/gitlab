---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Sidekiq 作业迁移 **(FREE SELF)**

WARNING:
这种操作非常少见。我们建议绝大多数极狐GitLab 实例不要使用。

Sidekiq 路由规则允许管理员将某些后台作业从常规队列重新路由到备用队列。默认情况下，极狐GitLab 为每个后台作业类型使用一个队列。 极狐GitLab 有超过 400 种后台作业类型，因此相应地有超过 400 个队列。

大多数管理员不需要更改此设置。在某些具有特别大的后台作业处理工作负载的情况下，Redis 性能可能会由于极狐GitLab 侦听的队列数量而受到影响。

如果 Sidekiq 路由规则发生更改，管理员需要小心迁移以避免完全丢失工作。基本的迁移步骤是：

1. 同时侦听旧队列和新队列。
1. 更新路由规则。
1. 等到没有发布者向旧队列分派作业。
1. 运行[未来作业的 Rake 任务](#future-jobs)。
1. 等待旧队列为空。
1. 停止侦听旧队列。

<a id="future-jobs"></a>

## 未来作业

步骤 4 涉及为已存储在 Redis 中但将在未来运行的作业重写一些 Sidekiq 作业数据。将来有两组作业要运行：计划作业和要重试的作业。我们提供一个单独的 Rake 任务来迁移每个集合：

- `gitlab:sidekiq:migrate_jobs:retry`：对于要重试的作业。
- `gitlab:sidekiq:migrate_jobs:scheduled` 对于计划作业。

大多数情况下，同时运行两者是正确的选择。有两个单独的任务允许在需要时进行更细粒度的控制。要同时运行两者：

```shell
# omnibus-gitlab
sudo gitlab-rake gitlab:sidekiq:migrate_jobs:retry gitlab:sidekiq:migrate_jobs:schedule

# source installations
bundle exec rake gitlab:sidekiq:migrate_jobs:retry gitlab:sidekiq:migrate_jobs:schedule RAILS_ENV=production
```

