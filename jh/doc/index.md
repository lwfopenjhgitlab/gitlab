---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
comments: false
description: 'Learn how to use and administer GitLab, the most scalable Git-based fully integrated platform for software development.'
---

<div class="d-none">
  <h3>访问 <a href="https://docs.gitlab.cn/jh/">docs.gitlab.cn</a> 获取此帮助信息的最新版本。</h3>
</div>
<!-- the div above will not display on the docs site but will display on /help -->

# 极狐GitLab 文档

欢迎来到[极狐GitLab](https://about.gitlab.cn/) 文档中心。

在这里，您可以访问极狐GitLab 的文档，极狐GitLab 作为单个应用程序，可以完成[整个 DevOps 生命周期](#the-entire-devops-lifecycle)。

## 概述

无论您如何使用极狐GitLab，我们都有适合您的文档。

| 必要文档 | 必要文档 |
|:------------------------|:------------------------|
| [**用户文档**](user/index.md)<br>帮助极狐GitLab 一般用户了解功能和概念。                          | [**管理员文档**](administration/index.md)<br/>帮助极狐GitLab 私有化部署版的管理员。 |
| [**从另一个平台来到极狐GitLab？**](#coming-to-gitlab-from-another-platform)<br/>查看我们的指南。                        | [**Git 和极狐GitLab 的新手？**](tutorials/make_your_first_git_commit.md)<br/>帮助您入门。 |
| [**安装极狐GitLab**](https://gitlab.cn/install/)<br/>不同平台的安装选项。            | [**极狐GitLab 技术支持**](https://gitlab.cn/support/)<br/>联系技术支持。 |

## 热门话题

| 热门话题                                                                              | 描述 |
|:-------------------------------------------------------------------------------------------|:------------|
| [双重验证](user/profile/account/two_factor_authentication.md)             | 提高极狐GitLab 帐户的安全性。 |
| [极狐GitLab 群组](user/group/index.md)                                                       | 在一处管理项目。 |
| [`.gitlab-ci.yml` 文件的关键字参考](ci/yaml/index.md)                        | `.gitlab-ci.yml` 文件的可用配置选项。 |
| [使用许可证激活极狐GitLab](user/admin_area/license.md)                            | 使用许可证激活极狐GitLab 专业版、旗舰版功能。 |
| [Omnibus 数据库设置](https://docs.gitlab.cn/omnibus/settings/database.html) | Omnibus 私有化部署实例的数据库设置。 |
| [Omnibus NGINX 设置](https://docs.gitlab.cn/omnibus/settings/nginx.html)       | Omnibus 私有化部署实例的 NGINX 设置。 |
| [Omnibus SSL 配置](https://docs.gitlab.cn/omnibus/settings/ssl.html)      | Omnibus 私有化部署实例的 SSL 设置。 |

<!--
| [GitLab.com settings](user/gitlab_com/index.md)                                            | Settings used for GitLab.com. |
| [Back up and restore GitLab](raketasks/backup_restore.md)                                  | Rake tasks for backing up and restoring GitLab self-managed instances. |
| [GitLab release and maintenance policy](policy/maintenance.md)                             | Policies for version naming and cadence, and also upgrade recommendations. |
| [Elasticsearch integration](integration/elasticsearch.md)                                  | Integrate Elasticsearch with GitLab to enable advanced searching. |
-->

<a id="the-entire-devops-lifecycle"></a>

## 完整的 DevOps 生命周期

极狐GitLab 是支持并发 DevOps 的软件开发、安全和操作的单一应用程序，使软件生命周期更快，并从根本上提高了业务速度。

极狐GitLab 为 [DevOps 生命周期的每个阶段](https://gitlab.cn/stages-devops-lifecycle/)提供解决方案。

### 用户帐户

了解有关极狐GitLab 帐户管理的更多信息：

| 话题                                                      | 描述 |
|:-----------------------------------------------------------|:------------|
| [用户帐户](user/profile/index.md)                      | 管理您的帐户。 |
| [用户设置](user/profile/index.md#access-your-user-settings) | 管理您的用户设置，双重身份验证等。 |

<!--
| [验证](topics/authentication/index.md)           | Account security with two-factor authentication, set up your SSH keys, and deploy keys for secure access to your projects. |
| [User permissions](user/permissions.md)                    | Learn what each role in a project can do. |
-->

<a id="coming-to-gitlab-from-another-platform"></a>

## 从另一个平台来到极狐GitLab

如果您从另一个平台来到极狐GitLab，以下信息很有用：

| 话题                                               | 描述 |
|:----------------------------------------------------|:------------|
| [导入极狐GitLab](user/project/import/index.md) | 将您的项目从 GitHub、Bitbucket 和 Gitee 导入极狐GitLab。 |
| [从 SVN 迁移](user/project/import/svn.md)    | 将 SVN 仓库转换到 Git 和极狐GitLab。 |

<!--
## Build an integration with GitLab

There are many ways to integrate with GitLab, including:

| Topic                                      | Description |
|:-------------------------------------------|:------------|
| [GitLab REST API](api/index.md)           | Integrate with GitLab using our REST API. |
| [GitLab GraphQL API](api/graphql/index.md) | Integrate with GitLab using our GraphQL API. |
| [Integrations](integration/index.md)      | Integrations with third-party products. |

## Contributing to GitLab

GitLab Community Edition is [open source](https://gitlab.com/gitlab-org/gitlab-foss/)
and GitLab Enterprise Edition is [open-core](https://gitlab.com/gitlab-org/gitlab/).

Learn how to contribute to GitLab with the following resources:

| Topic                                                       | Description |
|:------------------------------------------------------------|:------------|
| [Development](development/index.md)                        | How to contribute to GitLab development. |
| [Legal](legal/index.md)                                    | Contributor license agreements. |
| [Writing documentation](development/documentation/index.md) | How to contribute to GitLab Docs. |
-->
