---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: tutorial
---

# 使用 Dpl 作为部署工具 **(FREE)**

[Dpl](https://github.com/travis-ci/dpl) 是一个为持续部署而设计的部署工具，由 Travis CI 开发和使用，但也可以与 GitLab CI/CD 一起使用。

Dpl 可用于部署到任何[支持的 providers](https://github.com/travis-ci/dpl#supported-providers)。

## 要求

要使用 Dpl，您至少需要能够安装 gem 的 Ruby 1.9.3。

## 基本用法

Dpl 可以安装在任何机器上：

```shell
gem install dpl
```

允许您从本地终端测试所有命令，而不必在 CI 服务器上进行测试。

如果您没有安装 Ruby，你可以在 Debian 兼容的 Linux 上安装：

```shell
apt-get update
apt-get install ruby-dev
```

Dpl 为大量服务提供支持，包括：Heroku、Cloud Foundry、AWS/S3 等。
要使用它，只需定义 provider 和 provider 所需的任何其他参数。

例如，如果您想用它来部署你的应用程序到 Heroku，您需要指定 `heroku` 作为 provider，指定 `api_key` 和 `app`。
所有可能的参数都可以在 [Heroku API 部分](https://github.com/travis-ci/dpl#heroku-api) 中找到。

```yaml
staging:
  stage: deploy
  script:
    - gem install dpl
    - dpl heroku api --app=my-app-staging --api_key=$HEROKU_STAGING_API_KEY
  environment: staging
```

在上面的示例中，我们使用 Dpl 将 `my-app-staging` 部署到 Heroku 服务器，API 密钥存储在 `HEROKU_STAGING_API_KEY` 安全变量中。

要使用不同的提供商，请查看[支持的 provider](https://github.com/travis-ci/dpl#supported-providers) 的长列表。

## 将 Dpl 与 Docker 一起使用

在大多数情况下，您将 GitLab Runner 配置为使用服务器的 shell 命令。
这意味着所有命令都在本地用户的上下文中运行（例如 `gitlab_runner` 或 `gitlab_ci_multi_runner`）。
这也意味着您的 Docker 容器中很可能没有安装 Ruby 运行时。
您必须安装它：

```yaml
staging:
  stage: deploy
  script:
    - apt-get update -yq
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl heroku api --app=my-app-staging --api_key=$HEROKU_STAGING_API_KEY
  only:
    - main
  environment: staging
```

第一行 `apt-get update -yq` 更新可用软件包列表，其中第二行 `apt-get install -y ruby-dev` 在系统上安装 Ruby 运行时。
上述示例适用于所有 Debian 兼容系统。

## 在 staging 和生产中的使用

在开发工作流程中，拥有 staging（开发）和生产环境是很常见的。

让我们考虑以下示例：我们希望将 `main` 分支部署到 `staging` 并将所有标签部署到 `production` 环境。
该设置的最终 `.gitlab-ci.yml` 如下所示：

```yaml
staging:
  stage: deploy
  script:
    - gem install dpl
    - dpl heroku api --app=my-app-staging --api_key=$HEROKU_STAGING_API_KEY
  only:
    - main
  environment: staging

production:
  stage: deploy
  script:
    - gem install dpl
    - dpl heroku api --app=my-app-production --api_key=$HEROKU_PRODUCTION_API_KEY
  only:
    - tags
  environment: production
```

我们创建了两个在不同事件上执行的部署作业：

- `staging`：对推送到 `main` 分支的所有提交执行
- `production`：对所有推送的标签执行

我们还使用了两个安全变量：

- `HEROKU_STAGING_API_KEY`：用于部署 staging 应用的 Heroku API 密钥
- `HEROKU_PRODUCTION_API_KEY`：用于部署生产应用的 Heroku API 密钥

## 存储 API 密钥

要将 API 密钥存储为安全变量：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏，选择 **设置 > CI/CD**。
1. 展开 **变量**。

项目设置中定义的变量与构建脚本一起发送给 runner。
安全变量存储在仓库之外。永远不要将 secret 存储在项目的 `.gitlab-ci.yml` 文件中。将 secrets 在作业日志中隐藏也很重要。

您可以通过在其名称前加上 `$`（在非 Windows runner 上）或 `%`（对于 Windows Batch runner）来访问添加的变量：

- `$VARIABLE`：用于非 Windows runners
- `%VARIABLE%`：用于 Windows Batch runners

阅读有关 [CI/CD 变量](../../variables/index.md) 的更多信息。