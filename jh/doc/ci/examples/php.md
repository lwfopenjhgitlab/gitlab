---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: tutorial
---

# 测试 PHP 项目 **(FREE)**

本指南涵盖 PHP 项目的基本构建说明。

涵盖了两个测试场景：使用 Docker executor 和使用 Shell executor。

## 使用 Docker executor 测试 PHP 项目

虽然可以在任何系统上测试 PHP 应用程序，但需要开发人员手动配置。为了克服这个问题，我们使用可以在 Docker Hub 中找到的官方 [PHP Docker 镜像](https://hub.docker.com/_/php)。

这使我们能够针对不同版本的 PHP 测试 PHP 项目。
但是，并非所有东西都是即插即用的，您仍然需要手动配置一些东西。

与每项工作一样，您需要创建一个有效的 `.gitlab-ci.yml` 来描述构建环境。

让我们首先指定用于作业流程的 PHP 镜像。
<!--(You can read more about what an image means in the runner's lingo reading
about [Using Docker images](../docker/using_docker_images.md#what-is-an-image).)-->

首先将镜像添加到您的`.gitlab-ci.yml`：

```yaml
image: php:5.6
```

官方镜像很棒，但缺少一些有用的测试工具。
我们需要先准备好构建环境。克服这个问题的一种方法是创建一个脚本，在实际测试完成之前安装所有先决条件。

让我们在仓库的根目录中创建一个 `ci/docker_install.sh` 文件，其内容如下：

```shell
#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install git -yqq

# Install phpunit, the tool that we will use for testing
curl --location --output /usr/local/bin/phpunit "https://phar.phpunit.de/phpunit.phar"
chmod +x /usr/local/bin/phpunit

# Install mysql driver
# Here you can install any other extension that you need
docker-php-ext-install pdo_mysql
```

您可能想知道 `docker-php-ext-install` 是什么。简而言之，它是官方 PHP Docker 镜像提供的脚本，您可以使用它来轻松安装扩展。有关更多信息，请阅读[文档](https://hub.docker.com/_/php)。

现在我们创建了包含构建环境的所有先决条件的脚本，让我们将它添加到 `.gitlab-ci.yml` 中：

```yaml
before_script:
  - bash ci/docker_install.sh > /dev/null
```

最后一步，使用 `phpunit` 运行实际测试：

```yaml
test:app:
  script:
    - phpunit --configuration phpunit_myapp.xml
```

最后，提交您的文件并将它们推送到极狐GitLab，查看您的构建是否成功（或失败）。

最终的 `.gitlab-ci.yml` 应该类似于：

```yaml
# Select image from https://hub.docker.com/_/php
image: php:5.6

before_script:
  # Install dependencies
  - bash ci/docker_install.sh > /dev/null

test:app:
  script:
    - phpunit --configuration phpunit_myapp.xml
```

### 在 Docker 构建中针对不同的 PHP 版本进行测试

针对多个版本的 PHP 进行测试非常容易。只需添加另一个具有不同 Docker 镜像版本的作业，然后 runner 完成其余的工作：

```yaml
before_script:
  # Install dependencies
  - bash ci/docker_install.sh > /dev/null

# We test PHP5.6
test:5.6:
  image: php:5.6
  script:
    - phpunit --configuration phpunit_myapp.xml

# We test PHP7.0 (good luck with that)
test:7.0:
  image: php:7.0
  script:
    - phpunit --configuration phpunit_myapp.xml
```

### Docker 构建中的自定义 PHP 配置

有时您需要通过将 `.ini` 文件放入 `/usr/local/etc/php/conf.d/` 来自定义 PHP 环境。为此添加一个 `before_script` action：

```yaml
before_script:
  - cp my_php.ini /usr/local/etc/php/conf.d/test.ini
```

当然，`my_php.ini` 必须存在于仓库的根目录中。

## 使用 Shell executor 测试 PHP 项目

Shell executor 在您的服务器上的终端会话中运行您的作业。要测试您的项目，您必须首先确保安装了所有依赖项。

例如，在运行 Debian 8 的 VM 中，首先更新缓存，然后安装 `phpunit` 和 `php5-mysql`：

```shell
sudo apt-get update -y
sudo apt-get install -y phpunit php5-mysql
```

接下来，将以下代码段添加到您的 `.gitlab-ci.yml` 中：

```yaml
test:app:
  script:
    - phpunit --configuration phpunit_myapp.xml
```

最后，推送到极狐GitLab 并开始测试！

### 在 Shell 构建中针对不同的 PHP 版本进行测试

[phpenv](https://github.com/phpenv/phpenv) 项目允许您轻松管理不同版本的 PHP，每个版本都有自己的配置。这在使用 Shell executor 测试 PHP 项目时特别有用。

您必须按照[上游安装指南](https://github.com/phpenv/phpenv#installation)，在 `gitlab-runner` 用户下将它安装在构建机器上。

使用 phpenv 还可以轻松配置 PHP 环境：

```shell
phpenv config-add my_config.ini
```

***重要提示：**似乎 `phpenv/phpenv` [被放弃了](https://github.com/phpenv/phpenv/issues/57)。[`madumlao/phpenv`](https://github.com/madumlao/phpenv) 有一个派生。[`CHH/phpenv`](https://github.com/CHH/phpenv) 似乎也是一个不错的选择。选择任何提到的工具都适用于基本的 phpenv 命令。指导您选择正确的 phpenv 超出了本教程的范围。*

### 安装自定义扩展

由于这是 PHP 环境的一个非常简单的安装，您可能需要一些当前在构建机器上不存在的扩展。

要安装其他扩展，只需执行：

```shell
pecl install <extension>
```

不建议将此添加到 `.gitlab-ci.yml`。 您应该执行一次此命令，仅用于设置构建环境。

## 扩展您的测试

### 使用 `atoum`

除了 PHPUnit，您可以使用任何其他工具来运行单元测试。例如，您可以使用 [`atoum`](https://github.com/atoum/atoum)：

```yaml
before_script:
  - wget http://downloads.atoum.org/nightly/mageekguy.atoum.phar

test:atoum:
  script:
    - php mageekguy.atoum.phar
```

### 使用 Composer

大多数 PHP 项目使用 Composer 来管理他们的 PHP 包。
要在运行测试之前执行 Composer，请将以下内容添加到您的 `.gitlab-ci.yml` 中：

```yaml
# Composer stores all downloaded packages in the vendor/ directory.
# Do not use the following if the vendor/ directory is committed to
# your git repository.
cache:
  paths:
    - vendor/

before_script:
  # Install composer dependencies
  - wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig
  - php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
  - php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
  - php composer-setup.php
  - php -r "unlink('composer-setup.php'); unlink('installer.sig');"
  - php composer.phar install
```

## 访问私有包或依赖项

如果您的测试套件需要访问私有存储库，则需要配置 [SSH 密钥](../ssh_keys/index.md) 以便能够克隆它。

## 使用数据库或其他服务

大多数时候，您需要一个正在运行的数据库才能让您的测试运行。如果您使用的是 Docker executor，则可以利用 Docker 链接到其他容器的能力。使用 GitLab Runner，这可以通过定义 `service` 来实现。

此功能包含在 [CI 服务](../services/index.md) 文档中。

## 在本地测试

使用 GitLab Runner 1.0，您还可以在本地测试任何更改。 从您的终端执行：

```shell
# Check using docker executor
gitlab-runner exec docker test:app

# Check using shell executor
gitlab-runner exec shell test:app
```

<!--
## Example project

We have set up an [Example PHP Project](https://gitlab.com/gitlab-examples/php) for your convenience
that runs on [GitLab.com](https://gitlab.com) using our publicly available
[shared runners](../runners/index.md).

Want to hack on it? Simply fork it, commit, and push your changes. Within a few
moments the changes are picked by a public runner and the job begins.
-->