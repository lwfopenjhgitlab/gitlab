---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用 semantic-release 将 npm 包发布到 GitLab Package Registry **(FREE)**

本指南演示如何使用 [semantic-release](https://github.com/semantic) 自动将 npm 包发布到 [GitLab Package Registry](https://github.com/semantic-release/semantic-release)。

<!--
You can also view or fork the complete [example source](https://gitlab.com/gitlab-examples/semantic-release-npm).
-->

## 初始化模块

1. 打开终端并导航到项目的仓库。
1. 运行 `npm init`。根据 [Package Registry 的命名约定](../../user/packages/npm_registry/index.md#包命名约定)。例如项目的路径是 `gitlab-examples/semantic-release-npm`，则将模块命名为 `@gitlab-examples/semantic-release-npm`。

1. 安装以下 npm 包：

   ```shell
   npm install semantic-release @semantic-release/git @semantic-release/gitlab @semantic-release/npm --save-dev
   ```

1. 将以下属性添加到模块的 `package.json` 中：

   ```json
   {
     "scripts": {
       "semantic-release": "semantic-release"
     },
     "publishConfig": {
       "access": "public"
     },
     "files": [ <path(s) to files here> ]
   }
   ```

1. 使用 glob 模式更新 `files` 键，该模式选择应包含在已发布模块中的所有文件。关于 `files` 的更多信息可以在 [在 npm 的文档中](https://docs.npmjs.com/cli/v6/configuring-npm/package-json/#files) 中找到。

1. 将 `.gitignore` 文件添加到项目中，避免提交 `node_modules`：

   ```plaintext
   node_modules
   ```

## 配置流水线

使用以下内容创建一个 `.gitlab-ci.yml`：

```yaml
default:
  image: node:latest
  before_script:
    - npm ci --cache .npm --prefer-offline
    - |
      {
        echo "@${CI_PROJECT_ROOT_NAMESPACE}:registry=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/"
        echo "${CI_API_V4_URL#https?}/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=\${CI_JOB_TOKEN}"
      } | tee -a .npmrc
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - .npm/

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH

variables:
  NPM_TOKEN: ${CI_JOB_TOKEN}

stages:
  - release

publish:
  stage: release
  script:
    - npm run semantic-release
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

此示例使用单个作业 `publish` 配置流水线，该作业运行 `semantic-release`。semantic-release 库发布新版本的 npm 包并创建新的 GitLab 发布（如有必要）。

默认的 `before_script` 会生成一个临时的 `.npmrc`，用于在 `publish` 作业期间对软件包库进行身份验证。

## 设置 CI/CD 变量

作为发布包的一部分，semantic-release 发布会增加 `package.json` 中的版本号。对于 semantic-release 来提交此更改并将其推送回极狐GitLab，流水线需要一个名为 `GITLAB_TOKEN` 的自定义 CI/CD 变量。要创建此变量：

<!-- markdownlint-disable MD044 -->
1. 在顶部栏的右上角，选择您的头像。
1. 在左侧边栏，选择 **访问令牌**。
1. 在 **令牌名称** 框中，输入令牌名称。
1. 在 **选择范围** 下，选中 **api** 复选框。
1. 选择 **创建项目访问令牌**。
1. 复制值。
1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏，选择 **设置 > CI/CD**。
1. 展开 **变量**。
1. 选择 **添加变量**。
1. 在 **Key** 框中，输入 `GITLAB_TOKEN`。
1. 在 **Value** 框中，粘贴令牌。
1. 选择 **隐藏变量** 复选框。
1. 选择 **添加变量**。
<!-- markdownlint-enable MD044 -->

## 配置 semantic-release

semantic-release 从项目中的 .releaserc.json 文件中提取其配置信息。在仓库的根目录创建一个 `.releaserc.json`：

```json
{
  "branches": ["master"],
  "plugins": [
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",
    "@semantic-release/gitlab",
    "@semantic-release/npm",
    [
      "@semantic-release/git",
      {
        "assets": ["package.json"],
        "message": "chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}"
      }
    ]
  ]
}
```

## 开始发布 releases

通过使用以下消息创建提交来测试流水线：

```plaintext
fix: testing patch releases
```

将提交推送到默认分支。流水线应在项目的 **发布** 页面上创建新版本 (`v1.0.0`)，并将包的新版本发布到项目的 **软件包库** 页面。

要创建次要版本，请使用如下提交消息：

```plaintext
feat: testing minor releases
```

或者，对于重大更改：

```plaintext
feat: testing major releases

BREAKING CHANGE: This is a breaking change.
```

有关提交消息如何映射到版本的更多信息可以在 [semantic-releases 的文档](https://github.com/semantic-release/semantic-release#how-does-it-work)中找到。

## 在项目中使用模块

要使用已发布的模块，请将 `.npmrc` 文件添加到依赖于该模块的项目中。例如：<!--例如，使用[示例项目](https://gitlab.com/gitlab-examples/semantic-release-npm)的模块：-->

```plaintext
@gitlab-examples:registry=https://gitlab.com/api/v4/packages/npm/
```

然后，安装模块：

```shell
npm install --save @gitlab-examples/semantic-release-npm
```
