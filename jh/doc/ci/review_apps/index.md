---
stage: Verify
group: Testing
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# Review Apps **(FREE)**

Review Apps 是一种协作工具，可帮助提供展示产品更改的环境。

NOTE:
如果您有 Kubernetes 集群，则可以使用 Auto DevOps<!--[Auto DevOps](../../topics/autodevops/index.md)--> 在您的应用程序中自动执行此功能。

Review Apps：

- 通过为您的合并请求启动动态环境，提供对功能分支中所做更改的自动实时预览。
- 允许设计师和产品经理查看您的更改，而无需检出您的分支并在沙盒环境中运行您的更改。
- 与 GitLab DevOps LifeCycle<!--[GitLab DevOps LifeCycle](../../index.md#the-entire-devops-lifecycle)--> 完全集成。
- 允许您随时随地部署更改。

![Review Apps Workflow](img/continuous-delivery-review-apps.svg)

在前面的例子中：

- 每次将提交推送到 `topic branch` 时都会构建一个 Review App。
- 审核人在通过第三次审核之前未通过两次审核。
- 审核通过后，`topic branch` 被合并到默认分支，在那里它被部署到 staging。
- 在 staging 被批准后，合并到默认分支的更改将部署到生产中。

## Review Apps 工作原理

Review App 是分支与[环境](../environments/index.md)的映射。
可以通过与分支相关的[合并请求](../../user/project/merge_requests/index.md)上的链接访问 Review App。

以下是动态设置环境的合并请求示例。

![Review App in merge request](img/review_apps_preview_in_mr.png)

在这个例子中，一个分支：

- 成功构建。
- 部署在动态环境下，可以通过选择 **查看应用** 访问该环境。

将 Review Apps 添加到您的工作流程后，您可以遵循分支的 Git 流程：

1. 推送一个分支，让 runner 根据动态环境作业的 `script` 定义部署 Review App。
1. 等待 runner 构建和部署您的 Web 应用程序。
1. 要实时查看更改，请选择与分支相关的合并请求中的链接。

## 配置 Review Apps

Review Apps 建立在[动态环境](../environments/index.md#创建动态环境)之上，它允许您为每个分支动态创建一个新环境。

配置 Review Apps 的过程如下：

1. 设置基础架构以托管和部署 Review Apps（查看下面的[示例](#review-apps-示例)）。
1. 安装和配置一个 runner 进行部署。
1. 在 `.gitlab-ci.yml` 中设置一个作业，使用预定义的 CI/CD 变量 `${CI_COMMIT_REF_SLUG}` 来创建动态环境并将其限制为仅在分支上运行。或者，您可以通过[启用 Review Apps](#enable-review-apps-button) 为您的项目获取此作业的 YAML 模板。
1. （可选）设置[手动停止](../environments/index.md#stopping-an-environment) Review Apps 的作业。

<a id="enable-review-apps-button"></a>

### 启用 Review Apps 按钮

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/118844) in GitLab 12.8.
-->

为项目配置 Review Apps 时，您将新作业添加到 `.gitlab-ci.yml` 文件，如上所述。为方便起见，如果您使用 Kubernetes，您可以选择 **启用 Review Apps**，系统会提示您一个模板代码块，您可以将其复制并粘贴到 `.gitlab-ci.yml` 作为起点。

先决条件：

- 您至少需要该项目的开发者角色。

要使用 Review Apps 模板：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您要为其创建 Review App 作业的项目。
1. 在左侧边栏上，选择 **部署 > 环境**。
1. 选择 **启用 Review Apps**。
1. 复制提供的代码片段并将其粘贴到您的 `.gitlab-ci.yml` 文件中：

   ![Enable Review Apps modal](img/enable_review_app_v12_8.png)

您可以根据需要编辑此模板。

## Review Apps 自动停止

了解如何在给定的时间段后[配置 Review Apps 环境以使其过期并自动停止](../environments/index.md#在特定时间段后停止环境)。

## Review Apps 示例

以下是演示 Review App 配置的示例项目：

- [NGINX](https://gitlab.com/gitlab-examples/review-apps-nginx).
- [OpenShift](https://gitlab.com/gitlab-examples/review-apps-openshift).
- [HashiCorp Nomad](https://gitlab.com/gitlab-examples/review-apps-nomad).

<!--
Other examples of Review Apps:

- <i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
[Cloud Native Development with GitLab](https://www.youtube.com/watch?v=jfIyQEwrocw).
- [Review Apps for Android](https://about.gitlab.com/blog/2020/05/06/how-to-create-review-apps-for-android-with-gitlab-fastlane-and-appetize-dot-io/).
-->

<a id="route-maps"></a>

## 路由映射

路由映射（Route Maps）允许您直接从源文件转到为 Review Apps 定义的[环境](../environments/index.md)上的公共页面。

设置后，合并请求部件中的 Review App 链接可以将您直接带到更改的页面，从而更轻松、更快速地预览提议的修改。

配置路由映射告诉极狐GitLab，如何使用路由映射将仓库中的文件路径映射到网站页面的路径。
设置后，系统会显示 **View on ...** 按钮，可将您带到直接从合并请求更改的页面。

要设置路由映射，请在仓库中的 `.gitlab/route-map.yml` 中添加一个文件，其中包含一个 YAML 数组，该数组将 `source` 路径（在存储库中）映射到 `public` 路径（在网站上））。

<!--
### 路由映射示例

The following is an example of a route map for [Middleman](https://middlemanapp.com),
a static site generator (SSG) used to build the [GitLab website](https://about.gitlab.com),
deployed from its [project on GitLab.com](https://gitlab.com/gitlab-com/www-gitlab-com):

```yaml
# Team data
- source: 'data/team.yml'  # data/team.yml
  public: 'team/'  # team/

# Blogposts
- source: /source\/posts\/([0-9]{4})-([0-9]{2})-([0-9]{2})-(.+?)\..*/  # source/posts/2017-01-30-around-the-world-in-6-releases.html.md.erb
  public: '\1/\2/\3/\4/'  # 2017/01/30/around-the-world-in-6-releases/

# HTML files
- source: /source\/(.+?\.html).*/  # source/index.html.haml
  public: '\1'  # index.html

# Other files
- source: /source\/(.*)/  # source/images/blogimages/around-the-world-in-6-releases-cover.png
  public: '\1'  # images/blogimages/around-the-world-in-6-releases-cover.png
```

Mappings are defined as entries in the root YAML array, and are identified by a `-` prefix. Within an entry, there is a hash map with two keys:

- `source`
  - A string, starting and ending with `'`, for an exact match.
  - A regular expression, starting and ending with `/`, for a pattern match:
    - The regular expression needs to match the entire source path - `^` and `$` anchors are implied.
    - Can include capture groups denoted by `()` that can be referred to in the `public` path.
    - Slashes (`/`) can, but don't have to, be escaped as `\/`.
    - Literal periods (`.`) should be escaped as `\.`.
- `public`, a string starting and ending with `'`.
  - Can include `\N` expressions to refer to capture groups in the `source` regular expression in order of their occurrence, starting with `\1`.

The public path for a source path is determined by finding the first
`source` expression that matches it, and returning the corresponding
`public` path, replacing the `\N` expressions with the values of the
`()` capture groups if appropriate.

In the example above, the fact that mappings are evaluated in order
of their definition is used to ensure that `source/index.html.haml`
matches `/source\/(.+?\.html).*/` instead of `/source\/(.*)/`,
and results in a public path of `index.html`, instead of
`index.html.haml`.

After you have the route mapping set up, it takes effect in the following locations:

- In the merge request widget:
  - The **View app** button takes you to the environment URL set in the `.gitlab-ci.yml` file.
  - The list shows the first 5 matched items from the route map, but you can filter them if more
    than 5 are available.

    ![View app file list in merge request widget](img/view_on_mr_widget.png)

- In the diff for a comparison or commit.

  ![View on environment button in merge request diff](img/view_on_env_mr.png)

- In the blob file view.

  ![View on environment button in file view](img/view_on_env_blob.png)


## Visual Reviews **(PREMIUM)**


> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/10761) in GitLab 12.0.
> - [Moved](https://about.gitlab.com/blog/2021/01/26/new-gitlab-product-subscription-model/) to GitLab Premium in 13.9.
> - It's [deployed behind a feature flag](../../user/feature_flags.md), enabled by default.
> - It's enabled on GitLab.com.
> - For GitLab self-managed instances, GitLab administrators can opt to [disable it](#enable-or-disable-visual-reviews). **(PREMIUM SELF)**


借助 Visual Reviews，任何团队（产品、设计、质量等）的成员都可以通过您的 review apps 中的表单提供反馈意见。评论被添加到触发 review apps 的合并请求中。

### 使用 Visual Reviews

After Visual Reviews has been [configured](#configure-review-apps-for-visual-reviews) for the
Review App, the Visual Reviews feedback form is overlaid on the right side of every page.

![Visual review feedback form](img/toolbar_feedback_form_v13_5.png)

To use the feedback form to make a comment in the merge request:

1. On the right side of a page, select the **Review** tab.
1. Make a comment on the visual review. You can make use of all the
   [Markdown annotations](../../user/markdown.md) that are also available in
   merge request comments.
1. Enter your personal information:
   - If [`data-require-auth`](#authentication-for-visual-reviews) is `true`, you must enter your [personal access token](../../user/profile/personal_access_tokens.md).
   - Otherwise, enter your name, and optionally your email.
1. Select **Send feedback**.

<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
To see Visual reviews in action, see the [Visual Reviews Walk through](https://youtu.be/1_tvWTlPfM4).

### 配置 Review Apps 使用 Visual Reviews

The feedback form is served through a script you add to pages in your Review App.
It should be added to the `<head>` of your application and
consists of some project and merge request specific values. Here's how it
looks for a project with code hosted in a project on GitLab.com:

```html
<script
  data-project-id='11790219'
  data-merge-request-id='1'
  data-mr-url='https://gitlab.com'
  data-project-path='sarah/review-app-tester'
  data-require-auth='true'
  id='review-app-toolbar-script'
  src='https://gitlab.com/assets/webpack/visual_review_toolbar.js'>
</script>
```

Ideally, you should use [CI/CD variables](../variables/predefined_variables.md)
to replace those values at runtime when each review app is created:

- `data-project-id` is the project ID, which can be found by the `CI_PROJECT_ID`
  variable.
- `data-merge-request-id` is the merge request ID, which can be found by the
  `CI_MERGE_REQUEST_IID` variable. `CI_MERGE_REQUEST_IID` is available only if
  [`only: [merge_requests]`](../pipelines/merge_request_pipelines.md)
  is used and the merge request is created.
- `data-mr-url` is the URL of the GitLab instance and is the same for all
  review apps.
- `data-project-path` is the project's path, which can be found by `CI_PROJECT_PATH`.
- `data-require-auth` is optional for public projects but required for [private and internal ones](#authentication-for-visual-reviews). If this is set to `true`, the user is required to enter their [personal access token](../../user/profile/personal_access_tokens.md) instead of their name and email.
- `id` is always `review-app-toolbar-script`, you don't need to change that.
- `src` is the source of the review toolbar script, which resides in the
  respective GitLab instance and is the same for all review apps.

For example, in a Ruby application with code hosted on in a project GitLab.com, you would need to have this script:

```html
<script
  data-project-id="ENV['CI_PROJECT_ID']"
  data-merge-request-id="ENV['CI_MERGE_REQUEST_IID']"
  data-mr-url='https://gitlab.com'
  data-project-path="ENV['CI_PROJECT_PATH']"
  id='review-app-toolbar-script'
  src='https://gitlab.com/assets/webpack/visual_review_toolbar.js'>
</script>
```

Then, when your app is deployed via GitLab CI/CD, those variables should get
replaced with their real values.

### Determining merge request ID

The visual review tools retrieve the merge request ID from the `data-merge-request-id`
data attribute included in the `script` HTML tag used to add the visual review tools
to your review app.

After determining the ID for the merge request to link to a visual review app, you
can supply the ID by either:

- Hard-coding it in the script tag via the data attribute `data-merge-request-id` of the app.
- Dynamically adding the `data-merge-request-id` value during the build of the app.
- Supplying it manually through the visual review form in the app.

### Enable or disable Visual Reviews **(PREMIUM SELF)**

Visual Reviews is deployed behind a feature flag that is **enabled by default**.
[GitLab administrators with access to the GitLab Rails console](../../administration/feature_flags.md)
can opt to disable it.

To disable it:

```ruby
Feature.disable(:anonymous_visual_review_feedback)
```

To enable it:

```ruby
Feature.enable(:anonymous_visual_review_feedback)
```

### Authentication for Visual Reviews

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/42750#note_317271120) in GitLab 12.10.

To enable visual reviews for private and internal projects, set the
[`data-require-auth` variable](#enable-or-disable-visual-reviews) to `true`. When enabled,
the user must enter a [personal access token](../../user/profile/personal_access_tokens.md)
with `api` scope before submitting feedback.

This same method can be used to require authentication for any public projects.
-->