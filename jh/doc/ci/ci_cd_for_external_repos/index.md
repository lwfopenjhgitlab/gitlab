---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: index, howto
---

# 用于外部仓库的 GitLab CI/CD **(PREMIUM)**

<!--
>[Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/4642) in GitLab 10.6.
-->

GitLab CI/CD 可以与 [GitHub](github_integration.md)、Bitbucket Cloud 或任何其它 Git 服务器一起使用。

您可以连接外部仓库就可以获得 GitLab CI/CD 的优势，而不用将整个项目移动到极狐GitLab。

连接外部仓库会设置[仓库镜像](../../user/project/repository/mirror/index.md) 并创建一个轻量级项目，其中禁用了议题、合并请求、wiki 和代码片段。这些功能可以后续重新启用<!--[可以后续重新启用](../../user/project/settings/index.md#sharing-and-permissions)-->。

## 连接到外部仓库

要连接到外部仓库：

<!-- vale gitlab.Spelling = NO -->

1. 在极狐GitLab 中，在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目**。
1. 在页面右侧，选择 **新建项目**。
1. 选择 **为外部仓库运行 CI/CD**。
1. 选择 **GitHub** 或 **从 URL 导入仓库**。
1. 填写字段。

<!-- vale gitlab.Spelling = YES -->

## 外部拉取请求流水线

将 GitLab CI/CD 与 [GitHub 上的外部仓库](github_integration.md)一起使用时，可以在拉取请求的上下文中运行流水线。

当您将更改推送到 GitHub 中的远程分支时，GitLab CI/CD 可以为该分支运行流水线。但是，当您打开或更新该分支的拉取请求时，您可能希望：

- 运行额外的作业。
- 不运行特定的作业。

例如：

```yaml
always-run:
  script: echo 'this should always run'

on-pull-requests:
  script: echo 'this should run on pull requests'
  only:
    - external_pull_requests

except-pull-requests:
  script: echo 'this should not run on pull requests'
  except:
    - external_pull_requests
```

### 工作原理

当从 GitHub 导入仓库时，极狐GitLab 订阅 webhook 获取 `push` 和 `pull_request` 事件。一旦收到 `pull_request` 事件，Pull Request 数据就会被存储并作为参考。如果刚刚创建了拉取请求，极狐GitLab 会立即为外部拉取请求创建流水线。

如果将更改推送到拉取请求引用的分支，并且拉取请求仍处于开放状态，则会为外部拉取请求创建流水线。

在这种情况下，GitLab CI/CD 创建了 2 个流水线。一个用于分支推送，一个用于外部拉取请求。

拉取请求关闭后，即使新的更改被推送到同一个分支，也不会为外部拉取请求创建流水线。

### 其它预定义变量

通过对外部拉取请求使用流水线，极狐GitLab 向流水线作业暴露了额外的[预定义变量](../variables/predefined_variables.md)。

变量名称以 `CI_EXTERNAL_PULL_REQUEST_` 为前缀。

### 限制

此功能不支持：

- GitHub Enterprise 所需的手动连接方法。如果手动连接集成，则外部拉取请求不会触发流水线。
- 从派生仓库中拉取请求。来自派生仓库的拉取请求将被忽略。

鉴于系统创建了 2 个流水线，如果将更改推送到引用开放的拉取请求的远端分支，两者都会通过 GitHub 集成影响拉取请求的状态。如果您想在外部拉取请求上而不是在分支上专门运行流水线，您可以在作业 specs 中添加 `except: [branches]`。
