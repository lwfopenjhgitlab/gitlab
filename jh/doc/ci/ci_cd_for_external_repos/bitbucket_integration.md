---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 极狐GitLab CI/CD 与 Bitbucket Cloud 仓库一起使用 **(PREMIUM)**

极狐GitLab CI/CD 可以通过以下方式与 Bitbucket Cloud 一起使用：

1. 创建 [CI/CD 项目](index.md)。
1. 通过 URL 连接您的 Git 仓库。

要将极狐GitLab CI/CD 与 Bitbucket Cloud 仓库一起使用：

1. 在极狐GitLab 中，创建项目：
   1. 在极狐GitLab 中，在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目**。
   1. 在页面右侧，选择 **新建项目**。
   1. 选择 **为外部仓库运行 CI/CD**。
   1. 选择 **仓库（URL）**。
   1. 使用 Bitbucket 仓库中的信息填写字段：
      - 对于 **Git 仓库 URL**，请使用 Bitbucket 中的**克隆此仓库**面板中的 URL。
      - 将用户名留空。
      - 您可以为密码字段生成，并使用 [Bitbucket 应用密码](https://support.atlassian.com/bitbucket-cloud/docs/app-passwords/)。

   极狐GitLab 导入存储库并启用[拉取镜像](../../user/project/repository/mirror/pull.md)。您可以通过转到 **设置 > 仓库 > 镜像仓库** 来检查项目的镜像是否正常工作。

1. 在极狐GitLab 中，创建一个具有 `api` 范围的[个人访问令牌](../../user/profile/personal_access_tokens.md)。用于验证来自在 Bitbucket 中创建的 Webhook 的请求，通知极狐GitLab 有新提交。

1. 在 Bitbucket 中，在 **设置 > Webhooks** 中，创建一个新的 Webhook，通知极狐GitLab 有新提交。

   Webhook URL 应设置为极狐GitLab API，触发拉取镜像，可以使用刚刚为身份验证生成的个人访问令牌。

   ```plaintext
   https://gitlab.com/api/v4/projects/<PROJECT_ID>/mirror/pull?private_token=<PERSONAL_ACCESS_TOKEN>
   ```

   Webhook 触发器应设置为 **仓库推送**。

   ![Bitbucket Cloud webhook](img/bitbucket_webhook.png)

   保存后，通过将更改推送到您的 Bitbucket 仓库来测试 Webhook。

1. 在 Bitbucket 中，从 **Bitbucket Settings > App Passwords** 创建一个 **App Password**，验证构建状态脚本在 Bitbucket 中设置提交构建状态。需要仓库写入权限。

   ![Bitbucket Cloud webhook](img/bitbucket_app_password.png)

1. 在极狐GitLab 中，转到 **设置 > CI/CD > 变量**，添加变量以允许通过 Bitbucket API 与 Bitbucket 通信：

   - `BITBUCKET_ACCESS_TOKEN`：上面创建的 Bitbucket app password。这个变量应该是[隐藏的](../variables/index.md#mask-a-cicd-variable)。

   - `BITBUCKET_USERNAME`：Bitbucket 帐户的用户名。

   - `BITBUCKET_NAMESPACE`：如果您的极狐GitLab 和 Bitbucket 命名空间不同，请设置此变量。

   - `BITBUCKET_REPOSITORY`：如果您的极狐GitLab 和 Bitbucket 项目名称不同，请设置此变量。

1. 在 Bitbucket 中，添加脚本，将流水线状态推送到 Bitbucket。

   NOTE:
   必须在 Bitbucket 中进行更改，因为当极狐GitLab 下次镜像仓库时，Bitbucket 会覆盖极狐GitLab 仓库中的任何更改。

   创建一个文件 `build_status` 并在下面插入脚本并在终端中运行 `chmod +x build_status` 以使脚本可执行。

   ```shell
   #!/usr/bin/env bash

   # Push GitLab CI/CD build status to Bitbucket Cloud

   if [ -z "$BITBUCKET_ACCESS_TOKEN" ]; then
      echo "ERROR: BITBUCKET_ACCESS_TOKEN is not set"
   exit 1
   fi
   if [ -z "$BITBUCKET_USERNAME" ]; then
       echo "ERROR: BITBUCKET_USERNAME is not set"
   exit 1
   fi
   if [ -z "$BITBUCKET_NAMESPACE" ]; then
       echo "Setting BITBUCKET_NAMESPACE to $CI_PROJECT_NAMESPACE"
       BITBUCKET_NAMESPACE=$CI_PROJECT_NAMESPACE
   fi
   if [ -z "$BITBUCKET_REPOSITORY" ]; then
       echo "Setting BITBUCKET_REPOSITORY to $CI_PROJECT_NAME"
       BITBUCKET_REPOSITORY=$CI_PROJECT_NAME
   fi

   BITBUCKET_API_ROOT="https://api.bitbucket.org/2.0"
   BITBUCKET_STATUS_API="$BITBUCKET_API_ROOT/repositories/$BITBUCKET_NAMESPACE/$BITBUCKET_REPOSITORY/commit/$CI_COMMIT_SHA/statuses/build"
   BITBUCKET_KEY="ci/gitlab-ci/$CI_JOB_NAME"

   case "$BUILD_STATUS" in
   running)
      BITBUCKET_STATE="INPROGRESS"
      BITBUCKET_DESCRIPTION="The build is running!"
      ;;
   passed)
      BITBUCKET_STATE="SUCCESSFUL"
      BITBUCKET_DESCRIPTION="The build passed!"
      ;;
   failed)
      BITBUCKET_STATE="FAILED"
      BITBUCKET_DESCRIPTION="The build failed."
      ;;
   esac

   echo "Pushing status to $BITBUCKET_STATUS_API..."
   curl --request POST "$BITBUCKET_STATUS_API" \
   --user $BITBUCKET_USERNAME:$BITBUCKET_ACCESS_TOKEN \
   --header "Content-Type:application/json" \
   --silent \
   --data "{ \"state\": \"$BITBUCKET_STATE\", \"key\": \"$BITBUCKET_KEY\", \"description\":
   \"$BITBUCKET_DESCRIPTION\",\"url\": \"$CI_PROJECT_URL/-/jobs/$CI_JOB_ID\" }"
   ```

1. 在 Bitbucket 中，创建一个 `.gitlab-ci.yml` 文件，使用脚本将流水线成功和失败的状态推送到 Bitbucket。与上面添加的脚本类似，此文件作为镜像过程的一部分被复制到极狐GitLab 代码库。

   ```yaml
   stages:
     - test
     - ci_status

   unit-tests:
     script:
       - echo "Success. Add your tests!"

   success:
     stage: ci_status
     before_script:
       - ""
     after_script:
       - ""
     script:
       - BUILD_STATUS=passed BUILD_KEY=push ./build_status
     when: on_success

   failure:
     stage: ci_status
     before_script:
       - ""
     after_script:
       - ""
     script:
       - BUILD_STATUS=failed BUILD_KEY=push ./build_status
     when: on_failure
   ```

极狐GitLab 现在配置为镜像来自 Bitbucket 的更改，运行在 `.gitlab-ci.yml` 中配置的 CI/CD 流水线并将状态推送到 Bitbucket。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
