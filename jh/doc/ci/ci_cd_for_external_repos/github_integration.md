---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# GitLab CI/CD 与 GitHub 仓库一起使用 **(PREMIUM)**

通过创建 [CI/CD 项目](index.md) 将您的 GitHub 仓库连接到极狐GitLab，GitLab CI/CD 可以与 **GitHub.com** 和 **GitHub Enterprise** 一起使用。

NOTE:
由于 GitHub 的限制，GitHub OAuth 不能用于将 GitHub 作为外部 CI/CD 仓库进行身份验证。

## 使用个人访问令牌连接

个人访问令牌只能用于将 GitHub.com 仓库连接到极狐GitLab，并且 GitHub 用户必须具有[所有者角色](https://docs.github.com/en/get-started/learning-about-github/access-permissions-on-github)。

使用 GitHub 执行一次性授权极狐GitLab 访问您的仓库：

1. 在 GitHub 中，创建一个令牌：
   1. 打开 <https://github.com/settings/tokens/new>。
   1. 创建一个 **Personal Access Token**。
   1. 输入 **Token description** 并更新范围，允许 `repo` 和 `admin:repo_hook`，以便极狐GitLab 可以访问您的项目，更新提交状态，并创建一个 Web 挂钩来通知极狐GitLab 有新提交。
1. 在极狐GitLab 中，创建一个项目：
    1. 在极狐GitLab 中，在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目**。
    1. 在页面右侧，选择 **新建项目**。
    1. 选择 **为外部仓库运行 CI/CD**。
    1. 选择 **GitHub**。
    1. 对于 **个人访问令牌**，粘贴令牌。
    1. 选择 **列出仓库**。
    1. 选择 **连接**，来选择仓库。
1. 在 GitHub 中，将 `.gitlab-ci.yml` 添加到[配置 GitLab CI/CD](../quick_start/index.md)。

极狐GitLab：

1. 导入项目。
1. 启用[拉取镜像](../../user/project/repository/mirror/pull.md)。
1. 启用 [GitHub 项目集成](../../user/project/integrations/github.md)。
1. 在 GitHub 上创建一个 webhook 来通知极狐GitLab 有新的提交。

<!--
## 手动连接

To use **GitHub Enterprise** with **GitLab.com**, use this method.

To manually enable GitLab CI/CD for your repository:

1. In GitHub, create a token:
   1. Open <https://github.com/settings/tokens/new>.
   1. Create a **Personal Access Token**.
   1. Enter a **Token description** and update the scope to allow
      `repo` so that GitLab can access your project and update commit statuses.
1. In GitLab, create a project:
   1. On the top menu, select **Projects > Create new project**.
   1. Select **Run CI/CD for external repository** and **Repo by URL**.
   1. In the **Git repository URL** field, enter the HTTPS URL for your GitHub repository.
      If your project is private, use the personal access token you just created for authentication.
   1. Fill in all the other fields and select **Create project**.
      GitLab automatically configures polling-based pull mirroring.
1. In GitLab, enable [GitHub project integration](../../user/project/integrations/github.md):
   1. On the left sidebar, select **Settings > Integrations**.
   1. Select the **Active** checkbox.
   1. Paste your personal access token and HTTPS repository URL into the form and select **Save**.
1. In GitLab, create a **Personal Access Token** with `API` scope to
   authenticate the GitHub web hook notifying GitLab of new commits.
1. In GitHub, from **Settings > Webhooks**, create a web hook to notify GitLab of
   new commits.

   The web hook URL should be set to the GitLab API to
   [trigger pull mirroring](../../api/projects.md#start-the-pull-mirroring-process-for-a-project),
   using the GitLab personal access token we just created:

   ```plaintext
   https://gitlab.com/api/v4/projects/<NAMESPACE>%2F<PROJECT>/mirror/pull?private_token=<PERSONAL_ACCESS_TOKEN>
   ```

   Select the **Let me select individual events** option, then check the **Pull requests** and **Pushes** checkboxes. These settings are needed for [pipelines for external pull requests](index.md#pipelines-for-external-pull-requests).

1. In GitHub, add a `.gitlab-ci.yml` to configure GitLab CI/CD.
-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
