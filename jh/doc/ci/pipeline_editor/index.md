---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 流水线编辑器 **(FREE)**

> - 引入于 13.8 版本
> - 功能标志移除于 13.10 版本

流水线编辑器是在仓库根目录的 `.gitlab-ci.yml` 文件中编辑 GitLab CI/CD 配置的主要位置。要访问编辑器，请转到 **CI/CD > 编辑器**。

从流水线编辑器页面，您可以：

- 选择要工作的分支。
- [验证](#validate-ci-configuration)编辑文件时的配置语法。
- 对您的配置进行更深入的 [lint](#lint-ci-configuration)，使用添加了 [`include`](../yaml/index.md#include) 关键字的任何配置对其进行验证。
- 查看[使用 `include` 关键字添加的 CI/CD 配置列表](#view-included-cicd-configuration)。
- 查看当前配置的[可视化](#visualize-ci-configuration)。
- 查看[完整配置](#view-full-configuration)，显示添加了 `include` 的任何配置。
- [提交](#commit-changes-to-ci-configuration)对特定分支的更改。

在 13.9 及更早版本中，您必须在项目的默认分支上已经有 [`.gitlab-ci.yml` 文件](../quick_start/index.md#create-a-gitlab-ciyml-file)。

<a id="validate-ci-configuration"></a>

## 验证 CI 配置

当您编辑流水线配置时，它会根据 GitLab CI/CD 流水线架构不断进行验证。它检查您的 CI YAML 配置的语法，并运行一些基本的逻辑验证。

此验证的结果显示在编辑器页面的顶部。如果您的配置无效，则会显示一条提示以帮助您解决问题：

![Errors in a CI configuration validation](img/pipeline_editor_validate_v13_8.png)

<a id="lint-ci-configuration"></a>

## Lint CI 配置

要在提交更改之前测试 GitLab CI/CD 配置的有效性，您可以使用 CI lint 工具。要访问它，请转到 **CI/CD > 编辑器** 并选择 **Lint** 选项卡。

此工具会检查语法和逻辑错误，但比编辑器中的自动[验证](#validate-ci-configuration)更详细。

结果实时更新。您对配置所做的任何更改都会反映在 CI lint 中。它显示与现有 [CI Lint 工具](../lint.md)相同的结果。

![Linting errors in a CI configuration](img/pipeline_editor_lint_v13_8.png)

<a id="view-included-cicd-configuration"></a>

## 查看包含的 CI/CD 配置

> - 引入于 15.0 版本，功能标志名为 `pipeline_editor_file_tree`。默认禁用。
> - 功能标志删除于 15.1 版本。

您可以在流水线编辑器中查看使用 [`include`](../yaml/index.md#include) 关键字添加的配置。在右上角，选择文件树 (**{file-tree}**)，查看所有包含的配置文件的列表。所选文件在新选项卡中打开可供查看。

<a id="visualize-ci-configuration"></a>

## 可视化 CI 配置

> - 引入于 13.5 版本
> - 移动到 **CI/CD > 编辑器**于 13.7 版本。
> - 功能标志移除于 13.12 版本。

要查看您的 `.gitlab-ci.yml` 配置的可视化，请在您的项目中，转到 **CI/CD > 编辑器**，然后选择 **可视化** 选项卡。可视化显示所有阶段和作业。任何 [`needs`](../yaml/index.md#needs) 关系都显示为将作业连接在一起的线，显示执行的层次结构：

![CI configuration Visualization](img/ci_config_visualization_v13_7.png)

将鼠标悬停在工作上以突出其 `needs` 关系：

![CI configuration visualization on hover](img/ci_config_visualization_hover_v13_7.png)

如果配置没有任何  `needs` 关系，则不会绘制任何线条，因为每个作业仅取决于成功完成前一个阶段。

<a id="view-full-configuration"></a>

## 查看完整配置

> - 引入于 13.9 版本
> - 功能标志移除于 13.12 版本
> - **查看合并的 YAML** 选项卡重命名为 **完整配置** 于 16.0 版本。


要将完全扩展的 CI/CD 配置作为一个组合文件查看，请转到流水线编辑器的 **完整配置** 选项卡。此选项卡显示扩展配置，其中：

- 使用 [`include`](../yaml/index.md#include) 导入的配置被复制到视图中。
- 使用 [`extends`](../yaml/index.md#extends) 的作业，扩展配置合并显示到作业<!--[扩展配置合并到作业](../yaml/index.md#合并详细信息)-->。
- [YAML 锚点](../yaml/yaml_optimization.md#锚点)由链接配置替代。
- [YAML `!reference` 标签](../yaml/yaml_optimization.md#reference-标签)也被链接配置替换。

使用 `!reference` 标签可能会导致嵌套配置在展开视图中显示多个连字符 (`-`)。这是意料之中的，额外的连字符不会影响作业的执行。例如，以下配置和完全扩展的版本都是有效的：

- `.gitlab-ci.yml` 文件：

  ```yaml
  .python-req:
    script:
      - pip install pyflakes

  lint-python:
    image: python:latest
    script:
      - !reference [.python-req, script]
      - pyflakes python/
  ```

- **完整配置** 选项卡中的扩展配置：

  ```yaml
  ".python-req":
    script:
    - pip install pyflakes
  lint-python:
    script:
    - - pip install pyflakes  # <- The extra hyphens do not affect the job's execution.
    - pyflakes python/
    image: python:latest
  ```

<a id="commit-changes-to-ci-configuration"></a>

## 提交对 CI 配置的更改

提交表单出现在编辑器中每个选项卡的底部，因此您可以随时提交更改。

当您对更改感到满意时，添加描述性提交消息并输入分支。分支字段默认为您项目的默认分支。

如果您输入新的分支名称，则会出现 **使用这些更改开始新的合并请求** 复选框。选择它以在提交更改后启动新的合并请求。

![The commit form with a new branch](img/pipeline_editor_commit_v13_8.png)

## 故障排查

<a id="configuration-validation-currently-not-available-message"></a>

### `Configuration validation currently not available` 消息

此消息是由于流水线编辑器中的语法验证存在问题。
如果系统无法与验证语法的服务通信，则这些部分中的信息可能无法正确显示：

- **编辑** 选项卡上的语法状态（有效或无效）。
- **可视化**选项卡。
- **Lint** 选项卡。
- **完整配置** 选项卡。

您仍然可以处理您的 CI/CD 配置，并提交您所做的更改而不会出现任何问题。一旦服务再次可用，语法验证应立即显示。

使用 [`include`](../yaml/index.md#include)，但包含的配置文件会创建一个循环。例如，`.gitlab-ci.yml` 包含 `file1.yml`，其中包含 `file2.yml`，其中包含 `file1.yml`，在 `file1.yml` 和 `file2.yml` 之间创建一个循环。

删除 `include` 行之一，消除循环并解决问题。
