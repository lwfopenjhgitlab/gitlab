---
stage: Verify
group: Pipeline Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 作业产物 **(FREE)**

作业可以输出文件和目录的存档。此输出称为作业产物。

您可以使用 UI 或 [API](../../api/job_artifacts.md#get-job-artifacts) 下载作业产物。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview of job artifacts, watch the video [GitLab CI pipelines, artifacts, and environments](https://www.youtube.com/watch?v=PCKDICEe10s).
Or, for an introduction, watch [GitLab CI pipeline tutorial for beginners](https://www.youtube.com/watch?v=Jav4vbUrqII).
-->

有关作业产物存储的管理员信息，请参阅[管理作业产物](../../administration/job_artifacts.md)。

## 创建作业产物

要创建作业产物，请在 `.gitlab-ci.yml` 文件中使用 [`artifacts`](../yaml/index.md#artifacts) 关键字：

```yaml
pdf:
  script: xelatex mycv.tex
  artifacts:
    paths:
      - mycv.pdf
```

在此示例中，名为 `pdf` 的作业调用 `xelatex` 命令从 `LaTeX` 源文件 `mycv.tex` 构建 PDF 文件。

[`paths`](../yaml/index.md#artifactspaths) 关键字确定将哪些文件添加到作业产物中。
文件和目录的所有路径都相对于创建作业的仓库。

### 使用通配符

您可以对路径和目录使用通配符。例如，要创建一个产物，其中包含目录中以 `xyz` 结尾的所有文件：

```yaml
job:
  script: echo "build xyz project"
  artifacts:
    paths:
      - path/*xyz/*
```

### 使用排除

[`expire_in`](../yaml/index.md#artifactsexpire_in) 关键字决定极狐GitLab 保留作业产物的时间。例如：

```yaml
pdf:
  script: xelatex mycv.tex
  artifacts:
    paths:
      - mycv.pdf
    expire_in: 1 week
```

如果未定义 `expire_in`，则使用[实例范围设置](../../user/admin_area/settings/continuous_integration.md#default-artifacts-expiration)。

为防止产物过期，您可以从作业详细信息页面中选择 **保留**。
当产物没有设置到期时，该选项不可用。

### 使用动态定义的名称

您可以使用 [CI/CD 变量](../variables/index.md)动态定义产物文件的名称。

例如，要使用当前作业的名称创建存档：

```yaml
job:
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - binaries/
```

要使用当前分支或标记的名称创建一个存档，仅包括二进制文件目录：

```yaml
job:
  artifacts:
    name: "$CI_COMMIT_REF_NAME"
    paths:
      - binaries/
```

如果您的分支名称包含正斜杠（例如 `feature/my-feature`），使用 `$CI_COMMIT_REF_SLUG` 而不是 `$CI_COMMIT_REF_NAME` 来正确命名产物。

### 使用 Windows runner 或 shell 执行器

如果您使用 Windows Batch 来运行您的 shell 脚本，您必须将 `$` 替换为 `%`：

```yaml
job:
  artifacts:
    name: "%CI_JOB_STAGE%-%CI_COMMIT_REF_NAME%"
    paths:
      - binaries/
```

如果您使用 Windows PowerShell 来运行您的 shell 脚本，您必须将 `$` 替换为 `$env:`：

```yaml
job:
  artifacts:
    name: "$env:CI_JOB_STAGE-$env:CI_COMMIT_REF_NAME"
    paths:
      - binaries/
```

### 使用排除文件

使用 [`artifacts:exclude`](../yaml/index.md#artifactsexclude) 来阻止将文件添加到产物存档中。

例如，将所有文件存储在 `binaries/` 中，但不存储位于 `binaries/` 子目录中的 `*.o` 文件。

```yaml
artifacts:
  paths:
    - binaries/
  exclude:
    - binaries/**/*.o
```

与 [`artifacts:paths`](../yaml/index.md#artifactspaths) 不同，`exclude` 路径不是递归的。
要排除目录的所有内容，请明确匹配所有内容，而不是匹配目录本身。

例如，要将所有文件存储在 `binaries/` 中，但不存储在 `temp/` 子目录中：

```yaml
artifacts:
  paths:
    - binaries/
  exclude:
    - binaries/temp/**/*
```

### 使用未跟踪的文件

使用 [`artifacts:untracked`](../yaml/index.md#artifactsuntracked) 将所有 Git 未跟踪文件添加为产物（以及在 [`artifacts:paths`](../yaml/index.md#artifactspaths) 中定义的路径)。未跟踪的文件是那些尚未添加到仓库，但存在于仓库检出中的文件。

将所有 Git 未跟踪文件和文件保存在 `binaries` 中：

```yaml
artifacts:
  untracked: true
  paths:
    - binaries/
```

例如，要保存所有未跟踪的文件，但 [排除](../yaml/index.md#artifactsexclude) `*.txt` 文件：

```yaml
artifacts:
  untracked: true
  exclude:
    - "*.txt"
```

## 阻止作业获取产物

默认情况下，作业会从先前阶段的已完成作业中下载所有产物。
要阻止作业下载任何产物，请将 [`dependencies`](../yaml/index.md#dependencies) 设置为空数组 (`[]`)：

```yaml
job:
  stage: test
  script: make build
  dependencies: []
```

<a id="view-all-job-artifacts-in-a-project"></a>

## 查看项目中的所有作业产物

> 引入于 15.11 版本，[功能标志](../../administration/feature_flags.md)为 `artifacts_management_page`。默认禁用。

您可以从 **CI/CD > 产物** 页面查看项目中存储的所有产物。
此列表显示所有作业及其关联的产物。展开条目以访问与作业关联的所有产物，包括：

- 使用 `artifacts:` 关键字创建的产物。
- [报告产物](../yaml/artifacts_reports.md)。
- 作业日志和元数据，它们在内部存储中作为单独的产物。

您可以从此列表下载或删除单个产物。

<a id="download-job-artifacts"></a>

## 下载作业产物

您可以在以下位置下载作业产物：

- 任何**流水线**列表。在流水线右侧，选择 **下载产物** (**{download}**)。
- 任何**作业**列表。在作业右侧，选择 **下载产物** (**{download}**)。
- 作业的详细信息页面。在页面右侧，选择 **下载**。
- 合并请求**概览**页面。在最新流水线的右侧，选择 **产物** (**{download}**)。
- [**产物**](#view-all-job-artifacts-in-a-project)页面。在作业右侧，选择 **下载** (**{download}**)。
- [产物浏览器](#browse-the-contents-of-the-artifacts-archive)。在页面顶部，选择 **下载产物存档** (**{download}**)。

[报告产物](../yaml/artifacts_reports.md)只能从**流水线**列表或**产物**页面下载。

您可以使用[作业产物 API](../../api/job_artifacts.md)，从最新成功的流水线下载作业产物。
您不能使用作业产物 API 下载[产物报告](../yaml/artifacts_reports.md)，除非使用 `artifacts:paths` 将报告添加为常规产物。

### 使用 URL 下载

您可以使用作业产物 API 的可公开访问的 URL 下载特定作业的产物存档。

例如，要在 JiHuLab.com 上的项目的 `main` 分支中，下载名为 `build` 的作业的最新产物：

```plaintext
https://jihulab.com/api/v4/projects/<project-id>/jobs/artifacts/main/download?job=build
```

例如，要从 JiHuLab.com 上项目的 `main` 分支中名为 `build` 的最新作业下载文件 `review/index.html`：

```plaintext
https://jihulab.com/api/v4/projects/<project-id>/jobs/artifacts/main/raw/review/index.html?job=build
```

在这两个示例中，将 `<project-id>` 替换为位于项目详细信息页面顶部的有效项目 ID。

[父子流水线](../pipelines/downstream_pipelines.md#parent-child-pipelines)的产物按从父到子的层次顺序进行获取。例如，父流水线和子流水线都有同名的作业，则返回父流水线中的作业产物。

<a id="browse-the-contents-of-the-artifacts-archive"></a>

## 浏览作业档案的内容

您可以在以下位置，从 UI 浏览产物的内容，而无需在本地下载产物：

- 任何**作业**列表。在作业右侧，选择 **浏览** (**{folder-open}**)。
- 作业的详细信息页面。在页面右侧，选择 **浏览**。
- **产物**页面。在作业右侧，选择 **浏览** (**{folder-open}**)。

如果项目中启用了[极狐GitLab Pages](../../administration/pages/index.md)，您可以直接在浏览器中预览产物中的 HTML 文件。如果项目是内部或私有的，则必须启用[极狐GitLab Pages 访问控制](../../administration/pages/index.md#access-control) 才能预览 HTML 文件。

### 使用 URL 浏览

您可以使用可公开访问的 URL 浏览特定作业的最新成功流水线的作业产物。

例如，要浏览 JiHuLab.com 上项目的 `main` 分支中名为 `build` 的作业的最新产物：

```plaintext
https://jihulab.com/<full-project-path>/-/jobs/artifacts/main/browse?job=build
```

将 `<full-project-path>` 替换为有效的项目路径，您可以在项目的 URL 中找到它。

<a id="delete-job-log-and-artifacts"></a>

## 删除作业日志和产物

WARNING:
删除作业日志和产物是一种无法恢复的破坏性操作。谨慎使用。

您可以删除作业的产物和日志。

先决条件：

- 您必须是作业的所有者或至少具有项目维护者角色的用户。

要删除作业：

1. 进入作业详情页面。
1. 在作业日志的右上角，选择 **擦除作业日志和产物** (**{remove}**)。

## 在合并请求 UI 中链接作业产物

使用 [`artifacts:expose_as`](../yaml/index.md#artifactsexpose_as) 关键字，在[合并请求](../../user/project/merge_requests/index.md)用户界面中显示作业产物的链接。

例如，对于具有单个文件的产物：

```yaml
test:
  script: ["echo 'test' > file.txt"]
  artifacts:
    expose_as: 'artifact 1'
    paths: ['file.txt']
```

通过此配置，极狐GitLab 将 **artifact 1** 作为指向 `file.txt` 的链接，添加到相关合并请求的**查看已展示产物**部分。

<a id="keep-artifacts-from-most-recent-successful-jobs"></a>

## 保留最近成功作业的产物

> - 引入于 13.0 版本。
> - 功能标志移除于 13.4 版本。
> - 支持通过 CI/CD 设置可选于 13.8 版本。

默认情况下，对于每个 ref 上的最新提交，始终为成功的流水线保留产物。这意味着最新的产物不会根据 `expire_in` 规范立即过期。

如果同一 ref 上的新提交的流水线成功完成，则根据 `expire_in` 配置删除先前流水线的产物。新流水线的产物会自动保留。如果针对 ref 上的最新提交运行多个流水线，则保留所有产物。

保留最新的产物可以在有大量作业或大型产物的项目中使用大量存储空间。如果项目中不需要最新的产物，您可以禁用此行为以节省空间：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **产物**。
1. 清除 **保留最近成功作业的产物** 复选框。

您可以在[实例的 CI/CD 设置](../../user/admin_area/settings/continuous_integration.md#keep-the-latest-artifacts-for-all-jobs-in-the-latest-successful-pipelines)中，为私有化部署实例上的所有项目禁用此行为。

当启用 **保留最近成功作业的产物** 时，始终为[阻塞的](job_control.md#types-of-manual-jobs)流水线保留产物。这些产物仅在触发阻塞作业和流水线完成后才会过期。
