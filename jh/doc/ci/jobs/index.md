---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 作业 **(FREE)**

流水线配置从作业开始。作业是 `.gitlab-ci.yml` 文件中最基本的元素。

工作是：

- 定义了约束条件，说明它们应该在什么条件下执行。
- 具有任意名称的顶级元素，并且必须至少包含 [`script`](../yaml/index.md#script) 子句。
- 不限制可以定义的数量。

例如：

```yaml
job1:
  script: "execute-script-for-job1"

job2:
  script: "execute-script-for-job2"
```

上面的示例是最简单的 CI/CD 配置，具有两个单独的作业，其中每个作业执行不同的命令。
当然，命令可以直接执行代码（`./configure;make;make install`）或在仓库中运行脚本（`test.sh`）。

作业由 runners<!--[runners](../runners/index.md)--> 拾取并在 runner 的环境中执行。重要的是，每个作业都是相互独立运行的。

## 查看流水线中的作业

当您访问流水线时，您可以看到该流水线的相关作业。

单击单个作业会显示其作业日志，并允许您：

- 取消作业。
- 重试作业。
- 清除作业日志。

### 查看项目中的所有作业

要查看项目中运行的作业的完整列表：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到项目。
1. 在左侧边栏上，选择 **CI/CD > 作业**。

您可以按[作业状态](#the-order-of-jobs-in-a-pipeline)过滤列表。

## 查看作业失败的原因

当流水线发生故障或被允许发生故障时，您可以在以下几个地方找到原因：

- 在[流水线图](../pipelines/index.md#可视化流水线)中的流水线详细信息视图中。
- 在流水线部件，在合并请求和提交页面中。
- 在作业视图，在作业的全局视图和详细视图中。

在每个地方，如果您将鼠标悬停在失败的作业上，您可以看到它失败的原因。

![Pipeline detail](img/job_failure_reason.png)

您还可以在作业详细信息页面上查看失败的原因。

## 流水线中的作业顺序

流水线中作业的顺序取决于流水线图的类型。

- 对于[完整流水线图](../pipelines/index.md#查看完整的流水线图)，作业按名称排序。
- 对于[流水线迷你图](../pipelines/index.md#流水线迷你图)，作业按状态排序，然后按名称排序。

作业状态的顺序是：

- failed
- warning
- pending
- running
- manual
- scheduled
- canceled
- success
- skipped
- created

例如：

![Pipeline mini graph sorting](img/pipelines_mini_graph_sorting.png)

## 作业名称限制

> - 一般可用于 14.10 版本。功能标志 `ci_validate_job_length` 移除。

您不能将这些关键字用作作业名称：

- `image`
- `services`
- `stages`
- `types`
- `before_script`
- `after_script`
- `variables`
- `cache`
- `include`
- `true`
- `false`
- `nil`

作业名称不得超过 255 个字符。

为您的作业使用唯一名称。如果多个作业具有相同的名称，则只会将一个添加到流水线中，并且很难预测会选择哪一个。


## 流水线中分组作业

如果您有许多类似的作业，您的[流水线图](../pipelines/index.md#可视化流水线)会变得很长且难以阅读。

您可以自动将相似的作业组合在一起。如果作业名称以某种方式格式化，它们将在常规流水线图（而不是迷你图）中折叠成一个组。

如果您没有在其中看到重试或取消按钮，您可以识别流水线何时对作业进行了分组。将鼠标悬停在它们上会显示分组作业的数量，单击可以展开它们。

![Grouped pipelines](img/pipeline_grouped_jobs_v14_2.png)

要创建一组作业，请在 [CI/CD 流水线配置文件](../yaml/index.md)中，将每个作业名称用数字和以下之一分隔：

- 斜线（`/`），例如 `slash-test 1/3`、`slash-test 2/3`、`slash-test 3/3`。
- 冒号 (`:`)，例如 `colon-test 1:3`、`colon-test 2:3`、`colon-test 3:3`。
- 一个空格，例如 `space-test 0 3`、`space-test 1 3`、`space-test 2 3`。

您可以互换使用这些符号。

在下面的示例中，这三个作业位于名为 `build ruby` 的组中：

```yaml
build ruby 1/3:
  stage: build
  script:
    - echo "ruby1"

build ruby 2/3:
  stage: build
  script:
    - echo "ruby2"

build ruby 3/3:
  stage: build
  script:
    - echo "ruby3"
```

流水线图显示了一个名为 `build ruby` 的组，其中包含三个作业。

通过从左到右比较数字来对作业进行排序。您通常希望第一个数字是索引，第二个数字是总数。

[此正则表达式](https://jihulab.com/gitlab-cn/gitlab/-/blob/2f3dc314f42dbd79813e6251792853bc231e69dd/app/models/commit_status.rb#L99) 评估作业名称：`([\b\s:] +((\[.*\])|(\d+[\s:\/\\]+\d+)))+\s*\z`。
一个或多个`: [...]`、`X Y`、`X/Y` 或`X\Y` 序列仅从作业名称的**结尾**中删除。在作业名称的开头或中间找到的匹配子字符串不会被删除。

在 13.8 及更早版本中，正则表达式为 `\d+[\s:\/\\]+\d+\s*`。功能标志在 13.11 版本中删除。

## 隐藏作业

要暂时禁用作业而不将其从配置文件中删除：

- 注释掉作业的配置：

  ```yaml
  # hidden_job:
  #   script:
  #     - run test
  ```

- 以点（`.`）开头的作业名称，它不会被 GitLab CI/CD 处理：

  ```yaml
  .hidden_job:
    script:
      - run test
  ```

您可以使用以 `.` 开头的隐藏作业作为可重用配置的模板：

- [`extends` 关键字](../yaml/index.md#extends)。
- YAML 锚点。<!--[YAML anchors](../yaml/yaml_optimization.md#anchors).-->

## 控制默认关键字和全局变量的继承

您可以控制以下事项的继承：

- [默认关键字](../yaml/index.md#default) 和 [`inherit:default`](../yaml/index.md#inheritdefault)。
- [全局变量](../yaml/index.md#default) 和 [`inherit:variables`](../yaml/index.md#inheritvariables)。

例如：

```yaml
default:
  image: 'ruby:2.4'
  before_script:
    - echo Hello World

variables:
  DOMAIN: example.com
  WEBHOOK_URL: https://my-webhook.example.com

rubocop:
  inherit:
    default: false
    variables: false
  script: bundle exec rubocop

rspec:
  inherit:
    default: [image]
    variables: [WEBHOOK_URL]
  script: bundle exec rspec

capybara:
  inherit:
    variables: false
  script: bundle exec capybara

karma:
  inherit:
    default: true
    variables: [DOMAIN]
  script: karma
```

在这个例子中：

- `rubocop`：
   - 继承：没有。
- `rspec`：
   - 继承：默认的 `image` 和 `WEBHOOK_URL` 变量。
   - **不** 继承：默认的 `before_script` 和`DOMAIN` 变量。
- `capybara`：
   - 继承：默认的 `before_script` 和`image`。
   - **不** 继承：`DOMAIN` 和`WEBHOOK_URL` 变量。
- `karma`：
   - 继承：默认的 `image` 和`before_script`，以及 `DOMAIN` 变量。
   - **不** 继承：`WEBHOOK_URL` 变量。

<a id="specifying-variables-when-running-manual-jobs"></a>

## 运行手动作业时指定变量

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/30485) in GitLab 12.2.
-->

运行手动作业时，您可以提供额外的作业特定变量。

您可以从要使用其他变量运行的手动作业的作业页面执行此操作。要访问此页面，请单击流水线视图中手动作业的**名称**，*而不是*运行 (**{play}**) 按钮。

当您想要更改使用自定义 CI/CD 变量<!--[自定义 CI/CD 变量](../variables/index.md#custom-cicd-variables)-->的作业的执行时，这很有用。
在此处添加变量名称（键）和值以覆盖在 UI 或 `.gitlab-ci.yml`<!--[UI 或 `.gitlab-ci.yml`](../variables/index.md#custom-cicd-variables)--> 中定义的值，用于单次运行手动作业。

![Manual job variables](img/manual_job_variables_v13_10.png)

## 延迟作业

当您不想立即运行作业时，可以使用 `when:delayed`<!--[`when:delayed`](../jobs/job_control.md#run-a-job-after-a-delay)--> 关键字，在一定时期内来延迟作业的执行。

这对于逐步推出新代码的定时增量推出特别有用。

例如，如果您开始推出新代码并且：

- 用户没有遇到麻烦，极狐GitLab 可以自动完成从 0% 到 100% 的部署。
- 用户在使用新代码时遇到问题，您可以通过取消流水线和[回滚](../environments/index.md#重试或回滚部署)，停止定时增量部署，回到最后的稳定版。

![Pipelines example](img/pipeline_delayed_job_v14_2.png)

## 展开和折叠作业日志部分

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/14664) in GitLab 12.0.
-->

作业日志分为可以折叠或展开的部分。每个部分都显示持续时间。

在以下示例中：

- 三个部分折叠起来，可以展开。
- 三个部分展开并可折叠。

![Collapsible sections](img/collapsible_log_v13_10.png)

### 自定义可折叠部分

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/14664) in GitLab 12.0.
-->

您可以通过手动输出极狐GitLab 用于确定要折叠哪些部分的特殊代码，来创建[作业日志中的可折叠部分](#展开和折叠作业日志部分)：

- 部分开始标记：`\e[0Ksection_start:UNIX_TIMESTAMP:SECTION_NAME\r\e[0K` + `TEXT_OF_SECTION_HEADER`
- 部分结束标记：`\e[0Ksection_end:UNIX_TIMESTAMP:SECTION_NAME\r\e[0K`

您必须将这些代码添加到 CI 配置的脚本部分。例如，使用`echo`：

```yaml
job1:
  script:
    - echo -e "\e[0Ksection_start:`date +%s`:my_first_section\r\e[0KHeader of the 1st collapsible section"
    - echo 'this line should be hidden when collapsed'
    - echo -e "\e[0Ksection_end:`date +%s`:my_first_section\r\e[0K"
```

根据运行程序使用的 shell，例如，如果它使用 Zsh，您可能需要转义特殊字符，如：`\\e` 和 `\\r`。

在上面的例子中：

- `date +%s`：Unix 时间戳（例如`1560896352`）。
- `my_first_section`：该部分的名称。
- `\r\e[0K`：防止部分标记显示在渲染（彩色）作业日志中，但它们显示在原始作业日志中。要查看它们，请在作业日志的右上角单击 **{doc-text}**（**显示完整源**）。
   - `\r`：回车。
   - `\e[0K`：清除行 ANSI 转义码。

示例原始作业日志：

```plaintext
\e[0Ksection_start:1560896352:my_first_section\r\e[0KHeader of the 1st collapsible section
this line should be hidden when collapsed
\e[0Ksection_end:1560896353:my_first_section\r\e[0K
```

### 预折叠部分

> 引入于 13.5 版本。

您可以通过将 `collapsed` 选项添加到可折叠部分的开始，使作业日志自动折叠可折叠部分。
在节名称之后和 `\r` 之前添加 `[collapsed=true]`。部分结束标记保持不变：

- 带有 `[collapsed=true]` 的部分开始标记：`\e[0Ksection_start:UNIX_TIMESTAMP:SECTION_NAME[collapsed=true]\r\e[0K` + `TEXT_OF_SECTION_HEADER`
- 部分结束标记：`\e[0Ksection_end:UNIX_TIMESTAMP:SECTION_NAME\r\e[0K`

将更新的部分开始文本添加到 CI 配置。例如，使用`echo`：

```yaml
job1:
  script:
    - echo -e "\e[0Ksection_start:`date +%s`:my_first_section[collapsed=true]\r\e[0KHeader of the 1st collapsible section"
    - echo 'this line should be hidden automatically after loading the job log'
    - echo -e "\e[0Ksection_end:`date +%s`:my_first_section\r\e[0K"
```

<a id="deployment-jobs"></a>

## 部署作业

部署作业是一种特定类型的 CI 作业，因为它们将代码部署到[环境](../environments/index.md)。部署作业是使用 `environment` 关键字和 [`start` 环境 `action`](../yaml/index.md#environmentaction) 的任何作业。
部署作业不需要处于 `deploy` 阶段。以下 `deploy me` 作业是部署作业的一个示例。`action: start` 是默认行为，是为了示例而定义的，但您可以省略它：

```yaml
deploy me:
  script:
    - deploy-to-cats.sh
  environment:
    name: production
    url: https://cats.example.com
    action: start
```

<!--
部署作业的行为可以通过 [deployment safety](../environments/deployment_safety.md) 设置来控制，例如[跳过过时的部署作业](../environments/deployment_safety.md#prevent-deployments-during-deploy-freeze-windows) 和[确保一次只运行一个部署作业](../environments/deployment_safety.md#ensure-only-one-deployment-job-runs-at-a-time)。
-->
