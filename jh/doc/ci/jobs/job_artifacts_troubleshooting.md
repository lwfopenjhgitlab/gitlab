---
stage: Verify
group: Pipeline Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 作业产物故障排除

使用[作业产物](job_artifacts.md)时，您可能会遇到以下问题。

## 作业不获取某些产物

默认情况下，作业会从之前的阶段获取所有产物，但默认情况下，使用 `dependencies` 或 `needs` 的作业不会从所有作业中获取产物。

如果您使用这些关键字，则仅从作业的一个子集获取产物。查看关键字参考，获取有关如何使用这些关键字获取产物的信息：

- [`dependencies`](../yaml/index.md#dependencies)
- [`needs`](../yaml/index.md#needs)
- [`needs:artifacts`](../yaml/index.md#needsartifacts)

## 作业产物使用了过多的磁盘空间

如果作业产物使用过多磁盘空间，请参阅[作业产物管理文档](../../administration/job_artifacts.md#job-artifacts-using-too-much-disk-space)。

<a id="error-message-no-files-to-upload"></a>

## 错误消息 `No files to upload`

当 runner 找不到要上传的文件时，此消息会出现在作业日志中。文件路径不正确，或者文件未创建。您可以检查作业日志，查找指定文件名的其他错误或警告，以及未生成文件的原因。

如需更详细的作业日志，您可以[启用 CI/CD 调试日志记录](../variables/index.md#enable-debug-logging)并重试该作业。此日志记录可能会提供有关未创建文件的原因的更多信息。

## 错误消息 `Missing /usr/bin/gitlab-runner-helper. Uploading artifacts is disabled.`

> 引入于 15.2 版本，极狐GitLab Runner 使用 `RUNNER_DEBUG` 而不是 `DEBUG` 来解决此问题。

在 15.1 及更早的版本，设置名为 `DEBUG` 的 CI/CD 变量会导致产物上传失败。

要解决此问题，您可以：

- 更新到极狐GitLab 和极狐GitLab Runner 15.2
- 使用不同的变量名
- 在 `script` 命令中将其设置为环境变量：

  ```yaml
  failing_test_job:  # This job might fail due to issue gitlab-org/gitlab-runner#3068
    variables:
      DEBUG: true
    script: bin/mycommand
    artifacts:
      paths:
        - bin/results

  successful_test_job:  # This job does not define a CI/CD variable named `DEBUG` and is not affected by the issue
    script: DEBUG=true bin/mycommand
    artifacts:
      paths:
        - bin/results
  ```

## 在 Windows runner 上上传 dotenv 产物时，收到错误消息 `FATAL: invalid argument`

PowerShell `echo` 命令使用 UCS-2 LE BOM（字节顺序标记）编码写入文件，但仅支持 UTF-8。如果您尝试使用 `echo` 创建 [`dotenv`](../yaml/artifacts_reports.md) 产物，它会导致 `FATAL: invalid argument` 错误。

请改用 PowerShell `Add-Content`，它使用 UTF-8：

```yaml
test-job:
  stage: test
  tags:
    - windows
  script:
    - echo "test job"
    - Add-Content -Path build.env -Value "MY_ENV_VAR=true"
  artifacts:
    reports:
      dotenv: build.env
```

## 作业产物不会过期

如果某些作业产物没有按预期过期，请检查是否启用了[**保留最近成功作业的产物**](job_artifacts.md#keep-artifacts-from-most-recent-successful-jobs)设置。

启用此设置后，来自每个 ref 的最新成功流水线的作业产物不会过期，也不会被删除。

## 错误消息 `This job could not start because it could not retrieve the needed artifacts.`

如果出现以下情况，使用 [`needs:artifacts`](../yaml/index.md#needsartifacts) 关键字配置的作业无法启动并返回此错误消息：

- 找不到作业的依赖项。
- 由于权限不足，作业无法访问相关资源。

要遵循的故障排除步骤因作业使用的语法而异：

- [`needs:project`](#for-a-job-configured-with-needsproject)
- [`needs:pipeline:job`](#for-a-job-configured-with-needspipelinejob)

<a id="for-a-job-configured-with-needsproject"></a>

### 对于配置了 `needs:project` 的作业

`could not retrieve the needed artifacts.` 错误可能发生在使用 [`needs:project`](../yaml/index.md#needsproject) 的作业中，其配置类似于：

```yaml
rspec:
  needs:
    - project: my-group/my-project
      job: dependency-job
      ref: master
      artifacts: true
```

要解决此错误，请验证：

- 项目 `my-group/my-project` 在一个有高级订阅计划的群组中。
- 运行作业的用户可以访问 `my-group/my-project` 中的资源。
- `project`、`job` 和 `ref` 组合存在并导致所需的依赖关系。
- 使用中的任何变量都使用正确的值。

<a id="for-a-job-configured-with-needspipelinejob"></a>

### 对于配置了 `needs:pipeline:job` 的作业

`could not retrieve the needed artifacts.` 错误可能发生在使用 [`needs:pipeline:job`](../yaml/index.md#needspipelinejob) 的作业中，其配置类似于：

```yaml
rspec:
  needs:
    - pipeline: $UPSTREAM_PIPELINE_ID
      job: dependency-job
      artifacts: true
```

要解决此错误，请验证：

- `$UPSTREAM_PIPELINE_ID` CI/CD 变量在当前流水线的父子流水线层次结构中可用。
- `pipeline` 和 `job` 组合存在并解析为现有流水线。
- `dependency-job` 已成功运行并完成。
