---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab CI/CD 作业令牌

当流水线作业即将运行时，极狐GitLab 会生成一个唯一的令牌并将其作为 `CI_JOB_TOKEN` 预定义变量<!--[`CI_JOB_TOKEN` 预定义变量](../variables/predefined_variables.md)--> 注入。

您可以使用 GitLab CI/CD 作业令牌对特定 API 端点进行身份验证：

- 软件包：
   - 软件包库。
   - 容器镜像库（`$CI_REGISTRY_PASSWORD` 是 `$CI_JOB_TOKEN`）。
   - 容器镜像库 API（当启用 `ci_job_token_scope` 功能标志时，范围限定于作业的项目）。
- 获取作业产物。
- 获取作业令牌的作业。
- 流水线触发器，使用 `token=` 参数。
- 发布。
- Terraform 方案。

NOTE:
私有化部署管理版存在已知问题，会阻止您在内部项目中使用作业令牌。

令牌与导致作业运行的用户具有相同的访问 API 的权限。用户可以通过推送提交、触发手动作业、成为计划流水线的所有者等方式使作业运行。因此，必须将此用户分配给具有所需权限的角色<!--[具有所需权限的角色](../../user/permissions.md#gitlab-cicd-permissions)-->。

令牌仅在流水线作业运行时有效。作业完成后，您将无法再使用令牌。

作业令牌无需任何配置即可访问项目的资源，但它可能会授予不必要的额外权限。

您还可以使用作业令牌从 CI/CD 作业中的私有项目中验证和克隆仓库：

```shell
git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.example.com/<namespace>/<project>
```

### 极狐GitLab CI/CD 作业令牌安全

为确保此令牌不会泄漏：

- 隐藏作业日志中的作业令牌。
- 仅在作业运行时授予作业令牌权限。

为确保此令牌不会泄漏，您还应该将 runners<!--[runners](../runners/index.md)--> 配置安全。避免：

- 如果机器被重复使用，则使用 Docker 的 `privileged` 模式。
- 当作业在同一台机器上运行时使用 `shell` executor<!--[`shell` executor](https://docs.gitlab.com/runner/executors/shell.html)-->。

如果您的极狐GitLab Runner 配置不安全，则会增加有人试图从其他作业窃取令牌的风险。

<a id="configure-cicd-job-token-access"></a>

## 配置 CI/CD 作业令牌访问

您可以控制 CI/CD 作业令牌可以访问哪些项目，从而提高作业令牌的安全性。作业令牌可能会授予访问特定私有资源所不需要的额外权限。作业令牌范围仅控制对私有项目的访问。如果访问的项目是公开的或内部的，则令牌范围不适用。

如果作业令牌泄露，它可能会被用于访问作业令牌用户的私有数据。通过限制作业令牌访问范围，除非项目被明确授权，否则无法访问私有数据。

<!--
There is a proposal to add more strategic control of the access permissions,
see [epic 3559](https://gitlab.com/groups/gitlab-org/-/epics/3559).
-->

<a id="allow-access-to-your-project-with-a-job-token"></a>

### 允许使用作业令牌访问您的项目

> 引入于 15.9 版本。[功能标志](../../user/feature_flags.md)为 `:inbound_ci_scoped_job_token`，默认禁用。

创建一个**入站**项目白名单，这些项目可以通过它们的 `CI_JOB_TOKEN` 访问您的项目。

例如，项目 `A` 可以将项目 `B` 添加到入站白名单中。 项目 `B`（允许的项目）中的 CI/CD 作业现在可以使用其 CI/CD 作业令牌来验证 API 调用以访问项目 `A`。如果项目 `A` 是公开的或内部的，则项目 `B` 可以访问该项目，而无需将其添加到入站白名单中。

默认情况下，任何项目的入站白名单仅包括其自身。

禁用此功能存在安全风险，因此项目维护者或所有者应始终启用此设置。仅在需要跨项目访问时才将项目添加到白名单。

<a id="disable-the-job-token-scope-allowlist"></a>

### 禁用入站作业令牌范围许可名单

WARNING:
禁用许可名单存在安全风险。恶意用户可能会尝试破坏在未经授权的项目中创建的流水线。如果流水线是由您的维护者之一创建的，则可以使用作业令牌来尝试访问您的项目。

您可以出于测试或类似原因禁用入站作业令牌范围白名单，但您应该尽快再次启用它。

先决条件：

- 您必须至少具有该项目的维护者角色。

要禁用入站作业令牌范围白名单：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > CI/CD**。
1. 展开 **令牌访问**。
1. 将 **允许使用 CI_JOB_TOKEN 访问此项目** 切换为禁用。在新项目中默认启用。

<!--
You can also disable the allowlist [with the API](../../api/graphql/reference/index.md#mutationprojectcicdsettingsupdate).
-->

### 将项目添加到入站作业令牌范围许可名单

您可以将项目添加到项目的入站白名单中。添加到白名单的项目可以使用 CI/CD 作业令牌从正在运行的流水线进行 API 调用。

先决条件：

- 您必须至少在当前项目中具有维护者角色，在允许的项目中至少具有访客角色。
- 添加到白名单的项目不得超过 100 个。

添加项目：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > CI/CD**。
1. 展开 **令牌访问**。
1. 验证 **允许使用 CI_JOB_TOKEN 访问此项目** 已启用。
1. 在 **允许来自以下项目的 CI 作业令牌访问此项目** 下，将项目添加到白名单。

<!--
You can also add a target project to the allowlist [with the API](../../api/graphql/reference/index.md#mutationcijobtokenscopeaddproject).
-->

## 限制您项目的作业令牌访问

> - 引入于 14.1 版本。部署在 `:ci_scoped_job_token` 功能标志后默认禁用。
> - 适用于私有化部署版于 14.4 版本。
> - 功能标志删除于 14.6 版本。

NOTE:
默认情况下，所有新项目都禁用此功能，并计划删除。在 16.0 版本中。项目维护者或所有者应该改为启用**入站**访问控制。

通过创建项目的**出站**许可列表来控制项目的作业令牌范围，您的项目的作业令牌可以访问这些项目。

默认情况下，许可名单包括您当前的项目。
有权访问这两个项目的维护人员可以添加和删除其他项目。

禁用该设置后，所有项目都被视为白名单，并且作业令牌仅受用户访问权限的限制。

例如，启用该设置后，项目 `A` 流水线中的作业的 `CI_JOB_TOKEN` 范围仅限于项目 `A`。如果作业需要使用令牌向私有项目 `B` 发出 API 请求，则必须将 `B` 添加到 `A` 的许可名单中。
如果项目 `B` 是公开的或内部的，则无需将 `B` 添加到白名单即可授予访问权限。

### 配置出站作业令牌范围

先决条件：

- 添加到令牌范围的项目不得超过 100 个。

要配置出站作业令牌范围：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **令牌访问**。
1. 将 **限制 CI_JOB_TOKEN 访问** 切换为启用。
1. （可选）将现有项目添加到令牌的访问范围。添加项目的用户必须在两个项目中都具有维护者角色。

<a id="download-an-artifact-from-a-different-pipeline"></a>

## 从不同的流水线下载产物 **(PREMIUM)**

<!--
> `CI_JOB_TOKEN` for artifacts download with the API was [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/2346) in GitLab 9.5.
-->

您可以使用 `CI_JOB_TOKEN` 从先前流水线创建的作业中访问产物。您必须指定要从中检索产物的作业：

```yaml
build_submodule:
  stage: test
  script:
    - apt update && apt install -y unzip
    - curl --location --output artifacts.zip "https://gitlab.example.com/api/v4/projects/1/jobs/artifacts/main/download?job=test&job_token=$CI_JOB_TOKEN"
    - unzip artifacts.zip
```

<!--
Read more about the [jobs artifacts API](../../api/job_artifacts.md#download-the-artifacts-archive).
-->

## 故障排查

CI 作业令牌失败通常显示为类似 `404 Not Found` 或类似的响应：

- 未经授权的 Git 克隆：

  ```plaintext
  $ git clone https://gitlab-ci-token:$CI_JOB_TOKEN@gitlab.com/fabiopitino/test2.git

  Cloning into 'test2'...
  remote: The project you were looking for could not be found or you don't have permission to view it.
  fatal: repository 'https://gitlab-ci-token:[MASKED]@gitlab.com/<namespace>/<project>.git/' not found
  ```

- 未经授权的包下载：

  ```plaintext
  $ wget --header="JOB-TOKEN: $CI_JOB_TOKEN" ${CI_API_V4_URL}/projects/1234/packages/generic/my_package/0.0.1/file.txt

  --2021-09-23 11:00:13--  https://gitlab.com/api/v4/projects/1234/packages/generic/my_package/0.0.1/file.txt
  Resolving gitlab.com (gitlab.com)... 172.65.251.78, 2606:4700:90:0:f22e:fbec:5bed:a9b9
  Connecting to gitlab.com (gitlab.com)|172.65.251.78|:443... connected.
  HTTP request sent, awaiting response... 404 Not Found
  2021-09-23 11:00:13 ERROR 404: Not Found.
  ```

- 未经授权的 API 请求：

  ```plaintext
  $ curl --verbose --request POST --form "token=$CI_JOB_TOKEN" --form ref=master "https://gitlab.com/api/v4/projects/1234/trigger/pipeline"

  < HTTP/2 404
  < date: Thu, 23 Sep 2021 11:00:12 GMT
  {"message":"404 Not Found"}
  < content-type: application/json
  ```

在对 CI/CD 作业令牌身份验证问题进行故障排查时，请注意：

- 当启用了 [CI/CD 作业令牌限制](#configure-cicd-job-token-access)，并且作业令牌被用于访问不同的项目时：
   - 执行作业的用户必须是正在访问的项目的成员。
   - 用户必须具有权限才能执行操作。
   - 被访问的项目必须将尝试访问它的项目[添加到入站白名单](#add-a-project-to-the-inbound-job-token-scope-allowlist)中。
- 如果作业不再运行、已被删除或项目正在被删除，则 CI 作业令牌将无效。
