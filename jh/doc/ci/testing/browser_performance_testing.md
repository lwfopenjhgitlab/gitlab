---
stage: Verify
group: Pipeline Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 浏览器性能测试 **(PREMIUM)**

如果您的应用程序提供 Web 界面并且您正在使用[极狐GitLab CI/CD](../index.md)，您可以快速确定浏览器中，待处理的代码更改对渲染性能的影响。

NOTE:
您可以使用 [Auto DevOps](../../topics/autodevops/index.md) 在您的应用程序中自动执行此功能。

## 概览

极狐GitLab 使用免费的开源工具 [Sitespeed.io](https://www.sitespeed.io) 来测量网站的渲染性能。由系统构建的 Sitespeed 插件在名为 `browser-performance.json` 的文件中，输出分析的每个页面的性能分数，这些数据可以在合并请求中显示。

## 用例

考虑以下工作流程：

1. 营销团队的一名成员正试图通过添加新工具来跟踪参与度。
1. 通过浏览器性能指标，他们可以看到他们的更改如何影响最终用户页面的可用性。
1. 指标显示，更改后，页面的性能得分有所下降。
1. 在查看详细报告时，他们看到新的 JavaScript 库包含在 `<head>` 中，这会影响加载页面的速度。
1. 他们向前端开发人员寻求帮助，后者将库设置为异步加载。
1. 前端开发者批准合并请求，并授权其部署到生产环境。

<a id="how-browser-performance-testing-works"></a>

## 浏览器性能测试的工作原理

首先，在您的 `.gitlab-ci.yml` 文件中定义一个生成[浏览器性能报告产物](../yaml/artifacts_reports.md#artifactsreportsbrowser_performance)的作业。
系统然后检查此报告，比较源分支和目标分支之间每个页面的关键性能指标，并在合并请求中显示信息。

有关浏览器性能作业的示例，请参阅[配置浏览器性能测试](#configuring-browser-performance-testing)。

NOTE:
如果浏览器性能报告没有可比较的数据，例如当您第一次在 `.gitlab-ci.yml` 中添加浏览器性能作业时，浏览器性能报告小部件不会显示。它必须至少在目标分支（例如`main`）上运行一次，然后才能显示在针对该分支的合并请求中。

![Browser Performance Widget](img/browser_performance_testing.png)

<a id="configuring-browser-performance-testing"></a>

## 配置浏览器性能测试

这个例子展示了如何使用极狐GitLab CI/CD 和 Docker-in-Docker [sitespeed.io](https://hub.docker.com/r/sitespeedio/sitespeed.io/)，在您的代码上运行 [sitespeed.io 容器](https://hub.docker.com/r/sitespeedio/sitespeed.io/)。

1. 首先，使用 [Docker-in-Docker 构建](../docker/using_docker_build.md#use-docker-in-docker)设置极狐GitLab Runner。
1. 在 `.gitlab-ci.yml` 文件中配置默认的浏览器性能测试 CI/CD 作业，如下所示：

   ```yaml
   include:
     template: Verify/Browser-Performance.gitlab-ci.yml

   browser_performance:
     variables:
       URL: https://example.com
   ```

WARNING:
在 13.12 及更早版本中，该作业被命名为 `performance`。

在以上示例中：

- 在您的 CI/CD 流水线中创建一个 `browser_performance` 作业，并针对您在 `URL` 中定义的网页运行 sitespeed.io 以收集关键指标。
- 使用不适用于 Kubernetes 集群的模板。如果您使用的是 Kubernetes 集群，请使用 [`template: Jobs/Browser-Performance-Testing.gitlab-ci.yml`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Browser-Performance-Testing.gitlab-ci.yml) 代替。

<!--
- Uses a CI/CD template that is included in all GitLab installations since 12.4. If you are using
  GitLab 12.3 or earlier, you must [add the configuration manually](#gitlab-versions-132-and-earlier).
-->

该模板使用 gitlab plugin for sitespeed.io，并将完整的 HTML sitespeed.io 报告保存为[浏览器性能报告产物](../yaml/artifacts_reports.md#artifactsreportsbrowser_performance)，供您稍后下载和分析。此实现始终采用可用的最新浏览器性能产物。如果启用了极狐GitLab Pages，您可以直接在浏览器中查看报告。

您还可以使用 CI/CD 变量自定义作业：

- `SITESPEED_IMAGE`：配置用于作业的 Docker 镜像（默认为 `sitespeedio/sitespeed.io`），但不是镜像版本。
- `SITESPEED_VERSION`：配置用于作业的 Docker 镜像的版本（默认为 `14.1.0`）。
- `SITESPEED_OPTIONS`：根据需要配置任何其他 sitespeed.io 选项（默认为 `nil`）。有关详细信息，请参阅 [sitespeed.io 文档](https://www.sitespeed.io/documentation/sitespeed.io/configuration/)。

例如，您可以覆盖 sitespeed.io 在给定 URL 上的运行次数，并更改版本：

```yaml
include:
  template: Verify/Browser-Performance.gitlab-ci.yml

browser_performance:
  variables:
    URL: https://www.sitespeed.io/
    SITESPEED_VERSION: 13.2.0
    SITESPEED_OPTIONS: -n 5
```

### 配置降级阈值

您可以配置降级警报的敏感度，以避免收到有关指标轻微下降的警报。
这是通过设置 `DEGRADATION_THRESHOLD` CI/CD 变量来完成的。在下面的示例中，仅当 `Total Score` 指标下降 5 分或更多时才会显示警报：

```yaml
include:
  template: Verify/Browser-Performance.gitlab-ci.yml

browser_performance:
  variables:
    URL: https://example.com
    DEGRADATION_THRESHOLD: 5
```

`Total Score` 指标基于 sitespeed.io 的 [Coach 性能得分](https://www.sitespeed.io/documentation/sitespeed.io/metrics/#performance-score)。查看 [Coach 文档](https://www.sitespeed.io/documentation/coach/how-to/#what-do-the-coach-do)，获取更多信息。

### Review Apps 的性能测试

上面的 CI YAML 配置非常适合针对静态环境进行测试，并且可以针对动态环境进行扩展，但需要一些额外的步骤：

1. `browser_performance` 作业应该在动态环境启动后运行。
1. 在 `review` 作业中：
     1. 生成带有动态 URL 的 URL 列表文件。
     1. 将文件保存为产物，例如使用在作业的  `script` 中，使用 `echo $CI_ENVIRONMENT_URL > environment_url.txt`。
     1. 将列表作为 URL 环境变量（可以是 URL 或包含 URL 的文件）传递给 `browser_performance` 作业。
1. 您现在可以针对所需的主机名和路径运行 sitespeed.io 容器。

您的 `.gitlab-ci.yml` 文件：

```yaml
stages:
  - deploy
  - performance

include:
  template: Verify/Browser-Performance.gitlab-ci.yml

review:
  stage: deploy
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: http://$CI_COMMIT_REF_SLUG.$APPS_DOMAIN
  script:
    - run_deploy_script
    - echo $CI_ENVIRONMENT_URL > environment_url.txt
  artifacts:
    paths:
      - environment_url.txt
  only:
    - branches
  except:
    - master

browser_performance:
  dependencies:
    - review
  variables:
    URL: environment_url.txt
```

<!--
### GitLab versions 13.2 and earlier

Browser Performance Testing has gone through several changes since its introduction.
In this section we detail these changes and how you can run the test based on your
GitLab version:

- In 13.2 the feature was renamed from `Performance` to `Browser Performance` with additional
  template CI/CD variables.
- In GitLab 12.4 [a job template was made available](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Verify/Browser-Performance.gitlab-ci.yml).
- For 11.5 to 12.3 no template is available and the job has to be defined manually as follows:

  ```yaml
  performance:
    stage: performance
    image: docker:git
    variables:
      URL: https://example.com
      SITESPEED_VERSION: 14.1.0
      SITESPEED_OPTIONS: ''
    services:
      - docker:stable-dind
    script:
      - mkdir gitlab-exporter
      - wget -O ./gitlab-exporter/index.js https://gitlab.com/gitlab-org/gl-performance/raw/1.1.0/index.js
      - mkdir sitespeed-results
      - docker run --shm-size=1g --rm -v "$(pwd)":/sitespeed.io sitespeedio/sitespeed.io:$SITESPEED_VERSION --plugins.add ./gitlab-exporter --outputFolder sitespeed-results $URL $SITESPEED_OPTIONS
      - mv sitespeed-results/data/performance.json performance.json
    artifacts:
      paths:
        - performance.json
        - sitespeed-results/
      reports:
        performance: performance.json
  ```

- For 11.4 and earlier the job should be defined as follows:

  ```yaml
  performance:
    stage: performance
    image: docker:git
    variables:
      URL: https://example.com
    services:
      - docker:stable-dind
    script:
      - mkdir gitlab-exporter
      - wget -O ./gitlab-exporter/index.js https://gitlab.com/gitlab-org/gl-performance/raw/1.1.0/index.js
      - mkdir sitespeed-results
      - docker run --shm-size=1g --rm -v "$(pwd)":/sitespeed.io sitespeedio/sitespeed.io:6.3.1 --plugins.add ./gitlab-exporter --outputFolder sitespeed-results $URL
      - mv sitespeed-results/data/performance.json performance.json
    artifacts:
      paths:
        - performance.json
        - sitespeed-results/
  ```

Upgrading to the latest version and using the templates is recommended, to ensure
you receive the latest updates, including updates to the sitespeed.io versions.
-->
