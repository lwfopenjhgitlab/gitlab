---
stage: Verify
group: Pipeline Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 可访问性测试 **(FREE)**

如果您的应用程序提供 Web 界面，您可以使用[极狐GitLab CI/CD](../index.md) 来确定待处理的代码更改对可访问性的影响。

[Pa11y](https://pa11y.org/) 是一个免费的开源工具，用于衡量网站的可访问性。极狐GitLab 将 Pa11y 集成到 [CI 作业模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Verify/Accessibility.gitlab-ci.yml)。
`a11y` 作业分析一组定义的网页，并在名为 `accessibility` 的文件中报告可访问性违规、警告和通知。

从 14.5 版本开始，Pa11y 使用 [WCAG 2.1 规则](https://www.w3.org/TR/WCAG21/#new-features-in-wcag-2-1)。

## 可访问性合并请求部件

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/39425) in GitLab 13.0 behind the disabled [feature flag](../../../administration/feature_flags.md) `:accessibility_report_view`.
> - [Feature Flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/217372) in GitLab 13.1.
-->

系统在合并请求部件区域中显示**可访问性报告**：

![Accessibility merge request widget](img/accessibility_mr_widget_v13_0.png)

## 配置可访问性测试

您可以使用极狐GitLab Accessibility Docker 镜像，通过极狐GitLab CI/CD 运行 Pa11y。

在 12.9 及更高版本中，定义 `a11y` 作业：

1. [包含](../yaml/index.md#includetemplate)来自您的安装实例中的 [`Accessibility.gitlab-ci.yml` 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Verify/Accessibility.gitlab-ci.yml)。
1. 将以下配置添加到您的 `.gitlab-ci.yml` 文件中。

   ```yaml
   stages:
     - accessibility

   variables:
     a11y_urls: "https://about.gitlab.com https://gitlab.com/users/sign_in"

   include:
     - template: "Verify/Accessibility.gitlab-ci.yml"
   ```

1. 自定义 `a11y_urls` 变量，列出要使用 Pa11y 测试的网页的 URL。

CI/CD 流水线中的 `a11y` 作业会生成这些文件：

- `a11y_urls` 变量中列出的每个 URL 一份 HTML 报告。
- 一个包含收集到的报告数据的文件。在 12.11 及更高版本中，此文件名为 `gl-accessibility.json`。在 12.10 及更早版本中，此文件名为 `accessibility.json`。

您可以[在浏览器中查看作业产物](../pipelines/job_artifacts.md#download-job-artifacts)。

NOTE:
如果低于 12.9 版本，使用 `include:remote` 并链接到[默认分支中的当前模板](https://jihulab.com/gitlab-cn/gitlab/-/raw/master/lib/gitlab/ci/templates/Verify/Accessibility.gitlab-ci.yml)。

NOTE:
模板提供的作业定义不支持 Kubernetes。

您不能通过 CI 配置将配置传递到 Pa11y。
要更改配置，请在 CI 文件中编辑模板的副本。
