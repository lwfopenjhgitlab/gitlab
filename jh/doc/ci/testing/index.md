---
stage: Verify
group: Pipeline Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用极狐GitLab CI/CD 进行测试并在合并请求中生成报告 **(FREE)**

使用极狐GitLab CI/CD 测试功能分支中包含的更改。您还可以显示报告，或指向直接来自[合并请求](../../user/project/merge_requests/index.md)的重要信息的链接。

| 功能                                                                                         | 描述 |
|-------------------------------------------------------------------------------------------------|-------------|
| [可访问性测试](../../user/project/merge_requests/accessibility_testing.md)             | 自动报告合并请求中更改页面的 A11y 违规。 |
| [浏览器性能测试](../../user/project/merge_requests/browser_performance_testing.md) | 快速确定待处理代码更改对浏览器性能的影响。 |
| [负载性能测试](../../user/project/merge_requests/load_performance_testing.md)       | 快速确定待处理代码更改对服务器性能的影响。 |
| [代码质量](../../user/project/merge_requests/code_quality.md)                               | 使用 [Code Climate](https://codeclimate.com/) 分析器分析您的源代码质量，并在合并请求部件区域中显示 Code Climate 报告。 |
| [显示任意作业产物](../yaml/index.md#artifactsexpose_as)                          | 使用 `artifacts:expose_as` 参数配置 CI 流水线直接链接到合并请求中选定的[产物](../pipelines/job_artifacts.md)。 |
| [单元测试报告](unit_test_reports.md)                                                       | 将您的 CI 作业配置为使用单元测试报告，并让极狐GitLab 显示有关合并请求的报告，以便更轻松、更快地识别故障，而无需检查整个作业日志。 |
| 许可证合规<!--[License Compliance](../../user/compliance/license_compliance/index.md)-->                         | 管理依赖项的许可证。 |
| 指标报告<!--[指标报告](../metrics_reports.md)-->                                                        | 在合并请求上显示指标报告，以便更快速、更轻松地识别重要指标的更改。 |
| [测试覆盖率可视化](../../user/project/merge_requests/test_coverage_visualization.md) | 查看文件差异中，合并请求的测试覆盖率结果。 |
| [快速测试失败](../../user/project/merge_requests/fail_fast_testing.md)   | 运行 RSpec 测试套件的子集，因此失败的测试会在全套测试运行之前停止流水线，从而节省资源。 |

## 安全报告 **(ULTIMATE)**

除了上面列出的报告之外，极狐GitLab 还可以通过扫描和报告项目中发现的任何漏洞生成多种类型的[安全报告](../../user/application_security/index.md)：

| 功能                                                                                      | 描述 |
|----------------------------------------------------------------------------------------------|-------------|
| [容器扫描](../../user/application_security/container_scanning/index.md)            | 分析您的 Docker 镜像中的已知漏洞。 |
| [动态应用程序安全测试 (DAST)](../../user/application_security/dast/index.md) | 分析您正在运行的 Web 应用程序中的已知漏洞。 |
| [依赖项测试](../../user/application_security/dependency_scanning/index.md)          | 分析已知漏洞的依赖关系。 |
| [静态应用程序安全测试 (SAST)](../../user/application_security/sast/index.md)  | 分析您的源代码中的已知漏洞。 |
