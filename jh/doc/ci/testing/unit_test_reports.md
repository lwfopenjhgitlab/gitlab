---
stage: Verify
group: Pipeline Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 单元测试报告 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/45318) in GitLab 11.2. Requires GitLab Runner 11.2 and above.
> - [Renamed](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/39737) from JUnit test reports to Unit test reports in GitLab 13.4.
-->

[CI/CD 流水线](../pipelines/index.md)通常包含验证您的代码的测试作业。
如果测试失败，流水线将失败并通知用户。处理合并请求的人必须检查作业日志并查看测试失败的地方，以便可以修复它们。

您可以将作业配置为使用单元测试报告，极狐GitLab 会显示有关合并请求的报告，以便更轻松、更快速地识别故障，而无需检查整个日志。单元测试报告目前仅支持 JUnit 报告格式的测试报告。

如果您不使用合并请求但仍希望在不搜索作业日志的情况下查看单元测试报告输出，则流水线中提供完整的[单元测试报告](#view-unit-test-reports-on-gitlab)详细视图。

考虑以下工作流程：

1. 您的默认分支坚如磐石，您的项目正在使用极狐GitLab CI/CD，并且您的流水线表明没有任何损坏。
1. 您团队中的某个人提交了合并请求，测试失败并且流水线获得已知红色图标。要进行更多调查，您必须查看作业日志来找出测试失败的原因，这些日志通常包含数千行。
1. 您配置单元测试报告，极狐GitLab 立即收集并在合并请求中公开它们，您不再需要在作业日志中搜索。
1. 您的开发和调试工作流程变得更容易、更快、更高效。

<a id="how-it-works"></a>

## 工作原理

首先，极狐GitLab Runner 上传所有 [JUnit 报告格式 XML 文件](https://www.ibm.com/docs/en/adfz/developer-for-zos/14.1.0?topic=formats-junit-xml-format)到极狐GitLab，作为[产物](../yaml/artifacts_reports.md#artifactsreportsjunit)。然后，当您访问合并请求时，极狐GitLab 开始比较 head 和 base 分支的 JUnit 报告格式 XML 文件，其中：

- base 分支是目标分支（通常是默认分支）。
- head 分支是源分支（每个合并请求中的最新流水线）。

报告面板有一个摘要，显示有多少测试失败、有多少错误以及有多少已修复。如果由于 base 分支的数据不可用而无法进行比较，则面板仅显示 head 的失败测试列表。

有四种类型的结果：

1. **新失败的测试：**测试用例在 base 分支上通过，在 head 分支上失败
1. **新遇到的错误：**测试用例在 base 分支上通过，但由于 head 分支上的测试错误而失败
1. **现有故障：**测试用例在 base 分支上失败，在 head 分支上失败
1. **已解决的故障：** 测试用例在 base 分支上失败，在 head 分支上通过

面板中的每个条目都显示了上面列表中的测试名称及其类型。单击测试名称会打开一个窗口，其中包含其执行时间和错误输出的详细信息。

![Test Reports Widget](img/junit_test_report.png)

### 最近失败的次数

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/241759) in merge requests in GitLab 13.7.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/268249) in GitLab 13.8.
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/235525) in Test Reports in GitLab 13.9.
-->

如果过去 14 天内项目的默认分支中的测试失败，则会为该测试显示一条消息，如 `Failed {n} time(s) in {default_branch} in the last 14 days`。

## 如何设置

要在合并请求中启用单元测试报告，您必须在 `.gitlab-ci.yml` 中添加 [`artifacts:reports:junit`](../yaml/artifacts_reports.md#artifactsreportsjunit)，并指定生成测试报告的路径。
报告必须是 .xml 文件，否则极狐GitLab 返回错误 500。

在以下 Ruby 示例中，`test` 阶段的作业运行，系统从作业中收集单元测试报告。作业执行后，XML 报告作为产物存储在极狐GitLab 中，结果显示在合并请求部件中。

```yaml
## Use https://github.com/sj26/rspec_junit_formatter to generate a JUnit report format XML file with rspec
ruby:
  stage: test
  script:
    - bundle install
    - bundle exec rspec --format progress --format RspecJunitFormatter --out rspec.xml
  artifacts:
    when: always
    paths:
      - rspec.xml
    reports:
      junit: rspec.xml
```

要使单元测试报告输出文件可浏览，请将它们包含在 [`artifacts:paths`](../yaml/index.md#artifactspaths) 关键字中，如示例中所示。
即使作业失败（例如，如果测试未通过）也要上传报告，请使用 [`artifacts:when:always`](../yaml/index.md#artifactswhen) 关键字。

JUnit 报告格式 XML 文件中不能有多个具有相同名称和类的测试。

在 15.0 及更早版本中，来自 [parallel:matrix](../yaml/index.md#parallel:matrix) 作业的测试报告被聚合在一起，可能会导致某些报告信息不显示。
在 15.1 及更高版本中，此 bug 已修复，并显示所有报告信息。

<a id="view-unit-test-reports-on-gitlab"></a>

## 在极狐GitLab 上查看单元测试报告

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/24792) in GitLab 12.5 behind a feature flag (`junit_pipeline_view`), disabled by default.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/216478) in GitLab 13.3.
-->

如果生成 JUnit 报告格式 XML 文件并将其作为流水线的一部分上传，则可以在流水线详细信息页面中查看这些报告。此页面上的 **测试** 选项卡显示从 XML 文件报告的测试套件和案例列表。

![Test Reports Widget](img/pipelines_junit_test_report_v13_10.png)

您可以查看所有已知的测试套件并选择其中的每一个以查看更多详细信息，包括组成该套件的案例。

<!--
You can also retrieve the reports via the [GitLab API](../api/pipelines.md#get-a-pipelines-test-report).
-->

### 单元测试报告解析错误

> 引入于 13.10 版本

如果解析 JUnit 报告 XML 导致错误，作业名称旁边会显示一个指示符。将鼠标悬停在图标上会在工具提示中显示解析器错误。如果多个解析错误来自分组作业，仅显示该组中的第一个错误。

![Test Reports With Errors](img/pipelines_junit_test_report_with_errors_v13_10.png)

<!--
For test case parsing limits, see [Max test cases per unit test report](../user/gitlab_com/#gitlab-cicd).
-->

极狐GitLab 不会解析非常[大的节点](https://nokogiri.org/tutorials/parsing_an_html_xml_document.html#parse-options) 的 JUnit 报告。<!--There is [an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/268035) open to make this optional.-->

## 在极狐GitLab 上查看 JUnit 屏幕截图

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/202114) in GitLab 13.0 behind the `:junit_pipeline_screenshots_view` feature flag, disabled by default.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/216979) in GitLab 13.12.
-->

将您的屏幕截图作为产物上传到极狐GitLab。如果 JUnit 报告格式 XML 文件包含 `attachment` 标签，极狐GitLab 会解析附件。注意：

- `attachment` 标签**必须**包含您上传的屏幕截图的 `$CI_PROJECT_DIR` 的相对路径。例如：

  ```xml
  <testcase time="1.00" name="Test">
    <system-out>[[ATTACHMENT|/path/to/some/file]]</system-out>
  </testcase>
  ```

- 您应该将上传屏幕截图的作业设置为 [`artifacts:when: always`](../yaml/index.md#artifactswhen)，以便在测试失败时仍然上传屏幕截图。

测试用例附件的链接出现在[流水线测试报告](#view-unit-test-reports-on-gitlab)的测试用例详细信息中。
