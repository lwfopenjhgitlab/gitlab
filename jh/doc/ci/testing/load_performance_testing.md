---
stage: Verify
group: Pipeline Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 负载性能测试 **(PREMIUM)**

> 引入于 13.2 版本。

通过负载性能测试，您可以在[极狐GitLab CI/CD](../index.md) 中测试任何待定的代码更改对应用程序后端的影响。

极狐GitLab 使用 [k6](https://k6.io/)，一个免费的开源工具，测量负载下应用程序的系统性能。

与[浏览器性能测试](browser_performance_testing.md)不同的是，它用于衡量网站在客户端浏览器中的执行情况，负载性能测试可用于执行针对应用程序端点的各种类型的[负载测试](https://k6.io/docs/#use-cases)，例如 API、Web 控制器等，可用于测试后端或服务器如何大规模执行。

例如，您可以使用负载性能测试对应用程序中的流行 API 端点执行许多并发 GET 调用，以查看其执行情况。

## 负载性能测试工作原理

首先，在您的 `.gitlab-ci.yml` 文件中定义一个生成[负载性能报告产物](../yaml/artifacts_reports.md#artifactsreportsload_performance)的作业。
系统检查此报告，比较源分支和目标分支之间的关键负载性能指标，然后在合并请求部件中显示信息：

![Load Performance Widget](img/load_performance_testing.png)

接下来需要配置测试环境，编写 k6 测试。

测试完成后合并请求部件显示的关键性能指标：

- Checks：k6 测试中配置的 [checks](https://k6.io/docs/using-k6/checks) 的通过率百分比。
- TTFB P90：开始接收响应所需时间，也就是第一个字节的时间（TTFB）的 90 百分位（the 90th percentile）。
- TTFB P95：TTFB 的 95 百分位（the 95th percentile）。
- RPS：测试能够达到的平均每秒请求 (RPS) 速率。

NOTE:
如果加载性能报告没有可比较的数据，例如当您第一次在 `.gitlab-ci.yml` 中添加加载性能作业时，加载性能报告部件不会显示。它必须至少在目标分支（例如 `main`）上运行一次，然后才能显示在针对该分支的合并请求中。

## 配置负载性能测试作业

配置负载性能测试作业可以分为几个不同的部分：

- 确定吞吐量等测试参数。
- 设置负载性能测试的目标测试环境。
- 设计并编写 k6 测试。

### 确定测试参数

您需要做的第一件事是确定您要运行的[负载测试类型](https://k6.io/docs/test-types/introduction)，以及它将如何运行（例如用户数、吞吐量等）。

请参阅 [k6 文档](https://k6.io/docs/)，尤其是 [k6 测试指南](https://k6.io/docs/testing-guides)，获取有关上述内容的指导以及更多信息。

### 测试环境设置

负载性能测试的很大一部分工作是为高负载准备目标测试环境。您应该确保它能够处理将被测试的[吞吐量](https://k6.io/blog/monthly-visits-concurrent-users)。

通常还需要在目标环境中有代表性的测试数据，以供负载性能测试使用。

我们强烈建议[不要在生产环境中运行这些测试](https://k6.io/our-beliefs#load-test-in-a-pre-production-environment)。

### 编写负载性能测试

环境准备好之后，就可以自己编写 k6 测试了。k6 是一个灵活的工具，可用于运行[多种性能测试](https://k6.io/docs/test-types/introduction)。有关如何编写测试的详细信息，请参阅 [k6 文档](https://k6.io/docs/)。

### 在极狐GitLab CI/CD 中配置测试

当您的 k6 测试准备就绪后，下一步是在极狐GitLab CI/CD 中配置负载性能测试作业。最简单的方法是使用包含在实例中的模板 [`Verify/Load-Performance-Testing.gitlab-ci.yml`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Verify/Load-Performance-Testing.gitlab-ci.yml)。

NOTE:
对于大规模的 k6 测试，您需要确保执行实际测试的极狐GitLab Runner 实例能够处理运行测试。有关规范的详细信息，请参阅 [k6 的指南](https://k6.io/docs/testing-guides/running-large-tests#hardware-considerations)。<!--[默认共享 runner](../../../ci/runners/saas/linux_saas_runner.md)-->默认共享 runner 可能没有足够的规格来处理大多数大型 k6 测试。

此模板在作业中运行 [k6 Docker 容器](https://hub.docker.com/r/loadimpact/k6/)，并提供了多种自定义作业的方法。

示例配置工作流程：

1. 设置极狐GitLab Runner 来运行 Docker 容器，例如 [Docker-in-Docker 工作流程](../docker/using_docker_build.md#use-docker-in-docker)。
1. 在 `.gitlab-ci.yml` 文件中配置默认的负载性能测试 CI/CD 作业。您需要包含模板并使用 CI/CD 变量对其进行配置：

   ```yaml
   include:
     template: Verify/Load-Performance-Testing.gitlab-ci.yml

   load_performance:
     variables:
       K6_TEST_FILE: <PATH TO K6 TEST FILE IN PROJECT>
   ```

上面的示例在运行 k6 测试的 CI/CD 流水线中创建了一个 `load_performance` 作业。

NOTE:
对于 Kubernetes 设置，应使用不同的模板：[`Jobs/Load-Performance-Testing.gitlab-ci.yml`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Load-Performance-Testing.gitlab-ci.yml)。

k6 有[各种选项](https://k6.io/docs/using-k6/options)来配置它将如何运行测试，例如运行的吞吐量 (RPS)、测试应该运行多长时间等等。几乎所有选项都可以在测试本身中配置，但您也可以通过 `K6_OPTIONS` 变量传递命令行选项。

例如，您可以使用 CLI 选项覆盖测试的持续时间：

```yaml
  include:
    template: Verify/Load-Performance-Testing.gitlab-ci.yml

  load_performance:
    variables:
      K6_TEST_FILE: <PATH TO K6 TEST FILE IN PROJECT>
      K6_OPTIONS: '--duration 30s'
```

如果 k6 的结果通过[摘要导出](https://k6.io/docs/results-output/real-time/json/#summary-export)，作为[负载性能报告产物](../yaml/artifacts_reports.md#artifactsreportsload_performance)保存，系统仅在 MR 部件中显示关键性能指标。始终使用最新的可用负载性能产物，使用来自测试的汇总值。

如果启用了极狐GitLab Pages，您可以直接在浏览器中查看报告。

### Review Apps 中的负载性能测试

上面的 CI/CD YAML 配置示例适用于针对静态环境进行测试，通过一些额外步骤将其扩展为与 Review App 或[动态环境](../environments/index.md)一起使用。

最好的方法是在 [`.env` 文件](https://docs.docker.com/compose/env-file/) 中捕获动态 URL 作为要共享的作业产物，然后使用我们提供的名为 `K6_DOCKER_OPTIONS` 的自定义 CI/CD 变量来配置 k6 Docker 容器来使用该文件。这样，k6 就可以在标准 JavaScript 的脚本中，使用来自 `.env` 文件的任何环境变量，例如：`http.get(`${__ENV.ENVIRONMENT_URL}`)`。

例如：

1. 在 `review` 作业中：
   1. 捕获动态 URL 并将其保存到 `.env` 文件中，例如，`echo "ENVIRONMENT_URL=$CI_ENVIRONMENT_URL" >> review.env`。
   1. 将 `.env` 文件设置为[作业产物](../pipelines/job_artifacts.md)。
1. 在 `load_performance` 作业中：
   1. 将其设置为依赖于 review 作业，因此它会继承环境文件。
   1. 使用 [Docker CLI 环境文件选项](https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env-- -env-file)设置 `K6_DOCKER_OPTIONS` 变量，例如 `--env-file review.env`。
1. 配置 k6 测试脚本，在其步骤中使用环境变量。

您的 `.gitlab-ci.yml` 文件可能类似于：

```yaml
stages:
  - deploy
  - performance

include:
  template: Verify/Load-Performance-Testing.gitlab-ci.yml

review:
  stage: deploy
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: http://$CI_ENVIRONMENT_SLUG.example.com
  script:
    - run_deploy_script
    - echo "ENVIRONMENT_URL=$CI_ENVIRONMENT_URL" >> review.env
  artifacts:
    paths:
      - review.env
  rules:
    - if: $CI_COMMIT_BRANCH  # Modify to match your pipeline rules, or use `only/except` if needed.

load_performance:
  dependencies:
    - review
  variables:
    K6_DOCKER_OPTIONS: '--env-file review.env'
  rules:
    - if: $CI_COMMIT_BRANCH  # Modify to match your pipeline rules, or use `only/except` if needed.
```
