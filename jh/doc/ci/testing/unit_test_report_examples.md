---
stage: Verify
group: Pipeline Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 单元测试报告示例 **(FREE)**

[单元测试报告](unit_test_reports.md)可以为多种语言和包生成。
使用这些示例作为配置流水线，生成列出的语言和包的单元测试报告的指南。您可能需要编辑示例来匹配您正在使用的语言或包的版本。

## Ruby

在 `.gitlab-ci.yml` 中使用以下作业，包括 `artifacts:paths` 关键字来提供指向单元测试报告输出文件的链接。

```yaml
## Use https://github.com/sj26/rspec_junit_formatter to generate a JUnit report format XML file with rspec
ruby:
  image: ruby:3.0.4
  stage: test
  before_script:
    - apt-get update -y && apt-get install -y bundler
  script:
    - bundle install
    - bundle exec rspec --format progress --format RspecJunitFormatter --out rspec.xml
  artifacts:
    when: always
    paths:
      - rspec.xml
    reports:
      junit: rspec.xml
```

## Go

在 `.gitlab-ci.yml` 中使用以下作业：

```yaml
## Use https://github.com/gotestyourself/gotestsum to generate a JUnit report format XML file with go
golang:
  stage: test
  script:
    - go install gotest.tools/gotestsum@latest
    - gotestsum --junitfile report.xml --format testname
  artifacts:
    when: always
    reports:
      junit: report.xml
```

## Java

有一些工具可以在 Java 中生成 JUnit 报告格式的 XML 文件。

### Gradle

在以下示例中，`gradle` 用于生成测试报告。
如果定义了多个测试任务，`gradle` 会在 `build/test-results/` 下生成多个目录。在这种情况下，您可以通过定义以下路径来利用全局匹配：`build/test-results/test/**/TEST-*.xml`：

```yaml
java:
  stage: test
  script:
    - gradle test
  artifacts:
    when: always
    reports:
      junit: build/test-results/test/**/TEST-*.xml
```

在极狐GitLab Runner 13.0 及更高版本中，您可以使用 `**`。

### Maven

用于解析 [Surefire](https://maven.apache.org/surefire/maven-surefire-plugin/) 和 [Failsafe](https://maven.apache.org/surefire/maven-failsafe-plugin/) 测试报告，在 `.gitlab-ci.yml` 中使用以下作业：

```yaml
java:
  stage: test
  script:
    - mvn verify
  artifacts:
    when: always
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml
        - target/failsafe-reports/TEST-*.xml
```

## Python

此示例使用带有 `--junitxml=report.xml` 标志的 pytest 将输出格式化为 JUnit 报告 XML 格式：

```yaml
pytest:
  stage: test
  script:
    - pytest --junitxml=report.xml
  artifacts:
    when: always
    reports:
      junit: report.xml
```

## C/C++

有一些工具可以在 C/C++ 中生成 JUnit 报告格式的 XML 文件。

### GoogleTest

在以下示例中，`gtest` 用于生成测试报告。
如果为不同的架构（`x86`、`x64` 或 `arm`）创建了多个 `gtest` 可执行文件，则需要运行每个测试并提供唯一的文件名。然后将结果汇总在一起。

```yaml
cpp:
  stage: test
  script:
    - gtest.exe --gtest_output="xml:report.xml"
  artifacts:
    when: always
    reports:
      junit: report.xml
```

### CUnit

当在运行时使用它的 `CUnitCI.h` 宏，[CUnit](https://cunity.gitlab.io/cunit/)自动生成 [JUnit 报告格式 XML 文件](https://cunity.gitlab.io/cunit/group__CI.html)：

```yaml
cunit:
  stage: test
  script:
    - ./my-cunit-test
  artifacts:
    when: always
    reports:
      junit: ./my-cunit-test.xml
```

## .NET

[JunitXML.TestLogger](https://www.nuget.org/packages/JunitXml.TestLogger/) NuGet 包可以为 .Net Framework 和 .Net Core 应用程序生成测试报告。以下示例需要仓库根文件夹中的解决方案，子文件夹中包含一个或多个项目文件。每个测试项目生成一个结果文件，每个文件都放置在 artifacts 文件夹中。这个例子包括可选的格式化参数，它提高了测试部件中测试数据的可读性。<!--A full .Net Core
[example is available](https://gitlab.com/Siphonophora/dot-net-cicd-test-logging-demo).-->

```yaml
## Source code and documentation are here: https://github.com/spekt/junit.testlogger/

Test:
  stage: test
  script:
    - 'dotnet test --test-adapter-path:. --logger:"junit;LogFilePath=..\artifacts\{assembly}-test-result.xml;MethodFormat=Class;FailureBodyFormat=Verbose"'
  artifacts:
    when: always
    paths:
      - ./**/*test-result.xml
    reports:
      junit:
        - ./**/*test-result.xml
```

## JavaScript

有一些工具可以在 JavaScript 中生成 JUnit 报告格式的 XML 文件。

### Jest

[jest-junit](https://github.com/jest-community/jest-junit) npm 包可以为 JavaScript 应用程序生成测试报告。
在以下 `.gitlab-ci.yml` 示例中，`javascript` 作业使用 Jest 生成测试报告：

```yaml
javascript:
  image: node:latest
  stage: test
  before_script:
    - 'yarn global add jest'
    - 'yarn add --dev jest-junit'
  script:
    - 'jest --ci --reporters=default --reporters=jest-junit'
  artifacts:
    when: always
    reports:
      junit:
        - junit.xml
```

要在没有带有单元测试的 `.test.js` 文件时使作业通过，请在 `script:` 部分的 `jest` 命令末尾添加 `--passWithNoTests` 标志。

### Karma

[Karma-junit-reporter](https://github.com/karma-runner/karma-junit-reporter) npm 包可以为 JavaScript 应用程序生成测试报告。
在以下 `.gitlab-ci.yml` 示例中，`javascript` 作业使用 Karma 生成测试报告：

```yaml
javascript:
  stage: test
  script:
    - karma start --reporters junit
  artifacts:
    when: always
    reports:
      junit:
        - junit.xml
```

### Mocha

[JUnit Reporter for Mocha](https://github.com/michaelleeallen/mocha-junit-reporter) NPM 包可以为 JavaScript 应用程序生成测试报告。
在以下 `.gitlab-ci.yml` 示例中，`javascript` 作业使用 Mocha 生成测试报告：

```yaml
javascript:
  stage: test
  script:
    - mocha --reporter mocha-junit-reporter --reporter-options mochaFile=junit.xml
  artifacts:
    when: always
    reports:
      junit:
        - junit.xml
```

## Flutter 或 Dart

此示例 `.gitlab-ci.yml` 文件使用 [JUnit Report](https://pub.dev/packages/junitreport) 包将 `flutter test` 输出转换为 JUnit 报告 XML 格式：

```yaml
test:
  stage: test
  script:
    - flutter test --machine | tojunit -o report.xml
  artifacts:
    when: always
    reports:
      junit:
        - report.xml
```

## PHP

此示例使用带有 `--log-junit` 标志的 [PHPUnit](https://phpunit.de/)。
您还可以在 `phpunit.xml` 配置文件中使用 [XML](https://phpunit.readthedocs.io/en/stable/configuration.html#the-junit-element) 添加此选项。

```yaml
phpunit:
  stage: test
  script:
    - composer install
    - vendor/bin/phpunit --log-junit report.xml
  artifacts:
    when: always
    reports:
      junit: report.xml
```
