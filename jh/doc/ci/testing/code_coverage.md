---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 代码覆盖率 **(FREE)**

使用代码覆盖率提供有关测试套件正在验证哪些源代码的信息。代码覆盖率是可以确定软件性能和质量的众多测试指标之一。

## 查看代码覆盖率结果

代码覆盖率结果显示在：

- 合并请求部件
- 项目仓库分析
- 群组仓库分析
- 仓库徽章

更多关于合并请求文件差异中测试覆盖率可视化的信息，请参见[测试覆盖率可视化](test_coverage_visualization.md)。

<a id="view-code-coverage-results-in-the-mr"></a>

### 在合并请求中查看代码覆盖率结果

如果您在代码中使用测试覆盖率，则可以使用正则表达式在作业日志中查找覆盖率结果。然后，您可以将这些结果包含在极狐GitLab 的合并请求中。

如果流水线成功，覆盖将显示在合并请求部件和作业表中。如果流水线中的多个作业都有覆盖率报告，则对它们进行平均。

![MR widget coverage](img/pipelines_test_coverage_mr_widget.png)

![Build status coverage](img/pipelines_test_coverage_build.png)

#### 使用 `coverage` 关键字添加测试覆盖率结果

要使用项目的 `.gitlab-ci.yml` 文件将测试覆盖率结果添加到合并请求，请使用 [`coverage`](../yaml/index.md#coverage) 关键字提供正则表达式。

#### 测试覆盖率示例

将此正则表达式用于常用的测试工具。

<!-- vale gitlab.Spelling = NO -->

- Simplecov (Ruby)。示例：`/\(\d+.\d+\%\) covered/`。
- pytest-cov (Python)。示例：`/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/`。
- Scoverage (Scala)。示例：`/Statement coverage[A-Za-z\.*]\s*:\s*([^%]+)/`。
- `pest --coverage --colors=never` (PHP)。示例：`/^\s*Cov:\s*\d+\.\d+?%$/`。
- `phpunit --coverage-text --colors=never` (PHP)。示例：`/^\s*Lines:\s*\d+.\d+\%/`。
- gcovr (C/C++)。示例：`/^TOTAL.*\s+(\d+\%)$/`。
- `tap --coverage-report=text-summary` (NodeJS)。示例：`/^Statements\s*:\s*([^%]+)/`。
- `nyc npm test` (NodeJS)。示例：`/All files[^|]*\|[^|]*\s+([\d\.]+)/`。
- `jest --ci --coverage` (NodeJS)。示例：`/All files[^|]*\|[^|]*\s+([\d\.]+)/`。
- excoveralls (Elixir)。示例：`/\[TOTAL\]\s+(\d+\.\d+)%/`。
- `mix test --cover` (Elixir)。示例：`/\d+.\d+\%\s+\|\s+Total/`。
- JaCoCo (Java/Kotlin)。示例：`/Total.*?([0-9]{1,3})%/`。
- `go test -cover` (Go)。示例：`/coverage: \d+.\d+% of statements/`。
- .NET (OpenCover)。示例：`/(Visited Points).*\((.*)\)/`。
- .NET (`dotnet test` line coverage)。示例：`/Total\s*\|\s*(\d+(?:\.\d+)?)/`。
- tarpaulin (Rust)。示例：`/^\d+.\d+% coverage/`。
- Pester (PowerShell)。示例：`/Covered (\d+\.\d+%)/`。

<!-- vale gitlab.Spelling = YES -->

### 查看项目代码覆盖率的历史记录

> - 下载 `.csv` 功能引入于 12.10 版本。
> - 图标引入于 13.1 版本。

要查看项目代码覆盖率随时间的演变，
您可以查看图表或下载包含此数据的 CSV 文件。

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **分析 > 仓库**。

每个作业的历史数据列在图表上方的下拉列表中。

要查看数据的 CSV 文件，请选择 **下载原始数据 (`.csv`)**。

![Code coverage graph of a project over time](img/code_coverage_graph_v13_1.png)

### 查看群组代码覆盖率的历史记录 **(PREMIUM)**

要查看一段时间内某个群组下所有项目的代码覆盖率，您可以找到[群组仓库分析](../../user/group/repositories_analytics/index.md)视图。

![Code coverage graph of a group over time](img/code_coverage_group_report.png)

### 流水线徽章

您可以使用[流水线徽章](../../user/project/badges.md#test-coverage-report-badges)来指示项目的流水线状态和测试覆盖率。这些徽章由最新成功的流水线决定。

<a id="coverage-check-approval-rule"></a>

## 覆盖率检查批准规则 **(PREMIUM)**

> - 引入于 14.0 版本。
> - 在项目设置中可配置于 14.1 版本。

合并会导致项目测试覆盖率下降的合并请求时，您可以规定此类合并请求需要特定用户或群组的批准。

按照以下步骤启用 `Coverage-Check` 合并请求批准规则：

1. 为要包含在整体覆盖率值中的所有作业设置 [`coverage`](../yaml/index.md#coverage) 正则表达式。
1. 转到您的项目并选择 **设置 > 合并请求**。
1. 在 **合并请求批准** 下，选择 `Coverage-Check` 批准规则旁边的 **启用**。
1. 选择 **目标分支**。
1. 将 **需要批准** 的数量设置为大于零。
1. 选择要提供批准的用户或群组。
1. 选择 **添加批准规则**。

![Coverage-Check approval rule](img/coverage_check_approval_rule_14_1.png)

## 故障排除

### 使用代码覆盖率时删除代码颜色

某些测试覆盖工具输出的 ANSI 颜色代码未被正则表达式正确解析。这会导致覆盖解析失败。

一些覆盖工具不提供在输出中禁用颜色代码的选项。如果是这样，请通过去除颜色代码的单行脚本来传输覆盖工具的输出。

例如：

```shell
lein cloverage | perl -pe 's/\e\[?.*?[\@-~]//g'
```
