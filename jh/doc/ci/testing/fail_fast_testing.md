---
stage: Verify
group: Pipeline Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 快速测试失败 **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/198550) in GitLab 13.1.
-->

对于使用 RSpec 运行测试的应用程序，我们引入了 `Verify/Failfast` [运行测试套件子集的模板](https://jihulab.com/gitlab-cn/gitlab/-/tree/master/lib/gitlab/ci/templates/Verify/FailFast.gitlab-ci.yml)，基于您合并请求中的更改。

该模板使用 `test_file_finder` (`tff`) gem<!--[test_file_finder (`tff`) gem](https://gitlab.com/gitlab-org/ci-cd/test_file_finder/)--> 接受文件列表作为输入，并返回它认为与输入文件相关的规范（测试）文件列表。

`tff` 是为 Ruby on Rails 项目设计的，因此 `Verify/FailFast` 模板配置为在检测到对 Ruby 文件的更改时运行。默认情况下，它在极狐GitLab CI/CD 流水线的 [`.pre` 阶段](../yaml/index.md#stage-pre)中运行，在所有其它阶段之前。

## 示例用例

在向项目添加新功能和添加新的自动化测试时，快速测试失败很有用。

您的项目可能有数十万个测试需要很长时间才能完成。
您可能确信新测试会通过，但您必须等待所有测试完成才能验证它。即使使用并行化，这也可能需要一个小时或更长时间。

快速测试失败为您提供了来自流水线的更快反馈循环。它让您快速知道新测试正在通过并且新功能没有破坏其它测试。

## 要求

此模板需要：

- 在 Rails 中构建的项目，使用 RSpec 进行测试。
- CI/CD 配置为：
  - 使用带有 Ruby 的 Docker 镜像。
  - 使用[合并请求流水线](../pipelines/merge_request_pipelines.md#prerequisites)。
- 在项目设置中启用[合并结果流水线](../pipelines/merged_results_pipelines.md#enable-merged-results-pipelines)。
- 带有 Ruby 的 Docker 镜像可用。该模板默认使用 `image: ruby:2.6`，但您[可以覆盖](../yaml/includes.md#override-included-configuration-values)。

## 配置快速 RSpec 失败

我们使用以下简单的 RSpec 配置作为起点。它仅在合并请求流水线上安装所有项目 gem 并执行 `rspec`。
```yaml
rspec-complete:
  stage: test
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  script:
    - bundle install
    - bundle exec rspec
```

要首先运行最相关的 specs 而不是整个套件，请通过将以下内容添加到 CI/CD 配置中来 [`include`](../yaml/index.md#include) 模板：

```yaml
include:
  - template: Verify/FailFast.gitlab-ci.yml
```

要自定义作业，可以设置特定选项来覆盖模板。例如，要覆盖默认的 Docker 镜像：

```yaml
include:
  - template: Verify/FailFast.gitlab-ci.yml

rspec-rails-modified-path-specs:
  image: custom-docker-image-with-ruby
```

### 示例测试负载

出于说明目的，假设我们的 Rails 应用程序规范套件包含 10 个模型，每个模型有 100 个 specs。

如果没有更改任何 Ruby 文件：

- `rspec-rails-modified-paths-specs` 不运行任何测试。
- `rspec-complete` 运行全套 1000 次测试。

如果更改了一个 Ruby 模型，例如 `app/models/example.rb`，则 `rspec-rails-modified-paths-specs` 会为 `example.rb` 运行 100 次测试：

- 如果这 100 次测试全部通过，则允许运行包含 1000 次测试的完整 `rspec-complete` 套件。
- 如果这 100 次测试中的任何一个失败，它们会很快失败，并且 `rspec-complete` 不会运行任何测试。

最后一个案例节省了资源和时间，因为没有运行完整的 1000 个测试套件。
