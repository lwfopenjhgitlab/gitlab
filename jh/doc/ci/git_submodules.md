---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 在 GitLab CI/CD 中使用 Git 子模块 **(FREE)**

使用 [Git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) 将 Git 仓库作为另一个 Git 仓库的子目录。您可以将另一个仓库克隆到您的项目中，并将您的提交分开。

## 配置 `.gitmodules` 文件

当使用 Git 子模块时，您的项目应该有一个名为 `.gitmodules` 的文件。
您有多个选项可以将其配置为在极狐GitLab CI/CD 作业中工作。

### 使用绝对 URL

> 引入于极狐GitLab Runner 15.11 版本。

例如，您生成的 `.gitmodules` 配置可能如下所示：

- 您的项目位于 `https://jihulab.com/secret-group/my-project`。
- 您的项目依赖于 `https://jihulab.com/group/project`，您希望将其作为子模块包含在内。
- 您可以使用 SSH 地址查看源代码，例如 `git@gitlab.com:secret-group/my-project.git`。

```ini
[submodule "project"]
  path = project
  url = git@gitlab.com:secret-group/project.git
```

在这种情况下，请使用 [`GIT_SUBMODULE_FORCE_HTTPS`](runners/configure_runners.md#rewrite-submodule-urls-to-https) 变量指示极狐GitLab Runner 在克隆子模块之前将 URL 转换为 HTTPS。

或者，如果您也在本地使用 HTTPS，则可以配置 HTTPS URL：

```ini
[submodule "project"]
  path = project
  url = https://gitlab.com/secret-group/project.git
```

在这种情况下，您不需要配置其他变量，但需要使用[个人访问令牌](../user/profile/personal_access_tokens.md)在本地克隆它。

### 使用相对 URL

WARNING:
如果您使用相对 URL，子模块可能会在分叉工作流程中无法正确解析。
如果您希望您的项目有分支，请使用绝对 URL。

当您的子模块位于同一极狐GitLab 服务器上时，您还可以在 `.gitmodules` 文件中使用相对 URL：

```ini
[submodule "project"]
  path = project
  url = ../../project.git
```

上面的配置指示 Git 在克隆源时自动推导要使用的 URL。您可以在所有 CI/CD 作业中使用 HTTPS 进行克隆，并且您
可以继续使用 SSH 进行本地克隆。

对于不在同一极狐GitLab 服务器上的子模块，确保总是使用完整 URL：

```ini
[submodule "project-x"]
  path = project-x
  url = https://gitserver.com/group/project-x.git
```

## 在 CI/CD 作业中使用 Git 子模块

要使子模块在 CI/CD 作业中正常工作：

1. 您可以将 `GIT_SUBMODULE_STRATEGY` 变量设置为 `normal` 或 `recursive`，告诉 runner 在作业之前获取您的子模块<!--[在作业之前获取您的子模块](runners/configure_runners.md#git-submodule-strategy)-->：

   ```yaml
   variables:
     GIT_SUBMODULE_STRATEGY: recursive
   ```

1. 对于位于同一极狐GitLab 服务器上并配置了 Git 或 SSH URL 的子模块，请确保设置 [`GIT_SUBMODULE_FORCE_HTTPS`](runners/configure_runners.md#rewrite-submodule-urls-to-https) 变量。

1. 您可以过滤或排除特定子模块，控制将使用 `GIT_SUBMODULE_PATHS` 同步哪些子模块。

   ```yaml
   variables:
     GIT_SUBMODULE_PATHS: submoduleA submoduleB
   ```

1. 您可以使用 `GIT_SUBMODULE_UPDATE_FLAGS` 提供额外的标志来控制高级检出。

   ```yaml
   variables:
     GIT_SUBMODULE_STRATEGY: recursive
     GIT_SUBMODULE_UPDATE_FLAGS: --jobs 4
   ```
   
如果您使用 [`CI_JOB_TOKEN`](jobs/ci_job_token.md) 克隆流水线作业中的子模块，则必须为执行作业的用户，分配有权在上游子模块项目中触发流水线的角色。

## 故障排查

### 无法找到 `.gitmodules` 文件

`.gitmodules` 文件可能很难找到，因为它通常是一个隐藏文件。
您可以查看特定操作系统的文档以了解如何查找和显示隐藏文件。

如果没有 `.gitmodules` 文件，则子模块设置可能位于 [`git config`](https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-config) 文件中。
