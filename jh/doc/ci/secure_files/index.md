---
stage: Verify
group: Pipeline Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

# 项目级安全文件 **(FREE)**

> - 引入于 14.8 版本。[功能标志](../../administration/feature_flags.md)为 `ci_secure_files`，默认禁用。
> - 功能标志删除于 15.7 版本。

项目级安全文件是一项实验性功能，仍在开发中。

<!--
Project-level Secure Files is an experimental feature developed by [GitLab Incubation Engineering](https://about.gitlab.com/handbook/engineering/incubation/).
The feature is still in development, but you can:

- [Request a feature](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/feedback/-/issues/new?issuable_template=feature_request).
- [Report a bug](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/feedback/-/issues/new?issuable_template=report_bug).
- [Share feedback](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/feedback/-/issues/new?issuable_template=general_feedback).
-->

您可以安全地存储多达 100 个文件，将它们作为安全文件在 CI/CD 流水线中使用。这些文件安全地存储在项目仓库之外，并且不受版本控制。
在这些文件中存储敏感信息是安全的。安全文件支持纯文本和二进制文件类型，但必须为 5 MB 或更小。

您可以在项目设置中或使用 [API](../../api/secure_files.md) 管理安全文件。

CI/CD 作业可以使用 [download-secure-files](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files) 工具，[下载和使用安全文件](#use-secure-files-in-cicd-jobs)。

## 将安全文件添加到项目

将安全文件添加到项目：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > CI/CD**。
1. 在 **安全文件** 部分，选择 **展开**。
1. 选择 **上传文件**。
1. 找到要上传的文件，选择 **打开**，文件立即开始上传。上传完成后，文件会显示在列表中。

<a id="use-secure-files-in-cicd-jobs"></a>

## 在 CI/CD 作业中使用安全文件

要在 CI/CD 作业中使用您的安全文件，您必须使用 [`download-secure-files`](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files) 工具下载作业中的文件。下载后，您可以将它们与其他脚本命令一起使用。

在作业的 `script` 部分添加命令，下载 `download-secure-files` 工具并执行它。文件下载到项目根目录中的 `.secure_files` 目录中。
要更改安全文件的下载位置，请在 `SECURE_FILES_DOWNLOAD_PATH` [CI/CD 变量](../variables/index.md) 中设置路径。

```yaml
test:
  variables:
    SECURE_FILES_DOWNLOAD_PATH: './where/files/should/go/'
  script:
    - curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer" | bash
```
