---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
description: "An overview of Continuous Integration, Continuous Delivery, and Continuous Deployment, as well as an introduction to GitLab CI/CD."
type: concepts
---

# CI/CD 概念 **(FREE)**

通过软件开发的持续方法，您可以持续构建、测试和部署迭代代码更改。这种迭代过程有助于减少您基于有缺陷或失败的先前版本开发新代码的机会。
使用这种方法，您可以努力减少从开发新代码到部署的人工干预，甚至根本不需要干预。

持续方法的三种主要方式是：

- [持续集成](#持续集成)
- [持续交付](#持续交付)
- [持续部署](#持续部署)

NOTE:
开箱即用的管理系统可以将维护工具链的时间减少 10% 或更多。
<!--Watch our ["Mastering continuous software development"](https://about.gitlab.com/webcast/mastering-ci-cd/)
webcast to learn about continuous methods and how built-in GitLab CI/CD can help you simplify and scale software development.-->

<!--
> - <i class="fa fa-youtube-play youtube" aria-hidden="true"></i>&nbsp;Learn how to [configure CI/CD](https://www.youtube.com/embed/opdLqwz6tcE).
> - [Make the case for CI/CD in your organization](https://about.gitlab.com/devops-tools/github-vs-gitlab/).
> - <i class="fa fa-youtube-play youtube" aria-hidden="true"></i>&nbsp;Learn how [Verizon reduced rebuilds](https://about.gitlab.com/blog/2019/02/14/verizon-customer-story/)
>   from 30 days to under 8 hours with GitLab.
-->

## 持续集成

考虑一个应用程序，它的代码存储在极狐GitLab 的 Git 存储库中。开发人员每天多次推送代码更改。对于每次推送到仓库，您可以创建一组脚本来自动构建和测试您的应用程序。这些脚本有助于减少您在应用程序中引入错误的机会。

这种做法称为持续集成。提交给应用程序的每个更改，甚至是开发分支，都会自动且连续地构建和测试。这些测试可确保更改通过您为应用程序建立的所有测试、指南和代码合规性标准。

[极狐GitLab 本身](https://jihulab.com/gitlab-cn/gitlab) 是一个使用持续集成作为软件开发方法的项目示例。对于项目的每次推送，都会针对代码运行一组检查。

## 持续交付

[持续交付](https://continuousdelivery.com/) 是超越持续集成的一步。每次将代码更改推送到代码库时，不仅会构建和测试您的应用程序，还会持续部署应用程序。但是，对于持续交付，您需要手动触发部署。

持续交付会自动检查代码，但需要人工干预以手动和战略性地触发更改的部署。

## 持续部署

[持续部署](https://www.airpair.com/continuous-deployment/posts/continuous-deployment-for-practical-people)是超越持续集成的又一步，类似于持续交付。不同之处在于，不是手动部署应用程序，而是将其设置为自动部署。不需要人工干预。

## GitLab CI/CD

[GitLab CI/CD](../quick_start/index.md) 是极狐GitLab 的一部分，用于所有持续方法（持续集成、交付和部署）。使用 GitLab CI/CD，您可以测试、构建和发布您的软件，而无需第三方应用程序或集成。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see [Introduction to GitLab CI/CD](https://www.youtube.com/watch?v=l5705U8s_nQ&t=397) from an April 2020 GitLab meetup.
-->

### GitLab CI/CD 工作流

GitLab CI/CD 适合通用的开发工作流程。

您可以首先讨论议题中的代码实现，然后在本地处理您提议的更改。然后，您可以将提交推送到托管在极狐GitLab 中的远端仓库中的功能分支。
推送会触发项目的 CI/CD 流水线。然后，GitLab CI/CD：

- 运行自动化脚本（顺序或并行）：
   - 构建和测试您的应用程序。
   - 在 Review App 中预览更改，就像您在 `localhost` 上看到的一样。

实施后按预期工作：

- 审核并批准您的代码。
- 将功能分支合并到默认分支中。
   - GitLab CI/CD 将您的更改自动部署到生产环境。

如果出现问题，您可以回滚您的更改。 

![GitLab workflow example](img/gitlab_workflow_example_11_9.png)

此工作流显示了极狐GitLab 流程中的主要步骤。
您不需要任何外部工具来交付您的软件，并且您可以在 GitLab UI 中可视化所有步骤。

### 深入了解 CI/CD 工作流程

如果您深入了解工作流程，您可以看到 GitLab 在 DevOps 生命周期的每个阶段可用的功能。

![Deeper look into the basic CI/CD workflow](img/gitlab_workflow_example_extended_v12_3.png)

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
[Get a deeper look at GitLab CI/CD](https://youtu.be/l5705U8s_nQ?t=369).
-->
