---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# GitLab CI/CD 入门 **(FREE)**

跟随此文档进行 [GitLab CI/CD](../index.md) 入门。

在开始之前，请确保您拥有：

- 极狐GitLab 中您想使用 CI/CD 的项目。
- 项目的维护者或所有者角色。

<!--
如果您从其它 CI/CD 工具迁移，请查看此文档：

- [从 CircleCI 迁移](../migration/circleci.md)。
- [从 Jenkins 迁移](../migration/jenkins.md)。
-->

<!--
> - <i class="fa fa-youtube-play youtube" aria-hidden="true"></i>&nbsp;Watch [First time GitLab & CI/CD](https://www.youtube.com/watch?v=kTNfi5z6Uvk&t=553s). This includes a quick introduction to GitLab, the first steps with CI/CD, building a Go project, running tests, using the CI/CD pipeline editor, detecting secrets and security vulnerabilities and offers more exercises for async practice.
> - <i class="fa fa-youtube-play youtube" aria-hidden="true"></i>&nbsp;Watch [Intro to GitLab CI](https://www.youtube.com/watch?v=l5705U8s_nQ&t=358s). This workshop uses the Web IDE to quickly get going with building source code using CI/CD, and run unit tests.
-->

## CI/CD 流程概览

要使用 GitLab CI/CD：

1. [确保您有可用的 runner](#确保您有可用的-runner) 运行您的作业。
   如果您没有 runner，需要为您的示例、项目或群组安装 GitLab Runner<!--[安装 GitLab Runner](https://docs.gitlab.com/runner/install/)-->并注册 runner<!--[注册 runner](https://docs.gitlab.com/runner/register/)-->。SaaS 版提供 runner，如果您使用 SaaS 版，跳过此步骤。
1. [在仓库的根目录下创建一个 `.gitlab-ci.yml` 文件](#创建-gitlab-ciyml-文件)。该文件是您定义 CI/CD 作业的地方。

当您将文件提交到仓库时，runner 会运行您的作业。
作业结果[显示在流水线中](#查看流水线和作业的状态)。

### 确保您有可用的 runner

在极狐GitLab 中，runners 是运行 CI/CD 作业的代理。

您可能已经有可用于您的项目的 runner，包括共享 runner<!--[共享运行程序](../runners/runners_scope.md)-->，它们可用于您的实例中的所有项目。

要查看可用的 runner：

- 转到 **设置 > CI/CD** 并展开 **Runners**。

只要您至少有一个有效的 runner，旁边有一个绿色圆圈，您就有一个 runner 可以处理您的作业。

如果 UI 中的 **Runners** 页面上没有列出任何 runner，您或管理员必须安装 GitLab Runner<!--[安装 GitLab Runner](https://docs.gitlab.com/runner/install/)--> 和<!--[注册](https://docs.gitlab.com/runner/register/)-->注册至少一名 runner。

如果您正在测试 CI/CD，您可以在本地机器上安装 GitLab Runner 并注册 runner。
当您的 CI/CD 作业运行时，它们会在您的本地机器上运行。

### 创建 `.gitlab-ci.yml` 文件

`.gitlab-ci.yml` 文件是一个 YAML 文件，您可以在其中配置 GitLab CI/CD 的特定指令。

在此文件中，您定义：

- runner 应执行的作业的结构和顺序。
- runner 在遇到特定条件时应做出的决定。

例如，您可能希望在提交到除默认分支之外的任何分支时运行一组测试。当您提交到默认分支时，您希望运行相同的套件，但还要发布您的应用程序。

所有这些都在 `.gitlab-ci.yml` 文件中定义。

创建一个 `.gitlab-ci.yml` 文件：

1. 在左侧边栏上，选择 **项目信息 > 详细信息**。
1. 在文件列表上方，选择要提交的分支，点击加号图标，然后选择 **新建文件**：

   ![New file](img/new_file_v13_6.png)

1. 对于 **文件名称**，输入 `.gitlab-ci.yml` 并在较大的窗口中粘贴以下示例代码：

   ```yaml
   build-job:
     stage: build
     script:
       - echo "Hello, $GITLAB_USER_LOGIN!"

   test-job1:
     stage: test
     script:
       - echo "This job tests something"

   test-job2:
     stage: test
     script:
       - echo "This job tests something, but takes more time than test-job1."
       - echo "After the echo commands complete, it runs the sleep command for 20 seconds"
       - echo "which simulates a test that runs 20 seconds longer than test-job1"
       - sleep 20

   deploy-prod:
     stage: deploy
     script:
       - echo "This job deploys something from the $CI_COMMIT_BRANCH branch."
     environment: production
   ```

   `$GITLAB_USER_LOGIN` 和 `$CI_COMMIT_BRANCH` 是在作业运行时填充的预定义变量<!--[预定义变量](../variables/predefined_variables.md)-->。

1. 点击 **提交变更**。

提交后，流水线开始。

#### `.gitlab-ci.yml` tips

- 创建第一个 `.gitlab-ci.yml` 文件后，使用[流水线编辑器](../pipeline_editor/index.md) 对文件进行所有未来的编辑。使用流水线编辑器，您可以：
   - 使用自动语法突出显示和验证来编辑流水线配置。
   - 查看 [CI/CD 配置可视化](../pipeline_editor/index.md#可视化-ci-配置)，是您的 `.gitlab-ci.yml` 文件的图形化表示。

- 如果您希望 runner 使用 Docker 容器运行作业<!--[使用 Docker 容器运行作业](../docker/using_docker_images.md)-->，请编辑 `.gitlab-ci.yml` 文件以包含镜像名称：

  ```yaml
  default:
    image: ruby:2.7.5
  ```

  此命令告诉 runner 使用来自 Docker Hub 的 Ruby 镜像并在从该镜像生成的容器中运行作业。

  此过程不同于将应用程序构建为 Docker 容器<!--[将应用程序构建为 Docker 容器](../docker/using_docker_build.md)-->。您的应用程序无需构建为 Docker 容器即可在 Docker 容器中运行 CI/CD 作业。

- 每个作业都包含脚本和阶段：
  - <!--[`default`](../yaml/index.md#自定义默认关键字值)-->`default` 关键字用于自定义默认值，例如 [`before_script`](../yaml/index.md#before_script) 和 [`after_script`](../yaml/index.md#after_script)。
  - [`stage`](../yaml/index.md#stage) 描述了作业的顺序执行。
    只要有可用的 runner，单个阶段中的作业就会并行运行。
  - 使用 Directed Acyclic Graphs (DAG)<!--[Directed Acyclic Graphs (DAG)](../directed_acyclic_graph/index.md)--> 关键字以乱序运行作业。
- 您可以设置其他配置来自定义您的作业和阶段的执行方式：
  - 使用 [`rules`](../yaml/index.md#rules) 关键字指定何时运行或跳过作业。
    `only` 和 `except` 旧关键字仍受支持，但不能在同一作业中与 `rules` 一起使用。
  - 使用 [`cache`](../yaml/index.md#cache) 和 [`artifacts`](../yaml/index.md#artifacts) 在流水线中保持跨作业和阶段的信息持久化。这些关键字是存储依赖项和作业输出的方法，即使在为每个作业使用临时 runner 时也是如此。
- 有关完整的 `.gitlab-ci.yml` 语法，请参阅[完整的 `.gitlab-ci.yml` 参考主题](../yaml/index.md)。

### 查看流水线和作业的状态

当您提交更改时，流水线启动。

要查看您的流水线：

- 转到 **CI/CD > 流水线**。

   应显示具有三个阶段的流水线：

  ![Three stages](img/three_stages_v13_6.png)

- 要查看流水线的可视化表示，请单击流水线 ID。

  ![Pipeline graph](img/pipeline_graph_v13_6.png)

- 要查看作业的详细信息，请单击作业名称，例如 `deploy-prod`。

  ![Job details](img/job_details_v13_6.png)

如果作业状态为 `stuck`，请检查以确保为项目正确配置了 runner。
