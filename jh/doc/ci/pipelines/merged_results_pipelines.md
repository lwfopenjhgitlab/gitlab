---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
last_update: 2019-07-03
---

# 合并结果流水线 **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/7380) in GitLab 11.10.
-->

> - 从 15.1 版本开始，合并结果流水线也会在 [Draft 合并请求](../../user/project/merge_requests/drafts.md)上运行。

*合并结果流水线*是[合并请求流水线](merge_request_pipelines.md)的一种。它是针对合并在一起的源分支和目标分支的结果运行的流水线。

极狐GitLab 使用合并的结果创建一个内部提交，因此流水线可以针对它运行。两个分支中都不存在此提交，但您可以在流水线详细信息中查看它。内部提交的作者始终是创建合并请求的用户。

流水线针对目标分支运行，因为它在您运行流水线时存在。
随着时间的推移，当您在源分支中工作时，目标分支可能会发生变化。
任何时候您想确保合并的结果是准确的，您都应该重新运行流水线。

当目标分支的更改与源分支中的更改冲突时，合并结果流水线无法运行。

在这些情况下，流水线作为[合并请求的流水线](merge_request_pipelines.md)运行，并标记为 `merge request`。

## 先决条件

为合并结果启用流水线：

- 您必须具有维护者角色。
- 您必须使用 GitLab Runner 11.9 或更高版本。
- 您不能使用[快进式合并](../../user/project/merge_requests/methods/index.md)。
- 您的仓库必须是极狐GitLab 仓库，而不是外部仓库<!--[外部仓库](../ci_cd_for_external_repos/index.md)-->。

<a id="enable-merged-results-pipelines"></a>

## 为合并结果启用流水线

要为项目的合并结果启用流水线：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 合并请求**。
1. 在 **合并选项** 部分，选择 **启用合并结果流水线**。
1. 单击 **保存更改**。

WARNING:
如果您选中该复选框但未将 CI/CD 配置为将流水线用于合并请求，您的合并请求可能会卡在未解决状态或您的流水线可能会被丢弃。

## 使用合并队列

当您启用[合并结果的流水线](#合并结果流水线) 时，系统[自动显示](merge_trains.md#向合并队列添加合并请求) **Start/Add Merge Train** 按钮。

通常，这是比立即合并合并请求更安全的选择，因为在实际合并发生之前，您的合并请求会使用预期的合并后结果进行评估。

有关更多信息，请阅读[合并队列文档](merge_trains.md)。

## 自动流水线取消

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/12996) in GitLab 12.3.
-->

GitLab CI/CD 可以检测冗余流水线的存在，并取消它们以节省 CI 资源。

当用户在正在进行的合并队列中立即合并合并请求时，队列将被重建，因为它重新创建了预期的合并后提交和流水线。在这种情况下，合并队列可能已经有针对先前预期的合并后提交运行的流水线。
这些流水线被认为是多余的，会自动取消。

## 故障排查

### 即使将新更改推送到合并请求，也未创建合并结果的流水线

可能是由一些禁用的功能标志引起的。请确保在您的实例上启用了以下功能标志：

- `:merge_ref_auto_sync`

要检查和设置这些功能标志值，请让管理员执行以下操作：

1. 登录实例的 Rails 控制台：

   ```shell
   sudo gitlab-rails console
   ```

1. 检查标志是否启用：

   ```ruby
   Feature.enabled?(:merge_ref_auto_sync)
   ```

1. 如果需要，启用功能标志：

   ```ruby
   Feature.enable(:merge_ref_auto_sync)
   ```

<!--
### 间歇性流水线因 `fatal: reference is not a tree:` 错误失败

由于合并结果的流水线是在合并请求 (`refs/merge-requests/<iid>/merge`) 的合并引用上运行的，因此 Git 引用可能会在意外的时间被覆盖。例如，当源或目标分支被推进时。在这种情况下，流水线由于 `fatal: reference is not a tree:` 错误而失败，这表明在合并引用中找不到 checkout-SHA。

This behavior was improved at GitLab 12.4 by introducing [Persistent pipeline refs](../troubleshooting.md#fatal-reference-is-not-a-tree-error).
You should be able to create pipelines at any timings without concerning the error.
-->

### 成功的合并结果流水线覆盖失败的分支流水线

当启用[**流水线必须成功**设置](../../user/project/merge_requests/merge_when_pipeline_succeeds.md#require-a-successful-pipeline-for-merge)时，系统有时会忽略失败的分支流水线。
