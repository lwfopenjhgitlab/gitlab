---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
disqus_identifier: 'https://docs.gitlab.com/ee/user/project/pipelines/settings.html'
type: reference, howto
---

# 自定义流水线配置 **(FREE)**

您可以自定义流水线为您的项目运行的方式。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview of pipelines, watch the video [GitLab CI Pipeline, Artifacts, and Environments](https://www.youtube.com/watch?v=PCKDICEe10s).
Watch also [GitLab CI pipeline tutorial for beginners](https://www.youtube.com/watch?v=Jav4vbUrqII).
-->

## 更改哪些用户可以查看您的流水线

对于公共和内部项目，您可以更改谁可以看到您的：

- 流水线
- 作业输出日志
- 作业产物
- 流水线安全仪表盘<!--[流水线安全仪表板](../../user/application_security/security_dashboard/index.md#view-vulnerabilities-in-a-pipeline)-->

要更改流水线和相关功能的可见性：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 选中或清除 **公开流水线** 复选框。
   当它被选中时，流水线和相关功能是可见的：

   - 对于 **公开** 项目，所有人。
   - 对于 **内部** 项目，除外部用户之外的所有登录用户。
   - 对于 **私有** 项目，所有项目成员（访客或更高级别）。

   清除时：

   - 对于 **公开** 项目，作业日志、作业产物、流水线安全仪表盘和 **CI/CD** 菜单项仅对项目成员（报告者或更高级别）可见。包括来宾用户在内的其他用户只能查看流水线和作业的状态，并且只能在查看合并请求或提交时查看。
   - 对于 **内部** 项目，除外部用户外，所有登录用户都可以看到流水线。
     相关功能仅对项目成员（报告者或更高级别）可见。
   - 对于 **私有** 项目，流水线和相关功能仅对项目成员（报告者或更高级别）可见。

### 更改公开项目中非项目成员的流水线可见性

您可以在[公开项目](../../user/public_access.md)中，控制非项目成员的流水线的可见性。

此设置在以下情况下无效：

- 项目可见性设置为[**内部**或**私有**](../../user/public_access.md)，因为非项目成员无法访问内部或私有项目。
- [**公开流水线**](#change-which-users-can-view-your-pipelines)设置已禁用。

要更改非项目成员的流水线可见性：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **可见性、项目功能、权限**。
1. 对于 **CI/CD**，选择：
   - **仅项目成员**：只有项目成员可以查看流水线。
   - **具有访问权限的任何人**：非项目成员也可以查看流水线。
1. 选择 **保存更改**。

<!--
The [CI/CD permissions table](../../user/permissions.md#gitlab-cicd-permissions)
lists the pipeline features non-project members can access when **Everyone With Access**
is selected.
-->

<a id="auto-cancel-redundant-pipelines"></a>

## 自动取消冗余流水线

您可以将挂起或正在运行的流水线，设置为在同一分支上运行新流水线时自动取消。

WARNING:
通过提交（commit）触发的同一分支流水线可以自动冗余取消，但是通过手动触发的同一分支流水线不会进行自动冗余取消。

您可以在项目设置中启用此功能：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 选中 **自动取消多余的流水线** 复选框。
1. 选择 **保存修改**。

使用 [`interruptible`](../yaml/index.md#interruptible) 关键字来指示是否可以在完成之前取消正在运行的作业。

<a id="prevent-outdated-deployment-jobs"></a>

## 阻止过期的部署作业

> 引入于 12.9 版本

您的项目可能有多个并发部署作业，这些作业计划在同一时间范围内运行。

这可能导致较旧的部署作业在较新的部署作业之后运行的情况，这可能不是您想要的。

为避免这种情况：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 选中 **阻止已过时的部署作业** 复选框。
1. 选择 **保存修改**。

有关详细信息，请参阅[部署安全](../environments/deployment_safety.md#prevent-outdated-deployment-jobs)。

<a id="specify-a-custom-cicd-configuration-file"></a>

## 指定自定义 CI/CD 配置文件

> 支持外部`.gitlab-ci.yml` 位置引入于 12.6 版本

系统期望在项目的根目录中找到 CI/CD 配置文件（`.gitlab-ci.yml`）。但是，您可以指定备用文件名路径，包括项目外的位置。

自定义路径：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 在 **CI/CD 配置文件** 字段中，输入文件名。如果文件：
    - 不在根目录中，包含路径。
    - 在不同的项目中，包括群组和项目名称。
    - 在外部站点上，输入完整的 URL。
1. 选择 **保存修改**。

### 自定义 CI/CD 配置文件示例

如果 CI/CD 配置文件不在根目录中，则路径必须是相对于它的。
例如：

- `my/path/.gitlab-ci.yml`
- `my/path/.my-custom-file.yml`

如果 CI/CD 配置文件位于外部站点上，则 URL 必须以 `.yml` 结尾：

- `http://example.com/generate/ci/config.yml`

如果 CI/CD 配置文件在不同的项目中：

- 文件必须存在于其默认分支上，或者将分支指定为 refname。
- 路径必须相对于其他项目中的根目录。
- 路径后面必须跟一个 `@` 符号以及完整的群组和项目路径。

例如：

- `.gitlab-ci.yml@namespace/another-project`
- `my/path/.my-custom-file.yml@namespace/sub-group/another-project`
- `my/path/.my-custom-file.yml@namespace/sub-group1/sub-group2/another-project:refname`

如果配置文件在单独的项目中，可以设置更细粒度的权限。例如：

- 创建一个公共项目来托管配置文件。
- 仅向允许编辑文件的用户授予对项目的写入权限。

这样其他用户和项目就可以访问该配置文件而不能对其进行编辑。

## 选择默认的 Git 策略

您可以选择在作业运行时如何从极狐GitLab 获取您的仓库。

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 在 **Git 策略** 下，选择一个选项：
    - `git clone` 速度较慢，因为它为每个作业从头开始克隆仓库。但是，本地工作副本始终是 origin 的。
    - `git fetch` 更快，因为它重新使用本地工作副本（如果它不存在，则回退到克隆）。这是推荐方法，特别是对于大型仓库<!--[大型仓库](../large_repositories/index.md#git-strategy)-->。

配置的 Git 策略可以被 `.gitlab-ci.yml` 文件中的 `GIT_STRATEGY` 变量<!--[`GIT_STRATEGY` 变量](../runners/configure_runners.md#git-strategy)-->覆盖。

<a id="limit-the-number-of-changes-fetched-during-clone"></a>

## 限制克隆期间获取的更改数量

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/28919) in GitLab 12.0.
-->

您可以限制 GitLab CI/CD 在克隆仓库时获取的更改数量。

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 在 **Git 策略** 下，在 **Git 浅克隆** 下，输入一个值。
    最大值为`1000`。要禁用浅克隆并使 GitLab CI/CD 每次都获取所有分支和标签，请将值保留为空或设置为 `0`。

在 14.7 及更高版本中，新创建的项目的默认 `git depth` 值为 `20`。14.6 及更早版本的默认 `git depth` 值为 `50`。

这个值可以被 `.gitlab-ci.yml` 文件中的 <!--[`GIT_DEPTH` 变量](../large_repositories/index.md#shallow-cloning)-->`GIT_DEPTH` 变量覆盖。

<a id="set-a-limit-for-how-long-jobs-can-run"></a>

## 设置作业可以运行的时间限制

您可以定义作业在超时之前可以运行多长时间。

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 在 **超时** 字段中，输入分钟数或人类可读的值，如 `2 hours`。必须是 10 分钟或更多，并且少于 1 个月。 默认值为 60 分钟。

超过超时的作业被标记为失败。

您可以为单个 runner<!--[为单个 runner](../runners/configure_runners.md#set-maximum-job-timeout-for-a-runner)--> 覆盖此值。

## 流水线徽章

流水线徽章指示项目的流水线状态和测试覆盖率值。这些徽章由最新的成功流水线决定。

<a id="latest-release-badge"></a>

## 最新发布徽章

> 引入于 14.8 版本

最新发布徽章表示您的项目的最新发布标签名称。
默认情况下，徽章获取使用 `released_at` 时间排序的版本。

### 查看流水线状态和覆盖率报告徽章的代码

您可以查看徽章的确切链接。然后您可以在 HTML 或 Markdown 页面中嵌入徽章。

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 在 **流水线状态** 或 **覆盖率报告** 部分，查看图像的 URL。

![Pipelines badges](img/pipelines_settings_badges.png)

<a id="pipeline-status-badge"></a>

### 流水线状态徽章

根据流水线的状态，徽章可以具有以下值：

- `pending`
- `running`
- `passed`
- `failed`
- `skipped`
- `canceled`
- `unknown`

您可以使用以下链接访问流水线状态徽章图像：

```plaintext
https://gitlab.example.com/<namespace>/<project>/badges/<branch>/pipeline.svg
```

#### 仅显示非跳过状态

要使流水线状态徽章仅显示最后一个未跳过的状态，请使用 `?ignore_skipped=true` 查询参数：

```plaintext
https://gitlab.example.com/<namespace>/<project>/badges/<branch>/pipeline.svg?ignore_skipped=true
```

<a id="test-coverage-report-badge"></a>

### 测试覆盖率报告徽章

您可以为每个作业日志匹配的[覆盖率报告](#merge-request-test-coverage-results)定义正则表达式。这意味着流水线中的每个作业都可以定义测试覆盖率百分比值。

要访问测试覆盖率徽章，请使用以下链接：

```plaintext
https://gitlab.example.com/<namespace>/<project>/badges/<branch>/coverage.svg
```

要从特定作业获取覆盖率报告，请将 `job=coverage_job_name` 参数添加到 URL。例如，您可以使用类似于以下的代码，将 `coverage` 作业的测试覆盖率报告徽章添加到 Markdown 文件中：

```markdown
![coverage](https://gitlab.example.com/<namespace>/<project>/badges/<branch>/coverage.svg?job=coverage)
```

#### 测试覆盖率报告徽章颜色和限制

徽章的默认颜色和限制如下：

- 95 到 100% - 好 (`#4c1`)
- 90 到 95% - 可接受 (`#a3c51c`)
- 75 到 90% - 中等 (`#dfb317`)
- 0 到 75% - 低 (`#e05d44`)
- 没有覆盖 - 未知（`#9f9f9f`）

<!--
NOTE:
*Up to* means up to, but not including, the upper bound.
-->

您可以使用以下附加参数覆盖限制（引入于 14.4 版本）：

- `min_good` (默认 95，可以使用 3 到 100 之间的任何值)
- `min_acceptable` (默认 90，可以使用 2 和 min_good-1 之间的任何值)
- `min_medium` (默认 75，可以使用 1 到 min_acceptable-1 之间的任何值)

如果设置了无效边界，系统会自动将其调整为有效。例如，`min_good` 设置为`80`，并且 `min_acceptable` 设置为 `85`（太高），系统会自动将 `min_acceptable` 设置为 `79`（`min_good` - `1`）。

### 最新发布徽章

当您的项目中存在发布时，它会显示最新的发布标签名称。如果没有发布，则显示 `none`。

您可以使用以下链接访问最新发布的徽章图像：

```plaintext
https://gitlab.example.com/<namespace>/<project>/-/badges/release.svg
```

#### 排序首选项

默认情况下，最新发布徽章使用 `release_at` 时间获取发布。查询参数 `?order_by=release_at` 的使用可选：

```plaintext
https://gitlab.example.com/<namespace>/<project>/-/badges/release.svg?order_by=release_at
```

### 徽章样式

通过向 URL 添加 `style=style_name` 参数，可以不同的样式呈现流水线徽章。有两种款式可供选择：

- 平面（默认）：

  ```plaintext
  https://gitlab.example.com/<namespace>/<project>/badges/<branch>/coverage.svg?style=flat
  ```

  ![Badge flat style](https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=coverage&style=flat)

- 平方<!--([Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/30120) in GitLab 11.8)-->：

  ```plaintext
  https://gitlab.example.com/<namespace>/<project>/badges/<branch>/coverage.svg?style=flat-square
  ```

  ![Badge flat square style](https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=coverage&style=flat-square)

### 自定义徽章文本

> 引入于 13.1 版本

可以自定义徽章的文本，区分在同一流水线中运行的多个覆盖作业。通过向 URL 添加 `key_text=custom_text` 和 `key_width=custom_key_width` 参数来自定义徽章文本和宽度：

```plaintext
https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=karma&key_text=Frontend+Coverage&key_width=130
```

![Badge with custom text and width](https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=karma&key_text=Frontend+Coverage&key_width=130)

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
