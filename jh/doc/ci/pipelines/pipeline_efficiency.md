---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 流水线效率 **(FREE)**

[CI/CD 流水线](index.md) 是 [GitLab CI/CD](../index.md) 的基本构建模块。
使流水线更高效可帮助您节省开发人员的时间，从而：

- 加快您的 DevOps 流程
- 降低成本
- 缩短开发反馈循环

新团队或项目以缓慢且低效的流水线开始，并随着时间的推移通过反复试验来改进其配置，这是很常见的。更好的流程是使用流水线功能，立即提高效率，并更早地获得更快的软件开发生命周期。

首先确保您熟悉 [GitLab CI/CD 基础](../introduction/index.md) 并理解[快速入门指南](../quick_start/index.md)。

## 识别瓶颈和常见故障

检查低效流水线的最简单指标是作业、阶段的运行时间和流水线本身的总运行时间。流水线总持续时间受到以下因素的严重影响：

- 仓库的大小<!--[仓库的大小](../large_repositories/index.md)-->
- 阶段和作业的总数。
- 作业之间的依赖关系。
- [“关键路径”](#有向无环图-dag-可视化)，代表最小和最大流水线持续时间。

需要注意的其他点与 GitLab Runners<!--[GitLab Runners](../runners/index.md)--> 相关：

- Runner 的可用性以及为他们提供的资源。
- 构建依赖项及其安装时间。
- [容器镜像大小](#docker-镜像)。
- 网络延迟和连接缓慢。

流水线频繁发生不必要的故障也会导致开发生命周期的放缓。您应该寻找作业失败的有问题的模式：

- 片状单元测试随机失败，或产生不可靠的测试结果。
- 与该行为相关的测试覆盖率下降和代码质量。
- 可以安全地忽略但会停止流水线的故障。
- 在长流水线结束时失败的测试，但可能在较早的阶段，导致延迟反馈。

## 流水线分析

分析流水线的性能以找到提高效率的方法。分析可以帮助识别 CI/CD 基础架构中可能的阻碍因素。这包括分析：

- 工作量。
- 执行时间的瓶颈。
- 整体流水线架构。

理解和记录流水线工作流并讨论可能的操作和更改非常重要。重构流水线可能需要 DevSecOps 生命周期中的团队之间进行仔细的交互。

流水线分析可以帮助识别成本效率问题。例如，托管有付费云服务的 runners<!--[runners](../runners/index.md)--> 可以导致：

- 比 CI/CD 流水线所需的资源多，浪费金钱。
- 没有足够的资源，导致运行缓慢和浪费时间。

### 流水线洞察

<!--
The [Pipeline success and duration charts](index.md#pipeline-success-and-duration-charts)
give information about pipeline runtime and failed job counts.
-->

<!--[单元测试](../unit_test_reports.md)-->单元测试、集成测试、端到端测试、<!--[代码质量](../../user/project/merge_requests/code_quality.md)-->代码质量测试等测试确保 CI/CD 流水线自动发现问题，可能涉及许多流水线阶段，导致运行时间过长。

您可以通过在同一阶段并行运行测试不同事项的作业来改进运行时间，从而减少整体运行时间。缺点是您需要同时运行更多 runner 来支持并行作业。

<!--
[GitLab 的测试级别](../../development/testing_guide/testing_levels.md) 提供了一个包含许多组件的复杂测试策略的示例。
-->

### 有向无环图 (DAG) 可视化

<!--[有向无环图](../directed_acyclic_graph/index.md)-->有向无环图 (DAG) 可视化可以帮助分析流水线中的关键路径并了解可能的阻塞。

![CI Pipeline Critical Path with DAG](img/ci_efficiency_pipeline_dag_critical_path.png)

<!--
### 流水线监控

全局流水线运行状况是监控作业和流水线持续时间的关键指标。[CI/CD 分析](index.md#pipeline-success-and-duration-charts) 给出了管道健康状况的可视化表示。

实例管理员可以访问额外的[性能指标和自我监控](../../administration/monitoring/index.md)。

您可以从 [API](../../api/index.md) 获取特定的管道运行状况指标。
外部监控工具可以轮询 API 并验证管道运行状况或收集用于长期 SLA 分析的指标。

例如，Prometheus 的 [GitLab CI Pipelines Exporter](https://github.com/mvisonneau/gitlab-ci-pipelines-exporter) 从 API 和管道事件中获取指标。它可以自动检查项目中的分支并获取管道状态和持续时间。结合 Grafana 仪表板，这有助于为您的运营团队构建可操作的视图。指标图也可以嵌入到事件中，使问题解决更容易。此外，它还可以导出有关作业和环境的指标。

如果您使用 GitLab CI 管道导出器，您应该从 [示例配置](https://github.com/mvisonneau/gitlab-ci-pipelines-exporter/blob/main/docs/configuration_syntax.md) 开始。

![Grafana Dashboard for GitLab CI Pipelines Prometheus Exporter](img/ci_efficiency_pipeline_health_grafana_dashboard.png)

Alternatively, you can use a monitoring tool that can execute scripts, like
[`check_gitlab`](https://gitlab.com/6uellerBpanda/check_gitlab) for example.

#### Runner 监控

You can also [monitor CI runners](https://docs.gitlab.com/runner/monitoring/) on
their host systems, or in clusters like Kubernetes. This includes checking:

- Disk and disk IO
- CPU usage
- Memory
- Runner process resources

The [Prometheus Node Exporter](https://prometheus.io/docs/guides/node-exporter/)
can monitor runners on Linux hosts, and [`kube-state-metrics`](https://github.com/kubernetes/kube-state-metrics)
runs in a Kubernetes cluster.

You can also test [GitLab Runner auto-scaling](https://docs.gitlab.com/runner/configuration/autoscale.html)
with cloud providers, and define offline times to reduce costs.

#### 仪表板和事件管理

Use your existing monitoring tools and dashboards to integrate CI/CD pipeline monitoring,
or build them from scratch. Ensure that the runtime data is actionable and useful
in teams, and operations/SREs are able to identify problems early enough.
[Incident management](../../operations/incident_management/index.md) can help here too,
with embedded metric charts and all valuable details to analyze the problem.

### 存储使用

Review the storage use of the following to help analyze costs and efficiency:

- [Job artifacts](job_artifacts.md) and their [`expire_in`](../yaml/index.md#artifactsexpire_in)
  configuration. If kept for too long, storage usage grows and could slow pipelines down.
- [Container registry](../../user/packages/container_registry/index.md) usage.
- [Package registry](../../user/packages/package_registry/index.md) usage.
-->

## 流水线配置

配置流水线时请谨慎选择，以加快流水线速度并减少资源使用。这包括利用 GitLab CI/CD 的内置功能，使流水线运行得更快、更高效。

### 减少作业运行的频率

尝试找出哪些作业不需要在所有情况下都运行，并使用流水线配置来阻止它们运行：

- 当旧流水线被新流水线取代时，使用 [`interruptible`](../yaml/index.md#interruptible) 关键字停止旧流水线。
- 使用 [`rules`](../yaml/index.md#rules) 跳过不需要的测试。例如，当仅更改前端代码时跳过后端测试。
- 不那么频繁地运行非必要的[计划流水线](schedules.md)。

### 快速失败

确保在 CI/CD 流水线中尽早检测到错误。需要很长时间才能完成的作业可以防止流水线返回失败状态，直到作业完成。

设计流水线，以便可以快速失败<!--[快速失败](../../user/project/merge_requests/fail_fast_testing.md)-->的作业更早运行。例如，添加一个早期阶段并在其中做移动语法、样式检查、Git 提交消息验证和类似的工作。

确定在较快作业的快速反馈之前尽早运行长作业是否重要。最初的失败可能表明流水线的其余部分不应运行，从而节省了流水线资源。

### 有向无环图 (DAG)

在基本配置中，作业总是在运行之前等待早期阶段的所有其他作业完成。这是最简单的配置，但在大多数情况下也是最慢的。[有向无环图](../directed_acyclic_graph/index.md)和[父/子流水线](downstream_pipelines.md#parent-child-pipelines)更灵活，效率更高，但也会使流水线更难理解和分析。

### 缓存

另一种优化方法是 [cache](../caching/index.md) 依赖项。如果您的依赖项很少更改，例如 NodeJS `/node_modules`<!--[NodeJS `/node_modules`](../caching/index.md#cache-nodejs-dependencies)-->，缓存可以使流水线执行速度更快。

即使作业失败，您也可以使用 [`cache:when`](../yaml/index.md#cachewhen) 缓存下载的依赖项。

### Docker 镜像

下载和初始化 Docker 镜像可能是整个作业运行时间的很大一部分。

如果 Docker 镜像减慢作业执行速度，请分析基本镜像大小和与注册表的网络连接。如果示例在云中运行，请查找供应商提供的云容器镜像库。除此之外，您可以使用 [GitLab 容器镜像库](../../user/packages/container_registry/index.md)，相比其它镜像库，GitLab 实例可以更快地访问。

#### 优化 Docker 镜像

构建优化的 Docker 镜像，因为大型 Docker 镜像会占用大量空间，下载时间较长，连接速度较慢。如果可能，请避免对所有作业使用一个打镜像。使用多个较小的镜像，每个镜像用于特定任务，下载和运行速度更快。

尝试使用预装软件的自定义 Docker 镜像。下载更大的预配置镜像通常比每次使用通用镜像并在其上安装软件要快得多。Docker 的[编写 Dockerfiles 的最佳实践](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/) 有更多关于构建高效 Docker 镜像的信息。

减小 Docker 镜像大小的方法：

- 使用小型基础镜像，例如 `debian-slim`。
- 如果不是严格需要，不要安装像 vim、curl 等便利工具。
- 打造专属的开发镜像。
- 禁用由软件包安装的手册页和文档以节省空间。
- 减少 `RUN` 层并结合软件安装步骤。
- 使用 [multi-stage builds](https://blog.alexellis.io/mutli-stage-docker-builds/) 将多个使用构建器模式的 Dockerfile 合并为一个 Dockerfile，可以减少镜像大小。
- 如果使用 `apt`，添加 `--no-install-recommends` 以避免不必要的包。
- 清理最后不再需要的缓存和文件。例如 `rm -rf /var/lib/apt/lists/*` 适用于 Debian 和 Ubuntu，或 `yum clean all` 适用于RHEL和CentOS。
- 使用 [dive](https://github.com/wagoodman/dive) 或 [DockerSlim](https://github.com/docker-slim/docker-slim) 等工具来分析和缩小镜像。

为了简化 Docker 镜像管理，您可以创建一个专门的组来管理 [Docker 镜像](../docker/index.md) 并使用 CI/CD 流水线测试、构建和发布它们。

## 测试、记录和学习

改进流水线是一个迭代过程。做一些小的改变，监控效果，然后再次迭代。许多小的改进加起来可以大大提高流水线效率。

您可以直接在 GitLab 仓库中使用 [Mermaid charts in Markdown](../../user/markdown.md#mermaid) 执行此操作。它可以帮助记录流水线设计和架构。 

记录 CI/CD 流水线问题和问题中的事件，包括所做的研究和找到的解决方案。这有助于新团队成员的入职，还有助于识别 CI 流水线效率方面的反复出现的问题。

<!--
### Learn More

- [CI Monitoring Webcast Slides](https://docs.google.com/presentation/d/1ONwIIzRB7GWX-WOSziIIv8fz1ngqv77HO1yVfRooOHM/edit?usp=sharing)
- [GitLab.com Monitoring Handbook](https://about.gitlab.com/handbook/engineering/monitoring/)
- [Buildings dashboards for operational visibility](https://aws.amazon.com/builders-library/building-dashboards-for-operational-visibility/)
-->