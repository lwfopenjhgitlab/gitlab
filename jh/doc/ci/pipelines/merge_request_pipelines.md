---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---


# 合并请求的流水线 **(FREE)**

您可以将[流水线](index.md)配置为在每次将更改提交到分支时运行。
这种类型的流水线称为*分支流水线*。

或者，您可以将流水线配置为在每次更改合并请求的源分支时运行。这种类型的流水线称为*合并请求流水线*。

分支流水线：

- 当您将新提交推送到分支时运行。
- 是默认类型的流水线。
- 可以访问[一些预定义的变量](../variables/predefined_variables.md)。
- 可以访问[受保护的变量](../variables/index.md#protect-a-cicd-variable) 和[受保护的 runners](../runners/configure_runners.md#prevent-runners-from-revealing-sensitive-information)。

合并请求流水线：

- **默认情况下不运行**。CI/CD 配置文件中的作业必须配置为在合并请求流水线中运行。
- 如果已配置，合并请求流水线会在您执行以下操作时运行：
  - 从具有一个或多个提交的源分支创建新的合并请求。
  - 将新提交推送到源分支以获取合并请求。
  - 从合并请求的 **流水线** 选项卡中选择 **运行流水线**。仅当配置合并请求流水线并且源分支至少有一个提交时，此选项才可用。
- 可以访问[更多预定义变量](#available-predefined-variables)。
- 无权访问[受保护的变量](../variables/index.md#protect-a-cicd-variable) 或[受保护的 runners](../runners/configure_runners.md#prevent-runners-from-revealing-sensitive-information)。

这两种类型的流水线都可以出现在合并请求的 **流水线** 选项卡上。

## 合并请求的流水线类型

合并请求的三种流水线类型是：

- 合并请求流水线，它在合并请求的源分支中的更改上运行。在  14.9 版本中引入，这些流水线显示一个 `合并请求` 标签，表示流水线仅在源分支的内容上运行，而忽略目标分支。在 14.8 及更早版本中，标签是 `已游离`。
- [合并结果流水线](pipelines_for_merged_results.md)，它运行在源分支的更改与目标分支的更改组合的结果上。
- [合并队列](merge_trains.md)，在同时合并多个合并请求时运行。来自每个合并请求的更改与之前排队的合并请求中的更改合并到目标分支中，以确保它们一起生效。

<a id="prerequisites"></a>

## 先决条件

使用合并请求流水线：

- 您的项目的 [CI/CD 配置文件](../yaml/index.md)，必须配置为在合并请求流水线中运行作业。为此，您可以使用：
  - [`rules`](#使用-rules-添加作业)。
  - [`only/except`](#使用-only-添加作业)。
- 您必须至少在源项目中拥有开发人员角色才能运行合并请求的流水线。
- 您的仓库必须是 GitLab 仓库，而不是外部仓库<!--[外部仓库](../ci_cd_for_external_repos/index.md)-->。

## 使用 `rules` 添加作业

您可以使用 [`rules`](../yaml/index.md#rules) 关键字将作业配置为在合并请求的流水线中运行。例如：

```yaml
job1:
  script:
    - echo "This job runs in merge request pipelines"
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
```

您还可以使用 [`workflow: rules`](../yaml/index.md#workflowrules) 关键字将整个流水线配置为在合并请求流水线中运行。例如：

```yaml
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

job1:
  script:
    - echo "This job runs in merge request pipelines"

job2:
  script:
    - echo "This job also runs in merge request pipelines"
```

## 使用 `only` 添加作业

您可以使用带有 `merge_requests` 的 [`only`](../yaml/index.md#onlyrefs--exceptrefs) 关键字将作业配置为在合并请求的流水线中运行。

```yaml
job1:
  script:
    - echo "This job runs in merge request pipelines"
  only:
    - merge_requests
```

## 与派生项目一起使用

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/217451) in GitLab 13.3.
> - [Moved](https://about.gitlab.com/blog/2021/01/26/new-gitlab-product-subscription-model/) to GitLab Premium in 13.9.
-->

在派生项目中工作的外部贡献者不能在父项目中创建流水线。

来自提交给父项目的分支的合并请求会触发一个流水线：

- 在派生（源）项目中创建并运行，而不是在父（目标）项目中。
- 使用派生项目的 CI/CD 配置、资源和项目 CI/CD 变量。

用于派生项目的流水线在父项目中显示为带有 **派生** 标志：

![Pipeline ran in fork](img/pipeline_fork_v13_7.png)

<a id="run-pipelines-in-the-parent-project"></a>

### 在父项目中运行流水线 **(PREMIUM)**

父项目中的项目成员可以从派生项目提交的合并请求触发运行合并请求流水线。此流水线：

- 在父（目标）项目中创建并运行，而不是在派生（源）项目中。
- 使用派生项目分支中的 CI/CD 配置。
- 使用父项目的 CI/CD 配置、资源和项目 CI/CD 变量。
- 使用触发流水线的父项目成员的权限。

在派生项目 MR 中运行流水线，确保合并后的流水线在父项目中通过。此外，如果您不信任派生项目的 runner，则在父项目中运行流水线会使用父项目的受信任 runner。

WARNING:
派生合并请求可能包含恶意代码，这些代码会在流水线运行时（甚至在合并之前）尝试窃取父项目中的机密。作为审核者，请在触发流水线之前仔细检查合并请求中的更改。除非您通过 API 或 [`/rebase` 快速操作](../../user/project/quick_actions.md#issues-merge-requests-and-epics)触发流水线，极狐GitLab 会显示一条您必须在流水线运行之前接受的警告。否则，**不会显示警告**。

先决条件：

- 父项目的 [CI/CD 配置文件](../yaml/index.md)必须配置为[在合并请求流水线中运行作业](#prerequisites)。
- 您必须是具有[运行 CI/CD 流水线的权限](../../user/permissions.md#gitlab-cicd-permissions)的父项目的成员。如果分支受到保护，您可能需要额外的权限。
- 派生项目必须对运行流水线的用户[可见](../../user/public_access.md)。否则，合并请求中不会显示**流水线**选项卡。

要在父项目中，为来自派生项目的合并请求运行流水线：

1. 在合并请求中，转到 **流水线** 选项卡。
1. 选择 **运行流水线**。您必须阅读并接受警告，否则流水线不会运行。

您可以通过使用[项目 API](../../api/projects.md#edit-project) 禁用 `ci_allow_fork_pipelines_to_run_in_parent_project` 设置，来禁用此功能。该设置默认为 `enabled`。

## 可用的预定义变量

当您使用合并请求流水线时，您可以使用：

- 分支流水线中可用的所有相同[预定义变量](../variables/predefined_variables.md)。
- <!--[其他预定义变量](../variables/predefined_variables.md#predefined-variables-for-merge-request-pipelines)-->其它预定义变量仅适用于合并请求流水线中的作业。这些变量包含来自相关合并请求的信息<!--，可以在从作业调用 [GitLab 合并请求 API 端点](../../api/merge_requests.md) 时-->。

## 故障排查

### 推送到分支时的两条流水线

如果您在合并请求中获得重复的流水线，您的流水线可能配置为同时为分支和合并请求运行。调整您的流水线配置以[避免重复流水线](../jobs/job_control.md#防止重复流水线)。

在 13.7 及更高版本中，您可以将 `workflow:rules` 添加到从分支流水线切换到合并请求流水线<!--[从分支流水线切换到合并请求流水线](../yaml/workflow.md#switch-between-branch-pipelines-and-merge-request-pipelines)-->。
在分支上打开合并请求后，流水线将切换到合并请求流水线。

### 推送无效 CI/CD 配置文件时的两个流水线

如果将无效的 CI/CD 配置推送到合并请求的分支，则流水线选项卡中会出现两个失败的流水线。一个流水线是失败的分支流水线，另一个是失败的合并请求流水线。

修复配置语法后，不应出现更多失败的流水线。
要查找并修复配置问题，您可以使用：

- [流水线编辑器](../pipeline_editor/index.md)。
- [CI lint 工具](../lint.md)。

### 合并请求流水线被标记为失败，但最新的流水线成功

可以在单个合并请求的 **流水线** 选项卡中同时拥有分支流水线和合并请求流水线。可能是按配置，或偶然发生。

当使用[流水线成功时合并](../../user/project/merge_requests/merge_when_pipeline_succeeds.md)功能并且两种流水线类型都存在时，检查合并请求流水线，而不是分支流水线。

因此，如果**合并请求流水线**失败，合并请求流水线结果将被标记为不成功，与**分支流水线**结果无关。

然而：

- 这种情况不强制执行。
- 系统自动确定哪个流水线的结果用来阻止或通过合并请求。

<!--
### `An error occurred while trying to run a new pipeline for this merge request.`

This error can happen when you select **Run pipeline** in a merge request, but the
project does not have merge request pipelines enabled anymore.

Some possible reasons for this error message:

- The project does not have merge request pipelines enabled, has no pipelines listed
  in the **Pipelines** tab, and you select **Run pipelines**.
- The project used to have merge request pipelines enabled, but the configuration
  was removed. For example:

  1. The project has merge request pipelines enabled in the `.gitlab-ci.yml` configuration
     file when the merge request is created.
  1. The **Run pipeline** options is available in the merge request's **Pipelines** tab,
     and selecting **Run pipeline** at this point likely does not cause any errors.
  1. The project's `.gitlab-ci.yml` file is changed to remove the merge request pipelines configuration.
  1. The branch is rebased to bring the updated configuration into the merge request.
  1. Now the pipeline configuration no longer supports merge request pipelines,
     but you select **Run pipeline** to run a merge request pipeline.

If **Run pipeline** is available, but the project does not have merge request pipelines
enabled, do not use this option. You can push a commit or rebase the branch to trigger
new branch pipelines.
-->
