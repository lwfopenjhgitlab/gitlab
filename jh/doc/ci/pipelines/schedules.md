---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
disqus_identifier: 'https://docs.gitlab.com/ee/user/project/pipelines/schedules.html'
type: reference, howto
---

# 计划流水线 **(FREE)**

使用计划流水线定期运行 GitLab CI/CD [流水线](index.md)。

## 先决条件

要运行计划流水线：

- 计划所有者必须具有开发人员角色。对于受保护分支上的流水线，必须允许计划所有者合并到分支。
- [CI/CD 配置](../yaml/index.md) 必须有效。

否则，不会创建流水线。不显示错误消息。

<a id="add-a-pipeline-schedule"></a>

## 添加计划流水线

> 于 14.9 版本引入标签的计划流水线。

添加流水线计划：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **CI/CD > 计划**。
1. 选择 **新建计划** 并填写表格。
   - **循环周期**：选择预配置的间隔之一，或以 cron 表示法输入自定义间隔。您可以使用任何 cron 值，但计划流水线的运行频率不能超过实例的最大计划流水线频率。
   - **目标分支或标签**：选择流水线的分支或标签。
   - **变量**：将任意数量的 [CI/CD 变量](../variables/index.md) 添加到计划中。这些变量仅在计划流水线运行时可用，而不是在任何其它流水线运行时可用。

如果项目已经有[最大数量的流水线计划](../../administration/instance_limits.md#number-of-pipeline-schedules)，您必须先删除未使用的计划，然后才能添加另一个。

## 编辑流水线计划

流水线计划的所有者可以对其进行编辑：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **CI/CD > 计划**。
1. 在计划旁边，选择 **编辑** (**{pencil}**) 并填写表格。

用户必须具有项目的开发者或以上角色。如果用户不是计划的所有者，他们必须首先[取得所有权](#take-ownership)。

## 手动运行

手动触发流水线计划，使其立即运行而不是在下一个计划时间：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **CI/CD > 计划**。
1. 在列表右侧，对于您要运行的流水线，选择 **运行** (**{play}**)。

您可以每分钟手动运行一次计划流水线。

<a id="take-ownership"></a>

## 取得所有权

计划流水线以拥有计划的用户的权限执行。流水线可以访问与流水线所有者相同的资源，包括[受保护的环境](../environments/protected_environments.md)和 [CI/CD 作业令牌](../jobs/ci_job_token.md)。

要获得由其他用户创建的流水线的所有权：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **CI/CD > 计划**。
1. 在列表右侧，对于您要成为所有者的流水线，选择 **取得所有权**。

<!--
## Related topics

- Pipeline schedules can be maintained by using the [Pipeline schedules API](../../api/pipeline_schedules.md).
- You can [control which jobs are added to scheduled pipelines](../jobs/job_control.md#run-jobs-for-scheduled-pipelines).
-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
