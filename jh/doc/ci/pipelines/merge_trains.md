---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
last_update: 2019-07-03
---

# 合并队列 **(PREMIUM)**

FLAG:
在 15.11 及更高版本，**开启合并队列** 按钮变更为 **设置为自动合并**，**添加到合并队列** 按钮改为 **合并**。在私有化部署版上，这些变更不可用。要使其可用，
需要管理员[启用功能标志](../../administration/feature_flags.md) `auto_merge_labels_mr_widget`，在 SaaS 版上，此功能不可用。

使用合并队列对合并请求进行排队，并在将它们合并到目标分支之前验证它们的更改是否可以协同工作。

在频繁合并到默认分支的项目中，不同合并请求的更改可能会相互冲突。[合并结果流水线](merged_results_pipelines.md)确保更改适用于默认分支中的内容，但不适用于其他人同时合并的内容。

合并队列不适用于[半线性历史合并请求](../../user/project/merge_requests/methods/index.md#merge-commit-with-semi-linear-history)或[快进式合并请求](../../user/project/merge_requests/methods/index.md#fast-forward-merge)。

<!--
有关以下内容的更多信息：

- 合并队列如何工作，查看[合并队列工作流程](#merge-train-workflow)。
- Why you might want to use merge trains, read [How starting merge trains improve efficiency for DevOps](https://about.gitlab.com/blog/2020/01/30/all-aboard-merge-trains/).
-->

<a id="merge-train-workflow"></a>

## 合并队列工作流

当没有合并请求等待合并，并且您选择[**开始合并队列**](#start-a-merge-train)时，合并队列开始。极狐GitLab 启动合并队列流水线，验证更改是否可以合并到默认分支中。第一个流水线与[合并结果流水线](merged_results_pipelines.md)相同，在源分支和目标分支组合在一起的变化上运行。内部合并结果提交的作者是发起合并的用户。

要在第一个流水线完成后，立即合并第二个合并请求，请选择[**添加到合并队列**](#add-a-merge-request-to-a-merge-train)，并将其添加到队列。
第二个合并队列流水线运行在合并请求与目标分支相结合的变化上。类似地，如果您添加第三个合并请求，该流水线将在与目标分支合并的所有三个合并请求的更改上运行。流水线全部并行运行。

每个合并请求仅在以下情况下合并到目标分支：

- 合并请求的流水线成功完成。
- 在合并之前排队的所有其他合并请求。

如果合并队列流水线失败，则不会合并合并请求。极狐GitLab 从合并队列中删除该合并请求，并为在它之后排队的所有合并请求启动新的流水线。

例如：

三个合并请求（`A`、`B` 和 `C`）按顺序添加到合并队列中，将创建三个并行运行的合并结果流水线：

1. 第一个流水线运行在 `A` 与目标分支相结合的变化上。
1. 第二个流水线运行在来自 `A` 和 `B` 的变化以及目标分支上。
1. 第三个流水线运行在 `A`、`B` 和 `C` 与目标分支相结合的变化上。

如果 `B` 的流水线失败：

- 第一个流水线（`A`）继续运行。
- `B` 从队列中移除。
- `C` 的流水线[被取消](#automatic-pipeline-cancellation)，一个新的流水线开始，用于从 `A` 和 `C` 与目标分支相结合的变化（没有 `B` 变化）。

如果 `A` 成功完成，它会合并到目标分支，而 `C` 继续运行。添加到队列中的任何新合并请求包括目标分支中的 `A` 更改，以及合并队列中的 `C` 更改。

<a id="automatic-pipeline-cancellation"></a>

### 自动取消流水线

极狐GitLab CI/CD 检测冗余流水线，并取消它们以节省资源。

冗余合并队列流水线发生在：

- 流水线因合并队列中的合并请求之一而失败。
- 您[跳过合并队列并立即合并](#skip-the-merge-train-and-merge-immediately)。
- 您[从合并队列中删除合并请求](#remove-a-merge-request-from-a-merge-train)。

在这些情况下，极狐GitLab 必须为队列上的部分或全部合并请求创建新的合并队列流水线。旧流水线正在与合并队列中先前的组合更改进行比较，这些更改不再有效，因此这些旧流水线被取消。

<a id="enable-merge-trains"></a>

## 启用合并队列

先决条件：

- 您必须具有维护者角色。
- 您的代码库必须是极狐GitLab 代码库，而不是[外部代码库](../ci_cd_for_external_repos/index.md)。
- 您的流水线必须[配置为使用合并请求流水线](merge_request_pipelines.md#prerequisites)。否则，您的合并请求可能会陷入未解决状态，或者您的流水线可能会被丢弃。

要启用合并队列：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 合并请求**。
1. 在 **合并方法** 部分，验证是否选择了 **合并提交**。
1. 在 **合并选项** 部分：
   - 在 13.6 及更高版本，选择 **启用合并结果流水线** 和 **启用合并队列**。
   - 在 13.5 及更早版本，选择 **为合并结果启用合并队列和流水线**。<!--此外，必须正确设置[功能标志](#disable-merge-trains-in-gitlab-135-and-earlier)。-->
1. 选择 **保存更改**。

## 启动合并队列

先决条件：

- 您必须具有[权限](../../user/permissions.md)，才能合并或推送到目标分支。

要启动合并队列：

1. 访问合并请求。
1. 选择：
    - 当没有流水线运行时，**启动合并队列**。
    - 当有流水线运行时，**流水线成功时启动合并队列**。

合并请求的合并队列状态显示在流水线部件下方，并显示类似于 `A new merge train has started and this merge request is the first of the queue.` 的消息。

现在可以将其他合并请求添加到队列中。

<a id="add-a-merge-request-to-a-merge-train"></a>

## 将合并请求添加到合并队列

先决条件：

- 您必须具有[权限](../../user/permissions.md)，才能合并或推送到目标分支。

将合并请求添加到合并队列：

1. 访问合并请求。
1. 选择：
    - 当没有流水线运行时，**启动合并队列**。
    - 当有流水线运行时，**流水线成功时启动合并队列**。

合并请求的合并队列状态显示在流水线部件下方，并带有类似 `Added to the merge train. There are 2 merge requests waiting to be merged.` 的消息。

每个合并队列最多可以并行运行 20 个流水线。 如果您将超过 20 个合并请求添加到合并队列，额外的合并请求将排队等待流水线完成。等待加入合并队列的排队合并请求的数量没有限制。

## 从合并队列中删除合并请求

要从合并队列中删除合并请求，请选择 **从合并队列中删除**。
您可以稍后再次将合并请求添加到合并队列。

当您从合并队列中删除合并请求时：

- 合并请求的所有流水线在删除的合并请求重新启动后排队。
- 冗余流水线[被取消](#automatic-pipeline-cancellation)。

## 跳过合并队列并立即合并

如果您有高优先级合并请求，例如必须紧急合并的关键补丁，请选择 **立即合并**。

当您立即合并合并请求时：

- 重新创建当前合并队列。
- 所有流水线重新启动。
- 冗余流水线[被取消](#automatic-pipeline-cancellation)。

WARNING:
立即合并会使用大量的 CI/CD 资源。仅在紧急情况下使用此选项。

<!--
## Disable merge trains in GitLab 13.5 and earlier **(PREMIUM SELF)**

In [GitLab 13.6 and later](https://gitlab.com/gitlab-org/gitlab/-/issues/244831),
you can [enable or disable merge trains in the project settings](#enable-merge-trains).

In GitLab 13.5 and earlier, merge trains are automatically enabled when
[merged results pipelines](merged_results_pipelines.md) are enabled.
To use merged results pipelines but not merge trains, enable the `disable_merge_trains`
[feature flag](../../user/feature_flags.md).

[GitLab administrators with access to the GitLab Rails console](../../administration/feature_flags.md)
can enable the feature flag to disable merge trains:

```ruby
Feature.enable(:disable_merge_trains)
```

After you enable this feature flag, GitLab cancels existing merge trains and removes
the **Start/Add to merge train** option from merge requests.

To disable the feature flag, which enables merge trains again:

```ruby
Feature.disable(:disable_merge_trains)
```

## Troubleshooting

### Merge request dropped from the merge train

If a merge request becomes unmergeable while a merge train pipeline is running,
the merge train drops your merge request automatically. For example, this could be caused by:

- Changing the merge request to a [draft](../../user/project/merge_requests/drafts.md).
- A merge conflict.
- A new conversation thread that is unresolved, when [all threads must be resolved](../../user/discussions/index.md#prevent-merge-unless-all-threads-are-resolved)
  is enabled.

You can find reason the merge request was dropped from the merge train in the system
notes. Check the **Activity** section in the **Overview** tab for a message similar to:
`User removed this merge request from the merge train because ...`

### Cannot use merge when pipeline succeeds

You cannot use [merge when pipeline succeeds](../../user/project/merge_requests/merge_when_pipeline_succeeds.md)
when merge trains are enabled. See [the related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/12267)
for more information.

### Cannot retry merge train pipeline cannot

When a merge train pipeline fails, the merge request is dropped from the train and the pipeline can't be retried.
Merge train pipelines run on the merged result of the changes in the merge request and
changes from other merge requests already on the train. If the merge request is dropped from the train,
the merged result is out of date and the pipeline can't be retried.

You can:

- [Add the merge request to the train](#add-a-merge-request-to-a-merge-train) again,
  which triggers a new pipeline.
- Add the [`retry`](../yaml/index.md#retry) keyword to the job if it fails intermittently.
  If it succeeds after a retry, the merge request is not removed from the merge train.

### Unable to add to the merge train

When [**Pipelines must succeed**](../../user/project/merge_requests/merge_when_pipeline_succeeds.md#require-a-successful-pipeline-for-merge)
is enabled, but the latest pipeline failed:

- The **Start/Add to merge train** option is not available.
- The merge request displays `The pipeline for this merge request failed. Please retry the job or push a new commit to fix the failure.`

Before you can re-add a merge request to a merge train, you can try to:

- Retry the failed job. If it passes, and no other jobs failed, the pipeline is marked as successful.
- Rerun the whole pipeline. On the **Pipelines** tab, select **Run pipeline**.
- Push a new commit that fixes the issue, which also triggers a new pipeline.

See [the related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/35135)
for more information.
-->

