---
stage: Verify
group: Testing
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# 流水线产物 **(FREE)**

流水线产物是由极狐GitLab 在流水线完成后创建的文件。它们与[作业产物](../jobs/job_artifacts.md)不同，因为它们不是由 `.gitlab-ci.yml` 定义明确管理的。

<!--[测试覆盖可视化功能](../../user/project/merge_requests/test_coverage_visualization.md)-->测试覆盖可视化功能使用流水线产物来收集覆盖信息。

<a id="storage"></a>

## 存储

流水线产物被保存到磁盘或对象存储。它们计入项目的[存储使用配额](../../user/usage_quotas.md#存储使用配额)。存储使用配额页面上的 **产物** 是所有作业产物和流水线产物的总和。

## 何时流水线产物被删除

流水线产物被删除：

- 创建后 7 天。
- 在另一个流水线成功运行之后，如果它们来自最近成功的流水线。

此删除操作最多可能需要两天时间。
