---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# CI/CD 分钟数配额 **(PREMIUM)**

NOTE:
术语 **CI/CD 分钟数** 正在重命名为 **计算单位**。在此过渡期间，您可能会在 UI 和文档中看到 **CI/CD 分钟**、**CI 分钟**、**流水线分钟**、**CI 流水线分钟**、**流水线分钟数配额** 和 **计算单位** 混用。

管理员可以限制项目每月在[共享 runners](../runners/runners_scope.md#shared-runners) 上运行作业的时间。使用 CI/CD 分钟数配额跟踪此限制。

默认情况下，单个作业的一分钟执行时间使用 1 CI/CD 分钟。流水线使用的 CI/CD 分钟总数是[其所有作业持续时间的总和](#how-cicd-minute-usage-is-calculated)。
作业可以同时运行，因此总 CI/CD 分钟使用量可能高于流水线的端到端持续时间。

SaaS 版：

- 所有项目都启用了 CI/CD 分钟数配额，但公开项目消耗 CI/CD 分钟数的速度较慢。
- [命名空间](../../user/namespace/index.md)的每月基本 CI/CD 分钟数配额由其[许可证级别](https://gitlab.cn/pricing/)确定。
- 如果您需要超过每月配额中的 CI/CD 分钟数，您可以[购买额外的 CI/CD 分钟数](#purchase-additional-cicd-minutes)。

私有化部署版：

- CI/CD 分钟数配额默认禁用。
- 启用时，CI/CD 分钟数配额仅适用于私有项目。
- 如果命名空间使用其每月配额中的所有 CI/CD 分钟数，管理员可以[分配更多 CI/CD 分钟数](#set-the-quota-of-cicd-minutes-for-a-specific-namespace)。

[项目 runner](../runners/runners_scope.md#project-runners) 不受 CI/CD 分钟数配额限制。

## 为所有命名空间设置 CI/CD 分钟配额 

<!--
> [Moved](https://about.gitlab.com/blog/2021/01/26/new-gitlab-product-subscription-model/) to GitLab Premium in 13.9.
-->

默认情况下，极狐GitLab 实例没有 CI/CD 分钟数配额。
配额的默认值为 `0`，即授予无限的 CI/CD 分钟数。
但是，您可以更改此默认值。

先决条件：

- 您必须是管理员。

要更改适用于所有命名空间的默认配额：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏，选择 **设置 > CI/CD**。
1. 展开 **持续集成和部署**。
1. 在 **CI/CD 分钟数配额** 框中，输入 CI/CD 分钟的最大数量。
1. 选择 **保存修改**。

如果已为特定命名空间定义了配额，则此值不会更改该配额。

<a id="set-the-quota-of-cicd-minutes-for-a-specific-namespace"></a>

## 为特定命名空间设置 CI/CD 分钟配额

<!--
> [Moved](https://about.gitlab.com/blog/2021/01/26/new-gitlab-product-subscription-model/) to GitLab Premium in 13.9.
-->

您可以覆盖全局值并为特定命名空间设置 CI/CD 分钟数配额。

先决条件：

- 您必须是管理员。

为命名空间设置 CI/CD 分钟数配额：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，选择 **概览 > 群组**。
1. 对于您要更新的群组，选择 **编辑**。
1. 在 **CI/CD 分钟数配额** 框中，输入 CI/CD 分钟的最大数量。
1. 选择 **保存修改**。

<!--
You can also use the [update group API](../../api/groups.md#update-group) or the
[update user API](../../api/users.md#user-modification) instead.
-->

NOTE:
您可以仅为顶级组或用户命名空间设置 CI/CD 分钟数配额。
如果您为子组设置配额，则不会使用它。

## 查看 CI/CD 分钟数

先决条件：

- 您必须有权访问构建，才能查看与构建关联的命名空间的总使用量和配额摘要。
- 访问 **使用量配额** 页面的权限取决于您在相关命名空间或群组中的角色。

### 查看群组的使用量配额报告

> 显示每个项目的共享 runner 时长功能引入于 15.0 版本。

先决条件：

- 您必须是群组的所有者。

查看用于您的组的 CI/CD 分钟数：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组，此群组不能是子组。
1. 在左侧边栏中，选择 **设置 > 使用量配额**。
1. 选择 **流水线** 选项卡。

![Group CI/CD minutes quota](img/group_cicd_minutes_quota.png)

项目列表仅显示当月 CI/CD 分钟数使用量或共享 runner 使用量的项目。该列表包括命名空间及其子组中的所有项目，按 CI/CD 分钟数使用量降序排列。

### 查看个人命名空间的使用配额报告

> 显示共享 runner 时长功能引入于 15.0 版本。

先决条件：

- 命名空间必须是您的个人命名空间。

您可以查看个人命名空间使用的 CI/CD 分钟数：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏中，选择 **使用量配额**。

项目列表仅显示当月的[个人项目](../../user/project/working_with_projects.md#view-personal-projects) 使用 CI/CD 分钟数或共享 runner 的情况。该列表按 CI/CD 分钟数使用量的降序排列。

<a id="purchase-additional-cicd-minutes"></a>

## 购买额外的 CI/CD 分钟数 **(FREE SAAS)**

如果您使用的是 SaaS 版，则可以购买额外的 CI/CD 分钟数包。
额外的 CI/CD 分钟数：

- 仅在订阅中包含的每月配额用完后使用。
- 如果在月底有任何剩余，结转到下个月。
- 有效期为自购买之日起 12 个月或直到所有分钟用完，以先到者为准。当前未强制执行分钟数到期。

例如，使用 SaaS 专业版许可证时：

- 您每月有 10000 分钟。
- 您购买了额外的 5000 分钟。
- 您的总限额为 15000 分钟。

如果您在当月使用 13000 分钟，那么下个月您的额外分钟数将变为 2000。如果您在一个月内使用 9,000 分钟，您的额外分钟数保持不变。

如果您在试用订阅期间购买了额外的 CI/CD 分钟数，则这些分钟数将在试用期结束或您升级到付费计划后可用。

您可以在 [定价页面](https://about.gitlab.cn/pricing/) 上找到额外 CI/CD 分钟数的定价。

<!--
### 为群组购买 CI/CD 分钟数 **(FREE SAAS)**

您可以为您的群组购买额外的 CI/CD 分钟数。
您无法将购买的 CI/CD 分钟数从一个群组转移到另一个群组，因此请务必选择正确的群组。

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 使用量配额**。
1. 选择 **购买更多分钟数**。
1. 填写交易详情。

处理您的付款后，额外的 CI/CD 分钟数将添加到您的群组命名空间。

### Purchase CI/CD minutes for a personal namespace **(FREE SAAS)**

To purchase additional minutes for your personal namespace:

1. On the top bar, in the top right corner, select your avatar.
1. Select **Edit profile**.
1. On the left sidebar, select **Usage Quotas**.
1. Select **Buy additional minutes**. GitLab redirects you to the Customers Portal.
1. Locate the subscription card that's linked to your personal namespace on GitLab SaaS, select **Buy more CI minutes**,
   and complete the details of the transaction.

After your payment is processed, the additional CI/CD minutes are added to your personal
namespace.
-->

<a id="how-cicd-minute-usage-is-calculated"></a>

## 如何计算 CI/CD 分钟数

极狐GitLab 使用此公式来计算作业的 CI/CD 分钟使用量：

```plaintext
Job duration * Cost factor
```

- **作业时长**：作业在共享 runner 上运行的时间（以秒为单位），不包括在 `created` 或 `pending` 状态下花费的时间。
- **消耗参数**：基于项目可见性的数字。

该值将转换为分钟，并添加到作业所在的顶级命名空间中的已用 CI/CD 分钟数内。

例如，如果用户 `alice` 运行流水线：

- 在 `gitlab-cn` 命名空间下，流水线中每个作业使用的 CI/CD 分钟数被添加到 `gitlab-cn` 命名空间的整体消耗中，而不是 `alice` 命名空间。
- 对于命名空间中的其中一个个人项目，CI/CD 分钟数被添加到 `alice` 命名空间的总体消耗中。

一条流水线使用的 CI/CD 分钟数是流水线中运行的所有作业使用的总 CI/CD 分钟数。作业可以同时运行，因此总 CI/CD 分钟使用量可能高于流水线的端到端持续时间。

<a id="cost-factor"></a>

### 消耗参数

在 JiHuLab 的共享 runner 上运行作业的消耗参数为：

- `1`：内部和私有项目。
- `0`：公开项目。

私有化部署的消耗参数为：

- `0`：公开项目，不消耗 CI/CD 分钟数。
- `1`：内部和私有项目。

<a id="cost-factor-for-community-contributions-to-gitlab-projects"></a>

#### 免费版极狐GitLab 项目的消耗参数

使用免费版极狐GitLab 的贡献者在为极狐GitLab 维护的开源项目做出贡献时可以使用共享 Runner 上的 30 万分钟数。仅当专门为极狐GitLab 产品的项目部分做出贡献时，才可能使用最多 300,000 分钟。共享 runner 上可用的总分钟数减少了来自其他项目的流水线使用的 CI/CD 分钟数。
这 30 万分钟数适用于所有 SaaS 层，消耗参数计算方式如下：

- `Monthly minute quota / 300,000 job duration minutes = Cost factor`

例如，专业版中每个月拥有 10,000 的 CI/CD 分钟数：

- 10,000 / 300,000 = 0.03333333333 消耗参数

对于降低的消耗参数：

- 合并请求源项目必须是极狐GitLab 所维护项目的派生。
  <!--例如 [`gitlab-com/www-gitlab-com`](https://gitlab.com/gitlab-com/www-gitlab-com) 或
  [`gitlab-org/gitlab`](https://gitlab.com/gitlab-org/gitlab) 等。-->
- 合并请求目标项目必须是派生的父项目。
- 流水线必须是合并请求、合并结果或合并队列流水线。

极狐GitLab 管理员可以使用名为 `ci_minimal_cost_factor_for_gitlab_namespaces` 的[标志](../../administration/feature_flags.md)将命名空间添加到降低的消耗参数中。

### JiHuLab SaaS 上的额外消耗

JiHuLab SaaS Runner 根据不同的 Runner 类型（Linux、Windows 或 macOS）和虚拟机配置，拥有不同的消耗参数。

| JiHuLab SaaS Runner 类型     | 机器类型 | CI/CD 分钟数消耗参数 |
|:---------------------------|:-----|:---------------------------------|
| Linux OS + Docker 执行器      | 小    | 1                                |
| Linux OS + Docker 执行器 | 中    | 2                                |
| Linux OS + Docker 执行器 | 大    | 3                                |


### 每月重置 CI/CD 分钟数

在每个日历月的第一天，对于所有使用共享 runner 的命名空间，CI/CD 分钟数的累积使用量将重置为 `0`。这意味着您的全部配额可用，并且计算将从 `0` 重新开始。

例如，如果您的每月配额为 `10,000` CI/CD 分钟数：

- 在 **4 月 1 日**，您有 `10,000` 分钟。
- 在四月，您只使用了 `10,000` 分钟中的 `6000`。
- 在 **5 月 1 日**，累计使用分钟数重置为 `0`，您在 5 月有 `10,000` 分钟可再次使用。

保留上个月的使用数据，显示一段时间内的消耗历史视图。

### 购买的 CI/CD 分钟数的月间结转

如果您购买了额外的 CI/CD 分钟数且未全部使用，则剩余量将转入下个月。

例如：

- 在 **4 月 1 日**，您购买了 `5,000` CI/CD 分钟数。
- 在四月，您只使用了 `5,000` 分钟中的 `3000` 分钟。
- 在 **5 月 1 日**，剩余的 `2,000` 分钟会结转到您的配额中。

额外的 CI/CD 分钟数是一次性购买的，不会每月更新或刷新。

## 当您超过配额时会发生什么

当本月使用 CI/CD 分钟书配额时，极狐GitLab 将停止处理新作业。

- 任何应该由共享 runner 选择的非运行作业都会被自动丢弃。
- 任何正在重试的作业都会自动放弃。
- 如果整个命名空间使用量在宽限期内超出配额，则任何正在运行的作业都可以随时删除。

运行作业的宽限期为 `1,000` CI/CD 分钟。

项目 runner 上的作业不受 CI/CD 分钟配额的影响。

### SaaS 使用量通知

在 SaaS 版上，以下情况会向命名空间所有者发送电子邮件通知：

- 可用 CI/CD 分钟数低于配额的 30%。
- 可用 CI/CD 分钟数低于配额的 5%。
- 所有 CI/CD 分钟数已用完。

### 特殊配额限制

在某些情况下，配额限制由以下标记之一代替：

- **无限制分钟数**：适用于具有无限 CI/CD 分钟数的命名空间
- **不支持分钟数**：适用于未启用有效共享 runner 的命名空间

## 减少 CI/CD 分钟数的消耗

如果您的项目消耗过多的 CI/CD 分钟数，您可以使用一些策略来减少 CI/CD 分钟数的使用：

- 如果您使用项目镜像，请确保禁用[用于镜像更新的流水线](../../user/project/repository/mirror/pull.md#触发镜像更新的流水线)。
- 减少[计划流水线](schedules.md)的频率。
- 不需要时跳过流水线。
- 使用[可中断的](../yaml/index.md#interruptible)作业，如果新流水线启动，这些作业可以自动取消。
- 如果作业不必在每个流水线中运行，请使用 [`rules`](../jobs/job_control.md) 使其仅在需要时运行。
- 对某些作业使用私有 runner。
- 如果您正在从一个派生项目工作并向父项目提交合并请求，您可以要求维护者在[父项目中](merge_request_pipelines.md#在父项目中运行流水线)运行流水线。

如果您管理一个开源项目，这些改进还可以减少贡献者派生项目的 CI/CD 分钟消耗，从而实现更多贡献。

有关更多详细信息，请参阅我们的[流水线效率指南](pipeline_efficiency.md)。

## 重置使用的 CI/CD 分钟数 **(PREMIUM SELF)**

管理员可以重置当月命名空间使用的分钟数。

### 重置个人命名空间的分钟数

1. 在管理中心找到[用户](../../user/admin_area/index.md#administering-users)。
1. 选择 **编辑**。
1. 在 **限制** 中，选择 **重置流水线分钟数**。

### 重置群组命名空间的分钟数

1. [在管理中心找到群组](../../user/admin_area/index.md#administering-groups)。
1. 选择 **编辑**。
1. 在 **权限和组功能** 中，选择 **重置流水线分钟数**。
