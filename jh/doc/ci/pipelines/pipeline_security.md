---
stage: Verify
group: Pipeline Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

# 流水线安全

## Secrets 管理

Secrets 管理是开发人员用来在具有严格访问控制的安全环境中安全存储敏感数据的系统。**Secrets** 是应保密的敏感凭证，包括：

- 密码
- SSH 密钥
- 访问令牌
- 其他类型的凭证

## Secrets 存储

### Secrets 管理提供商

最敏感且遵循最严格策略的 secrets 应存储在单独的 secrets 管理提供商中，例如 [Vault](https://www.vaultproject.io)。
Secrets 存储在极狐GitLab 实例外部，这是最安全的选择。

您可以使用极狐GitLab [Vault 集成](../secrets/index.md#use-vault-secrets-in-a-ci-job)，在需要时检索 CI/CD 流水线中的这些 secrets。

### CI/CD 变量

[CI/CD 变量](../variables/index.md)是在 CI/CD 流水线中存储和使用数据的便捷方法，但变量的安全性不如 secrets 管理提供商。
变量值：

- 存储在极狐GitLab 项目、群组或实例设置中。有权访问设置的用户可以访问变量。
- 可以被[覆盖](../variables/index.md#override-a-define-cicd-variable)，很难确定使用了哪个值。
- 更容易因意外的流水线配置错误而暴露。

敏感数据应存储在 secrets 管理解决方案中。如果您想要将低敏感度数据存储在 CI/CD 变量中，请务必：

- [隐藏变量](../variables/index.md#mask-a-cicd-variable)。
- 尽可能[保护变量](../variables/index.md#protect-a-cicd-variable)。
