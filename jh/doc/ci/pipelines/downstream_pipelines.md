---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 下游流水线 **(FREE)**

下游流水线是由另一个流水线触发的任何极狐GitLab CI/CD 流水线。
下游流水线可以是：

- 一个[父子流水线](parent_child_pipelines.md)，它是与第一个流水线在同一个项目中触发的下游流水线。
- [多项目流水线](#multi-project-pipelines)，它是在与第一个流水线不同的项目中触发的下游流水线。

父子流水线和多项目流水线有时可用于类似目的，但存在一些关键差异。

<a id="parent-child-pipelines"></a>

## 父子流水线

父流水线是在同一项目中触发下游流水线的流水线。
下游流水线称为子流水线。

子流水线：

- 在与父流水线相同的项目、ref 和提交 SHA 下运行。
- 不直接影响流水线运行相关的 ref 的整体状态。例如，
  如果主分支的流水线出现故障，通常会说“主分支已损坏”。
  如果子流水线由 [`strategy:depend`](../yaml/index.md#triggerstrategy) 触发，子流水线的状态只影响 ref 的状态。
- 当为同一 ref 创建新流水线时，如果使用 [`interruptible`](../yaml/index.md#interruptible) 配置流水线，则自动取消。
- 不显示在项目的流水线列表中。您只能在其父流水线的详情页面查看子流水线。

### 嵌套子流水线

父子流水线的最大深度为两级子流水线。

一个父流水线可以触发多个子流水线，这些子流水线又可以触发自己的子流水线。您不能触发另一级别的子流水线。

<a id="multi-project-pipelines"></a>

## 多项目流水线

您可以跨多个项目设置[极狐GitLab CI/CD](../index.md)，以便一个项目中的流水线可以触发另一个项目中的流水线。您可以在一个地方可视化整个流水线，包括所有跨项目的相互依赖关系。

多项目流水线：

- 从另一个流水线触发，但上游（触发）流水线对下游（被触发）流水线没有太多控制。但是，它可以选择下游流水线的 ref，并将 CI/CD 变量传递给它。
- 影响它运行的项目的 ref 的整体状态，但不影响触发流水线的 ref 的状态，除非用 [`strategy:depend`](../yaml/index.md#triggerstrategy)。
- 如果在上游流水线中为相同的 ref 运行新流水线，则在使用 [`interruptible`](../yaml/index.md#interruptible) 时不会在下游项目中自动取消。如果为下游项目上的相同 ref 触发了新流水线，它们可以自动取消。
- 在下游项目的流水线列表中可见。
- 是独立的，因此没有嵌套限制。

如果您在下游私有项目中触发流水线，在上游项目的流水线页面，您可以查看：

- 项目名称。
- 流水线的状态。

如果您的公共项目可以在私有项目中触发下游流水线，请确保不存在保密问题。

## 从 `.gitlab-ci.yml` 文件中的作业触发下游流水线

在 `.gitlab-ci.yml` 文件中使用 [`trigger`](../yaml/index.md#trigger) 关键字来创建触发下游流水线的作业。该作业称为触发作业。

父子流水线示例：

```yaml
trigger_job:
  trigger:
    include:
      - local: path/to/child-pipeline.yml
```

多项目流水线示例：

```yaml
trigger_job:
  trigger:
    project: project-group/my-downstream-project
```

触发作业启动后，当极狐GitLab 尝试创建下游流水线时，作业的初始状态为 `pending`。如果下游流水线创建成功，则触发器作业显示 `passed`，否则显示 `failed`。或者，您可以[设置触发作业显示下游流水线的状态](#mirror-the-status-of-a-downstream-pipeline-in-the-trigger-job)。

### 使用 `rules` 控制下游流水线

在下游流水线中使用 CI/CD 变量或 [`rules`](../yaml/index.md#rulesif) 关键字来[控制作业行为](../jobs/job_control.md)。

当您使用 [`trigger`](../yaml/index.md#trigger) 关键字触发下游流水线时，适用于所有作业的 [`$CI_PIPELINE_SOURCE` 预定义变量](../variables/predefined_variables.md)的值是：

- 多项目流水线：`pipeline`。
- 父子流水线：`parent_pipeline`。

例如，要在同时运行合并请求流水线的项目中，控制多项目流水线中的作业：

```yaml
job1:
  rules:
    - if: $CI_PIPELINE_SOURCE == "pipeline"
  script: echo "This job runs in multi-project pipelines only"

job2:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  script: echo "This job runs in merge request pipelines only"

job3:
  rules:
    - if: $CI_PIPELINE_SOURCE == "pipeline"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  script: echo "This job runs in both multi-project and merge request pipelines"
```

### 在不同的项目中使用子流水线配置文件

您可以在触发器作业中，使用 [`include:project`](../yaml/index.md#includeproject) 来使用不同项目中的配置文件触发子流水线：

```yaml
microservice_a:
  trigger:
    include:
      - project: 'my-group/my-pipeline-library'
        ref: 'main'
        file: '/path/to/child-pipeline.yml'
```

### 组合多个子流水线配置文件

定义子流水线时，您最多可以包含三个配置文件。子流水线的配置由合并在一起的所有配置文件组成：

```yaml
microservice_a:
  trigger:
    include:
      - local: path/to/microservice_a.yml
      - template: Security/SAST.gitlab-ci.yml
      - project: 'my-group/my-pipeline-library'
        ref: 'main'
        file: '/path/to/child-pipeline.yml'
```

### 动态子流水线

您可以从作业中生成的 YAML 文件而不是项目中保存的静态文件触发子流水线。这种技术对于生成针对已更改内容的流水线或构建目标和体系结构矩阵非常强大。

包含生成的 YAML 文件的产物不得大于 5MB。

您可以对 [Dhall](https://dhall-lang.org/) 或 [ytt](https://get-ytt.io/) 等其他模板语言使用类似的过程。

#### 触发动态子流水线

从动态生成的配置文件触发子流水线：

1. 在作业中生成配置文件，保存为[产物](../yaml/index.md#artifactspaths)：

   ```yaml
   generate-config:
     stage: build
     script: generate-ci-config > generated-config.yml
     artifacts:
       paths:
         - generated-config.yml
   ```

1. 将触发器作业配置为在生成配置文件的作业之后运行，并将 `include: artifact` 设置为生成的产物：

   ```yaml
   child-pipeline:
     stage: test
     trigger:
       include:
         - artifact: generated-config.yml
           job: generate-config
   ```

在此示例中，极狐GitLab 检索 `generated-config.yml` 并使用该文件中的 CI/CD 配置触发子流水线。

产物路径由极狐GitLab 而非 runner 解析，因此该路径必须与运行极狐GitLab 的操作系统的语法相匹配。如果极狐GitLab 在 Linux 上运行但使用 Windows runner 进行测试，则触发作业的路径分隔符为 `/`。使用 Windows runner 的作业的其他 CI/CD 配置，如脚本，使用 <code>&#92;</code>。

### 使用合并请求流水线运行子流水线

当不使用 `rules` 或 `workflow:rules` 时，流水线（包括子流水线）默认作为分支流水线运行。

要将子流水线配置为在从合并请求（父）流水线触发时运行，请使用 `rules` 或 `workflow:rules`。例如，使用 `rules`：

1. 将父流水线的触发作业设置为在合并请求时运行：

   ```yaml
   trigger-child-pipeline-job:
     trigger:
       include: path/to/child-pipeline-configuration.yml
     rules:
       - if: $CI_PIPELINE_SOURCE == "merge_request_event"
   ```

1. 使用 `rules` 将子流水线作业配置为在父流水线触发时运行：

   ```yaml
   job1:
     script: echo "This child pipeline job runs any time the parent pipeline triggers it."
     rules:
       - if: $CI_PIPELINE_SOURCE == "parent_pipeline"

   job2:
     script: echo "This child pipeline job runs only when the parent pipeline is a merge request pipeline"
     rules:
       - if: $CI_MERGE_REQUEST_ID
   ```

在子流水线中，`$CI_PIPELINE_SOURCE` 的值始终为 `parent_pipeline`，因此：

- 您可以使用 `if: $CI_PIPELINE_SOURCE == "parent_pipeline"` 来确保子流水线作业始终运行。
- 您不能使用 `if: $CI_PIPELINE_SOURCE == "merge_request_event"` 来配置子流水线作业来运行合并请求流水线。 相反，使用 `if: $CI_MERGE_REQUEST_ID` 将子流水线作业设置为仅在父流水线是合并请求流水线时运行。父流水线的 [`CI_MERGE_REQUEST_*` 预定义变量](../variables/predefined_variables.md#predefined-variables-for-merge-request-pipelines)被传递给子流水线作业。

### 为多项目流水线指定一个分支

您可以指定在触发多项目流水线时使用的分支。极狐GitLab 使用分支头部的提交来创建下游流水线。例如：

```yaml
staging:
  stage: deploy
  trigger:
    project: my/deployment
    branch: stable-11-2
```

使用：

- `project` 关键字指定下游项目的完整路径。在 15.3 及更高版本中，您可以使用[变量扩展](../variables/where_variables_can_be_used.md#gitlab-ciyml-file)。
- `branch` 关键字指定 `project` 指定的项目中的分支或[标签](../../user/project/repository/tags/index.md)的名称。您可以使用变量扩展。

### 使用 API 触发多项目流水线

您可以将 [CI/CD 作业令牌（`CI_JOB_TOKEN`）](../jobs/ci_job_token.md)与[流水线触发器 API 端点](../../api/pipeline_triggers.md#trigger-a-pipeline-with-a-token)，从 CI/CD 作业中触发多项目流水线。极狐GitLab 将使用作业令牌触发的流水线设置为包含进行 API 调用的作业的流水线的下游流水线。

例如：

```yaml
trigger_pipeline:
  stage: deploy
  script:
    - curl --request POST --form "token=$CI_JOB_TOKEN" --form ref=main "https://gitlab.example.com/api/v4/projects/9/trigger/pipeline"
  rules:
    - if: $CI_COMMIT_TAG
  environment: production
```

## 查看下游流水线

在[流水线图视图](index.md#view-full-pipeline-graph)中，下游流水线显示为图表右侧的卡片列表。在此视图中，您可以：

- 选择一个触发作业，查看触发的下游流水线的作业。
- 在流水线选项卡上选择 **展开作业** **{chevron-lg-right}**，展开包含下游流水线作业的视图。您一次可以查看一个下游流水线。
- 将鼠标悬停在流水线选项卡上，突出显示触发下游流水线的作业。

### 重试下游流水线中失败和取消的作业

> - 从图表视图重试功能引入于 15.0 版本，[功能标志](../../administration/feature_flags.md)为 `downstream_retry_action`。默认禁用。
> - 从图表视图重试功能一般可用于 15.1 版本。功能标志删除。

要重试失败的和取消的作业，请选择 **重试** (**{retry}**)：

- 从下游流水线的详细信息页面。
- 在流水线图视图中的流水线选项卡上。

<a id="recreate-a-downstream-pipeline"></a>

### 重新创建下游流水线

> - 从流水线图视图重试触发作业功能引入于 15.10 版本，[功能标志](../../administration/feature_flags.md)为 `ci_recreate_downstream_pipeline`。默认禁用。
> - 一般可用于 15.11 版本，删除功能标志 `ci_recreate_downstream_pipeline`。

您可以通过重试相应的触发作业来重新创建下游流水线。新创建的下游流水线将替换流水线图中当前的下游流水线。

要重新创建下游流水线：

- 在流水线图视图中的触发作业卡上选择 **再次运行** (**{retry}**)。

### 取消下游流水线

要取消仍在运行的下游流水线，请选择 **取消** (**{cancel}**)：

- 从下游流水线的详细信息页面。
- 在[流水线图视图](index.md#view-full-pipeline-graph)中的流水线卡上。

<a id="mirror-the-status-of-a-downstream-pipeline-in-the-trigger-job"></a>

### 在触发器作业中镜像下游流水线的状态

您可以使用 [`strategy: depend`](../yaml/index.md#triggerstrategy) 在触发器作业中镜像下游流水线的状态：

父子流水线示例：

```yaml
trigger_job:
  trigger:
    include:
      - local: path/to/child-pipeline.yml
    strategy: depend
```

多项目流水线示例：

```yaml
trigger_job:
  trigger:
    project: my/project
    strategy: depend
```

### 在流水线图中查看多项目流水线 **(PREMIUM)**

当您触发多项目流水线时，下游流水线会显示在[流水线图](index.md#visualize-pipelines)的右侧。

在[流水线迷你图](index.md#pipeline-mini-graphs)中，下游流水线显示在迷你图的右侧。

<a id="fetch-artifacts-from-an-upstream-pipeline"></a>

## 从上游流水线获取产物

使用 [`needs:project`](../yaml/index.md#needsproject) 从上游流水线获取产物：

1. 在上游流水线中，使用 [`artifacts`](../yaml/index.md#artifacts) 关键字将产物保存在作业中，然后使用触发器作业触发下游流水线：

   ```yaml
   build_artifacts:
     stage: build
     script:
       - echo "This is a test artifact!" >> artifact.txt
     artifacts:
       paths:
         - artifact.txt

   deploy:
     stage: deploy
     trigger: my/downstream_project
   ```

1. 在下游流水线的作业中使用 `needs:project` 来获取产物。

   ```yaml
   test:
     stage: test
     script:
       - cat artifact.txt
     needs:
       - project: my/upstream_project
         job: build_artifacts
         ref: main
         artifacts: true
   ```

   设置：

   - `job` 为创建产物的上游流水线中的作业。
   - `ref` 为分支。
   - `artifacts` 为 `true`。

### 从上游合并请求流水线中获取产物

当您使用 `needs:project` 来[将产物传递到下游流水线](#fetch-artifacts-from-an-upstream-pipeline)时，`ref` 值通常是分支名称，例如 `main` 或 `development`。

对于合并请求流水线，`ref` 值的格式为 `refs/merge-requests/<id>/head`，其中 `id` 是合并请求 ID。您可以使用 [`CI_MERGE_REQUEST_REF_PATH`](../variables/predefined_variables.md#predefined-variables-for-merge-request-pipelines) CI/CD 变量检索此 ref。不要将分支名称用作合并请求流水线的 `ref`，因为下游流水线会尝试从最新的分支流水线中获取产物。

要从上游 `merge request` 流水线而不是 `branch` 流水线获取产物，请使用变量继承将 `CI_MERGE_REQUEST_REF_PATH` 传递给下游流水线：

1. 在上游流水线的作业中，使用 [`artifacts`](../yaml/index.md#artifacts) 关键字保存产物。
1. 在触发下游流水线的作业中，传递 `$CI_MERGE_REQUEST_REF_PATH` 变量：

   ```yaml
   build_artifacts:
     stage: build
     script:
       - echo "This is a test artifact!" >> artifact.txt
     artifacts:
       paths:
         - artifact.txt

   upstream_job:
     variables:
       UPSTREAM_REF: $CI_MERGE_REQUEST_REF_PATH
     trigger:
       project: my/downstream_project
       branch: my-branch
   ```

1. 在下游流水线的作业中，使用 `needs:project` 和传递的变量作为 `ref` 从上游流水线获取产物：

   ```yaml
   test:
     stage: test
     script:
       - cat artifact.txt
     needs:
       - project: my/upstream_project
         job: build_artifacts
         ref: UPSTREAM_REF
         artifacts: true
   ```

您可以使用此方法从上游合并请求流水线中获取产物，但不能从[合并结果流水线](merged_results_pipelines.md)中获取产物。

## 将 CI/CD 变量传递到下游流水线

您可以根据创建或定义变量的位置，使用几种不同的方法将 CI/CD 变量传递到下游流水线。

<a id="pass-yaml-defined-cicd-variables"></a>

### 传递 YAML 定义的 CI/CD 变量

您可以使用 `variables` 关键字将 CI/CD 变量传递到下游流水线，就像处理任何其他作业一样。

父子流水线示例：

```yaml
variables:
  VERSION: "1.0.0"

staging:
  variables:
    ENVIRONMENT: staging
  stage: deploy
  trigger:
    include:
      - local: path/to/child-pipeline.yml
```

多项目流水线示例：

```yaml
variables:
  VERSION: "1.0.0"

staging:
  variables:
    ENVIRONMENT: staging
  stage: deploy
  trigger: my-group/my-deployment-project
```

`ENVIRONMENT` 变量被传递给下游流水线中定义的每个作业。当极狐GitLab Runner 选择作业时，它可以作为变量使用。

在以下配置中，`MY_VARIABLE` 变量被传递到在 `trigger-downstream` 作业排队时创建的下游流水线。这是因为 `trigger-downstream` 作业继承了全局变量块中声明的变量，然后我们将这些变量传递给下游流水线。

### 防止全局变量被传递

您可以使用 [`inherit:variables:false`](../yaml/index.md#inheritvariables) 阻止全局 CI/CD 变量到达下游流水线。

父子流水线示例：

```yaml
variables:
  GLOBAL_VAR: value

trigger-job:
  inherit:
    variables: false
  variables:
    JOB_VAR: value
  trigger:
    include:
      - local: path/to/child-pipeline.yml
```

多项目流水线示例：

```yaml
variables:
  GLOBAL_VAR: value

trigger-job:
  inherit:
    variables: false
  variables:
    JOB_VAR: value
  trigger: my-group/my-project
```

在此示例中，`GLOBAL_VAR` 变量在触发流水线中不可用，但 `JOB_VAR` 可用。

### 传递一个预定义的变量

使用[预定义的 CI/CD 变量](../variables/predefined_variables.md)传递有关上游流水线的信息，使用插值。将预定义变量保存为触发作业中的新作业变量，传递给下游流水线。

父子流水线示例：

```yaml
trigger-job:
  variables:
    PARENT_BRANCH: $CI_COMMIT_REF_NAME
  trigger:
    include:
      - local: path/to/child-pipeline.yml
```

多项目流水线示例：

```yaml
trigger-job:
  variables:
    UPSTREAM_BRANCH: $CI_COMMIT_REF_NAME
  trigger: my-group/my-project
```

包含上游流水线的 `$CI_COMMIT_REF_NAME` 预定义 CI/CD 变量值的 `UPSTREAM_BRANCH` 变量在下游流水线中可用。

不要使用此方法将[隐藏的变量](../variables/index.md#mask-a-cicd-variable)传递到多项目流水线。CI/CD 隐藏配置不会传递到下游流水线，变量可能会在下游项目的作业日志中被取消隐藏。

您不能使用此方法将[作业级持久变量](../variables/where_variables_can_be_used.md#persisted-variables)转发到下游流水线，因为它们在触发器作业中不可用。

上游流水线优先于下游流水线。如果在上游和下游项目中定义了两个同名变量，则上游项目中定义的变量优先。

<a id="pass-dotenv-variables-created-in-a-job"></a>

### 传递作业中创建的 dotenv 变量 **(PREMIUM)**

您可以使用 [`dotenv` 变量继承](../variables/index.md#pass-an-environment-variable-to-another-job) 和 [`needs:project`](../yaml/index.md#needsproject) 将变量传递到下游的作业。这些变量仅在作业脚本中可用，不能用于配置它，例如使用 `rules` 或 `artifact:paths`。

例如，在[多项目流水线](#multi-project-pipelines)中：

1. 将变量保存在 `.env` 文件中。
1. 将 `.env` 文件保存为 `dotenv` 报告。
1. 触发下游流水线。

   ```yaml
   build_vars:
     stage: build
     script:
       - echo "BUILD_VERSION=hello" >> build.env
     artifacts:
       reports:
         dotenv: build.env

   deploy:
     stage: deploy
     trigger: my/downstream_project
   ```

1. 在下游流水线中设置 `test` 作业以从具有 `needs` 的上游项目中的 `build_vars` 作业继承变量。`test` 作业继承了 `dotenv` 报告中的变量，它可以访问脚本中的 `BUILD_VERSION`：

   ```yaml
   test:
     stage: test
     script:
       - echo $BUILD_VERSION
     needs:
       - project: my/upstream_project
         job: build_vars
         ref: master
         artifacts: true
   ```
