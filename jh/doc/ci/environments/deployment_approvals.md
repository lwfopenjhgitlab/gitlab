---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
description: Require approvals prior to deploying to a Protected Environment
---

# 部署批准 **(PREMIUM)**

> 引入于 14.7 版本，功能标志名为 `deployment_approvals`。默认禁用。
> 功能标志移除于 14.8 版本。

在部署到某些受保护的环境（例如，生产环境）之前要求额外的批准可能很有用。部署前批准要求对于适应每次部署之前必须进行的测试、安全性或合规性流程很有用。

当受保护的环境需要一个或多个批准时，该环境的所有部署都会被阻止，并在运行前等待所需的来自 `Allowed to Deploy` 列表的批准。

<!--
NOTE:
See the [epic](https://gitlab.com/groups/gitlab-org/-/epics/6832) for planned features.
-->

## 先决条件

- [极狐GitLab 环境和部署](index.md)的基础知识。
- [受保护环境](protected_environments.md)的基础知识。

## 为项目配置部署批准

为项目配置部署批准：

1. [创建部署作业](#创建部署作业)。
1. [需要为受保护环境批准](#需要为受保护环境批准)。

### 创建部署作业

在所需项目的 `.gitlab-ci.yml` 文件中创建部署作业。这个作业**不需要**手动（`when:manual`）。

示例：

   ```yaml
   stages:
     - deploy

   production:
     stage: deploy
     script:
       - 'echo "Deploying to ${CI_ENVIRONMENT_NAME}"'
     environment:
       name: ${CI_JOB_NAME}
   ```

### 需要为受保护环境批准

配置批准要求有两种方式：

- [统一批准设置](#unified-approval-setting-deprecated)：您可以定义谁可以执行**和**批准部署。
  当组织中的执行者和批准者之间没有职责分离时。
- [多个批准规则](#multiple-approval-rules)：您可以定义谁可以执行**或**批准部署。
  当组织中的执行者和批准者之间存在职责分离时。

NOTE:
多个批准规则是比统一批准设置更灵活的选项，因此两种配置不应共存，多个批准规则优先于统一审批设置。

<a id="unified-approval-setting-deprecated"></a>

#### 统一批准设置（已废弃）

> - UI 配置功能删除于 15.11 版本。

要为受保护的环境配置批准：

- 使用 [REST API](../../api/protected_environments.md#protect-a-single-environment)，将 `required_approval_count` 字段设置为 1 或更多。

配置完成后，部署到此环境的所有作业都会自动进入阻止状态并等待批准才能运行。确保所需的批准数量少于允许部署的用户数量。

示例：

```shell
curl --header 'Content-Type: application/json' --request POST \
     --data '{"name": "production", "deploy_access_levels": [{"group_id": 9899826}], "required_approval_count": 1}' \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/22034114/protected_environments"
```

NOTE:
要保护、更新或取消保护环境，您必须至少具有维护者角色。

<a id="multiple-approval-rules"></a>

#### 多个批准规则

> - 引入于 14.10 版本，带有名为 `deployment_approval_rules` 的功能标志。默认禁用。
> - 一般可用于 15.0 版本。删除功能标志 `deployment_approval_rules`。
> - UI 配置引入于 15.11 版本。

- 使用 REST API。
  - `deploy_access_levels` 表示哪个实体可以执行部署作业。
  - `approval_rules` 表示哪个实体可以批准部署作业。
- 使用 [UI](protected_environments.md#protecting-environments)。
  - **允许部署** 设置哪些实体可以执行部署作业。
  - **核准人** 设置哪些实体可以批准部署作业。

配置完成后，部署到此环境的所有作业都会自动进入阻止状态并等待批准才能运行。确保所需的批准数量少于允许部署的用户数量。

示例：

```shell
curl --header 'Content-Type: application/json' --request POST \
     --data '{"name": "production", "deploy_access_levels": [{"group_id": 138}], "approval_rules": [{"group_id": 134}, {"group_id": 135, "required_approvals": 2}]}' \
     --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/groups/128/protected_environments"
```

使用此设置：

- 运维人员组（`group_id: 138`）有权执行部署作业到组织中的 `production` 环境（`group_id: 128`）。
- QA 测试人员组 (`group_id: 134`) 和安全组 (`group_id: 135`) 有权批准将部署作业部署到组织中的 `production` 环境 (`group_id: 128`)。
- 除非已经收集到两个安全组的批准和一个 QA 测试组的批准，否则运维人员组无法执行部署作业。

NOTE:
要保护、更新或取消保护环境，您必须至少具有维护者角色。

### 允许自行批准 **(PREMIUM)**

> 引入于 15.8 版本。

默认情况下，不允许触发部署流水线的用户同时批准部署作业。
要允许自行批准部署作业：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > CI/CD**。
1. 展开 **受保护的环境**。
1. 从 **批准选项** 中，选中 **允许流水线触发器批准部署** 复选框。

当流水线运行时，如果允许触发部署的用户批准，部署作业将在流水线中自动被批准。

## 批准或拒绝部署

> 引入于 14.9 版本

使用 GitLab UI 或 API，您可以：

- 批准部署使其继续进行。
- 拒绝部署来阻止它。

NOTE:
极狐GitLab 管理员可以批准或拒绝所有部署。

### 使用 UI 批准或拒绝部署

先决条件：

- 部署到受保护环境的权限。

要使用 UI 批准或拒绝部署到受保护环境：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 环境**。
1. 选择环境的名称。
1. 在部署行中，选择 **批准选项** (**{thumb-up}**)。
   在批准或拒绝部署之前，您可以查看批准数量和剩余所需的批准数，以及批准或拒绝它的人。
1. 可选。添加评论，说明您批准或拒绝部署的原因。
1. 选择 **批准** 或 **拒绝**。

### 使用 API 批准或拒绝部署

先决条件：

- 部署到受保护环境的权限。

要使用 API 批准或拒绝部署到受保护的环境，请传递所需的属性。<!--有关更多详细信息，请参阅[批准或拒绝被阻止的部署](../../api/deployments.md#approve-or-reject-a-blocked-deployment)。-->

示例：

```shell
curl --data "status=approved&comment=Looks good to me" \
     --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/1/deployments/1/approval"
```

### 查看部署的批准详情

先决条件：

- 部署到受保护环境的权限。

只有在获得所有必需的批准后，才能继续部署到受保护的环境。

查看部署的批准详细详情：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **部署 > 环境**。
1. 选择环境名称。
1. 在部署行中，选择 **批准选项** (**{thumb-up}**)。

显示批准状态详情：

- 符合条件的审批人
- 授予的批准数量和需要的批准数量
- 已批准的用户
- 批准或拒绝的历史

## 如何查看被阻止的部署

### 使用 UI

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 环境**。
1. 选择要部署到的环境。
1. 寻找 `blocked` 标签。

### 使用 API

使用部署 API<!--[部署 API](../../api/deployments.md)--> 查看部署。`status` 字段指示部署是否被阻止。


- `status` 字段指示部署是否被阻止。
- 当配置了[统一批准设置](#unified-approval-setting-deprecated)时：
  - `pending_approval_count` 字段表示还有多少批准可以运行部署。
  - `approvals` 字段包含部署的批准。
- 当配置了[多个批准规则](#multiple-approval-rules)时：
  - `approval_summary` 字段包含每个规则的当前批准状态。

<!--
## Related features

For details about other GitLab features aimed at protecting deployments, see [safe deployments](deployment_safety.md).
-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
