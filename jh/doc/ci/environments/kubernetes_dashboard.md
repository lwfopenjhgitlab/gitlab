---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

# Kubernetes 仪表盘（Beta）**(FREE)**

> 引入于 16.1 版本，[功能标志](../../administration/feature_flags.md)为 `environment_settings_to_graphql`、`kas_user_access`、`kas_user_access_project` 和 `expose_authorized_cluster_agents`。此功能处于 [Beta](../../policy/experiment-beta-support.md#beta) 阶段。

使用 Kubernetes 仪表盘通过直观的可视化界面了解集群的状态。
该仪表盘适用于每个连接的 Kubernetes 集群，无论您是使用 CI/CD 还是 GitOps 部署它们。

对于 Flux 用户，给定环境的同步状态不会显示在仪表盘中。
<!--[Issue 391581](https://gitlab.com/gitlab-org/gitlab/-/issues/391581) proposes to add this functionality.-->

## 配置仪表盘

配置仪表盘，将其用于特定环境。
您可以为已存在的环境配置仪表盘，也可以在创建环境时添加仪表盘。

先决条件：

- Kubernetes 代理必须使用 [`user_access`](../../user/clusters/agent/user_access.md) 关键字与环境的项目或其父组共享。

### 环境已经存在

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**)，查找您的项目。
1. 选择 **运维 > 环境**。
1. 选择要关联 Kubernetes 的环境。
1. 选择 **编辑**。
1. 选择适用于 Kubernetes 的极狐GitLab 代理。
1. 选择 **保存**。

### 环境未存在

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**)，查找您的项目。
1. 选择 **运维 > 环境**。
1. 选择 **新建环境**。
1. 填写 **名称** 字段。
1. 选择适用于 Kubernetes 的极狐GitLab 代理。
1. 选择 **保存**。

## 查看仪表盘

要查看配置的仪表盘：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**)，查找您的项目。
1. 选择 **运维 > 环境**。
1. 扩展与适用于 Kubernetes 的极狐GitLab 代理关联的环境。
1. 展开 **Kubernetes 概述**。

## 故障排除

使用 Kubernetes 仪表盘时，您可能会遇到以下问题。

### 用户无法列出 API 组中的资源

您可能会收到一条错误，`Error: services is forbidden: User "gitlab:user:<user-name>" cannot list resource "<resource-name>" in API group "" at the cluster scope`。

当不允许用户在 [Kubernetes RBAC](https://kubernetes.io/docs/reference/access-authn-authz/rbac/) 中执行指定操作时，会发生此错误。

要解决此问题，请检查您的 [RBAC 配置](../../user/clusters/agent/user_access.md#configure-kubernetes-access)。如果 RBAC 配置正确，请联系 Kubernetes 管理员。
