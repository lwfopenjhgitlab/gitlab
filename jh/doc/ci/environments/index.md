---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
disqus_identifier: 'https://docs.gitlab.com/ee/ci/environments.html'
---

# 环境和部署 **(FREE)**

环境描述了代码的部署位置。

每次 [GitLab CI/CD](../yaml/index.md) 将一个版本的代码部署到环境中，就会创建一个部署。

极狐GitLab：

- 提供每个环境的完整部署历史。
- 跟踪您的部署，以便您始终了解服务器上部署的内容。

如果你有一个像 Kubernetes<!--[Kubernetes](../../user/infrastructure/clusters/index.md)--> 这样的部署服务与您的项目相关联，您可以用它来协助你的部署。
<!--您甚至可以从极狐GitLab 中，[Web 终端](#web-terminals-deprecated)访问您的环境。-->

## 查看环境和部署

先决条件：

- 您必须至少拥有报告者角色。

有几种方法可以查看特定项目的环境列表：

- 在项目的概览页面上，如果至少有一个环境可用（未停止）。

  ![Number of Environments](img/environments_project_home.png)

- 在左侧边栏中，选择 **部署 > 环境**，显示环境。

   ![Environments list](img/environments_list_v14_8.png)

- 要查看环境的部署列表，请选择环境名称，例如 `staging`。

   ![Deployments list](img/deployments_list.png)

部署仅在部署作业创建之后才会显示在此列表中。

## 搜索环境

> - 引入于 15.5 版本。
> - 在文件夹中搜索环境功能引入于 15.7 版本，功能标志为 `enable_environments_search_within_folder`。默认禁用。

要按名称搜索环境：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **部署 > 环境**。
1. 在搜索栏中，输入您的搜索词。
   - 您的**搜索词的长度应为 3 个或更多字符**。
   - 从环境名称的开头开始匹配。
     - 例如，`devel` 匹配环境名称 `development`，但 `elop` 不匹配。
   - 对于具有文件夹名称格式的环境，从基本文件夹名称之后匹配。
     - 例如，当名称为 `review/test-app` 时，搜索词 `test` 匹配 `review/test-app`。
     - 搜索带有前缀如 `review/test` 的文件夹名称匹配 `review/test-app`。

## 环境类型

环境是静态的或动态的：

- 静态环境
  - 通常由连续的部署重用。
  - 具有静态名称 - 例如，`staging` 或 `production`。
  - 手动创建或作为 CI/CD 流水线的一部分创建。
- 动态环境
  - 通常在 CI/CD 流水线中创建并仅由单个部署使用，然后停止或删除。
  - 具有动态名称，通常基于 CI/CD 变量的值。
  - 作为 [review apps](../review_apps/index.md) 的一项功能。


<a id="create-a-static-environment"></a>

### 创建静态环境

您可以在 UI 或您的 `.gitlab-ci.yml` 文件中创建静态环境。

#### UI 界面

先决条件：

- 您必须至少具有开发者角色。

要在 UI 中创建静态环境：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 环境**。
1. 选择 **新建环境**。
1. 配置各个字段。
1. 选择 **保存**。

#### `.gitlab-ci.yml` 文件

先决条件：

- 您必须至少具有开发者角色。

要创建静态环境，请在您的 `.gitlab-ci.yml` 文件中：

1. 在 `deploy` 阶段定义一个作业。
1. 在作业中，定义环境 `name` 和`url`。如果流水线运行时不存在该名称的环境，则会创建它。

NOTE:
环境名称中不能使用某些字符。有关 `environment` 关键字的更多信息，请参阅 [`.gitlab-ci.yml` 关键字参考](../yaml/index.md#environment)。

例如，要创建一个名为 `staging` 的环境，URL 为 `https://staging.example.com`：

```yaml
deploy_staging:
  stage: deploy
  script:
    - echo "Deploy to staging server"
  environment:
    name: staging
    url: https://staging.example.com
```

<a id="create-a-dynamic-environment"></a>

### 创建动态环境

要创建动态环境，您可以使用每个流水线唯一的 [CI/CD 变量](../variables/index.md)。

先决条件：

- 您必须至少具有开发者角色。

要创建动态环境，请在您的 `.gitlab-ci.yml` 文件中：

1. 在 `deploy` 阶段定义一个作业。
1. 在作业中，定义以下环境属性：
   - `name`：使用相关的 CI/CD 变量，如 `$CI_COMMIT_REF_SLUG`。（可选）向环境名称添加静态前缀，[在 UI 中分组](#group-similar-environments)所有具有相同前缀的环境。
   - `url`：可选。在主机名前加上相关的 CI/CD 变量，例如 `$CI_ENVIRONMENT_SLUG`。

NOTE:
环境名称中不能使用某些字符。有关 `environment` 关键字的更多信息，请参阅 [`.gitlab-ci.yml` 关键字参考](../yaml/index.md#environment)。

在以下示例中，每次运行 `deploy_review_app` 作业时，环境的名称和 URL 都使用唯一值定义。

```yaml
deploy_review_app:
  stage: deploy
  script: make deploy
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://$CI_ENVIRONMENT_SLUG.example.com
  only:
    - branches
  except:
    - main
```

<!--
### Rename an environment

> - Renaming an environment by using the UI was [removed](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68550) in GitLab 14.3.
> - Renaming an environment by using the API was [deprecated](https://gitlab.com/gitlab-org/gitlab/-/issues/338897) in GitLab 15.9 and is planned for removal in GitLab 16.0.

You cannot rename an environment by using the UI, and the API method was deprecated in GitLab 15.9.

To achieve the same result as renaming an environment:

1. [Stop the existing environment](#stop-an-environment-through-the-ui).
1. [Delete the existing environment](#delete-an-environment).
1. [Create a new environment](#create-a-static-environment) with the desired name.
-->

<a id="deployment-tier-of-environments"></a>

## 环境部署级别

> 引入于 13.10 版本。

有时，您可能不想使用行业标准环境名称（如 `production`），而是希望使用代码名称（如 `customer-portal`）。
虽然没有技术上的理由不使用 `“customer-portal` 这样的名称，但该名称不再表明该环境用于生产。

要指示特定环境用于特定用途，您可以使用 tier：

| 环境 tier | 环境名称示例                          |
|------------------|----------------------------------------------------|
| `production`     | Production, Live                                   |
| `staging`        | Staging, Model, Demo                               |
| `testing`        | Test, QC                                           |
| `development`    | Dev, Review apps, Trunk |
| `other`          |                                                    |

默认情况下，系统假定一个基于 [环境名称](../yaml/index.md#environmentname) 的 tier。
相反，您可以使用 [`deployment_tier` 关键字](../yaml/index.md#environmentdeployment_tier) 来指定 tier。

## 配置手动部署

您可以创建需要有人手动启动部署的作业。
例如：

```yaml
deploy_prod:
  stage: deploy
  script:
    - echo "Deploy to production server"
  environment:
    name: production
    url: https://example.com
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
```

`when: manual` 操作：

- 在 GitLab UI 中公开作业的运行按钮，带有**可以手动部署到&lt;环境&gt;**的文字。
- 表示只有在单击运行按钮时才会触发 `deploy_prod` 作业。

您可以在流水线、环境、部署和作业视图中找到运行按钮。

<a id="configure-kubernetes-deployments-deprecated"></a>

## 配置 Kubernetes 部署（已弃用）

> - 引入于 12.6 版本。
> - 废弃于 14.5 版本。

WARNING:
此功能废弃于 14.5 版本。

如果您要部署到与您的项目关联的 Kubernetes 集群，您可以从您的 `.gitlab-ci.yml` 文件中配置这些部署。

NOTE:
由极狐GitLab 管理的 Kubernetes 集群不支持 Kubernetes 配置。

支持以下配置选项：

- [`namespace`](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)

在以下示例中，该作业将您的应用程序部署到 `production` Kubernetes 命名空间。

```yaml
deploy:
  stage: deploy
  script:
    - echo "Deploy to production server"
  environment:
    name: production
    url: https://example.com
    kubernetes:
      namespace: production
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

当您使用 GitLab Kubernetes 集成部署到 Kubernetes 集群时，您可以查看集群和命名空间信息。在部署作业页面上，它显示在作业跟踪上方：

![Deployment cluster information](../img/environments_deployment_cluster_v12_8.png)

<!--
### 配置增量部署

Learn how to release production changes to only a portion of your Kubernetes pods with
[incremental rollouts](../environments/incremental_rollouts.md).
-->

## 环境和部署的 CI/CD 变量

创建环境时，指定名称和 URL。

如果要在另一个作业中使用名称或 URL，可以使用：

- `$CI_ENVIRONMENT_NAME`。`.gitlab-ci.yml` 文件中定义的名称。
- `$CI_ENVIRONMENT_SLUG`。例如，名称的 “cleaned-up” 版本，适用于 URL 和 DNS。这个变量保证是唯一的。
- `$CI_ENVIRONMENT_URL`。环境的 URL，在 `.gitlab-ci.yml` 文件中指定或自动分配。

如果更改现有环境的名称，则：

- `$CI_ENVIRONMENT_NAME` 变量更新为新的环境名称。
- `$CI_ENVIRONMENT_SLUG` 变量保持不变以防止意外的副作用。

<a id="set-dynamic-environment-urls-after-a-finishes"></a>

## 作业完成后设置动态环境 URL

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/17066) in GitLab 12.9.
-->

在作业脚本中，您可以指定静态环境 URL。
但是，有时您可能需要动态 URL。 例如，如果您将 Review App 部署到每次部署生成随机 URL 的外部托管服务，例如 `https://94dd65b.amazonaws.com/qa-lambda-1234567`。
在这种情况下，在部署脚本完成之前您不知道 URL。如果要在极狐GitLab 中使用环境 URL，则必须手动更新它。

为了解决这个问题，您可以配置一个部署作业来报告一组变量。这些变量包括由外部服务动态生成的 URL。
系统支持 [dotenv (`.env`)](https://github.com/bkeepers/dotenv) 文件格式，并使用 `.env` 文件中定义的变量扩展 `environment:url` 值。

要使用此功能，请在 `.gitlab-ci.yml` 中指定 `artifacts:reports:dotenv`<!--[`artifacts:reports:dotenv`](../yaml/artifacts_reports.md#artifactsreportsdotenv)--> 关键字。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see [Set dynamic URLs after a job finished](https://youtu.be/70jDXtOf4Ig).
-->

### 设置动态环境 URL 示例

以下示例显示了一个 Review App，它为每个合并请求创建一个新环境。`review` 作业由每次推送触发，并创建或更新名为 `review/your-branch-name` 的环境。环境 URL 设置为 `$DYNAMIC_ENVIRONMENT_URL`：

```yaml
review:
  script:
    - DYNAMIC_ENVIRONMENT_URL=$(deploy-script)                                 # In script, get the environment URL.
    - echo "DYNAMIC_ENVIRONMENT_URL=$DYNAMIC_ENVIRONMENT_URL" >> deploy.env    # Add the value to a dotenv file.
  artifacts:
    reports:
      dotenv: deploy.env                                                       # Report back dotenv file to rails.
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: $DYNAMIC_ENVIRONMENT_URL                                              # and set the variable produced in script to `environment:url`
    on_stop: stop_review

stop_review:
  script:
    - ./teardown-environment
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
```

一旦 `review` 作业完成，系统就会更新 `review/your-branch-name` 环境的 URL。
通过解析 `deploy.env` 报告产物，将变量列表注册为 runtime-created，使用它来扩展 `environment:url: $DYNAMIC_ENVIRONMENT_URL` 并将其设置为环境 URL。
您还可以在 `environment:url` 指定 URL 的静态部分，例如 `https://$DYNAMIC_ENVIRONMENT_URL`。 如果 `DYNAMIC_ENVIRONMENT_URL`的值为 `example.com`，则最终结果为 `https://example.com`。

为 `review/your-branch-name` 环境分配的 URL 在 UI 中可见。

请注意以下事项：

- `stop_review` 不会生成 dotenv 报告产物，因此它无法识别 `DYNAMIC_ENVIRONMENT_URL` 环境变量。因此您不应该在 `stop_review` 作业中设置 `environment:url`。
- 如果环境 URL 无效（例如，URL 格式错误），系统不会更新环境 URL。
- 如果在 `stop_review` 中运行的脚本只存在于您的仓库中，因此不能使用 `GIT_STRATEGY: none`，请为这些作业配置[合并请求流水线](../../ci/pipelines/merge_request_pipelines.md)。这确保即使在删除功能分支后， runners 也可以获取仓库。有关更多信息，请参阅 [runners 参考规范](../pipelines/index.md#ref-specs-for-runners)。

NOTE:
对于 Windows runners，使用 `echo` 写入 `.env` 文件可能会失败。在这种情况下，使用 PowerShell `Add-Content` 命令会有所帮助。 例如：

```powershell
Add-Content -Path deploy.env -Value "DYNAMIC_ENVIRONMENT_URL=$DYNAMIC_ENVIRONMENT_URL"
```

## 跟踪每个部署新包含的合并请求

极狐GitLab 可以跟踪每个部署中新包含的合并请求。
当部署成功时，系统会计算最新部署和先前部署之间的提交差异。
此跟踪信息可以通过 Deployment API<!--[Deployment API](../../api/deployments.md#list-of-merge-requests-associate-with-a-deployment)--> 获取并在[合并请求页面](../../user/project/merge_requests/index.md)，显示在合并后的流水线中。

要激活此跟踪，您的环境必须配置如下：

- [环境名称](../yaml/index.md#environmentname) 没有用 `/`（即顶级/长期环境），*或*
- [环境 tier](#环境部署级别) 为 `production` 或 `staging`。

以下是 `.gitlab-ci.yml` 中 [`environment` 关键字](../yaml/index.md#environment)的示例设置：

```yaml
# Trackable
environment: production
environment: production/aws
environment: development

# Non Trackable
environment: review/$CI_COMMIT_REF_SLUG
environment: testing/aws
```

## 使用环境

配置环境后，极狐GitLab 提供了许多使用它们的功能，如下所述。

### 环境回滚

当您在特定提交上回滚部署时，会创建一个*新*部署。此部署有自己唯一的作业 ID。
它指向您要回滚的提交。

为了使回滚成功，部署过程必须在作业的 `script` 中定义。

#### 重试或回滚部署

如果部署出现问题，您可以重试或回滚。

要重试或回滚部署：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 环境**。
1. 选择环境。
1. 在部署名称的右侧：
    - 要重试部署，请选择 **重新部署至环境**。
    - 要回滚到部署，在之前成功的部署旁边，选择 **回滚环境**。

NOTE:
如果您的项目中[阻止过期的部署作业](deployment_safety.md#prevent-outdated-deployment-jobs)，回滚按钮可能会被隐藏或禁用。在这种情况下，请参阅[如何回滚到过期的部署](deployment_safety.md#how-to-rollback-to-an-outdated-deployment)。

### 环境 URL

[环境 URL](../yaml/index.md#environmenturl) 显示在极狐GitLab 的几个地方：

- 在作为链接的合并请求中：
  ![Environment URL in merge request](../img/environments_mr_review_app.png)
- 在环境视图中作为一个按钮：
  ![Open live environment from environments view](img/environments_open_live_environment_v14_8.png)
- 在部署视图中作为一个按钮：
  ![Environment URL in deployments](../img/deployments_view.png)

如果出现以下情况，您可以在合并请求中看到此信息：

- 合并请求最终合并到默认分支（通常是 `main`）。
- 该分支还部署到环境（例如，`staging` 或 `production`）。

例如：

![Environment URLs in merge request](../img/environments_link_url_mr.png)

#### 从源文件转到公共页面

使用 GitLab Route Maps<!--[Route Maps](../review_apps/index.md#route-maps)-->，您可以直接从源文件转到为 Review Apps 设置的环境中的公共页面。

<a id="stopping-an-environment"></a>

### 停止环境

停止环境意味着无法在目标服务器上访问其部署。您必须先停止环境，然后才能将其删除。

如果环境定义了 [`on_stop` 操作](../yaml/index.md#environmenton_stop)，可以执行此操作来停止环境。

<a id="stop-an-environment-when-a-branch-is-deleted"></a>

#### 删除分支时停止环境

您可以将环境配置为在删除分支时停止。

在以下示例中，`deploy_review` 作业调用 `stop_review` 作业来清理和停止环境。

- 两个作业必须具有相同的 [`rules`](../yaml/index.md#rules) 或 [`only/except`](../yaml/index.md#only--except) 配置。否则，`stop_review` 作业可能不会包含在具有 `deploy_review` 作业的所有流水线中，并且您无法触发 `action: stop` 来自动停止环境。
- 如果比启动环境的作业晚。带有 [`action: stop` 的作业可能不会运行](#the-job-with-action-stop-doesnt-run)。
- 如果您不能使用[合并请求流水线](../pipelines/merge_request_pipelines.md)，请将 [`GIT_STRATEGY`](../runners/configure_runners.md#git-strategy) 在 `stop_review` 作业中设置为 `none`。然后 [runner](https://docs.gitlab.cn/runner/) 在分支被删除后不会尝试检出代码。

```yaml
deploy_review:
  stage: deploy
  script:
    - echo "Deploy a review app"
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://$CI_ENVIRONMENT_SLUG.example.com
    on_stop: stop_review

stop_review:
  stage: deploy
  script:
    - echo "Remove review app"
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
  when: manual
```

<a id="stop-an-environment-when-a-merge-request-is-merged-or-closed"></a>

#### 合并或关闭合并请求时停止环境

当您使用[合并请求流水线](../pipelines/merge_request_pipelines.md)配置时，会自动启用 `stop` 触发器。

在以下示例中，`deploy_review` 作业调用 `stop_review` 作业来清理和停止环境。

```yaml
deploy_review:
  stage: deploy
  script:
    - echo "Deploy a review app"
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    on_stop: stop_review
  rules:
    - if: $CI_MERGE_REQUEST_ID

stop_review:
  stage: deploy
  script:
    - echo "Remove review app"
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: manual
```

#### 在环境停止时运行流水线作业

您可以指定在环境停止时运行的作业。

先决条件：

- 两个作业必须具有相同的规则或 only/except 配置。
- `stop_review_app` 作业**必须**定义以下关键字：
  - `when`，定义于：
    - [工作级别](../yaml/index.md#when)。
    - [在规则子句中](../yaml/index.md#rules)。如果您使用 `rules:` 和 `when: manual`，您还应该设置 [`allow_failure: true`](../yaml/index.md#allow_failure)，这样即使作业没有运行，流水线也可以完成。
  - `environment:name`
  - `environment:action`

在您的 `.gitlab-ci.yml` 文件中，在 [`on_stop`](../yaml/index.md#environmenton_stop) 关键字中指定停止环境的作业的名称。

在以下示例中：

- `review_app` 作业在第一个作业完成后调用 `stop_review_app` 作业。
- `stop_review_app` 是根据 `when` 下定义的内容触发的。在这种情况下，它被设置为 `manual`，因此需要来自 UI 的[手动操作](../jobs/job_control.md#create-a-job-that-must-be-run-manually) 来运行。
- `GIT_STRATEGY` 设置为 `none`。如果 `stop_review_app` 作业是[自动触发的](../environments/index.md#stopping-an-environment)，runner 不会在删除分支后尝试检查代码。

```yaml
review_app:
  stage: deploy
  script: make deploy-app
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://$CI_ENVIRONMENT_SLUG.example.com
    on_stop: stop_review_app

stop_review_app:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  script: make delete-app
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
```

<a id="stop-an-environment-after-a-certain-time-period"></a>

#### 在特定时间段后停止环境

您可以将环境设置为在特定时间段后自动停止。

NOTE:
由于资源限制，用于停止环境的后台 worker 每小时仅运行一次。这意味着环境可能不会在指定的确切时间段后停止，而是在后台 worker 检测到过期环境时停止。

在您的 `.gitlab-ci.yml` 文件中，指定 [`environment:auto_stop_in`](../yaml/index.md#environmentauto_stop_in) 关键字。您可以用自然语言指定时间段，例如 `1 hour and 30 minutes` 或 `1 day`。
时间段过去后，系统会自动触发作业以停止环境。

在以下示例：

- 合并请求的每次提交都会触发一个 `review_app` 作业，该作业将最新的更改部署到环境并重置其有效期。
- 如果环境处于非活动状态超过一周，极狐GitLab 会自动触发 `stop_review_app` 作业来停止环境。

```yaml
review_app:
  script: deploy-review-app
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    on_stop: stop_review_app
    auto_stop_in: 1 week
  rules:
    - if: $CI_MERGE_REQUEST_ID

stop_review_app:
  script: stop-review-app
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: manual
```

##### 查看环境的预定停止日期和时间

当某个环境已[计划在指定时间段后停止](#stop-an-environment-after-a-certain-time-period)时，您可以查看其到期日期和时间。

要查看环境的到期日期和时间：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **部署 > 环境**。
1. 选择环境名称。

到期日期和时间显示在左上角，环境名称旁边。

##### 覆盖环境的预定停止日期和时间

当某个环境已[计划在指定时间段后停止](#stop-an-environment-after-a-certain-time-period)时，您可以覆盖其过期时间。

要覆盖环境的到期时间：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **部署 > 环境**。
1. 选择部署名称。
1. 在右上角，选择图钉 (**{thumbtack}**)。

`auto_stop_in` 设置被覆盖，环境保持活动状态，直到它被手动停止。

#### 在不运行 `on_stop` 操作的情况下停止环境

有时您可能想在不运行定义的 [`on_stop`](../yaml/index.md#environmenton_stop) 操作的情况下停止环境。例如，您想在不使用 CI/CD 分钟数的情况下删除许多环境。

要在不运行定义的 `on_stop` 操作的情况下停止环境，请使用参数 `force=true` 执行[停止环境 API](../../api/environments.md#stop-an-environment)。

#### 通过 UI 停止环境

NOTE:
要触发 `on_stop` 操作并从环境视图手动停止环境，停止和部署作业必须位于相同的 [`resource_group`](../yaml/index.md#resource_group) 中。

要在 UI 中停止环境：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **部署 > 环境**。
1. 在您要停止的环境旁边，选择 **停止**。
1. 在确认对话框中，选择 **停止环境**。

#### 环境的多个停止操作

> - 引入于 14.10 版本，带有名为 `environment_multiple_stop_actions` 的功能标志。默认禁用。
> - 一般可用于 15.0 版本。功能标志 `environment_multiple_stop_actions` 已删除。

要在环境上配置多个**并行**停止操作，请在多个[部署作业](../jobs/index.md#deployment-jobs)相同的 `environment` 上指定 [`on_stop`](../yaml/index.md#environmenton_stop)，如 `.gitlab-ci.yml` 文件中的定义。

当环境停止时，只有成功部署作业的匹配 `on_stop` 操作才会并行运行，没有特定的顺序。

在以下示例中，对于 `test` 环境，有两个部署作业：

- `deploy-to-cloud-a`
- `deploy-to-cloud-b`

当环境停止时，系统会并行运行 `on_stop` 操作 `teardown-cloud-a` 和 `teardown-cloud-b`。

```yaml
deploy-to-cloud-a:
  script: echo "Deploy to cloud a"
  environment:
    name: test
    on_stop: teardown-cloud-a

deploy-to-cloud-b:
  script: echo "Deploy to cloud b"
  environment:
    name: test
    on_stop: teardown-cloud-b

teardown-cloud-a:
  script: echo "Delete the resources in cloud a"
  environment:
    name: test
    action: stop
  when: manual

teardown-cloud-b:
  script: echo "Delete the resources in cloud b"
  environment:
    name: test
    action: stop
  when: manual
```

<a id="delete-an-environment"></a>

### 删除环境

当您想要删除环境及其所有部署时，请删除该环境。

先决条件：

- 您必须至少具有开发者角色。
- 您必须先[停止](#stopping-an-environment)环境才能将其删除。

要在 UI 中删除环境：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 环境**。
1. 选择 **已停止** 选项卡。
1. 在要删除的环境旁边，选择 **删除环境**。
1. 在确认对话框中，选择 **删除环境**。

### 访问环境用于准备或验证的环境

> 引入于 13.2 版本

您可以定义访问环境用于各种目的的作业，例如验证或准备，可以有效地绕过部署创建，以便您可以更准确地调整 CD 工作流程。

为此，请将 `action: prepare`、`action: verify` 或 `action: access` 添加到作业的 `environment` 部分：

```yaml
build:
  stage: build
  script:
    - echo "Building the app"
  environment:
    name: staging
    action: prepare
    url: https://staging.example.com
```

这使您可以访问[环境范围变量](#使用-specs-设置环境范围)，并可用于保护构建免受未经授权的访问<!--[保护构建免受未经授权的访问](protected_environments.md)-->。您可以访问环境范围的变量，并可用于保护构建免受未经授权的访问。此外，有效避免[阻止过期的部署作业](deployment_safety.md#prevent-outdated-deployment-jobs)功能。

### 分组相似的环境

您可以将环境分组到 UI 中的可折叠部分。

例如，如果您的所有环境都以名称 `review` 开头，那么在 UI 中，环境将分组在该标题下：

![Environment groups](img/environments_dynamic_groups_v13_10.png)

下面的例子展示了如何用 `review` 开始您的环境名称。
`$CI_COMMIT_REF_SLUG` 变量在运行时用分支名称填充：

```yaml
deploy_review:
  stage: deploy
  script:
    - echo "Deploy a review app"
  environment:
    name: review/$CI_COMMIT_REF_SLUG
```

### 环境事件管理

生产环境可能会意外停机，包括由于您无法控制的原因。例如，外部依赖、基础设施或人为错误的问题可能会导致环境出现重大问题。例如：

- 依赖的云服务出现故障。
- 更新了第三方库，它与您的应用程序不兼容。
- 有人对您服务器中的易受攻击的端点进行 DDoS 攻击。
- 运营商错误配置基础设施。
- 在生产应用程序代码中引入了一个错误。

您可以使用事件管理<!--[事件管理](../../operations/incident_management/index.md)-->在出现需要立即关注的关键问题时获取警报。

#### 查看环境的最新警报 **(ULTIMATE)**

> 引入于 13.4 版本

如果您为 Prometheus 指标设置警报<!--[为 Prometheus 指标设置警报](../../operations/metrics/alerts.md)-->，环境警报将显示在环境页面上。显示具有最高严重性的警报，因此您可以确定哪些环境需要立即关注。

![Environment alert](img/alert_for_environment.png)

当触发警报的问题得到解决后，它会被删除并且在环境页面上不再可见。

如果警报需要[回滚](#重试或回滚部署)，您可以从环境页面中选择部署选项卡，然后选择要回滚到的部署。

#### 自动回滚 **(ULTIMATE)**

> 引入于 13.7 版本

在典型的持续部署工作流中，CI 流水线在部署到生产之前测试每个提交。但是，有问题的代码仍然可以投入生产。例如，逻辑上正确的低效代码可以通过测试，即使它会导致严重的性能下降。
运营商和 SRE 监控系统尽快发现这些问题。如果他们发现有问题的部署，他们可以回滚到以前的稳定版本。

系统自动回滚通过在检测到严重警报<!--[严重警报](../../operations/incident_management/alerts.md)-->时，自动触发回滚来简化此工作流程。系统选择并重新部署最近成功的部署。

系统自动回滚的限制：

- 如果检测到警报时部署正在运行，则会跳过回滚。
- 回滚只能在三分钟内发生一次。如果一次检测到多个警报，则只执行一次回滚。

系统自动回滚默认是关闭的。要开启：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **自动部署回滚**。
1. 选中 **启用自动回滚** 复选框。
1. 选择 **保存修改**。

<!--
### 监控环境

To monitor the behavior of your app as it runs in each environment,
enable [Prometheus for monitoring system and response metrics](../../user/project/integrations/prometheus.md).
For the monitoring dashboard to appear, configure Prometheus to collect at least one
[supported metric](../../user/project/integrations/prometheus_library/index.md).

All deployments to an environment are shown on the monitoring dashboard.
You can view changes in performance for each version of your application.

GitLab attempts to retrieve [supported performance metrics](../../user/project/integrations/prometheus_library/index.md)
for any environment that has had a successful deployment. If monitoring data was
successfully retrieved, a **Monitoring** button appears for each environment.

To view the last eight hours of performance data, select the **Monitoring** button.
It may take a minute or two for data to appear after initial deployment.

![Monitoring dashboard](../img/environments_monitoring.png)

#### Embed metrics in GitLab Flavored Markdown

Metric charts can be embedded in GitLab Flavored Markdown. See [Embedding Metrics in GitLab Flavored Markdown](../../operations/metrics/embed.md) for more details.

### Web 终端（已废弃）

> [Deprecated](https://gitlab.com/groups/gitlab-org/configure/-/epics/8) in GitLab 14.5.

WARNING:
This feature was [deprecated](https://gitlab.com/groups/gitlab-org/configure/-/epics/8) in GitLab 14.5.

If you deploy to your environments with the help of a deployment service (for example,
the [Kubernetes integration](../../user/infrastructure/clusters/index.md)), GitLab can open
a terminal session to your environment. You can then debug issues without leaving your web browser.

The Web terminal is a container-based deployment, which often lack basic tools (like an editor),
and can be stopped or restarted at any time. If this happens, you lose all your
changes. Treat the Web terminal as a debugging tool, not a comprehensive online IDE.

Web terminals:

- Are available to project Maintainers and Owners only.
- Must [be enabled](../../administration/integration/terminal.md).

In the UI, you can view the Web terminal by selecting **Terminal** from the actions menu:

![Terminal button on environment index](img/environments_terminal_button_on_index_v14_3.png)

You can also access the terminal button from the page for a specific environment:

![Terminal button for an environment](img/environments_terminal_button_on_show_v13_10.png)

Select the button to establish the terminal session:

![Terminal page](../img/environments_terminal_page.png)

This works like any other terminal. You're in the container created
by your deployment so you can:

- Run shell commands and get responses in real time.
- Check the logs.
- Try out configuration or code tweaks.

You can open multiple terminals to the same environment. They each get their own shell
session and even a multiplexer like `screen` or `tmux`.
-->

### 在本地检出部署

每次部署都会在 Git 仓库中保存一个引用，因此只需通过 `git fetch` 即可了解当前环境的状态。

在您的 Git 配置中，将 `[remote "<your-remote>"]` 块附加一个额外的提取行：

```plaintext
fetch = +refs/environments/*:refs/remotes/origin/environments/*
```

### 存档旧部署

> 引入于 14.5 版本。
>  适用于自助管理版于 14.6 版本。
<!--
> - [Generally available](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/73628) in GitLab 14.0. [Feature flag `deployments_archive`](https://gitlab.com/gitlab-org/gitlab/-/issues/345027) removed.
-->

当您的项目中发生新部署时，系统会创建[一个特殊的 Git-ref 部署](#在本地检出部署)。
由于这些 Git-refs 是从远端 GitLab 仓库填充的，您会发现一些 Git 操作，例如 `git-fetch` 和 `git-pull`，随着项目中部署数量的增加而变慢。

为了保持 Git 操作的效率，系统仅保留最近的部署 refs（最多 50,000 个）并删除其余的旧部署 refs。
存档部署在 UI 中或通过使用 API 仍然可用，用于审计目的。
此外，您仍然可以通过指定提交 SHA（例如，`git checkout <deployment-sha>`）从仓库中获取部署的提交，即使在存档之后也是如此。

NOTE:
系统将所有提交保留为 [`keep-around` refs](../../user/project/repository/reducing_the_repo_size_using_git.md) 以便部署的提交不会被垃圾收集，即使它没有被部署 refs 引用。

### 限制 CI/CD 变量的环境范围

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/2112) in GitLab Premium 9.4.
> - Environment scoping for CI/CD variables was [moved](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/30779) from GitLab Premium to GitLab Free in 12.2.
-->

> - 群组 CI/CD 变量的环境范围设置添加到专业版于 13.11 版本。

默认情况下，所有 [CI/CD 变量](../variables/index.md)都可用于流水线中的任何作业。因此，如果项目在测试作业中使用受损工具，它可能会暴露部署作业使用的所有 CI/CD 变量。这是供应链攻击中的常见场景。极狐GitLab 通过限制变量的环境范围来帮助缓解供应链攻击。

您可以通过定义可用于哪些环境来限制 CI/CD 变量的环境范围。
例如，如果环境范围是 `production`，那么只有定义了环境 `production` 的作业才会有这个特定的变量。

默认环境范围是通配符 (`*`)，这意味着任何作业都可以具有此变量，无论是否定义了环境。

如果环境范围是 `review/*`，那么对于环境名称以 `review/` 开头的作业，该变量可用。

在大多数情况下，这些功能使用 *environment specs* 机制，它提供了一种在每个环境组中实现范围界定的有效方法。

例如，如果有四个环境：

- `production`
- `staging`
- `review/feature-1`
- `review/feature-2`

每个环境都可以与以下 environment spec 匹配：

| Environment Spec | `production` | `staging` | `review/feature-1` | `review/feature-2` |
|:-----------------|:-------------|:----------|:-------------------|:-------------------|
| *                | 匹配      | 匹配   | 匹配             | 匹配            |
| production       | 匹配      |           |                    |                    |
| staging          |              | 匹配    |                    |                    |
| review/*         |              |           | 匹配             | 匹配           |
| review/feature-1 |              |           | 匹配             |                    |

您可以使用特定匹配来选择特定环境。
您还可以使用通配符匹配 (`*`) 来选择特定的环境组，例如 Review Apps<!--[Review Apps](../review_apps/index.md) (`review/*`)-->。

最具体的 specs 优先于其他通配符匹配。在这种情况下，`review/feature-1` spec 优先于 `review/*` 和 `*` spec。

<!--
## Related topics

- [Use GitLab CI to deploy to multiple environments (blog post)](https://about.gitlab.com/blog/2021/02/05/ci-deployment-and-environments/)
- [Review Apps](../review_apps/index.md): Use dynamic environments to deploy your code for every branch.
- [Deploy boards](../../user/project/deploy_boards.md): View the status of your applications running on Kubernetes.
- [Protected environments](protected_environments.md): Determine who can deploy code to your environments.
- [Environments Dashboard](../environments/environments_dashboard.md): View a summary of each
  environment's operational health. **(PREMIUM)**
- [Deployment safety](deployment_safety.md#restrict-write-access-to-a-critical-environment): Secure your deployments.
-->

## 故障排查

### 带有 `action:stop` 的作业没有运行

在某些情况下，[分支删除时](#删除分支时停止环境)环境不会停止。

例如，环境可能从一个有失败的作业的阶段开始。
然后后期阶段的作业不会开始。如果环境中带有 `action: stop` 的作业也处于后期阶段，则它无法启动并且环境不会被删除。

为确保 `action: stop` 可以在需要时始终运行，您可以：

- 将两个作业放在同一阶段：

  ```yaml
  stages:
    - build
    - test
    - deploy

  ...

  deploy_review:
    stage: deploy
    environment:
      name: review/$CI_COMMIT_REF_SLUG
      url: https://$CI_ENVIRONMENT_SLUG.example.com
      on_stop: stop_review

  stop_review:
    stage: deploy
    environment:
      name: review/$CI_COMMIT_REF_SLUG
      action: stop
    when: manual
  ```

- 向 `action: stop` 作业添加 [`needs`](../yaml/index.md#needs) 条目，以便作业可以不按阶段顺序启动：

  ```yaml
  stages:
    - build
    - test
    - deploy
    - cleanup

  ...

  deploy_review:
    stage: deploy
    environment:
      name: review/$CI_COMMIT_REF_SLUG
      url: https://$CI_ENVIRONMENT_SLUG.example.com
      on_stop: stop_review

  stop_review:
    stage: cleanup
    needs:
      - deploy_review
    environment:
      name: review/$CI_COMMIT_REF_SLUG
      action: stop
    when: manual
  ```

### 部署作业失败，并显示 "This job could not be executed because it would create an environment with an invalid parameter" 错误

> 引入于 14.4 版本。

如果您的项目配置为[创建动态环境](#创建动态环境)，您可能会遇到此错误，因为动态生成的参数不能用于创建环境。

例如，您的项目具有以下 `.gitlab-ci.yml`：

```yaml
deploy:
  script: echo
  environment: production/$ENVIRONMENT
```

由于流水线中不存在 `$ENVIRONMENT` 变量，系统尝试创建一个名为 `production/` 的环境，这在[环境名称约束](../yaml/index.md)中是无效的。

要解决此问题，请使用以下解决方案之一：

- 从部署作业中删除 `environment` 关键字。系统已经忽略了 invalid 关键字，因此即使在删除关键字后，您的部署流水线也保持完整。
- 确保变量存在于流水线中。请注意，支持的变量有限制<!--[支持的变量有限制](../variables/where_variables_can_be_used.md#gitlab-ciyml-file)-->。

#### 如果您在 Review Apps 上收到此错误

例如，如果您的 `.gitlab-ci.yml` 中有以下内容：

```yaml
review:
  script: deploy review app
  environment: review/$CI_COMMIT_REF_NAME
```

当创建一个分支名称为 `bug-fix!` 的新合并请求时，`review` 作业会尝试创建一个带有 `review/bug-fix!` 的环境。
但是，`!` 是环境的无效字符，因此部署作业将失败，因为它即将在没有环境的情况下运行。

要解决此问题，请使用以下解决方案之一：

- 重新创建没有无效字符的功能分支，例如 `bug-fix`。
- 将 `CI_COMMIT_REF_NAME` <!--[预定义变量](../variables/predefined_variables.md)-->预定义变量替换为去掉任何无效字符的 `CI_COMMIT_REF_SLUG`：

  ```yaml
  review:
    script: deploy review app
    environment: review/$CI_COMMIT_REF_SLUG
  ```

### 未找到部署 refs

从 14.5 版本开始，系统[删除旧的部署 refs](#存档旧部署) 以保持您的 Git 仓库的性能。

如果您必须恢复存档的 Git-refs，请让自助管理实例的管理员在 Rails 控制台上执行以下命令：

```ruby
Project.find_by_full_path(<your-project-full-path>).deployments.where(archived: true).each(&:create_ref)
```

请注意，将来可能会出于性能问题放弃此支持。
<!--
您可以在 GitLab 问题跟踪器中打开一个问题来讨论此功能的行为。
-->
