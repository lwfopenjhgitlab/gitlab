---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 环境看板 **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/3713) in GitLab 12.5.
-->

环境看板提供了一个跨项目的基于环境的视图，让您可以看到每个环境中正在发生的事情的大图。从这个地方，您可以跟踪从开发到 staging，然后到生产（或通过您可以设置的任何系列自定义环境流）的更改流程的进度。
通过对多个项目的概览，您可以立即看到哪些流水线是绿色的，哪些是红色的，从而可以诊断特定点是否存在阻塞，或者是否存在需要调查的系统性问题。

您可以通过选择 **主菜单 > 环境** 访问顶部栏上的仪表板。

![Environments Dashboard with projects](img/environments_dashboard_v12_5.png)

环境仪表板显示项目的分页列表，每个项目最多包含三个环境。

每个项目列出的环境都是独一无二的，例如 "production" 和 "staging"。不显示 Review apps 和其他分组环境。

## 将项目添加到仪表板

要将项目添加到仪表板：

1. 单击仪表板主屏幕中的 **添加项目** 按钮。
1. 使用 **搜索您的项目** 字段搜索并添加一个或多个项目。
1. 单击 **添加项目** 按钮。

添加后，您可以查看每个项目的环境运行状况摘要，包括最新提交、流水线状态和部署时间。

环境和运维<!--[运维](../../user/operations_dashboard/index.md)-->仪表板共享相同的项目列表。当您从仪表板中添加或删除项目时，系统会从另一个仪表板中添加或删除该项目。
 
您最多可以添加 150 个项目在仪表板上显示。

<!--
## Environment dashboards on GitLab.com

GitLab.com users can add public projects to the Environments
Dashboard for free. If your project is private, the group it belongs
to must have a [GitLab Premium](https://about.gitlab.com/pricing/) plan.
-->