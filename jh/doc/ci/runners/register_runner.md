---
stage: Verify
group: Runner
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 生成 runner 令牌 **(FREE)**

要注册 runner，您可以使用：

- 在 UI 中创建 runner 时分配给 runner 的身份验证令牌。当从作业队列中选取作业时，runner 使用令牌向极狐GitLab 进行身份验证。
- 注册令牌（已废弃）。

## 生成身份验证令牌

所有 runner 都可以使用身份验证令牌进行注册。

NOTE:
令牌仅在注册期间在 UI 中显示一小段时间。

### 共享 runner

> - 引入于 15.10 版本。功能标志为 `create_runner_workflow_for_admin`。
> - 默认启用于 16.0 版本。

FLAG:
在私有化部署版上，此功能默认可用。要隐藏此功能，需要管理员[禁用功能标志](../../administration/feature_flags.md) `create_runner_workflow_for_admin`。

先决条件：

- 您必须是管理员。

为共享 runner 生成身份验证令牌：

1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **CI/CD > Runners**。
1. 选择 **新建实例 runner**。
1. 选择一个平台。
1. 可选。输入 runner 的配置。
1. 选择 **提交**。
1. 按照说明使用命令行注册 runner。

### 群组 runner

> - 引入于 15.10 版本。功能标志为 `create_runner_workflow_for_namespace`。
> - 默认启用于 16.0 版本。

FLAG:
在私有化部署版上，此功能默认可用。要隐藏此功能，需要管理员[禁用功能标志](../../administration/feature_flags.md) `create_runner_workflow_for_namespace`。

先决条件：

- 您必须是群组所有者。

为群组 runner 生成身份验证令牌：

1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **CI/CD > Runners**。
1. 选择 **新建群组 runner**。
1. 选择一个平台。
1. 可选。输入 runner 的配置。
1. 选择 **提交**。
1. 按照说明使用命令行注册 runner。

### 项目 runner

> - 引入于 15.10 版本。功能标志为 `create_runner_workflow_for_namespace`。
> - 默认启用于 16.0 版本。

FLAG:
在私有化部署版上，此功能默认可用。要隐藏此功能，需要管理员[禁用功能标志](../../administration/feature_flags.md) `create_runner_workflow_for_namespace`。

先决条件：

- 您必须是项目的维护者。

为项目 runner 生成身份验证令牌：

1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **CI/CD > Runners**。
1. 选择 **新建项目 runner**。
1. 选择一个平台。
1. 可选。输入 runner 的配置。
1. 选择 **提交**。
1. 按照说明使用命令行注册 runner。

## 生成注册令牌（已废弃）

WARNING:
传递 runner 注册令牌的功能以及对某些配置参数的支持废弃于 15.6 版本。您应该使用身份验证令牌来注册 runner，注册令牌和对某些配置参数的支持将在 16.6 版本，引入功能标志来禁用，并在 17.0 版本中删除。`glrt-` 标记禁用的配置参数是 `--locked`、`--access-level`、`--run-untagged`、`--maximum-timeout`、`--paused`、`-- tag-list` 和 `--maintenance-note`。

### 共享 runner

1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **CI/CD > Runners**。
1. 选择 **注册实例 runner**。
1. 复制注册令牌。

### 群组 runner

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **CI/CD > Runners**。
1. 复制注册令牌。

### 项目 runner

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > CI/CD**。
1. 展开 **Runners** 部分。
1. 复制注册令牌。
