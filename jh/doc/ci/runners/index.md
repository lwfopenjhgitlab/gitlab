---
stage: Verify
group: Runner
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# Runner SaaS **(FREE SAAS)**

如果您使用的是私有化部署版，或者使用 SaaS 但想使用自己的 runner，您可以安装和配置自己的 runner<!--[安装和配置自己的运行器](https://docs.gitlab.com/runner/install/)-->。

如果您使用 SaaS，您的 CI 作业会自动在极狐GitLab 提供的 runner 上运行。无需配置。<!--Your jobs can run on:-->

<!--
- [Linux runners](saas/linux_saas_runner.md).
- [Windows runners](saas/windows_saas_runner.md) ([Beta](../../policy/alpha-beta-support.md#beta-features)).
- [macOS runners](saas/macos_saas_runner.md) ([Beta](../../policy/alpha-beta-support.md#beta-features)).
-->

您可以在 runner 上使用的分钟数，取决于您的[订阅计划](https://about.gitlab.cn/pricing/)。
