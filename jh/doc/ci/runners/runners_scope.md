---
stage: Verify
group: Runner
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# Runner 范围 **(FREE)**

Runner 可根据您想要访问的人提供：

- [共享 runners](#shared-runners)，可用于极狐GitLab 实例中的所有群组和项目。
- [群组 runners](#group-runners)，可用于群组中的所有项目和子组。
- [项目 runners](#project-runners) 与特定项目相关联。通常，项目 runner 只用于一个项目。

<a id="shared-runners"></a>

## 共享 runners

*共享 runners* 可用于极狐GitLab 实例中的每一个项目。

当您有多个具有相似要求的作业时，请使用共享 runner。与其让多个 runner 闲置在许多项目中，不如让几个 runner 处理多个项目。

如果您使用的是极狐GitLab 的私有化部署实例：

- 您的管理员可以通过转到项目的 **设置 > CI/CD**，展开 **Runners** 部分，然后单击 **显示 runner 安装和注册说明** 来安装和注册共享 runner。<!--These instructions are also available [in the documentation](https://docs.gitlab.com/runner/install/index.html).-->
- 管理员还可以配置提供给共享 runner 的[每个群组的 CI/CD 分钟数](../pipelines/cicd_minutes.md#set-the-quota-of-cicd-minutes-for-a-specific-namespace) 的最大数量。

如果您在使用 SaaS：

- 您可以从极狐GitLab 维护的共享 runner 列表中进行选择。
- 共享 runner 消耗您帐户中包含的 [CI/CD 分钟数](../pipelines/cicd_minutes.md)。

<a id="enable-shared-runners-for-a-project"></a>

### 为项目启用共享 runners

在 SaaS 上，默认情况下在所有项目中启用共享 runners。

在极狐GitLab 的私有化部署实例上，管理员可以[为所有新的项目启用它们](../../user/admin_area/settings/continuous_integration.md#enable-shared-runners-for-new-projects)。

对于现有项目，管理员必须安装并注册它们。

要为项目启用共享 runner：

1. 转到项目的 **设置 > CI/CD** 并展开 **Runners** 部分。
1. 选择 **为该项目启用共享 Runner**。

<a id="enable-shared-runners-for-a-group"></a>

### 为群组启用共享 runners

要为群组启用共享 runners：

1. 转到组的 **设置 > CI/CD** 并展开 **Runners** 部分。
1. 选择 **为该组启用共享 Runner**。

### 为项目禁用共享 runners

您可以为单个项目或群组禁用共享 runner。
您必须具有项目或群组的所有者角色。

要禁用项目的共享 runner：

1. 转到项目的 **设置 > CI/CD** 并展开 **Runners** 部分。
1. 在 **共享 runner** 区域中，选择 **为该项目启用共享 Runner**，使切换显示为灰色。

项目的共享 runner 在以下情况会自动禁用：

- 如果父组的共享 runner 设置被禁用，并且
- 如果在项目级别不允许覆盖此设置。

### 为群组禁用共享 runners

要为群组禁用共享 runner：

1. 转到组的 **设置 > CI/CD** 并展开 **Runners** 部分。
1. 在 **共享 runner** 区域，关闭 **为该群组启用共享 Runner** 开关。
1. （可选）要允许为单个项目或子组启用共享 runner，请单击 **允许项目和子群组覆盖群组设置**。

NOTE:
如果在禁用后重新启用群组的共享 runner，则具有所有者或维护者角色的用户必须手动在每个子组或项目更改此设置。

### 共享 runner 如何选择作业

共享 runner 通过使用公平使用队列来处理作业。此队列可防止项目创建数百个作业并使用所有可用的共享 runner 资源。

公平使用队列算法根据已在共享 runner 上运行的作业数量最少的项目分配作业。

例如，以下作业在队列中：

- Job 1 for Project 1
- Job 2 for Project 1
- Job 3 for Project 1
- Job 4 for Project 2
- Job 5 for Project 2
- Job 6 for Project 3

当几个 CI/CD 作业同步运行，公平使用算法按以下顺序分配作业：

1. Job 1 排在第一位，因为它在没有运行作业的项目（即所有项目）中具有最低的作业编号。
1. 接下来是 Job 4，因为 4 现在是没有运行作业的项目中的最低作业编号（项目 1 有作业正在运行）。
1. 接下来是 Job 6，因为 6 现在是没有运行作业的项目中的最低作业编号（项目 1 和 2 有作业正在运行）。
1. 接下来是 Job 2，因为在运行作业数量最少的项目中（每个项目都有 1），它是作业编号最少的项目。
1. 接下来是 Job 5，因为项目 1 现在有 2 个作业正在运行，而 Job 5 是项目 2 和 3 之间剩余的最低作业编号。
1. 最后是 Job 3，因为它是唯一剩下的作业。

当每次运行一个作业，公平使用算法按以下顺序分配作业：

1. 首先选择 Job 1，因为它在没有运行作业的项目（即所有项目）中具有最低的作业编号。
1. 我们完成 Job 1。
1. 接下来是 Job 2，因为在完成 Job 1 后，所有项目都有 0 个作业再次运行，而 2 是可用的最低作业编号。
1. 接下来是 Job 4，因为在项目 1 运行作业的情况下，4 是未运行作业的项目（项目 2 和 3）中的最低数字。
1. 我们完成 Job 4。
1. 接下来是 Job 5，因为完成了 Job 4，项目 2 没有再次运行作业。
1. 接下来是 Job 6，因为项目 3 是唯一没有运行作业的项目。
1. 最后我们选择 Job 3，它是唯一剩下的作业。

<a id="group-runners"></a>

## 群组 runner

当您希望群组中的所有项目都可以访问一组 runner时，请使用*群组 runner*。

群组 runner 使用先进先出 (FIFO) 队列来处理作业。

### 创建群组 runner

> 引入于 14.10 版本，路径从 **设置 > CI/CD > Runners** 变更而来。


您可以为私有化部署版极狐 GitLab 实例或 SaaS 创建群组 runner。
您必须具有该群组的所有者角色。

要创建群组 runner：

1. 安装 GitLab Runner。
1. 转到您想让 runner 工作的群组。
1. 在左侧边栏上，选择 **CI/CD > Runners**。
1. 记下 URL 和令牌。
1. 注册 Runner。

### 查看并管理群组 runners

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/37366/) in GitLab 13.2.
-->

您可以查看和管理一个群组、其子组和项目的所有 runner。
您可以为自己管理的极狐GitLab 实例或 SaaS 执行此操作。
您必须具有该群组的所有者角色。

1. 进入你要查看 runner 的群组。
1. 在左侧边栏上，选择 **CI/CD > Runners**。
1. 显示以下字段。

   | 属性    | 描述 |
   | ------------ | ----------- |
   | 类型         | 显示 runner 类型：`group` 或 `specific`，以及可选状态 `paused` |
   | Runner 令牌 | 用于识别 runner 的令牌，以及 runner 用来与极狐GitLab 实例进行通信的令牌 |
   | 描述  | 创建 runner 时给予的描述 |
   | 版本      | GitLab Runner 版本 |
   | IP 地址   | runner 注册的主机 IP 地址 |
   | 项目     | 分配给 runner 的项目数 |
   | 作业         | Runner 运行的作业总数 |
   | 标签         | 与 runner 相关的标签 |
   | 最后联系 | 指示极狐GitLab 实例上次联系 runner 的时间戳 |

在此页面中，您可以从群组、其子组和项目中编辑、暂停和删除 runner。

### 暂停或移除群组 runner

您可以为私有化部署版实例或 SaaS 暂停或移除群组 runner。
您必须具有该群组的所有者角色。

1. 转到您要删除或暂停 runner 的群组。
1. 在左侧边栏上，选择 **CI/CD > Runners**。
1. 点击 **暂停** 或 **移除 Runner**。
    - 如果您暂停多个项目使用的群组 runner，runner 会暂停所有项目。
    - 从群组视图中，您不能移除分配给多个项目的 runner。您必须先将其从每个项目中删除。
1. 在确认对话框中，单击 **确定**。

<a id="project-runners"></a>

## 项目 runner

当您想将 runner 用于特定项目时，请使用*项目 runner*。 例如，当您有：

- 具有特定要求的作业，例如需要凭据的部署作业。
- 具有大量 CI 活动的项目可以从与其它 runner 分开中受益。

您可以设置一个项目 Runner 供多个项目使用。必须为每个项目明确启用项目 Runner。

项目 Runner 使用先进先出 (FIFO) 队列来处理作业。

NOTE:
项目 Runner 不会自动与派生项目共享。
派生复制了克隆仓库的 CI/CD 设置。

### 创建项目 runner

您可以为私有化部署版实例或 SaaS 创建项目 runner。

先决条件：

- 您必须至少具有项目的维护者角色。

要创建项目 runner：

1. 安装极狐GitLab Runner。
1. 在顶部栏，选择 **主菜单 > 项目** 并找到您要使用 runner 的项目。
1. 在左侧边栏，选择 **设置 > CI/CD**。
1. 展开 **Runner**。
1. 在 **项目 Runner** 部分，记下 URL 和令牌。
1. [注册 runner](https://docs.gitlab.cn/runner/register/)。

现在为项目启用了 runner。

### 为另一个项目启用项目 runner

创建项目 runner 后，您可以为其他项目启用它。

先决条件：

- 您必须在已启用 runner 的项目中，至少具有维护者角色。
- 您必须在要启用 runner 的项目中，至少具有维护者角色。
- 项目 runner 不得[锁定](#prevent-a-specific-runner-from-being-enabled-for-other-projects)。

要为项目启用项目 runner：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到您要启用 runner 的项目。
1. 在左侧边栏，选择 **设置 > CI/CD**。
1. 展开 **Runners**。
1. 在 **项目 runner** 区域，通过您想要的 runner，选择 **为此项目启用**。

您可以从启用它的任何项目中编辑项目 runner。
包括解锁、编辑标签和描述在内的修改会影响所有使用 runner 的项目。

管理员可以[为多个项目启用 runner](../../user/admin_area/settings/continuous_integration.md#enable-a-specific-runner-for-multiple-projects)。

<a id="prevent-a-project-runner-from-being-enabled-for-other-projects"></a>

### 防止为其它项目启用项目 runner

您可以配置指定 runner，使其锁定，并且无法为其它项目启用。
首次注册 runner 时可以启用此设置，但也可以稍后更改。

锁定或解锁指定 runner：

1. 转到项目的 **设置 > CI/CD**。
1. 展开 **Runners** 部分。
1. 找到您要锁定或解锁的项目 runner，确保它已启用。您不能锁定共享或群组 runner 。
1. 选择 **编辑** (**{pencil}**)。
1. 选中 **锁定到当前项目** 选项。
1. 选择 **保存更改**。
