---
stage: Verify
group: Runner
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 配置 runners **(FREE)**

如果您已经安装了自己的 runner，则可以在极狐GitLab 中配置和保护它们。

<!--
If you need to configure runners on the machine where you installed GitLab Runner, see
[the GitLab Runner documentation](https://docs.gitlab.com/runner/configuration/).
-->

## 手动清除 runner 缓存

阅读[清除缓存](../caching/index.md#清除缓存)。

<a id="set-maximum-job-timeout-for-a-runner"></a>

## 设置 runner 的最大作业超时

对于每个 runner，您可以指定*最大作业超时*。如果此超时小于[项目定义的超时](../pipelines/settings.md#set-a-limit-for-how-long-jobs-can-run)，则此超时设置优先。

此功能可用于防止您的共享 runner 被具有长时间超时（例如，一周）的作业的项目压倒。

<!--
On GitLab.com, you cannot override the job timeout for shared runners and must use the [project defined timeout](../pipelines/settings.md#set-a-limit-for-how-long-jobs-can-run).
-->

要设置最大作业超时：

1. 在项目中，转到 **设置 > CI/CD > Runners**。
1. 选择您的项目 runner，来编辑设置。
1. 在 **最大作业超时** 下输入一个值。 必须是 10 分钟或更长时间。如果未定义，则使用[项目的作业超时设置](../pipelines/settings.md#set-a-limit-for-how-long-jobs-can-run)。
1. 选择 **保存修改**。

此功能的工作原理：

**示例 1 - Runner 超时大于项目超时**

1. 您将 runner 的 *最大作业超时* 设置为 24 小时
1. 您将项目的 *CI/CD 超时* 设置为 **2 小时**
1. 启动作业
1. 如果运行时间较长，作业会在 **2 小时后超时**

**示例 2 - 未配置 runner 超时**

1. 您从 runner 中删除 *最大作业超时* 配置
1. 您将项目的 *CI/CD 超时* 设置为 **2 小时**
1. 启动作业
1. 如果运行时间较长，作业会在 **2 小时后超时**

**示例 3 - Runner 超时小于项目超时**

1. 您将 runner 的 *最大作业超时* 设置为 **30 分钟**
1. 您将项目的 *CI/CD 超时* 设置为 **2 小时**
1. 启动作业
1. 如果运行时间较长，作业会在 **30 分钟后超时**

## 小心敏感信息

对于某些 runner executors，如果您可以在 runner 上运行作业，您可以获得对文件系统的完全访问权限，因此可以访问它运行的任何代码，以及 runner 的令牌。使用共享 runner，这意味着在 runner 上运行作业的其他用户都可以访问在 runner 上运行的任何其他人的代码。

此外，由于您可以访问 runner 令牌，因此可以创建 runner 的克隆并提交虚假作业。

通过在大型公开实例上限制共享 runner 的使用、控制对实例的访问以及使用更安全的 runner executors，可以轻松避免上述情况。

<a id="prevent-runners-from-revealing-sensitive-information"></a>

### 防止 runners 泄露敏感信息

您可以保护 runner，使他们不会泄露敏感信息。
当 runner 受到保护时，runner 只会选择在[受保护的分支](../../user/project/protected_branches.md)或[受保护的标签](../../user/project/protected_tags.md)上创建的作业，忽略其它作业。

保护或取消保护 runner：

1. 转到项目的 **设置 > CI/CD** 并展开 **Runners** 部分。
1. 找到您想保护或取消保护的 runner。确保它已启用。
1. 点击铅笔按钮。
1. 勾选 **受保护** 选项。
1. 单击 **保存修改**。

![specific runners edit icon](img/protected_runners_check_box_v14_1.png)

<a id="forks"></a>

### 派生

每当一个项目被派生时，它都会复制与其相关的作业的设置。这意味着，如果您为项目设置了共享 runner 并且有人派生了该项目，则共享 runner 将为该项目的作业提供服务。

<!--
### Attack vectors in runners

Mentioned briefly earlier, but the following things of runners can be exploited.
We're always looking for contributions that can mitigate these
[Security Considerations](https://docs.gitlab.com/runner/security/).
-->

### 重置项目的 runner 注册令牌

如果您认为某个项目的注册令牌被泄露，您应该重置它。注册令牌可用于为项目注册另一个 runner。
然后可以使用该新 runner 来获取 secret 变量的值或克隆项目代码。

要重置注册令牌：

1. 进入项目的 **设置 > CI/CD**。
1. 展开 **General pipelines settings** 部分。
1. 找到 **Runner 令牌** 表单字段，然后单击 **显示值** 按钮。
1. 删除值并保存表格。
1. 页面刷新后，展开 **Runners settings** 部分并检查注册令牌 - 应该已经更改。

从现在开始，旧令牌不再有效，并且不会向项目注册任何新的 runner。如果您使用任何工具来配置和注册新的 runner，则应更新这些工具中使用的令牌来反映新令牌的值。

<!--
### Reset the runner authentication token

If you think that an authentication token for a runner was revealed, you should
reset it. An attacker could use the token to [clone a runner](https://docs.gitlab.com/runner/security/#cloning-a-runner).

To reset the authentication token, [unregister the runner](https://docs.gitlab.com/runner/commands/#gitlab-runner-unregister)
and then [register](https://docs.gitlab.com/runner/commands/#gitlab-runner-register) it again.

To verify that the previous authentication token has been revoked, use the [Runners API](../../api/runners.md#verify-authentication-for-a-registered-runner).
-->

## 确定 runner 的 IP 地址

了解 runner 的 IP 地址可能很有用，这样您就可以解决该 runner 的问题。极狐GitLab 在轮询作业时通过查看它向极狐GitLab 发出的 HTTP 请求的来源来存储和显示 IP 地址。IP 地址始终保持最新，因此如果 runner IP 更改，会在极狐GitLab 中自动更新。

共享 runner 和项目 runner 的 IP 地址可以在不同的地方找到。

### 确定共享 runner 的 IP 地址

要查看共享 runner 的 IP 地址，您必须具有极狐GitLab 实例的管理员访问权限：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏，选择 **CI/CD > Runners**。
1. 找到表中的 runner，查看 **IP地址** 栏。

![shared runner IP address](img/shared_runner_ip_address_14_5.png)

### 确定项目 runner 的 IP 地址

要查找特定项目的 runner 的 IP 地址，您必须具有该项目的 Owner 角色。

1. 转到项目的 **设置 > CI/CD** 并展开 **Runners** 部分。
1. 在详细信息页面上，您应该会看到 **IP 地址** 一行。

![specific runner IP address](img/project_runner_ip_address.png)

## 使用标签来控制 runner 可以运行哪些作业

您必须设置一个 runner，以便能够运行它在共享的项目上可能遇到的所有不同类型的作业。如果不使用标签，这对于大量项目来说将是有问题的。

GitLab CI/CD 标签与 Git 标签不同。GitLab CI/CD 标签与 runner 相关联。
Git 标签与提交相关联。

通过标记 runner 可以处理的作业类型，您可以确保共享 runner 将[仅运行他们配备运行的作业](../yaml/index.md#tags)。

例如，在极狐GitLab 中，如果 runner 包含运行 Rails 测试套件的适当依赖项，我们会使用 `rails` 标记 runner。

### 设置 runner 运行未标记的作业

当您注册一个 runner 时，它的默认行为是**只选择**[已标记的作业](../yaml/index.md#tags)。
要更改此设置，您必须具有项目的所有者角色。

要让 runner 选择未标记的工作：

1. 转到项目的 **设置 > CI/CD** 并展开 **Runners** 部分。
1. 找到您要选择未标记工作的 runner 并确保它已启用。
1. 单击铅笔按钮。
1. 选中 **运行未标记的作业** 选项。
1. 单击 **保存修改** 按钮使更改生效。

NOTE:
当不允许选择未标记的作业时，runner 标记列表不能为空。

下面是一些不同变化的示例场景。

### Runner 只运行有标签作业

以下示例说明了将 runner 设置为仅运行有标签作业的潜在影响。

示例 1：

1. Runner 被配置为仅运行有标签的作业并具有 `docker` 标签。
1. 一个带有 `hello` 标签的作业被执行并卡住。

示例 2：

1. Runner 被配置为仅运行有标签的作业并具有 `docker` 标签。
1. 执行并运行具有 `docker` 标签的作业。

示例 3：

1. Runner 被配置为仅运行有标签的作业并具有 `docker` 标签。
1. 未定义标签的作业被执行并卡住。

### Runner 被允许运行未标记的作业

以下示例说明了将 runner 设置为运行有标签和未标记作业的潜在影响。

示例 1：

1. Runner 被配置为运行未标记的作业并具有 `docker` 标记。
1. 执行并运行未定义标签的作业。
1. 执行并运行定义了 `docker` 标签的第二个作业。

示例 2：

1. Runner 被配置为运行未标记的作业并且没有定义任何标记。
1. 执行并运行未定义标签的作业。
1. 定义了 `docker` 标签的第二个作业卡住了。

### runner 和作业有多个标签

匹配作业和 runner 的选择逻辑基于作业中定义的 `tags` 列表。

以下示例说明了 runner 和具有多个标签的作业的影响。对于要选择的 runner 来运行作业，它必须具有作业脚本块中定义的所有标签。

示例 1：

1. runner 配置了标签 `[docker, shell, gpu]`。
1. 拥有标签 `[docker, shell, gpu]` 的作业执行。

示例 2：

1. runner 配置了标签 `[docker, shell, gpu]`。
1. 拥有标签 `[docker, shell,]` 的作业执行。

示例 3：

1. runner 配置了标签 `[docker, shell]`。
1. 拥有标签 `[docker, shell, gpu]` 的作业没有被执行。

### 使用标签在不同平台上运行作业

您可以使用标签在不同的平台上运行不同的作业。例如，您有一个带有标签 `osx` 的 OS X runner 和一个带有标签 `windows` 的 Windows runner，您可以在每个平台上运行一个作业：

```yaml
windows job:
  stage: build
  tags:
    - windows
  script:
    - echo Hello, %USERNAME%!

osx job:
  stage: build
  tags:
    - osx
  script:
    - echo "Hello, $USER!"
```

### 在标签中使用 CI/CD 变量

> 引入于 14.1 版本

您可以使用带有 `tags` 的 [CI/CD 变量](../variables/index.md) 进行动态 runner 选择：

```yaml
variables:
  KUBERNETES_RUNNER: kubernetes

  job:
    tags:
      - docker
      - $KUBERNETES_RUNNER
    script:
      - echo "Hello runner selector feature"
```

## 使用变量配置 runner 行为

您可以使用 [CI/CD 变量](../variables/index.md) 全局或为单个作业配置 runner Git 行为：

- [`GIT_STRATEGY`](#git-strategy)
- [`GIT_SUBMODULE_STRATEGY`](#git-submodule-strategy)
- [`GIT_CHECKOUT`](#git-checkout)
- [`GIT_CLEAN_FLAGS`](#git-clean-flags)
- [`GIT_FETCH_EXTRA_FLAGS`](#git-fetch-extra-flags)
- [`GIT_SUBMODULE_PATHS`](#git-submodule-paths)
- [`GIT_SUBMODULE_UPDATE_FLAGS`](#git-submodule-update-flags)
- [`GIT_SUBMODULE_FORCE_HTTPS`](#rewrite-submodule-urls-to-https)
- [`GIT_DEPTH`](#shallow-cloning)（浅克隆）
- [`GIT_CLONE_PATH`](#custom-build-directories)（自定义构建目录）
- [`TRANSFER_METER_FREQUENCY`](#artifact-and-cache-settings)（产物/缓存计量更新频率）
- [`ARTIFACT_COMPRESSION_LEVEL`](#artifact-and-cache-settings)（产物归档工具压缩级别）
- [`CACHE_COMPRESSION_LEVEL`](#artifact-and-cache-settings)（缓存归档工具压缩级别）
- [`CACHE_REQUEST_TIMEOUT`](#artifact-and-cache-settings)（缓存请求超时）

您还可以使用变量来配置 runner [尝试某些作业执行阶段](#job-stages-attempts)的次数。

### Git strategy

<!--
> - Introduced in GitLab 8.9 as an experimental feature.
> - `GIT_STRATEGY=none` requires GitLab Runner v1.7+.
-->

您可以在全局或每个作业 [`variables`](../yaml/index.md#variables) 部分中设置用于获取仓库内容的 `GIT_STRATEGY`：

```yaml
variables:
  GIT_STRATEGY: clone
```

有三个可能的值：`clone`、`fetch` 和 `none`。如果未指定，作业将使用项目的流水线设置。

`clone` 是最慢的选项。它为每个作业从头开始克隆仓库，确保本地工作副本始终是原始的。
如果找到现有工作树，则在克隆之前将其删除。

`fetch` 更快，因为它重新使用本地工作副本（如果它不存在则回退到 `clone`）。`git clean` 用于撤消上一个作业所做的任何更改，而 `git fetch` 用于检索上一个作业运行后所做的提交。

然而，`fetch` 确实需要访问前一个工作树。这在使用 `shell` 或 `docker` executor 时效果很好，因为它们会尝试保留工作树并尝试在默认情况下重用它们。

<!--
This has limitations when using the [Docker Machine executor](https://docs.gitlab.com/runner/executors/docker_machine.html).
-->

`none` 的 Git 策略也会重用本地工作副本，但会跳过通常由极狐GitLab 完成的所有 Git 操作。GitLab Runner 预克隆脚本也会被跳过（如果存在）。这种策略可能意味着您需要将 `fetch` 和 `checkout` 命令添加到[您的 `.gitlab-ci.yml` 脚本](../yaml/index.md#script)。

它可用于专门对产物进行操作的作业，例如部署作业。Git 仓库数据可能存在，但可能已过时。您应该只依赖从缓存或产物带入本地工作副本的文件。

### Git submodule strategy

<!--
> Requires GitLab Runner v1.10+.
-->

`GIT_SUBMODULE_STRATEGY` 变量用于控制在构建之前获取代码时是否/如何包含 Git 子模块。您可以在 [`variables`](../yaml/index.md#variables) 部分中全局或按作业设置它们。

有三个可能的值：`none`、`normal` 和 `recursive`：

- `none` 表示在获取项目代码时不包含子模块。这是默认值，与 v1.10 之前的行为相匹配。

- `normal` 表示只包含顶级子模块。相当于：

  ```shell
  git submodule sync
  git submodule update --init
  ```

- `recursive` 表示包含所有子模块（包括子模块的子模块）。此功能需要 Git v1.8.1 及更高版本。将 GitLab Runner 与不基于 Docker 的 executor 一起使用时，请确保 Git 版本满足该要求。相当于：

  ```shell
  git submodule sync --recursive
  git submodule update --init --recursive
  ```

要使此功能正常工作，必须使用以下任一方式配置子模块（在 `.gitmodules` 中）：

- 可公开访问的仓库的 HTTP(S) URL，或
- 同一 GitLab 服务器上另一个仓库的相对路径。 请参阅 [Git 子模块](../git_submodules.md) 文档。

您可以使用 [`GIT_SUBMODULE_UPDATE_FLAGS`](#git-submodule-update-flags) 提供额外的标志来控制高级行为。

### Git checkout

当 `GIT_STRATEGY` 设置为 `clone` 或 `fetch` 来指定是否应该运行 `git checkout` 时，可以使用 `GIT_CHECKOUT` 变量。如果未指定，则默认为 true。您可以在 [`variables`](../yaml/index.md#variables) 部分中全局或按作业设置它们。

如果设置为 `false`，则 runner：

- 在执行 `fetch` 时 - 更新仓库并将工作副本保留在当前修订版上，
- 在执行 `clone` 时 - 克隆仓库并将工作副本保留在默认分支上。

如果 `GIT_CHECKOUT` 设置为 `true`，则 `clone` 和 `fetch` 的工作方式相同。
Runner 检查与 CI 流水线相关的修订的工作副本：

```yaml
variables:
  GIT_STRATEGY: clone
  GIT_CHECKOUT: "false"
script:
  - git checkout -B master origin/master
  - git merge $CI_COMMIT_SHA
```

### Git clean flags

`GIT_CLEAN_FLAGS` 变量用于在检查源代码后控制 `git clean` 的默认行为。您可以在 [`variables`](../yaml/index.md#variables) 部分中全局或按作业设置它。

`GIT_CLEAN_FLAGS` 接受 [`git clean`](https://git-scm.com/docs/git-clean) 命令的所有可能选项。

如果指定了 `GIT_CHECKOUT: "false"`，则禁用 `git clean`。

如果 `GIT_CLEAN_FLAGS`：

- 未指定，`git clean` 标志默认为 `-ffdx`。
- 给定值 `none`，不执行 `git clean`。

例如：

```yaml
variables:
  GIT_CLEAN_FLAGS: -ffdx -e cache/
script:
  - ls -al cache/
```

### Git fetch extra flags

> 引入于 GitLab Runner 13.1。

使用 `GIT_FETCH_EXTRA_FLAGS` 变量来控制 `git fetch` 的行为。您可以在 [`variables`](../yaml/index.md#variables) 部分中全局或按作业设置它。

`GIT_FETCH_EXTRA_FLAGS` 接受 [`git fetch`](https://git-scm.com/docs/git-fetch) 命令的所有选项。 但是，`GIT_FETCH_EXTRA_FLAGS` 标志附加在无法修改的默认标志之后。

默认标志是：

- [`GIT_DEPTH`](#shallow-cloning)。
- [refspecs](https://git-scm.com/book/en/v2/Git-Internals-The-Refspec) 列表。
- 一个名为 `origin` 的远端。

如果 `GIT_FETCH_EXTRA_FLAGS`：

- 未指定，`git fetch` 标志默认为 `--prune --quiet` 以及默认标志。
- 给定值 `none`，`git fetch` 仅使用默认标志执行。

例如，默认标志是 `--prune --quiet`，因此您可以通过仅使用 `--prune` 覆盖它来使 `git fetch` 更详细：

```yaml
variables:
  GIT_FETCH_EXTRA_FLAGS: --prune
script:
  - ls -al cache/
```

上面的配置导致 `git fetch` 被这样调用：

```shell
git fetch origin $REFSPECS --depth 50  --prune
```

其中 `$REFSPECS` 是极狐GitLab 在内部提供给 runner 的值。

### 从 CI 作业中同步或排除特定 submodules

使用 `GIT_SUBMODULE_PATHS` 变量来控制必须同步或更新哪些子模块。
您可以在 [`variables`](../yaml/index.md#variables) 部分中全局或按作业设置它。

路径语法同 [`git submodule`](https://git-scm.com/docs/git-submodule#Documentation/git-submodule.txt-ltpathgt82308203)：

- 同步和更新特定路径：

   ```yaml
   variables:
      GIT_SUBMODULE_PATHS: submoduleA submoduleB
   ```

- 要排除特定路径：

   ```yaml
   variables:
      GIT_SUBMODULE_PATHS: :(exclude)submoduleA :(exclude)submoduleB
   ```

WARNING:
Git 忽略嵌套路径。要忽略嵌套 submodule，请排除 submodule，然后在作业脚本中手动克隆它。例如，`git clone <repo> --recurse-submodules=':(exclude)nested-submodule'`。确保将字符串用单引号引起来，以便可以成功解析 YAML。

### Git submodule 更新标志

> 引入于 GitLab Runner 14.8 版本

当 [`GIT_SUBMODULE_STRATEGY`](#git-submodule-strategy) 设置为 `normal` 或 `recursive` 时，使用`GIT_SUBMODULE_UPDATE_FLAGS` 变量来控制 `git submodule update` 的行为。您可以在 [`variables`](../yaml/index.md#variables) 部分中全局或按作业设置它。

`GIT_SUBMODULE_UPDATE_FLAGS` 接受 [`git submodule update`](https://git-scm.com/docs/git-submodule#Documentation/git-submodule.txt-update--init--remote-N--no-fetch--no-recommend-shallow-f--force--checkout--rebase--merge--referenceltrepositorygt--depthltdepthgt--recursive--jobsltngt--no-single-branch--ltpathgt82308203) 的所有选项子命令。但是，请注意 `GIT_SUBMODULE_UPDATE_FLAGS` 标志附加在几个默认标志之后：

- `--init`，如果 [`GIT_SUBMODULE_STRATEGY`](#git-submodule-strategy) 设置为 `normal` 或 `recursive`。
- `--recursive`，如果 [`GIT_SUBMODULE_STRATEGY`](#git-submodule-strategy) 设置为 `recursive`。
- [`GIT_DEPTH`](#shallow-cloning)。请参阅下面的默认值。

Git 尊重参数列表中最后一次出现的标志，因此在 `GIT_SUBMODULE_UPDATE_FLAGS` 中手动提供它们也将覆盖这些默认标志。

您可以使用此变量在仓库中获取最新的远端 `HEAD` 而不是跟踪的提交，或者通过在多个并行作业中获取子模块来加速检出：

```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_SUBMODULE_UPDATE_FLAGS: --remote --jobs 4
script:
  - ls -al .git/modules/
```

上面的配置导致 `git submodule update` 被这样调用：

```shell
git submodule update --init --depth 50 --recursive --remote --jobs 4
```

WARNING:
使用 `--remote` 标志时，您应该注意对构建的安全性、稳定性和可重复性的影响。在大多数情况下，最好按照设计显式跟踪子模块提交，并使用自动修复/依赖机器人更新它们。

<a id="rewrite-submodule-urls-to-https"></a>

### 将子模块 URL 重写为 HTTPS

> 引入于极狐GitLab Runner 15.11 版本。

使用 `GIT_SUBMODULE_FORCE_HTTPS` 变量强制将所有 Git 和 SSH 子模块 URL 重写为 HTTPS，系统允许您在使用绝对 URL 的同一极狐GitLab 实例上克隆子模块，即使它们是使用 Git 或 SSH 协议配置的。

```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_SUBMODULE_FORCE_HTTPS: "true"
```

启用后，极狐GitLab Runner 使用 [CI/CD 作业令牌](../jobs/ci_job_token.md)来执行作业的用户权限克隆子模块，并且不需要 SSH 凭据。

### Shallow cloning

> 实验功能

您可以使用 `GIT_DEPTH` 指定获取和克隆的深度。`GIT_DEPTH` 对仓库进行浅层克隆，可以显著加快克隆速度。
它对于具有大量提交或旧的大型二进制文件的仓库很有帮助。此值被传递给 `git fetch` 和 `git clone`。

在 12.0 及更高版本中，新创建的项目自动具有 `git depth` 值 `50`。

如果您使用 `1` 的深度并且有一个作业队列或重试作业，作业可能会失败。

Git 获取和克隆基于 ref，例如分支名称，因此 runner 无法克隆特定的提交 SHA。如果队列中有多个作业，或者您正在重试旧作业，则要测试的提交必须在克隆的 Git 历史记录中。将 `GIT_DEPTH` 的值设置得太小会导致无法运行这些旧提交，并且在作业日志中会显示 `unresolved reference`。然后，您应该重新考虑将 `GIT_DEPTH` 更改为更高的值。

当设置了 `GIT_DEPTH` 时，依赖 `git describe` 的作业可能无法正常工作，因为只有部分 Git 历史存在。

仅获取或克隆最后 3 个提交：

```yaml
variables:
  GIT_DEPTH: "3"
```

您可以在 [`variables`](../yaml/index.md#variables) 部分中全局或按作业设置它。

### Custom build directories

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/2211) in GitLab Runner 11.10.
-->

默认情况下，GitLab Runner 将仓库克隆到 `$CI_BUILDS_DIR` 目录的唯一子路径中。但是，您的项目可能需要特定目录中的代码（例如 Go 项目）。 在这种情况下，您可以指定 `GIT_CLONE_PATH` 变量来告诉 runner 克隆仓库的目录：

```yaml
variables:
  GIT_CLONE_PATH: $CI_BUILDS_DIR/project-name

test:
  script:
    - pwd
```

`GIT_CLONE_PATH` 必须始终位于 `$CI_BUILDS_DIR` 内。`$CI_BUILDS_DIR` 中设置的目录取决于 executor 和 runners.builds_dir 设置的配置。

只能在 runner 配置中启用 `custom_build_dir` 时使用。

#### Handling concurrency

使用大于 `1` 的并发 executor 可能会导致失败。如果 `builds_dir` 在作业之间共享，则多个作业可能在同一个目录上工作。

Runner 不会试图阻止这种情况。是否符合 runner 配置的要求取决于管理员和开发人员。

为避免这种情况，您可以在 `$CI_BUILDS_DIR` 中使用唯一路径，因为 runner 公开了两个额外的变量，它们提供了唯一的并发 `ID`：

- `$CI_CONCURRENT_ID`：在特定 runner 中运行的所有作业的唯一 ID。
- `$CI_CONCURRENT_PROJECT_ID`：在特定 runner 和项目中运行的所有作业的唯一 ID。

在任何场景和任何 executor 上都能正常工作的最稳定配置是在 `GIT_CLONE_PATH` 中使用 `$CI_CONCURRENT_ID`。例如：

```yaml
variables:
  GIT_CLONE_PATH: $CI_BUILDS_DIR/$CI_CONCURRENT_ID/project-name

test:
  script:
    - pwd -P
```

`$CI_CONCURRENT_PROJECT_ID` 应与 `$CI_PROJECT_PATH` 一起使用，因为 `$CI_PROJECT_PATH` 提供了仓库的路径。 即 `group/subgroup/project`。例如：

```yaml
variables:
  GIT_CLONE_PATH: $CI_BUILDS_DIR/$CI_CONCURRENT_ID/$CI_PROJECT_PATH

test:
  script:
    - pwd -P
```

#### Nested paths

`GIT_CLONE_PATH` 的值被扩展一次，不支持嵌套变量。

例如，您在 `.gitlab-ci.yml` 文件中定义以下两个变量：

```yaml
variables:
  GOPATH: $CI_BUILDS_DIR/go
  GIT_CLONE_PATH: $GOPATH/src/namespace/project
```

`GIT_CLONE_PATH` 的值被展开一次到 `$CI_BUILDS_DIR/go/src/namespace/project`，由于 `$CI_BUILDS_DIR` 没有展开而导致失败。

### Job stages attempts

<!--
> Introduced in GitLab, it requires GitLab Runner v1.9+.
-->

您可以设置正在运行的作业尝试执行以下阶段的尝试次数：

| 变量                        | 描述                                            |
|---------------------------------|--------------------------------------------------------|
| `ARTIFACT_DOWNLOAD_ATTEMPTS`    | 尝试下载运行作业的产物的次数 |
| `EXECUTOR_JOB_SECTION_ATTEMPTS` | 在 12.10 及更高版本中，在 `No Such Container` 错误（仅限 Docker executor）后尝试运行作业中的部分的次数。|
| `GET_SOURCES_ATTEMPTS`          | 尝试获取运行作业的源的次数      |
| `RESTORE_CACHE_ATTEMPTS`        | 尝试恢复运行作业的缓存的次数  |

默认是一次尝试。

示例：

```yaml
variables:
  GET_SOURCES_ATTEMPTS: 3
```

您可以在 [`variables`](../yaml/index.md#variables) 部分中全局或按作业设置它们。

<!--
## System calls not available on GitLab.com shared runners

GitLab.com shared runners run on CoreOS. This means that you cannot use some system calls, like `getlogin`, from the C standard library.
-->

<a id="artifact-and-cache-settings"></a>

## 产物和缓存设置

> 引入于 GitLab Runner 13.9

产物和缓存设置控制产物和缓存的压缩率。
使用这些设置来指定作业生成的存档的大小。

- 在慢速网络上，对于较小的存档，上传速度可能会更快。
- 在不考虑带宽和存储的快速网络上，使用最快的压缩比上传可能会更快，尽管生成的存档更大。

<!--
For [GitLab Pages](../../user/project/pages/index.md) to serve
[HTTP Range requests](https://developer.mozilla.org/en-US/docs/Web/HTTP/Range_requests), artifacts
should use the `ARTIFACT_COMPRESSION_LEVEL: fastest` setting, as only uncompressed zip archives
support this feature.
-->

可以启用 meter 来提供上传和下载的传输速率。

您可以使用 `CACHE_REQUEST_TIMEOUT` 设置来设置缓存上传和下载的最长时间。当缓慢的缓存上传显著增加作业的持续时间时，此设置可能很有用。

```yaml
variables:
  # output upload and download progress every 2 seconds
  TRANSFER_METER_FREQUENCY: "2s"

  # Use fast compression for artifacts, resulting in larger archives
  ARTIFACT_COMPRESSION_LEVEL: "fast"

  # Use no compression for caches
  CACHE_COMPRESSION_LEVEL: "fastest"

  # Set maximum duration of cache upload and download
  CACHE_REQUEST_TIMEOUT: 5
```

| 变量                        | 描述                                            |
|---------------------------------|--------------------------------------------------------|
| `TRANSFER_METER_FREQUENCY`      | 指定打印 meter 传输率的频率，可以设置为持续时间（例如，`1s` 或 `1m30s`）。`0` 的持续时间表示禁用 meter（默认）。设置值后，流水线会显示产物和缓存上传和下载的进度。 |
| `ARTIFACT_COMPRESSION_LEVEL`    | 要调整压缩率，请设置为 `fastest`、`fast`、`default`、`slow` 或 `slowest`。此设置仅适用于 Fastzip archiver，因此还必须启用 GitLab Runner 功能标志 `FF_USE_FASTZIP`。 |
| `CACHE_COMPRESSION_LEVEL`       | 要调整压缩率，请设置为 `fastest`、`fast`、`default`、`slow` 或 `slowest`。此设置仅适用于 Fastzip archiver，因此还必须启用 GitLab Runner 功能标志 `FF_USE_FASTZIP`。 |
| `CACHE_REQUEST_TIMEOUT`         | 配置单个作业的缓存上传和下载操作的最大持续时间（以分钟为单位）。默认为 `10` 分钟。 |

## 产物证明

> 引入于极狐GitLab Runner 15.1 版本。

NOTE:
Zip 存档是唯一支持的产物类型。

极狐GitLab Runner 可以为所有构建产物生成和制作证明元数据。要启用此功能，您必须将 `RUNNER_GENERATE_ARTIFACTS_METADATA` 环境变量设置为 `true`。此变量可以全局设置，也可以为单个作业设置。元数据呈现在与产物一起存储的纯文本 `.json` 文件中。 文件名如下：`{ARTIFACT_NAME}-metadata.json`，其中 `ARTIFACT_NAME` 是 CI 文件中产物名称的定义。但是，如果没有为构建产物指定名称，则文件名默认为 `artifacts-metadata.json`。

### 证明格式

证明元数据以规范版本 [v0.1](https://github.com/in-toto/attestation/tree/v0.1.0/spec) 的 [in-toto 证明格式](https://github.com/in-toto/attestation)生成。默认填充以下字段：

| 字段  | 值  |
| ------ | ------ |
| `_type` | `https://in-toto.io/Statement/v0.1` |
| `subject.name` | 产物的文件名 |
| `subject.digest.sha256` | 产物的 `sha256` 校验和 |
| `predicateType` | `https://slsa.dev/provenance/v0.2` |
| `predicate.buildType` | `https://gitlab.com/gitlab-org/gitlab-runner/-/blob/{GITLAB_RUNNER_VERSION}/PROVENANCE.md`。例如 v15.0.0 |
| `predicate.builder.id` | 指向 runner 详细信息页面的 URI，例如 `https://gitlab.com/gitlab-com/www-gitlab-com/-/runners/3785264` |
| `predicate.invocation.configSource.uri` | ``https://gitlab.example.com/.../{PROJECT_NAME}`` |
| `predicate.invocation.configSource.digest.sha256` | 代码仓库的 `sha256` 校验和 |
| `predicate.invocation.configSource.entryPoint` | 触发构建的 CI 作业的名称 |
| `predicate.invocation.environment.name` | Runner 的名称 |
| `predicate.invocation.environment.executor` | Runner 执行器 |
| `predicate.invocation.environment.architecture` | 运行 CI 作业的架构 |
| `predicate.invocation.parameters` | 运行构建命令时存在的任何 CI/CD 或环境变量的名称。该值始终表示为空字符串，以避免泄露任何 secrets |
| `metadata.buildStartedOn` | 构建开始的时间。`RFC3339` 格式 |
| `metadata.buildEndedOn` | 构建结束的时间。由于元数据生成是在构建过程中发生的，因此这一时刻比极狐GitLab 中报告的时间稍早。`RFC3339` 格式 |
| `metadata.reproducible` | 通过收集所有生成的元数据，构建是否可重现。始终为 `false` |
| `metadata.completeness.parameters` | 是否提供参数。始终为 `true` |
| `metadata.completeness.environment` | 是否报告 builder 的环境。始终为 `true` |
| `metadata.completeness.materials` | 是否报告构建材料。始终为 `false` |

极狐GitLab Runner 可能生成的证明示例如下：

```yaml
{
    "_type": "https://gitlab.com/gitlab-org/gitlab-runner/-/blob/v15.1.0/PROVENANCE.md",
    "subject": [
        {
            "name": "script.sh",
            "digest": {
                "sha256": "f5ae5ced234922eebe6461d32228ba8ab9c3d0c0f3983a3bef707e6e1a1ab52a"
            }
        }
    ],
    "predicateType": "https://slsa.dev/provenance/v0.2",
    "predicate": {
        "buildType": "https://gitlab.com/gitlab-org/gitlab-runner/-/blob/v15.1.0/PROVENANCE.md",
        "builder": {
            "id": "https://gitlab.com/ggeorgiev_gitlab/playground/-/runners/14811533"
        },
        "invocation": {
            "configSource": {
                "uri": "https://gitlab.com/ggeorgiev_gitlab/playground",
                "digest": {
                    "sha256": "f0582e2c9a16b5cc2cde90e8be8f1b50fd67c631"
                },
                "entryPoint": "whoami shell"
            },
            "environment": {
                "name": "local",
                "executor": "shell",
                "architecture": "amd64"
            },
            "parameters": {
                "CI_PIPELINE_ID": "",
                "CI_PIPELINE_URL": "",
                // All other CI variable names are listed here. Values are always represented as empty strings to avoid leaking secrets.
            }
        },
        "metadata": {
            "buildStartedOn": "2022-06-17T00:47:27+03:00",
            "buildFinishedOn": "2022-06-17T00:47:28+03:00",
            "completeness": {
                "parameters": true,
                "environment": true,
                "materials": false
            },
            "reproducible": false
        },
        "materials": []
    }
}
```

### 暂存目录

> 引入于极狐GitLab Runner 15.0 版本。

如果您不想在系统的默认临时目录中归档缓存和产物，您可以指定一个不同的目录。

如果系统的默认临时路径有限制，您可能需要更改目录。
如果您使用快速磁盘作为目录位置，也可以提高性能。

要更改目录，请将 `ARCHIVER_STAGING_DIR` 设置为 CI 作业中的变量，或者在注册 runner 时使用 runner 变量（`gitlab register --env ARCHIVER_STAGING_DIR=<dir>`）。

您指定的目录用作在提取之前下载产物的位置。如果使用 `fastzip` 归档器，这个位置在归档时也用作暂存空间。

### 配置 `fastzip` 提高性能

> 引入于极狐GitLab Runner 15.0 版本。

要调整 `fastzip`，请确保启用 [`FF_USE_FASTZIP`](https://docs.gitlab.cn/runner/configuration/feature-flags.html#available-feature-flags) 功能标志。
然后使用以下任何环境变量。

| 变量                        | 描述                                         |
|---------------------------------|--------------------------------------------------------|
| `FASTZIP_ARCHIVER_CONCURRENCY`  | 要同时压缩的文件数。默认值是可用的 CPU 数。 |
| `FASTZIP_ARCHIVER_BUFFER_SIZE`  | 每个文件的每个并发分配的缓冲区大小。超过此数量的数据将移至暂存空间。默认值为 2 MiB。  |
| `FASTZIP_EXTRACTOR_CONCURRENCY` | 要并发解压缩的文件数。默认值是可用的 CPU 数。 |

zip 存档中的文件按顺序附加，这使得并发压缩具有挑战性。`fastzip` 通过先同时将文件压缩到磁盘，然后按顺序将结果复制回 zip 存档来解决此限制。

为了避免写入磁盘并读回较小文件的内容，每个并发使用一个小缓冲区。此设置可以通过 `FASTZIP_ARCHIVER_BUFFER_SIZE` 进行控制，此缓冲区的默认大小为 2 MiB，因此，16 的并发分配 32 MiB。超过缓冲区大小的数据将写入磁盘并从磁盘读回。
因此，不使用缓冲区，`FASTZIP_ARCHIVER_BUFFER_SIZE: 0`，并且只使用暂存空间是一个有效的选项。

`FASTZIP_ARCHIVER_CONCURRENCY` 控制有多少文件被并发压缩。如上所述，此设置因此可以增加正在使用的内存量，但也可以增加写入暂存空间的临时数据量。
默认值是可用 CPU 的数量，但考虑到内存影响，可能并不是最佳设置。

`FASTZIP_EXTRACTOR_CONCURRENCY` 控制一次解压多少文件。来自 zip 存档的文件可以从本地并发读取，因此除了解压缩器需要的内存之外，不会分配额外的内存，默认为可用的 CPU 数。

## 清理陈旧的 runners **(ULTIMATE)**

> 引入于 15.1 版本。

您可以清理超过三个月不活跃的群组 runners。

群组 runners 是在群组级别创建的。

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > CI/CD**。
1. 展开 **Runners**。
1. 打开 **启用陈旧的 runner 清理** 开关。

### 查看陈旧的 runner 清理日志

您可以查看 [Sidekiq 日志](../../administration/logs/index.md#sidekiq-logs)，查看清理结果。在 Kibana 中，您可以使用以下查询：

```json
{
  "query": {
    "match_phrase": {
      "json.class.keyword": "Ci::Runners::StaleGroupRunnersPruneCronWorker"
    }
  }
}
```

过滤陈旧的 runners 的条目：

```json
{
  "query": {
    "range": {
      "json.extra.ci_runners_stale_group_runners_prune_cron_worker.total_pruned": {
        "gte": 1,
        "lt": null
      }
    }
  }
}
```

<a id="determine-which-runners-need-to-be-upgraded"></a>

## 确定哪些 runners 需要升级 **(ULTIMATE)**

> 引入于 15.3 版本。

您的 runners 使用的极狐GitLab Runner 版本应该[保持最新](https://docs.gitlab.cn/runner/index.html#gitlab-runner-versions)。

要确定哪些 runners 需要升级：

1. 查看 runners 列表：
   - 对于一个群组，在顶部栏上，选择 **主菜单 > 群组**，找到您的群组，然后在左侧栏上选择 **CI/CD > Runners**。
   - 对于实例，选择 **主菜单 > 管理员** 并在左侧边栏中选择 **Runners**。

1. 在 runners 列表上方，查看状态：
   - **过时 - 推荐**：Runner 没有最新的 `PATCH` 版本，这可能使其容易受到安全或高严重性错误的影响。或者，Runner 比极狐GitLab 实例落后一个或多个 `MAJOR` 版本，因此某些功能可能不可用或无法正常工作。
   - **过时 - 可用**：有较新的版本可用，但升级并不重要。

1. 按状态过滤列表，查看需要升级的个人 runners。

## 查看 runner 表现的统计数据 **(ULTIMATE)**

> 引入于 15.8 版本。

作为管理员，您可以查看 runner 统计信息，了解 runner 队列的表现。

1. 选择 **主菜单 > 管理**。
1. 在左侧边栏中，选择 **CI/CD > Runners**。
1. 选择 **查看指标**。

**作业排队中位时间**值，是通过对实例 runner 运行的最近 100 个作业的队列持续时间进行采样来计算的。仅考虑来自最新的 5000 个 runner 的作业。

中位时间是属于第 50 个百分位数的值：一半的作业排队时间超过中位时间，一半的作业排队时间小于中位时间。

## 身份验证令牌安全

> - 引入于 15.3 版本，功能标志为 `enforce_runner_token_expires_at`。默认禁用。
> - 一般可用于 15.5 版本。功能标志 `enforce_runner_token_expires_at` 删除。

每个 runner 都有一个身份验证令牌，来连接极狐GitLab 实例。

为了帮助防止令牌被泄露，您可以让令牌以指定的时间间隔自动轮换。当令牌轮换时，它们会为每个 runner 更新，不论 runner 的状态（`online` 或  `offline`）如何。

不需要人工干预，也不会影响正在运行的作业。

如果您需要手动更新身份验证令牌，可以运行命令[重置令牌](https://docs.gitlab.cn/runner/commands/#gitlab-runner-reset-token)。

### 自动轮换身份验证令牌

您可以指定身份验证令牌轮换的时间间隔。
这种轮换有助于确保分配给您的 runner 的令牌的安全性。

先决条件：

- 确保您的 runners 使用极狐GitLab Runner 15.3 或更高版本。

自动轮换 runner 身份验证令牌：

1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > CI/CD**。
1. 展开 **持续集成和部署**。
1. 为 runner 设置一个 **runner 过期** 时间，留空表示不过期。
1. 选择 **保存**。

在间隔到期之前，runner 会自动请求一个新的身份验证令牌。
