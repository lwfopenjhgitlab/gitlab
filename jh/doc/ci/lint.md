---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 验证极狐GitLab CI/CD 配置 **(FREE)**

使用 CI Lint 工具检查极狐GitLab CI/CD 配置的有效性。
您可以从 `.gitlab-ci.yml` 文件或任何其它示例 CI/CD 配置中验证语法。
该工具检查语法和逻辑错误，并且可以模拟流水线创建，尝试发现更复杂的配置问题。

如果您使用[流水线编辑器](pipeline_editor/index.md)，它会自动验证配置语法。

<!--
如果您使用 VS Code，可以通过[极狐GitLab Workflow VS Code 扩展](../user/project/repository/vscode.md)，来验证您的 CI/CD 配置。
-->

<a id="check-cicd-syntax"></a>

## 检查 CI/CD 语法

CI lint 工具检查极狐GitLab CI/CD 配置的语法，包括使用 [`includes` 关键字](yaml/index.md#include)添加的配置。

使用 CI lint 工具检查 CI/CD 配置：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **CI/CD > 流水线**。
1. 在右上角，选择 **CI lint**。
1. 将要检查的 CI/CD 配置的副本粘贴到文本框中。
1. 选择 **验证**。

<a id="simulate-a-pipeline"></a>

## 模拟流水线

> 引入于 13.3 版本。

您可以模拟创建极狐GitLab CI/CD 流水线，查找更复杂的问题，包括 [`needs`](yaml/index.md#needs) 和 [`rules`](yaml/index.md#rules) 的配置问题。模拟在默认分支上作为 Git `push` 事件运行。

先决条件：

- 您必须拥有在此分支上创建流水线的权限，才能通过模拟进行验证。

模拟流水线：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **CI/CD > 流水线**。
1. 在右上角，选择 **CI lint**。
1. 将要检查的 CI/CD 配置的副本粘贴到文本框中。
1. 选择 **模拟默认分支的流水线创建**。
1. 选择 **验证**。
