---
stage: Plan
group: Certify
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
description: Test cases in GitLab can help your teams create testing scenarios in their existing development platform.
type: reference
---

# 测试用例 **(ULTIMATE)**

> - 引入于 13.6 版本。
> - 功能标志删除于 13.7 版本。
> - 极狐GitLab 自 15.4 版本开始，将该功能从旗舰版降入专业版。而 GitLab CE 与 EE 版本中，该功能依旧为旗舰版。
> - 极狐GitLab 自 15.8 版本开始，将该功能从专业版调整为旗舰版。

极狐GitLab 中的测试用例可以帮助您的团队在他们现有的开发平台中创建测试场景。

现在，您的实施和测试团队可以更好地协作，因为他们不再需要外部测试计划工具，避免额外开销、上下文切换和费用。

## 创建测试用例

先决条件：

- 您必须至少具有报告者角色。

在极狐GitLab 项目中创建测试用例：

1. 转到 **CI/CD > 测试用例**。
1. 选择 **新建测试用例** 按钮，将进入新建测试用例表单，可以输入新用例的标题、[描述](../../user/markdown.md)、附加文件，并分配[标记](../../user/project/labels.md)。
1. 选择 **提交测试用例** 按钮，可以查看新的测试用例。

<a id="view-a-test-case"></a>

## 查看测试用例

您可以在测试用例列表中查看项目中的所有测试用例。使用搜索查询过滤议题列表，包括标记或测试用例的标题。

先决条件：

是否可以查看测试用例取决于[项目可见性级别](../../user/public_access.md)：

- 公开项目：您不必成为该项目的成员。
- 私有项目：您必须至少拥有该项目的访客角色。

查看测试用例：

1. 在项目中，转到 **CI/CD > 测试用例**。
1. 选择要查看的测试用例的标题。您将被带到测试用例页面。

![An example test case page](img/test_case_show_v13_10.png)

## 编辑测试用例

您可以编辑测试用例的标题和描述。

先决条件：

- 您必须至少具有报告者角色。
- 降级为访客角色的用户，可以继续编辑他们具有更高角色时创建的测试用例。

编辑测试用例：

1. [查看测试用例](#view-a-test-case)。
1. 选择 **编辑标题和描述** (**{pencil}**)。
1. 编辑测试用例的标题或描述。
1. 选择 **保存更改**。

## 归档测试用例

当您想停止使用测试用例时，可以将其归档。您可以稍后[重新打开存档的测试用例](#reopen-an-archived-test-case)。

先决条件：

- 您必须至少具有报告者角色。

要存档测试用例，请在测试用例页面上，选择 **归档测试用例** 按钮。

查看存档的测试用例：

1. 转到 **CI/CD > 测试用例**。
1. 选择 **已存档**。

<a id="reopen-an-archived-test-case"></a>

## 重新打开存档的测试用例

如果您决定再次开始使用存档的测试用例，您可以重新打开它。

您必须至少具有报告者角色。

要重新打开已存档的测试用例，请在测试用例页面上选择 **重新打开测试用例**。
