---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
type: reference
---

# `.gitlab-ci.yml` 文件 **(FREE)**

要使用 GitLab CI/CD，您需要：

- 托管在 Git 仓库中的应用程序代码。
- 仓库根目录中名为 [`.gitlab-ci.yml`](index.md) 的文件，其中包含 CI/CD 配置。

在`.gitlab-ci.yml`文件中，您可以定义：

- 要运行的脚本。
- 要包含的其他配置文件和模板。
- 依赖项和缓存。
- 要按顺序运行的命令和要并行运行的命令。
- 将应用程序部署到的位置。
- 无论您是想自动运行脚本还是手动触发它们中的任何一个。

脚本被分组到 **作业** 中，并且作业作为更大的 **流水线** 的一部分运行。您可以将多个独立作业分组到按定义顺序运行的**阶段**。
CI/CD 配置至少需要一项非隐藏<!--[非隐藏](../jobs/index.md#hide-jobs)-->的作业。

您应该按照适合您的应用程序并根据您希望执行的测试的顺序来组织您的工作。要<!--[可视化](../pipeline_editor/index.md#visualize-ci-configuration)-->可视化过程，请想象您添加到作业的脚本与您在计算机上运行的 CLI 命令相同。

当您将 `.gitlab-ci.yml` 文件添加到仓库时，极狐GitLab 会检测到它，并且名为 <!--[GitLab Runner](https://docs.gitlab.com/runner/)-->GitLab Runner 的应用程序会运行作业中定义的脚本。

`.gitlab-ci.yml` 文件可能包含：

```yaml
stages:
  - build
  - test

build-code-job:
  stage: build
  script:
    - echo "Check the ruby version, then build some Ruby project files:"
    - ruby -v
    - rake

test-code-job1:
  stage: test
  script:
    - echo "If the files are built successfully, test some files with one command:"
    - rake test1

test-code-job2:
  stage: test
  script:
    - echo "If the files are built successfully, test other files with a different command:"
    - rake test2
```

在这个例子中，`build` 阶段的 `build-code-job` 作业首先运行。 它输出作业使用的 Ruby 版本，然后运行 `rake` 来构建项目文件。
如果此作业成功完成，则 `test` 阶段中的两个 `test-code-job` 作业将并行启动并对文件运行测试。

示例中的完整流水线由三个作业组成，分为两个阶段，`build` 和 `test`。 每次将更改推送到项目中的任何分支时，流水线都会运行。

GitLab CI/CD 不仅会执行作业，还会向您显示执行期间发生的情况，就像您在终端中看到的一样：

![job running](img/job_running_v13_10.png)

您为您的应用程序创建策略，极狐GitLab 根据您定义的内容运行流水线。 极狐GitLab 还会显示您的流水线状态：

![pipeline status](img/pipeline_status.png)

如果出现任何问题，您可以回滚<!--[回滚](../environments/index.md#retry-or-roll-back-a-deployment)-->更改：

![rollback button](img/rollback.png)

[查看 `.gitlab-ci.yml` 文件的完整语法](index.md)。
