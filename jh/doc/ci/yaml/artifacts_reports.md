---
stage: Verify
group: Pipeline Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab CI/CD 产物报告类型 **(FREE)**

使用 [`artifacts:reports`](index.md#artifactsreports)：

- 收集作业中包含的模板生成的测试报告、代码质量报告、安全报告和其它产物。
- 其中一些报告用于显示以下信息：
   - 合并请求。
   - 流水线视图。
   - [安全仪表盘](../../user/application_security/security_dashboard/index.md)。

无论作业结果如何（成功或失败），为 `artifacts: reports` 创建的产物总是被上传。
您可以使用 [`artifacts:expire_in`](index.md#artifactsexpire_in) 为其产物设置到期日期。

某些 `artifacts:reports` 类型可以由同一流水线中的多个作业生成，并由每个作业的合并请求或流水线功能使用。

为了能够浏览报告输出文件，请确保包含 [`artifacts:paths`](index.md#artifactspaths) 关键字。

NOTE:
不支持使用[子流水线中的产物](index.md#needspipelinejob)在父流水线中组合报告。<!--Track progress on adding support in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/215725).-->

## `artifacts:reports:accessibility`

`accessibility` 报告使用 [pa11y](https://pa11y.org/)，报告合并请求中引入的更改对可访问性的影响。

系统可以在合并请求的可访问性部件中，显示一个或多个报告的结果。

<!--
For more information, see [Accessibility testing](../../user/project/merge_requests/accessibility_testing.md).
-->

## `artifacts:reports:api_fuzzing` **(ULTIMATE)**

> - 引入于 13.4 版本
> - 需要极狐GitLab Runner 13.4 或更高版本

`api_fuzzing` 报告收集了 API Fuzzing bug<!--[API Fuzzing bug](../../user/application_security/api_fuzzing/index.md)--> 作为产物。

系统可以在以下位置显示一个或多个报告的结果：

- 合并请求安全小部件。
- [项目漏洞报告](../../user/application_security/vulnerability_report/index.md)。
- 流水线[**安全**选项卡](../../user/application_security/vulnerability_report/pipeline.md#view-vulnerabilities-in-a-pipeline)。
- 安全仪表盘。

## `artifacts:reports:browser_performance` **(PREMIUM)**

> 于 14.0  版本，名称从 `artifacts:reports:performance` 变更为当前名称

`browser_performance` 报告收集[浏览器性能测试指标](../testing/browser_performance_testing.md)作为产物。

系统可以在合并请求[浏览器性能测试小部件](../testing/browser_performance_testing.md#how-browser-performance-testing-works)中显示一份报告的结果。

系统无法显示多个 `browser_performance` 报告的组合结果。

## `artifacts:reports:coverage_report`

> 引入于 14.10 版本。

使用 `coverage_report` 收集 Cobertura 格式的覆盖率报告。

`cobertura` 报告收集 [Cobertura 覆盖率 XML 文件](../testing/test_coverage_visualization.md)。

Cobertura 最初是为 Java 开发的，但有许多第三方端口可用于其他语言，例如 JavaScript、Python 和 Ruby。

```yaml
artifacts:
  reports:
    coverage_report:
      coverage_format: cobertura
      path: coverage/cobertura-coverage.xml
```

收集的覆盖率报告作为产物上传到极狐GitLab。

系统可以在合并请求[差异注释](../testing/test_coverage_visualization.md)中显示覆盖率报告的结果。

## `artifacts:reports:codequality`

> 对差异注释和完整流水线报告中的多个报告的支持引入于 15.7 版本。

`codequality` 报告收集[代码质量问题](../testing/code_quality.md)。收集的代码质量报告作为产物上传到极狐GitLab。

系统可以显示一个或多个报告：

- 合并请求[代码质量部分](../testing/code_quality.md#merge-request-widget)。
- 合并请求[差异注释](../testing/code_quality.md#merge-request-changes-view)。
- [完整报告](../testing/metrics_reports.md)。

## `artifacts:reports:container_scanning` **(ULTIMATE)**

`container_scanning` 报告收集了[容器扫描漏洞](../../user/application_security/container_scanning/index.md)。
收集的容器扫描报告作为产物上传到极狐GitLab。

系统可以在以下位置显示一个或多个报告的结果：

- 合并请求[容器扫描小部件](../../user/application_security/container_scanning/index.md)。
- 流水线[**安全**选项卡](../../user/application_security/vulnerability_report/pipeline.md#view-vulnerabilities-in-a-pipeline)。
- [安全仪表盘](../../user/application_security/security_dashboard/index.md)。
- [项目漏洞报告](../../user/application_security/vulnerability_report/index.md)。

## `artifacts:reports:coverage_fuzzing` **(ULTIMATE)**

> - 引入于 13.4 版本。
> - 需要极狐GitLab Runner 13.4 或更高版本。

`coverage_fuzzing` 报告收集了 coverage fuzzing bug。
收集到的覆盖率模糊测试报告作为产物上传到极狐GitLab。
系统可以在以下位置显示一个或多个报告的结果：

- 合并请求的覆盖率模糊测试小部件。
- 流水线[**安全**选项卡](../../user/application_security/vulnerability_report/pipeline.md#view-vulnerabilities-in-a-pipeline)。
- [安全仪表盘](../../user/application_security/security_dashboard/index.md)。
- [项目漏洞报告](../../user/application_security/vulnerability_report/index.md)。

## `artifacts:reports:cyclonedx`

> 引入于 15.3 版本。

本报告是一份软件材料清单，描述了遵循 [CycloneDX](https://cyclonedx.org/docs/1.4) 协议格式的项目组件。

您可以为每个作业指定多个 CycloneDX 报告，可以作为文件名列表、文件名样式提供：

- 文件名样式（`cyclonedx: gl-sbom-*.json`、`junit: test-results/**/*.json`）。
- 文件名数组（`cyclonedx: [gl-sbom-npm-npm.cdx.json, gl-sbom-bundler-gem.cdx.json]`）。
- 两者的组合（`cyclonedx: [gl-sbom-*.json, my-cyclonedx.json]`）。
- 不支持目录（`cyclonedx: test-results`、`cyclonedx: test-results/**`）。

以下是公开 CycloneDX 产物的作业示例：

```yaml
artifacts:
  reports:
    cyclonedx:
      - gl-sbom-npm-npm.cdx.json
      - gl-sbom-bundler-gem.cdx.json
```

## `artifacts:reports:dast` **(ULTIMATE)**

`dast` 报告收集 DAST 漏洞<!--[DAST 漏洞](../../user/application_security/dast/index.md)-->。收集的 DAST 报告作为产物上传到极狐GitLab。

系统可以在以下位置显示一个或多个报告的结果：

- 合并请求安全小部件。
- 流水线[**安全**选项卡](../../user/application_security/vulnerability_report/pipeline.md#view-vulnerabilities-in-a-pipeline)。
- [安全仪表盘](../../user/application_security/security_dashboard/index.md)。
- [项目漏洞报告](../../user/application_security/vulnerability_report/index.md)。

## `artifacts:reports:dependency_scanning` **(ULTIMATE)**

dependency_scanning` 报告收集[依赖扫描漏洞](../../user/application_security/dependency_scanning/index.md)。
收集的依赖扫描报告作为产物上传到极狐GitLab。

系统可以在以下位置显示一个或多个报告的结果：

- 合并请求的[依赖项扫描小部件](../../user/application_security/dependency_scanning/index.md)。
- 流水线[**安全**选项卡](../../user/application_security/vulnerability_report/pipeline.md#view-vulnerabilities-in-a-pipeline)。
- [安全仪表盘](../../user/application_security/security_dashboard/index.md)。
- [项目漏洞报告](../../user/application_security/vulnerability_report/index.md)。
- 依赖项列表。

## `artifacts:reports:dotenv`

`dotenv` 报告收集一组环境变量作为产物。

收集的变量被注册为作业的运行时创建的变量，您可以使用它来[在作业完成后设置动态环境 URL](../environments/index.md#set-dynamic-environment-urls-after-a-finishes)。

如果 `dotenv` 报告中存在重复的环境变量：

- 在 14.6 及更高版本中，使用指定的最后一个。
- 在 14.5 及更早版本中，发生错误。

[原始 dotenv 规则](https://github.com/motdotla/dotenv#rules)的例外是：

- 变量键只能包含字母、数字和下划线 (`_`)。
- `.env` 文件的最大大小为 5 KB。此限制[可以在私有化部署版实例上更改](../../administration/instance_limits.md#limit-dotenv-file-size)。
- 在 SaaS 版上继承变量的最大数量：免费版为 50，专业版为 100，旗舰版为 150。在私有化部署版上的默认值为 150，可以通过更改 `dotenv_variables` [应用程序限制](../../administration/instance_limits.md#limit-dotenv-variables)来更改。
- 不支持 `.env` 文件中的变量替换。
- [`.env` 文件中的多行值](https://github.com/motdotla/dotenv#multiline-values)不受支持。
- `.env` 文件不能有空行或注释（以 `#` 开头）。
- `env` 文件中的键值不能包含空格或换行符 (`\n`)，包括使用单引号或双引号时。
- 不支持解析期间的引号转义 (`key = 'value'` -> `{key: "value"}`)。
- 仅[支持](../jobs/job_artifacts_troubleshooting.md#error-message-fatal-invalid-argument-when-uploading-a-dotenv-artifact-on-a-windows-runner) UTF-8 编码。

## `artifacts:reports:junit`

`junit` 报告收集 [JUnit 报告格式 XML 文件](https://www.ibm.com/docs/en/adfz/developer-for-zos/14.1.0?topic=formats-junit-xml-format)。
收集的单元测试报告作为产物上传到极狐GitLab。尽管 JUnit 最初是用 Java 开发的，但有许多第三方端口可用于其他语言，例如 JavaScript、Python 和 Ruby。

有关更多详细信息和示例，请参阅[单元测试报告](../testing/unit_test_reports.md)。
下面是从 Ruby 的 RSpec 测试工具收集 JUnit 报告格式 XML 文件的示例：

```yaml
rspec:
  stage: test
  script:
    - bundle install
    - rspec --format RspecJunitFormatter --out rspec.xml
  artifacts:
    reports:
      junit: rspec.xml
```

极狐GitLab 可以在以下位置显示一个或多个报告的结果：

- 合并请求[代码质量小部件](../testing/unit_test_reports.md#how-it-works)。
- [完整报告](../testing/unit_test_reports.md#view-unit-test-reports-on-gitlab)。

一些 JUnit 工具导出到多个 XML 文件。您可以在单个作业中指定多个测试报告路径，以将它们连接到单个文件中，使用以下方式之一：

- 文件名模式（`junit: rspec-*.xml`、`junit: test-results/**/*.xml`）。
- 文件名数组（`junit: [rspec-1.xml, rspec-2.xml, rspec-3.xml]`）。
- 两者的组合（`junit: [rspec.xml, test-results/TEST-*.xml]`）。
- 不支持目录（`junit: test-results`、`junit: test-results/**`）。

## `artifacts:reports:license_scanning` **(ULTIMATE)**

许可证合规性报告收集许可证<!--[许可证](../../user/compliance/license_compliance/index.md)-->。许可证合规性报告作为产物上传到极狐GitLab。

系统可以在以下位置显示一个或多个报告的结果：

- 合并请求许可证合规性小部件<!--[许可证合规性小部件](../../user/compliance/license_compliance/index.md)-->。
- 许可列表<!--[许可列表](../../user/compliance/license_compliance/index.md#license-list)-->。

## `artifacts:reports:load_performance` **(PREMIUM)**

> - 引入于 13.2 版本。
> - 需要极狐GitLab Runner 11.5 或更高版本。

`load_performance` 报告收集负载性能测试指标<!--[负载性能测试指标](../testing/load_performance_testing.md)-->。
该报告作为产物上传到极狐GitLab。

系统只能在合并请求负载测试小部件中显示一份报告的结果。

系统无法显示多个 `load_performance` 报告的组合结果。

## `artifacts:reports:metrics` **(PREMIUM)**

`metrics` 报告收集指标。收集的 Metrics 报告作为产物上传到极狐GitLab。

系统可以在合并请求指标报告小部件中显示一个或多个报告的结果。

## `artifacts:reports:requirements` **(ULTIMATE)**

`requirements` 报告收集 `requirements.json` 文件。收集的需求报告作为产物上传到极狐GitLab，现有的[需求](../../user/project/requirements/index.md)被标记为满意。

系统可以在[项目需求](../../user/project/requirements/index.md#view-a-requirement)中显示一份或多份报告的结果。

## `artifacts:reports:sast`

`sast` 报告收集 SAST 漏洞。收集的 SAST 报告作为产物上传到极狐GitLab。

系统可以在以下位置显示一个或多个报告的结果：

- 合并请求 SAST 小部件。
- [安全仪表盘](../../user/application_security/security_dashboard/index.md)。

## `artifacts:reports:secret_detection`

> - 引入于 13.1 版本。
> - 移动到免费版于 13.3 版本。
> - 需要极狐GitLab Runner 11.5 及更高版本，

`secret-detection` 报告收集[检测到的 secrets](../../user/application_security/secret_detection/index.md)。
收集到的 Secret Detection 报告上传到极狐GitLab。

系统可以在以下位置显示一个或多个报告的结果：

- 合并请求 [secret 扫描小部件](../../user/application_security/secret_detection/index.md)。
- 流水线**安全**选项卡。
- [安全仪表盘](../../user/application_security/security_dashboard/index.md)。

<!--
## `artifacts:reports:terraform`

> - 引入于 13.0 版本。
> - 需要 [GitLab Runner](https://docs.gitlab.com/runner/) 11.5 及更高版本。

The `terraform` report obtains a Terraform `tfplan.json` file. [JQ processing required to remove credentials](../../user/infrastructure/iac/mr_integration.md#configure-terraform-report-artifacts).
The collected Terraform plan report uploads to GitLab as an artifact.

GitLab can display the results of one or more reports in the merge request
[terraform widget](../../user/infrastructure/iac/mr_integration.md#output-terraform-plan-information-into-a-merge-request).

For more information, see [Output `terraform plan` information into a merge request](../../user/infrastructure/iac/mr_integration.md).
-->
