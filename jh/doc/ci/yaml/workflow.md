---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# GitLab CI/CD `workflow` 关键字 **(FREE)**

使用 [`workflow`](index.md#workflow) 关键字来控制何时创建流水线。

`workflow` 关键字在作业之前进行评估。例如，将作业配置为针对标签运行，但工作流阻止标签流水线，则该作业永远不会运行。

## `workflow:rules` 的常见 `if` 子句

`workflow: rules` 的一些示例 `if` 子句：

| 示例规则                                        | 详情                                                  |
|------------------------------------------------------|-----------------------------------------------------------|
| `if: '$CI_PIPELINE_SOURCE == "merge_request_event"'` | 控制合并请求流水线何时运行。                 |
| `if: '$CI_PIPELINE_SOURCE == "push"'`                | 控制分支流水线和标签流水线何时运行。 |
| `if: $CI_COMMIT_TAG`                                 | 控制标签流水线何时运行。                           |
| `if: $CI_COMMIT_BRANCH`                              | 控制分支流水线何时运行。                        |

查看 [`rules` 的常见 `if` 子句](../jobs/job_control.md#common-if-clauses-for-rules) 获取更多示例。

## `workflow: rules` 示例

在以下示例中：

- 为所有 `push` 事件运行流水线（更改分支和新标签）。
- 对于带有以 `-draft` 结尾的提交消息的推送事件，流水线不会运行，因为它们被设置为 `when: never`。
- 计划或合并请求的流水线也不会运行，因为没有任何规则对它们进行评估。

```yaml
workflow:
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /-draft$/
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"
```

此示例有严格的规则，流水线在任何其它情况下都**不**运行。

或者，所有规则都可以是 `when: never`，最后一个规则是 `when: always`。匹配 `when: never` 规则的流水线不会运行。
所有其它流水线类型都运行。例如：

```yaml
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"
      when: never
    - when: always
```

此示例阻止计划的流水线或 `push`（分支和标签）流水线。
最后的 `when: always` 规则运行所有其它流水线类型，**包括**合并请求流水线。

### 在分支流水线和合并请求流水线之间切换

> 引入于 13.8 版本

要在创建合并请求后，使流水线从分支流水线切换到合并请求流水线，请将 `workflow: rules` 部分添加到您的 `.gitlab-ci.yml` 文件中。

如果您同时使用两种流水线类型，[重复流水线](../jobs/job_control.md#avoid-duplicate-pipelines)可能会同时运行。为防止重复流水线，请使用 [`CI_OPEN_MERGE_REQUESTS` 变量](../variables/predefined_variables.md)。

以下示例适用于仅运行分支和合并请求流水线的项目，但不为任何其它情况运行流水线：

- 未为分支打开合并请求时的分支流水线。
- 当分支的合并请求打开时的合并请求流水线。

```yaml
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
```

如果极狐GitLab 尝试触发：

- 一个合并请求流水线，则启动流水线。例如，合并请求流水线可以通过推送到具有关联的开放合并请求的分支来触发。
- 分支流水线，但该分支的合并请求已打开，请勿运行分支流水线。例如，分支流水线可以由对分支的更改、API 调用、计划的流水线等触发。
- 分支流水线，但没有为分支打开合并请求，运行分支流水线。

您还可以将规则添加到现有的 `workflow` 部分，以便在创建合并请求时，从分支流水线切换到合并请求流水线。

将此规则添加到 `workflow` 部分的顶部，然后是已经存在的其它规则：

```yaml
workflow:
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never
    - ...                # Previously defined workflow rules here
```

在分支上运行的[触发流水线](../triggers/index.md) 设置了 `$CI_COMMIT_BRANCH`，并且可能被类似规则阻止。触发的流水线有一个 `trigger` 或 `pipeline` 的流水线源，所以 `&& $CI_PIPELINE_SOURCE == "push"` 确保规则不会阻止触发的流水线。

## 带有合并请求流水线的 Git Flow

您可以将 `workflow: rules` 作为 Git Flow 或类似策略的一部分，与合并请求流水线一起使用。使用这些规则，您可以将[合并请求流水线功能](../pipelines/merge_request_pipelines.md)与功能分支一起使用，同时保留长期存在的分支来支持您的软件的多个版本。

例如，只为您的合并请求、标签和受保护的分支运行流水线：

```yaml
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_REF_PROTECTED
```

此示例假定您的长期分支是[受保护的](../../user/project/protected_branches.md)。

## `workflow:rules` 模板

极狐GitLab 提供了为常见场景设置 `workflow: rules` 的模板。这些模板有助于防止重复的流水线。

[`Branch-Pipelines` 模板](https://jihulab.com/gitlab-cn/gitlab/-/tree/master/lib/gitlab/ci/templates/Workflows/Branch-Pipelines.gitlab-ci.yml)使您的流水线针对分支和标签运行。

分支流水线状态显示在使用分支作为源的合并请求中。但是，此流水线类型不支持[合并请求流水线](../pipelines/merge_request_pipelines.md) 提供的任何功能，例如[合并结果流水线](../pipelines/merged_results_pipelines.md)或[合并队列](../pipelines/merge_trains.md)。此模板有意避免这些功能。

要 [include](index.md#include) 此模板：

```yaml
include:
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'
```

[`MergeRequest-Pipelines` 模板](https://jihulab.com/gitlab-cn/gitlab/-/tree/master/lib/gitlab/ci/templates/Workflows/MergeRequest-Pipelines.gitlab-ci.yml) 使您的流水线针对默认分支、标签和所有类型的合并请求流水线运行。如果您使用任何[合并请求流水线功能](../pipelines/merge_request_pipelines.md)，请使用此模板。

要 [include](index.md#include) 此模板：

```yaml
include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
```

## 故障排除

### 合并请求阻塞，带有 `Checking pipeline status.` 消息

如果合并请求显示 `Checking pipeline status.`，但消息永远不会消失，则可能是由于 `workflow:rules`。
如果项目具有[**流水线必须成功**](../../user/project/merge_requests/merge_when_pipeline_succeeds.md#only-allow-merge-requests-to-be-merged-if-the-pipeline-succeeds)已启用，但 `workflow:rules` 阻止流水线为合并请求运行。

例如，使用此工作流，无法合并合并请求，因为没有流水线可以运行：

```yaml
workflow:
  rules:
    - changes:
      - .gitlab/**/**.md
      when: never
```
