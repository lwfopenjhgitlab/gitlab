---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 使用来自其他文件的 CI/CD 配置 **(FREE)**

您可以使用 [`include`](index.md#include) 在 CI/CD 作业中包含外部 YAML 文件。

## 包含单个配置文件

要包含单个配置文件，请使用以下任一语法选项：

- `include` 本身带有单个文件。如果是本地文件，则与 [`include:local`](index.md#includelocal) 相同。如果是一个远端文件，则与 [`include:remote`](index.md#includeremote) 相同。

  ```yaml
  include: '/templates/.after-script-template.yml'
  ```

## 包含一组配置文件

您可以包含一组配置文件：

- 如果不指定 `include` 类型，根据需要，每个数组项默认为 [`include:local`](index.md#includelocal) 或 [`include:remote`](index.md#includeremote)：

  ```yaml
  include:
    - 'https://gitlab.com/awesome-project/raw/main/.before-script-template.yml'
    - '/templates/.after-script-template.yml'
  ```

- 您可以定义单个项目数组：

  ```yaml
  include:
    - remote: 'https://gitlab.com/awesome-project/raw/main/.before-script-template.yml'
  ```

- 您可以定义一个数组并明确指定多个 `include` 类型：

  ```yaml
  include:
    - remote: 'https://gitlab.com/awesome-project/raw/main/.before-script-template.yml'
    - local: '/templates/.after-script-template.yml'
    - template: Auto-DevOps.gitlab-ci.yml
  ```

- 您可以定义一个结合默认和特定 `include` 类型的数组：

  ```yaml
  include:
    - 'https://gitlab.com/awesome-project/raw/main/.before-script-template.yml'
    - '/templates/.after-script-template.yml'
    - template: Auto-DevOps.gitlab-ci.yml
    - project: 'my-group/my-project'
      ref: main
      file: '/templates/.gitlab-ci-template.yml'
  ```

## 使用包含的配置文件中的 `default` 配置

您可以在配置文件中定义 [`default`](index.md#default) 部分。当您使用带有 `include` 关键字的 `default` 部分时，默认值适用于流水线中的所有作业。

例如，您可以使用带有 [`before_script`](index.md#before_script) 的 `default` 部分。

名为 `/templates/.before-script-template.yml` 的自定义配置文件的内容：

```yaml
default:
  before_script:
    - apt-get update -qq && apt-get install -y -qq sqlite3 libsqlite3-dev nodejs
    - gem install bundler --no-document
    - bundle install --jobs $(nproc)  "${FLAGS[@]}"
```

`.gitlab-ci.yml` 的内容：

```yaml
include: '/templates/.before-script-template.yml'

rspec1:
  script:
    - bundle exec rspec

rspec2:
  script:
    - bundle exec rspec
```

默认的 `before_script` 命令在 `script` 命令之前在两个 `rspec` 作业中执行。

<a id="override-included-configuration-values"></a>

## 覆盖包含的配置值

当您使用 `include` 关键字时，您可以覆盖包含的配置值以使其适应您的流水线要求。

以下示例显示了在 `.gitlab-ci.yml` 文件中自定义的 `include` 文件。 特定的 YAML 定义的变量和 `production` 作业的详细信息被覆盖。

名为 `autodevops-template.yml` 的自定义配置文件的内容：

```yaml
variables:
  POSTGRES_USER: user
  POSTGRES_PASSWORD: testing_password
  POSTGRES_DB: $CI_ENVIRONMENT_SLUG

production:
  stage: production
  script:
    - install_dependencies
    - deploy
  environment:
    name: production
    url: https://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

`.gitlab-ci.yml` 的内容：

```yaml
include: 'https://company.com/autodevops-template.yml'

image: alpine:latest

variables:
  POSTGRES_USER: root
  POSTGRES_PASSWORD: secure_password

stages:
  - build
  - test
  - production

production:
  environment:
    url: https://domain.com
```

`.gitlab-ci.yml` 文件中定义的 `POSTGRES_USER` 和 `POSTGRES_PASSWORD` 变量以及 `production` 作业的 `environment:url` 覆盖了 `autodevops-template.yml` 文件中定义的值。其他关键字不变。这种方法称为 *merging*。

### `include` 的合并方法

`include` 配置通过以下过程与主配置文件合并：

- 包含的文件按照配置文件中定义的顺序读取，包含的配置按照相同的顺序合并在一起。
- 如果包含的文件也使用了 `include`，则嵌套的 `include` 配置首先（递归）合并。
- 如果参数重叠，则在合并来自包含文件的配置时，最后包含的文件优先。
- 所有用 `include` 添加的配置合并后，主配置和被包含的配置合并。

这种合并方法是深度合并，其中哈希映射在配置中的任何深度合并。要合并哈希映射 `A`（包含到目前为止合并的配置）和 `B`（下一个配置），键和值按如下方式处理：

- 当键只存在于 A 中时，使用 A 中的键和值。
- 当键在 A 和 B 中都存在，且它们的值都是哈希映射时，合并这些哈希映射。
- 当键同时存在于 A 和 B 中，并且其中一个值不是哈希映射时，使用 B 中的值。
- 否则，使用 B 中的键和值。

例如，使用包含两个文件的配置：

- `.gitlab-ci.yml` 文件：

  ```yaml
  include: 'common.yml'

  variables:
    POSTGRES_USER: username

  test:
    rules:
      - if: $CI_PIPELINE_SOURCE == "merge_request_event"
        when: manual
    artifacts:
      reports:
        junit: rspec.xml
  ```

- `common.yml` 文件：

  ```yaml
  variables:
    POSTGRES_USER: common_username
    POSTGRES_PASSWORD: testing_password

  test:
    rules:
      - when: never
    script:
      - echo LOGIN=${POSTGRES_USER} > deploy.env
      - rake spec
    artifacts:
      reports:
        dotenv: deploy.env
  ```

合并结果：

```yaml
variables:
  POSTGRES_USER: username
  POSTGRES_PASSWORD: testing_password

test:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
  script:
    - echo LOGIN=${POSTGRES_USER} > deploy.env
    - rake spec
  artifacts:
    reports:
      junit: rspec.xml
      dotenv: deploy.env
```

在此示例中：

- 仅在所有文件合并在一起后才评估变量。包含文件中的作业可能最终使用在不同文件中定义的变量值。
- `rules` 是一个数组，因此无法合并。顶级文件优先。
- `artifacts` 是一个哈希映射，因此可以进行深度合并。

## 覆盖包含的配置数组

您可以使用合并来扩展和覆盖包含模板中的配置，但不能添加或修改数组中的单个项目。例如，将额外的 `notify_owner` 命令添加到扩展的 `production` 作业的 `script` 数组中：

`autodevops-template.yml` 的内容：

```yaml
production:
  stage: production
  script:
    - install_dependencies
    - deploy
```

`.gitlab-ci.yml` 的内容：

```yaml
include: 'autodevops-template.yml'

stages:
  - production

production:
  script:
    - install_dependencies
    - deploy
    - notify_owner
```

如果 `.gitlab-ci.yml` 文件中没有重复 `install_dependencies` 和 `deploy`，那么 `production` 作业在脚本中将只有 `notify_owner`。

## 使用 nested includes

您可以在配置文件中嵌套 `include` 部分，然后将其包含在另一个配置中。例如，对于 `include` 关键字嵌套了三层深度：

`.gitlab-ci.yml` 的内容：

```yaml
include:
  - local: /.gitlab-ci/another-config.yml
```

`/.gitlab-ci/another-config.yml` 的内容：

```yaml
include:
  - local: /.gitlab-ci/config-defaults.yml
```

`/.gitlab-ci/config-defaults.yml` 的内容：

```yaml
default:
  after_script:
    - echo "Job complete."
```

### Use nested includes with duplicate `includes` entries

> 引入于 14.8 版本。

Nested includes 可以包含相同的配置文件。重复的配置文件被多次包含，但效果和只包含一次一样。

例如，使用以下 nested includes，其中 `defaults.gitlab-ci.yml` 被包含多次：

- `.gitlab-ci.yml` 文件的内容：

  ```yaml
  include:
    - template: defaults.gitlab-ci.yml
    - local: unit-tests.gitlab-ci.yml
    - local: smoke-tests.gitlab-ci.yml
  ```

- `defaults.gitlab-ci.yml` 文件的内容：

  ```yaml
  default:
    before_script: default-before-script.sh
    retry: 2
  ```

- `unit-tests.gitlab-ci.yml` 文件的内容：

  ```yaml
  include:
    - template: defaults.gitlab-ci.yml

  unit-test-job:
    script: unit-test.sh
    retry: 0
  ```

- `smoke-tests.gitlab-ci.yml` 文件的内容：

  ```yaml
  include:
    - template: defaults.gitlab-ci.yml

  smoke-test-job:
    script: smoke-test.sh
  ```

最终的配置将是：

```yaml
unit-test-job:
  before_script: default-before-script.sh
  script: unit-test.sh
  retry: 0

smoke-test-job:
  before_script: default-before-script.sh
  script: smoke-test.sh
  retry: 2
```
<a id="use-variables-with-include"></a>

## 将变量与 `include` 一起使用

> - 引入于 13.8 版本。
> - 功能标志移除于 13.9 版本
> - 对项目、群组和实例变量的支持添加于 14.2 版本。
> - 对流水线变量的支持添加于 14.5 版本。

在您的 `.gitlab-ci.yml` 文件的 `include` 部分，您可以使用：

- 项目变量
- 群组变量
- 实例变量
- 项目预定义变量
- 在 14.2 及更高版本，`$CI_COMMIT_REF_NAME` 预定义变量

  当在 `include` 中使用时，`CI_COMMIT_REF_NAME` 变量返回完整的 ref 路径，如 `refs/heads/branch-name`。在 `include:rules` 中，可能需要使用 `if: $CI_COMMIT_REF_NAME =~ /main/`（不是`== main`）。已在 GitLab 14.5 中解决。

在 14.5 及更高版本中，您还可以使用：

- 触发器变量
- 计划流水线变量
- 手动流水线运行变量
- 流水线预定义变量

  在创建流水线之前解析 YAML 文件，因此以下流水线预定义变量**不**可用：

  - `CI_PIPELINE_ID`
  - `CI_PIPELINE_URL`
  - `CI_PIPELINE_IID`
  - `CI_PIPELINE_CREATED_AT`

示例：

```yaml
include:
  project: '$CI_PROJECT_PATH'
  file: '.compliance-gitlab-ci.yml'
```

您不能使用在作业中定义的变量，或者在定义所有作业的默认变量的全局 [`variables`](../yaml/index.md#variables) 部分中。因为 Includes 在 jobs 之前生效，因此这些变量不能与 `include` 一起使用。

<!--
For an example of how you can include these predefined variables, and the variables' impact on CI/CD jobs,
see this [CI/CD variable demo](https://youtu.be/4XR8gw3Pkos).
-->

## 使用 `rules` with `include`

> - 引入于 14.2 版本，在名为 `ci_include_rules` 的功能标志后默认禁用。
> - 在私有化部署版上启用于 14.3 版本。
> - 功能标志 `ci_include_rules` 移除于 14.4 版本。
> - `exists` 关键字支持添加于 14.5 版本。
> - `needs` 作业依赖的支持引入于 15.11 版本。

您可以使用 [`rules`](index.md#rules) 和 `include` 来有条件地包含其他配置文件。

您只能将 `rules` 与[某些变量](#use-variables-with-include)，以及以下关键字一起使用：

- [`rules:if`](index.md#rulesif)。
- [`rules:exists`](index.md#rulesexists)。

### `include` with `rules:if`

使用 [`rules:if`](index.md#rulesif) 根据 CI/CD 变量的状态有条件地包含其他配置文件。例如：

```yaml
include:
  - local: builds.yml
    rules:
      - if: $INCLUDE_BUILDS == "true"
  - local: deploys.yml
    rules:
      - if: $CI_COMMIT_BRANCH == "main"

test:
  stage: test
  script: exit 0
```

### `include` with `rules:exists`

使用 [`rules:exists`](index.md#rulesexists) 根据文件的存在有条件地包含其他配置文件。例如：

```yaml
include:
  - local: builds.yml
    rules:
      - exists:
          - file.md

test:
  stage: test
  script: exit 0
```

在此示例中，极狐GitLab 检查当前项目中是否存在 `file.md`。

如果您使用 `rules:exists` 配置 `include`，添加来自不同项目的配置文件，则会出现一个已知问题。极狐GitLab 检查其他项目中是否存在该文件。例如：

```yaml
include:
- project: my-group/my-project-2
  ref: main
  file: test-file.yml
  rules:
    - exists:
        - file.md

test:
  stage: test
  script: exit 0
```

在此示例中，极狐GitLab 检查 `my-group/my-project-2` 中是否存在 `test-file.yml`，而不是当前项目。

<a id="use-includelocal-with-wildcard-file-paths"></a>

## 将 `include:local` 与通配符文件路径一起使用

> - 引入于 13.11 版本。
> - 功能标志移除于 14.2 版本。

您可以在 `include:local` 中使用通配符路径（`*` 和 `**`）。

例子：

```yaml
include: 'configs/*.yml'
```

当流水线运行时，系统：

- 将 `configs` 目录中的所有 `.yml` 文件添加到流水线配置中。
- 不在 `configs` 目录的子文件夹中添加 `.yml` 文件。 为此，请添加以下配置：

  ```yaml
  # This matches all `.yml` files in `configs` and any subfolder in it.
  include: 'configs/**.yml'

  # This matches all `.yml` files only in subfolders of `configs`.
  include: 'configs/**/*.yml'
  ```

## 为使用 `include` 添加的配置定义输入（Beta）

> 引入于 15.11 版本，作为 Beta 功能。

FLAG:
`spec` 和 `with` 是实验性的 [Open Beta 功能](../../policy/alpha-beta-support.md#beta)，如有更改，恕不另行通知。

<a id="define-input-parameters-with-specinputs"></a>

### 使用 `spec:inputs` 定义输入参数

使用 `spec:inputs` 来定义 CI/CD 配置的输入参数，这些配置将被添加到带有 `include` 的流水线中。使用 [`include:inputs`](#set-input-parameter-values-with-includeinputs) 定义流水线运行时要使用的值。

规范必须在配置文件顶部的 header 中声明。
使用 `---` 将 header 与其余配置分开。

使用插值格式 `$[[ input.input-id ]]` 来引用 header 部分之外的值。
在流水线创建期间获取配置时，但在配置与 `.gitlab-ci.yml` 的内容合并之前，针对输入，将评估和插值一次。

```yaml
spec:
  inputs:
    environment:
    job-stage:
---

scan-website:
  stage: $[[ inputs.job-stage ]]
  script: ./scan-website $[[ inputs.environment ]]
```

使用 `spec:inputs` 时：

- 默认情况下，定义的输入是强制性的。
- 通过指定 `default` 可以使输入可选。使用 `default: null` 没有默认值。
- 包含插值块的字符串不得超过 1 MB。
- 插值块内的字符串不得超过 1 KB。

`custom_configuration.yml` 示例：

```yaml
spec:
  inputs:
    website:
    user:
      default: 'test-user'
    flags:
      default: null
---

# The pipeline configuration would follow...
```

在此示例中：

- `website` 是强制性的，必须定义。
- `user` 是可选的。如果未定义，则值为 `test-user`。
- `flags` 是可选的。如果没有定义就没有值。

<a id="set-input-parameter-values-with-includeinputs"></a>

### 使用 `include:inputs` 设置输入参数值

> `include:with` 重命名为 `include:inputs` 于 16.0 版本。

当包含的配置添加到流水线时，使用 `include:inputs` 设置参数的值。

例如，要包含与[上面的示例](#define-input-parameters-with-specinputs)具有相同 specs 的 `custom_configuration.yml`：

```yaml
include:
  - local: 'custom_configuration.yml'
    inputs:
      website: "My website"
```

在此示例中：

- 对于包含的配置，`website` 的值为 `My website`。
- `user` 的值为 `test-user`，因为未指定时这是默认值。
- `flags` 没有值，因为它是可选的，并且在未指定时没有默认值。

## 故障排除

### `Maximum of 150 nested includes are allowed!` 错误

流水线的[嵌套包含文件](#use-nested-includes)的最大数量为 150。
如果您在流水线中收到 `Maximum 150 includes are allowed` 错误消息，则可能是：

- 一些嵌套配置包括过多的额外嵌套 `include` 配置。
- 嵌套包含中存在意外循环。例如，`include1.yml` 包含 `include2.yml`，后者包含 `include1.yml`，从而创建递归循环。

为帮助降低发生这种情况的风险，请使用[流水线编辑器](../pipeline_editor/index.md)，编辑流水线配置文件，以验证是否达到限制。您可以一次删除一个包含文件，以尝试缩小哪个配置文件是循环源或过多包含文件的范围。
