---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 密码和 OAuth 令牌存储 **(FREE)**

极狐GitLab 管理员可以配置密码和 OAuth 令牌的存储方式。

## 密码存储

> PBKDF2 和 SHA512 引入于 15.2 版本，[功能标志](../administration/feature_flags.md)为 `pbkdf2_password_encryption` 和 `pbkdf2_password_encryption_write`。默认禁用。

极狐GitLab 以哈希格式存储用户密码，以防止密码以纯文本形式存储。

极狐GitLab 使用 [Devise](https://github.com/heartcombo/devise) 身份验证库来哈希用户密码。创建的密码哈希具有以下属性：

- **哈希**：
  - **bcrypt**：默认情况下，`bcrypt` 哈希函数用于生成所提供密码的哈希。这是一个强大的、行业标准的加密哈希函数。
  - **PBKDF2 和 SHA512**：从 15.2 版本开始，以下功能标志（默认禁用）支持 PBKDF2 和 SHA512：
    - `pbkdf2_password_encryption` - 启用 PBKDF2 + SHA512 哈希密码的读取和比较，并支持 BCrypt 哈希密码的回退。
    - `pbkdf2_password_encryption_write` - 允许使用 PBKDF2 和 SHA512 保存新密码，并在用户登录时迁移现有 bcrypt 密码。

- **拉伸**：密码哈希被拉伸以抵御暴力攻击。默认情况下，系统对 BCrypt 使用 10 的拉伸参数，对 PBKDF2 + SHA512 使用 20,000。
- **加盐**：每个密码都添加了加密盐，以加强对预先计算的哈希和字典攻击的防范。为了提高安全性，每个盐由每个密码随机生成，没有两个密码共享一个盐。

## OAuth 访问令牌存储

> - PBKDF2+SHA512 引入于 15.3 版本，[功能标志](../administration/feature_flags.md)为 `hash_oauth_tokens`。
> - 默认启用于 15.5 版本。

根据您的极狐GitLab 版本和配置，OAuth 访问令牌以 PBKDF2+SHA512 格式存储在数据库中。

与 PBKDF2+SHA512 密码存储一样，访问令牌值被拉伸 20,000 倍，以抵御暴力攻击。
