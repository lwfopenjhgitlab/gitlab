---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: concepts, reference, howto
---

# 过滤出站请求 **(FREE SELF)**

为了防止数据丢失和暴露的风险，极狐GitLab 管理员现在可以使用出站请求过滤控件，限制极狐GitLab 实例发出的某些出站请求。

## 安全的 webhooks 和集成

至少具有维护者角色的用户，可以设置在项目或群组中发生特定更改时触发的 [webhooks](../user/project/integrations/webhooks.md)。触发时，会向 URL 发送一个 `POST` HTTP 请求。Webhook 通常配置为将数据发送到特定的外部 Web 服务，该服务以适当的方式处理数据。

但是，可以使用内部 Web 服务而不是外部 Web 服务的 URL 配置 Webhook。
当 webhook 被触发时，运行在极狐GitLab 服务器或其本地网络中的非极狐GitLab Web 服务可能会被利用。

Webhook 请求由极狐GitLab 服务器本身发出，每个钩子使用一个可选的 secret 令牌进行授权，而不是：

- 用户令牌。
- 特定仓库的令牌。

因此，这些请求可能具有比预期更广泛的访问权限，包括对托管 Webhook 的服务器上运行的所有内容的访问权限，包括：

- 极狐GitLab 服务器。
- API 本身。
- 对于某些 webhook，网络访问该 webhook 服务器的本地网络中的其他服务器，即使这些服务受到保护并且无法从外部访问。

Webhook 可用于不需要身份验证的 Web 服务触发破坏性命令。这些 webhook 可以让极狐GitLab 服务器向删除资源的端点发出 `POST` HTTP 请求。

### 允许来自 webhooks 和集成对本地网络的请求

先决条件：

- 您必须具有实例的管理员访问权限。

为防止利用不安全的内部 Web 服务，不允许对以下本地网络地址的所有 Webhook 请求：

- 当前极狐GitLab 实例服务器地址。
- 私网地址，包括 `127.0.0.1`、`::1`、`0.0.0.0`、`10.0.0.0/8`、`172.16.0.0/12`、`192.168.0.0/16` 和 IPv6 站点本地（`ffc0::/10`）地址。

要允许访问这些地址：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 网络**。
1. 展开 **出站请求**。
1. 选中 **允许来自 webhooks 和集成对本地网络的请求** 复选框。

## 阻止系统钩子对本地网络的请求

先决条件：

- 您必须具有实例的管理员访问权限。

[系统钩子](../administration/system_hooks.md)默认允许向本地网络发出请求，因为它们是由管理员设置的。要阻止系统钩子请求到本地网络：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 网络**。
1. 展开 **出站请求**。
1. 清除 **允许系统钩子向本地网络发送请求** 复选框。

## 过滤请求

> 引入于 15.10 版本，[功能标志](../administration/feature_flags.md)为 `deny_all_requests_except_allowed`。默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，需要管理员[启用功能标志](../administration/feature_flags.md) `deny_all_requests_except_allowed`。在 SaaS 版上，此功能不可用。

先决条件：

- 您必须具有实例的管理员访问权限。

通过阻止许多请求来过滤请求：

1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 网络**。
1. 展开 **出站请求**。
1. 选择 **阻止所有请求，但白名单中定义的 IP 地址、IP 范围和域名除外**复选框。

选中此复选框后，仍不会阻止对以下内容的请求：

- 核心服务，如 Geo、Git、GitLab Shell、Gitaly、PostgreSQL 和 Redis。
- 对象存储。
- [白名单](#allow-outbound-requests-to-certain-ip-addresses-and-domains)中的 IP 地址和域名。

此设置仅适用于极狐GitLab 主应用程序，因此 Gitaly 等其他服务仍然可以发出违反规则的请求。此外，某些部分不遵守出站过滤规则。

<a id="allow-outbound-requests-to-certain-ip-addresses-and-domains"></a>

## 允许对特定 IP 地址和域名的出站请求

先决条件：

- 您必须具有实例的管理员访问权限。

允许对特定 IP 地址和域名的出站请求：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 网络**。
1. 展开 **出站请求**。
1. 在 **Webhook 和集成可以访问的本地 IP 地址和域名** 中，输入您的 IP 地址和域名。

条目可以：

- 用分号、逗号或空格（包括换行符）分隔。
- 采用不同的格式，如主机名、IP 地址、IP 地址范围。支持 IPv6。包含 Unicode 字符的主机名应使用 [Internationalized Domain Names in Applications](https://www.icann.org/en/icann-acronyms-and-terms/internationalized-domain-names-in-applications-en) (IDNA) 编码。
- 包括端口。例如，`127.0.0.1:8080` 只允许连接到 `127.0.0.1` 上的 8080 端口。如果未指定端口，则允许该 IP 地址或域上的所有端口。IP 地址范围允许该范围内所有 IP 地址上的所有端口。
- 编号不超过 1000 个条目，每个条目不超过 255 个字符。
- 不包含通配符（例如，`*.example.com`）。

例如：

```plaintext
example.com;gitlab.example.com
127.0.0.1,1:0:0:0:0:0:0:1
127.0.0.0/8 1:0:0:0:0:0:0:0/124
[1:0:0:0:0:0:0:1]:8080
127.0.0.1:8080
example.com:8080
```

## 故障排除

在过滤出站请求时，您可能会遇到以下问题。

### 配置的 URL 被阻止

如果配置的 URL 不应被阻止，您只能选择 **阻止所有请求，白名单中定义的 IP 地址、IP 范围和域名除外** 复选框。否则，您可能会收到一条错误消息，指出该 URL 已被阻止。

如果您无法启用此设置，请执行以下操作之一：

- 禁用 URL 设置。
- 配置另一个 URL，或将 URL 设置留空。
- 将配置的 URL 添加到[白名单](#allow-requests-to-the-local-network-from-webhooks-and-integrations)。

<!--
### Public runner releases URL is blocked

Most GitLab instances have their `public_runner_releases_url` set to
`https://gitlab.com/api/v4/projects/gitlab-org%2Fgitlab-runner/releases`.
You can't change or disable this setting in the Admin Area, which can prevent you from [filtering requests](#filter-requests).

To enable the setting, use the [Rails console](../administration/operations/rails_console.md) to set `public_runner_releases_url` to the instance host:

```ruby
current_settings = ApplicationSetting.find_or_create_without_cache;
ApplicationSettings::UpdateService.new(current_settings, nil, public_runner_releases_url: Gitlab.config.gitlab.base_url).execute
```

### GitLab subscription management is blocked

When you [filter requests](#filter-requests), [GitLab subscription management](../subscriptions/self_managed/index.md)
is blocked.

To work around this problem, add `customers.gitlab.com:443` to the
[allowlist](#allow-outbound-requests-to-certain-ip-addresses-and-domains).
-->
