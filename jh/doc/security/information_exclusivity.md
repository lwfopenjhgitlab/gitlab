---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: concepts
---

# 信息排他性 **(FREE)**

Git 是一个分布式版本控制系统 (DVCS)。这意味着使用源代码的每个人都拥有完整仓库的本地副本。

在极狐GitLab 中，每个不是访客的项目成员（报告者、开发者和维护者）都可以克隆仓库来创建本地副本。获得本地副本后，用户可以将完整的仓库上传到任何地方，包括到他们控制的另一个项目或另一个服务器上。

因此，不可能建立访问控制来防止有权访问源代码的用户故意共享源代码。

这是 DVCS 的固有特性。所有 Git 管理系统都有这个限制。

您可以采取措施防止无意的共享和信息破坏。此限制是为什么只允许某些人[将用户添加到项目](../user/project/members/index.md)，以及为什么只有极狐GitLab 管理员可以[强制推送受保护的分支](../user/project/protected_branches.md)。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
