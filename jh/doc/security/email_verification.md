---
stage: Anti-Abuse
group: Anti-Abuse
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 账户邮箱验证 **(FREE)**

> 引入于 15.2 版本，[功能标志](../administration/feature_flags.md)为 `require_email_verification`。默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，需要管理员[启用功能标志](../administration/feature_flags.md) `require_email_verification`。在 SaaS 版上，此功能不可用。

账户电子邮件验证提供了额外的极狐GitLab 账户安全。
当满足某些条件时，账户将被锁定。如果您的账户被锁定，您必须验证您的身份或重置密码才能登录。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For a demo, see [Require email verification - demo](https://www.youtube.com/watch?v=wU6BVEGB3Y0).
-->

## 没有双重身份验证 (2FA) 的账户

账户在以下任一情况下被锁定：

- 24 小时内有 3 次或更多次登录尝试失败。
- 用户尝试从新 IP 地址登录，并且启用了 `check_ip_address_for_email_verification` 功能标志。

没有 2FA 的锁定账户不会自动解锁。

成功登录后，将发送一封包含六位验证码的电子邮件。验证码将在 60 分钟后过期。

要解锁您的账户，请登录并输入验证码。您还可以[重置密码](https://jihulab.com/users/password/new)。

## 具有 2FA 或 OAuth 的账户

如果在 10 分钟内尝试登录失败五次或更多次，则账户将被锁定。

使用 2FA 或 OAuth 的账户会在 10 分钟后自动解锁。要手动解锁账户，请重设密码。

<!--
## Related topics

- [Locked and blocked account support](https://about.gitlab.com/handbook/support/workflows/reinstating-blocked-accounts.html)
-->
