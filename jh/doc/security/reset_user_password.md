---
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# 重置用户的密码 **(FREE SELF)**

您可以使用 Rake 任务、Rails 控制台或 Users API<!--[Users API](../api/users.md#user-modification)--> 重置用户密码。

## 先决条件

要重置用户密码，您必须是私有化部署实例的管理员。

用户的新密码必须满足所有[密码要求](../user/profile/user_passwords.md#password-requirements)。

<a id="use-the-ui"></a>

## 使用 UI

要在 UI 中重置用户密码：

1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **概览 > 用户**。
1. 对于您要更新其密码的用户，选择 **编辑** (**{pencil-square}**)。
1. 在 **密码** 区域，输入密码，并再次确认密码。
1. 选择 **保存更改**。

系统显示确认信息。

<a id="use-a-rake-task"></a>

## 使用 Rake 任务

> 引入 13.9 版本。

使用以下 Rake 任务重置用户的密码：

- **Omnibus 安装实例**

  ```shell
  sudo gitlab-rake "gitlab:password:reset"
  ```

- **源安装实例**

  ```shell
  bundle exec rake "gitlab:password:reset"
  ```

极狐GitLab 请求用户名、密码和密码确认。完成后，将更新用户的密码。

Rake 任务可以将用户名作为参数。例如，要为用户名为 `sidneyjones` 的用户重置密码：

- **Omnibus 安装实例**

  ```shell
  sudo gitlab-rake "gitlab:password:reset[sidneyjones]"
  ```

- **源安装实例**

  ```shell
  bundle exec rake "gitlab:password:reset[sidneyjones]"
  ```

## 使用 Rails 控制台

如果您知道用户名、用户 ID 或电子邮件地址，您可以使用 Rails 控制台重置他们的密码：

1. 打开 Rails 控制台<!--[Rails 控制台](../administration/operations/rails_console.md)-->。
1. 找到用户：

   - 通过用户名：

     ```ruby
     user = User.find_by_username 'exampleuser'
     ```

   - 通过用户 ID：

     ```ruby
     user = User.find(123)
     ```

   - 通过电子邮件地址：

     ```ruby
     user = User.find_by(email: 'user@example.com')
     ```
      
1. 通过设置 `user.password` 和 `user.password_confirmation` 的值来重置密码。例如，要设置一个新的随机密码：

   ```ruby
   new_password = ::User.random_password
   user.password = new_password
   user.password_confirmation = new_password
   ```

   要为新密码设置特定值：

   ```ruby
   new_password = 'examplepassword'
   user.password = new_password
   user.password_confirmation = new_password
   ```

1. 可选。通知用户，管理员已更改他们的密码：

   ```ruby
   user.send_only_admin_changed_your_password_notification!
   ```

1. 保存更改：

   ```ruby
   user.save!
   ```

1. 退出控制台：

   ```ruby
   exit
   ```

## 重置 root 密码

要重置 root 密码，请按照前面列出的步骤操作。

- 如果 root 帐户名称未更改，请使用用户名 `root`。
- 如果 root 帐户名称已更改，而您不知道新用户名，则可以使用带有用户 ID `1` 的 Rails 控制台。在几乎所有情况下，第一个用户是默认管理员帐户。

## 故障排查

如果新密码不起作用，则可能是电子邮件确认问题<!--[电子邮件确认问题](../user/upgrade_email_bypass.md)-->。您可以尝试在 Rails 控制台中修复此问题。例如，如果新的 `root` 密码不起作用：

1. 打开 Rails 控制台<!--[Rails 控制台](../administration/operations/rails_console.md)-->。
1. 找到用户并跳过重新确认：

   ```ruby
   user = User.find(1)
   user.skip_reconfirmation!
   ```

1. 尝试重新登录。
