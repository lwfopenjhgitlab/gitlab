---
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# 锁定用户 **(FREE SELF)**

用户在十次登录尝试失败后被锁定。这些用户保持锁定状态：

- 10 分钟后，它们会自动解锁。
- 直到管理员在 10 分钟内从[管理中心](../user/admin_area/index.md)或命令行解锁它们。

## 从管理中心解锁用户

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **概览 > 用户**。
1. 使用搜索栏找到被锁定的用户。
1. 从 **用户管理** 下拉列表中选择 **解锁**。

## 从命令行解锁用户

在十次失败的登录尝试后，用户进入锁定状态。

要解锁锁定的用户：

1. 通过 SSH 连接到您的 GitLab 服务器。
1. 启动 Ruby on Rails 控制台：

   ```shell
   ## For Omnibus GitLab
   sudo gitlab-rails console -e production

   ## For installations from source
   sudo -u git -H bundle exec rails console -e production
   ```

1. 找到要解锁的用户。您可以通过电子邮件或 ID 进行搜索。

   ```ruby
   user = User.find_by(email: 'admin@local.host')
   ```

   或

   ```ruby
   user = User.where(id: 1).first
   ```

1. 解锁用户：

   ```ruby
   user.unlock_access!
   ```

1. 使用 <kbd>Control</kbd>+<kbd>d</kbd> 退出控制台

用户现在应该可以登录了。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
