---
type: howto
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 强制双重认证 **(FREE)**

双重身份认证 (2FA) 为用户的极狐GitLab 帐户提供了额外的安全级别。启用后，除了提供用户名和密码以进行登录外，还会提示用户输入应用程序生成的代码。

了解更多关于[双重身份认证 (2FA)](../user/profile/account/two_factor_authentication.md)。

## 对所有用户强制执行 2FA **(FREE SELF)**

极狐GitLab 上的用户无需任何管理员干预即可启用它。如果您想强制每个人都设置 2FA，您可以从两种不同的方式中进行选择：

- 下次登录时强制执行
- 建议下次登录时执行，但在执行前允许宽限期。

配置的宽限期过后，用户可以登录，但不能离开 `/-/profile/two_factor_auth` 的 2FA 配置区域。

为所有用户启用 2FA：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 通用**（`/admin/application_settings/general`）。
1. 展开 **登录限制** 部分，您可以在其中配置。

如果您希望 2FA 强制在下次登录尝试期间生效，请将宽限期更改为 `0`。

### 通过 rails 控制台禁用 2FA 强制执行

使用 [rails 控制台](../administration/operations/rails_console.md)，可以禁用对所有用户强制执行 2FA。连接到 rails 控制台并运行：

```ruby
Gitlab::CurrentSettings.update!('require_two_factor_authentication': false)
```

## 对群组中的所有用户强制执行 2FA **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/24965) in GitLab 12.0, 2FA settings for a group are also applied to subgroups.
-->

先决条件：

- 您必须具有该群组的维护者或所有者角色。

仅对某些群组强制执行 2FA：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **权限和群组功能**。
1. 选择 **此群组中的所有用户必须设置双重身份验证**。
1. 选择 **保存更改**。

您还可以在 **2FA 强制执行延迟** 选项中指定宽限期。

如果您只想对某些群组强制执行 2FA，您可以在群组设置中，启用它并如上所述指定宽限期。

以下是有关 2FA 的重要说明：

- 对于启用 2FA 的群组与禁用 2FA 的群组共享的项目，将*不*要求禁用 2FA 的群组的成员，为项目使用 2FA。例如，如果项目 *P* 属于启用 2FA 的群组 *A* 并与禁用 2FA 的群组 *B* 共享, *B* 群组的成员可以在没有 2FA 的情况下访问项目 *P*。如果要确保不发生这种情况，防止为启用 2FA 的组共享项目。
- 如果您将其它成员添加到启用了 2FA 的群组或子组中的项目，则这些单独添加的成员**不需要** 2FA。
- 如果有多个 2FA 要求（例如，群组 + 所有用户或多个群组），则使用最短的宽限期。
- 可以禁止子组设置自己的 2FA 要求：
   1. 转到顶级组的 **设置 > 通用**。
   1. 展开 **权限和群组功能** 部分。
   1. 取消选中 **允许子组设置自己的双重认证规则** 字段。

  此操作会导致所有具有 2FA 要求的子组，停止对其成员要求 2FA。

- 不需要访问令牌为身份验证提供二次验证，因为它们是基于 API 的。在执行 2FA 之前生成的令牌仍然有效。

## 禁用 2FA **(FREE SELF)**

WARNING:
为用户禁用 2FA 不会禁用[对所有用户强制 2FA](#对所有用户强制执行-2fa) 或[对群组中的所有用户强制执行 2FA](#对群组中的所有用户强制执行-2fa)设置。您还必须禁用任何强制执行的 2FA 设置，这样用户下次登录时就不会被要求再次设置 2FA。

WARNING:
这是一个永久且不可逆转的行为。用户必须重新激活 2FA 才能再次使用它。

### 为单个用户

要为非管理员用户禁用 2FA，我们建议使用 API 端点<!--[API 端点](../api/users.md#disable-two-factor-authentication)-->而不是 Rails 控制台。
使用 [Rails 控制台](../administration/operations/rails_console.md)，可以禁用单个用户的 2FA。
连接到 Rails 控制台并运行：

```ruby
admin = User.find_by_username('<USERNAME>')
user_to_disable = User.find_by_username('<USERNAME>')

TwoFactor::DestroyService.new(admin, user: user_to_disable).execute
```

### 为所有用户

在某些特殊情况下，即使禁用了强制 2FA，您也可能希望为所有人禁用 2FA。执行一个 Rake 任务：

```shell
# Omnibus installations
sudo gitlab-rake gitlab:two_factor:disable_for_all_users

# Installations from source
sudo -u git -H bundle exec rake gitlab:two_factor:disable_for_all_users RAILS_ENV=production
```

<a id="2fa-for-git-over-ssh-operations"></a>

## Git over SSH 操作的 2FA **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/270554) in GitLab 13.7.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/299088) from GitLab Free to GitLab Premium in 13.9.
> - It's [deployed behind a feature flag](../user/feature_flags.md), disabled by default.
> - It's disabled on GitLab.com.
> - It's not recommended for production use.
> - To use it in GitLab self-managed instances, ask a GitLab administrator to [enable it](#enable-or-disable-2fa-for-git-operations).

WARNING:
This feature might not be available to you. Check the **version history** note above for details.
-->

可以通过 SSH 操作对 Git 实施双重认证。但是，我们建议改用 [ED25519_SK](../user/ssh.md#ed25519_sk-ssh-密钥) 或 [ECDSA_SK](../user/ssh.md#ecdsa_sk-ssh-密钥) SSH 密钥。

可以使用以下命令完成一次性密码 (OTP) 验证：

```shell
ssh git@<hostname> 2fa_verify
```

验证 OTP 后，Git over SSH 操作可用于 15 分钟（默认）的会话持续时间以及关联的 SSH 密钥。

### 安全限制

2FA 不会保护带有 compromised *私有* SSH 密钥的用户。

一旦验证了 OTP，任何人都可以在配置的会话持续时间内，使用该私有 SSH 密钥通过 SSH 运行 Git。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
