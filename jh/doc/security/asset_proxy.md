---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 代理 assets **(FREE SELF)**

管理面向公众的极狐GitLab 实例时，一个可能存在的安全问题是通过引用提议和评论中的图像来窃取用户 IP 地址的能力。

例如，在议题描述中添加 `![Example image](http://example.com/example.png)` 会导致从外部服务器加载图像以便显示。但是，这也允许外部服务器记录用户的 IP 地址。

缓解这种情况的一种方法是，将任何外部图像代理到您控制的服务器。

极狐GitLab 可以配置为在议题和评论中请求外部图像/视频/音频时使用 assets 代理服务器。这有助于确保恶意图像在获取时不会暴露用户的 IP 地址。

我们目前推荐使用 [cactus/go-camo](https://github.com/cactus/go-camo#how-it-works)，因为它支持代理视频、音频，并且更具可配置性。

## 安装 Camo 服务器

Camo 服务器用于充当代理。

安装 Camo 服务器为 assets 代理：

1. 部署一个 `go-camo` 服务器。 参考 [building cactus/go-camo](https://github.com/cactus/go-camo#building)。

1. 确保您的极狐GitLab 实例正在运行，并且您已创建私有 API 令牌。使用 API，在极狐GitLab 实例上配置 assets 代理设置。例如：

   ```shell
   curl --request "PUT" "https://gitlab.example.com/api/v4/application/settings?\
   asset_proxy_enabled=true&\
   asset_proxy_url=https://proxy.gitlab.example.com&\
   asset_proxy_secret_key=<somekey>" \
   --header 'PRIVATE-TOKEN: <my_private_token>'
   ```

   支持以下设置：

   | 参数               | 描述                                                                                                                          |
   |:-------------------------|:-------------------------------------------------------------------------------------------------------------------------------------|
   | `asset_proxy_enabled`    | 启用 assets 代理。如果启用，需要：`asset_proxy_url`。                                                                 |
   | `asset_proxy_secret_key` | 与 assets 代理服务器共享密钥。                                                                                           |
   | `asset_proxy_url`        | assets 代理服务器的 URL。                                                                                                       |
   | `asset_proxy_whitelist`  | （已弃用：改用 `asset_proxy_allowlist`）匹配这些域名的资产不会被代理。允许使用通配符。自动允许您的极狐GitLab 安装实例 URL。        |
   | `asset_proxy_allowlist`  | 与这些域名匹配的 assets 不会被代理。允许使用通配符。自动允许您的极狐GitLab 安装实例 URL。         |

1. 重新启动服务器以使更改生效。每次更改 assets 代理的任何值时，都需要重新启动服务器。

## 使用 Camo 服务器

一旦 Camo 服务器运行并且您启用了极狐GitLab 设置，任何引用外部源的图像、视频或音频都会被代理到 Camo 服务器。

例如，以下是 Markdown 中图像的链接：

```markdown
![logo](https://about.gitlab.cn/images/press/logo/jpg/gitlab-icon-rgb.jpg)
```

以下是可能的源链接示例：

```plaintext
http://proxy.gitlab.example.com/f9dd2b40157757eb82afeedbf1290ffb67a3aeeb/68747470733a2f2f61626f75742e6769746c61622e636f6d2f696d616765732f70726573732f6c6f676f2f6a70672f6769746c61622d69636f6e2d7267622e6a7067
```
