---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# 应对安全事件 **(FREE SELF)**

发生安全事件时，您应该遵循组织定义的流程。但是，您可能会考虑一些额外的步骤。这些建议旨在补充您组织内现有的安全事件响应流程。

<a id="suspected-compromised-user-account"></a>

## 怀疑被盗用的用户帐户

如果您怀疑用户帐户或机器人帐户已被盗用，请考虑采取以下步骤：

- [禁用用户](../user/admin_area/moderate_users.md#block-a-user)以减轻当前的任何风险。
- [查看审计事件](../administration/audit_events.md)，识别任何可疑的帐户行为。例如：
  - 可疑的登录事件。
  - 创建或删除个人访问令牌、项目访问令牌和群组访问令牌。
  - 创建或删除 SSH 或 GPG 密钥。
  - 创建、修改或删除双重身份验证。
  - 对仓库的更改。
  - 更改群组或项目配置。
  - 添加或修改 runners。
  - 添加或修改 webhook 或 Git hooks。
- 重置用户可能有权访问的任何凭据。例如，至少具有维护者角色的用户可以查看受保护的 [CI/CD 变量](../ci/variables/index.md)和 [runner 注册令牌](token_overview.md#runner-registration-tokens)。
- [重置用户密码](reset_user_password.md)。
- 让用户[启用双重身份验证](../user/profile/account/two_factor_authentication.md)（2FA），并考虑[在实例或群组级别执行 2FA](two_factor_authentication.md)。
- 完成调查并减轻影响后，取消禁用用户。

## 疑似被盗实例 **(FREE SELF)**

私有化部署版极狐GitLab 客户和管理员负责：

- 其底层主机的安全性。
- 使极狐GitLab 本身保持最新。

重要的是[定期更新极狐GitLab](../policy/maintenance.md)，更新您的操作系统及其软件，并根据供应商指导强化您的主机。

如果您怀疑您的极狐GitLab 实例已被入侵，请考虑采取以下步骤：

- [查看审计事件](../administration/audit_events.md)查看可疑帐户行为。
- [查看所有用户](../user/admin_area/moderate_users.md)（包括管理员 root 用户），如有必要，请按照[怀疑被盗用的用户帐户](#suspected-compromised-user-account)中的步骤操作。
- 查看[凭据库](../user/admin_area/credentials_inventory.md)，如果您可以使用。
- 更改位于例如实例配置、数据库、CI/CD 流水线或其他地方的任何敏感凭据、变量、令牌和 secret。
- 升级到最新版本的极狐GitLab，并在每个安全补丁发布后采取升级计划。

此外，以下建议是在服务器受到恶意行为者入侵时在事件响应计划中采取的常见步骤。

WARNING:
使用这些建议需要您自担风险。

- 将任何服务器状态和日志保存到一次写入（write-once）位置，以供日后调查。
- 寻找无法识别的后台进程。
- 检查系统上的开放端口。
- 从已知良好的备份或从头开始重建主机，并应用所有最新的安全补丁。
- 查看不常见流量的网络日志。
- 建立网络监控和网络级控制。
- 仅限授权用户和服务器的入站和出站网络访问。
- 确保所有日志都路由到独立的只写数据存储。
