---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# 项目导入解压存档大小限制 **(FREE SELF)**

> - 引入于 13.2 版本。
> - 默认启用于 14.0 版本。

使用[项目导入](../user/project/settings/import_export.md)时，解压后的项目存档大小限制为 2Gb。

如果解压后的大小超过此限制，则返回 `Decompressed archive size validation failed` 错误。

## 启用/禁用大小验证

如果您的项目的解压缩大小超过此限制，则可以通过关闭 `validate_import_decompressed_archive_size` 功能标志来禁用验证。

启动 [Rails 控制台](../administration/operations/rails_console.md#starting-a-rails-console-session)。

```ruby
# Disable
Feature.disable(:validate_import_decompressed_archive_size)

# Enable
Feature.enable(:validate_import_decompressed_archive_size)
```
