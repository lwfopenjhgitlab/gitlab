---
stage: Manage
group: Authentication & Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
comments: false
type: index
---

# 安全 **(FREE)**

- [密码长度限制](password_length_limits.md)
- [限制 SSH 关键技术和最小长度](ssh_keys_restrictions.md)
- [速率限制](rate_limits.md)
- [Webhook 和不安全的内部 Web 服务](webhooks.md)
- [重置用户密码](reset_user_password.md)
- [锁定用户](unlock_user.md)
- [强制双重验证](two_factor_authentication.md)
- [CI/CD 变量](../ci/variables/index.md#cicd-variable-security) 


## 保护您的极狐GitLab 安装实例

使用诸如[注册限制](../user/admin_area/settings/sign_up_restrictions.md)和身份验证选项之类的访问控制功能，强化极狐GitLab 实例并最大限度地降低不受欢迎的用户帐户创建的风险。

<!--
Self-hosting GitLab customers and administrators are responsible for the security of their underlying hosts, and for keeping GitLab itself up to date. It is important to [regularly patch GitLab](../policy/maintenance.md), patch your operating system and its software, and harden your hosts in accordance with vendor guidance.
-->