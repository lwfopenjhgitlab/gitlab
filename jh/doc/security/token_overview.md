---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 极狐GitLab 令牌概览 **(FREE)**

本文档列出了极狐GitLab 中使用的令牌、它们的用途以及安全指南（如适用）。

<a id="personal-access-tokens"></a>

## 个人访问令牌

您可以创建[个人访问令牌](../user/profile/personal_access_tokens.md)，进行以下身份验证：

- 极狐GitLab API。
- 极狐GitLab 仓库。
- 极狐GitLab registry。

您可以限制个人访问令牌的范围和到期日期。默认情况下，它们从创建它们的用户那里继承权限。

## OAuth2 令牌

极狐GitLab 可以充当 [OAuth2 provider](../api/oauth2.md)，允许其他服务代表用户访问极狐GitLab API。

您可以限制 OAuth2 令牌的范围和生命周期。

## 模拟令牌

[模拟令牌](../api/index.md#impersonation-tokens)是一种特殊类型的个人访问令牌。它只能由管理员为特定用户创建。模拟令牌可以帮助您构建应用程序或脚本，以特定用户身份使用极狐GitLab API、仓库和 registry 进行身份验证。

您可以限制模拟令牌的范围并设置到期日期。

## 项目访问令牌

[项目访问令牌](../user/project/settings/project_access_tokens.md#project-access-tokens)的范围为项目。与[个人访问令牌](#personal-access-tokens)一样，您可以使用它们进行以下身份验证：

- 极狐GitLab API。
- 极狐GitLab 仓库。
- 极狐GitLab registry。

您可以限制项目访问令牌的范围和到期日期。当您创建项目访问令牌时，极狐GitLab 会创建一个[用于项目的机器人用户](../user/project/settings/project_access_tokens.md#bot-users-for-projects)。
项目的 Bot 用户是服务帐户，不计入许可席位。

## 群组访问令牌

[群组访问令牌](../user/group/settings/group_access_tokens.md#group-access-tokens)的范围为群组。与[个人访问令牌](#personal-access-tokens)一样，您可以使用它们进行身份验证：

- 极狐GitLab API。
- 极狐GitLab 仓库。
- 极狐GitLab registry。

您可以限制群组访问令牌的范围和到期日期。当您创建群组访问令牌时，极狐GitLab 会创建一个[用于群组的机器人用户](../user/group/settings/group_access_tokens.md#bot-users-for-groups)。
群组的 Bot 用户是服务帐户，不计入许可席位。

## 部署令牌

[部署令牌](../user/project/deploy_tokens/index.md)允许您在没有用户和密码的情况下，下载（`git clone`）或推送、拉取项目的软件包和容器镜像库镜像。部署令牌不能与极狐GitLab API 一起使用。部署令牌可以由项目维护者和所有者管理。

## 部署密钥

[部署密钥](../user/project/deploy_keys/index.md)通过将 SSH 公钥导入极狐GitLab 实例，允许对仓库进行只读或读写访问。部署密钥不能与极狐GitLab API 或 registry 一起使用。

例如，这对于将仓库克隆到您的持续集成 (CI) 服务器很有用。通过使用部署密钥，您不必设置虚假用户帐户。

项目维护者和所有者可以为项目仓库添加或启用部署密钥。

<a id="runner-registration-tokens"></a>

## Runner 注册令牌

Runner 注册令牌用于向极狐GitLab 注册 runner。群组或项目的所有者、实例管理员可以通过极狐GitLab 用户界面获取它们。注册令牌仅限于 runner 注册，没有其它使用范围。

您可以使用 runner 注册令牌添加在项目或群组中执行作业的 runner。Runner 可以访问项目的代码，因此在分配项目和群组级别的权限时要小心。

## Runner 身份验证令牌（也称为 runner 令牌）

注册后，runner 会收到一个身份验证令牌，当从作业队列中提取作业时，它会使用该令牌对极狐GitLab 进行身份验证。身份验证令牌本地存储在 runner 的 `config.toml` 文件中。

在通过极狐GitLab 进行身份验证后，runner 会收到一个[作业令牌](../ci/jobs/ci_job_token.md)，它用于执行作业。

在使用 Docker Machine/Kubernetes/VirtualBox/Parallels/SSH 执行器的情况下，执行环境无法访问 runner 身份验证令牌，因为它保留在 runner 机器上。他们只能访问作业令牌，这是执行作业所必需的。

恶意访问 runner 的文件系统可能会暴露 `config.toml` 文件和身份验证令牌，从而允许攻击者克隆 runner。

## CI/CD 作业令牌

[CI/CD](../ci/jobs/ci_job_token.md) 作业令牌是一个短暂的令牌，仅在作业期间有效。它使 CI/CD 作业可以访问有限数量的 API 端点。
API 身份验证通过使用触发作业的用户的授权，使用作业令牌。

作业令牌因其生命周期短且范围有限而受到保护。如果多个作业在同一台机器上运行（例如使用 shell runner），则可能会泄漏。在 Docker 机器 runner 上，建议配置 `MaxBuilds=1`，确保 runner 机器只运行一个构建并在之后被销毁。这可能会影响性能，因为配置机器需要一些时间。

## 可用范围

此表显示每个令牌的可用范围。可以在创建令牌时进一步限制范围。

|                             | API 访问 | Registry 访问 | 仓库访问 |
|-----------------------------|------------|-----------------|-------------------|
| 个人访问令牌       | ✅         | ✅              | ✅                |
| OAuth2 令牌                | ✅         | 🚫              | ✅                |
| 模拟令牌         | ✅         | ✅              | ✅                |
| 项目访问令牌        | ✅(1)      | ✅(1)           | ✅(1)             |
| 群组访问令牌          | ✅(2)      | ✅(2)           | ✅(2)             |
| 部署令牌                | 🚫         | ✅              | ✅                |
| 部署密钥                  | 🚫         | 🚫              | ✅                |
| Runner 注册令牌   | 🚫         | 🚫              | ✴️(3)              |
| Runner 验证令牌 | 🚫          | 🚫              | ✴️(3)              |
| 作业令牌                   | ✴️(4)       | 🚫              | ✅                |

1. 仅限于一个项目。
1. 仅限于一个群组。
1. Runner 注册和身份验证令牌不提供对仓库的直接访问，但可用于注册和验证一个新 runner，该 runner 可以执行可以访问仓库的作业。
1. 仅限于某些[端点](../ci/jobs/ci_job_token.md)。

## 安全注意事项

访问令牌应被视为密码并保持安全。

将它们添加到 URL 会带来安全风险。在克隆或添加远端时尤其如此，因为 Git 然后将 URL 以纯文本形式写入其 `.git/config` 文件。URL 通常也由代理和应用程序服务器记录，这使得系统管理员可以看到这些凭据。

相反，API 调用可以使用标头传递访问令牌，例如 `Private-Token` header。

也可以使用 [Git 凭据存储](https://git-scm.com/book/en/v2/Git-Tools-Credential-Storage)存储令牌。

令牌不应提交到您的源代码。相反，请考虑一种方法，例如[在 CI 中使用外部机密](../ci/secrets/index.md)。

创建范围令牌时，请考虑使用尽可能有限的范围，以减少意外泄漏令牌的影响。

创建令牌时，请考虑设置一个在任务完成时过期的令牌。例如，如果执行一次性导入，请将令牌设置为几小时或一天后过期。这减少了意外泄露的令牌的影响，因为它在过期时是无用的。
