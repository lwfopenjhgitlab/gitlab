---
type: reference
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 用户文件上传 **(FREE)**

> - 强制授权检查引入于 14.8 版本，[功能标志](../administration/feature_flags.md) 为 `enforce_auth_checks_on_uploads`，默认禁用。
> - 强制授权检查普遍可用于 15.3 版本。功能标志 `enforce_auth_checks_on_uploads` 已删除。
> - 用户界面中的项目设置引入于 15.3 版本。

在私有或内部项目中，极狐GitLab 将上传文件（如 PDF）的访问权限仅限于经过身份验证的用户。默认情况下，图像文件不受相同限制，未经身份验证的用户可以使用 URL 查看文件。如果您为所有媒体文件启用授权检查，则图像会受到相同的保护，并且只能由经过身份验证的用户查看。

用户可以将文件上传到项目中的议题、合并请求或评论。极狐GitLab 中这些图像的直接 URL 包含一个随机的 32 个字符的 ID，以帮助防止未经授权的用户猜测图像 URL。如果图像包含敏感信息，这种随机化提供了一些保护。

图像的身份验证检查可能会导致通知电子邮件正文出现显示问题。
经常从未经极狐GitLab 身份验证的客户端（例如 Outlook、Apple Mail 或您的移动设备）读取电子邮件。如果客户端未获得极狐GitLab 授权，则电子邮件中的图像会出现损坏且不可用。

<a id="enable-authorization-checks-for-all-media-files"></a>

## 为所有媒体文件启用授权检查

非图像附件（包括 PDF）始终需要验证才能查看。
您可以使用此设置将此保护扩展到图像文件。

先决条件：

- 您必须具有项目的维护者或所有者角色。
- 您的项目可见性设置必须是**私有**或**内部**。

为所有媒体文件配置身份验证设置：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **可见性、项目功能、权限**。
1. 滚动到 **项目可见性** 并选择 **需要验证才能查看媒体文件**。您不能为具有**公开**可见性的项目选择此选项。

## 删除已上传的文件

> 引入于 15.3 版本。

当文件包含敏感或机密信息时，您应该删除上传的文件。当您删除该文件后，用户将无法访问该文件，并且直接返回 404 错误。

项目所有者和维护者可以使用[交互式 GraphiQL 资源管理器](../api/graphql/index.md#graphiql)访问 [GraphQL 端点](../api/graphql/reference/index.md#mutationuploaddelete)，删除上传的文件。

例如：

```graphql
mutation{
  uploadDelete(input: { projectPath: "<path/to/project>", secret: "<32-character-id>" , filename: "<filename>" }) {
    upload {
      id
      size
      path
    }
    errors
  }
}
```

没有所有者或维护者角色的项目成员无法访问此 GraphQL 端点。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
