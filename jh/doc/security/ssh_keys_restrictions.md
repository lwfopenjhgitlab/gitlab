---
type: reference, howto
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 限制允许的 SSH 密钥技术和最小长度 **(FREE SELF)**

`ssh-keygen` 允许用户创建少至 768 位的 RSA 密钥，这远低于某些标准组织（例如 US NIST）的建议。一些组织需要强制执行最小密钥强度，以满足内部安全策略或遵从合规。

同样，某些标准组建议在旧的 DSA 上使用 RSA、ECDSA、ED25519、ECDSA_SK 或 ED25519_SK，并且管理员可能需要限制允许的 SSH 密钥算法。

极狐GitLab 允许您限制可用的 SSH 密钥技术以及指定每种技术的最小密钥长度：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 通用** (`/admin/application_settings/general`)。
1. 展开 **可见性与访问控制** 部分：

   ![SSH keys restriction admin settings](img/ssh_keys_restrictions_settings.png)

如果对任何密钥类型施加限制，用户将无法上传不符合要求的新 SSH 密钥。任何不符合它的现有密钥都将被禁用但不会被删除，用户无法使用它们拉取或推送代码。

在其配置文件的 SSH 密钥部分中，受限密钥的用户可以看到一个图标：

![Restricted SSH key icon](img/ssh_keys_restricted_key_icon.png)

将鼠标悬停在此图标上会告诉您密钥受限的原因。

## 默认设置

默认情况下，支持的密钥类型<!--[支持的密钥类型](../ssh/index.md#supported-ssh-key-types)-->为：

- 允许 RSA SSH 密钥
- 禁止 DSA SSH 密钥<!--([since GitLab 11.0](https://about.gitlab.com/releases/2018/06/22/gitlab-11-0-released/#support-for-dsa-ssh-keys)).-->
- 允许 ECDSA SSH 密钥
- 允许 ED25519 SSH 密钥
- 允许 ECDSA_SK SSH 密钥（14.8 及更高版本）
- 允许 ED25519_SK SSH 密钥（14.8 及更高版本）

### 阻止被禁用或泄露的密钥 **(FREE)**

> - 引入于 15.1 版本，功能标志为 `ssh_banned_key`。默认启用。
> - 一般可用于 15.2 版本。功能标志 `ssh_banned_key` 已删除。

当用户尝试[添加新的 SSH 密钥](../user/ssh.md#add-an-ssh-key-to-your-gitlab-account)到极狐GitLab 帐户时，会根据 SSH 列表检查该密钥是否是已知被泄露的密钥。用户无法将此列表中的密钥添加到任何极狐GitLab 帐户。
此限制无法配置。因为当与密钥对关联的私钥是公开的，并且可用于使用该密钥对访问帐户时，需要存在此限制。

如果您的密钥不满足此限制，请[生成新的 SSH 密钥对](../user/ssh.md#generate-an-ssh-key-pair)。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
