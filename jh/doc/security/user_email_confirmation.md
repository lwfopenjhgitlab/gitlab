---
type: howto
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 注册时的用户电子邮件确认 **(FREE SELF)**

极狐GitLab 可以配置为在用户注册时，要求确认用户的电子邮件地址。启用此设置后，用户在确认其电子邮件地址之前无法登录。

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 通用** (`/admin/application_settings/general`)。
1. 展开 **注册限制** 部分并查找 **电子邮件确认设置** 选项。

## 确认令牌到期

默认情况下，用户可以在发送确认电子邮件后 24 小时内确认其帐户。
24 小时后，确认令牌将失效。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
