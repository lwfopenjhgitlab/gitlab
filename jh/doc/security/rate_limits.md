---
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# 速率限制 **(FREE SELF)**

<!--
NOTE:
For GitLab.com, please see
[GitLab.com-specific rate limits](../user/gitlab_com/index.md#gitlabcom-specific-rate-limits).
-->

速率限制是一种用于提高 Web 应用程序的安全性和持久性的常用技术。

例如，一个简单的脚本每秒可以发出数千个 Web 请求。请求可能是：

- 恶意的。
- 冷漠的
- 只是一个 bug。

您的应用程序和基础架构可能无法应对负载。有关更多详细信息，请参阅拒绝服务攻击。
大多数情况可以通过限制来自单个 IP 地址的请求速率来缓解。

大多数蛮力攻击同样可以通过速率限制得到缓解。

## 可配置的限制

您可以在实例的管理中心设置以下速率限制：

- [导入/导出速率限制](../user/admin_area/settings/import_export_rate_limits.md)
- [议题速率限制](../user/admin_area/settings/rate_limit_on_issues_creation.md)
- [备注速率限制](../user/admin_area/settings/rate_limit_on_notes_creation.md)
- [受保护路径](../user/admin_area/settings/protected_paths.md)
- [原始端点速率限制](../user/admin_area/settings/rate_limits_on_raw_endpoints.md)
- [用户和 IP 速率限制](../user/admin_area/settings/user_and_ip_rate_limits.md)
- [软件包库速率限制](../user/admin_area/settings/package_registry_rate_limits.md)
- [Git LFS 速率限制](../user/admin_area/settings/git_lfs_rate_limits.md)
- [Git SSH 操作的速率限制](../user/admin_area/settings/rate_limits_on_git_ssh_operations.md)
- [文件 API 速率限制](../user/admin_area/settings/files_api_rate_limits.md)
- [废弃 API 速率限制](../user/admin_area/settings/deprecated_api_rate_limits.md)
- [极狐GitLab Pages 速率限制](../administration/pages/index.md#rate-limits)
- [流水线速率限制](../user/admin_area/settings/rate_limit_on_pipelines_creation.md)
- [事件管理速率限制](../user/admin_area/settings/incident_management_rate_limits.md)
- [未经身份验证的列出项目 API 的速率限制](../user/admin_area/settings/rate_limit_on_projects_api.md)

您可以使用 Rails 控制台设置以下速率限制：

<!--
- [Webhook rate limit](../administration/instance_limits.md#webhook-rate-limit)
-->

- Webhook 速率限制

## Git 和容器镜像库的身份验证禁止失败

如果在 3 分钟内从单个 IP 地址收到 30 个失败的身份验证请求，极狐GitLab 会在 1 小时内返回 HTTP 状态代码 `403`。这仅适用于：

- Git 请求
- 容器镜像库 (`/jwt/auth`) 请求：

此限制：

- 由成功验证的请求重置。例如，29 个失败的身份验证请求后跟 1 个成功的请求，然后再有 29 个失败的身份验证请求不会触发禁令。
- 不适用于通过 `gitlab-ci-token` 验证的 JWT 请求。
- 默认禁用。

没有提供响应 headers。

<!--
For configuration information, see
[Omnibus GitLab configuration options](https://docs.gitlab.com/omnibus/settings/configuration.html#configure-a-failed-authentication-ban).
-->

<a id="non-configurable-limits"></a>

## 不可配置的限制

### 使用 SSH 的 Git 操作

> - 引入于 14.7 版本。功能标志为 `rate_limit_gitlab_shell`。默认禁用。
> - 在 SaaS 和私有化部署版上启用于 14.8 版本。

极狐GitLab 限制用户帐户和项目使用 SSH 的 Git 操作速率。如果用户对项目的 Git 操作请求超过速率限制，系统会丢弃该用户对该项目的进一步连接请求。

速率限制适用于 Git 命令 ([plumbing](https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain)) 级别。
每个命令的速率限制为每分钟 600 次。 例如：

- `git push` 的速率限制为每分钟 600 次。
- `git pull` 有自己的每分钟 600 次的速率限制。

因为 `git-upload-pack`、`git pull` 和 `git clone` 共享相同的命令，所以它们共享一个速率限制。

此速率限制的每分钟请求阈值不可配置。私有化部署版用户可以通过[禁用功能标志](../administration/feature_flags.md#enable-or-disable-the-feature) `Feature.disable(:rate_limit_gitlab_shell)` 来禁用此速率限制。

### 仓库归档

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/25750) in GitLab 12.9.
-->

<!--[下载仓库归档](../api/repositories.md#get-file-archive)-->下载仓库归档的速率限制可用。该限制适用于项目以及通过 UI 或 API 启动下载的用户。

**速率限制**是每个用户每分钟 5 个请求。

### Webhook 测试

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/commit/35bc85c3ca093fee58d60dacdc9ed1fd9a15adec) in GitLab 13.4.
-->

<!--[测试 webhook](../user/project/integrations/webhooks.md#test-a-webhook)-->测试 webhook 有一个速率限制，可防止滥用 webhook 功能。

**速率限制**是每个用户每分钟 5 个请求。

### 用户注册

> 引入于 14.7 版本。

`/users/sign_up` 端点上的每个 IP 地址都有一个速率限制。这是为了减轻滥用端点的尝试，例如，大量发现正在使用的用户名或电子邮件地址。

**速率限制**是每个 IP 地址每分钟 20 次调用。

### 更新用户名

> 引入于 14.7 版本。

更改用户名的频率有速率限制。这是为了减轻对该功能的滥用。例如，大量发现正在使用的用户名。

**速率限制**为每位登录用户每分钟 10 次调用。

### 用户名存在

> 引入于 14.7 版本。

内部端点 `/users/:username/exists` 有一个速率限制，用于在注册时检查所选用户名是否已被使用。
这是为了减轻滥用的风险，例如大量发现正在使用的用户名。

**速率限制**是每个 IP 地址每分钟 20 次调用。

### 项目作业 API 端点

> 引入于 15.7 版本，[功能标志](../administration/feature_flags.md)为 `ci_enforce_rate_limits_jobs_api`。默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，需要管理员[启用功能标志](../administration/feature_flags.md)  `ci_enforce_rate_limits_jobs_api`。该功能尚未准备好用于生产。

端点 `project/:id/jobs` 有一个强制执行的速率限制，减少检索作业时的超时。

此**速率限制**为每个经过身份验证的用户每分钟进行 600 次调用。

## 故障排查

### Rack Attack 正在将负载均衡器列入黑名单

如果所有流量似乎都来自负载均衡器，Rack Attack 可能会阻止您的负载均衡器。在这种情况下，您必须：

1. <!--[配置 `nginx[real_ip_trusted_addresses]`](https://docs.gitlab.com/omnibus/settings/nginx.html#configuring-gitlab-trusted_proxies-and-the-nginx-real_ip-module)-->配置 `nginx[real_ip_trusted_addresses]`。
   这可以防止用户的 IP 被列为负载均衡器 IP。
1. 将负载均衡器的 IP 地址列入白名单。
1. 重新配置极狐GitLab：

   ```shell
   sudo gitlab-ctl reconfigure
   ```

### 使用 Redis 从 Rack Attack 中删除被阻止的 IP

要删除被禁用的 IP：

1. 在生产日志中查找被禁用的IP：

   ```shell
   grep "Rack_Attack" /var/log/gitlab/gitlab-rails/auth.log
   ```

1. 由于拒绝列表存储在 Redis 中，因此您必须打开 `redis-cli`：

   ```shell
   /opt/gitlab/embedded/bin/redis-cli -s /var/opt/gitlab/redis/redis.socket
   ```

1. 您可以使用以下语法删除块，将 `<ip>` 替换为被列入黑名单的实际 IP：

   ```plaintext
   del cache:gitlab:rack::attack:allow2ban:ban:<ip>
   ```

1. 确认带有 IP 的密钥不再显示：

   ```plaintext
   keys *rack::attack*
   ```

  默认情况下，`keys` 命令禁用。<!--[`keys` command is disabled](https://docs.gitlab.com/omnibus/settings/redis.html#renamed-commands).-->

1. 或者，将 IP 加入白名单<!--[IP 加入白名单](https://docs.gitlab.com/omnibus/settings/configuration.html#configuring-rack-attack)-->，防止它再次被列入黑名单。
