---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# 自定义密码长度限制 **(FREE SELF)**

默认情况下，极狐GitLab 支持以下长度的密码：

- 最少：8 个字符
- 最多：128 个字符

您只能更改最小密码长度。更改最小长度不会影响现有用户密码，不会要求现有用户重置密码以遵守新限制。新的限制仅适用于新用户注册期间和现有用户执行密码重置时。

<a id="modify-minimum-password-length"></a>

## 修改最小密码长度

默认情况下，用户密码长度设置为最少 8 个字符。

使用 GitLab UI 更改最小密码长度：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏，选择 **设置 > 通用**，然后展开 **注册限制**。
1. 输入一个大于或等于 `8` 的 **最小密码长度** 值。
1. 选择 **保存修改**。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
