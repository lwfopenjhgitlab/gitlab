---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 为通过集成身份验证创建的用户生成的密码 **(FREE)**

极狐GitLab 允许用户通过与外部身份验证和授权提供程序集成，来设置帐户。

这些身份验证方法不需要用户为其帐户显式创建密码。
但是，为了保持数据一致性，极狐GitLab 需要所有用户帐户的密码。

对于此类帐户，我们使用 Devise gem 提供的 [`friendly_token`](https://github.com/heartcombo/devise/blob/f26e05c20079c9acded3c0ee16da0df435a28997/lib/devise.rb#L492) 方法生成随机、唯一且安全的密码，并在注册时将其设置为帐户密码。

生成密码的长度是根据设备配置中设置的[最大密码长度](password_length_limits.md#modify-maximum-password-length-using-configuration-file)的值设置的。默认值为 128 个字符。
