---
stage: Plan
group: Product Planning
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 客户关系管理（CRM） **(FREE)**

> - 引入于 14.6 版本，功能标志为 `customer_relations`。默认禁用。
> - 在 14.8 及更高版本，您只能在根群组中创建联系人和组织。
> - 在 SaaS 版和私有化部署版上启用于 15.0 版本。

<!--
FLAG:
On self-managed GitLab, by default this feature is available. To hide the feature, ask an administrator to [disable the feature flag](../../administration/feature_flags.md) named `customer_relations`.
On GitLab.com, this feature is available.
-->

通过客户关系管理 (CRM)，您可以创建联系人（个人）和组织（公司）的记录，并将它们与议题相关联。

只能为根群组创建联系人和组织。

您可以使用联系人和组织将工作与客户联系起来，用于计费和报告目的。

## 权限

| 权限 | 访客            | 报告者 | 开发者、维护者和所有者 |
| ---------- | ---------------- | -------- | -------------------------------- |
| 查看联系人/组织 | | ✓        | ✓ |
| 查看议题联系人 |         | ✓        | ✓ |
| 添加/删除议题联系人 |   | ✓        | ✓ |
| 创建/编辑联系人/组织 | |   | ✓ |

## 启用客户关系管理 (CRM)

客户关系管理功能必须在群组级别启用。如果您的群组还包含子组，并且您想在子组中使用 CRM 功能，则必须为子组启用 CRM 功能。

要在群组或子组中启用客户关系管理：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组或子组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **权限和群组功能** 部分。
1. 选择 **启用客户关系**。
1. 选择 **保存修改**。

<a id="contacts"></a>

## 联系人

### 查看群组关联的联系人

要查看群组的联系人：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏，选择 **客户关系 > 联系人**。

![Contacts list](crm_contacts_v14_10.png)

### 创建联系人

创建联系人：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏，选择 **客户关系 > 联系人**。
1. 选择 **新建联系人**。
1. 填写所有必填字段。
1. 选择 **创建新联系人**。

<!--
You can also [create](../../api/graphql/reference/index.md#mutationcustomerrelationscontactcreate)
contacts using the GraphQL API.
-->

### 编辑联系人

编辑现有联系人：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏，选择 **客户关系 > 联系人**。
1. 在您要编辑的联系人旁边，选择 **编辑** (**{pencil}**）。
1. 编辑必填字段。
1. 选择 **保存修改**。

<!--
You can also [edit](../../api/graphql/reference/index.md#mutationcustomerrelationscontactupdate)
contacts using the GraphQL API.
-->

<a id="organizations"></a>

## 组织

### 查看组织

要查看群组的组织：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **客户关系 > 组织**。

![Organizations list](crm_organizations_v14_10.png)

### 创建组织

要创建组织：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **客户关系 > 组织**。
1. 选择 **新建组织**。
1. 填写所有必填字段。
1. 选择 **创建新组织**。

<!--
You can also [create](../../api/graphql/reference/index.md#mutationcustomerrelationsorganizationcreate)
organizations using the GraphQL API.
-->

### 编辑组织

要编辑现有组织：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **客户关系 > 组织**。
1. 在您要编辑的组织旁边，选择 **编辑**（**{pencil}**）。
1. 编辑必填字段。
1. 选择 **保存修改**。

<!--
You can also [edit](../../api/graphql/reference/index.md#mutationcustomerrelationsorganizationupdate)
organizations using the GraphQL API.
-->

## 议题

如果您使用[服务台](../project/service_desk.md)并从电子邮件创建议题，则议题将关联到与电子邮件发件人和抄送中的电子邮件地址匹配的联系人。

### 查看联系人关联的议题

要查看联系人的议题，请从议题侧边栏中选择联系人，或者：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏，选择 **客户关系 > 联系人**。
1. 在您希望查看关联议题的联系人旁边，选择 **查看议题**（**{issues}**）。

### 查看与组织关联的议题

要查看组织的议题：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **客户关系 > 组织**。
1. 在您希望查看关联议题的组织旁边，选择 **查看议题**（**{issues}**）。

### 查看与议题关联的联系人

您可以在右侧栏中查看与议题关联的联系人。

要查看联系人的详细信息，请将鼠标悬停在联系人姓名上。

![Issue contacts](issue_crm_contacts_v14_6.png)

<!--
You can also view issue contacts using the
[GraphQL](../../api/graphql/reference/index.md#mutationcustomerrelationsorganizationcreate)
API.
-->

### 添加或删除议题联系人

### 将联系人添加到议题

要将联系人添加到议题，请使用 `/add_contacts [contact:address@example.com]` [快速操作](../project/quick_actions.md)。

<!--
You can also add, remove, or replace issue contacts using the
[GraphQL](../../api/graphql/reference/index.md#mutationissuesetcrmcontacts)
API.
-->

### 从议题中删除联系人

要从议题中删除联系人，请使用 `/remove_contacts [contact:address@example.com]` [快速操作](../project/quick_actions.md)。

<!--
You can also add, remove, or replace issue contacts using the
[GraphQL](../../api/graphql/reference/index.md#mutationissuesetcrmcontacts)
API.
-->

## 自动填入联系人 **(FREE SELF)**

> - 引入于 14.8 版本，功能标志为 `contacts_autocomplete`。默认禁用。
> - 在 SaaS 版和私有化部署版上启用于 15.0 版本。
> - 一般可用于 15.2 版本。功能标志 `contacts_autocomplete` 已删除。


当您使用 `/add_contacts` 或 `/remove_contacts` 快速操作时，请使用 `[contact:` 跟随它们，然后会出现一个自动完成列表：

```plaintext
/add_contacts [contact:
/remove_contacts [contact:
```

## 使用 CRM 条目移动对象

根群组是群组层次结构中最顶层的群组。

当您在**同一群组层次结构**内移动议题、项目或群组时，议题会保留其联系人。

当您移动议题或项目并且**根群组更改**时，
议题不再关联联系人。

当您移动一个群组并且其**根群组更改**时：

- 所有唯一的联系人和组织都迁移到新的根群组。
- 已经存在的联系人（按电子邮件地址）被视为重复并删除。
- 已经存在的组织（按名称）被视为重复并删除。
- 所有议题都保留其联系人，或更新指向具有相同电子邮件地址的联系人。

如果您在新的根群组中，没有创建联系人和组织的权限，则群组转移会失败。
