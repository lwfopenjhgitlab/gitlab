---
redirect_to: '../../administration/custom_project_templates.md'
remove_date: '2023-10-10'
---

此文档已移动到[另一个位置](../../administration/custom_project_templates.md)。

<!-- This redirect file can be deleted after <2023-10-10>. -->
<!-- Redirects that point to other docs in the same project expire in three months. -->
<!-- Redirects that point to docs in a different project or site (for example, link is not relative and starts with `https:`) expire in one year. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/redirects.html -->
