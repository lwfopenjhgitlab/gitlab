---
stage: Data Stores
group: Global Search
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 在极狐GitLab 中搜索 **(FREE)**

极狐GitLab 有两种可用的搜索类型：*基本*和*高级*。

两种类型的搜索都是相同的，除非您通过代码进行搜索。

- 当您使用基本搜索来搜索代码时，您的搜索一次包括一个项目。
- 当您使用[高级搜索](advanced_search.md)来搜索代码时，您的搜索会同时包含所有项目。

## 全局搜索范围 **(FREE SELF)**

> 引入于 14.3 版本。

要提高实例全局搜索的性能，您可以限制搜索范围。为此，您可以通过禁用 `ops` 功能标志<!--[`ops` 功能标志](../../development/feature_flags/index.md#ops-type)-->来排除全局搜索范围。

全局搜索在 SaaS 和私有化部署实例中默认启用所有范围。极狐GitLab 管理员可以禁用以下 `ops` 功能标志，限制实例的全局搜索范围并优化其性能：

| 范围 | 功能标志 | 描述 |
|--|--|--|
| 代码 | `global_search_code_tab` | 启用后，全局搜索会将代码作为搜索的一部分。 |
| 提交 | `global_search_commits_tab` | 启用后，全局搜索会将提交作为搜索的一部分。 |
| 议题 | `global_search_issues_tab` | 启用后，全局搜索会将议题作为搜索的一部分。 |
| 合并请求 | `global_search_merge_requests_tab` | 启用后，全局搜索将合并请求作为搜索的一部分。 |
| 用户 | `global_search_users_tab` | 启用后，全局搜索会将用户作为搜索的一部分。 |
| Wiki | `global_search_wiki_tab` | 启用后，全局搜索会将 wiki 作为搜索的一部分。[群组 wiki](../project/wiki/group.md) 不包括在内。 |

## 全局搜索验证

为防止滥用搜索，例如可能导致分布式拒绝服务 (DDoS) 的搜索，全局搜索会根据以下条件忽略、记录并且不返回任何被视为滥用的搜索结果：

- 搜索少于 2 个字符。
- 搜索任何超过 100 个字符的词。URL 搜索词最多包含 200 个字符。
- 使用停止词作为唯一搜索词进行搜索（例如，“the”、“and”、“if” 等）。
- 使用不完全数字的 `group_id` 或 `project_id` 参数进行搜索。
- 使用包含 [Git refname](https://git-scm.com/docs/git-check-ref-format) 不允许的特殊字符的 `repository_ref` 或 `project_ref` 参数进行搜索。
- 使用未知的 `scope` 进行搜索。

不符合下述标准的搜索不会被记录为滥用，但会被标记为错误：

- 搜索超过 4096 个字符。
- 搜索超过 64 个字词。

## 执行搜索

要开始搜索，请在屏幕右上角的搜索栏中输入您的搜索查询。
您必须输入至少两个字符。

![搜索导航栏](img/search_navbar_v15_7.png)

显示结果后，您可以修改搜索，选择不同类型的数据进行搜索，或者选择特定的群组或项目。

![basic_search_results](img/search_scope_v15_7.png)

## 在代码中搜索

在项目中搜索代码或其他文档：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在顶部栏的搜索字段中，键入要搜索的字符串。
1. 按 **进入**。

代码搜索仅显示文件中的第一个结果。

要搜索整个极狐GitLab，请让您的管理员启用[高级搜索](advanced_search.md)。

### 从代码搜索中查看 Git blame

> 引入于 14.7 版本。

找到搜索结果后，您可以查看对结果行进行最后更改的人员。

1. 在代码搜索结果中，将鼠标悬停在行号上。
1. 在左侧，选择 **查看 blame**。

   ![code search results](img/code_search_git_blame_v15_1.png)

## 按完整路径搜索项目

> 引入于 15.9 版本，[功能标志](../../administration/feature_flags.md)为 `full_path_project_search`。默认禁用。

FLAG:
在私有化部署实例上，默认情况下此功能不可用。
要使其可用，请要求管理员[启用功能标志](../../administration/feature_flags.md) `full_path_project_search`。在 JiHuLab.com 上，此功能不可用。

您可以通过在搜索框中输入项目的完整路径（包括其所属的命名空间）来搜索项目。
当您键入项目路径时，会显示[自动完成建议](#autocomplete-suggestions)。

例如，搜索查询：

- `gitlab-cn/gitlab` 在 `gitlab-cn` 命名空间中搜索 `gitlab` 项目。
- `gitlab-cn/` 显示属于 `gitlab-cn` 命名空间的项目的自动完成建议。

## 搜索 SHA

您可以搜索提交 SHA。

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在顶部栏的搜索字段中，输入 SHA。

如果返回单个结果，极狐GitLab 将重定向到提交结果，并为您提供返回搜索结果页面的选项。

![project SHA search redirect](img/project_search_sha_redirect.png)

## 搜索特定字词

您可以按标题或描述中包含的特定字词过滤议题和合并请求。

- 语法
  - 搜索以任意顺序查找查询中的所有单词。例如：搜索议题时使用 `display bug`，会以任意顺序返回与这两个词匹配的所有议题。
  - 要查找确切的搜索词，请使用双引号：`"display bug"`
- 限制
  - 出于性能原因，少于 3 个单词的搜索词将被忽略。例如：搜索议题时，`included in titles` 与 `included titles` 相同。
  - 每个查询的搜索限制为 4096 个字符和 64 个搜索词。
  - 搜索议题时，不支持部分匹配。例如：搜索 `play` 将不会返回包含 `display` 一词的议题。但是单词的变体匹配，因此搜索 `displays` 也会返回包含 `display` 一词的议题。

## 获取搜索结果作为 feed

> 对合并请求的支持引入于 14.3 版本。

极狐GitLab 为您的项目提供搜索结果的 RSS feeds。订阅搜索结果的 RSS feed：

1. 转到您的项目页面。
1. 在左侧边栏中，选择 **议题** 或 **合并请求**。
1. 执行搜索。
1. 选择 feed 符号 **{rss}**，将结果显示为 Atom 格式的 RSS feed。

结果的 URL 包含 feed 令牌和您的搜索查询。
您可以将此 URL 添加到您的 feed 阅读器。

## 搜索历史

搜索历史可用于议题和合并请求，并存储在本地浏览器中。从历史中运行搜索：

1. 在顶部菜单中，选择 **议题** 或 **合并请求**。
1. 在搜索栏的左侧，选择 **最近搜索**，然后从列表中选择一个搜索。

## 删除搜索过滤器

可以通过单击过滤器的 (x) 按钮或键盘删除键，来删除单个过滤器。单击搜索框的 (x) 按钮或通过 <kbd>⌘</kbd> (Mac) + <kbd>⌫</kbd> 可以清除整个搜索过滤器。

要一次删除一个过滤器 token，可以使用 <kbd>⌥</kbd> (Mac) / <kbd>Control</kbd> + <kbd>⌫</kbd> 键盘组合。

<a id="autocomplete-suggestions"></a>

## 自动完成建议

在搜索栏中，您可以查看以下自动完成建议：

- [项目](#search-for-projects-by-full-path)和群组
- 用户
- 各种帮助页面（尝试输入 **API 帮助**）
- 项目功能页面（尝试输入 **里程碑**）
- 各种设置页面（尝试输入 **用户设置**）
- 最近查看的议题（尝试输入最近查看议题的标题中的一些文本）
- 最近查看的合并请求（尝试输入最近查看的合并请求的标题中的一些文本）
- 最近浏览的史诗（尝试输入最近浏览的史诗的标题中的一些文本）
- 用于项目中议题的 [GitLab Flavored Markdown](../markdown.md#gitlab-specific-references) (GLFM)（尝试输入议题的 GLFM 引用）

## 搜索设置

> - 引入于 13.8 版本，[功能标志](../../administration/feature_flags.md)为 `search_settings_in_page`。默认禁用。
> - 添加到群组、管理员或用户设置页面于 13.9 版本。
> - 功能标志 `search_settings_in_page` 删除于 13.11 版本。

通过在页面顶部的搜索框中输入搜索词，您可以在项目、群组、管理员或用户的设置页面中进行搜索。搜索结果在与搜索词匹配的部分中突出显示。

![Search project settings](img/project_search_general_settings_v13_8.png)
