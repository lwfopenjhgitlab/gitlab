---
stage: Data Stores
group: Global Search
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# 极狐GitLab 高级搜索 **(PREMIUM)**

> 移动到专业版于 13.9 版本。

高级搜索使用 Elasticsearch 在整个极狐GitLab 实例中进行更快、更高级的搜索。

在以下位置搜索时使用高级搜索：

- 项目
- 议题
- 合并请求
- 里程碑
- 用户
- 史诗（仅在群组中搜索时）
- 代码
- 评论
- 提交
- Wiki（除了[群组 wikis](../project/wiki/group.md)）

高级搜索在各种情况下都很有用：

- **更快的搜索：**
  高级搜索基于 Elasticsearch，这是一个专门构建的全文搜索引擎，可以水平缩放，因此在大多数情况下它可以在 1-2 秒内提供搜索结果。
- **代码维护：**
  在整个实例中查找需要一次更新的所有代码，可以节省维护代码所花费的时间。这对于拥有超过 10 个活动项目的组织特别有用。这也有助于在代码重构时识别未知影响。
- **提升内部溯源：**
  您的公司可能由许多不同的开发团队组成，每个团队都有自己负责托管各种项目的群组。您的一些应用程序可能相互连接，因此您的开发人员需要立即搜索整个极狐GitLab 实例并找到他们要搜索的代码。

## 配置高级搜索

对于私有化部署实例，管理员必须配置 Elasticsearch 集成<!--[配置高级搜索](../../integration/advanced_search/elasticsearch.md)-->。

在 SaaS 上，高级搜索已启用。

## 高级搜索语法

高级搜索使用[Elasticsearch 语法](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-simple-query-string-query.html#simple-query-string-syntax)，支持精确和模糊搜索查询。高级搜索仅搜索默认项目分支。

<!-- markdownlint-disable -->

| 使用 | 描述 | 示例                                                                                                                                        |
|-----|--------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| `"` | 精确搜索 | `"gem sidekiq"`                   |
| <code>&#124;</code> | 或 | <code>display &#124; banner</code>          |
| `+` | 与          | `display +banner` |
| `-` | 除外      | `display -banner`                              |
| `*` | 部分      | `bug error 50*`       |
| `\` | 转义       | `\*md`         |
| `#`          | 议题 ID                        | `#23456`               |
| `!`          | 合并请求 ID                | `!23456`       |

## 代码搜索

| 使用          | 描述                    | 示例                                                                                                                                              |
|--------------|---------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| `filename:`  | 文件名                        | `filename:*spec.rb`    |
| `path:`      | 仓库位置             | `path:spec/workers/` |
| `extension:` | 文件扩展名，不包含 `.` | `extension:js`              |
| `blob:`      | Git 对象 ID                   | `blob:998707*`                           |

`extension` 和 `blob` 只返回完全匹配。

## 示例

| 示例                                                                                                                                                                             | 描述                                                          |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------|
| `rails -filename:gemfile.lock`              | 在除 *`gemfile.lock`* 文件之外的所有文件中显示 *rails*。          |
| `RSpec.describe Resolvers -*builder`                              | 显示所有不以 *builder* 开头的 *RSpec.describe Resolvers*。 |
| <code>bug &#124; (display +banner)</code>  | 显示 *bug*，**或** *display* **和** *banner*。                        |
