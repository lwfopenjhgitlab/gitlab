---
type: reference, howto
stage: Secure
group: Composition Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# CycloneDX 文件的许可证扫描 **(ULTIMATE)**

> - 在 SaaS 上引入于 15.9 版本，[功能标志](../../../administration/feature_flags.md) 为 `license_scanning_sbom_scanner` 和 `package_metadata_synchronization`。默认情况下禁用这两个标志，必须启用这两个标志才能使此功能生效。
> - 在 SaaS 上启用于 15.10 版本。
> - 在私有化部署版上引入于 15.10 版本，[功能标志](../../../administration/feature_flags.md) 为 `license_scanning_sbom_scanner` 和 `package_metadata_synchronization`，必须启用这两个标志才能使此功能生效，由于首次导入许可证数据库时的初始性能负载，这些标志在默认情况下处于禁用状态。
> - 在私有化部署版上默认启用于 15.11 版本。

为了检测正在使用的许可证，许可证合规依赖于运行 [Dependency Scanning CI 作业](../../application_security/dependency_scanning/index.md)，并分析这些作业生成的 [CycloneDX](https://cyclonedx.org/) 软件物料清单（SBOM）。

您也可以使用其他第三方扫描器，只要它们生成一个 CycloneDX 文件，其中包含[我们支持的一种语言](#supported-languages-and-package-managers)的依赖项列表。
这种扫描方法还能够解析和识别 500 多种不同类型的许可证，并可以从双重许可或具有多个不同许可证的包中提取许可证信息。

## 启用许可证扫描

先决条件：

- 在极狐GitLab 实例的管理中心，启用[与极狐GitLab 许可证数据库同步](../../admin_area/settings/security_and_compliance.md#choose-package-registry-metadata-to-sync)。
- 启用[依赖扫描](../../application_security/dependency_scanning/index.md#configuration)并确保满足其先决条件。

在 `.gitlab-ci.yml` 文件中，删除不推荐使用的行 `Jobs/License-Scanning.gitlab-ci.yml`（如果存在）。

<a id="supported-languages-and-package-managers"></a>

## 支持的语言和包管理器

以下语言和包管理器支持许可证扫描：

<!-- markdownlint-disable MD044 -->
<table class="supported-languages">
  <thead>
    <tr>
      <th>Language</th>
      <th>Package Manager</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>.NET</td>
      <td rowspan="2"><a href="https://www.nuget.org/">NuGet</a></td>
    </tr>
    <tr>
      <td>C#</td>
    </tr>
    <tr>
      <td>C</td>
      <td rowspan="2"><a href="https://conan.io/">Conan</a></td>
    </tr>
    <tr>
      <td>C++</td>
    </tr>
    <tr>
      <td>Go</td>
      <td><a href="https://go.dev/">Go</a></td>
    </tr>
    <tr>
      <td rowspan="2">Java</td>
      <td><a href="https://gradle.org/">Gradle</a></td>
    </tr>
    <tr>
      <td><a href="https://maven.apache.org/">Maven</a></td>
    </tr>
    <tr>
      <td rowspan="3">JavaScript and TypeScript</td>
      <td><a href="https://www.npmjs.com/">npm</a></td>
    </tr>
    <tr>
      <td><a href="https://pnpm.io/">pnpm</a></td>
    </tr>
    <tr>
      <td><a href="https://classic.yarnpkg.com/en/">yarn</a></td>
    </tr>
    <tr>
      <td>PHP</td>
      <td><a href="https://getcomposer.org/">Composer</a></td>
    </tr>
    <tr>
      <td rowspan="4">Python</td>
      <td><a href="https://setuptools.readthedocs.io/en/latest/">setuptools</a></td>
    </tr>
    <tr>
      <td><a href="https://pip.pypa.io/en/stable/">pip</a></td>
    </tr>
    <tr>
      <td><a href="https://pipenv.pypa.io/en/latest/">Pipenv</a></td>
    </tr>
    <tr>
      <td><a href="https://python-poetry.org/">Poetry</a></td>
    </tr>
    <tr>
      <td>Ruby</td>
      <td><a href="https://bundler.io/">Bundler</a></td>
    </tr>
    <tr>
      <td>Scala</td>
      <td><a href="https://www.scala-sbt.org/">sbt</a></td>
    </tr>
  </tbody>
</table>
<!-- markdownlint-disable MD044 -->

支持的文件和版本也是 [Dependency Scanning](../../application_security/dependency_scanning/index.md#supported-languages-and-package-managers) 支持的文件和版本。

## 许可证表达式

极狐GitLab 对[复合许可证](https://spdx.github.io/spdx-spec/v2-draft/SPDX-license-expressions/)的支持有限。
许可证合规可以读取多个许可证，但始终将它们视为使用 `AND` 运算符组合在一起。例如，一个依赖项有两个许可证，其中一个被允许，另一个被项目[策略](../license_approval_policies.md)拒绝，那么极狐GitLab 将复合许可证定为拒绝，因为这是更安全的选择。

<!--
The ability to support other license expression operators (like `OR`, `WITH`) is tracked
in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/6571).
-->

## 根据检测到的许可证阻止合并请求

用户可以要求合并请求的批准，要基于通过[许可证批准策略](../license_approval_policies.md)检测过的许可证。
