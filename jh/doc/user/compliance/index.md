---
type: reference
stage: Manage
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 合规 **(ULTIMATE)**

极狐GitLab 提供的合规工具可帮助您密切关注项目的各个方面。

- [合规报告](compliance_report/index.md)
- [许可证批准策略](license_approval_policies.md)
- [许可证列表](license_list.md)
- [CycloneDX 文件的许可证扫描](license_scanning_of_cyclonedx_files/index.md)

有关项目、群组和实例的其他极狐GitLab 合规功能的更多信息，请参阅[合规功能](../../administration/compliance.md)文档。
