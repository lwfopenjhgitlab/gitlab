---
type: reference, howto
stage: Secure
group: Composition Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 许可证合规（已废弃） **(ULTIMATE)**

WARNING:
此功能废弃于 15.9 版本。您应该在 16.1 版本之前迁移，使用[许可证批准策略](../license_approval_policies.md)和[许可证扫描的新方法](../license_scanning_of_cyclonedx_files/index.md)作为代替。

如果您使用[极狐GitLab CI/CD](../../../ci/index.md)，您可以使用许可证合规（License Compliance）搜索项目的依赖项来获取其许可证。然后，您可以决定是允许还是拒绝使用每个许可证。例如，您的应用程序使用了一个与您的许可证不兼容的外部（开源）库，那么您可以拒绝使用该许可证。

为了检测正在使用的许可证，许可证合规使用作为 CI/CD 流水线的一部分运行的 [License Finder](https://github.com/pivotal/LicenseFinder) 扫描工具。许可证合规作业不依赖于流水线中的任何其他作业。

对于要激活的作业，License Finder 需要在项目目录中找到兼容的包定义。有关详细信息，请参阅 [License Finder 文档中的激活部分](https://github.com/pivotal/LicenseFinder#activation)。
极狐GitLab 检查许可证合规性报告，比较源分支和目标分支之间的许可证，并在合并请求上显示信息权。被拒绝的许可证旁边会显示一个 `x` 红色图标，包括需要您做出决定的新许可证。此外，您可以在项目的安全策略部分[手动允许或拒绝](../license_approval_policies.md)许可证。如果在新提交中检测到被拒绝的许可证，极狐GitLab 会阻止任何包含该提交的合并请求，并指示开发人员删除许可证。

NOTE:
如果许可证合规报告没有可比较的内容，则合并请求区域中不会显示任何信息。当您第一次在 `.gitlab-ci.yml` 中添加 `license_scanning` 作业时就是这种情况。连续的合并请求有一些可比较的东西，并且许可证合规性报告正确显示。

结果保存为[许可证合规性报告产物](../../../ci/yaml/artifacts_reports.md#artifactsreportslicense_scanning)，您可以稍后下载和分析。

WARNING:
许可证合规扫描不支持编译器和解释器的运行时安装。

![License Compliance Widget](img/license_compliance_v13_0.png)

您可以选择许可证来查看更多信息。

当系统检测到**已拒绝**的许可证时，您可以在[许可列表](#license-list)中查看它。

![License List](img/license_list_v13_0.png)

您可以从[策略](#policies)选项卡查看和修改现有策略。

![Edit Policy](img/policies_maintainer_edit_v14_3.png)

<a id="enable-license-compliance"></a>

## 启用许可证合规

要在项目的流水线中启用许可证合规，请执行以下任一操作：

- 启用 [Auto License Compliance](../../../topics/autodevops/stages.md#auto-license-compliance)（由 [Auto DevOps](../../../topics/autodevops/index.md) 提供）。
- 在您的 `.gitlab-ci.yml` 文件中，包含 [`License-Scanning.gitlab-ci.yml` 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/License-Scanning.gitlab-ci.yml)。

请注意，在启用 FIPS 的情况下运行极狐GitLab 时，不支持许可证合规。

<a id="include-the-license-scanning-template"></a>

### 包含许可证扫描模板

先决条件：

- [极狐GitLab Runner](../../../ci/runners/index.md) 可用，带有 [`docker` 执行器](https://docs.gitlab.cn/runner/executors/docker.html)。如果您在 JihuLab.com 上使用共享 runner，则默认启用此功能。
- 许可证扫描在 `test` 阶段运行，默认情况下可用。如果在 `.gitlab-ci.yml` 文件中重新定义阶段，则需要 `test` 阶段。
- FIPS 模式必须禁用。

[包含](../../../ci/yaml/index.md#includetemplate) [`License-Scanning.gitlab-ci.yml` 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/License-Scanning.gitlab-ci.yml)，将它添加到您的 `.gitlab-ci.yml` 文件中：

```yaml
include:
  - template: Security/License-Scanning.gitlab-ci.yml
```

包含的模板会在您的 CI/CD 流水线中创建一个 `license_scanning` 作业，并扫描您的依赖项来找到它们的许可证。

## 许可证表示

系统对[复合许可证](https://spdx.github.io/spdx-spec/SPDX-license-expressions/)的支持有限。许可证合规可以读取多个许可证，但始终使用 `AND` 运算符将它们组合在一起。例如一个依赖项有两个许可证，其中一个被允许，另一个被项目的[许可证批准策略](../license_approval_policies.md)拒绝，系统将复合许可证评估为拒绝，因为这是更安全的选择。

<!--
The ability to support other license expression operators (like `OR`, `WITH`) is tracked
in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/6571).
-->

<a id="supported-languages-and-package-managers"></a>

## 支持的语言和包管理器

支持以下语言和包管理器。

不支持 Gradle 1.x 项目。支持的最低 Maven 版本是 3.2.5。

| 语言   | 包管理器                                                                             | 备注 |
|------------|----------------------------------------------------------------------------------------------|-------|
| JavaScript | [Bower](https://bower.io/)、[npm](https://www.npmjs.com/) (7 或更早版本)                    |       |
| Go         | [Godep](https://github.com/tools/godep)（已废弃）、[go mod](https://github.com/golang/go/wiki/Modules) |       |
| Java       | [Gradle](https://gradle.org/) <sup>1</sup>、[Maven](https://maven.apache.org/)                            |       |
| .NET       | [NuGet](https://www.nuget.org/)                                                              | 通过 [mono 项目](https://www.mono-project.com/) 支持 .NET 框架。但是，有一些限制。扫描程序不支持特定于 Windows 的依赖项，并且不报告项目列出的依赖项。此外，扫描程序始终将检测到的所有依赖项的许可证标记为 `unknown`。 |
| Python     | [pip](https://pip.pypa.io/en/stable/)                                                        | 通过 [requirements.txt](https://pip.pypa.io/en/stable/user_guide/#requirements-files) 和 [Pipfile.lock](https://github.com/pypa/pipfile#pipfilelock) 支持 Python。 |
| Ruby       | [gem](https://rubygems.org/) |  |

1. 不支持 Gradle 7 及更高版本，因为包含在 `implementation` 指令中时不会发现依赖项。<!--Please see [GitLab#341222](https://gitlab.com/gitlab-org/gitlab/-/issues/341222) for more details.-->

### 实验支持

以下语言和包管理器属于[实验性支持](https://github.com/pivotal/LicenseFinder#experimental-project-types)。
报告的许可证可能不完整或不准确。

| 语言   | 包管理器                                                                                             |
|------------|---------------------------------------------------------------------------------------------------------------|
| JavaScript | [Yarn](https://yarnpkg.com/)                                                                                  |
| Go         | `go get`、`gvt`、`glide`、`dep`、`trash`、`govendor`                                                          |
| Erlang     | [Rebar](https://rebar3.org/)                                                                                  |
| Objective-C、Swift | [Carthage](https://github.com/Carthage/Carthage)、[CocoaPods](https://cocoapods.org/) v0.39 及更低版本 |
| Elixir     | [Mix](https://elixir-lang.org/getting-started/mix-otp/introduction-to-mix.html)                               |
| C++/C      | [Conan](https://conan.io/)                                                                                    |
| Rust       | [Cargo](https://crates.io/)                                                                                   |
| PHP        | [Composer](https://getcomposer.org/)                                                                          |

<a id="available-cicd-variables"></a>

## 可用的 CI/CD 变量

可以使用 CI/CD 变量配置许可证合规。

| CI/CD 变量             | 是否必需 | 描述 |
|-----------------------------|----------|-------------|
| `ADDITIONAL_CA_CERT_BUNDLE` | no       | 可信 CA 证书捆绑包（目前在 Pip、Pipenv、Maven、Gradle、Yarn 和 npm 项目中支持）。 |
| `ASDF_JAVA_VERSION`         | no       | 用于扫描的 Java 版本。 |
| `ASDF_NODEJS_VERSION`       | no       | 用于扫描的 Node.js 版本。 |
| `ASDF_PYTHON_VERSION`       | no       | 用于扫描的 Python 版本。参考[配置](#selecting-the-version-of-python)。 |
| `ASDF_RUBY_VERSION`         | no       | 用于扫描的 Ruby 版本。 |
| `GRADLE_CLI_OPTS`           | no       | Gradle 可执行文件的附加参数。如果未提供，则默认为 `--exclude-task=test`。 |
| `LICENSE_FINDER_CLI_OPTS`   | no       | `license_finder` 可执行文件的附加参数。例如，您在嵌套目录中有多个项目，则可以更新 `.gitlab-ci.yml` 模板来指定递归扫描，例如 `LICENSE_FINDER_CLI_OPTS: '--recursive'`。 |
| `LM_JAVA_VERSION`           | no       | Java 版本。如果设置为 `11`，Maven 和 Gradle 使用 Java 11 而不是 Java 8。参考[配置](#selecting-the-version-of-java)。 |
| `LM_PYTHON_VERSION`         | no       | Python 版本。如果设置为 `3`，则使用 Python 3 而不是 Python 2.7 安装依赖项。参考[配置](#selecting-the-version-of-python)。 |
| `MAVEN_CLI_OPTS`            | no       | `mvn` 可执行文件的附加参数。如果未提供，则默认为 `-DskipTests`。 |
| `PIP_INDEX_URL`             | no       | Python 包索引的基本 URL（默认值：`https://pypi.org/simple/`）。 |
| `SECURE_ANALYZERS_PREFIX`   | no       | 设置要从中下载分析器的 Docker 镜像库基础地址。 |
| `SETUP_CMD`                 | no       | 依赖项安装的自定义设置（实验性）。 |

## 安装自定义依赖项

`license_finder` 镜像已经嵌入了许多自动检测脚本、语言和包。然而，几乎不可能涵盖所有项目的所有案例。
这就是为什么有时需要安装额外的包，或者在项目自动设置中有额外的步骤，比如下载和安装证书。
为此，可以将 `SETUP_CMD` CI/CD 变量传递给容器，并在许可证检测之前运行所需的命令。

如果存在，此变量将覆盖安装应用程序的所有包所需的设置步骤（例如：对于具有 `Gemfile` 的项目，设置步骤可以是 `bundle install`）。

例如：

```yaml
include:
  - template: Security/License-Scanning.gitlab-ci.yml

variables:
  SETUP_CMD: sh my-custom-install-script.sh
```

在本例中，`my-custom-install-script.sh` 是项目根目录中的一个 shell 脚本。

## 使用 Monorepos

根据您的语言，您可能需要使用 `LICENSE_FINDER_CLI_OPTS` 变量指定单个项目的路径。与使用 `--recursive` license_finder 选项相比，传入项目路径可以显着加快构建速度。

```yaml
include:
  - template: Security/License-Scanning.gitlab-ci.yml

variables:
  LICENSE_FINDER_CLI_OPTS: "--aggregate_paths=relative-path/to/sub-project/one relative-path/to/sub-project/two"
```

## 覆盖模板

WARNING:
从 13.0 版本开始，不再支持使用 [`only` 和 `except`](../../../ci/yaml/index.md#only--except)。覆盖模板时，您必须改用 [`rules`](../../../ci/yaml/index.md#rules)。

如果您想覆盖作业定义（例如，更改 `variables` 或 `dependencies` 等属性），则需要在模板包含后声明 `license_scanning` 作业并在其下指定任何其他键。例如：

```yaml
include:
  - template: Security/License-Scanning.gitlab-ci.yml

license_scanning:
  variables:
    CI_DEBUG_TRACE: "true"
```

## 配置 Maven 项目

License Compliance 工具提供了一个`MAVEN_CLI_OPTS` CI/CD 变量，它可以保存命令行参数以传递给在后台执行的 `mvn install` 命令。随意使用它来定制 Maven 执行。例如：

```yaml
include:
  - template: Security/License-Scanning.gitlab-ci.yml

license_scanning:
  variables:
    MAVEN_CLI_OPTS: --debug
```

在[构建生命周期](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html)中，`mvn install` 运行在 `install` 之前的所有阶段，包括 `test`。对于许可证扫描，运行单元测试并不是直接必要的并且会消耗时间，因此通过将 `MAVEN_CLI_OPTS` 的默认值设置为 `-DskipTests` 来跳过它。如果您想提供自定义的 `MAVEN_CLI_OPTS` 并同时跳过测试，不要忘记在您的选项中明确添加 `-DskipTests`。
如果在 `mvn install` 期间仍需要运行测试，请将 `-DskipTests=false` 添加到 `MAVEN_CLI_OPTS`。

### 使用私有 Maven 仓库

如果您有一个需要登录凭据的私有 Maven 仓库，您可以使用 `MAVEN_CLI_OPTS` CI/CD 变量。

了解[如何使用私有 Maven 仓库](../../application_security/index.md#using-private-maven-repositories)。

您还可以使用 `MAVEN_CLI_OPTS`，连接到使用自签名或内部信任证书的受信任 Maven 仓库。例如：

```yaml
include:
  - template: Security/License-Scanning.gitlab-ci.yml

license_scanning:
  variables:
    MAVEN_CLI_OPTS: -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true -Dmaven.wagon.http.ssl.insecure=true
```

或者，您可以使用 Java 密钥库来验证 TLS 连接。有关如何生成密钥存储文件的说明，请参阅 [Maven 使用经过身份验证的 HTTPS 访问远端仓库的指南](https://maven.apache.org/guides/mini/guide-repository-ssl.html)。

<a id="selecting-the-version-of-java"></a>

## 选择 Java 版本

许可证合规默认使用 Java 8。您可以使用 `LM_JAVA_VERSION` 指定不同的 Java 版本。

`LM_JAVA_VERSION` 只接受版本：8、11、14、15。

<a id="selecting-the-version-of-python"></a>

## 选择 Python 版本

许可证合规默认使用 Python 3.8 和 pip 19.1。
如果您的项目需要 Python 2，您可以通过将 `LM_PYTHON_VERSION` CI/CD 变量设置为 `2`，来切换到 Python 2.7 和 pip 10.0。

```yaml
include:
  - template: Security/License-Scanning.gitlab-ci.yml

license_scanning:
  variables:
    LM_PYTHON_VERSION: 2
```

`LM_PYTHON_VERSION` 或 `ASDF_PYTHON_VERSION` 可用于指定所需的 Python 版本。当两个变量都指定时，`LM_PYTHON_VERSION` 优先。

## Python 的自定义根证书

您可以使用 `ADDITIONAL_CA_CERT_BUNDLE` [CI/CD 变量](#available-cicd-variables)，提供自定义根证书来完成 TLS 验证。

### 使用私有 Python 仓库

如果您有一个私有 Python 仓库，您可以使用 `PIP_INDEX_URL` [CI/CD 变量](#available-cicd-variables)来指定它的位置。

## 配置 npm 项目

您可以使用 [`.npmrc`](https://docs.npmjs.com/configuring-npm/npmrc.html/) 文件配置 npm 项目。

### 使用私有 npm 仓库

如果您有一个私有的 npm 仓库，您可以使用 [`registry`](https://docs.npmjs.com/using-npm/config/#registry)
设置指定其位置。

例如：

```plaintext
registry = https://npm.example.com
```

### npm 的自定义根证书

您可以使用 `ADDITIONAL_CA_CERT_BUNDLE` [CI/CD 变量](#available-cicd-variables)提供自定义根证书来完成 TLS 验证。

要禁用 TLS 验证，您可以提供 [`strict-ssl`](https://docs.npmjs.com/using-npm/config/#strict-ssl) 设置。

例如：

```plaintext
strict-ssl = false
```

## 配置 Yarn 项目

您可以使用 [`.yarnrc.yml`](https://yarnpkg.com/configuration/yarnrc/) 文件配置 Yarn 项目。

### 使用私有 Yarn 仓库

如果您有一个私有的 Yarn 镜像库，您可以使用 [`npmRegistryServer`](https://yarnpkg.com/configuration/yarnrc/#npmRegistryServer) 设置来指定它的位置。

例如：

```plaintext
npmRegistryServer: "https://npm.example.com"
```

### Yarn 的自定义根证书

您可以使用 `ADDITIONAL_CA_CERT_BUNDLE` [CI/CD 变量](#available-cicd-variables)提供自定义根证书来完成 TLS 验证。

## 配置 Bower 项目

您可以使用 [`.bowerrc`](https://bower.io/docs/config/#bowerrc-specification) 文件配置 Bower 项目。

### 使用私有 Bower 仓库

如果您有一个私有的 Bower 仓库，您可以使用 [`registry`](https://bower.io/docs/config/#bowerrc-specification) 设置来指定它的位置。

例如：

```plaintext
{
  "registry": "https://registry.bower.io"
}
```

### Bower 的自定义根证书

您可以使用 `ADDITIONAL_CA_CERT_BUNDLE` [CI/CD 变量](#available-cicd-variables) 或通过 [`.bowerrc`](https://bower.io/docs/config/#bowerrc-specification) 文件。

## 配置 Bundler 项目

### 使用私有 Bundler 仓库

如果您有一个私有的 Bundler 仓库，您可以使用 [`source`](https://bundler.io/man/gemfile.5.html#GLOBAL-SOURCES) 设置来指定它的位置。

例如：

```plaintext
source "https://gems.example.com"
```

### Bundler 的自定义根证书

您可以使用 `ADDITIONAL_CA_CERT_BUNDLE` [CI/CD 变量](#available-cicd-variables)或指定 [`BUNDLE_SSL_CA_CERT`](https://bundler.io/v2.0/man/bundle-config.1.html) 提供自定义根证书，在作业定义中完成 TLS 验证[变量](../../../ci/variables/index.md#custom-cicd-variables)。

## 配置 Cargo 项目

### 使用私有 Cargo 仓库

如果您有一个私有的 Cargo 仓库，您可以使用 [`registries`](https://doc.rust-lang.org/cargo/reference/registries.html) 设置来指定它的位置。

例如：

```toml
[registries]
my-registry = { index = "https://my-intranet:8080/git/index" }
```

### Cargo 的自定义根证书

要提供自定义根证书完成 TLS 验证，请执行以下操作之一：

- 使用 `ADDITIONAL_CA_CERT_BUNDLE` [CI/CD 变量](#available-cicd-variables)。
- 在作业定义中，指定一个 [`CARGO_HTTP_CAINFO`](https://doc.rust-lang.org/cargo/reference/environment-variables.html) [变量](../../../ci/variables/index.md#custom-cicd-variables)。

## 配置 Composer 项目

### 使用私有 Composer 仓库

如果您有一个私有的 Composer 仓库，可以使用 [`repositories`](https://getcomposer.org/doc/05-repositories.md) 设置来指定它的位置。

例如：

```json
{
  "repositories": [
    { "packagist.org": false },
    {
      "type": "composer",
      "url": "https://composer.example.com"
    }
  ],
  "require": {
    "monolog/monolog": "1.0.*"
  }
}
```

### Composer 的自定义根证书

您可以使用 `ADDITIONAL_CA_CERT_BUNDLE` [CI/CD 变量](#available-cicd-variables) 或在作业定义中指定 [`COMPOSER_CAFILE`](https://getcomposer.org/doc/03-cli.md#composer-cafile) [变量](../../../ci/variables/index.md#custom-cicd-variables)提供自定义根证书来完成 TLS 验证。

## 配置 Conan 项目

您可以通过将 `.conan` 目录添加到项目根目录来配置 [Conan](https://conan.io/) 项目。项目根目录用作 [`CONAN_USER_HOME`](https://docs.conan.io/en/latest/reference/env_vars.html#conan-user-home)。

有关可以应用的设置列表，请参阅 [Conan](https://docs.conan.io/en/latest/reference/config_files/conan.conf.html#conan-conf) 文档。

`license_scanning` 作业在 [Debian 10](https://www.debian.org/releases/buster/) Docker 镜像中运行。提供的镜像附带一些构建工具，例如 [CMake](https://cmake.org/) 和 [GCC](https://gcc.gnu.org/)。
但是，并非默认支持所有项目类型。要安装编译依赖项所需的其他工具，请使用 [`before_script`](../../../ci/yaml/index.md#before_script) 安装使用 [`apt`]( https://wiki.debian.org/PackageManagementTools) 的包管理器。如需完整列表，请参阅 [Conan 文档](https://docs.conan.io/en/latest/introduction.html#all-platforms-all-build-systems-and-compilers)。

默认 [Conan](https://conan.io/) 配置设置 [`CONAN_LOGIN_USERNAME`](https://docs.conan.io/en/latest/reference/env_vars.html#conan-login-username-conan-login-username-remote-name) 到 `ci_user`，并绑定 [`CONAN_PASSWORD`](https://docs.conan.io/en/latest/reference/env_vars.html#conan-password-conan-password-remote-name) 到正在运行的作业的 [`CI_JOB_TOKEN`](../../../ci/variables/predefined_variables.md)。

如果在 `.conan/remotes.json` 文件中指定了极狐GitLab 远端，允许 Conan 项目从极狐GitLab Conan 仓库中获取包。

要覆盖默认凭据，请指定 [`CONAN_LOGIN_USERNAME_{REMOTE_NAME}`](https://docs.conan.io/en/latest/reference/env_vars.html#conan-login-username-conan-login-username-remote-name) 匹配 `.conan/remotes.json` 文件中指定的远端名称。

NOTE:
[MSBuild](https://github.com/mono/msbuild#microsoftbuild-msbuild) 项目不受支持。`license_scanning` 镜像随 [Mono](https://www.mono-project.com/) 和 [MSBuild](https://github.com/mono/msbuild#microsoftbuild-msbuild) 一起提供。
可能需要额外的设置来为此项目配置构建包。

### 使用私有 Conan 仓库

默认情况下，[Conan](https://conan.io/) 使用 `conan-center` 远端。例如：

```json
{
 "remotes": [
  {
   "name": "conan-center",
   "url": "https://conan.bintray.com",
   "verify_ssl": true
  }
 ]
}
```

要从备用远端获取依赖项，请在 `.conan/remotes.json` 中指定该远端。例如：

```json
{
 "remotes": [
  {
   "name": "gitlab",
   "url": "https://gitlab.com/api/v4/packages/conan",
   "verify_ssl": true
  }
 ]
}
```

如果需要凭据来进行身份验证，那么您可以按照[`CONAN_LOGIN_USERNAME` 文档](https://docs.conan.io/en/latest/reference/env_vars.html#conan-login-username-conan-login-username-remote-name) 中描述的命名约定配置[受保护的 CI/CD 变量](../../../ci/variables/index.md#protected-cicd-variables)。

### Conan 的自定义根证书

您可以通过将 `.conan/cacert.pem` 文件添加到项目根目录，并设置 [`CA_CERT_PATH`](https://docs.conan.io/en/latest/reference/env_vars.html#conan-cacert-path) 来提供自定义证书到 `.conan/cacert.pem`。

如果您指定 `ADDITIONAL_CA_CERT_BUNDLE` [CI/CD 变量](#available-cicd-variables)，此变量的 X.509 证书将安装在 Docker 映像的默认信任库中，并且 Conan 配置为将其用作默认的 `CA_CERT_PATH`。

## 配置 Go 项目

要配置基于 [Go 模块](https://github.com/golang/go/wiki/Modules)的项目，请在 `.gitlab-ci.yml` 的 `license_scanning` 作业的[变量](#available-cicd-variables)部分，指定 [CI/CD 变量](https://pkg.go.dev/cmd/go#hdr-Environment_variables)。

如果一个项目已经[供应了它的模块](https://pkg.go.dev/cmd/go#hdr-Vendor_Directories)，那么 `vendor` 目录和 `mod.sum` 文件的组合用于检测与 Go 模块依赖项相关的软件许可证。

### 使用私有 Go 仓库

您可以使用 [`GOPRIVATE`](https://pkg.go.dev/cmd/go#hdr-Environment_variables) 和 [`GOPROXY`](https://pkg.go.dev/cmd/go#hdr-Environment_variables) 环境变量来控制模块的来源。或者，您可以使用 [`go mod vendor`](https://go.dev/ref/mod#tmp_28) 来提供项目的模块。

### Go 的自定义根证书

您可以通过导出 [`GOFLAGS`](https://pkg.go.dev/cmd) 来指定 [`-insecure`](https://pkg.go.dev/cmd/go/internal/get/go#hdr-Environment_variables) 标志环境变量。 例如：

```yaml
include:
  - template: Security/License-Scanning.gitlab-ci.yml

license_scanning:
  variables:
    GOFLAGS: '-insecure'
```

### 使用私有 NuGet 仓库

如果您有私有 NuGet 镜像库，则可以将其添加到 [`nuget.config`](https://learn.microsoft.com/en-us/nuget/reference/nuget-config-file) 文件的 [`packageSources`](https://learn.microsoft.com/en-us/nuget/reference/nuget-config-file#package-source-sections) 部分。

例如：

```xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <packageSources>
    <clear />
    <add key="custom" value="https://nuget.example.com/v3/index.json" />
  </packageSources>
</configuration>
```

### NuGet 的自定义根证书

您可以使用 `ADDITIONAL_CA_CERT_BUNDLE` [CI/CD 变量](#available-cicd-variables) 提供自定义根证书以完成 TLS 验证。

<!--
### 从 `license_management` 迁移到 `license_scanning`

WARNING:
`license_management` 作业在 12.8 中已废弃。`License-Management.gitlab-ci.yml` 模板已从 14.0 中删除。

In GitLab 12.8 a new name for `license_management` job was introduced. This change was made to improve clarity around the purpose of the scan, which is to scan and collect the types of licenses present in a projects dependencies.
GitLab 13.0 drops support for `license_management`.
If you're using a custom setup for License Compliance, you're required
to update your CI configuration accordingly:

1. Change the CI template to `License-Scanning.gitlab-ci.yml`.
1. Change the job name to `license_scanning` (if you mention it in `.gitlab-ci.yml`).
1. Change the artifact name to `license_scanning`, and the filename to `gl-license-scanning-report.json` (if you mention it in `.gitlab-ci.yml`).

For example, the following `.gitlab-ci.yml`:

```yaml
include:
  - template: License-Management.gitlab-ci.yml

license_management:
  artifacts:
    reports:
      license_management: gl-license-management-report.json
```

Should be changed to:

```yaml
include:
  - template: Security/License-Scanning.gitlab-ci.yml

license_scanning:
  artifacts:
    reports:
      license_scanning: gl-license-scanning-report.json
```

If you use the `license_management` artifact in GitLab 13.0 or later, the License Compliance job generates this error:

```plaintext
WARNING: Uploading artifacts to coordinator... failed id=:id responseStatus=400 Bad Request status=400 Bad Request token=:sha

FATAL: invalid_argument
```

If you encounter this error, follow the instructions described in this section.
-->

## 在离线环境中运行许可证合规

对于通过 Internet 对外部资源进行有限、受限或间歇性访问的环境中的私有化部署实例，许可证合规作业需要进行一些调整才能成功运行。有关详细信息，请参阅[离线环境](../../application_security/offline_deployments/index.md)。

### 离线许可证合规的要求

要在离线环境中使用许可证合规，您需要：

- 满足标准[许可证合规先决条件](#include-the-license-scanning-template)。
- 具有本地可用的许可证合规分析器镜像副本的 Docker 容器镜像库。

NOTE:
极狐GitLab Runner 有一个[默认 `pull policy` 为 `always`](https://docs.gitlab.cn/runner/executors/docker.html#using-the-always-pull-policy)，这意味着即使本地副本可用，运行程序也会尝试从极狐GitLab 容器镜像库中提取 Docker 镜像。如果您更喜欢仅使用本地可用的 Docker 镜像，则可以在离线环境中将极狐GitLab Runner [`pull_policy` 设置为 `if-not-present`](https://docs.gitlab.cn/runner/executors/docker.html#using-the-if-not-present-pull-policy)。

但是，如果不在离线环境中，我们建议将拉取策略设置保持为 `always`，因为这样可以在 CI/CD 流水线中使用更新的扫描程序。

### 在 Docker 镜像库中提供极狐GitLab 许可证合规分析器镜像

对于所有[支持的语言和包管理器](#supported-languages-and-package-managers)的许可证合规，将以下默认许可证合规分析器镜像从 `registry.gitlab.cn` 导入到您的离线[本地 Docker 容器镜像库](../../packages/container_registry/index.md)：

```plaintext
registry.gitlab.cn/security-products/license-finder:latest
```

将 Docker 映像导入本地离线 Docker 镜像库的过程取决于**您的网络安全策略**。请咨询您的 IT 人员，找到可以导入或临时访问外部资源的已接受和批准的流程。请注意，这些扫描程序定期更新<!--[定期更新](../../application_security/index.md#vulnerability-scanner-maintenance)-->新定义，因此请考虑您是否能够自己进行定期更新。

有关将 Docker 镜像作为文件保存和传输的详细信息，请参阅 Docker 关于 [`docker save`](https://docs.docker.com/engine/reference/commandline/save/)、[`docker load`]( https://docs.docker.com/engine/reference/commandline/load/)、[`docker export`](https://docs.docker.com/engine/reference/commandline/export/) 和 [`docker import`](https://docs.docker.com/engine/reference/commandline/import/) 的文档。

### 设置许可证合规 CI/CD 变量，使用本地许可证合规分析器

将以下配置添加到您的 `.gitlab-ci.yml` 文件中。您必须替换 `image` 引用本地 Docker 容器镜像库上托管的许可证合规 Docker 镜像：

```yaml
include:
  - template: Security/License-Scanning.gitlab-ci.yml

license_scanning:
  image:
    name: localhost:5000/analyzers/license-management:latest
```

许可证合规作业现在应该使用许可证合规性分析器的本地副本，来扫描您的代码并生成安全报告，而无需访问 Internet。

<!--
Additional configuration may be needed for connecting to private registries for:

- [Bower](#using-private-bower-registries),
- [Bundler](#using-private-bundler-registries),
- [Conan](#using-private-bower-registries),
- [Go](#using-private-go-registries),
- [Maven repositories](#using-private-maven-repositories),
- [npm](#using-private-npm-registries),
- [Python repositories](#using-private-python-repositories),
- [Yarn](#using-private-yarn-registries).
-->

### SPDX 许可证列表名称匹配

> 引入于 13.3 版本。

在 13.3 版本之前，离线环境需要与[项目策略](../license_approval_policies.md)完全匹配的名称。
在 13.3 及更高版本中，系统将[项目策略](../license_approval_policies.md)的名称与 [SPDX 许可证列表](https://spdx.org/licenses/)中的标识符匹配。
SPDX 许可证列表的本地副本与极狐GitLab 实例一起分发。如果需要，实例的管理员可以使用 [Rake 任务](../../../raketasks/spdx.md)手动更新它。

<a id="license-list"></a>

## 许可证列表

许可证列表允许您查看项目的许可证和有关它们的关键详细信息。

要使许可证显示在许可证列表下，必须满足以下要求：

1. 许可证合规性 CI/CD 作业必须为您的项目[启用](#enable-license-compliance)。
1. 您的项目必须使用至少一种[支持的语言和包管理器](#supported-languages-and-package-managers)。

配置完所有内容后，在左侧栏中，选择 **安全与合规 > 许可证合规**。

将显示许可证，其中：

- **名称：**许可证的名称。
- **组件：**拥有此许可证的组件。
- **违反策略：**许可证的[许可策略](#policies)标记为**拒绝**。

![License List](img/license_list_v13_0.png)

<a id="policies"></a>

## 策略

策略允许您指定项目中 `allowed` 或 `denied` 的许可证。如果新提交了 `denied` 许可证，它会阻止合并请求并指示开发人员将其删除。
请注意，在删除 `denied` 许可证之前，无法合并合并请求。
您可以添加 [`License-Check` 批准规则](#enabling-license-approvals-within-a-project)，它启用指定的核准人，该核准人可以批准，然后将合并请求与 `denied` 许可证合并。

可以使用 [Managed Licenses API](../../../api/managed_licenses.md) 配置这些策略。

![Merge request with denied licenses](img/denied_licenses_v15_3.png)

项目许可证合规部分中的**策略**选项卡显示您项目的许可证策略。项目维护者可以在本节中指定策略。

![Edit Policy](img/policies_maintainer_edit_v14_3.png)

![Add Policy](img/policies_maintainer_add_v14_3.png)

项目的开发者可以查看项目中配置的策略。

![View Policies](img/policies_v13_0.png)

<a id="enabling-license-approvals-within-a-project"></a>

## 在项目中启用许可证批准

先决条件：

- 维护者或所有者角色。

`License-Check` 是一个[合并请求批准](../../project/merge_requests/approvals/index.md)规则，您可以启用该规则，允许个人或群组批准包含 `denied` 许可证的合并请求。

您可以通过以下两种方式之一启用 `License-Check`：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开**合并请求批准**。
1. 选择 **启用** 或 **编辑**。
1. 将 **规则名称** 添加或更改为 `License-Check`（区分大小写）。

![License Check Approver Rule](img/license-check_v13_4.png)

- 在[许可证合规项目策略部分](#policies)中，创建批准组。您必须将此批准组的所需批准数设置为大于零。在项目中启用此组后，将为所有合并请求启用批准规则。

任何代码更改都会导致重置批准数。

以下情况需要批准：

- 许可证报告包含一个依赖项，该依赖项包含一个 `denied` 的软件许可证。
- 许可证报告在流水线执行期间未生成。

当时，以下情况批准是可选的：

- 许可证报告不包含违反软件许可的行为。
- 许可证报告仅包含 `allowed` 或未知的新许可证。

## 警告

我们建议您使用所有容器的最新版本，以及所有包管理器和语言的最新支持版本。使用以前的版本会增加安全风险，因为不受支持的版本可能不再受益于主动安全报告和安全修复的反向移植。

## 故障排除

### 许可证合规性部件卡在加载状态

在以下情况下会显示加载微调器：

- 当流水线正在进行时。
- 如果流水线已完成，但仍在后台解析结果。
- 如果许可证扫描作业已完成，但流水线仍在运行。

许可证合规性部件每隔几秒钟轮询一次以获取更新的结果。当流水线完成时，流水线完成后的第一次轮询会触发解析结果。这可能需要几秒钟，具体取决于生成的报告的大小。

最终状态是成功的流水线运行已完成，已解析，许可证显示在小部件中。

### ASDF_PYTHON_VERSION 不会自动安装版本

在 ASDF_PYTHON_VERSION 中定义非最新 Python 版本不会自动安装它。如果您的项目需要非最新版本的 Python：

1. 通过设置 `ASDF_PYTHON_VERSION` CI/CD 变量来定义所需的版本。
1. 将自定义脚本传递给 `SETUP_CMD` CI/CD 变量以安装所需的版本和依赖项。

例如：

```yaml
include:
  - template: Security/License-Scanning.gitlab-ci.yml

license_scanning:
    SETUP_CMD: ./setup.sh
    ASDF_PYTHON_VERSION: "3.7.2"
  before_script:
    - echo "asdf install python 3.7.2 && pip install -r requirements.txt" > setup.sh
    - chmod +x setup.sh
    - apt-get -y update
    - apt-get -y install build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl git
```

### `ERROR -- : asdf: No preset version installed for command`

当您的项目使用的工具版本与 `license_scanning` Docker 镜像中可用的预安装工具的版本不匹配时，会发生此错误。 `license_scanning` 作业使用 [asdf-vm](https://asdf-vm.com/) 来激活项目所依赖的工具的适当版本。例如，您的项目依赖于特定版本的 [Node.js](https://nodejs.org/) 或任何其他支持的工具，您可以通过添加 [`.tool-versions`](https://asdf-vm.com/#/core-configuration?id=tool-versions) 文件到项目中或使用适当的 [`ASDF_<tool>_VERSION`](https://asdf-vm.com/#/core-configuration?id=environment-variables) 环境变量来激活相应的版本。

例如，以下 `.tool-versions` 文件激活 [Node.js](https://nodejs.org/) 的 `12.16.3` 版本和 [Ruby](https://www.ruby-lang.org/)。

```plaintext
nodejs 12.16.3
ruby 2.7.4
```

下一个示例展示了如何使用项目的 `.gitlab-ci.yml` 文件中定义的 CI/CD 变量来激活上述工具的相同版本。

```yaml
include:
  - template: Security/License-Scanning.gitlab-ci.yml

license_scanning:
  variables:
    ASDF_NODEJS_VERSION: '12.16.3'
    ASDF_RUBY_VERSION: '2.7.4'
```

完整的变量列表可以在 [CI/CD 变量](#available-cicd-variables)中找到。

要了解 `license_scanning` Docker 镜像中预安装了哪些工具，请使用以下命令：

```shell
$ docker run --entrypoint='' -ti --rm registry.gitlab.com/security-products/license-finder:4 \
  /bin/bash -c 'dpkg -i /opt/toolcache/*.deb && asdf list'
...
dotnet-core
  3.1.302
elixir
  1.10.4
golang
  1.15.5
  1.16.2
gradle
No versions installed
java
  11
  14
  15
  8
maven
No versions installed
nodejs
  10.21.0
  12.18.2
  14.17.1
php
  7.4.8
python
  2.7.18
  3.3.7
  3.4.10
  3.5.9
  3.6.11
  3.7.7
  3.8.5
ruby
  2.4.10
  2.4.5
  2.4.9
  2.5.8
  2.6.0
  2.6.1
  2.6.2
  2.6.3
  2.6.4
  2.6.5
  2.6.6
  2.7.0
  2.7.1
  2.7.2
rust
  1.45.0
```

运行上述命令可能需要 10 多分钟。
这是因为它安装了 Docker 镜像中可用的每个工具版本。

要与 `license_scanning` 运行时环境交互，请使用以下命令：

```shell
$ docker run -it --entrypoint='' registry.gitlab.cn/security-products/license-finder:4 /bin/bash -l
root@6abb70e9f193:~#
```

NOTE:
目前不支持选择 [Mono](https://www.mono-project.com/) 或 [.NET Core](https://dotnet.microsoft.com/download/dotnet) 的自定义版本。

### `LicenseFinder::Maven: is not installed` 错误

如果您的项目包含 `mvnw` 或 `mvnw.cmd` 文件，则许可证扫描作业可能会失败并出现 `LicenseFinder::Maven: is not installed error` 错误。要解决此问题，请修改许可证扫描作业，删除 `before_script` 部分中的文件。示例：

```yaml
include:
  - template: License-Scanning.gitlab-ci.yml

license_scanning:
  before_script:
    - rm mvnw
    - rm mvnw.cmd
```
