---
type: reference, howto
stage: Govern
group: Threat Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 许可证列表 **(ULTIMATE)**

许可证列表允许您查看项目的许可证和有关它们的关键详细信息。

要使许可证显示在许可证列表下，必须满足以下要求：

1. 您必须生成一个 SBOM 文件，其中的组件来自我们的一种[我们支持的语言](license_scanning_of_cyclonedx_files/index.md#supported-languages-and-package-managers)。
1. 如果使用我们的 [`Dependency-Scanning.gitlab-ci.yml` 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/License-Scanning.gitlab-ci.yml)生成 SBOM 文件，那么您的项目必须至少使用[支持的语言和包管理器](license_compliance/index.md#supported-languages-and-package-managers)中的一种。

或者，当使用我们已弃用的 [`License-Scanning.gitlab-ci.yml` 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/License-Scanning.gitlab-ci.yml)时，只要满足以下要求即可：

1. 必须为您的项目[启用](license_compliance/index.md#enable-license-compliance)许可证合规 CI/CD 作业。
1. 您的项目必须至少使用一种[支持的语言和包管理器](license_compliance/index.md#supported-languages-and-package-managers)。

配置完所有内容后，在左侧栏中选择 **安全与合规 > 许可证合规**。

显示许可证，其中：

- **名称：**许可证的名称。
- **组件：**具有此许可证的组件。
- **违反策略：**许可证有一个标记为 **拒绝** 的[许可证策略](license_approval_policies.md)。

![License List](img/license_list_v13_0.png)
