---
type: reference, howto
stage: Govern
group: Security Policies
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 许可证批准策略 **(ULTIMATE)**

> - 引入于 15.9 版本，[功能标志](../../administration/feature_flags.md)为 `license_scanning_policies`。默认禁用。
> - 一般可用于 15.11 版本，删除功能标志 `license_scanning_policies`。

许可证批准策略允许您指定多种类型的条件，这些条件定义在合并请求可以合并之前何时需要批准。

## 创建新的许可证批准策略

创建许可证批准策略，强制执行许可证合规。

要创建许可证批准策略：

1. [关联一个安全策略项目](../application_security/policies/index.md#managing-the-linked-security-policy-project)到您的开发群组、子组或项目（必须具有所有者角色）。
1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **安全与合规 > 策略**。
1. 创建一个新的[扫描结果策略](../application_security/policies/scan-result-policies.md)。
1. 在您的策略规则中，选择 **许可证扫描**。

## 定义哪些许可证需要批准的标准

以下类型的标准可用于确定哪些许可证是“已批准”，或“已拒绝”并需要批准。

- 当检测到明确禁止的许可证列表中的任何许可证时。
- 当检测到除已明确列为可接受的许可证之外的任何许可证时。

## 将合并请求分支中检测到的许可证与默认分支中检测到的许可证进行比较的标准

以下类型的条件可用于根据默认分支中存在的许可证来确定是否需要批准：

- 如果被拒绝的许可证是默认分支中尚不存在的依赖项的一部分，则可以将拒绝的许可证配置为仅需要批准。
- 如果默认分支中已存在的任何组件中存在被拒绝的许可证，则可以将被拒绝的许可证配置为需要批准。

![License approval policy](img/license_approval_policy_v15_9.png)

如果发现违反许可证批准政策的许可证，系统将阻止合并请求并指示开发人员将其删除。请注意，在删除 `denied` 许可证之前，无法合并合并请求，除非许可证批准策略的合资格批准人批准合并请求。

![Merge request with denied licenses](img/denied_licenses_v15_3.png)

## 故障排除

### 许可证合规性部件卡在加载状态

在以下情况下会显示正在加载：

- 流水线正在进行中。
- 如果流水线已完成，但仍在后台解析结果。
- 如果许可证扫描作业已完成，但流水线仍在运行。

许可证合规部件每隔几秒轮询一次更新结果。当流水线完成时，流水线完成后的第一次轮询触发结果解析。这可能需要几秒钟，具体取决于生成的报告的大小。

最终状态是当成功的流水线运行已完成、已解析且许可证显示在部件中。
