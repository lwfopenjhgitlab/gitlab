---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 创建 Kubernetes 集群

您可以在云提供商上，使用基础架构即代码（IaC）创建集群。
您可以使用 Kubernetes 代理将集群连接到极狐GitLab。

<!--
- [Create a cluster on Google GKE](../../infrastructure/clusters/connect/new_gke_cluster.md)
- [Create a cluster on Amazon EKS](../../infrastructure/clusters/connect/new_eks_cluster.md)
- [Create a cluster on Civo](../../infrastructure/clusters/connect/new_civo_cluster.md)
-->
