---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 管理集群应用程序 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/25318) in GitLab 12.10 with Helmfile support via Helm v2.
-->
> - 对 Helm v2 的支持结束于 14.0 版本。使用 Helm v3 代替。
> - 迁移到极狐GitLab 代理于 14.5 版本。

极狐GitLab 提供了一个集群管理项目模板，您可以使用它来创建项目。该项目包括与极狐GitLab 集成并扩展极狐GitLab 功能的集群应用程序。您可以使用项目中显示的 pattern 来扩展您的自定义集群应用程序。

## 用于代理和您的  manifests 的一个项目

如果您**尚未**使用代理将您的集群与极狐GitLab 连接：

1. [从集群管理项目模板创建项目](#create-a-project-based-on-the-cluster-management-project-template)。
1. [为代理配置项目](agent/install/index.md)。
1. 在项目的设置中，创建一个名为 `$KUBE_CONTEXT` 的[环境变量](../../ci/variables/index.md#add-a-cicd-variable-to-a-project)，并将值设置为 `path/to/agent-configuration-project:your-agent-name`。
1. 按需[配置文件](#configure-the-project)。

## 为代理和 manifests 使用单独的项目

如果您已经配置了代理并已将极狐GitLab 与集群连接：

1. [从集群管理项目模板创建项目](#create-a-project-based-on-the-cluster-management-project-template)。
1. 在您配置代理的项目中，[授予代理对新项目的访问权限](agent/ci_cd_workflow.md#authorize-the-agent)。
1. 在项目的设置中，创建一个名为 `$KUBE_CONTEXT` 的[环境变量](../../ci/variables/index.md#add-a-cicd-variable-to-a-project)，并将值设置为 `path/to/agent-configuration-project:your-agent-name`。
1. 按需[配置文件](#configure-the-project)。

<a id="create-a-project-based-on-the-cluster-management-project-template"></a>

## 基于集群管理项目模板创建项目

从集群管理项目模板创建项目：

1. 在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目**。
1. 在页面右侧，选择 **新建项目**。
1. 选择 **从模板创建**。
1. 从模板列表中的 **GitLab Cluster Management** 旁边，选择 **使用模板**。
1. 输入项目详情。
1. 选择 **创建项目**。

如果您使用私有化部署版，您的实例可能不包含最新版本的模板。
在这种情况下，选择 **导入项目**、**使用 URL 定位仓库**，并在 **Git 仓库 URL** 输入 `https://gitlab.com/gitlab-org/project-templates/cluster-management.git`。

<a id="configure-the-project"></a>

## 配置项目

使用集群管理模板创建项目后，可以配置：

- [`.gitlab-ci.yml` 文件](#the-gitlab-ciyml-file)。
- [主要 `helmfile.yml` 文件](#the-main-helmfileyml-file)。
- [包含内置应用程序的目录](#built-in-applications)。

<a id="the-gitlab-ciyml-file"></a>

### `.gitlab-ci.yml` 文件

`.gitlab-ci.yml` 文件：

- 确保您使用的是 Helm 3。
- 从项目部署启用的应用程序。

您可以编辑和扩展流水线定义。

流水线中使用的基础镜像<!--由 [cluster-applications](https://gitlab.com/gitlab-org/cluster-integration/cluster-applications) 项目构建。此图像-->包含一组 Bash utility 脚本，支持 [Helm v3 版本](https://helm.sh/docs/intro/using_helm/#three-big-concepts)。

<a id="the-main-helmfileyml-file"></a>

### 主要 `helmfile.yml` 文件

该模板包含一个 [Helmfile](https://github.com/helmfile/helmfile)，您可以使用 [Helm v3](https://helm.sh/) 来管理集群应用程序。

此文件包含每个应用程序的其它 Helm 文件的路径列表。默认情况下它们都被注释掉，因此您必须取消注释您想在集群中使用的应用程序的路径。

默认情况下，这些子路径中的每个 `helmfile.yaml` 都具有 `installed: true` 属性。这意味着每次流水线运行时，Helmfile 都会尝试根据集群的当前状态和 Helm 版本来安装或更新您的应用程序。如果您将此属性更改为 `installed: false`，Helmfile 会尝试从集群中卸载此应用程序。[阅读更多](https://helmfile.readthedocs.io/en/latest/)，了解 Helmfile 的工作原理。

<a id="built-in-applications"></a>

### 内置应用程序

该模板包含一个 `applications` 目录，其中为模板中的每个应用程序配置了一个 `helmfile.yaml`。

内置支持的应用程序包括：

<!--
- [Cert-manager](../infrastructure/clusters/manage/management_project_applications/certmanager.md)
- [GitLab Runner](../infrastructure/clusters/manage/management_project_applications/runner.md)
- [Ingress](../infrastructure/clusters/manage/management_project_applications/ingress.md)
- [Prometheus](../infrastructure/clusters/manage/management_project_applications/prometheus.md)
- [Sentry](../infrastructure/clusters/manage/management_project_applications/sentry.md)
- [Vault](../infrastructure/clusters/manage/management_project_applications/vault.md)
-->

- Cert-manager
- 极狐GitLab Runner
- Ingress
- Prometheus
- Sentry
- Vault

每个应用程序都有一个 `applications/{app}/values.yaml` 文件。对于极狐GitLab Runner，该文件是 `applications/{app}/values.yaml.gotmpl`。

在此文件中，您可以为应用的 Helm chart 定义默认值。一些应用程序已经定义了默认值。
