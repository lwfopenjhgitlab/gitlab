---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 从极狐GitLab 托管应用程序迁移到集群管理项目 **(FREE)**

极狐GitLab 托管应用程序（GMA）在 14.0 版本中废弃，转而支持用户控制的集群管理项目。
通过项目管理集群应用程序，使您能够比通过后期极狐GitLab 托管应用程序更灵活地管理集群。
要迁移到集群管理项目，您需要可用的[极狐GitLab Runners](../../ci/runners/index.md)，并熟悉 [Helm](https://helm.sh/)。

## 迁移到集群管理项目

要从极狐GitLab 托管应用程序迁移到集群管理项目，请按照以下步骤操作。
<!--See also [video walk-throughs](#video-walk-throughs) with examples.-->

1. 基于[集群管理项目模板](management_project_template.md#create-a-project-based-on-the-cluster-management-project-template)，新建一个项目。
1. 为您的集群中的这个项目[安装代理](agent/install/index.md)。
1. 按照项目模板中的 `.gitlab-ci.yml` 中的说明，将 `KUBE_CONTEXT` CI/CD 变量设置为新安装的代理的上下文。
1. 使用预配置的 [`.gitlab-ci.yml`](management_project_template.md#the-gitlab-ciyml-file) 文件，检测通过 Helm v2 releases 部署的应用程序：

   - 如果您覆盖了默认的极狐GitLab 托管应用程序命名空间，请编辑 `.gitlab-ci.yml`，并确保脚本接收正确的命名空间作为参数：

     ```yaml
     script:
       - gl-fail-if-helm2-releases-exist <your_custom_namespace>
     ```

   - 如果您保留默认名称（`gitlab-managed-apps`），则脚本已经设置。

   无论哪种方式，[手动运行流水线](../../ci/pipelines/index.md#run-a-pipeline-manually) 并阅读 `detect-helm2-releases` 作业的日志，了解您是否有任何 Helm v2 releases，以及它们是哪些。

1. 如果您没有 Helm v2 releases，请跳过此步骤。否则，请按照 Helm 官方文档：[如何从 Helm v2 迁移到 Helm v3] (https://helm.sh/blog/migrate-from-helm-v2-to-helm-v3/)，在您确信它们已成功迁移后，清理 Helm v2 releases。

1. 在这一步中，您应该已经只有 Helm v3 releases。
   从主要 [`./helmfile.yaml`](management_project_template.md#the-main-helmfileyml-file) 中取消注释您要使用此项目管理的应用程序的路径。虽然您可以一次取消注释所有要管理的应用程序，但我们建议您为每个应用程序分别重复以下步骤。
1. 编辑关联的 `applications/{app}/helmfiles.yaml`，匹配当前为您的应用部署的 chart 版本。以极狐GitLab Runner Helm v3 版本为例：

   以下命令列出了 releases 及其版本：

   ```shell
   helm ls -n gitlab-managed-apps

   NAME NAMESPACE REVISION UPDATED STATUS CHART APP VERSION
   runner gitlab-managed-apps 1 2021-06-09 19:36:55.739141644 +0000 UTC deployed gitlab-runner-0.28.0 13.11.0
   ```

   从 `CHART` 列中获取版本，格式为 `{release}-v{chart_version}`，然后编辑 `./applications/gitlab-runner/helmfile.yaml` 中的 `version:` 属性，使其与您当前部署的版本相匹配。这是避免在此迁移期间升级版本的安全步骤。如果您将应用程序部署到不同的命名空间，请确保从上述命令中替换 `gitlab-managed-apps`。

1. 编辑与您的应用关联的 `applications/{app}/values.yaml` 以匹配当前部署的值。例如，对于极狐GitLab Runner：

   1. 复制以下命令的输出（可能很大）：

      ```shell
      helm get values runner -n gitlab-managed-apps -a --output yaml
      ```

   1. 用上一个命令的输出覆盖 `applications/gitlab-runner/values.yaml`。

   这个安全步骤将保证没有意外的默认值覆盖您当前部署的值。对于实例，您的极狐GitLab Runner 可能会错误地覆盖其 `gitlabUrl` 或 `runnerRegistrationToken`。

1. 一些应用程序需要特别注意：

   - Ingress：由于存在 [chart 问题](https://github.com/helm/charts/pull/13646)，您可能会在尝试运行 [`./gl-helmfile`](management_project_template.md#the-gitlab-ciyml-file) 时看到 `spec.clusterIP: Invalid value` 命令。要解决此问题，在覆盖 `applications/ingress/values.yaml` 中的 release 值后，您可能需要覆盖所有出现的 `omitClusterIP: false`，将其设置为 `omitClusterIP: true`。另一种方法是通过运行 `kubectl get services -n gitlab-managed-apps` 来收集这些 IP，然后用您从该命令获得的值覆盖涉及的每个 `ClusterIP`。

   - Vault：此应用程序引入了从 Helm v2 中使用的 chart 到 Helm v3 中使用的 chart 的重大更改。因此，将其与此集群管理项目集成的唯一方法是，实际卸载此应用程序并接受 `applications/vault/values.yaml` 中建议的 chart 版本。

   - Cert-manager：

     - 对于 Kubernetes 版本 1.20 或更高版本的用户，已弃用的 cert-manager v0.10 不再有效，升级包含重大更改。所以我们建议您[备份和卸载 cert-manager v0.10](#backup-and-uninstall-cert-manager-v010)，并安装最新的 cert-manager。要安装此版本，请从 [`./helmfile.yaml`](management_project_template.md#the-main-helmfileyml-file) 中取消注释 `applications/cert-manager/helmfile.yaml`。这会触发安装新版本的流水线。
     - 对于 Kubernetes 版本低于 1.20 的用户，您可以通过在项目的主 Helmfile ([`./helmfile.yaml`](management_project_template.md#the-main-helmfileyml-file)） 中，取消注释 `applications/cert-manager-legacy/helmfile.yaml` 来坚持使用 v0.10。

       WARNING:
       当 Kubernetes 升级到 1.20 或更高版本时，Cert-manager v0.10 会崩溃。

1. 完成上述所有步骤后，[手动运行流水线](../../ci/pipelines/index.md#run-a-pipeline-manually)并查看 `apply` 作业日志，查看您的任何应用程序是否已成功检测、安装，以及是否有任何意外更新。

   预计将更新一些注释校验和，以及此属性：

   ```diff
   --- heritage: Tiller
   +++ heritage: Tiller
   ```

获得成功的流水线后，对要使用集群管理项目管理的任何其它已部署应用程序重复这些步骤。

<a id="backup-and-uninstall-cert-manager-v010"></a>

## 备份和卸载 cert-manager v0.10

1. 按照[官方文档](https://cert-manager.io/docs/tutorials/backup/)，了解如何备份您的 cert-manager v0.10 数据。
1. 通过在 `applications/cert-manager/helmfile.yaml` 文件中，将所有出现的 `installed: true` 设置为 `installed: false` 来卸载 cert-manager。
1. 通过执行以下命令搜索任何剩余资源：`kubectl get Issuers,ClusterIssuers,Certificates,CertificateRequests,Orders,Challenges,Secrets,ConfigMaps -n gitlab-managed-apps | grep certmanager`。
1. 对于上一步中找到的每个资源，使用 `kubectl delete -n gitlab-managed-apps {ResourceType} {ResourceName}` 删除它们。例如，您找到一个名为 `cert-manager-controller` 的 `ConfigMap` 类型的资源，请执行以下命令将其删除：`kubectl delete configmap -n gitlab-managed-apps cert-manager-controller`。

<!--
## Video walk-throughs

You can watch these videos with examples on how to migrate from GMA to a Cluster Management project:

- [Migrating from scratch using a brand new cluster management project](https://youtu.be/jCUFGWT0jS0). Also covers Helm v2 apps migration.
- [Migrating from an existing GitLab managed apps CI/CD project](https://youtu.be/U2lbBGZjZmc).
-->
