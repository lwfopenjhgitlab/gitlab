---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 安装 Kubernetes 代理 **(FREE)**

> - 从专业版移动到免费版于 14.5。
> - 多架构镜像引入于 14.8 版本。第一个多架构版本是 `v14.8.1`。它支持 AMD64 和 ARM64 架构。
> - ARM 架构支持引入于 14.9 版本。

要将 Kubernetes 集群连接到极狐GitLab，您必须在集群中安装代理。

## 先决条件

在集群中安装代理之前，您需要：

- 现有的 Kubernetes 集群。如果您没有集群，您可以在云提供商上创建一个。
- 在私有化部署版极狐GitLab 实例上，管理员必须设置[代理服务器](../../../../administration/clusters/kas.md)。然后将默认在 `wss://gitlab.example.com/-/kubernetes-agent/` 上可用。在 JiHuLab.com 上，代理服务器在 `wss://kas.jihulab.com` 上可用。

## 安装步骤

要在集群中安装代理：

1. 可选。[创建一个代理配置文件](#create-an-agent-configuration-file)。
1. [向极狐GitLab 注册代理](#register-the-agent-with-gitlab)。
1. [在集群中安装代理](#install-the-agent-in-the-cluster)。

<a id="create-an-agent-configuration-file"></a>

### 创建代理配置文件

> - 引入于 13.7 版本，代理配置文件可以添加到仓库的多个目录（或子目录）中。
> - 群组授权引入于 14.3 版本。

代理使用 YAML 文件进行配置设置。如果出现以下情况，您需要一个配置文件：

- 您想使用 [GitOps 工作流](../gitops.md#gitops-configuration-reference)。
- 您想授权不同的项目将代理用于[极狐GitLab CI/CD 工作流](../ci_cd_workflow.md#authorize-the-agent)。

要创建代理配置文件：

1. 为您的代理选择一个名称。代理名称遵循 [RFC 1123 中的 DNS 标签标准](https://www.rfc-editor.org/rfc/rfc1123)。名称必须：

    - 在项目中独一无二。
    - 最多包含 63 个字符。
    - 仅包含小写字母数字字符或 `-`。
    - 以字母和数字开头。
    - 以字母和数字结尾。

1. 在仓库中，在默认分支中，在根目录下创建此目录：

   ```plaintext
   .gitlab/agents/<agent-name>
   ```

1. 在该目录中，创建一个 `config.yaml` 文件。确保文件名以 `.yaml` 结尾，而不是 `.yml`。

您现在可以将文件留空，稍后[配置它](#configure-your-agent)。

<a id="register-the-agent-with-gitlab"></a>

### 向极狐GitLab 注册代理

> - 引入于 14.1 版本，您可以直接从 UI 创建新的代理记录。
> - 引入于 14.9 版本，无需创建代理配置文件即可注册代理。

FLAG:
于 14.10 版本，功能标志 `certificate_based_clusters` 更改了 **操作** 菜单，用于代理而不是证书。<!--The flag is [enabled on GitLab.com and self-managed](https://gitlab.com/groups/gitlab-org/configure/-/epics/8).-->

先决条件：

- 对于[极狐GitLab CI/CD 工作流](../ci_cd_workflow.md)，确保[极狐GitLab CI/CD 已启用](../../../../ci/enable_or_disable_ci.md#enable-cicd-in-a-project)。

您必须先注册代理，然后才能在集群中安装代理。注册代理的步骤：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。如果您有一个[代理配置文件](#create-an-agent-configuration-file)，它必须在这个项目中。您的集群 manifest 文件也应该在此项目中。
1. 从左侧边栏中，选择 **基础架构 > Kubernetes 集群**。
1. 选择 **连接集群（代理）**。
   - 如果要使用 CI/CD 默认值创建配置，请键入符合[命名约定](#agent-naming-convention)的名称。
   - 如果您已有[代理配置文件](#create-an-agent-configuration-file)，请从列表中选择它。
1. 选择 **注册代理**。
1. 极狐GitLab 为代理生成访问令牌。您在集群中安装代理时，需要此令牌<!--并将 [更新代理](#update-the-agent-version) 到另一个版本-->。

   WARNING:
   安全地存储代理访问令牌。不良行为者可以使用此令牌访问代理配置项目中的源代码，访问极狐GitLab 实例上任何公共项目中的源代码，甚至在非常特定的条件下，获取 Kubernetes manifests。

1. 复制 **推荐安装方法** 下的命令。当您使用单线安装方法在集群中安装代理时，您需要使用此命令。

<a id="install-the-agent-in-the-cluster"></a>

### 在集群中安装代理

> 引入于 14.10 版本，极狐GitLab 推荐使用 Helm 安装代理。

要将集群连接到极狐GitLab，请在集群中安装已注册的代理，[使用 Helm 安装代理](#install-the-agent-with-helm)。

NOTE:
要连接到多个集群，您必须在每个集群中配置、注册和安装代理。确保给每个代理一个唯一的名称。

<a id="install-the-agent-with-helm"></a>

#### 使用 Helm 安装代理

要使用 Helm 在集群上安装代理：

1. [安装 Helm](https://helm.sh/docs/intro/install/)。
1. 在您的计算机中，打开一个终端并[连接到您的集群](https://kubernetes.io/docs/tasks/access-application-cluster/access-cluster/)。
1. 运行您在[使用极狐GitLab 注册代理](#register-the-agent-with-gitlab)时复制的命令。

或者，您可以[自定义 Helm 安装](#customize-the-helm-installation)。

<a id="customize-the-helm-installation"></a>

##### 自定义 Helm 安装

默认情况下，极狐GitLab 生成的 Helm 安装命令：

- 为部署创建命名空间 `gitlab-agent`（`--namespace gitlab-agent`）。您可以通过省略 `--create-namespace` 标志来跳过创建命名空间。
- 为具有 `cluster-admin` 权限的代理设置服务帐户。您可以：
  - 通过将 `--set serviceAccount.create=false` 添加到 `helm install` 命令来跳过创建服务帐户。在这种情况下，您必须将 `serviceAccount.name` 设置为预先存在的服务帐户。
  - 通过将 `--set rbac.create=false` 添加到 `helm install` 命令来跳过创建 RBAC 权限。在这种情况下，您必须为代理带来自己的 RBAC 权限。否则代理没有权限。
- 为代理的访问令牌创建一个 `Secret` 资源。要使用令牌带来您自己的 secret，请省略令牌（`--set token=...`），而使用 `--set config.secretName=<your secret name>`。
- 为 `agentk` pod 创建一个 `Deployment` 资源。

##### 当 KAS 在自签名证书后面时使用代理

当 [KAS](../../../../administration/clusters/kas.md) 在自签名证书后面时，您可以将 `config.caCert` 的值设置为证书。例如：

```shell
helm update --install gitlab-agent gitlab/gitlab-agent \
  --set-file config.caCert=my-custom-ca.pem
```

在此示例中，`my-custom-ca.pem` 是包含 KAS 使用的 CA 证书的本地文件的路径。证书自动存储在配置映射中并安装在 `agentk` pod 中。

如果 KAS 与极狐GitLab chart 一起安装，并且 chart 配置为提供[自动生成的自签名通配符证书](https://docs.gitlab.cn/charts/installation/tls.html#option-4-use-auto-generated-self-signed-wildcard-certificate)，您可以从 `RELEASE-wildcard-tls-ca` secret 中提取 CA 证书。

<!--
##### 使用 HTTP proxy 后面的代理

> 引入于 15.0 版本，GitLab 代理 Helm 图表支持设置环境变量。

To configure an HTTP proxy when using the Helm chart, you can use the environment variables `HTTP_PROXY`, `HTTPS_PROXY`,
and `NO_PROXY`. Upper and lowercase are both acceptable.

You can set these variables by using the `extraEnv` value, as a list of objects with keys `name` and `value`.
For example, to set only the environment variable `HTTPS_PROXY` to the value `https://example.com/proxy`, you can run:

```shell
helm upgrade --install gitlab-agent gitlab/gitlab-agent \
  --set extraEnv[0].name=HTTPS_PROXY \
  --set extraEnv[0].value=https://example.com/proxy \
  ...
```
-->

<a id="configure-your-agent"></a>

### 配置您的代理

要配置您的代理，请将内容添加到 `config.yaml` 文件：

- GitOps 工作流：[查看配置参考](../gitops.md#gitops-configuration-reference)。
- 极狐GitLab CI/CD 工作流：[查看配置参考](../ci_cd_workflow.md)。

<!--
To see the full list of customizations available, see the Helm chart's [default values file](https://gitlab.com/gitlab-org/charts/gitlab-agent/-/blob/main/values.yaml).
-->

<!--
#### Advanced installation method

GitLab also provides a [KPT package for the agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/tree/master/build/deployment/gitlab-agent). This method provides greater flexibility, but is only recommended for advanced users.

## Install multiple agents in your cluster

To install a second agent in your cluster, you can follow the [previous steps](#register-the-agent-with-gitlab) a second time. To avoid resource name collisions within the cluster, you must either:

- Use a different release name for the agent, for example, `second-gitlab-agent`:

  ```shell
  helm upgrade --install second-gitlab-agent gitlab/gitlab-agent ...
  ```

- Or, install the agent in a different namespace, for example, `different-namespace`:

  ```shell
  helm upgrade --install gitlab-agent gitlab/gitlab-agent \
    --namespace different-namespace \
    ...
  ```

## Example projects

The following example projects can help you get started with the agent.

- [Configuration repository with minimal manifests](https://gitlab.com/gitlab-examples/ops/gitops-demo/k8s-agents)
- [Distinct application and manifest repository example](https://gitlab.com/gitlab-examples/ops/gitops-demo/hello-world-service-gitops)
- [Auto DevOps setup that uses the CI/CD workflow](https://gitlab.com/gitlab-examples/ops/gitops-demo/hello-world-service)
- [Cluster management project template example that uses the CI/CD workflow](https://gitlab.com/gitlab-examples/ops/gitops-demo/cluster-management)

## Updates and version compatibility

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/340882) in GitLab 14.8, GitLab warns you on the agent's list page to update the agent version installed on your cluster.

For the best experience, the version of the agent installed in your cluster should match the GitLab major and minor version. The previous minor version is also supported. For example, if your GitLab version is v14.9.4 (major version 14, minor version 9), then versions v14.9.0 and v14.9.1 of the agent are ideal, but any v14.8.x version of the agent is also supported. See [the release page](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/releases) of the GitLab agent.

<a id="update-the-agent-version"></a>

### 更新代理版本

要将代理更新到最新版本，您可以运行：

```shell
helm repo update
helm upgrade --install gitlab-agent gitlab-jh/gitlab-agent \
  --namespace gitlab-agent \
  --reuse-values
```

要设置特定版本，您可以覆盖 `image.tag` 值。例如，要安装版本 `v14.9.1`，请运行：

```shell
helm upgrade gitlab-agent gitlab/gitlab-agent \
  --namespace gitlab-agent \
  --reuse-values \
  --set image.tag=v14.9.1
```

## Uninstall the agent

If you [installed the agent with Helm](#install-the-agent-with-helm), then you can also uninstall with Helm. For example, if the release and namespace are both called `gitlab-agent`, then you can uninstall the agent using the following command:

```shell
helm uninstall gitlab-agent \
    --namespace gitlab-agent
```
-->