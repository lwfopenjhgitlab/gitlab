---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# GitOps 与 Kubernetes 集群一起使用 **(FREE)**

> - 引入于 13.7 版本。
> - 引入于 14.0 版本，`resource_inclusions` 和 `resource_exclusions` 属性被删除，添加了 `reconcile_timeout`、`dry_run_strategy`、`prune`、`prune_timeout`、`prune_propagation_policy` 和 `inventory_policy` 属性。
> - 从专业版移动到免费版于 15.3 版本。

使用 GitOps，您可以从满足以下条件的 Git 仓库管理容器化集群和应用程序：

- 是您系统的唯一真实来源。
- 是您运营系统的唯一位置。

通过结合极狐GitLab、Kubernetes 和 GitOps，您可以实现：

- 极狐GitLab 运营 GitOps 。
- Kubernetes 作为自动化和融合系统。
- 用于持续集成的极狐GitLab CI/CD 和用于持续部署的代理。
- 内置自动偏移修复。
- 使用[服务器端应用](https://kubernetes.io/docs/reference/using-api/server-side-apply/)进行资源管理，用于透明的多参与者字段管理。

此图显示了 GitOps 部署中的仓库和主要参与者：

```mermaid
sequenceDiagram
  participant D as Developer
  participant A as Application code repository
  participant M as Manifest repository
  participant K as GitLab agent
  participant C as Agent configuration repository
  loop Regularly
    K-->>C: Grab the configuration
  end
  D->>+A: Pushing code changes
  A->>M: Updating manifest
  loop Regularly
    K-->>M: Watching changes
    M-->>K: Pulling and applying changes
  end
```

<!--
For details, view the [architecture documentation](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/architecture.md#high-level-architecture).
-->

## GitOps 工作流步骤

要使用 GitOps 更新 Kubernetes 集群，请完成以下步骤。

1. 确保您有一个正常工作的 Kubernetes 集群，并且 manifests 位于极狐GitLab 项目中。
1. 在同一个项目中，[注册并安装极狐GitLab 代理](install/index.md)。
1. 配置代理配置文件，以便代理监控项目对 Kubernetes manifests 的更改。参考 [GitOps 配置参考](#gitops-configuration-reference)作为指导。

每当您提交对 Kubernetes 清单的更新时，代理都会更新集群。

<a id="gitops-configuration-reference"></a>

## GitOps 配置参考

以下片段显示了代理配置文件的 GitOps 部分的可能键和值的示例。

```yaml
gitops:
  manifest_projects:
  - id: gitlab-org/cluster-integration/gitlab-agent
    default_namespace: my-ns
    paths:
      # Read all YAML files from this directory.
    - glob: '/team1/app1/*.yaml'
      # Read all .yaml files from team2/apps and all subdirectories.
    - glob: '/team2/apps/**/*.yaml'
      # If 'paths' is not specified or is an empty list, the configuration below is used.
    - glob: '/**/*.{yaml,yml,json}'
    reconcile_timeout: 3600s
    dry_run_strategy: none
    prune: true
    prune_timeout: 3600s
    prune_propagation_policy: foreground
    inventory_policy: must_match
```

| 关键字 | 描述 |
|--|--|
| `manifest_projects` | 存储 Kubernetes manifests 的项目。代理监视这些项目的仓库中的文件。当 manifests 文件更改时，代理会将更改部署到集群。 |
| `id` | 必需。具有 YAML 或 JSON 格式的 Kubernetes manifests 的 Git 仓库的路径。当前不支持任何身份验证机制。 |
| `default_namespace` | 要使用的命名空间，如果未在对象 manifest 中明确设置。也用于 `ConfigMap` 对象。 |
| `paths` | 用于扫描 manifest 文件的仓库路径。名称以点 `(.)` 开头的目录将被忽略。 |
| `paths[].glob` | 必需。对于 globbing 规则，查看 [doublestar](https://github.com/bmatcuk/doublestar#about) 和[匹配函数](https://pkg.go.dev/github.com/bmatcuk/doublestar/v2#Match)。 |
| `reconcile_timeout` | 确定应用程序是否应该等到所有应用的资源都已协调，如果是，等待多长时间。默认值为 3600 秒（1 小时）。 |
| `dry_run_strategy` | 确定变更是否[应该执行](https://github.com/kubernetes-sigs/cli-utils/blob/d6968048dcd80b1c7b55d9e4f31fc25f71c9b490/pkg/common/common.go#L68-L89)。可以是：`none`、`client` 或 `server`。默认为 `none`。 |
| `prune` | 确定是否应在应用后修剪先前应用的对象。默认为 `true`。 |
| `prune_timeout` | 决定修剪后是否等待所有资源完全删除，如果是，等待多长时间。默认值为 3600 秒（1 小时）。 |
| `prune_propagation_policy` | [应该用于修剪](https://github.com/kubernetes/apimachinery/blob/44113beed5d39f1b261a12ec398a356e02358307/pkg/apis/meta/v1/types.go#L456-L470)的删除传播策略。可以是：`orphan`、`background` 或 `foreground`。默认为 `foreground`。 |
| `inventory_policy` | 确定库对象是否可以接管属于另一个库对象或不属于任何库对象的对象，这是通过比较包中的 `inventory-id` 值和[活动对象](https://github.com/kubernetes-sigs/cli-utils/blob/d6968048dcd80b1c7b55d9e4f31fc25f71c9b490/pkg/inventory/policy.go#L12-L66)中的 `owning-inventory` annotation（`config.k8s.io/owning-inventory`）来确定资源的应用/修剪操作是否可以完成。可以是：`must_match`、`adopt_if_no_inventory` 或 `adopt_all`。默认是`must_match`。 |

## GitOps annotations

Kubernetes 的极狐GitLab 代理具有 annotations，可用于：

- **资源排序**：按特定顺序应用或删除资源。
- **使用应用时突变**：将字段从一种资源配置动态替换为另一种。

代理有[默认排序](https://github.com/kubernetes-sigs/cli-utils/blob/d7d63f4b62897f584ca9e02b6faf4d2f327a9b09/pkg/ordering/sort.go#L74)，但是通过 annotations，您可以微调顺序并应用时间值注入。

为了提供 GitOps 功能，Kubernetes 的极狐GitLab 代理使用了 [`cli-utils` 库](https://github.com/kubernetes-sigs/cli-utils/)，这是一个 Kubernetes SIG 项目。您可以在 [`cli-utils` 文档](https://github.com/kubernetes-sigs/cli-utils/blob/master/README.md#apply-sort-ordering) 中阅读有关可用注释的更多信息。

- [了解有关应用排序顺序的更多信息](https://github.com/kubernetes-sigs/cli-utils#apply-sort-ordering)。
- [了解有关应用时间突变的更多信息](https://github.com/kubernetes-sigs/cli-utils#apply-time-mutation)。

## 自动偏移修复

当基础设施资源的当前配置与其预期配置不同时，就会发生偏移。
通常，这是由直接通过创建资源的服务手动编辑资源引起的。将偏移风险降至最低有助于确保配置一致性和成功运行。

在极狐GitLab 中，Kubernetes 的代理会定期将 `git` 仓库中的预期状态与 `cluster` 中的已知状态进行比较。每次检查都会修复与 `git` 状态的偏差。这些检查每 5 分钟自动进行一次。它们是不可配置的。

代理使用[服务器端应用](https://kubernetes.io/docs/reference/using-api/server-side-apply/)。
因此，资源中的每个字段都可以有不同的管理者。仅检查由 `git` 管理的字段是否存在偏差。这有助于使用集群内控制器来修改资源，例如 [Horizo​​ntal Pod Autoscalers](https://kubernetes.io/docs/tasks/run-application/horizo​​ntal-pod-autoscale/)。

<!--
## Related topics

- [GitOps working examples for training and demos](https://gitlab.com/groups/guided-explorations/gl-k8s-agent/gitops/-/wikis/home)
- [Self-paced classroom workshop](https://gitlab-for-eks.awsworkshop.io) (Uses AWS EKS, but you can use for other Kubernetes clusters)
- [Managing Kubernetes secrets in a GitOps workflow](gitops/secrets_management.md)
- [Application and manifest repository example](https://gitlab.com/gitlab-examples/ops/gitops-demo/hello-world-service-gitops)
-->

## 故障排查

### 有多个项目时避免冲突

代理独立监视项目的 `paths` 部分下设置的每个 glob 模式，并同时对集群进行更新。
如果在多个路径中发现更改，则当代理尝试更新集群时，可能会发生冲突。

为防止这种情况发生，请考虑将 manifest 的逻辑组存储在一个位置，并仅引用它们一次以避免重叠 glob。

例如，这两个 glob 都匹配根目录中的 `*.yaml` 文件，并可能导致冲突：

```yaml
gitops:
  manifest_projects:
  - id: project1
    paths:
    - glob: '/**/*.yaml'
    - glob: '/*.yaml'
```

相反，指定一个匹配所有 `*.yaml` 文件的单个 glob 递归：

```yaml
gitops:
  manifest_projects:
  - id: project1
    paths:
    - glob: '/**/*.yaml'
```

### 使用多个代理或项目

如果您将 Kubernetes manifest 存储在单独的极狐GitLab 项目中，请使用这些项目的位置更新您的代理配置文件。

WARNING:
具有代理配置文件的项目可以是私有的或公开的。其它具有 Kubernetes manifests 的项目必须是公开的。<!--Support for private manifest projects is tracked
in [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/283885).-->
