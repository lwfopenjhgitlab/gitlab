---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用 Kubernetes 代理 **(FREE)**

使用 Kubernetes 代理时，请使用以下任务。

## 查看您的代理

> 引入于 14.8 版本，安装的 `agentk` 版本显示在 **代理** 选项卡上。

先决条件：

- 您必须至少具有开发者角色。

查看代理列表：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到包含您的代理配置文件的项目。
1. 在左侧边栏中，选择 **基础架构 > Kubernetes 集群**。
1. 选择 **代理** 选项卡，查看通过代理连接到极狐GitLab 的集群。

在此页面上，您可以查看：

- 当前项目的所有注册代理。
- 连接状态。
- 集群上安装的 `agentk` 版本。
- 每个代理配置文件的路径。

## 查看代理的活动信息

> 引入于 14.6 版本。

活动日志可帮助您识别问题并获取故障排除所需的信息。您可以查看当前日期前一周的事件。查看代理的活动：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到包含您的代理配置文件的项目。
1. 在左侧边栏中，选择 **基础架构 > Kubernetes 集群**。
1. 选择您要查看活动的代理。

活动清单包括：

- 代理注册事件：**创建新令牌时**。
- 连接事件：当代理成功**连接**到集群时。

当您第一次连接代理或超过一个小时不活动后，连接状态会被记录。

## Debug 代理

> `grpc_level` 引入于 15.1 版本。

要调试代理的集群端组件（`agentk`），请根据可用选项设置日志级别：

- `error`
- `info`
- `debug`

代理有两个记录器：

- 通用记录器，默认为 `info`。
- 一个 gRPC 记录器，默认为 `error`。

可以通过使用[代理配置文件](install/index.md#configure-your-agent)中的顶级 `observability` 部分，来更改其日志级别，例如将级别设置为 `debug` 和 `warning`：

```yaml
observability:
  logging:
    level: debug
    grpc_level: warn
```

当 `grpc_level` 设置为 `info` 或以下时，会出现大量 gRPC 日志。

提交配置更改并检查代理服务日志：

```shell
kubectl logs -f -l=app=gitlab-agent -n gitlab-agent
```

有关调试的更多信息，请参阅 [疑难解答文档](troubleshooting.md)。

## 重置代理令牌

> - 引入于 14.9 版本。
> - 引入于 14.10 版本，可以从 UI 中撤销代理令牌。

要在不停机的情况下重置代理令牌：

1. 创建一个新令牌：
   1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
   1. 在左侧边栏中，选择 **基础架构 > Kubernetes 集群**。
   1. 选择您要为其创建令牌的代理。
   1. 在 **访问令牌** 选项卡上，选择 **创建令牌**。
   1. 输入令牌的名称和描述（可选）并选择 **创建令牌**。
1. 安全存储生成的令牌。
1. 使用令牌，[在集群中安装代理](install/index.md#install-the-agent-in-the-cluster)<!--，和[更新代理](install/index.md#update-the-agent-version)到另一个版本-->。
1. 要删除您不再使用的令牌，请返回令牌列表并选择 **撤销** (**{remove}**)。

## 删除代理

您可以使用 [UI](#remove-an-agent-through-the-gitlab-ui) 或 [GraphQL API](#remove-an-agent-with-the-gitlab-graphql-api)。代理和任何相关的令牌都会从极狐GitLab 中删除，但不会在您的 Kubernetes 集群中进行任何更改。您必须手动清理这些资源。

<a id="remove-an-agent-through-the-gitlab-ui"></a>

### 通过 UI 删除代理

> 引入于 14.7 版本。

要从 UI 中删除代理：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到包含代理配置文件的项目。
1. 从左侧边栏中，选择 **基础架构 > Kubernetes 集群**。
1. 在表格中，在您的代理所在行的 **选项** 列中，选择垂直省略号 (**{ellipsis_v}**)。
1. 选择 **删除代理**。

<a id="remove-an-agent-with-the-gitlab-graphql-api"></a>

### 使用 GraphQL API 删除代理

1. 从交互式 GraphQL 资源管理器中的查询中获取 `<cluster-agent-token-id>`。
   - 对于 SaaS，访问 <https://jihulab.com/-/graphql-explorer> 打开 GraphQL Explorer。
   - 对于私有化部署实例，请转到 `https://gitlab.example.com/-/graphql-explorer`，将 `gitlab.example.com` 替换为您的实例的 URL。

   ```graphql
   query{
     project(fullPath: "<full-path-to-agent-configuration-project>") {
       clusterAgent(name: "<agent-name>") {
         id
         tokens {
           edges {
             node {
               id
             }
           }
         }
       }
     }
   }
   ```

1. 通过删除 `clusterAgentToken` 来使用 GraphQL 删除代理记录。

   ```graphql
   mutation deleteAgent {
     clusterAgentDelete(input: { id: "<cluster-agent-id>" } ) {
       errors
     }
   }

   mutation deleteToken {
     clusterAgentTokenDelete(input: { id: "<cluster-agent-token-id>" }) {
       errors
     }
   }
   ```

1. 验证删除是否成功。如果 Pod 日志中的输出包含 `unauthenticated`，则表示代理已成功移除：

   ```json
   {
       "level": "warn",
       "time": "2021-04-29T23:44:07.598Z",
       "msg": "GetConfiguration.Recv failed",
       "error": "rpc error: code = Unauthenticated desc = unauthenticated"
   }
   ```

1. 删除集群中的代理：

   ```shell
   kubectl delete -n gitlab-kubernetes-agent -f ./resources.yml
   ```
