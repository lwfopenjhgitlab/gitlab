---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 运营容器扫描 **(ULTIMATE)**

> - 引入于 14.8 版本。
> - starboard 指令废弃于 15.4 版本。计划删除于 16.0 版本。


查看集群漏洞，可以查看[漏洞报告](../../application_security/vulnerabilities/index.md)。
您还可以配置您的代理，以便漏洞与极狐GitLab 中的其他代理信息一起显示。

<a id="enable-operational-container-scanning"></a>

## 启用运营容器扫描 **(ULTIMATE)**

您可以使用运营容器扫描，来扫描集群中的容器镜像以查找安全漏洞。

NOTE:
在 15.0 及更高版本中，您无需在 Kubernetes 集群中安装 Starboard operator。

要开始扫描集群中的所有资源，请在代理配置中添加一个 `container_scanning` 配置块，其中的 `cadence` 字段包含一个 CRON 表达式，用于指示何时运行扫描。

```yaml
container_scanning:
  cadence: '0 0 * * *' # Daily at 00:00 (Kubernetes cluster time)
```

`cadence` 字段是必需的。极狐GitLab 支持 cadence 字段的以下类型的 CRON 语法：

- 在指定时间每小时一次的每日周期，例如：`0 18 * * *`
- 在指定日期和指定时间每周一次的每周周期，例如：`0 13 * * 0`

CRON 语法的其他元素可能会在 cadence 字段中起作用，但是，极狐GitLab 并未正式测试或支持它们。

默认情况下，运营容器扫描将尝试扫描所有命名空间中的工作负载以查找漏洞。`vulnerability_report` 块有一个 `namespaces` 字段，可用于限制扫描哪些命名空间。例如，如果您只想扫描 `development`、`staging` 和 `production` 命名空间，可以使用以下配置：

```yaml
container_scanning:
  cadence: '0 0 * * *'
  vulnerability_report:
    namespaces:
      - development
      - staging
      - production
```

## 查看集群漏洞

先决条件：

- 您必须至少具有开发者角色。

在极狐GitLab 中查看漏洞信息：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到包含代理配置文件的项目。
1. 在左侧边栏中，选择 **基础架构 > Kubernetes 集群**。
1. 选择 **代理** 选项卡。
1. 选择代理查看集群漏洞。

![Cluster agent security tab UI](../img/cluster_agent_security_tab_v14_8.png)

此信息也可以在[运营漏洞](../../../user/application_security/vulnerability_report/index.md#operational-vulnerabilities)下找到。
