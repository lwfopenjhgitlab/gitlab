---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用极狐GitLab 连接 Kubernetes 集群

> - 引入于 13.4 版本。
> - 对 `grpcs` 的支持引入于 13.6 版本。
> - 从专业版移动到免费版于 14.5 版本。
> - 从“极狐GitLab Kubernetes 代理” 重命名为“用于 Kubernetes 的极狐GitLab 代理”于 14.6 版本。

您可以将 Kubernetes 集群与极狐GitLab 连接，部署、管理和监控您的云原生解决方案。

要将 Kubernetes 集群连接到极狐GitLab，您必须首先[在集群中安装代理](install/index.md)。

代理在集群中运行，您可以使用它来：

- 与位于防火墙或 NAT 后面的集群通信。
- 实时访问集群中的 API 端点。
- 推送有关集群中发生的事件的信息。
- 启用 Kubernetes 对象的缓存，这些对象以极低的延迟保持最新。

<!--
For more details about the agent's purpose and architecture, see the [architecture documentation](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/architecture.md).
-->

## 工作流

您可以从两个主要工作流中进行选择。推荐使用 GitOps 工作流。

### GitOps 工作流

在 [**GitOps** 工作流中](gitops.md)：

- 您将 Kubernetes manifests 保存在极狐GitLab 中。
- 在集群中安装极狐GitLab 代理。
- 每当您更新清单时，代理都会更新集群。
- 集群自动清理意外更改。它使用[服务器端应用](https://kubernetes.io/docs/reference/using-api/server-side-apply/)来修复第三方引入的任何不一致的配置。

这个工作流完全由 Git 驱动，被认为是**基于拉取的**流程，因为集群正在从极狐GitLab 仓库中提取更新。

### 极狐GitLab CI/CD 工作流

在 [**CI/CD** 工作流中](ci_cd_workflow.md)：

- 您将极狐GitLab CI/CD 配置为使用 Kubernetes API 来查询和更新集群。

此工作流被认为是**基于推送的**流程，因为极狐GitLab 正在将来自极狐GitLab CI/CD 的请求推送到您的集群。

在以下情况，使用此工作流：

- 当您有大量面向流水线的流程时。
- 当您需要迁移到代理但 GitOps 工作流无法支持您需要的用例时。

此工作流的安全模型较弱，不建议用于生产部署。

<a id="gitlab-agent-for-kubernetes-supported-cluster-versions"></a>

## 适用于 Kubernetes 的极狐GitLab 代理所支持的集群版本

适用于 Kubernetes 的代理支持以下 Kubernetes 版本。您可以随时将 Kubernetes 版本升级到受支持的版本：

- 1.26 (支持于 2024 年 3 月 22 日或可支持 1.29 时结束)
- 1.25 (支持于 2023 年 10 月 22 日或可支持 1.28 时结束)
- 1.24（支持于 2023 年 9 月 22 日或可支持 1.27 时结束）

极狐GitLab 的目标是在首次发布三个月后支持新的 Kubernetes 次要版本。极狐GitLab 在任何时间都支持至少三个可用于生产的 Kubernetes 小版本。

<!--
当我们放弃对仅支持已弃用 API 的 Kubernetes 版本的支持时，可以从极狐GitLab 代码库中删除对已弃用 API 的支持。

Some GitLab features might work on versions not listed here. [This epic](https://gitlab.com/groups/gitlab-org/-/epics/4827) tracks support for Kubernetes versions.
-->

<!--
## 从旧的基于证书的集成迁移到代理

从基于证书的集成中了解如何[迁移到 Kubernetes 代理](../../infrastructure/clusters/migrate_to_gitlab_agent.md)。

## Related topics

- [GitOps workflow](gitops.md)
- [GitOps examples and learning materials](gitops.md#related-topics)
- [GitLab CI/CD workflow](ci_cd_workflow.md)
- [Install the agent](install/index.md)
- [Work with the agent](repository.md)
- [Troubleshooting](troubleshooting.md)
- [Guided explorations for a production ready GitOps setup](https://gitlab.com/groups/guided-explorations/gl-k8s-agent/gitops/-/wikis/home#gitlab-agent-for-kubernetes-gitops-working-examples)
- [CI/CD for Kubernetes examples and learning materials](ci_cd_workflow.md#related-topics)
- [Contribute to the agent's development](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/tree/master/doc)
-->