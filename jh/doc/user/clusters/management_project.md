---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 集群管理项目（已弃用） **(FREE)**

> - 废弃于 14.5 版本。
> - 在私有化部署版上禁用于 15.0 版本。

WARNING:
集群管理项目废弃于 14.5 版本。
要管理集群应用程序，请使用[极狐GitLab 代理](agent/index.md)和[集群管理项目模板](management_project_template.md)。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，询问管理员启用功能标志 `certificate_based_clusters`。

一个项目可以被指定为一个集群的管理项目。
管理项目可用于运行具有 Kubernetes [`cluster-admin`](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#user-facing-roles) 权限的部署作业。

可以用于：

- 创建流水线，将集群范围的应用程序安装到集群中，有关详细信息，请参阅[管理项目模板](management_project_template.md)。
- 任何需要 `cluster-admin` 权限的作业。

## 权限

只有管理项目获得 `cluster-admin` 权限。所有其它项目继续获得[命名空间范围的 `edit` 级别权限](../project/clusters/cluster_access.md#rbac-cluster-resources)。

管理项目限于以下内容：

- 对于项目级集群，管理项目必须与集群的项目位于相同的命名空间（或下级）中。
- 对于群组级集群，管理项目必须与集群所在的群组在同一群组（或下级）中。
- 对于实例级集群，没有这样的限制。

## 如何创建和配置集群管理项目

要使用集群管理项目来管理您的集群：

1. 创建一个新项目作为集群的集群管理项目。
1. [将集群与管理项目关联](#associate-the-cluster-management-project-with-the-cluster)。
1. [配置集群的流水线](#configuring-your-pipeline)。
1. [设置环境范围](#setting-the-environment-scope)。

<a id="associate-the-cluster-management-project-with-the-cluster"></a>

### 将集群管理项目与集群关联

要将集群管理项目与您的集群关联：

1. 导航到相应的配置页面：
   - 项目级集群，转到项目的 **基础设施 > Kubernetes 集群** 页面。
   - 群组级集群，转到群组的 **Kubernetes** 页面。
   - 实例级集群，在顶部栏，选择 **主菜单 > 管理员 > Kubernetes**。
1. 展开 **高级设置**。
1. 从 **集群管理项目** 下拉列表中，选择您在上一步中创建的集群管理项目。

<a id="configuring-your-pipeline"></a>

### 配置您的流水线

指定一个项目为集群的管理项目后，在该项目中写一个 [`.gitlab-ci.yml`](../../ci/yaml/index.md)。例如：

```yaml
configure cluster:
  stage: deploy
  script: kubectl get namespaces
  environment:
    name: production
```

<a id="setting-the-environment-scope"></a>

### 设置环境范围

[环境范围](../project/clusters/multiple_kubernetes_clusters.md#setting-the-environment-scope)在将多个集群关联到同一个管理项目时可用。

每个范围只能由单个集群用于管理项目。

例如，假设以下 Kubernetes 集群与管理项目相关联：

| 集群     | 环境范围 |
| ----------- | ----------------- |
| Development | `*`               |
| Staging     | `staging`         |
| Production  | `production`      |

[`.gitlab-ci.yml`](../../ci/yaml/index.md) 中设置的以下环境分别部署到 Development、Staging 和 Production 集群。

```yaml
stages:
  - deploy

configure development cluster:
  stage: deploy
  script: kubectl get namespaces
  environment:
    name: development

configure staging cluster:
  stage: deploy
  script: kubectl get namespaces
  environment:
    name: staging

configure production cluster:
  stage: deploy
  script: kubectl get namespaces
  environment:
    name: production
```
