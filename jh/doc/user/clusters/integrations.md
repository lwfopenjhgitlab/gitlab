---
stage: Deploy
group: Environments
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
remove_date: '2023-08-22'
redirect_to: '../index.md'
---

# 集群集成（已删除） **(FREE)**

此功能废弃于 14.7 版本，删除于 16.0 版本。
