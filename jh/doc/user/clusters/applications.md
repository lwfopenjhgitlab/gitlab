---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
remove_date: '2022-08-22'
---

# 极狐GitLab 托管应用（GMA）（已删除） **(FREE)**

此功能废弃于 13.12 版本并删除于 15.0 版本。
使用[集群管理项目模板](management_project_template.md)代替。
