---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
remove_date: '2022-08-22'
---

# Crossplane 配置（已删除） **(FREE)**

此功能废弃于 14.5 版本并删除于 15.0 版本。使用 [crossplane](https://crossplane.io/) 直接代替。
