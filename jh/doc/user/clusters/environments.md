---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 集群环境（已废弃） **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/13392) in GitLab 12.3 for group-level clusters.
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/14809) in GitLab 12.4 for instance-level clusters.
-->

> - 废弃于 14.5 版本。
> - 在私有化部署版上禁用于 15.0 版本。

WARNING:
此功能废弃于 14.5 版本。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，询问管理员启用功能标志 `certificate_based_clusters`。

集群环境提供了将哪些 CI [环境](../../ci/environments/index.md) 部署到 Kubernetes 集群的统一视图，同时：

- 显示与部署相关的项目和相关环境。
- 显示该环境的 pod 的状态。

## 概览

借助集群环境，您可以深入了解：

- 哪些项目部署到集群。
- 每个项目的环境正在使用多少个 pod。
- 用于部署到该环境的 CI 作业。

![Cluster environments page](img/cluster_environments_table_v12_3.png)

对集群环境的访问仅限于群组维护者和所有者。

## 使用

- 为了跟踪集群的环境，您必须成功[部署到 Kubernetes 集群](../project/clusters/deploy_to_cluster.md)。
- 为了正确显示 pod 使用情况，您必须[启用部署看板](../project/deploy_boards.md#enabling-deploy-boards)。

成功部署到群组级或实例级集群后：

1. 导航到您群组的 **Kubernetes** 页面。
1. 选择 **环境** 选项卡。

此页面仅包含成功部署到集群的内容。
不包括非集群环境。
