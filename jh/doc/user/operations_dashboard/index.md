---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 运维仪表盘 **(PREMIUM)**

运维仪表盘提供每个项目运行状况的摘要，包括流水线和警报状态。

要访问仪表盘，请在顶部栏上选择 **主菜单 > 运维**。

## 将项目添加到仪表盘

要将项目添加到仪表盘：

1. 确保您的警报具有您在 Prometheus 中设置的警报上的 `gitlab_environment_name` 标签。此值应与极狐GitLab 中的环境名称匹配。在 13.9 版本中，您只能显示 `production` 环境的警报。
1. 在仪表盘的主屏幕中选择 **添加项目**。
1. 使用 **搜索您的项目** 字段搜索，并添加一个或多个项目。
1. 选择 **添加项目**。

添加后，仪表盘会显示项目的活跃警报数量、上次提交、流水线状态以及上次部署的时间。

运维和[环境](../../ci/environments/environments_dashboard.md)仪表盘共享相同的项目列表。从一个仪表盘中添加或删除项目，会同时从另一个仪表盘中添加或删除项目。

![Operations Dashboard with projects](img/index_operations_dashboard_with_projects.png)

## 在仪表盘上安排项目

您可以拖动项目卡来更改其顺序。卡片顺序目前仅保存到您的浏览器，因此不会更改其他人的仪表盘。

## 登录时将其设为默认仪表盘

运维仪表盘也可以成为您登录时显示的默认极狐GitLab 仪表盘。要使其成为默认仪表盘，请访问您的[配置文件偏好设置](../profile/preferences.md)。
