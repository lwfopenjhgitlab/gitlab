---
type: howto
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 通过上传 manifest 文件导入多个仓库 **(FREE)**

GitLab 允许您基于 manifest 文件导入所有必需的 Git 仓库，例如 [Android 仓库](https://android.googlesource.com/platform/manifest/+/2d6f081a3b05d8ef7a2b1b52b0d536b2b74feab4/default.xml)使用的文件。
当您需要导入具有许多仓库（如 Android 开源项目 (AOSP)）的项目时，此功能非常方便。

## 要求

因为 manifest 导入需要[子组](../../group/subgroups/index.md)才能工作，必须为其数据库使用 PostgreSQL。

阅读有关[数据库要求](../../../install/requirements.md#数据库)的更多信息。

## Manifest 格式

Manifest 必须是 XML 文件，必须有一个带有 `review` 属性的 `remote` 标签，其中包含一个 Git 服务器的 URL，并且每个 `project` 标签必须有一个 `name` 和 `path` 属性。然后，极狐GitLab 将通过将来自 `remote` 标记的 URL 与项目名称相结合来构建仓库的 URL。
路径属性用于表示极狐GitLab 中的项目路径。

下面是一个有效的 manifest 文件示例：

```xml
<manifest>
  <remote review="https://android.googlesource.com/" />

  <project path="build/make" name="platform/build" />
  <project path="build/blueprint" name="platform/build/blueprint" />
</manifest>
```

结果创建了以下项目：

| 极狐GitLab                                          | 导入 URL                                                  |
|:------------------------------------------------|:------------------------------------------------------------|
| `https://jihulab.com/YOUR_GROUP/build/make`      | <https://android.googlesource.com/platform/build>           |
| `https://jihulab.com/YOUR_GROUP/build/blueprint` | <https://android.googlesource.com/platform/build/blueprint> |

## 导入仓库

开始导入：

1. 从您的极狐GitLab 仪表板中单击 **新建项目**。
1. 切换到 **导入项目** 选项卡。
1. 点击 **Manifest 文件** 按钮。
1. 为极狐GitLab 提供一个 manifest XML 文件。
1. 选择您要导入的群组（如果没有群组，则需要先创建群组）。
1. 单击 **列出可用仓库**。此时，您将被重定向到包含基于 manifest 文件的项目列表的导入状态页面。
1. 检查列表并单击 **导入所有仓库** 开始导入。

   ![Manifest status](img/manifest_status_v13_3.png)
