---
type: reference, howto
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 从 Bitbucket 服务器导入您的项目 **(FREE)**

NOTE:
此过程不同于[从 Bitbucket Cloud 导入](bitbucket.md)。

从 Bitbucket Server，您可以导入：

- 仓库描述
- Git 仓库数据
- 拉取请求
- 拉取请求评论

导入时，将保留仓库公开访问权限。如果仓库在 Bitbucket 中是私有的，也会在极狐GitLab 中创建为私有的。

<a id="import-your-bitbucket-repositories"></a>

## 导入您的 Bitbucket 仓库

您可以将 Bitbucket 仓库导入极狐GitLab。

### 先决条件

> 所需权限由开发者变更为维护者于 16.0 版本，并向后移植到 15.11.1 版本和 15.10.5 版本。

- 必须启用 [Bitbucket Server 导入源](../../admin_area/settings/visibility_and_access_controls.md#configure-allowed-import-sources)。如果未启用，请让您的管理员启用它。SaaS 默认启用 Bitbucket Server 导入源。
- 至少是要导入到的目标组的 Maintainer 角色。

### 导入仓库

要导入您的 Bitbucket 仓库：

1. 登录极狐GitLab。
1. 在顶部栏上，选择 **新建** (**{plus}**)。
1. 选择 **新建项目/仓库**。
1. 选择 **导入项目**。
1. 选择 **Bitbucket 服务器**。
1. 登录 Bitbucket 并授予极狐GitLab 访问您的 Bitbucket 帐户的权限。
1. 选择要导入的项目，或导入所有项目。您可以按名称过滤项目，并选择要导入每个项目的命名空间。

### 不导入的事项

不导入以下数据：

- 拉取请求批准
- Markdown 中的附件
- 任务列表
- Emoji 回应

## 导入且被更改的事项

- 极狐GitLab 不允许对任意代码行进行评论。任何越界的 Bitbucket 评论都将作为评论插入到合并请求中。
- Bitbucket 服务器允许多个主题级别。导入器将其折叠成一个主题并引用原始评论的一部分。
- 拒绝的拉取请求具有无法访问的提交，会阻止导入器生成正确的差异。这些拉取请求显示为空更改。
- 项目过滤不支持模糊搜索。仅支持以匹配字符串开头或完全匹配的字符串。


## 用户指派

当议题和拉取请求正在导入时，导入器会尝试在极狐GitLab 用户数据库中，使用已确认的电子邮件地址查找作者的电子邮件地址。如果没有此类用户可用，则将项目创建者设置为作者。导入器在评论中附加注释来标记原始创建者。

如果它们不存在，导入器会创建任何新的命名空间（群组）。如果采用命名空间，仓库将在启动导入过程的用户的命名空间下导入。

### 按用户名指派用户

> - 引入于 13.4 版本，功能标志名为 `bitbucket_server_user_mapping_by_username`。默认禁用。
> - 不推荐用于生产。

<!--
FLAG:
On self-managed GitLab and GitLab.com, by default this feature is not available. To make it
available, ask an administrator to [enable the feature flag](../../../administration/feature_flags.md)
named `bitbucket_server_user_mapping_by_username`. This feature is not ready for production use.
-->

启用此功能后，导入器会尝试在极狐GitLab 用户数据库中使用作者的以下数据查找用户：

- `username`
- `slug`
- `displayName`

如果没有用户匹配这些属性，则将项目创建者设置为作者。

## 故障排查

### 通用

如果基于 GUI 的导入器不起作用，您可以尝试：

- 使用 GitLab Import API<!--[GitLab Import API](../../../api/import.md#import-repository-from-bitbucket-server)--> Bitbucket 服务器端点。
- 设置[仓库镜像](../repository/mirror/index.md)，提供详细的错误输出。

<!--
See the [troubleshooting section](bitbucket.md#troubleshooting)
for Bitbucket Cloud.
-->

### 未导入 LFS 对象

如果项目导入完成，但无法下载或克隆 LFS 对象，则您可能正在使用包含特殊字符的密码或个人访问令牌。<!--For more information, see
[this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/337769).-->

<!--
## Related topics

For information on automating user, group, and project import API calls, see
[Automate group and project import](index.md#automate-group-and-project-import).
-->
