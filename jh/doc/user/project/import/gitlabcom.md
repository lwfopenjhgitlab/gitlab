---
type: howto
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 从 GitLab.com 迁移到 JihuLab.com **(FREE SAAS)**

本文介绍将项目从 GitLab.com 迁移到 JihuLab.com 的简要过程。

## 迁移前准备

- 帐户申请：使用极狐GitLab 前，请确保您已拥有已实名认证的帐户；若您还没有帐户，请先[注册](https://jihulab.com/users/sign_up)，并完成实名认证。

## 仓库迁移操作步骤

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择 **新建项目**。
1. 在 **新建项目** 页面，选择 **导入项目**。
   
   ![importproject](img/gitlabcom_1.png)

1. 选择从 **GitLab.com** 导入。
 
     ![importproject](img/gitlabcom_2.png)

1. 系统将重定向到 GitLab.com，您需要同意授予访问项目的权限。
1. 选择要导入的项目，支持导入所有项目。在 **目标路径** 下，您可以按名称过滤目标群组，并输入每个导入项目的命名空间。

   ![importproject](img/gitlabcom_3.png)