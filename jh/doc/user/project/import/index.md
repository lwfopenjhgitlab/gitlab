---
type: reference, howto
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 将项目迁移到极狐GitLab 实例 **(FREE)**

查看以下文档：

- [从 GitHub 迁移](github.md)
- [从 Gitee 迁移（API）](gitee.md)
- [从 Gitee 迁移（UI）](gitee_ui.md)
- [从 Bitbucket Cloud](bitbucket.md)
- [从 Bitbucket Server（Stash）](bitbucket_server.md)
- [从 SVN](svn.md)
- [从仓库 URL](repo_by_url.md)
- [通过上传 manifest 文件 (AOSP)](manifest.md)

<!--
- [From ClearCase](clearcase.md)
- [From CVS](cvs.md)
- [From FogBugz](fogbugz.md)
- [From GitLab.com](gitlab_com.md)
- [From Gitea](gitea.md)
- [From Perforce](perforce.md)
- [From SVN](svn.md)
- [From TFVC](tfvc.md)
- [From repository by URL](repo_by_url.md)
- [By uploading a manifest file (AOSP)](manifest.md)
- [From Jira (issues only)](jira.md)

You can also import any Git repository through HTTP from the **New Project** page. Note that if the
repository is too large, the import can timeout.

You can also [connect your external repository to get CI/CD benefits](../../../ci/ci_cd_for_external_repos/index.md).
-->

## 项目导入历史

您可以查看您创建的所有项目导入。此列表包括以下内容：

- 来源（出于安全原因没有凭据）
- 导入目的地
- 状态
- 导入失败时的错误详情

查看项目导入历史：

1. 登录极狐GitLab。
1. 在顶部栏上，选择 **新建** (**{plus}**)。
1. 选择 **新建项目/仓库**。
1. 选择 **导入项目**。
1. 选择 **历史记录**。

![Project import history page](img/gitlab_import_history_page_v14_10.png)

历史记录还包括从[内建](../working_with_projects.md#create-a-project-from-a-built-in-template)或[自定义](../working_with_projects.md#create-a-project-from-a-built-in-template)模板创建的项目。极狐GitLab 使用[通过 URL 导入仓库](repo_by_url.md)的方式，从模板创建新项目。

## LFS 认证

导入包含 LFS 对象的项目时，如果项目中有 [`.lfsconfig`](https://github.com/git-lfs/git-lfs/blob/master/docs/man/git-lfs-config.5.ronn) 文件的 URL 主机 (`lfs.url`)，与仓库 URL 主机不同，不会下载 LFS 文件。

<!--
## Migrate from self-managed GitLab to GitLab.com

If you only need to migrate Git repositories, you can [import each project by URL](repo_by_url.md).
However, you can't import issues and merge requests this way. To retain all metadata like issues and
merge requests, use the [import/export feature](../settings/import_export.md)
to export projects from self-managed GitLab and import those projects into GitLab.com. All GitLab
user associations (such as comment author) are changed to the user importing the project. For more
information, see the prerequisites and important notes in these sections:

- [Export a project and its data](../settings/import_export.md#export-a-project-and-its-data).
- [Import the project](../settings/import_export.md#import-a-project-and-its-data).

NOTE:
When migrating to GitLab.com, you must create users manually unless [SCIM](../../../user/group/saml_sso/scim_setup.md)
will be used. Creating users with the API is limited to self-managed instances as it requires
administrator access.

To migrate all data from self-managed to GitLab.com, you can leverage the [API](../../../api/index.md).
Migrate the assets in this order:

1. [Groups](../../../api/groups.md)
1. [Projects](../../../api/projects.md)
1. [Project variables](../../../api/project_level_variables.md)

Keep in mind the limitations of the [import/export feature](../settings/import_export.md#items-that-are-exported).

You must still migrate your [Container Registry](../../packages/container_registry/)
over a series of Docker pulls and pushes. Re-run any CI pipelines to retrieve any build artifacts.

## Migrate from GitLab.com to self-managed GitLab

The process is essentially the same as [migrating from self-managed GitLab to GitLab.com](#migrate-from-self-managed-gitlab-to-gitlabcom).
The main difference is that an administrator can create users on the self-managed GitLab instance
through the UI or the [users API](../../../api/users.md#user-creation).

## Migrate between two self-managed GitLab instances

To migrate from an existing self-managed GitLab instance to a new self-managed GitLab instance, it's
best to [back up](../../../raketasks/backup_restore.md)
the existing instance and restore it on the new instance. For example, this is useful when migrating
a self-managed instance from an old server to a new server.

The backups produced don't depend on the operating system running GitLab. You can therefore use
the restore method to switch between different operating system distributions or versions, as long
as the same GitLab version [is available for installation](../../../administration/package_information/supported_os.md).

To instead merge two self-managed GitLab instances together, use the instructions in
[Migrate from self-managed GitLab to GitLab.com](#migrate-from-self-managed-gitlab-to-gitlabcom).
This method is useful when both self-managed instances have existing data that must be preserved.

Also note that administrators can use the [Users API](../../../api/users.md)
to migrate users.
-->

<a id="project-aliases"></a>

## 项目别名 **(PREMIUM SELF)**

极狐GitLab 仓库通常使用命名空间和项目名称访问。但是，当将经常访问的仓库迁移到极狐GitLab 时，您可以使用项目别名来访问具有原始名称的那些仓库。通过项目别名访问仓库可降低与迁移此类仓库相关的风险。

此功能仅在 Git over SSH 上可用。此外，只有极狐GitLab 管理员可以创建项目别名，而且他们只能通过 API 来创建。有关详细信息，请参阅[项目别名 API 文档](../../../api/project_aliases.md)。

管理员为项目创建别名后，您可以使用别名克隆仓库。例如，如果管理员为项目 `https://jihulab.com/gitlab-cn/gitlab` 创建别名 `gitlab`，您可以使用 `git clone git@jihulab.com:gitlab.git` 克隆该项目，而不是`git clone git@jihulab.com:gitlab-cn/gitlab.git`。

<!--
## Automate group and project import **(PREMIUM)**

The GitLab Professional Services team uses [Congregate](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate)
to orchestrate user, group, and project import API calls. With Congregate, you can migrate data to
GitLab from:

- Other GitLab instances
- GitHub Enterprise
- GitHub.com
- Bitbucket Server
- Bitbucket Data Center

See the [Quick Start Guide](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/blob/master/docs/using-congregate.md#quick-start)
to learn how to use this approach for migrating users, groups, and projects at scale.
-->