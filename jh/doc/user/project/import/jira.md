---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 将 Jira 项目议题导入极狐GitLab **(FREE)**

> 引入于 12.10 版本。

使用极狐GitLab Jira 导入器，您可以将 Jira 议题导入 SaaS 或您自行管理的极狐GitLab 实例。

Jira 问题导入是一个 MVC 项目级功能，这意味着来自多个 Jira 项目的议题可以导入到一个极狐GitLab 项目中。MVC 版本导入议题标题和描述，以及一些其他议题元数据，作为极狐GitLab 议题描述中的一个部分。

## 已知限制

从 Jira 导入到极狐GitLab 字段的信息取决于极狐GitLab 的版本：

- 从 12.10 到 13.1 版本，只直接导入议题的标题和描述。
- 从 13.2 版本开始：
  - 议题的标记也是直接导入的。
  - 在准备导入时，您还可以将 Jira 用户映射到极狐GitLab 项目成员。

其他未正式映射到极狐GitLab 议题字段的 Jira 议题元数据将作为纯文本导入到极狐GitLab 议题的描述中。

我们用于将 Jira 问题中的文本转换为 GitLab Flavored Markdown 的解析器仅与 Jira V3 REST API 兼容。

<!--
There is an [epic](https://gitlab.com/groups/gitlab-org/-/epics/2738) tracking the addition of issue assignees, comments, and much more in the future
iterations of the GitLab Jira importer.
-->

## 先决条件

### 权限

为了能够从 Jira 项目导入议题，您必须具有 Jira 议题的读取权限，并且至少在您希望导入的极狐GitLab 项目中具有维护者角色。

### Jira 集成

此功能使用现有的极狐GitLab [Jira 集成](../../../integration/jira/index.md)。

在尝试导入 Jira 议题之前，请确保已设置集成。

## 将 Jira 议题导入极狐GitLab

NOTE:
导入 Jira 问题是作为异步后台作业完成的，这可能会导致基于导入队列负载、系统负载或其他因素的延迟。导入大型项目可能需要几分钟时间，具体取决于导入的大小。

将 Jira 议题导入极狐GitLab 项目：

1. 在 **{issues}** **议题** 页面上，选择 **导入议题** (**{import}**) **> 从 Jira 导入**。

   ![Import issues from Jira button](img/jira/import_issues_from_jira_button_v12_10.png)

   **从 Jira 导入**选项仅在您拥有[正确权限](#permissions)时可见。

   出现以下表格。如果您之前设置了 [Jira 集成](../../../integration/jira/index.md)，您现在可以在下拉列表中看到您有权访问的 Jira 项目。

   ![Import issues from Jira form](img/jira/import_issues_from_jira_form_v13_2.png)

1. 选择 **导入自** 下拉列表并选择您希望从中导入议题的 Jira 项目。

   在 **Jira-GitLab 用户映射模板** 部分，该表显示了您的 Jira 用户映射到哪些极狐GitLab 用户。当表单出现时，下拉列表默认为执行导入的用户。

1. 要更改任何映射，请选择 **极狐GitLab 用户名** 列中的下拉列表，然后选择要映射到每个 Jira 用户的用户。

   下拉列表可能不会显示所有用户，因此请使用搜索栏查找此极狐GitLab 项目中的特定用户。

1. 选择 **继续**。系统会向您显示导入已开始的确认信息。

   当导入在后台运行时，您可以从导入状态页面导航到议题页面，您可以看到新议题出现在议题列表中。

1. 要检查导入状态，请再次转到 Jira 导入页面。
