---
type: howto
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 通过 URL 从仓库导入项目 **(FREE)**

您可以通过提供 Git URL 来导入现有仓库：

1. 在顶部栏上，选择 **菜单 > 创建新项目**。
1. 选择 **导入项目** 选项卡。
1. 选择 **仓库（URL）**。
1. 输入 **Git 仓库 URL**。
1. 填写其余字段。
1. 选择 **创建项目**。

将显示您新创建的项目。

<!--
## 自动化群组和项目导入 **(PREMIUM)**

有关自动化用户、群组和项目导入 API 调用的信息，请参阅[自动化群组和项目导入](index.md#automate-group-and-project-import)。
-->
