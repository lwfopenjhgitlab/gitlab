---
type: reference, howto
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 将您的项目从 Bitbucket Cloud 导入极狐GitLab **(FREE)**

NOTE:
Bitbucket Cloud 导入器仅适用于 [Bitbucket.org](https://bitbucket.org/)，不适用于 Bitbucket Server（又名 Stash）。如果您尝试从 Bitbucket Server 导入项目，请使用 [Bitbucket Server 导入器](bitbucket_server.md)。

轻松将您的项目从 Bitbucket Cloud 导入极狐GitLab。

Bitbucket 导入器可以导入：

- 仓库描述
- Git 仓库数据
- 议题
- 议题评论
- 拉取请求
- 拉取请求评论
- 里程碑
- Wiki

导入时：

- 保留对拉取请求和议题的引用。
- 保留仓库公共访问权限。如果仓库在 Bitbucket 中是私有的，它也会在极狐GitLab 中创建为私有的。

## 先决条件

- 要从 Bitbucket Cloud 导入您的项目，必须启用 [Bitbucket Cloud 集成](../../../integration/bitbucket.md)。如果尚未启用，请让您的管理员启用它。
- [Bitbucket Cloud 导入源](../../admin_area/settings/visibility_and_access_controls.md#configure-allowed-import-sources)必须启用。如果未启用，请让您的管理员启用它。SaaS 默认启用 Bitbucket Cloud 导入源。
- 至少具有导入目标群组的维护者角色。

## 工作原理

当导入议题/拉取请求时，Bitbucket 导入器使用作者/受让人的 Bitbucket 昵称，并尝试在极狐GitLab 中找到相同的 Bitbucket 身份。如果它们不匹配或在极狐GitLab 数据库中找不到用户，则将项目创建者（大多数情况下是启动导入过程的当前用户）设置为作者，但有关原始 Bitbucket 议题的作者的引用被保留。

导入器将创建任何新的命名空间（组），如果它们不存在，或者在命名空间被占用的情况下，仓库将在启动导入过程的用户命名空间下导入。

## 用户映射贡献的要求

对于要映射的用户贡献，每个用户必须在项目导入之前完成以下操作：

1. 验证 [Bitbucket 账户设置](https://bitbucket.org/account/settings/) 中的用户名是否与 [Atlassian 账户设置](https://id.atlassian.com/manage-profile/profile-and-visibility)中的公共名称匹配。如果不匹配，请修改 Atlassian 帐户设置中的公开名称来匹配 Bitbucket 帐户设置中的用户名。

1. 在[极狐GitLab 个人资料内的服务登录](https://jihulab.com/-/profile/account)中，连接您的 Bitbucket 帐户。

1. [设置您的公开电子邮件](../../profile/index.md#设置您的公开电子邮件)。

<a id="import-your-bitbucket-repositories"></a>

## 导入您的 Bitbucket 仓库

1. 登录极狐GitLab。
1. 在顶部栏上，选择 **新建** (**{plus}**)。
1. 选择 **新建项目/仓库**。
1. 选择 **导入项目**。
1. 选择 **Bitbucket Cloud**。
1. 登录 Bitbucket 并授予极狐GitLab 访问您的 Bitbucket 帐户的权限。

   ![Grant access](img/bitbucket_import_grant_access.png)

1. 选择您要导入的项目或导入所有项目。您可以按名称过滤项目，并选择每个项目将为其导入的命名空间。

   ![Import projects](img/bitbucket_import_select_project_v12_3.png)

## 故障排查

### 如果您有多个 Bitbucket 帐户

请务必登录正确的帐户。

如果您不小心使用错误的帐户启动了导入过程，请按照以下步骤操作：

1. 撤销极狐GitLab 对您的 Bitbucket 帐户的访问权限，实质上是在以下过程中反转该过程：[导入您的 Bitbucket 仓库](#import-your-bitbucket-repositories)。

1. 退出 Bitbucket 帐户。 按照上一步链接的过程进行操作。

<!--
### User mapping fails despite matching names

[For user mapping to work](#requirements-for-user-mapped-contributions),
the username in the Bitbucket account settings must match the public name in the Atlassian account
settings. If these names match but user mapping still fails, the user may have modified their
Bitbucket username after connecting their Bitbucket account in the
[GitLab profile social sign-in](https://gitlab.com/-/profile/account).

To fix this, the user must verify that their Bitbucket external UID in the GitLab database matches their
current Bitbucket public name, and reconnect if there's a mismatch:

1. [Use the API to get the currently authenticated user](../../../api/users.md#list-current-user-for-normal-users).

1. In the API's response, the `identities` attribute contains the Bitbucket account that exists in
   the GitLab database. If the `extern_uid` doesn't match the current Bitbucket public name, the
   user should reconnect their Bitbucket account in the [GitLab profile social sign-in](https://gitlab.com/-/profile/account).

1. Following reconnection, the user should use the API again to verify that their `extern_uid` in
   the GitLab database now matches their current Bitbucket public name.

The importer must then [delete the imported project](../../project/working_with_projects.md#delete-a-project)
and import again.
-->
