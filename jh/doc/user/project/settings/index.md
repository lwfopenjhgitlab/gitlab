---
stage: Manage
group: Workspace
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, index, howto
---

# 项目设置 **(FREE)**

使用**设置**页面来管理[项目](../index.md)中的配置选项。

## 查看项目设置

您必须至少具有维护者角色才能查看项目设置。

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 要显示一个部分中的所有设置，请选择 **展开**。
1. 可选。使用搜索框查找设置。

<a id="edit-project-name-and-description"></a>

## 编辑项目名称和描述

使用项目通用设置来编辑您的项目详细信息。

1. 至少以维护者角色登录极狐GitLab。
1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 在 **项目名称** 文本框中，输入您的项目名称。
1. 在 **项目描述** 文本框中，输入您的项目描述。
1. 在 **项目头像** 下，要更改您的项目头像，请选择 **选择文件**。

## 将主题分配给项目

使用主题对项目进行分类并查找类似的新项目。

将主题分配给项目：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 在 **主题** 文本框中，输入项目主题。键入时会建议热门主题。
1. 选择 **保存更改**。

<a id="add-a-compliance-framework-to-a-project"></a>

## 向项目添加合规性框架 **(PREMIUM)**

[合规框架](../../group/compliance_frameworks.md)可以分配给群组内具有合规框架的项目，使用以下任一方法：

- UI：
  1. 在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目** 并找到您的项目。
  1. 在左侧边栏中，选择 **设置** > **通用**。
  1. 展开 **合规框架** 部分。
  1. 选择合规框架。
  1. 选择 **保存更改**。
- 在 14.2 或更高版本，使用 GraphQL API。如果您使用 GraphQL 在子组上创建合规性框架，如果用户具有正确的权限，则会在根祖先上创建框架。UI 提供了一个只读视图来阻止这种行为。

<a id="configure-project-visibility-features-and-permissions"></a>

## 配置项目可见性、功能和权限

要为项目配置可见性、功能和权限：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **可见性、项目功能、权限** 部分。
1. 要更改项目可见性，请选择下拉列表。如果您选择 **公开**，则会将某些功能的访问权限限制为 **仅项目成员**。
1. 要允许用户请求访问项目，请选中 **用户可以请求访问** 复选框。
1. 使用切换开关，启用或禁用项目中的功能。
1. 选择 **保存更改**。

### 项目功能设置

使用切换开关来启用或禁用项目中的功能。

| 选项                          | 更多访问限制选项 | 描述   |
| :--------------------------------- | :-------------------------- | :-------------- |
| **议题**                       | ✓                         | 激活极狐GitLab 议题跟踪器 |
| **仓库**                   | ✓                         | 启用仓库功能 |
| **合并请求**               | ✓                         | 启用合并请求功能 |
| **派生**                        | ✓                         | 启用派生功能 |
| **Git 大文件存储 (LFS)** |                           | 允许使用大文件 |
| **软件包**                     |                           | 支持软件包库功能 |
| **CI/CD**                        | ✓                         | 启用 CI/CD 功能 |
| **Container Registry**           |                           | 为您的 Docker 镜像激活仓库 |
| **分析**                    | ✓                         | 启用分析 |
| **需求**                 | ✓                         | 控制对需求管理的访问 |
| **安全与合规**        | ✓                         | 控制对安全功能的访问 |
| **Wiki**                         | ✓                         | 启用单独的文档系统 |
| **代码片段**                     | ✓                         | 允许分享代码和文本 |
| **Pages**                        | ✓                         | 允许您发布静态网站 |
| **指标仪表盘**            | ✓                         | 控制对指标仪表板的访问 |
| **发布**                     | ✓                         | 控制对[发布](../releases/index.md)的访问                                                                                                                                                                                      |
| **环境**                 | ✓                         | 控制对[环境和部署](../../../ci/environments/index.md)的访问                                                                   |
| **功能标志**                | ✓                         | 控制对[功能标志](../../../operations/feature_flags.md)的访问。                                                                               |
| **监控**                      | ✓                         | 控制对[监控](../../../operations/index.md)功能的访问。                                                                                            |
| **基础设施**               | ✓                         | 控制对[基础设施](../../infrastructure/index.md)功能的访问。                                                                                                    |

当您禁用某个功能时，以下附加功能也会被禁用：

- 如果禁用**议题**功能，项目用户将无法使用：

   - **议题看板**
   - **服务台**
   - 项目用户仍然可以通过合并请求访问**里程碑**。

- 如果禁用**议题**和**合并请求**，项目用户将无法使用：

  - **标记**
  - **里程碑**

- 如果您禁用**仓库**，项目用户将无法使用：

  - **合并请求**
  - **CI/CD**
  - **容器镜像库**
  - **Git 大文件存储**
  - **软件包**

- 指标仪表盘访问需要读取项目环境和部署。有权访问指标仪表板的用户还可以访问环境和部署。

<!--
#### Disabling the CVE ID request button **(FREE SAAS)**

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41203) in GitLab 13.4, only for public projects on GitLab.com.

In applicable environments, a [**Create CVE ID Request** button](../../application_security/cve_id_request.md)
is present in the issue sidebar. The button may be disabled on a per-project basis by toggling the
setting **Enable CVE ID requests in the issue sidebar**.

![CVE ID Request toggle](img/cve_id_request_toggle.png)
-->

## 禁用项目电子邮件通知

先决条件：

- 您必须是项目的所有者才能禁用与项目相关的电子邮件通知。

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **可见性、项目功能、权限** 部分。
1. 清除 **禁用电子邮件通知** 复选框。

## 为项目配置合并请求设置

设置项目的合并请求设置：

- 设置[合并请求方法](../merge_requests/methods/index.md)（合并提交，快进合并）。
- 添加合并请求[描述模板](../description_templates.md#description-templates)。
- 启用[合并请求批准](../merge_requests/approvals/index.md)。
- 启用[状态检查](../merge_requests/status_checks.md)。
- 启用[仅在流水线成功时合并](../merge_requests/merge_when_pipeline_succeeds.md)。
- 启用[仅在所有主题都解决时合并](../../discussions/index.md#prevent-merge-unless-all-threads-are-resolved)。
- 启用<!--[需要来自 Jira 的关联问题](../../../integration/jira/issues.md#require-associated-jira-issue-for-merge-requests-to-be-merged)-->需要来自 Jira 的关联议题。
- 默认启用[**接受合并请求时删除源分支**](#delete-the-source-branch-on-merge-by-default)。
- 配置[建议的更改提交消息](../merge_requests/reviews/suggestions.md#configure-the-commit-message-for-applied-suggestions)。
- 配置[合并和压缩提交消息模板](../merge_requests/commit_templates.md)。
- 为来自派生项目的合并请求，配置[默认目标项目](../merge_requests/creating_merge_requests.md#set-the-default-target-project)。

## 服务台

为您的项目启用[服务台](../service_desk.md)以提供客户支持。

<!--
### Export project

Learn how to [export a project](import_export.md#import-the-project) in GitLab.
-->

## 高级项目设置

使用高级设置来归档、重命名、转移、删除派生关系或删除项目。

### 归档项目

归档项目时，仓库、软件包、议题、合并请求和所有其他功能都是只读的。存档的项目也会从项目列表中隐藏。

要归档项目：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 在 **存档项目** 部分，选择 **存档项目**。
1. 要确认，请选择 **OK**。

### 取消归档项目

取消归档项目时，您将删除只读限制并使其在项目列表中可用。

先决条件：

- 要取消归档项目，您必须是管理员或项目所有者。

1. 找到存档的项目。
     1. 在顶部栏上，选择 **主菜单 > 项目**。
     1. 选择 **浏览项目**。
     1. 在 **排序项目** 下拉列表，选择 **显示已归档的项目**。
     1. 在 **按名称过滤** 字段中，输入项目名称。
     1. 选择项目链接。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 在 **高级** 下，单击 **扩展**。
1. 在 **取消归档项目** 部分，单击 **取消归档项目** 按钮。
1. 要确认，选择 **OK**。

<a id="rename-a-repository"></a>

## 重命名仓库

项目的仓库名称定义了它的 URL 和它安装在极狐GitLab 实例文件磁盘上的位置。

先决条件：

- 您必须是项目维护者或管理员才能重命名仓库。

NOTE:
当您更改仓库路径时，如果用户推送到旧 URL 或从旧 URL 拉取，他们可能会遇到问题。有关更多信息，请参阅[重命名仓库时重定向](../repository/index.md#what-happens-when-a-repository-path-changes)。

要重命名仓库：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **高级** 部分。
1. 在 **更改路径** 文本框中，编辑路径。
1. 选择 **更改路径**。

<a id="delete-the-source-branch-on-merge-by-default"></a>

## 默认情况下删除合并时的源分支

在合并请求中，您可以更改设置，默认始终选中**删除源分支**复选框。

要设置此默认值：

1. 在顶部栏中，选择 **主菜单 > 项目**，并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 合并请求**。
1. 选择 **默认启用“删除源分支”选项**。
1. 选择 **保存更改**。

<a id="transfer-a-project-to-another-namespace"></a>

## 将项目转移到另一个命名空间

当您将项目转移到另一个命名空间时，您将项目移动到不同的群组。

先决条件：

- 您必须至少具有要转移到的[群组](../../group/manage.md#create-a-group)的维护者角色。
- 您必须是要转移的项目的所有者。
- 该群组必须允许创建新项目。
- 该项目不得包含任何[容器镜像](../../packages/container_registry/index.md#limitations)。
- 删除任何 npm 包。如果将项目转移到不同的根命名空间，则该项目不得包含任何 npm 包。当您更新用户或群组的路径，或者转移子组或项目时，您必须先删除所有 npm 包。您不能使用 npm 包更新项目的根命名空间。确保更新 `.npmrc` 文件以遵循命名约定，并在必要时运行 `npm publish`。
- 如果为项目分配了安全策略，则在传输过程中会自动取消分配。

要转移项目：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 在 **转移项目** 下，选择要将项目转移到的命名空间。
1. 选择 **转移项目**。
1. 输入项目名称并选择 **确认**。

您将被重定向到项目的新页面，极狐GitLab 会应用重定向。有关仓库重定向的更多信息，请参阅[仓库路径更改时会发生什么](../repository/index.md#what-happens-when-a-repository-path-changes)。

NOTE:
如果您是管理员，您还可以使用管理界面<!--[管理界面](../../admin_area/index.md#administering-projects)-->将任何项目移动到任何命名空间。

<!--
##### Transferring a GitLab.com project to a different subscription tier

When you transfer a project from a namespace that's licensed for GitLab SaaS Premium or Ultimate to Free, some data related to the paid features is deleted.

For example, [project access tokens](../../../user/project/settings/project_access_tokens.md) are revoked, and
[pipeline subscriptions](../../../ci/pipelines/multi_project_pipelines.md#trigger-a-pipeline-when-an-upstream-project-is-rebuilt)
and [test cases](../../../ci/test_cases/index.md) are deleted.
-->

<a id="delete-a-project"></a>

## 删除一个项目

您可以标记要删除的项目。

先决条件：

- 您必须具有项目的所有者角色。

要删除项目：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 在“删除项目”部分，选择 **删除项目**。
1. 根据要求确认操作。

此操作将删除包含所有关联资源（议题、合并请求等）的项目。

<!--
WARNING:
The default deletion behavior for projects was changed to [delayed project deletion](https://gitlab.com/gitlab-org/gitlab/-/issues/32935)
in GitLab 12.6, and then to [immediate deletion](https://gitlab.com/gitlab-org/gitlab/-/issues/220382) in GitLab 13.2.
-->

<a id="delayed-project-deletion"></a>

### 延迟项目删除 **(PREMIUM)**

> - 在 15.1 版本中，可以为个人命名空间中的项目启用。
> - 为个人命名空间中的项目禁用于 15.3 版本。

延迟一段时间后可以删除群组中的项目（非个人命名空间中）。多个设置会影响是否为特定项目启用延迟项目删除：

- 在私有化部署版实例设置，<!--[设置](../../admin_area/settings/visibility_and_access_controls.md#default-delayed-project-deletion).-->您可以启用延迟删除项目作为新群组的默认设置，并配置延迟天数。<!--For GitLab.com, see the [GitLab.com settings](../../gitlab_com/index.md#delayed-project-deletion).-->
- 在群组设置中<!--[设置](../../group/index.md#enable-delayed-project-deletion)-->，为群组中的所有项目启用延迟项目删除。

### 立即删除一个项目 **(PREMIUM)**

> 引入于 14.1 版本。

如果您不想等待，可以立即删除项目。

先决条件：

- 您必须具有项目的所有者角色。
- 您已[将项目标记为删除](#delete-a-project)。

要立即删除标记为删除的项目：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 在“永久删除项目”部分，选择**删除项目**。
1. 根据要求确认操作。

删除了以下内容：

- 您的项目及其仓库。
- 所有相关资源，包括议题和合并请求。

<a id="restore-a-project"></a>

## 恢复项目 **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/32935) in GitLab 12.6.
-->

要恢复标记为删除的项目：

1. 导航到您的项目，然后选择 **设置 > 通用 > 高级**。
1. 在恢复项目部分，点击 **恢复项目** 按钮。

<a id="remove-a-fork-relationship"></a>

## 删除一个派生关系

先决条件：

- 您必须是项目所有者才能删除派生关系。

WARNING:
删除后，您无法向源发送合并请求，如果有人派生了您的项目，他们的派生也会失去关系。<!--要恢复分叉关系，[使用 API](../../../api/projects.md#create-a-forked-fromto-relation-between-existing-projects)。-->

要删除派生关系：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 在 **删除派生关系** 部分，选择 **删除派生关系**。
1. 确认，输入项目路径并选择 **确认**。

<!--
## Monitor settings

### Alerts

Configure [alert integrations](../../../operations/incident_management/integrations.md#configuration) to triage and manage critical problems in your application as [alerts](../../../operations/incident_management/alerts.md).

### Incidents

#### Alert integration

Automatically [create](../../../operations/incident_management/incidents.md#create-incidents-automatically), [notify on](../../../operations/incident_management/paging.md#email-notifications), and [resolve](../../../operations/incident_management/incidents.md#automatically-close-incidents-via-recovery-alerts) incidents based on GitLab alerts.

#### PagerDuty integration

[Create incidents in GitLab for each PagerDuty incident](../../../operations/incident_management/incidents.md#create-incidents-via-the-pagerduty-webhook).

#### Incident settings

[Manage Service Level Agreements for incidents](../../../operations/incident_management/incidents.md#service-level-agreement-countdown-timer) with an SLA countdown timer.

### Error Tracking

Configure Error Tracking to discover and view [Sentry errors within GitLab](../../../operations/error_tracking.md).

### Status Page

[Add Storage credentials](../../../operations/incident_management/status_page.md#sync-incidents-to-the-status-page)
to enable the syncing of public Issues to a [deployed status page](../../../operations/incident_management/status_page.md#create-a-status-page-project).
-->

## 故障排除

使用项目设置时，您可能会遇到以下问题，或者需要其他方法来完成特定任务。

### 通过控制台删除派生关系

如果通过 UI 或 API 删除分支不起作用，您可以尝试在 [Rails 控制台会话](../../../administration/operations/rails_console.md#starting-a-rails-console-session)操作。

```ruby
p = Project.find_by_full_path('<project_path>')
u = User.find_by_username('<username>')
Projects::UnlinkForkService.new(p, u).execute
```

### 通过控制台转移项目

如果通过 UI 或 API 转移项目不起作用，您可以尝试在 [Rails 控制台会话](../../../administration/operations/rails_console.md#starting-a-rails-console-session)操作。

```ruby
p = Project.find_by_full_path('<project_path>')

# To set the owner of the project
current_user = p.creator

# Namespace where you want this to be moved
namespace = Namespace.find_by_full_path("<new_namespace>")

Projects::TransferService.new(p, current_user).execute(namespace)
```
