---
stage: Manage
group: Import
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
---

# 使用文件导出迁移项目 **(FREE)**

任何私有化部署实例或 SaaS 上的现有项目都可以导出到文件中，然后导入到新的极狐GitLab 实例中。您还可以：

- 使用首选方法[迁移群组](../../group/import/index.md)。
- [使用文件导出迁移群组](../../group/settings/import_export.md)。

当使用管理员访问令牌执行导入时，极狐GitLab 正确映射用户贡献。

因此，当您将项目从私有化部署实例导入 JiHuLab.com 时，使用文件导出迁移项目不会正确映射用户贡献。

相反，所有极狐GitLab 用户关联（例如评论作者）都更改为导入项目的用户。有关详细信息，请参阅以下部分中的先决条件和重要说明：

- [导出项目及其数据](../settings/import_export.md#export-a-project-and-its-data)
- [导入项目](../settings/import_export.md#import-a-project-and-its-data)

为了保留贡献历史，[使用直接传输进行迁移](../../group/import/index.md#migrate-groups-by-direct-transfer-recommended)。

如果您从 JiHuLab.com 迁移到私有化部署版的极狐GitLab 实例，管理员可以在私有化部署版的极狐GitLab 实例上创建用户。

<a id="compatibility"></a>

## 兼容性

FLAG:
默认情况下，在私有化部署版实例的项目中，文件导出为 NDJSON 格式。要使极狐GitLab 生成 JSON 格式的项目文件导出，请要求管理员[禁用功能标志](../../../administration/feature_flags.md) `project_export_as_ndjson`。要允许极狐GitLab 以 JSON 格式导入项目文件导出，请要求管理员[禁用功能标志](../../../administration/feature_flags.md) `project_import_ndjson`。在 JiHuLab.com 上，项目文件导出仅采用 NDJSON 格式。

项目文件导出为 NDJSON 格式。在 14.0 版本之前，极狐GitLab 生成 JSON 格式的项目文件导出。
为了支持转换，如果配置相关的功能标志，您仍然可以导入 JSON 格式的项目文件导出。

<!--
From GitLab 13.0, GitLab can import project file exports that were exported from a version of GitLab up to two
[minor](../../../policy/maintenance.md#versioning) versions behind, which is similar to our process for
[security releases](../../../policy/maintenance.md#security-releases).

For example:

| Destination version | Compatible source versions |
|:--------------------|:---------------------------|
| 13.0                | 13.0, 12.10, 12.9          |
| 13.1                | 13.1, 13.0, 12.10          |
-->

## 将文件导出配置为导入源 **(FREE SELF)**

在私有化部署版实例上使用文件导出迁移项目之前，极狐GitLab 管理员必须：

1. 在源实例上[启用文件导出](../../admin_area/settings/visibility_and_access_controls.md#enable-project-export)。
1. 启用文件导出作为目标实例的导入源。在 JiHuLab.com 上，文件导出已作为导入源启用。

启用文件导出作为目标实例的导入源：

1. 在顶部栏中，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **可见性和访问控制**。
1. 滚动到 **导入来源**。
1. 选择 **极狐GitLab 导出**。

<!--
## Between CE and EE

You can export projects from the [Community Edition to the Enterprise Edition](https://about.gitlab.com/install/ce-or-ee/)
and vice versa. This assumes [version history](#version-history)
requirements are met.

If you're exporting a project from the Enterprise Edition to the Community Edition, you may lose
data that is retained only in the Enterprise Edition. For more information, see
[downgrading from EE to CE](../../../index.md).
-->

<a id="export-a-project-and-its-data"></a>

## 导出项目及其数据

在您可以导入项目之前，您必须将其导出。

先决条件：

- 查看[导出的数据](#items-that-are-exported)的列表，并非所有数据都导出。
- 您必须至少具有项目的维护者角色。
- 用户必须在源实例中[设置公开电子邮件](../../profile/index.md#set-your-public-email)，该电子邮件与他们在目标实例中经过验证的电子邮件之一相匹配，用户映射才能正常工作。

要导出项目及其数据，请执行以下步骤：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 选择 **导出项目**。
1. 导出生成后，您应该会收到一封电子邮件，其中包含下载文件的链接。
1. 或者，您可以返回项目设置，并从那里下载文件或生成新的导出。文件可用后，页面显示 **下载导出** 按钮。

导出是在您配置的 `shared_path`（一个临时共享目录）中生成的，然后移动到您配置的 `uploads_directory`。每 24 小时，worker 会删除这些导出文件。

<a id="items-that-are-exported"></a>

### 导出的数据

导出以下数据：

- 项目和 wiki 仓库
- 项目上传文件
- 项目配置，不包括集成
- 议题
  - 议题评论
  - 议题资源状态事件（引入于 15.4 版本）
  - 议题资源里程碑事件（引入于 15.4 版本）
- 合并请求
  - 合并请求差异
  - 合并请求评论
  - 合并请求资源状态事件（引入于 15.4 版本）
- 标记
- 里程碑
- 代码片段
- 时间跟踪和其他项目实体
- 设计管理文件和数据
- LFS 对象
- 议题看板
- 流水线历史
- 推送规则
- 赞
- 群组成员导出为项目成员，只要用户在导出的项目群组中具有维护者或管理员角色，

**不**导出：

- 子流水线历史
- 构建跟踪和产物
- 软件包和容器镜像库镜像
- CI/CD 变量
- 流水线触发器
- Webhooks
- 任何加密令牌
- 合并请求核准人和所需的批准数量
- 仓库大小限制
- 允许推送到受保护分支的部署密钥
- 安全文件

<a id="import-a-project-and-its-data"></a>

## 导入项目及其数据

> 默认最大导入文件大小从 50 MB 修改为无限制于 13.8 版本。

WARNING:
仅从您信任的来源导入项目。如果您从不受信任的来源导入项目，攻击者可能会窃取您的敏感数据。

先决条件：

- 您必须已[导出项目及其数据](#export-a-project-and-its-data)。
- 比较极狐GitLab 版本，并确保您要导入的极狐GitLab 版本比您导出到的极狐GitLab 版本相同或更高。
- 查看[兼容性](#compatibility)是否有任何问题。
- 至少具有迁移目标群组的维护者角色。对开发者角色的支持废弃于 15.8 版本，并将在 16.0 版本中删除。

导入项目：

1. 新建项目时，选择 **导入项目**。
1. 在 **导入项目自** 中，选择 **GitLab导出**。
1. 输入您的项目名称和 URL。然后选择您之前导出的文件。
1. 选择 **导入项目** 开始导入。您新导入的项目页面很快就会出现。

<!--
To get the status of an import, you can query it through the [Project import/export API](../../../api/project_import_export.md#import-status).
As described in the API documentation, the query may return an import error or exceptions.
-->

### 导入的数据

以下数据将被导入但略有更改：

- 具有所有者角色的项目成员作为维护者导入。
- 如果导入的项目包含源自派生项目的合并请求，则在导入/导出期间，在项目中创建与此类合并请求关联的新分支。因此，导出项目中的分支数量可能比原始项目中的多。
- 如果使用 `Internal` 可见性级别[受到限制](../../public_access.md#restrict-use-of-public-or-internal-projects)，则所有导入的项目可见性都被指定为 `Private`。

未导入部署密钥。要使用部署密钥，您必须在导入的项目中启用它们并更新受保护的分支。

<!--
### 导入大型项目 **(FREE SELF)**

如果您有更大的项目，请考虑使用 [开发人员文档](../../../development/import_project.md#importing-via-a-rake-task) 中描述的 Rake 任务。


## Automate group and project import **(PREMIUM)**

For information on automating user, group, and project import API calls, see
[Automate group and project import](../import/index.md#automate-group-and-project-import).
-->

<a id="maximum-import-file-size"></a>

## 最大导入文件大小

<!--
管理员可以通过以下两种方式之一设置最大导入文件大小：

- With the `max_import_size` option in the [Application settings API](../../../api/settings.md#change-application-settings).
- In the [Admin Area UI](../../admin_area/settings/account_and_limit_settings.md#max-import-size).
-->

管理员可以通过[管理中心 UI](../../admin_area/settings/account_and_limit_settings.md#最大导入大小) 设置最大导入文件大小。

默认值为 `0`（无限制）。

## 映射导入的用户

如果管理员（不是所有者）进行导入，则导入的用户可以通过他们在私有化部署实例上的公共电子邮件地址进行映射。

- 项目必须由具有所有者角色的项目或群组成员导出。
- 默认情况下未设置公共电子邮件地址。用户必须[在他们的配置文件中设置它](../../profile/index.md#设置您的公开电子邮件)，才能使映射正常工作。
- 为了正确映射贡献，用户必须是命名空间的现有成员，或者可以添加为项目的成员。否则，将留下补充评论来提及原始作者和导入者拥有的 MR、注释或议题。
- 导入的用户在导入的项目中设置为[直接成员](../members/index.md)。

<!--
For project migration imports performed over GitLab.com groups, preserving author information is
possible through a [professional services engagement](https://about.gitlab.com/services/migration/).
-->

<a id="rate-limits"></a>

## 速率限制

为了帮助避免滥用，默认情况下，用户的速率限制为：

| 请求类型     | 限制 |
| ---------------- | ----- |
| 导出          | 每分钟 6 个项目 |
| 下载导出  | 每个群组每分钟下载 1 个 |
| 导入           | 每分钟 6 个项目 |
