---
stage: Manage
group: Access
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, howto
---

# 项目访问令牌

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/210181) in GitLab 13.0.
> - [Became available on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/235765) in GitLab 13.5 for paid groups only.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/235765) in GitLab 13.5.
-->

项目访问令牌类似于密码，但您可以[限制对资源的访问](#scopes-for-a-project-access-token)、选择受限角色并提供到期日期。

使用项目访问令牌进行身份验证：

- 使用 GitLab API<!--[GitLab API](../../../api/index.md#personalproject-access-tokens)-->。
- 使用 Git，当使用 HTTP 基本身份验证时，使用：
   - 任何非空白值作为用户名。
   - 项目访问令牌作为密码。

配置项目访问令牌后，进行身份验证时不需要密码。相反，您可以输入任何非空白值。

项目访问令牌类似于[个人访问令牌](../../profile/personal_access_tokens.md)。

在私有化部署实例中，如果设置了限制，项目访问令牌的最大生命周期限制与个人访问令牌相同。

WARNING:
创建没有过期的项目访问令牌的功能废弃于 15.4 版本，并删除于 16.0 版本。在 16.0 及更高版本中，没有到期日期的现有项目访问令牌会自动指定一个比当前日期晚 365 天的到期日期。当私有化部署实例升级到 16.0 版本时，会自动添加到期日期。

您可以使用项目访问令牌：

- 在 SaaS 版上，如果您拥有专业版或旗舰版许可证。项目访问令牌不适用于[试用许可证](https://about.gitlab.cn/free-trial/)。

- 在具有任何许可证级别的自助管理实例上。如果您有标准版：
  - 查看围绕用户自行注册<!--[用户自行注册](../../admin_area/settings/sign_up_restrictions.md#disable-new-sign-ups)-->的安全和合规策略。
  - 考虑[禁用项目访问令牌](#enable-or-disable-project-access-token-creation)以减少潜在的滥用。

您不能使用项目访问令牌来创建其它群组、项目或个人访问令牌。

项目访问令牌继承为个人访问令牌配置的默认前缀设置<!--[默认前缀设置](../../admin_area/settings/account_and_limit_settings.md#personal-access-token-prefix)-->。

NOTE:
项目访问令牌不兼容 FIPS，并且在启用 FIPS 模式时会禁用创建和使用。

## 创建项目访问令牌

> - 于 15.1 版本中引入，所有者可以为项目访问令牌选择所有者角色。
> - 于 15.3 版本中引入，默认到期时间为 30 天，并且在 UI 中填充默认的访客角色。
> - 创建没有过期的项目访问令牌的功能删除于 16.0 版本。

WARNING:
项目访问令牌被视为内部用户。如果内部用户创建项目访问令牌，则该令牌能够访问可见性级别设置为[内部](../../public_access.md)的所有项目。

要创建项目访问令牌：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 访问令牌**。
1. 输入名称。任何有权查看项目的用户都可以看到令牌名称。
1. 输入令牌的到期日期。
   - 令牌在该日期的午夜 UTC 到期。
   - 如果您不输入到期日期，到期日期将自动设置为比当前日期晚 365 天。
   - 默认情况下，此日期最多可以比当前日期晚 365 天。
   - 实例范围的[最长生命周期](../../admin_area/settings/account_and_limit_settings.md#limit-the-lifetime-of-access-tokens)设置可以限制私有化部署实例中允许的最长生命周期。
1. 为令牌选择一个角色。
1. 选择[所需范围](#scopes-for-a-project-access-token)。
1. 选择 **创建项目访问令牌**。

显示项目访问令牌。 将项目访问令牌保存在安全的地方。 离开或刷新页面后，将无法再次查看。

## 撤销项目访问令牌

要撤销项目访问令牌：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 访问令牌**。
1. 在要撤销的项目访问令牌旁边，选择 **撤销**。

<a id="scopes-for-a-project-access-token"></a>

## 项目访问令牌的范围

范围决定了您在使用项目访问令牌进行身份验证时可以执行的操作。

| 范围              | 描述                                                                                                                                                 |
|:-------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `api`              | 授予对范围项目 API 的完整读写访问权限，包括 [Package Registry](../../packages/package_registry/index.md)。               |
| `read_api`         | 授予对范围项目 API 的读取访问权限，包括 [Package Registry](../../packages/package_registry/index.md)。                                   |
| `read_registry`    | 如果项目是私有的并且需要授权，则允许对 [Container Registry](../../packages/container_registry/index.md) 镜像进行读取访问（拉取）。 |
| `write_registry`   | 允许对 [Container Registry](../../packages/container_registry/index.md) 进行写访问（推送）。                                                             |
| `read_repository`  | 允许对仓库进行读取访问（拉取）。                                                                                                                |
| `write_repository` | 允许对仓库进行读写访问（拉取和推送）。                                                                                            |

<a id="enable-or-disable-project-access-token-creation"></a>

## 启用或禁用项目访问令牌创建

> 引入于 13.11 版本。

要为顶级组中的所有项目启用或禁用项目访问令牌创建：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **权限和群组功能**。
1. 在 **权限** 下，开启或关闭 **允许创建项目和群组访问令牌**。

即使创建被禁用，您仍然可以使用和撤销现有的项目访问令牌。

<a id="bot-users-for-projects"></a>

## 项目机器人用户

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/210181) in GitLab 13.0.
> - [Excluded from license seat use](https://gitlab.com/gitlab-org/gitlab/-/issues/223695) in GitLab 13.5.
-->

项目机器人用户是系统创建的服务帐户。
每次创建项目访问令牌时，都会创建一个机器人用户并将其添加到项目中。
这些机器人用户不算作许可席位。

机器人用户具有与项目访问令牌所选角色和[范围](#项目访问令牌的范围)对应的权限。

- 名称设置为令牌的名称。
- 第一个访问令牌的用户名设置为 `project_{project_id}_bot`。例如，`project_123_bot`。
- 电子邮件设置为 `project{project_id}_bot@noreply.{Gitlab.config.gitlab.host}`。例如，`project123_bot@noreply.example.com`。
- 对于同一项目中的其他访问令牌，用户名设置为 `project_{project_id}_bot{bot_count}`。例如，`project_123_bot1`。
- 对于同一项目中的其他访问令牌，电子邮件设置为 `project{project_id}_bot{bot_count}@noreply.{Gitlab.config.gitlab.host}`。例如，`project123_bot1@noreply.example.com`。

使用项目访问令牌进行的 API 调用与相应的机器人用户相关联。

机器人用户：

- 包含在项目的成员列表中但不能修改。
- 不能添加到任何其他项目。
- 可以具有的最大项目角色为所有者。

当项目访问令牌被[撤销](#撤销项目访问令牌)时：

- 机器人用户被删除。
- 所有记录都移动到用户名为 `Ghost User` 的系统范围内的用户。有关详细信息，请参阅[关联记录](../../profile/account/delete_account.md#关联记录)。

<!--
See also [Bot users for groups](../../group/settings/group_access_tokens.md#bot-users-for-groups).
-->
