---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 部署令牌 **(FREE)**

您可以使用部署令牌来启用部署任务的身份验证，而与用户帐户无关。在大多数情况下，您使用来自外部主机的部署令牌，例如构建服务器或 CI/CD 服务器。

使用部署令牌，自动化任务可以：

- 克隆 Git 仓库。
- 从极狐GitLab 容器镜像库中提取和推送。
- 从极狐GitLab 软件包库中提取和推送。

部署令牌是一对值：

- **用户名**：HTTP身份验证框架中的 `username`。默认用户名格式为 `gitlab+deploy-token-{n}`。您可以在创建部署令牌时指定自定义用户名。
- **令牌**：HTTP 身份验证框架中的 `password`。

您可以将部署令牌对以下端点进行 [HTTP 身份验证](https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication)：

- 极狐GitLab 软件包库公共 API。
- [Git 命令](https://git-scm.com/docs/gitcredentials#_description)。

您可以在项目或群组级别创建部署令牌：

- **项目部署令牌**：权限仅适用于项目。
- **组部署令牌**：权限适用于群组中的所有项目。

默认情况下，部署令牌不会过期。您可以选择在创建时设置到期日期。到期发生在该日期的 UTC 午夜。

WARNING:
如果启用了[外部授权](../../admin_area/settings/external_authorization.md)，则不能将新的或现有的部署令牌用于 Git 操作和软件包库操作。

<a id="scope"></a>

## 范围

部署令牌的范围决定了它可以执行的操作。

| 范围                    | 描述                                                                                                  |
|--------------------------|--------------------------------------------------------------------------------------------------------------|
| `read_repository`        | 使用 `git clone` 对仓库进行只读访问。                                                        |
| `read_registry`          | 对项目[容器镜像库](../../packages/container_registry/index.md)中镜像的只读访问权限。 |
| `write_registry`         | 对项目[容器镜像库](../../packages/container_registry/index.md)中镜像的写入访问（推送）权限。       |
| `read_package_registry`  | 对项目的软件包库的只读访问权限。                                                          |
| `write_package_registry` | 对项目的软件包库的写入权限。                                                              |

## 极狐GitLab 部署令牌

> - 在群组级别对 `gitlab-deploy-token` 的支持引入于 15.1 版本，[功能标志](../../../administration/feature_flags.md)为 `ci_variable_for_group_gitlab_deploy_token`。默认启用。
> - 功能标志 `ci_variable_for_group_gitlab_deploy_token` 删除于 15.4 版本。

极狐GitLab 部署令牌是一种特殊类型的部署令牌。如果您创建一个名为 `gitlab-deploy-token` 的部署令牌，部署令牌会自动作为变量公开给 CI/CD 作业，以便在 CI/CD 流水线中使用：

- `CI_DEPLOY_USER`：用户名
- `CI_DEPLOY_PASSWORD`：令牌

例如，要使用极狐GitLab 令牌登录到极狐GitLab 容器镜像库：

```shell
docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
```

NOTE:
在 15.0 及更早版本中，`gitlab-deploy-token` 部署令牌的特殊处理不适用于群组部署令牌。要使群组部署令牌可用于 CI/CD 作业，请将 **设置 > CI/CD > 变量** 中的 `CI_DEPLOY_USER` 和 `CI_DEPLOY_PASSWORD` CI/CD 变量设置为群组部署令牌的名称和令牌。

### 极狐GitLab 公共 API

部署令牌不能与极狐GitLab 公共 API 一起使用。但是，您可以将部署令牌与某些端点一起使用，例如来自软件包库的端点。有关详细信息，请参阅[软件包库身份验证](../../packages/package_registry/index.md#authenticate-with-the-registry)。

## 创建部署令牌

创建部署令牌以自动执行可独立于用户帐户运行的部署任务。

先决条件：

- 您必须至少具有项目或群组的维护者角色。

1. 在顶部栏上，选择 **主菜单**，然后：
   - 对于项目部署令牌，选择 **项目** 并找到您的项目。
   - 对于群组部署令牌，选择 **群组** 并找到您的群组。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **部署令牌**。
1. 填写字段，然后选择所需的[范围](#scope)。
1. 选择 **创建部署令牌**。

记录部署令牌的值。离开或刷新页面后，**无法再次访问**。

## 撤销部署令牌

不再需要令牌时将其撤消。

先决条件：

- 您必须至少具有项目或群组的维护者角色。

要撤销部署令牌：

1. 在顶部栏，选择 **主菜单**，然后：
    - 对于项目部署令牌，选择 **项目** 并找到您的项目。
    - 对于组部署令牌，选择 **群组** 并找到您的群组。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **部署令牌**。
1. 在 **可用部署令牌** 部分，通过您要撤销的令牌，选择 **撤销**。

## 克隆仓库

您可以使用部署令牌来克隆仓库。

先决条件：

- 具有 `read_repository` 范围的部署令牌。

使用部署令牌克隆仓库的示例：

```shell
git clone https://<username>:<deploy_token>@gitlab.example.com/tanuki/awesome_project.git
```

## 从容器镜像库中拉取镜像

您可以使用部署令牌从容器镜像库中提取镜像。

先决条件：

- 具有 `read_registry` 范围的部署令牌。

使用部署令牌从容器镜像库中提取镜像的示例：

```shell
docker login -u <username> -p <deploy_token> registry.example.com
docker pull $CONTAINER_TEST_IMAGE
```

## 将镜像推送到容器镜像库

您可以使用部署令牌将镜像推送到容器镜像库。

先决条件：

- 具有 `write_registry` 范围的部署令牌。

使用部署令牌将镜像推送到容器镜像库的示例：

```shell
docker login -u <username> -p <deploy_token> registry.example.com
docker push $CONTAINER_TEST_IMAGE
```

## 从软件包库中拉取软件包

您可以使用部署令牌从软件包库中拉取包。

先决条件：

- 具有 `read_package_registry` 范围的部署令牌。

对于[您选择的包类型](../../packages/index.md)，请按照部署令牌的身份验证说明进行操作。

从极狐GitLab 库安装 NuGet 包的示例：

```shell
nuget source Add -Name GitLab -Source "https://gitlab.example.com/api/v4/projects/10/packages/nuget/index.json" -UserName <username> -Password <deploy_token>
nuget install mypkg.nupkg
```

## 推送软件包到软件包库

您可以使用部署令牌将包推送到极狐GitLab 软件包库。

先决条件：

- 具有 `write_package_registry` 范围的部署令牌。

对于[您选择的包类型](../../packages/index.md)，请按照部署令牌的身份验证说明进行操作。

将 NuGet 包发布到软件包库的示例：

```shell
nuget source Add -Name GitLab -Source "https://gitlab.example.com/api/v4/projects/10/packages/nuget/index.json" -UserName <username> -Password <deploy_token>
nuget push mypkg.nupkg -Source GitLab
```

## 从依赖代理中拉取镜像

> 引入于 14.2 版本。

您可以使用部署令牌从依赖代理中拉取镜像。

先决条件：

- 具有 `read_registry` 和 `write_registry` 范围的部署令牌。

遵循依赖代理[认证说明](../../packages/dependency_proxy/index.md)。

