---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 代码所有者 **(PREMIUM)**

使用代码所有者功能来定义谁拥有项目代码库特定部分的专业知识。
定义仓库中文件和目录的所有者后，您可以方便地：

- **要求所有者批准更改。**将受保护的分支与代码所有者结合起来，要求专家在合并到受保护的分支之前批准合并请求。
- **识别所有者。**代码所有者名称显示在他们拥有的文件和目录上：
  
  ![Code Owners displayed in UI](../img/codeowners_in_UI_v15_10.png)

将代码所有者与[合并请求批准规则](../merge_requests/approvals/rules.md)（可选或必需）结合使用，可以构建灵活的批准工作流程：

- 使用**代码所有者**来确保质量。定义对存款中的特定路径具有领域专业知识的用户。
- 使用**批准规则**来定义与仓库中特定文件路径不对应的专业领域。批准规则有助于将合并请求创建者引导至正确的审核者团队，例如前端开发人员或安全团队。

例如：

| 类型 | 名称 | 范围  | 说明    |
|------|------|--------|------------|
| 批准规则            | UX                   | 所有文件     | 用户体验 (UX) 团队成员评审项目中所有更改的用户体验。 |
| 批准规则            | Security             | 所有文件     | 安全团队成员评审所有更改是否存在漏洞。                                     |
| 代码所有者批准规则 | Frontend: Code Style | `*.css` 文件 | 前端工程师检查 CSS 文件更改是否符合项目样式标准。              |
| 代码所有者批准规则 | Backend: Code Review | `*.rb` 文件  | 后端工程师评审 Ruby 文件的逻辑和代码风格。                                  |

## 查看文件或目录的代码所有者

查看文件或目录的代码所有者：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **仓库 > 文件**。
1. 转到您要查看其代码所有者的文件或目录。
1. 可选。选择一个分支或标签。

极狐GitLab 在页面顶部显示代码所有者。

## 设置代码所有者

1. 在您的[偏好位置](#code-owners-file)，创建一个 `CODEOWNERS` 文件。
1. 按照 [Code Owners 语法参考](reference.md)，在文件中定义一些规则。
   建议：
   - 配置[所有合资格的批准人](../merge_requests/approvals/rules.md#code-owners-as-eligible-approvers)的批准规则。
   - 在受保护的分支上[要求代码所有者批准](../protected_branches.md#require-code-owner-approval-on-a-protected-branch)。
1. 提交您的更改，并将它们推送到极狐GitLab。

<a id="code-owners-file"></a>

### 代码所有者文件

`CODEOWNERS` 文件（没有扩展名）指定负责仓库中特定文件和目录的用户或[共享群组](../members/share_project_with_groups.md)。

每个仓库使用一个单独的 `CODEOWNERS` 文件。极狐GitLab 按此顺序检查仓库中的这些位置。使用找到的第一个 `CODEOWNERS` 文件，忽略所有其他文件：

1. 在根目录中：`./CODEOWNERS`。
1. 在 `docs` 目录中：`./docs/CODEOWNERS`。
1. 在`.gitlab`目录中：`./.gitlab/CODEOWNERS`。

<a id="groups-as-code-owners"></a>

### 群组作为代码所有者

您可以将群组和子组的成员用作项目的代码所有者。

```mermaid
graph TD
    A[Parent group X] -->|owns| B[Project A]
    A -->|contains| C[Subgroup Y]
    C -->|owns| D[Project B]
    A-. inherits ownership .-> D
```

在示例中：

- **父组 X** (`group-x`) 拥有 **项目 A**。
- **父组 X** 也包含子组，**子组 Y** (`group-x/subgroup-y`)。
- **子组 Y** 拥有 **项目 B**。

符合条件的代码所有者是：

- **项目 A**：仅限 **群组 X** 的成员，因为 **项目 A** 不属于 **子组 Y**。
- **项目 B**：**群组 X** 和 **子组 Y** 的成员。

您可以[邀请](members/share_project_with_groups.md) **子组 Y** 到 **项目 A**，以便他们的成员也成为合格的代码所有者。

```mermaid
graph LR
    A[Parent group X] -->|owns| B[Project A]
    A -->|also contains| C[Subgroup Y]
    C -.->D{Invite Subgroup Y<br/>to Project A?} -.->|yes| F[Approvals can be<br/> required] -.-> B
    D{Invite Subgroup Y<br/>to Project A?} -.->|no| I[Subgroup Y cannot be<br /> an approver] -.-> B
    C -.->E{Add Subgroup Y<br/>as Code Owners<br/>to Project A?} -.->|yes| H[Approvals can only<br/>be optional] -.-> B
```

如果您不邀请 **子组 Y** 加入 **项目 A**，而是让他们成为代码所有者，则他们对合并请求的批准变为可选。

不支持将 **子组 Y** 邀请到 **项目 A** 的父组。要将 **子组 Y** 设置为代码所有者，请将该群组直接添加到项目本身。

NOTE:
要获得批准，作为代码所有者的群组必须在项目中具有直接成员资格（而非继承成员资格）。对于继承成员资格的群组，批准只能是可选的。代码所有者群组中的成员也必须是直接成员，并且不能继承任何父组的成员资格。

#### 添加群组作为代码所有者

将群组设置为代码所有者：

在 `CODEOWNERS` 文件中，输入遵循以下模式之一的文本：

```plaintext
# All group members as Code Owners for a file
file.md @group-x

# All subgroup members as Code Owners for a file
file.md @group-x/subgroup-y

# All group and subgroup members as Code Owners for a file
file.md @group-x @group-x/subgroup-y
```

<a id="define-more-specific-owners-for-more-specifically-defined-files-or-directories"></a>

### 为更具体定义的文件或目录定义特定的所有者

当一个文件或目录与 `CODEOWNERS` 文件中的多个条目匹配时，将使用来自最后一个匹配该文件或目录的样式的用户。当您以合理的方式对条目进行排序时，这使您能够为更具体定义的文件或目录定义特定的所有者。

例如，在以下 `CODEOWNERS` 文件中：

```plaintext
# This line would match the file terms.md
*.md @doc-team

# This line would also match the file terms.md
terms.md @legal-team
```

`terms.md` 的代码所有者将是 `@legal-team`。

如果您使用节（section），则使用与每个节的文件或目录匹配的最后一个样式。
例如，在使用节（section）的 `CODEOWNERS` 文件中：

```plaintext
[README Owners]
README.md @user1 @user2
internal/README.md @user4

[README other owners]
README.md @user3
```

根目录中 `README.md` 的代码所有者是 `@user1`、`@user2` 和 `@user3`。`internal/README.md` 的代码所有者是 `@user4` 和 `@user3`。

每个节只有一个 `CODEOWNERS` 样式与文件路径匹配。

### 通过将代码所有者设置到不同的节来组织代码所有者

在 `CODEOWNERS` 文件中，您可以通过将代码所有者放入命名的节，来组织代码所有者。

您可以使用设置了共享目录的节，以便多个团队可以成为审核者。

要将部分添加到 `CODEOWNERS` 文件，请在括号中输入节名称，后跟文件或目录，以及用户、群组或子组：

```plaintext
[README Owners]
README.md @user1 @user2
internal/README.md @user2
```

合并请求部件中的每个代码所有者都列在一个标记下。
下图显示了 **Groups** 和 **Documentation** 节：

![MR widget - Sectional Code Owners](../img/sectional_code_owners_v13.2.png)

#### 设置默认所有者

> - 引入于 15.11 版本，[功能标志](../../../administration/feature_flags.md)为 `codeowners_default_owners`。默认禁用。
> - 一般可用于 15.11 版本。删除功能标志 `codeowners_default_owners`。

在 `CODEOWNERS` 文件中，如果一个节内的多个文件路径共享相同的所有权，请定义一个默认的代码所有者。该节中的所有路径都继承此默认值，除非您覆盖特定行上的默认值。

当没有为文件路径指定特定所有者时，将应用默认所有者。
在文件路径旁边定义的特定所有者会覆盖默认所有者：

```plaintext
[Documentation] @docs-team
docs/
README.md

[Database] @database-team
model/db/
config/db/database-setup.md @docs-team
```

在示例中：

- `@docs-team` 拥有 `Documentation` 节中的所有项。
- `@database-team` 拥有 `Database` 节中的所有项，除了 `config/db/database-setup.md`，它被覆盖分配给 `@docs-team`。

要将默认所有者的语法与[可选节](#make-a-code-owners-section-optional)，以及需要的批准次数相结合，请将默认所有者放在末尾：

```plaintext
[Documentation][2] @docs-team
docs/
README.md

^[Database] @database-team
model/db/
config/db/database-setup.md @docs-team
```

<a id="sections-with-duplicate-names"></a>

#### 具有重复名称的节

如果多个节具有相同的名称，则系统将它们组合到一起。
此外，节标题不区分大小写。例如：

```plaintext
[Documentation]
ee/docs/    @docs
docs/       @docs

[Database]
README.md  @database
model/db/   @database

[DOCUMENTATION]
README.md  @docs
```

以上示例在 **Documentation** 节标题下产生三个条目，在 **Database** 下产生两个条目。**Documentation** 和 **DOCUMENTATION** 部分下定义的条目组合在一起，使用第一个节的配置。

<a id="make-a-code-owners-section-optional"></a>

#### 设置代码所有者为可选

您可以在代码所有者文件中指定可选的节（section）。在节名称前加上插入符号 `^` 字符，将整个节视为可选。
可选节使您能够为代码库的各个部分指定责任方，但不需要他们的批准。这种方法为项目中经常更新但不需要严格审核的部分提供了更宽松的政策。

在此示例中，`[Go]` 部分是可选的：

```plaintext
[Documentation]
*.md @root

[Ruby]
*.rb @root

^[Go]
*.go @root
```

可选的代码所有者部分显示在**批准规则**区域下的合并请求中：

![MR widget - Optional Code Owners sections](../img/optional_code_owners_sections_v13_8.png)

如果节在文件中重复，并且其中一个被标记为可选而另一个没有标记，则该节是必需的。

仅当使用合并请求提交更改时，`CODEOWNERS` 文件中的可选节才被视为可选。如果将更改直接提交给受保护的分支，即使该部分被标记为可选，仍然需要代码所有者的批准。

### 需要代码所有者的多次批准

> 引入于 15.9 版本。

在节名称后面加上括号中的数字 `n`，表示需要本节代码所有者的 n 次批准。
请注意，`n` 的有效条目数是大于等于 1 的整数。`[1]` 是可选的，因为它是默认值。`n` 的无效值被视为 `1`。

WARNING:
不要故意设置无效值。它们可能在未来变为有效，并导致意外行为。

请确认您在 **设置 > 仓库 > 受保护分支** 中启用了 **需要代码所有者的批准**，否则代码所有者的批准将是可选的。

在此示例中，`[Documentation]` 节中设置的代码需要两次批准：

```plaintext
[Documentation][2]
*.md @tech-writer-team

[Ruby]
*.rb @dev-team
```

**批准规则**区域下的 `Documentation` 代码所有者部分显示需要两次批准：

![MR widget - Multiple Approval Code Owners sections](../img/multi_approvals_code_owners_sections_v15_9.png)

### 允许推送

代码所有者批准和受保护的分支功能不适用于**允许推送**的用户。

<!--
## Technical Resources

[Code Owners development guidelines](../../../development/code_owners/index.md)
-->

## 故障排除

有关代码所有者功能如何处理错误的更多信息，请参阅[代码所有者参考](reference.md)。

### 批准显示为可选

如果满足以下任何条件，则代码所有者批准规则是可选的：

- 用户或群组不是项目的成员。代码所有者不能从父组继承。
- 尚未设置[受保护分支的代码所有者批准](../protected_branches.md#require-code-owner-approval-on-a-protected-branch)。
- `CODEOWNERS` 文件中的节[标记为可选](#make-a-code-owners-section-optional)。

### 批准不显示

代码所有者批准规则仅在创建合并请求时更新。
如果您更新 `CODEOWNERS` 文件，请关闭合并请求，并创建一个新的合并请求。

### 用户未显示为可能的批准人

如果满足以下任何条件，则用户可能不会在代码所有者合并请求批准规则中显示为批准人：

- 规则阻止特定用户批准合并请求。检查项目[合并请求批准](../merge_requests/approvals/settings.md#edit-merge-request-approval-settings)设置。
- 代码所有者群组的可见性为**私有**，当前用户不是代码所有者群组的成员。
- 当前用户是无权访问内部代码所有者群组的外部用户。

### `Approval rule is invalid. GitLab has approved this rule automatically to unblock the merge request` 错误

如果批准规则使用的代码所有者不是项目的直接成员，则可能会出现此消息。
检查群组或用户是否已被邀请加入项目。
