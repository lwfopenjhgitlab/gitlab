---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 代码所有者语法和错误处理 **(PREMIUM)**

本页描述代码所有者文件中使用的语法和错误处理，并提供示例文件。

## 代码所有者语法

### 评论

以 `#` 开头的行将被忽略：

```plaintext
# This is a comment
```

### 节（section）

节是由一组条目构成的。一节以方括号中的标题开头，然后是条目。

```plaintext
[Section name]
/path/of/protected/file.rb @username
/path/of/protected/dir/ @group
```

#### 节标题

节标题必须始终有一个名称，节标题中可以设置可选，或者设置需要批准。您可以将默认所有者列表添加到节的标题行。

```plaintext
# Required section
[Section name]

# Optional section
^[Section name]

# Section requiring 5 approvals
[Section name][5]

# Section with @username as default owner
[Section name] @username

# Section with @group and @subgroup as default owners and requiring 2 approvals
[Section name][2] @group @subgroup
```

<a id="section-names"></a>

#### 节名称

节名称在方括号之间定义，不区分大小写。[具有重复名称的节](index.md#sections-with-duplicate-names)自动合并。

```plaintext
[Section name]
```

#### 必需的节

必需的节在[节名称](#section-names)之前不包含 `^`。

```plaintext
[Required section]
```

#### 可选的节

可选的节在[节名称](#section-names)之前包含一个 `^`。

```plaintext
^[Optional section]
```

#### 需要多次批准的节

需要多次批准的节的[节名称](#section-names)后方括号中，设置了需要的批准次数。

```plaintext
[Section requiring 5 approvals][5]
```

NOTE:
可选节忽略需要的批准次数。

#### 设置默认所有者的节

您可以通过将所有者附加到[节标题](#section-headings)，来为节中的条目定义默认所有者。

```plaintext
# Section with @username as default owner
[Section name] @username

# Section with @group and @subgroup as default owners and requiring 2 approvals
[Section name][2] @group @subgroup
```

### 代码所有者条目

每个代码所有者条目都包含一个路径，后面跟着一个或多个所有者。

```plaintext
README.md @username1
```

NOTE:
如果一个条目在一个节中重复，[则使用每个节的最后一个条目。](index.md#define-more-specific-owners-for-more-specifically-defined-files-or-directories)。

### 相对路径

如果路径不以 `/` 开头，则路径将被视为以[星号](#globstar-paths)开头。`README.md` 的处理方式与 `/**/README.md` 相同：

```plaintext
# This will match /README.md, /internal/README.md, /app/lib/README.md
README.md @username

# This will match /internal/README.md, /docs/internal/README.md, /docs/api/internal/README.md
internal/README.md
```

### 绝对路径

如果路径以 `/` 开头，则它与仓库的根目录相匹配。

```plaintext
# Matches only the file named `README.md` in the root of the repository.
/README.md

# Matches only the file named `README.md` inside the `/docs` directory.
/docs/README.md
```

### 目录路径

如果路径以 `/` 结尾，则该路径与目录中的任何文件匹配。

```plaintext
# This is the same as `/docs/**/*`
/docs/
```

### 通配符路径

通配符可用于匹配路径的多个字符之一。

```plaintext
# Any markdown files in the docs directory
/docs/*.md @username

# /docs/index file of any filetype
# For example: /docs/index.md, /docs/index.html, /docs/index.xml
/docs/index.* @username

# Any file in the docs directory with 'spec' in the name.
# For example: /docs/qa_specs.rb, /docs/spec_helpers.rb, /docs/runtime.spec
/docs/*spec* @username

# README.md files one level deep within the docs directory
# For example: /docs/api/README.md
/docs/*/README.md @username
```

### 星号路径

星号 (`**`) 可用于匹配零个或多个目录和子目录。

```plaintext
# This will match /docs/index.md, /docs/api/index.md, /docs/api/graphql/index.md
/docs/**/index.md
```

<a id="entry-owners"></a>

### 条目所有者

条目必须包括一位或多位所有者，可以是群组、子组和用户。所有者的顺序并不重要。

```plaintext
/path/to/entry.rb @group
/path/to/entry.rb @group/subgroup
/path/to/entry.rb @user
/path/to/entry.rb @group @group/subgroup @user
```

#### 群组作为条目所有者

群组和子组可以是条目的所有者。
每个条目都可以具有[一个或多个所有者](#entry-owners)。
有关详细信息，请参阅[群组作为代码所有者](index.md#groups-as-code-owners)。

```plaintext
/path/to/entry.rb @group
/path/to/entry.rb @group/subgroup
/path/to/entry.rb @group @group/subgroup
```

### 用户作为条目所有者

用户可以是条目的所有者。每个条目都可以具有[一个或多个所有者](#entry-owners)。

```plaintext
/path/to/entry.rb @username1
/path/to/entry.rb @username1 @username2
```

## 代码所有者中的错误处理

### 带空格的条目

包含空格的路径必须用反斜杠转义：`path\ with\ spaces/*.md`。
如果没有反斜杠，第一个空格之后的路径将被解析为所有者。
极狐GitLab 将 `folder with spaces/*.md @group` 解析为 `path: "folder", owners: " with spaces/*.md @group"`。

### 无法解析的节

如果无法解析节标题，则系统将该节：

1. 解析为条目。
1. 添加到上一节。
1. 如果前面的节不存在，则该节被添加到默认节。

例如，以下文件缺少方括号：

```plaintext
* @group

[Section name
docs/ @docs_group
```

极狐GitLab 将标题 `[Section name` 识别为条目。默认节包括 3 个规则：

- 默认部分
  - `*` 由 `@group` 拥有
  - `[Section` 由 `name` 拥有
  - `docs/` 由 `@docs_group` 拥有

以下文件在单词 `Section` 和 `name` 之间包含一个未转义的空格。
极狐GitLab 将标题识别为条目：

```plaintext
[Docs]
docs/**/* @group

[Section name]{2} @group
docs/ @docs_group
```

`[Docs]` 节包含 3 条规则：

- `docs/**/*` 由 `@group` 拥有
- `[Section` 由 `name]{2} @group` 拥有
- `docs/` 由 `@docs_group` 拥有

### 格式错误的所有者

每个条目必须包含 1 个或多个所有者才能有效，格式错误的所有者将被忽略。
例如，`/path/* @group user_without_at_symbol @user_with_at_symbol` 由 `@group` 和 `@user_with_at_symbol` 所有。

<a id="inaccessible-or-incorrect-owners"></a>

### 无法访问或不正确的所有者

无法访问或不正确的所有者将被忽略。例如，在项目中可以访问 `@group`、`@username` 和 `example@gitlab.com`，我们创建一个条目：

```plaintext
* @group @grou @username @i_left @i_dont_exist example@gitlab.com invalid@gitlab.com
```

极狐GitLab 会忽略 `@grou`、`@i_left`、`@i_dont_exist` 和 `invalid@gitlab.com`。

有关谁可以访问的更多信息，请参阅[群组作为代码所有者](index.md#groups-as-code-owners)。

### 零个所有者

如果条目不包含任何所有者，或者存在零个[可访问的所有者](#inaccessible-or-incorrect-owners)，则该条目无效。因为这个规则永远无法满足，所以极狐GitLab 在合并请求中自动批准它。

NOTE:
当受保护的分支启用“需要代码所有者批准”时，零个所有者的规则仍然生效。

### 少于 1 个必需的批准

当为节[定义批准次数](index.md#require-multiple-approvals-from-code-owners)时，最小批准数量为 `1`。将批准数设置为 `0` 会导致必需一次批准。

## `CODEOWNERS` 示例文件

```plaintext
# This is an example of a CODEOWNERS file.
# Lines that start with `#` are ignored.

# app/ @commented-rule

# Specify a default Code Owner by using a wildcard:
* @default-codeowner

# Specify multiple Code Owners by using a tab or space:
* @multiple @code @owners

# Rules defined later in the file take precedence over the rules
# defined before.
# For example, for all files with a filename ending in `.rb`:
*.rb @ruby-owner

# Files with a `#` can still be accessed by escaping the pound sign:
\#file_with_pound.rb @owner-file-with-pound

# Specify multiple Code Owners separated by spaces or tabs.
# In the following case the CODEOWNERS file from the root of the repo
# has 3 Code Owners (@multiple @code @owners):
CODEOWNERS @multiple @code @owners

# You can use both usernames or email addresses to match
# users. Everything else is ignored. For example, this code
# specifies the `@legal` and a user with email `janedoe@gitlab.com` as the
# owner for the LICENSE file:
LICENSE @legal this_does_not_match janedoe@gitlab.com

# Use group names to match groups, and nested groups to specify
# them as owners for a file:
README @group @group/with-nested/subgroup

# End a path in a `/` to specify the Code Owners for every file
# nested in that directory, on any level:
/docs/ @all-docs

# End a path in `/*` to specify Code Owners for every file in
# a directory, but not nested deeper. This code matches
# `docs/index.md` but not `docs/projects/index.md`:
/docs/* @root-docs

# Include `/**` to specify Code Owners for all subdirectories
# in a directory. This rule matches `docs/projects/index.md` or
# `docs/development/index.md`
/docs/**/*.md @root-docs

# This code makes matches a `lib` directory nested anywhere in the repository:
lib/ @lib-owner

# This code match only a `config` directory in the root of the repository:
/config/ @config-owner

# If the path contains spaces, escape them like this:
path\ with\ spaces/ @space-owner

# Code Owners section:
[Documentation]
ee/docs    @docs
docs       @docs

# Use of default owners for a section. In this case, all files (*) are owned by
the dev team except the README.md and data-models which are owned by other teams.
[Development] @dev-team
*
README.md @docs-team
data-models/ @data-science-team

# This section is combined with the previously defined [Documentation] section:
[DOCUMENTATION]
README.md  @docs
```
