---
stage: Create
group: Incubation
info: Machine Learning Experiment Tracking is a GitLab Incubation Engineering program. No technical writer assigned to this group.
---

# 机器学习模型实验 **(FREE)**

FLAG:
在私有化部署版上，模型实验跟踪功能默认禁用。
要启用此功能，需要管理员[启用功能标志](../../../../administration/feature_flags.md) `ml_experiment_tracking`。
在 SaaS 版上，此功能处于内部测试中。

NOTE:
模型实验跟踪功能是一项实验功能。

在创建机器学习模型时，数据科学家经常试验不同的参数、配置和功能工程，来提高模型的性能。跟踪所有这些元数据和相关的产物使数据科学家以后可以复制实验，这并非易事。机器学习实验跟踪使他们能够将参数、指标和产物直接记录到极狐GitLab 中，以便以后轻松访问。

目标包括以下功能：

- 搜索实验。
- 候选项的可视化比较。
- 通过 UI 创建、删除和更新实验。
- 通过 UI 创建、删除和更新候选项。

## 什么是实验？

在一个项目中，一个实验是一组可比较的候选模型。
实验可以是长期的（例如，当它们代表一个用例时），也可以是短期的（由合并请求触发的超参数调整的结果），但通常持有具有由相同指标测量的相似参数集的模型候选项。

![List of Experiments](img/experiments_v15_11.png)

## 模型候选项

候选模型是机器学习模型训练的变体，最终可以提升为模型的一个版本。

![Experiment Candidates](img/candidates_v15_11.png)

数据科学家的目标是找到实现最佳模型性能的参数值的候选模型，如给定指标所示。

![Candidate Detail](img/candidate_v15_11.png)

参数示例：

- 算法（例如线性回归或决策树）。
- 算法的超参数（学习率、树深度、时期数）。
- 包括的功能。

## 跟踪新的实验和候选项

实验和试用只能通过 [MLFlow](https://www.mlflow.org/docs/latest/tracking.html) 客户端集成进行跟踪。
有关如何将极狐GitLab 用作 MLFlow 客户端后端的更多信息，请参阅 [MLFlow 客户端集成](../../integrations/mlflow_client.md)。

## 浏览模型候选项

先决条件：

- 您必须至少具有开发者角色才能查看实验数据。

要列出当前活动的实验，请访问 `https/-/ml/experiments` 或：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **软件包与镜像库 > 模型实验**。
1. 要显示已记录的所有候选项及其指标、参数和元数据，请选择一个实验。
1. 要显示候选人的详细信息，请选择 **详情**。

## 查看日志产物

试用产物保存为[通用软件包](../../../packages/generic_packages/index.md)，并遵循其所有限制。在为候选项记录产物后，为候选项记录的所有产物都列在软件包库中。候选项软件包名称为 `ml_experiment_<experiment_id>`，其中版本为候选 IID。也可以从 **实验候选项** 列表或 **候选项详情** 访问产物的链接。

<!--
## Related topics

- Development details in [epic 8560](https://gitlab.com/groups/gitlab-org/-/epics/8560).
- Add feedback in [issue 381660](https://gitlab.com/gitlab-org/gitlab/-/issues/381660).
-->
