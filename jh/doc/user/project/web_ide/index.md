---
stage: Create
group: IDE
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Web IDE **(FREE)**

> - 引入于 15.7 版本，[功能标志](../../../administration/feature_flags.md)为 `vscode_web_ide`。默认禁用。
> - 在 SaaS 上启用于 15.7 版本。
> - 在私有化部署版上启用于 15.11 版本。

FLAG:
在私有化部署版上，此功能默认可用。要隐藏此功能，需要管理员[禁用功能标志](../../../administration/feature_flags.md) `vscode_web_ide`。在 SaaS 上，此功能可用。

Web IDE 是具有提交暂存功能的高级编辑器。
您可以使用 Web IDE 直接从极狐GitLab UI 更改多个文件。
有关更基本的实现，请参阅 [Web 编辑器](../repository/web_editor.md)。

<!--
To pair the Web IDE with a remote development environment, see [remote development](../remote_development/index.md).
-->

## 使用 Web IDE

要从极狐GitLab UI 打开 Web IDE：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 使用 <kbd>.</kbd> 键盘快捷键。

您还可以从以下位置打开 Web IDE：

- 一个文件
- 仓库文件列表
- 合并请求

### 在文件或仓库文件列表中打开

要从文件或仓库文件列表打开 Web IDE：

- 在页面右上角，选择 **在 Web IDE 中打开**。

如果 **在 Web IDE 中打开** 不可见：

1. 在 **编辑** 或 **Gitpod** 旁边，选择向下箭头（**{chevron-lg-down}**）。
1. 从下拉列表中选择 **在 Web IDE 中打开**。
1. 选择 **在 Web IDE 中打开**。

### 从合并请求打开

从合并请求打开 Web IDE：

1. 访问您的合并请求。
1. 在右上角，选择 **代码 > 在 Web IDE 中打开**。

Web IDE 在单独的选项卡中打开新文件和修改后的文件，并与原始源代码并排显示更改。
为了优化加载时间，仅自动打开前 10 个文件（按更改的行数）。

在文件树中，合并请求中的任何新文件或修改过的文件都由文件名旁边的图标指示。
要查看对文件的更改，请右键单击文件名并选择 **与合并请求 base 比较**。

## 在 Web IDE 中打开文件

要按文件名打开任何文件：

1. 按<kbd>Command</kbd>+<kbd>P</kbd>。
1. 输入文件名。

![fuzzy_finder_v15_7](img/fuzzy_finder_v15_7.png)

## 跨文件搜索

您可以使用 Web IDE 搜索打开的文件夹中的所有文件。

跨文件搜索：

1. 按 <kbd>Shift</kbd>+<kbd>Command</kbd>+<kbd>F</kbd>。
1. 输入您的搜索词。

在 Web IDE 中，仅显示打开文件的部分结果。

## 查看已更改文件的列表

查看您在 Web IDE 中更改的文件列表：

- 在左侧的活动栏上，选择 **源控制**，或按 <kbd>Control</kbd>+<kbd>Shift</kbd>+<kbd>G</kbd>。

您的 `CHANGES`、`STAGED CHANGES` 和 `MERGE CHANGES` 会显示出来。
有关详细信息，请参阅 [VS Code 文档](https://code.visualstudio.com/docs/sourcecontrol/overview#_commit)。

## 上传新文件

要上传新文件并将其添加到 Git 仓库：

1. 在 **Explorer** 文件树中，导航到要上传文件的目录。
1. 可选。如果该目录尚不存在，请选择您希望拥有新目录的目录路径，然后：
   - 右键单击目录路径，然后选择 **新建文件夹...**。您可以使用 `/` 分隔符创建嵌套目录路径，例如 `parentdir/subdir1/subdir2`。
   - 在 **Explorer** 面板的右上角，选择新建文件夹 (**{folder-new}**) 图标。
1. 输入新目录的名称，然后按 <kbd>Enter/Return</kbd> 创建它。
1. 右键单击目录路径并选择 **上传...**。
1. 选择您要上传的文件，然后选择 **打开**。您可以一次选择并添加多个文件。

该文件已上传并作为新文件自动添加到 Git 仓库。

## 切换分支

Web IDE 默认使用当前选择的分支。
在 Web IDE 中切换分支：

1. 在状态栏左下角，选择当前分支名称。
1. 在搜索框中，开始输入分支名称。
1. 从下拉列表中选择分支。

## 创建分支

在 Web IDE 中从当前分支创建分支：

1. 在状态栏左下角，选择当前分支名称。
1. 从下拉列表中选择 **创建新分支...**。
1. 输入分支名称。
1. 按 <kbd>Enter</kbd>。

如果您没有仓库的写入权限，则 **创建新分支...** 不可见。

## 提交更改

要在 Web IDE 中提交更改：

1. 在左侧的活动栏上，选择 **源控制**，或按 <kbd>Control</kbd>+<kbd>Shift</kbd>+<kbd>G</kbd>。
1. 输入您的提交信息。
1. 选择 **提交并推送**。
1. 提交到当前分支，或者创建一个新分支。

## 使用命令面板

在 Web IDE 中，您可以通过命令面板访问许多命令。
要打开命令面板并在 Web IDE 中运行命令：

1. 按 <kbd>Shift</kbd>+<kbd>Command</kbd>+<kbd>P</kbd>。
1. 在搜索框中，开始键入命令名称。
1. 从下拉列表中选择命令。

<a id="edit-settings"></a>

## 编辑设置

您可以使用设置编辑器来查看和修改您的用户和工作区设置。
要在 Web IDE 中打开设置编辑器：

- 在顶部菜单栏上，选择 **文件 > 偏好设置 > 设置**，或按 <kbd>Command</kbd>+<kbd>,</kbd>。

在设置编辑器中，您可以搜索要修改的设置。

## 编辑键盘快捷键

您可以使用键盘快捷键编辑器来查看和修改所有可用命令的默认键位绑定。
要在 Web IDE 中打开键盘快捷键编辑器：

- 在顶部菜单栏上，选择 **文件 > 偏好设置 > 键盘快键键**，或按 <kbd>Command</kbd>+<kbd>K</kbd> 然后按 <kbd>Command</kbd>+<kbd >S</kbd>。

在键盘快捷键编辑器中，您可以搜索：

- 您要更改的键位绑定
- 您要为其添加或删除键位绑定的命令

键位绑定基于您的键盘布局。如果您更改键盘布局，现有的键位绑定会自动更新。

## 更改主题

您可以为 Web IDE 选择不同的主题。Web IDE 的默认主题是 **GitLab Dark**。

要更改 Web IDE 主题：

1. 在顶部菜单栏上，选择 **文件 > 偏好设置 > 主题 > 颜色主题**，或按 <kbd>Command</kbd>+<kbd>K</kbd> 然后按 <kbd>Command</kbd> +<kbd>T</kbd>。
1. 从下拉列表中，使用箭头键预览主题。
1. 选择一个主题。

活动颜色主题存储在[用户设置](#edit-settings)中。

<!-- ## Privacy and data collection for extensions

The Web IDE Extension Marketplace is based on Open VSX. Open VSX does not collect any
data about you or your activities on the platform.

However, the privacy and data collection practices of extensions available on Open VSX can vary.
Some extensions might collect data to provide personalized recommendations or to improve the functionality.
Other extensions might collect data for analytics or advertising purposes.

To protect your privacy and data:

- Carefully review the permissions requested by an extension before you install the extension.
- Keep your extensions up to date to ensure that any security or privacy vulnerabilities are addressed promptly. -->

## Web IDE 的交互式 Web 终端（Beta）

当您在 Web IDE 中设置远程开发服务器时，您可以使用交互式 Web 终端：

- 访问服务器上的远程 shell。
- 与服务器的文件系统交互并远程执行命令。

您不能使用交互式网络终端与 runner 互动。
但是，您可以使用终端来安装依赖项以及编译和调试代码。

<!--
For more information about configuring a workspace that supports interactive web terminals, see [remote development](../remote_development/index.md).
-->
