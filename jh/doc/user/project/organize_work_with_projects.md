---
stage: Manage
group: Organization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 使用项目组织工作 **(FREE)**

在极狐GitLab 中，您可以创建项目来托管您的代码库。您还可以使用项目来跟踪议题、计划工作、协作编写代码以及持续构建、测试，和使用内置 CI/CD 来部署您的应用程序。

项目可见性可以为[公开、内部或私有](../public_access.md)。
极狐GitLab 不限制您可以创建的私有项目的数量。

- [创建项目](index.md)
- [管理项目](working_with_projects.md)
- [项目可见性](../public_access.md)
- [项目设置](../project/settings/index.md)
- [项目访问令牌](../project/settings/project_access_tokens.md)
- [分享项目](../project/members/share_project_with_groups.md)
- [保留的项目和群组名称](../../user/reserved_names.md)
- [搜索](../../user/search/index.md)
- [徽章](../../user/project/badges.md)
- [代码智能](../../user/project/code_intelligence.md)
- [合规](../../user/compliance/index.md)
- [描述模板](../../user/project/description_templates.md)
- [部署密钥](../../user/project/deploy_keys/index.md)
- [部署令牌](../../user/project/deploy_tokens/index.md)
- [文件查找器](../../user/project/repository/file_finder.md)
- [迁移项目](../../user/project/import/index.md)
- [使用文件导出迁移项目](../../user/project/settings/import_export.md)
