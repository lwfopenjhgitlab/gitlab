---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 发布 CI/CD 示例

极狐GitLab 发布功能很灵活，可以根据您的工作流程进行配置。此页面包含 CI/CD 发布作业示例。每个示例都演示了一种在 CI/CD 流水线中创建发布的方法。

<a id="create-a-release-when-a-git-tag-is-created"></a>

## 创建 Git 标签时创建发布

在此 CI/CD 示例中，发布由以下事件之一触发：

- 将 Git 标签推送到仓库。
- 在 UI 中创建 Git 标签。

如果您更喜欢手动创建 Git 标签，则可以使用此方法，然后创建一个发布。

NOTE:
在 UI 中创建 Git 标签时不要提供 release notes。提供 release notes 会创建一个发布，从而导致流水线失败。

以下为 `.gitlab-ci.yml` 示例文件摘录中的要点：

- `rules` 部分定义何时将作业添加到流水线。
- Git 标签用于发布的名称和描述。

```yaml
release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG                 # Run this job when a tag is created
  script:
    - echo "running release_job"
  release:                               # See https://docs.gitlab.com/ee/ci/yaml/#release for available properties
    tag_name: '$CI_COMMIT_TAG'
    description: '$CI_COMMIT_TAG'
```

<a id="create-a-release-when-a-commit-is-merged-to-the-default-branch"></a>

## 当提交合并到默认分支时创建发布

在此 CI/CD 示例中，当您将提交合并到默认分支时会触发发布。如果您的发布工作流程不手动创建标签，您可以使用此方法。

以下为 `.gitlab-ci.yml` 示例文件摘录中的要点：

- Git 标签、描述和引用在流水线中自动创建。
- 如果您手动创建标签，`release_job` 作业不会运行。

```yaml
release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
      when: never                                  # Do not run this job when a tag is created manually
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH  # Run this job when commits are pushed or merged to the default branch
  script:
    - echo "running release_job for $TAG"
  release:                                         # See https://docs.gitlab.com/ee/ci/yaml/#release for available properties
    tag_name: 'v0.$CI_PIPELINE_IID'                # The version is incremented per pipeline.
    description: 'v0.$CI_PIPELINE_IID'
    ref: '$CI_COMMIT_SHA'                          # The tag is created from the pipeline SHA.
```

NOTE:
在 `before_script` 或 `script` 中设置的环境变量不可用于在同一个作业中展开。

<a id="create-release-metadata-in-a-custom-script"></a>

## 在自定义脚本中创建发布元数据

在这个 CI/CD 示例中，发布准备被拆分为单独的作业，以获得更大的灵活性：

- `prepare_job` 作业生成发布元数据。任何镜像都可用于运行作业，包括自定义镜像。生成的元数据存储在变量文件 `variables.env` 中。此元数据[传递给下游作业](../../../ci/variables/index.md#pass-an-environment-variable-to-another-job)。
- `release_job` 使用变量文件中的内容创建发布，使用变量文件中传递给它的元数据。此作业必须使用 `registry.gitlab.cn/gitlab-org/release-cli:latest` 镜像，因为它包含发布 CLI。

```yaml
prepare_job:
  stage: prepare                                              # This stage must run before the release stage
  rules:
    - if: $CI_COMMIT_TAG
      when: never                                             # Do not run this job when a tag is created manually
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH             # Run this job when commits are pushed or merged to the default branch
  script:
    - echo "EXTRA_DESCRIPTION=some message" >> variables.env  # Generate the EXTRA_DESCRIPTION and TAG environment variables
    - echo "TAG=v$(cat VERSION)" >> variables.env             # and append to the variables.env file
  artifacts:
    reports:
      dotenv: variables.env                                   # Use artifacts:reports:dotenv to expose the variables to other jobs

release_job:
  stage: release
  image: registry.gitlab.cn/gitlab-org/release-cli:latest
  needs:
    - job: prepare_job
      artifacts: true
  rules:
    - if: $CI_COMMIT_TAG
      when: never                                  # Do not run this job when a tag is created manually
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH  # Run this job when commits are pushed or merged to the default branch
  script:
    - echo "running release_job for $TAG"
  release:
    name: 'Release $TAG'
    description: 'Created using the release-cli $EXTRA_DESCRIPTION'  # $EXTRA_DESCRIPTION and the $TAG
    tag_name: '$TAG'                                                 # variables must be defined elsewhere
    ref: '$CI_COMMIT_SHA'                                            # in the pipeline. For example, in the
    milestones:                                                      # prepare_job
      - 'm1'
      - 'm2'
      - 'm3'
    released_at: '2020-07-15T08:00:00Z'  # Optional, is auto generated if not defined, or can use a variable.
    assets:
      links:
        - name: 'asset1'
          url: 'https://example.com/assets/1'
        - name: 'asset2'
          url: 'https://example.com/assets/2'
          filepath: '/pretty/url/1' # optional
          link_type: 'other' # optional
```

## 创建发布时跳过多个流水线

如果关联的标签尚不存在，则使用 CI/CD 作业创建发布可能会触发多个流水线。要了解这是如何发生的，请考虑以下工作流程：

- 先打标签，再发布：

  1. 通过 UI 创建标签或推送标签。
  1. 标签流水线被触发，并运行 `release` 作业。
  1. 创建一个发布。

- 先发布，再打标签：

  1. 当提交被推送或合并到默认分支时触发流水线。流水线运行 `release` 作业。
  1. 创建一个发布。
  1. 创建一个标签。
  1. 触发标签流水线。流水线还运行 `release` 作业。

在第二个工作流程中，`release` 作业在多个流水线中运行。为防止这种情况，您可以使用 [`workflow:rules` 关键字](../../../ci/yaml/index.md#workflowrules)来确定发布作业是否应在标签流水线中运行：

```yaml
release_job:
  rules:
    - if: $CI_COMMIT_TAG
      when: never                                  # Do not run this job in a tag pipeline
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH  # Run this job when commits are pushed or merged to the default branch
  script:
    - echo "Create release"
  release:
    name: 'My awesome release'
    tag_name: '$CI_COMMIT_TAG'
```
