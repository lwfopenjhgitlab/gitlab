---
type: reference, howto
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 发布 **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/41766) in GitLab 11.7.
-->

在极狐GitLab 中，发布使您可以为用户创建项目的快照，包括安装包和发行说明。您可以在任何分支上创建极狐GitLab 发布。创建发布还会创建一个 [Git 标签](https://git-scm.com/book/en/v2/Git-Basics-Tagging) 来标记源代码中的发布点。

WARNING:
删除与发布关联的 Git 标签也会删除发布。

一个发布可以包括：

- 仓库源代码的快照。
- 从作业产物创建的[通用包](../../packages/generic_packages/index.md)。
- 与您的代码的已发布版本相关联的其他元数据。
- 发行说明。

当您[创建发布](#create-a-release)时：

- 极狐GitLab 自动存档源代码并将其与发布相关联。
- 极狐GitLab 会自动创建一个 JSON 文件，列出发布中的所有内容，以便您可以比较和审核发布。这个文件叫做 [release evidence](release_evidence.md)。

创建发布时或之后，您可以：

- 添加发行说明。
- 为与发布关联的 Git 标签添加一条消息。
- [将里程碑与其关联](#associate-milestones-with-a-release)。
- 附加[发布 assets](release_fields.md#release-assets)，例如 Runbook 或包。

## 查看发布

要查看发布列表：

- 在左侧边栏上，选择 **部署 > 发布**，或

- 在项目概览页面上，如果至少存在一个发布，请单击发布数。

  ![Number of Releases](img/releases_count_v13_2.png "Incremental counter of Releases")

  - 在公开项目中，这个数字对所有用户都是可见的。
  - 在私有项目中，具有报告者或更高权限的用户可以看到此数字。

### 排序发布

要按 **发布日期** 或 **创建日期** 对版本进行排序，请从排序顺序下拉列表中进行选择。要在升序或降序之间切换，请选择 **排序顺序**。

![Sort Releases dropdown button](img/releases_sort_v13_6.png)

<a id="create-a-release"></a>

## 创建发布

您可以使用以下方式创建发布：

- [在 CI/CD 流水线中使用作业](#creating-a-release-by-using-a-cicd-job)。
- [在发布页面](#create-a-release-in-the-releases-page)。
- 使用[发布 API](../../../api/releases/index.md#create-a-release)。

我们建议将发布版本作为 CI/CD 流水线中的最后一步之一。

<a id="create-a-release-in-the-releases-page"></a>

### 在发布页面创建发布

先决条件：

- 您必须至少具有项目的开发人员角色。<!--For more information, read
[Release permissions](#release-permissions).-->

要在“发布”页面中创建发布：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 发布** 并选择 **新建发布**。
1. 从 [**标签名称**](release_fields.md#tag-name) 下拉菜单中，可以：
   - 选择现有的 Git 标签。 选择已与发布关联的现有标签会导致验证错误。
   - 输入新的 Git 标签名称。
      1. 从 **创建标签** 下拉列表中，选择在创建新标签时使用的分支或提交 SHA。
      1. 可选。在 **设置标签消息** 文本框中，输入消息以创建[有注释的标签](https://git-scm.com/book/en/v2/Git-Basics-Tagging#_annotated_tags)。
     1. 选择 **保存**。

1. 可选。输入有关发布的其他信息，包括：
   - [标题](release_fields.md#title)。
   - [里程碑](#associate-milestones-with-a-release)。
   - [发布说明](release_fields.md#release-notes-description)。
   - 是否包含[标签消息](../../../topics/git/tags.md)。
   - [Asset 链接](release_fields.md#links)。
1. 选择 **创建发布**。

<a id="creating-a-release-by-using-a-cicd-job"></a>

### 使用 CI/CD 作业创建发布

您可以通过在作业定义中使用 [`release` 关键字](../../../ci/yaml/index.md#release)，将直接创建发布作为极狐GitLab CI/CD 流水线的一部分。

仅当作业处理无误时才会创建发布。如果 API 在发布创建过程中返回错误，则发布作业失败。

使用 CI/CD 作业创建发布的方法包括：

- [创建 Git 标签时创建发布](release_cicd_examples.md#create-a-release-when-a-git-tag-is-created)。
- [当提交合并到默认分支时创建发布](release_cicd_examples.md#create-a-release-when-a-commit-is-merged-to-the-default-branch)。
- [在自定义脚本中创建发布元数据](release_cicd_examples.md#create-release-metadata-in-a-custom-script)。

### 使用自定义 SSL CA 证书颁发机构

您可以使用 `ADDITIONAL_CA_CERT_BUNDLE` CI/CD 变量来配置自定义 SSL CA 证书颁发机构，用于在 `release-cli` 通过 API 使用带有自定义证书的 HTTPS 创建版本时验证对等方。
`ADDITIONAL_CA_CERT_BUNDLE` 值应包含 [X.509 PEM 公钥证书的文本表示](https://www.rfc-editor.org/rfc/rfc7468#section-5.1) 或 `path/to/file` 包含证书颁发机构。
例如，要在 `.gitlab-ci.yml` 文件中配置此值，请使用以下命令：

```yaml
release:
  variables:
    ADDITIONAL_CA_CERT_BUNDLE: |
        -----BEGIN CERTIFICATE-----
        MIIGqTCCBJGgAwIBAgIQI7AVxxVwg2kch4d56XNdDjANBgkqhkiG9w0BAQsFADCB
        ...
        jWgmPqF3vUbZE0EyScetPJquRFRKIesyJuBFMAs=
        -----END CERTIFICATE-----
  script:
    - echo "Create release"
  release:
    name: 'My awesome release'
    tag_name: '$CI_COMMIT_TAG'
```

`ADDITIONAL_CA_CERT_BUNDLE` 值也可以配置为 UI 中的自定义变量<!--[UI 中的自定义变量](../../../ci/variables/index.md#custom-cicd-variables)-->，或者作为 `file`，需要证书的路径；或作为变量，需要证书的文本表示。

### 在单个流水线中创建多个发布

一个流水线可以有多个 `release` 作业，例如：

```yaml
ios-release:
  script:
    - echo "iOS release job"
  release:
    tag_name: v1.0.0-ios
    description: 'iOS release v1.0.0'

android-release:
  script:
    - echo "Android release job"
  release:
    tag_name: v1.0.0-android
    description: 'Android release v1.0.0'
```

<!--
### Release assets as Generic packages

You can use [Generic packages](../../packages/generic_packages/index.md) to host your release assets.
For a complete example, see the [Release assets as Generic packages](https://gitlab.com/gitlab-org/release-cli/-/tree/master/docs/examples/release-assets-as-generic-package/)
project.
-->

## 即将发布

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/38105) in GitLab 12.1.
-->

您可以使用 Releases API<!--[Releases API](../../../api/releases/index.md#upcoming-releases)--> 提前创建发布。
当您设置未来的 `released_at` 日期时，发布标签旁边会显示 **即将发布** 徽章。当 `released_at` 日期和时间过去后，徽章会自动移除。

![An upcoming release](img/upcoming_release_v12_7.png)

## 历史发布

> 引入于 15.2 版本。

您可以使用[发布 API](../../../api/releases/index.md#historical-releases) 或 UI 创建过去的发布。当您设置过去的 `released_at` 日期时，发布标签旁边会显示一个 **历史发布** 徽章。由于在过去的时间被发布，[release evidence](release_evidence.md) 不可用。

<a id="edit-a-release"></a>

## 编辑发布

只有至少具有开发人员角色的用户才能编辑版本。
阅读有关[发布权限](#发布权限)的更多信息。

要编辑发布的详细信息：

1. 在左侧边栏上，选择 **部署 > 发布**。
1. 在您要修改的版本的右上角，单击 **编辑此发布**（铅笔图标）。
1. 在 **编辑发布** 页面上，更改发布的详细信息。
1. 单击 **保存修改**。

您可以编辑版本标题、注释、关联的里程碑和 assets 链接。
要更改发布日期，请使用 Releases API<!--[Releases API](../../../api/releases/index.md#update-a-release)-->。

## 删除发布

> 引入于 15.2 版本。

当您删除一个发布时，它的 assets 也会被删除。但是，关联的 Git 标记不会被删除。

先决条件：

- 您必须至少具有开发者角色。阅读有关[发布权限](#release-permissions)的更多信息。

要在 UI 中删除版本：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 发布**。
1. 在您要删除的发布的右上角，选择 **编辑此发布** (**{pencil}**)。
1. 在 **编辑发布** 页面上，选择 **删除**。
1. 选择 **删除发布**。

<a id="associate-milestones-with-a-release"></a>

## 将里程碑与发布相关联

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/29020) in GitLab 12.5.
> - [Updated](https://gitlab.com/gitlab-org/gitlab/-/issues/39467) to edit milestones in the UI in GitLab 13.0.
-->

您可以将一个发布与一个或多个项目里程碑<!--[项目里程碑](../milestones/index.md#project-milestones-and-group-milestones)-->相关联。

[专业版](https://gitlab.cn/pricing/)客户可以指定群组里程碑<!--[群组里程碑](../milestones/index.md#project-milestones-and-group-milestones)-->与发布相关联。

您可以在用户界面中执行此操作，或者通过在对 Releases API<!--[Releases API](../../../api/releases/index.md#create-a-release)--> 的请求中包含一个 `milestones` 数组。

在用户界面中，要将里程碑与发布相关联：

1. 在左侧边栏上，选择 **部署 > 发布**。
1. 在您要修改的发布的右上角，单击 **编辑此发布**（铅笔图标）。
1. 从 **里程碑** 列表中，选择要关联的每个里程碑。您可以选择多个里程碑。
1. 单击 **保存修改**。

在 **部署 > 发布** 页面上，**里程碑** 列在顶部，以及有关里程碑中议题的统计信息。

![A Release with one associated milestone](img/release_with_milestone_v12_9.png)

在 **议题 > 里程碑** 页面上以及单击此页面上的里程碑时，也可以看到发布。

下面是一个里程碑示例，分别是没有发布、一个发布和两个发布。

![Milestones with and without Release associations](img/milestone_list_with_releases_v12_5.png)

NOTE:
子组的项目发布不能与超级组的里程碑相关联。<!--To learn
more, read issue #328054,
[Releases cannot be associated with a supergroup milestone](https://gitlab.com/gitlab-org/gitlab/-/issues/328054).-->

## 创建发布时收到通知

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/26001) in GitLab 12.4.
-->

当为您的项目创建新发布时，您可以通过电子邮件收到通知。

要订阅发布通知：

1. 在左侧边栏上，选择 **项目信息**。
1. 点击 **通知设置**（铃铛图标）。
1. 在列表中，单击 **自定义**。
1. 选中 **新建版本** 复选框。
1. 关闭对话框进行保存。

<a id="prevent-unintentional-releases-by-setting-a-deploy-freeze"></a>

## 通过设置部署冻结来防止意外发布

> - 引入于 13.0 版本。
> - 通过 UI 删除冻结期的能力引入于 14.3 版本。

通过设置*部署冻结*期<!--[*部署冻结*期](../../../ci/environments/deployment_safety.md)-->，在您指定的时间段内防止意外的生产发布。
部署冻结有助于减少自动化部署时的不确定性和风险。

维护者可以在用户界面中设置一个部署冻结窗口，或者通过使用 Freeze Periods API<!--[Freeze Periods API](../../../api/freeze_periods.md)--> 来设置一个 `freeze_start` 和一个 `freeze_end`，定义为 [crontab](https://crontab.guru/) 条目。

如果正在执行的作业在冻结期内，GitLab CI/CD 会创建一个名为 `$CI_DEPLOY_FREEZE` 的环境变量。

为了防止部署作业执行，在你的 `.gitlab-ci.yml` 中创建一个 `rules` 条目，例如：

```yaml
deploy_to_production:
  stage: deploy
  script: deploy_to_prod.sh
  rules:
    - if: $CI_DEPLOY_FREEZE == null
  environment: production
```

要在 UI 中设置部署冻结窗口，请完成以下步骤：

1. 用具有维护者角色的用户身份登录极狐GitLab。
1. 在左侧边栏上，选择 **项目信息**。
1. 在左侧导航菜单中，导航至 **设置 > CI/CD**。
1. 滚动到 **部署冻结**。
1. 点击 **展开** 查看部署冻结表。
1. 点击 **添加部署冻结** 打开部署冻结窗口。
1. 输入所需部署冻结期的开始时间、结束时间和时区。
1. 在窗口中点击 **添加部署冻结**。
1. 部署冻结保存后，您可以通过选择编辑按钮（**{pencil}**）对其进行编辑，并通过选择删除按钮（**{remove}**）将其删除。

   ![Deploy freeze modal for setting a deploy freeze period](img/deploy_freeze_v14_3.png)

如果项目包含多个冻结期，则所有冻结期都适用。如果它们重叠，则冻结覆盖整个重叠期。

<!--
For more information, see [Deployment safety](../../../ci/environments/deployment_safety.md).
-->

## Release evidence

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/26019) in GitLab 12.6.
-->



```json
{
  "release": {
    "id": 5,
    "tag_name": "v4.0",
    "name": "New release",
    "project": {
      "id": 20,
      "name": "Project name",
      "created_at": "2019-04-14T11:12:13.940Z",
      "description": "Project description"
    },
    "created_at": "2019-06-28 13:23:40 UTC",
    "description": "Release description",
    "milestones": [
      {
        "id": 11,
        "title": "v4.0-rc1",
        "state": "closed",
        "due_date": "2019-05-12 12:00:00 UTC",
        "created_at": "2019-04-17 15:45:12 UTC",
        "issues": [
          {
            "id": 82,
            "title": "The top-right popup is broken",
            "author_name": "John Doe",
            "author_email": "john@doe.com",
            "state": "closed",
            "due_date": "2019-05-10 12:00:00 UTC"
          },
          {
            "id": 89,
            "title": "The title of this page is misleading",
            "author_name": "Jane Smith",
            "author_email": "jane@smith.com",
            "state": "closed",
            "due_date": "nil"
          }
        ]
      },
      {
        "id": 12,
        "title": "v4.0-rc2",
        "state": "closed",
        "due_date": "2019-05-30 18:30:00 UTC",
        "created_at": "2019-04-17 15:45:12 UTC",
        "issues": []
      }
    ],
    "report_artifacts": [
      {
        "url":"https://gitlab.example.com/root/project-name/-/jobs/111/artifacts/download"
      }
    ]
  }
}
```

<a id="release-permissions"></a>

## 发布权限

> 创建、更新和删除操作的权限窗口修复于 14.1 版本

<a id="view-a-release-and-download-assets"></a>

### 查看发布并下载 assets

> 访客角色的访问更改于 14.5 版本。

- 具有报告者或以上角色的用户，具有对项目发布的 read 和下载权限。
- 具有访客角色的用户具有对项目发布的读取和下载访问权限。包括相关的 Git 标签名称、发布描述、发布的作者信息。但是，其他与仓库相关的信息，例如[源代码](release_fields.md#source-code)、[release evidence](release_evidence.md) 已被编辑。

### 发布版本而不授予对源代码的访问权限

> 引入于 15.6 版本。

非项目成员可以访问发布，同时保持与代码库相关的信息私有，例如[源代码](release_fields.md#source-code)和 [release evidence](release_evidence.md)，这对于使用发布作为可访问的新版本软件，但不希望公开源代码的方式的项目很有用。

要公开发布，请设置以下[项目设置](../settings/index.md#project-feature-settings)：

- 代码库已启用并设置为**仅项目成员**
- 发布已启用并设置为**具有访问权限的任何人**

### 创建、更新、删除发布及其 assets

- 具有开发者及以上角色的用户具有对项目发布和 assets 的写入权限。
- 如果发布与[受保护标签](../protected_tags.md)相关联，则用户也必须[允许创建受保护标签](../protected_tags.md#configuring-protected-tags)。

作为发布权限控制的示例，您可以通过带有通配符 (`*`) 的标记保护，只允许维护者或以上权限创建、更新和删除发布 ，并在 **允许创建** 列中设置 **维护者**。

## 发布指标 **(ULTIMATE)**

> 引入于专业版 13.9 版本。

通过导航到 **群组 > 分析 > CI/CD**，可以获得群组级发布指标。
这些指标包括：

- 群组内发布总数
- 群组中至少有一个发布的项目的百分比

<!--
## 工作示例项目

The Guided Exploration project [Utterly Automated Software and Artifact Versioning with GitVersion](https://gitlab.com/guided-explorations/devops-patterns/utterly-automated-versioning) demonstrates:

- Using GitLab releases.
- Using the GitLab `release-cli`.
- Creating a generic package.
- Linking the package to the release.
- Using a tool called [GitVersion](https://gitversion.net/) to automatically determine and increment versions for complex repositories.

You can copy the example project to your own group or instance for testing. More details on what other GitLab CI patterns are demonstrated are available at the project page.
-->

## 故障排查

### 在创建、更新或删除版本及其 assets 时出现 `403 Forbidden` 或 `Something went wrong while creating a new release` 错误

如果发布与[受保护的标签](../protected_tags.md)相关联，则 UI/API 请求可能会导致授权失败。
确保用户或服务/机器人帐户也被允许[创建受保护的标签](../protected_tags.md#configuring-protected-tags)。

有关更多信息，请参阅[发布权限](#release-permissions)。

### 存储注意事项

请注意，该功能是基于 Git 标签构建的，因此除了创建发布本身之外几乎不需要额外的数据。自动生成的 assets 和 release evidence 会消耗存储空间。
