---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 发布字段

创建或编辑发布时，以下字段可用。

<a id="title"></a>

## 标题

在创建或编辑发布时，可以使用 **发布标题** 字段自定义发布标题。如果未提供标题，则使用发布的标签名称。

<a id="tag-name"></a>

## 标签名称

发布标签名称应包含发布版本。极狐GitLab 使用[语义版本控制](https://semver.org/)发布我们的版本，我们建议您也这样做，使用 `(Major).(Minor).(Patch)`<!--，详见 [GitLab Policy for Versioning](../../../policy/maintenance.md#versioning)-->。

例如，对于版本 `10.5.7`：

- `10` 代表主要版本。主要版本是 `10.0.0`，但通常称为 `10.0`。
- `5` 代表小版本。次要版本是 `10.5.0`，但通常称为 `10.5`。
- `7` 表示补丁编号。

版本号的任何部分都可以是多个数字，例如 `13.10.11`。

<a id="release-notes-description"></a>

## 发行说明描述

每个发布都有一个描述。您可以添加任何您喜欢的文本，但我们建议您包含一个更改日志来描述您的版本内容。这有助于用户快速扫描您发布的每个版本之间的差异。

[Git 的标签消息](https://git-scm.com/book/en/v2/Git-Basics-Tagging) 可以通过选择 **在发行说明中包含标签消息**，将其包含在发行说明描述中。说明支持 [Markdown](../../markdown.md)。

<a id="release-assets"></a>

## 发布 assets

一个版本包含以下类型的 assets：

- [源代码](#source-code)
- [链接](#links)

<a id="source-code"></a>

### 源代码

极狐GitLab 从给定的 Git 标签自动生成 `zip`、`tar.gz`、`tar.bz2` 和 `tar` 归档源代码。这些是只读 assets。

<a id="links"></a>

### 链接

链接是可以指向您喜欢的任何内容的任何 URL：文档、构建的二进制文件或其他相关材料。这些可以是来自实例的内部或外部链接。作为 assets 的每个链接都具有以下属性：

| 属性   | 描述                            | 是否必需 |
| ----        | -----------                            | ---      |
| `name`      | 链接的名称。                  | Yes      |
| `url`       | 下载文件的 URL。            | Yes      |
| `filepath`  | 指向 `url` 的重定向链接。有关更多信息，请参阅[此部分](#permanent-links-to-release-assets)。 | No       |
| `link_type` | 用户可以通过 `url` 下载的内容类型。有关更多信息，请参阅[此部分](#link-types)。 | No       |

<a id="permanent-link-to-latest-release"></a>

#### 最新发布的永久链接

> 引入于 14.9 版本。

最新发布页面可通过永久 URL 访问。
系统将在访问时重定向到最新的发布页面 URL。

URL 的格式为：

```plaintext
https://host/namespace/project/-/releases/permalink/latest
```

我们还支持，后缀路径继续重定向到最新发布。
例如，如果 `v14.8.0-jh` 是最新发布并且具有可读链接 `https://host/namespace/project/-/releases/v14.8.0-jh#release`，则可以将其寻址为 `https://host/namespace/project/-/releases/permalink/latest#release`。

请参阅[发布 assets 的永久链接](#permanent-links-to-latest-release-assets)部分，了解有关后缀路径结转使用的更多信息。

##### 排序首选项

默认情况下，极狐GitLab 使用 `released_at` 时间获取版本。查询参数 `?order_by=released_at` 的使用是可选的。

<a id="permanent-links-to-release-assets"></a>

#### 发布 assets 的永久链接

> 引入于 15.9 版本，可以使用个人访问令牌访问私有发布的链接。

与发布相关的 assets 可通过永久 URL 访问。
系统始终将此 URL 重定向到实际 assets 位置，因此即使 assets 移动到不同位置，您也可以继续使用相同的 URL。这是在[链接创建](../../../api/releases/links.md#create-a-link)或[更新时](../../../api/releases/links.md#update-a-link)定义的。

URL 的格式为：


```plaintext
https://host/namespace/project/-/releases/:release/downloads/:filepath
```

<!--
If you have an asset for the `v11.9.0-rc2` release in the `gitlab-org`
namespace and `gitlab-runner` project on `gitlab.com`, for example:

```json
{
  "name": "linux amd64",
  "filepath": "/binaries/gitlab-runner-linux-amd64",
  "url": "https://gitlab-runner-downloads.s3.amazonaws.com/v11.9.0-rc2/binaries/gitlab-runner-linux-amd64",
  "link_type": "other"
}
```

This asset has a direct link of:

```plaintext
https://gitlab.com/gitlab-org/gitlab-runner/-/releases/v11.9.0-rc2/downloads/binaries/gitlab-runner-linux-amd64
```

The physical location of the asset can change at any time and the direct link remains unchanged.
-->

如果发布是私有的，您需要在发出请求时使用 `private_token` 查询参数或 `HTTP_PRIVATE_TOKEN` header 提供具有 `api` 或 `read_api` 范围的个人访问令牌。例如：

```shell
curl --location --output filename "https://gitlab.example.com/my-group/my-project/-/releases/:release/downloads/:filepath?private_token=<your_access_token>"
curl --location --output filename --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/my-group/my-project/-/releases/:release/downloads/:filepath"
```

<a id="permanent-links-to-latest-release-assets"></a>

#### 最新发布 assets 的永久链接

> 引入于 14.9 版本。

[发布 assets 的永久链接](#permanent-links-to-release-assets)中的 `filepath` 可以与[最新版本的永久链接](#permanent-link-to-latest-release)结合使用。当我们想要链接永久 URL，从*最新发布*下载 assets 时，它很有用。

URL 的格式为：


```plaintext
https://host/namespace/project/-/releases/permalink/latest/downloads/:filepath
```

<!--
If you have an asset with [`filepath`](../../../api/releases/links.md#create-a-link) for the `v11.9.0-rc2` latest release in the `gitlab-org`
namespace and `gitlab-runner` project on `gitlab.com`, for example:

```json
{
  "name": "linux amd64",
  "filepath": "/binaries/gitlab-runner-linux-amd64",
  "url": "https://gitlab-runner-downloads.s3.amazonaws.com/v11.9.0-rc2/binaries/gitlab-runner-linux-amd64",
  "link_type": "other"
}
```

This asset has a direct link of:

```plaintext
https://gitlab.com/gitlab-org/gitlab-runner/-/releases/permalink/latest/downloads/binaries/gitlab-runner-linux-amd64
```
-->

<a id="link-types"></a>

#### 链接类型

> 引入于 13.1 版本。

四种类型的链接是 “Runbook”、“Package”、“Image” 和 “Other”。
`link_type` 参数接受以下四个值之一：

- `runbook`
- `package`
- `image`
- `other`（默认）

此字段对 URL 没有影响，它仅用于项目的发布页面中的可视化区分。


#### 使用通用包来附加二进制文件

您可以使用通用包<!--[通用包](../../packages/generic_packages/index.md)-->存储来自发布或标签流水线的任何产物，也可用于将二进制文件附加到单个发布条目。
基本上，您需要：

1. 将产物推送到通用软件包库<!--[将产物推送到通用软件包库](../../packages/generic_packages/index.md#publish-a-package-file)-->。
1. [将软件包链接附加到发布](#links)。

以下示例生成发布 assets，将它们作为通用包发布，然后创建发布：


```yaml
stages:
  - build
  - upload
  - release

variables:
  # Package version can only contain numbers (0-9), and dots (.).
  # Must be in the format of X.Y.Z, i.e. should match /\A\d+\.\d+\.\d+\z/ regular expresion.
  # See https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file
  PACKAGE_VERSION: "1.2.3"
  DARWIN_AMD64_BINARY: "myawesomerelease-darwin-amd64-${PACKAGE_VERSION}"
  LINUX_AMD64_BINARY: "myawesomerelease-linux-amd64-${PACKAGE_VERSION}"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/myawesomerelease/${PACKAGE_VERSION}"

build:
  stage: build
  image: alpine:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - mkdir bin
    - echo "Mock binary for ${DARWIN_AMD64_BINARY}" > bin/${DARWIN_AMD64_BINARY}
    - echo "Mock binary for ${LINUX_AMD64_BINARY}" > bin/${LINUX_AMD64_BINARY}
  artifacts:
    paths:
      - bin/

upload:
  stage: upload
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/${DARWIN_AMD64_BINARY} "${PACKAGE_REGISTRY_URL}/${DARWIN_AMD64_BINARY}"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/${LINUX_AMD64_BINARY} "${PACKAGE_REGISTRY_URL}/${LINUX_AMD64_BINARY}"

release:
  # Caution, as of 2021-02-02 these assets links require a login, see:
  # https://gitlab.com/gitlab-org/gitlab/-/issues/299384
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
        --assets-link "{\"name\":\"${DARWIN_AMD64_BINARY}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${DARWIN_AMD64_BINARY}\"}" \
        --assets-link "{\"name\":\"${LINUX_AMD64_BINARY}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${LINUX_AMD64_BINARY}\"}"
```

<!--
PowerShell users may need to escape the double quote `"` inside a JSON
string with a `` ` `` (back tick) for `--assets-link` and `ConvertTo-Json`
before passing on to the `release-cli`.
For example:

```yaml
release:
  script:
    - $env:asset = "{`"name`":`"MyFooAsset`",`"url`":`"https://gitlab.com/upack/artifacts/download/$env:UPACK_GROUP/$env:UPACK_NAME/$($env:GitVersion_SemVer)?contentOnly=zip`"}"
    - $env:assetjson = $env:asset | ConvertTo-Json
    - release-cli create --name $CI_COMMIT_TAG --description "Release $CI_COMMIT_TAG" --ref $CI_COMMIT_TAG --tag-name $CI_COMMIT_TAG --assets-link=$env:assetjson
```
-->

NOTE:
不建议将[作业产物](../../../ci/jobs/job_artifacts.md)链接直接附加到发布，因为产物是短暂的，用于在同一流水线中传递数据。这意味着它们可能会过期或有人可能会手动删除它们。

<!--
### Number of new and total features **(FREE SAAS)**

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/235618) in GitLab 13.5.

On [GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/releases), you can view the number of new and total features in the project.

![Feature count](img/feature_count_v14_6.png "Number of features in a release")

The totals are displayed on [shields](https://shields.io/) and are generated per release by
[a Rake task in the `www-gitlab-com` repository](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/lib/tasks/update_gitlab_project_releases_page.rake).

| Item             | Formula                                                                            |
|------------------|------------------------------------------------------------------------------------|
| `New features`   | Total count of release posts across all tiers for a single release in the project. |
| `Total features` | Total count of release posts in reverse order for all releases in the project.     |

The counts are also shown by license tier.

| Item             | Formula                                                                                             |
|------------------|-----------------------------------------------------------------------------------------------------|
| `New features`   | Total count of release posts across a single tier for a single release in the project.              |
| `Total features` | Total count of release posts across a single tier in reverse order for all releases in the project. |
-->
