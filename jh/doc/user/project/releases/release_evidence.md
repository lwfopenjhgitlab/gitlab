---
stage: Govern
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Release evidence **(FREE)**

每次创建发布时，系统都会生成与其相关的数据的快照。
此数据保存在 JSON 文件中，称为 *release evidence*。该功能包括测试产物和链接的里程碑，以促进内部流程，如外部审计。

要访问 release evidence，请在发布页面上，单击 **凭证集** 标题下列出的 JSON 文件的链接。

您还可以使用 API<!--[使用 API](../../../api/releases/index.md#collect-release-evidence)--> 为现有发布生成 release evidence。因此，每个发布都可以有多个 release evidence 快照。您可以在发布页面上查看 release evidence 及其详细信息。

禁用议题跟踪器后，无法下载 release evidence。

下面是一个 release evidence 对象的示例：

```json
{
  "release": {
    "id": 5,
    "tag_name": "v4.0",
    "name": "New release",
    "project": {
      "id": 20,
      "name": "Project name",
      "created_at": "2019-04-14T11:12:13.940Z",
      "description": "Project description"
    },
    "created_at": "2019-06-28 13:23:40 UTC",
    "description": "Release description",
    "milestones": [
      {
        "id": 11,
        "title": "v4.0-rc1",
        "state": "closed",
        "due_date": "2019-05-12 12:00:00 UTC",
        "created_at": "2019-04-17 15:45:12 UTC",
        "issues": [
          {
            "id": 82,
            "title": "The top-right popup is broken",
            "author_name": "John Doe",
            "author_email": "john@doe.com",
            "state": "closed",
            "due_date": "2019-05-10 12:00:00 UTC"
          },
          {
            "id": 89,
            "title": "The title of this page is misleading",
            "author_name": "Jane Smith",
            "author_email": "jane@smith.com",
            "state": "closed",
            "due_date": "nil"
          }
        ]
      },
      {
        "id": 12,
        "title": "v4.0-rc2",
        "state": "closed",
        "due_date": "2019-05-30 18:30:00 UTC",
        "created_at": "2019-04-17 15:45:12 UTC",
        "issues": []
      }
    ],
    "report_artifacts": [
      {
        "url":"https://gitlab.example.com/root/project-name/-/jobs/111/artifacts/download"
      }
    ]
  }
}
```

### 收集 release evidence **(PREMIUM SELF)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/199065) in [GitLab Premium](https://about.gitlab.com/pricing/) 12.10.
-->

创建发布时，会自动收集 release evidence。要在任何其他时间启动收集，请使用 API 调用<!--[API 调用](../../../api/releases/index.md#collect-release-evidence)-->。您可以为一次发布多次收集 release evidence。

在发布页面上可以看到 release evidence 快照以及时间戳。

### 包括报告产物作为 release evidence **(ULTIMATE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/32773) in [GitLab Ultimate](https://about.gitlab.com/pricing/) 13.2.
-->

创建发布时，如果[作业产物](../../../ci/yaml/index.md#artifactsreports)包含在最后运行的流水线中，它们会自动包含在发布中作为 release evidence。

尽管作业产物通常会过期，但 release evidence 中包含的产物不会过期。

要启用作业产物收集，您需要同时指定：

1. [`artifacts:paths`](../../../ci/yaml/index.md#artifactspaths)
1. [`artifacts:reports`](../../../ci/yaml/index.md#artifactsreports)

```yaml
ruby:
  script:
    - gem install bundler
    - bundle install
    - bundle exec rspec --format progress --format RspecJunitFormatter --out rspec.xml
  artifacts:
    paths:
      - rspec.xml
    reports:
      junit: rspec.xml
```

如果流水线成功运行，当您创建发布时，`rspec.xml` 文件将保存为 release evidence。

如果您[计划 release evidence 收集](#schedule-release-evidence-collection)，一些产物可能在收集时已经过期。为了避免这种情况，您可以使用 [`artifacts:expire_in`](../../../ci/yaml/index.md#artifactsexpire_in) 关键字。

<a id="schedule-release-evidence-collection"></a>

### 计划 release evidence 收集

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/23697) in GitLab 12.8.
-->

在 API 中：

- 如果您指定未来的 `released_at` 日期，则该版本将成为 **未来发布**，并在发布日期收集 release evidence。在此之前您不能收集 release evidence。
- 如果您指定过去的 `released_at` 日期，则该版本将成为 **历史发布**，并且不会收集任何 release evidence。
- 如果您未指定 `released_at` 日期，则在创建发布之日收集 release evidence。


