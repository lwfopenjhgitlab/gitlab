---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 系统备注 **(FREE)**

系统备注是简短的描述，可帮助您了解极狐GitLab 对象生命周期中发生的事件的历史记录，例如：

- [警报](../../operations/incident_management/alerts.md)
- [设计](issues/design_management.md)
- [议题](issues/index.md)
- [合并请求](merge_requests/index.md)
- [目标和关键结果](../okrs.md)（OKR）
- [任务](../tasks.md)

极狐GitLab 在系统备注中，记录有关由 Git 或极狐GitLab 应用程序触发的事件的信息。系统备注使用格式 `<Author> <action> <time ago>`。

## 显示或过滤系统备注

默认情况下，不显示系统备注。显示系统备注时，优先显示最早的。
如果您更改过滤器或排序选项，系统跨会话使用您的选择。
过滤选项：

- **显示所有活动**：显示评论和历史记录。
- **仅显示评论**：隐藏系统备注。
- **仅显示历史记录**：隐藏用户评论。

### 史诗

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **史诗** (**{epic}**)。
1. 确定您想要的史诗，并选择其标题。
1. 转到 **活动** 部分。
1. 对于 **排序或筛选**，选择 **显示所有活动**。

### 议题

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **议题** 并找到您的议题。
1. 转到 **活动** 部分。
1. 对于 **排序或筛选**，选择 **显示所有活动**。

### 合并请求

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **合并请求** 并找到您的合并请求。
1. 转到 **活动** 部分。
1. 对于 **排序或筛选**，选择 **显示所有活动**。

<!--
## Related topics

- [Notes API](../../api/notes.md)
-->
