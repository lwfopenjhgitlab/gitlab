---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

<a id="squash-and-merge"></a>

# 压缩和合并 **(FREE)**

当你在一个特性分支上工作时，你经常会创建小的、自包含的提交。这些小提交有助于描述构建功能的过程，但可能会在功能完成后弄乱您的 Git 历史记录。完成功能后，您可以合并这些提交，并使用 *压缩和合并* 策略确保 Git 仓库中的合并历史更加清晰。

- 小提交被连接在一起，使得[恢复更改的所有部分](revert_changes.md)变得更简单。
- 当单个提交合并到目标分支时，它会保留完整的提交历史。
- 您的基础分支保持干净，并包含有意义的提交消息。

每次分支合并到您的基础分支时，最多添加两个提交：

- 通过压缩来自分支的提交创建的单个提交。
- 合并提交，除非您在项目中[启用了快进合并](methods/index.md#fast-forward-merge)。快进式合并禁用合并提交和压缩。

默认情况下，压缩提交包含以下元数据：

- 消息：压缩提交的描述，或自定义消息
- 作者：创建合并请求的用户
- 提交者：发起压缩的用户

项目所有者可以为所有压缩提交和合并提交，创建新的默认消息<!--[创建新的默认消息](commit_templates.md)-->。

## 为合并请求设置默认压缩选项

有权创建或编辑合并请求的用户可以为合并请求设置默认的压缩选项。

先决条件：

- 您的项目必须[配置](#configure-squash-options-for-a-project)为允许或鼓励压缩。

1. 转到合并请求并选择 **编辑**。
1. 选中或清除 **此项目不允许在接受合并请求时压缩提交** 复选框。
1. 选择 **保存修改**。

## 合并请求中的压缩提交

如果您的项目允许您为合并请求选择压缩选项，则在合并过程中压缩提交：

1. 转到合并请求，然后滚动到包含 **合并** 按钮的合并请求报告部分。
1. 确保选中 **压缩提交** 复选框。 如果项目的压缩选项设置为 **不允许** 或 **必须**，则不会显示此复选框。
1. 可选。要修改压缩提交消息或合并提交消息（取决于您的项目配置），请选择 **修改提交消息**。
1. 当合并请求准备好合并时，选择 **合并**。

<a id="configure-squash-options-for-a-project"></a>"

## 为项目配置压缩选项

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/17613) in GitLab 13.2 [with a flag](../../../administration/feature_flags.md) named `squash_options`, disabled by default.
> - [Enabled on GitLab.com and self-managed by default](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/39382) in GitLab 13.3.
> - [Generally available](https://gitlab.com/gitlab-org/gitlab/-/issues/232536) in GitLab 13.8. Feature flag `squash_options` removed.
-->

先决条件：

- 您必须至少具有此项目的维护者角色。

为项目中的所有合并请求配置默认压缩行为：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的项目。
1. 选择 **设置 > 合并请求**。
1. 在 **合并时压缩提交** 部分中，选择：
    - **不允许**：从不执行压缩，并且不显示该选项。
    - **允许**：允许压缩，但默认取消选择。
    - **鼓励**：默认情况下允许并选择压缩，但可以禁用。
    - **必须**：始终执行压缩。虽然合并请求显示压缩选项，但用户无法更改。
1. 选择 **保存更改**。

<!--
## Related topics

- [Commit message templates](commit_templates.md)
- [Fast-forward merges](fast_forward_merge.md)
-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
