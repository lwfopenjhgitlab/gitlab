---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: index, reference
---

# 合并请求中的提交选项卡 **(FREE)**

合并请求中的 **提交** 选项卡显示提交到合并请求所基于的 Git 分支的顺序列表。在此页面中，您可以在需要[拣选更改](cherry_pick_changes.md)时查看完整的提交消息并复制提交的 SHA。

## 合并请求提交导航

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/18140) in GitLab 13.0.
-->

在合并请求中的提交之间无缝导航：

1. 选择 **提交** 选项卡。
1. 选择一个提交，在单提交视图中打开它。
1. 通过以下任一方式浏览提交：

   - 选择选项卡按钮下方的 **Prev** 和 **Next** 按钮。
   - 使用 <kbd>X</kbd> 和 <kbd>C</kbd> 键盘快捷键。

![Merge requests commit navigation](img/commit_nav_v13_11.png)

## 在上下文中查看合并请求提交

> - 引入于 13.12 版本，使用名为 `context_commits` 的功能标志，默认启用。
> - 于 14.8 版本适用于 SaaS 版。
> - 普遍可用于 14.9 版本。功能标志 `context_commits` 移除。

在审核合并请求时，此功能有助于了解所做更改的更多上下文，包括未更改文件中未更改的行，以及更改所基于的已合并的先前提交。

将先前合并的提交添加到合并请求，来获取更多上下文：

1. 转到您的合并请求。
1. 选择 **提交** 选项卡。
1. 滚动到提交列表的末尾，然后选择 **添加先前合并的提交**：

   ![Add previously merged commits button](img/add_previously_merged_commits_button_v14_1.png)

1. 选择您要添加的提交。
1. 选择 **保存修改**。

要查看对先前合并的提交所做的更改：

1. 在您的合并请求中，选择 **变更** 选项卡。
1. 滚动到 **(file-tree)** **比较** 并选择 **先前合并的提交**：

   ![Previously merged commits](img/previously_merged_commits_v14_1.png)
