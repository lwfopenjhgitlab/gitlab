---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, concepts
disqus_identifier: 'https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html'
---

# 合并请求批准 **(FREE)**

<!--
> Redesign [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/1979) in [GitLab Premium](https://about.gitlab.com/pricing/) 11.8 and [feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/10685) in 12.0.
-->

您可以配置合并请求，以便在合并之前必须批准它们。虽然[标准版](https://about.gitlab.cn/pricing/)允许所有拥有开发者或更高权限的用户批准合并请求，这些批准是[可选的](#可选批准)。
[专业版](https://about.gitlab.cn/pricing/)和[旗舰版](https://about.gitlab.cn/pricing/)提供额外灵活性：

- 在工作可以合并之前，创建有关审核者数量和类型的必需[规则](rules.md)。
- 指定充当特定文件的<!--[代码所有者](../../code_owners.md)-->代码所有者的用户列表，并需要他们的批准才能合并工作。

您可以在每个项目的基础上，或[群组级别](../../../group/manage.md#group-merge-request-approval-settings)上，配置合并请求批准。专业版和旗舰版私有化部署实例的管理员还可以为整个实例配置批准。

## 批准如何工作

使用[合并请求批准规则](rules.md)，您可以在工作合并到您的项目之前设置所需的最小批准次数。您还可以扩展这些规则来定义哪些类型的用户可以批准工作。您可以创建的一些规则示例包括：

- 具有特定权限的用户始终可以批准工作。
- <!--[代码所有者](../../code_owners.md)-->代码所有者可以批准他们拥有的文件的工作。
- 具有特定权限的用户可以批准工作，即使他们没有合并权限。
- 可以允许或拒绝具有特定权限的用户覆盖特定合并请求的批准规则。

您还可以配置：

- 额外的[合并请求批准设置](settings.md)，以更好地控制您的项目所需的监督和安全级别。
- 通过 GitLab UI 或合并请求批准 API<!--[合并请求批准 API](../../../../api/merge_request_approvals.md)--> 合并请求批准规则和设置。

## 批准合并请求

当[合资格的批准人](rules.md#合资格的核准人)访问开放合并请求时，系统在合并请求正文后显示以下按钮之一：

- **批准**：合并请求尚未获得所需数量的批准。
- **额外批准**：合并请求具有所需数量的批准。
- **撤销批准**：查看合并请求的用户已经批准了合并请求。

在向合并请求添加评论时，合资格的批准人还可以使用 `/approve` [快速操作](../../../project/quick_actions.md)。在 13.10 及更高版本中，如果用户批准合并请求并显示在审核者列表中，则会在其名称旁边显示绿色复选标记 (**{check-circle-filled}**)。

在合并请求收到您配置的[批准数量和类型](rules.md)后，它可以合并，除非它因其他原因被阻止。合并请求可能会被其他问题阻塞，例如合并冲突、<!--[待讨论](../../../discussions/index.md#prevent-merge-unless-all-threads-are-resolved)-->，或[失败的 CI/CD 流水线](../merge_when_pipeline_succeeds.md)。

要阻止合并请求作者批准他们自己的合并请求，请在您的项目设置中启用[**阻止作者批准**](settings.md#阻止作者批准)。

如果启用[批准规则覆盖](settings.md#阻止在合并请求中编辑批准规则)，则在更改默认批准规则之前创建的合并请求不受影响。
唯一的例外是对规则的[目标分支](rules.md#受保护分支的批准)的更改。

## 可选批准

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/27426) in GitLab 13.2.
-->

极狐GitLab 允许所有具有开发者或更高权限的用户批准合并请求。标准版中的批准是可选的，并且不会阻止合并请求在未经批准的情况下合并。

## 必须批准 **(PREMIUM)**

> 于 13.9 版本移动到专业版。

必须批准是按您指定的用户数量和类型强制执行代码审核。
未经批准，工作无法合并。必须批准支持多个用例：

- 强制审核合并到仓库中的所有代码。
- 通过[批准规则](rules.md)为给定的拟议代码更改指定审核者，以及最少数量的审核者。
- 为所有提议的代码更改指定审核者的类别，例如后端、前端、质量保证或数据库。
- 使用更改文件的代码所有者<!--[更改文件的代码所有者](rules.md#code-owners-as-eligible-approvers)-->来确定谁应该审查工作。
- 需要在合并导致测试覆盖率下降的代码之前获得批准<!--[在合并导致测试覆盖率下降的代码之前获得批准](../../../../ci/pipelines/settings.md#coverage-check-approval-rule)-->
- 旗舰版用户在合并可能引入漏洞的代码之前，也可以要求安全团队批准<!--[需要安全团队的批准](../../../application_security/index.md#security-approvals-in-merge-requests)-->。

## 无效规则

> - 引入于 15.1 版本。
> - 变更于 15.11 版本，[功能标志](../../../../administration/feature_flags.md)为 `invalid_scan_result_policy_prevents_merge`。默认禁用。

FLAG:
在私有化部署版中，此功能默认不可用。要使其在每个项目或整个实例中可用，需要管理员[启用功能标志](../../../../administration/feature_flags.md) `invalid_scan_result_policy_prevents_merge`。在 SaaS 中，此功能可用，但只能由 SaaS 管理员配置。

每当无法满足批准规则时，该规则将显示为 `Invalid`，适用于以下条件：

- 唯一符合条件的批准者是合并请求的作者。
- 没有将符合条件的批准人（群组或用户）指派给批准规则。
- 所需批准的数量多于合资格批准人的数量。

这些规则会被自动批准以免阻止它们各自的合并请求，除非它们是通过安全策略创建的。

通过安全策略创建的无效批准规则以 **(!) 需要操作** 显示，并且不会自动批准，从而阻止它们各自的合并请求。

<!--
## Related links

- [Merge request approvals API](../../../../api/merge_request_approvals.md)
- [Instance-level approval rules](../../../admin_area/merge_requests_approvals.md) for self-managed installations
-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, for example, `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
