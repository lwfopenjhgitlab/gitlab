---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, concepts
---

# 合并请求批准规则 **(PREMIUM)**

批准规则定义合并请求在合并之前必须接收多少[核准](index.md)，以及哪些用户应该进行批准。它们可以与[代码所有者](#code-owners-as-eligible-approvers)一起使用，以确保维护该功能的群组和负责特定监督领域的任何群组都评审更改。

您可以按以下定义批准规则：

- [项目默认](#添加批准规则)
- [每个合并请求](#编辑或覆盖合并请求批准规则)
- 实例级别<!--[实例级别](../../../admin_area/merge_requests_approvals.md)-->

如果您未定义[默认批准规则](#添加批准规则)，则任何用户都可以核准合并请求。即使您没有定义规则，您仍然可以在项目的设置中强制执行一个[所需的最小数量](settings.md)。

您可以定义单个规则来，也可以选择[多个批准规则](#添加多个批准规则)。

针对不同项目的合并请求，例如从派生项目到上游项目，使用来自目标（上游）项目而不是源（派生）的默认批准规则。

<a id="add-an-approval-rule"></a>

## 添加批准规则

要添加合并请求批准规则：

1. 转到您的项目并选择 **设置 > 合并请求**。
1. 在 **合并请求批准** 部分，滚动到 **批准规则**。
1. 选择 **添加批准规则**。
1. 添加一个人类可读的 **规则名称**。
1. 在 **需要核准** 中设置需要的核准数量。`0` 的值使[规则可选](#配置可选批准规则)，任何大于 `0` 的数字都会创建一个必需的规则。
1. 要将用户或群组添加为核准人，请搜索[有资格核准](#合资格的核准人)的用户或群组，然后选择 **添加**。系统根据合并请求更改的文件的先前作者建议核准人。

<!--
     NOTE:
     On GitLab.com, you can add a group as an approver if you're a member of that group or the
     group is public.
-->

1. 选择 **添加批准规则**。

专业版和更高级别的用户可以创建[额外的批准规则](#添加多个批准规则)。

您的批准规则覆盖配置，决定了新规则是否应用于现有合并请求：

- 如果允许[批准规则覆盖](settings.md#阻止在合并请求中编辑批准规则)，则对这些默认规则的更改不会应用于现有合并请求，但对规则中，[目标分支的更改除外](#受保护分支的批准)。
- 如果不允许批准规则覆盖，则对默认规则的所有更改都将应用于现有合并请求。在批准规则覆盖允许的情况下，之前手动[覆盖](#编辑或覆盖合并请求批准规则)的任何批准规则都不会被修改。

## 编辑批准规则

要编辑合并请求批准规则：

1. 转到您的项目并选择 **设置 > 合并请求**。
1. 在 **合并请求批准** 部分，滚动到 **批准规则**。
1. 选择要编辑的规则旁边的**编辑**。
1. （可选）更改 **规则名称**。
1. 在 **需要核准** 中设置需要的核准数量。最小值为 `0`。
1. 根据需要添加或删除合资格的核准人：
    - *要将用户或群组添加为核准人，*搜索[合资格](#合资格的核准人)的用户或群组，然后选择 **添加**。

<!--
     NOTE:
     On GitLab.com, you can add a group as an approver if you're a member of that group or the
     group is public.
-->

   - *要删除用户或群组，* 确定要删除的群组或用户，然后选择**{remove}** **删除**。
1. 选择 **更新批准规则**。

## 添加多个批准规则

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/1979) in GitLab Premium 11.10.
-->

在专业版和更高级别中，您可以对合并请求强制执行多个批准规则，并为项目强制执行多个默认批准规则。如果您的级别支持多个默认规则：

- 当[添加](#添加批准规则)或[编辑](#编辑批准规则)项目的批准规则时，会显示 **添加批准规则** 按钮，即使在规则被定义之后。
- [在合并请求中](#编辑或覆盖合并请求批准规则)，当编辑或覆盖多个批准规则时，即使在定义了规则之后，系统也会显示 **添加批准规则** 按钮。

当[合资格的核准人](#合资格的核准人)核准合并请求时，核准人所属的所有规则的剩余批准数量（**批准** 列）会减少：

![Approvals premium merge request widget](img/approvals_premium_mr_widget_v13_3.png)

<a id="eligible-approvers"></a>

## 合资格的核准人

> 引入于 13.3 版本, 当符合条件的审核人对合并请求发表评论时，它会出现在审核部件的 **已评论** 列中。

要成为项目的核准人，用户必须是以下一项或多项的成员：

- 该项目。
- 项目的直接父级[群组](#群组核准人)。
- 可以通过<!--[share](../../members/share_project_with_groups.md)-->访问项目的群组。
- [添加为核准人的群组](#群组核准人)。

如果以下用户拥有开发者或更高权限，他们可以核准合并请求：

- 在项目或合并请求级别添加为核准人的用户。
- 作为合并请求中更改文件的[代码所有者](#代码所有者作为合资格的核准人)的用户。

为了显示谁参与了合并请求审核，合并请求中的核准部件会显示 **已评论** 列，此列列出了对合并请求发表评论的合资格核准人，可以帮助作者和审核者确定与合并请求内容相关的问题联系谁。

如果所需的核准数大于分配的核准人数，则项目中具有开发者权限或更高级别的其他用户的核准计入满足所需的核准数，即使用户未明确列在批准规则中。

<a id="group-approvers"></a>

### 群组核准人

您可以将一组用户添加为核准人，但这些用户仅当拥有该群组的**直接成员资格**时才算作核准人。继承的成员不计算在内。群组核准人仅限于对项目具有共享访问权限的群组。

用户在核准人群组中的成员资格会通过以下方式影响他们的个人核准能力：

- 已经是群组核准人的一部分，后来作为个人核准人添加的用户算作一个核准人，而不是两个。
- 默认情况下，合并请求作者在他们自己的合并请求中不计为合资格的核准人。要更改，请禁用 [**阻止作者批准**](settings.md#阻止作者批准) 项目设置。
- 合并请求的提交者可以批准合并请求。要更改，请启用 [**阻止添加提交的用户批准**](settings.md#阻止添加提交的用户批准) 项目设置。

### 代码所有者作为合资格的核准人

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/7933) in GitLab 11.5.
-->

> - 于 13.9 版本移动到专业版。

如果您将代码所有者<!--[代码所有者](../../code_owners.md)-->添加到您的仓库，文件的所有者将成为项目中合资格的核准人。要启用此合并请求批准规则：

1. 转到您的项目并选择 **设置 > 合并请求**。
1. 在 **合并请求批准** 部分，滚动到 **批准规则**。
1. 找到 **所有符合条件的用户** 规则，并选择所需的批准数量：

   ![MR approvals by Code Owners](img/mr_approvals_by_code_owners_v15_2.png)

您还可以为受保护的分支要求代码所有者核准<!--[要求代码所有者核准](../../protected_branches.md#require-code-owner-approval-on-a-protected-branch)-->。

## 合并请求批准职责分离

> - 引入于 13.4 版本
> - 于 13.9 版本引入专业版

您可能需要授予具有报告者权限的用户核准合并请求的权限，然后才能合并到受保护的分支。一些用户（如经理）可能不需要推送或合并代码的权限，但仍需要对提议的工作进行监督。要为这些用户启用核准权限而不授予他们推送访问权限：

1. 创建受保护的分支<!--[创建受保护的分支](../../protected_branches.md)-->。
1. 创建新群组<!--[创建新群组](../../../group/index.md#创建一个群组)-->。
1. 将用户加入群组，为用户选择报告者角色。
1. 基于报告者角色，与您的群组共享项目<!--[与您的群组共享项目](../../members/share_project_with_groups.md#sharing-a-project-with-a-group-of-users)-->。
1. 转到您的项目并选择 **设置 > 合并请求**。
1. 在 **合并请求批准** 部分，滚动到 **批准规则**。
   - 对于新规则，选择 **添加批准规则** 并定位受保护的分支。
   - 对于现有规则，选择 **编辑** 并定位受保护的分支。
1. 添加群组到权限列表。

   ![Update approval rule](img/update_approval_rule_v13_10.png)

### 编辑或覆盖合并请求批准规则

默认情况下，合并请求作者（或具有足够权限的用户）可以编辑合并请求中列出的批准规则。在编辑合并请求的批准规则时，您可以添加或删除核准人：

1. 在合并请求中，找到 **批准规则部分**。
1. 创建新的合并请求时，滚动到 **批准规则** 部分，然后在选择 **创建合并请求** 之前添加或删除所需的批准规则。
1. 查看现有合并请求时：
    1. 选择 **编辑**。
    1. 滚动到 **批准规则** 部分。
    1. 添加或删除所需的批准规则。
    1. 选择 **保存更改**。

管理员可以更改[合并请求审批设置](settings.md#阻止在合并请求中编辑批准规则) 以防止用户覆盖合并请求的批准规则。

## 配置可选批准规则
对于非必需核准的项目，合并请求核准可以是可选的。要将批准规则设为可选：

- 当您[创建或编辑规则](#编辑批准规则)时，将 **需要核准** 设置为 `0`。
- 使用合并请求核准 API<!--[合并请求核准 API](../../../../api/merge_request_approvals.md#update-merge-request-level-rule)--> 将 `approvals_required` 属性设置为 `0`。

## 受保护分支的批准

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/460) in GitLab Premium 12.8.
-->
> - **所有受保护分支**的目标分支选项引入于 15.3 版本。

批准规则通常仅与特定分支相关，例如您的[默认分支](../../repository/branches/default.md)。 配置一个
部分分行审批规则：

1. [创建审批规则](#add-an-approval-rule)。
1. 转到您的项目并选择 **设置 > 合并请求**。
1. 在 **合并请求批准** 部分，滚动到 **批准规则**。
1. 选择一个 **目标分支**：
    - 要将规则应用于所有分支，请选择 **所有分支**。
    - 要将规则应用于所有受保护的分支，请选择 **所有受保护的分支**（15.3 及更高版本）。
    - 要将规则应用于特定分支，请从列表中选择它。

## 代码覆盖率检查批准

如果合并请求会减少代码测试覆盖率，您可以要求特定批准。

有关详细信息，请参阅[覆盖率检查批准规则](../../../../ci/testing/code_coverage.md#coverage-check-approval-rule)。

## 安全批准 **(ULTIMATE)**

> 引入于 15.0 版本。

您可以使用[扫描结果策略](../../../application_security/policies/scan-result-policies.md#scan-result-policy-editor)，基于合并请求和默认分支中的漏洞状态定义安全批准。
每个安全策略的详细信息显示在合并请求配置的安全批准部分中。

安全批准规则适用于所有合并请求，直到流水线完成。安全批准规则的应用可防止用户在安全扫描运行之前合并代码。流水线完成后，将检查安全批准规则，确定是否仍需要安全批准。

![Security Approvals](img/security_approvals_v15_0.png)

这些策略都是在[安全策略编辑器](../../../application_security/policies/index.md#policy-editor)中创建和编辑的。


<!--
1. To enable this configuration, read
   [Code Owner's approvals for protected branches](../../protected_branches.md#require-code-owner-approval-on-a-protected-branch).
-->

<!--
## Troubleshooting

### Approval rule name can't be blank

As a workaround for this validation error, you can delete the approval rule through
the API.

1. [GET a project-level rule](../../../../api/merge_request_approvals.md#get-a-single-project-level-rule).
1. [DELETE the rule](../../../../api/merge_request_approvals.md#delete-project-level-rule).

For more information about this validation error, read
[issue 285129](https://gitlab.com/gitlab-org/gitlab/-/issues/285129).
-->

