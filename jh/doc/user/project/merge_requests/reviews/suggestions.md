---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: index, reference
---

# 建议更改 **(FREE)**

> - 建议的自定义提交消息引入于 13.9 版本，功能标志为 `suggestions_custom_commit`，默认禁用。
> - 一般可用于 13.10 版本。删除功能标志 `suggestions_custom_commit`。

审核者可以在合并请求差异行中使用 Markdown 语法建议代码更改。
合并请求作者（或具有适当角色的其他用户）可以应用来自 UI 的任何或所有建议。应用建议会向合并请求添加提交，提交作者为提出更改建议的用户。

## 创建建议

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **合并请求** 并找到您的合并请求。
1. 在二级菜单上，选择 **变更**。
1. 找到您要更改的代码行。将鼠标悬停在行号上，然后选择 **向此行添加评论** (**{comment}**)。
1. 在评论工具栏中，选择 **插入建议** (**{doc-code}**)。极狐GitLab 会在您的评论中插入一个预填入的代码块，如下所示：

   ````markdown
   ```suggestion:-0+0
   The content of the line you selected is shown here.
   ```
   ````

1. 编辑预填入的代码块，添加您的建议。
1. 选择 **启动评审** 或 **添加到评审** 将您的评论添加到[审核](index.md)，或选择 **立即添加评论** 立即将评论添加到主题。

## 应用建议

合并请求作者可以直接从合并请求中应用建议的更改：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **合并请求** 并找到您的合并请求。
1. 找到包含您要应用的建议的评论。
    - 要单独应用建议，请选择 **应用建议**。
    - 要在一次提交中应用多个建议，请选择 **将建议加入批量处理**。
1. 可选。提供自定义提交消息来描述您的更改。如果您不提供自定义消息，则使用默认提交消息。
1. 选择 **应用**。

应用建议后：

- 该建议被标记为 **已应用**。
- 评论主题已解决。
- 极狐GitLab 使用更改创建新的提交。
- 如果用户具有开发者角色，极狐GitLab 会将建议的更改直接推送到合并请求分支所在的代码库中。

## 多行建议

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/53310) in GitLab 11.10.
-->

审核者还可以通过调整范围偏移量，或选择并拖动选择到所有相关行号，在合并请求差异主题中，使用单个建议对多行提出更改建议。偏移量与 diff 主题的位置相关，并指定在应用建议时要替换的范围。

![Multi-line suggestion syntax](img/multi-line-suggestion-syntax.png)

在前面的示例中，建议涵盖注释行上方三行和下方四行。应用后，它将用建议的更改替换注释行的上方 3 行到下方 4 行。

![Multi-line suggestion preview](img/multi-line-suggestion-preview.png)

NOTE:
对多行的建议限制为注释差异行的上方 100 行到下方 100 行。允许每个建议最多更改 200 行。

## 嵌套在建议中的代码块

如果您需要提出涉及代码块<!--[围栏代码块](../../../markdown.md#code-spans-and-blocks)-->的建议，请将您的建议用四个反引号包裹起来，而不是通常的三个。

`````markdown
````suggestion:-0+2
```shell
git config --global receive.advertisepushoptions true
```
````
`````

![Output of a comment with a suggestion with a fenced code block](img/suggestion_code_block_output_v12_8.png)

<a id="configure-the-commit-message-for-applied-suggestions"></a>

## 为应用的建议配置提交消息

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/13086) in GitLab 12.7.
-->

极狐GitLab 在应用建议时使用默认提交消息：`Apply %{suggestions_count} 建议到 %{files_count} 文件`

例如，假设用户将 3 个建议应用于 2 个不同的文件，默认提交消息是：**将 3 个建议应用于 2 个文件**

可以自定义这些提交消息以遵循您可能拥有的任何准则。
为此，请展开项目 **通用** 设置中的 **合并请求** 选项卡并更改 **合并推荐** 文本：

![Custom commit message for applied suggestions](img/suggestions_custom_commit_messages_v14_7.png)

除了静态文本，您还可以使用以下变量：

| 变量               | 描述 | 输出示例 |
|------------------------|-------------|----------------|
| `%{branch_name}`       | 建议所应用的分支的名称。 | `my-feature-branch` |
| `%{files_count}`       | 已应用建议的文件数。 | **2** |
| `%{file_paths}`        | 已应用文件建议的路径。 路径用逗号分隔。 | `docs/index.md, docs/about.md` |
| `%{project_path}`      | 项目路径。 | `my-group/my-project` |
| `%{project_name}`      | 项目的人类可读名称。 | **My Project** |
| `%{suggestions_count}` | 已应用的建议数。 | **3** |
| `%{username}`          | 应用建议的用户的用户名。  | `user_1` |
| `%{user_full_name}`    | 应用建议的用户的全名。 | **User 1** |

例如，要自定义提交消息以输出 **Addresses user_1's review**，请将自定义文本设置为 `Addresses %{username}'s review`。

<!--
NOTE:
Custom commit messages for each applied suggestion is
introduced by [#25381](https://gitlab.com/gitlab-org/gitlab/-/issues/25381).
-->

对于从派生项目创建的合并请求，极狐GitLab 使用目标项目中定义的模板。

## 批量建议

> - 引入于 13.1 版本作为 alpha 功能，在功能标志后默认禁用
> - 于 13.2 版本默认启用。
> - 功能标志移除于 13.11 版本。
> - 自定义提交消息的批处理建议引入于 14.4 版本。

您可以一次应用多个建议，以减少添加到您的分支的提交数量，以满足您的审核者的请求。

1. 要开始通过单个提交应用一批建议，请选择 **将建议加入批量处理**：

   ![A code change suggestion displayed, with the button to add the suggestion to a batch highlighted.](img/add_first_suggestion_to_batch_v13_1.jpg "Add a suggestion to a batch")

1. 根据需要向批次添加尽可能多的其他建议：

   ![A code change suggestion displayed, with the button to add an additional suggestion to a batch highlighted.](img/add_another_suggestion_to_batch_v13_1.jpg "Add another suggestion to a batch")

1. 要删除建议，请选择 **从批处理中移除**：

   ![A code change suggestion displayed, with the button to remove that suggestion from its batch highlighted.](img/remove_suggestion_from_batch_v13_1.jpg "Remove a suggestion from a batch")

1. 根据您的喜好添加所有建议后，准备就绪后，选择**应用建议**。您可以选择[批量建议](#批量建议)（14.4 及更高版本）指定自定义提交消息来描述您的更改。如果未指定，则使用默认提交消息。

   ![A code change suggestion displayed, with the button to apply the batch of suggestions highlighted.](img/apply_batch_of_suggestions_v13_1.jpg "Apply a batch of suggestions")

WARNING:
从多个作者应用的建议会创建由应用建议的用户创作的提交。

<!--
## Related links

- [Suggestions API](../../../../api/suggestions.md)
-->
