---
redirect_to: 'conflicts.md'
remove_date: '2022-01-26'
---

此文档已移动到[另一个位置](conflicts.md)。

<!-- This redirect file can be deleted after <2022-01-26>. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/#move-or-rename-a-page -->