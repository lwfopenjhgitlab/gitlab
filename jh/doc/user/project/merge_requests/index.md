---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: index, reference
---

# 合并请求 **(FREE)**

合并请求 (MR) 是源代码更改进入到分支中的方式。
当您打开合并请求时，您可以在合并之前对代码更改进行可视化和协作。
合并请求包括：

- 请求的描述。
- 代码更改和内联代码审查。
- 有关 CI/CD 流水线的信息。
- 讨论主题的评论部分。
- 提交列表。

## 创建合并请求

了解[创建合并请求](creating_merge_requests.md)的各种方法。

### 使用合并请求模板

当您创建合并请求时，极狐GitLab 会检查是否存在[描述模板](../description_templates.md)，可以将数据添加到您的合并请求中。
极狐GitLab 按从 1 到 5 的顺序检查以下位置，并将找到的第一个模板应用于您的合并请求：

| 名称 | 项目 UI<br>设置 | 群组<br>`default.md` | 实例<br>`default.md` | 项目<br>`default.md` | 无模板 |
| :-- | :--: | :--: | :--: | :--: | :--: |
| 标准提交消息 | 1 | 2 | 3 | 4 | 5 |
| 带有[议题关闭样式](../issues/managing_issues.md#closing-issues-automatically)（例如 `Closes #1234`）的提交消息 | 1 | 2 | 3 | 4 | 5 \* |
| [已议题 ID 为前缀的](../repository/branches/index.md#prefix-branch-names-with-issue-numbers)分支名称（例如 `1234-example`） | 1 \* | 2 \* | 3 \* | 4 \* | 5 \* |

NOTE:
标有星号 (\*) 的事项还会附加一个[议题关闭样式](../issues/managing_issues.md#closing-issues-automatically)。

## 查看合并请求

您可以查看您的项目、群组或您自己的合并请求。

### 查看项目的合并请求

查看项目的所有合并请求：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **合并请求**。

或者，要使用[键盘快捷键](../../shortcuts.md)，请按 <kbd>g</kbd> + <kbd>m</kbd>。

### 查看群组中所有项目的合并请求

查看群组中所有项目的合并请求：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **合并请求**。

如果您的群组包含子组，此视图还会显示来自子组项目的合并请求。

## 查看指派给您的所有合并请求

查看指派给您的所有合并请求：

1. 在顶部栏上，将光标放在 **搜索** 框中。
1. 从下拉列表中，选择 **指派给我的合并请求**。

或者：

- 要使用[键盘快捷键](../../shortcuts.md)，请按 <kbd>Shift</kbd> + <kbd>m</kbd>。

或者：

1. 在顶部栏的右上角，选择 **{merge-request-open}** **合并请求**。
1. 从下拉列表中，选择 **分配给您的**。

## 过滤合并请求列表

过滤合并请求列表：

1. 在合并请求列表上方，选择 **搜索或过滤结果...**。
1. 在出现的下拉列表中，选择您要过滤的属性。
1. 选择或键入用于过滤属性的运算符。可以使用以下运算符：
   - `=`：是
   - `!=`：不是
1. 输入文本，过滤属性。您可以使用 **None** 或 **Any** 过滤某些属性。
1. 重复此过程可以按多个属性进行过滤。多个属性由 `AND` 逻辑连接。

<!--
GitLab displays the results on-screen, but you can also
[retrieve them as an RSS feed](../../search/index.md#retrieve-search-results-as-feed).
-->

### 按 ID 过滤合并请求

您可以过滤**合并请求**列表，按 ID 查找合并请求。

例如，输入 `#30`，仅返回合并请求 30。

### 按核准人过滤合并请求 **(PREMIUM)**

> 移动到专业版于 13.9 版本。

要过滤单个合格核准人（[代码所有者](../code_owners.md)）的合并请求，您可以键入（或从下拉列表中选择）**核准人**，并选择用户。

![Filter MRs by an approver](img/filter_approver_merge_requests_v14_6.png)

### 按“已核准”过滤合并请求 **(PREMIUM)**

> - 引入于 13.0 版本。
> - 移动到专业版于 13.9 版本。

要过滤已由特定个人批准的合并请求，您可以键入（或从下拉列表中选择）**已核准**，并选择用户。

![Filter MRs by approved by](img/filter_approved_by_merge_requests_v14_6.png)

### 按审核者过滤合并请求

> 引入于 13.7 版本。

要过滤特定个人的审核合并请求的请求，您可以键入（或从下拉列表中选择）**审核者**，并选择用户。

### 按环境或部署日期过滤合并请求

> 引入于 13.6 版本。

要按部署数据（例如环境或日期）过滤合并请求，您可以键入（或从下拉列表中选择）以下内容：

- 环境
- 部署前
- 部署后

NOTE:
使用[快进式合并方法](methods/index.md#fast-forward-merge)的项目不会返回结果，因为此方法不会创建合并提交。

按环境过滤时，下拉列表会显示您可以选择的所有环境：

![Filter MRs by their environment](img/filtering_merge_requests_by_environment_v14_6.png)

当通过 `Deployed-before` 或 `Deployed-after` 过滤时，日期指的是部署到环境（由合并提交触发）成功的时间。
您必须手动输入部署日期。部署日期使用 `YYYY-MM-DD` 格式，如果您希望同时指定日期和时间（`"YYYY-MM-DD HH:MM"`），则必须引用：

![Filter MRs by a deploy date](img/filtering_merge_requests_by_date_v14_6.png)

## 向合并请求添加更改

如果您有权向合并请求添加更改，则可以通过多种方式将更改添加到现有合并请求，具体取决于更改的复杂性以及您是否需要访问开发环境：

- 在浏览器中使用 <kbd>.</kbd> [键盘快捷键](../../shortcuts.md)，在 Web IDE 中编辑更改<!--[在 Web IDE 中编辑更改](../web_ide/index.md)-->。使用这种基于浏览器的方法来编辑多个文件，或者如果您对 Git 命令不满意。您不能从 Web IDE 运行测试。
- 如果您需要一个功能齐全的环境来编辑文件，然后运行测试，<!--[在 Gitpod 编辑更改](../../../integration/gitpod.md#launch-gitpod-in-gitlab)-->在 Gitpod 编辑更改。Gitpod 支持运行 GitLab Development Kit (GDK)。要使用 Gitpod，您必须在您的用户帐户中启用 Gitpod<!--[在您的用户帐户中启用 Gitpod](../../../integration/gitpod.md#enable-gitpod-in-your-user-settings)-->。
- 如果您熟悉 Git 和命令行，[从命令行推送更改](../../../gitlab-basics/start-using-git.md)。

<a id="assign-a-user-to-a-merge-request"></a>

## 将用户指派给合并请求

要将合并请求指派给其它人，请在合并请求的文本区域中使用 `/assign @user` [快速操作](../quick_actions.md#issues-merge-requests-and-epics)，或者：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **合并请求** 并找到您的合并请求。
1. 在右侧边栏上，展开右侧边栏并找到 **指派人** 部分。
1. 选择 **编辑**。
1. 搜索您要指派的用户，然后选择用户。

<!--
The merge request is added to the user's
[assigned merge request list](../../search/index.md#search-issues-and-merge-requests).
-->

### 指派多个用户 **(PREMIUM)**

> 移动到专业版于 13.9 版本。

如果多个人负责，极狐GitLab 允许合并请求具有多个指派人：

![multiple assignees for merge requests sidebar](img/multiple_assignees_for_merge_requests_sidebar.png)

要将多个指派人分配给合并请求，请在文本区域中使用 `/assign @user` [快速操作](../quick_actions.md#issues-merge-requests-and-epics)，或者：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **合并请求** 并找到您的合并请求。
1. 在右侧边栏上，展开右侧边栏并找到 **指派人** 部分。
1. 选择 **编辑**，然后从下拉列表中选择您要指派的所有用户。

要删除指派人，请从同一下拉列表中清除该用户。

## 关闭合并请求

如果您决定永久停止处理合并请求，建议您关闭合并请求而不是[删除它](#删除合并请求)。合并请求的作者和指派人，项目中具有开发者、维护者或拥有者角色的用户可以关闭项目中的合并请求：

1. 转到您要关闭的合并请求。
1. 滚动到页面底部的评论框。
1. 在评论框之后，选择 **关闭合并请求**。

极狐GitLab 关闭合并请求，但保留合并请求、其注释和任何相关流水线的记录。

### 删除合并请求

建议您关闭而不是删除合并请求。

WARNING:
您无法撤消对合并请求的删除。

要删除合并请求：

1. 以项目所有者角色的用户身份登录极狐GitLab。只有具有此角色的用户才能删除项目中的合并请求。
1. 前往您要删除的合并请求，然后选择 **编辑**。
1. 滚动到页面底部，然后选择 **删除合并请求**。

### 目标分支合并时更新合并请求 **(FREE SELF)**

> - 引入于 13.9 版本。
> - 在私有化部署版上禁用于 13.9 版本。
> - 在私有化部署版上启用于 13.10 版本。

合并请求通常关联在一起，一个合并请求取决于在另一个合并请求中添加或更改的代码。为了支持使单个合并请求保持较小，当目标分支合并到 `main` 时，系统最多可以更新四个开放的合并请求。例如：

- **合并请求 1**：将 `feature-alpha` 合并到 `main`。
- **合并请求 2**：将 `feature-beta` 合并到 `feature-alpha`。

如果这些合并请求同时开放，并且合并请求 1 (`feature-alpha`) 合并到 `main`，系统会将合并请求 2 的目标从`feature-alpha` 更新为 `main`。

具有互连内容更新的合并请求通常以下列方式之一处理：

- 合并请求 1 首先合并到 `main` 中。 然后将合并请求 2 重定向到 `main`。
- 合并请求 2 被合并到 `feature-alpha` 中。更新后的合并请求 1，现在包含 `feature-alpha` 和 `feature-beta` 的内容，被合并到 `main` 中。

此功能仅在合并请求合并时有效。合并后选择**删除源分支**，不会重新定位开放的合并请求。<!--This improvement is
[proposed as a follow-up](https://gitlab.com/gitlab-org/gitlab/-/issues/321559).-->

<a id="move-sidebar-actions"></a>

## 移动侧边栏操作

<!-- When the `moved_mr_sidebar` feature flag is removed, delete this topic and update the steps for these actions
like in https://gitlab.com/gitlab-org/gitlab/-/merge_requests/87727/diffs?diff_id=522279685#5d9afba799c4af9920dab533571d7abb8b9e9163 -->

> - 引入于 14.10 版本，[功能标志](../../../administration/feature_flags.md)为 `moved_mr_sidebar`。默认禁用。
> - 支持移动议题、事件和史诗的操作的功能引入于 16.0 版本。

<!--
FLAG:
On self-managed GitLab, by default this feature is not available. To make it available per project or for your entire instance, ask an administrator to [enable the feature flag](../../../administration/feature_flags.md) named `moved_mr_sidebar`.
On GitLab.com, this feature is not available.
-->

启用此功能标志后，您可以在右上角的 **合并请求操作** (**{ellipsis_v}**) 中找到以下操作：

- [通知](../../profile/notifications.md#edit-notification-settings-for-issues-merge-requests-and-epics)切换开关
- 将合并请求标记为就绪或 [draft](../merge_requests/drafts.md)
- 关闭合并请求
- [锁定讨论](../../discussions/index.md#prevent-comments-by-locking-the-discussion)
- 复制引用

在 16.0 及更高版本中，类似的操作菜单可用于议题、事件和史诗。

当禁用此功能标志时，这些操作位于右侧栏中。

## 合并请求工作流

对于在团队中工作的软件开发人员：

1. 您检出一个新分支，并通过合并请求提交您的更改。
1. 您从团队收集反馈。
1. 您使用代码质量报告<!--[代码质量报告](code_quality.md)-->处理实现优化代码。
1. 您使用 GitLab CI/CD 中的单元测试报告<!--[单元测试报告](../../../ci/unit_test_reports.md)-->验证您的更改。
1. 您避免使用许可证与您项目的许可证合规报告<!--[许可证合规报告](../../compliance/license_compliance/index.md)-->不兼容的依赖项。
1. 您向您的经理请求[核准](approvals/index.md)。
1. 您的经理：
   1. 使用他们的最终审核推送提交。
   1. [核准合并请求](approvals/index.md)。
   1. 设置为[流水线成功时合并](merge_when_pipeline_succeeds.md)。
1. 使用 GitLab CI 的手动作业<!--[手动作业](../../../ci/jobs/job_control.md#create-a-job-that-must-be-run-manually)-->将您的更改部署到生产环境中。
1. 您的实施已成功交付给您的客户。

对于为您公司的网站编写网页的 Web 开发人员：

1. 您检出一个新分支并通过合并请求提交一个新页面。
1. 您从审核者那里收集反馈。
1. 您可以使用 Review Apps<!--[Review Apps](../../../ci/review_apps/index.md)--> 预览您的更改。
1. 您要求您的网页设计师实现。
1. 您向您的经理请求[核准](approvals/index.md)。
1. 一旦获得核准，您的合并请求将被[压缩和合并](squash_and_merge.md)，并通过 GitLab Pages 部署到 staging。
1. 您的生产团队[拣选](cherry_pick_changes.md)合并提交到生产环境中。

## 过滤合并请求中的动态

> 引入于 15.11 版本，[功能标志](../../../administration/feature_flags.md)为 `mr_activity_filters`。默认禁用。

FLAG:
在私有化部署版中，此功能默认不可用。
要使其可用，询问管理员为单个用户或群组的用户[启用功能标志](../../../administration/feature_flags.md) `mr_activity_filters`。在 SaaS 版中，此功能不可用。

要了解合并请求的历史记录，您可以过滤其活动提要，仅向您显示与您相关的事项。

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **合并请求**。
1. 选择一个合并请求。
1. 滚动到 **动态**。
1. 在页面右侧，选择 **活动过滤器** 以显示过滤器选项。
   如果您之前选择了过滤器选项，此字段会显示您的选择摘要。
1. 选择您要查看的动态类型。选项包括：

   - 指派人和审核者
   - 批准
   - 评论
   - 提交和分支
   - 编辑
   - 标记
   - 锁定状态
   - 提及
   - 合并请求状态
   - 跟踪

1. 可选。选择 **排序** (**{sort-lowest}**) 来反转排序顺序。

您的选择在所有合并请求中持续存在。您还可以通过单击右侧的排序按钮来更改排序顺序。

<!--
## Related topics

- [Create a merge request](creating_merge_requests.md)
- [Review a merge request](reviews/index.md)
- [Authorization for merge requests](authorization_for_merge_requests.md)
- [Testing and reports](testing_and_reports_in_merge_requests.md)
- [GitLab keyboard shortcuts](../../shortcuts.md)
-->

## 故障排除

### 从 Rails 控制台变基合并请求 **(FREE SELF)**

除了 `/rebase` [快速操作](../quick_actions.md#issues-merge-requests-and-epics)之外，有权访问 [Rails 控制台](../../../administration/operations/rails_console.md)的用户可以从 Rails 控制台变基合并请求。将 `<username>`、`<namespace/project>` 和 `<iid>` 替换为适当的值：

WARNING:
无论运行是否正确，任何直接更改数据的命令都可能造成破坏。我们强烈建议在测试环境中运行它们，并准备好要恢复的实例备份，以防万一。

```ruby
u = User.find_by_username('<username>')
p = Project.find_by_full_path('<namespace/project>')
m = p.merge_requests.find_by(iid: <iid>)
MergeRequests::RebaseService.new(project: m.target_project, current_user: u).execute(m)
```

### 修复不正确的合并请求状态 **(FREE SELF)**

如果合并请求在合并更改后保持**开放**，则有权访问 [Rails 控制台](../../../administration/operations/rails_console.md)的用户可以更正合并请求的状态。将 `<username>`、`<namespace/project>` 和 `<iid>` 替换为适当的值：

WARNING:
无论运行是否正确，任何直接更改数据的命令都可能造成破坏。我们强烈建议在测试环境中运行它们，并准备好要恢复的实例备份，以防万一。

```ruby
u = User.find_by_username('<username>')
p = Project.find_by_full_path('<namespace/project>')
m = p.merge_requests.find_by(iid: <iid>)
MergeRequests::PostMergeService.new(project: p, current_user: u).execute(m)
```

针对具有未合并更改的合并请求运行此命令，会导致合并请求显示不正确的消息：`merged into <branch-name>`。

### 从 Rails 控制台关闭合并请求 **(FREE SELF)**

如果通过 UI 或 API 关闭合并请求不起作用，您可能需要在 [Rails 控制台会话](../../../administration/operations/rails_console.md#starting-a-rails-console-session)操作：

WARNING:
无论运行是否正确，任何直接更改数据的命令都可能造成破坏。我们强烈建议在测试环境中运行它们，并准备好要恢复的实例备份，以防万一。

```ruby
u = User.find_by_username('<username>')
p = Project.find_by_full_path('<namespace/project>')
m = p.merge_requests.find_by(iid: <iid>)
MergeRequests::CloseService.new(project: p, current_user: u).execute(m)
```

### 从 Rails 控制台删除合并请求 **(FREE SELF)**

如果通过 UI 或 API 删除合并请求不起作用，您可能需要在 [Rails 控制台会话](../../../administration/operations/rails_console.md#starting-a-rails-console-session)操作：

WARNING:
无论运行是否正确，任何直接更改数据的命令都可能造成破坏。我们强烈建议在测试环境中运行它们，并准备好要恢复的实例备份，以防万一。

```ruby
u = User.find_by_username('<username>')
p = Project.find_by_full_path('<namespace/project>')
m = p.merge_requests.find_by(iid: <iid>)
Issuable::DestroyService.new(project: m.project, current_user: u).execute(m)
```
