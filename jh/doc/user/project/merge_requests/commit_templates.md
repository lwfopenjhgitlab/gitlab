---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# 提交消息模板 **(FREE)**

> - 引入于 14.5 版本。
> - 压缩提交模板添加于 14.6 版本。

极狐GitLab 使用提交模板为特定类型的提交创建默认消息。这些模板鼓励提交消息遵循特定格式，或包含特定信息。用户可以在合并合并请求时覆盖这些模板。

提交模板使用的语法类似于[评论建议](reviews/suggestions.md#configure-the-commit-message-for-applied-suggestions)的语法。

## 配置提交模板

如果默认模板不包含您需要的信息，请更改项目的提交模板。

先决条件：

- 您必须至少具有项目的维护者角色。

操作步骤：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 合并请求**。
1. 根据您要创建的模板类型，滚动到[**合并提交消息模板**](#default-template-for-merge-commits)或[**压缩提交消息模板**](#default-template-for-squash-commits)。
1. 对于您想要的提交类型，输入您的默认消息。您可以同时使用静态文本和[变量](#supported-variables-in-commit-templates)。每个模板限制为最多 500 个字符，但将模板替换为数据后，最终消息可能会更长。
1. 选择**保存更改**。

<a id="default-template-for-merge-commits"></a>

## 合并提交的默认模板

```plaintext
Merge branch '%{source_branch}' into '%{target_branch}'

%{title}

%{issues}

See merge request %{reference}
```

<a id="default-template-for-squash-commits"></a>

## 压缩提交的默认模板

如果您已将项目配置为[合并时压缩提交](squash_and_merge.md)，系统会使用此模板创建一个压缩提交消息：

```plaintext
%{title}
```

<a id="supported-variables-in-commit-templates"></a>

## 提交模板中支持的变量

> - 引入于 14.5 版本。
> - `first_commit` 和 `first_multiline_commit` 变量添加于 14.6 版本。
> - `url`、`approved_by` 和 `merged_by` 变量添加于 14.7 版本。
> - `co_authored_by` 变量添加于 14.7 版本。
> - `all_commits` 变量添加于 14.9 版本。

提交消息模板支持以下变量：

| 变量 | 描述 | 输出示例 |
|----------|-------------|----------------|
| `%{source_branch}` | 被合并的分支的名称。 | `my-feature-branch` |
| `%{target_branch}` | 应用变更的分支的名称。 | `main` |
| `%{title}`         | 合并请求的标题。 | `Fix tests and translations` |
| `%{issues}`        | 带有短语 `Closes <issue numbers>` 的字符串，包含合并请求描述中提到的与[议题关闭 pattern](../issues/managing_issues.md#closure-issues-automatically) 匹配的所有议题。如果没有提及任何议题，则为空。 | `Closes #465, #190 and #400` |
| `%{description}`   | 合并请求的描述。 | `Merge request description.`<br>`Can be multiline.` |
| `%{reference}`     | 引用合并请求。 | `group-name/project-name!72359` |
| `%{first_commit}`  | 合并请求差异中第一次提交的完整消息。 | `Update README.md` |
| `%{first_multiline_commit}` | 第一次提交的完整消息，此提交不是合并提交并且在消息正文中包含多行。如果所有提交都不是多行的，则为合并请求标题。 | `Update README.md`<br><br>`Improved project description in readme file.` |
| `%{url}`           | 合并请求的完整 URL。 | `https://gitlab.com/gitlab-org/gitlab/-/merge_requests/1` |
| `%{approved_by}`   | 合并请求核准人的行分隔列表。 | `Approved-by: Sidney Jones <sjones@example.com>` <br> `Approved-by: Zhang Wei <zwei@example.com>` |
| `%{merged_by}`     | 合并合并请求的用户。 | `Alex Garcia <agarcia@example.com>` |
| `%{co_authored_by}` |`Co-authored-by` Git 提交 trailer 格式的提交作者的姓名和电子邮件。仅限于合并请求中 100 个最近提交的作者。 | `Co-authored-by: Zane Doe <zdoe@example.com>` <br> `Co-authored-by: Blake Smith <bsmith@example.com>` |
| `%{all_commits}`   | 来自合并请求中所有提交的消息。限于 100 个最近的提交。跳过超过 100KiB 的提交正文并合并提交消息。 | `* Feature introduced` <br><br> `This commit implements feature` <br> `Changelog:added` <br><br> `* Bug fixed` <br><br> `* Documentation improved` <br><br>`This commit introduced better docs.`|

删除仅包含空变量的任何行。如果要删除的行前后都有一个空行，则前面的空行也将被删除。

在开放的合并请求上编辑提交消息后，系统再次自动更新提交消息。
要将提交消息恢复到项目模板，请重新加载页面。

## 相关文档

- [压缩和合并](squash_and_merge.md)。
