---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: index, reference
---

# 合并请求部件 **(FREE)**

合并请求的**概览**页面显示来自对您的合并请求执行操作的服务的状态更新。所有订阅级别都显示一个小部件区域，但该区域的内容取决于您的订阅级别和您为项目配置的服务。

## 流水线信息

如果您在项目中设置了[极狐GitLab CI/CD](../../../ci/index.md)，则[合并请求](index.md)会在小部件区域中显示流水线信息**概览**选项卡：

- 合并前和合并后流水线，以及环境信息（如果有）。
- 正在进行哪些部署。

如果应用程序成功部署到[环境](../../../ci/environments/index.md)，则会显示已部署的环境和 Review App 的链接。

NOTE:
当流水线在合并请求中失败但仍可以合并时，**合并** 按钮显示为红色。

## 合并后流水线状态

当合并请求被合并时，您可以看到合并请求被合并到的分支的合并后流水线状态。例如，当合并请求被合并到[默认分支](../repository/branches/default.md)中，然后触发部署到 staging 环境。

显示正在进行的部署，以及环境的状态（正在部署或已部署）。如果这是第一次部署分支，直到完成前，链接会返回 `404` 错误。在部署期间，停止按钮被禁用。如果流水线部署失败，则隐藏部署信息。

![Merge request pipeline](img/merge_request_pipeline.png)

有关更多信息，[阅读有关流水线的文档](../../../ci/pipelines/index.md)。

## 流水线成功时合并（MWPS）

将看起来准备好合并的合并请求设置为 [CI 流水线成功时自动合并](merge_when_pipeline_succeeds.md)。

## 使用 Review Apps 进行实时预览

如果您为项目配置了 Review Apps，则可以通过合并请求在每个分支的基础上预览提交到功能分支的更改。您无需在本地检出、安装和预览分支。
任何人都可以通过 Review Apps 链接预览您的所有更改。

设置[路由映射](../../../ci/review_apps/index.md#route-maps)后，合并请求部件会将您直接带到更改的页面，从而更轻松、更快速地预览建议修改。

[阅读有关 Review Apps 的更多信息](../../../ci/review_apps/index.md)。

## 许可证合规 **(ULTIMATE)**

如果您已为您的项目配置了[许可证合规](../../compliance/license_compliance/index.md)，那么您可以查看为您的项目的依赖项，检测到的许可证列表。

![Merge request pipeline](img/license_compliance_widget_v15_3.png)

## 外部状态检查 **(ULTIMATE)**

如果您已配置[外部状态检查](status_checks.md)，您可以在合并请求中的[特定部件](status_checks.md#status-checks-widget)中，查看这些检查的状态。
