---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
---

# 还原更改 **(FREE)**

您可以在极狐GitLab 中恢复单个提交或整个合并请求。当您在 Git 中恢复一个提交时，会创建一个新的提交来撤销在原始提交中执行的所有操作：

- 删除原始提交中添加的行。
- 在原始提交中删除的行被添加回来。
- 在原始提交中修改的行恢复到之前的状态。

您的 **还原提交** 仍然受制于项目的访问控制和流程。

## 还原合并请求

合并请求合并后，您可以还原合并请求中的所有更改。

先决条件：

- 您必须在项目中拥有允许您编辑合并请求并将代码添加到仓库的角色。
- 您的项目必须使用[合并方法](methods/index.md#fast-forward-merge)**合并提交**，该方法在项目的 **设置 > 通用 > 合并请求** 中设置。您无法从 UI 恢复快进式提交。

操作步骤：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **合并请求** 并确定您的合并请求。
1. 滚动到合并请求报告区域，找到显示合并请求何时合并的报告。
1. 选择 **还原**。
1. 在 **还原分支** 中，选择要将更改还原到的分支。
1. 可选。选择 **开启新的合并请求**，使用新的还原提交启动新的合并请求。
1. 选择 **还原**。

还原合并请求后，不再显示**还原**选项。

<a id="revert-a-commit"></a>

## 还原提交

您可以将仓库中的任何提交还原为：

- 当前分支。
- 新的合并请求。

先决条件：

- 您必须在项目中拥有允许您编辑合并请求并将代码添加到仓库的角色。

操作步骤：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到您的项目。
1. 如果您知道包含提交的合并请求：
    1. 在左侧边栏中，选择 **合并请求** 并确定您的合并请求。
    1. 选择 **提交**，然后选择要恢复的提交的标题。系统将在合并请求的 **变更** 选项卡中显示提交。
    1. 选择要还原的提交哈希。极狐GitLab 显示提交的内容。
1. 如果您不知道包含提交的合并请求：
    1. 在左侧边栏上，选择 **仓库 > 提交**。
    1. 选择提交的标题，显示有关提交的完整信息。
1. 在右上角，选择 **选项**，然后选择 **还原**。
1. 在 **还原分支** 中，选择要将更改还原到的分支。
1. 可选。 选择 **开启新的合并请求**，使用新的还原提交启动新的合并请求。
1. 选择 **还原**。

还原提交后，不再显示**还原**选项。

### 将合并提交还原为不同的父提交

当您还原合并提交时，您合并到的分支（通常是 `main`）始终是第一个父级。要将合并提交恢复到不同的父级，您必须从命令行恢复提交：

1. 确定要恢复到的父提交的 SHA。
1. 确定要恢复到的提交的父编号。（默认为 1，对于第一个父级。）
1. 修改此命令，将 `2` 替换为父编号，将 `7a39eb0` 替换为提交 SHA：

   ```shell
   git revert -m 2 7a39eb0
   ```

## 相关文档

- [官方 `git revert` 文档](https://git-scm.com/docs/git-revert)

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
