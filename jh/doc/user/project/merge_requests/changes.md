---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: index, reference
---

# 合并请求中的变更 **(FREE)**

[合并请求](index.md)提议对仓库中某一分支的文件进行一组变更。这些变更显示为当前状态与提议更改之间的 *diff*（差异）。

默认情况下，差异视图将合并请求源分支中的文件版本，与目标分支中的文件进行比较，并仅显示文件中已更改的部分。

![Example screenshot of a source code diff](img/mr-diff-example_v15.png)

## 显示合并请求中的所有变更

要查看合并请求中包含的更改差异：

1. 转到您的合并请求。
1. 在合并请求标题下方，选择 **变更**。
1. 如果合并请求更改了很多文件，可以直接跳转到特定文件：
    1. 选择 **显示文件浏览器** (**{file-tree}**)，显示文件树。
    1. 选择您要查看的文件。
    1. 要隐藏文件浏览器，请再次选择 **显示文件浏览器**。

在 13.4 及更高版本中，具有太多更改的文件会被折叠，从而提高性能。系统显示消息：**一些变化未显示**。要查看该文件的更改，请选择 **展开文件**。

## 一次显示一个文件

> - 引入于 13.2 版本。
> - 功能标志移除于 13.7 版本。

对于较大的合并请求，您可以一次查看一个文件。您可以[暂时在合并请求中](#in-a-merge-request-show-only-one-file-at-a-time)更改此设置，或者让其[适用于所有合并请求](#in-all-merge-requests-show-only-one-file-at-a-time)。

<a id="in-a-merge-request-show-only-one-file-at-a-time"></a>

### 在合并请求中，一次只显示一个文件

> 引入于 13.7 版本。

要临时更改特定合并请求的查看偏好设置：

1. 转到您的合并请求，在合并请求标题下方，选择 **变更**。
1. 选择 **偏好设置** (**{settings}**)。
1. 选中或清除 **一次显示一个文件** 复选框。

此更改会覆盖您在用户偏好设置中的选择，它会一直存在，直到您清除浏览器的 cookie 或再次更改此设置。

<a id="in-all-merge-requests-show-only-one-file-at-a-time"></a>

### 在所有合并请求中，一次只显示一个文件

一次查看所有合并请求的一个文件：

1. 在右上角，选择您的头像。
1. 选择 **偏好设置**。
1. 滚动到 **个性化** 部分并选中 **在合并请求的变更标签上一次只显示一个文件** 复选框。
1. 选择**保存更改**。

启用此设置后，系统在您查看合并请求时一次仅显示一个文件。要查看其它更改的文件，请执行以下任一操作：

- 滚动到文件末尾并选择 **Prev** 或 **Next**。
- 选择 **显示文件浏览器** (**{file-tree}**) 并选择另一个文件进行查看。

## 内联比较变更

您可以内联查看更改：

1. 转到您的合并请求，然后在标题下方选择 **变更**。
1. 选择 **偏好设置**（**{settings}**）。
1. 在 **比较变更** 区域中，选择 **内联**。

更改显示在原始文本之后。

![inline changes](img/changes-inline_v14_8.png)

## 并排比较变更

根据合并请求中更改的长度，您可能会发现内联或并排查看更改更容易：

1. 转到您的合并请求，然后在标题下方选择 **变更**。
1. 选择 **偏好设置**（**{settings}**）。
1. 在 **比较变更** 区域中，选择 **并排**。

更改与原始文本左右相对显示。

![side-by-side changes](img/changes-sidebyside_v14_8.png)

## 展开或折叠评论

查看代码更改时，您可以隐藏行内评论：

1. 转到您的合并请求，然后在标题下方选择 **变更**。
1. 滚动到包含您要隐藏的评论的文件。
1. 滚动到评论所在的行，然后选择 **隐藏**（**{collapse}**）：
   ![collapse a comment](img/collapse-comment_v14_8.png)

要展开行内评论并再次显示它们：

1. 转到您的合并请求，然后在标题下方选择 **变更**。
1. 滚动到包含您要显示的折叠评论的文件。
1. 滚动到评论所在的行，然后选择用户头像：
   ![expand a comment](img/expand-comment_v14_8.png)

## 忽略空白变更

空白变更会使查看合并请求中的实质性更改变得更加困难。您可以选择隐藏或显示空白变更：

1. 转到您的合并请求，然后在标题下方选择 **变更**。
1. 在更改的文件列表前，选择 **偏好设置**（**{settings}**）。
1. 选择或清除 **显示空白变更内容** 复选框：

   ![MR diff](img/merge_request_diff_v14_2.png)

## 将文件标记为已查看

> - 引入于 13.9 版本，[功能标志](../../../administration/feature_flags.md)名为 `local_file_reviews`。默认启用。
> - 功能标志移除于 14.3 版本。

多次查看包含多个文件的合并请求时，您可以忽略已查看的文件。要隐藏自上次评审以来未更改的文件：

1. 转到您的合并请求，然后在标题下方选择 **变更**。
1. 在文件的标题中，选中 **已查看** 复选框。

标记为已查看的文件不会再次为您显示，除非：

- 其内容发生了新的更改。
- 您清除了 **已查看** 复选框。

## 在差异中显示合并请求冲突 **(FREE SELF)**

> 引入于 13.5 版本，[功能标志](../../../administration/feature_flags.md)名为 `display_merge_conflicts_in_diff`。默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用，若要启用，询问管理员[启用功能标志](../../../administration/feature_flags.md)，功能标志名为 `display_merge_conflicts_in_diff`。在 SaaS 版上，此功能不可用。此功能尚未准备好用于生产环境。

为了避免在差异中显示已经在目标分支上的更改，我们将合并请求的源分支与目标分支的 HEAD 进行比较。

当源分支和目标分支之间存在冲突时，我们会在合并请求差异上显示冲突：

![Example of a conflict shown in a merge request diff](img/conflict_ui_v15_6.png)
