---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
---

# 合并方法 **(FREE)**

您为项目选择的合并方法决定了合并请求中的更改如何合并到现有分支中。

本文的示例假设有一个包含提交 A、C 和 E 的 `main` 分支，以及一个包含提交 B 和 D 的 `feature` 分支：

```mermaid
gitGraph
   commit id: "A"
   branch feature
   commit id: "B"
   commit id: "D"
   checkout main
   commit id: "C"
   commit id: "E"
```

## 配置项目的合并方法

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的项目。
1. 选择 **设置 > 合并请求**。
1. 从以下选项中选择您所需的 **合并方法**：
   - 合并提交
   - 合并提交与半线性历史记录
   - 快进式（Fast-forward）合并
   - 单独的压缩合并提交

1. 在 **合并时压缩提交** 中，选择处理提交的默认行为：
   
   - **不允许**：从不执行压缩，用户无法更改。
   - **允许**：默认情况下压缩处于关闭状态，用户可以更改。
   - **鼓励**：默认情况下压缩处于启用状态，用户可以更改。
   - **必须**：始终执行压缩，用户无法更改。

1. 选择 **保存更改**。

<a id="merge-commit"></a>

## 合并提交

默认情况下，当分支合并到 `main` 时，极狐GitLab 会创建合并提交。
无论提交[合并时是否被压缩](../squash_and_merge.md)，都会创建单独的合并提交。此方法可能会导致压缩提交和合并提交都添加到您的 `main` 分支中。

下图显示了如果您使用**合并提交**方法，`feature` 分支如何合并到 `main` 中，相当于使用命令 `git merge --no-ff <feature>`，并在 UI 中选择**合并提交**作为**合并方法**：

合并方法：

```mermaid
%%{init: { 'gitGraph': {'logLevel': 'debug', 'showBranches': true, 'showCommitLabel':true,'mainBranchName': 'main'}} }%%
gitGraph
   commit id: "A"
   branch feature
   commit id: "B"
   commit id: "D"
   checkout main
   commit id: "C"
   commit id: "E"
   merge feature
```

使用**合并提交**方法合并功能分支后，您的 `main` 分支如下所示：

```mermaid
%%{init: { 'gitGraph': {'logLevel': 'debug', 'showBranches': true, 'showCommitLabel':true,'mainBranchName': 'main'}} }%%
gitGraph
   commit id: "A"
   commit id: "C"
   commit id: "E"
   commit id: "squash commit"
   commit id: "merge commit"
```

相比之下，**压缩合并**生成一个压缩提交，即来自 `feature` 分支的所有提交的虚拟拷贝。原始提交（B 和 D）在 `feature` 分支上保持不变，而压缩提交被放置在 `main` 分支上：

```mermaid
%%{init: { 'gitGraph': {'showBranches': true, 'showCommitLabel':true,'mainBranchName': 'main'}} }%%
gitGraph
   commit id:"A"
   branch feature
   checkout main
   commit id:"C"
   checkout feature
   commit id:"B"
   commit id:"D"
   checkout main
   commit id:"E"
   commit id:"squash commit" type: HIGHLIGHT
```

压缩合并相当于在 UI 中进行以下设置：

- **合并方法**：合并提交。
- **合并时压缩提交** 应设置为：
   - **必须**
   - 在合并请求中选择 **允许**、**鼓励** 或 **压缩**

压缩合并方法相当于使用以下命令：

  ```shell
  git checkout `git merge-base feature main`
  git merge --squash <feature>
  SOURCE_SHA=`git rev-parse HEAD`
  git checkout <main>
  git merge --no-ff $SOURCE_SHA
  ```

<a id="single-squash-merge-commit"></a>

## 单独的压缩合并提交 **(FREE)**

> [引入](https://jihulab.com/gitlab-cn/gitlab/-/issues/2822)于 16.2 版本，[功能标志](../../../../administration/feature_flags.md)为 `single_squash_merge_ff`。默认禁用。

**单独的压缩合并提交**方法将所有提交压缩为单独的一个压缩提交（squash commit），不会另外生成合并提交（merge commit）。

选择**单独的压缩合并提交**作为合并方法时，在**合并时压缩提交**部分中，默认选择**必须**，且不能更改。

下图显示了如果您使用**单独的压缩合并提交**方法，将 `feature` 分支合并到 `main` 时，将生成一个压缩提交（squash commit），其中包括来自 `feature` 分支的所有提交：

```mermaid
%%{init: { 'gitGraph': {'logLevel': 'debug', 'showBranches': true, 'showCommitLabel':true,'mainBranchName': 'main'}} }%%
gitGraph
   commit id: "A"
   branch feature
   commit id: "B"
   commit id: "D"
   checkout main
   commit id: "C"
   commit id: "E"
   merge feature
```

使用**单独的压缩合并提交**方法合并功能分支后，您的 `main` 分支如下所示：

```mermaid
%%{init: { 'gitGraph': {'logLevel': 'debug', 'showBranches': true, 'showCommitLabel':true,'mainBranchName': 'main'}} }%%
gitGraph
   commit id: "A"
   commit id: "C"
   commit id: "E"
   commit id: "squash commit"
```

**单独的压缩合并提交**方法相当于使用以下命令：

  ```shell
  git checkout `git merge-base feature main`
  git merge --squash feature
  # git commit -m 'Squashed'
  git rebase main
  SOURCE_SHA=`git rev-parse HEAD`
  git checkout <main>
  git merge --ff-only $SOURCE_SHA
  ```

<a id="merge-commit-with-semi-linear-history"></a>

## 合并提交与半线性历史记录

每次合并都会创建一个合并提交，但只有在可以进行快进合并时才会合并分支，这确保了如果合并请求构建成功，则目标分支构建在合并后也成功。使用此合并方法生成的示例提交图：

```mermaid
gitGraph
  commit id: "Init"
  branch mr-branch-1
  commit
  commit
  checkout main
  merge mr-branch-1
  branch mr-branch-2
  commit
  commit
  checkout main
  merge mr-branch-2
  commit
  branch squash-mr
  commit id: "Squashed commits"
  checkout main
  merge squash-mr
```

当您访问选择了“合并提交与半线性历史记录”方法的合并请求页面时，**仅当可以进行快进合并时**，您可以接受它。
当无法进行快进合并时，用户可以选择变基，请参阅[（半）线性合并方法中的变基](#rebasing-in-semi-linear-merge-methods)。

此方法等效于**合并提交**方法中的相同 Git 命令。但是，如果您的源分支基于目标分支的过时版本（例如 `main`），则必须重新定位源分支。
这种合并方法创建了一个看起来更清晰的历史记录，同时仍然使您能够查看每个分支的开始和合并位置。

<a id="fast-forward-merge"></a>

## 快进式合并

有时，工作流程规则可能会要求一个干净的提交历史，而不需要合并提交。在这种情况下，快进式合并是合适的。使用快进式合并请求，您可以保留线性 Git 历史记录，以及在不创建合并提交的情况下接受合并请求的方式。使用此合并方法生成的示例提交图：

```mermaid
gitGraph
  commit id: "Init"
  commit id: "Merge mr-branch-1"
  commit id: "Merge mr-branch-2"
  commit id: "Commit on main"
  commit id: "Merge squash-mr"
```

此方法等效于常规合并的 `git merge --ff <source-branch>`，以及压缩合并的 `git merge --squash <source-branch>`。

当快进式合并（[`--ff-only`](https://git-scm.com/docs/git-merge#git-merge---ff-only)）设置已启用，不创建合并提交并且所有合并都是快进的，这意味着只有当分支可以快进时才允许合并。
当无法进行快进合并时，用户可以选择变基，请参阅[（半）线性合并方法中变基](#rebasing-in-semi-linear-merge-methods)。

NOTE:
使用快进式合并策略的项目无法按部署日期过滤合并请求，因为没有创建合并提交。

当您访问选择了“快进式合并”方法的合并请求页面时，**仅当可以进行快进合并时**，您可以接受它。

![Fast-forward merge request](../img/ff_merge_mr.png)

<a id="rebasing-in-semi-linear-merge-methods"></a>

## （半）线性合并方法中的变基

在以下合并方法中，您只能在源分支与目标分支保持同步时进行合并：

- 合并提交与半线性历史记录。
- 快进式合并。

如果无法进行快进式合并但可以进行无冲突的变基，系统为您提供 [`/rebase` 快速操作](../../../../topics/git/git_rebase.md#rebase-from-the-gitlab-ui)，以及使用用户界面进行**变基**：

如果目标分支在源分支之前并且不可能进行无冲突的变基，则必须在本地变基源分支，然后才能进行快进式合并。

![Fast forward merge rebase locally](../img/ff_merge_rebase_locally.png)

在压缩之前可能需要重新设置 base，即使压缩本身可以被认为等同于重新设置 base。

### 没有 CI/CD 流水线的变基

> - 引入于 14.7 版本，功能标志为 `rebase_without_ci_ui`。默认禁用。
> - 一般可用于 15.3 版本，功能标志 `rebase_without_ci_ui` 已删除。

要在不触发 CI/CD 流水线的情况下重新调整合并请求的分支，请从合并请求报告部分选择 **没有流水线的变基**。
当无法进行快进合并但可以进行无冲突的变基时，此选项可用。

在没有 CI/CD 流水线的情况下进行变基，可以在需要频繁变基的半线性工作流程的项目中节省资源。

## 相关话题

- [提交历史](../commits.md)
- [压缩与合并](../squash_and_merge.md)
