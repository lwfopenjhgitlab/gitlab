---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 跨分支协作处理合并请求 **(FREE)**

当您从分支打开合并请求时，您可以允许上游成员在您的分支上与您协作。
当您启用此选项时，有权合并到目标分支的成员将获得写入合并请求的源分支的权限。

然后，上游项目的成员可以在合并之前进行小修复或 rebase 分支。

此功能可用于跨可公开访问的派生项目的合并请求。

## 允许来自上游成员的提交

> 默认启用于 13.7 或更高版本。

作为合并请求的作者，您可以允许来自您正在参与的项目的上游成员的提交编辑：

1. 在创建或编辑合并请求时，滚动到 **贡献** 并选中 **允许具有合并到目标分支权限的成员提交** 复选框。
1. 完成创建合并请求。

创建合并请求后，合并请求部件会显示消息 **允许可以合并的成员添加提交**。然后上游成员可以直接提交到您的分支，以及重试合并请求的流水线和作业。

## 阻止来自上游成员的提交

作为合并请求的作者，您可以阻止您正在参与的项目的上游成员进行提交编辑：

1. 在创建或编辑合并请求时，滚动到 **贡献** 并清除 **允许可以合并到目标分支的成员提交** 复选框。
1. 完成创建合并请求。

## 作为上游成员推送到派生项目

在以下情况，您可以直接推送到派生仓库的分支：

- 合并请求的作者启用了上游成员的贡献。
- 您至少在上游项目中拥有开发者角色。

在以下示例中：

- 派生的仓库 URL 是 `git@jihulab.com:contributor/forked-project.git`。
- 合并请求的分支是 `fork-branch`。

要更改或向贡献者的合并请求添加提交：

1. 进入合并请求。
1. 在右上角选择 **代码**，然后选择 **检出分支**。
1. 在窗口中，选择 **复制** (**{copy-to-clipboard}**)。
1. 在您的终端中，转到您的仓库的克隆版本，然后粘贴命令。例如：

   ```shell
   git fetch "git@jihulab.com:contributor/forked-project.git" 'fork-branch'
   git checkout -b 'contributor/fork-branch' FETCH_HEAD
   ```

   这些命令从派生项目中获取分支，并创建一个本地分支供您使用。

1. 对分支的本地副本进行更改，然后提交它们。
1. 将本地更改推送到派生项目。以下命令将本地分支 `contributor/fork-branch` 推送到 `git@jihulab.com:contributor/forked-project.git` 仓库的 `fork-branch` 分支：

   ```shell
   git push git@jihulab.com:contributor/forked-project.git contributor/fork-branch:fork-branch
   ```

   如果您修改或压缩了任何提交，则必须强制推送。请谨慎操作，因为此命令会重写提交历史记录：

   ```shell
   git push --force git@jihulab.com:contributor/forked-project.git contributor/fork-branch:fork-branch
   ```

   注意两个分支之间的冒号（`:`）：

   ```shell
   git push <forked_repository_git_url> <local_branch>:<fork_branch>
   ```

## 故障排查

### 派生项目的 MR 页面中流水线状态不可用

当用户派生项目时，派生副本的权限不会从原始项目中复制。派生项目的创建者必须先授予派生副本的权限，上游项目的成员才能查看或合并合并请求中的更改。

要从派生项目的合并请求页面返回到原始项目查看流水线状态：

1. 创建一个包含所有上游成员的群组。
1. 进入派生项目中的 **项目信息 > 成员** 页面，邀请新创建的群组加入派生项目。
