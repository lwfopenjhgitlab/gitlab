---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
---

# 自动合并 **(FREE)**

> **当流水线成功时合并** 和 **流水线成功时添加到合并队列** 功能重命名为 **自动合并** 于 16.0 版本，[功能标志](../../../administration/feature_flags.md)为 `auto_merge_labels_mr_widget`。默认启用。

如果您查看合并请求并准备好合并，但流水线尚未完成，您可以将其设置为在流水线成功时合并（MWPS），以后不必进行手动合并工作：

![Enable MWPS on a merge request](img/mwps_v15_4.png)

如果流水线成功，则合并请求。如果流水线失败，作者可以重试任何失败的作业，或者推送新的提交来修复失败：

- 如果重试作业在第二次尝试时成功，则合并请求。
- 如果新的提交被添加到合并请求中，极狐GitLab 会取消 MWPS 请求，确保在合并之前审查新的更改。

## 设置合并请求为 MWPS

先决条件：

- 您必须在项目中至少拥有开发者角色。
- 如果项目配置为需要它，则合并请求中的所有主题[必须已解决](../../discussions/index.md#resolve-a-thread)。
- 合并请求必须已获得所有必需的批准。

要在从命令行推送时执行此操作，请使用 `merge_request.merge_when_pipeline_succeeds` [推送选项](../push_options.md)。

要从极狐GitLab 用户界面执行此操作：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **合并请求**。
1. 滚动到合并请求报告部分。
1. 可选。选择您想要的合并选项，例如 **删除源分支**、**压缩提交** 或 **编辑提交消息**。
1. 选择 **流水线成功时合并**。

如果在您选择 **当流水线成功时合并**，但在流水线完成之前向合并请求添加了新注释，极狐GitLab 会阻止合并，直到您解决所有现有主题。

## 取消自动合并

如果合并请求设置为 MWPS，您可以取消它。

先决条件：

- 您必须是合并请求的作者，或者至少具有开发者角色的项目成员。
- 合并请求的流水线必须仍在进行中。

操作步骤：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **合并请求**。
1. 滚动到合并请求报告部分。
1. 选择 **取消自动合并**。

![Status](img/cancel-mwps_v15_4.png)

<a id="require-a-successful-pipeline-for-merge"></a>

## 需要成功的合并流水线

您可以将项目配置为在合并之前需要完整且成功的流水线。此配置适用于：

- 极狐GitLab CI/CD 流水线。
- 从[外部 CI 集成](../integrations/index.md#available-integrations)运行的流水线。

因此，[禁用极狐GitLab CI/CD 流水线](../../../ci/enable_or_disable_ci.md)不会禁用此功能，但您可以使用来自外部 CI 提供商的流水线。

先决条件：

- 确保 CI/CD 配置为为每个合并请求运行流水线。
- 您必须在项目中至少具有维护者角色。

要启用此设置：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 合并请求**。
1. 滚动到 **合并检查**，然后选择 **流水线必须成功**。
   如果没有流水线，此设置还可以防止合并请求被合并，这可能[与某些规则冲突](#merge-requests-dont-merge-when-successful-pipeline-is-required)。
1. 选择 **保存**。

### 跳过流水线后允许合并

当 **流水线必须成功** 复选框被选中时，[跳过的流水线](../../../ci/pipelines/index.md#skip-a-pipeline)阻止合并请求被合并。

先决条件：

- 您必须在项目中至少具有维护者角色。

操作步骤：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **合并请求**。
1. 在 **合并检查** 下：
    - 选择 **流水线必须成功**。
    - 选择 **跳过的流水线被认为是成功的**。
1. 选择 **保存**。

## 故障排除

<a id="merge-requests-dont-merge-when-successful-pipeline-is-required"></a>

### 当需要成功的流水线时，合并请求不合并

如果您需要成功的流水线进行合并，此设置可能会与一些不生成流水线的用例发生冲突，例如 [`only/except`](../../../ci/yaml/index.md#only--except) 或 [`rules`](../../../ci/yaml/index.md#rules)。确保您的项目为每个合并请求运行一个流水线，并且该流水线是成功的。

### 确保流水线类型之间的测试奇偶性

如果合并请求同时触发分支流水线和合并请求流水线，则仅检查*合并请求流水线*的成功或失败。
如果合并请求流水线包含的作业比分支流水线少，它可能允许合并测试失败的代码，如下例所示：

```yaml
branch-pipeline-job:
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
  script:
    - echo "Testing happens here."

merge-request-pipeline-job:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  script:
    - echo "No testing happens here. This pipeline always succeeds, and enables merge."
    - echo true
```

相反，尽可能使用分支（`push`）流水线或合并请求流水线。
有关避免单个合并请求使用两个流水线的详细信息，请阅读 [`rules` 文档](../../../ci/jobs/job_control.md#avoid-duplicate-pipelines)。
