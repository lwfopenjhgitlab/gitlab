---
stage: Manage
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 导出合并请求为 CSV **(FREE)**

> 引入于 13.6 版本。

将从项目的合并请求中收集的所有数据导出到逗号分隔值 (CSV) 文件中。

要将合并请求导出到 CSV 文件：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **合并请求**。
1. 添加任何搜索或过滤器。这可以帮助您将 CSV 文件的大小保持在 15MB 的限制之下。该限制确保文件可以通过电子邮件发送给各种电子邮件提供商。
1. 选择 **操作** (**{ellipsis_v}**) **> 导出为 CSV** (**{export}**)。
1. 确认要导出正确数量的合并请求。
1. 选择 **导出合并请求**。

## CSV 输出

下表显示了 CSV 中将出现的属性。

| 列             | 描述                                                  |
|--------------------|--------------------------------------------------------------|
| Title              | 合并请求标题                                          |
| Description        | 合并请求描述                                    |
| MR ID              | MR `iid`                                                     |
| URL                | 极狐GitLab 上的合并请求链接                       |
| State              | Opened, Closed, Locked, 或 Merged                            |
| Source Branch      | 源分支                                                |
| Target Branch      | 目标分支                                                |
| Source Project ID  | 源项目的 ID                                    |
| Target Project ID  | 目标项目的 ID                                     |
| Author             | 合并请求作者的全名                       |
| Author Username    | 作者的用户名，省略 @ 符号            |
| Assignees          | 合并请求指派人的全名，用 `,` 连接 |
| Assignee Usernames | 指派人的用户名，省略 @ 符号         |
| Approvers          | 核准人的全名，用`,`连接               |
| Approver Usernames | 核准人的用户名，省略 @ 符号        |
| Merged User        | 合并用户的全名                                |
| Merged Username    | 合并用户的用户名，省略 @ 符号        |
| Milestone ID       | 合并请求里程碑的 ID                           |
| Created At (UTC)   | 格式为 `YYYY-MM-DD HH:MM:SS`                           |
| Updated At (UTC)   | 格式为 `YYYY-MM-DD HH:MM:SS`                           |

在 14.7 及更早版本中，前两列是 `MR ID` 和 `URL`，在重新导入时会导致问题。
