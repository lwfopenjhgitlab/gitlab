---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
---

# 合并请求依赖 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/9688) in [GitLab Premium](https://about.gitlab.com/pricing/) 12.2.
> - [Renamed](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/17291) from "Cross-project dependencies" to "Merge request dependencies" in [GitLab Premium](https://about.gitlab.com/pricing/) 12.4.
> - Intra-project MR dependencies were [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/16799) in [GitLab Premium](https://about.gitlab.com/pricing/) 12.4.
-->

合并请求相关性允许表达合并请求之间所需的合并顺序。如果合并请求“依赖于”另一个请求，则在其依赖项本身合并之前无法合并。

NOTE:
合并请求依赖项是 **专业版** 功能，但此限制仅对依赖合并请求强制执行。**免费版** 项目中的合并请求可以是 **专业版** 合并请求的依赖项，但反过来不行。

## 用例

- 确保在更改导入库的项目之前合并对库的更改。
- 在实现功能的合并请求之前，防止合并仅含文档的合并请求。
- 在合并来自尚未被授予权限的人的合并请求之前，需要合并请求更新要合并的权限矩阵。

单个逻辑更改跨越多个合并请求、分布在多个项目中是很常见的，并且它们合并的顺序可能很重要。

例如，给定一个项目 `mycorp/awesome-project`，它在 `myfriend/awesome-lib` 中导入一个库，在 `awesome-project` 中添加一个特性可能**也**需要对 `awesome-lib` 进行更改，因此需要两个合并请求。在 `awesome-lib` 之前合并 `awesome-project` 合并请求会破坏默认分支。

`awesome-project` 合并请求可以[标记为 **Draft**](drafts.md)，并在评论中说明 Draft 的原因。但是，这需要手动跟踪 `awesome-lib` 合并请求的状态，并且如果 `awesome-project` 合并请求依赖于 **几个** 其他项目的更改，则无法很好地扩展。

通过让 `awesome-project` 合并请求依赖于 `awesome-lib` 合并请求，系统会自动跟踪这种关系，并且草稿状态可用于在每个单独的合并请求中传达代码的准备情况.

## 配置

为了继续上面的例子，您可以在 `awesome-project` 中创建新的合并请求时配置一个依赖项（或者通过编辑它，如果它已经存在）。依赖需要在 **依赖** 合并请求上配置。 表单中有一个 **合并请求依赖** 部分：

![Merge request dependencies form control](img/dependencies_edit_v12_4.png)

任何可以编辑合并请求的人都可以更改依赖项列表。

可以通过引用或 URL 添加新的依赖项。要删除依赖项，请按其引用的 **X**。

由于可以跨项目指定依赖项，因此其他人可能在您无权访问的项目中为合并请求添加了依赖项。
显示为一个简单的计数：

![Merge request dependencies form control with inaccessible merge requests](img/dependencies_edit_inaccessible_v12_4.png)

如有必要，您可以通过按 **X** 删除所有这样的依赖项，就像删除单个可见依赖项一样。

完成后，按 **保存更改** 按钮提交请求，或按 **取消** 返回而不进行任何更改。

已配置的依赖项列表以及每个依赖项的状态显示在合并请求部件中：

![Dependencies in merge request widget](img/dependencies_view_v15_3.png)

在所有依赖项本身都被合并之前，**合并** 按钮对于依赖合并请求是禁用的。特别要注意的是，**关闭的合并请求** 仍然会阻止它们的依赖项被合并——无法自动确定关闭的合并请求所表达的依赖项是否以某种其他方式得到满足。

如果合并请求已关闭**并且**依赖项不再相关，则必须在合并之前按照上述说明将其作为依赖项删除。

## 限制

- API 支持
- 跨项目导出/导入不保留依赖项
- 不支持复杂的合并顺序依赖项

最后一项需要多解释一下。合并请求之间的依赖关系可以描述为关系图。最简单的是有一个依赖于另一个的合并请求：

```mermaid
graph LR;
    myfriend/awesome-lib!10-->mycorp/awesome-project!100;
```

更复杂（且仍受支持）的图可能有一个直接依赖于其他几个请求的合并请求：

```mermaid
graph LR;
    myfriend/awesome-lib!10-->mycorp/awesome-project!100;
    herfriend/another-lib!1-->mycorp/awesome-project!100;
```

几个不同的合并请求也可以直接依赖于同一个合并请求：

```mermaid
graph LR;
    herfriend/another-lib!1-->myfriend/awesome-lib!10;
    herfriend/another-lib!1-->mycorp/awesome-project!100;
```

**不**支持的是依赖关系的“深度”或“嵌套”图。例如：

```mermaid
graph LR;
    herfriend/another-lib!1-->myfriend/awesome-lib!10;
    myfriend/awesome-lib!10-->mycorp/awesome-project!100;
```

在这个例子中，`myfriend/awesome-lib!10` 依赖于 `herfriend/another-lib!1`，它本身也是 `mycorp/awesome-project!100` 的依赖。 这意味着 `herfriend/another-lib!1` 成为`mycorp/awesome-project!100` 的**间接**依赖项，目前尚不支持。
