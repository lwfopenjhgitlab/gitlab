---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
disqus_identifier: 'https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html'
---

# Draft 合并请求 **(FREE)**

如果合并请求未准备好合并，可能是因为持续开发或开放主题，您可以在[将其标记为就绪](#标记为就绪)之前阻止它被接受。将其标记为草稿以禁用 **合并** 按钮，直到您删除 **Draft** 标志：

![Blocked Merge Button](img/draft_blocked_merge_button_v13_10.png)

## 标记为草稿

> - 引入于 13.2 版本, Work-In-Progress (WIP) 合并请求重命名为 **Draft**。
> - 在 14.8 版本中，删除了对使用 **WIP** 的所有支持。
> - **标记为草稿** 和 **标记为就绪** 按钮引入于 13.5 版本。`/draft` 快速操作切换废弃于 15.4 版本。

有几种方法可以将合并请求标记为草稿：

- **查看合并请求**：在合并请求的右上角，单击 **标记为草稿**。
- **创建或编辑合并请求**：将`[Draft]`、`Draft:` 或`(Draft)` 添加到合并请求标题的开头，或单击 **以 Draft 开始标题：** 在 **标题** 字段下方。
- **在现有合并请求中评论**：在评论中添加 `/draft` [快速操作](../quick_actions.md#议题合并请求和史诗)。这个快速操作可以实现切换，可以重复操作，将状态更改回 Ready。
- **创建提交**：将 `draft:`、`Draft:`、`fixup!` 或 `Fixup!` 添加到针对合并请求源分支的提交消息的开头。这个快速操作不能切换，并且在以后的提交中再次添加此文本不会将合并请求标记为就绪。

<a id="mark-merge-requests-as-ready"></a>

## 标记为就绪

当合并请求准备好合并时，您可以通过以下几种方式删除 `Draft` 标志：

- **查看合并请求**：在合并请求的右上角，单击 **标记为就绪**。拥有开发者或更高权限的用户也可以滚动到合并请求描述的底部，然后单击 **标记为就绪**：

  ![Mark as ready](img/draft_blocked_merge_button_v13_10.png)

- **编辑现有的合并请求**：从标题的开头移除`[Draft]`、`Draft:` 或 `(Draft)`，或者点击**标题** 字段下方的 **Remove the Draft: 前缀**。
- **在现有合并请求中评论**：在合并请求的评论中添加 `/ready` [快速操作](../quick_actions.md#议题合并请求和史诗)。

在 13.10 和之后的版本，当您将合并请求标记为就绪，触发发送通知给合并请求的参与者和关注者。

## 搜索时包括或排除草稿

在项目的合并请求列表中查看或搜索时，您可以包含或排除草稿状态的合并请求：

1. 转到您的项目并选择 **合并请求**。
1. 在导航栏中，单击 **Open**、**Merged**、**Closed** 或 **All** 以按合并请求状态过滤。
1. 单击搜索框以显示过滤器列表并选择 **草稿**，或输入 “Draft” 一词。
1. 选择`=`。
1. 选择 **是** 以包含草稿，或选择 **否** 以排除，然后按 **回车** 更新合并请求列表：

   ![Filter draft merge requests](img/filter_draft_merge_requests_v13_10.png)

## 草稿合并请求流水线

草稿合并请求与标记为就绪的合并请求运行相同的流水线。

在 15.0 及更早版本中，如果要运行[合并结果流水线](../../../ci/pipelines/merged_results_pipelines.md)，则必须[将合并请求标记为就绪](#mark-merge-requests-as-ready)。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
