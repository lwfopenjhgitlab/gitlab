---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
---

# 拣选更改 **(FREE)**

在 Git 中，*拣选*是从一个分支获取一个提交并将其添加为另一个分支的最新提交。源分支中的其余提交不会添加到目标。当您需要单个提交中包含的更改时，您应该挑选一个提交，但您不能或不想将该分支的全部内容拉入另一个。

您可以使用 UI 来拣选单个提交或整个合并请求，可以从[派生的项目](#cherry-pick-into-a-project)中拣选一个提交。

## 拣选提交

在这个拣选的示例中，一个 Git 仓库有两个分支：`develop` 和 `main`。
此例展示了一个从一个分支中拣选出来的提交被添加到另一个分支：

```mermaid
gitGraph
 commit id: "A"
 branch develop
 commit id:"B"
 checkout main
 commit id:"C"
 checkout develop
 commit id:"D"
 checkout main
 commit id:"E"
 cherry-pick id:"B"
 commit id:"G"
 checkout develop
 commit id:"H"
```

在此示例中，在 `main` 分支中的提交 `E` 之后添加了来自 `develop` 分支的提交 `B` 的拣选。

提交 `G` 是在拣选之后添加的。

## 从合并请求中拣选所有更改

合并请求合并后，您可以拣选合并请求引入的所有更改：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **合并请求**，然后找到您的合并请求。
1. 滚动到合并请求报告部分，找到 **合并者** 报告。
1. 在右上角，选择 **拣选**：

   ![Cherry-pick merge request](img/cherry_pick_v15_4.png)

1. 在窗口中，选择项目和分支来拣选。
1. 可选。选择 **使用这些更改开始新的合并请求**。
1. 选择 **拣选**。

<a id="cherry-pick-a-single-commit"></a>

## 拣选单个提交

您可以从极狐GitLab 项目的多个位置中拣选单个提交。

### 从项目提交列表

要从项目的所有提交列表中拣选一个提交：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **仓库 > 提交**。
1. 选择您要拣选的提交的标题。
1. 在窗口中，选择项目和分支来拣选。
1. 可选。选择 **使用这些更改开始新的合并请求**。
1. 选择 **拣选**。

### 从合并请求

无论合并请求是开放还是关闭，您都可以从项目中的任何合并请求中拣选提交。要从合并请求中包含的提交列表中拣选一个提交：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **合并请求**，然后找到您的合并请求。
1. 在合并请求的二级菜单中，选择 **提交** 以显示提交详情页面。
1. 选择您要拣选的提交的标题。
1. 在右上角，选择 **选项 > 拣选**，显示拣选窗口。
1. 在窗口中，选择项目和分支来拣选。
1. 可选。选择 **使用这些更改开始新的合并请求**。
1. 选择 **拣选**。

### 从仓库的文件视图

当您在项目的 Git 仓库中查看该文件时，您可以从影响单个文件的先前提交列表中拣选：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **仓库 > 文件** 并转到提交更改的文件。
1. 选择 **历史**，然后选择您要拣选的提交的标题。
1. 在右上角，选择 **选项 > 拣选** 来显示拣选窗口。
1. 在窗口中，选择项目和分支来拣选。
1. 可选。选择 **使用这些更改开始新的合并请求**。
1. 选择 **拣选**。

<a id="cherry-pick-into-a-project"></a>

### 拣选到项目中

> - 引入到 13.11 版本，使用功能标志，默认禁用。
> - 功能标志移除于 14.0 版本。

您可以从用户界面中拣选来自同一项目或同一项目的派生项目的合并请求：

1. 在合并请求的二级菜单中，点击 **提交**，显示提交详情页面。
1. 在右上角，选择 **选项 > 拣选** 来显示拣选窗口。
1. 在 **选择项目** 和 **选择分支** 中，选择目标项目和分支：
   ![Cherry-pick commit](img/cherry_pick_into_project_v13_11.png)
1. （可选）如果您已准备好创建合并请求，请选择 **启动新的合并请求**。
1. 点击 **拣选**。

## 查看拣选提交的系统说明

当您在 UI 或 API 中拣选合并提交时，极狐GitLab 会相关的合并请求主题添加系统备注：

![Cherry-pick tracking in merge request timeline](img/cherry_pick_mr_timeline_v15_4.png)

<!--
The system note crosslinks the new commit and the existing merge request.
Each deployment's [list of associated merge requests](../../../api/deployments.md#list-of-merge-requests-associated-with-a-deployment) includes cherry-picked merge commits.
-->

## 相关文档

- 使用 [Commits API](../../../api/commits.md) 将自定义消息添加到使用 API 进行拣选时的更改。

## 故障排除

### 拣选时选择不同的父提交

当您在 UI 中拣选合并提交时，主线始终是第一个父级。使用命令行选择不同的主线。

以下是一个快速使用第二个父级作为主线来拣选合并提交的示例：

```shell
git cherry-pick -m 2 7a39eb0
```
