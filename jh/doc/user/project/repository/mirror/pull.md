---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
disqus_identifier: 'https://docs.gitlab.com/ee/workflow/repository_mirroring.html'
---

# 从远端仓库拉取 **(PREMIUM)**

> 移动到专业版于 13.9 版本。

您可以使用极狐GitLab 界面浏览仓库的内容和活动，即使它没有托管在极狐GitLab 上。创建拉取[镜像](index.md)，将分支、标签和提交从上游仓库复制到您的。

与[推送镜像](push.md)不同，拉取镜像按计划从上游（远端）仓库检索更改。为防止镜像与上游仓库分叉，请勿将提交直接推送到下游镜像。改为将提交推送到上游仓库。远端仓库中的更改在以下情况被拉入极狐GitLab 仓库：

- 在一定时间内自动。自助管理实例可以配置拉取镜像间隔<!--[拉取镜像间隔](../../../../administration/instance_limits.md#pull-mirroring-interval)-->。
- 当管理员[强制更新镜像](index.md#强制更新)。
- 当 [API 调用触发更新](#使用-api-触发更新)时。

默认情况下，如果下游拉取镜像上的任何分支或标签与本地仓库不同，极狐GitLab 将停止更新该分支。这可以防止数据丢失。上游仓库中删除的分支和标签不会反映在下游仓库中。

NOTE:
对于从下游拉取镜像仓库中删除但仍在上游仓库中的项目，将在下一次拉取时恢复。例如：仅在镜像仓库中删除的分支，将在下一次拉取后重新出现。

## 拉取镜像如何工作

将 GitLab 仓库配置为拉取镜像后：

1. 极狐GitLab 将仓库添加到队列中。
1. 每分钟一次，Sidekiq cron 作业调度仓库镜像更新，基于：
   - 可用容量，由 Sidekiq 设置决定。<!--对于 GitLab.com，请阅读 [GitLab.com Sidekiq 设置](../../../gitlab_com/index.md#sidekiq)。-->
   - 队列中已有多少镜像需要更新。到期取决于上次更新仓库镜像的时间以及重试更新的次数。
1. Sidekiq 可用于处理更新，镜像被更新。如果更新过程：
   - **成功**：更新再次排队等待至少 30 分钟。
   - **失败**：稍后再次尝试更新。在 14 次失败后，镜像被标记为 [hard failure](#镜像时修复硬故障) 并且不再排队等待更新。分支与其上游对应分支的分叉可能导致故障。为防止分支分叉，请在创建镜像时配置[覆盖分叉的分支](#覆盖分叉的分支)。

## 配置拉取镜像

先决条件：

- 如果您的远端仓库在 GitHub 上并且您已[配置双重身份验证 (2FA)](https://docs.github.com/en/authentication/securing-your-account-with-two-factor-authentication-2fa)，创建一个 [GitHub 的个人访问令牌](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token) `repo` 范围。如果启用了 2FA，此个人访问令牌将用作您的 GitHub 密码。

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **镜像仓库**。
1. 输入 **Git 仓库 URL**。如果需要，请在 URL 中包含用户名：`https://MYUSERNAME@github.com/GROUPNAME/PROJECTNAME.git`
1. 在 **镜像方向**，选择 **拉取**。
1. 在 **验证方式** 中，选择您的认证方式。
1. 选择您需要的任何选项：
   - [**覆盖分叉分支**](#覆盖分叉的分支)
   - [**触发镜像更新的流水线**](#触发镜像更新的流水线)
   - **仅镜像受保护的分支**
1. 要保存配置，请选择 **镜像仓库**。

### 覆盖分叉的分支

> 移动到专业版于 13.9 版本。

要始终使用远端版本更新本地分支，即使它们已从远端分支，请在创建镜像时选择 **覆盖分叉分支**。

WARNING:
对于镜像分支，启用此选项会导致本地更改丢失。

### 触发镜像更新的流水线

> 移动到专业版于 13.9 版本。

如果启用此选项，则在从远端仓库更新分支或标签时触发流水线。根据远端仓库的活动，这可能会大大增加 CI 运行程序的负载。仅当您知道它们可以处理负载时才启用此功能。CI 使用设置拉取镜像时分配的凭据。

## 使用 API 触发更新

> 移动到专业版于 13.9 版本。

拉取镜像使用轮询来检测上游添加的新分支和提交，通常是在几分钟之后。您可以使用 API 调用<!--[API 调用](../../../../api/projects.md#start-the-pull-mirroring-process-for-a-project)-->通知极狐GitLab，但[最小拉取拉镜像限制的间隔](index.md#force-an-update)仍然强制执行。

<!--
For more information, read
[Start the pull mirroring process for a project](../../../../api/projects.md#start-the-pull-mirroring-process-for-a-project).
-->

## 镜像时修复硬故障

> 移动到专业版于 13.9 版本。

连续 14 次重试失败后，镜像过程将被标记为硬故障并停止镜像尝试。此故障在以下任一情况下可见：

- 项目的主仪表板。
- 拉取镜像设置页面。

要恢复项目镜像，[强制更新](index.md#强制更新)。

<!--
## Related topics

- Configure [pull mirroring intervals](../../../../administration/instance_limits.md#pull-mirroring-interval)
  on self-managed instances.
- Configure [pull mirroring through the API](../../../../api/projects.md#configure-pull-mirroring-for-a-project).
-->