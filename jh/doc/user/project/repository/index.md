---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: concepts, howto
---

# 仓库 **(FREE)**

[仓库](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)是您存储代码并对其进行更改的地方。您的更改通过版本控制进行跟踪。

每个[项目](../index.md)都包含一个仓库。

## 创建仓库

要创建一个仓库，您可以：

- [创建一个项目](../../../user/project/working_with_projects.md#创建项目) 或
- [派生一个已有的项目](forking_workflow.md)。

## 添加文件到仓库

您可以将文件添加到仓库：

- 创建项目时。
- 创建项目后：
   - 通过使用 [web 编辑器](web_editor.md)。
   - [从命令行](../../../gitlab-basics/command-line-commands.md)。

<a id="commit-changes-to-a-repository"></a>

## 提交对仓库的更改

您可以[提交您的更改](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository)到仓库中的一个分支。当您使用命令行时，您可以在推送之前多次提交。

- **提交消息：**
  提交消息标识正在更改的内容以及原因。
  在极狐GitLab 中，您可以向提交消息添加关键字以执行以下操作之一：
  - **触发 GitLab CI/CD 流水线：**
  如果项目配置了 GitLab CI/CD<!--[GitLab CI/CD](../../../ci/index.md)-->，则每次推送都会触发流水线，而不是每次提交。
  - **跳过流水线：**
  将 `ci skip`<!--[`ci skip`](../../../ci/pipelines/index.md#skip-a-pipeline)--> 关键字添加到您的提交消息中，以使 GitLab CI/CD 跳过流水线。
  - **Cross-linking 议题和合并请求：**
  使用 [cross-linking](../issues/crosslinking_issues.md#from-commit-messages) 来跟踪工作流程的相关部分。
  如果您在提交消息中提及议题或合并请求，它们将显示在各自的主题中。
- **拣选提交：**
  在极狐GitLab 中，您可以从 UI [拣选提交](../merge_requests/cherry_pick_changes.md#cherry-pick-a-single-commit)。
- **还原提交：**
  [还原提交](../merge_requests/revert_changes.md#revert-a-commit) 从 UI 到选定的分支。
- **签名提交：**
  使用 GPG 签名您的提交<!--[签名您的提交](gpg_signed_commits/index.md)-->。

## 克隆仓库

您可以[使用命令行克隆存储库](../../../gitlab-basics/start-using-git.md#clone-a-repository)。

或者，您可以直接克隆到代码编辑器中。

### 克隆并在 Apple Xcode 中打开

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/45820) in GitLab 11.0.
-->

包含 `.xcodeproj` 或 `.xcworkspace` 目录的项目可以在 macOS 上克隆到 Xcode。

1. 从 GitLab UI，转到项目的概览页面。
1. 选择 **克隆**。
1. 选择 **Xcode**。

该项目被克隆到您的计算机上，并提示您打开 XCode。

### 克隆并在 Visual Studio Code 中打开

> 引入于 13.10 版本。

所有项目都可以从用户界面克隆到 Visual Studio Code，但您也可以安装 GitLab Workflow VS Code 扩展<!--[GitLab Workflow VS Code 扩展](vscode.md)-->，从 Visual Studio Code 克隆：

- 从 GitLab 界面：
   1. 进入项目概览页面。
   1. 选择 **克隆**。
   1. 在 **HTTPS** 或 **SSH** 方法下，选择 **使用 Visual Studio Code 克隆**。
   1. 选择一个文件夹来克隆项目。

      在 Visual Studio Code 克隆您的项目后，它会打开该文件夹。
- 从 Visual Studio Code 安装扩展，使用扩展的 [`Git: Clone` 命令](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow#clone-gitlab-projects)。

## 下载仓库中的代码

<!--
> - Support for directory download was [introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/24704) in GitLab 11.11.
> - Support for [including Git LFS blobs](../../../topics/git/lfs#lfs-objects-in-project-archives) was [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/15079) in GitLab 13.5.
-->

您可以下载存储在仓库中的源代码。

1. 在文件列表上方，选择下载图标 (**{download}**)。
1. 从选项中，选择要下载的文件。

    - **源代码：**
      从您正在查看的当前分支下载源代码。
      可用的扩展名：`zip`、`tar`、`tar.gz` 和`tar.bz2`。
    - **目录：**
      下载特定目录。 仅在查看子目录时可见。
      可用的扩展名：`zip`、`tar`、`tar.gz` 和`tar.bz2`。
    - **产物：**
      从最新的 CI 作业下载产物。

## 仓库语言

对于每个仓库的默认分支，极狐GitLab 确定使用哪些编程语言。该信息显示在 **项目信息** 页面上。

![Repository Languages bar](img/repository_languages_v15_2.png)

添加新文件时，此信息最多可能需要五分钟才能更新。

### 添加仓库语言

并非所有文件都被检测到并列在 **项目信息** 页面上。文档、供应商代码和大多数标记语言被排除在外。

您可以通过覆盖默认设置来更改。

1. 在仓库的根目录中，创建一个名为 `.gitattributes` 的文件。
1. 添加一行告诉极狐GitLab 包含这种类型的文件。例如，要启用 `.proto` 文件，请添加以下代码：

   ```plaintext
   *.proto linguist-detectable=true
   ```

查看[支持的数据类型](https://github.com/github/linguist/blob/master/lib/linguist/languages.yml)列表。

此功能可能会使用过多的 CPU。
有关更多信息，请参阅故障排查部分<!--[故障排查部分](#repository-languages-excessive-cpu-use)-->。

<a id="supported-markup-languages"></a>

### 支持的标记语言

如果您的文件具有以下文件扩展名之一，极狐GitLab 会在 UI 中呈现文件标记语言的内容。

| 标记语言 | 扩展名 |
| --------------- | ---------- |
| 纯文本 | `txt` |
| Markdown<!--[Markdown](../../markdown.md)--> | `mdown`, `mkd`, `mkdn`, `md`, `markdown` |
| [reStructuredText](https://docutils.sourceforge.io/rst.html) | `rst` |
| AsciiDoc<!--[AsciiDoc](../../asciidoc.md)--> | `adoc`, `ad`, `asciidoc` |
| [Textile](https://textile-lang.com/) | `textile` |
| [Rdoc](https://rdoc.sourceforge.net/doc/index.html)  | `rdoc` |
| [Org mode](https://orgmode.org/) | `org` |
| [creole](http://www.wikicreole.org/) | `creole` |
| [MediaWiki](https://www.mediawiki.org/wiki/MediaWiki) | `wiki`, `mediawiki` |

### README 和 index 文件

当仓库中存在 `README` 或 `index` 文件时，极狐GitLab 会呈现其内容。
这些文件可以是纯文本文件，也可以是[支持的标记语言](#支持的标记语言) 的扩展名。

- 当 `README` 和`index` 文件都存在时，`README` 总是优先。
- 当多个文件名称相同但扩展名不同时，文件按字母顺序排列。任何没有扩展名的文件最后排序。
   例如，`README.adoc` 优先于`README.md`，`README.rst` 优先于`README`。

### OpenAPI 查看器

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/19515) in GitLab 12.6.
-->

极狐GitLab 可以呈现 OpenAPI 规范文件。文件名必须包含 `openapi` 或 `swagger`，扩展名必须是 `yaml`、`yml` 或 `json`。下面的例子都是正确的： 

- `openapi.yml`
- `openapi.yaml`
- `openapi.json`
- `swagger.yml`
- `swagger.yaml`
- `swagger.json`
- `gitlab_swagger.yml`
- `openapi_gitlab.yml`
- `OpenAPI.YML`
- `openapi.Yaml`
- `openapi.JSON`
- `openapi.gitlab.yml`
- `gitlab.openapi.yml`

要呈现 OpenAPI 文件：

1. 转到仓库中的 OpenAPI 文件。
1. 在 **显示源** 和 **编辑** 按钮之间，选择 **显示 OpenAPI**。 找到 OpenAPI 文件后，它会替换 **显示渲染后文件** 按钮。

## 仓库大小

> 引入于 15.3 版本，功能标志 `gitaly_revlist_for_repo_size` 和 `gitaly_catfile_repo_size` 用于替代仓库大小计算。

FLAG:
在私有化部署版实例上，默认情况下，极狐GitLab 使用 `du -sk` 命令来确定仓库的大小。极狐GitLab 可以使用 `git-rev-list`（使用功能标志 `gitaly_revlist_for_repo_size` 启用）或 `git-cat-file`（使用功能标志 `gitaly_catfile_repo_size` 启用）。 要在不同的计算方法之间切换，请让管理员[启用或禁用](../../../administration/feature_flags.md)这些功能标志。

**项目信息**页面显示了仓库中所有文件的大小。大小最多每 15 分钟更新一次。文件大小包括仓库文件、产物和 LFS。

由于压缩、清理和其他因素，大小可能因实例而略有不同。

管理员可以设置仓库大小限制<!--[仓库大小限制](../../admin_area/settings/account_and_limit_settings.md)。 [GitLab 为 GitLab.com 设置大小限制](../../gitlab_com/index.md#account-and-limit-settings)。-->

## 仓库贡献者图

所有代码贡献者都显示在您项目的 **仓库 > 贡献者** 下。

该图按从贡献最多到最少的顺序显示贡献者。

![contributors to code](img/contributors_graph.png)

## 仓库分支图

仓库图显示仓库网络的可视历史记录，包括分支和合并。
此图可以帮助您可视化仓库中使用的 Git 流策略。

转到您项目的 **仓库 > 分支图**。

![repository Git flow](img/repo_graph.png)

<a id="what-happens-when-a-repository-path-changes"></a>

## 仓库路径更改时会发生什么

当仓库路径更改时，系统会通过重定向处理从旧位置到新位置的转换。

当您[重命名用户](../../profile/index.md#更改您的用户名)，[更改群组路径](../../group/manage.md#change-a-groups-path)或重命名仓库<!--[重命名仓库](../settings/index.md#renaming-a-repository)-->：

- 命名空间及其下的所有内容（如项目）的 URL 被重定向到新 URL。
- 命名空间下项目的 Git 远端 URL 重定向到新的远端 URL。当您推送或拉取到已更改位置的仓库时，会显示一条警告消息以更新您的远端。自动化脚本或 Git 客户端在重命名后继续工作。
- 只要原始路径未被其他群组、用户或项目声明，重定向就可用。

<!--
## Related links

- [GitLab Workflow VS Code extension](vscode.md)
-->

## 故障排查

### 仓库语言：过度使用 CPU

为了确定仓库文件中的语言，极狐GitLab 使用 Ruby gem。
当 gem 解析文件以确定它是哪种类型时，该进程可能会使用过多的 CPU。
gem 包含一个 [启发式配置文件](https://github.com/github/linguist/blob/master/lib/linguist/heuristics.yml)，用于定义必须解析哪些文件扩展名。

带有 `.txt` 扩展名的文件和带有 gem 未定义扩展名的 XML 文件可能会占用过多的 CPU。

解决方法是指定要分配给特定文件扩展名的语言。
同样的方法还应该允许修复错误识别的文件类型。

1. 确定要指定的语言。gem 包含一个[已知数据类型的配置文件](https://github.com/github/linguist/blob/master/lib/linguist/languages.yml)。
    要为文本文件添加条目，例如：

   ```yaml
   Text:
     type: prose
     wrap: true
     aliases:
     - fundamental
     - plain text
     extensions:
     - ".txt"
   ```

1. 在您的仓库根目录中添加或修改 `.gitattributes`：

   ```plaintext
   *.txt linguist-language=Text
   ```

  `*.txt` 文件在启发式文件中有一个条目。此示例防止解析这些文件。

WARNING:
[CI/CD `includes` 关键字](../../../ci/yaml/includes.md)不能遵循项目重定向。当配置为使用 `includes` 从重命名或移动的项目中获取配置时，流水线失败并出现语法错误。

<!--
### Search sequence of pushes to a repository

If it seems that a commit has gone "missing", search the sequence of pushes to a repository.
[This StackOverflow article](https://stackoverflow.com/questions/13468027/the-mystery-of-the-missing-commit-across-merges)
describes how you can end up in this state without a force push. Another cause can be a misconfigured [server hook](../../../administration/server_hooks.md) that changes a HEAD ref in a `git reset` operation.

If you look at the output from the sample code below for the target branch, you
see a discontinuity in the from/to commits as you step through the output.
The `commit_from` of each new push should equal the `commit_to` of the previous push.
A break in that sequence indicates one or more commits have been "lost" from the repository history.

Using the [rails console](../../../administration/operations/rails_console.md#starting-a-rails-console-session), the following example checks the last 100 pushes and prints the `commit_from` and `commit_to` entries:

```ruby
p = Project.find_by_full_path('project/path')
p.events.pushed_action.last(100).each do |e|
  puts "%-20.20s %8s...%8s (%s)", e.push_event_payload[:ref], e.push_event_payload[:commit_from], e.push_event_payload[:commit_to], e.author.try(:username)
end ; nil
```

Example output showing break in sequence at line 4:

```plaintext
master f21b07713251e04575908149bdc8ac1f105aabc3...6bc56c1f46244792222f6c85b11606933af171de root
master 6bc56c1f46244792222f6c85b11606933af171de...132da6064f5d3453d445fd7cb452b148705bdc1b root
master 132da6064f5d3453d445fd7cb452b148705bdc1b...a62e1e693150a2e46ace0ce696cd4a52856dfa65 root
master 58b07b719a4b0039fec810efa52f479ba1b84756...f05321a5b5728bd8a89b7bf530aa44043c951dce root
master f05321a5b5728bd8a89b7bf530aa44043c951dce...7d02e575fd790e76a3284ee435368279a5eb3773 root
```
-->
