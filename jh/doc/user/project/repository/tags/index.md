---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 标签 **(FREE)**

在 Git 中，标签标记代码仓库历史中的一个重要节点。
Git 支持两种类型的标签：

- **轻量级标签**指向特定的提交，不包含其他信息，也称为软标签。您可以根据需要创建或删除它们。
- **注释的标签**包含元数据，可以签名来进行验证，并且不能更改。

标签的创建或删除可以用作自动化的触发器，包括：

- 使用 [webhook](../../integrations/webhook_events.md#tag-events) 自动执行 Slack 通知等操作。
- 发出更新[仓库镜像](../mirror/index.md)的信号。
- 使用 [`if: $CI_COMMIT_TAG`](../../../../ci/jobs/job_control.md#common-if-clauses-for-rules) 运行 CI/CD 流水线。

当您[创建一个发布](../../releases/index.md)时，极狐GitLab 也会创建一个标签来标记发布点。许多项目将注释的发布标签与稳定分支结合在一起。考虑自动设置部署或发布标签。

要防止用户使用 `git push` 删除标签，请创建一个[推送规则](../push_rules.md)。

在极狐GitLab UI 中，每个标签显示：

![Example of a single tag](img/tag-display_v15_9.png)

- 标签名称（**{tag}**）。
- 可选。如果标签是[受保护的](../../protected_tags.md)，显示一个**受保护**徽章。
- 提交 SHA（**{commit}**），链接到提交内容。
- 提交标题和创建日期。
- 可选。发布的链接（**{rocket}**）。
- 可选。如果流水线已运行，则显示当前流水线状态。
- 源代码和关联标签的产物的下载链接。
- [**创建发布**](../../releases/index.md#create-a-release) (**{pencil}**) 的链接。
- 删除标签的链接。

## 查看项目的标签

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **仓库 > 标签**。

## 在提交列表中查看有标签的提交

> 引入于 15.10 版本。

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **仓库 > 提交**。
1. 具有标签的提交，标有标签图标 (**{tag}**) 和标签名称。以下示例显示标签为 `v1.26.0` 的提交：

   ![A tagged commit in the Commits view](img/tags_commits_view_v15_10.png)

要查看此标签中的提交列表，请选择标签名称。

## 创建标签

您可以从命令行或极狐GitLab UI 创建标签。

### 命令行

要从命令行创建轻量级或注释的标签，并将其推送到上游：

1. 要创建轻量级标签，请运行命令 `git tag TAG_NAME`，将 `TAG_NAME` 更改为您想要的标签名称。
1. 要创建注释的标签，请使用 `git tag` 的一个版本运行命令：

   ```shell
   # In this short version, the annotated tag's name is "v1.0",
   # and the message is "Version 1.0".
   git tag -a v1.0 -m "Version 1.0"

   # Use this version to write a longer tag message
   # for annotated tag "v1.0" in your text editor.
   git tag -a v1.0
   ```

1. 使用 `git push origin --tags` 将您的标签推送到上游。

### UI

要从 UI 创建标签：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **仓库 > 标签**。
1. 选择 **新建标签**。
1. 提供一个 **标签名称**。
1. 选择现有分支名称、标签或提交 SHA。
1. 可选。添加 **消息** 可以创建注释的标签，或留空可以创建轻量级标签。
1. 选择 **创建标签**。

<!--
## Related topics

- [Tagging](https://git-scm.com/book/en/v2/Git-Basics-Tagging) Git reference page.
- [Protected tags](../../protected_tags.md).
- [Tags API](../../../../api/tags.md).
-->
