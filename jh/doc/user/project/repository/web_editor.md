---
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# Web Editor **(FREE)**

您可以使用 Web Editor，直接从 UI 对单个文件进行更改。要更改多个文件，请参阅 [Web IDE](../web_ide/index.md)。

在 Web Editor 中，您可以：

- [创建文件](#create-a-file)。
- [编辑文件](#edit-a-file)。
- [上传文件](#upload-a-file)。
- [创建目录](#create-a-directory)。
- [创建分支](#create-a-branch)。
- [创建标签](#create-a-tag)。

默认情况下，您的主要电子邮件地址被用于您通过 Web 编辑器提交的任何更改。

<a id="create-a-file"></a>

## 创建文件

要在 Web 编辑器中创建文本文件：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在项目仪表盘或仓库中，选择分支名称旁边的加号图标 (**{plus}**)。
1. 从下拉列表中选择 **新建文件**。
1. 完成配置字段。
1. 要使用新文件创建合并请求，如果您选择了一个**目标分支**而不是[默认分支（例如 `main`)](../../../user/project/repository/branches/default.md)，请确保选中 **使用这些更改开始新的合并请求** 复选框。
1. 选择 **提交更改**。

### 使用模板创建文件

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **仓库 > 文件**。
1. 在项目名称旁边，选择加号图标 (**{plus}**) 显示下拉列表，然后从列表中选择 **新建文件**。
1. 对于 **文件名称**，提供极狐GitLab 为其提供模板的文件名之一：
   - `.gitignore`
   - `.gitlab-ci.yml`
   - `LICENSE`
   - `Dockerfile`
1. 选择 **应用模板**，然后选择您要应用的模板。
1. 对文件进行更改。
1. 提供 **提交消息**。
1. 输入要合并到的**目标分支**。要使用您的更改创建一个新的合并请求，请输入一个非[默认分支](../../../user/project/repository/branches/default.md)的分支名称。
1. 选择 **提交更改** 将提交添加到您的分支。

<a id="edit-a-file"></a>

## 编辑文件

要在 Web Editor 中编辑文本文件：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 转到您的文件。
1. 在文件的右上角，选择 **编辑**。

   如果 **编辑** 按钮不可见：

   1. 在 **在 Web IDE 中打开** 或 **在 Gitpod 打开** 旁边，选择向下箭头 (**{chevron-lg-down}**)。
   1. 从下拉列表中，选择 **编辑** 作为默认设置。
   1. 选择 **编辑**。

### 键盘快捷键

当您在 Web Editor 中[编辑文件](#edit-a-file) 时，您可以使用与 Web IDE 相同的键盘快捷键。请参阅[可用的快捷方式](../../shortcuts.md#web-ide)。

### 预览 Markdown

> 引入于 15.6 版本。

在 Web Editor 中预览 Markdown 内容：

1. [编辑文件](#edit-a-file)。
1. 执行以下操作之一：
    - 选择 **预览** 选项卡。
    - 从上下文菜单中，选择 **预览 Markdown**。

在 **预览** 选项卡中，您可以在内容旁边看到实时 Markdown 预览。

要关闭预览面板，请执行以下操作之一：

- 选择 **编辑** 选项卡。
- 从上下文菜单中，选择 **隐藏实时预览**。

<a id="link-to-specific-lines"></a>

### 链接到特定行

> 引入于 13.11 版本。

要链接到 Web Editor 中的单行或多行，请将信息添加到 URL 的文件名部分。例如：

- `MY_FILE.js#L3` 突出显示 `MY_FILE.js` 中的第 3 行。
- `MY_FILE.js#L3-10` 突出显示 `MY_FILE.js` 中的第 3 到 10 行。

要链接到单行，您还可以：

1. [编辑文件](#edit-a-file)。
1. 选择行号。

<a id="upload-a-file"></a>

## 上传文件

在 Web Editor 中上传二进制文件：

<!-- This list is duplicated at doc/gitlab-basics/add-file.md#from-the-ui -->
<!-- For why we duplicated the info, see https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111072#note_1267429478 -->

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在项目仪表盘或仓库中，选择分支名称旁边的加号图标 (**{plus}**)。
1. 在下拉列表中选择 **上传文件**。
1. 完成配置字段。要使用上传的文件创建合并请求，请确保 **使用这些更改开始新的合并请求** 开关已打开。
1. 选择 **上传文件**。

<a id="create-a-directory"></a>

## 创建目录

在 Web Editor 中创建目录：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在项目仪表盘或仓库中，选择分支名称旁边的加号图标 (**{plus}**)。
1. 在下拉列表中选择 **新建目录**。
1. 完成配置字段。要使用新目录创建合并请求，请确保 **使用这些更改开始新的合并请求** 开关已打开。
1. 选择 **创建目录**。

<a id="create-a-branch"></a>

## 创建分支

在 Web Editor 中创建[分支](branches/index.md)：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在项目仪表盘或仓库中，选择分支名称旁边的加号图标 (**{plus}**)。
1. 在下拉列表中选择 **新建分支**。
1. 完成配置字段。
1. 选择 **创建分支**。

<a id="create-a-tag"></a>

## 创建标签

您可以创建[标签](tags/index.md)，来记录里程碑，例如生产版本和候选版本。要在 Web Editor 中创建标签：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在项目仪表盘或仓库中，选择分支名称旁边的加号图标 (**{plus}**)。
1. 在下拉列表中选择 **新建标签**。
1. 完成配置字段。
1. 选择 **创建标签**。
