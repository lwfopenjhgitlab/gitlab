---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, howto
---

# 推送规则 **(PREMIUM)**

推送规则是 [pre-receive Git 钩子](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks)，您可以在用户友好的界面中启用。推送规则使您可以更好地控制可以推送和不可以推送到仓库的内容。虽然极狐GitLab 提供受保护的分支功能，但您可能需要更具体的规则，例如：

- 评估提交的内容。
- 确认提交消息与预期格式匹配。
- 执行分支名称规则。
- 评估文件的详细信息。
- 防止删除 Git 标签。

极狐GitLab 对推送规则中的正则表达式使用 [RE2 语法](https://github.com/google/re2/wiki/Syntax)。您可以在 [regex101 正则表达式测试器](https://regex101.com/)上对其进行测试。

对于自定义推送规则，请使用服务器钩子。

## 启用全局推送规则

您可以为所有要继承的新项目创建推送规则，但可以在项目级别或[群组级别](../../group/access_and_permissions.md#group-push-rules)覆盖它们。
配置全局推送规则后创建的所有项目都继承此配置。但是，必须使用[覆盖每个项目的全局推送规则](#override-global-push-rules-per-project)中描述的过程，手动更新每个现有项目。

先决条件：

- 您必须是管理员。

创建全局推送规则：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，选择 **推送规则**。
1. 展开 **推送规则**。
1. 设置您想要的规则。
1. 选择 **保存推送规则**。

<a id="override-global-push-rules-per-project"></a>

## 覆盖每个项目的全局推送规则

单个项目的推送规则会覆盖全局推送规则。
要覆盖特定项目的全局推送规则，或更新现有项目的规则以匹配新的全局推送规则：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **推送规则**。
1. 设置您想要的规则。
1. 选择 **保存推送规则**。

## 验证用户

使用以下规则来验证提交的用户。

- **拒绝未经验证的用户**：用户必须有一个已确认的电子邮件地址。
- **检查提交作者是否是 GitLab 用户**：提交作者和提交者必须具有经过极狐GitLab 验证的电子邮件地址。
- **提交作者的电子邮件**：作者和提交者的电子邮件地址都必须与正则表达式匹配。要允许任何电子邮件地址，请留空。

## 验证提交消息

以下规则用于您的提交消息。

- **在提交消息中的需要表达式**：消息必须与表达式匹配。要允许任何提交消息，请留空。使用多行模式，可以使用 `(?-m)` 禁用。

   例如，如果每个提交都应该引用 Jira 议题（例如 `Refactored css. Fixes JIRA-123.`），则正则表达式将是 `JIRA\-\d+`。
- **提交消息中的拒绝表达式**：提交消息不能与表达式匹配。要允许任何提交消息，请留空。使用多行模式，可以使用 `(?-m)` 禁用。

## 拒绝未经 DCO 认证的提交

> 引入于 15.5 版本。

使用 [Developer Certificate of Origin](https://developercertificate.org/) (DCO) 签名的提交证明贡献者有权提交和编写在该提交中贡献的代码。
您可以要求对项目的所有提交都遵守 DCO。此推送规则要求在每条提交消息中都有一个 `Signed-off-by:`，并拒绝任何缺少它的提交。

## 验证分支名称

要验证您的分支名称，请为**分支名称**输入正则表达式。
要允许任何分支名称，请留空。始终允许您的[默认分支](branches/default.md)。出于安全目的，某些格式的分支名称默认受到限制。禁止使用 40 个十六进制字符的名称，类似于 Git 提交哈希。

一些验证示例如下：

- 分支必须以 `JIRA-` 开头。

  ```plaintext
  `^JIRA-`
  ```

- 分支必须以 `-JIRA` 结尾。

  ```plaintext
  `-JIRA$`
  ```

- 分支的长度必须在 `4` 到 `15` 个字符之间，只接受小写字母、数字和破折号。

  ```plaintext
  `^[a-z0-9\\-]{4,15}$`
  ```

## 防止意外后果

使用以下规则来防止意外后果。

- **拒绝未签名的提交**：提交必须通过 [GPG](gpg_signed_commits/index.md) 签名。此规则可以阻止一些合法提交 [在 Web IDE 中创建](#reject-unsigned-commits-push-rule-disables-web-ide)，并允许[在 UI 中创建的未签名提交](#unsigned-commits-created-in-the-gitlab-ui)。
- **不允许用户使用 `git push` 删除 Git 标签**：用户不能使用 `git push` 删除 Git 标签。用户仍然可以删除 UI 中的标签。

## 验证文件

使用以下规则来验证提交中包含的文件。

- **防止推送秘密文件**：文件不得包含 [secret](#prevent-pushing-secrets-to-the-repository)。
- **禁止的文件名**：仓库中不存在的文件不得与正则表达式匹配。要允许所有文件名，请留空。请参阅[常见示例](#prohibit-files-by-name)。
- **最大文件大小**：添加或更新的文件不得超过此文件大小（以 MB 为单位）。要允许任何大小的文件，请设置为 `0`。Git LFS 跟踪的文件被豁免。

<a id="prevent-pushing-secrets-to-the-repository"></a>

## 防止将 secret 推送到仓库

> 移动到专业版于 13.9 版本。

绝不能将诸如凭证文件和 SSH 私钥之类的 secret 提交给版本控制系统。在极狐GitLab 中，您可以使用预定义的文件列表来阻止仓库中的这些文件。任何包含与列表匹配的文件的合并请求都将被阻止合并。
已提交到仓库的文件不受此推送规则的限制。
您必须使用[覆盖每个项目的全局推送规则](#override-global-push-rules-per-project)中描述的过程，更新现有项目的配置来使用该规则。

下面列出了此规则阻止的文件。有关标准的完整列表，请参阅 [`files_denylist.yml`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/gitlab/checks/files_denylist.yml)。

- AWS CLI credential blobs：

  - `.aws/credentials`
  - `aws/credentials`
  - `homefolder/aws/credentials`

- 私有 RSA SSH 密钥：

  - `/ssh/id_rsa`
  - `/.ssh/personal_rsa`
  - `/config/server_rsa`
  - `id_rsa`
  - `.id_rsa`

- 私有 DSA SSH 密钥：

  - `/ssh/id_dsa`
  - `/.ssh/personal_dsa`
  - `/config/server_dsa`
  - `id_dsa`
  - `.id_dsa`

- 私有 ed25519 SSH 密钥：

  - `/ssh/id_ed25519`
  - `/.ssh/personal_ed25519`
  - `/config/server_ed25519`
  - `id_ed25519`
  - `.id_ed25519`

- 私有 ECDSA SSH 密钥：

  - `/ssh/id_ecdsa`
  - `/.ssh/personal_ecdsa`
  - `/config/server_ecdsa`
  - `id_ecdsa`
  - `.id_ecdsa`

- 任何以这些后缀结尾的文件：

  - `*.pem`
  - `*.key`
  - `*.history`
  - `*_history`

<a id="prohibit-files-by-name"></a>

## 禁止的文件名

> 移动到专业版于 13.9 版本。

在 Git 中，文件名既包括文件名，也包括文件名之前的所有目录。
当您执行 `git push` 命令时，推送中的每个文件名都会与**禁止的文件名**中的正则表达式进行比较。

您的**禁止的文件名**推送规则中的正则表达式可以包含多个要排除的独立匹配项。您可以将文件名广泛匹配到仓库中的任何位置，或仅在某些位置进行限制。文件名匹配也可以是部分的，并按扩展名排除文件类型。

这些示例使用 regex（正则表达式）字符串边界字符来匹配字符串的开头 (`^`) 和结尾 (`$`)，还包括目录路径或文件名可以包含 `.` 或 `/` 的实例。如果您想在匹配条件中将它们用作普通字符，则必须使用反斜杠 `\\` 对这两个特殊的正则表达式字符进行转义。

- **防止将 `.exe` 文件推送到仓库中的任何位置** - 此正则表达式匹配任何末尾包含 `.exe` 的文件名：

  ```plaintext
  \.exe$
  ```

- **防止在仓库根目录中推送特定的配置文件**

  ```plaintext
  ^config\.yml$
  ```

- **防止将特定配置文件推送到已知目录**

  ```plaintext
  ^directory-name\/config\.yml$
  ```

- **防止将特定文件推送到仓库中的任何位置** - 此示例测试任何名为 `install.exe` 的文件。带括号的表达式 `(^|\/)` 匹配目录分隔符后面的文件或仓库根目录中的文件：

  ```plaintext
  (^|\/)install\.exe$
  ```

- **将所有前面的表达式组合成一个表达式** - 前面的表达式依赖于字符串结尾字符 `$`。我们可以将每个表达式的该部分移动到匹配条件分组集合的末尾，将其附加到所有匹配项：

  ```plaintext
  (\.exe|^config\.yml|^directory-name\/config\.yml|(^|\/)install\.exe)$
  ```

## 故障排除

<a id="reject-unsigned-commits-push-rule-disables-web-ide"></a>

### 拒绝未签名的提交推送规则禁用 Web IDE

在 13.10 版本中，如果项目具有**拒绝未签名的提交**推送规则，则用户无法通过 Web IDE 创建提交。

要允许使用此推送规则在项目上通过 Web IDE 提交，管理员必须禁用功能标志 `reject_unsigned_commits_by_gitlab`。

```ruby
Feature.disable(:reject_unsigned_commits_by_gitlab)
```

<a id="unsigned-commits-created-in-the-gitlab-ui"></a>

### 在 UI 中创建的未签名提交

**拒绝未签名的提交**推送规则忽略由系统验证和创建的提交（通过 UI 或 API）。启用此推送规则后，如果在极狐GitLab 本身中创建了提交，未签名的提交可能仍会出现在提交历史记录中。正如预期的那样，在极狐GitLab 外部创建并推送到仓库的提交被拒绝。
