---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 项目派生工作流 **(FREE)**

只要有可能，建议在公共 Git 仓库中工作并使用[分支策略](../../../topics/gitlab_flow.md)来管理您的工作。但是，如果您对要贡献的仓库没有写入权限，则可以创建一个分支。

派生是仓库及其所有分支的个人副本，您可以在您选择的命名空间中创建它。通过这种方式，您可以在自己的派生项目中进行更改，并通过合并请求将它们提交到您无权访问的仓库。

<a id="create-a-fork"></a>

## 创建派生

在极狐GitLab 中派生现有项目：

1. 在项目主页的右上角，选择 **{fork}** **派生**：
   ![Fork this project](img/forking_workflow_fork_button_v13_10.png)
1. 可选。编辑 **项目名称**。
1. 对于 **项目 URL**, 选择您的派生项目应该属于的命名空间。
1. 添加一个 **项目标识串**。此值成为您的派生项目的 URL 的一部分，在命名空间中必须是唯一的。
1. 可选。添加 **项目描述**。
1. 为您的派生项目选择**可见性级别**。有关可见性级别的更多信息，请阅读[项目和群组可见性](../../public_access.md)。
1. 选择 **派生项目**。

极狐GitLab 创建您的派生项目，并将您重定向到新的派生项目页面。

## 更新您的派生项目

要将上游仓库中的最新更改复制到您的派生项目中，您可以使用命令行更新。专业版和更高级别版本还可以[将派生项目配置为拉取上游仓库的镜像](#with-repository-mirroring)。

### 使用命令行

要使用命令行更新您的派生项目，首先确保您已经为派生项目配置了一个 `upstream` 远端仓库：

1. 在本地克隆您的派生项目。有关详细信息，请参阅[克隆代码仓库](../../../gitlab-basics/start-using-git.md#clone-a-repository)。
1. 查看为您的派生项目配置的远端：

   ```shell
   git remote -v
   ```

1. 如果您的派生项目没有指向原始仓库的远端，请使用以下示例之一来配置名为 `upstream` 的远端：

   ```shell
   # Use this line to set any repository as your upstream after editing <upstream_url>
   git remote add upstream <upstream_url>

   # Use this line to set the main GitLab repository as your upstream
   git remote add upstream https://gitlab.com/gitlab-org/gitlab.git
   ```

   在确保您的本地副本配置了额外的远端之后，您就可以更新您的派生项目了。

1. 在您的本地副本中，确保您已检出[默认分支](branches/default.md)，将 `main` 替换为默认分支的名称：

   ```shell
   git checkout main
   ```

   如果 Git 识别出未暂存的更改，请在继续之前提交或存储它们。

1. 获取上游仓库的更改：

   ```shell
   git fetch upstream
   ```

1. 将更改拉入您的派生项目，将 `main` 替换为您正在更新的分支的名称：

   ```shell
   git pull upstream main
   ```

1. 将更改推送到服务器上的派生项目仓库：

   ```shell
   git push origin main
   ```

<a id="with-repository-mirroring"></a>

### 使用仓库镜像 **(PREMIUM)**

如果满足所有以下条件，则可以将派生项目配置为上游项目的镜像：

1. 您的订阅级别是专业版或更高。
1. 您在分支中创建所有更改（不是 `main`）。
1. 您不处理[机密问题的合并请求](../merge_requests/confidential.md)，这需要更改 `main`。

[仓库镜像](mirror/index.md)使您的派生项目与原始仓库同步。
此方法每小时更新一次您的派生项目，无需手动 `git pull`。
有关说明，请阅读[配置拉取镜像](mirror/pull.md#configure-pull-mirroring)。

WARNING:
使用镜像，在批准合并请求之前，系统会要求您进行同步。您应该自动化镜像。

## 合并到上游

当您准备好将代码发送回上游项目时，[创建合并请求](../merge_requests/creating_merge_requests.md)。对于 **源分支**，选择您的派生项目的分支。对于**目标分支**，选择原始项目的分支。

NOTE:
创建合并请求时，如果派生项目的可见性比父项目的限制更多（例如派生项目是私有的，父项目是公共的），则目标分支默认为派生项目的默认分支。这可以防止潜在地暴露派生项目的私有代码。

![Selecting branches](img/forking_workflow_branch_select_v15_9.png)

然后您可以添加标记、里程碑，并将合并请求分配给可以查看您的更改的人员。然后单击 **提交合并请求** 以结束该过程。成功合并后，您的更改将添加到您要合并到的仓库和分支中。

<a id="unlink-a-fork"></a>

## 取消关联派生项目

删除派生关系会取消您的派生项目与其上游项目的关联。
然后您的派生项目就变成了一个独立的项目。

先决条件：

- 您必须是项目所有者才能取消关联派生。

WARNING:
如果删除派生关系，则无法向源发送合并请求。
如果有人派生了您的项目，他们的派生项目也会失去关联。
要恢复派生关系，需要[使用 API](../../../api/projects.md#create-a-forked-fromto-relation-between-existing-projects)。

删除派生关系：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 在 **删除派生关系** 部分，选择 **删除派生关系**。
1. 要确认，请输入项目路径并选择 **确认**。

当您取消关联使用[哈希存储池](../../../administration/repository_storage_types.md#hashed-object-pools)，与另一个仓库共享对象的派生项目时：

- 所有对象都从池中复制到您的派生项目中。
- 复制过程完成后，不会将存储池中的进一步更新传播到您的派生项目。

<!--
## Related topics

- GitLab blog post: [Keep your fork up to date with its origin](https://about.gitlab.com/blog/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/)
- GitLab community forum: [Refreshing a fork](https://forum.gitlab.com/t/refreshing-a-fork/)
-->

## 故障排除

### 错误：`An error occurred while forking the project. Please try again`

此错误可能是由于派生项目和新命名空间之间的共享 runner 设置不匹配造成的。有关详细信息，请参阅 Runner 文档中的[派生部分](../../../ci/runners/configure_runners.md#forks)。

### 删除派生关系失败

如果通过 UI 或 API 删除派生关系无效，您可以尝试使用 [Rails 控制台会话](../../../administration/operations/rails_console.md#starting-a-rails-console-session)：

```ruby
p = Project.find_by_full_path('<project_path>')
u = User.find_by_username('<username>')
Projects::UnlinkForkService.new(p, u).execute
```
