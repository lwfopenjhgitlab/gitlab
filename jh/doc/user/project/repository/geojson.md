---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

# GeoJSON 文件 **(FREE)**

> 引入于 16.1 版本。

GeoJSON 文件是一种使用 JavaScript 对象表示法 (JSON) 编码地理数据结构的格式。
它通常用于表示地理特征，例如点、线和多边形及其相关属性。

添加到存储库后，带有 `.geojson` 扩展名的文件在极狐GitLab 中查看时将呈现为包含 GeoJSON 数据的地图。

地图数据来自 [OpenStreetMap](https://www.openstreetmap.org/)，并遵循[开放数据库许可证](https://www.openstreetmap.org/copyright)。

![GeoJSON file rendered as a map](img/geo_json_file_rendered_v16_1.png)
