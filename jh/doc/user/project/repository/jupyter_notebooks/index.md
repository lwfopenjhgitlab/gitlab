---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---
# Jupyter Notebook 文件 **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/2508/) in GitLab 9.1.
-->

[Jupyter](https://jupyter.org/) Notebook（以前的 IPython Notebook）文件用于许多领域的交互式计算，包含用户会话的完整记录，包括代码、叙述文本、方程和丰富的输出。

添加到仓库后，带有`.ipynb` 扩展名的 Jupyter Notebook 在查看时会呈现为 HTML：

![Jupyter Notebook Rich Output](img/jupyter_notebook.png)

在极狐GitLab 中查看时，交互功能（包括 JavaScript plots）不起作用。

## 更清洁的差异和原始差异 

> - 引入于 14.5 版本，作为 Alpha 功能，功能标志为 `jupyter_clean_diffs`。默认启用。
> - 功能切换开关重新引入于 15.0 版本，功能标志为 `ipynb_semantic_diff`。默认启用。
> - 一般可用于 15.6 版本。功能标志 `ipynb_semantic_diff` 已删除。

当提交包括对 Jupyter Notebook 文件的更改时：

- 系统将机器可读的 `.ipynb` 文件转换为人类可读的 Markdown 文件。
- 显示包含语法突出显示的更清晰版本的差异。
- 在提交和比较页面上启用原始差异和渲染差异之间的切换。（在合并请求页面上不可用。）
- 渲染差异上的图像。

当 notebook 太大时，不会生成更清晰的 notebook 差异。

![Jupyter Notebook Clean Diff](img/jupyter_notebook_diff_v14_5.png)

## Jupyter Git 集成

Jupyter 可以配置为具有仓库访问权限的 OAuth 应用程序，代表经过身份验证的用户。 <!--See the
[Runbooks documentation](../../../project/clusters/runbooks/index.md) for an
example configuration.-->
