---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: concepts, howto
---

# 默认分支 **(FREE)**

当您创建一个新的[项目](../../index.md)时，极狐GitLab 在仓库中创建一个默认分支。默认分支具有其它分支不共享的特殊配置选项：

- 无法删除。
- <!--[初始保护](../../protected_branches.md#protected-branches)-->初始保护可防止强制推送。
- 当合并请求使用[议题关闭模式](../../issues/managing_issues.md#自动关闭议题)关闭议题时，工作将合并到此分支中。

[新项目](../../index.md)默认分支的名称取决于您的管理员所做的任何实例级或群组级配置更改。
极狐GitLab 首先检查特定的自定义设置，然后在更广泛的级别进行检查，仅当未设置自定义设置时才使用极狐GitLab 默认值：

1. [项目特定](#更改项目的默认分支名称)自定义默认分支名称。
1. [子组级别](#群组级别自定义初始分支名称)自定义默认分支名称。
1. [群组级别](#群组级别自定义初始分支名称)自定义默认分支名称。
1. [实例级别](#实例级别自定义初始分支名称)自定义默认分支名称。
1. 如果任何级别都没有设置自定义默认分支名称，极狐GitLab 默认为：
    - `main`：使用 14.0 或更高版本创建的项目。
    - `master`：在 14.0 版本之前创建的项目。

在 GitLab UI 中，您可以在任何级别更改默认值。极狐GitLab 还提供了 [您需要的 Git 命令](#更新仓库中的默认分支名称)来更新您的仓库副本。

## 更改项目的默认分支名称

要更新单个[项目](../../index.md)的默认分支名称：

1. 至少使用维护者角色登录极狐GitLab。
1. 在左侧导航菜单中，转到 **设置 > 仓库**。
1. 展开 **分支默认值**，并选择一个新的默认分支。
1.（可选）选中 **自动关闭默认分支上的引用议题** 复选框，在合并请求[使用关闭模式](../../issues/managing_issues.md#自动关闭议题)。
1. 选择 **保存更改**。

API 用户还可以在创建或编辑项目时使用 Projects API<!--[Projects API](../../../../api/projects.md)--> 的 `default_branch` 属性。

## 更改实例或群组的默认分支名称

管理员可以在[实例级别](#实例级别自定义初始分支名称) 或[群组级别](#群组级别自定义初始分支名称)配置新的默认分支名称。

<a id="instance-level-custom-initial-branch-name"></a>

### 实例级别自定义初始分支名称 **(FREE SELF)**

> - 引入于 13.2 版本。
> - 功能标志移除于 13.12 版本。

自助管理实例的管理员可以为该实例上托管的项目自定义初始分支。单个群组和子组可以为其项目覆盖此实例范围的设置。

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **默认初始分支名称**。
1. 将默认初始分支更改为您选择的自定义名称。
1. 选择 **保存更改**。

更改设置后在此实例上创建的项目使用自定义分支名称，除非群组级别或子组级别配置覆盖它。

<a id="group-level-custom-initial-branch-name"></a>

### 群组级别自定义初始分支名称

> - 引入于 13.6 版本。

至少具有群组和子组所有者角色的用户可以为群组配置默认分支名称：

1. 转到群组 **设置 > 仓库**。
1. 展开 **默认分支**。
1. 将默认初始分支更改为您选择的自定义名称。
1. 选择 **保存更改**。

更改设置后，在此群组中创建的项目使用自定义分支名称，除非子组配置覆盖它。

## 保护初始默认分支 **(FREE SELF)**

实例管理员和群组所有者可以使用以下选项之一，在实例级别和群组级别定义应用于每个仓库的默认分支的分支保护：

- **未保护** - 开发人员和维护人员都可以推送新的提交和强制推送。
- **针对推送保护** - 开发人员不能推送新的提交，但可以接受分支的合并请求。维护人员可以推送到分支。
- **部分保护** - 开发人员和维护人员都可以推送新的提交，但不能强制推送。
- **完全保护** - 开发人员不能推送新的提交，但维护人员可以。 没有人可以强行推动。

<a id="instance-level-default-branch-protection"></a>

### 实例级默认分支保护 **(FREE SELF)**

此设置仅适用于每个仓库的默认分支。要保护其他分支，您必须：

- 在仓库中配置分支保护。
- 为群组配置分支保护。

私有化部署实例的管理员可以为托管在该实例上的项目自定义初始默认分支保护。各个群组和子组可以为其项目覆盖此实例范围的设置。

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **默认分支**。
1. 选择 **初始默认分支保护**。
1. 要允许群组所有者覆盖实例的默认分支保护，请选择 **允许所有者管理每个群组的默认分支保护**。
1. 选择 **保存修改**。

<a id="prevent-overrides-of-default-branch-protection"></a>

#### 防止覆盖默认分支保护 **(PREMIUM SELF)**

默认分支的实例级保护可以由群组的所有者在每个群组的基础上覆盖。在[专业版或更高版本](https://about.gitlab.cn/pricing/)中，管理员可以为群组所有者禁用此权限，执行实例级保护规则：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **默认分支** 部分。
1. 清除 **允许所有者管理每个群组的默认分支保护** 复选框。
1. 选择 **保存修改**。

NOTE:
管理员仍然可以更新群组的默认分支保护。

<a id="group-level-default-branch-protection"></a>

### 群组级别默认分支保护 **(PREMIUM)**

> - 引入于 12.9 版本
> - 设置已移动并重命名于 14.9 版本。

默认分支的实例级保护可以由群组的所有者在每个群组的基础上覆盖。在[专业版或更高版本](https://about.gitlab.cn/pricing/)中，管理员可以强制保护初始默认分支，从而为群组所有者锁定此设置。

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **默认分支**。
1. 选择 **初始默认分支保护**。
1. 选择 **保存修改**。

## 更新仓库中的默认分支名称

WARNING:
更改默认分支的名称可能会破坏测试、CI/CD 配置、服务、辅助实用程序以及仓库使用的任何集成。在更改此分支名称之前，请咨询您的项目所有者和维护人员。确保他们了解此更改的范围，包括对相关代码和脚本中旧分支名称的引用。

更改现有仓库的默认分支名称时，您应该通过重命名来保留默认分支的历史记录，而不是创建新分支。此示例重命名 Git 仓库（`example`）的默认分支。

1. 在您的本地命令行上，导航到您的 `example` 仓库，并确保您位于默认分支上：

   ```plaintext
   cd example
   git checkout master
   ```

1. 将现有的默认分支重命名为新名称（`main`）。参数 `-m` 将所有提交历史转移到新分支：

   ```plaintext
   git branch -m master main
   ```

1. 将新创建的 `main` 分支推送到上游，并设置本地分支跟踪同名的远程分支：

   ```plaintext
   git push -u origin main
   ```

1. 如果您打算删除旧的默认分支，更新 `HEAD` 以指向你的新默认分支 `main`：

   ```plaintext
   git symbolic-ref refs/remotes/origin/HEAD refs/remotes/origin/main
   ```

1. 至少使用维护者角色登录极狐GitLab，并按照说明[更改此项目的默认分支](#更改项目的默认分支名称)。选择 `main` 作为你的新默认分支。
1. 将新的 `main` 分支设置为受保护。
1. （可选）如果要删除旧的默认分支：
    1. 确认没有任何东西指向它。
    1. 删除远端分支：

      ```plaintext
      git push origin --delete master
      ```

      在您确认新的默认分支按预期工作后，您可以稍后删除该分支。

1. 将此更改通知您的项目贡献者，因为他们还必须采取一些步骤：

    - 贡献者应将新的默认分支拉到其仓库的本地副本。
    - 具有针对旧默认分支的开放合并请求的贡献者应手动将合并请求重新指向使用 `main`。

1. 在您的仓库中，更新对代码中旧分支名称的所有引用。
1. 在仓库之外的相关代码和脚本中更新对旧分支名称的引用，例如帮助实用程序和集成。

## 默认分支重命名重定向

> 引入于 14.1 版本

项目中特定文件或目录的 URL 嵌入了项目的默认分支名称，通常可以在文档或浏览器书签中找到。当您[更新仓库中的默认分支名称](#更新仓库中的默认分支名称) 时，这些 URL 会更改，并且必须更新。

为了缩短过渡期，每当项目的默认分支发生更改时，极狐GitLab 都会记录旧默认分支的名称。如果该分支被删除，则尝试查看其上的文件或目录将重定向到当前默认分支，而不是显示“未找到”页面。

<!--
## Resources

- [Configure a default branch for your wiki](../../wiki/index.md)
- [Discussion of default branch renaming](https://lore.kernel.org/git/pull.656.v4.git.1593009996.gitgitgadget@gmail.com/)
  on the Git mailing list
- [March 2021 blog post: The new Git default branch name](https://about.gitlab.com/blog/2021/03/10/new-git-default-branch-name/)
-->

## 故障排查

### Unable to change default branch: resets to current branch

当仓库中存在名为 `HEAD` 的分支时，通常会发生此问题。
要解决问题：

1. 在您的本地存储库中，创建一个新的临时分支并将其推送：

   ```shell
   git checkout -b tmp_default && git push -u origin tmp_default
   ```

1. 在极狐GitLab 中，继续[更改默认分支](#更改项目的默认分支名称)到该临时分支。
1. 从您的本地仓库中，删除 `HEAD` 分支：

   ```shell
   git push -d origin HEAD
   ```

1. 在极狐GitLab中，[更改默认分支](#更改项目的默认分支名称)到您要使用的。

### 查询 GraphQL 以获取默认分支

您可以使用 GraphQL 查询<!--[GraphQL 查询](../../../../api/graphql/index.md)-->来检索组中所有项目的默认分支。

要在单个结果页面中返回所有项目，请将 `GROUPNAME` 替换为您的群组的完整路径。系统返回第一页结果。 如果`hasNextPage` 为`true`，则可以通过将 `after: null` 中的 `null` 替换为 `endCursor` 的值来请求下一页： 

```graphql
{
 group(fullPath: "GROUPNAME") {
   projects(after: null) {
     pageInfo {
       hasNextPage
       endCursor
     }
     nodes {
       name
       repository {
         rootRef
       }
     }
   }
 }
}
```
