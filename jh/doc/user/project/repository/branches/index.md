---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
---

# 分支 **(FREE)**

分支是项目工作树的版本。当您创建一个新的[项目](../../index.md)时，极狐GitLab 会为您的代码仓库创建一个[默认分支](default.md)（无法删除）。您可以在项目、子组、群组或实例级别配置默认分支设置。

随着项目的发展，您的团队会[创建](../web_editor.md#create-a-branch)更多分支，您最好遵循[分支命名模式](#prefix-branch-names-with-issue-numbers)。
每个分支代表一组更改，允许并行完成开发工作。一个分支的开发工作不会影响另一个分支。

分支是项目发展的基础：

1. 首先，创建一个分支并向其添加提交。
1. 当工作准备好接受审核时，创建一个[合并请求](../../merge_requests/index.md)，提议合并您分支中的更改。要简化此过程，您应该遵循[分支命名模式](#prefix-branch-names-with-issue-numbers)。
1. 使用 [review app](../../../../ci/review_apps/index.md) 预览分支中的更改。
1. 在你的分支内容合并后，[删除合并的分支](#delete-merged-branches)。

## 创建分支

在 UI 中创建一个新分支：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **仓库 > 分支**。
1. 在右上角，选择 **新建分支**。
1. 输入一个 **分支名称**。
1. 在 **创建自** 中，选择分支的基础：现有分支、现有标签或提交 SHA。
1. 选择 **创建分支**。

### 在空白项目中创建

[空白项目](../../index.md#create-a-blank-project)不包含分支，但您可以添加分支。

先决条件：

- 您必须至少在项目中具有开发者角色。
- 除非您具有维护者或所有者角色，否则[默认分支保护](../../../group/manage.md#change-the-default-branch-protection-of-a-group)必须设置为 `Partially protected` 或 `Not protected`，才能将提交推送到默认分支。

添加一个[默认分支](default.md)到一个空项目：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 滚动到 **该项目仓库当前为空** 并选择要添加的文件类型。
1. 在 Web IDE 中，对该文件进行任何所需的更改，然后选择 **创建提交**。
1. 输入提交消息，然后选择 **提交**。

极狐GitLab 创建一个默认分支并将您的文件添加到其中。

### 从议题创建

查看议题时，您可以直接从该页面创建关联的分支。

先决条件：

- 您必须在项目中具有开发者或更高的角色。

从议题创建分支：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **议题** (**{issues}**) 并找到您的议题。
1. 在问题描述下方，找到 **创建合并请求** 下拉列表，然后选择 **{chevron-down}** 显示下拉列表。
1. 选择 **创建分支**。根据此项目的[默认样式](#configure-default-pattern-for-branch-names-from-issues)，提供默认的 **分支名称**。如果需要，您可以输入不同的 **分支名称**。
1. 选择 **创建分支**，根据您项目的[默认分支](default.md)创建分支。

## 管理和保护分支

极狐GitLab 为您提供多种方法来保护各个分支。这些方法确保您的分支从创建到删除都接受监督和质量检查：

- 项目中的[默认分支](default.md)获得额外保护。
- 配置[受保护的分支](../../protected_branches.md#protected-branches)来限制谁可以提交到一个分支，将其他分支合并到其中，或者将分支本身合并到另一个分支。
- 配置[批准规则](../../merge_requests/approvals/rules.md)来设置审核要求，包括[安全相关批准](../../merge_requests/approvals/rules.md#security-approvals)，然后分支才能合并。
- 与第三方[状态检查](../../merge_requests/status_checks.md)集成，可以确保您的分支内容符合您的质量标准。

您可以通过以下方式管理您的分支：

- 使用极狐GitLab 用户界面。
- 使用[命令行](../../../../gitlab-basics/start-using-git.md#create-a-branch)。
- 使用[分支 API](../../../../api/branches.md)。

### 查看所有分支

在极狐GitLab 用户界面中查看和管理您的分支：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **仓库 > 分支**。

在此页面上，您可以：

- 查看所有分支、活动分支或陈旧分支。
- 创建新分支。
- [比较分支](#compare-branches)。
- 删除合并的分支。

### 查看具有保护配置的分支

> - 引入于 15.1 版本，功能标志为 `branch_rules`。默认禁用。
> - 在 SaaS 上启用于 15.10 版本。
> - 在私有化部署版上启用于 15.11 版本。

FLAG:
在私有化部署版上，此功能默认可用。要隐藏此功能，需请求管理员[禁用功能分支](../../../feature_flags.md) `branch_rules`。在 SaaS 版上，此功能可用。

仓库中的分支可以通过多种方式[受保护](../../protected_branches.md)。您可以：

- 限制谁可以推送到分支。
- 限制谁可以合并分支。
- 需要批准所有更改。
- 需要外部测试才能通过。

**分支规则概览**页面显示了具有任何保护配置的所有分支及其保护方法：

![Example of a branch with configured protections](img/view_branch_protections_v15_10.png)

先决条件：

- 您必须至少在项目中具有维护者角色。

查看 **分支规则概览** 列表：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 仓库**。
1. 展开 **分支规则** 查看所有受保护的分支。
   - 为新分支添加保护：
     1. 选择 **添加分支规则**。
     1. 选择 **创建受保护的分支**。
   - 要查看有关现有分支保护的更多信息：
     1. 确定您想要了解更多信息的分支。
     1. 选择 **详情** 查看有关信息：
        - [分支保护](../../protected_branches.md)。
        - [批准规则](../../merge_requests/approvals/rules.md)。
        - [状态检查](../../merge_requests/status_checks.md)。

## 命名您的分支

如果您遵循[命名分支](#prefix-branch-names-with-issue-numbers)的标准，并配置遵循这些标准的分支名称，极狐GitLab 可以简化您的工作流程：

- 将合并请求关联到其父议题。
- 将一个分支关联到一个议题。
- 当关联的合并请求关闭并且关联的分支合并时，[关闭议题](../../issues/managing_issues.md#closing-issues-automatically)。

<a id="configure-default-pattern-for-branch-names-from-issues"></a>

### 为来自议题的分支名称配置默认样式

默认情况下，极狐GitLab 在从议题创建分支时使用样式 `%{id}-%{title}`，但您可以更改此样式。

先决条件：

- 您必须至少具有该项目的维护者角色。

要更改从议题创建的分支的默认样式：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 仓库** 并展开 **分支默认值**。
1. 滚动到 **分支名称模板** 并输入一个值。该字段支持这些变量：
   - `%{id}`：议题的数字 ID。
   - `%{title}`：议题的标题，修改为仅使用 Git 分支名称中可接受的字符。
1. 选择 **保存更改**。

<a id="prefix-branch-names-with-issue-numbers"></a>

### 在分支名称前加上议题编号

要简化合并请求的创建，请以议题编号作为您的分支名称开头。极狐GitLab 使用议题编号将数据导入合并请求：

- 该议题被标记为相关。议题和合并请求显示彼此的链接。
- 如果您的项目配置了[默认关闭样式](../../issues/managing_issues.md#default-closing-pattern)，合并合并请求时，[也关闭](../../issues/managing_issues.md#closing-issues-automatically)相关议题。
- 议题里程碑和标签被复制到合并请求中。

## 比较分支

> - 仓库过滤器搜索框功能引入于 13.10 版本。
> - 修订版本交换功能引入于 13.12 版本。

默认比较模式使用 `git diff from...to` 方法而不是 `git diff from to` 方法来比较分支。`git diff from...to` 方法提供了一个更易于阅读的差异，因为它不包括在创建源分支后对目标分支所做的无关更改。

NOTE:
`git diff from...to` 方法将来自拣选提交的所有更改显示为新更改。它使用合并基础而不是实际提交内容来比较分支。

比较仓库中的分支：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **仓库 > 比较修订版本**。
1. 选择 **源** 分支搜索你想要的分支。首先显示精确匹配。您可以使用运算符优化搜索：
   - `^` 匹配分支名称的开头：`^feat` 匹配 `feat/user-authentication`。
   - `$` 匹配分支名称的末尾：`widget$` 匹配`feat/search-box-widget`。
   - `*` 使用通配符匹配：`branch*cache*` 匹配 `fix/branch-search-cache-expiration`。
   - 您可以组合运算符：`^chore/*migration$` 匹配 `chore/user-data-migration`。
1. 选择 **目标** 仓库和分支。首先显示精确匹配。
1. 选择 **比较**，显示提交列表和更改的文件。要反转 **源分支** 和 **目标分支**，请选择 **交换版本**。

## 删除合并的分支

![Delete merged branches](img/delete_merged_branches.png)

此功能允许批量删除合并的分支。作为此操作的一部分，仅删除已合并到项目默认分支中且[不受保护](../../protected_branches.md)的分支。

清理合并请求时未自动删除的旧分支特别有用。

<!--
## Related topics

- [Protected branches](../../protected_branches.md)
- [Branches API](../../../../api/branches.md)
- [Protected Branches API](../../../../api/protected_branches.md)
- [Getting started with Git](../../../../topics/git/index.md)
-->

## 故障排除

### 包含相同提交的多个分支

在更深的技术层面上，Git 分支不是单独的实体，而是附加到一组提交 SHA 的标签。当极狐GitLab 确定分支是否已合并时，它会检查目标分支是否存在那些提交的 SHA。
当两个合并请求包含相同的提交时，可能会导致意外结果。在此示例中，分支 `B` 和 `C` 都从分支 `A` 上的相同提交（`3`）开始：

```mermaid
gitGraph
    commit id:"a"
    branch "branch A"
    commit id:"b"
    commit id:"c" type: HIGHLIGHT
    branch "branch B"
    commit id:"d"
    checkout "branch A"
    branch "branch C"
    commit id:"e"
    checkout main
    merge "branch B" id:"merges commits b, c, d"
```

如果您合并分支 `B`，分支 `A` 也会显示为已合并（您无需执行任何操作），因为来自分支 `A` 的所有提交现在都出现在目标分支 `main` 中。分支 `C` 保持未合并，因为提交 `5` 不是分支 `A` 或 `B` 的一部分。

合并请求 `A` 保持合并状态，即使您尝试将新提交推送到其分支。如果合并请求 `A` 中的任何更改仍未合并（因为它们不是合并请求 `A` 的一部分），请为它们打开一个新的合并请求。

### 错误：存在不明确的 `HEAD` 分支

在 2.16.0 之前的 Git 版本中，您可以创建一个名为 `HEAD` 的分支。
这个名为 `HEAD` 的分支与 Git 用来描述活动（检出）分支的内部引用（也称为 `HEAD`）发生冲突。这种命名冲突会阻止您更新仓库的默认分支：

```plaintext
Error: Could not set the default branch. Do you have a branch named 'HEAD' in your repository?
```

要解决此问题：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **仓库 > 分支**。
1. 搜索名为 `HEAD` 的分支。
1. 确保分支没有未提交的更改。
1. 选择 **删除分支**，然后选择 **是，删除分支**。

Git 版本 [2.16.0 及更高版本](https://github.com/git/git/commit/a625b092cc59940521789fe8a3ff69c8d6b14eb2)，阻止您使用此名称创建分支。

