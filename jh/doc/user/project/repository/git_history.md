---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
description: "Documentation on Git file history."
---

# Git 文件历史 **(FREE)**

Git 文件历史记录提供有关与文件关联的提交历史记录的信息。要使用它：

1. 转到您项目的 **仓库 > 文件**。
1. 在右上角，选择 **历史**。

当您选择 **历史** 时，会显示以下信息：

![Git log output](img/file_history_output_v12_6.png "History button output")

如果您将鼠标悬停在 UI 中的提交上，则会显示提交修改的准确日期和时间。

## 关联的 `git` 命令

如果您从命令行运行 `git`，等效的命令是 `git log <filename>`。例如，如果要在本地目录中查找有关 `README.md` 文件的 `history` 信息，请运行以下命令：

```shell
git log README.md
```

Git 显示类似于以下的输出，其中包括 UTC 格式的提交时间：

```shell
commit 0e62ed6d9f39fa9bedf7efc6edd628b137fa781a
Author: Mike Jang <mjang@gitlab.com>
Date:   Tue Nov 26 21:44:53 2019 +0000

    Deemphasize GDK as a doc build tool

commit 418879420b1e3a4662067bd07b64bb6988654697
Author: Marcin Sedlak-Jakubowski <msedlakjakubowski@gitlab.com>
Date:   Mon Nov 4 19:58:27 2019 +0100

    Fix typo

commit 21cc1fef11349417ed515557748369cfb235fc81
Author: Jacques Erasmus <jerasmus@gitlab.com>
Date:   Mon Oct 14 22:13:40 2019 +0000

    Add support for modern JS

    Added rollup to the project

commit 2f5e895aebfa5678e51db303b97de56c51e3cebe
Author: Achilleas Pipinellis <axil@gitlab.com>
Date:   Fri Sep 13 14:03:01 2019 +0000

    Remove gitlab-foss Git URLs as we don't need them anymore

    [ci skip]
```
