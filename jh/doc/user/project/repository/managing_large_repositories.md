---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
description: "Documentation on large repositories."
---

# 管理大型仓库 **(FREE SELF)**

与任何基于 Git 的系统一样，极狐GitLab 在涉及到千兆字节的大型仓库时也受到类似的性能限制。

在接下来的部分中，我们将详细介绍几个在极狐GitLab 上使用并提高这些大型仓库的性能的最佳实践。

## 大型文件系统（LFS）

*强烈*建议在任何 Git 系统中，将二进制或 blob 文件（例如，软件包、音频、视频、图形等）存储为大文件存储 (LFS) 对象。在这种设置中，对象存储在其他地方，例如“对象存储”中，这可以显着减少仓库大小，从而提高性能。

要分析仓库是否有这些对象，建议运行 [`git-sizer`](https://github.com/github/git-sizer) 进行详细分析。该工具详细显示了仓库的组成部分，并突出显示了所有关注领域。如果发现任何大型对象，则建议使用 [BFG Repo Cleaner](https://rtyley.github.io/bfg-repo-cleaner/) 等工具将其删除。

## Gitaly Pack 对象缓存

Gitaly 是为 Git 仓库提供存储的服务，可以配置为缓存 Git 获取响应的短滚动窗口。建议用于大型仓库，因为当您的服务器接收大量抓取流量时，它可以显着减少服务器负载。

<!--
Refer to the [Gitaly Pack Objects Cache for more info](../../../administration/gitaly/configure_gitaly.md#pack-objects-cache).


## Reference Architectures

Large repositories tend to be found in larger organisations with many users. The GitLab Quality and Support teams provide several [Reference Architectures](../../../administration/reference_architectures/index.md) that are the recommended way to deploy GitLab at scale.

In these types of setups it's recommended that the GitLab environment used matches a Reference Architecture to improve performance.
-->

## Gitaly 集群

Gitaly 集群可以显着提高大型仓库的性能，因为它跨多个节点保存仓库的多个副本。因此，Gitaly 集群可以对这些仓库的读取请求进行负载平衡，并且具有容错能力。

建议用于大型仓库，但是，Gitaly 集群是一个大型解决方案，具有额外的设置和管理复杂性。<!--Refer to the [Gitaly Cluster docs for more info](../../../administration/gitaly/index.md), specifically the [Before deploying Gitaly Cluster](../../../administration/gitaly/index.md#before-deploying-gitaly-cluster) section.-->

## 让极狐GitLab 保持最新状态

极狐GitLab 不断添加性能改进和修复。因此，建议您尽可能将极狐GitLab 更新到最新版本并从中受益。

## 减少 CI/CD 中的并发克隆

大型仓库往往是单一仓库。这通常意味着这些仓库不仅从用户那里获得大量流量，而且从 CI/CD 获得大量流量。

CI/CD 负载往往是并发的，因为流水线是在设定的时间安排的。因此，在这段时间内，针对仓库的 Git 请求可能会显着增加，并导致 CI 和用户的性能下降。

在设计 CI/CD 流水线时，建议通过错开它们在不同时间运行来降低它们的并发性，例如，一个集合在一次运行，然后另一个集合在几分钟后运行。

可以探索其它一些操作来提高大型仓库的 CI/CD 性能。<!--Refer to the [Runner docs for more info](../../../ci/large_repositories/index.md).-->
