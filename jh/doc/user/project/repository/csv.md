---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# CSV 文件 **(FREE)**

> 引入于 14.1 版本。

逗号分隔值 (CSV) 文件是使用逗号分隔值的分隔文本文件。
文件的每一行都是一个数据记录。每条记录由一个或多个字段组成，以逗号分隔。使用逗号作为字段分隔符是此文件格式名称的来源。
CSV 文件通常以纯文本形式存储表格数据（数字和文本），在这种情况下，每行将具有相同数量的字段。

CSV 文件格式未完全标准化。其它字符可用作列分隔符。
字段可能会或可能不会被包围来转义特殊字符。

添加到仓库时，带有 `.csv` 扩展名的文件在极狐GitLab 中查看时呈现为表格。

![CSV file rendered as a table](img/csv_file_rendered_as_table_v14_1.png)
