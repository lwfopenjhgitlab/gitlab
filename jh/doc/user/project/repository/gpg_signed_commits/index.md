---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用 GPG 签名提交 **(FREE)**

您可以使用 GPG ([GNU Privacy Guard](https://gnupg.org/)) 密钥对在极狐GitLab 仓库中所做的提交进行签名。当您将加密签名添加到您的提交时，您提供了额外的保证，即提交来自您，而不是冒充者。如果极狐GitLab 可以使用公共 GPG 密钥验证提交作者的身份，则提交在 GitLab UI 中标记为 **已验证**。然后，您可以为您的项目配置推送规则，拒绝未使用 GPG 签名的单个提交，或拒绝来自未验证用户的所有提交。

NOTE:
极狐GitLab 使用术语 GPG 来表示所有 OpenPGP、PGP 和 GPG 相关的材料和实现。

对于极狐GitLab，考虑已验证的提交：

- 提交者必须具有 GPG 公钥/私钥对。
- 提交者的公钥必须上传到他们的极狐GitLab 帐户。
- GPG 公钥中的其中一个电子邮件地址必须与极狐GitLab 中提交者使用的**已验证**电子邮件地址匹配。要将此地址保密，请使用极狐GitLab 在您的个人资料中提供的自动生成的私人提交电子邮件地址。
- 提交者的电子邮件地址必须与 GPG 密钥中经过验证的电子邮件地址相匹配。

极狐GitLab 使用自己的密钥环来验证 GPG 签名。它不访问任何公钥服务器。

不支持 GPG 验证标签。

<!--
For more details about GPG, refer to the [related topics list](#related-topics).
-->

## 查看用户的公共 GPG 密钥

要查看用户的公共 GPG 密钥，您可以：

- 转到 `https://gitlab.example.com/<USERNAME>.gpg`。如果用户配置了 GPG 密钥，系统将显示 GPG 密钥，或者为没有配置 GPG 密钥的用户显示空白页面。
- 转到用户的个人资料（例如`https://gitlab.example.com/<USERNAME>`）。在用户个人资料的右上角，选择 **查看公共 GPG 密钥** (**{key}**)。

## 配置提交签名

要签名提交，您必须同时配置本地计算机和极狐GitLab 帐户：

1. [创建 GPG 密钥](#create-a-gpg-key)。
1. [将 GPG 密钥添加到您的帐户](#add-a-gpg-key-to-your-account)。
1. [将您的 GPG 密钥与 Git 关联](#associate-your-gpg-key-with-git)。
1. [签名您的 Git 提交](#sign-your-git-commits)。

<a id="create-a-gpg-key"></a>

### 创建 GPG 密钥

如果您还没有 GPG 密钥，请创建：

1. 为您的操作系统[安装 GPG](https://www.gnupg.org/download/) 。如果您的操作系统安装了 `gpg2`，请在本页的命令中将 `gpg` 替换为 `gpg2`。
1. 要生成您的密钥对，请运行适合您的 `gpg` 版本的命令：

   ```shell
   # Use this command for the default version of GPG, including
   # Gpg4win on Windows, and most macOS versions:
   gpg --gen-key

   # Use this command for versions of GPG later than 2.1.17:
   gpg --full-gen-key
   ```

1. 选择您的密钥应该使用的算法，或按 <kbd>Enter</kbd> 选择默认选项 `RSA and RSA`。
1. 选择密钥长度，以位为单位。建议使用 4096 位密钥。
1. 指定密钥的有效期。这个值是主观的，默认值是没有过期。
1. 要确认您的答案，请输入 `y`。
1. 输入您的姓名。
1. 输入您的电子邮件地址。它必须与您 GitLab 帐户中的[已验证电子邮件地址](../../../profile/index.md#change-the-email-displayed-on-your-commits)匹配。
1. 可选。输入要在您的姓名后的括号中显示的评论。
1. GPG 显示您目前输入的信息。编辑信息或按 <kbd>O</kbd>（表示 `Okay`）继续。
1. 输入强密码，然后再次输入确认。
1. 要列出您的私人 GPG 密钥，请运行以下命令，将 `<EMAIL>` 替换为您在生成密钥时使用的电子邮件地址：

   ```shell
   gpg --list-secret-keys --keyid-format LONG <EMAIL>
   ```

1. 在输出中，识别 `sec` 行，并复制 GPG 密钥 ID。它在 `/` 字符之后开始。在此示例中，密钥 ID 为 `30F2B65B9246B6CA`：

   ```plaintext
   sec   rsa4096/30F2B65B9246B6CA 2017-08-18 [SC]
         D5E4F29F3275DC0CDA8FFC8730F2B65B9246B6CA
   uid                   [ultimate] Mr. Robot <your_email>
   ssb   rsa4096/B7ABC0813E4028C0 2017-08-18 [E]
   ```

1. 要显示关联的公钥，请运行以下命令，将 `<ID>` 替换为上一步中的 GPG 密钥 ID：

   ```shell
   gpg --armor --export <ID>
   ```

1. 复制公钥，包括 `BEGIN PGP PUBLIC KEY BLOCK` 和 `END PGP PUBLIC KEY BLOCK` 行。您在下一步中需要此密钥。

<a id="add-a-gpg-key-to-your-account"></a>

### 将 GPG 密钥添加到您的帐户

要将 GPG 密钥添加到您的用户设置：

1. 登录极狐GitLab。
1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏，选择 **GPG 密钥** (**{key}**)。
1. 在 **密钥** 中，粘贴您的 *公共* 密钥。
1. 要将密钥添加到您的帐户，请选择 **添加密钥**。极狐GitLab 显示密钥的指纹、电子邮件地址和创建日期：

   ![GPG key single page](img/profile_settings_gpg_keys_single_key.png)

添加密钥后，您将无法对其进行编辑。您可以删除有问题的密钥并重新添加它。

<a id="associate-your-gpg-key-with-git"></a>

### 将您的 GPG 密钥与 Git 关联

创建 GPG 密钥并将其添加到您的帐户后，您必须配置 Git 以使用此密钥：

1. 运行以下命令列出您刚刚创建的 GPG 私钥，将 `<EMAIL>` 替换为您的密钥的电子邮件地址：

   ```shell
   gpg --list-secret-keys --keyid-format LONG <EMAIL>
   ```

1. 复制以 `sec` 开头的 GPG 私钥 ID。在本例中，私钥 ID 为 `30F2B65B9246B6CA`：

   ```plaintext
   sec   rsa4096/30F2B65B9246B6CA 2017-08-18 [SC]
         D5E4F29F3275DC0CDA8FFC8730F2B65B9246B6CA
   uid                   [ultimate] Mr. Robot <your_email>
   ssb   rsa4096/B7ABC0813E4028C0 2017-08-18 [E]
   ```

1. 运行此命令配置 Git 以使用您的密钥签名您的提交，将 `<KEY ID>` 替换为您的 GPG 密钥 ID：

   ```shell
   git config --global user.signingkey <KEY ID>
   ```

1. 可选。如果 Git 使用 `gpg` 并且您收到诸如 `secret key not available` 或 `gpg: signing failed: secret key not available` 之类的错误，请运行以下命令以使用 `gpg2` 代替：

   ```shell
   git config --global gpg.program gpg2
   ```

<a id="sign-your-git-commits"></a>

### 签名您的 Git 提交

将公钥添加到您的帐户后，您可以手动签名单个提交，或将 Git 配置为默认为已签名的提交：

- 手动签名单个 Git 提交：
  1. 将 `-S` 标志添加到您要签名的任何提交：

     ```shell
     git commit -S -m "My commit message"
     ```

  1. 询问时输入您的 GPG 密钥的密码。
  1. 推送到极狐GitLab 并检查您的提交是否经过验证。
- 默认情况下，通过运行以下命令对所有 Git 提交进行签名：

  ```shell
  git config --global commit.gpgsign true
  ```

## 验证提交

您可以查看合并请求或整个项目的提交：

1. 查看项目的提交：
    1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
    1. 在左侧边栏上，选择 **仓库 > 提交**。
1. 查看合并请求的提交：
    1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
    1. 在左侧边栏上，选择 **合并请求**，然后选择您的合并请求。
    1. 选择 **提交**。
1. 确定您要查看的提交。根据 GPG 签名的验证状态，已签名的提交会显示 **已验证** 或 **未验证** 徽章。未签名的提交不显示徽章：

   ![Signed and unsigned commits](img/project_signed_and_unsigned_commits.png)

1. 要显示提交的签名详细信息，请选择 GPG 徽章：

   ![Signed commit with verified signature](img/project_signed_commit_verified_signature.png)

   ![Signed commit with unverified signature](img/project_signed_commit_unverified_signature.png)

<a id="revoke-a-gpg-key"></a>

## 撤销 GPG 密钥

如果 GPG 密钥被泄露，请撤销它。撤销密钥会更改未来和过去的提交：

- 此密钥签名的过去提交被标记为未验证。
- 由该密钥签名的未来提交被标记为未验证。

撤销 GPG 密钥：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏，选择**GPG 密钥** (**{key}**)。
1. 选择要删除的 GPG 密钥旁边的 **撤销**。

## 删除 GPG 密钥

当您从极狐GitLab 帐户中删除 GPG 密钥时：

- 使用此密钥签名的先前提交仍然为已验证。
- 尝试使用此密钥的未来提交（包括任何已创建但尚未推送的提交）为未验证。

要从您的帐户中删除 GPG 密钥：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏，选择 **GPG 密钥** (**{key}**)。
1. 选择要删除的 GPG 密钥旁边的 **删除** (**{remove}**)。

如果您必须取消验证未来和过去的提交，请改为[撤销关联的 GPG 密钥](#revoke-a-gpg-key)。

<!--
## Related topics

- [Sign commits and tags with X.509 certificates](../x509_signed_commits/index.md)
- [Commits API](../../../../api/commits.md)
- GPG resources:
  - [Git Tools - Signing Your Work](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work)
  - [Managing OpenPGP Keys](https://riseup.net/en/security/message-security/openpgp/gpg-keys)
  - [OpenPGP Best Practices](https://riseup.net/en/security/message-security/openpgp/best-practices)
  - [Creating a new GPG key with subkeys](https://www.void.gr/kargig/blog/2013/12/02/creating-a-new-gpg-key-with-subkeys/) (advanced)
  - [Review existing GPG keys in your instance](../../../admin_area/credentials_inventory.md#review-existing-gpg-keys)
-->

## 故障排除

<a id="fix-verification-problems-with-signed-commits"></a>

### 修复签名提交的验证问题

可以使用 [X.509 证书](../x509_signed_commits/index.md)或 GPG 密钥对提交进行签名。两种方法的验证过程可能因多种原因而失败：

| 值                       | 描述 | 修复方法 |
|-----------------------------|-------------|----------------|
| `UNVERIFIED`                | 提交签名无效。 | 使用有效签名签署提交。 |
| `SAME_USER_DIFFERENT_EMAIL` | 用于签署提交的 GPG 密钥不包含提交者电子邮件，但确实包含提交者的不同有效电子邮件。 | 修改提交，使用与 GPG 密钥匹配的电子邮件地址，或更新 GPG 密钥，[包含电子邮件地址](https://security.stackexchange.com/a/261468)。 |
| `OTHER_USER`                | 签名和 GPG 密钥是有效的，但密钥属于与提交者不同的用户。 | 修改提交来使用正确的电子邮件地址，或修改提交来使用与您的用户关联的 GPG 密钥。 |
| `UNVERIFIED_KEY`            | 与 GPG 签名关联的密钥没有与提交者关联的经过验证的电子邮件地址。 | 添加并验证电子邮件到您的极狐GitLab 配置文件，[更新 GPG 密钥来包含电子邮件地址](https://security.stackexchange.com/a/261468)，或修改提交来使用不同的提交者电子邮件地址。 |
| `UNKNOWN_KEY`               | 极狐GitLab 不知道与此提交的 GPG 签名关联的 GPG 密钥。 | [添加 GPG 密钥](#add-a-gpg-key-to-your-account)到您的极狐GitLab 配置文件。 |
| `MULTIPLE_SIGNATURES`       | 已为提交找到多个 GPG 或 X.509 签名。 | 修改提交，仅使用一个 GPG 或 X.509 签名。 |
