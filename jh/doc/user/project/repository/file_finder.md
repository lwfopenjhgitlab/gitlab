---
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
disqus_identifier: 'https://docs.gitlab.com/ee/workflow/file_finder.html'
---

# 文件查找器 **(FREE)**

文件查找器功能允许您使用 UI 在仓库中搜索文件：

1. 转到您项目的 **Repository > Files**。
1. 在右上角，选择 **查找文件**。

如果您希望将手指放在键盘上，请使用[快捷键](../../shortcuts.md)，您可以从项目中的任何位置调用它。

在**议题**、**合并请求**、**里程碑**，甚至是项目的设置中，按 `t` 启动文件搜索功能。

开始输入您要搜索的内容。使用向上/向下箭头，您可以向上和向下搜索结果，使用 `Esc` 关闭搜索并返回到**文件**。

## 工作原理

文件查找器功能由 [Fuzzy filter](https://github.com/jeancroy/fuzz-aldrin-plus) 库提供支持。

它使用突出显示实现模糊搜索，并尝试通过识别人们在搜索时使用的 pattern 来提供直观的结果。

<!--
例如，考虑 [GitLab FOSS 存储库](https://gitlab.com/gitlab-org/gitlab-foss/tree/master) 并且我们要打开 `app/controllers/admin/deploy_keys_controller.rb` 文件 .
-->

使用模糊搜索，我们首先键入使我们更接近文件的字母。

NOTE:
要缩小搜索范围，请在搜索词中包含 `/`。

![Find file button](img/file_finder_find_file_v12_10.png)
