---
type: reference, howto
stage: Plan
group: Certify
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 需求管理 **(ULTIMATE)**

NOTE:
在 14.4 版本中，需求已移至 **议题**。

> - 引入于旗舰版 12.10 版本。
> - 添加和编辑需求的详细描述的能力引入于旗舰版 13.5 版本。
> - 移动到议题下于 14.4 版本。

根据需求，您可以设置标准来检查您的产品。它们可以基于用户、利益相关者、系统、软件或您认为重要的任何其他内容。

需求是极狐GitLab 中描述产品特定行为的产物。
需求是长期存在的，除非手动清除，否则不会消失。

如果行业标准*要求*您的应用程序具有特定功能或行为，您可以[创建需求](#创建需求) 来反映这一点。
当不再需要某个功能时，您可以[归档相关需求](#归档需求)。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see [GitLab 12.10 Introduces Requirements Management](https://www.youtube.com/watch?v=uSS7oUNSEoU).

<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For a more in-depth walkthrough using a [demonstration project](https://gitlab.com/gitlab-org/requiremeents-mgmt),
see [GitLab Requirements Traceability Walkthrough](https://youtu.be/VIiuTQYFVa0) (Feb 2021).
-->

![requirements list view](img/requirements_list_v13_5.png)

## 创建需求

每个项目中都有一个分页的需求列表，您可以在那里创建一个新的需求。

具有报告者或更高权限的用户可以创建需求。

创建需求：

1. 在项目中，转到 **议题 > 需求**。
1. 选择 **新建需求**。
1. 输入标题和描述并选择 **创建需求**。

![requirement create view](img/requirement_create_v13_5.png)

您可以在列表顶部看到新创建的需求，需求列表按创建日期降序排列。

<a id="view-a-requirement"></a>

## 查看需求

您可以通过选择从列表中查看需求。

![requirement view](img/requirement_view_v13_5.png)

要在查看需求时对其进行编辑，请选择需求标题旁边的 **编辑** 图标 (**{pencil}**)。

## 编辑需求

<!--
> The ability to mark a requirement as Satisfied [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/218607) in [GitLab Ultimate](https://about.gitlab.com/pricing/) 13.5.
-->

您可以从需求列表页面编辑需求。

具有报告者或更高权限的用户可以编辑需求。

要编辑需求：

1. 从需求列表中，选择 **编辑** 图标 (**{pencil}**)。
1. 更新文本输入字段中的标题和描述。您还可以使用复选框 **Satisfied** 在编辑表单中将需求标记为已满足。
1. 选择 **保存更改**。

## 归档需求

您可以在 **Open** 选项卡中存档打开的需求。

具有报告者或更高权限的用户可以归档需求。

要归档需求，请选择 **归档** (**{archive}**)。

一旦需求被归档，它就不再出现在 **Open** 选项卡中。

## 重新打开需求

您可以在 **Archived** 选项卡中查看已归档需求的列表。

具有报告者或更高权限的用户可以重新打开存档的需求。

![archived requirements list](img/requirements_archived_list_view_v13_1.png)

要重新打开存档的需求，请选择**重新打开**。

一旦重新打开需求，它就不再出现在 **Archived** 选项卡中。

## 搜索需求

> - 引入于旗舰版 13.1 版本。
> - 按状态搜索引入于 13.10 版本。

您可以根据以下条件从需求列表页面搜索需求：

- 标题
- 作者的用户名
- 状态（满意、失败或缺失）

要搜索需求：

1. 在项目中，转到 **议题 > 需求 > 列表**。
1. 选择 **搜索或过滤结果** 字段。 出现一个下拉菜单。
1. 从下拉列表中选择需求作者或状态或输入纯文本，按需求标题搜索。
1. 按键盘上的 <kbd>Enter</kbd> 过滤列表。

您还可以按以下方式对要求列表进行排序：

- 创建日期
- 更新日期

## 允许在 CI 作业中满足需求

<!--
> - [Introduced](https://gitlab.com/groups/gitlab-org/-/epics/2859) in [GitLab Ultimate](https://about.gitlab.com/pricing/) 13.1.
> - [Added](https://gitlab.com/gitlab-org/gitlab/-/issues/215514) ability to specify individual requirements and their statuses in [GitLab Ultimate](https://about.gitlab.com/pricing/) 13.2.
-->

现在支持需求测试报告<!--[需求测试报告](../../../ci/yaml/index.md#artifactsreportsrequirements)-->。
您可以向 CI 流水线添加作业，该作业在触发时将所有现有需求标记为“已满足”（您可以在编辑表单中手动满足需求 [编辑需求](#编辑需求)）。

### 添加手动作业到 CI

要将您的 CI 配置为在触发手动作业时将需求标记为 Satisfied，请将以下代码添加到您的 `.gitlab-ci.yml` 文件中。

```yaml
requirements_confirmation:
  when: manual
  allow_failure: false
  script:
    - mkdir tmp
    - echo "{\"*\":\"passed\"}" > tmp/requirements.json
  artifacts:
    reports:
      requirements: tmp/requirements.json
```

此定义向 CI 流水线添加了一个手动触发的（`when: manual`）作业。它是阻塞的（`allow_failure: false`），但是使用什么条件来触发 CI 作业取决于您。此外，您可以使用任何现有的 CI 作业将所有需求标记为满足，只要 CI 作业生成并上传了 `requirements.json` 产物。

当您手动触发此作业时，包含 `{"*":"passed"}` 的 `requirements.json` 文件将作为产物上传到服务器。在服务器端，检查需求报告中是否有“全部通过”记录（`{"*":"passed"}`），如果成功，它将所有现有的开放需求标记为 Satisfied。

#### 指定个别需求

可以指定个别需求及其状态。

如果存在以下要求：

- `REQ-1`（带有 IID `1`）
- `REQ-2`（带有 IID `2`）
- `REQ-3`（带有 IID `3`）

可以指定第一个需求通过，第二个需求失败。
有效值为 "passed" 和 "failed"。
通过省略需求 IID（在本例中为 `REQ-3` 的 IID `3`），不会记录到任何结果。

```yaml
requirements_confirmation:
  when: manual
  allow_failure: false
  script:
    - mkdir tmp
    - echo "{\"1\":\"passed\", \"2\":\"failed\"}" > tmp/requirements.json
  artifacts:
    reports:
      requirements: tmp/requirements.json
```

### 有条件地将手动作业添加到 CI

要将您的 CI 配置为仅在有一些开放需求时才包含手动作业，请添加一个规则来检查 CI/CD 变量 `CI_HAS_OPEN_REQUIREMENTS`。

```yaml
requirements_confirmation:
  rules:
    - if: "$CI_HAS_OPEN_REQUIREMENTS" == "true"
      when: manual
    - when: never
  allow_failure: false
  script:
    - mkdir tmp
    - echo "{\"*\":\"passed\"}" > tmp/requirements.json
  artifacts:
    reports:
      requirements: tmp/requirements.json
```

## 从 CSV 文件导入需求

> 引入于 13.7 版本。

您可以通过上传包含 `title` 和 `description` 列的 CSV 文件来将需求导入到项目中。

导入后，上传 CSV 文件的用户被设置为导入需求的作者。

具有报告者或更高权限的用户可以导入需求。

### 导入文件

在导入文件之前：

- 考虑导入仅包含几个需求的测试文件。如果不使用 GitLab API，就无法撤消大型导入。
- 确保您的 CSV 文件符合[文件格式](#导入-csv-文件格式)要求。

导入需求：

1. 在项目中，转到 **议题 > 需求**。
    - 如果项目已有需求，选择右上角的导入图标（**{import}**）。
    - 对于没有任何需求的项目，选择页面中间的 **导入CSV**。
1. 选择文件并选择 **导入需求**。

该文件在后台处理，并在导入完成后向您发送通知电子邮件。

### 导入 CSV 文件格式

从 CSV 文件导入需求时，必须以某种方式对其进行格式化：

- **标题行：** CSV 文件必须包含以下标题：`title` 和 `description`。标题不区分大小写。
- **列：** 来自除 `title` 和 `description` 之外的列的数据不会被导入。
- **分隔符：** 从标题行自动检测列分隔符。支持的分隔符有：逗号（`,`）、分号（`;`）和制表符（`\t`）。行分隔符可以是`CRLF` 或 `LF`。
- **双引号字符：** 双引号 (`"`) 字符用于引用字段，允许在字段中使用列分隔符（请参阅下面示例 CSV 数据中的第三行）。在引用字段中插入双引号 (`"`)，连续使用两个双引号字符 (`""`)。
- **数据行：**在标题行下方，后续行必须遵循相同的列顺序。标题文本是必需的，而描述是可选的，可以留空。

示例 CSV 数据：

```plaintext
title,description
My Requirement Title,My Requirement Description
Another Title,"A description, with a comma"
"One More Title","One More Description"
```

### 文件大小

该限制取决于实例的 Max Attachment Size 的配置值。

<!--
For GitLab.com, it is set to 10 MB.
-->

## 导出需求到 CSV 文件

> - 引入于 13.8 版本
> - 修订后的 CSV 列标题引入于 13.9 版本
> - 选择导出的字段引入于 13.9 版本

您可以将需求导出到 CSV 文件作为附件发送到您的默认通知电子邮件。

通过导出需求，您和您的团队可以将它们导入另一个工具或与您的客户共享。导出需求可以帮助与更高级别的系统协作，以及审计和合规框架任务。

具有报告者或更高权限的用户可以导出需求。

导出要求：

1. 在项目中，转到 **议题 > 需求**。
1. 在右上角，选择 **导出为 CSV** 图标 (**{export}**)。

    出现确认窗口。

1. 在 **高级导出选项** 下，选择要导出的字段。

    默认情况下选择所有字段。要从导出中排除某个字段，请清除其旁边的复选框。

1. 选择 **导出需求**。导出的 CSV 文件将发送到与您的用户关联的电子邮件地址。

### 导出的 CSV 文件格式

<!-- vale gitlab.Spelling = NO -->

您可以在电子表格编辑器（例如 Microsoft Excel、OpenOffice Calc 或 Google 表格）中预览导出的 CSV 文件。

<!-- vale gitlab.Spelling = YES -->

导出的 CSV 文件包含以下标题：

- 在 13.8 版本中：

   - 需求 ID
   - 标题
   - 描述
   - 作者用户名
   - 最新测试报告状态
   - 最新测试报告的创建于 (UTC) 

- 在 13.9 及更高版本中：

   - 需求 ID
   - 标题
   - 描述
   - 作者
   - 作者用户名
   - 创建于 (UTC)
   - 状态
   - 状态更新于 (UTC)