---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
description: "Autocomplete characters in Markdown fields."
---

# 自动完成字符 **(FREE)**

> 引入于 13.9 版本：您可以在用户自动完成中使用全名进行搜索。

自动完成字符提供了一种在 Markdown 字段中输入字段值的快速方法。当您开始在 Markdown 字段中输入具有以下字符之一的单词时，极狐GitLab 会根据一组匹配值逐步自动完成。字符串匹配不区分大小写。

| 符号 | 自动完成 | 相关匹配项显示 |
| :-------- | :------------ | :---- |
| `~`       | 标记 | 20 |
| `%`       | 里程碑 | 5 |
| `@`       | 用户和群组 | 10 |
| `#`       | 议题 | 5 |
| `!`       | 合并请求 | 5 |
| `&`       | 史诗 | 5 |
| `$`       | 代码片段 | 5 |
| `:`       | Emoji | 5 |
| `/`       | 快速操作 | 100 |

当您从列表中选择一个符号时，该值将输入到该字段中。
您输入的字符越多，匹配就越精确。

与[快速操作](quick_actions.md)结合使用时，自动完成字符很有用。

## 用户自动完成

假设您的极狐GitLab 实例包括以下用户：

<!-- vale gitlab.Spelling = NO -->

| 用户名        | 姓名 |
| :-------------- | :--- |
| alessandra      | Rosy Grant |
| lawrence.white  | Kelsey Kerluke |
| leanna          | Rosemarie Rogahn |
| logan_gutkowski | Lee Wuckert |
| shelba          | Josefine Haley |

<!-- vale gitlab.Spelling = YES -->

用户自动完成按用户名或姓名排序，首先是您查询开头的用户。
例如，先输入 `@lea` 会显示 `leanna`，输入 `@ros` 会先显示 `Rosemarie Rogahn` 和 `Rosy Grant`。
包含您的查询的任何用户名或姓名随后都会显示在自动完成菜单中。

您还可以搜索全名来查找用户。
要查找 `Rosy Grant`，即使她的用户名是例如 `alessandra`，您也可以键入全名，而不是空格，例如 `@rosygrant`。
