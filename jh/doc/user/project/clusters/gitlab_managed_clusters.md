---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab 管理的集群（已废弃） **(FREE)**

> - 废弃于 14.5 版本。
> - 在私有化部署版上禁用于 15.0 版本。

WARNING:
此功能弃用于 14.5 版本。
要将集群连接到极狐GitLab，请使用[极狐GitLab 代理](../../../user/clusters/agent/index.md)。
<!--To manage applications, use the [Cluster Project Management Template](../../../user/clusters/management_project_template.md).-->

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，询问管理员[启用功能标志](../../../administration/feature_flags.md) `certificate_based_clusters`。

您可以选择允许极狐GitLab 为您管理集群。如果您的集群由极狐GitLab 管理，则会自动为您的项目创建资源。<!--有关已创建资源的详细信息，请参阅 [访问控制](cluster_access.md) 部分。-->

如果您选择管理自己的集群，则不会自动创建特定于项目的资源。
如果您使用 [Auto DevOps](../../../topics/autodevops/index.md)，则必须明确提供 `KUBE_NAMESPACE` 部署变量以供部署作业使用。否则，将为您创建一个命名空间。

WARNING:
请注意，手动管理极狐GitLab 创建的资源（例如命名空间和服务帐户）可能会导致意外错误。如果发生这种情况，请尝试[清除集群缓存](#clearing-the-cluster-cache)。

<a id="clearing-the-cluster-cache"></a>

## 清除集群缓存

如果您允许极狐GitLab 管理您的集群，极狐GitLab 会存储为您的项目创建的命名空间和服务帐户的缓存版本。如果您手动修改集群中的这些资源，此缓存可能会与您的集群不同步。这可能会导致部署作业失败。

要清除缓存：

1. 导航到您项目的 **基础设施 > Kubernetes 集群** 页面，然后选择您的集群。
1. 展开 **高级设置** 部分。
1. 选择 **清除集群缓存**。

<a id="base-domain"></a>

## 基础域名

指定基础域名会自动将 `KUBE_INGRESS_BASE_DOMAIN` 设置为部署变量。
如果您使用 [Auto DevOps](../../../topics/autodevops/index.md)，则此域名用于不同的阶段。例如，Auto Review Apps 和 Auto Deploy。

域名应该有一个配置为入口 IP 地址的通配符 DNS。
您可以：

- 使用您的域名提供商创建一个指向 Ingress IP 地址的 `A` 记录。
- 使用 `nip.io` 或 `xip.io` 等服务输入通配符 DNS 地址。例如，`192.168.1.1.xip.io`。

要确定外部 Ingress IP 地址或外部 Ingress 主机名，按照您的 Kubernetes 提供商的特定说明使用正确的凭据配置 `kubectl`。以下示例输出显示了集群的外部端点。
然后，此信息可用于设置 DNS 条目和转发规则，以允许外部访问您部署的应用程序。

<!--
- *If the cluster is on GKE*:
  1. Select the **Google Kubernetes Engine** link in the **Advanced settings**,
     or go directly to the [Google Kubernetes Engine dashboard](https://console.cloud.google.com/kubernetes/).
  1. Select the proper project and cluster.
  1. Select **Connect**.
  1. Execute the `gcloud` command in a local terminal or using the **Cloud Shell**.

- *If the cluster is not on GKE*: Follow the specific instructions for your
  Kubernetes provider to configure `kubectl` with the right credentials.
  The output of the following examples show the external endpoint of your
  cluster. This information can then be used to set up DNS entries and forwarding
  rules that allow external access to your deployed applications.
-->

根据您的 Ingress，可以通过多种方式检索外部 IP 地址。
此列表提供了一个通用解决方案，以及一些特定于极狐GitLab 的方法：

- 通常，您可以通过运行以下命令列出所有负载均衡器的 IP 地址：

  ```shell
  kubectl get svc --all-namespaces -o jsonpath='{range.items[?(@.status.loadBalancer.ingress)]}{.status.loadBalancer.ingress[*].ip} '
  ```

- 如果您使用**应用程序**安装了 Ingress，请运行：

  ```shell
  kubectl get service --namespace=gitlab-managed-apps ingress-nginx-ingress-controller -o jsonpath='{.status.loadBalancer.ingress[0].ip}'
  ```

- 一些 Kubernetes 集群会返回主机名，例如 [Amazon EKS](https://aws.amazon.com/eks/)。对于这些平台，运行：

  ```shell
  kubectl get service --namespace=gitlab-managed-apps ingress-nginx-ingress-controller -o jsonpath='{.status.loadBalancer.ingress[0].hostname}'
  ```

  如果您使用 EKS，还会创建一个 [Elastic Load Balancer](https://docs.aws.amazon.com/elasticloadbalancing/)，这会产生额外的 AWS 费用。

- Istio/Knative 使用不同的命令。运行：

  ```shell
  kubectl get svc --namespace=istio-system istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip} '
  ```

如果您在某些 Kubernetes 版本上看到尾随 `%`，请不要包含它。
