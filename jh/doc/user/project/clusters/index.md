---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 项目级 Kubernetes 集群（基于证书）（已废弃）**(FREE)**

> - 废弃于 14.5 版本。

WARNING:
此功能废弃于 14.5 版本。要将集群连接到极狐GitLab，请使用[极狐GitLab 代理](../../clusters/agent/index.md)。

<!--[项目级](../../infrastructure/clusters/connect/index.md#cluster-levels-deprecated)-->项目级 Kubernetes 集群允许您将 Kubernetes 集群连接到极狐GitLab 中的项目。

您还可以[连接多个集群](multiple_kubernetes_clusters.md)到单个项目。

## 查看您的项目级集群

查看项目级 Kubernetes 集群：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **基础架构 > Kubernetes 集群**。
