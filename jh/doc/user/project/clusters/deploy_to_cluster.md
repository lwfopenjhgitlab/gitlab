---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用集群证书部署到 Kubernetes 集群（已弃用） **(FREE)**

> 废弃于 14.5 版本。

WARNING:
此功能废弃于 14.5 版本。要将集群连接到极狐GitLab，请使用[极狐GitLab 代理](../../clusters/agent/index.md)。要使用代理进行部署，请使用 [CI/CD 工作流](../../clusters/agent/ci_cd_workflow.md)。

Kubernetes 集群可以是部署作业的目标。

- 如果集群与极狐GitLab 集成，则特殊的[部署变量](#deployment-variables)可用于您的作业，并且不需要配置。您可以使用诸如 `kubectl` 或 `helm` 之类的工具从作业中立即开始与集群交互。
- 如果您不使用极狐GitLab 集群集成，您仍然可以部署到您的集群。但是，您必须使用 [CI/CD 变量](../../../ci/variables/index.md#for-a-project)自行配置 Kubernetes 工具，然后才能从作业与集群交互。

<a id="deployment-variables"></a>

## 部署变量

部署变量需要一个名为 [`gitlab-deploy-token`](../deploy_tokens/index.md#gitlab-deploy-token) 的有效[部署令牌](../deploy_tokens/index.md)，以及在您的部署作业脚本中添加以下命令 ，让 Kubernetes 访问仓库：

- 使用 Kubernetes 1.18+：

  ```shell
  kubectl create secret docker-registry gitlab-registry --docker-server="$CI_REGISTRY" --docker-username="$CI_DEPLOY_USER" --docker-password="$CI_DEPLOY_PASSWORD" --docker-email="$GITLAB_USER_EMAIL" -o yaml --dry-run=client | kubectl apply -f -
  ```

- 使用 Kubernetes <1.18:

  ```shell
  kubectl create secret docker-registry gitlab-registry --docker-server="$CI_REGISTRY" --docker-username="$CI_DEPLOY_USER" --docker-password="$CI_DEPLOY_PASSWORD" --docker-email="$GITLAB_USER_EMAIL" -o yaml --dry-run | kubectl apply -f -
  ```

Kubernetes 集群集成将极狐GitLab CI/CD 构建环境中的这些[部署变量](../../../ci/variables/predefined_variables.md#deployment-variables) 暴露给部署作业。部署作业已经[定义了一个目标环境](../../../ci/environments/index.md)。

| 部署变量        | 描述 |
|----------------------------|-------------|
| `KUBE_URL`                 | 与 API URL 等同。 |
| `KUBE_TOKEN`               | [环境服务帐户](cluster_access.md) 的 Kubernetes 令牌。在 11.5 版本之前，`KUBE_TOKEN` 是集群集成的主要服务帐户的 Kubernetes 令牌。 |
| `KUBE_NAMESPACE`           | 与项目的部署服务帐户关联的命名空间，格式为 `<project_name>-<project_id>-<environment>`。对于极狐GitLab 托管的集群，系统会在集群中自动创建一个匹配的命名空间。如果您的集群是在 12.2 版本之前创建的，则默认的 `KUBE_NAMESPACE` 设置为 `<project_name>-<project_id>`。 |
| `KUBE_CA_PEM_FILE`         | 包含 PEM 数据的文件的路径。仅在指定自定义 CA 捆绑包时出现。 |
| `KUBE_CA_PEM`              | （**已废弃**）原始 PEM 数据。仅当指定了自定义 CA 捆绑包时适用。 |
| `KUBECONFIG`               | 包含此部署的 `kubeconfig` 的文件的路径。如果指定，将嵌入 CA 包。此配置还嵌入了在 `KUBE_TOKEN` 中定义的相同令牌，因此您可能只需要此变量。这个变量名也会被 `kubectl` 自动获取，所以如果使用 `kubectl`，您不需要明确引用它。 |
| `KUBE_INGRESS_BASE_DOMAIN` | 此变量可用于为每个集群设置一个域名。有关详细信息，请参阅[集群域名](gitlab_managed_clusters.md#base-domain)。 |

## 自定义命名空间

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/27630) in GitLab 12.6.
> - An option to use project-wide namespaces [was added](https://gitlab.com/gitlab-org/gitlab/-/issues/38054) in GitLab 13.5.
-->

Kubernetes 集成为部署作业提供了一个带有自动生成命名空间的 `KUBECONFIG`。它默认使用 `<prefix>-<environment>` 形式的项目环境特定命名空间，其中 `<prefix>` 的形式为 `<project_name>-<project_id>`。要了解更多信息，请阅读[部署变量](#deployment-variables)。

您可以通过以下几种方式自定义部署命名空间：

- 您可以选择**按[环境](../../../ci/environments/index.md)设置命名空间**，或**按项目设置命名空间**。按环境设置命名空间是默认和推荐的设置，因为它可以防止生产环境和非生产环境之间的资源混合。
- 使用项目级集群时，您可以额外自定义命名空间前缀。当按环境设置命名空间时，部署命名空间是`<prefix>-<environment>`，否则只是`<prefix>`。
- 对于**非托管**集群，自动生成的命名空间设置在 `KUBECONFIG` 中，但用户有责任确保其存在。您可以使用 `.gitlab-ci.yml` 中的 [`environment:kubernetes:namespace`](../../../ci/environments/index.md#configure-kubernetes-deployments-deprecated) 完全自定义此值。

当您自定义命名空间时，现有环境将保持关联到它们当前的命名空间，直到您[清除集群缓存](gitlab_managed_clusters.md#clearing-the-cluster-cache)。

### 保护凭证

默认情况下，任何可以创建部署作业的人都可以访问环境部署作业中的任何 CI/CD 变量，包括 `KUBECONFIG`，它允许访问集群中相关服务帐户可用的任何 secret。
为确保您的生产环境凭证安全，请考虑使用[受保护的环境](../../../ci/environments/protected_environments.md)，并结合以下措施*之一*：

- 每个环境一个极狐GitLab 托管的集群和命名空间。
- 每个受保护环境对应一个环境范围集群。可以使用多个受限服务帐户多次添加同一个集群。

## Kubernetes 集群的 Web 终端

Kubernetes 集成为您的[环境](../../../ci/environments/index.md)添加了 Web 终端支持。这是基于 Docker 和 Kubernetes 中的 exec 功能，因此您可以在现有容器中获得一个新的 shell 会话。要使用此集成，您应该使用上面的部署变量部署到 Kubernetes，确保所有部署、副本集和 pod 都使用以下注释：

- `app.gitlab.com/env: $CI_ENVIRONMENT_SLUG`
- `app.gitlab.com/app: $CI_PROJECT_PATH_SLUG`

`$CI_ENVIRONMENT_SLUG` 和 `$CI_PROJECT_PATH_SLUG` 是 CI/CD 变量的值。

您必须是项目所有者或拥有 `maintainer` 权限才能使用终端。
支持仅限于环境的第一个 pod 中的第一个容器。

<a id="troubleshooting"></a>

## 故障排除

在部署作业开始之前，极狐GitLab 专门为部署作业创建以下内容：

- 命名空间。
- 服务帐户。

但是，有时极狐GitLab 无法创建它们。在这种情况下，您的作业可能会失败并显示以下消息：

```plaintext
This job failed because the necessary resources were not successfully created.
```

要在创建命名空间和服务帐户时查找此错误的原因，请检查日志<!--[logs](../../../administration/logs.md#kuberneteslog)-->。

失败的原因包括：

- 您提供的令牌没有极狐GitLab 所需的 [`cluster-admin`](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#user-facing-roles) 权限。
- 缺少 `KUBECONFIG` 或 `KUBE_TOKEN` 部署变量。要传递给您的作业，它们必须具有匹配的 [`environment:name`](../../../ci/environments/index.md)。如果您的作业没有设置 `environment:name`，则不会将 Kubernetes 凭据传递给它。

NOTE:
从 12.0 或更早版本升级的项目级集群的配置方式可能会导致此错误。如果您想自己管理命名空间和服务帐户，请确保清除[极狐GitLab 托管集群](gitlab_managed_clusters.md)选项。
