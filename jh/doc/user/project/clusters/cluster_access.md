---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用集群证书进行访问控制（RBAC 或 ABAC）（已废弃） **(FREE)**

<!--
> - Restricted service account for deployment was [introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/51716) in GitLab 11.5.
-->
> 废弃于 14.5 版本。

WARNING:
此功能废弃于 14.5 版本。要将集群连接到极狐GitLab，请改用[极狐GitLab 代理](../../clusters/agent/index.md)。

在极狐GitLab 中创建集群时，系统会询问您是否要创建：

- [基于角色的访问控制 (RBAC)](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)集群，这是极狐GitLab 默认和推荐的选项。
- [基于属性的访问控制 (ABAC)](https://kubernetes.io/docs/reference/access-authn-authz/abac/)集群。

当极狐GitLab 创建集群时，会在 `default` 命名空间中创建一个具有 `cluster-admin` 权限的 `gitlab` 服务帐户来管理新创建的集群。

Helm 还为每个已安装的应用程序创建额外的服务帐户和其它资源。有关详细信息，请参阅每个应用程序的 Helm chart 文档。

如果是添加现有的 Kubernetes 集群<!--[添加现有的 Kubernetes 集群](add_existing_cluster.md)-->，请确保账户的 token 具有集群的管理员权限。

极狐GitLab 创建的资源因集群类型而异。

## 重要注意事项

请注意以下有关访问控制的内容：

- 仅当您的集群[由极狐GitLab 托管](gitlab_managed_clusters.md)时，才会创建特定于环境的资源。
- 如果您的集群是在 12.2 版本之前创建的，它对所有项目环境使用一个命名空间。

<a id="rbac-cluster-resources"></a>

## RBAC 集群资源

极狐GitLab 为 RBAC 集群创建以下资源。

| 名称                  | 类型                 | 详情                                                                                                    | 何时创建           |
|:----------------------|:---------------------|:-----------------------------------------------------------------------------------------------------------|:-----------------------|
| `gitlab`              | `ServiceAccount`     | `default` 命名空间                                                                                        | 创建新集群时 |
| `gitlab-admin`        | `ClusterRoleBinding` | [`cluster-admin`](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#user-facing-roles) 角色 | 创建新集群时 |
| `gitlab-token`        | `Secret`             | `gitlab` ServiceAccount 的令牌                                                                          | 创建新集群时 |
| 环境命名空间 | `Namespace`          | 包含所有特定于环境的资源                                                                | 部署到集群时 |
| 环境命名空间 | `ServiceAccount`     | 使用环境的命名空间                                                                              | 部署到集群时 |
| 环境命名空间 | `Secret`             | 环境 ServiceAccount 的令牌                                                                       | 部署到集群时 |
| 环境命名空间 | `RoleBinding`        | [`admin`](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#user-facing-roles) 角色         | 部署到集群时 |

环境命名空间 `RoleBinding` 在 13.6 版本中更新为 `admin` 角色，之前使用的是 `edit` 角色。

## ABAC 集群资源

极狐GitLab 为 ABAC 集群创建以下资源。

| 名称                  | 类型                                                         | 何时创建           |
|:----------------------|:---------------------|:-------------------------------------|:---------------------------|
| `gitlab`              | `ServiceAccount`     | `default` 命名空间                         | 创建新集群时 |
| `gitlab-token`        | `Secret`             | `gitlab` ServiceAccount 的令牌           | 创建新集群时 |
| 环境命名空间 | `Namespace`          | 包含所有特定于环境的资源  | 部署到集群时 |
| 环境命名空间 | `ServiceAccount`     | 使用环境的命名空间               | 部署到集群时 |
| 环境命名空间 | `Secret`             | 环境 ServiceAccount 的令牌        | 部署到集群时 |

<a id="security-of-runners"></a>

## runners 安全

Runners 默认启用特权模式，允许其执行特殊命令，并运行 Docker in Docker。运行某些 [Auto DevOps](../../../topics/autodevops/index.md) 作业需要此功能。这意味着容器正在特权模式下运行，因此您应该注意一些重要的细节。

特权标志为正在运行的容器提供了所有功能，而容器又可以执行主机可以执行的几乎所有操作。请注意与对任意镜像执行 `docker run` 操作相关的固有安全风险，因为它们实际上具有 root 访问权限。

如果您不想在特权模式下使用 runner：

- 在 SaaS 版上使用共享 runner。
- 设置您自己的使用 `docker+machine` runner。
