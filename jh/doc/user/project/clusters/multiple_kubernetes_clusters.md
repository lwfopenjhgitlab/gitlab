---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 每个项目通过集群证书连接多个集群（已废弃） **(FREE)**

> - 废弃于 14.5 版本。

WARNING:
此功能废弃于 14.5 版本。要将集群连接到极狐GitLab，请使用[极狐GitLab 代理](../../clusters/agent/index.md)。

您可以将多个 Kubernetes 集群关联到您的项目，您就可以使得不同的环境（如开发、staging、生产等）拥有不同的集群。
像首次添加一样，添加另一个集群，并确保[设置环境范围](#setting-the-environment-scope)，将新集群与其他集群区分开来。

<a id="setting-the-environment-scope"></a>

## 设置环境范围

当向您的项目添加多个 Kubernetes 集群时，您需要使用环境范围来区分它们。环境范围将集群与[环境](../../../ci/environments/index.md)相关联，类似于[特定于环境的 CI/CD 变量](../../../ci/environments/index.md#limit-the-environment-scope-of-a-cicd-variable)。

默认环境范围是 `*`，这意味着所有作业，无论其环境如何，都使用该集群。每个范围只能由项目中的单个集群使用，否则会发生验证错误。此外，没有设置环境关键字的作业无法访问任何集群。

例如，项目中存在以下 Kubernetes 集群：

| 集群     | 环境范围 |
| ----------- | ----------------- |
| 开发 | `*`               |
| 生产  | `production`      |

并且在 [`.gitlab-ci.yml`](../../../ci/yaml/index.md) 中设置了以下环境：

```yaml
stages:
  - test
  - deploy

test:
  stage: test
  script: sh test

deploy to staging:
  stage: deploy
  script: make deploy
  environment:
    name: staging
    url: https://staging.example.com/

deploy to production:
  stage: deploy
  script: make deploy
  environment:
    name: production
    url: https://example.com/
```

结果：

- 开发集群详细信息可在 `deploy to staging` 作业中找到。
- 生产集群详细信息可在 `deploy to production` 作业中找到。
- `test` 作业中没有可用的集群详细信息，因为它没有定义任何环境。
