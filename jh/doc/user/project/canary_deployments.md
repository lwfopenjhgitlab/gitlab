---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 金丝雀部署 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/1659) in GitLab 9.1.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/212320) from GitLab Premium to GitLab Free in 13.8.
-->

金丝雀部署是一种流行的持续部署策略，将小部分用户更新到您的应用程序的新版本。

在采用持续交付时，公司需要决定使用哪种类型的部署策略。最流行的策略之一是金丝雀部署，将小部分用户首先更新到新版本。

如果应用程序的新版本出现问题，只有一小部分用户会受到影响，并且可以修复或快速恢复更改。

## 用例

当您只想将功能发送到您的 pod 队列的一部分并观察它们的行为时，可以使用金丝雀部署，一定百分比的用户群访问临时部署功能。如果一切正常，您可以将该功能部署到生产环境，因为它不会导致问题。

金丝雀部署对于后端重构、性能改进或其它用户界面不变，但您希望确保性能保持不变或有所改进。开发人员在使用带有面向用户更改的金丝雀版本时需要小心，因为默认情况下，来自同一用户的请求会随机分布在金丝雀和非金丝雀 pod 之间，这可能会导致混淆甚至错误。如果需要，您可能需要考虑[在 Kubernetes 服务定义中将 `service.spec.sessionAffinity` 设置为 `ClientIP`](https://kubernetes.io/docs/concepts/services-networking/service/#virtual-ips-and-service-proxies)，这超出了本文档的范围。

## 使用 Canary Ingress 进行高级流量控制

通过使用 [Canary Ingress](https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#canary)，金丝雀部署可以更具战略性，这是一种高级流量路由服务，可根据权重、会话、cookie 等因素控制稳定部署和金丝雀部署之间的传入 HTTP 请求。 极狐GitLab 在 Auto Deploy 架构中使用此服务让用户快速安全地推出他们的新部署。

### 如何在金丝雀部署中设置 Canary Ingress

如果您的 Auto DevOps 流水线使用 `auto-deploy-image` 的 `v2.0.0+` 版本，则默认安装 Canary Ingress。
当您创建新的金丝雀部署时，Canary Ingress 变得可用，并在金丝雀部署升级到生产环境时被销毁。

以下是一个从头开始的示例设置流程：

1. 准备一个[启用了 Auto DevOps](../../topics/autodevops/index.md) 的项目。
1. 在您的项目中建立一个 Kubernetes 集群<!--[Kubernetes 集群](../../user/infrastructure/clusters/index.md)-->。
1. 在集群中安装 [NGINX Ingress](https://github.com/kubernetes/ingress-nginx/tree/master/charts/ingress-nginx)。
1. 根据上面分配的 Ingress Endpoint 设置基础域名<!--[基础域名](../../user/project/clusters/gitlab_managed_clusters.md#base-domain)-->。
1. 检查您的 Auto DevOps 流水线中是否使用了 [`auto-deploy-image` 的 `v2.0.0+` 版本](../../topics/autodevops/upgrading_auto_deploy_dependencies.md#verify-dependency-versions)。如果不是，请按照文档指定镜像版本。
1. [运行新的 Auto DevOps 流水线](../../ci/pipelines/index.md#run-a-pipeline-manually) 并确保 `production` 作业成功并创建生产环境。
1. 为 Auto DevOps 流水线配置 [`canary` 部署作业](../../topics/autodevops/customize.md#deploy-policy-for-canary-environments)。
1. [运行新的 Auto DevOps 流水线](../../ci/pipelines/index.md#run-a-pipeline-manually) 并确保 `canary` 作业成功并使用 Canary Ingress 创建金丝雀部署入口。

<!--
### Show Canary Ingress deployments on deploy boards (deprecated)

> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/215501) in GitLab 13.6.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/212320) from GitLab Premium to GitLab Free in 13.8.
> - [Deprecated](https://gitlab.com/groups/gitlab-org/configure/-/epics/8) in GitLab 14.5.

WARNING:
This feature was [deprecated](https://gitlab.com/groups/gitlab-org/configure/-/epics/8) in GitLab 14.5.

To view canary deployments you must properly configure deploy boards:

1. Follow the steps to [enable deploy boards](deploy_boards.md#enabling-deploy-boards).
1. To track canary deployments you must label your Kubernetes deployments and
   pods with `track: canary`. To get started quickly, you can use the [Auto Deploy](../../topics/autodevops/stages.md#auto-deploy)
   template for canary deployments that GitLab provides.

Depending on the deploy, the label should be either `stable` or `canary`.
GitLab assumes the track label is `stable` if the label is blank or missing.
Any other track label is considered `canary` (temporary).
This allows GitLab to discover whether a deployment is stable or canary (temporary).

Once all of the above are set up and the pipeline has run at least once,
Go to the environments page under **Pipelines > Environments**.
As the pipeline executes, deploy boards clearly mark canary pods, enabling
quick and clear insight into the status of each environment and deployment.

Canary deployments are marked with a yellow dot in the deploy board so that you
can quickly notice them.

![Canary deployments on deploy board](img/deploy_boards_canary_deployments.png)

#### How to check the current traffic weight on a Canary Ingress (deprecated)

WARNING:
This feature was [deprecated](https://gitlab.com/groups/gitlab-org/configure/-/epics/8) in GitLab 14.5.

1. Visit the [deploy board](../../user/project/deploy_boards.md).
1. View the current weights on the right.

   ![Rollout Status Canary Ingress](img/canary_weight.png)

#### How to change the traffic weight on a Canary Ingress (deprecated)

WARNING:
This feature was [deprecated](https://gitlab.com/groups/gitlab-org/configure/-/epics/8) in GitLab 14.5.

You can change the traffic weight in your environment's deploy board by using [GraphiQL](../../api/graphql/getting_started.md#graphiql),
or by sending requests to the [GraphQL API](../../api/graphql/getting_started.md#command-line).

To use your [deploy board](../../user/project/deploy_boards.md):

1. Go to **Deployments > Environments** for your project.
1. Set the new weight with the dropdown list on the right side.
1. Confirm your selection.

Here's an example using [GraphiQL](../../api/graphql/getting_started.md#graphiql):

1. Visit [GraphiQL Explorer](https://gitlab.com/-/graphql-explorer).
1. Execute the `environmentsCanaryIngressUpdate` GraphQL mutation:

   ```shell
   mutation {
     environmentsCanaryIngressUpdate(input:{
       id: "gid://gitlab/Environment/29",              # Your Environment ID. You can get the ID from the URL of the environment page.
       weight: 45                                      # The new traffic weight. e.g. If you set `45`, 45% of traffic goes to a canary deployment and 55% of traffic goes to a stable deployment.
     }) {
       errors
     }
   }
   ```

1. If the request succeeds, the `errors` response contains an empty array. GitLab sends a `PATCH`
   request to your Kubernetes cluster for updating the weight parameter on a Canary Ingress.
-->