---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 描述模板 **(FREE)**

您可以定义模板，用于[议题](issues/index.md)和[合并请求](merge_requests/index.md)的描述。

您可以在项目、群组或实例中定义这些模板。项目继承更高级别定义的模板。

您可能想要使用这些模板：

- 针对您工作流程的不同阶段，例如，功能提议、功能改进或错误报告。
- 对于特定项目的每个议题或合并请求，因此布局是一致的。
- 对于[服务台电子邮件模板](service_desk.md#use-a-custom-template-for-service-desk-issues)。

要使描述模板起作用，它们必须：

- 以 `.md` 扩展名保存。
- 存储在 `.gitlab/issue_templates` 或 `.gitlab/merge_request_templates` 目录中的项目仓库中。
- 出现在默认分支上。

<a id="create-an-issue-template"></a>

## 创建议题模板

在仓库的 `.gitlab/issue_templates/` 目录中创建一个新的 Markdown (`.md`) 文件。提交并推送到您的默认分支。

创建议题描述模板：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **仓库**。
1. 在默认分支旁边，选择 **{plus}**。
1. 选择 **新建文件**。
1. 在默认分支旁边的 **文件名称** 文本框中，输入 `.gitlab/issue_templates/mytemplate.md`，其中 `mytemplate` 是您的议题模板的名称。
1. 提交到您的默认分支。

要检查是否正常工作，请创建一个新议题并查看是否可以在 **选择一个模板** 下拉列表中找到您的描述模板。

<a id="create-a-merge-request-template"></a>

## 创建合并请求模板

与议题模板类似，在仓库的 `.gitlab/merge_request_templates/` 目录中创建一个新的 Markdown (`.md`) 文件。提交并推送到您的默认分支。与议题模板不同，合并请求具有[附加继承规则](merge_requests/creating_merge_requests.md)，取决于提交消息的内容和分支名称。

要为项目创建合并请求描述模板：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **仓库**。
1. 在默认分支旁边，选择 **{plus}**。
1. 选择 **新建文件**。
1. 在默认分支旁边，在 **文件名** 文本框中，输入 `.gitlab/merge_request_templates/mytemplate.md`，其中 `mytemplate` 是您的合并请求模板的名称。
1. 提交到您的默认分支。

要检查是否正常工作，请[创建一个新的合并请求](merge_requests/creating_merge_requests.md) 并查看您是否可以在 **选择一个模板** 下拉列表中找到您的描述模板。

## 使用模板

当您创建或编辑议题或合并请求时，它会显示在 **选择一个模板** 下拉列表中。

要应用模板：

1. 创建或编辑议题或合并请求。
1. 选择 **选择一个模板** 下拉列表。
1. 如果 **描述** 文本框不为空，要确认，请选择 **应用模板**。
1. 选择 **保存修改**。

当您选择描述模板时，其内容将复制到描述文本框中。

要放弃选择模板后对描述所做的任何更改：展开 **选择一个模板** 下拉列表并选择 **重置模板**。

![Choosing a description template in an issue](img/description_templates_v14_7.png)

NOTE:
您可以使用指定模板创建快捷链接来创建议题。
例如：`https://jihulab.com/gitlab-cn/gitlab/-/issues/new?issuable_template=Feature%20proposal`。<!--阅读更多关于 [使用带有预填充值的 URL 创建问题](issues/managing_issues.md#using-a-url-with-prefilled-values)。-->

### 合并请求模板中支持的变量

> 引入于 15.7 版本。

NOTE:
此功能仅适用于[默认模板](#set-a-default-template-for-merge-requests-and-issues)。

当您第一次保存合并请求时，极狐GitLab 会用它们的值替换合并请求模板中的以下变量：

| 变量 | 描述 | 输出示例 |
|----------|-------------|----------------|
| `%{all_commits}` | 来自合并请求中所有提交的消息。限制为 100 个最近的提交。跳过超过 100 KiB 的提交主体并合并提交消息。 | `* Feature introduced` <br><br> `This commit implements feature` <br> `Changelog:added` <br><br> `* Bug fixed` <br><br> `* Documentation improved` <br><br>`This commit introduced better docs.` |
| `%{co_authored_by}` | 提交作者的姓名和电子邮件，采用 `Co-authored-by` Git 提交 trailer 格式。限于合并请求中 100 个最近提交的作者。 | `Co-authored-by: Zane Doe <zdoe@example.com>` <br> `Co-authored-by: Blake Smith <bsmith@example.com>` |
| `%{first_commit}` | 合并请求差异中第一次提交的完整消息。 | `Update README.md` |
| `%{first_multiline_commit}` | 第一个提交（非合并提交）并且在消息正文中有多行的完整消息。如果所有提交都不是多行，则显示合并请求标题。 | `Update README.md` <br><br> `Improved project description in readme file.` |
| `%{source_branch}` | 被合并的分支的名称。 | `my-feature-branch`  |
| `%{target_branch}` | 应用更改的分支的名称。 | `main` |

<a id="set-instance-level-description-templates"></a>

### 设置实例级描述模板 **(PREMIUM SELF)**

您可以使用实例模板仓库<!--[实例模板仓库](../admin_area/settings/instance_template_repository.md)-->，在 **实例级别** 为议题和合并请求设置描述模板。
您还可以将实例模板仓库用于文件模板。

您可能还对在实例中创建新项目时可以使用的项目模板<!--[项目模板](../admin_area/custom_project_templates.md)-->感兴趣。

<a id="set-group-level-description-templates"></a>

### 设置群组级描述模板 **(PREMIUM)**

> - 引入于 13.9 版本。
> - 功能标志移除于 14.0 版本。

使用 **群组级别** 描述模板，您可以将模板存储在单个仓库中，并将群组文件模板设置配置为指向该仓库。因此，您可以在所有群组项目中的议题和合并请求中使用相同的模板。

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **模板**。
1. 从下拉列表中，选择您的模板项目作为群组级别的模板仓库。
1. 选择 **保存修改**。

![Group template settings](../group/img/group_file_template_settings.png)

您可能还对各种[群组中的文件类型](../group/manage.md#group-file-templates)的模板感兴趣。

<a id="set-a-default-template-for-merge-requests-and-issues"></a>

### 为合并请求和议题设置默认模板

> `Default.md`（不区分大小写）模板引入于 14.8 版本。

在项目中，您可以为新议题和合并请求选择默认描述模板。因此，每次创建新的合并请求或议题时，都会预先填充您在模板中输入的文本。

先决条件：

- 在项目的左侧边栏中，选择 **设置 > 通用** 并展开 **可见性，项目功能，权限**。确保将议题或合并请求设置为 **具有访问权限的任何人** 或 **仅项目成员**。

要为合并请求设置默认描述模板，请执行以下任一操作：

- 在 14.8 及更高版本，[创建名为 `Default.md`（不区分大小写）的合并请求模板](#create-a-merge-request-template)，并将其保存在 `.gitlab/merge_request_templates/`。如果在项目设置中设置了默认模板，则[不覆盖](#priority-of-default-description-templates)默认模板。
- 专业版及更高版本的用户可以在项目设置中设置默认模板：

   1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
   1. 在左侧边栏，选择 **设置 > 合并请求**。
   1. 在 **合并提交消息模板** 部分，填写文本区域。
   1. 选择 **保存更改**。

要为议题设置默认描述模板，请执行以下任一操作：

- 在 14.8 及更高版本，[创建名为 `Default.md`（不区分大小写）的合并请求模板](#create-an-issue-template)，并将其保存在 `.gitlab/issue_templates/`。如果在项目设置中设置了默认模板，则[不覆盖](#priority-of-default-description-templates)默认模板。
- 专业版及更高版本的用户可以在项目设置中设置默认模板：

   1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
   1. 在左侧边栏中，选择 **设置 > 通用**。
   1. 展开 **议题的默认描述模板**。
   1. 填写文本区域。
   1. 选择 **保存修改**。

由于合并请求和议题支持 Markdown<!--[Markdown](../markdown.md)-->，您可以使用它来格式化标题、列表等。

您还可以在 Projects REST API<!--[Projects REST API](../../api/projects.md)--> 中提供 `issues_template` 和 `merge_requests_template` 属性，使您的默认议题和合并请求模板保持最新。

<a id="priority-of-default-description-templates"></a>

#### 默认描述模板的优先级

当您在各个地方设置[议题描述模板](#set-a-default-template-for-merge-requests-and-issues)时，它们在项目中具有以下优先级：

1. 项目设置中设置的模板。
1. 来自父组的 `Default.md`（不区分大小写）。
1. 来自项目仓库的 `Default.md`（不区分大小写）。

合并请求具有[额外的继承规则](merge_requests/creating_merge_requests.md)取决于提交消息的内容和分支名称。

## 描述模板示例

我们在项目的 [`.gitlab` 文件夹](https://jihulab.com/gitlab-cn/gitlab/-/tree/master/.gitlab) 中使用议题和合并请求的描述模板，您可以参考一些例子。

NOTE:
可以在描述模板中使用[快速操作](quick_actions.md)来快速添加标记、指派人和里程碑。仅当提交议题或合并请求的用户具有执行相关操作的权限时，才会执行快速操作。

以下是错误报告模板的示例：

```markdown
## Summary

(Summarize the bug encountered concisely)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## Example Project

(If possible, please create an example project here on GitLab.com that exhibits the problematic
behavior, and link to it here in the bug report.
If you are using an older version of GitLab, this will also determine whether the bug has been fixed
in a more recent version)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise.)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

/label ~bug ~reproduced ~needs-investigation
/cc @project-manager
/assign @qa-tester
```
