---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto, reference
---

# 部署看板（已废弃） **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/1589) in GitLab 9.0.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/212320) from GitLab Premium to GitLab Free in 13.8.
> - In GitLab 13.5 and earlier, apps that consist of multiple deployments are shown as
>   duplicates on the deploy board. This is [fixed](https://gitlab.com/gitlab-org/gitlab/-/issues/8463)
>   in GitLab 13.6.
> - In GitLab 13.11 and earlier, environments in folders do not show deploy boards.
>   This is [fixed](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/60525) in
>   GitLab 13.12.
> - [Deprecated](https://gitlab.com/groups/gitlab-org/configure/-/epics/8) in GitLab 14.5.
> - [Disabled on self-managed](https://gitlab.com/gitlab-org/gitlab/-/issues/353410) in GitLab 15.0.
-->

> - 废弃于 14.5 版本。
> - 在私有化部署版上禁用于 15.0 版本。

WARNING:
此功能废弃于 14.5 版本。

FLAG:
在私有化部署版上，此功能默认不可用。要使其可用，询问管理员[启用功能标志](../../administration/feature_flags.md) `certificate_based_clusters`。

部署看板提供了在 [Kubernetes](https://kubernetes.io) 上运行的每个 CI [环境](../../ci/environments/index.md) 的当前运行状况和状态的综合视图，显示部署中 pod 的状态。开发人员和其它团队成员可以在他们已经使用的工作流程中，逐个 pod 地查看部署的进度和状态，而无需访问 Kubernetes。

NOTE:
如果您有 Kubernetes 集群，您可以使用 [Auto DevOps](../../topics/autodevops/index.md) 将应用程序自动部署到生产环境。

## 概览

借助部署看板，您可以更深入地了解部署，其具有以下优势：

- 从一开始就进行部署，而不仅仅是在部署完成后
- 查看跨多个服务器构建的发布
- 更精细的状态详细信息（成功、运行中、失败、待定、未知）
- 参见[金丝雀部署](canary_deployments.md)

下图是生产环境部署看板的示例。

![deploy boards landing page](img/deploy_boards_landing_page.png)

正方形代表 Kubernetes 集群中与给定环境关联的 Pod。将鼠标悬停在每个方块上方，您可以看到展开的部署状态。百分比是更新到最新版本的 pod 的百分比。

由于部署看板与 Kubernetes 紧密耦合，因此需要一些知识。特别是，您应该熟悉：

- [Kubernetes pods](https://kubernetes.io/docs/concepts/workloads/pods/)
- [Kubernetes labels](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/)
- [Kubernetes namespaces](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)
- [Kubernetes canary deployments](https://kubernetes.io/docs/concepts/cluster-administration/manage-deployment/#canary-deployments)

## 用例

由于部署看板是特定环境的 Kubernetes pod 的可视化表示，因此有很多用例。仅举几例：

- 您想将 staging 环境中正在运行的特性推广到生产环境中。您转到环境列表，验证 staging 环境中运行的特性是您认为应运行的特性，然后选择[手动作业](../../ci/jobs/job_control.md#create-a-job-that-must-be-run-manually)，将其部署到生产环境。
- 您触发了部署，并且您有许多容器要升级，因此您知道这需要一段时间（您还限制了部署，一次只删除 X 个容器）。但是您需要在部署时告诉某人，因此您转到环境列表，查看生产环境，查看每个 pod 滚动发布时的实时进度。
- 您收到一份报告说生产环境中出现了异常情况，因此您查看生产环境，查看正在运行的特性，以及部署是否正在进行、卡住或失败。
- 你有一个看起来不错的 MR，但您想在 staging 环境上运行它，因为 staging 环境是以某种更接近生产的方式设置的。您转到环境列表，找到您感兴趣的 [Review App](../../ci/review_apps/index.md)，然后选择手动操作将其部署到 staging 环境。

<a id="enabling-deploy-boards"></a>

## 启用部署看板

要显示特定[环境](../../ci/environments/index.md)的部署看板，您应该：

1. 通过部署阶段，已经[定义了一个环境](../../ci/environments/index.md)。

1. 启动并运行 Kubernetes 集群。

   NOTE:
   如果您使用的是 OpenShift，请确保您使用的是 `Deployment` 资源而不是 `DeploymentConfiguration`。否则，部署看板将无法正确呈现。有关更多信息，请阅读 [OpenShift 文档](https://docs.openshift.com/container-platform/3.7/dev_guide/deployments/kubernetes_deployments.html#kubernetes-deployments-vs-deployment-configurations)。

1. [配置极狐GitLab Runner](../../ci/runners/index.md)，与 `docker` 或 `kubernetes` 执行器。
1. 在您的项目中为集群配置 Kubernetes 集成。Kubernetes 命名空间特别值得注意，因为您需要它用于部署脚本（由 `KUBE_NAMESPACE` 部署变量公开）。
1. 确保 `app.gitlab.com/env: $CI_ENVIRONMENT_SLUG` 和 `app.gitlab.com/app: $CI_PROJECT_PATH_SLUG` 的 Kubernetes 注释应用于部署、副本集和 Pod，其中 `$CI_ENVIRONMENT_SLUG` 和 `$CI_PROJECT_PATH_SLUG ` 是 CI/CD 变量的值。这样我们就可以在可能有多个的集群/命名空间中查找合适的环境。这些资源应包含在 Kubernetes 服务设置中定义的命名空间中。您可以使用 [Auto deploy](../../topics/autodevops/stages.md#auto-deploy) `.gitlab-ci.yml` 模板，该模板具有预定义的阶段和要使用的命令，并自动应用注释。每个项目在 Kubernetes 中也必须有一个唯一的命名空间。下图演示了它在 Kubernetes 中的显示方式。

<!--
   NOTE:
   Matching based on the Kubernetes `app` label was removed in [GitLab
   12.1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/14020).
   To migrate, please apply the required annotations (see above) and
   re-deploy your application. If you are using Auto DevOps, this will
   be done automatically and no action is necessary.

   If you use GCP to manage clusters, you can see the deployment details in GCP itself by navigating to **Workloads > deployment name > Details**:
-->

   ![deploy boards Kubernetes Label](img/deploy_boards_kubernetes_label.png)

设置完上述所有内容并且流水线至少运行一次后，导航到 **部署 > 环境** 下的环境页面。

默认情况下，部署看板是可见的。您可以选择其各自环境名称旁边的三角，来隐藏它们。

### 示例 manifest 文件

以下示例是 Kubernetes manifest 部署文件的摘录，使用两个注释 `app.gitlab.com/env` 和 `app.gitlab.com/app` 来启用**部署看板**：

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "APPLICATION_NAME"
  annotations:
    app.gitlab.com/app: ${CI_PROJECT_PATH_SLUG}
    app.gitlab.com/env: ${CI_ENVIRONMENT_SLUG}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: "APPLICATION_NAME"
  template:
    metadata:
      labels:
        app: "APPLICATION_NAME"
      annotations:
        app.gitlab.com/app: ${CI_PROJECT_PATH_SLUG}
        app.gitlab.com/env: ${CI_ENVIRONMENT_SLUG}
```

注释应用于部署、副本集和 Pod。通过更改副本的数量，例如 `kubectl scale -replicas=3 deploy APPLICATION_NAME -n ${KUBE_NAMESPACE}`，您可以从看板上关注实例的 pod。

NOTE:
YAML 文件是静态的。如果使用 `kubectl apply` 应用它，则必须手动提供项目和环境 slug，或创建脚本在应用前替换 YAML 中的变量。

## 金丝雀部署

一种流行的 CI 策略，其中一小部分更改更新到您的应用程序的新版本。

[阅读有关金丝雀部署的更多信息。](canary_deployments.md)

<!--
## Further reading

- [GitLab Auto deploy](../../topics/autodevops/stages.md#auto-deploy)
- [GitLab CI/CD variables](../../ci/variables/index.md)
- [Environments and deployments](../../ci/environments/index.md)
- [Kubernetes deploy example](https://gitlab.com/gitlab-examples/kubernetes-deploy)
-->