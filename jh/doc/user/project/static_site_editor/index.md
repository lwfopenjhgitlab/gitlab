---
stage: Create
group: Editor
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
remove_date: '2022-08-03'
redirect_to: '../web_ide/index.md'
---

# 静态站点编辑器（已删除） **(FREE)**

此功能在 14.7 版本中已弃用，并在 15.0 中删除。
请改用 [Web 编辑器](../repository/web_editor.md) 或 [Web IDE](../web_ide/index.md)。

## 删除静态站点编辑器

静态站点编辑器本身不是您项目的一部分。要从现有项目中删除静态站点编辑器，请删除指向编辑器的链接：

1. 删除项目中使用 `edit_page_url` 的所有链接。如果您使用 **Middleman - Static Site Editor** 项目模板，则此帮助程序的唯一实例位于 `/source/layouts/layout.erb` 中。 完全删除此行：

   ```ruby
   <%= link_to('Edit this page', edit_page_url(data.config.repository, current_page.file_descriptor.relative_path), id: 'edit-page-link') %>
   ```

1. 在 `/data/config.yml` 中，删除 `repository` 键/值对：

   ```yaml
   repository: https://gitlab.com/<username>/<myproject>
   ```

   - 如果 `repository` 是 `/data/config.yml` 中存储的唯一值，您可以删除整个文件。
1. 在 `/helpers/custom_helpers.rb` 中，删除 `edit_page_url()` 和 `endcode_path()`：

   ```ruby
   def edit_page_url(base_url, relative_path)
     "#{base_url}/-/sse/#{encode_path(relative_path)}/"
   end

   def encode_path(relative_path)
     ERB::Util.url_encode("master/source/#{relative_path}")
   end
   ```

   - 如果 `edit_page_url()` 和 `encode_path()` 是唯一的 helper，你可以完全删除 `/helpers/custom_helpers.rb`。
1. 清理所有无关的配置文件。
1. 提交并推送您的更改。
