---
stage: Manage
group: Workspace
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

<a id="create-a-project"></a>

# 创建项目 **(FREE)**

您可以在极狐GitLab 中以多种方式创建项目。

<a id="create-a-blank-project"></a>

## 创建空白项目

要在极狐GitLab 中创建项目：

1. 在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目**。
1. 在页面右侧，选择 **新建项目**。
1. 选择 **创建空白项目**。
1. 输入项目详情：
   - 在 **项目名称** 字段中，输入您的项目名称。名称必须以小写或大写字母（`a-z, A-Z`）、数字（`0-9`）、表情符号或下划线（`_`）开头。 它还可以包含点（`.`）、加号（`+`）、破折号（`-`）或空格。
   - 在 **项目标识串** 字段中，输入项目的路径。极狐GitLab 实例使用标识串作为项目的 URL 路径。要更改标识串，首先输入项目名称，然后更改标识串。
   - 在 **项目部署目标（可选）** 字段中，选择项目的部署目标。此信息有助于极狐GitLab 更好地了解其用户及其部署要求。
   - 要为用户修改项目的[查看和访问权限](../public_access.md)，请更改 **可见性级别**。
   - 要创建 README 文件以便初始化 Git 仓库、具有默认分支并可以克隆，请选择 **使用自述文件初始化仓库**。
   - 要分析项目中的源代码是否存在已知安全漏洞，请选择 **启用静态应用程序安全测试 (SAST)**。
1. 选择 **创建项目**。

<a id="create-a-project-from-a-built-in-template"></a>

## 从内置模板创建项目

内置项目模板使用文件初始化新项目，可以帮助您入门。
内置模板来自以下群组：

- [`project-templates`](https://jihulab.com/gitlab-cn/project-templates)

<!--
- [`pages`](https://gitlab.com/pages)

Anyone can [contribute a built-in template](../../development/project_templates.md).
-->

从内置模板创建项目：

1. 在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目**。
1. 在页面右侧，选择 **新建项目**。
1. 选择 **从模板创建**。
1. 选择 **内置** 选项卡。
1. 从模板列表中：
   - 要查看模板的预览，请选择 **预览**。
   - 要为项目使用模板，请选择 **使用模板**。
1. 输入项目详情：
   - 在 **项目名称** 字段中，输入您的项目名称。名称必须以小写或大写字母（`a-z, A-Z`）、数字（`0-9`）、表情符号或下划线（`_`）开头。 它还可以包含点（`.`）、加号（`+`）、破折号（`-`）或空格。
   - 在 **项目标识串** 字段中，输入项目的路径。极狐GitLab 实例使用标识串作为项目的 URL 路径。要更改标识串，首先输入项目名称，然后更改标识串。
   - 在 **项目描述（可选）** 字段中，输入项目仪表盘的描述。
   - 要为用户修改项目的[查看和访问权限](../public_access.md)，请更改 **可见性级别**。
1. 选择 **创建项目**。

NOTE:
[从模板](#create-a-project-from-a-built-in-template)或[通过导入](settings/import_export.md#import-a-project-and-its-data)创建项目的用户，显示为导入对象（如议题和合并请求）的作者，保留模板或导入的原始时间戳。
导入的对象被标记为 `By <username> on <timestamp> (imported from GitLab)`。
因此，导入对象的创建日期可能早于用户帐户的创建日期，可能导致对象是在用户拥有帐户之前创建的。

<a id="create-a-project-from-a-custom-template"></a>

## 从自定义模板创建项目 **(PREMIUM)**

自定义项目模板可在以下位置获得：

- [实例级模板](../../user/admin_area/custom_project_templates.md)
- [群组级模板](../../user/group/custom_project_templates.md)

1. 在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目**。
1. 在页面右侧，选择 **新建项目**。
1. 选择 **从模板创建**。
1. 选择 **实例** 或 **群组** 选项卡。
1. 从模板列表中：
   - 要查看模板的预览，请选择 **预览**。
   - 要为项目使用模板，请选择 **使用模板**。
1. 输入项目详情：
   - 在 **项目名称** 字段中，输入您的项目名称。名称必须以小写或大写字母（`a-z, A-Z`）、数字（`0-9`）、表情符号或下划线（`_`）开头，还可以包含点（`.`）、加号（`+`）、破折号（`-`）或空格。
   - 在 **项目标识串** 字段中，输入项目的路径。极狐GitLab 实例使用标识串作为项目的 URL 路径。要更改标识串，首先输入项目名称，然后更改标识串。
   - 在 **项目描述（可选）** 字段中，输入项目仪表盘的描述。
   - 要为用户修改项目的[查看和访问权限](../public_access.md)，请更改 **可见性级别**。
1. 选择 **创建项目**。

## 从 HIPAA 审计协议模板创建项目 **(ULTIMATE)**

HIPAA 审计协议模板包含美国卫生与公共服务部发布的 HIPAA 审计协议中的审计查询问题。

从 HIPAA 审计协议模板创建项目：

1. 在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目**。
1. 在页面右侧，选择 **新建项目**。
1. 选择 **从模板创建**。
1. 选择 **内置** 选项卡。
1. 找到 **HIPAA 审计协议** 模板：
   - 要查看模板的预览，请选择 **预览**。
   - 要为项目使用模板，请选择 **使用模板**。
1. 输入项目详情：
   - 在 **项目名称** 字段中，输入您的项目名称。名称必须以小写或大写字母（`a-z, A-Z`）、数字（`0-9`）、表情符号或下划线（`_`）开头。 它还可以包含点（`.`）、加号（`+`）、破折号（`-`）或空格。
   - 在 **项目标识串** 字段中，输入项目的路径。极狐GitLab 实例使用标识串作为项目的 URL 路径。要更改标识串，首先输入项目名称，然后更改标识串。
   - 在 **项目描述（可选）** 字段中，输入项目仪表盘的描述。
   - 要为用户修改项目的[查看和访问权限](../public_access.md)，请更改 **可见性级别**。
1. 选择 **创建项目**。

## 通过 Git 推送创建新项目

使用 `git push` 将本地项目仓库推送到极狐GitLab。推送仓库后，极狐GitLab 在您选择的命名空间中创建您的项目。

您不能使用 `git push` 来创建具有以下项目路径的项目：

- 以前使用过。
- 已[重命名](settings/index.md#rename-a-repository)。

以前使用的项目路径有一个重定向，重定向会导致推送尝试将请求重定向到重命名的项目位置，而不是创建新项目。要为以前使用过或重命名的项目创建新项目，请使用 [UI](#create-a-project) 或[项目 API](../../api/projects.md#create-project)。

先决条件：

- 要使用 SSH 推送，您必须有[一个 SSH 密钥](../ssh/index.md)，[添加到您的极狐GitLab 帐户](../ssh/index.md#add-an-ssh-key-to-your-gitlab-account)。
- 您必须具有向命名空间添加新项目的权限。要检查您是否有权限：

   1. 在顶部栏上，选择 **主菜单 > 群组**，并找到您的群组。
   1. 确认右上角有 **新建项目**。如果您需要权限，请联系您的极狐GitLab 管理员。

要推送您的代码仓库并创建一个项目：

1. 使用 SSH 或 HTTPS 推送：
   - 使用 SSH 推送：

      ```shell
      git push --set-upstream git@gitlab.example.com:namespace/myproject.git master
      ```

   - 使用 HTTPS 推送：

      ```shell
      git push --set-upstream https://gitlab.example.com/namespace/myproject.git master
      ```

    - 对于 `gitlab.example.com`，使用托管 Git 仓库的机器的域名。
    - 对于 `namespace`，使用您的[命名空间](../namespace/index.md)的名称。
    - 对于 `myproject`，使用您的项目名称。
    - 可选。要导出现有的代码仓库标签，请将 `--tags` 标志附加到您的 `git push` 命令。
1. 可选。配置远端：

   ```shell
   git remote add origin https://gitlab.example.com/namespace/myproject.git
   ```

推送完成后，系统会显示以下消息：

```shell
remote: The private project namespace/myproject was created.
```

要查看您的新项目，请转到 `https://gitlab.example.com/namespace/myproject`。
默认情况下，您项目的可见性设置为**私有**。要更改项目可见性，调整您的[项目设置](../public_access.md#change-project-visibility)。

## 相关主题

- 有关不能用作项目名称的单词列表，查看[保留的项目和群组名称](../../user/reserved_names.md)。
- 有关不能在项目和群组名称中使用的字符列表，请参阅[项目和群组名称的限制](../../user/reserved_names.md#limitations-on-project-and-group-names)。
- [管理项目](working_with_projects.md)。
