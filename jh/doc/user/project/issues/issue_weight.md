---
disqus_identifier: 'https://docs.gitlab.com/ee/workflow/issue_weight.html'
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 议题权重 **(PREMIUM)**

> 于 13.9 移动到专业版

当您遇到很多议题时，可能很难获得概览。通过加权议题，您可以更好地了解给定议题的时间、价值或复杂性成本。

## 查看议题权重

您可以在以下位置查看议题权重：

- 每个议题的右侧边栏。
- 议题页面，旁边有一个权重图标 (**{weight}**)。
- [议题看板](../issue_board.md)，旁边有一个权重图标 (**{weight}**)。
- [里程碑](../milestones/index.md)页面，查看议题权重的总和。

## 设置议题权重

先决条件：

- 您必须至少具有项目的报告者角色。

您可以在创建或编辑议题时设置议题权重。

您必须输入整数、正数。

当您更改议题的权重时，新值会覆盖以前的值。

### 当您创建议题时

要在[创建议题](create_issues.md)时设置议题权重，请在 **权重** 下输入一个数字。

### 从现有议题创建

要从现有议题设置议题权重：

1. 转到议题。
1. 在右侧边栏的 **权重** 部分，选择 **编辑**。
1. 输入新权重。
1. 选择下拉列表之外的任何区域。

### 从议题看板创建

当您[从议题看板编辑议题](../issue_board.md#edit-an-issue)时设置议题权重：

1. 转到您的议题看板。
1. 选择一张议题卡片（不是它的标题）。
1. 在右侧边栏的 **权重** 部分，选择 **编辑**。
1. 输入新权重。
1. 选择下拉列表之外的任何区域。

## 删除议题权重

先决条件：

- 您必须至少具有项目的报告者角色。

要删除议题权重，请按照设置议题权重时的相同步骤操作，然后选择**删除权重**。
