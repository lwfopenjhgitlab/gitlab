---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 管理议题 **(FREE)**

创建议题后，您可以开始使用议题。

## 编辑议题

> 重新排序列表项功能引入于 15.0 版本。

您可以编辑议题的标题和描述。

先决条件：

- 您必须至少具有该项目的报告者角色，作为议题的作者，或被分配给议题。

要编辑问题：

1. 在标题右侧，选择 **编辑标题和描述** (**{pencil}**)。
1. 编辑可用字段。
1. 选择 **保存修改**。

## 批量编辑项目中的议题

<!--
> - Assigning epic [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/210470) in GitLab 13.2.
> - Editing health status [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/218395) in GitLab 13.2.
> - Editing iteration [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/196806) in GitLab 13.9.
-->

当您在一个项目中时，您可以一次编辑多个议题。

先决条件：

- 您必须至少具有该项目的报告者角色。

同时编辑多个议题：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **议题**。
1. 选择 **批量编辑**。屏幕右侧会出现一个侧边栏。
1. 选中您要编辑的每个议题旁边的复选框。
1. 从侧边栏中，编辑可用字段。
1. 选择 **全部更新**。

在项目中批量编辑议题时，您可以编辑以下属性：

- 状态（开放中或已关闭）
- [指派人](#assignee)
- [史诗](../../group/epics/index.md)
- [里程碑](../milestones/index.md)
- [标记](../labels.md)
- [健康状态](#health-status)
- [通知](../../profile/notifications.md)订阅
- [迭代](../../group/iterations/index.md)

### 批量编辑群组中的议题 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/7249) in GitLab 12.1.
> - Assigning epic [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/210470) in GitLab 13.2.
> - Editing health status [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/218395) in GitLab 13.2.
> - Editing iteration [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/196806) in GitLab 13.9.
-->

当您在一个群组中时，您可以跨多个项目编辑多个议题。

先决条件：

- 您必须至少具有该项目的报告者角色。

同时编辑多个议题：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **议题**。
1. 选择 **批量编辑**。屏幕右侧会出现一个侧边栏。
1. 选中您要编辑的每个议题旁边的复选框。
1. 从侧边栏中，编辑可用字段。
1. 选择 **全部更新**。

在群组中批量编辑议题时，您可以编辑以下属性：

- [史诗](../../group/epics/index.md)
- [里程碑](../milestones/index.md)
- [迭代](../../group/iterations/index.md)
- [标记](../labels.md)
- [健康状态](#健康状态)

## 移动议题

当您移动议题时，它会关闭并复制到目标项目。
原始议题没有被删除。系统注释被添加到两个议题中，显示它来源和qu'xiang。 

将议题移至具有不同访问规则的项目时要小心。在移动议题之前，请确保它不包含敏感数据。

先决条件：

- 您必须至少具有该项目的报告者角色。

移动问题：

1. 转到议题。
1. 在右侧边栏上，选择 **移动议题**。
1. 搜索将议题移至的项目。
1. 选择 **移动**。

### 批量移动议题 **(FREE SELF)**

#### 从议题列表

> 引入于 15.6 版本。

当您在一个项目中时，您可以同时移动多个议题。
您不能移动任务或测试用例。

先决条件：

- 您必须至少具有该项目的报告者角色。

同时移动多个议题：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **议题**。
1. 选择 **批量编辑**。屏幕右侧会出现一个侧边栏。
1. 选中您要移动的每个议题旁边的复选框。
1. 从右侧边栏中选择 **移动所选**。
1. 从下拉列表中选择目标项目。
1. 选择 **移动**。

#### 从 Rails 控制台

您可以将所有未解决的议题从一个项目移动到另一个项目。

先决条件：

- 您必须至少具有该项目的报告者角色。

1. 可选（但推荐），在控制台中尝试任何更改之前创建备份<!--[创建备份](../../../raketasks/backup_restore.md)-->。
1. 打开 Rails 控制台<!--[Rails 控制台](../../../administration/operations/rails_console.md)-->。
1. 运行以下脚本。确保将 `project`、`admin_user` 和 `target_project` 更改为您的值。

   ```ruby
   project = Project.find_by_full_path('full path of the project where issues are moved from')
   issues = project.issues
   admin_user = User.find_by_username('username of admin user') # make sure user has permissions to move the issues
   target_project = Project.find_by_full_path('full path of target project where issues moved to')

   issues.each do |issue|
      if issue.state != "closed" && issue.moved_to.nil?
         Issues::MoveService.new(container: project, current_user: admin_user).execute(issue, target_project)
      else
         puts "issue with id: #{issue.id} and title: #{issue.title} was not moved"
      end
   end; nil
   ```

1. 要退出 Rails 控制台，请输入 `quit`。

### 删除任务列表项

> 引入于 15.9 版本。

先决条件：

- 您必须至少具有项目的报告者角色，或者是议题的作者或指派人。

在带有任务列表项的议题描述中：

1. 将鼠标悬停在任务列表项上并选择选项菜单（**{ellipsis_v}**）。
1. 选择 **删除**。

任务列表项已从议题描述中删除。
任何嵌套的任务列表项都会向上移动一个嵌套级别。

## 关闭议题

当您决定某个议题已解决或不再需要时，您可以关闭它。
该议题已标记为已关闭但未删除。

先决条件：

- 您必须至少具有该项目的报告者角色。

要关闭议题，您可以执行以下操作：

- 在议题的顶部，选择 **关闭议题**。
- 在[议题看板](../issue_board.md)中，将议题卡从其列表拖到 **已关闭** 列表中。

### 重新打开关闭的议题

先决条件：

- 您必须至少具有该项目的报告者角色。

要重新打开已关闭的议题，请在议题顶部选择 **重启议题**。
重新打开的问题与任何其他未解决的问题没有不同。

<a id="closing-issues-automatically"></a>

### 自动关闭议题

您可以通过在提交消息或合并请求描述中，使用关闭语法样式来自动关闭议题。私有化部署实例的管理员可以[更改默认关闭样式](../../../administration/issue_closing_pattern.md)。

如果提交消息或合并请求描述包含与[定义的 pattern](#default-closing-pattern) 匹配的文本，则在以下任一情况下，匹配文本中引用的所有议题都将关闭：

- 提交被推送到项目的[**默认**分支](../repository/branches/default.md)。
- 提交或合并请求，合并到默认分支中。

例如，如果您在合并请求描述中包含 `Closes #4, #6, Related to #5`：

- 合并 MR 时会自动关闭议题 `#4` 和 `#6`。
- 议题 `#5` 被标记为[相关议题](related_issues.md)，但它不会自动关闭。

或者，当您[从议题创建合并请求](../merge_requests/creating_merge_requests.md#from-an-issue)时，它会继承议题的里程碑和标签。

出于性能原因，从现有仓库第一次推送时禁用自动关闭议题。

<a id="default-closing-pattern"></a>

#### 默认关闭 pattern

要自动关闭议题，请使用以下关键字，后跟议题引用。

可用关键字：

- Close, Closes, Closed, Closing, close, closes, closed, closing
- Fix, Fixes, Fixed, Fixing, fix, fixes, fixed, fixing
- Resolve, Resolves, Resolved, Resolving, resolve, resolves, resolved, resolving
- Implement, Implements, Implemented, Implementing, implement, implements, implemented, implementing

可用的议题引用格式：

- 本地议题 (`#123`).
- 跨项目议题 (`group/project#123`).
- 议题的完整 URL (`https://gitlab.example.com/group/project/issues/123`).

示例：

```plaintext
Awesome commit message

Fix #20, Fixes #21 and Closes group/otherproject#22.
This commit is also related to #17 and fixes #18, #19
and https://gitlab.example.com/group/otherproject/issues/23.
```

上一个提交消息关闭了此提交推送到的项目中的`#18`、`#19`、`#20` 和 `#21`，以及 `group/otherproject` 中的 `#22` 和 `#23`。`#17` 没有关闭，因为它与 pattern 不匹配。

您可以在多行提交消息或单行中使用关闭模式，使用 `git commit -m` 从命令行完成。

默认议题结束 pattern 正则表达式：

```shell
\b((?:[Cc]los(?:e[sd]?|ing)|\b[Ff]ix(?:e[sd]|ing)?|\b[Rr]esolv(?:e[sd]?|ing)|\b[Ii]mplement(?:s|ed|ing)?)(:?) +(?:(?:issues? +)?%{issue_ref}(?:(?: *,? +and +| *,? *)?)|([A-Z][A-Z0-9_]+-\d+))+)
```

#### 禁用自动关闭议题

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/19754) in GitLab 12.7.
-->

您可以在[项目设置](../settings/index.md)中按项目禁用自动议题关闭功能。

先决条件：

- 您必须至少具有该项目的维护者角色。

要禁用自动关闭议题：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **分支默认值**。
1. 选择 **在默认分支上自动关闭引用的议题**。
1. 选择 **保存修改**。

引用的议题仍会显示，但不会自动关闭。

更改此设置仅适用于新的合并请求或提交。已经关闭的议题保持原样。
禁用自动关闭议题仅适用于禁用了该设置的项目中的议题，在这个项目中，合并请求和提交仍然可以关闭另一个项目的议题。

#### 自定义议题关闭样式 **(FREE SELF)**

先决条件：

- 您必须对实例具有[实例管理员访问权限](../../../administration/index.md)。

了解如何更改安装的默认[议题关闭样式](../../../administration/issue_closing_pattern.md)。

## 更改议题类型

先决条件：

- 您必须是议题作者或至少具有项目的报告者角色。

要更改议题类型：

1. 在标题右侧，选择 **编辑标题和描述** (**{pencil}**)。
1. 编辑议题并从 **议题类型** 下拉列表中选择一个议题类型：

   - Issue
   - Incident<!--[Incident](../../../operations/incident_management/index.md)-->

1. 选择 **保存修改**.

## 删除议题

> 从垂直省略号菜单中删除议题引入于 14.6 版本

先决条件：

- 您必须具有项目的所有者角色。

要删除议题：

1. 在一个议题中，选择 **议题操作** (**{ellipsis_v}**)。
1. 选择 **删除议题**。

或者：

1. 在一个议题中，选择 **编辑标题和描述** (**{pencil}**)。
1. 选择 **删除议题**。

## 将议题提升为史诗 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/3777) in GitLab Ultimate 11.6.
> - Moved to GitLab Premium in 12.8.
> - Promoting issues to epics via the UI [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/233974) in GitLab Premium 13.6.
-->

您可以将议题提升到直接父组中的[史诗](../../group/epics/index.md)。

将议题提升为史诗：

1. 在一个议题中，选择 **议题操作** (**{ellipsis_v}**)。
1. 选择 **提升为史诗**。

或者，您可以使用 `/promote` [快速操作](../quick_actions.md#issues-merge-requests-and-epics)。

<!--
Read more about [promoting an issues to epics](../../group/epics/manage_epics.md#promote-an-issue-to-an-epic).
-->

## 将议题升级为事件（incident）

> - 引入于 14.5 版本。
> - 在创建时将议题类型设置为事件（incident）的快速操作引入于 15.8 版本。

您可以使用 `/promote_to_incident` [快速操作](../quick_actions.md)，将议题升级为[事件](../../../operations/incident_management/incidents.md)。

## 向迭代添加议题 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/216158) in GitLab 13.2.
> - Moved to GitLab Premium in 13.9.
-->

将议题添加到[迭代](../../group/iterations/index.md)：

1. 转到议题。
1. 在右侧边栏的 **迭代** 部分，选择 **编辑**。
1. 从下拉列表中，选择要与此议题关联的迭代。
1. 选择下拉列表之外的任何区域。

或者，您可以使用 `/iteration` [快速操作](../quick_actions.md#issues-merge-requests-and-epics)。

## 查看指派给您的所有议题

要查看指派给您的所有议题：

1. 在顶部栏上，将光标放在 **搜索** 框中。
1. 从下拉列表中，选择 **指派给我的议题**。

或者：

- 要使用[键盘快捷键](../../shortcuts.md)，请按 <kbd>Shift</kbd> + <kbd>i</kbd>。
- 在顶部栏的右上角，选择 **{issues}** **议题**。

<a id="filter-the-list-of-issues"></a>

## 过滤议题列表

> - 按迭代过滤功能引入于 13.6 版本。
> - 按迭代过滤功能从旗舰版移动到专业版于 13.9 版本。
> - 按类型过滤功能引入于 13.10 版本，功能标志为 `vue_issues_list`。默认禁用。
> - 按类型过滤功能在私有化部署版上启用于 14.10 版本。
> - 按类型过滤功能一般可用于 15.1 版本。功能标志 `vue_issues_list` 删除。
> - 按健康状态过滤功能引入于 15.5 版本。

要过滤议题列表：

1. 在议题列表上方，选择 **搜索或过滤结果...**。
1. 在出现的下拉列表中，选择您要过滤的属性。
1. 选择或键入用于过滤属性的运算符。可以使用以下运算符：
   - `=`：是
   - `!=`：不是
1. 输入文本，过滤属性。您可以使用 **None** 或 **Any** 过滤某些属性。
1. 重复此过程，按多个属性进行过滤。多个属性由逻辑 `AND` 连接。

<!--
GitLab displays the results on-screen, but you can also
[retrieve them as an RSS feed](../../search/index.md#retrieve-search-results-as-feed).

### Filter with the OR operator

> OR filtering for assignees was [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/23532) in GitLab 15.6 [with a flag](../../../administration/feature_flags.md) named `or_issuable_queries`. Disabled by default.

FLAG:
On self-managed GitLab, by default this feature is not available.
To make it available, ask an administrator to [enable the feature flag](../../../administration/feature_flags.md) named `or_issuable_queries`.
The feature is not ready for production use.

When this feature is enabled, you can use the OR operator (**is one of: `||`**)
when you [filter the list of issues](#filter-the-list-of-issues).

`is one of` represents an inclusive OR. For example, if you filter by `Assignee is one of Sidney Jones` and
`Assignee is one of Zhang Wei`, GitLab shows issues where either Sidney, Zhang, or both of them are assignees.
-->

### 按 ID 过滤议题

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **议题 > 列表**。
1. 在 **搜索** 框中，输入议题 ID。例如，输入过滤器 `#10`，仅返回议题 10。

![filter issues by specific ID](img/issue_search_by_id_v15_0.png)

## 复制议题引用

要引用实例中其他地方的议题，您可以使用其完整 URL 或简短引用，类似于 `namespace/project-name#123`，其中 `namespace` 是群组或用户名。

要将议题引用复制到剪贴板：

1. 转到议题。
1. 在右侧边栏的 **引用** 旁边，选择 **复制引用** (**{copy-to-clipboard}**)。

您现在可以将引用粘贴到另一个描述或评论中。

<!--
Read more about issue references in [GitLab-Flavored Markdown](../../markdown.md#gitlab-specific-references).
-->

## 复制议题电子邮件地址

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/18816) in GitLab 13.8.
-->

您可以通过发送电子邮件在议题中创建评论。
向此地址发送电子邮件会创建包含电子邮件正文的评论。

<!--
要了解有关通过发送电子邮件和必要配置创建评论的更多信息，请参阅[通过发送电子邮件回复评论](../../discussions/index.md#reply-to-a-comment-by-sending-电子邮件）。
-->

要复制议题的电子邮件地址：

1. 转到议题。
1. 在右侧边栏 **发邮件** 旁边，选择 **复制引用** (**{copy-to-clipboard}**)。

<a id="assignee"></a>

## 指派人

一个问题可以分配给一个或[多个用户](multiple_assignees_for_issues.md)。

指派人可以根据需要随时更改，指派人是对议题负责的人。
当一个议题被分配给某人时，它会出现在他们分配的议题列表中。

如果用户不是项目的成员，则只能将议题分配给他们，前提是他们自己创建议题或其他项目成员将议题分配给他们。

要更改议题的指派人：

1. 转到您的议题。
1. 在右侧边栏的 **指派人** 部分中，选择 **编辑**。
1. 从下拉列表中选择要添加为指派人的用户。
1. 选择下拉列表之外的任何区域。

无需刷新页面即可更改指派人。

## 类似议题

为防止同一主题出现重复议题，系统在您创建新议题时会搜索类似议题。

先决条件：

- GraphQL 必须启用。

当您在 **新建议题** 页面的标题文本框中键入时，系统会在当前项目中的所有议题中搜索标题和描述。仅返回您有权访问的议题。
标题文本框下方最多显示五个类似议题，按最近更新排序。

<a id="health-status"></a>

## 健康状态 **(ULTIMATE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/36427) in GitLab 12.10.
> - Health status of closed issues [can't be edited](https://gitlab.com/gitlab-org/gitlab/-/issues/220867) in GitLab 13.4 and later.
> - Issue health status visible in issue lists [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45141) in GitLab 13.6.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/213567) in GitLab 13.7.
-->

为了帮助您跟踪议题状态，您可以为每个议题分配一个状态。
此状态将议题标记为按计划进行，或需要注意以按计划进行。

先决条件：

- 您必须至少具有该项目的报告者角色。

要编辑议题的运行状况：

1. 转到议题。
1. 在右侧边栏的 **健康状态** 部分，选择 **编辑**。
1. 从下拉列表中，选择要添加到此问题的状态：

   - On track (green)
   - Needs attention (amber)
   - At risk (red)

您可以在以下位置查看议题的运行状况：

- 议题列表
- 史诗树
- 议题看板上的议题卡片

议题关闭后，无法编辑其运行状况，并且在重新打开议题之前 **编辑** 按钮将变为禁用状态。

## 发布议题 **(ULTIMATE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/30906) in GitLab 13.1.
-->

如果状态页面应用程序与项目相关联，您可以使用 `/publish` [快速操作](../quick_actions.md) 发布议题。

<!--
For more information, see [GitLab Status Page](../../../operations/incident_management/status_page.md).
-->

## 与议题相关的快速操作

您还可以使用[快速操作](../quick_actions.md#议题合并请求和史诗)来管理议题。

某些操作还没有相应的 UI 按钮。
您可以**仅通过使用快速操作**来执行以下操作：

- [添加或删除 Zoom 会议](associate_zoom_meeting.md)（`/zoom` 和 `/remove_zoom`）。
- [发布议题](#发布议题) (`/publish`)。
- 将议题克隆到同一个或另一个项目 (`/clone`)。
- 关闭一个议题并将其标记为另一个议题的副本（`/duplicate`）。
- 从项目中的另一个合并请求或议题中复制标记和里程碑 (`/copy_metadata`)。
