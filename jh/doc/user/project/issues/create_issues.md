---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 创建议题 **(FREE)**

创建议题时，系统会提示您输入议题的字段。
如果您知道要分配给议题的值，则可以使用[快捷操作](../quick_actions.md)输入它们。

您可以在极狐GitLab 中以多种方式创建议题：

- [从项目创建](#from-a-project)
- [从群组创建](#from-a-group)
- [从另一个议题或事件创建](#from-another-issue-or-incident)
- [从议题看板创建](#from-an-issue-board)
- [通过发送电子邮件创建](#by-sending-an-email)
- [使用带有预填入字段的 URL 创建](#using-a-url-with-prefilled-values)
- [使用服务台创建](#using-service-desk)

<a id="from-a-project"></a>

## 从项目创建议题

先决条件：

- 您必须至少拥有该项目的访客角色。

要创建议题：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 您可以：

   - 在左侧边栏上，选择 **议题**，然后在右上角选择 **新建议题**。
   - 在顶部栏上，选择加号 (**{plus-square}**)，然后在 **此项目** 下，选择 **新建议题**。

1. 完成[填写字段](#fields-in-the-new-issue-form)。
1. 选择 **创建议题**。

新创建的议题开放。

<a id="from-a-group"></a>

## 从群组创建议题

议题属于项目，但是当您在群组中时，您可以访问和创建属于群组中的项目的议题。

先决条件：

- 您必须至少具有群组中项目的访客角色。

要从群组创建议题：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **议题**。
1. 在右上角，选择 **选择要创建议题的项目**。
1. 选择您要为其创建议题的项目。该按钮现在反映了选定的项目。
1. 选择 **在 `<project name>` 中新建议题**。
1. 完成[填写字段](#fields-in-the-new-issue-form)。
1. 选择 **创建议题**。

新创建的议题开放。

您最近选择的项目将成为您下次访问的默认项目。
如果您主要为同一项目创建议题，这可以为您节省大量时间。

<a id="from-another-issue-or-incident"></a>

## 从另一个议题或事件创建

> - 新议题与原始议题相关联，引入于 14.3 版本。
> - **关联到**复选框引入于 14.9 版本。

您可以从现有议题创建新议题，然后可以将这两个议题标记为相关。

先决条件：

- 您必须至少具有群组中项目的访客角色。

从另一个议题创建议题：

1. 在现有议题中，选择 **议题操作** (**{ellipsis_v}**)。
1. 选择 **新建关联议题**。
1. 完成[填写字段](#fields-in-the-new-issue-form)。
   新建议题表单有一个 **关联到议题 #123** 复选框，其中 `123` 是议题来源的 ID。如果您选中此复选框，则这两个议题将变为[相关联](related_issues.md)。
1. 选择 **创建议题**。

新创建的议题开放。

<a id="from-an-issue-board"></a>

## 从议题看板创建议题

您可以从[议题看板](../issue_board.md)创建新议题。

先决条件：

- 您必须至少具有项目的访客角色。

要从项目议题看板创建议题：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 选择 **议题 > 看板**。
1. 在版块列表的顶部，选择 **新建议题** (**{plus-square}**)。
1. 输入议题的标题。
1. 选择 **创建议题**。

要从群组议题看板创建议题：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 选择 **议题 > 看板**。
1. 在版块列表的顶部，选择 **新建议题** (**{plus-square}**)。
1. 输入议题的标题。
1. 在 **项目** 下，选择议题所属的群组中的项目。
1. 选择 **创建议题**。

议题已创建并显示在看板列表中，它共享列表的特性，因此，例如列表的范围限定为标记 `Frontend`，则新议题也具有此标记。

<a id="by-sending-an-email"></a>

## 通过发送电子邮件创建

您可以在项目的**议题列表**页面上，发送电子邮件以在项目中创建议题。

先决条件：

- 您的实例必须配置[接收电子邮件](../../../administration/incoming_email.md)。
- 议题列表中必须至少有一个议题。
- 您必须至少拥有该项目的访客角色。

要将议题通过电子邮件发送到项目：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 选择 **议题**。
1. 在页面底部，选择 **通过电子邮件向该项目发送新议题**。
1. 要复制电子邮件地址，请选择 **复制** (**{copy-to-clipboard}**)。
1. 从您的电子邮件客户端，向该地址发送一封电子邮件。主题用作新议题的标题，电子邮件正文成为描述。您可以使用 [Markdown](../../markdown.md) 和[快速操作](../quick_actions.md)。

成功后，创建了一个新议题，您的用户作为作者。
您可以将此地址保存为电子邮件客户端中的联系人以再次使用。

WARNING:
您看到的电子邮件地址是私人电子邮件地址，专为您生成。**仅保存给自己**，因为任何知道它的人都可以创建议题或合并请求。

要重新生成电子邮件地址：

1. 在议题列表中，选择 **通过电子邮件向该项目发送新议题**。
1. 选择 **重置此令牌**。

<a id="using-a-url-with-prefilled-values"></a>

## 使用带有预填入值的 URL 创建

要直接链接到带有预填充字段的新议题页面，请在 URL 中使用查询字符串参数。您可以在外部 HTML 页面中嵌入 URL 以使用预填充的某些字段创建议题。

| 字段                | URL 参数         | 备注                                                                                                                          |
| -------------------- | --------------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| 标题              | `issue[title]`        | 必须是 URL 编码<!--[URL 编码](../../../api/index.md#namespaced-path-encoding)-->。                                                          |
| 议题类型           | `issue[issue_type]`   | `incident` 或 `issue`。                                                                                                  |
| 描述模板 | `issuable_template`   | 必须是 URL 编码<!--[URL 编码](../../../api/index.md#namespaced-path-encoding)-->。 |
| 描述          | `issue[description]`  | 必须是 URL 编码<!--[URL 编码](../../../api/index.md#namespaced-path-encoding)-->。如果与 `issuable_template` 或[默认议题模板](../description_templates.md#set-a-default-template-for-merge-requests-and-issues)结合使用，则 `issue[description]` 值附加到模板中。  |
| 私密         | `issue[confidential]` | 如果为 `true`，则议题被标记为机密。                                                                                |
| 关联到           | `add_related_issue`   | 议题的数字 ID。如果存在，议题表单显示 [**关联到...** 复选框](#from-another-issue-or-incident)，可以选择性地将新议题链接到指定的现有议题。 |

调整这些示例以形成带有预填入字段的新议题 URL。

在项目中创建议题：

- 带有预填入的标题和描述：

  ```plaintext
  https://jihulab.com/gitlab-cn/gitlab/-/issues/new?issue[title]=Whoa%2C%20we%27re%20half-way%20there&issue[description]=Whoa%2C%20livin%27%20in%20a%20URL
  ```

- 使用预填入的标题和描述模板：

  ```plaintext
  https://jihulab.com/gitlab-cn/gitlab/-/issues/new?issue[title]=Validate%20new%20concept&issuable_template=Feature%20Proposal%20-%20basic
  ```

- 带有预填入的标题、描述并标记为私密：

  ```plaintext
  https://jihulab.com/gitlab-cn/gitlab/-/issues/new?issue[title]=Validate%20new%20concept&issue[description]=Research%20idea&issue[confidential]=true
  ```

## 使用服务台创建

要提供电子邮件支持，请为您的项目启用[服务台](../service_desk.md)。

现在，当您的客户发送新电子邮件时，可以在适当的项目中创建新议题并从那里跟进。

<a id="fields-in-the-new-issue-form"></a>

### 新建议题表单中的字段

> 迭代字段引入于 15.6 版本。

创建新议题时，您可以填写以下字段：

- 标题
- 类型：issue（默认）或 incident
- [描述模板](../description_templates.md)：覆盖描述文本框中的任何内容
- 描述：您可以使用 [Markdown](../../markdown.md) 和[快速操作](../quick_actions.md)
- 使议题具有[机密性](confidential_issues.md)的复选框
- [指派人](managing_issues.md#assignee)
- [权重](issue_weight.md)
- [史诗](../../group/epics/index.md)
- [截止日期](due_dates.md)
- [里程碑](../milestones/index.md)
- [标记](../labels.md)
- [迭代](../../group/iterations/index.md)
