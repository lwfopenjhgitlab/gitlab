---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 私密议题 **(FREE)**

私密议题是仅对具有[足够权限](#私密议题的访问权限)的项目成员可见的[议题](index.md)。
开源项目和公司都可以使用私密问题来保护安全漏洞的私密性或防止意外泄露。

## 设置私密议题

您可以在议题创建期间或通过编辑现有议题，将问议题设为私密。

创建新议题时，文本区域正下方的复选框可用于将议题标记为私密。选中该框并点击 **创建议题** 按钮以创建议题。对于现有议题，编辑它们，选中私密复选框并点击 **保存更改**。

当您在项目中创建私密议题时，该项目将列在您的[个人资料](../../profile/index.md)的 **参与贡献的项目** 部分中。**参与贡献的项目**不显示有关私密议题的信息；它只显示项目名称。

![Creating a new confidential issue](img/confidential_issues_create_v15_4.png)

## 修改议题私密性

有两种方法可以更改议题的私密性。

第一种方法是编辑议题并切换私密性复选框。保存议题后，会更新议题的私密性。

第二种方法是在侧边栏中找到**私密性**部分，然后单击**编辑**。出现一个弹出窗口，让您可以选择打开或关闭私密性。

| 关闭私密性 | 打开私密性 |
| :-----------: | :----------: |
| ![Turn off confidentiality](img/turn_off_confidentiality_v15_1.png) | ![Turn on confidentiality](img/turn_on_confidentiality_v15_1.png) |

每次从常规更改为私密，反之亦然，都会在问题评论中的系统注释中指出。

![Confidential issues system notes](img/confidential_issues_system_notes_v15_4.png)

将议题设为私密后，只有至少具有项目报告者角色的用户才能访问该议题。
具有 Guest 或 Minimal 角色的用户即使在更改前积极参与，也无法访问该问题。

## 私密议题的表现

有一些事情可以在视觉上将私密议题与常规议题区分开来。在议题索引页面视图中，您可以在标记为私密的议题旁边看到斜线 (**(eye-slash)**) 图标：

![Confidential issues index page](img/confidential_issues_index_page.png)

如果您没有[足够的权限](#私密议题的访问权限)，您根本看不到私密议题。

---

同样，在议题内部，您可以在议题编号旁边看到斜线图标。如果您评论的议题是私密的，评论区也有类似现象。

![Confidential issue page](img/confidential_issues_issue_page.png)

侧边栏上还有一个指示私密性的指示器。

| 私密议题 | 非私密议题 |
| :-----------: | :----------: |
| ![Sidebar confidential issue](img/sidebar_confidential_issue.png) | ![Sidebar not confidential issue](img/sidebar_not_confidential_issue.png) |

## 私密议题的合并请求

尽管您可以在公共项目中将议题设为私密，但您不能提出合并请求。了解如何创建私密议题的合并请求<!--[私密议题的合并请求](../merge_requests/confidential.md)-->以防止私人数据泄露。

尽管您可以在公开项目中创建私密议题（并将现有议题设为私密），但您不能提出私密合并请求。
了解如何创建 [私密议题的合并请求](../merge_requests/confidential.md) 来防止私人数据泄露。

## 私密议题的访问权限

私密议题有两种级别的访问权限。一般规则是私密议题仅对至少具有报告者权限<!--[报告者权限](../../permissions.md#项目成员权限)-->的项目成员可见。但是，来宾用户也可以创建私密议题，但只能查看他们自己创建的议题。
具有访客角色或非成员的用户也可以阅读私密议题（如果他们被指派到该议题）。当访客用户或非成员从私密议题中被取消指派时，他们将无法再查看它。

私密议题也隐藏在非特权用户的搜索结果中。例如，以下是具有维护者角色<!--[维护者角色](../../permissions.md)-->和来宾访问权限的用户在项目的搜索结果中分别看到的内容。

| 维护者角色                                                                       | 来宾访问                                                                     |
|:---------------------------------------------------------------------------------------|:---------------------------------------------------------------------------------|
| ![Confidential issues search by maintainer](img/confidential_issues_search_master.png) | ![Confidential issues search by guest](img/confidential_issues_search_guest.png) |

<!--
## Related links

- [Merge requests for confidential issues](../merge_requests/confidential.md)
- [Make an epic confidential](../../group/epics/manage_epics.md#make-an-epic-confidential)
- [Mark a comment as confidential](../../discussions/index.md#mark-a-comment-as-confidential)
- [Security practices for confidential merge requests](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/developer.md#security-releases-critical-non-critical-as-a-developer) at GitLab
-->
