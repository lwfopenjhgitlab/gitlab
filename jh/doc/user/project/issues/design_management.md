---
stage: Plan
group: Product Planning
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 设计管理 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/groups/gitlab-org/-/epics/660) in GitLab 12.2.
> - Support for SVGs [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/12771) in GitLab 12.4.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/212566) from GitLab Premium to GitLab Free in 13.0.
> - Design Management section in issues [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/223193) in GitLab 13.2, with a feature flag named `design_management_moved`. In earlier versions, designs were displayed in a separate tab.
> - Design Management section in issues [feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/223197) for new displays in GitLab 13.4.
-->

使用设计管理，您可以将设计资产（包括线框和模型）上传到议题并将它们存储在一个地方。产品设计师、产品经理和工程师可以通过单一的来源就设计进行协作。

您可以与您的团队共享设计模型，或者可以查看和解决视觉回归。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For a video overview, see [Design Management (GitLab 12.2)](https://www.youtube.com/watch?v=CCMtCqdK_aM).
-->

## 要求

- 必须启用 [Git 大文件存储(LFS)](../../../topics/git/lfs/index.md)：
  - 在 SaaS 上已启用
  - 在私有化部署实例上，管理员必须全局启用 LFS
  - 在 SaaS 和  私有化部署实例上，必须为项目本身启用 LFS。
    如果全局启用，则默认为所有项目启用 LFS。如果您为项目禁用了它，则必须再次启用它。

  设计存储为 LFS 对象。
  图像缩略图存储为其它上传文件，并且不与项目相关联，而是与特定设计模型相关联。

- 项目必须使用哈希存储<!--[hashed storage](../../../administration/raketasks/storage.md#migrate-to-hashed-storage)-->。

  新创建的项目默认使用哈希存储。

  极狐GitLab 管理员可以通过转到 **管理中心 > 项目** 然后选择有问题的项目来验证项目的存储类型。如果项目的 **Gitaly 相对路径** 包含 `@hashed`，则可以将项目标识为哈希存储。

如果不满足要求，您会在**设计**部分收到通知。

## 支持的文件类型

您可以上传以下类型的文件作为设计：

- BMP
- GIF
- ICO
- JPEG
- JPG
- PNG
- SVG
- TIFF
- WEBP

<!--
Support for PDF files is tracked in [issue 32811](https://gitlab.com/gitlab-org/gitlab/-/issues/32811).

## Known issues

- Design Management data isn't deleted when:
  - [A project is destroyed](https://gitlab.com/gitlab-org/gitlab/-/issues/13429).
  - [An issue is deleted](https://gitlab.com/gitlab-org/gitlab/-/issues/13427).
- In GitLab 12.7 and later, Design Management data [can be replicated](../../../administration/geo/replication/datatypes.md#limitations-on-replicationverification)
  by Geo but [not verified](https://gitlab.com/gitlab-org/gitlab/-/issues/32467).
-->

## 查看设计

**设计**部分在议题描述中。

先决条件：

- 您必须至少具有项目的访客角色。

查看设计：

1. 访问议题。
1. 在 **设计** 部分，选择您要查看的设计图像。

您选择的设计将打开。然后，您可以[放大](#zoom-in-on-a-design)或[创建评论](#add-a-comment-to-a-design)。

![Designs section](img/design_management_v14_10.png)

查看设计时，您可以移动到其他设计：

- 在右上角，选择 **转到上一个设计** (**{chevron-lg-left}**) 或 **转到下一个设计** (**{chevron-lg-right}**)。
- 按键盘上的 <kbd>Left</kbd> 或 <kbd>Right</kbd>。

要返回问题视图，请执行以下任一操作：

- 在左上角，选择关闭图标 (**{close}**)。
- 按键盘上的 <kbd>Esc</kbd>。

添加设计时，图像缩略图上会显示一个绿色图标 (**{plus-square}**)。当前版本中的设计已[更改](#add-a-new-version-of-a-design)时，会显示一个蓝色图标 (**{file-modified-solid}**)。

<a id="zoom-in-on-a-design"></a>

### 放大设计

您可以通过放大和缩小图像来更详细地浏览设计：

- 要控制缩放量，请选择图像底部的加号 (`+`) 和减号 (`-`)。
- 要重置缩放级别，请选择重做图标 (**{redo}**)。

要在放大时移动图像，请拖动图像。

<a id="add-a-design-to-an-issue"></a>

## 添加设计到议题

先决条件：

- 您必须至少具有项目的开发者角色。
- 在 13.1 及更高版本中，上传文件的名称不得超过 255 个字符。

要将设计添加到议题：

1. 访问议题。
1. 您可以执行以下两者之一：
   - 选择 **上传设计**，然后从文件浏览器中选择图像。您一次最多可以选择 10 个文件。
   <!-- vale gitlab.SubstitutionWarning = NO -->
   - 选择 **点击上传**，然后从您的文件浏览器中选择图像。您一次最多可以选择 10 个文件。
   <!-- vale gitlab.SubstitutionWarning = YES -->

   - 从您的文件浏览器中拖动一个文件并将其拖放到 **设计** 部分的拖放区中。

     ![Drag and drop design uploads](img/design_drag_and_drop_uploads_v13_2.png)

   - 截取屏幕截图或将本地图像文件复制到剪贴板，将光标悬停在拖放区上，然后按 <kbd>Control</kbd> 或 <kbd>Cmd</kbd> + <kbd>V</kbd>。

     像这样粘贴图像时，请记住以下几点：

     - 您一次只能粘贴一张图片。当您粘贴多个复制的文件时，只会上传第一个。
     - 如果您要粘贴屏幕截图，图像将添加，生成名称为：`design_<timestamp>.png` 的 PNG 文件。
     - Internet Explorer 不支持。

<a id="add-a-new-version-of-a-design"></a>

## 添加新版本的设计

随着对设计的讨论继续进行，您可能想要上传设计的新版本。

先决条件：

- 您必须至少具有项目的开发者角色。

为此，请使用相同的文件名[添加设计](#add-a-design-to-an-issue)。

要浏览所有设计版本，请使用 **设计** 部分顶部的下拉列表。

### 跳过的设计

当您上传与现有上传设计具有相同文件名且相同的图像时，它会被跳过。这意味着不会创建新版本的设计。
跳过设计时，会显示警告消息。

## 归档设计

您可以归档单个设计或一次选择其中几个进行归档。

先决条件：

- 您必须至少具有项目的开发者角色。

归档单个设计：

1. 选择设计以放大查看。
1. 在右上角，选择 **归档设计** (**{archive}**)。
1. 选择 **归档设计**。

一次归档多个设计：

1. 选中您要存档的设计上的复选框。
1. 选择 **存档已选择项**。

NOTE:
只能存档最新版本的设计。
存档的设计不会永久丢失。您可以浏览[以前的版本](#add-a-new-version-of-a-design)。

## 重新排序设计

您可以通过将设计拖动到新位置来更改它们的顺序。

<a id="add-a-new-version-of-a-design"></a>

## 为设计添加评论

您可以在上传的设计上开始[讨论](../../discussions/index.md)：

<!-- vale gitlab.SubstitutionWarning = NO -->
1. 访问议题。
1. 选择设计。
1. 单击或点击图像。在该位置创建一个图钉，标识讨论的位置。
1. 输入您的信息。
1. 选择 **评论**。
<!-- vale gitlab.SubstitutionWarning = YES -->

您可以通过在图像周围拖动图钉来调整图钉的位置。当您的设计布局发生变化时，或者当您想要移动一个图钉，来在其位置添加一个新图钉时，您可以使用它。

新的讨论主题有不同的 pin 号，您可以使用它们来引用。

在 12.5 及更高版本中，新的讨论会输出到议题活动中，以便参与的每个人都可以参与讨论。

<a id="resolve-a-discussion-thread-on-a-design"></a>

## 解决有关设计的讨论主题

讨论完设计的一部分后，您可以解决讨论主题。

要将主题标记为已解决或未解决，请执行以下任一操作：

- 在讨论的第一条评论的右上角，选择 **解决主题** 或 **将主题置为未解决** (**{check-circle}**)。
- 向主题添加新评论并选中或清除 **解决主题** 复选框。

解决讨论主题还会将与主题内的笔记相关的任何待处理的[待办事项](../../todos.md)标记为已完成。只有触发操作的用户的待办事项受到影响。

您已解决的评论图钉会从设计中消失，以便为新的讨论腾出空间。
要重新访问已解决的讨论，请在可见线程下方展开 **已解决的评论**。

## 为设计添加待办事项

要为设计添加[待办事项](../../todos.md)，请在设计侧边栏中选择 **添加待办事项**。

## 在 Markdown 中引用设计

要在极狐GitLab 的 [Markdown](../../markdown.md) 文本框中引用设计，例如，在评论或描述中，粘贴其 URL。然后将其显示为简短引用。

例如，如果您在某处引用设计：

```markdown
See https://gitlab.com/gitlab-org/gitlab/-/issues/13195/designs/Group_view.png.
```

它呈现为：

> See [#13195[Group_view.png]](https://gitlab.com/gitlab-org/gitlab/-/issues/13195/designs/Group_view.png).

## 设计活动记录

设计中的用户活动事件（创建、删除和更新）由极狐GitLab 跟踪并显示在用户配置文件、群组和项目活动页面。

## GitLab-Figma 插件

您可以使用 GitLab-Figma 插件将您的设计从 Figma 直接上传到极狐GitLab 中的议题。

要在 Figma 中使用该插件，请从 [Figma 目录](https://www.figma.com/community/plugin/860845891704482356)安装它并通过个人访问令牌连接到极狐GitLab。

<!--
For more information, see the [plugin documentation](https://gitlab.com/gitlab-org/gitlab-figma-plugin/-/wikis/home).
-->
