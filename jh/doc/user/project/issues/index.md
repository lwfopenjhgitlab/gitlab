---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 议题 **(FREE)**

使用议题来协作提出想法、解决问题和计划工作。与您的团队和外部合作者分享和讨论提案。

您可以将议题用于多种用途，根据您的需求和工作流程进行定制。

- 讨论一个想法的实施。
- 跟踪任务和工作状态。
- 接受功能建议、问题、支持请求或错误报告。
- 详细说明代码实现。

<!--
For more information about using issues, see the GitLab blog post:
[Always start a discussion with an issue](https://about.gitlab.com/blog/2016/03/03/start-with-an-issue/).
-->

议题始终与特定项目相关联。如果您在一个群组中有多个项目，则可以一次查看所有项目的议题。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
To learn how the GitLab Strategic Marketing department uses GitLab issues with [labels](../labels.md) and
[issue boards](../issue_board.md), see the video on
[Managing Commitments with Issues](https://www.youtube.com/watch?v=cuIHNintg1o&t=3).
-->

## 相关话题

- [创建议题](managing_issues.md#创建议题)
<!--- [Create an issue from a template](../../project/description_templates.md#use-the-templates)-->
- [移动议题](managing_issues.md#移动议题)
- [关闭议题](managing_issues.md#关闭议题)
- [删除议题](managing_issues.md#删除议题)
- [提升议题](managing_issues.md#将议题提升为史诗)
- [设定截止日期](due_dates.md)
- [导入议题](csv_import.md)
- [导出议题](csv_export.md)
- [上传设计到议题](design_management.md)
- [关联议题](related_issues.md)
- [Cross-link 议题](crosslinking_issues.md)
- [排序议题列表](sorting_issue_lists.md)
<!--- [Search for issues](../../search/index.md#filter-issue-and-merge-request-lists)-->
- [史诗](../../group/epics/index.md)
- [议题看板](../issue_board.md)

<!--
- [Issues API](../../../api/issues.md)
- [Configure an external issue tracker](../../../integration/external-issue-tracker.md)
-->

