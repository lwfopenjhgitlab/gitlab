---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 议题的多指派人 **(PREMIUM)**

> - 在 13.9 版本中移到专业版

在拥有共享所有权的大型团队中，可能很难跟踪谁在处理某个议题、谁已经完成或谁还没有开始。

您可以为一个议题添加多个[指派人](managing_issues.md#指派人)，这样更容易跟踪，并明确谁对它负责。

议题的多个指派人使协作更顺畅，可以清楚地显示共享的责任。 所有指派人都会显示在您团队的工作流程中并接收通知（就像他们作为单个指派人一样），从而简化了沟通成本和所有权混乱的问题。

一旦指派人完成了他们的工作，他们就会将自己作为指派人移除，明确表示他们的任务已经完成。

![multiple assignees for issues](img/multiple_assignees_for_issues.png)
