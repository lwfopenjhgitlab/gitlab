---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 截止日期 **(FREE)**

可以在[议题](index.md)中使用截止日期，来跟踪截止日期并确保功能按时交付。用户至少需要报告者权限<!--[报告者权限](../../permissions.md)-->才能编辑截止日期。所有有权查看议题的用户都可以查看截止日期。

## 设置一个截止日期

创建问题时，选择 **截止日期** 字段以显示用于选择日期的日历。要删除日期，请选择日期文本并将其删除。日期与服务器的时区有关，而不是用户设置截止日期的时区。

![Create a due date](img/due_dates_create.png)

您还可以使用议题侧栏设置截止日期。展开侧边栏并选择 **编辑** 以选择截止日期或删除现有日期。更改会立即保存。

![Edit a due date with the sidebar](img/due_dates_edit_sidebar.png)

设置截止日期的最后一种方法是，直接在议题的描述或评论中使用[快速操作](../quick_actions.md)：

- `/due <date>`：设置截止日期。 有效的 `<date>` 示例包括 `in 2 days`、`this Friday` 和 `December 31st`。
- `/remove_due_date`：删除截止日期。

## 利用截止日期

您可以在议题列表中查看带有到期日期的议题。过期议题的图标和日期为红色。要按截止日期对议题进行排序，请从右侧的下拉菜单中选择 **截止日期**。然后将议题从最早的截止日期到最晚进行排序。要在顶部显示最新截止日期的议题，请选择 **排序方向** (**{sort-lowest}**)。

截止日期也会出现在您的待办事项列表<!--[待办事项列表](../../todos.md) 中-->。

![Issues with due dates in the to dos](img/due_dates_todos.png)

在未决议题到期的前一天，会向该议题的所有参与者发送一封电子邮件。与截止日期一样，“截止日期前一天”由服务器的时区决定。

带有到期日期的议题也可以导出为 iCalendar feed。可以将 feed 的 URL 添加到日历应用程序。通过选择在以下页面中，**操作** (**{ellipsis_v}**) 下拉列表中的 **订阅日历** 按钮，可以访问此 feed：

- 顶部右侧链接的 **已分配议题** 页面
- **项目议题** 页面
- **群组议题** 页面
