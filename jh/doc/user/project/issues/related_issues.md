---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 关联议题 **(FREE)**

>  "relates to" 关系在 13.4 版本中被转移到[标准版](https://about.gitlab.cn/pricing/)。 

关联议题用来表示任何两个议题之间的双向关系，会显示在议题描述下方的块中。您可以链接不同项目中的议题。

只有当用户可以看到这两个议题时，关系才会显示在UI中。当您尝试关闭具有开放受阻的议题时，会显示警告。

<!--
NOTE:
如果希望通过API管理相关议题，参考[关联议题API文档](../../../api/issue_links.md).
-->

## 添加一个关联议题

> - 从 12.8版本开始引入。 
> - 在 13.0 版本中尝试关闭被其他人阻止的问题时发出警告。当您尝试关闭开发受阻的议题时，您会看到可以一条可以忽略的警告。 

要将一个议题链接到另一个议题：

1. 在议题的 **关联项** 部分中，选择添加关联议题按钮 (**{plus}**)。

1. 选择两个议题的关联关系。可以有如下选项：

   - **relates to**
   - **[blocks](#blocking-issues)**
   - **[is blocked by](#blocking-issues)**

1. 输入议题号或者议题的完整URL地址。

   ![增加一个关联议题](img/related_issues_add_v15_3.png)

   同一项目的议题可以仅通过编号指定。
   来自不同项目的会议需要额外的信息，比如组和项目名称。 例如：

   - 同一个项目：`#44`
   - 同一个组：`project#44`
   - 不同的组：`group/project#44`

   正确有效的引用会被添加到一个临时列表中供您查看。 

1. 选择了所有的相关议题后，选择 **添加**.

添加完所有关联议题后，会自动进行归类，以便可以更好地理解它们的关系。

![关联议题分类](img/related_issue_block_v15_3.png)

您还可以从提交信息或另一个议题或 MR 的描述中添加关联议题。[交叉关联议题](crosslinking_issues.md)。

## 移除关联议题

在 **关联项** 部分, 点击要删除的关联议题右边的删除按钮 (**{close}**)。

由于双向关系的因素，该关系不再出现在任一议题中。

![移除关联议题](img/related_issues_remove_v15_3.png)

<!--参阅[权限](../../permissions.md)章节了解更多信息。-->

## 阻塞议题 **(PREMIUM)**

当您[添加链接议题](#添加链接议题)时，您可以显示它**阻塞**另一个议题，或被另一个议题**阻塞**。

对于阻塞其它议题的议题，在议题列表和[看板](../issue_board.md) 中，在标题旁边显示一个图标 (**{issue-block}**)。
当阻塞议题关闭或它们的关系发生更改或[删除](#移除关联议题)时，该图标会消失。

如果您尝试使用“关闭议题”按钮关闭被阻止的议题，则会出现一条确认消息。
