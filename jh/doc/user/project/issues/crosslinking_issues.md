---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Crosslinking 议题 **(FREE)**

有几种方法可以提及某个议题或使[议题](index.md)出现在彼此的[相关议题](related_issues.md) 部分中。

获取更多关于议题的信息，请参阅[议题文档](index.md)。

<a id="from-commit-messages"></a>

## 来自提交信息

每次在提交消息中提及议题时，您都在开发工作流程的两个阶段之间建立了关系：议题本身和与该议题相关的第一次提交。

如果议题和您提交的代码都在同一个项目中，请将 `#xxx` 添加到提交消息，其中 `xxx` 是议题编号。

```shell
git commit -m "this is my commit message. Ref #xxx"
```

如果它们在不同的项目中，但在同一群组中，则在提交消息中添加 `projectname#xxx`。

```shell
git commit -m "this is my commit message. Ref projectname#xxx"
```

如果他们不在同一个群组中，您可以将完整的 URL 添加到议题中（`https://jihulab.com/<username>/<projectname>/issues/<xxx>`）。

```shell
git commit -m "this is my commit message. Related to https://jihulab.com/<username>/<projectname>/issues/<xxx>"
```

当然，你可以用你自己实例的 URL 替换 `gitlab.cn`。

将您的第一次提交与您的议题相关联，这与使用价值流分析跟踪您的过程有关。它衡量计划实施该议题所花费的时间，即创建议题和进行第一次提交之间的时间。

## 来自相关议题

在合并请求和其他议题中，提及相关议题有助于您的团队成员和协作者知道存在关于同一主题的未解决议题。

当[从提交消息中提到一个议题](#来自提交信息)时，您会按照上面的解释这样做。

在 议题 `#222` 中提到议题 `#111` 时，议题 `#111` 也会在其跟踪器中显示通知。也就是说，您只需要提及一次相关，即可在两个议题中显示。在[合并请求](#来自合并请求)中提及议题时同样有效。

![issue mentioned in issue](img/mention_in_issue.png)

## 来自合并请求

在合并请求评论中提及议题的方式，与在[相关议题](#来自相关议题)中提及的方式完全相同。

当您在合并请求描述中提及议题时，它[将议题和合并请求关联在一起](#来自相关议题)。此外，您还可以在合并请求合并后，立即[将议题设置为自动关闭](managing_issues.md#自动关闭议题)。

![issue mentioned in MR](img/mention_in_merge_request.png)
