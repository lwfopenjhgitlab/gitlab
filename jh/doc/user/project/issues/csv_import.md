---
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 从 CSV 导入议题 **(FREE)**

您可以通过上传包含以下列的 CSV 文件将议题导入项目：

| 名称          | 是否必需？              | 描述                                     |
|:--------------|:-----------------------|:-------------------------------------------------|
| `title`       | **{check-circle}** Yes | 议题的标题。                                     |
| `description` | **{check-circle}** Yes | 议题描述。                               |
| `due_date`    | **{dotted-circle}** No | `YYYY-MM-DD` 格式的议题到期日期。引入于 15.2 版本。 |

不导入其他列中的数据。

您可以使用 `description` 字段嵌入[快速操作](../quick_actions.md)，向议题添加其他数据。
例如，标记、指派人和里程碑。

或者，您可以[移动议题](managing_issues.md#move-an-issue)。移动议题会保留更多数据。

上传 CSV 文件的用户被设置为导入议题的作者。

NOTE:
导入议题需要开发者<!--[开发者](../../permissions.md)-->或更高的权限级别。

## 准备导入

- 考虑导入仅包含几个议题的测试文件。如果不使用 API，则无法撤消大型导入。
- 确保您的 CSV 文件符合[文件格式](#csv-文件格式)要求。

## 导入文件

要导入议题：

1. 转到您项目的议题列表页面。
1. 打开导入功能，具体取决于项目是否有问题：
    - 存在现有议题：选择右上角 **批量编辑** 旁边的导入图标，再选择 **操作** (**{ellipsis_v}**) **> 导入 CSV**。
    - 项目没有议题：选择页面中间的 **导入 CSV**。
1. 选择要导入的文件，然后选择 **导入议题**。

文件在后台处理，导入完成后会向您发送通知电子邮件。

## CSV 文件格式

从 CSV 文件导入议题时，必须以某种方式对其进行格式化：

要导入议题，极狐GitLab 要求 CSV 文件具有特定格式：

| 元素                | 格式 |
|------------------------|--------|
| 标题行             | CSV 文件必须包含以下标题：`title` 和 `description`。标题的大小写无关紧要。 |
| 列                | 不导入 `title` 和`description` 之外的列中的数据。 |
| 分隔符            | 从标题行中自动检测列分隔符。支持的分隔符有：逗号（`,`）、分号（`;`）和制表符（`\t`）。行分隔符可以是 `CRLF` 或 `LF`。 |
| 双引号字符 | 双引号 (`"`) 字符用于引用字段，允许在字段中使用列分隔符（请参阅下面示例 CSV 数据中的第三行）。插入双引号 (`"`) 在带引号的字段中，连续使用两个双引号字符，即 `""`。 |
| 数据行              | 在标题行之后，后续行必须遵循相同的列顺序。议题标题是必需的，而描述是可选的。 |

如果您有特殊字符（例如，`,` 或 `\n`）或字段中有多行（例如，使用[快速操作](../quick_actions.md)时），请用双引号将字符括起来 (`"`)。

使用[快速操作](../quick_actions.md)时，每个操作必须位于单独的行上。

示例 CSV 数据：

```plaintext
title,description,due date
My Issue Title,My Issue Description,2022-06-28
Another Title,"A description, with a comma",
"One More Title","One More Description",
An Issue with Quick Actions,"Hey can we change the frontend?

/assign @sjones
/label ~frontend ~documentation",
```

### 文件大小

限制取决于极狐GitLab 实例的托管方式：

- 私有化部署版：由极狐GitLab 实例的“最大附件大小”的配置值设置。
- SaaS 版：在 JiHuLab.com 上，设置为 10 MB。
