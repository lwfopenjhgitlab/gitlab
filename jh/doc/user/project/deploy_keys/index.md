---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 部署密钥 **(FREE)**

使用部署密钥访问托管在极狐GitLab 中的仓库。在大多数情况下，您使用部署密钥从外部主机访问仓库，例如构建服务器或持续集成 (CI) 服务器。

根据您的需要，您可能希望使用[部署令牌](../deploy_tokens/index.md)来访问仓库。

| 属性        |  部署密钥 | 部署令牌 |
|------------------|-------------|--------------|
| Sharing          | 可在多个项目之间共享，即使是不同群组中的项目。 | 属于一个项目或群组。 |
| Source           | 在外部主机上生成的公共 SSH 密钥。 | 在您的极狐GitLab 实例上生成，仅在创建时提供给用户。 |
| Validity         | 只要注册并启用并且创建它的用户存在时就有效。 | 可以指定一个有效期。 |
| Registry access  | 无法访问软件包库。 | 可以读取和写入软件包库。 |

## 范围

部署密钥在创建时具有定义的范围：

- **项目部署密钥：**访问仅限于所选项目。
- **公开部署密钥：**可以向极狐GitLab 实例中的*任何*项目授予访问权限。必须由至少具有维护者角色的用户[授予](#grant-project-access-to-a-public-deploy-key)对每个项目的访问权限。

创建部署密钥后，您无法更改它的范围。

## 权限

部署密钥在创建时被授予权限级别：

- **Read-only：**一个 read-only 部署密钥只能从仓库中读取。
- **Read-write：**一个 read-write 部署密钥可以读取和写入仓库。

您可以在创建部署密钥后更改其权限级别。更改项目部署密钥的权限仅适用于当前项目。

当使用 Read-write 部署密钥推送提交时，系统会检查部署密钥的创建者是否有权访问资源。

例如：

- 当使用部署密钥将提交推送到[受保护的分支](../protected_branches.md)时，部署密钥的创建者必须有权访问该分支。
- 当部署密钥用于推送触发 CI/CD 流水线的提交时，部署密钥的创建者必须有权访问 CI/CD 资源，包括受保护的环境和 secret 变量。

## 查看部署密钥

查看项目可用的部署密钥：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **部署密钥**。

列出了可用的部署密钥：

- **已启用的部署密钥：**有权访问项目的部署密钥。
- **可私有访问的部署密钥：**无权访问项目的项目部署密钥。
- **可公开访问的部署密钥：**无权访问项目的公共部署密钥。

## 创建项目部署密钥

先决条件：

- 您必须至少具有项目的维护者角色。
- [生成 SSH 密钥对](../../ssh.md#generate-an-ssh-key-pair)。将私有 SSH 密钥放在需要访问仓库的主机上。

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **部署密钥**。
1. 填写字段。
1. 可选。要授予 `read-write` 权限，请选中 **授予此密钥写入权限** 复选框。
1. 可选。更新 **过期日期**。

项目部署密钥在创建时启用。您只能修改项目部署密钥的名称和权限。

## 创建公共部署密钥 **(FREE SELF)**

先决条件：

- 您必须具有实例的管理员访问权限。
- [生成 SSH 密钥对](../../ssh.md#generate-an-ssh-key-pair)。将私有 SSH 密钥放在需要访问仓库的主机上。

要创建公共部署密钥：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **部署密钥**。
1. 选择 **新建部署密钥**。
1. 填写字段。
    - **名称**：使用有意义的描述。例如，包括要使用公共部署密钥的外部主机或应用程序的名称。

您只能修改公共部署密钥的名称。

## 授予项目对公共部署密钥的访问权限

先决条件：

- 您必须至少具有项目的维护者角色。

要授予对项目的公共部署密钥访问权限：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **部署密钥**。
1. 选择 **公开访问的部署密钥**。
1. 在密钥行中，选择 **启用**。
1. 授予公共部署密钥读写权限：
    1. 在键的行中，选择 **编辑** (**{pencil}**)。
    1. 选中 **授予此密钥写入权限** 复选框。

## 撤销部署密钥的项目访问权限

要撤消部署密钥对项目的访问权限，您可以禁用它。当密钥被禁用时，任何依赖于部署密钥的服务都会停止工作。

先决条件：

- 您必须至少具有项目的维护者角色。

要禁用部署密钥：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **部署密钥**。
1. 选择 **禁用**（**{cancel}**）。

禁用部署密钥后会发生什么取决于以下情况：

- 如果密钥可公开访问，则会从项目中删除，但仍可在 **公开访问的部署密钥** 选项卡中使用。
- 如果密钥可私有访问且仅由该项目使用，则将其删除。
- 如果密钥可以私有访问并且也被其他项目使用，它会从项目中删除，但仍可在 **私有访问的部署密钥** 选项卡中使用。

## 故障排除

### 部署密钥无法推送到受保护的分支

在某些情况下，部署密钥无法推送到[受保护的分支](../protected_branches.md)。

- 与部署密钥关联的所有者无权访问受保护的分支。
- 与部署密钥关联的所有者对受保护分支的项目没有[成员资格](../members/index.md)。
- 在受保护分支的 [**允许推送和合并** 部分](../protected_branches.md#configure-a-protected-branch)中选择 **禁止**。

所有部署密钥都与一个帐户相关联。由于帐户的权限可能会更改，这可能会导致正在工作的部署密钥突然无法推送到受保护的分支的情况。

对于使用部署密钥的项目，我们建议您创建服务帐户并将部署密钥与服务帐户相关联。

#### 识别与非成员和被阻止用户关联的部署密钥

如果您需要查找属于非成员或被阻止用户的密钥，可以使用 [Rails 控制台](../../../administration/operations/rails_console.md#starting-a-rails-console-session)，使用类似于以下的脚本来识别不可用的部署密钥：

```ruby
ghost_user_id = User.ghost.id

DeployKeysProject.with_write_access.find_each do |deploy_key_mapping|
  project = deploy_key_mapping.project
  deploy_key = deploy_key_mapping.deploy_key
  user = deploy_key.user

  access_checker = Gitlab::DeployKeyAccess.new(deploy_key, container: project)

  # can_push_for_ref? tests if deploy_key can push to default branch, which is likely to be protected
  can_push = access_checker.can_do_action?(:push_code)
  can_push_to_default = access_checker.can_push_for_ref?(project.repository.root_ref)

  next if access_checker.allowed? && can_push && can_push_to_default

  if user.nil? || user.id == ghost_user_id
    username = 'none'
    state = '-'
  else
    username = user.username
    user_state = user.state
  end

  puts "Deploy key: #{deploy_key.id}, Project: #{project.full_path}, Can push?: " + (can_push ? 'YES' : 'NO') +
       ", Can push to default branch #{project.repository.root_ref}?: " + (can_push_to_default ? 'YES' : 'NO') +
       ", User: #{username}, User state: #{user_state}"
end
```
