---
stage: Manage
group: Workspace
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 与群组共享项目 **(FREE)**

当您希望某个群组可以访问您的项目时，您可以邀请[一个群组](../../group/index.md)加入该项目。
该群组的成员可以访问该项目，该项目成为一个*共享项目*。

## 示例

对于由 `Group 1` 创建的项目：

- `Group 1` 的成员可以访问该项目。
- `Group 1` 的所有者可以邀请 `Group 2` 加入项目。

这样，`Group 1` 和 `Group 2` 的成员都可以访问共享项目。

## 先决条件

要邀请群组加入项目，您必须至少是以下人员之一：

- 明确定义为项目的[成员](index.md)。
- 明确定义为有权访问项目的群组或子组的成员。
- 管理员。

此外：

- 您邀请的群组必须比项目具有更严格的[可见性级别](../../public_access.md#project-and-group-visibility)。例如，您可以邀请：

  - 私有群组到公开项目。
  - 内部群组到公开项目。
  - 私有群组到内部项目。

- 如果项目的根群组[不允许项目在层次结构外共享](../../group/access_and_permissions.md#prevent-group-sharing-outside-the-group-hierarchy)，则被邀请的群组或子组必须在项目的[命名空间](../../namespace/index.md)中。例如，命名空间 `group/subgroup01/project` 中的一个项目：

  - 可以与 `group/subgroup02` 或 `group/subgroup01/subgroup03` 共享。
  - 无法与 `group` 共享。

<a id="share-a-project-with-a-group"></a>

## 与群组共享项目

> - 由表单变更为模态窗口于 13.11 版本，带有功能标志，默认禁用。
> - 模态窗口于 14.8 版本可用于 SaaS 版和私有化部署版。
> - 一般可用于 14.9 版本，功能标志 `invite_members_group_modal` 移除。

您可以通过邀请群组加入项目，来与该群组共享项目。

邀请群组加入项目：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **项目信息 > 成员**。
1. 选择 **邀请群组**。
1. 选择要添加到项目中的群组。
1. 选择要分配给群组的角色。
1. 可选。选择一个 **访问到期日期**。
1. 选择 **邀请**。

所有群组成员、子组成员以及该组有权访问的其他项目的成员，都被授予对该项目的访问权限。此外：

- 在群组页面上，项目列在 **共享项目** 选项卡上。
- 在项目的 **成员** 页面上，该组列在 **群组** 选项卡上。
- 每个用户都分配有最大角色。

## 最大角色

当多个群组包含相同的成员，并且这些群组可以访问同一个项目时，群组成员将被赋予该项目的最具限制性的角色。

这种限制性最强的角色称为 **最大角色**。

成员的 **最大角色** 限制性更强的是：

- 为群组分配给用户的角色。
- 您邀请群组加入项目时选择的角色。

### 查看成员的最大角色

查看分配给成员的最大角色：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **项目信息 > 成员**。
1. 在 **最大角色** 列中，查看用户的最大分配角色。

## 查看群组的共享项目

在群组中，共享项目是群组成员通过[**邀请群组**](#share-a-project-with-a-group)操作，获得访问权限的项目。

查看群组的共享项目：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在组页面上，选择 **共享项目**选项卡。

显示共享项目列表。

## 相关主题

- [防止项目与群组共享](../../group/access_and_permissions.md#prevent-a-project-from-being-shared-with-groups)。
