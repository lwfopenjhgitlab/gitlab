---
stage: Create
group: Editor
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, how-to
---

# Wiki **(FREE)**

如果您不想将文档保存在仓库中，但希望将其与代码保存在同一个项目中，您可以使用极狐GitLab 在每个极狐GitLab 项目中提供的 wiki。每个 wiki 都是一个单独的 Git 仓库，因此您可以在 Web 界面中创建 wiki 页面，或者[本地使用 Git](#在本地创建或编辑-wiki-页面)。

GitLab wiki 支持 Markdown、RDoc、AsciiDoc 和 Org 的内容。用 Markdown 编写的 Wiki 页面支持所有 Markdown 功能<!--[Markdown 功能](../../markdown.md)-->，还提供一些对于链接的 wiki 特定支持<!--[wiki 特定支持](../../markdown.md#wiki-specific-markdown)-->。

在 13.5 及更高版本中，wiki 页面显示侧边栏，您[可以自定义](#自定义侧边栏)。此侧边栏包含 wiki 中的部分页面列表，显示为嵌套树，同级页面按字母顺序列出。要查看所有页面的列表，请选择侧边栏中的 **查看所有页面**：

![Wiki sidebar](img/wiki_sidebar_v13_5.png)

## 查看项目 wiki

要访问项目 wiki：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 要显示 wiki，请执行以下任一操作：
    - 在左侧边栏上，选择 **Wiki**。
    - 在项目的任何页面上，使用 <kbd>g</kbd> + <kbd>w</kbd> [wiki 键盘快捷键](../../shortcuts.md)。

如果 **Wiki** 未列在您项目的左侧边栏中，则项目管理员已[禁用它](#启用或禁用项目-wiki)。

## 为您的 wiki 配置默认分支

> 引入于 14.1 版本

wiki 仓库的默认分支取决于您的 GitLab 版本：

- *14.1 及更高版本：* Wiki 继承您的实例或群组配置的[默认分支名称](../repository/branches/default.md)。 如果没有配置自定义值，使用 `main`。
- *14.0 及更早版本：* 使用 `master`。

对于任何版本，您可以为以前创建的 wiki [重命名此默认分支](../repository/branches/default.md#更新仓库中的默认分支名称)。

## 创建 wiki 主页

创建 wiki 时是空的。在您第一次访问时，您可以创建用户在查看 wiki 时看到的主页。此页面需要一个特定的标题才能用作您的 wiki 主页。要创建它：

1. 在顶部栏上，选择**主菜单**。
    - 对于项目 wiki，选择 **项目** 并找到您的项目。
    - 对于群组 wiki，选择 **群组** 并找到您的群组。
1. 在左侧边栏上，选择 **Wiki**。
1. 选择 **创建您的第一个页面**。
1. 系统要求第一页的标题为 `home`。具有此标题的页面用作您的 wiki 的首页。
1. 选择 **格式** 来设置文本样式。
1. 在 **内容** 部分为您的主页添加欢迎信息。您以后可以随时对其进行编辑。
1. 添加 **提交消息**。Git 需要提交消息，因此如果您不自己输入，系统会创建一个。
1. 选择 **创建页面**。

<a id="create-a-new-wiki-page"></a>

## 创建新的 wiki 页面

具有开发者角色的用户可以创建新的 wiki 页面：

1. 在顶部栏上，选择 **主菜单**。
   - 对于项目 wiki，选择 **项目** 并找到您的项目。
   - 对于群组 wiki，选择 **群组** 并找到您的群组。
1. 在左侧边栏上，选择 **Wiki**。
1. 在此页面或任何其他维基页面上选择 **新建页面**。
1. 选择内容格式。
1. 为新页面添加标题。页面标题使用[特殊字符](#页面标题中的特殊字符)作为子目录和格式，并有[长度限制](#文件名和目录名的长度限制)。
1. 向您的 wiki 页面添加内容。
1. （可选）附加一个文件，系统将其存储在 wiki 的 Git 仓库中。
1. 添加 **提交消息**。Git 需要提交消息，因此如果您不自己输入，极狐GitLab 会创建一个。
1. 选择 **创建页面**。

### 在本地创建或编辑 wiki 页面

Wiki 是基于 Git 仓库的，因此您可以在本地克隆它们并像处理其他所有 Git 仓库一样编辑它们。要在本地克隆 wiki 仓库，请从任何 wiki 页面的右侧边栏中选择 **克隆仓库**，然后按照屏幕上的说明进行操作。

您在本地添加到 wiki 的文件必须使用以下受支持的扩展名之一，具体取决于您希望使用的标记语言。
推送到极狐GitLab 时不显示扩展名不受支持的文件：

- Markdown 扩展名：`.mdown`、`.mkd`、`.mkdn`、`.md`、`.markdown`。
- AsciiDoc 扩展名：`.adoc`、`.ad`、`.asciidoc`。
- 其他标记扩展名：`.textile`、`.rdoc`、`.org`、`.creole`、`.wiki`、`.mediawiki`、`.rst`。

### 页面标题中的特殊字符

Wiki 页面作为文件存储在 Git 仓库中，因此某些字符具有特殊含义：

- 存储页面时，空格会转换为连字符。
- 显示页面时，连字符 (`-`) 被转换回空格。
- 斜线 (`/`) 用作路径分隔符，不能显示在标题中。如果您创建包含 `/` 字符的标题，系统会创建构建该路径所需的所有子目录。例如，标题为 `docs/my-page` 会创建一个路径为 `/wikis/docs/my-page` 的 wiki 页面。

### 文件名和目录名的长度限制

> 引入于 12.8 版本

许多常见的文件系统对文件名和目录名有 255 字节的限制。Git 和极狐GitLab 都支持超过这些限制的路径。但是，如果您的文件系统强制执行这些限制，则您无法检出包含超过此限制的文件名的 wiki 本地副本。为防止出现此问题，Web 界面和 API 强制执行以下限制：

- 245 字节用于页面标题（保留 10 字节用于文件扩展名）。
- 255 字节的目录名称。

非 ASCII 字符占用超过 1 个字节。

虽然您仍然可以在本地创建超过这些限制的文件，但您的团队成员之后可能无法在本地查看 wiki。

## 编辑 wiki 页面

您至少需要开发者角色来编辑 wiki 页面：

1. 在顶部栏上，选择 **主菜单**。
    - 对于项目 wiki，选择 **项目** 并找到您的项目。
    - 对于群组 wiki，选择 **群组** 并找到您的群组。
1. 在左侧边栏上，选择 **Wiki**。
1. 转到您要编辑的页面，然后：
    - 使用 <kbd>e</kbd> wiki [键盘快捷键](../../shortcuts.md#wiki-页面)。
    - 选择编辑图标 (**{pencil}**)。
1. 编辑内容。
1. 选择 **保存更改**。

对 wiki 页面的未保存更改会保存在本地浏览器存储中，以防止意外数据丢失。

### 创建目录

要从 wiki 页面的副标题生成目录，请使用 `[[_TOC_]]` 标签。
<!--例如，阅读[目录](../../markdown.md#table-of-contents)。-->

## 删除 wiki 页面

您至少需要开发者角色才能删除 wiki 页面：

1. 在顶部栏上，选择 **主菜单**。
    - 对于项目 wiki，选择 **项目** 并找到您的项目。
    - 对于群组 wiki，选择 **群组** 并找到您的群组。
1. 在左侧边栏上，选择 **Wiki**。
1. 前往您要删除的页面。
1. 选择编辑图标 (**{pencil}**)。
1. 选择 **删除页面**。
1. 确认删除。

## 移动 wiki 页面

您至少需要开发者角色才能移动 wiki 页面：

1. 在顶部栏上，选择 **主菜单**。
    - 对于项目 wiki，选择 **项目** 并找到您的项目。
    - 对于群组 wiki，选择 **群组** 并找到您的群组。
1. 在左侧边栏上，选择 **Wiki**。
1. 转到您要移动的页面。
1. 选择编辑图标 (**{pencil}**)。
1. 将新路径添加到 **标题** 字段。例如，如果您在 `company` 下有一个名为 `about` 的 wiki 页面，并且您想将其移动到 wiki 的根目录，请将 **标题** 从 `about` 更改为 `/about`。
1. 选择 **保存更改**。

## 查看 wiki 页面历史

wiki 页面随时间的变化记录在 wiki 的 Git 仓库中。
历史页面显示：

![Wiki page history](img/wiki_page_history.png)

- 页面的修订（Git commit SHA）。
- 页面作者。
- 提交消息。
- 最后一次更新。
- 以前的修订，通过在**页面版本** 列中选择修订号。

要查看 Wiki 页面的更改：

1. 在顶部栏上，选择 **主菜单**。
    - 对于项目 wiki，选择 **项目** 并找到您的项目。
    - 对于群组 wiki，选择 **群组** 并找到您的群组。
1. 在左侧边栏上，选择 **Wiki**。
1. 转到您要查看其历史记录的页面。
1. 选择 **页面历史**。

### 查看页面版本之间的更改

> 引入于 13.2 版本

您可以查看某个 wiki 页面版本中所做的更改，类似于版本化差异文件视图：

1. 在顶部栏上，选择 **主菜单**。
    - 对于项目 wiki，选择 **项目** 并找到您的项目。
    - 对于群组 wiki，选择 **群组** 并找到您的群组。
1. 在左侧边栏上，选择 **Wiki**。
1. 前往您感兴趣的 wiki 页面。
1. 选择 **页面历史** 以查看所有页面版本。
1. 在 **变更** 列中选择您感兴趣的版本的提交消息。

   ![Wiki page changes](img/wiki_page_diffs_v13_2.png)

## 跟踪 wiki 事件

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/14902) in **GitLab 12.10.**
> - Git events were [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/216014) in **GitLab 13.0.**
> - [Feature flag for Git events was removed](https://gitlab.com/gitlab-org/gitlab/-/issues/258665) in **GitLab 13.5**
-->

极狐GitLab 跟踪 wiki 创建、删除和更新事件。这些事件显示在这些页面上：

- [用户个人资料](../../profile/index.md#访问您的用户资料)。
- 动态页面，取决于 wiki 的类型：
   - [群组动态](../../group/manage.md#查看群组动态)。
   - [项目动态](../working_with_projects.md#项目动态)。

对 wiki 的提交不计入存储库分析<!--[存储库分析](../../analytics/repository_analytics.md)-->。

## 自定义侧边栏

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/23109) in GitLab 13.8, the sidebar can be customized by selecting the **Edit sidebar** button.
-->

您至少需要开发者角色来自定义 wiki 导航侧边栏。这个过程会创建一个名为 `_sidebar` 的 wiki 页面，它完全取代了默认的侧边栏导航：

1. 在顶部栏上，选择 **主菜单**。
    - 对于项目 wiki，选择 **项目** 并找到您的项目。
    - 对于群组 wiki，选择 **群组** 并找到您的群组。
1. 在左侧边栏上，选择 **Wiki**。
1. 在页面右上角，选择 **编辑侧边栏**。
1. 完成后，选择 **保存更改**。

一个 `_sidebar` 示例，使用 Markdown 格式：

```markdown
### [Home](home)

- [Hello World](hello)
- [Foo](foo)
- [Bar](bar)

---

- [Sidebar](_sidebar)
```

正在考虑支持显示带有自定义侧边导航的生成目录。

## 启用或禁用项目 wiki

极狐GitLab 中默认启用 Wiki。项目管理员可以按照[共享和权限](../settings/index.md#configure-project-visibility-features-and-permissions)中的说明，启用或禁用项目 wiki。

自助管理安装实例的管理员可以配置其它 wiki 设置<!--[配置其他 wiki 设置](../../../administration/wikis/index.md)-->。

您可以从[群组设置](group.md#configure-group-wiki-visibility)禁用群组 wiki。

<a id="link-an-external-wiki"></a>

## 链接外部 wiki

要从项目的左侧边栏中添加到外部 wiki 的链接：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **外部 wiki**。
1. 将 URL 添加到您的外部 wiki。
1. （可选）选择 **测试设置** 以验证连接。
1. 选择 **保存更改**。

您现在可以从项目的左侧边栏中看到 **外部 wiki** 选项。

当您启用此集成时，指向外部 wiki 的链接不会替换指向内部 wiki 的链接。
要从侧边栏中隐藏内部 wiki，请[禁用项目的 wiki](#禁用项目-wiki)。

要隐藏指向外部 wiki 的链接：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **外部 wiki**。
1. 在 **启用集成** 部分，清除 **启用** 复选框。
1. 选择 **保存更改**。

## 禁用项目 wiki

要禁用项目的内部 wiki：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 转到您的项目并选择 **设置 > 通用**。
1. 展开 **可见性、项目功能、权限**。
1. 向下滚动以找到 **Wiki** 并将其关闭（灰色）。
1. 选择 **保存更改**。

内部 wiki 现已禁用，用户和项目成员：

- 无法从项目的侧边栏中找到 wiki 的链接。
- 无法添加、删除或编辑 wiki 页面。
- 无法查看任何 wiki 页面。

以前添加的 wiki 页面会被保留，以防您想重新启用 wiki。要重新启用它，请重复该过程以禁用 wiki 但将其打开（蓝色）。

## 内容编辑器

> - 引入于 14.0 版本
> - 在编辑体验之间切换功能引入于 14.7 版本，功能标志名为 `wiki_switch_between_content_editor_raw_markdown`。默认启用。
> - 在编辑体验之间切换功能一般可用于 14.10 版本。功能标志 `wiki_switch_between_content_editor_raw_markdown` 移除。

极狐GitLab 为 wiki 中的极狐GitLab Flavored Markdown 提供了所见即所得的编辑体验。

支持包括：

- 文本格式选项，包括粗体、斜体、块引号、标题和内联代码。
- 无序、有序和清单的列表格式。
- 创建和编辑表格的结构。
- 使用语法高亮插入和格式化代码块。
- Mermaid、PlantUML 和 Kroki 图的实时预览（引入于 15.2 版本）。

### 使用内容编辑器

1. [创建](#创建新的-wiki-页面)一个新的 wiki 页面，或[编辑](#编辑-wiki-页面)一个现有的页面。
1. 选择 **Markdown** 作为您的格式。
1. 在 **内容** 上方，选择 **编辑富文本**。
1. 使用内容编辑器中提供的各种格式选项自定义页面内容。
1. 为新页面选择 **创建页面**，或为现有页面选择 **保存更改**：

富文本编辑模式保持默认，直到您切换回[编辑原始源](#switch-back-to-the-old-editor)。

<a id="switch-back-to-the-old-editor"></a>

### 切换回旧版编辑器

1. *如果您在内容编辑器中编辑页面，*滚动到 **内容**。
1. 选择 **编辑源**。

<!--
### GitLab Markdown 支持

Supporting all GitLab Flavored Markdown content types in the Content Editor is a work in progress.
For the status of the ongoing development for CommonMark and GitLab Flavored Markdown support, read:

- [Basic Markdown formatting extensions](https://gitlab.com/groups/gitlab-org/-/epics/5404) epic.
- [GitLab Flavored Markdown extensions](https://gitlab.com/groups/gitlab-org/-/epics/5438) epic.
-->

<!--
## Related topics

- [Wiki settings for administrators](../../../administration/wikis/index.md)
- [Project wikis API](../../../api/wikis.md)
- [Group repository storage moves API](../../../api/group_repository_storage_moves.md)
- [Group wikis API](../../../api/group_wikis.md)
- [Wiki keyboard shortcuts](../../shortcuts.md#wiki-pages)
-->

## 故障排除

### 使用 Apache 反向代理的页面 slug 渲染

在 14.9 及更高版本中，页面 slug 现在使用 [`ERB::Util.url_encode`](https://www.rubydoc.info/stdlib/erb/ERB%2FUtil.url_encode) 方法进行编码。
如果您使用 Apache 反向代理，您可以在 Apache 配置的 `ProxyPass` 行添加一个 `nocanon` 参数，以确保您的页面 slug 正确呈现。

### 使用 Rails 控制台重新创建项目 wiki **(FREE SELF)**

WARNING:
此操作会删除 wiki 中的所有数据。

WARNING:
如果运行不正确或在正确的条件下，任何直接更改数据的命令都可能造成破坏。我们强烈建议在测试环境中运行它们，并准备好恢复实例的备份，以防万一。

要清除项目 wiki 中的所有数据并在空白状态下重新创建它：

1. [启动 Rails 控制台会话](../../../administration/operations/rails_console.md#starting-a-rails-console-session)。
1. 运行这些命令：

   ```ruby
   # Enter your project's path
   p = Project.find_by_full_path('<username-or-group>/<project-name>')

   # This command deletes the wiki project from the filesystem.
   GitlabShellWorker.perform_in(0, :remove_repository, p.repository_storage, p.wiki.disk_path)

   # Refresh the wiki repository state.
   p.wiki.repository.expire_exists_cache
   ```

Wiki 中的所有数据都已清除，可以使用了。
