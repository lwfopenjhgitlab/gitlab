---
stage: Create
group: Editor
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, how-to
---

# 群组 wiki **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/13195) in [GitLab Premium](https://about.gitlab.com/pricing/) 13.5.
-->

如果您使用极狐GitLab 群组来管理多个项目，您的某些文档可能会跨越多个群组。您可以创建群组 wiki，而不是[项目 wiki](index.md)，以确保所有群组成员都具有正确的访问权限来贡献。群组 wiki 类似于[项目 wiki](index.md)，但有一些限制：

- <!--[Git LFS](../../../topics/git/lfs/index.md)-->Git LFS 不受支持。
- <!--[全局搜索](../../search/advanced_search.md)-->全局搜索中不包含群组 wiki。
- 对群组 wiki 的更改不会显示在群组动态 feed <!--[群组动态源](../../group/index.md#group-activity-analytics)-->中。
- 默认情况下为专业版和更高级别启用群组 wiki。您不能从用户界面关闭它们。

<!--
For updates, follow [the epic that tracks feature parity with project wikis](https://gitlab.com/groups/gitlab-org/-/epics/2782).
-->

与项目 wiki 类似，具有开发者角色及更高级别的组成员可以编辑群组 wiki。<!--可以使用 [Group repository storage move API](../../../api/group_repository_storage_moves.md) 移动组 wiki 存储库。-->

## 查看群组 wiki

要访问群组 wiki：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 要显示 wiki，请执行以下任一操作：
    - 在左侧边栏上，选择 **Wiki**。
    - 在项目的任何页面上，使用 <kbd>g</kbd> + <kbd>w</kbd> [wiki 键盘快捷键](../../shortcuts.md)。

## 导出群组 wiki

> 引入于 13.9 版本。

群组中具有所有者角色的用户在导入或导出群组时可以[导入和导出群组 wiki](../../group/settings/import_export.md)。

当帐户降级或试用结束时，不会删除在群组 wiki 中创建的内容。

<a id="configure-group-wiki-visibility"></a>

## 配置群组 wiki 可见性

> 引入于 15.0 版本。

极狐GitLab 中默认启用 Wiki。群组管理员可以通过群组设置启用或禁用群组 wiki。

打开群组设置：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **权限和群组功能**。
1. 滚动到 **Wiki** 并选择以下选项之一：
    - **启用**：可以访问群组的每个人都可以访问 wiki。
    - **私有**：只有群组成员才能访问 wiki。
    - **已禁用**：wiki 无法访问，也无法下载。
1. 选择 **保存修改**。

<!--
## Related topics

- [Wiki settings for administrators](../../../administration/wikis/index.md)
- [Project wikis API](../../../api/wikis.md)
- [Group repository storage moves API](../../../api/group_repository_storage_moves.md)
- [Group wikis API](../../../api/group_wikis.md)
- [Wiki keyboard shortcuts](../../shortcuts.md#wiki-pages)
- [Epic: Feature parity with project wikis](https://gitlab.com/groups/gitlab-org/-/epics/2782)
-->
