---
stage: Manage
group: Workspace
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
---

# 管理项目 **(FREE)**

极狐GitLab 中的大部分工作都是在[项目](../../user/project/index.md)中完成的。文件和代码保存在项目中，大部分功能都在项目范围内。

## 查看项目

要查看项目，请在顶部栏中选择 **主菜单 > 项目 > 查看所有项目**。

NOTE:
未经身份验证的用户可以看到 **浏览项目** 选项卡，除非限制了**公开**可见性级别<!--[**公开**可见性级别](../admin_area/settings/visibility_and_access_controls.md#restrict-visibility-levels)-->，然后该选项卡仅对登录用户可见。

### 谁可以查看项目页面

当您选择一个项目时，项目登录页面会显示项目内容。

对于公开项目，以及有权查看项目代码的内部和私有项目成员，项目登陆页面显示：

- [`README` 或索引文件](repository/index.md#readme-和-index-文件)。
- 项目仓库中的目录列表。

对于没有权限查看项目代码的用户，登陆页面显示：

- wiki 主页。
- 项目中的议题列表。

### 使用项目 ID 访问项目页面

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/53671) in GitLab 11.8.
-->

要使用项目 ID 从 GitLab UI 访问项目，请在浏览器或其他访问项目的工具中访问 `/projects/:id` URL。

## 浏览主题

要浏览项目主题：

1. 在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目**。
1. 选择 **浏览主题** 选项卡。
1. 要查看与某个主题相关的项目，请选择一个主题。

**浏览主题**选项卡显示按相关项目数量排序的主题列表。
要查看与主题关联的项目，请从列表中选择一个主题。

您可以在项目设置页面<!--[项目设置页面](settings/index.md#topics)-->上为项目分配主题。

如果您是实例管理员，则可以从管理中心的主题页面<!--[管理中心的主题页面](../admin_area/index.md#administering-topics)-->，管理所有项目主题。

<a id="star-a-project"></a>

## 星标一个项目

您可以为经常使用的项目添加星号，以便更容易找到它们。

给项目加星：

1. 在顶部栏上，选择 **主菜单 > 项目**，并找到您的项目。
1. 在页面右上角，选择 **星标**。

## 查看星标项目

1. 在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目**。
1. 选择 **星标项目** 选项卡。
1. 系统显示有关您已加星标的项目的信息，包括：

    - 项目描述，包括名称、描述和图标。
    - 该项目被星标的次数。
    - 该项目被派生的次数。
    - 打开合并请求的数量。
    - 开放议题的数量。

<a id="view-personal-projects"></a>

## 查看个人项目

个人项目是在您的个人命名空间下创建的项目。

例如，如果您使用用户名 `alex` 创建一个帐户，并在您的用户名下创建一个名为 `my-project` 的项目，则该项目将创建在 `https://gitlab.example.com/alex/my-project`。

查看您的个人项目：

1. 在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目**。
1. 在 **您的项目** 选项卡中，选择 **个人**。

## 删除项目

删除项目后，个人命名空间中的项目会立即删除。要延迟删除群组中的项目，您可以[启用延迟项目删除](../group/index.md#启用延迟项目移除)。

要删除项目：

1. 在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目**，并找到您的项目。
1. 选择 **设置 > 通用**。
1. 展开 **高级** 部分。
1. 向下滚动到 **删除项目** 部分。
1. 选择 **删除项目**。
1. 通过填写字段确认此操作。

## 查看待删除的项目 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/37014) in GitLab 13.3 for Administrators.
> - [Tab renamed](https://gitlab.com/gitlab-org/gitlab/-/issues/347468) from **Deleted projects** in GitLab 14.6.
-->

> - 于 14.8 版本中适用于所有用户，功能标志名为 `project_owners_list_project_pending_deletion`。默认启用。

<!--
FLAG:
On self-managed GitLab, by default this feature is available to all users. To make it available for administrators only,
ask an administrator to [disable the feature flag](../../administration/feature_flags.md) named `project_owners_list_project_pending_deletion`.
On GitLab.com, this feature is available to all users.
-->

当为群组启用延迟项目删除时，该群组中的项目不会立即删除，而只会在延迟后删除。要访问所有待删除项目的列表：

1. 在顶部栏上，选择 **主菜单 > 项目 > 查看所有项目**。
1. 选择 **待删除** 选项卡（在 14.6 及更高版本中）或 **已删除项目** 选项卡（GitLab 14.5 及更早版本）。

为每个项目列出：

- 项目被标记为删除的时间。
- 计划最终删除项目的时间。
- 一个 **还原** 链接，可以阻止项目最终被删除。

<a id="view-project-activity"></a>

## 查看项目动态

要查看项目的动态：

1. 在顶部栏上，选择 **主菜单 > 项目**，并找到您的项目。
1. 在左侧边栏上，选择 **项目信息 > 动态**。
1. 选择一个选项卡以查看项目动态的类型。

## 在项目中搜索

您可以搜索您的项目。

1. 在顶部栏上，选择 **主菜单**。
1. 在 **搜索您的项目** 中，输入项目名称。

系统在您键入时进行过滤。

您还可以查找您[已加星标](#star-a-project)的项目。

<!--
You can **Explore** all public and internal projects available in GitLab.com, from which you can filter by visibility, through **Trending**, best rated with **Most stars**, or **All** of them.
-->

您可以按以下方式对项目进行排序：

- 姓名
- 创建日期
- 更新日期
- 所有者

您还可以选择隐藏或显示归档项目。

<a id="filter-projects-by-language"></a>

### 按语言过滤项目

> - 引入于 15.9 版本，[功能标志](../../administration/feature_flags.md)为 `project_language_search`。默认启用。
> - [一般可用于](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/110956) 15.9 版本。功能标志 `project_language_search` 已删除。

您可以按项目使用的编程语言过滤项目：

1. 在顶部栏中，选择 **主菜单 > 项目 > 查看所有项目**。
1. 从 **语言** 下拉列表中，选择您要作为筛选项目依据的语言。

显示使用所选语言的项目列表。

## 更改项目中各个功能的可见性

您可以更改项目中各个功能的可见性。

先决条件：

- 您必须具有项目的所有者角色。

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **可见性、项目功能、权限**。
1. 使用您要打开或关闭的每个功能旁边的开关，或更改访问权限。
1. 选择 **保存更改**。

<a id="leave-a-project"></a>

## 离开项目

当您离开一个项目时：

- 您不再是项目成员，不能贡献代码。
- 指派给您的所有议题和合并请求均变为无指派人。

离开项目：

1. 在顶部栏上，选择 **主菜单 > 项目**，并找到您的项目。
1. 选择 **离开项目**。**离开项目** 选项仅在项目属于[群组命名空间](../namespace/index.md)下的群组时，才会显示在项目仪表板上。

## 使用项目作为 Go 包

先决条件：

- 联系您的管理员以启用 GitLab Go Proxy<!--[GitLab Go Proxy](../packages/go_proxy/index.md)-->。
- 要将子组中的私有项目用作 Go 包，您必须[验证 Go 请求](#验证对私有项目的-go-请求)。未经身份验证的 Go 请求会导致 `go get` 失败。 对于不在子组中的项目，您不需要验证 Go 请求。

要将项目用作 Go 包，请使用 `go get` 和 `godoc.org` 发现请求。您可以使用元标签：

- [`go-import`](https://pkg.go.dev/cmd/go#hdr-Remote_import_paths)
- [`go-source`](https://github.com/golang/gddo/wiki/Source-Code-Links)

### 验证对私有项目的 Go 请求

先决条件：

- 您的极狐GitLab 实例必须可通过 HTTPS 访问。
- 您必须拥有[个人访问令牌](../profile/personal_access_tokens.md)，包含 `read_api` 范围。

要验证 Go 请求，请创建一个 [`.netrc`](https://everything.curl.dev/usingcurl/netrc) 文件，其中包含以下信息：

```plaintext
machine gitlab.example.com
login <gitlab_user_name>
password <personal_access_token>
```

在 Windows 上，Go 读取 `~/_netrc` 而不是 `~/.netrc`。

`go` 命令不会通过不安全的连接传输凭据。它验证 Go 发出的 HTTPS 请求，但不验证通过 Git 发出的请求。

### 验证 Git 请求

如果 Go 无法从代理获取模块，它会使用 Git。Git 使用 `.netrc` 文件对请求进行身份验证，但您可以配置其他身份验证方法。

将 Git 配置为：

- 在请求 URL 中嵌入凭据：

    ```shell
    git config --global url."https://${user}:${personal_access_token}@gitlab.example.com".insteadOf "https://gitlab.example.com"
    ```

- 使用 SSH 而不是 HTTPS：

    ```shell
    git config --global url."git@gitlab.example.com".insteadOf "https://gitlab.example.com/"
    ```

### 禁用私有项目的 Go 模块获取

为了获取模块或包，Go 使用环境变量：

- `GOPRIVATE`
- `GONOPROXY`
- `GONOSUMDB`

要禁用获取：

1. 禁用 `GOPRIVATE`：
     - 要禁用对一个项目的查询，请禁用 `GOPRIVATE=gitlab.example.com/my/private/project`。
     - 要禁用 SaaS 上所有项目的查询，请禁用 `GOPRIVATE=gitlab.example.com`。
1. 在 `GONOPROXY` 中禁用代理查询。
1. 禁用 `GONOSUMDB` 中的校验和查询。
- 如果模块名称或其前缀在 `GOPRIVATE` 或 `GONOPROXY` 中，Go 不会查询模块代理。
- 如果模块名称或其前缀在 `GONOPRIVATE` 或 `GONOSUMDB` 中，Go 不会查询校验和数据库。

### 从 Geo 次要站点获取 Go 模块

使用 Geo<!--[Geo](../../administration/geo/index.md)--> 访问包含次要 Geo 服务器上的 Go 模块的 Git 存储库。

您可以使用 SSH 或 HTTP 访问 Geo 次要服务器。

#### 使用 SSH 访问 Geo 次要服务器

要使用 SSH 访问 Geo 次要服务器：

1. 在客户端重新配置 Git 以将主服务器的流量发送到次要服务器：

   ```shell
   git config --global url."git@gitlab-secondary.example.com".insteadOf "https://gitlab.example.com"
   git config --global url."git@gitlab-secondary.example.com".insteadOf "http://gitlab.example.com"
   ```

    - For `gitlab.example.com`, use the primary site domain name.
    - For `gitlab-secondary.example.com`, use the secondary site domain name.

1. 确保客户端设置为通过 SSH 访问 GitLab 仓库。您可以在主服务器上进行测试，系统会将公钥复制到次要服务器。

`go get` 请求生成到主 Geo 服务器的 HTTP 流量。当模块下载开始时，`insteadOf` 配置将流量发送到次要 Geo 服务器。

#### 使用 HTTP 访问 Geo 次要节点

您必须使用复制到次要服务器的持久访问令牌。您不能使用 CI/CD 作业令牌通过 HTTP 获取 Go 模块。

使用 HTTP 访问 Geo 次要服务器：

1. 在客户端添加一个 Git `insteadOf` 重定向：

   ```shell
   git config --global url."https://gitlab-secondary.example.com".insteadOf "https://gitlab.example.com"
   ```

   - 对于 `gitlab.example.com`，使用主站点域名。
   - 对于 `gitlab-secondary.example.com`，使用次要站点域名。

1. 生成[个人访问令牌](../profile/personal_access_tokens.md)并在客户端的`~/.netrc` 文件中添加凭据：

   ```shell
   machine gitlab.example.com login USERNAME password TOKEN
   machine gitlab-secondary.example.com login USERNAME password TOKEN
   ```

`go get` 请求生成到主 Geo 服务器的 HTTP 流量。当模块下载开始时，`insteadOf` 配置将流量发送到次要 Geo 服务器。

<!--
## Related topics

- [Import a project](../../user/project/import/index.md).
- [Connect an external repository to GitLab CI/CD](../../ci/ci_cd_for_external_repos/index.md).
- [Fork a project](repository/forking_workflow.md#creating-a-fork).
- [Adjust project visibility and access levels](settings/index.md#sharing-and-permissions).
-->
