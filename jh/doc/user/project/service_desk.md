---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.cn/handbook/engineering/ux/technical-writing/#assignments
---

# 服务台 **(FREE)**

> 在 13.2 版本中移至标准版

服务台是一个服务模块，它可以在不借助任何外部工具的情况下，使您的团队通过电子邮件与任何外部方联系。

借助服务台，您可以为客户提供高效的电子邮件支持。 他们可以通过电子邮件向您发送错误报告、功能请求或一般反馈。它们最终都会作为新的议题出现在您的项目中。反过来，您的团队可以直接从项目中做出响应。

由于服务台内置于极狐GitLab 中，因此消除了多种工具和外部集成的复杂性和低效性。这大大缩短了从反馈到软件更新的周期时间。

<!--
点击[GitLab 服务台](https://about.gitlab.com/blog/2017/05/09/demo-service-desk/)查看功能视频演示。
-->

## 如何工作


服务台使人们可以在您的极狐GitLab 实例中创建议题，而无需提供他们自己的账号信息。

它为最终用户在项目中创建问题提供了一个唯一的电子邮件地址。后续内容可以通过极狐GitLab 界面或通过电子邮件发送。最终用户只能通过电子邮件看到。

例如，假设您为 iOS 或 Android 开发游戏。代码库托管在您的极狐GitLab 实例中，构建和部署使用极狐GitLab CI/CD。

服务台是这样工作的：

1. 您为付费客户提供特定项目的电子邮件地址，用户可以直接从应用程序向您发送电子邮件。
1. 他们发送的每封电子邮件都会在相应的项目中产生一个议题。
1. 您的团队成员导航到服务台问题跟踪器，在那里他们可以查看新的技术支持请求并在相关议题中做出响应。
1. 您的团队与客户来回多次沟通以了解请求。
1. 您的团队开始致力于代码编写以解决客户的问题。
1. 当您的团队完成功能实现时，合并请求随即被处理并自动关闭议题。
1. 客户的请求通过电子邮件处理，无需访问您的极狐GitLab 实例。
1. 您的团队不必奔波来回于您的客户，这节省了大量时间。

## 配置服务台

要开始为项目使用服务台，您必须先将其打开。默认情况下，服务台处于关闭状态。

先决条件：

- 您必须至少具有项目的维护者角色。
- 在私有化部署版上，您必须为极狐GitLab 实例[设置传入电子邮件](../../administration/incoming_email.md#set-it-up)。您应该使用[电子邮件子寻址](../../administration/incoming_email.md#email-sub-addressing)，但您也可以使用[万能邮箱](../../administration/incoming_email.md#catch-all-mailbox)。为此，您必须具有管理员访问权限。

在您的项目中启用服务台：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **服务台**。
1. 打开 **启用服务台** 开关。
1. 可选。填写字段。
   - [添加后缀](#configure-a-custom-email-address-suffix)到您的服务台电子邮件地址。
   - 如果 **附加到所有服务台生成议题的模板** 下面的列表是空的，请在您的存储库中创建一个[描述模板](description_templates.md)。
1. 选择 **保存更改**。

现在已为此项目启用服务台。
如果有人向 **用于服务台的电子邮件地址** 下的可用地址发送电子邮件，极狐GitLab 使用电子邮件的内容创建了一个机密议题。

### 提高项目的安全性

要提高服务台项目的安全性，您应该：

- 在您的电子邮件系统为服务台邮件地址配置一个别名，以后方便更改。
- 在您的实例中开启 Akismet<!--[开启 Akismet](../../integration/akismet.md)--> 添加垃圾邮件检查。那些未被阻止的垃圾电子邮件可能会导致创建许多垃圾邮件议题。

### 创建自定义邮件模版

> - 在 12.7 版本的专业版中引入。
> - 在 13.2 版本中该功能转移到标准版。

在以下情况下会向作者发送电子邮件：

- 用户使用服务台提交新议题。
- 在服务台议题上创建了一个新注释。

您可以使用模版来定制化邮件内容。

#### 电子邮件页眉和页脚 **(FREE SELF)**

> 引入于 15.9 版本。

实例管理员可以为极狐GitLab 实例添加一个小的页眉或页脚，并使它们在电子邮件模板中可见。有关详细信息，请参阅[系统页眉和页脚消息](../admin_area/appearance.md#system-header-and-footer-messages)。

#### 感谢电子邮件

> `%{ISSUE_DESCRIPTION}` 引入于 16.0 版本。

当用户通过服务台提交议题时，极狐GitLab 会发送 **感谢电子邮件**。

要创建自定义电子邮件模板，请在仓库的 `.gitlab/service_desk_templates/` 目录中创建一个名为 `thank_you.md` 的文件。

您可以在电子邮件中使用下面这些占位符：

- `%{ISSUE_ID}`：议题 IID
- `%{ISSUE_PATH}`：附加议题 IID 的项目路径
- `%{ISSUE_DESCRIPTION}`：基于原始邮件的议题秒述
- `%{UNSUBSCRIBE_URL}`：退订 URL
- `%{SYSTEM_HEADER}`：[系统页眉消息](../admin_area/appearance.md#system-header-and-footer-messages)
- `%{SYSTEM_FOOTER}`：[系统页脚消息](../admin_area/appearance.md#system-header-and-footer-messages)
- `%{ADDITIONAL_TEXT}`：[自定义附加文本](../admin_area/settings/email.md#custom-additional-text)

因为服务台议题是[机密](issues/confidential_issues.md)的（只有项目成员可以看到它们），回复电子邮件不包含议题链接。

#### 新笔记邮件

> `%{ISSUE_DESCRIPTION}` 引入于 16.0 版本。

当用户提交的议题收到新评论时，极狐GitLab 会发送一封新的注释电子邮件。

要创建自定义电子邮件模板，请在仓库的 `.gitlab/service_desk_templates/` 目录中创建一个名为 `new_note.md` 的文件。

您可以在电子邮件中使用下面这些占位符：

- `%{ISSUE_ID}`：议题 ID
- `%{ISSUE_PATH}`：附加议题 ID 的项目路径
- `%{ISSUE_DESCRIPTION}`：生成电子邮件时的议题描述。如果用户编辑了描述，则可能包含不打算传递给外部参与者的敏感信息。仅当您从未修改描述或您的团队知道模板设计时才使用此占位符。
- `%{NOTE_TEXT}`：笔记内容
- `%{UNSUBSCRIBE_URL}`：退订 URL
- `%{SYSTEM_HEADER}`：[系统页眉消息](../admin_area/appearance.md#system-header-and-footer-messages)
- `%{SYSTEM_FOOTER}`：[系统页脚消息](../admin_area/appearance.md#system-header-and-footer-messages)
- `%{ADDITIONAL_TEXT}`：[自定义附加文本](../admin_area/settings/email.md#custom-additional-text)

<a id="use-a-custom-template-for-service-desk-issues"></a>

#### 为服务台议题使用自定义模板

您可以为**每个项目**选择一个[议题描述模板](description_templates.md#create-an-issue-template)，他们会附加到每个新的服务台议题的描述中。

您可以设置不同级别的描述模板：

- 整个[实例](description_templates.md#set-instance-level-description-templates)。
- 特定的[群组或子组](description_templates.md#set-group-level-description-templates)。
- 特定的[项目](description_templates.md#set-a-default-template-for-merge-requests-and-issues)。

模板是继承的。例如，在项目中，您还可以访问为实例或项目的父组设置的模板。

先决条件：

- 您必须[创建描述模板](description_templates.md#create-an-issue-template)。

要将自定义描述模板与服务台一起使用：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **服务台**。
1. 从 **附加到所有服务台生成议题的模板** 下拉列表中，搜索或选择您的模板。

<a id="support-bot-user"></a>

### 支持机器人用户

在后台，由创建议题的特殊支持机器人用户为服务台工作。
该用户不是[计费用户](../../subscriptions/self_managed/index.md#billable-users)，因此不计入许可证限制计数。

#### 更改支持机器人的显示名称

您可以更改支持机器人用户的显示名称。从服务台发送的电子邮件在 `From` 标头中使用此名称。默认显示名称为 `GitLab Support Bot`。

要编辑自定义电子邮件显示名称：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **服务台**。
1. 在 **电子邮件显示名称** 下方，输入一个新名称。
1. 选择 **保存更改**。

### 使用自定义电子邮件地址 **(FREE SELF)**

您可以将自定义电子邮件地址用于服务台。

为此，您必须配置一个[自定义邮箱](#configure-a-custom-mailbox)。您还可以配置[自定义后缀](#configure-a-custom-email-address-suffix)。

<a id="configure-a-custom-mailbox"></a>

#### 配置自定义邮箱

<!--
NOTE:
在 GitLab.cn 上，自定义邮箱已经配置了 `contact-project+%{key}@incoming.gitlab.cn` 作为电子邮件地址，因此您只需在项目设置中配置[自定义后缀](#配置自定义邮箱后缀)。 
-->

使用 `service_desk_email` 配置，您可以自定义服务台使用的邮箱，系统允许您通过在项目设置中配置[自定义后缀](#configure-a-custom-email-address-suffix)来为服务台提供单独的电子邮件地址。

<!--
The address must include the +%{key} placeholder within the 'user'
portion of the address, before the @. This is used to identify the project
where the issue should be created.
-->

先决条件：

- `address` 必须包含 'user' 中 `+%{key}` 占位符 `@` 之前的地址部分，用于标示应在哪个项目中创建议题。
- `service_desk_email` 和 `incoming_email` 配置必须始终使用单独的邮箱以确保正确处理服务台电子邮件。

要使用 IMAP 为服务台配置自定义邮箱，请将以下片段完整添加到您的配置文件中：

**Omnibus**

```ruby
gitlab_rails['service_desk_email_enabled'] = true
gitlab_rails['service_desk_email_address'] = "project_contact+%{key}@gmail.com"
gitlab_rails['service_desk_email_email'] = "project_contact@gmail.com"
gitlab_rails['service_desk_email_password'] = "[REDACTED]"
gitlab_rails['service_desk_email_mailbox_name'] = "inbox"
gitlab_rails['service_desk_email_idle_timeout'] = 60
gitlab_rails['service_desk_email_log_file'] = "/var/log/gitlab/mailroom/mail_room_json.log"
gitlab_rails['service_desk_email_host'] = "imap.gmail.com"
gitlab_rails['service_desk_email_port'] = 993
gitlab_rails['service_desk_email_ssl'] = true
gitlab_rails['service_desk_email_start_tls'] = false
```

**源安装**

```yaml
service_desk_email:
  enabled: true
  address: "project_contact+%{key}@example.com"
  user: "project_contact@example.com"
  password: "[REDACTED]"
  host: "imap.gmail.com"
  delivery_method: webhook
  secret_file: .gitlab-mailroom-secret
  port: 993
  ssl: true
  start_tls: false
  log_path: "log/mailroom.log"
  mailbox: "inbox"
  idle_timeout: 60
  expunge_deleted: true
```


<!--
这里的配置项目与[incoming email](../../administration/incoming_email.md#set-it-up)的配置是相同的。
-->

##### Microsoft Graph

> 在 13.11 版本中引入。

服务台可以配置为使用 Microsoft Graph API 而不是 IMAP 读取 Microsoft Exchange Online 邮箱。<!--参看[文档](../../administration/incoming_email.md#microsoft-graph)。-->

- 以 Ominbus 方式安装配置示例：

  ```ruby
  gitlab_rails['service_desk_email_enabled'] = true
  gitlab_rails['service_desk_email_address'] = "project_contact+%{key}@example.onmicrosoft.com"
  gitlab_rails['service_desk_email_email'] = "project_contact@example.onmicrosoft.com"
  gitlab_rails['service_desk_email_mailbox_name'] = "inbox"
  gitlab_rails['service_desk_email_log_file'] = "/var/log/gitlab/mailroom/mail_room_json.log"
  gitlab_rails['service_desk_email_inbox_method'] = 'microsoft_graph'
  gitlab_rails['service_desk_email_inbox_options'] = {
   'tenant_id': '<YOUR-TENANT-ID>',
   'client_id': '<YOUR-CLIENT-ID>',
   'client_secret': '<YOUR-CLIENT-SECRET>',
   'poll_interval': 60  # Optional
  }
  ```

以源代码方式安装暂时不支持 Microsoft Graph API 配置。<!--相关细节请参考[议题](https://gitlab.com/gitlab-org/gitlab/-/issues/326169)。-->

<a id="configure-a-custom-email-address-suffix"></a>

#### 配置自定义邮箱后缀

配置[自定义邮箱](#configure-a-custom-mailbox)后，您可以在项目的服务台设置中设置自定义后缀。它只能包含小写字母 (`a-z`)、数字 (`0-9`) 或下划线 (`_`)。

配置后，自定义后缀会创建一个新的服务台电子邮件地址，其中包括 `service_desk_email_address` 设置和格式：`<project_full_path>-<custom_suffix>`

先决条件：

- 您必须配置了一个[自定义邮箱](#configure-a-custom-mailbox)。

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **服务台**。
1. 在 **电子邮件地址后缀** 下方，输入要使用的后缀。
1. 选择 **保存更改**。

比如 `mygroup/myproject` 项目服务台设置的配置如下：

- 电子邮件后缀为 `support`.
- 服务台邮箱地址配置为`contact+%{key}@example.com`.

这个项目的服务台邮箱地址就是：`contact+mygroup-myproject-support@example.com`。同时 incoming email<!--[incoming email](../../administration/incoming_email.md)--> 地址也是可以正常工作的。

如果不配置自定义后缀，将使用默认的项目标识来标识项目。您可以在项目设置中看到该电子邮件地址。

## 使用服务台

您可以使用服务台[创建议题](#as-an-end-user-issue-creator)或[回复一个议题](#as-a-responder-to-the-issue)。

在这些议题中，您还可以看到[支持机器人用户](#support-bot-user)。

### 查看服务台电子邮件地址

要检查您的项目的服务台电子邮件地址：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **议题 > 服务台**。

电子邮件地址位于议题列表的顶部。

<a id="as-an-end-user-issue-creator"></a>

### 作为最终用户（议题创建者）

> 支持额外的 email headers 引入于 14.6 版本。
> 在早期版本中，服务台电子邮件地址必须位于 “To” 字段中。

要创建服务台议题，最终用户不需要了解有关极狐GitLab 实例的任何信息。他们只是向他们提供的地址发送一封电子邮件，然后收到一封确认收到的电子邮件：

![服务台开启](img/service_desk_confirmation_email.png)

这也为最终用户提供了取消订阅的选项。

如果他们不选择退订，则添加到该问题的任何新评论都将作为电子邮件发送：

![服务台回复邮件](img/service_desk_reply.png)

议题中会显示所有通过邮件做出的回复。

<!--
For information about headers used for treating email, see
[the incoming email documentation](../../administration/incoming_email.md#accepted-headers).
-->

<a id="as-a-responder-to-the-issue"></a>

### 作为议题回复者

对于议题的回复者来说，一切都和其它极狐GitLab 议题一样。回复者可以在其中查看通过客户支持请求创建的问题，并过滤它们或与之交互。

![服务台议题追踪器](img/service_desk_issue_tracker.png)

来自最终用户的消息会显示为来自特殊的机器人用户<!--[机器人用户](../../subscriptions/self_managed/index.md#billable-users)-->。您可以像在极狐GitLab 中一样阅读和编写评论：

![服务台议题线程](img/service_desk_thread.png)

NOTE:
- 项目的可见性（私有、内部、公共）不影响服务台。
- 项目路径，包括其组或命名空间，显示在电子邮件中。

#### 查看服务台议题

先决条件：

- 您必须至少具有该项目的报告者角色。

查看服务台问题：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **议题 > 服务台**。

### 电子邮件内容和格式

#### HTML 电子邮件中的特殊 HTML 格式

> - 引入于 15.9 版本，[功能标志](../../administration/feature_flags.md)为 `service_desk_html_to_text_email_handler`。默认禁用。
> - 一般可用于 15.11 版本，删除功能标志 `service_desk_html_to_text_email_handler`。

HTML 电子邮件显示 HTML 格式，例如：

- 表格
- 引用
- 图片
- 可折叠部分

#### 附加到评论的文件

> - 引入于 15.8 版本，[功能标志](../../administration/feature_flags.md)为 `service_desk_new_note_email_native_attachments`，默认禁用。
> - 在 SaaS 和私有化部署版上启用于 15.10 版本。

如果评论包含任何附件并且它们的总大小小于或等于 10 MB，这些附件将作为电子邮件的一部分发送。在其他情况下，电子邮件包含指向附件的链接。

在 15.9 及更早版本中，上传到评论的内容将作为电子邮件中的链接发送。

## 隐私注意事项

服务台议题是私密的，所以只对项目成员可见。但项目所有者可以将议题公开。
当服务台议题公开时，至少具有项目报告者角色的登录用户可以看到议题创建者和参与者的电子邮件地址。

您项目中的任何人都可以使用服务台电子邮件地址在此项目中创建议题，**无论他们在项目中的角色为何**。

唯一的内部电子邮件地址对至少为报告者角色的项目成员可见。
外部用户（议题创建者）无法看到信息说明中显示的内部电子邮件地址。

### 移动服务台议题

> 变更于 15.7 版本：移动服务台议题后，客户会继续收到通知。

您可以像在极狐GitLab 中[移动普通议题](issues/managing_issues.md#move-an-issue)一样移动服务台议题。

如果将服务台议题移至启用了服务台的不同项目，创建该议题的客户将继续收到电子邮件通知。
因为移动的议题会首先被关闭，然后再复制，所以用户会被认为是这两个议题的参与者。他们继续收到旧议题和新议题的任何通知。

## 服务台故障排除

### 发送至服务台的电子邮件不会创建议题

您的电子邮件会被忽略，可能是因为它们包含[极狐GitLab 忽略的电子邮件 headers](../../administration/incoming_email.md#rejected-headers) 之一。
