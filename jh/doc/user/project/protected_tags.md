---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, howto
---

# 受保护的标签 **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/10356) in GitLab 9.1.
-->

受保护的标签：

- 允许控制谁有权创建标签。
- 一旦创建，防止意外更新或删除。

每个规则都允许您匹配：

- 一个单独的标签名称。
- 通配符一次控制多个标签。

此功能由[受保护的分支](protected_branches.md)演变而来。

## 谁可以修改受保护的标签

默认情况下：

- 要创建标签，您必须具有维护者角色。
- 没有人可以更新或删除标签。

## 配置受保护的标签

先决条件：

- 您必须至少具有该项目的维护者角色。

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 仓库**。
1. 展开 **受保护的标签**。
1. 要保护单个标签，请选择 **标签**，然后从下拉列表中选择您的标签。
1. 保护名称与字符串匹配的所有标签：
    1. 选择 **标签**。
    1. 输入用于标签匹配的字符串。支持通配符 (`*`)。
    1. 选择 **创建通配符**。
1. 在 **允许创建** 中，选择可以创建受保护标签的用户或角色。
1. 选择 **保护**。

受保护的标签（或通配符）显示在 **受保护的标签** 列表中。

## 受保护的标签通配符

您可以指定一个保护标签通配符，它会保护所有与通配符匹配的标签。例如：

| 受保护的标签通配符 | 匹配标签                 |
|------------------------|-------------------------------|
| `v*`                   | `v1.0.0`, `version-9.1`       |
| `*-deploy`             | `march-deploy`, `1.0-deploy`  |
| `*gitlab*`             | `gitlab`, `gitlab/v1`         |
| `*`                    | `v1.0.1rc2`, `accidental-tag` |

两个不同的通配符可能匹配同一个标签。 例如，`*-stable` 和 `production-*` 都会匹配一个 `production-stable` 标签。
在这种情况下，如果这些受保护标签中的*任何*具有类似 **允许创建** 的设置，那么 `production-stable` 也会继承此设置。

如果单击受保护标签的名称，系统会显示所有匹配标签的列表：

![Protected tag matches](img/protected_tag_matches.png)

## 防止创建与分支同名的标签

同名的标签和分支可以包含不同的提交。如果您的标签和分支使用相同的名称，则运行 `git checkout` 命令的用户可能会检查*标签* `qa`，而不是检查*分支* `qa`。

为了防止这个问题：

1. 确定您不想用作标签的分支名称。
1. 如[配置受保护的标签](#配置受保护的标签)中所述，创建受保护的标签：

   - 在 **名称** 中，请提供一个名称，例如 `stable`。您还可以创建通配符，例如 `stable-*` 来匹配多个名称，例如 `stable-v1` 和 `stable-v2`。
   - 在 **允许创建** 中，选择 **No one**。
   - 选择 **保护**。

用户仍然可以使用受保护的名称创建分支，但不能创建标签。

## 删除受保护的标签

您可以使用 API 或用户界面手动删除受保护的标签。

先决条件：

- 您必须在项目中至少拥有维护者角色。

操作步骤：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **仓库 > 标签**。
1. 在您要删除的标签旁边，选择 **删除** (**{remove}**)。
1. 在确认对话框中，输入标签名称并选择 **是，删除受保护的标签**。

只能使用极狐GitLab 从 UI 或 API 删除受保护的标签。
这些保护措施可防止您通过本地 Git 命令或第三方 Git 客户端意外删除标签。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
