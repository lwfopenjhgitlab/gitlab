---
type: index, reference
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 里程碑 **(FREE)**

极狐GitLab 中的里程碑是一种跟踪议题和合并请求的方法，这些请求是为了在特定时间段内实现更广泛的目标而创建的。

里程碑允许您组织议题并将请求合并到一个有凝聚力的组中，并具有可选的开始日期和可选的截止日期。

## 作为发布的里程碑

里程碑可以用于跟踪发布：

1. 设置里程碑截止日期以代表您的发布日期，并将里程碑开始日期留空。
1. 将里程碑标题设置为您发布的版本，例如 `Version 9.4`。
1. 通过从议题的右侧边栏中关联所需的里程碑，将议题添加到您的版本中。

此外，您可以将里程碑与发布功能<!--[发布功能](../releases/index.md#associate-milestones-with-a-release)-->集成。

## 项目里程碑和群组里程碑

里程碑可以属于[项目](../index.md)或[群组](../../group/index.md)。

您只能将 **项目里程碑** 分配给该项目中的议题或合并请求。

您可以将 **群组里程碑** 分配给该群组中任何项目的任何议题或合并请求。

您还可以在仪表板里程碑列表中查看您有权访问的所有里程碑。
要查看您有权访问的项目里程碑和群组里程碑，请选择顶部栏中的 **主菜单 > 里程碑**。

<!--
有关项目和组里程碑 API 的信息，请参阅：

- [Project Milestones API](../../../api/milestones.md)
- [Group Milestones API](../../../api/group_milestones.md)
-->

### 查看项目或群组里程碑

查看里程碑列表：

1. 在顶部栏上，选择 **主菜单 >项目** 并找到您的项目或 **主菜单 > 群组** 并找到您的群组。
1. 选择 **议题 > 里程碑**。

在项目中，系统显示属于该项目的里程碑。
在一个群组中，系统显示属于该群组的里程碑以及该群组中的所有项目。

### 查看已关闭议题的项目中的里程碑

如果项目议题跟踪[关闭](../settings/index.md#configure-project-visibility-features-and-permissions)，您可以通过转到其 URL 来访问里程碑页面。

操作步骤：

1. 转到您的项目。
1. 添加：`/-/milestones` 到您的项目 URL。

该项目的议题在群组的里程碑页面中可见。

### 查看所有里程碑

您可以在整个极狐GitLab 命名空间中查看您有权访问的所有里程碑。
您可能看不到某些里程碑，因为它们位于您不属于的项目或群组中。

为此，请在顶部栏中选择 **主菜单 > 里程碑**。

### 查看里程碑详情

要查看有关里程碑的更多信息，请在里程碑列表中选择要查看的里程碑的标题。

里程碑视图显示标题和描述。

这些下方还有显示以下内容的选项卡：

- **议题**：显示分配给里程碑的所有议题。它们显示在如下三列中：
   - 未启动的议题（开放和未分配）
   - 处理中的议题（开放和已分配）
   - 已完成的议题（已关闭）
- **合并请求**：显示分配给里程碑的所有合并请求。它们显示在如下四列中：
   - 正在进行的工作（开放和未分配）
   - 等待合并（开放和未分配）
   - 拒绝（已关闭）
   - 已合并
- **参与者**：显示分配给里程碑的议题的所有指派人。
- **标记**：显示分配给里程碑的议题中使用的所有标记。

### 燃尽图

里程碑视图包含一个[燃尽图和燃起图](burndown_and_burnup_charts.md)，显示完成里程碑的进度。

![burndown chart](img/burndown_and_burnup_charts_v15_3.png)

### 里程碑侧边栏

里程碑视图上的里程碑侧栏显示以下内容：

- 完成百分比，计算方法为已关闭议题的数量除以议题总数。
- 开始日期和截止日期。
- 在分配给里程碑的所有议题和合并请求上花费的总时间。
- 分配给里程碑的所有议题的总议题权重。

![Project milestone page](img/milestones_project_milestone_page_sidebar_v13_11.png)

## 创建里程碑

> 于 15.0 版本，所需的最小用户角色从开发者更改为报告者。

可以在项目或群组级别创建里程碑。

先决条件：

- 对于里程碑所属的项目或群组，您必须至少具有报告者角色。

创建里程碑：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目或 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **议题 > 里程碑**。
1. 选择 **新建里程碑**。
1. 输入标题。
1. 可选。输入描述、开始日期和截止日期。
1. 选择 **新建里程碑**。

![New milestone](img/milestones_new_project_milestone.png)

## 编辑里程碑

> 于 15.0 版本，所需的最小用户角色从开发者更改为报告者。

先决条件：

- 对于里程碑所属的项目或群组，您必须至少具有报告者角色。

要编辑里程碑：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目或 **主菜单 > 群组** 并找到您的群组。
1. 选择里程碑的标题。
1. 在右上角，选择 **里程碑操作** (**{ellipsis_v}**)，然后选择 **编辑**。
1. 编辑标题、开始日期、截止日期或描述。
1. 选择 **保存更改**。

## 删除里程碑

> 于 15.0 版本，所需的最小用户角色从开发者更改为报告者。

先决条件：

- 对于里程碑所属的项目或群组，您必须至少具有报告者角色。

删除里程碑：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目或 **主菜单 > 群组** 并找到您的群组。
1. 选择里程碑的标题。
1. 在右上角，选择 **里程碑操作** (**{ellipsis_v}**)，然后选择 **删除**。
1. 选择 **删除里程碑**。

<a id="promote-a-project-milestone-to-a-group-milestone"></a>

### 将项目里程碑提升为群组里程碑

如果您要扩展群组中的项目数量，您可能希望在该群组的项目之间共享相同的里程碑。您还可以将项目里程碑提升为群组里程碑，以便将它们提供给同一群组中的其他项目。

提升里程碑，会将该群组中具有相同名称的所有项目中的所有项目里程碑合并为一个群组里程碑。
之前分配给这些项目里程碑的所有议题和合并请求都将分配给新的群组里程碑。

WARNING:
此操作无法撤消，并且更改是永久性的。

先决条件：

- 您必须至少具有该群组的报告者角色。

提升项目里程碑：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 执行以下两个操作之一：
   - 选择 **提升至群组里程碑** (**{level-up}**)。
   - 选择里程碑标题，然后选择 **里程碑操作** (**{ellipsis_v}**) > **提升**。
1. 选择 **提升里程碑**。

## 为议题或合并请求分配里程碑

每个问题和合并请求都可以分配一个里程碑。
里程碑在右侧边栏中的每个议题和合并请求页面上都可见。
它们在议题看板上也可见。

要分配或取消分配里程碑：

1. 查看议题或合并请求。
1. 在右侧边栏的 **里程碑** 旁边，选择 **编辑**。
1. 在 **分配里程碑** 列表中，通过输入里程碑的名称来搜索里程碑。您可以从项目里程碑和群组里程碑中进行选择。
1. 选择您要分配的里程碑。

您还可以在评论中使用 `/assign` [快速操作](../quick_actions.md)。

## 按里程碑过滤议题和合并请求

### 在列表页面中过滤

从项目和群组议题/合并请求列表页面，您可以按群组和项目里程碑过滤<!--[过滤](../../search/index.md#issues-and-merge-requests)-->。

### 在议题看板中过滤

从[项目议题看板](../issue_board.md)，您可以按群组里程碑和项目里程碑进行过滤：

- 搜索和过滤栏<!--[搜索和过滤栏](../../search/index.md#issue-boards)-->
- 议题看板配置<!--[议题看板配置](../issue_board.md#configurable-issue-boards)-->

从[群组议题看板](../issue_board.md#群组议题看板)，您可以仅按群组里程碑进行过滤：

- 搜索和过滤栏<!--[搜索和过滤栏](../../search/index.md#issue-boards)-->
- 议题看板配置<!--[议题看板配置](../issue_board.md#configurable-issue-boards)-->

### 特殊里程碑过滤器

按里程碑过滤时，除了选择特定的项目里程碑或群组里程碑外，您还可以选择特殊的里程碑过滤器。

- **无**：显示没有指定里程碑的议题或合并请求。
- **任何**：显示具有指定里程碑的议题或合并请求。
- **即将到来的**：显示已分配为开放的里程碑并在未来具有最近截止日期的议题或合并请求。
- **已开始**：显示具有开放分配里程碑且开始日期早于今天的议题或合并请求。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
