---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# 文件锁定 **(FREE)**

要防止由无法解决的合并冲突导致的工作浪费需要不同的工作方式，意味着需要明确请求写入权限，并在您开始之前，验证没有其它人正在编辑同一个文件。

尽管分支策略对于源代码和纯文本通常很有效，因为不同的版本可以合并在一起，但它们不适用于二进制文件。

设置文件锁定后，可锁定文件默认为 **只读**。

当文件被锁定时，只有锁定该文件的用户才能对其进行修改。该用户被称为“持有锁”，因为一次只有一个用户可以锁定一个文件。当文件或目录被解锁时，被称为用户“释放锁”。

极狐GitLab 支持两种不同的文件锁定模式：

- [独占文件锁](#独占文件锁)，适用于二进制文件：**通过命令行**使用 Git LFS 和 `.gitattributes` 完成，它可以防止在任何分支上修改锁定的文件。
- [默认分支文件和目录锁](#默认分支文件和目录锁)：**通过 GitLab UI** 完成，它可以防止在默认分支上修改锁定的文件和目录。

## 权限

任何在仓库中至少拥有开发者权限的人都可以创建锁。

只有锁定文件或目录的用户才能编辑锁定的文件。其它用户无法通过推送、合并或任何其他方式修改锁定的文件，并显示如下错误：`The path '.gitignore' is locked by Administrator`。

## 独占文件锁

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/35856) in GitLab 10.5.
-->

此过程允许您锁定单个文件或文件扩展名，它是通过命令行完成的，不需要极狐GitLab 付费订阅。

Git LFS 以跟踪文件以减少 Git 仓库的存储而闻名，但它也可用于[锁定文件](https://github.com/git-lfs/git-lfs/wiki/File-Locking)。 
这是用于独占文件锁的方法。

### 安装 Git LFS

在开始之前，请确保您的计算机中有 Git LFS 安装<!--[Git LFS 安装](../../topics/git/lfs/index.md)-->。打开终端窗口并运行：

```shell
git-lfs --version
```

如果不能识别此命令，则必须安装它。有多种[安装方式](https://git-lfs.com/)，可以根据自己的操作系统选择。要使用 Homebrew 安装它：

```shell
brew install git-lfs
```

安装后，**在终端窗口中打开您的本地仓库**，并在您的仓库中安装 Git LFS。如果您确定已经安装了 LFS，则可以跳过此步骤。 如果您不确定，重新安装亦无害处：

```shell
git lfs install
```

<!--
Check this document to learn more about [using Git LFS](../../topics/git/lfs/index.md#using-git-lfs).
-->

### 配置独占文件锁

您需要维护者角色通过命令行，为您的项目配置独占文件锁。

在使用文件锁定之前要做的第一件事是告诉 Git LFS 哪种文件是可锁定的。以下命令将 PNG 文件存储在 LFS 中并将它们标记为可锁定：

```shell
git lfs track "*.png" --lockable
```

执行上述命令后，一个名为 `.gitattributes` 的文件被创建或更新为以下内容：

```shell
*.png filter=lfs diff=lfs merge=lfs -text lockable
```

您还可以在不使用 LFS 的情况下将文件类型注册为可锁定（例如，能够在实现 LFS 文件锁定 API 的远程服务器中锁定/解锁您需要的文件）。为此，您可以手动编辑 `.gitattributes` 文件：

```shell
*.pdf lockable
```

`.gitattributes` 文件是该过程的关键，**必须** 推送到远端仓库以使更改生效。

将文件类型注册为可锁定后，Git LFS 会自动将它们在文件系统上设为只读。这意味着您必须在[编辑它](#编辑可锁定文件)之前**锁定文件**。

### 锁定文件

通过锁定文件，您可以确认没有其它人正在编辑它，并在您完成之前阻止其他任何人编辑该文件。另一方面，当您解锁文件时，您表示您已完成编辑并允许其它人对其进行编辑。

要使用独占文件锁定来锁定或解锁文件，请在您的仓库目录中打开一个终端窗口并运行如下所述的命令。

**锁定**一个文件：

```shell
git lfs lock path/to/file.png
```

**解锁**文件：

```shell
git lfs unlock path/to/file.png
```

您还可以通过文件 ID 解锁（当您[查看锁定的文件](#查看独占锁定的文件)时由 LFS 提供）：

```shell
git lfs unlock --id=123
```

如果由于某种原因您需要解锁一个未被自己锁定的文件，只要您对项目具有维护者权限，就可以使用 `--force` 标志：

```shell
git lfs unlock --id=123 --force
```

您通常可以将文件推送到极狐GitLab，无论它们是锁定的还是未锁定的。

NOTE:
虽然可以通过 Git LFS 命令行界面创建和管理多分支文件锁，但可以为任何文件创建文件锁。

### 查看独占锁定的文件

要列出本地用 LFS 锁定的所有文件，请在仓库中打开一个终端窗口并运行：

```shell
git lfs locks
```

输出列出了锁定的文件，后面是锁定每个文件的用户和文件的 ID。

在仓库文件树上，极狐GitLab 为 Git LFS 跟踪的文件显示 LFS 徽章，并在独占锁定的文件上显示挂锁图标：

![LFS-Locked files](img/lfs_locked_files_v13_2.png)

您还可以从 GitLab UI [查看和删除现有锁](#查看和删除现有的锁)。

NOTE:
重命名独占锁定的文件时，锁定将丢失。您必须再次锁定它才能保持锁定状态。

### 编辑可锁定文件

一旦文件[配置为可锁定](#配置独占文件锁)，它被设置为只读。
因此，您需要在编辑之前锁定它。

共享项目的建议工作流程：

1. 锁定文件。
1. 编辑文件。
1. 提交您的更改。
1. 推送到仓库。
1. 审核、批准和合并您的更改。
1. 解锁文件。

## 默认分支文件和目录锁 **(PREMIUM)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/440) in GitLab Enterprise Edition 8.9. Available in [GitLab Premium](https://about.gitlab.com/pricing/).
-->

此过程允许您通过 GitLab UI 一次锁定一个文件，并且需要访问[专业版](https://about.gitlab.cn/pricing/)或更高级别。

默认分支文件和目录锁仅适用于项目设置中设置的[默认分支](repository/branches/default.md)。

阻止对默认分支上锁定文件的更改，包括修改锁定文件的合并请求。解锁文件以允许更改。

### 锁定文件或目录

要锁定文件：

1. 在极狐GitLab 中打开文件或目录。
1. 在右上角的文件上方，选择 **锁定**。
1. 在确认对话框中，选择 **OK**。

如果您没有锁定文件的权限，则不会启用该按钮。

要查看锁定文件的用户（如果不是您），请将鼠标悬停在按钮上。

### 查看和删除现有的锁

查看和删除文件锁定：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **仓库 > 已锁定文件**。

此列表显示通过 LFS 或 GitLab UI 锁定的所有文件。

锁可以由其作者或至少具有维护者角色的任何用户删除。
