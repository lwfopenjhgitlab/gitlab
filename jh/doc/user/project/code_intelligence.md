---
stage: Create
group: Code Review
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# Code Intelligence **(FREE)**

> 引入于 13.1 版本。

Code Intelligence 添加了交互式开发环境 (IDE) 常见的代码导航功能，包括：

- 类型签名和符号文档。
- Go-to 定义。

Code Intelligence 内置于极狐GitLab 并由 [LSIF](https://lsif.dev/)（语言服务器索引格式）提供支持，这是一种用于预计算代码智能数据的文件格式。

NOTE:
您可以使用 [Auto DevOps](../../topics/autodevops/index.md) 在您的应用程序中自动执行此功能。

## 配置

通过将极狐GitLab CI/CD 作业添加到生成 LSIF 产物的项目的 `.gitlab-ci.yml` 中，为项目启用代码智能：

```yaml
code_navigation:
  image: sourcegraph/lsif-go:v1
  allow_failure: true # recommended
  script:
    - lsif-go
  artifacts:
    reports:
      lsif: dump.lsif
```

生成的 LSIF 文件大小可能会受到[产物应用程序限制 (`ci_max_artifact_size_lsif`)](../../administration/instance_limits.md#maximum-file-size-per-type-of-artifact) 的限制，默认为 100MB（可由实例管理员配置）。

作业成功后，可以边浏览代码边查看代码智能数据：

![Code intelligence](img/code_intelligence_v13_4.png)

## 查找引用

> - 引入于 13.2 版本，功能标志名为 `code_navigation_references`。默认禁用。
> - 在 SaaS 版和私有化部署版上启用于 13.3 版本。功能标志 `code_navigation_references` 已删除。

要查找使用特定对象的位置，您可以在 **引用** 选项卡下查看指向特定代码行的链接：

![Find references](img/code_intelligence_find_references_v13_3.png)

## 语言支持

生成 LSIF 文件需要相关语言的语言服务器索引器实现。在他们的网站上查看 [可用 LSIF 索引器](https://lsif.dev/#implementations-server) 的完整列表，并参阅他们的文档，了解如何为您的特定语言生成 LSIF 文件。
