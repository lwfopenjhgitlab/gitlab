---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 洞察 **(ULTIMATE)**

配置对您的项目十分重要的洞察来浏览数据，例如分类、特定时间段创建/关闭的议题、合并请求的平均合并时间等等。

![Insights example bar chart](img/project_insights.png)

NOTE:
此功能[在群组级别也可用](../../group/insights/index.md)。

## 查看您的项目的洞察

您可以通过单击左侧边栏中的 **分析 > 洞察** 链接来访问项目的洞察。

## 配置您的洞察

洞察是使用项目中名为 `.gitlab/insights.yml` 的 YAML 文件配置的。该文件用于项目的洞察页面。

查看下方的[编写您的 `.gitlab/insights.yml`](#writing-your-gitlabinsightsyml)，获取文件内容的详细信息。
NOTE:
创建配置文件后，您还可以[将其用于您的项目群组](../../group/insights/index.md#configure-your-insights)。

NOTE:
如果项目没有任何配置文件，会尽可能尝试使用群组配置。如果群组没有任何配置，则使用默认配置。

## 权限

如果您有权限查看项目，则您有权限查看项目的洞察。

NOTE:
您无权访问的议题或合并请求（因为您无权访问它们所属的项目，或者因为它们是私密的）将从洞察图表中过滤掉。

<!--
You may also consult the [group permissions table](../../permissions.md#group-members-permissions).
-->

<a id="writing-your-gitlabinsightsyml"></a>

## 编写您的`.gitlab/insights.yml`

`.gitlab/insights.yml` 文件定义了项目或群组的每个洞察页面中显示的洞察图表的结构和顺序。

每个页面都有一个唯一的键和一组要获取和显示的图表。

例如，下面是洞察的一个定义，它显示一个页面和一个图表：

```yaml
bugsCharts:
  title: "Charts for bugs"
  charts:
    - title: "Monthly bugs created"
      description: "Open bugs created per month"
      type: bar
      query:
        issuable_type: issue
        issuable_state: opened
        filter_labels:
          - bug
        group_by: month
        period_limit: 24
```

每个图表定义都由键值对哈希组成。

例如单个图表定义：

```yaml
- title: "Monthly bugs created"
  description: "Open bugs created per month"
  type: bar
  query:
    issuable_type: issue
    issuable_state: opened
    filter_labels:
      - bug
    group_by: month
    period_limit: 24
```

## 配置参数

图表被定义为定义图表行为的参数列表。

下表列出了图表的可用参数：

| 关键字                                           | 描述 |
|:---------------------------------------------------|:------------|
| [`title`](#title)                                  | 图表的标题，显示在洞察页面上。 |
| [`description`](#description)                      | 单个图表的描述，显示在相关图表上方。 |
| [`type`](#type)                                    | 图表类型：`bar`、`line` 或 `stacked-bar`。 |
| [`query`](#query)                                  | 定义议题/合并请求成为图表一部分的条件的哈希。 |

## 参数详情

以下是用于配置洞察图表的参数的详细说明。

### `title`

`title` 是图表在洞察页面上显示的标题。
例如：

```yaml
monthlyBugsCreated:
  title: "Monthly bugs created"
```

### `description`

`description` 文本显示在图表上方，但在标题下方，用于提供有关图表的额外详细信息，例如：

```yaml
monthlyBugsCreated:
  title: "Monthly bugs created"
  description: "Open bugs created per month"
```

### `type`

`type` 是图标类型。

例如：

```yaml
monthlyBugsCreated:
  title: "Monthly bugs created"
  type: bar
```

支持的值包括：

| 名称 | 示例 |
| ----- | ------- |
| `bar` | ![Insights example bar chart](img/insights_example_bar_chart.png) |
| `bar`（时间序列，使用 `group_by` 时） | ![Insights example bar time series chart](img/insights_example_bar_time_series_chart.png) |
| `line` | ![Insights example stacked bar chart](img/insights_example_line_chart.png) |
| `stacked-bar` | ![Insights example stacked bar chart](img/insights_example_stacked_bar_chart.png) |

### `query`

`query` 允许定义议题/合并请求成为图表一部分的条件。

示例：

```yaml
monthlyBugsCreated:
  title: "Monthly bugs created"
  description: "Open bugs created per month"
  type: bar
  query:
    issuable_type: issue
    issuable_state: opened
    filter_labels:
      - bug
    collection_labels:
      - S1
      - S2
      - S3
      - S4
    group_by: week
    period_limit: 104
```

#### `query.issuable_type`

定义要为其创建图表的“问题”类型。

支持的值包括：

- `issue`：图表显示议题的数据。
- `merge_request`：图表显示合并请求的数据。

#### `query.issuable_state`

按查询的“问题”的当前状态过滤。

默认情况下，应用 `opened` 状态过滤器。

支持的值是：

- `opened`：开放的议题/合并请求。
- `closed`：关闭开放的议题/合并请求。
- `locked`：已锁定讨论的议题/合并请求。
- `merged`：合并的合并请求。
- `all`：所有状态的议题/合并请求。

#### `query.filter_labels`

按当前应用于查询“问题”的标签过滤。

默认情况下，不应用标签过滤器。所有定义的标签当前必须应用于“问题”才能被选中。

示例：

```yaml
monthlyBugsCreated:
  title: "Monthly regressions created"
  type: bar
  query:
    issuable_type: issue
    issuable_state: opened
    filter_labels:
      - bug
      - regression
```

#### `query.collection_labels`

按配置的标签对“问题”进行分组。

默认情况下，不进行分组。使用此关键字时，您需要将 `type` 设置为 `line` 或 `stacked-bar`。

示例：

```yaml
weeklyBugsBySeverity:
  title: "Weekly bugs by severity"
  type: stacked-bar
  query:
    issuable_type: issue
    issuable_state: opened
    filter_labels:
      - bug
    collection_labels:
      - S1
      - S2
      - S3
      - S4
```

#### `query.group_by`

定义图表的 X 轴。

支持的值是：

- `day`：每天的群组数据。
- `week`：每周的群组数据。
- `month`：每月的群组数据。

#### `query.period_limit`

定义查询“问题”的追溯过去程度（使用 `query.period_field`）。

该单元与您定义的 `query.group_by` 相关。例如，如果您定义了 `query.group_by: 'day'`，则 `query.period_limit: 365` 将意味着“收集并显示过去 365 天的数据”。

默认情况下，根据您定义的 `query.group_by` 应用默认值。

| `query.group_by` | 默认值 |
| ---------------- | ------------- |
| `day`            | 30            |
| `week`           | 4             |
| `month`          | 12            |

#### `query.period_field`

定义用于对“问题”进行分组的时间戳字段。

支持的值是：

- `created_at`（默认）：使用 `created_at` 字段对数据进行分组。
- `closed_at`：使用 `closed_at` 字段对数据进行分组（仅针对议题）。
- `merged_at`：使用 `merged_at` 字段对数据进行分组（仅适用于合并请求）。

`period_field` 自动设置为：

- `closed_at`：如果 `query.issuable_state` 是 `close`
- `merged_at`：如果 `query.issuable_state` 是 `merged`
- `created_at`：其它情况 

NOTE:
您可能会看到 `created_at` 代替了 `merged_at`，`created_at` 被使用。

### `projects`

您可以限制查询“问题”的位置：

- 如果 `.gitlab/insights.yml` 用于[群组的洞察](../../group/insights/index.md#configure-your-insights)，使用 `projects` 可以限制要查询的项目。默认情况下，使用当前群组下的所有项目。
- 如果 `.gitlab/insights.yml` 用于项目的洞察，指定任何其它项目不会产生任何结果。默认情况下，使用项目本身。

#### `projects.only`

`projects.only` 选项指定应该从中查询“问题”的项目。

在以下情况，将忽略此处列出的项目：

- 项目不存在。
- 当前用户没有足够的权限来读取项目。
- 项目在群组之外。

在下面的 `insights.yml` 示例中，指定了使用查询的项目。此示例在设置群组的洞察时很有用：

```yaml
monthlyBugsCreated:
  title: "Monthly bugs created"
  description: "Open bugs created per month"
  type: bar
  query:
    issuable_type: issue
    issuable_state: opened
    filter_labels:
      - bug
  projects:
    only:
      - 3                         # You can use the project ID
      - groupA/projectA           # Or full project path
      - groupA/subgroupB/projectC # Projects in subgroups can be included
      - groupB/project            # Projects outside the group will be ignored
```

## 完整示例

```yaml
.projectsOnly: &projectsOnly
  projects:
    only:
      - 3
      - groupA/projectA
      - groupA/subgroupB/projectC

bugsCharts:
  title: "Charts for bugs"
  charts:
    - title: "Monthly bugs created"
      description: "Open bugs created per month"
      type: bar
      <<: *projectsOnly
      query:
        issuable_type: issue
        issuable_state: opened
        filter_labels:
          - bug
        group_by: month
        period_limit: 24

    - title: "Weekly bugs by severity"
      type: stacked-bar
      <<: *projectsOnly
      query:
        issuable_type: issue
        issuable_state: opened
        filter_labels:
          - bug
        collection_labels:
          - S1
          - S2
          - S3
          - S4
        group_by: week
        period_limit: 104

    - title: "Monthly bugs by team"
      type: line
      <<: *projectsOnly
      query:
        issuable_type: merge_request
        issuable_state: opened
        filter_labels:
          - bug
        collection_labels:
          - Manage
          - Plan
          - Create
        group_by: month
        period_limit: 24
```
