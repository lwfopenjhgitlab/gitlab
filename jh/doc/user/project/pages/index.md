---
description: 'Learn how to use GitLab Pages to deploy a static website at no additional cost.'
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab Pages **(FREE SELF)**

使用极狐GitLab Pages，您可以直接从极狐GitLab 的仓库中发布静态网站。

<div class="row">
<div class="col-md-9">
<p style="margin-top: 18px;">
<ul>
<li>用于任何个人或商业网站。</li>
<li>使用任何静态站点生成器 (SSG) 或纯 HTML。</li>
<li>为您的项目、群组或用户帐户创建网站。</li>
<li>在您自己的极狐GitLab 私有化部署实例上免费托管您的网站。</li>
<li>连接您的自定义域名和 TLS 证书。</li>
<li>将任何许可证归于您的内容。</li>
</ul>
</p>
</div>
<div class="col-md-3"><img src="img/ssgs_pages.png" alt="Examples of SSGs supported by Pages" class="middle display-block"></div>
</div>

要使用 Pages 发布网站，您可以使用任何静态网站生成器，例如 Gatsby、Jekyll、Hugo、Middleman、Harp、Hexo 或 Brunch。您还可以发布任何直接用纯 HTML、CSS 和 JavaScript 编写的网站。

Pages 不支持动态服务器端处理，例如，`.php` 和 `.asp` 需要。
<!--
Learn more about
[static websites compared to dynamic websites](https://about.gitlab.com/blog/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/).
-->

<!--
## 入门

要创建一个 Pages 网站：

| 文档 | 描述 |
|----------|-------------|
| [从头开始创建一个 `.gitlab-ci.yml` 文件](getting_started/pages_from_scratch.md) | 将 Pages 网站添加到现有项目。了解如何创建和配置您自己的 CI 文件。 |
| [使用 `.gitlab-ci.yml` 模板](getting_started/pages_ci_cd_template.md) | 将 Pages 网站添加到现有项目。使用预填充的 CI 模板文件。 |
| [派生示例项目](getting_started/pages_forked_sample_project.md) | 通过派生一个示例项目，使用已经配置的 Pages 创建一个新项目。 |
| [使用项目模板](getting_started/pages_new_project_template.md) | 通过已使用模板配置的 Pages 创建一个新项目。 |

要更新一个 Pages 网站：

| 文档 | 描述 |
|----------|-------------|
| [GitLab Pages domain names, URLs, and base URLs](getting_started_part_one.md) | Learn about GitLab Pages default domains. |
| [Explore GitLab Pages](introduction.md) | Requirements, technical aspects, specific GitLab CI/CD configuration options, Access Control, custom 404 pages, limitations, and FAQ. |
| [Custom domains and SSL/TLS Certificates](custom_domains_ssl_tls_certification/index.md) | Custom domains and subdomains, DNS records, and SSL/TLS certificates. |
| [Let's Encrypt integration](custom_domains_ssl_tls_certification/lets_encrypt_integration.md) | Secure your Pages sites with Let's Encrypt certificates, which are automatically obtained and renewed by GitLab. |
| [Redirects](redirects.md) | Set up HTTP redirects to forward one page to another. |

Learn more and see examples:

| Document | Description |
|----------|-------------|
| [Static vs dynamic websites](https://about.gitlab.com/blog/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/) | Static versus dynamic site overview. |
| [Modern static site generators](https://about.gitlab.com/blog/2016/06/10/ssg-overview-gitlab-pages-part-2/) | SSG overview. |
| [Build any SSG site with GitLab Pages](https://about.gitlab.com/blog/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/) | Use SSGs for GitLab Pages. |
-->

## 工作原理

要使用 Pages，您必须在极狐GitLab 中创建一个项目来上传您网站的文件。这些项目可以是公开的、内部的或私有的。

极狐GitLab 始终从仓库中名为 `public` 的特定文件夹部署您的网站。当您在极狐GitLab 中创建新项目时，[仓库](../repository/index.md)会自动变为可用。

为了部署您的站点，极狐GitLab 使用[极狐GitLab CI/CD](../../../ci/index.md) 的内置工具来构建您的站点，并将其发布到 Pages 服务器。极狐GitLab CI/CD 为完成此任务而运行的脚本序列是从 `.gitlab-ci.yml` 文件中创建的，您可以创建和修改。
配置文件中名为 `pages` 的特定 `job` 使极狐GitLab 知晓您正在部署 Pages 网站。

您可以使用 Pages 网站的默认域名<!--、`*.gitlab.io` -->或您自己的域名 (`example.com`)。在这种情况下，您必须是域名注册商（或控制面板）的管理员才能使用 Pages 进行设置。

下图显示了您在开始使用 Pages 时可能遵循的工作流程。

<img src="img/new_project_for_pages_v12_5.png" alt="New projects for GitLab Pages">

## 访问您的 Pages 网站

<!--
如果您使用 Pages 默认域名 (`.gitlab.io`)，您的网站会自动安全并在 HTTPS 下可用。 如果您使用自己的自定义域，则可以选择使用 SSL/TLS 证书对其进行保护。

如果您使用 GitLab.com，则您的网站可在 Internet 上公开访问。
要限制对您网站的访问，请启用 [GitLab 页面访问控制](pages_access_control.md)。
-->

您使用的是私有化部署版实例，您的网站将根据您的系统管理员选择的 [Pages 设置](../../../administration/pages/index.md)，在您自己的服务器上发布，管理员可以将它们设置为公开或内部。

<!--
## Pages examples

These GitLab Pages website examples can teach you advanced techniques to use
and adapt for your own needs:

- [Posting to your GitLab Pages blog from iOS](https://about.gitlab.com/blog/2016/08/19/posting-to-your-gitlab-pages-blog-from-ios/).
- [GitLab CI: Run jobs sequentially, in parallel, or build a custom pipeline](https://about.gitlab.com/blog/2016/07/29/the-basics-of-gitlab-ci/).
- [GitLab CI: Deployment & environments](https://about.gitlab.com/blog/2021/02/05/ci-deployment-and-environments/).
- [Building a new GitLab docs site with Nanoc, GitLab CI, and GitLab Pages](https://about.gitlab.com/blog/2016/12/07/building-a-new-gitlab-docs-site-with-nanoc-gitlab-ci-and-gitlab-pages/).
- [Publish code coverage reports with GitLab Pages](https://about.gitlab.com/blog/2016/11/03/publish-code-coverage-report-with-gitlab-pages/).
-->

## 为私有化部署实例管理极狐GitLab Pages

如果您正在运行极狐GitLab 的私有化部署实例，请[按照管理步骤](../../../administration/pages/index.md)配置 Pages。

<!--
## 安全

如果您的用户名是 `example`，那么您的 GitLab Pages 网站位于 `example.gitlab.io`。
极狐GitLab 允许用户名包含 `.`，因此名为 `bar.example` 的用户可以创建一个 Pages 网站 `bar.example.gitlab.io`，它实际上是您的 `example.gitlab.io` 网站的子域名。如果您使用 JavaScript 为您的网站设置 cookie，请务必小心。
使用 JavaScript 手动设置 cookie 的安全方法是不指定 `domain`：

```javascript
// Safe: This cookie is only visible to example.gitlab.io
document.cookie = "key=value";

// Unsafe: This cookie is visible to example.gitlab.io and its subdomains,
// regardless of the presence of the leading dot.
document.cookie = "key=value;domain=.example.gitlab.io";
document.cookie = "key=value;domain=example.gitlab.io";
```

This issue doesn't affect users with a custom domain, or users who don't set any
cookies manually with JavaScript.
-->
