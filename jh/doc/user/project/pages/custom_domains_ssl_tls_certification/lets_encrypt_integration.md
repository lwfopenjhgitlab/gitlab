---
type: reference
description: "Automatic Let's Encrypt SSL certificates for GitLab Pages."
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Pages 与 Let's Encrypt 集成 **(FREE SELF)**

Pages 与 Let's Encrypt (LE) 的集成允许您将 LE 证书用于具有自定义域名的 Pages 网站，而无需自己发布和更新它们<!--；GitLab 为您提供开箱即用的功能-->。

[Let's Encrypt](https://letsencrypt.org) 是一个免费的、自动化的和开源的证书颁发机构。

WARNING:
此功能仅涵盖**自定义域名**的证书，不包括运行 [Pages daemon](../../../../administration/pages/index.md) 所需的通配符证书 **(FREE SELF)**。

## 先决条件

在为您的域名启用 SSL 证书的自动配置之前，请确保您具有：

- 在极狐GitLab 中创建了一个[项目](../index.md#getting-started)，其中包含您网站的源代码。
- 获取一个域名 (`example.com`) 并添加一个 [DNS 条目](index.md)，将其指向您的 Pages 网站。顶级域名 (`.com`) 必须是[公共后缀](https://publicsuffix.org/)。
- [将您的域名添加到您的 Pages 项目](index.md#1-add-a-custom-domain-to-pages)并验证您的所有权。
- 验证您的网站已启动并正在运行，可通过您的自定义域名访问。

<!--The GitLab integration with Let's Encrypt is enabled and available on GitLab.com.-->
对于**私有化部署**实例，请确保您的管理员已[启用它](../../../../administration/pages/index.md#lets-encrypt-integration)。

<a id="enabling-lets-encrypt-integration-for-your-custom-domain"></a>

## 为您的自定义域名启用 Let's Encrypt 集成

满足要求后，启用 Let's Encrypt 集成：

1. 导航到您项目的 **设置 > Pages**。
1. 找到您的域并选择 **详情**。
1. 选择右上角的 **编辑**。
1. 通过切换 **使用 Let's Encrypt 的自动证书管理** 启用 Let's Encrypt 集成：

   ![Enable Let's Encrypt](img/lets_encrypt_integration_v12_1.png)

1. 选择 **保存修改**。

启用后，系统将获得 LE 证书并将其添加到关联的 Pages 域名。系统也会自动更新它。

**注意：**

- 颁发证书和更新 Pages 配置**最多可能需要一个小时**。
- 如果您在域名设置中已经有 SSL 证书，它会继续工作，直到被 Let's Encrypt 的证书取代。

<a id="troubleshooting"></a>

## 故障排除

### 错误 "Something went wrong while obtaining the Let's Encrypt certificate"

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/30146) in GitLab 13.0.
-->

如果您收到错误 "Something went wrong while obtaining the Let's Encrypt certificate"，首先，请确保您的 Pages 站点在项目的 **设置 > 通用 > 可见性** 中设置为“所有人”，这样可以允许 Let's Encrypt Servers 访问您的 Pages 站点。确认后，您可以按照以下步骤再次尝试获取证书：

1. 转到您项目的 **设置 > Pages**。
1. 在您的域中选择 **编辑**。
1. 选择 **重试**。
1. 如果您仍然看到相同的错误：
     1. 确保您只为您的域名正确设置了一个 DNS `CNAME` 或 `A` 记录。
     1. 确保您的域名**没有** `AAAA` DNS 记录。
     1. 如果您的域名或任何更高级别的域名有 `CAA` DNS 记录，请确保 [它包括 `letsencrypt.org`](https://letsencrypt.org/docs/caa/)。
     1. 确保[您的域名已验证](index.md#1-add-a-custom-domain-to-pages)。
     1. 转到步骤 1。

### 消息 "GitLab is obtaining a Let's Encrypt SSL certificate for this domain. This process can take some time. Please try again later." 持续超过一小时

如果您已启用 Let's Encrypt 集成，但一个小时后证书不存在，并且您看到消息 "GitLab is obtaining a Let's Encrypt SSL certificate for this domain. This process can take some time. Please try again later."，尝试按照以下步骤再次删除和添加 Pages 的域名：

1. 转到您项目的 **设置 > Pages**。
1. 在您的域名中选择 **删除**。
1. [再次添加域名并验证](index.md#1-add-a-custom-domain-to-pages)。
1. [为您的域名启用 Let's Encrypt 集成](#enabling-lets-encrypt-integration-for-your-custom-domain)。
1. 如果一段时间后您仍然看到相同的消息：
     1. 确保您只为您的域名正确设置了一个 DNS `CNAME` 或 `A` 记录。
     1. 确保您的域名**没有** `AAAA` DNS 记录。
     1. 如果您的域名或任何更高级别的域名有 `CAA` DNS 记录，请确保[它包括 `letsencrypt.org`](https://letsencrypt.org/docs/caa/)。
     1. 转到步骤 1。

<!-- Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, for example, `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
