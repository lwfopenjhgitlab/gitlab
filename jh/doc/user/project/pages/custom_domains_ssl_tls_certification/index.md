---
disqus_identifier: 'https://docs.gitlab.com/ee/user/project/pages/getting_started_part_three.html'
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 自定义域名和 SSL/TLS 证书 **(FREE SELF)**

> 引入于 15.4 版本，您可以使用经过验证的域名绕过 SAML 或 SCIM 配置用户的用户电子邮件确认。

您可以使用自定义域名：

- 用于极狐GitLab Pages。
- 绕过 SAML 或 SCIM 配置用户的电子邮件确认。以这种方式使用自定义域名时，您可以使用极狐GitLab Pages 功能，但可以跳过[先决条件](#prerequisites)。

要使用一个或多个自定义域名：


- 添加[自定义**根域名**或**子域名**](#set-up-pages-with-a-custom-domain)。
- 添加 [SSL/TLS 认证](#adding-an-ssltls-certificate-to-pages)。

<a id="set-up-pages-with-a-custom-domain"></a>

## 使用自定义域名设置 Pages

要使用自定义域名设置 Pages，请阅读以下要求和步骤。

<a id="prerequisites"></a>

### 先决条件

- 一个启动并运行的 Pages 网站，在默认 Pages 域名下提供服务。
- 自定义域名 `example.com` 或子域名 `subdomain.example.com`。
- 访问您域名的服务器控制面板以设置 DNS 记录：
  - 将您的域名指向 Pages 服务器的 DNS 记录（`A`、`ALIAS` 或 `CNAME`）。如果该名称上有多个 DNS 记录，则必须使用 `ALIAS` 记录。
  - 用于验证您的域名所有权的 DNS `TXT` 记录。
- 将 `/etc/gitlab/gitlab.rb` 中的 `external_http` 或 `external_https` 设置为 [Pages Daemon](../../../../administration/pages/index.md#overview) 的 IP 和端口。如果您没有 IPv6，则可以省略 IPv6 地址。

  示例：

  ```ruby
  # Redirect pages from HTTP to HTTPS
  gitlab_pages['external_http'] = ['192.0.2.2:80', '[2001:db8::2]:80'] # The secondary IPs for the GitLab Pages daemon
  gitlab_pages['external_https'] = ['192.0.2.2:443', '[2001:db8::2]:443'] # The secondary IPs for the GitLab Pages daemon
  ```

### 步骤

按照以下步骤将您的自定义域添加到 Pages。另请参阅本文档，查看 [DNS 记录概述](dns_concepts.md)。

<a id="1-add-a-custom-domain-to-pages"></a>

#### 1. 添加自定义域名到 Pages

导航到您项目的 **设置 > Pages** 并选择 **+ 新建域名**，将您的自定义域名添加到极狐GitLab Pages。您可以选择添加 [SSL/TLS 证书](#adding-an-ssltls-certificate-to-pages)，或将其留空（可以稍后添加）。

选择 **创建新域名**。

![Add new domain](img/add_certificate_to_pages.png)

#### 2. 获取验证码

将新域名添加到 Pages 后，验证码会提示您。从极狐GitLab 复制值，并将它们粘贴到您域名的控制面板中，作为下一步的 `TXT` 记录。

![Get the verification code](img/get_domain_verification_code_v12_0.png)

#### 3. 为 Pages 设置 DNS 记录

请参阅本文档，查看 [DNS 记录概述](dns_concepts.md)。
如果您熟悉该主题，请根据您希望在 Pages 网站上使用的域名类型按照以下说明进行操作：

- [根域名](#for-root-domains)，`example.com`。
- [子域名](#for-subdomains)，`subdomain.example.com`。
- [根域名和子域名](#for-both-root-and-subdomains)。

<!--
You can [configure IPv6 on self-managed instances](../../../../administration/pages/index.md#advanced-configuration),
but IPv6 is not currently configured for Pages on GitLab.com.
Follow [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/214718) for details.
-->

<a id="for-root-domains"></a>

##### 根域名

根域名（`example.com`）需要：

- 一条 [DNS `A` 记录](dns_concepts.md#a-record)，将您的域名指向 Pages 服务器。
- 一条 [`TXT` 记录](dns_concepts.md#txt-record)，验证您的域名的所有权。

| From                                          | DNS 记录 | To              |
| --------------------------------------------- | ---------- | --------------- |
| `example.com`                                 | `A`        | `35.185.44.232` |
| `_gitlab-pages-verification-code.example.com` | `TXT`      | `gitlab-pages-verification-code=00112233445566778899aabbccddeeff` |

<!--
For projects on GitLab.com, this IP is `35.185.44.232`.
For projects living in other GitLab instances (CE or EE), please contact
your sysadmin asking for this information (which IP address is Pages
server running on your instance).
-->

![DNS `A` record pointing to GitLab.com Pages server](img/dns_add_new_a_record_example_updated_2018.png)

WARNING:
请注意，如果您**仅**将根域名用于 Pages 网站，并且您的域名注册商支持此功能，则可以添加 DNS apex `CNAME` 记录而不是 `A` 记录。<!--这样做的主要优点是，当 GitLab.com 上的 GitLab Pages IP 因任何原因发生更改时，您无需更新“A”记录。-->可能有一些例外，但**不建议使用此方法**，因为如果您为根域名设置 [`MX` 记录](dns_concepts.md#mx-record)，它很可能不起作用。

<a id="for-subdomains"></a>

##### 子域名

子域名（`subdomain.example.com`）需要：

- 一条 DNS [`ALIAS` 或 `CNAME` 记录](dns_concepts.md#cname-record)，将您的子域名指向 Pages 服务器。
- 一条 DNS [`TXT` 记录](dns_concepts.md#txt-record)，验证您的域名的所有权。

| From                                                    | DNS 记录      | To                    |
|:--------------------------------------------------------|:----------------|:----------------------|
| `subdomain.example.com`                                 | `ALIAS`/`CNAME` | `namespace.gitlab.io` |
| `_gitlab-pages-verification-code.subdomain.example.com` | `TXT`           | `gitlab-pages-verification-code=00112233445566778899aabbccddeeff` |

请注意，无论是用户还是项目网站，DNS 记录都应指向您的 Pages 域名（`namespace.gitlab.io`），不带任何`/project-name`。

![DNS `CNAME` record pointing to GitLab.com project](img/dns_cname_record_example.png)

<a id="for-both-root-and-subdomains"></a>

##### 根域名和子域名

在某些情况下，您需要将子域名和根域名都指向同一个网站，例如 `example.com` 和 `www.example.com`。

需要：

- 域名的 DNS `A` 记录。
- 子域名的 DNS `ALIAS`/`CNAME` 记录。
- 域名和子域名都有一个 DNS `TXT` 记录。

| From                                              | DNS 记录 | To                     |
| ------------------------------------------------- | ---------- | ---------------------- |
| `example.com`                                     | `A`        | `35.185.44.232`        |
| `_gitlab-pages-verification-code.example.com`     | `TXT`      | `gitlab-pages-verification-code=00112233445566778899aabbccddeeff` |
|---------------------------------------------------+------------+------------------------|
| `www.example.com`                                 | `CNAME`    | `namespace.gitlab.io`  |
| `_gitlab-pages-verification-code.www.example.com` | `TXT`      | `gitlab-pages-verification-code=00112233445566778899aabbccddeeff` |

<!--
如果您使用的是 Cloudflare，请选中[使用 Cloudflare 将 `www.domain.com` 重定向到 `domain.com`](#redirecting-wwwdomaincom-to-domaincom-with-cloudflare)。
-->

**注意：**

- 如果您想将您的 `domain.com` 指向您的 Pages 站点，请不要使用 `CNAME` 记录。请改用 `A` 记录。
- **不要**在默认 Pages 域名之后添加任何特殊字符。例如，不要将 `subdomain.domain.com` 指向 `namespace.gitlab.io/`。不过，一些域名托管服务提供商可能会要求一个尾随点（`namespace.gitlab.io.`）。

#### 4. 验证域名的所有权

添加所有 DNS 记录后：

1. 返回您项目的 **设置 > Pages**。
1. 找到您的域名并选择 **详细信息**。
1. 选择 **重试验证** 按钮激活您的新域名。

![Verify your domain](img/retry_domain_verification_v12_0.png)

一旦您的域名生效，您的网站就可以通过您的域名访问。

WARNING:
对于启用域名验证的极狐GitLab 实例，如果域名无法验证 7 天，则将其从极狐GitLab 项目中删除。

**注意：**

- 对于私有化部署实例，您的极狐GitLab 管理员可以选择[禁用自定义域名验证](../../../../administration/pages/index.md#custom-domain-verification)。
> - [DNS 传播可能需要一些时间（最长 24 小时）](https://www.inmotionhosting.com/support/domain-names/dns-nameserver-changes/complete-guide-to-dns-records/)，虽然通常几分钟就可以完成。在此之前，验证失败，并且尝试访问您的域名会导致 404。
> - 验证您的域名后，请保留验证记录。您的域名会定期重新验证，如果记录被删除，您的域名可能会被禁用。

### 添加更多域名别名

您可以为同一个项目添加多个别名（自定义域名和子域名）。
别名可以理解为有许多门通向同一个房间。

您为网站设置的所有别名都列在 **设置 > Pages** 中。
在该页面中，您可以查看、添加和删除它们。

<!--
### Redirecting `www.domain.com` to `domain.com` with Cloudflare

If you use Cloudflare, you can redirect `www` to `domain.com`
without adding both `www.domain.com` and `domain.com` to GitLab.

To do so, you can use Cloudflare's page rules associated to a
`CNAME` record to redirect `www.domain.com` to `domain.com`. You
can use the following setup:

1. In Cloudflare, create a DNS `A` record pointing `domain.com` to `35.185.44.232`.
1. In GitLab, add the domain to GitLab Pages and get the verification code.
1. In Cloudflare, create a DNS `TXT` record to verify your domain.
1. In GitLab, verify your domain.
1. In Cloudflare, create a DNS `CNAME` record pointing `www` to `domain.com`.
1. In Cloudflare, add a Page Rule pointing `www.domain.com` to `domain.com`:
   - Navigate to your domain's dashboard and select **Page Rules**
     on the top nav.
   - Select **Create Page Rule**.
   - Enter the domain `www.domain.com` and select **+ Add a Setting**.
   - From the dropdown menu, choose **Forwarding URL**, then select the
     status code **301 - Permanent Redirect**.
   - Enter the destination URL `https://domain.com`.
-->

<a id="adding-an-ssltls-certificate-to-pages"></a>

## 向 Pages 添加 SSL/TLS 证书

要使用 GitLab Pages 保护您的自定义域名，您可以选择：

- 使用 [Let's Encrypt 与 Pages 的集成](lets_encrypt_integration.md)，它会自动获取和更新您的 Pages 域名的 SSL 证书。
- 按照以下步骤手动将 SSL/TLS 证书添加到 Pages 网站。

<a id="manual-addition-of-ssltls-certificates"></a>

### 手动添加 SSL/TLS 证书

您可以使用满足以下要求的任何证书：

- 一个可通过自定义域名访问的 Pages 网站。
- **PEM证书**：CA 生成的证书，需要添加到 **证书（PEM）** 字段。
- **中间证书**：（又名根证书），它是标识 CA 的加密钥匙串的一部分。通常它与 PEM 证书结合使用，但在某些情况下您需要手动添加它们。<!--[Cloudflare 证书](https://about.gitlab.com/blog/2017/02/07/setting-up-gitlab-pages-with-cloudflare-certificates/) 就是其中一种情况。-->
- **一个私钥**，它是一个加密的密钥，可以根据您的域名验证您的 PEM。

<!--
For example, [Cloudflare certificates](https://about.gitlab.com/blog/2017/02/07/setting-up-gitlab-pages-with-cloudflare-certificates/)
meet these requirements.
-->

#### 步骤

- 要在添加新域名时添加证书，请转到项目的 **设置 > Pages > 新建域名**，添加域名和证书。
- 要将证书添加到之前添加的域名，请转到您项目的 **设置 > Pages**，找到您的域名，选择 **详情** 和 **编辑** 可以添加证书。

![Pages project - adding certificates](img/add_certificate_to_pages.png)

1. 将 PEM 证书添加到其对应字段。
1. 如果您的证书缺少中间证书，请复制并粘贴根证书（通常可从您的 CA 网站获得）并将其粘贴到与您的 PEM 证书相同的字段。
1. 复制您的私钥并将其粘贴到最后一个字段中。

**不要**在常规文本编辑器中打开证书或加密密钥。始终使用代码编辑器（例如 Sublime Text、Atom、Dreamweaver、Brackets 等）。

## 强制 Pages 网站使用 HTTPS

为了让您网站的访问者更加安全，您可以选择强制 Pages 使用 HTTPS。通过这样做，所有通过 HTTP 访问您的网站的尝试都会通过 301 自动重定向到 HTTPS。

适用于默认域名和您的自定义域名（只要您为其设置了有效证书）。

要启用此设置：

1. 导航到您项目的 **设置 > Pages**。
1. 勾选复选框 **强制 HTTPS（需要有效证书）**。如果您在 Pages 前使用 Cloudflare CDN，请确保将 SSL 连接设置设置为 `full` 而不是 `flexible`。<!--For more details, see the [Cloudflare CDN directions](https://developers.cloudflare.com/ssl/origin-configuration/ssl-modes#h_4e0d1a7c-eb71-4204-9e22-9d3ef9ef7fef).-->

## 故障排除

### 域名验证

要手动验证您是否已正确配置域名验证 `TXT` DNS 条目，您可以在终端中运行以下命令：

```shell
dig _gitlab-pages-verification-code.<YOUR-PAGES-DOMAIN> TXT
```

期望输出：

```plaintext
;; ANSWER SECTION:
_gitlab-pages-verification-code.<YOUR-PAGES-DOMAIN>. 300 IN TXT "gitlab-pages-verification-code=<YOUR-VERIFICATION-CODE>"
```

在某些情况下，添加与您尝试注册的域名相同的验证码会有所帮助。

对于根域名：

| From                                              | DNS 记录 | To                     |
| ------------------------------------------------- | ---------- | ---------------------- |
| `example.com`                                     | `TXT`      | `gitlab-pages-verification-code=00112233445566778899aabbccddeeff` |
| `_gitlab-pages-verification-code.example.com`     | `TXT`      | `gitlab-pages-verification-code=00112233445566778899aabbccddeeff` |

对于子域名：

| From                                              | DNS 记录 | To                     |
| ------------------------------------------------- | ---------- | ---------------------- |
| `www.example.com`                                 | `TXT`      | `gitlab-pages-verification-code=00112233445566778899aabbccddeeff` |
| `_gitlab-pages-verification-code.www.example.com` | `TXT`      | `gitlab-pages-verification-code=00112233445566778899aabbccddeeff` |

