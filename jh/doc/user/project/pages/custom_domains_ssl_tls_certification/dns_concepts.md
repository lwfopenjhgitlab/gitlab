---
type: concepts
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# DNS 记录概览 **(FREE SELF)**

*阅读本文档，简要概述 Pages 范围内的 DNS 记录，适用于 Web 开发的初学者。*

域名系统 (DNS) Web 服务通过将域名（例如 `www.example.com`）转换为计算机用来相互连接的数字 IP 地址（例如 `192.0.2.1`），将访问者路由到网站.

创建 DNS 记录以将（子）域名指向某个位置，该位置可以是 IP 地址或另一个域名。如果您想将 Pages 与您自己的（子）域名一起使用，您需要访问您的域名的注册商控制面板，添加将其指向您的 Pages 站点的 DNS 记录。

请注意，**如何**添加 DNS 记录取决于您的域名所在的服务器。每个控制面板都有自己的地方来做这件事。如果您不是您的域名的管理员，并且无权访问您的注册商，您必须请求您的托管服务的技术支持来为您做这件事。

<!--
为了帮助您，我们收集了一些有关如何为最受欢迎的托管服务执行此操作的说明：
-->
<!-- vale gitlab.Spelling = NO -->

<!--
- [Amazon](https://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html)
- [Bluehost](https://www.bluehost.com/help/article/dns-management-add-edit-or-delete-dns-entries)
- [Cloudflare](https://support.cloudflare.com/hc/en-us/articles/201720164-Creating-a-Cloudflare-account-and-adding-a-website)
- [cPanel](https://documentation.cpanel.net/display/84Docs/Edit+DNS+Zone)
- [DigitalOcean](https://docs.digitalocean.com/products/networking/dns/how-to/manage-records/)
- [DreamHost](https://help.dreamhost.com/hc/en-us/articles/360035516812)
- [Gandi](https://docs.gandi.net/en/domain_names/faq/dns_records.html)
- [Go Daddy](https://www.godaddy.com/help/add-an-a-record-19238)
- [Hostgator](https://www.hostgator.com/help/article/changing-dns-records)
- [Inmotion hosting](https://www.bluehost.com/help/article/dns-management-add-edit-or-delete-dns-entries)
- [Media Temple](https://mediatemple.net/community/products/dv/204403794/how-can-i-change-the-dns-records-for-my-domain)
- [Microsoft](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-2000-server/bb727018(v=technet.10))
- [Namecheap](https://www.namecheap.com/support/knowledgebase/subcategory/2237/host-records-setup/)
-->

<!-- vale gitlab.Spelling = YES -->

<!--
If your hosting service is not listed above, you can just try to
search the web for `how to add dns record on <my hosting service>`.
-->

<a id="a-record"></a>

## `A` 记录

DNS `A` 记录将主机映射到 IPv4 IP 地址。
它将根域名作为 `example.com` 指向主机的 IP 地址 `192.192.192.192`。

示例：

- `example.com` => `A` => `192.192.192.192`

<a id="cname-record"></a>

## `CNAME` 记录

`CNAME` 记录为您的服务器定义了一个规范名称的别名（由 `A` 记录定义的别名）。它将一个子域名指向另一个域名。

示例：

- `www` => `CNAME` => `example.com`

这样，访问 `www.example.com` 的访问者会被重定向到 `example.com`。

<a id="mx-record"></a>

## MX 记录

MX 记录定义用于域名的邮件交换，有助于电子邮件正确到达您的邮件服务器。

示例：

- `MX` => `mail.example.com`

然后您可以为 `users@mail.example.com` 注册电子邮件。

<a id="txt-record"></a>

## `TXT` 记录

`TXT` 记录可以将任意文本与主机或其他名称相关联。一个常见的用途是用于站点验证。

示例：

- `example.com`=> `TXT` => `"google-site-verification=6P08Ow5E-8Q0m6vQ7FMAqAYIDprkVV8fUf_7hZ4Qvc8"`

这样，您可以验证该域名的所有权。

## 全部组合

您可以拥有一个 DNS 记录或多个组合：

- `example.com` => `A` => `192.192.192.192`
- `www` => `CNAME` => `example.com`
- `MX` => `mail.example.com`
- `example.com`=> `TXT` => `"google-site-verification=6P08Ow5E-8Q0m6vQ7FMAqAYIDprkVV8fUf_7hZ4Qvc8"`
