---
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 为极狐GitLab Pages 创建重定向 **(FREE SELF)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/24) in GitLab Pages 1.25.0 and GitLab 13.4 behind a feature flag, disabled by default.
> - [Became enabled by default](https://gitlab.com/gitlab-org/gitlab-pages/-/merge_requests/367) in GitLab 13.5.
-->

在 Pages 中，您可以配置规则，使用 [Netlify 样式](https://docs.netlify.com/routing/redirects/#syntax-for-the-redirects-file) HTTP 重定向，将一个 URL 转发到另一个。

并非所有 [Netlify 提供的特殊选项](https://docs.netlify.com/routing/redirects/redirect-options/)都受支持。

| 功能 | 是否支持？ | 示例 |
| ------- | --------- | ------- |
| [重定向 (`301`, `302`)](#redirects) | **{check-circle}** Yes  | `/wardrobe.html /narnia.html 302`
| [重写 (`200`)](#rewrites)          | **{check-circle}** Yes  | `/* / 200` |
| [Splats](#splats)                      | **{check-circle}** Yes  | `/news/*  /blog/:splat` |
| [Placeholders](#placeholders)          | **{check-circle}** Yes  | `/news/:year/:month/:date /blog-:year-:month-:date.html` |
| 重写 (除 `200` 外)            | **{dotted-circle}** No  | `/en/* /en/404.html 404` |
| 查询参数 | **{dotted-circle}** No | `/store id=:id  /blog/:id  301` |
| 强制 ([shadowing](https://docs.netlify.com/routing/redirects/rewrites-proxies/#shadowing)) | **{dotted-circle}** No | `/app/  /app/index.html  200!` |
| 域名级重定向 | **{dotted-circle}** No | `http://blog.example.com/* https://www.example.com/blog/:splat 301` |
| 按国家或语言重定向 | **{dotted-circle}** No | `/  /anz     302  Country=au,nz` |
| 按角色重定向 | **{dotted-circle}** No | `/admin/*  200!  Role=admin` |

<!--
NOTE:
The [matching behavior test cases](https://gitlab.com/gitlab-org/gitlab-pages/-/blob/master/internal/redirects/matching_test.go)
are a good resource for understanding how GitLab implements rule matching in
detail. Community contributions are welcome for any edge cases that aren't included in
this test suite!
-->

## 创建重定向

要创建重定向，请在 Pages 站点的 `public/` 目录中创建一个名为 `_redirects` 的配置文件。

注意：

- 所有路径必须以正斜杠 `/` 开头。
- 如果未提供[状态码](#http-status-codes)，则应用默认状态代码 `301`。
- `_redirects` 文件具有文件大小限制和每个项目的最大规则数，在实例级别配置。仅处理配置第一个匹配规则的最大值。默认文件大小限制为 64KB，默认最大规则数为 1,000。
- 如果您的 Pages 站点使用默认域名（例如 `namespace.gitlab.io/projectname`），您必须在每个规则前面加上项目名称：

  ```plaintext
  /projectname/wardrobe.html /projectname/narnia.html 302
  ```

- 如果您的 Pages 站点使用[自定义域名](custom_domains_ssl_tls_certification/index.md)，则不需要项目名称前缀。例如，您的自定义域名是 `example.com`，您的 `_redirects` 文件将如下所示：

  ```plaintext
  /wardrobe.html /narnia.html 302
  ```

## 文件覆盖重定向

文件优先于重定向。 如果磁盘上存在文件，Pages 会提供该文件而不是您的重定向。例如，存在 `hello.html` 和 `world.html` 文件，并且 `_redirects` 文件包含以下行，则重定向将被忽略，因为 `hello.html` 存在：

```plaintext
/projectname/hello.html /projectname/world.html 302
```

系统不支持 Netlify 的[强制选项](https://docs.netlify.com/routing/redirects/rewrites-proxies/#shadowing)来更改此行为。

<a id="http-status-codes"></a>

## HTTP 状态码

如果未提供状态代码，则应用默认状态代码 `301`，但您可以明确设置自己的状态代码支持以下 HTTP 代码：

- **301**：永久重定向。
- **302**：临时重定向。
- **200**：成功 HTTP 请求的标准响应。如果存在，Pages 会在 `to` 规则中提供内容，而不更改地址栏中的 URL。

## Redirects

> - 在私有化部署版上启用于 14.6 版本。

要创建重定向，请添加一个包含 `from` 路径、`to` 路径和 [HTTP 状态码](#http-status-codes)的规则：

```plaintext
# 301 permanent redirect
/old/file.html /new/file.html 301

# 302 temporary redirect
/old/another_file.html /new/another_file.html 302
```

## Rewrites

> - 引入于 14.3 版本。
> - 在私有化部署版上默认禁用，功能标志为 [`FF_ENABLE_PLACEHOLDERS`](#feature-flag-for-rewrites)。

当请求与 `from` 匹配时，提供状态码 `200` 以提供 `to` 路径的内容：

```plaintext
/old/file.html /new/file.html 200
```

该状态码可以与 [SPLAT 规则](#splats)结合使用，动态重写 URL。

## Splats

> 引入于 14.3 版本。

在 `from` 路径（称为 splat）中带有星号（`*`）的规则，在请求路径的开头，中部或结尾处匹配任何内容。此示例匹配 `/old/` 之后的任何内容，然后将其重写为 `/new/file.html`：

```plaintext
/old/* /new/file.html 200
```

### Splat placeholders

在规则的 `from` 路径中，通过 `*` 匹配的内容可以使用 `:splat` placeholder 注入到 `to` 路径中：

```plaintext
/old/* /new/:splat 200
```

在此例中，对 `/old/file.html` 的请求，响应 `/new/file.html`，状态码为 `200`。

如果规则的 `from` 路径包含多个 splats，第一个 splat 匹配的值替换 `to` 路径中的任何 `:splat`。

### Splat matching behavior

Splats是“贪婪”的，匹配尽可能多的字符：

```plaintext
/old/*/file /new/:splat/file 301
```

在此示例中，规则重定向 `/old/a/a/b/c/file` 到 `/new/a/a/b/c/file`。

Splats 还匹配空字符串，因此前面的规则将 `/old/file` 重定向到 `/new/file`。

### 将所有请求重写为根 `index.html`

NOTE:
如果您使用[极狐GitLab Pages 与 Let's Encrypt 集成](custom_domains_ssl_tls_certification/lets_encrypt_integration.md)，则必须在添加此规则之前启用它。否则，重定向会破坏 Let's Encrypt 集成。<!--For more details, see
[GitLab Pages issue 649](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/649).-->

Single page applications (SPAs) 通常使用客户端路由执行自己的路由。对于这些应用程序，将*所有*请求重写到根 `index.html` 非常重要，以便 JavaScript 应用程序可以处理路由逻辑。您可以使用 `_redirects` 规则执行此操作，例如：

```plaintext
/* /index.html 200
```

## Placeholders

> 引入于 14.3 版本。

在规则中使用 placeholder 匹配所请求的 URL 的部分，并在重写或重定向到新 URL 时使用这些匹配项。

在 `from` and `to` 路径中，placeholder 的格式为 `:` 字符跟在字母 (`[a-zA-Z]+`) 字符串的后面：

```plaintext
/news/:year/:month/:date/:slug /blog/:year-:month-:date-:slug 200
```

本规则指示 Pages 对于 `/news/2021/08/12/file.html` 的请求，重定向到 `/blog/2021-08-12-file.html`，状态码为 `200`。

### Placeholder matching behavior

与 [splats](#splats) 相比，placeholder 在匹配的内容上更加有限，placeholder 匹配前斜线 `/` 之间的文本，因此请使用 placeholder 匹配单路径。

此外，placeholder 不匹配空字符串。如以下规则**不**匹配请求 URL，例如 `/old/file`：

```plaintext
/old/:path /new/:path
```

## Debug 重定向规则

如果重定向无法正常工作，或者您想检查重定向语法，请访问 `https://[namespace.gitlab.io]/projectName/_redirects`，替换 `namespace.gitlab.io` 域名。`_redirect` 文件不是直接提供的，您的浏览器显示您的重定向规则的编号列表，以及该规则是有效的还是无效的：

```plaintext
11 rules
rule 1: valid
rule 2: valid
rule 3: error: splats are not supported
rule 4: valid
rule 5: error: placeholders are not supported
rule 6: valid
rule 7: error: no domain-level redirects to outside sites
rule 8: error: url path must start with forward slash /
rule 9: error: no domain-level redirects to outside sites
rule 10: valid
rule 11: valid
```

## 与 Netlify 的差异

大多数支持的 `_redirect` 规则的行为相同。但是，有一些较小的差异：

- **所有规则 URL 必须以斜杠开始：**

  Netlify 不需要 URL 以斜线开头：

  ```plaintext
  # Valid in Netlify, invalid in GitLab
  */path /new/path 200
  ```

  极狐GitLab 需要 URL 以斜线开头，与以上规则等效：

  ```plaintext
  # Valid in both Netlify and GitLab
  /old/path /new/path 200
  ```

- **所有 placeholder 值都被填充：**

  Netlify 仅填入 `to` 路径中出现的 placeholder 值：

  ```plaintext
  /old /new/:placeholder
  ```

  向 `/old` 发送请求：

  - Netlify 重定向到 `/new/:placeholder`（带有字面的 `/:placeholder`）。
  - 极狐GitLab 重定向到 `/new/`。

<a id="feature-flag-for-rewrites"></a>

## rewrites 功能标志

FLAG:
重写功能正在开发中，并在默认情况下**禁用**的功能标志后部署。

对于 Omnibus 安装实例，在全局设置中定义 `FF_ENABLE_PLACEHOLDERS` 环境变量。
添加以下行到 `/etc/gitlab/gitlab.rb` 并[重新配置实例](../../../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)：

```ruby
gitlab_pages['env']['FF_ENABLE_PLACEHOLDERS'] = 'true'
```

对于源安装实例，定义 `FF_ENABLE_PLACEHOLDERS` 环境变量，然后[重启极狐GitLab](../../../administration/restart_gitlab.md#installations-from-source)：

```shell
export FF_ENABLE_PLACEHOLDERS="true"
/path/to/pages/bin/gitlab-pages -config gitlab-pages.conf
```
