---
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 探索极狐GitLab Pages **(FREE SELF)**

本文档是探索极狐GitLab Pages 提供的选项和设置的用户指南。

<!--
To familiarize yourself with GitLab Pages first:

- Read an [introduction to GitLab Pages](index.md).
- Learn [how to get started with Pages](index.md#getting-started).
- Learn how to enable GitLab Pages
  across your GitLab instance on the [administrator documentation](../../../administration/pages/index.md).
-->

## 极狐GitLab Pages 要求

简而言之，以下是您在极狐GitLab Pages 中上传网站所需的内容：

1. 实例的域名：用于 Pages 的域名（询问您的管理员）。
1. 极狐GitLab CI/CD：一个 `.gitlab-ci.yml` 文件，在您的仓库的根目录中有一个名为 [`pages`](../../../ci/yaml/index.md#pages) 的特定作业。
1. 站点仓库中名为 `public` 的目录，其中包含要发布的内容。
1. 为项目启用了极狐GitLab Runner。

<!--
## GitLab Pages on GitLab.com

If you are using [GitLab Pages on GitLab.com](#gitlab-pages-on-gitlabcom) to host your website, then:

- The domain name for GitLab Pages on GitLab.com is `gitlab.io`.
- Custom domains and TLS support are enabled.
- Shared runners are enabled by default, provided for free and can be used to
  build your website. If you want you can still bring your own runner.

## Example projects

Visit the [GitLab Pages group](https://gitlab.com/groups/pages) for a complete list of example projects. Contributions are very welcome.
-->

## 自定义错误码 Pages

您可以通过在产物中包含的 `public/` 目录的根目录中分别创建 `403.html` 和 `404.html` 文件，来提供自己的 `403` 和 `404` 错误页面。通常在您项目的根目录，但可能会因您的静态生成器配置而异。

如果是 `404.html` 的情况，有不同的场景。例如：

- 如果您使用项目 Pages（在 `/projectname/` 下提供），并尝试访问 `/projectname/non/existing_file`，Pages 会尝试首先提供 `/projectname/404.html`，然后是 `/404.html`。
- 如果您使用用户/群组 Pages（在 `/` 下提供）并尝试访问 `/non/existing_file`，Pages 会尝试提供 `/404.html`。
- 如果您使用自定义域名并尝试访问 `/non/existing_file`，Pages 仅尝试提供 `/404.html`。

## 重定向

您可以使用 `_redirects` 文件为您的站点配置重定向。要了解更多信息，请阅读[重定向文档](redirects.md)。

## 删除您的页面

要删除您的页面：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > Pages**。
1. 选择 **删除页面**。

<a id="subdomains-of-subdomains"></a>

## 子域名的子域名

在极狐GitLab 实例 (`*.example.io`) 的顶级域名下使用 Pages 时，您不能将 HTTPS 与子域名的子域名一起使用。如果您的命名空间或群组名称包含一个点（例如，`foo.bar`），则域名 `https://foo.bar.example.io` *不*工作。

此限制是由于 [HTTP Over TLS 协议](https://www.rfc-editor.org/rfc/rfc2818#section-3.1)。只要您不将 HTTP 重定向到 HTTPS，HTTP 页面就可以工作。

## 项目和群组中的极狐GitLab Pages

您必须在项目中托管您的极狐GitLab Pages 网站。该项目可以是[私有、内部或公开](../../../user/public_access.md)项目，并且属于[群组](../../group/index.md)或[子组](../../group/subgroups/index.md)。

对于群组网站，该群组必须位于顶层，而不是子组。

对于项目网站，您可以先创建您的项目并在 `http(s)://namespace.example.io/projectname` 下访问它。

## Pages 的特定配置选项

了解如何为特定用例设置极狐GitLab CI/CD。

### `.gitlab-ci.yml` 用于纯 HTML 网站

假设您的仓库包含以下文件：

```plaintext
├── index.html
├── css
│   └── main.css
└── js
    └── main.js
```

然后下面的 `.gitlab-ci.yml` 示例简单地将所有文件从项目的根目录移动到 `public/` 目录。`.public` 的解决方法是，`cp` 不会在无限循环中将 `public/` 复制到自身：

```yaml
pages:
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - main
```

### `.gitlab-ci.yml` 用于静态站点生成器

<!--
请参阅此文档以获取[分步指南](getting_started/pages_from_scratch.md)。
-->

### `.gitlab-ci.yml` 用于包含实际代码的仓库

请记住，Pages 默认情况下与分支/标签无关，它们的部署仅依赖于您在 `.gitlab-ci.yml` 中指定的内容。每当新的提交被推送到专门用于您的页面的分支时，您可以使用 [`only` 参数](../../../ci/yaml/index.md#only--except) 限制 `pages` 作业。

这样，您可以将项目代码放在 `main` 分支中，并使用孤立分支（我们将其命名为 `pages`）来托管您的静态生成器站点。

您可以像这样创建一个新的空分支：

```shell
git checkout --orphan pages
```

在这个新分支上进行的第一次提交没有上级分支，并且是与所有其它分支和提交完全断开的新历史的根分支。
将静态生成器的源文件推送到 `pages` 分支。

下面是 `.gitlab-ci.yml` 的副本，其中最重要的是最后一行，指定执行 `pages` 分支中的所有内容：

```yaml
image: ruby:2.6

pages:
  script:
    - gem install jekyll
    - jekyll build -d public/
  artifacts:
    paths:
      - public
  only:
    - pages
```

<!--
See an example that has different files in the [`main` branch](https://gitlab.com/pages/jekyll-branched/tree/main)
and the source files for Jekyll are in a [`pages` branch](https://gitlab.com/pages/jekyll-branched/tree/pages) which
also includes `.gitlab-ci.yml`.
-->

### 提供压缩的 assets

大多数现代浏览器都支持以压缩格式下载文件，通过减小文件大小来加快下载速度。

在提供未压缩文件之前，Pages 会检查是否存在扩展名为 `.br` 或 `.gz` 的相同文件。如果是这样，并且浏览器支持接收压缩文件，它会提供该版本而不是未压缩的版本。

要利用此功能，您上传到 Pages 的产物应具有以下结构：

```plaintext
public/
├─┬ index.html
│ | index.html.br
│ └ index.html.gz
│
├── css/
│   └─┬ main.css
│     | main.css.br
│     └ main.css.gz
│
└── js/
    └─┬ main.js
      | main.js.br
      └ main.js.gz
```

这可以通过在 `.gitlab-ci.yml` 页面作业中包含这样的 `script:` 命令来实现：

```yaml
pages:
  # Other directives
  script:
    # Build the public/ directory first
    - find public -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec gzip -f -k {} \;
    - find public -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec brotli -f -k {} \;
```

通过预压缩文件并在产物中包含这两个版本，Pages 可以为压缩和未压缩内容的请求提供服务，而无需按需压缩文件。

### 解析不明确的 URL

当接收到不包含扩展名的 URL 请求时，Pages 会假设要提供哪些文件。

例如使用以下文件部署的 Pages 站点：

```plaintext
public/
├── index.html
├── data.html
├── info.html
├── data/
│   └── index.html
└── info/
    └── details.html
```

Pages 支持通过多个不同的 URL 访问这些文件中的每一个。特别是，如果 URL 仅指定目录，它总是会查找 `index.html` 文件。如果 URL 引用了一个不存在的文件，但将 `.html` 添加到 URL 会导致文件*确实*存在，则它会被提供。以下是给定上述 Pages 网站的一些示例：

| URL 路径               | HTTP 响应         |
| -------------------- | ------------- |
| `/`                  | `200 OK`: `public/index.html` |
| `/index.html`        | `200 OK`: `public/index.html` |
| `/index`             | `200 OK`: `public/index.html` |
| `/data`              | `302 Found`: 重定向到 `/data/` |
| `/data/`             | `200 OK`: `public/data/index.html` |
| `/data.html`         | `200 OK`: `public/data.html` |
| `/info`              | `302 Found`: 重定向到 `/info/` |
| `/info/`             | `404 Not Found` 错误页面 |
| `/info.html`         | `200 OK`: `public/info.html` |
| `/info/details`      | `200 OK`: `public/info/details.html` |
| `/info/details.html` | `200 OK`: `public/info/details.html` |

请注意，当 `public/data/index.html` 存在时，对于 `/data` 和 `/data/` URL 路径，它优先于 `public/data.html` 文件。

<!--
## Known issues

For a list of known issues, visit the GitLab [public issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name[]=Category%3APages).
-->

## 故障排除

### 访问 Pages 站点 URL 时出现 404 错误

这个问题很可能是由于 public 目录中缺少 `index.html` 文件造成的。如果在部署 Pages 站点后遇到 404，请确认 public 目录包含 `index.html` 文件。如果文件包含不同的名称，例如 `test.html`，仍然可以访问 Pages 站点，但需要完整路径。例如：`https//group-name.pages.example.com/project-name/test.html`。

Public 目录的内容可以通过从最新的流水线中[浏览产物](../../../ci/jobs/job_artifacts.md#download-job-artifacts)来确认。

可以通过项目的 Pages URL 访问 public 目录下列出的文件。

404 也可能与不正确的权限有关。如果启用了 [Pages 访问控制](pages_access_control.md)，并且用户导航到 Pages URL 并收到 404 响应，则用户可能无权查看该站点。
要解决此问题，请验证用户是否是项目的成员。

### 无法在 Safari 上播放媒体内容

Safari 需要网络服务器支持 [Range 请求标头](https://developer.apple.com/library/archive/documentation/AppleApplications/Reference/SafariWebContent/CreatingVideoforSafarioniPhone/CreatingVideoforSafarioniPhone.html#//apple_ref/doc/uid/TP40006514-SW6)，来播放您的媒体内容。对于 Pages 服务的 HTTP Range 请求，您应该在 `.gitlab-ci.yml` 文件中使用以下两个变量：

```yaml
pages:
  stage: deploy
  variables:
    FF_USE_FASTZIP: "true"
    ARTIFACT_COMPRESSION_LEVEL: "fastest"
  script:
    - echo "Deploying pages"
  artifacts:
    paths:
      - public
  environment: production
```

`FF_USE_FASTZIP` 变量启用了 [`ARTIFACT_COMPRESSION_LEVEL`](../../../ci/runners/configure_runners.md#artifact-and-cache-settings)。
