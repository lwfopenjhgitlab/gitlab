---
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Pages 访问控制 **(FREE SELF)**

如果您的管理员在您的实例上[启用了访问控制功能](../../../administration/pages/index.md#access-control)，您可以在您的项目上启用 Pages 访问控制。启用后，只有经过验证的您项目的成员（至少是访客）可以访问您的网站：

1. 导航到您项目的 **设置 > 通用** 并展开 **可见性、项目功能、权限**。

1. 切换 **Pages** 按钮启用访问控制。如果您没有看到切换按钮，则表示它未启用，请询问您的管理员[启用它](../../../administration/pages/index.md#access-control)。

1. Pages 访问控制下拉列表允许您设置谁可以查看由 Pages 托管的页面，具体取决于您的项目的可见性：

   - 如果您的项目是私有的：
     - **仅限项目成员**：只有项目成员才能浏览网站。
     - **所有人**：所有人无论是登录还是退出极狐GitLab，都可以浏览网站，无论他们的项目成员身份如何。
   - 如果您的项目是内部的：
     - **仅限项目成员**：只有项目成员才能浏览网站。
     - **具有访问权限的任何人**：登录极狐GitLab 的每个人都可以浏览该网站，无论他们的项目成员身份如何。
     - **所有人**：登录和退出极狐GitLab 的每个人都可以浏览该网站，无论他们的项目成员身份如何。
   - 如果您的项目是公开的：
     - **仅限项目成员**：只有项目成员才能浏览网站。
     - **具有访问权限的任何人**：登录和退出极狐GitLab 的每个人都可以浏览该网站，无论他们的项目成员身份如何。

1. 选择 **保存修改**。请注意，您的更改可能不会立即生效。Pages 使用缓存机制来提高效率。在缓存失效之前，您的更改可能不会生效，这通常需要不到一分钟的时间。

下次有人尝试访问您的网站并启用访问控制时，他们会看到一个页面，可以登录极狐GitLab 并验证他们可以访问该网站。

## 终止 Pages 会话

要退出 Pages 网站，请撤销 GitLab Pages 的应用程序访问令牌：

1. 在顶部菜单中，选择您的个人资料，然后选择 **设置**。
1. 在左侧边栏中，选择 **应用程序**。
1. 滚动到 **授权应用** 部分，找到 **GitLab Pages** 条目，然后选择其 **撤销** 按钮。
