---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 徽章 **(FREE)**

徽章是一种统一的方式，来呈现有关您的项目的浓缩信息。
徽章由一个小图片和图片指向的 URL 组成。
在极狐GitLab 中，徽章显示在项目描述下方。
您可以在[项目](#project-badges)和[群组](#group-badges)级别使用徽章。

![Badges on Project information page](img/project_overview_badges_v13_10.png)

## 可用的徽章

极狐GitLab 提供以下流水线徽章：

- [流水线状态徽章](#pipeline-status-badges)
- [测试覆盖率报告徽章](#test-coverage-report-badges)
- [最新发布徽章](#latest-release-badges)

极狐GitLab 还支持[自定义徽章](#customize-badges)。

<a id="pipeline-status-badges"></a>

## 流水线状态徽章

流水线状态徽章表示项目中最新流水线的状态。
根据流水线的状态，徽章可以具有以下值之一：

- `pending`
- `running`
- `passed`
- `failed`
- `skipped`
- `manual`
- `canceled`
- `unknown`

您可以使用以下链接访问流水线状态徽章图片：

```plaintext
https://gitlab.example.com/<namespace>/<project>/badges/<branch>/pipeline.svg
```

### 仅显示非跳过状态

要使流水线状态标志仅显示最后一个未跳过的状态，请使用 `?ignore_skipped=true` 查询参数：

```plaintext
https://gitlab.example.com/<namespace>/<project>/badges/<branch>/pipeline.svg?ignore_skipped=true
```

## 测试覆盖率报告徽章

测试覆盖率报告徽章表示项目中测试的代码百分比。
该值是根据最新成功的流水线计算得出的。

您可以使用以下链接访问测试覆盖率报告徽章图片：

```plaintext
https://gitlab.example.com/<namespace>/<project>/badges/<branch>/coverage.svg
```

您可以为每个作业日志匹配的[覆盖率报告](../../ci/testing/code_coverage.md#view-code-coverage-results-in-the-mr)定义正则表达式。
这意味着流水线中的每个作业都可以定义测试覆盖百分比值。

要从特定作业获取覆盖率报告，请将 `job=coverage_job_name` 参数添加到 URL。
例如，您可以使用类似于以下的代码将 `coverage` 作业的测试覆盖率报告徽章添加到 Markdown 文件中：

```markdown
![coverage](https://gitlab.example.com/<namespace>/<project>/badges/<branch>/coverage.svg?job=coverage)
```

### 测试覆盖率报告徽章颜色和限制

徽章的默认颜色和限制如下：

- 95 - 100% - 好 (`#4c1`)
- 90 - 95% - 可接受 (`#a3c51c`)
- 75 - 90% - 中等 (`#dfb317`)
- 0 - 75% - 低 (`#e05d44`)
- 无覆盖率 - 未知 (`#9f9f9f`)

您可以使用以下附加参数覆盖限制（引入于 14.4 版本）：

- `min_good`（默认 95，可以使用 3 到 100 之间的任何值）
- `min_acceptable`（默认 90，可以使用 2 和 min_good-1 之间的任何值）
- `min_medium`（默认 75，可以使用 1 到 min_acceptable-1 之间的任何值）

如果设置了无效边界，极狐GitLab 会自动将其调整为有效边界。例如，`min_good` 设置为 `80`，并且 `min_acceptable` 设置为 `85`（太高），极狐GitLab 会自动将 `min_acceptable` 设置为 `79` (`min_good` - `1`)。

## 最新发布徽章

> 引入于 14.8 版本。

最新发布徽章表示您的项目的最新发布标签名称。
如果没有发布，则显示“无”。

您可以使用以下链接访问最新发布徽章的图片：

```plaintext
https://gitlab.example.com/<namespace>/<project>/-/badges/release.svg
```

默认情况下，徽章获取使用 [`released_at`](../../api/releases/index.md#create-a-release) 时间和 `?order_by` 查询参数排序的版本。

```plaintext
https://gitlab.example.com/<namespace>/<project>/-/badges/release.svg?order_by=release_at
```

您可以使用 `value_width` 参数更改版本名称字段的宽度（引入于 15.10 版本）。
该值必须在 1 到 200 之间，默认值为 54。
如果您设置的值超出范围，极狐GitLab 会自动将其调整为默认值。

<a id="project-badges"></a>

## 项目徽章

徽章可以由维护者或所有者添加到项目中，并且在项目的概览页面上可见。
如果您发现必须为多个项目添加相同的徽章，您可能需要在[群组级别](#group-badges)添加它们。

### 向项目添加徽章

为项目添加新徽章：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **徽章**。
1. 在 **链接** 下，输入徽章应指向的 URL，在 **徽章图片网址** 下，输入应显示的图片的 URL。
1. 选择 **添加徽章**。

为项目添加徽章后，您可以在表单下方的列表中看到徽章。

### 编辑或删除项目徽章

要编辑徽章，请选择 **编辑** (**{pencil}**)。

要删除徽章，请选择 **删除** (**{remove}**)。

### 示例项目徽章：流水线状态

一个常见的项目徽章表示极狐GitLab CI 流水线状态。

将此徽章添加到项目中：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **徽章**。
1. 在 **名称** 下，输入 *流水线状态*。
1. 在 **链接** 下，输入以下 URL：`https://jihulab.com/%{project_path}/-/commits/%{default_branch}`
1. 在 **徽章图片网址** 下，输入以下 URL：`https://jihulab.com/%{project_path}/badges/%{default_branch}/pipeline.svg`
1. 选择 **添加徽章**。

<a id="group-badges"></a>

## 群组徽章

通过向群组添加徽章，您可以为群组中的所有项目添加并强制实施项目级徽章。群组徽章在属于该组的任何项目的**概览**页面上可见。

NOTE:
虽然这些徽章在代码库中显示为项目级别的徽章，但它们不能在项目级别进行编辑或删除。

如果您需要为每个项目提供单独的徽章，则可以：

- 在[项目级别](#project-badges)添加徽章。
- 使用 [placeholders](#placeholders)。

### 向群组添加徽章

要将新徽章添加到群组：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **徽章**。
1. 在“链接”下，输入徽章应指向的 URL，在“徽章图片网址”下输入应显示的图片的 URL。
1. 选择 **添加徽章**。

将徽章添加到群组后，您可以在表单下方的列表中看到它。

### 编辑或删除群组徽章

要编辑徽章，请选择 **编辑** (**{pencil}**)。

要删除徽章，请选择 **删除** (**{remove}**)。

只能在[群组级别](#group-badges)编辑或删除与群组关联的徽章。

## 查看流水线徽章的 URL

您可以查看徽章的链接。
然后您可以使用该链接，将徽章嵌入到您的 HTML 或 Markdown 页面中。

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 在 **流水线状态**、**覆盖率报告** 或 **最新发布** 部分，查看图片的 URL。

![Pipelines badges](img/pipelines_settings_badges.png)

<a id="customize-badges"></a>

## 自定义徽章

您可以自定义徽章的以下方面：

- 样式
- 文本
- 宽度
- 图片

### 自定义徽章样式

通过将 `style=style_name` 参数添加到 URL，可以以不同的样式呈现流水线徽章。有两种款式可供选择：

- Flat（默认值）：

  ```plaintext
  https://gitlab.example.com/<namespace>/<project>/badges/<branch>/coverage.svg?style=flat
  ```

  ![Badge flat style](https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=coverage&style=flat)

- Flat square：

  ```plaintext
  https://gitlab.example.com/<namespace>/<project>/badges/<branch>/coverage.svg?style=flat-square
  ```

  ![Badge flat square style](https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=coverage&style=flat-square)

### 自定义徽章文本

用户可以自定义徽章的文本，区分在同一流水线中运行的多个覆盖率作业。
通过将 `key_text=custom_text` 和 `key_width=custom_key_width` 参数添加到 URL，来自定义徽章文本和宽度：

```plaintext
https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=karma&key_text=Frontend+Coverage&key_width=130
```

![Badge with custom text and width](https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=karma&key_text=Frontend+Coverage&key_width=130)

### 自定义徽章图片

如果您想使用默认徽章以外的徽章，请在项目或群组中使用自定义徽章图片。

先决条件：

- 直接指向所需徽章图片的有效 URL。如果图片位于极狐GitLab 仓库中，请使用图片的原始链接。

使用 placeholders，以下是一个示例徽章图片 URL，它引用仓库根目录中的原始图片：

```plaintext
https://gitlab.example.com/<project_path>/-/raw/<default_branch>/my-image.svg
```

要将新徽章添加到具有自定义图片的群组或项目：

1. 在顶部栏上，选择 **主菜单** 并找到您的群组或项目。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **徽章**。
1. 在 **名称** 下，输入徽章的名称。
1. 在 **链接** 下，输入徽章应指向的 URL。
1. 在 **徽章图片网址** 下，输入直接指向应显示的自定义图片的 URL。
1. 选择 **添加徽章**。

要了解如何使用通过流水线生成的自定义图片，请参阅[关于通过 URL 访问最新作业产物的文档](../../ci/jobs/job_artifacts.md#from-a-url)。

## Placeholders

徽章指向的 URL 和图片 URL 都可以包含 placeholders。可以使用以下 placeholders：

- `%{project_path}`：项目的路径，包括父组
- `%{project_title}`：项目标题
- `%{project_name}`：项目名称
- `%{project_id}`：与项目关联的数据库 ID
- `%{default_branch}`：为项目仓库配置的默认分支名称
- `%{commit_sha}`：最近提交到项目仓库默认分支的 ID

NOTE:
Placeholders 允许徽章公开其它私有信息，例如当项目配置为具有私有仓库时的默认分支或提交 SHA。这是设计使然，因为徽章旨在公开使用。如果信息敏感，请避免使用这些 placeholders。

## 通过 API 配置徽章

您还可以通过极狐GitLab API 配置徽章。在设置中，[项目级别](../../api/project_badges.md)和[群组级别](../../api/group_badges.md)的徽章端点之间存在区别。
