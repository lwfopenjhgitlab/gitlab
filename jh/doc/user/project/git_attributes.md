---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# Git 属性 **(FREE)**

GitLab 支持定义自定义 [Git 属性](https://git-scm.com/docs/gitattributes)，例如将哪些文件视为二进制文件，以及用于语法突出显示差异的语言。

要定义这些属性，请在存储库的根目录中创建一个名为 `.gitattributes` 的文件，并将其推送到项目的默认分支。

## 编码要求

`.gitattributes` 文件*必须*以 UTF-8 编码并且*不得*包含字节顺序标记。如果使用不同的编码，文件的内容将被忽略。

## 支持混合文件编码

极狐GitLab 尝试自动检测文件的编码，但默认为 UTF-8，除非检测器支持不同的类型（例如 ISO-8859-1）。不正确的编码检测可能导致某些字符不显示在文本中，例如非 UTF-8 编码中的重音字符。

Git 内置支持处理这种情况，并自动在仓库本身的指定编码和 UTF-8 之间转换文件。使用 `working-tree-encoding` 属性在 `.gitattributes` 文件中配置对混合文件编码的支持。

示例：

```plaintext
*.xhtml text working-tree-encoding=ISO-8859-1
```

通过此示例配置，Git 在本地树中以 ISO-8859-1 编码维护仓库中的所有 `.xhtml` 文件，但在提交到仓库时与 UTF-8 相互转换。极狐GitLab 准确地渲染文件，因为它只能处理正确编码的 UTF-8。

如果将此配置应用于现有仓库，如果本地副本具有正确的编码但仓库没有，则可能需要修改并重新提交文件。您可以对整个仓库执行 `git add --renormalize .` 命令。

有关详细信息，请参阅 [working-tree-encoding](https://git-scm.com/docs/gitattributes#_working_tree_encoding) 文档。

## 语法高亮

`.gitattributes` 文件可用于定义在语法高亮文件和差异时使用的语言。<!--有关更多信息，请参阅 ["语法高亮"](highlighting.md)。-->
