---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 项目集成 **(FREE)**

您可以将极狐GitLab 项目与其他应用程序集成。集成就像插件，让您可以自由地向极狐GitLab 添加功能。

## 查看项目集成

先决条件：

- 您必须至少具有项目的维护者角色。

要查看项目的可用集成：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。

您还可以跨[实例或群组中的所有项目](../../admin_area/settings/project_integration_management.md)查看和管理集成设置。
对于单个项目，您可以选择继承实例或群组配置，或提供自定义设置。

NOTE:
基于实例和群组的集成管理取代了服务模板，这些模板在 14.0 版本中已删除。

## 管理 SSL 验证

默认情况下，传出 HTTP 请求的 SSL 证书是根据证书颁发机构的内部列表进行验证的，这意味着证书不能自签名。

您可以在 webhooks 和一些集成的配置设置中关闭 SSL 验证。

<a id="available-integrations"></a>

## 部分可用的集成

单击服务链接，查看更多配置说明和详细信息。

| 服务                                                   | 描述                                                                                  |  服务钩子          |
| --------------------------------------------------------- | -------------------------------------------------------------------------------------------- | ---------------------- |
| [禅道](zentao.md)                                   | 使用禅道作为议题跟踪器。                                                           | **{dotted-circle}** No |
| [Harbor](harbor.md)                           | 使用 Harbor 作为容器镜像库。 | **{dotted-circle}** No |

## 项目 webhooks

您可以配置项目 webhook 侦听特定事件，例如推送、议题或合并请求。当 webhook 被触发时，极狐GitLab 会向指定的 webhook URL 发送一个带有数据的 POST 请求。

<!--了解更多[关于 webhooks](webhooks.md)。-->

## 推送 hooks 限制

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/17874) in GitLab 12.4.
-->

如果单个推送包含对三个以上分支或标签的更改，则不会执行 `push_hooks` 和 `tag_push_hooks` 事件支持的服务。

可以通过 `push_event_hooks_limit` 应用设置<!--[`push_event_hooks_limit` 应用设置](../../../api/settings.md#list-of-settings-that-can-be-accessed-via-api-calls)-->更改支持的分支或标签的数量。

## 集成故障排除

<!--
Some integrations use hooks to integrate with external applications. To confirm which ones use integration hooks, see the [available integrations](#available-integrations). Learn more about [troubleshooting integration hooks](webhooks.md#troubleshoot-webhooks).
-->

### `Test Failed. Save Anyway` 错误

当您在未初始化的仓库上设置某些集成时，它们会失败并显示错误 `Test Failed. Save Anyway`。出现此错误是因为集成使用推送数据构建测试负载，并且项目中没有推送事件。

要解决此错误，请通过将测试文件推送到项目来初始化仓库，并再次设置集成。
