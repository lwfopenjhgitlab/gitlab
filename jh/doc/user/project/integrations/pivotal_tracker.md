---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Pivotal Tracker 服务 **(FREE)**

该服务将提交消息作为评论添加到 Pivotal Tracker 故事中。

启用后，将检查提交消息是否包含方括号，其中包含哈希标记，后跟故事 ID（例如，`[#555]`）。找到的每个故事 ID 都会添加提交评论。

您还可以使用包含以下内容的消息关闭故事：`fix [#555]`。
您可以使用以下任何词：

- `fix`
- `fixed`
- `fixes`
- `complete`
- `completes`
- `completed`
- `finish`
- `finished`
- `finishes`
- `delivers`

在 Pivotal Tracker API 文档中阅读有关 [Source Commits 端点](https://www.pivotaltracker.com/help/api/rest/v5#Source_Commits)的更多信息。

<!--
See also the [Pivotal Tracker integration API documentation](../../../api/integrations.md#pivotal-tracker).
-->

## 设置 Pivotal Tracker

在 Pivotal Tracker 中，[创建 API 令牌](https://www.pivotaltracker.com/help/articles/api_token/)。

在极狐GitLab 中完成以下步骤：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **Pivotal Tracker**。
1. 确保打开了 **启用** 切换开关。
1. 粘贴您在 Pivotal Tracker 中生成的令牌。
1. 可选。要将此设置限制为特定分支，请在 **仅限于分支** 字段中列出它们，用逗号分隔。
1. 选择 **保存更改** 或选择 **测试设置**（可选）。
