---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Bugzilla 服务 **(FREE)**

[Bugzilla](https://www.bugzilla.org/) 是一个基于 Web 的通用错误跟踪系统和测试工具。

您可以将 Bugzilla 配置为极狐GitLab 中的外部议题跟踪器。

要在项目中启用 Bugzilla 集成：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **Bugzilla**。
1. 选中 **启用集成** 下的复选框。
1. 填写必填字段：

   - **项目 URL**：Bugzilla 中项目的 URL。例如，对于名为“Fire Tanuki”的产品：`https://bugzilla.example.org/describecomponents.cgi?product=Fire+Tanuki`。
   - **议题 URL**: 在 Bugzilla 项目中查看议题的 URL。URL 必须包含 `:id`。极狐GitLab 将 `:id` 替换为议题编号（例如，`https://bugzilla.example.org/show_bug.cgi?id=:id`，变为 `https://bugzilla.example.org/show_bug.cgi?id=123`）。
   - **新建议题 URL**：在链接的 Bugzilla 项目中创建新议题的 URL。例如，对于名为“My Cool App”的项目：`https://bugzilla.example.org/enter_bug.cgi#h=dupes%7CMy+Cool+App`。

1. 选择 **保存修改** 或 **测试设置**。

配置并启用 Bugzilla 后，极狐GitLab 项目页面上会显示一个链接。此链接将您带到相应的 Bugzilla 项目。

您还可以在此项目中禁用[极狐GitLab 内部议题跟踪](../issues/index.md)。

<!--
在[共享和权限](../settings/index.md#configure-project-visibility-features-and-permissions)中了解有关禁用极狐GitLab 议题的步骤和后果等更多信息。
-->

## 在极狐GitLab 中引用 Bugzilla 议题

您可以使用以下方法引用 Bugzilla 中的议题：

- `#<ID>`，其中 `<ID>` 是一个数字（例如，`#143`）。
- `<PROJECT>-<ID>`（例如 `API_32-143`）其中：
   - `<PROJECT>` 以大写字母开头，后跟大写字母、数字或下划线。
   - `<ID>` 是一个数字。

链接中的 `<PROJECT>` 部分被忽略，链接始终指向 **议题 URL** 中指定的地址。

如果您同时启用了内部和外部议题跟踪器，我们建议使用较长的格式（`<PROJECT>-<ID>`）。如果您使用较短的格式，并且内部议题跟踪器中存在具有相同 ID 的议题，则会链接内部议题。

<!--
## Troubleshooting

To see recent service hook deliveries, check [service hook logs](index.md#troubleshooting-integrations).
-->
