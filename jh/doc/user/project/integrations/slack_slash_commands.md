---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Slack 指令 **(FREE SELF)**

如果您想在使用 Slack 时控制和查看极狐GitLab 内容，可以使用 Slack 指令。
要使用 Slack 指令，您必须同时配置 Slack 和极狐GitLab。

<!--
极狐GitLab 还可以将事件（例如，`issue created`）作为通知发送到 Slack。
[Slack 通知服务](slack.md)是单独配置的。

NOTE:
For GitLab.com, use the [GitLab Slack app](gitlab_slack_application.md) instead.
-->

## 配置极狐GitLab 和 Slack

Slack 指令集成的范围是一个项目。

1. 在极狐GitLab 的顶部栏中，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **Slack 指令**。 保持此浏览器选项卡处于打开状态。
1. 打开一个新的浏览器选项卡，登录您的 Slack 团队，然后[开始新的指令集成](https://my.slack.com/services/new/slash-commands)。
1. 输入触发命令。我们建议您使用项目名称。选择 **添加指令集成**。
1. 使用极狐GitLab 浏览器选项卡中的信息完成 Slack 配置页面中的其余字段。特别是，请确保复制并粘贴 **URL**。

   ![Slack setup instructions](img/slack_setup.png)

1. 在 Slack 配置页面，选择 **保存集成** 并复制 **令牌**。
1. 回到极狐GitLab 配置页面，粘贴 **令牌**。
1. 确保选中 **启用** 复选框并选择 **保存修改**。

## 指令

您现在可以使用可用的 [Slack 指令](../../../integration/slash_commands.md)。
