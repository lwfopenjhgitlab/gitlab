---
stage: Manage
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Apple App Store **(FREE)**

> - 引入于 15.8 版本，[功能标志](../../../administration/feature_flags.md)为 `apple_app_store_integration`。默认禁用。
> - 一般可用于 15.10 版本。功能标志 `apple_app_store_integration` 删除。

通过 Apple App Store 集成，您可以配置 CI/CD 流水线连接到 [App Store Connect](https://appstoreconnect.apple.com)，从而为 iOS、iPadOS、macOS、tvOS 和 watchOS 构建和发布应用程序。

该集成旨在能够与 [fastlane](http://fastlane.tools/) 开箱即用，但也可以与其他构建工具一起使用。

## 先决条件

启用此集成需要在 [Apple Developer Program](https://developer.apple.com/programs/enroll/) 中注册的 Apple ID。

## 配置极狐GitLab

极狐GitLab 支持在项目级别启用 Apple App Store 集成。在极狐GitLab 中完成以下步骤：

1. 在 Apple App Store Connect 门户中，按照[说明文档](https://developer.apple.com/documentation/appstoreconnectapi/creating_api_keys_for_app_store_connect_api)，为您的项目生成一个新的私钥。
1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **设置 > 集成**。
1. 选择 **Apple App Store**。
1. 打开 **启用集成** 下的 **启用** 开关。
1. 提供 Apple App Store Connect 配置信息：
   - **Issuer ID**：Apple App Store Connect Issuer ID 可在 Apple App Store Connect 门户的*用户和访问*下的*密钥*部分找到。
   - **Key ID**：刚刚生成的新私钥的 Key ID。
   - **Private Key**：刚刚生成的私钥。注意：您只能下载此密钥一次。

1. 选择 **保存更改**。

启用 Apple App Store 集成后：

- 全局变量 `$APP_STORE_CONNECT_API_KEY_ISSUER_ID`、`$APP_STORE_CONNECT_API_KEY_KEY_ID` 和`$APP_STORE_CONNECT_API_KEY_KEY` 是为 CI/CD 使用而创建的。
- `$APP_STORE_CONNECT_API_KEY_KEY` 包含 Base64 编码的私钥。

## 安全注意事项

### CI/CD 变量安全

推送到您的 `.gitlab-ci.yml` 文件的恶意代码可能会破坏您的变量，包括 `$APP_STORE_CONNECT_API_KEY_KEY`，并将它们发送到第三方服务器。有关详细信息，请参阅 [CI/CD 变量安全](../../../ci/variables/index.md#cicd-variable-security)。

## fastlane 示例

因为这种集成与 fastlane 开箱即用，所以将下面的代码添加到应用程序的 `fastlane/Fastfile` 会启用集成，并为与 Apple App Store 上传 Test Flight 或公共 App Store 版本的任何交互创建连接。

```ruby
app_store_connect_api_key(
  is_key_content_base64: true
)
```
