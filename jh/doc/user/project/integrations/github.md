---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# GitHub 项目集成 **(PREMIUM)**

您可以使用来自 GitLab 的流水线状态更新来更新 GitHub。
如果您将极狐GitLab 用于 CI/CD，此集成可为您提供帮助。

![Pipeline status update on GitHub](img/github_status_check_pipeline_update.png)

此项目集成独立于实例范围的 GitHub 集成，并在您导入 GitHub 项目时自动配置。

## 配置集成

此集成需要带有已授予 `repo:status` 访问权限的 [GitHub API 令牌](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token)。

在 GitHub 上完成以下步骤：

1. 转到 <https://github.com/settings/tokens> 上的 **个人访问令牌** 页面。
1. 选择 **生成新令牌**。
1. 在 **Note** 下，输入新令牌的名称。
1. 确保选中 `repo:status` 并选择 **Generate token**。
1. 复制生成的令牌，后续在极狐GitLab 中使用。

在极狐GitLab 上完成以下步骤：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **GitHub**。
1. 确保选中 **启用** 复选框。
1. 在 **令牌** 中，粘贴在 GitHub 上生成的 token。
1. 在 **仓库 URL** 中，输入您在 GitHub 上的项目的路径，例如 `https://github.com/username/repository`。
1. 可选。要禁用[静态状态检查名称](#静态或动态状态检查名称)，请清除 **启用静态状态检查名称** 复选框。
1. 可选。选择 **测试设置**。
1. 选择 **保存修改**。

<!--
After configuring the integration, see [Pipelines for external pull requests](../../../ci/ci_cd_for_external_repos/#pipelines-for-external-pull-requests)
to configure pipelines to run for open pull requests.
-->
配置集成后，查看[外部拉取请求流水线](../../../ci/ci_cd_for_external_repos/#外部拉取请求流水线)，配置流水线针对开放的拉取请求运行。

<a id="static-or-dynamic-status-check-names"></a>

### 静态或动态状态检查名称

<!--
> [Changed](https://gitlab.com/gitlab-org/gitlab/-/issues/9931) in GitLab 12.4 to make static status check names the default behavior for new projects.
-->

状态检查名称可以是静态的或动态的：

- **静态**：极狐GitLab 实例的主机名附加到状态检查名称。

- **动态**：分支名称附加到状态检查名称。

**启用静态状态检查名称**选项使您能够在 GitHub 中配置所需的状态检查，需要一致的（静态）名称才能正常工作。

如果您[禁用此选项](#配置集成)，极狐GitLab 将使用动态状态检查名称。
