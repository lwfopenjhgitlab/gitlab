---
stage: Create
group: Incubation
info: Machine Learning Experiment Tracking is a GitLab Incubation Engineering program. No technical writer assigned to this group.
---

# MLFlow 客户端集成 **(FREE)**

> 引入于 15.11 版本，[功能标志](../../../administration/feature_flags.md)为 `ml_experiment_tracking`。作为实验功能，默认禁用。

<!--
NOTE:
Model experiment tracking is an [experimental feature](../../../policy/alpha-beta-support.md).
Refer to <https://gitlab.com/gitlab-org/gitlab/-/issues/381660> for feedback and feature requests.
-->

[MLFlow](https://mlflow.org/) 是最流行的机器学习实验跟踪开源工具之一。极狐GitLab 作为 MLFlow 客户端的后端，[记录实验](../ml/experiment_tracking/index.md)。
设置您的集成需要对现有代码进行少量更改。

极狐GitLab 扮演代理服务器的角色，既用于产物存储，也用于跟踪数据，参考 MLFlow [场景 5](https://www.mlflow.org/docs/latest/tracking.html#scenario-5-mlflow-tracking-server-enabled-with-proxied-artifact-storage-access)。

## 启用 MLFlow 客户端集成

先决条件：

- 项目的[个人访问令牌](../../../user/profile/personal_access_tokens.md)，最低访问级别为 `api`。
- 项目 ID。要查找项目 ID，请在顶部栏中选择 **主菜单 > 项目** 并找到您的项目。在左侧边栏中，选择 **设置 > 通用**。

要启用 MLFlow 客户端集成：

1. 在运行代码的主机上设置跟踪 URI 和令牌环境变量，可以是您的本地环境、CI 流水线或远端主机。例如：

   ```shell
   export MLFLOW_TRACKING_URI="http://<your gitlab endpoint>/api/v4/projects/<your project id>/ml/mlflow"
   export MLFLOW_TRACKING_TOKEN="<your_access_token>"
   ```

1. 如果您的训练代码包含对 `mlflow.set_tracking_uri()` 的调用，请将其删除。

运行训练代码时，MLFlow 在极狐GitLab 上创建实验、运行、日志参数、指标、元数据和产物。

记录实验后，它们列在 `/<your project>/-/ml/experiments` 下。每次运行注册为：

- 模型候选项，可以通过选择一个实验来探索。
- 标签，注册为元数据。

## 支持的 MlFlow 客户端方法和注意事项

极狐GitLab 支持来自 MLFlow 客户端的以下方法，其他方法可能受支持但未经过测试。更多信息请参考 [MLFlow 文档](https://www.mlflow.org/docs/1.28.0/python_api/mlflow.html)。

| 方法                   | 是否受支持        | 添加版本  | 备注 |
|--------------------------|------------------|----------------|----------|
| `get_experiment`         | 是              | 15.11          |   |
| `get_experiment_by_name` | 是              | 15.11          |   |
| `set_experiment`         | 是              | 15.11          |   |
| `get_run`                | 是              | 15.11          |   |
| `start_run`              | 是              | 15.11          |   |
| `log_artifact`           | 是，有限制  | 15.11          | (15.11) `artifact_path` 必须为空字符串。不支持目录。 |
| `log_artifacts`          | 是，有限制  | 15.11          | (15.11) `artifact_path` 必须为空字符串。不支持目录。 |
| `log_batch`              | 是              | 15.11          |   |
| `log_metric`             | 是              | 15.11          |   |
| `log_metrics`            | 是              | 15.11          |   |
| `log_param`              | 是              | 15.11          |   |
| `log_params`             | 是              | 15.11          |   |
| `log_figure`             | 是              | 15.11          |   |
| `log_image`              | 是              | 15.11          |   |
| `log_text`               | 是，有限制  | 15.11          | (15.11) 不支持目录。 |
| `log_dict`               | 是，有限制  | 15.11          | (15.11) 不支持目录。 |
| `set_tag`                | 是              | 15.11          |   |
| `set_tags`               | 是              | 15.11          |   |
| `set_terminated`         | 是              | 15.11          |   |
| `end_run`                | 是              | 15.11          |   |
| `update_run`             | 是              | 15.11          |   |
| `log_model`              | 部分支持          | 15.11          | (15.11) 保存产物，但不保存模型数据。`artifact_path` 必须为空。 |

## 限制

- 极狐GitLab 支持的 API 是在 MLFlow 版本 1.28.0 中定义的 API。
- 不支持上面未列出的 API 端点。
- 在创建实验和运行期间，ExperimentTags 被存储，即使它们没有显示。
- 不支持 MLFlow 模型库。
