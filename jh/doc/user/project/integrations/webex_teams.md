---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Webex Teams 服务 **(FREE)**

您可以将极狐GitLab 配置为向 Webex Teams 空间发送通知：

1. 为空间创建一个 webhook。
1. 将 webhook 添加到极狐GitLab。

## 为空间创建一个 webhook

1. 转到[传入 Webhooks 应用程序页面](https://apphub.webex.com/applications/incoming-webhooks-cisco-systems-38054-23307)。
1. 如果需要，选择 **Connect** 并登录 Webex Teams。
1. 输入 webhook 的名称并选择接收通知的空间。
1. 选择 **ADD**。
1. 复制 **Webhook URL**。

## 在极狐GitLab 中配置设置

获得 Webex Teams 空间的 Webhook URL 后，您可以配置极狐GitLab 来发送通知：

1. 导航至：
    - 在项目中的 **设置 > 集成**，启用项目级别的集成。
    - 在群组中的 **设置 > 集成**，启用群组级别的集成。
    - 在顶部栏上，选择 **菜单 > 管理员**。然后，在左侧边栏中，选择 **设置 > 集成** 来启用实例级集成。
1. 选择 **Webex Teams** 集成。
1. 确保打开了 **启用** 切换开关。
1. 选中您希望在 Webex Teams 中接收的极狐GitLab 事件对应的复选框。
1. 粘贴 Webex Teams 空间的 **Webhook** URL。
1. 配置其余选项，然后选择 **测试设置并保存更改**。

Webex Teams 空间开始接收所有适用的极狐GitLab 事件。
