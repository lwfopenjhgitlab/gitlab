---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# IBM Engineering Workflow Management (EWM) 集成 **(FREE)**

此服务允许您从极狐GitLab 跳转到合并请求描述和提交消息中提到的 EWM 工作项。
每个工作项引用都会自动转换为工作项的链接。

这个 IBM 产品[原名为 Rational Team Concert](https://jazz.net/blog/index.php/2019/04/23/renaming-the-ibm-continuous-engineering-portfolio/)(RTC)。这种集成与所有版本的 RTC 和 EWM 兼容。

要启用 EWM 集成，请在项目中：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **EWM**。
1. 选中 **启用集成** 下的复选框。
1. 填写必填字段：

   - **项目 URL**：EWM 项目区域的 URL。

     要获取您的项目区域 URL，请导航到路径 `/ccm/web/projects` 并复制列出的项目的 URL。例如，`https://example.com/ccm/web/Example%20Project`。
   - **议题 URL**：EWM 项目区域中工作项编辑器的 URL。

     格式为`<your-server-url>/resource/itemName/com.ibm.team.workitem.WorkItem/:id`。极狐GitLab 将 `:id` 替换为议题编号（例如，`https://example.com/ccm/resource/itemName/com.ibm.team.workitem.WorkItem/:id`，变为 `https:// example.com/ccm/resource/itemName/com.ibm.team.workitem.WorkItem/123`）。
   - **新建议题 URL**：在 EWM 项目区域中创建新工作项的 URL。

     将以下片段附加到您的项目区域 URL：`#action=com.ibm.team.workitem.newWorkItem`。例如，`https://example.com/ccm/web/projects/JKE%20Banking#action=com.ibm.team.workitem.newWorkItem`。

1. 选择 **保存更改** 或可选地选择 **测试设置**。

## 在提交消息中引用 EWM 工作项

要引用工作项，您可以使用 EWM Git 集成工具包支持的任何关键字。使用格式：`<keyword> <id>`。

您可以使用以下关键字：

- `bug`
- `defect`
- `rtcwi`
- `task`
- `work item`
- `workitem`

避免使用关键字 `#`。在 EWM 文档页面的[从提交评论创建链接](https://www.ibm.com/docs/en/elm/7.0.0?topic=commits-creating-links-from-commit-comments)，了解更多信息。
