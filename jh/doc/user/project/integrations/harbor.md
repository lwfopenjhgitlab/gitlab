---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Harbor Container Registry 集成 **(FREE)**

> 引入于 14.9 版本。

使用 Harbor 作为极狐GitLab 群组或项目的容器镜像库。

[Harbor](https://goharbor.io/) 是一个用于存储和分发 Docker 镜像的企业级 Registry 服务器，通过添加一些企业必需的功能特性，例如安全、标识和管理等，扩展了开源 Docker Distribution。作为一个企业级私有 Registry 服务器，Harbor 提供了更好的性能和安全，提升用户使用 Registry 构建和运行环境传输镜像的效率。

Harbor 容器镜像库集成对于使用 GitLab CI 并需要 Container Registry 的用户非常有用。

## 先决条件

* 在 Harbor 实例中已创建要集成的项目，登录用户需要具有在该项目中拉取、推送、编辑镜像的权限。

## 配置极狐GitLab

支持在群组级别或项目级别集成 Harbor 项目，在极狐GitLab 上完成以下步骤：

1. 在顶部栏上：
   - 配置群组级别集成：选择 **菜单 > 群组** 并找到您的群组。
   - 配置项目级别集成：选择 **菜单 > 项目** 并找到您的项目。

1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 Harbor。
1. 在 **启用集成** 下，选择 **启用**。
1. 提供以下 Harbor 配置信息：
   - **Harbor URL**：要集成的 Harbor 实例的 base URL，例如 `https://harbor.example.com`。
   - **Harbor 项目名称**：在 Harbor 实例中，要集成的 Harbor 项目的名称。
   - **Harbor 用户名**：Harbor 用户的名称。用户需满足[先决条件](#先决条件)中的要求。  
   - **Harbor 密码**：Harbor 用户的密码。

1. 选择 **保存修改**。

Harbor 集成激活后：

* 将创建全局变量 `$HARBOR_USERNAME`、`$HARBOR_HOST`、`$HARBOR_OCI``$HARBOR_PASSWORD`、`$HARBOR_PROJECT` 和 `$HARBOR_URL` 供 CI/CD 使用。

* 项目级别的设置覆盖所在群组级别的设置。

## 安全注意事项

### 保护您对 Harbor API 的请求

对于通过 Harbor 集成的每个 API 请求，连接到 Harbor API 的凭据使用 `username:password` 组合。以下是安全使用的建议：

* 连接 Harbor API 时使用 TLS。
* 您的凭据应遵循最小权限原则（用于访问 Harbor 时）。
* 对您的凭据制定轮换策略。

### CI/CD 变量安全

推送到您的 `.gitlab-ci.yml` 文件的恶意代码可能会破坏您的变量，包括 `$HARBOR_PASSWORD`，并将它们发送到第三方服务器。有关详细信息，请参阅 [CI/CD 变量安全性](../../../ci/variables/index.md#cicd-variable-security)。

## CI/CD 中的 Harbor 变量示例

### 使用 kaniko 推送 Docker 镜像

有关更多信息，请参阅[使用 kaniko 构建 Docker 镜像](../../../ci/docker/using_kaniko.md)。

```yaml
docker:
  stage: docker
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: ['']
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${HARBOR_HOST}\":{\"auth\":\"$(echo -n ${HARBOR_USERNAME}:${HARBOR_PASSWORD} | base64)\"}}}" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${HARBOR_HOST}/${HARBOR_PROJECT}/${CI_PROJECT_NAME}:${CI_COMMIT_TAG}"
  rules:
  - if: $CI_COMMIT_TAG
```

### 使用 OCI 库推送 Helm chart

Helm 默认支持 OCI 库。[Harbor 2.0](https://github.com/goharbor/harbor/releases/tag/v2.0.0) 及更高版本支持 OCI。在 Helm 的[博客](https://helm.sh/blog/storing-charts-in-oci/)和[文档](https://helm.sh/docs/topics/registries/#enabling-oci-support)中阅读有关 OCI 的更多信息。

```yaml
helm:
  stage: helm
  image:
    name: dtzar/helm-kubectl:latest
    entrypoint: ['']
  variables:
    # Enable OCI support (not required since Helm v3.8.0)
    HELM_EXPERIMENTAL_OCI: 1
  script:
    # Log in to the Helm registry
    - helm registry login "${HARBOR_URL}" -u "${HARBOR_USERNAME}" -p "${HARBOR_PASSWORD}"
    # Package your Helm chart, which is in the `test` directory
    - helm package test
    # Your helm chart is created with <chart name>-<chart release>.tgz
    # You can push all building charts to your Harbor repository
    - helm push test-*.tgz ${HARBOR_OCI}/${HARBOR_PROJECT}
```
