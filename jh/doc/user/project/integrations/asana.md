---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Asana 集成 **(FREE)**

此集成将提交消息作为注释添加到 Asana 任务。
启用后，将为提交消息检查 Asana 任务 URL（例如，`https://app.asana.com/0/123456/987654`）或以 `#` 开头的任务 ID（例如，`#987654`）的。找到的每个任务 ID 都会获得添加到其中的提交评论。

您还可以使用包含以下信息的消息关闭任务：`fix #123456`。
您可以使用以下任何一个词：

- `fix`
- `fixed`
- `fixes`
- `fixing`
- `close`
- `closes`
- `closed`
- `closing`

<!--
See also the [Asana integration API documentation](../../../api/integrations.md#asana).
-->

## 设置

在 Asana 中，创建个人访问令牌。[了解 Asana 中的个人访问令牌](https://developers.asana.com/docs/personal-access-token)。

在极狐GitLab 中完成以下步骤：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **Asana**。
1. 确保打开了 **启用** 切换开关。
1. 粘贴您在 Asana 中生成的令牌。
1. 可选。要将此设置限制为特定分支，请在 **限制到分支** 字段中列出它们，用逗号分隔。
1. 选择 **保存修改** 或选择 **测试设置**。

<!-- ## Troubleshooting -->
