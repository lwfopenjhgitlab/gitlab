---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Redmine 服务 **(FREE)**

使用 [Redmine](https://www.redmine.org/) 作为议题跟踪器。

要在项目中启用 Redmine 集成：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **Redmine**。
1. 选中 **启用集成** 下的复选框。
1. 填写必填字段：

   - **项目 URL**：关联到此极狐GitLab 项目的 Redmine 项目的 URL。
   - **议题 URL**：关联到此极狐GitLab 项目的 Redmine 项目议题的 URL。URL 必须包含 `:id`。极狐GitLab 将此 ID 替换为议题编号。
   - **新建议题 URL**：用于在关联到此极狐GitLab 项目的 Redmine 项目中创建新议题的 URL。
     <!-- The line below was originally added in January 2018: https://gitlab.com/gitlab-org/gitlab/-/commit/778b231f3a5dd42ebe195d4719a26bf675093350 -->
     **此 URL 未使用，计划在未来版本中删除。**

1. 选择 **保存更改** 或选择 **测试设置**（可选）。

配置并启用 Redmine 后，您会在极狐GitLab 项目页面上看到 Redmine 链接，该链接会将您带到您的 Redmine 项目。

例如，这是一个名为 `gitlab-ci` 的项目的配置：

- 项目 URL：`https://redmine.example.com/projects/gitlab-ci`
- 议题 URL：`https://redmine.example.com/issues/:id`
- 新建议题 URL：`https://redmine.example.com/projects/gitlab-ci/issues/new`

您还可以在此项目中禁用[极狐GitLab 内部的议题跟踪](../issues/index.md)。
在[共享和权限文档](../settings/index.md#configure-project-visibility-features-and-permissions)中了解有关禁用极狐GitLab 议题的步骤和后果的更多信息。

## 在极狐GitLab 中引用 Redmine 议题

您可以使用以下方法引用您的 Redmine 议题：

- `#<ID>`，其中 `<ID>` 是一个数字（例如 `#143`）。
- `<PROJECT>-<ID>`，例如 `API_32-143`，其中：
  - `<PROJECT>` 以大写字母开头，后跟大写字母、数字或下划线。
  - `<ID>` 是一个数字。

在链接中，`<PROJECT>` 部分被忽略，它们总是指向**议题 URL** 中指定的地址。

如果您同时启用了内部和外部议题跟踪器，我们建议使用较长的格式（`<PROJECT>-<ID>`）。如果您使用较短的格式，并且内部议题跟踪器中存在具有相同 ID 的议题，则会链接到内部议题。
