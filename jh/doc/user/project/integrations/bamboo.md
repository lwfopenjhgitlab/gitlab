---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Atlassian Bamboo 集成 **(FREE)**

当您将更改推送到极狐GitLab 中的项目时，您可以在 Atlassian Bamboo 中自动触发构建。

在接受 webhook 和提交数据时，Bamboo 不提供与传统构建系统相同的功能。在极狐GitLab 中配置集成之前，您必须配置 Bamboo 构建计划。

## 配置 Bamboo

1. 在 Bamboo 中，转到构建计划并选择 **Actions > Configure plan**。
1. 选择 **Triggers** 选项卡。
1. 选择 **Add trigger**。
1. 输入类似“极狐GitLab 触发器”的描述。
1. 选择 **Repository triggers the build when changes are committed**。
1. 选中一个或多个仓库的复选框。
1. 在 **Trigger IP addresses** 中输入极狐GitLab IP地址。允许这些 IP 地址触发 Bamboo 构建。
1. 保存触发器。
1. 在左侧窗格中，选择构建阶段。如果您有多个构建阶段，请选择包含 Git 检出任务的最后一个阶段。
1. 选择 **Miscellaneous** 选项卡。
1. 在 **Pattern Match Labeling** 下，在 **Labels** 中输入 `${bamboo.repository.revision.number}`。
1. 选择 **Save**。

Bamboo 已准备好接受来自极狐GitLab 的触发。接下来，在极狐GitLab 中设置 Bamboo 集成。

## 配置极狐GitLab

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **Atlassian Bamboo**。
1. 确保选中 **启用** 复选框。
1. 输入您的 Bamboo 服务器的基本 URL。例如，`https://bamboo.example.com`。
1. 可选。清除 **启用 SSL 验证** 复选框，禁用 SSL 验证<!--[SSL 验证](index.md#manage-ssl-verification)-->。
1. 输入 Bamboo 构建计划中的[构建密钥](#identify-the-bamboo-build-plan-build-key)。
1. 如有必要，输入有权触发构建计划的 Bamboo 用户的用户名和密码。如果您不需要身份验证，请将这些字段留空。
1. 可选。要在 Bamboo 中测试配置并触发构建，请选择 **测试设置**。
1. 选择 **保存修改**。

<a id="identify-the-bamboo-build-plan-build-key"></a>

### 确定 Bamboo 构建计划构建密钥

构建密钥是通常由项目密钥和计划密钥组成的唯一标识符。
构建密钥很短，全部大写，并用破折号 (`-`) 分隔，例如 `PROJ-PLAN`。

当您在 Bamboo 中查看计划时，构建密钥包含在浏览器 URL 中。例如，`https://bamboo.example.com/browse/PROJ-PLAN`。

## 故障排除

### 构建未触发

如果未触发构建，请确保您在 Bamboo 中的 **Trigger IP addresses** 下输入了正确的极狐GitLab IP 地址。还要检查服务钩子日志<!--[服务钩子日志](index.md#troubleshooting-integrations)-->是否有请求失败。

### 高级 Atlassian Bamboo 功能在极狐GitLab UI 中不可用

高级 Atlassian Bamboo 功能与极狐GitLab 不兼容。这些功能包括但不限于从极狐GitLab UI 查看构建日志的能力。
