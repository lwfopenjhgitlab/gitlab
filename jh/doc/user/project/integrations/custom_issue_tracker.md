---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 自定义议题跟踪器 **(FREE)**

您可以将[外部议题跟踪器](../../../integration/external-issue-tracker.md)与极狐GitLab 集成。<!--如果您的首选议题跟踪器未在[集成列表](../../../integration/external-issue-tracker.md#configure-an-external-issue-tracker) 中列出，您可以启用自定义议题跟踪器。-->

启用自定义议题跟踪器后，议题跟踪器的链接将显示在项目的左侧栏中。

![Custom issue tracker link](img/custom_issue_tracker_v14_5.png)

## 启用自定义议题跟踪器

要在项目中启用自定义议题跟踪器：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **自定义议题跟踪器**。
1. 选中 **启用集成** 下的复选框。
1. 填写必填字段：

   - **项目 URL**：在自定义议题跟踪器中查看所有议题的 URL。
   - **议题 URL**：在自定义议题跟踪器中查看议题的 URL。URL 必须包含 `:id`。极狐GitLab 将 `:id` 替换为议题编号（例如，`https://customissuetracker.com/project-name/:id`，变为 `https://customissuetracker.com/project-name/123`）。

<!--
   - **New issue URL**:
     <!-- The line below was originally added in January 2018: https://gitlab.com/gitlab-org/gitlab/-/commit/778b231f3a5dd42ebe195d4719a26bf675093350 -->
     **This URL is not used and an [issue exists](https://gitlab.com/gitlab-org/gitlab/-/issues/327503) to remove it.**
     Enter any URL.-->

1. 可选。选择 **测试设置**。
1. 选择 **保存修改**。

## 在极狐GitLab 中引用外部议题跟踪器议题

您可以使用以下方法引用外部议题跟踪器中的议题：

- `#<ID>`，其中 `<ID>` 是一个数字（例如，`#143`）。
- `<PROJECT>-<ID>`（例如 `API_32-143`）其中：
   - `<PROJECT>` 以大写字母开头，后跟大写字母、数字或下划线。
   - `<ID>` 是一个数字。

链接中的 `<PROJECT>` 部分被忽略，链接始终指向 **议题 URL** 中指定的地址。

如果您同时启用了内部和外部议题跟踪器，我们建议使用较长的格式（`<PROJECT>-<ID>`）。如果您使用较短的格式，并且内部议题跟踪器中存在具有相同 ID 的议题，则会链接内部议题。
