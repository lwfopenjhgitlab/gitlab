---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Discord Notifications 服务 **(FREE)**

Discord Notifications 服务将事件通知从极狐GitLab 发送到为其创建 webhook 的 channel。

要将极狐GitLab 事件通知发送到 Discord channel，[在 Discord 中创建 webhook](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks) 并在极狐GitLab 中进行配置。

## 创建 webhook

1. 打开您想要接收极狐GitLab 事件通知的 Discord channel。
1. 从菜单中，选择 **Edit channel**。
1. 选择 **集成**。
1. 如果没有现有的 webhook，请选择 **创建 Webhook**。否则，请选择 **查看 Webhook** 然后选择 **新建 Webhook**。
1. 输入要发布消息的机器人的名称。
1. 可选。编辑头像。
1. 复制 **WEBHOOK URL** 字段中的 URL。
1. 选择 **保存**。

## 在极狐GitLab 中配置创建的 webhook

使用在 Discord channel 中创建的 webhook URL，您可以在极狐GitLab 中设置 Discord Notifications 服务。

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **Discord Notifications**。
1. 确保打开了 **启用** 切换开关。
1. 选中与您要向 Discord 发送通知的极狐GitLab 事件对应的复选框。
1. 粘贴您从创建 Discord webhook 步骤中复制的 webhook URL。
1. 配置其余选项并选择 **保存更改** 按钮。

您创建 webhook 的 Discord channel 现在会收到已配置的极狐GitLab 事件的通知。
