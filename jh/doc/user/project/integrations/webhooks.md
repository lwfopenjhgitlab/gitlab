---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Webhooks **(FREE)**

Webhook 是您定义的自定义 HTTP 回调。它们通常由事件触发，例如将代码推送到仓库或在博客上发表评论。
当事件发生时，源应用程序会向为 webhook 配置的 URI 发出 HTTP 请求，要采取的操作可能是任何。例如，您可以使用 webhook 来：

- 触发持续集成 (CI) 作业、更新外部议题跟踪器、更新备份镜像或部署到您的生产服务器。
- 每次作业失败时向 [Slack](https://api.slack.com/incoming-webhooks) 发送通知。

<!--
- 与 Twilio 集成，每次在极狐GitLab 中为特定项目或群组创建议题时都会通过 SMS 通知。
- [Automatically assign labels to merge requests](https://about.gitlab.com/blog/2016/08/19/applying-gitlab-labels-automatically/).
-->

您可以配置极狐GitLab 项目或[群组](#group-webhooks)，发生事件时触发 [percent-encoded](https://developer.mozilla.org/en-US/docs/Glossary/percent-encoding) webhook URL，例如，当推送新代码或创建新议题时。Webhook 侦听特定的[事件](#events)，系统将带有数据的 POST 请求发送到 webhook URL。

通常，您设置自己的 [webhook 接收器](#create-an-example-webhook-receiver)，根据您的要求从极狐GitLab 接收信息并将其发送到另一个应用程序。
我们有一个[内置接收器](slack.md)，用于为每个项目发送 [Slack](https://api.slack.com/incoming-webhooks) 通知。

<!--
GitLab.com enforces [webhook limits](../../../user/gitlab_com/index.md#webhooks),
including:

- The maximum number of webhooks and their size, both per project and per group.
- The number of webhook calls per minute.
-->

<a id="group-webhooks"></a>

## 群组 webhooks **(PREMIUM)**

您可以配置一个群组 webhook，它由群组中所有项目发生的事件触发。如果您在群组和项目中配置相同的 webhook，它们都由项目中的事件触发。

群组 webhook 也可以配置为侦听特定于群组的事件，包括：

- [群组成员事件](webhook_events.md#group-member-events)
- [子组事件](webhook_events.md#subgroup-events)

<a id="configure-a-webhook-in-gitlab"></a>

## 在极狐GitLab 中配置 webhook

您可以为群组或项目配置 webhook。

1. 在您的项目或群组中，在左侧边栏中，选择 **设置 > Webhooks**。
1. 在 **URL** 中，输入 webhook 端点的 URL。如果 URL 包含一个或多个特殊字符，则该 URL 必须是百分比编码的。
1. 在 **Secret 令牌** 中，输入 [secret 令牌](#validate-payloads-by-using-a-secret-token)，验证有效负载。
1. 在 **触发器** 部分，选择[事件](webhook_events.md)来触发 webhook。
1. 可选。清除 **启用 SSL 验证** 复选框，禁用 SSL 验证<!--[SSL 验证](index.md#manage-ssl-verification)-->。
1. 选择 **添加 webhook**。

## 配置您的 webhook 接收器端点

Webhook 接收器应该*快速*且*稳定*。
可能会暂时禁用慢速和不稳定的接收器以确保系统可靠性。
如果您正在编写自己的端点（Web 服务器）来接收极狐GitLab webhook，请记住以下几点：

- 您的端点应尽快发送其 HTTP 响应。在所有情况下，您都应该以秒级响应时间为目标。如果响应时间超过配置的超时时间，系统会假定钩子失败，这可能导致重试并可能导致重复事件。要自定义超时，请参阅 [Webhook 失败或触发多个 webhook 请求](#webhook-fails-or-multiple-webhook-requests-are-triggered)。
- 您的端点应始终返回有效的 HTTP 响应。如果没有，系统会假定钩子失败并重试。大多数 HTTP 库会自动为您处理响应，但如果您正在编写低级挂钩，请记住这一点。
- 系统通常会忽略端点返回的 HTTP 状态码，除非设置了 [`web_hooks_disable_failed` 功能标志](#failing-webhooks)。

Webhook 接收器的最佳实践：

- 最好返回 `200` 或 `201` 状态响应。仅返回错误状态（在 `4xx` 范围内）指示 webhook 配置错误。例如，如果您的接收器仅支持推送事件，则在发送议题有效负载时返回 `400` 是可以接受的，因为这表明钩子设置不正确。或者，忽略无法识别的事件有效负载是可以接受的。如果事件已被处理，切勿返回 `500` 状态响应。
- 您的服务应该是幂等的。在某些情况下（包括超时），同一事件可能会被发送两次。准备好处理重复的事件，您可以通过确保端点可靠快速且稳定来减少这种情况的发生。
- 保持响应负载尽可能短，空的响应也很好，系统不检查响应正文，它仅被存储，以便您稍后在日志中检查它。
- 限制响应 headers 的数量和大小。在检查 webhook 日志时，仅发送有助于诊断问题的 header。
- 要支持快速响应时间，请异步执行 I/O 或计算密集型操作。您可以通过返回 `201` 来表明 webhook 是异步的。

<a id="failing-webhooks"></a>

### 失败的 webhooks

> - 引入于 13.12 版本，功能标志为 `web_hooks_disable_failed`。默认禁用
> - 在 SaaS 上启用于 14.9 版本。

FLAG:
在私有化部署版实例上，此功能默认不可用，要使其可用，询问管理员启用功能标志 `web_hooks_disable_failed`。此功能尚未准备好用于生产。

如果 webhook 反复失败，它可能会被自动禁用。

返回 `5xx` 范围内的响应代码的 Webhook 被理解为间歇性失败，并被暂时禁用，最初持续 10 分钟。如果继续失败，则每次重试时都会延长回退时间，最长禁用时间为 24 小时。

返回 `4xx` 范围内的失败代码的 Webhook 被理解为配置错误，并且在您手动重新启用它们之前，这些 Webhook 将被禁用。 这些 webhook 不会自动重试。

有关如何查看 Webhook 是否已禁用以及如何重新启用它的信息，请参阅[故障排除](#troubleshoot-webhooks)。

<a id="test-a-webhook"></a>

## 测试 webhook

您可以手动触发 webhook，以确保其正常工作。您还可以发送测试请求，重新启用[已禁用的 webhook](#re-enable-disabled-webhook)。

例如，要测试 `push events`，您的项目应该至少有一个提交，webhook 使用此提交。

NOTE:
项目和群组 webhook 的某些类型的事件不支持测试。

先决条件：

- 要测试项目 webhook，您必须至少具有该项目的维护者角色。
- 要测试群组 webhook，您必须具有该群组的所有者角色。

测试 webhook：

1. 在您的项目或群组中，在左侧边栏中，选择 **设置 > Webhooks**。
1. 向下滚动到配置的 webhook 列表。
1. 从 **测试** 下拉列表中，选择要测试的事件类型。

您还可以从其编辑页面测试 webhook。

![Webhook testing](img/webhook_testing.png)

<a id="create-an-example-webhook-receiver"></a>

## 创建一个示例 webhook 接收器

要测试 webhook 的工作方式，您可以使用在控制台会话中运行的 echo 脚本。要使以下脚本正常工作，您必须安装 Ruby。

1. 将以下文件另存为 `print_http_body.rb`：

   ```ruby
   require 'webrick'

   server = WEBrick::HTTPServer.new(:Port => ARGV.first)
   server.mount_proc '/' do |req, res|
     puts req.body
   end

   trap 'INT' do
     server.shutdown
   end
   server.start
   ```

1. 选择一个未使用的端口（例如，`8000`）并启动脚本：

   ```shell
   ruby print_http_body.rb 8000
   ```

1. 在极狐GitLab 中，[配置 webhook](#configure-a-webhook-in-gitlab) 并添加接收者的 URL，例如，`http://receiver.example.com:8000/`。

1. 选择 **测试**。您应该在控制台中看到类似这样的内容：

   ```plaintext
   {"before":"077a85dd266e6f3573ef7e9ef8ce3343ad659c4e","after":"95cd4a99e93bc4bbabacfa2cd10e6725b1403c60",<SNIP>}
   example.com - - [14/May/2014:07:45:26 EDT] "POST / HTTP/1.1" 200 0
   - -> /
   ```

NOTE:
您可能需要[允许对本地网络的请求](../../../security/webhooks.md)，才能添加此接收器。

## 使用 secret 令牌验证有效负载

您可以指定一个 secret 令牌来验证接收到的有效负载。
令牌与 `X-Gitlab-Token` HTTP header 中的钩子请求一起发送。您的 webhook 端点可以检查令牌，验证请求是否合法。

## 按分支过滤推送事件

您可以按分支过滤推送事件。使用以下选项之一来筛选将哪些推送事件发送到您的 webhook 端点：

- **所有分支**：从所有分支推送事件。
- **通配符样式**：从匹配通配符样式的分支推送事件（例如，`*-stable` 或 `production/*`）。
- **正则表达式**：从与正则表达式匹配的分支推送事件（例如，`(feature|hotfix)/*`）。

您可以在项目的 [webhook 设置](#configure-a-webhook-in-gitlab)中配置分支过滤。

## 图像 URL 如何在 webhook 正文中显示

相对路径图像引用可以在 webhook 的正文中，被重写使用绝对 URL。
例如，如果图像、合并请求、评论或 wiki 页面包含以下图像引用：

```markdown
![image](/uploads/$sha/image.png)
```

如果：

- 极狐GitLab 安装在 `gitlab.example.com`。
- 项目位于 `example-group/example-project`。

引用在 webhook 正文中重写如下：

```markdown
![image](https://gitlab.example.com/example-group/example-project/uploads/$sha/image.png)
```

如果出现以下情况，则不会重写图像 URL：

- 它们已经指向 HTTP、HTTPS 或协议相关的 URL。
- 他们使用高级 Markdown 功能，如链接标签。

<a id="events"></a>

## 事件

有关 Webhook 支持的事件的更多信息，请转到 [Webhook 事件](webhook_events.md)。

<a id="troubleshoot-webhooks"></a>

## Webhooks 故障排除

> 群组 webhooks 的**最近事件**引入于 15.3 版本。

系统记录每个 webhook 请求的历史记录。
您可以在 **最近事件** 表格中查看过去 2 天提出的请求。

先决条件：

- 要对项目 webhook 进行故障排除，您必须至少具有该项目的维护者角色。
- 要对群组 webhook 进行故障排除，您必须具有该群组的所有者角色。

查看表格：

1. 在您的项目或群组中，在左侧边栏中，选择 **设置 > Webhooks**。
1. 向下滚动到 webhook。
1. 每个[失败的 webhook](#failing-webhooks) 都有一个标记，将其列为：

   - **Failed to connect**：如果它配置错误，并且需要手动干预以重新启用它。
   - **Fails to connect**：如果它被暂时禁用并稍后重试。

   ![Badges on failing webhooks](img/failed_badges.png)

1. 选择要查看的 webhook 对应的 **编辑**。

该表包含有关每个请求的以下详细信息：

- HTTP 状态码（绿色表示 `200`-`299`，红色表示其它状态码，`internal error` 表示发送失败）
- 触发事件
- 请求的经过时间
- 提出请求的相对时间

![Recent deliveries](img/webhook_logs.png)

每个 webhook 事件都有一个对应的 **详情** 页面。此页面详细介绍了极狐GitLab 发送的数据（请求 header 和正文）和接收的数据（响应 header 和正文）。
要查看 **详情** 页面，请为 webhook 事件选择 **查看详情**。

要使用相同的数据重复交付，请选择 **重新发送请求**。

NOTE:
如果您更新 Webhook 的 URL 或 secret 令牌，数据将传送到新地址。

<a id="webhook-fails-or-multiple-webhook-requests-are-triggered"></a>

### Webhook 失败或触发多个 Webhook 请求

当极狐GitLab 发送 webhook 时，默认情况下它会在 10 秒内得到响应。
如果端点在这 10 秒内没有发送 HTTP 响应，系统可能会假设 webhook 失败并重试。

如果您的 webhook 失败或您收到多个请求，管理员可以尝试通过取消注释或在 `/etc/gitlab/gitlab.rb` 中添加以下设置来更改默认超时值：

```ruby
gitlab_rails['webhook_timeout'] = 10
```

### 无法获取本地颁发者证书

启用 SSL 验证后，您可能会收到极狐GitLab 无法验证 webhook 端点的 SSL 证书的错误。
通常，发生此错误是因为根证书不是由 [CAcert.org](http://www.cacert.org/) 确定的受信任证书颁发机构颁发的。

如果不是这种情况，请考虑使用 [SSL Checker](https://www.sslshopper.com/ssl-checker.html) 来识别故障。
缺少中间证书是验证失败的常见原因。

<a id="re-enable-disabled-webhook"></a>

### 重新启用禁用的 webhook

如果 webhook 失败，编辑页面顶部会显示一个 banner，说明禁用它的原因以及何时自动重新启用。例如：

![A banner for a failing webhook, warning it failed to connect and will retry in 60 minutes](img/failed_banner.png)

如果 webhook 失败，则会显示错误 banner：

![A banner for a failed webhook, showing an error state, and explaining how to re-enable it](img/failed_banner_error.png)

要重新启用失败或失败的 webhook，请[发送测试请求](#test-a-webhook)。如果测试请求成功，则重新启用 webhook。
