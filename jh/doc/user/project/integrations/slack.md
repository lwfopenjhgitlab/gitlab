---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Slack 通知服务 **(FREE)**

Slack 通知服务使您的极狐GitLab 项目能够将事件（例如问题创建）作为通知发送到您现有的 Slack 团队。设置 Slack 通知需要更改 Slack 和极狐GitLab 的配置。

您还可以使用 [Slack 指令](slack_slash_commands.md) 从 Slack 控制极狐GitLab。指令是单独配置的。

<a id="configure-slack"></a>

## 配置 Slack

1. 登录您的 Slack 团队并[开始新的 Incoming WebHooks 配置](https://my.slack.com/services/new/incoming-webhook)。
1. 确定默认情况下应将通知发送到的 Slack 频道。选择 **Add Incoming WebHooks integration** 添加配置。
1. 复制 **Webhook URL**，后续配置极狐GitLab 时使用。

## 配置极狐GitLab

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **Slack 通知**。
1. 在 **启用集成** 部分，选中 **启用** 复选框。
1. 在 **触发器** 部分，选中要作为通知发送到 Slack 的每种类型的极狐GitLab 事件的复选框。有关完整列表，请参阅 [Slack 通知的触发器](#triggers-for-slack-notifications)。默认情况下，消息会发送到您在[配置 Slack](#configure-slack) 时配置的频道。
1. （可选）要将消息发送到不同的频道、多个频道或作为直接消息：
   - *要向频道发送消息，*输入 Slack 频道名称，以逗号分隔。
   - *要发送直接消息，*使用用户 Slack 个人资料中的会员 ID。

   NOTE:
   不支持用户名和私有频道。

1. 在 **Webhook** 中，输入您在[配置 Slack ](#configure-slack) 步骤中复制的 webhook URL。
1. （可选）在 **用户名** 中，输入发送通知的 Slack 机器人的用户名。
1. 选择 **仅流水线失败时通知** 复选框，仅在流水线失败时通知。
1. 在 **要发送通知的分支** 下拉列表中，选择要发送通知的分支类型。
1. 将 **需要通知的标记** 字段留空，可以获取所有通知；或者添加议题或合并请求必须具有的标记，才能触发通知。
1. 选择 **测试设置** 可以验证您的信息，然后选择 **保存修改**。

您的 Slack 团队现在开始按照配置接收极狐GitLab 事件通知。

<a id="triggers-for-slack-notifications"></a>

## Slack 通知的触发器

以下触发器可用于 Slack 通知：

| 触发器名称                                                             | 触发器事件                                        |
|--------------------------------------------------------------------------|------------------------------------------------------|
| **推送**                                                                 | 推送到仓库。                                           |
| **议题**                                                                | 创建、更新或关闭议题。                                         |
| **私密议题**                                                   | 创建、更新或关闭私密议题。                                                        |
| **合并请求**                                                        | 创建、更新或合并合并请求。                                                    |
| **备注**                                                                 | 添加了一条评论。                                        |
| **私密备注**                                                    | 添加了私密备注。                                        |
| **标签推送**                                                             | 新标签被推送到仓库。                                     |
| **流水线**                                                             | 流水线状态已更改。                                      |
| **Wiki 页面**                                                            | 创建或更新 wiki 页面。                                  |
| **部署**                                                           | 部署开始或结束。                                        |
| **警报**                                                                | 记录了一个新的、独特的警报。                              |
| [**漏洞**](../../application_security/vulnerabilities/index.md) | 记录了一个新的、独特的漏洞。                              |

## 故障排除

如果您的 Slack 集成不起作用，请通过在 <!--[Sidekiq 日志](../../../administration/logs.md#sidekiqlog)-->Sidekiq 日志中搜索与您的 Slack 服务相关的错误来开始故障排除。

### Something went wrong on our end

您可能会在 UI 中收到此通用错误消息。
查看<!--[日志](../../../administration/logs.md#productionlog)-->日志查找错误消息，并从那里继续进行故障排除。

### `certificate verify failed`

您可能会在 Sidekiq 日志中看到如下条目：

```plaintext
2019-01-10_13:22:08.42572 2019-01-10T13:22:08.425Z 6877 TID-abcdefg Integrations::ExecuteWorker JID-3bade5fb3dd47a85db6d78c5 ERROR: {:class=>"Integrations::ExecuteWorker :integration_class=>"SlackService", :message=>"SSL_connect returned=1 errno=0 state=error: certificate verify failed"}
```

当极狐GitLab 与 Slack 通信或极狐GitLab 与自身通信存在问题时，会出现此问题。
前者不太可能，因为应该始终信任 Slack 安全证书。

要查看哪一个是问题的原因：

1. 启动 Rails 控制台：

   ```shell
   sudo gitlab-rails console -e production

   # for source installs:
   bundle exec rails console -e production
   ```

1. 运行以下命令：

   ```ruby
   # replace <SLACK URL> with your actual Slack URL
   result = Net::HTTP.get(URI('https://<SLACK URL>'));0

   # replace <GITLAB URL> with your actual GitLab URL
   result = Net::HTTP.get(URI('https://<GITLAB URL>'));0
   ```

如果极狐GitLab 不信任自身的 HTTPS 连接，[将您的证书添加到极狐GitLab 受信任的证书](https://docs.gitlab.cn/omnibus/settings/ssl.html#install-custom-public-certificates)。

如果极狐GitLab 不信任与 Slack 的连接，则极狐GitLab OpenSSL 信任存储不正确。典型原因包括：

- 使用 `gitlab_rails['env'] = {"SSL_CERT_FILE" => "/path/to/file.pem"}` 覆盖了信任库。
- 不小心修改了默认 CA 包 `/opt/gitlab/embedded/ssl/certs/cacert.pem`。

### 批量更新来禁用 Slack Notification 集成

要为所有启用了 Slack 集成的项目禁用通知，[启动 Rails 控制台会话](../../../administration/operations/rails_console.md#starting-a-rails-console-session)，使用类似于以下的脚本：

WARNING:
无论运行是否正确，更改数据的命令可能会造成损坏。始终首先在测试环境中运行命令，并准备好备份实例以进行恢复。

```ruby
# Grab all projects that have the Slack notifications enabled
p = Project.find_by_sql("SELECT p.id FROM projects p LEFT JOIN integrations s ON p.id = s.project_id WHERE s.type_new = 'Slack' AND s.active = true")

# Disable the integration on each of the projects that were found.
p.each do |project|
  project.slack_integration.update!(:active, false)
end
```
