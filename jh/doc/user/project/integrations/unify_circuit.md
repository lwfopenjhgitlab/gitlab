---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Unify Circuit 服务 **(FREE)**

Unify Circuit 服务将通知从极狐GitLab 发送到 Circuit 对话。

## 设置 Unify Circuit 服务

在 Unify Circuit 中，[添加 webhook](https://www.circuit.com/unifyportalfaqdetail?articleId=164448) 并复制其 URL。

在极狐GitLab 中：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **Unify Circuit**。
1. 打开 **启用** 开关。
1. 选中您希望在 Unify Circuit 中接收的极狐GitLab 事件对应的复选框。
1. 粘贴您从 Unify Circuit 配置步骤复制的 **Webhook URL**。
1. 选择 **仅在流水线失败时通知** 复选框，仅在失败时通知。
1. 在 **要发送通知的分支**下拉列表中，选择要发送通知的分支类型。
1. 选择 **保存修改** 或 **测试设置**。

您的 Unify Circuit 对话现在开始接收极狐GitLab 事件通知。
