---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# YouTrack 服务 **(FREE)**

JetBrains [YouTrack](https://www.jetbrains.com/youtrack/) 是一个基于 Web 的议题跟踪和项目管理平台。

您可以在极狐GitLab 中将 YouTrack 配置为[外部议题跟踪器](../../../integration/external-issue-tracker.md)。

要在项目中启用 YouTrack 集成：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 集成**。
1. 选择 **YouTrack**。
1. 选中 **启用集成** 下的复选框。
1. 填写必填字段：
   - **项目 URL**：YouTrack 中项目的 URL。
   - **议题 URL**：在 YouTrack 项目中查看议题的 URL。URL 必须包含 `:id`。极狐GitLab 将 `:id` 替换为议题编号。
1. 选择 **保存变更** 或选择 **测试设置**（可选）。

配置并启用 YouTrack 后，极狐GitLab 项目页面上会显示一个链接。此链接会将您带到相应的 YouTrack 项目。

您还可以在此项目中禁用[极狐GitLab 内部议题跟踪](../issues/index.md)。
在[共享和权限文档](../settings/index.md#configure-project-visibility-features-and-permissions)中了解有关禁用极狐GitLab 议题的步骤和后果的更多信息。

## 在极狐GitLab 中引用 YouTrack 议题

您可以使用 `<PROJECT>-<ID>`（例如 `YT-101`、`Api_32-143` 或 `gl-030`）在 YouTrack 中引用问题，其中：

- `<PROJECT>` 以字母开头，后跟字母、数字或下划线。
- `<ID>` 是一个数字。

合并请求、提交或评论中对 `<PROJECT>-<ID>` 的引用会自动链接到 YouTrack 议题 URL。
