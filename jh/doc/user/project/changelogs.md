---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments"
type: reference, api
---

# 变更日志 **(FREE)**

变更日志是根据提交标题和 Git trailers 生成的。要包含在变更日志中，提交必须包含特定的 Git trailers。变更日志由提交标题生成，并按 Git trailers 类型分类。您可以使用其他数据丰富变更日志条目，例如合并请求的链接或有关提交作者的详细信息。更新日志格式[可以自定义](#customize-the-changelog-output)使用模板。

默认更新日志中的每个部分都有一个包含版本号和发布日期的标题，如下所示：

````markdown
## 1.0.0 (2021-01-05)

### Features (4 changes)

- [Feature 1](gitlab-org/gitlab@123abc) by @alice ([merge request](gitlab-org/gitlab!123))
- [Feature 2](gitlab-org/gitlab@456abc) ([merge request](gitlab-org/gitlab!456))
- [Feature 3](gitlab-org/gitlab@234abc) by @steve
- [Feature 4](gitlab-org/gitlab@456)
````

部分的日期格式可以自定义，但标题的其余部分不能。
添加新部分时，极狐GitLab 解析这些标题，确定将新信息放置在文件中的位置。极狐GitLab 根据版本而不是日期对部分进行排序。

每个部分都包含按类别排序的更改（如 **功能**），并且可以更改这些部分的格式。名称源自用于包含或排除提交的 Git trailers 的值。

在镜像上操作时可以检索更改日志的提交。极狐GitLab 本身使用此功能，因为安全发布可以包括来自公开项目和私有安全镜像的更改。

## 向 Git 提交添加 trailer

您可以在编写提交消息时手动添加 trailer。要包含使用 `Changelog` 的默认 trailer 的提交并将其归类为功能，请将字符串 `Changelog: feature` 添加到您的提交消息中，如下所示：

```plaintext
<Commit message subject>

<Commit message description>

Changelog: feature
```

## 创建变更日志

要根据仓库中的提交生成变更日志数据，请参阅 API 文档。

变更日志输出采用 Markdown 格式，[您可以自定义](#customize-the-changelog-output)。

### 还原的提交

> 引入于 13.10 版本。

要被视为还原提交，提交消息必须包含字符串 `This reverts commit <SHA>`，其中 `SHA` 是要还原的提交的 SHA。

为范围生成变更日志时，系统会忽略在该范围内添加和还原的提交。在此示例中，提交 C 还原提交 B。因为提交 C 没有其他 trailer，所以只有提交 A 被添加到更改日志中：

```mermaid
graph LR
    A[Commit A<br>Changelog: changed] --> B[Commit B<br>Changelog: changed]
    B --> C[Commit C<br>Reverts commit B]
```

但是，如果还原提交（提交 C）还包含更改日志 trailer，则提交 A 和 C 都包含在更改日志中：

```mermaid
graph LR
    A[Commit A<br><br>Changelog: changed] --> B[Commit B<br><br>Changelog: changed]
    B --> C[Commit C<br>Reverts commit B<br>Changelog: changed]
```

提交 B 已跳过。

<a id="customize-the-changelog-output"></a>

### 自定义变更日志输出

要自定义变更日志输出，请编辑变更日志配置文件。此配置的默认位置是 `.gitlab/changelog_config.yml`。该文件支持以下变量：

- `date_format`：日期格式，在 `strftime` 格式中，用于新添加的更新日志数据的标题。
- `template`：生成变更日志数据时使用的自定义模板。
- `include_groups`：包含用户的群组完整路径列表，无论项目成员身份如何，其贡献都应记入。生成变更日志的用户必须有权访问每个群组才能获得贡献值。
- `categories`：将原始类别名称映射到要在变更日志中使用的名称的哈希。要更改变更日志中显示的名称，请将这些行添加到您的配置文件并编辑它们以满足您的需要。此示例将类别标题呈现为 `### Features`、`### Bug fixes` 和 `### Performance improvements`：

  ```yaml
   ---
   categories:
     feature: Features
     bug: Bug fixes
     performance: Performance improvements
  ```

### 自定义模板

类别部分是使用模板生成的。默认模板：

```plaintext
{% if categories %}
{% each categories %}
### {{ title }} ({% if single_change %}1 change{% else %}{{ count }} changes{% end %})

{% each entries %}
- [{{ title }}]({{ commit.reference }})\
{% if author.credit %} by {{ author.reference }}{% end %}\
{% if merge_request %} ([merge request]({{ merge_request.reference }})){% end %}

{% end %}

{% end %}
{% else %}
No changes.
{% end %}
```

`{% ... %}` 标签用于语句，`{{ ... }}` 用于打印数据。语句必须使用 `{% end %}` 终止标签。`if` 和 `each` 语句都需要一个参数。

例如，对于名为 `valid` 的变量，您可以在该值为 true 时显示 `yes`，否则显示 `nope`，方法如下：

```plaintext
{% if valid %}
yes
{% else %}
nope
{% end %}
```

`else` 的使用是可选的。当一个值是非空值或布尔值 `true` 时，该值被视为 `true`。空数组和哈希被认为是错误的。

循环是使用 `each` 完成的，循环内的变量被限定在它的范围内。
在循环中引用当前值是使用变量标签 `{{ it }}` 完成的。其他变量从当前循环值中读取它们的值。以这个模板为例：

```plaintext
{% each users %}
{{name}}
{% end %}
```

假设 `users` 是一个对象数组，每个对象都有一个 `name` 字段，那么这将打印每个用户的名字。

使用变量标签，您可以访问嵌套对象。例如，`{{ users.0.name }}` 在 `users` 变量中打印第一个用户的名字。

如果一行以反斜杠结尾，则忽略下一个换行符，允许您跨多行包装代码，而不会在 Markdown 输出中引入不必要的换行符。

使用 `{%` 和 `%}` 的标签（称为表达式标签）使用直接跟在它们后面的换行符（如果有的话）。例如：

```plaintext
---
{% if foo %}
bar
{% end %}
---
```

编译为：

```plaintext
---
bar
---
```

而不是：

```plaintext
---

bar

---
```

您可以在配置中指定自定义模板，如下所示：

```yaml
---
template: |
  {% if categories %}
  {% each categories %}
  ### {{ title }}

  {% each entries %}
  - [{{ title }}]({{ commit.reference }})\
  {% if author.credit %} by {{ author.reference }}{% end %}

  {% end %}

  {% end %}
  {% else %}
  No changes.
  {% end %}
```

在指定模板时，您应该使用 `template: |` 而不是 `template: >`，因为后者不会在模板中保留换行符。

### 模板数据

在顶层，以下变量可用：

- `categories`：一组对象，每个更新日志类别一个。

在类别中，可以使用以下变量：

- `count`：该类别中的条目数。
- `entries`：属于这个类别的条目。
- `single_change`：一个布尔值，指示是否只有一个更改（`true`）或多个更改（`false`）。
- `title`：类别的标题（重新映射后）。

在条目中，可以使用以下变量（这里的 `foo.bar` 表示 `bar` 是 `foo` 的子字段）：

- `author.contributor`：当作者不是项目成员时设置为 `true` 的布尔值，否则为 `false`。
- `author.credit`：当 `author.contributor` 为 `true` 或配置了 `include_groups` 且作者是其中一个组的成员时，布尔值设置为 `true`。
- `author.reference`：对提交作者的引用（例如，`@alice`）。
- `commit.reference`：对提交的引用，例如，`gitlab-org/gitlab@0a4cdd86ab31748ba6dac0f69a8653f206e5cfc7`。
- `commit.trailers`：包含提交正文中存在的所有 Git 预告片的对象。
- `merge_request.reference`：对首次引入更改的合并请求的引用（例如，`gitlab-org/gitlab!50063`）。
- `title`：更改日志条目的标题（这是提交标题）。

如果无法确定数据，则 `author` 和 `merge_request` 对象可能不存在。例如，在没有相应合并请求的情况下创建提交时，不会显示任何合并请求。

### 提取版本时自定义标签格式

> 引入于 13.11 版本。

极狐GitLab 使用正则表达式（使用 [re2](https://github.com/google/re2/) 引擎和语法）从标签名称中提取语义版本。默认的正则表达式是：

```plaintext
^v?(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<pre>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<meta>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$
```

此正则表达式基于官方[语义版本控制](https://semver.org/)正则表达式，还包括对以字母 `v` 开头的标签名称的支持。

如果您的项目使用不同的标签格式，您可以指定不同的正则表达式。使用的正则表达式必须生成以下捕获组。如果这些捕获组中的任何一个丢失，标签将被忽略：

- `major`
- `minor`
- `patch`

以下捕获组是可选的：

- `pre`：如果设置，标签将被忽略。忽略 `pre` 标签可确保在确定要为其生成变更日志的提交范围时，不考虑发布候选标签和其他预发布标签。
- `meta`：可选。指定构建元数据。

使用此信息，极狐GitLab 构建了 Git 标签及其发布版本的映射。然后根据从每个标签中提取的版本确定最新标签是什么。

要指定自定义正则表达式，请在更改日志配置 YAML 文件中使用 `tag_regex` 设置。例如，此模式匹配诸如 `version-1.2.3` 之类的标签名称，但不匹配 `version-1.2`。

```yaml
---
tag_regex: '^version-(?P<major>\d+)\.(?P<minor>\d+)\.(?P<patch>\d+)$'
```

要测试您的正则表达式是否有效，您可以使用 [regex101](https://regex101.com/) 等网站。如果正则表达式语法无效，则在生成变更日志时会产生错误。

<!--
## Related topics

- [Changelog-related endpoints](../../api/repositories.md) in the Repositories API
- Developer documentation for [changelog entries](../../development/changelog.md) in GitLab
-->
