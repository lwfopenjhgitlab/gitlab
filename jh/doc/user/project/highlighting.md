---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# 语法高亮 **(FREE)**

极狐GitLab 通过  [Highlight.js](https://github.com/highlightjs/highlight.js/) 和 [Rouge](https://rubygems.org/gems/rouge) Ruby gem 为所有文件提供语法高亮显示。它试图根据文件扩展名猜测使用哪种语言，满足大多数情况下的需求。

这里的路径是 Git 内置的 [`.gitattributes` 接口](https://git-scm.com/docs/gitattributes)。

NOTE:
[Web IDE](web_ide/index.md) 和 [Snippets](../snippets.md) 使用 [Monaco Editor](https://microsoft.github.io/monaco-editor/) 进行文本编辑，其中 内部使用 [Monarch](https://microsoft.github.io/monaco-editor/monarch.html) 库进行语法高亮。

## 覆盖文件类型的语法高亮显示

NOTE:
Web IDE 不支持 `.gitattribute` 文件。

要覆盖文件类型的语法高亮显示：

1. 如果您的项目根目录中不存在 `.gitattributes` 文件，创建一个该名称的空白文件。
1. 对于要修改的每种文件类型，在 `.gitattributes` 文件中添加一行，声明文件扩展名和所需的高亮显示语言：

   ```conf
   # This extension would normally receive Perl syntax highlighting
   # but if we also use Prolog, we may want to override highlighting for
   # files with this extension:
   *.pl gitlab-language=prolog
   ```

1. 提交、推送和合并您的更改到您的默认分支。

在更改合并到您的 [默认分支](repository/branches/default.md) 后，项目中的所有 `*.pl` 文件都会以您的首选语言高亮显示。

您还可以使用通用网关接口 (CGI) 选项扩展突出显示，例如：

``` conf
# JSON file with .erb in it
/my-cool-file gitlab-language=erb?parent=json

# An entire file of highlighting errors!
/other-file gitlab-language=text?token=Error
```

## 禁用文件类型的语法突出显示

要完全禁用文件类型的高亮显示，请按照说明覆盖文件类型的高亮显示，并使用 `gitlab-language=text`：

```conf
# Disable syntax highlighting for this file type
*.module gitlab-language=text
```

## 配置高亮显示的最大文件大小

默认情况下，极狐GitLab 以纯文本形式呈现任何大于 512 KB 的文件。要更改此值：

1. 打开项目的 `gitlab.yml` 配置文件。

1. 添加此部分，将 `maximum_text_highlight_size_kilobytes` 替换为您想要的值。

   ```yaml
   gitlab:
     extra:
       ## Maximum file size for syntax highlighting
       ## https://docs.gitlab.com/ee/user/project/highlighting.html
       maximum_text_highlight_size_kilobytes: 512
   ```

1. 提交、推送和合并您的更改到您的默认分支。
