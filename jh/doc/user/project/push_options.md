---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, howto
---

# 推送选项 **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/15643) in GitLab 11.7.
-->

极狐GitLab 支持使用客户端 [Git 推送选项](https://git-scm.com/docs/git-push#Documentation/git-push.txt--oltoptiongt) 在推送更改的同时执行各种操作。此外，[推送规则](repository/push_rules.md)提供服务器端控制和执行选项。

目前，有推送选项可用于：

- [跳过 CI 作业](#极狐gitlab-cicd-推送选项)
- [合并请求](#合并请求推送选项)

NOTE:
Git 推送选项仅适用于 Git 2.10 或更新版本。

对于 Git 版本 2.10 到 2.17，使用`--push-option`：

```shell
git push --push-option=<push_option>
```

对于 2.18 及更高版本，您可以使用上述格式，或者更短的 `-o`：

```shell
git push -o <push_option>
```

## 极狐GitLab CI/CD 推送选项

您可以使用推送选项跳过 CI/CD 流水线，或传递 CI/CD 变量。

| 推送选项                    | 描述                                                                                 | 引入版本 |
| ------------------------------ | ------------------------------------------------------------------------------------------- |---------------------- |
| `ci.skip`                      | 不要为最新推送创建 CI 流水线。只跳过分支流水线而不是[合并请求流水线](../../ci/pipelines/merge_request_pipelines.md)。                                            | 11.7 |
| `ci.variable="<name>=<value>"` | 提供 CI/CD 变量<!--[CI/CD 变量](../../ci/variables/index.md)-->以在 CI 流水线中使用（如果由于推送而创建）。 | 12.6 |

使用 `ci.skip` 的示例：

```shell
git push -o ci.skip
```

为流水线传递一些 CI/CD 变量的示例：

```shell
git push -o ci.variable="MAX_RETRIES=10" -o ci.variable="MAX_TIME=600"
```

## 合并请求推送选项

您可以使用 Git 推送选项在推送更改的同时对合并请求执行某些操作：

| 推送选项                                  | 描述                                                                                                     | 引入版本 |
| -------------------------------------------- | --------------------------------------------------------------------------------------------------------------- | --------------------- |
| `merge_request.create`                       | 为推送的分支创建一个新的合并请求。                                                               | 11.10 |
| `merge_request.target=<branch_name>`         | 将合并请求的目标设置为特定分支，或上游项目，比如：`git push -o merge_request.target=project_path/branch`                                                     | 11.10 |
| `merge_request.merge_when_pipeline_succeeds` | 将合并请求设置为流水线成功时合并<!--[流水线成功时合并](merge_requests/merge_when_pipeline_succeeds.md)-->。    | 11.10 |
| `merge_request.remove_source_branch`         | 设置合并请求以在合并时删除源分支。                                             | 12.2          |
| `merge_request.title="<title>"`              | 设置合并请求的标题。例如：`git push -o merge_request.title="我想要的标题"`。                   | 12.2          |
| `merge_request.description="<description>"`  | 设置合并请求的描述。例如：`git push -o merge_request.description="我想要的描述"`。 | 12.2         |
| `merge_request.draft`                        | 将合并请求标记为 draft。例如：`git push -o merge_request.draft`。                                      | 15.0          |
| `merge_request.milestone="<milestone>"`      | 设置合并请求的里程碑。例如：`git push -o merge_request.milestone="3.0"`。                        | 14.1       |
| `merge_request.label="<label>"`              | 向合并请求添加标签。如果标签不存在，则创建它。例如，对于两个标签：`git push -o merge_request.label="label1" -o merge_request.label="label2"`。 | 12.3 |
| `merge_request.unlabel="<label>"`            | 从合并请求中删除标签。例如，对于两个标签：`git push -o merge_request.unlabel="label1" -o merge_request.unlabel="label2"`。 | 12.3 |
| `merge_request.assign="<user>"`              | 将用户分配给合并请求。接受用户名或用户 ID。例如，对于两个用户：`git push -o merge_request.assign="user1" -o merge_request.assign="user2"`。对用户名的支持添加于 15.5 版本。 | 13.10 |
| `merge_request.unassign="<user>"`            | 从合并请求中删除分配的用户。接受用户名或用户 ID。例如，对于两个用户：`git push -o merge_request.unassign="user1" -o merge_request.unassign="user2"`。对用户名的支持添加于 15.5 版本。 | 13.10 |

如果您使用要求文本中包含空格的推送选项，则需要将其括在引号 (`"`) 中。如果没有空格，您可以省略引号。一些示例：

```shell
git push -o merge_request.label="Label with spaces"
git push -o merge_request.label=Label-with-no-spaces
```

通过使用多个 `-o`（或 `--push-option`）标志，您可以组合推送选项以一次完成多个任务。例如，如果想创建一个新的合并请求，并定位一个名为 `my-target-branch` 的分支：

```shell
git push -o merge_request.create -o merge_request.target=my-target-branch
```

此外，如果您希望在流水线成功后立即合并合并请求，您可以执行以下操作：

```shell
git push -o merge_request.create -o merge_request.target=my-target-branch -o merge_request.merge_when_pipeline_succeeds
```

## 有用的 Git 别名

如上所示，Git 推送选项会导致 Git 命令变得很长。如果您经常使用相同的推送选项，创建 [Git 别名](https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases) 会很有用。Git 别名是 Git 的命令行快捷方式，可以显着简化长 Git 命令的使用。

### 流水线成功时合并别名

为[流水线成功时合并的 Git 推送选项](#合并请求推送选项)设置一个 Git 别名：

```shell
git config --global alias.mwps "push -o merge_request.create -o merge_request.target=master -o merge_request.merge_when_pipeline_succeeds"
```

然后快速推送以默认分支为目标的本地分支，并在流水线成功时合并：

```shell
git mwps origin <local-branch-name>
```
