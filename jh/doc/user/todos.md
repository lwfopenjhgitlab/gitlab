---
disqus_identifier: 'https://docs.gitlab.com/ee/workflow/todos.html'
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 待办事项列表 **(FREE)**

您的*待办事项列表*是等待您输入的按时间顺序排列的项目列表。
这些项目被称为*待办事项*。

您可以使用待办事项列表来跟踪与以下相关的[操作](#actions-that-create-to-do-items)：

- [议题](project/issues/index.md)
- [合并请求](project/merge_requests/index.md)
- [史诗](group/epics/index.md)
- [设计](project/issues/design_management.md)

## 访问待办事项列表

要访问您的待办事项列表：

在顶部栏的右上角，选择待办事项列表 (**{task-done}**)。

## 搜索待办事项列表

您可以通过 `to do` 和 `done` 搜索您的待办事项列表。

您可以按项目、作者、类型和操作过滤待办事项。
此外，您可以按[**标记优先级**](project/labels.md#set-label-priority)、**最近创建** 和 **最早创建** 对它们进行排序。

<a id="actions-that-create-to-do-items"></a>

## 创建待办事项的操作

许多待办事项是自动创建的。
将待办事项添加到待办事项列表中的一些操作：

- 向您分配了一个议题或合并请求。
- 请求[合并请求评审](project/merge_requests/reviews/index.md)。
- 您在议题、合并请求或史诗的描述或评论中被提及<!--[提及](project/issues/issue_data_and_actions.md#提及)-->。
- 在提交或设计的评论中提到了您。
- 合并请求的 CI/CD 流水线失败。
- 由于冲突，无法合并打开的合并请求，并且存在以下情况之一：
  - 您是作者。
  - 您是将合并请求设置为在流水线成功后自动合并的用户。
- 在 13.2 及更高版本，合并请求从合并队列<!--[合并队列](../ci/pipelines/merge_trains.md)-->删除，并且您是添加它的用户。
- 在 15.8 及更高版本，对您所拥有的群组或项目提出的成员访问请求。

当同一用户在同一对象上发生多个操作时，系统会将第一个操作显示为单个待办事项。要更改此行为，启用
[每个对象有多个待办事项](#multiple-to-do-items-per-object)。

待办事项不受[通知电子邮件设置](profile/notifications.md)的影响。

<a id="multiple-to-do-items-per-object"></a>

### 每个对象有多个待办事项 **(FREE SELF)**

<!-- When the feature flag is removed, integrate this topic into the one above. -->

> - 引入于 13.8 版本，功能标志名为 `multiple_todos`。默认禁用。
> - 引入于 14.9 版本：仅提及创建多个待办事项。

<!--
FLAG:
On self-managed GitLab, by default this feature is not available. To make it available per user,
ask an administrator to [enable the feature flag](../administration/feature_flags.md) named `multiple_todos`.
On GitLab.com, this feature is not available.
-->

该功能尚未准备好用于生产用途。

当您启用此功能时：

- 每次提到您时，极狐GitLab 都会为您创建一个新的待办事项。
- 其它[创建待办事项的操作](#actions-that-create-to-do-items)，针对议题、MR 等的每种操作类型创建一个待办事项。

## 创建待办事项

> 对 OKR 和任务的支持引入于 16.0 版本。

您可以手动将项目添加到您的待办事项列表。

1. 转到您的：

   - [议题](project/issues/index.md)
   - [合并请求](project/merge_requests/index.md)
   - [史诗](group/epics/index.md)
   - [设计](project/issues/design_management.md)
   - [事件（Incident）](../operations/incident_management/incidents.md)
   - [目标和关键结果（OKR）](../user/okrs.md)
   - [任务](tasks.md)

1. 在右上角，选择 **添加待办事项**。

   ![Adding a to-do item from the issuable sidebar](img/todos_add_todo_sidebar_v14_1.png)

   ![Adding a to-do item from the Objective and Key results](img/todos_add_okrs_v16_0.png)

## 通过提及某人来创建待办事项

您可以通过在除代码块之外的任何地方提及某人来创建待办事项。在一条消息中多次提及用户只会创建一个待办事项。

例如，从下面的评论中，除了 `frank` 之外的每个人都会得到一个为他们创建的待办事项：

````markdown
@alice What do you think? cc: @bob

- @carol can you please have a look?

> @dan what do you think?

Hey @erin, this is what they said:

```
Hi, please message @frank :incoming_envelope:
```
````

## 将待办事项标记为已完成的操作

对待办事项对象上的各种操作（如议题、合并请求或史诗），会将其相应的待办事项标记为已完成。

将待办事项标记为已完成的操作包括：

- 在描述或评论中添加表情回复。
- 添加或删除标记。
- 更改指派人。
- 更改里程碑。
- 关闭待办事项的对象。
- 创建评论。
- 编辑描述。
- 解决[设计讨论主题](project/issues/design_management.md#resolve-a-discussion-thread-on-a-design)。
- 接受或拒绝项目或群组成员资格请求。

在以下情况下，待办事项**不会**标记为已完成：

- 添加关联项（例如[关联议题](project/issues/related_issues.md)）。
- 添加子项（例如[子史诗](group/epics/manage_epics.md#multi-level-child-epics)或[任务](tasks.md)）。
- 添加一个[时间条目](project/time_tracking.md)。
- 指派给您自己
- 更改[健康状态](project/issues/managing_issues.md#health-status)。

如果其他人关闭、合并或对议题、合并请求或史诗采取行动，您的待办事项仍处于待处理状态。

## 将待办事项标记为已完成

您可以手动将待办事项标记为已完成。

有两种方法可以做到这一点：

- 在待办事项列表的待办事项右侧，选择 **标记为完成**（**{check}**）。
- 在议题、合并请求或史诗页面的右上角，选择 **标记为完成**。

  ![Mark as done from the sidebar](img/todos_mark_done_sidebar_v14_1.png)

  ![Mark as done from the Objectives and Key results](img/todos_mark_done_okrs_v16_0.png)

## 将所有待办事项标记为已完成

您可以同时将所有待办事项标记为已完成。

在待办事项列表的右上角，选择**全部标记为已完成**。

## 当用户的访问权限发生变化时，用户的待办事项列表如何受到影响

出于安全原因，当用户不再有权访问相关资源时，系统会删除待办事项。
例如，如果用户不再有权访问议题、合并请求、史诗、项目或群组，系统会删除相关的待办事项。

此过程发生在他们的访问权限更改后的一小时内。删除被延迟以防止数据丢失，防止用户的访问被意外撤销。
