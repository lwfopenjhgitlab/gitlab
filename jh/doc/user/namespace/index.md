---
stage: Manage
group: Workspace
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 命名空间

在极狐GitLab 中，命名空间提供了一个地方来组织您的相关项目。一个命名空间中的项目与其他命名空间中的项目是分开的，这意味着您可以为不同命名空间中的项目使用相同的名称。

- 个人命名空间，基于您的用户名，并在您创建帐户时提供给您。
  - 如果您更改用户名，您帐户中的项目和命名空间 URL 也会更改。在更改用户名之前，请阅读[仓库重定向](../project/repository/index.md#what-happens-when-a-repository-path-changes)。
  - 您不能在个人命名空间中创建子组。
  - 您命名空间中的群组不会继承您的命名空间权限和群组功能。
  - 创建的所有个人项目都将属于此命名空间的范围。

- 群组或子组命名空间：
  - 您可以创建多个子组来管理多个项目。
  - 您可以更改群组和子组命名空间的 URL。
  - 您可以专门为命名空间中的每个子组和项目配置设置。
  - 创建子组时，它会继承一些父组设置。您可以在子组**设置**中查看这些内容。

要确定您查看的是群组命名空间还是个人命名空间，您可以查看 URL。例如：

| 命名空间用于 | URL | 命名空间 |
| ------------- | --- | --------- |
| 用户 `alex` | `https://gitlab.example.com/alex` | `alex` |
| 群组 `alex-team` | `https://gitlab.example.com/alex-team` | `alex-team` |
| 群组 `alex-team` 下的子组 `marketing` |  `https://gitlab.example.com/alex-team/marketing` | `alex-team/marketing` |
