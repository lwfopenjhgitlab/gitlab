---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# AsciiDoc **(FREE)**

极狐GitLab 使用 [Asciidoctor](https://asciidoctor.org) gem 将 AsciiDoc 内容转换为 HTML5。
请参阅 [Asciidoctor 用户手册](https://asciidoctor.org/docs/user-manual/) 以获取完整的 Asciidoctor 参考。

## 句法

这是最常用的 AsciiDoc 语法的简要参考。
您可以在 <https://asciidoctor.org/docs/> 找到 AsciiDoc 语法的完整文档。

### 段落

```plaintext
A normal paragraph.
Line breaks are not preserved.
```

行注释，即以 `//` 开头的行，将被跳过：

```plaintext
// this is a comment
```

一个空行分隔段落。

带有 `[%hardbreaks]` 选项的段落会保留换行符：

```plaintext
[%hardbreaks]
This paragraph carries the `hardbreaks` option.
Notice how line breaks are now preserved.
```

缩进（文字）段落禁用文本格式，保留空格和换行符，并以固定宽度字体显示：

```plaintext
 This literal paragraph is indented with one space.
 As a consequence, *text formatting*, spaces,
 and lines breaks will be preserved.
```

提示段落抓住了读者的注意力：

- `NOTE: This is a brief reference, please read the full documentation at https://asciidoctor.org/docs/.`
- `TIP: Lists can be indented. Leading whitespace is not significant.`

### 文本格式

**受限制（应用于单词边界）**

```plaintext
*strong importance* (aka bold)
_stress emphasis_ (aka italic)
`monospaced` (aka typewriter text)
"`double`" and '`single`' typographic quotes
+passthrough text+ (substitutions disabled)
`+literal text+` (monospaced with substitutions disabled)
```

**不受约束（适用于任何地方）**

```plaintext
**C**reate+**R**ead+**U**pdate+**D**elete
fan__freakin__tastic
``mono``culture
```

**替换**

```plaintext
A long time ago in a galaxy far, far away...
(C) 1976 Arty Artisan
I believe I shall--no, actually I won't.
```

**宏**

```plaintext
// where c=specialchars, q=quotes, a=attributes, r=replacements, m=macros, p=post_replacements
The European icon:flag[role=blue] is blue & contains pass:[************] arranged in a icon:circle-o[role=yellow].
The pass:c[->] operator is often referred to as the stabby lambda.
Since `pass:[++]` has strong priority in AsciiDoc, you can rewrite pass:c,a,r[C++ => C{pp}].
// activate stem support by adding `:stem:` to the document header
stem:[sqrt(4) = 2]
```

### 属性

**用户定义的属性**

```plaintext
// define attributes in the document header
:name: value
```

```plaintext
:url-gem: https://rubygems.org/gems/asciidoctor

You can download and install Asciidoctor {asciidoctor-version} from {url-gem}.
C{pp} is not required, only Ruby.
Use a leading backslash to output a word enclosed in curly braces, like \{name}.
```

**环境属性**

极狐GitLab 设置以下环境属性：

| 属性       | 描述                                                                                                            |
| :-------------- | :--------------------------------------------------------------------------------------------------------------------- |
| `docname`       | 源文档的根名称（没有前导路径或文件扩展名）。                                                  |
| `outfilesuffix` | 与后端输出相对应的文件扩展名（默认为 `.adoc` 以使文档间交叉引用工作）。 |

### 链接

```plaintext
https://example.org/page[A webpage]
link:../path/to/file.txt[A local file]
xref:document.adoc[A sibling document]
mailto:hello@example.org[Email to say hello!]
```

### 锚点

```plaintext
[[idname,reference text]]
// or written using normal block attributes as `[#idname,reftext=reference text]`
A paragraph (or any block) with an anchor (aka ID) and reftext.

See <<idname>> or <<idname,optional text of internal link>>.

xref:document.adoc#idname[Jumps to anchor in another document].

This paragraph has a footnote.footnote:[This is the text of the footnote.]
```

### 列表

#### 无序

```plaintext
* level 1
** level 2
*** level 3
**** level 4
***** level 5
* back at level 1
+
Attach a block or paragraph to a list item using a list continuation (which you can enclose in an open block).

.Some Authors
[circle]
- Edgar Allen Poe
- Sheri S. Tepper
- Bill Bryson
```

#### 有序

```plaintext
. Step 1
. Step 2
.. Step 2a
.. Step 2b
. Step 3

.Remember your Roman numerals?
[upperroman]
. is one
. is two
. is three
```

#### 清单

```plaintext
* [x] checked
* [ ] not checked
```

<!-- vale gitlab.Spelling = NO -->

#### 标注

<!-- vale gitlab.Spelling = YES -->

```plaintext
// enable callout bubbles by adding `:icons: font` to the document header
[,ruby]
----
puts 'Hello, World!' # <1>
----
<1> Prints `Hello, World!` to the console.
```

#### 描述

```plaintext
first term:: description of first term
second term::
description of second term
```

### 文档结构

#### Header

```plaintext
= Document Title
Author Name <author@example.org>
v1.0, 2019-01-01
```

#### Sections

```plaintext
= Document Title (Level 0)
== Level 1
=== Level 2
==== Level 3
===== Level 4
====== Level 5
== Back at Level 1
```

#### Includes

NOTE:
以 AsciiDoc 格式创建的 [Wiki 页面](project/wiki/index.md#create-a-new-wiki-page)，以文件扩展名 `.asciidoc` 保存。使用 AsciiDoc wiki 页面时，将文件名从 `.adoc` 更改为 `.asciidoc`。

```plaintext
include::basics.adoc[]

// define -a allow-uri-read to allow content to be read from URI
include::https://example.org/installation.adoc[]
```

为了保证良好的系统性能并防止恶意文档引起问题，极狐GitLab 对任何一个文档中处理的包含指令的数量实施了最大限制。您最多可以包含 32 个文档，其中包括传递依赖项。

### Blocks

```plaintext
--
open - a general-purpose content wrapper; useful for enclosing content to attach to a list item
--
```

```plaintext
// recognized types include CAUTION, IMPORTANT, NOTE, TIP, and WARNING
// enable admonition icons by setting `:icons: font` in the document header
[NOTE]
====
admonition - a notice for the reader, ranging in severity from a tip to an alert
====
```

```plaintext
====
example - a demonstration of the concept being documented
====
```

```plaintext
.Toggle Me
[%collapsible]
====
collapsible - these details are revealed by clicking the title
====
```

```plaintext
****
sidebar - auxiliary content that can be read independently of the main content
****
```

```plaintext
....
literal - an exhibit that features program output
....
```

```plaintext
----
listing - an exhibit that features program input, source code, or the contents of a file
----
```

```plaintext
[,language]
----
source - a listing that is embellished with (colorized) syntax highlighting
----
```

````plaintext
\```language
fenced code - a shorthand syntax for the source block
\```
````

```plaintext
[,attribution,citetitle]
____
quote - a quotation or excerpt; attribution with title of source are optional
____
```

```plaintext
[verse,attribution,citetitle]
____
verse - a literary excerpt, often a poem; attribution with title of source are optional
____
```

```plaintext
++++
pass - content passed directly to the output document; often raw HTML
++++
```

```plaintext
// activate stem support by adding `:stem:` to the document header
[stem]
++++
x = y^2
++++
```

```plaintext
////
comment - content which is not included in the output document
////
```

### Tables

```plaintext
.Table Attributes
[cols=>1h;2d,width=50%,frame=topbot]
|===
| Attribute Name | Values

| options
| header,footer,autowidth

| cols
| colspec[;colspec;...]

| grid
| all \| cols \| rows \| none

| frame
| all \| sides \| topbot \| none

| stripes
| all \| even \| odd \| none

| width
| (0%..100%)

| format
| psv {vbar} csv {vbar} dsv
|===
```

### Colors

可以使用颜色指示器呈现以 `HEX`、`RGB` 或 `HSL` 格式编写的颜色。
支持的格式（不支持命名颜色）：

- `HEX`：`` `#RGB[A]` `` 或 `` `#RRGGBB[AA]` ``
- `RGB`：`` `RGB[A](R, G, B[, A])` ``
- `HSL`：`` `HSL[A](H, S, L[, A])` ``

写在反引号内的颜色后面是颜色方块：

```plaintext
- `#F00`
- `#F00A`
- `#FF0000`
- `#FF0000AA`
- `RGB(0,255,0)`
- `RGB(0%,100%,0%)`
- `RGBA(0,255,0,0.3)`
- `HSL(540,70%,50%)`
- `HSLA(540,70%,50%,0.3)`
```

### 方程式和公式（STEM）

如果您需要包含科学、技术、工程和数学 (STEM) 表达式，请将文档标题中的 `stem` 属性设置为 `latexmath`。
使用 [KaTeX](https://katex.org/) 呈现方程和公式：

```plaintext
:stem: latexmath

latexmath:[C = \alpha + \beta Y^{\gamma} + \epsilon]

[stem]
++++
sqrt(4) = 2
++++

A matrix can be written as stem:[[[a,b\],[c,d\]\]((n),(k))].
```

### 图表和流程图

可以使用 [Mermaid](https://mermaidjs.github.io/) 或 [PlantUML](https://plantuml.com)，从极狐GitLab 中的文本生成图表和流程图。

#### Mermaid

> 引入于 13.3 版本。

更多详情请访问[官方页面](https://mermaidjs.github.io/)。
如果您不熟悉使用 Mermaid 或需要帮助识别 Mermaid 代码中的问题，[Mermaid 实时编辑器](https://mermaid-js.github.io/mermaid-live-editor/) 是一个有用的创建工具，并解决 Mermaid 图中的问题。

要生成图表或流程图，请在 `Mermaid` 块中输入您的文本：

```plaintext
[mermaid]
----
graph LR
    A[Square Rect] -- Link text --> B((Circle))
    A --> C(Round Rect)
    B --> D{Rhombus}
    C --> D
----
```

#### Kroki

Kroki 支持十几个图表库。
为了使 Kroki 在极狐GitLab 中可用，极狐GitLab 管理员需要先启用它。
在 [Kroki 集成](../administration/integration/kroki.md)页面中阅读更多内容。

启用 Kroki 后，您可以在 AsciiDoc 和 Markdown 文档中创建图表。
这是使用 GraphViz 图的示例：

**AsciiDoc**

```plaintext
[graphviz]
....
digraph G {
  Hello->World
}
....
```

**Markdown**

````markdown
```graphviz
digraph G {
  Hello->World
}
```
````

#### PlantUML

在 SaaS 上启用了 PlantUML 集成。为了使 PlantUML 在极狐GitLab 的私有化部署实例中可用，极狐GitLab 管理员[必须启用它](../administration/integration/plantuml.md)。

启用 PlantUML 后，在 `plantuml` 块中输入您的文本：

```plaintext
[plantuml]
----
Bob -> Alice : hello
----
```

<!--
### Multimedia

```plaintext
image::screenshot.png[block image,800,450]

Press image:reload.svg[reload,16,opts=interactive] to reload the page.

video::movie.mp4[width=640,start=60,end=140,options=autoplay]
```

GitLab does not support embedding YouTube and Vimeo videos in AsciiDoc content.
Use a standard AsciiDoc link:

```plaintext
https://www.youtube.com/watch?v=BlaZ65-b7y0[Link text for the video]
```
-->


### Breaks

```plaintext
// thematic break (aka horizontal rule)
---
```

```plaintext
// page break
<<<
```
