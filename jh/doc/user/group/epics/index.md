---
type: reference, howto
stage: Plan
group: Product Planning
info: 要确定分配给与此页面相关的阶段/群组的技术作家，请参见 https://about.gitlab.cn/handbook/engineering/ux/technical-writing/#assignments
---


# 史诗 **(PREMIUM)**

当[议题](../../project/issues/index.md)跨项目和里程碑共享一个主题时，您可以用史诗来管理它们。

您还可以创建子史诗，并指定开始和结束日期，将为您创建一个可视化的路线图来查看进度。

使用史诗：

- 当您的团队在做一个大的功能时，涉及到在一个群组<!--[群组](.../index.md)-->中的不同项目中的不同问题的多个讨论。
- 跟踪该组问题的工作何时开始和结束。
- 要在高层次上讨论并协作相关功能的想法和范围。

## 史诗与议题的关系

史诗和议题之间可能存在的关系是：

- 一个史诗包含一个或多个议题。
- 一个史诗包含一个或多个子史诗。详见[多级子史诗](manage_epics.md#多级子史诗)。

```mermaid
graph TD
    Parent_epic --> Issue1
    Parent_epic --> Child_epic
    Child_epic --> Issue2
```

### 来自不同群组层次结构的子议题

<!-- When feature flag is removed, integrate this info as a sentence in
https://docs.gitlab.com/ee/user/group/epics/manage_epics.html#add-an-existing-issue-to-an-epic -->

> - 引入于 15.5 版本，功能标志为 `epic_issues_from_different_hierarchies`。默认禁用。
> - 在 SaaS 上启用于 15.5 版本。
> - 功能标志 `epic_issues_from_different_hierarchies` 删除于 15.6 版本。

您可以将来自不同群组层次结构的议题添加到史诗中。在[添加现有议题](manage_epics.md#add-an-existing-issue-to-an-epic)时，粘贴议题的 URL 即可。

## 史诗的路线图 **(ULTIMATE)**

> - 极狐GitLab 自 15.2 版本开始，将该功能从旗舰版降入专业版。而 GitLab CE 与 EE 版本中，该功能依旧为旗舰版。
> - 极狐GitLab 自 15.8 版本开始，将该功能从专业版调整为旗舰版。

如果您的史诗包含一个或多个[子史诗](manage_epics.md#多级子史诗)，并且有一个开始或到期的日期，一个可视化的父史诗包含子史诗的[路线图](../roadmap/index.md)如下所示。

![子史诗路线图](img/epic_view_roadmap_v12_9.png)

## 相关话题

- [管理史诗](manage_epics.md)和多级子史诗。
- 根据一种关系类型关联[相关史诗](linked_epics.md)。
- 用[史诗看板](epic_boards.md)创建工作流。
<!--- [打开通知](.../.../profile/notifications.md)以了解史诗事件。-->
- 为史诗或其评论[奖励表情符号](../../award_emojis.md)。
- 通过在[主题帖](../../discussions/index.md)中发表评论，在史诗中进行合作。
- 使用[健康状态](../../project/issues/managing_issues.md#健康状态)来跟踪您的进展。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
