---
stage: Plan
group: Product Planning
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 关联史诗 **(ULTIMATE)**

> - 引入于 14.9 版本，功能标志名为 `related_epics_widget`。默认启用。
> - 功能标志 `related_epics_widget` 移除于 15.0 版本。

关联史诗是任意两个史诗之间的双向关系，并出现在史诗描述下方的块中。您可以关联不同群组中的史诗。

仅当用户可以看到两个史诗时，该关系才会显示在 UI 中。当您尝试关闭已打开阻止程序的史诗时，会显示警告。

<!--
NOTE:
To manage linked epics through our API, visit the [epic links API documentation](../../../api/linked_epics.md).
-->

<a id="add-a-linked-epic"></a>

## 添加关联史诗

先决条件：

- 您必须至少具有两个群组的报告者角色。
- 对于 SaaS：您正在编辑的史诗必须在订阅旗舰版的群组中。您关联的史诗可以位于较低级别版本的群组中。

要将史诗与另一个史诗关联起来：

1. 在史诗的 **关联史诗**部分，选择添加关联史诗按钮（**{加号}**）。
1. 选择两个史诗之间的关系：
    - **涉及到**
    - **[阻止](#blocking-epics)**
    - **[已被阻止。阻止项为](#blocking-epics)**
1. 要输入关联的史诗，请执行以下任一操作：

    - 输入 `&`，后跟史诗的编号。例如，`&123`。
    - 输入 `&`，然后输入史诗标题中的单词。例如，`&Deliver`。
    - 粘贴史诗的完整 URL。

   ![Adding a related epic](img/related_epics_add_v14_9.png)

   同一群组的史诗可以仅通过参考编号指定。来自不同群组的史诗需要额外的信息，例如群组名。例如：

   - 同一群组：`&44`
   - 不同群组：`group&44`

   有效的会被添加到您可以查看的临时列表中。

1. 选择 **添加**。

然后，关联的史诗会显示在按关系分组的史诗上。

![Related epic block](img/related_epic_block_v14_9.png)

## 删除关联史诗

先决条件：

- 您必须至少具有史诗所在群组的报告者角色。

要删除关联的史诗，请在史诗的 **关联史诗** 部分，选择每个史诗旁边的 **删除** (**{close}**)。

这种关系已从两个史诗中删除。

![Removing a related epic](img/related_epics_remove_v14_9.png)

<a id="blocking-epics"></a>

## 阻止史诗

当您[添加关联的史诗](#add-a-linked-epic) 时，您可以显示它**阻止**另一个史诗或被另一个史诗阻止。

如果您尝试使用“关闭史诗”按钮关闭被阻止的史诗，则会出现一条确认消息。
