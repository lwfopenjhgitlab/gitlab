---
type: reference
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 群组的议题分析 **(PREMIUM)**

议题分析是一个条形图，说明了每个月创建的议题数量。
默认时间跨度为 13 个月，包括当前月份和前 12 个月。

要访问图表：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **分析 > 议题分析**。

将鼠标悬停在每个栏上，查看议题总数。

要缩小图表中包含的议题范围，请在 **搜索或过滤结果...** 字段中输入您的条件。可以输入或从菜单中选择：

- 作者（Author）
- 指派人（Assignee）
- 里程碑（Milestone）
- 标记（label）
- 我的反应（My reaction）
- 权重（Weight）

您可以通过设置 URL 参数来更改显示的总月数。
例如，`https://jihulab.com/groups/gitlab-cn/-/issues_analytics?months_back=15` 显示群组中的图表，总共 15 个月。

![Issues created per month](img/issues_created_per_month_v12_8_a.png)

## 深入了解信息

您可以通过浏览位于图表下方的表格来检查各个议题的详细信息。

该图表显示了基于全局页面过滤器的前 100 个议题。

![Issues table](img/issues_table_v13_1.png)

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
