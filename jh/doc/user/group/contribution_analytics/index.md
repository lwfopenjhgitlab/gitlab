---
type: reference
stage: Manage
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 贡献分析 **(PREMIUM)**

使用贡献分析，您可以概览您的群组中的贡献事件。

- 分析您的团队在一段时间内的贡献。
- 与可能从额外支持中受益的群组成员一起确定改进机会。

## 查看贡献分析

查看贡献分析：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的组。
1. 在左侧边栏中，选择 **分析 > 贡献**。

## 使用贡献分析

有三个主要的条形图说明了每个群组成员的以下贡献数量：

- 推送事件
- 合并请求
- 关闭的议题

将鼠标悬停在每个栏上，显示特定群组成员的事件数。

![Contribution analytics bar graphs](img/group_stats_graph.png)

## 更改时间段

您可以从以下三个时段中进行选择：

- 上周（默认）
- 上个月
- 最近三个月

从日历下拉列表中选择所需的时间段。

![Contribution analytics choose period](img/group_stats_cal.png)

## 按不同因素排序

每个群组成员的贡献也以表格形式显示。单击列标题，按该列对表格进行排序：

- 成员名字
- 推送事件的数量
- 开放的议题数量
- 已关闭议题的数量
- 打开的 MR 数量
- 合并的 MR 数量
- 关闭的 MR 数量
- 总贡献数

![Contribution analytics contributions table](img/group_stats_table.png)

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
