---
stage: Anti-Abuse
group: Anti-Abuse
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Git 滥用速率限制 **(ULTIMATE SELF)**

> 引入于 15.2 版本，[功能标志](../../../administration/feature_flags.md)为 `limit_unique_project_downloads_per_namespace_user`。默认禁用。

FLAG:
在私有化部署版中，此功能默认不可用。要使其可用，需要管理员[启用功能标志](../../../administration/feature_flags.md) `limit_unique_project_downloads_per_namespace_user`。在 SaaS 版中，此功能可用。

本文是群组级文档。对于私有化部署实例，请参阅[管理员文档](../../admin_area/reporting/git_abuse_rate_limit.md)。

Git 滥用速率限制是一种功能，可以自动封禁在特定时间范围内下载、克隆或派生超过指定数量的群组仓库的用户。封禁用户无法通过 HTTP 或 SSH 访问顶级群组或其任何非公开子组。速率限制也适用于使用[个人](../../../user/profile/personal_access_tokens.md)或[群组访问令牌](../../../user/group/settings/group_access_tokens.md)进行身份验证的用户。对不相关群组的访问不受影响。

Git 滥用速率限制不适用于顶级群组所有者、[部署令牌](../../../user/project/deploy_tokens/index.md)或[部署密钥](../../../user/project/deploy_keys/index.md)。

## 自动封禁通知

如果启用了 `limit_unique_project_downloads_per_namespace_user` 功能标志，则当用户即将被封禁时，所有具有主群组所有者角色的用户都会收到一封电子邮件。

如果禁用自动封禁，当用户超过限制时不会自动封禁用户。但是，具有主群组所有者角色的用户仍会收到通知。在启用自动封禁之前，您可以使用此设置来确定速率限制设置的正确值。

如果启用自动封禁，则当用户即将被封禁时，具有主群组所有者角色的用户会收到一封电子邮件，并且该用户会自动被禁止进入该群组及其子组。

## 配置 Git 滥用速率限制

1. 在左侧边栏中，选择 **设置 > 报告**。
1. 更新 Git 滥用速率限制设置：
   1. 在 **仓库数量** 字段中输入一个大于或等于 `0` 且小于或等于 `10,000` 的数字。此数字指定用户在被封禁之前的指定时间段内可以下载的仓库的最大数量。设置为 `0` 时，禁用 Git 滥用速率限制。
   1. 在 **报告时间段（秒）** 字段中输入一个数字，大于或等于 `0` 且小于或等于 `86,400`（10 天）。此数字指定用户在被禁止之前可以下载最大数量仓库的时间（以秒为单位）。设置为 `0` 时，禁用 Git 滥用速率限制。
   1. 可选。在 **排除的用户** 字段中添加要排除的用户，最多 100 个。被排除的用户不会被自动封禁。
   1. 可选。打开 **当用户超过指定限制时自动禁止用户进入此命名空间** 切换开关，启用自动封禁。
1. 选择 **保存更改**。
