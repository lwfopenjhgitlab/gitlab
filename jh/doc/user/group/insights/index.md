---
type: reference, howto
stage: Manage
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 洞察 **(ULTIMATE)**

配置对您的群组非常重要的洞察。浏览分类、特定时间段内创建或关闭的议题、合并请求的平均合并时间等数据。

![Insights example stacked bar chart](img/insights_example_stacked_bar_chart_v13_11.png)

## 查看您的群组的洞察

要访问您的群组的洞察：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **分析 > 洞察**。

<a id="configure-your-insights"></a>

## 配置您的洞察

系统从[默认配置文件](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/fixtures/insights/default.yml)中读取洞察。
如果您想自定义：

1. 在属于您群组的项目中，创建一个新文件 [`.gitlab/insights.yml`](../../project/insights/index.md)。
1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **分析** 选项卡并找到 **洞察** 部分。
1. 选择包含您的 `.gitlab/insights.yml` 配置文件的项目。
1. 选择 **保存修改**。

## 权限

如果您有权限查看群组，则您有权限查看其洞察。

NOTE:
您无权访问的议题或合并请求（因为您无权访问它们所属的项目，或者因为它们是私密的）将从洞察图表中过滤掉。

<!--
You may also consult the [group permissions table](../../permissions.md#group-members-permissions).
-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
