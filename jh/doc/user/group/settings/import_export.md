---
type: reference
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用文件导出迁移群组（已废弃） **(FREE)**

> - 于 13.0 版本引入，作为实验性功能。可能会在未来版本中更改。
> - 废弃于 14.6 版本。

WARNING:
此功能废弃于 14.6 版本，由[一种不同的迁移方法](../import/index.md)代替。<!--To follow progress on a solution for
[offline environments](../../application_security/offline_deployments/index.md), see
[the relevant issue](https://gitlab.com/gitlab-org/gitlab/-/issues/363406).-->

您可以将群组及其所有相关数据，从一个实例导出到另一个实例。您也可以：

- 使用首选方法[迁移群组](../import/index.md)。
- [使用文件导出迁移项目](../../project/settings/import_export.md)。

## 为群组启用导出

先决条件：

- 您必须拥有该群组的所有者角色。

要为群组启用导入和导出：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **可见性和访问控制**。
1. 在 **导入源** 部分，选中所需来源的复选框。

## 重要注意事项

注意以下事项：

- 导出存储在临时目录中，并由特定工作人员每 24 小时删除一次。
- 要保留导入项目的群组级关系，请先运行群组导入/导出，以允许将项目导入到所需的群组结构中。
- 除非导入到父组中，否则导入的组具有“私有”可见性级别。
- 如果导入到父组中，子组将继承相同级别的可见性，除非另有限制。
- 要保留导入群组的成员列表及其各自的权限，请查看这些群组中的用户。 在导入所需的群组之前确保这些用户存在。

### 导出的内容

以下将被导出：

- 里程碑
- 标记
- 看板和看板列表
- 徽章
- 子组（包括所有上述数据）
- 史诗
  - 史诗资源状态事件（引入于 15.4 版本）
- 事件
- Wikis（引入于 13.9 版本）<!--[Wikis](../../project/wiki/index.md#group-wikis)--> **(PREMIUM SELF)**
- 迭代周期（引入于 15.4 版本）

以下**不会**被导出：

- 项目
- Runner 令牌
- SAML 发现令牌

NOTE:
有关群组导出中持久化的特定数据的更多详细信息，请参阅 [`import_export.yml`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/import_export/group/import_export.yml) 文件。

## 导出群组

群组中具有所有者角色<!--[所有者角色](../../permissions.md)-->的用户可以导出该组的内容：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 在 **高级** 部分，选择 **导出群组**。
1. 导出生成后，您应该会收到一封电子邮件，其中包含压缩 tar 存档中的 [导出内容](#导出的内容) 链接，内容为 NDJSON 格式。
1. 或者，您可以从 UI 下载导出：

    1. 返回您群组的 **设置 > 通用** 页面。
    1. 在 **高级** 部分，选择 **下载导出**。您还可以通过选择 **重新生成导出** 来生成新文件。

NOTE:
管理员可以设置最大导入文件大小，默认为 `0`（无限制）。
作为管理员，您可以修改最大导入文件大小。为此，请使用应用程序设置 API<!--[应用程序设置 API](../../../api/settings.md#change-application-settings)--> 或 Admin UI<!--[Admin UI](../../admin_area/settings/account_and_limit_settings.md)-->。13.8 版本中的默认值已从 50MB 修改到 0。

<!--
### Between CE and EE

You can export groups from the [Community Edition to the Enterprise Edition](https://about.gitlab.com/install/ce-or-ee/) and vice versa.

The Enterprise Edition retains some group data that isn't part of the Community Edition. If you're exporting a group from the Enterprise Edition to the Community Edition, you may lose this data. For more information, see [downgrading from EE to CE](../../../index.md).
-->

## 导入群组

1. 创建一个新群组：
   - 在顶部栏选择 **新建** (**{plus}**) ，然后选择 **新建群组**。
   - 在现有群组的页面上，选择 **新建子组** 按钮。
1. 选择 **导入群组**。
1. 输入您的群组名称。
1. 接受或修改关联的群组 URL。
1. 选择 **选择文件**。
1. 选择您在[导出群组](#导出群组)部分导出的文件。
1. 要开始导入，选择**导入群组**。

操作完成后，将显示您新导入的群组页面。

<a id="compatibility"></a>

## 兼容性

### 14.0+

在 14.0 版本中，项目和群组导出不再支持 JSON 格式。为了留出过渡期，您仍然可以导入任何 JSON 导出。导入和导出的新格式是 NDJSON。

### 13.0+

可以导入从不同的极狐GitLab 部署导出的包。此功能仅限于之前的两个 <!--[小](../../../policy/maintenance.md#versioning)-->小版本<!--，这类似于我们的 [Security Releases](../../. ./policy/maintenance.md#security-releases）-->。

例如：

| 当前版本 | 可以导入导出的包 |
|-----------------|----------------------------------|
| 13.0            | 13.0, 12.10, 12.9                |
| 13.1            | 13.1, 13.0, 12.10                |

<a id="rate-limits"></a>

## 速率限制

为了帮助避免滥用，默认情况下，用户被限制为：

| 请求类型     | 限制                                  |
| ---------------- | ---------------------------------------- |
| 导出          | 每分钟 6 个群组                 |
| 下载导出  | 每个群组每分钟 1 次下载  |
| 导入           | 每分钟 6 个群组             |

<!--GitLab.com may have [different settings](../../gitlab_com/index.md#importexport) from the defaults.-->
