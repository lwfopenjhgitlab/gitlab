---
stage: Manage
group: Authentication and Authorization
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, howto
---

# 群组访问令牌

使用群组访问令牌，您可以使用单个令牌：

- 为群组执行操作。
- 管理群组内的项目。

您可以使用群组访问令牌进行身份验证：

- 使用 GitLab API。
- 在 14.2 及更高版本，通过 HTTPS 使用 Git 进行身份验证
  使用：

  - 任何非空白值作为用户名。
  - 群组访问令牌作为密码。

群组访问令牌类似于[项目访问令牌](../../project/settings/project_access_tokens.md)和[个人访问令牌](../../profile/personal_access_tokens.md)，除了它们与群组相关联，而不是与项目或用户相关联。

在私有化部署版实例中，如果设置了限制，群组访问令牌的[最大生命周期限制](../../admin_area/settings/account_and_limit_settings.md#limit-the-lifetime-of-access-tokens)与个人访问令牌相同。

WARNING:
在 15.4 版本中已弃用创建不过期的群组访问令牌的功能，并计划在 16.0 版本中删除。删除此功能后，计划为不过期的现有群组访问令牌添加到期时间。<!--在 16.0 里程碑期间，GitLab.com 上会自动添加到期。 当自我管理的实例升级到 GitLab 16.0 时，会自动添加到期时间。 这种变化是一个突破性的变化。-->

以下情况，您可以使用群组访问令牌：

- 您在 SaaS 上具有专业版或旗舰版许可证。使用[试用许可证](https://gitlab.cn/free-trial/)，群组访问令牌不可用。
- 在私有化部署实例上，具有任何级别的许可证。如果您有免费版：
  - 查看您围绕[用户自行注册](../../admin_area/settings/sign_up_restrictions.md#disable-new-sign-ups)的安全和合规政策。
  - 考虑[禁用群组访问令牌](#enable-or-disable-group-access-token-creation)以减少潜在的滥用。

您不能使用群组访问令牌来创建其它访问群组、项目或个人令牌。

群组访问令牌继承为个人访问令牌配置的[默认前缀设置](../../admin_area/settings/account_and_limit_settings.md#personal-access-token-prefix)。

NOTE:
群组访问令牌不适用 FIPS，并且在启用 FIPS 模式时，禁止创建和使用。

## 使用 UI 创建群组访问令牌

> - 引入于 14.7 版本
> - 从 15.3 版本开始，默认到期时间为 30 天，并且在 UI 中填充默认的访客角色。
> - 创建不会过期的群组访问令牌的功能删除于 16.0 版本。

WARNING:
项目访问令牌被视为内部用户。如果内部用户创建项目访问令牌，则该令牌能够访问可见性级别设置为[内部](../../public_access.md)的所有项目。

创建群组访问令牌：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **设置 > 访问令牌**。
1. 输入名称。任何有权查看该群组的用户都可以看到令牌名称。
1. 输入令牌的到期日期：
   - 令牌在该日期的午夜 UTC 到期。
   - 如果您不输入到期日期，到期日期将自动设置为比当前日期晚 365 天。
   - 默认情况下，此日期最多可以比当前日期晚 365 天。
   - 实例范围的[最长生命周期](../../admin_area/settings/account_and_limit_settings.md#limit-the-lifetime-of-access-tokens)设置可以限制私有化部署实例中允许的最长生命周期。
1. 选择令牌的角色。
1. 选择[所需范围](#scopes-for-a-group-access-token)。
1. 选择 **创建群组访问令牌**。

显示群组访问令牌。将群组访问令牌保存在安全的地方。离开或刷新页面后，您将无法再次查看。

## 使用 Rails 控制台创建群组访问令牌

14.6 及更早版本不支持使用 UI 或 API 创建群组访问令牌。但是，管理员可以使用以下解决方法：

1. 在 [Rails 控制台](../../../administration/operations/rails_console.md)中运行以下命令：

   ```ruby
   # Set the GitLab administration user to use. If user ID 1 is not available or is not an administrator, use 'admin = User.admins.first' instead to select an administrator.
   admin = User.find(1)

   # Set the group group you want to create a token for. For example, group with ID 109.
   group = Group.find(109)

   # Create the group bot user. For further group access tokens, the username should be group_#{group.id}_bot#{bot_count}. For example, group_109_bot2 and email address group_109_bot2@example.com.
   bot = Users::CreateService.new(admin, { name: 'group_token', username: "group_#{group.id}_bot", email: "group_#{group.id}_bot@example.com", user_type: :project_bot }).execute

   # Confirm the group bot.
   bot.confirm

   # Add the bot to the group with the required role.
   group.add_member(bot, :maintainer)

   # Give the bot a personal access token.
   token = bot.personal_access_tokens.create(scopes:[:api, :write_repository], name: 'group_token')

   # Get the token value.
   gtoken = token.token
   ```

1. 测试生成的群组访问令牌是否有效：

   1. 将 `PRIVATE-TOKEN` header 中的群组访问令牌与 GitLab REST API 一起使用。例如：

      - 在群组中创建史诗。<!--[Create an epic](../../../api/epics.md#new-epic) in the group.-->
      - 在群组的一个项目中创建项目流水线。<!--[Create a project pipeline](../../../api/pipelines.md#create-a-new-pipeline) in one of the group's projects.-->
      - 在群组的一个项目中创建议题。<!--[Create an issue](../../../api/issues.md#new-issue) in one of the group's projects.-->

   1. 使用群组令牌通过 HTTPS [克隆群组的项目](../../../gitlab-basics/start-using-git.md#clone-with-https)。

## 使用 UI 撤销群组访问令牌

> 引入于 14.7 版本

撤销群组访问令牌：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **设置 > 访问令牌**。
1. 在要撤销的群组访问令牌旁边，选择 **撤销**。

## 使用 Rails 控制台撤销群组访问令牌

14.6 及更早版本不支持使用 UI 或 API 撤销群组访问令牌。但是，管理员可以使用以下解决方法：

在 [Rails 控制台](../../../administration/operations/rails_console.md)中运行以下命令：

```ruby
bot = User.find_by(username: 'group_109_bot') # the owner of the token you want to revoke
token = bot.personal_access_tokens.last # the token you want to revoke
token.revoke!
```

<a id="scopes-for-a-group-access-token"></a>

## 群组访问令牌的范围

范围确定您在使用群组访问令牌进行身份验证时可以执行的操作。

| 范围             | 描述                                                                                                                                                                      |
|:-------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `api`              | 授予对群组和相关项目 API 的完整读写访问权限，包括软件包库。                   |
| `read_api`         | 授予对群组和相关项目 API 的读访问权限，包括软件包库。                                     |
| `read_registry`    | 如果群组中的任何项目是私有的并且需要授权，则允许对 Container Registry 镜像进行读取访问（拉取）。 |
| `write_registry`   | 允许对 Container Registry 的写入访问（推送）。                                                                              |
| `read_repository`  | 允许对群组内的所有仓库进行读取访问（拉取）。                                                                                                                    |
| `write_repository` | 允许对群组内的所有仓库进行读写访问（拉取和推送）。                                                                                                 |

<a id="enable-or-disable-group-access-token-creation"></a>

## 启用或禁用群组访问令牌创建

为顶级群组中的所有子组启用或禁用群组访问令牌创建：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **权限和群组功能**。
1. 在 **权限** 下，打开或关闭 **用户可以在该群组中创建项目访问令牌和群组访问令牌**。

即使禁用创建，您仍然可以使用和撤销现有的群组访问令牌。

<a id="bot-users-for-groups"></a>

## 群组的机器人用户

每次创建组访问令牌时，都会创建一个机器人用户并将其添加到群组中。这些 bot 用户类似于[项目机器人用户](../../project/settings/project_access_tokens.md#bot-users-for-projects)，不同之处在于它们被添加到群组而不是项目中。群组的机器人用户：

- 不计入许可证席位。
- 可以拥有一个群组的最大所有者角色。<!--有关详细信息，请参阅[创建群组访问令牌](../../../api/group_access_tokens.md#create-a-group-access-token)。-->
- 将第一个访问令牌的用户名设置为 `group_{group_id}_bot`。例如，`group_123_bot`。
- 将电子邮件设置为 `group{group_id}_bot@noreply.{Gitlab.config.gitlab.host}`。例如，`group123_bot@noreply.example.com`。


