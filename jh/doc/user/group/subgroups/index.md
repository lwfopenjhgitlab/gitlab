---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 子组 **(FREE)**

您可以将极狐GitLab [群组](../index.md)组织成子组。您可以使用子组：

- 内部和外部组织分开。因为每个子组都可以有自己的可见性级别，您可以在同一个父组下托管用于不同目的的子组。
- 组织大型项目。您可以使用子组来授予对部分源代码的不同访问权限。
- 管理人员并控制可见性。为用户所属的每个群组赋予不同的角色。

子组可以：

- 属于一个直系父组。
- 有许多子组。
- 最多可嵌套 20 层。
- 使用注册到父组的 [runners](../../../ci/runners/index.md)：
   - 为父组配置的 secret 可用于子组作业。
   - 属于子组的项目中具有维护者角色的用户，可以查看注册到父组的 runner 的详细信息。

例如：

```mermaid
graph TD
    subgraph "Parent group"
      subgraph "Subgroup A"
        subgraph "Subgroup A1"
          G["Project E"]
        end
        C["Project A"]
        D["Project B"]
        E["Project C"]
      end
      subgraph "Subgroup B"
        F["Project D"]
      end
    end
```

## 查看群组的子组

先决条件：

- 要查看私有嵌套子组，您必须是私有子组的直接成员或继承成员。

要查看群组的子组：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 选择 **子组和项目** 选项卡。
1. 要查看嵌套子组，请在层次结构列表中展开子组。

### 公开父组中的私有子组

在层次结构列表中，具有私有子组的公开群组，包含用于指示存在子组的所有用户的扩展选项 (**{chevron-down}**)。当不是私有子组的直接或继承成员的用户选择展开 (**{chevron-down}**) 时，不会显示嵌套子组。

如果您希望将有关嵌套子组的存在信息保密，我们建议您仅将私有子组添加到私有父组。

<a id="create-a-subgroup"></a>

## 创建子组

先决条件：

- 您必须具有以下角色之一：
   - 至少具有组的维护者角色，以便为其创建子组。
   - 具有[由设置确定的角色](#change-who-can-create-subgroups)。即使在用户的设置中管理员禁用了群组创建，这些用户也可以创建子组。

NOTE:
您不能使用顶级域名托管 GitLab Pages 子组网站。例如，`subgroupname.example.io`。

创建子组：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到并选择要添加子组的父组。
1. 在父组的概览页面的右上角，选择 **新建子组**。
1. 选择 **创建群组**。
1. 填写字段。查看不能用作组名的[保留名称](../../reserved_names.md)列表。
1. 选择 **创建群组**。

<a id="change-who-can-create-subgroups"></a>

### 更改谁可以创建子组

要创建子组，您必须至少具有该群组的维护者角色，具体取决于群组的设置。默认情况下：

要更改可以在群组上创建子组的人员：

- 作为在组中具有所有者角色的用户：
   1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
   1. 在左侧边栏中，选择 **设置 > 通用**。
   1. 展开 **权限和群组功能**。
   1. 从 **允许创建子组的角色** 中选择一个角色。
   1. 选择 **保存更改**。
- 作为管理员：
   1. 在顶部栏上，选择 **主菜单 > 管理员**。
   1. 在左侧边栏上，选择 **概览 > 群组**。
   1. 在群组的行中选择 **编辑**。
   1. 从 **允许创建子组** 中选择一个角色。
   1. 选择 **保存更改**。

<!--
For more information, view the [permissions table](../../permissions.md#group-members-permissions).
-->

<a id="subgroup-membership"></a>

## 子组成员

将成员添加到群组时，该成员也会添加到所有子组。用户的权限继承自群组的父级。

子组成员可以：

1. 成为子组的直接成员。
1. 从子组的父组继承子组的成员资格。
1. 成为与子组的顶级组共享的群组的成员。

```mermaid
flowchart RL
  subgraph Group A
    A(Direct member)
    B{{Shared member}}
    subgraph Subgroup A
      H(1. Direct member)
      C{{2. Inherited member}}
      D{{Inherited member}}
      E{{3. Shared member}}
    end
    A-->|Direct membership of Group A\nInherited membership of Subgroup A|C
  end
  subgraph Group C
    G(Direct member)
  end
  subgraph Group B
    F(Direct member)
  end
  F-->|Group B\nshared with\nGroup A|B
  B-->|Inherited membership of Subgroup A|D
  G-->|Group C shared with Subgroup A|E
```

成员的群组权限只能通过以下方式更改：

- 在群组中具有所有者角色的用户。
- 更改成员添加到的群组的配置。

### 确定成员继承

查看成员是否继承了父组的权限：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到该群组。
1. 选择 **群组信息 > 成员**。

示例子组 _Four_ 的成员列表：

![Group members page](img/group_members_v14_4.png)

在上面的屏幕截图中：

- 五名成员可以访问群组 *Four*。
- 用户 0 在群组 *Four* 上具有报告者角色，并从群组 *One* 继承了权限：
  - 用户 0 是群组 *One* 的直接成员。
  - 群组 *One* 在层次结构中高于组 *Four*。
- 用户 1 在群组 *Four* 上具有开发人员角色，并从群组 *Two* 继承了权限：
  - 用户 0 是群组 *Two* 的直接成员，该群组是群组 *One* 的子组。
  - 群组 *One / Two* 在层次结构中高于群组 *Four*。
- 用户 2 在群组 *Four* 上具有开发人员角色，并从群组 *Three* 继承了权限：
  - 用户 0 是群组 *Three* 的直接成员，该群组是群组 *Two* 的子组。群组 *Two* 是群组 *One* 的子组。
  - 群组 *One / Two / Three* 在层次结构中高于群组 *Four*。
- 用户 3 是群组 *Four* 的直接成员。这意味着其直接从群组 *Four* 获得维护者角色。
- 管理员在群组 *Four* 上具有所有者角色，并且是所有子组的成员。因此，与用户 3 一样，**源** 列表示其是直接成员。

成员可以按继承或直接成员资格进行过滤。

### 覆盖上级群组成员资格

在子组中具有所有者角色的用户可以向其中添加成员。

您不能为用户分配的子组角色低于他们在上级组中的角色。要覆盖用户在上级组中的角色，请将用户再次添加到具有更高角色的子组中。例如：

- 如果将用户 1 添加到具有开发人员角色的组 *Two* 中，将在组 *Two* 的每个子组中继承该角色。
- 要为用户 1 赋予组 *Four* 的维护者角色（在  *One / Two / Three* 下），请将其再次添加到组 *Four* 并具有维护者角色。
- 如果用户 1 从组 *Four* 中删除，角色将回退到在组 *Two* 上的角色，再次在组 _Four_ 中具有开发人员角色。

## 提及子组

在议题、提交和合并请求中提及子组 (`@<subgroup_name>`) 会通知该群组的所有成员。子组的继承成员不会通过提及来通知。提及的工作方式与项目和群组相同，您可以选择要通知的人员组。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
