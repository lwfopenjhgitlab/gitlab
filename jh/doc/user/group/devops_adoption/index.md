---
stage: Manage
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 群组 DevOps Adoption **(ULTIMATE)**

> - 引入于 13.11 版本，作为 Beta 功能
>  - 功能标志移除于 14.1 版本
> - 概览选项卡引入于 14.1 版本
> - DAST 和 SAST 指标添加于 14.1 版本
> - 模糊测试指标添加于 14.2 版本
> - 依赖项扫描指标添加于 14.2 版本
> - 多选添加于 14.2 版本
> - 概览表格添加于 14.3 版本
>  - 采用随时间变化图表添加于 14.4 版本

DevOps Adoption 向您展示了组织中的团队如何采用和使用极狐GitLab 的最基本功能。

您可以使用群组 DevOps Adoption：

- 确定在采用极狐GitLab 功能方面滞后的特定子组，以便您可以指导他们进行 DevOps 之旅。
- 找到采用某些功能的子组，并指导其它子组如何使用这些功能。
- 验证您是否从极狐GitLab 获得了预期的投资回报。

![DevOps Adoption](img/group_devops_adoption_v14_2.png)

## 查看 DevOps Adoption

先决条件：

- 您必须至少具有该群组的报告者角色。

查看 DevOps Adoption：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **分析 > DevOps adoption**

## DevOps Adoption 类别

DevOps Adoption 展示了开发、安全和运营方面的功能采用情况。

| 类别 | 功能 |
| ---      | ---      |
| 开发   | 批准<br>代码所有者<br>议题<br>合并请求   |
| 安全  | DAST<br>依赖项扫描<br>模糊测试<br>SAST  |
| 运营   | 部署<br>流水线<br>Runners   |

## 功能采用

DevOps Adoption 显示上一个日历月的群组和子组的功能采用数据。

当群组在该时间段内使用项目中的功能时，该功能将显示为 **已采用**。
这包括该群组的任何子组中的项目。例如，在群组的项目中创建了一个议题，则该群组在那个时候已经采用了议题功能。

### 功能采用数据的例外情况

当极狐GitLab 测量 DevOps adoption 时，一些常见的 DevOps 信息不包括在内：

- 休眠项目。群组中有多少项目使用一个特性并不重要。即使您有许多休眠项目，它也不会降低 adoption。
- 新的极狐GitLab 功能。Adoption 是采用的功能总数，而不是功能的百分比。

## 何时收集 DevOps adoption 数据

有一个每周任务处理 DevOps Adoption 的数据。在您首次访问某个群组的 DevOps Adoption 之前，此任务将被禁用。

数据处理任务在每个月的第一天更新数据。如果每月更新失败，任务会每天尝试，直到成功。

在极狐GitLab 处理群组的数据时，DevOps Adoption 数据可能需要一分钟才能显示。

## 查看一段时间内的功能采用情况

**采用随时间推移**图表显示了过去 12 个月采用的功能总数。该图表仅显示您为该群组启用 DevOps Adoption 时的数据。

要查看一段时间内的功能采用情况：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **分析 > DevOps adoption**。
1. 选择 **概述** 选项卡。

工具提示显示有关各个月份跟踪的功能的信息。

## 添加或移除子组

要从 DevOps Adoption 报告中添加或删除子组：

1. 选择 **添加或删除子组**。
1. 选择您要添加或删除的子组，然后选择 **保存修改**。

在极狐GitLab 收集数据时，可能需要一分钟才能显示子组数据。
