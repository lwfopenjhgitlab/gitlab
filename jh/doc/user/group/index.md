---
type: reference, howto
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 群组 **(FREE)**

在极狐GitLab 中，您可以使用群组同时管理一个或多个相关项目。

您可以使用群组来管理项目的权限。如果某人有权访问该群组，则他们可以访问该群组中的所有项目。

您还可以查看群组中项目的所有议题和合并请求，并查看显示群组活动的分析。

您可以使用群组同时与群组的所有成员进行通信。

对于较大的组织，您还可以创建[子组](subgroups/index.md)。

有关创建和管理群组的更多信息，请参阅[管理群组](manage.md)。

## 群组可见性

与项目一样，群组可以配置为将其可见性限制为：

- 匿名用户。
- 所有登录用户。
- 只有明确的群组成员。

应用程序设置级别上对可见性级别<!--[可见性级别](../admin_area/settings/visibility_and_access_controls.md#restrict-visibility-levels)-->的限制也适用于群组。如果设置为内部，则匿名用户的浏览页面为空。群组页面有一个可见性级别图标。

管理员用户不能创建比直接父组可见性级别更高的子组或项目。

<!--
## Related topics

- [Group wikis](../project/wiki/index.md)
- [Maximum artifacts size](../admin_area/settings/continuous_integration.md#maximum-artifacts-size). **(FREE SELF)**
- [Repositories analytics](repositories_analytics/index.md): View overall activity of all projects with code coverage. **(PREMIUM)**
- [Contribution analytics](contribution_analytics/index.md): View the contributions (pushes, merge requests,
  and issues) of group members. **(PREMIUM)**
- [Issue analytics](issues_analytics/index.md): View a bar chart of your group's number of issues per month. **(PREMIUM)**
- Use GitLab as a [dependency proxy](../packages/dependency_proxy/index.md) for upstream Docker images.
- [Epics](epics/index.md): Track groups of issues that share a theme. **(ULTIMATE)**
- [Security Dashboard](../application_security/security_dashboard/index.md): View the vulnerabilities of all
  the projects in a group and its subgroups. **(ULTIMATE)**
- [Insights](insights/index.md): Configure insights like triage hygiene, issues created/closed per a given period, and
  average time for merge requests to be merged. **(ULTIMATE)**
- [Webhooks](../project/integrations/webhooks.md).
- [Kubernetes cluster integration](clusters/index.md).
- [Audit Events](../../administration/audit_events.md#group-events). **(PREMIUM)**
- [Pipelines quota](../admin_area/settings/continuous_integration.md): Keep track of the pipeline quota for the group.
- [Integrations](../admin_area/settings/project_integration_management.md).
- [Transfer a project into a group](../project/settings/index.md#transferring-an-existing-project-into-another-namespace).
- [Share a project with a group](../project/members/share_project_with_groups.md): Give all group members access to the project at once.
- [Lock the sharing with group feature](#prevent-a-project-from-being-shared-with-groups).
- [Enforce two-factor authentication (2FA)](../../security/two_factor_authentication.md#enforce-2fa-for-all-users-in-a-group): Enforce 2FA
  for all group members.
- Namespaces [API](../../api/namespaces.md) and [Rake tasks](../../raketasks/features.md)..
-->

