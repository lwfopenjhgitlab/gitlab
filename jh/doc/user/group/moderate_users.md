---
stage: Anti-Abuse
group: Anti-Abuse
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 修改用户  **(FREE)**

> 引入于 15.8 版本。

这是群组级文档。对于私有化部署实例，请参阅[管理员文档](../admin_area/moderate_users.md)。

群组所有者可以通过封禁和解禁用户，来调节用户访问。

## 解禁用户

<!--
To unban a user with the GraphQL API, see [`Mutation.namespaceBanDestroy`](../../api/graphql/reference/index.md#mutationnamespacebandestroy).

<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For a demo on unbanning a user at the group level, see [Namespace level ban - Unbanning a user](https://www.youtube.com/watch?v=mTQVbP3MQrs).
-->

先决条件：

- 在顶级群组中，您必须具有所有者角色。

解禁用户：

1. 进入顶级群组。
1. 在左侧边栏，选择 **群组信息 > 成员**。
1. 选择 **禁用** 选项卡。
1. 对于您要解禁的帐户，选择 **解禁**。

## 封禁用户

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For a demo on banning a user at the group level, see [Namespace level ban - Banning a user](https://youtu.be/1rbi1uEJmOI).
-->

先决条件：

- 在顶级群组中，您必须具有所有者角色。
- 在顶级群组中，如果要禁止的用户具有所有者角色，则必须[降级用户](manage.md#change-the-owner-of-a-group)。

手动封禁用户：

1. 进入顶级群组。
1. 在左侧边栏，选择 **群组信息 > 成员**。
1. 在您要封禁的成员旁边，选择垂直省略号 (**{ellipsis_v}**)。
1. 从下拉列表中选择 **封禁成员**。
