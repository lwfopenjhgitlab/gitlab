---
stage: Manage
group: Workspace
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 管理群组

使用群组同时管理一个或多个相关项目。

## 查看群组

要查看群组，请在顶部栏上选择 **主菜单 > 群组 > 查看所有群组**。

**群组**页面显示按上次更新日期排序的群组列表。

- 要浏览所有公开群组，请选择 **浏览群组**。
- 要查看您拥有直接或间接成员资格的群组，请选择 **您的群组**。此选项卡也显示以下您所属的群组：
  - 通过子组获取的父组的成员资格。
  - 通过群组或子组中项目的直接或继承成员资格。

<a id="create-a-group"></a>

## 创建一个群组

要创建一个群组：

1. 在顶部导航栏，有两种方式可选：
   - 选择 **主菜单 > 群组 > 查看所有群组**，然后在右侧选择 **新建群组**。
   - 在搜索框的左侧，选择加号，然后选择 **新建群组**。
1. 选择 **创建群组**。
1. 在 **群组名称** 中输入群组的名称。有关不能用作群组名的单词列表，请参阅[保留名称](../reserved_names.md)。
1. 在 **群组 URL** 中输入群组的路径，用于[命名空间](#namespaces)。
1. 选择[可见性级别](../public_access.md)。
1. 通过回答以下问题来个性化您的体验：
   - 您的角色是什么？
   - 谁将使用这个群组？
   - 您会用这个群组做什么？
1. 邀请极狐GitLab 成员或其他用户加入群组。

<a id="remove-a-group"></a>

## 删除群组

要删除群组及其内容：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **高级** 部分。
1. 在 **删除群组** 部分，选择 **删除群组**。
1. 输入群组名。
1. 选择 **确认**。

群组也可以从群组仪表盘中删除：

1. 在顶部栏上，选择 **主菜单 > 群组 > 查看所有群组**。
1. 选择 **您的群组**。
1. 选择要删除的群组对应的 (**{ellipsis_v}**)。
1. 选择 **删除**。
1. 在删除群组部分，选择 **删除群组**。
1. 输入群组名。
1. 选择 **确认**。

此操作将删除该群组。它还添加了一个后台作业来删除组中的所有项目。

特别注意：

- 在 12.8 及更高版本的专业版或更高级别，此操作添加了一个后台作业来标记要删除的群组。默认情况下，作业将删除安排在未来 7 天。您可以通过[实例设置](../admin_area/settings/visibility_and_access_controls.md#deletion-protection)修改这个等待时间。

- 在 13.6 及更高版本，如果设置删除的用户在删除之前从群组中删除，则作业将被取消，该群组不再被安排删除。

## 立即删除群组 **(PREMIUM)**

> 引入于 14.2 版本。

如果您不想等待，可以立即删除一个群组。

先决条件：

- 您必须至少具有群组的所有者角色。
- 您已[标记要删除的组](#remove-a-group)。

立即删除标记为删除的群组：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 在 “永久删除群组” 部分，选择 **删除群组**。
1. 根据要求确认操作。

您的群组、其子组、项目和所有相关资源（包括议题和合并请求）都将被删除。

## 恢复群组 **(PREMIUM)**

要恢复标记为删除的组：

1. 在顶部栏上，选择 **主菜单 > 群组**，并找到您的群组。
1. 选择 **设置 > 通用**。
1. 展开 **路径, 转移, 删除** 部分。
1. 在恢复群组部分，选择 **恢复群组**。

## 请求访问群组

作为用户，如果管理员允许，您可以请求成为群组的成员。

1. 在顶部栏上，选择 **主菜单 > 群组**。
1. 选择 **您的群组**。
1. 找到群组并选择它。
1. 在群组名下，选择 **请求访问**。

多达 10 个最近活跃的群组所有者会收到一封包含您的请求的电子邮件。任何群组所有者都可以批准或拒绝请求。

如果您在请求获得批准之前改变主意，请选择 **撤销访问请求**。

## 过滤和排序群组中的成员

要查找群组中的成员，您可以排序、过滤或搜索。

### 过滤群组

过滤群组以查找成员。默认情况下，显示群组和子组中的所有成员。

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在成员列表上方，在 **筛选成员** 框中，输入筛选条件。
    - 要仅查看群组中的成员，请选择 **成员 = 直接**。
    - 要查看群组及其子组的成员，请选择 **成员 = 继承**。
    - 要查看启用或禁用双重身份验证的成员，请选择 **2FA = 启用** 或 **已禁用**。
    - 在 14.0 及更高版本，要查看由 SAML SSO 或 SCIM provisioning 创建的用户，选择 **Enterprise = true**。

### 搜索群组

您可以按姓名、用户名或电子邮件搜索成员。

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧导航栏，选择 **群组信息 > 成员**。
1. 在成员列表上方，在 **筛选成员** 框中，输入搜索条件。
1. 在 **筛选成员** 框的右侧，选择放大镜 (**{search}**)。

### 对群组中的成员进行排序

您可以按 **帐户**、**已授予访问**、**最大角色** 或 **上次登录** 对成员进行排序。

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧导航栏，选择 **群组信息 > 成员**。
1. 在成员列表上方的右上角，从 **帐户** 列表中，选择筛选条件。
1. 要在升序和降序之间切换排序，在 **帐户** 列表的右侧，选择箭头（**{sort-lowest}** 或 **{sort-highest}**）。

<a id="add-users-to-a-group"></a>

## 添加用户到群组

先决条件： 

- 您必须具有所有者角色。


您可以授予用户访问群组中所有项目的权限。

1. 在顶部栏上，选择 **主菜单 > 群组**。
1. 选择 **您的群组**。
1. 从左侧边栏中，选择 **群组信息 > 成员**。
1. 选择 **邀请成员**。
1. 填写字段。
    - 该角色适用于群组中的所有项目。<!--[了解有关权限的更多信息](../permissions.md)。-->
    - 在**访问过期时间**，用户无法再访问组中的项目。
1. 选择 **邀请**。

未自动添加的成员显示在 **已邀请** 选项卡上。
在此选项卡上的用户：

- 尚未接受邀请。
- 正在等待[管理员批准](../admin_area/moderate_users.md)。
- [超过群组用户上限](#user-cap-for-groups)。

## 从群组中删除成员

先决条件：

- 您必须具有所有者角色。
- 该成员必须是该群组的直接成员。如果成员资格是从父组继承的，则只能从父组中删除该成员。

要从群组中删除成员：

1. 进入群组。
1. 从左侧菜单中，选择 **群组信息 > 成员**。
1. 在您要删除的成员旁边，选择 **删除**。
1. 可选。在 **删除成员** 确认框中：
   - 要从子组和项目中删除直接用户成员资格，请选中 **同时从子组和项目中删除直接用户成员资格** 复选框。
   - 要从关联的议题和合并请求中取消指派用户，请选中 **同时从相关的议题和合并请求中取消指派此用户** 复选框。
1. 选择 **移除成员**。

## 确保删除的用户无法邀请自己回来

具有维护者或所有者角色的恶意用户可以利用有利条件，邀请自己回到极狐GitLab 管理员已将他们从中删除的群组或项目。

为了避免这个问题，极狐GitLab 管理员可以[确保删除的用户不能邀请自己回来](../project/members/index.md#ensure-removed-users-cannot-invite-themselves-back)。

## 添加项目到群组

将新项目添加到群组有两种不同的方法：

- 选择一个群组，然后单击 **新建项目**。然后您可以继续[创建您的项目](../../user/project/index.md#create-a-project)。
- 在创建项目时，从下拉菜单中选择一个群组。

  ![Select group](img/select_group_dropdown_13_10.png)

<a id="specify-who-can-add-projects-to-a-group"></a>

### 指定谁可以将项目添加到群组

默认情况下，至少具有开发者角色的用户可以在一个群组下创建项目。

要为特定群组更改此设置：

1. 在顶部栏上，选择 **主菜单 > 群组 > 查看所有群组**。
1. 选择 **您的群组**。
1. 找到群组并选择它。
1. 从左侧菜单中，选择 **设置 > 通用**。
1. 展开 **权限和群组功能** 部分。
1. 在 **允许创建项目的角色** 下拉列表中选择所需的选项。
1. 单击 **保存更改**。

要全局更改此设置，请参阅[默认项目创建保护](../admin_area/settings/visibility_and_access_controls.md#define-which-roles-can-create-projects)。

## 添加群组 README

作为群组所有者或成员，您可以使用 README 提供有关您的团队的更多信息，并邀请用户为您的项目做出贡献。
README 显示在群组概览页面，可以在群组设置中更改。所有群组成员都可以编辑 README。

先决条件：

- 要在群组设置创建 README 文件，您必须具有该群组的所有者角色。

添加群组 README：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 在 **群组 README** 部分，选择 **添加 README**。此操作创建一个包含 README.md 文件的新项目 `gitlab-profile`。
1. 在创建 README 的提示中，选择 **创建并添加 README**。您将被重定向到 Web IDE，其中创建了一个 README 文件。
1. 在 Web IDE 中，编辑并提交 `README.md` 文件。

<a id="change-the-owner-of-a-group"></a>

## 更改群组的所有者

您可以更改群组的所有者。每个群组必须始终至少有一个具有所有者角色的成员。

- 作为管理员：
   1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
   1. 在左侧导航栏，选择 **群组信息 > 成员**。
   1. 给不同的成员 **所有者** 角色。
   1. 刷新页面。您现在可以删除原来的 **所有者** 角色。
- 作为当前组的所有者：
   1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
   1. 在左侧导航栏，选择 **群组信息 > 成员**。
   1. 给不同的成员 **所有者** 角色。
   1. 让新所有者登录并删除您的 **所有者** 角色。

## 更改群组路径

更改群组的路径（群组 URL）可能会产生意想不到的副作用。在继续之前阅读[重定向文档](../project/repository/index.md#what-happens-when-a-repository-path-changes)。

如果您正在更改路径以便其他组或用户可以声明它，您必须重命名该组。名称和路径都必须是唯一的。

更改群组路径后，新的群组路径是一个新的命名空间，您必须更新以下资源中的现有项目 URL：

- [Include 语句](../../ci/yaml/includes.md#include-a-single-configuration-file)。
- CI 文件中的 Docker 镜像引用。
- 指定项目或命名空间的变量。

要保留原始命名空间的所有权并保护 URL 重定向，请创建一个新群组并将项目转移到该群组。

要更改您的群组路径（群组 URL）：

1. 在顶部栏上，选择 **主菜单 > 群组**，并找到您的群组。
1. 选择 **设置 > 通用**。
1. 展开 **高级** 部分。
1. 在 **更改群组 URL** 下，输入一个新名称。
1. 选择 **更改群组 URL**。

WARNING:
如果命名空间包含带有 [Container Registry](../packages/container_registry/index.md) 标签的项目，则无法重命名命名空间，因为该项目无法移动。

<a id="change-the-default-branch-protection-of-a-group"></a>

## 更改群组的默认分支保护

默认情况下，每个群组都会继承在全局级别设置的分支保护。

要更改特定群组的设置，请参阅[群组级别默认分支保护](../project/repository/branches/default.md#group-level-default-branch-protection)。

要全局更改此设置，请参阅[初始默认分支保护](../project/repository/branches/default.md#instance-level-default-branch-protection)。

NOTE:
在[专业版或更高级别](https://gitlab.cn/pricing/)，管理员可以选择[禁用群组所有者更新默认分支保护](../project/repository/branches/default.md#prevent-overrides-of-default-branch-protection)。

## 为初始分支使用自定义名称

创建新项目时，会在第一次推送时创建一个默认分支。群组所有者可以为群组的项目[自定义初始分支](../project/repository/branches/default.md#group-level-custom-initial-branch-name)，满足您群组的需求。

<a id="share-a-group-with-another-group"></a>

## 与另一个群组共享群组

> - 窗口在私有化部署实例上启用，引入于 14.8 版本。
> - 一般可用于 14.9 版本。功能标志 `invite_members_group_modal` 删除。

和[与群组共享项目](../project/members/share_project_with_groups.md)的方式类似，您可以与另一个群组共享一个群组。要邀请某个群组，您必须是该群组的成员。要与另一个群组共享特定群组，例如，与 `Frontend` 群组共享 `Engineering` 群组：

1. 进入 `Frontend` 群组。
1. 从左侧菜单中，选择 **群组信息 > 成员**。
1. 选择 **邀请群组**。
1. 在 **选择要邀请的群组** 列表中，选择 `Engineering`。
1. 选择角色作为最大访问级别。
1. 选择 **邀请**。

在与 `Engineering` 群组共享 `Frontend` 群组后：

- **群组** 选项卡列出了 `Engineering` 群组。
- **群组** 选项卡列出群组，无论它是公开群组还是私有群组。
- `Engineering` 群组的所有直接成员都可以访问 `Frontend` 群组。成员的相同访问级别适用于共享群组时选择的最大访问级别。

## 转移群组

您可以通过以下方式转移群组：

- 将子组转移到新的父组。
- 通过将顶级组转移到所需组，将其转移为子组。
- 通过将子组从当前组中移出，将子组转移为顶级组。

转移群组时，请注意：

- 更改群组的父级可能会产生意想不到的副作用。请参阅[仓库路径更改时会发生什么](../project/repository/index.md#what-happens-when-a-repository-path-changes)。
- 您只能将群组转移到您管理的群组。
- 您必须更新本地仓库以指向新位置。
- 如果直接父组的可见性低于该组的当前可见性，子组和项目的可见性级别会更改以匹配新父组的可见性。
- 仅转移明确的群组成员资格，而不是继承的成员资格。如果群组的所有者仅继承了成员资格，则该群组将没有所有者。在这种情况下，转移群组的用户将成为组的所有者。
- 如果 [npm 软件包](../packages/npm_registry/index.md)存在于群组中的任何项目或其任何子组中，则转换失败。
- 使用群组级端点（Maven、NuGet、PyPI、Composer 和 Debian）的现有软件包，需要根据不同类型软件包各自的设置群组级端点的步骤进行更新。
- 如果软件包使用实例级端点 ([Maven](../packages/maven_repository/index.md#naming-convention)、[npm](../packages/npm_registry/index.md#naming-convention)、[Conan](../packages/conan_repository/index.md#package-recipe-naming-convention-for-instance-remotes))，则需要更新现有软件包名称，并且该群组移动至另一个根级命名空间。
- [Maven 软件包](../packages/maven_repository/index.md#naming-convention)遵循命名约定，在群组转移后组织从群组级端点安装或发布相应的软件包。
- 无法转移在 SaaS 上订阅的顶级群组。要使转移成为可能，必须先删除顶级群组的订阅。然后顶级群组可以作为子组转移到另一个顶级群组。

转移群组：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **高级** 部分。
1. 在 **删除群组** 部分，选择 **转移群组**。
1. 在下拉菜单中选择群组名称。
1. 选择 **转移群组**。

<a id="enable-delayed-project-deletion"></a>

## 启用延迟项目删除 **(PREMIUM)**

> - 引入于 13.2 版本。
> - 于 13.11 版本添加了继承和执行。
> - 于 14.2 版本添加了默认启用的实例设置。
> - 从 15.1 版本开始，在禁用时，实例设置被继承和强制执行。
> - 用户界面变更于 15.1 版本。
> - 启用功能标志 `always_perform_delayed_deletion` 可以删除相关选项的功能引入于 15.11 版本。此功能标志默认禁用。
> - 启用延迟删除并删除“立即删除”的选项于 16.0 版本。

除非为仅群组，或群组和项目启用删除保护的实例级设置，否则[延迟项目删除](../project/settings/index.md#delayed-project-deletion)将被锁定和禁用。
在群组上启用时，群组中的项目会在延迟一段时间后被删除。在此期间，项目处于只读状态，可以恢复。
默认期限为 7 天，但可在实例级别配置<!--[可在实例级别配置](../admin_area/settings/visibility_and_access_controls.md#retention-period)-->。

在私有化部署版上，默认情况下会立即删除项目。
在 14.2 及更高版本中，管理员可以为新创建的群组中的项目更改默认设置<!--[更改默认设置](../admin_area/settings/visibility_and_access_controls.md#default-delayed-project-deletion)-->。

<!--On GitLab.com, see the [GitLab.com settings page](../gitlab_com/index.md#delayed-project-deletion) for
the default setting.-->

要启用群组中项目的延迟删除：

1. 在顶部栏上，选择 **主菜单 > 群组**，并找到您的群组。
1. 选择 **设置 > 通用**。
1. 展开 **权限和群组功能** 部分。
1. 滚动到：
   - （15.1 及更高版本）**删除保护** 并选择 **保留已删除的项目**。
   - （15.0 及更早版本）**启用延迟项目删除** 并勾选复选框。
1. 可选。要防止子组更改此设置，请选择：
   - （15.1 及更高版本）**对所有子组实施删除保护**。
   - （15.0 及更早版本）**对所有子组强制执行**。
1. 选择 **保存修改**。

在 13.11 及更高版本中，延迟项目删除的群组设置由子组继承。<!--如[级联设置](../../development/cascading_settings.md)中所述，-->继承可以被覆盖，除非由父组强制执行。

在启用了 `always_perform_delayed_deletion` 功能标志的极狐GitLab 15.11 中，此设置被删除，所有项目在[管理员定义的保留期](../admin_area/settings/visibility_and_access_controls.md#retention-period)之后被删除。这将是 16.0 及更高版本中的默认行为。

## 禁用电子邮件通知

您可以禁用与群组相关的所有电子邮件通知，其中包括其子组和项目。

要禁用电子邮件通知：

1. 在顶部栏上，选择 **主菜单 > 群组 > 查看所有群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置** > **通用**。
1. 展开 **权限和群组功能** 部分。
1. 选择 **电子邮件通知已禁用**。
1. 选择 **保存修改**。

## 禁用群组提及

您可以防止将用户添加到对话中，并在有人提及这些用户所属的群组时收到通知。

禁用提及的群组在自动完成下拉菜单中相应地可视化。

这对于拥有大量用户的群组特别有用。

要禁用群组提及：

1. 在顶部栏上，选择 **主菜单 > 群组 > 查看所有群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置** > **通用**。
1. 展开 **权限和群组功能** 部分。
1. 选择 **群组提及已禁用**。
1. 选择 **保存修改**。

## 将成员导出为 CSV **(PREMIUM)**

> - 引入于 14.2 版本。
> - 功能标志移除于 14.5 版本。

您可以将群组或子组中的成员列表导出为 CSV。

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组或子组。
1. 在左侧边栏，选择 **群组信息 > 成员** 或 **子组信息 > 成员**。
1. 选择 **导出为 CSV**。
1. 生成 CSV 文件后，它会作为附件通过电子邮件发送给请求它的用户。

输出列出了直接成员和从上级群组继承的成员。
对于所选群组中具有“最小访问权限”的成员，他们的“最大角色”和“来源”源自他们在子组中的成员资格。

<!--
## User cap for groups

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/330027) in GitLab 14.7.

FLAG:
On self-managed GitLab, this feature is not available. On GitLab.com, this feature is available for some groups.
This feature is not ready for production use.

When the number of billable members reaches the user cap, new users can't be added to the group
without being approved by the group owner.

Groups with the user cap feature enabled have [group sharing](#share-a-group-with-another-group)
disabled for the group and its subgroups.

### Specify a user cap for a group

Prerequisite:

- You must be assigned the Owner role) for the group.

To specify a user cap:

1. On the top bar, select **Menu > Groups** and find your group.
   You can set a cap on the top-level group only.
1. On the left sidebar, select **Settings > General**.
1. Expand **Permissions and group features**.
1. In the **User cap** box, enter the desired number of users.
1. Select **Save changes**.

If you already have more users in the group than the user cap value, users
are not removed. However, you can't add more without approval.

Increasing the user cap does not approve pending members.

### Remove the user cap for a group

You can remove the user cap, so there is no limit on the number of members you can add to a group.

Prerequisite:

- You must be assigned the Owner role) for the group.

To remove the user cap:

1. On the top bar, select **Menu > Groups** and find your group.
1. On the left sidebar, select **Settings > General**.
1. Expand **Permissions and group features**.
1. In the **User cap** box, delete the value.
1. Select **Save changes**.

Decreasing the user cap does not approve pending members.

### Approve pending members for a group

When the number of billable users reaches the user cap, any new member is put in a pending state
and must be approved.

Pending members do not count as billable. Members count as billable only after they have been approved and are no longer in a pending state.

Prerequisite:

- You must be assigned the Owner role) for the group.

To approve members that are pending because they've exceeded the user cap:

1. On the top bar, select **Menu > Groups** and find your group.
1. On the left sidebar, select **Settings > Usage Quotas**.
1. On the **Seats** tab, under the alert, select **View pending approvals**.
1. For each member you want to approve, select **Approve**.
-->

<a id="group-file-templates"></a>

## 群组文件模板 **(PREMIUM)**

使用群组文件模板与群组中的每个项目共享一组常见文件类型的模板。它类似于[实例模板仓库](../admin_area/settings/instance_template_repository.md)。所选项目应遵循该页面上记录的相同命名约定。

您只能选择群组中的项目作为模板源。包括与群组共享的项目，但**排除**正在配置的群组的子组或父组中的项目。

您可以为子组和直接父组配置此功能。子组中的项目可以访问该子组以及任何直接父组的模板。

要了解如何为议题和合并请求创建模板，请参阅[描述模板](../project/description_templates.md)。

通过将群组设置为模板源，在群组级别定义项目模板。<!--[Learn more about group-level project templates](custom_project_templates.md).--> **(PREMIUM)**

### 启用群组文件模板 **(PREMIUM)**

要启用群组文件模板：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置** > **通用**。
1. 展开 **模板** 部分。
1. 选择一个项目作为模板库。
1. 选择 **保存更改**。

## 群组合并检查设置 **(PREMIUM)**

> 引入于极狐GitLab 15.9，[功能标志](../../administration/feature_flags.md)为 `support_group_level_merge_checks_setting`。默认禁用。

FLAG:
在私有化部署的极狐GitLab 上，默认情况下此功能不可用。要使其可用，请让管理员启用名为 `support_group_level_merge_checks_setting` 的[功能标志](../../administration/feature_flags.md)。在 JiHuLab.com 上，此功能不可用。

群组所有者可以在顶级群组上设置合并请求检查，适用于所有子群组和项目。

如果设置被子群组或项目继承，则不能在继承它们的子群组或项目中进行更改。

### 合并需要成功的流水线

您可以将您群组中的所有子项目配置为在合并之前需要完整且成功的流水线。

另见[项目级设置](../project/merge_requests/merge_when_pipeline_succeeds.md#require-a-successful-pipeline-for-merge)。

先决条件：

- 您必须是群组的所有者。

要启用此设置：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **合并请求**。
1. 在 **合并检查** 下，选择 **流水线必须成功**。
   此设置还可以防止没有流水线的情况下合并请求被合并。
1. 选择 **保存更改**。

#### 跳过流水线后允许合并

您可以配置[跳过流水线](../../ci/pipelines/index.md#skip-a-pipeline)以防止合并请求被合并。

另见[项目级设置](../project/merge_requests/merge_when_pipeline_succeeds.md#allow-merge-after-skipped-pipelines)。

先决条件：

- 您必须是群组的所有者。

要更改此行为：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **合并请求**。
1. 在 **合并检查** 下：
    - 选择 **流水线必须成功**。
    - 选择 **跳过的流水线视为成功**。
1. 选择 **保存更改**。

### 解决所有主题后才可以合并

您可以设置解决所有主题后才可以合并。启用此设置后，对于您群组中的所有子项目，当至少有一个主题未解决时，合并请求中的**未解决主题**计数显示为橙色。

先决条件：

- 您必须是群组的所有者。

要启用此设置：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **合并请求**。
1. 在 **合并检查** 下，选择 **必须解决所有主题**。
1. 选择 **保存更改**。

<a id="group-merge-request-approval-settings"></a>

## 群组合并请求批准设置 **(PREMIUM)**

> - 引入于 13.9 版本。部署在 `group_merge_request_approval_settings_feature_flag` 标记之后，默认禁用。
> - 默认启用于 14.5 版本
> - 功能标志 `group_merge_request_approval_settings_feature_flag` 移除于 14.9 版本。

群组批准设置在顶级群组级别管理项目合并请求批准设置。这些设置级联到属于该群组的所有项目。

查看群组的合并请求批准设置：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置** > **通用**。
1. 展开 **合并请求批准** 部分。
1. 选择您想要的设置。
1. 选择 **保存修改**。

## 群组动态分析 **(PREMIUM)**

对于群组，您可以查看在过去 90 天内创建的合并请求、议题和成员的数量。

可以使用 `group_activity_analytics` 功能标志<!--[功能标志](../../development/feature_flags/index.md#enabling-a-feature-flag-locally-in-development)-->启用群组动态分析。

![Recent Group Activity](img/group_activity_analytics_v13_10.png)

对[群组 wikis](../project/wiki/group.md) 的更改不会出现在群组活动分析中。

### 查看群组动态

您可以在浏览器或 RSS 提要中查看群组中最近执行的操作：

1. 在顶部栏上，选择 **主菜单 > 群组 > 查看所有群组** 并找到您的群组。
1. 在左侧边栏上，选择 **群组信息 > 动态**。

要以 Atom 格式查看动态提要，请选择 **RSS** (**{rss}**) 图标。

## 故障排除

### 命名空间和群组的验证错误

14.4 及更高版本在创建或更新命名空间或组时执行以下检查：

- 命名空间不能有父级。
- 父组必须是组而不是命名空间。

如果显示以下错误，您可以禁用验证：

- `A user namespace cannot have a parent`
- `A group cannot have a user namespace as its parent`

您在安装实例中看到这些错误时，可以尝试联系技术支持。

### 使用 SQL 查询查找群组

要根据 [rails 控制台](../../administration/operations/rails_console.md)中的 SQL 查询查找和存储数组：

```ruby
# Finds groups and subgroups that end with '%oup'
Group.find_by_sql("SELECT * FROM namespaces WHERE name LIKE '%oup'")
=> [#<Group id:3 @test-group>, #<Group id:4 @template-group/template-subgroup>]
```

### 使用 Rails 控制台将子组转移到另一个位置

如果无法通过 UI 或 API 转移群组，您可能需要尝试在 [Rails 控制台会话](../../administration/operations/rails_console.md#starting-a-rails-console-session)：

WARNING:
如果运行不正确，或在正确的条件下，任何直接更改数据的命令都可能造成破坏。我们强烈建议在测试环境中运行它们，并准备好恢复实例的备份，以防万一。

```ruby
user = User.find_by_username('<username>')
group = Group.find_by_name("<group_name>")
## Set parent_group = nil to make the subgroup a top-level group
parent_group = Group.find_by(id: "<group_id>")
service = ::Groups::TransferService.new(group, user)
service.execute(parent_group)
```

### 使用 Rails 控制台查找待删除的群组

如果需要查找所有待删除的群组，可以在 [Rails 控制台会话](../../administration/operations/rails_console.md#starting-a-rails-console-session)中使用以下命令：

```ruby
Group.all.each do |g|
 if g.marked_for_deletion?
    puts "Group ID: #{g.id}"
    puts "Group name: #{g.name}"
    puts "Group path: #{g.full_path}"
 end
end
```

### 使用 Rails 控制台删除群组

有时，群组删除可能会卡住。如果需要，在 [Rails 控制台会话](../../administration/operations/rails_console.md#starting-a-rails-console-session)中，您可以尝试使用以下命令删除群组：

WARNING:
如果运行不正确，或在正确的条件下，任何直接更改数据的命令都可能造成破坏。我们强烈建议在测试环境中运行它们，并准备好恢复实例的备份，以防万一。

```ruby
GroupDestroyWorker.new.perform(group_id, user_id)
```

### 查找用户对群组或项目的最大权限

管理员可以找到用户对群组或项目的最大权限。

1. 启动 [Rails 控制台会话](../../administration/operations/rails_console.md#starting-a-rails-console-session)。
1. 运行以下命令：

   ```ruby
   user = User.find_by_username 'username'
   project = Project.find_by_full_path 'group/project'
   user.max_member_access_for_project project.id
   ```

   ```ruby
   user = User.find_by_username 'username'
   group = Group.find_by_full_path 'group'
   user.max_member_access_for_group group.id
   ```
