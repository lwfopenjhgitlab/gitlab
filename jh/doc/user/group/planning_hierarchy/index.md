---
type: reference
stage: Plan
group: Product Planning
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 规划层次结构 **(PREMIUM)**

规划层次结构是分解您在极狐GitLab 中工作的一个组成部分。
要了解如何在层次结构中同时使用史诗和议题，请记住以下几点：

- [史诗](../epics/index.md)存在于群组。
- [议题](../../project/issues/index.md)存在于项目。

<!--
GitLab is not opinionated on how you structure your work and the hierarchy you can build with multi-level
epics. For example, you can use the hierarchy as a folder of issues for bigger initiatives.


To learn about hierarchies in general, common frameworks, and using GitLab for
portfolio management, see
[How to use GitLab for Agile portfolio planning and project management](https://about.gitlab.com/blog/2020/11/11/gitlab-for-agile-portfolio-planning-project-management/).
-->

## 具有史诗的层次结构

使用史诗，您可以实现以下层次结构：

```mermaid
graph TD
    Group_epic --> Project1_Issue1
    Group_epic --> Project1_Issue2
    Group_epic --> Project2_Issue1
```

### 具有多级史诗的层次结构 **(ULTIMATE)**

> - 极狐GitLab 自 15.2 版本开始，将多级史诗功能从旗舰版降入专业版。而 GitLab CE 与 EE 版本中，多级史诗功能依旧为旗舰版。
> - 极狐GitLab 自 15.8 版本开始，将该功能从专业版调整为旗舰版。

通过添加[多级史诗](../epics/manage_epics.md#multi-level-child-epics)和最多七级嵌套史诗，您可以实现以下层次结构：

<!--
Image below was generated with the following Mermaid code.
Attached as an image because a rendered diagram doesn't look clear on the docs page.

```mermaid
classDiagram
  direction TD
  class Epic
  class Issue

  Epic *-- "0..7" Epic
Epic "1"*-- "0..*" Issue
```

 -->

![Diagram showing possible relationships of multi-level epics](img/hierarchy_with_multi_level_epics.png)

## 查看议题的起源

在议题中，您可以在右侧边栏的 **史诗** 下，查看议题的上级史诗。

![epics state dropdown](img/issue-view-parent-epic-in-sidebar_v14_6.png)

## 查看史诗的上级史诗

在议题中，您可以在右侧边栏的 **祖先** 下，查看史诗的各级父史诗。

![epics state dropdown](img/epic-view-ancestors-in-sidebar_v14_6.png)
