---
stage: Data Stores
group: Tenant Scale
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 群组故障排除

## 命名空间和群组的验证错误

在[14.4 及更高版本中](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70365)，当创建或更新命名空间或群组时，执行以下检查：

- 命名空间不得有父级。
- 群组的父级必须是群组而非命名空间。

万一您在极狐GitLab 安装实例中看到这些错误，请[联系支持](https://about.gitlab.com/support/)，以便我们改进此验证。

## 使用 SQL 查询查找群组

要在 [rails 控制台](../../administration/operations/rails_console.md)中基于 SQL 查询查找并存储群组数组：

```ruby
# Finds groups and subgroups that end with '%oup'
Group.find_by_sql("SELECT * FROM namespaces WHERE name LIKE '%oup'")
=> [#<Group id:3 @test-group>, #<Group id:4 @template-group/template-subgroup>]
```

## 使用 Rails 控制台将子组转移到另一个位置

如果无法通过 UI 或 API 传输群组，您可能需要尝试使用 [Rails 控制台会话](../../administration/operations/rails_console.md#starting-a-rails-console-session)传输：

WARNING:
如果未正确运行或在正确的条件下运行，更改数据的命令可能会造成损坏。始终首先在测试环境中运行命令并准备好备份实例以进行恢复。

```ruby
user = User.find_by_username('<username>')
group = Group.find_by_name("<group_name>")
## Set parent_group = nil to make the subgroup a top-level group
parent_group = Group.find_by(id: "<group_id>")
service = ::Groups::TransferService.new(group, user)
service.execute(parent_group)
```

## 使用 Rails 控制台查找待删除的群组

如果需要查找所有待删除的群组，可以在 [Rails 控制台会话](../../administration/operations/rails_console.md#starting-a-rails-console-session)使用以下命令：

```ruby
Group.all.each do |g|
 if g.marked_for_deletion?
    puts "Group ID: #{g.id}"
    puts "Group name: #{g.name}"
    puts "Group path: #{g.full_path}"
 end
end
```

## 使用 Rails 控制台删除群组

有时，群组删除可能会陷入困境。如果需要，在 [Rails 控制台会话](../../administration/operations/rails_console.md#starting-a-rails-console-session)中，您可以尝试使用以下命令删除群组：

WARNING:
如果未正确运行或在正确的条件下运行，更改数据的命令可能会造成损坏。始终首先在测试环境中运行命令并准备好备份实例以进行恢复。

```ruby
GroupDestroyWorker.new.perform(group_id, user_id)
```

## 查找用户对群组或项目的最大权限

管理员可以找到用户对群组或项目的最大权限。

1. 启动 [Rails 控制台会话](../../administration/operations/rails_console.md#starting-a-rails-console-session)。
1. 运行以下命令：

   ```ruby
   user = User.find_by_username 'username'
   project = Project.find_by_full_path 'group/project'
   user.max_member_access_for_project project.id
   ```

   ```ruby
   user = User.find_by_username 'username'
   group = Group.find_by_full_path 'group'
   user.max_member_access_for_group group.id
   ```

## 无法删除带有 `Project Invite/Group Invite` 徽章的计费成员

```plaintext
Members who were invited via a group invitation cannot be removed. You can either remove the entire group, or ask an Owner of the invited group to remove the member.
```

当您尝试删除的用户属于已[与您的一个或多个项目](../project/members/share_project_with_groups.md)或[群组](manage.md#share-a-group-with-another-group)的外部群组时，通常会发生此错误。要将用户作为计费会员删除，请执行以下操作之一：

- 从您的项目或群组成员页面中删除受邀请的群组成员资格。
- 推荐方法，如果您有权访问该群组，请直接从受邀群组中删除用户。

<!--
The feature request to **Update billable_members endpoint to include invited group** is currently being worked on. For more information, see [issue 386583](https://gitlab.com/gitlab-org/gitlab/-/issues/386583)
-->
