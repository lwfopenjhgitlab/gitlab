---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# SaaS 群组的 SAML SSO **(PREMIUM SAAS)**

用户可以通过其 SAML 身份提供商登录极狐GitLab。

<!--[SCIM](scim_setup.md)-->SCIM 将用户与 SaaS 上的群组同步。

- 当您从 SCIM 应用程序添加或删除用户时，SCIM 会从极狐GitLab 群组添加或删除用户。
- 如果用户还不是群组成员，则会在登录过程中将用户添加到群组中。

您只能为顶级群组配置 SAML SSO。

<a id="set-up-your-identity-provider"></a>

## 设置您的身份提供商

由于 SAML 标准的存在，您可以在极狐GitLab 中使用各种身份提供商。您的身份提供商可能有相关文档，可能是通用的 SAML 文档，也可能是专门针对极狐GitLab 的文档。

设置身份提供商时，请使用以下提供商特定文档来帮助避免常见问题，并作为所用术语的指南。

对于未列出的身份提供商，您可以参阅[有关配置身份提供商的实例 SAML 说明](../../../integration/saml.md#configure-saml-on-your-idp)来获取更多指导。

极狐GitLab 提供以下信息仅供指导。
如果您对配置 SAML 应用程序有任何疑问，请联系提供商的支持人员。

<!--
如果您在设置身份提供商时遇到问题，请参阅[故障排除文档](#troubleshooting)。
-->

### Azure

要使用 Azure 作为身份提供商来设置 SSO：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的群组。
1. 选择 **设置 > SAML SSO**。
1. 请注意此页面上的信息。
1. 转到 Azure 并[遵循为应用程序配置 SSO 的说明](https://learn.microsoft.com/en-us/azure/active-directory/manage-apps/add-application-portal-setup-sso)。极狐GitLab 设置与 Azure 字段对应如下。

   | 极狐GitLab 设置                           | Azure 字段                                    |
   | -----------------------------------------| ---------------------------------------------- |
   | **身份标识**                           | **Identifier (Entity ID)**                     |
   | **断言消费者服务 URL**       | **Reply URL (Assertion Consumer Service URL)** |
   | **极狐GitLab 单点登录网址**            | **Sign on URL**                                |
   | **身份验证提供商单点登录 URL** | **Login URL**                                  |
   | **证书指纹**              | **Thumbprint**                                 |

1. 您应该设置以下属性：
   - 将 **Unique User Identifier (Name identifier)** 设置为 `user.objectID`。
   - 将 **nameid-format** 设置为 `persistent`。有关更多信息，请参阅如何[管理用户 SAML 身份](#manage-user-saml-identity)。
   - 将 **Additional claims** 设置为[支持的属性](#user-attributes)。

1. 确保身份提供商设置为由提供商发起的调用，关联现有的极狐GitLab 帐户。

1. 可选。如果您使用群组同步<!--[群组同步](group_sync.md)-->，请自定义群组声明的名称来匹配所需的属性。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
View a demo of [SCIM provisioning on Azure using SAML SSO for groups](https://youtu.be/24-ZxmTeEBU). The `objectID` mapping is outdated in this video. Follow the [SCIM documentation](scim_setup.md#configure-azure-active-directory) instead.

For more information, see an [example configuration page](example_saml_config.md#azure-active-directory).
-->

<!--
### Google Workspace

To set up Google Workspace as your identity provider:

1. On the left sidebar, at the top, select **Search GitLab** (**{search}**) to find your group.
1. Select **Settings > SAML SSO**.
1. Note the information on this page.
1. Follow the instructions for [setting up SSO with Google as your identity provider](https://support.google.com/a/answer/6087519?hl=en). The following GitLab settings correspond to the Google Workspace fields.

   | GitLab setting                           | Google Workspace field |
   |:-----------------------------------------|:-----------------------|
   | **Identifier**                           | **Entity ID**          |
   | **Assertion consumer service URL**       | **ACS URL**            |
   | **GitLab single sign-on URL**            | **Start URL**          |
   | **Identity provider single sign-on URL** | **SSO URL**            |

1. Google Workspace displays a SHA256 fingerprint. To retrieve the SHA1 fingerprint
   required by GitLab to [configure SAML](#configure-gitlab):
   1. Download the certificate.
   1. Run this command:

      ```shell
      openssl x509 -noout -fingerprint -sha1 -inform pem -in "GoogleIDPCertificate-domain.com.pem"
      ```

1. Set these values:
   - For **Primary email**: `email`.
   - For **First name**: `first_name`.
   - For **Last name**: `last_name`.
   - For **Name ID format**: `EMAIL`.
   - For **NameID**: `Basic Information > Primary email`.
     For more information, see [manage user SAML identity](#manage-user-saml-identity).

1. Make sure the identity provider is set to have provider-initiated calls
   to link existing GitLab accounts.

On the GitLab SAML SSO page, when you select **Verify SAML Configuration**, disregard
the warning that recommends setting the **NameID** format to `persistent`.

For more information, see an [example configuration page](example_saml_config.md#google-workspace).

<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
View a demo of [how to configure SAML with Google Workspaces and set up Group Sync](https://youtu.be/NKs0FSQVfCY).
-->

### Okta

要使用 Okta 作为身份提供商来设置 SSO：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的群组。
1. 选择 **设置 > SAML SSO**。
1. 请注意此页面上的信息。
1. 按照[在 Okta 中设置 SAML 应用程序](https://developer.okta.com/docs/guides/build-sso-integration/saml2/main/)的说明进行操作。

   极狐GitLab 与 Okta 字段的对应如下。

   | 极狐GitLab 设置                          | Okta 字段                                                     |
   | ---------------------------------------- | -------------------------------------------------------------- |
   | **身份标识**                          | **Audience URI**                                               |
   | **断言消费者服务 URL**          | **Single sign-on URL**                                         |
   | **极狐GitLab 单点登录网址**            | **Login page URL**（在 **Application Login Page** 设置下） |
   | **身份验证提供商单点登录 URL** | **Identity Provider Single Sign-On URL**                       |

1. 在 Okta **Single sign-on URL** 字段下，选中 **Use this for Recipient URL and Destination URL** 复选框。

1. 设置以下值：
   - 对于 **Application username (NameID)**：**自定义** `user.getInternalProperty("id")`。
   - 对于 **Name ID Format**：`Persistent`。有关更多信息，请参阅[管理用户 SAML 身份](#manage-user-saml-identity)。

1. 确保身份提供商设置为由提供商发起的调用，关联现有的极狐GitLab 帐户。

应用程序目录中提供的 Okta GitLab 应用程序仅支持 <!--[SCIM](scim_setup.md)-->SCIM。<!--Support
for SAML is proposed in [issue 216173](https://gitlab.com/gitlab-org/gitlab/-/issues/216173).-->

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For a demo of the Okta SAML setup including SCIM, see [Demo: Okta Group SAML & SCIM setup](https://youtu.be/0ES9HsZq0AQ).

For more information, see an [example configuration page](example_saml_config.md#okta)
-->

<!--
### OneLogin

OneLogin supports its own [GitLab (SaaS) application](https://onelogin.service-now.com/support?id=kb_article&sys_id=92e4160adbf16cd0ca1c400e0b961923&kb_category=50984e84db738300d5505eea4b961913).

To set up OneLogin as your identity provider:

1. On the left sidebar, at the top, select **Search GitLab** (**{search}**) to find your group.
1. Select **Settings > SAML SSO**.
1. Note the information on this page.
1. If you use the OneLogin generic
   [SAML Test Connector (Advanced)](https://onelogin.service-now.com/support?id=kb_article&sys_id=b2c19353dbde7b8024c780c74b9619fb&kb_category=93e869b0db185340d5505eea4b961934),
   you should [use the OneLogin SAML Test Connector](https://onelogin.service-now.com/support?id=kb_article&sys_id=93f95543db109700d5505eea4b96198f). The following GitLab settings correspond
   to the OneLogin fields:

   | GitLab setting                                       | OneLogin field                   |
   | ---------------------------------------------------- | -------------------------------- |
   | **Identifier**                                       | **Audience**                     |
   | **Assertion consumer service URL**                   | **Recipient**                    |
   | **Assertion consumer service URL**                   | **ACS (Consumer) URL**           |
   | **Assertion consumer service URL (escaped version)** | **ACS (Consumer) URL Validator** |
   | **GitLab single sign-on URL**                        | **Login URL**                    |
   | **Identity provider single sign-on URL**             | **SAML 2.0 Endpoint**            |

1. For **NameID**, use `OneLogin ID`. For more information, see [manage user SAML identity](#manage-user-saml-identity).

1. Make sure the identity provider is set to have provider-initiated calls
   to link existing GitLab accounts.
-->

### 使用元数据

要配置某些身份提供商，您需要极狐GitLab 元数据 URL。
要查找此 URL：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的群组。
1. 选择 **设置 > SAML SSO**。
1. 复制提供的 **极狐GitLab 元数据 URL**。
1. 按照身份提供商的文档进行操作，并在需要时粘贴元数据 URL。

查看您的身份提供商的文档，了解它是否支持极狐GitLab 元数据 URL。

### 管理身份提供商

设置身份提供商后，您可以：

- 更改身份提供商。
- 更改电子邮件域名。

#### 更改身份提供商

您可以更改为使用不同的身份提供商。在更改过程中，用户无法访问任何 SAML 群组。为了规避这种情况，您可以禁用[强制 SSO](#sso-enforcement)。

要更改身份提供商：

1. 使用新身份提供商[配置](#set-up-your-identity-provider)群组。
1. 可选。如果 **NameID** 不相同，请[更改用户的 **NameID**](#manage-user-saml-identity)。

#### 更改电子邮件域名

要将用户迁移到新的电子邮件域名，请通知用户：

1. [添加新电子邮件](../../profile/index.md#change-your-primary-email)作为其账户的主要电子邮件并进行验证。
1. 可选。从账户中删除他们的旧电子邮件。

如果 **NameID** 配置了电子邮件地址，请[更改用户的 **NameID**](#manage-user-saml-identity)。

<a id="configure-gitlab"></a>

## 配置极狐GitLab

设置身份提供商与极狐GitLab 配合使用后，您必须配置极狐GitLab，使用它进行身份验证：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的群组。
1. 选择 **设置 > SAML SSO**。
1. 填写字段：
   - 在 **身份提供商单点登录 URL** 字段中，输入来自您的身份提供商的 SSO URL。
   - 在 **证书指纹** 字段中，输入 SAML 令牌签名证书的指纹。
1. 在 **默认成员角色** 字段中，选择要分配给新用户的角色。默认角色是 **访客**。在 13.10 及更高版本中，群组所有者可以设置除 **访客** 之外的默认成员角色。
   为此，[为群组配置 SAML SSO](#configure-gitlab)。该角色成为添加到群组中的所有用户的起始角色。
1. 选择 **为此群组启用 SAML 身份验证** 复选框。
1. 可选。选择：
   - **对该群组的 Web 活动强制执行仅 SSO 身份验证**。
   - **对该群组的 Git 活动强制执行仅 SSO 身份验证**。
   有关详细信息，请参阅 [SSO 强制执行文档](#sso-enforcement)。
1. 选择 **保存更改**。

NOTE:
证书[指纹算法](../../../integration/saml.md#configure-saml-on-your-idp)必须采用 SHA1。配置身份提供商时，请使用安全签名算法。

<!--
如果您在配置极狐GitLab 时遇到问题，请参阅[故障排除文档](#troubleshooting)。
-->

## 用户访问和管理

配置并启用群组 SSO 后，用户可以通过身份提供商的仪表盘访问 SaaS 群组。
<!--如果配置了 [SCIM](scim_setup.md)，请参阅 SCIM 页面上的[用户访问](scim_setup.md#user-access)。-->

当用户尝试使用群组 SSO 登录时，极狐GitLab 会尝试根据以下内容查找或创建用户：

- 查找具有匹配 SAML 身份的现有用户。这意味着用户可能拥有由 SCIM<!--[SCIM](scim_setup.md)--> 创建的账户，或者可能之前已使用该群组的 SAML IdP 登录。
- 如果没有具有相同电子邮件地址的冲突用户，则自动创建一个新账户。
- 如果存在具有相同电子邮件地址的冲突用户，则将用户重定向到登录页面：
  - 使用另一个电子邮件地址创建一个新账户。
  - 登录到其现有账户来关联 SAML 身份。

<a id="user-attributes"></a>

### 用户属性

您可以将用户信息作为 SAML 断言中的属性传递给极狐GitLab。

- 用户的电子邮件地址可能是 **email** 或 **mail** 属性。
- 用户名可能是 **username** 或 **nickname** 属性。您仅需指定其中之一。

有关更多信息，请参阅[可用于私有化部署版极狐GitLab 实例的属性](../../../integration/saml.md#configure-assertions)。

<a id="link-saml-to-your-existing-gitlabcom-account"></a>

### 将 SAML 关联到您现有的 SaaS 账户

> **记住我** 复选框引入于 15.7 版本。

要将 SAML 关联到您现有的 SaaS 账户：

1. 登录您的 SaaS 账户。如有必要，[重置您的密码](https://jihulab.com/users/password/new)。
1. 找到并访问您要登录的群组的**极狐GitLab 单点登录 URL**。群组所有者可以在群组的 **设置 > SAML SSO** 页面上找到此信息。 如果配置了登录 URL，用户可以从身份提供商连接到极狐GitLab 应用程序。
1. 可选。选择 **记住我** 复选框可在 2 周内保持登录极狐GitLab 的状态。您可能仍会被要求更频繁地向 SAML 提供商重新进行身份验证。
1. 选择 **授权**。
1. 如果出现提示，请在身份提供商上输入您的凭据。
1. 然后您将被重定向回 SaaS，现在应该可以访问该群组。将来，您可以使用 SAML 登录 SaaS。

如果用户已经是该群组的成员，则关联 SAML 身份不会更改其角色。

在后续访问中，您应该能够[使用 SAML 登录 SaaS](#sign-in-to-gitlabcom-with-saml) 或直接访问链接。如果启用 **强制 SSO** 选项，您将被重定向来通过身份提供商登录。

<a id="sign-in-to-gitlabcom-with-saml"></a>

### 使用 SAML 登录 SaaS

1. 登录您的身份提供商。
1. 从应用程序列表中，选择 SaaS 对应的应用程序（该名称由身份提供商的管理员设置）。
1. 然后，您将登录 SaaS 并重定向到该群组。

<a id="manage-user-saml-identity"></a>

### 管理用户 SAML 身份

> 使用 SAML API 更新 SAML 身份功能引入于 15.5 版本。

SaaS 使用 SAML **NameID** 来识别用户。**NameID** 是：

- SAML 响应中的必填字段。
- 区分大小写。

**NameID** 必须：

- 每个用户都是独一无二的。
- 是一个永不改变的持久值，例如随机生成的唯一用户 ID。
- 与后续登录尝试完全匹配，因此不应依赖于可能在大小写之间变化的用户输入。

**NameID** 不应是电子邮件地址或用户名，因为：

- 电子邮件地址和用户名更有可能随着时间的推移而改变。例如，当一个人的名字改变时。
- 电子邮件地址不区分大小写，这可能会导致用户无法登录。

**NameID** 格式必须是 `Persistent`，除非您使用的字段（例如电子邮件）需要不同的格式。您可以使用除 `Transient` 之外的任何格式。

#### 更改用户 **NameID**

群组所有者可以使用 [SAML API](../../../api/saml.md#update-extern_uid-field-for-a-saml-identity) 更改其群组成员的 **NameID** 并更新他们的 SAML 身份。

如果配置了 [SCIM](scim_setup.md)，群组所有者可以使用 [SCIM API](../../../api/scim.md#update-extern_uid-field-for-a-scim-identity)。

或者，要求用户重新连接其 SAML 账户。

1. 要求相关用户[取消其账户与群组的关联](#unlink-accounts)。
1. 要求相关用户[将他们的账户关联到新的 SAML 应用程序](#link-saml-to-your-existing-gitlabcom-account)。

WARNING:
用户使用 SSO SAML 登录极狐GitLab 后，更改 **NameID** 值会破坏配置，并可能将用户锁定在极狐GitLab 群组之外。

有关特定身份提供商的建议值和格式的详细信息，请参阅[设置您的身份提供商](#set-up-your-identity-provider)。

### 从 SAML 响应配置用户设置

> 引入于 13.7 版本。

极狐GitLab 允许根据 SAML 响应中的值设置某些用户属性。
如果现有用户最初是由群组配置的，则该用户的属性将从 SAML 响应值进行更新。通过以下方式创建账户时，用户由群组配置：

- 通过 [SCIM](scim_setup.md)。
- 首次使用 SAML SSO 登录 SaaS 群组。

#### 支持的用户属性

- **can_create_group** - `true` 或 `false` 指示用户是否可以创建新群组。默认为 `true`。
- **projects_limit** - 用户可以创建的个人项目总数。`0` 值表示用户无法在其个人命名空间中创建新项目。默认值为 `10000`。

#### SAML 响应示例

您可以在开发者工具或浏览器控制台中找到 Base64 编码格式的 SAML 响应。使用您选择的 base64 解码工具将信息转换为 XML。以下为 SAML 响应示例。

```xml
   <saml2:AttributeStatement>
      <saml2:Attribute Name="email" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:basic">
         <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">user.email</saml2:AttributeValue>
      </saml2:Attribute>
      <saml2:Attribute Name="username" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:basic">
        <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">user.nickName</saml2:AttributeValue>
      </saml2:Attribute>
      <saml2:Attribute Name="first_name" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
         <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">user.firstName</saml2:AttributeValue>
      </saml2:Attribute>
      <saml2:Attribute Name="last_name" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
         <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">user.lastName</saml2:AttributeValue>
      </saml2:Attribute>
      <saml2:Attribute Name="can_create_group" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
         <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">true</saml2:AttributeValue>
      </saml2:Attribute>
      <saml2:Attribute Name="projects_limit" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified">
         <saml2:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">10</saml2:AttributeValue>
      </saml2:Attribute>
   </saml2:AttributeStatement>
```

### 使用经过验证的域名绕过用户电子邮件确认

> 引入于 15.4 版本。

默认情况下，配置有 SAML 或 SCIM 的用户会收到一封验证电子邮件来验证其身份。此外，您可以使用自定义域名配置极狐GitLab，极狐GitLab 会自动确认用户账户。用户仍然会收到企业用户欢迎电子邮件。符合以下条件的用户会绕过确认：

- 使用 SAML 或 SCIM 进行配置的用户。
- 拥有属于已验证域名的电子邮件地址的用户。

### 阻止用户访问

要在仅配置 SAML SSO 时撤销用户对群组的访问权限，请执行以下任一操作：

- 从以下位置（按顺序）删除用户：
  1. 身份提供商上的用户数据存储或特定应用程序上的用户列表。
  1. SaaS 群组。
- 在顶级群组使用群组同步<!--[群组同步](group_sync.md#automatic-member-removal)-->，并将默认角色设置为[最小访问权限](../../permissions.md#users-with-minimal-access)，从而自动阻止其对群组中所有资源的访问。

<!--
要在使用 SCIM 时取消用户对组的访问权限，请参阅[删除访问权限](scim_setup.md#remove-access)。
-->

<a id="unlink-accounts"></a>

### 取消关联账户

用户可以从其个人资料页面取消群组的 SAML 关联，特别适用于以下情况：

- 您不再希望某个群组能够让您登录 SaaS。
- 您的 SAML **NameID** 已更改，因此极狐GitLab 无法再找到您的用户。

WARNING:
取消关联账户会删除群组中分配给该用户的所有角色。如果用户重新关联其账户，则需要重新分配角色。

群组需要至少一名所有者。如果您的账户是该群组中的唯一所有者，则您无法取消该账户的关联。在这种情况下，请将另一个用户设置为群组所有者，然后您可以取消关联该帐户。

例如，要取消关联 `MyOrg` 账户：

1. 在左侧边栏中，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **账户**。
1. 在 **服务登录** 部分中，选择已连接账户旁边的 **断开连接**。

<a id="sso-enforcement"></a>

## 强制 SSO

对于以下情况，在 SaaS 上强制执行 SSO：

- 当启用 SAML SSO 时。
- 对于具有现有 SAML 身份的用户，在访问组织的群组层次结构中的群组和项目时。用户可以使用其 SaaS 凭据查看其他群组和项目及其用户设置，而无需 SSO 登录。

如果满足以下一项或两项条件，则用户拥有 SAML 身份：

- 用户已使用极狐GitLab 群组的单点登录 URL 登录到极狐GitLab。
- 用户由 SCIM 提供。

每次访问时，系统不会提示用户通过 SSO 登录。极狐GitLab 检查用户是否已通过 SSO 进行身份验证。如果用户上次登录已超过 24 小时，极狐GitLab 会提示用户通过 SSO 再次登录。

SSO 的执行方式如下：

| 项目/群组可见性 | 强制 SSO 设置 | 有身份的成员 | 无身份的成员 | 无成员或未登录 |
|--------------------------|---------------------|----------------------|-------------------------|-----------------------------|
| 私有                  | 关闭                 | 强制             | 非强制            | 非强制  |
| 私有                  | 打开                  | 强制               | 强制             | 强制                    |
| 公开                   | 关闭                 | 强制             | 非强制              | 非强制                  |
| 公开                   | 打开                  | 强制              | 强制                | 非强制                 |

<!--
An [issue exists](https://gitlab.com/gitlab-org/gitlab/-/issues/297389) to add a similar SSO requirement for API activity.
-->

### Web 活动强制执行仅 SSO 身份验证

当启用 **对该群组的 Web 活动强制执行仅 SSO 身份验证** 选项时：

- 所有成员都必须使用其极狐GitLab 群组的单点登录 URL 来访问极狐GitLab 群组资源，无论他们是否具有现有的 SAML 身份。
- 当用户访问组织的群组层次结构中的群组和项目时，会强制执行 SSO。用户无需 SSO 登录，可查看其他群组和项目。
- 无法手动将用户添加为新成员。
- 具有所有者角色的用户可以使用标准登录流程对顶级群组设置进行必要的更改。
- 对于非成员或未登录的用户：
  - 当他们访问公开群组资源时，不会强制执行 SSO。
  - 当他们访问私有群组资源时，会强制执行 SSO。

启用后，针对 Web 活动的 SSO 强制执行会产生以下影响：

- 对于群组，用户不能在顶级群组之外的群组中共享项目，即使该项目是派生的。
- 源自 CI/CD 作业的 Git 活动不强制执行 SSO 检查。
- 对于不与用户绑定的凭据（例如，项目和群组访问令牌以及部署密钥），不会强制执行 SSO 检查。
- 用户必须通过 SSO 登录才能使用[依赖项代理](../../packages/dependency_proxy/index.md)拉取镜像。
- 启用 **对该群组的 Git 和依赖代理活动强制执行仅 SSO 身份验证** 选项时，涉及 Git 活动的任何 API 端点都将受到 SSO 强制执行。例如，创建或删除分支、提交或标签。对于通过 SSH 和 HTTPS 进行的 Git 活动，用户必须至少有一个通过 SSO 登录的活动会话，然后才能向极狐GitLab 仓库推送或拉取。

当对 Web 活动强制执行 SSO 时，非 SSO 群组成员不会立即失去访问权限。如果用户：

- 具有活动会话，他们可以继续访问群组长达 24 小时，直到身份提供商会话超时。
- 已注销，从身份提供商中删除后他们无法访问该群组。

<!--
## Related topics

- [SAML SSO for self-managed GitLab instances](../../../integration/saml.md)
- [Glossary](../../../integration/saml.md#glossary)
- [Authentication comparison between SaaS and self-managed](../../../administration/auth/index.md#saas-vs-self-managed-comparison)
- [Passwords for users created through integrated authentication](../../../security/passwords_for_integrated_authentication_methods.md)
- [SAML Group Sync](group_sync.md)
-->

## 故障排除

如果您发现很难明白极狐GitLab 和身份提供商之间的不同 SAML 术语：

1. 查看您的身份提供商的文档，查看他们的 SAML 配置示例，了解有关他们使用的术语的信息。
1. 查看[用于私有化部署版实例的 SAML SSO 文档](../../../integration/saml.md)。私有化部署版实例的 SAML 配置文件支持比 SaaS 配置文件更多的选项。您可以在以下位置找到有关私有化部署版实例文件的信息：
   - 外部 [OmniAuth SAML 文档](https://github.com/omniauth/omniauth-saml/)
   - [`ruby-saml` 库](https://github.com/onelogin/ruby-saml)
1. 将您的提供商提供的 XML 响应，与我们的[用于内部测试的示例 XML](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/spec/fixtures/saml/response.xml) 进行比较。

<!--
For other troubleshooting information, see the [troubleshooting SAML guide](troubleshooting.md).
-->
