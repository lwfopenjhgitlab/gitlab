---
type: reference
stage: Manage
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 群组价值流分析 **(FREE)**

价值流分析提供有关软件开发过程每个阶段的指标。

**价值流**是为客户提供价值的整个工作流程。例如，[DevOps 生命周期](https://gitlab.cn/stages-devops-lifecycle/)是一个价值流，从管理阶段开始，到防护阶段结束。

使用价值流分析来识别：

- 从构思到生产所需的时间。
- 特定项目的速度。
- 开发过程中的瓶颈。
- 长期运行的议题或合并请求。
- 导致您的软件开发生命周期减慢的因素。

价值流分析可帮助企业：

- 可视化他们的端到端 DevSecOps 工作流。
- 识别并解决效率低下的问题。
- 优化他们的工作流，促使更快地提供更多价值。

价值流分析可用于项目和群组。

## 可用功能

价值流分析在项目和群组级别为 FOSS 和许可版本提供不同的功能。

- 在免费版上，价值流分析不聚合数据，而直接查询数据库，其中日期范围过滤器适用于议题和合并请求的创建日期。您可以使用预定义的默认阶段查看价值流分析。
- 在专业版上，价值流分析聚合数据，且结束事件适用于日期范围过滤器。您还可以创建、编辑和删除价值流。

| 功能 | 群组级别（licensed） | 项目级别（licensed） | 项目级别（FOSS） |
|-|-|-|-|
| 创建自定义价值流 | 是 | 是 | 否，只有一个价值流（默认）存在于默认阶段 |
| 创建自定义阶段| 是 | 是 | 否 |
| 过滤（例如按作者、标记、里程碑过滤) | 是 | 是 | 是 |
| 阶段时间图表 | 是 | 是 | 否 |
| 总时间图表| 是 | 是 | 否 |
| 任务类型图表 | 是 | 否 | 否 |
| DORA 指标 | 是 | 是 | 否 |
| 周期时间和前置时间总结（关键指标） | 是 | 是 | 否 |
| 新建议题、提交和部署（关键指标） |Yes, excluding commits| 是 | 是 |
| 使用聚合后端 | 是 | 是 | 否 |
| 日期过滤器 | 过滤日期范围内完成的事项 | 按创建日期过滤事项 | 按创建日期过滤事项 |
| 授权 | 至少为报告者 | 至少为报告者 | 可以公开 |

## 价值流分析工作原理

价值流分析计算软件开发过程每个阶段的持续时间。

价值流分析由三个核心对象组成：

- **价值流**包含价值流阶段列表。
- 每个价值流阶段列表包含一个或多个**阶段**。
- 每个阶段都有两个**事件**：开始和停止。

### 价值流阶段

一个阶段代表一个事件对（开始和结束事件）和额外的元数据，例如阶段的名称。您可以在后端定义的配对规则中配置阶段。

### 价值流

价值流是阶段的容器对象。每个群组可以有多个价值流，专注于 DevOps 生命周期的不同方面。

### 价值流阶段事件

事件是价值流分析功能的最小构成部分。一个阶段由开始事件和结束事件组成。

以下阶段事件可用：

- 已关闭议题
- 已创建议题
- 首次将议题添加到看板
- 首次指派议题
- 首次将议题关联到里程碑
- 首次提及议题
- 已添加议题标记
- 已移除议题标记
- 已关闭合并请求
- 已合并合并请求
- 已创建合并请求
- 首次提交合并请求的时间
- 首次指派合并请求
- 首次部署合并请求
- 已添加合并请求标记
- 已移除合并请求标记
- 合并请求的最新流水线的持续时间

这些事件在持续时间的计算中起着关键作用，持续时间的计算公式为：持续时间 = 结束事件时间 - 开始事件时间。

<!--
To learn what start and end events can be paired, see [Validating start and end events](../../../development/value_stream_analytics.md#validating-start-and-end-events).
-->

## 价值流分析如何测量每个阶段

价值流分析使用开始和结束事件来测量议题或合并请求在每个阶段花费的时间。

例如，一个阶段可能会在用户向议题添加标记时开始，并在他们添加另一个标记时结束。
如果事项尚未到达结束事件，则该事项不包含在阶段时间计算中。

价值流分析允许您根据预定义的事件自定义您的阶段。为了使配置更容易，系统提供了可用作模板的预定义阶段列表。

下表进一步描述了价值流分析的每个预定义阶段。

| 阶段  | 测量方法   |
|---------|----------------------|
| Issue   | 创建议题和采取行动解决议题之间的时间中位值，方法是标记议题或将其添加到里程碑。仅当标记已经包含为标记创建的议题看板列表时，才会跟踪标记。 |
| Plan    | 您为上一阶段执行的操作与将第一次提交推送到分支之间的时间中位数。第一个分支提交会触发从 **Plan** 到 **Code** 的转换，并且该分支中至少有一个提交必须包含相关的议题编号（例如 `#42`）。如果提交中不包含议题编号，则该数据不包含在阶段的测量时间中。 |
| Code    | 推送第一次提交（上一阶段）和创建合并请求之间的时间中位数。使用合并请求描述中的议题关闭 pattern 跟踪该过程。例如，如果使用 `Close #xxx` pattern 关闭议题，则 `xxx` 是合并请求的议题编号。如果没有关闭 pattern，则将开始时间设置为第一次提交的创建时间。 |
| Test    | 所有流水线从开始到结束的时间。测量为该项目运行整个流水线的时间中位数。与极狐GitLab CI/CD 为推送到该合并请求的提交运行每个作业所需的时间相关，如前一阶段所定义。 |
| Review  | 从创建到合并，评审具有关闭议题 pattern 的合并请求所需的时间中位值. |
| Staging | 将合并请求（具有关闭议题 pattern）合并到第一次部署到生产环境之间的时间中位值。没有生产环境就不会收集数据。 |

## 工作流示例

此示例显示了一天内通过所有七个阶段的工作流。在此示例中，已创建里程碑并配置了用于测试和设置环境的 CI。

- 09:00：创建议题。**Issue** 阶段开始。
- 11:00：将议题添加到里程碑，开始处理议题，并在本地创建分支。**Issue** 阶段结束，**Plan** 阶段开始。
- 12:00：进行第一次提交。
- 12:30：对提及议题编号的分支进行第二次提交。**Plan** 阶段结束，**Code** 阶段开始。
- 14:00：推送分支并创建包含议题关闭 pattern 的合并请求。**Code** 阶段结束，**Test** 和 **Review** 阶段开始。
- CI 需要 5 分钟来运行 [`.gitlab-ci.yml`](../../../ci/yaml/index.md) 中定义的脚本。**Test** 阶段结束。
- 审核合并请求。
- 19:00：合并合并请求。**Review** 阶段结束，**Staging** 阶段开始。
- 19:30：部署到“生产”环境开始并完成。**Staging** 阶段结束。

价值流分析记录每个阶段的以下时间：

- **Issue**：09:00 到 11:00：2 小时
- **Plan**：11:00 到 12:00：1 小时
- **Code**：12:00 到 14:00：2 小时
- **Test**：5 分支
- **Review**：14:00 到 19:00：5 小时
- **Staging**：19:00 到 19:30：30 分钟

此示例还有一些其它注意事项：

- 尽管此示例在稍后的提交中指定了议题编号，但该过程仍会收集议题的分析数据。
- **Test** 阶段所需的时间包含在 **Review** 流程中，因为每个合并请求都应进行测试。
- 此示例仅说明了多个阶段的一个循环。价值流分析仪表盘显示计算出的这些议题的平均运行时间。
- 价值流分析根据环境的部署级别识别生产环境。

## 价值流分析如何识别生产环境

价值流分析通过查找名称与以下任何样式匹配的项目环境来识别生产环境：

- `prod` 或 `prod/*`
- `production` 或 `production/*`

这些样式不区分大小写。

您可以在极狐GitLab CI/CD 配置中更改项目环境的名称。

## 查看价值流分析

先决条件：

- 您必须至少具有报告者角色才能查看群组的价值流分析。
- 您必须创建一个[自定义价值流](#create-a-value-stream-with-gitlab-default-stages)。价值流分析仅显示为您的群组或项目创建的自定义价值流。

要查看群组或项目的价值流分析：

1. 在顶部栏上，选择 **主菜单**，并且：
   - 对于项目价值流，选择 **项目** 并找到您的项目。
   - 对于群组价值流，选择 **群组** 并找到您的群组。
1. 在左侧边栏上，选择 **分析 > 价值流**。
1. 要查看特定阶段的指标，在 **筛选结果** 文本框下方，选择一个阶段。
1. 可选。过滤结果：
   1. 选择 **筛选结果** 文本框。
   1. 选择一个参数。
   1. 选择一个值或输入文本来优化结果。
   1. 要调整日期范围：
      - 在 **从** 字段中，选择开始日期。
      - 在 **到** 字段中，选择结束日期。
1. 可选。按升序或降序对结果进行排序：
       - 要按最新或最旧的工作流程事项排序，请选择 **最近活动** header。
       - 要按每个阶段花费的最多或最少时间排序，请选择 **时长** header。

该表显示所选阶段的相关工作流事项列表。根据您选择的阶段，可以是：

- 议题
- 合并请求

### 数据过滤器

您可以过滤价值流分析，查看符合特定条件的数据。支持以下过滤器：

- 日期范围
- 项目
- 指派人
- 作者
- 里程碑
- 标记

NOTE:
对于任务类型图表，只有日期范围和项目选择过滤器可用，标记过滤器和其他过滤器不适用，您需要从图表旁边的下拉列表中单独选择标记。

## 价值流分析指标

价值流分析中的**概览**页面显示了项目和群组的 DevSecOps 生命周期性能的关键指标。

### 关键指标

**概览**仪表盘显示以下衡量团队绩效的关键指标：

- 前置时间：从创建议题到关闭议题的中位时间。
- 周期时间：从第一次提交到议题关闭的中位时间。系统计算从[关联议题的合并请求](../../project/issues/crosslinking_issues.md#from-commit-messages)的最早提交，到该议题关闭的周期时间。周期时间小于前置时间，因为合并请求的创建总是晚于提交时间。
- 新议题：创建的新议题数量。
- 部署：部署到生产环境的总数。

### DORA 指标 **(ULTIMATE)**

> - 变更的前置时间 DORA 指标引入于 14.5 版本。
> - 群组价值流分析的 DORA API-based 部署指标从旗舰版移动到专业版于 14.3 版本。
> - 恢复服务时间引入于 15.0 版本。
> - 变更失败率引入于 15.0 版本。
> - 极狐GitLab 自 15.4 版本开始，将该功能从旗舰版降入专业版。而 GitLab CE 与 EE 版本中，该功能依旧为旗舰版。
> - 极狐GitLab 自 15.8 版本开始，将该功能从专业版调整为旗舰版。

价值流分析**概览**仪表盘显示以下 [DORA](../../../user/analytics/dora_metrics.md) 指标：

- 部署频率。
- 变更的前置时间。
- 恢复服务时间。
- 变更失败率。

DORA 指标是根据来自 [DORA API](../../../api/dora/metrics.md#devops-research-and-assessment-dora-key-metrics-api) 的数据计算的。

如果您有专业版或旗舰版订阅：

- 成功部署的数量是用 DORA 数据计算的。
- 数据根据环境和环境级别进行过滤。

NOTE:
在 13.9 及更高版本，部署频率指标是根据部署完成的时间计算的。在 13.8 及更早版本，部署频率指标是根据创建部署的时间计算的。

## 查看 DORA 指标和关键指标

先决条件：

- 要查看部署指标，您必须[配置生产环境](#how-value-stream-analytics-identifies-the-production-environment)。

查看关键生命周期指标：

1. 在顶部栏上，选择 **主菜单**，并且：
   - 对于项目价值流，选择 **项目** 并找到您的项目。
   - 对于群组价值流，选择 **群组** 并找到您的群组。
1. 在左侧边栏中，选择 **分析 > 价值流**。关键指标显示在 **筛选结果** 文本框下方。
1. 可选。过滤结果：
   1. 选择 **筛选结果** 文本框。根据您选择的过滤器，仪表盘会自动汇总关键指标并显示价值流的状态。
   1. 选择参数。
   1. 选择一个值或输入文本来优化结果。
   1. 要调整日期范围：
      - 在 **从** 字段中，选择开始日期。
      - 在 **到** 字段中，选择结束日期。

查看[价值流仪表盘](../../analytics/value_streams_dashboard.md)和 [DORA 指标](../../analytics/dora_metrics.md)：

1. 在顶部栏上，选择 **主菜单**，并且：
   - 对于项目价值流，选择 **项目** 并找到您的项目。
   - 对于群组价值流，选择 **群组** 并找到您的群组。
1. 在左侧边栏中，选择 **分析 > 价值流**。
1. 在 **筛选结果** 文本框下方的 **关键指标** 行中，选择 **价值流仪表盘 / DORA**。
1. 可选。要打开新页面，将此路径 `/analytics/dashboards/value_streams_dashboard` 附加到群组 URL（例如 `https://jihulab.com/groups/gitlab-cn/-/analytics/dashboards/value_streams_dashboard`）。

## 查看每个开发阶段的指标

> - 引入于 13.0 版本。
> - 功能标志删除于 13.12 版本。

价值流分析显示了每个开发阶段中议题或合并请求所花费的平均时间。

要查看一个群组在每个阶段花费的中位时间：

1. 在顶部栏上，选择 **主菜单**，并且：
   - 对于项目价值流，选择 **项目** 并找到您的项目。
   - 对于群组价值流，选择 **群组** 并找到您的群组。
1. 在左侧边栏上，选择 **分析 > 价值流**。
1. 可选。过滤结果：
   1. 选择 **筛选结果** 文本框。
   1. 选择一个参数。
   1. 选择一个值或输入文本以优化结果。
   1. 调整日期范围：
      - 在 **从** 字段中，选择开始日期。
      - 在 **到** 字段中，选择结束日期。
1. 要查看每个阶段的指标，在 **筛选结果** 文本框上方，将鼠标悬停在某个阶段上。

NOTE:
日期范围选择器按事件时间过滤项目。事件时间是特定项目的选定阶段完成的时间。

## 按类型查看任务 **(PREMIUM)**

**按类型划分任务**图表显示您的群组每天的议题和合并请求的累计数量。

该图表使用全局页面过滤器根据特定的群组和时间范围显示数据。

按类型查看任务：

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **分析 > 价值流**。
1. 在 **筛选结果** 文本框下方，选择 **概览**。**按类型划分任务** 图表显示在 **总时间** 图表下方。
1. 要在任务类型之间切换，请选择 **设置** (**{settings}**) 下拉列表并选择 **议题** 或 **合并请求**。
1. 要添加或删除标记，请选择 **设置** (**{settings}**) 下拉列表并选择或搜索标记。默认情况下，选择最上面的群组级标记（最多 10 个）。您最多可以选择 15 个标记。

## 创建价值流 **(PREMIUM)**

<a id="create-a-value-stream-with-gitlab-default-stages"></a>

## 使用默认阶段创建价值流

> 引入于 13.3 版本。

创建价值流时，您可以使用默认阶段，并隐藏或重新排序进行自定义。除了默认模板中提供的阶段之外，您还可以创建自定义阶段。

1. 在顶部栏上，选择 **主菜单**，并且：
   - 对于项目价值流，选择 **项目** 并找到您的项目。
   - 对于群组价值流，选择 **群组** 并找到您的群组。
1. 在左侧边栏中，选择 **分析 > 价值流**。
1. 选择 **创建新的价值流**。
1. 输入价值流的名称。
1. 选择 **从默认模板创建**。
1. 自定义默认阶段：
    - 要重新排序阶段，请选择向上或向下箭头。
    - 要隐藏阶段，请选择 **隐藏** (**{eye-slash}**)。
1. 要添加自定义阶段，请选择 **添加另一个阶段**。
    - 输入阶段名称。
    - 选择一个 **开始事件** 和一个 **停止事件**。
1. 选择 **创建价值流**。

NOTE:
如果您最近升级到专业版，则收集和显示数据最多可能需要 30 分钟。

## 使用自定义阶段创建价值流

创建价值流时，您可以创建和添加与您自己的开发工作流程相一致的自定义阶段。

1. 在顶部栏上，选择 **主菜单**，并且：
   - 对于项目价值流，选择 **项目** 并找到您的项目。
   - 对于群组价值流，选择 **群组** 并找到您的群组。
1. 在左侧边栏中，选择 **分析 > 价值流**。
1. 选择 **创建价值流**。
1. 对于每个阶段：
    - 输入阶段名称。
    - 选择一个 **开始事件** 和一个 **停止事件**。
1. 要添加另一个阶段，请选择 **添加另一个阶段**。
1. 要重新排序阶段，请选择向上或向下箭头。
1. 选择 **创建价值流**。

### 自定义价值流的基于标记的阶段

要衡量复杂的工作流程，您可以使用[范围标记](../../project/labels.md#scoped-labels)。例如，要测量从 staging 环境到生产环境的部署时间，您可以使用以下标签：

- 当代码部署到 staging 时，`workflow::staging` 标记被添加到合并请求中。
- 当代码部署到生产环境时，`workflow::production` 标记被添加到合并请求中。

![Label-based value stream analytics stage](img/vsa_label_based_stage_v14_0.png "Creating a label-based value stream analytics stage")

#### 自定义价值流配置示例

![Example configuration](img/object_hierarchy_example_V14_10.png "Example custom value stream configuration")

在上面的示例中，为在 **Test Group**（顶级命名空间）中使用不同开发工作流的两个团队设置了两个独立的价值流。

第一个价值流使用标准的基于时间戳的事件来定义阶段。第二个价值流使用标记事件。

## 编辑价值流 **(PREMIUM)**

> 引入于 13.10 版本

创建价值流后，您可以对其进行自定义以满足您的目的。编辑价值流：

1. 在顶部栏上，选择 **主菜单**，并且：
   - 对于项目价值流，选择 **项目** 并找到您的项目。
   - 对于群组价值流，选择 **群组** 并找到您的群组。
1. 在左侧边栏中，选择 **分析 > 价值流**。
1. 在右上角，选择下拉列表，然后选择相关的价值流。
1. 在价值流下拉列表旁边，选择 **编辑**。编辑表单中填入了价值流详细信息。
1. 可选：
     - 重命名价值流。
     - 隐藏或重新排序默认阶段。
     - 删除现有的自定义阶段。
     - 通过选择“添加另一个阶段”按钮添加新阶段
     - 选择 staging 的开始和结束事件。
1. 可选。要撤消任何修改，请选择 **恢复价值流默认值**。
1. 选择 **保存价值流**。

## 删除价值流 **(PREMIUM)**

要删除自定义价值流：

1. 在顶部栏上，选择 **主菜单**，并且：
   - 对于项目价值流，选择 **项目** 并找到您的项目。
   - 对于群组价值流，选择 **群组** 并找到您的群组。
1. 在左侧边栏中，选择 **分析 > 价值流**。
1. 在右上角，选择下拉列表，然后选择您要删除的价值流。
1. 选择 **删除（价值流名称）**。
1. 要确认，请选择 **删除**。

![Delete value stream](img/delete_value_stream_v13_12.png "Deleting a custom value stream")

## 查看一个周期完成的天数 **(PREMIUM)**

**总时间图表**显示完成开发周期所需的平均天数。
该图表显示最后 500 个工作流事项的数据。

1. 在顶部栏上，选择 **主菜单**，并且：
   - 对于项目价值流，选择 **项目** 并找到您的项目。
   - 对于群组价值流，选择 **群组** 并找到您的群组。
1. 在左侧边栏上，选择 **分析 > 价值流**。
1. 在 **筛选结果** 框上方，选择一个阶段：
    - 要查看所有阶段的周期时间摘要，请选择 **概览**。
    - 要查看特定阶段的周期时间，请选择一个阶段。
1. 可选。过滤结果：
    1. 选择 **筛选结果** 文本框。
    1. 选择一个参数。
    1. 选择一个值或输入文本来优化结果。
    1. 调整日期范围：
       - 在 **从** 字段中，选择开始日期。
       - 在 **到** 字段中，选择结束日期。

## 价值流分析的访问权限

价值流分析的访问权限取决于项目类型。

| 项目类型 | 权限                            |
|--------------|----------------------------------------|
| 公开       | 任何人都可以访问。                    |
| 内部     | 任何经过身份验证的用户都可以访问。     |
| 私有      | 任何访客及以上级别成员均可访问。 |

## 故障排除

### Sidekiq `cronjob:analytics_cycle_analytics` 的 CPU 利用率为 100%

价值流分析后台作业可能会通过独占 CPU 资源来强烈影响性能。

要从这种情况中恢复：

1. 在 [Rails 控制台](../../../administration/operations/rails_console.md)中禁用所有项目的功能，并删除现有作业：

   ```ruby
   Project.find_each do |p|
     p.analytics_access_level='disabled';
     p.save!
   end

   Analytics::CycleAnalytics::GroupStage.delete_all
   Analytics::CycleAnalytics::Aggregation.delete_all
   ```

1. 配置一个 [Sidekiq 路由](../../../administration/sidekiq/processing_specific_job_classes.md)，例如使用单个 `feature_category=value_stream_management` 和多个 `feature_category!=value_stream_management` 条目。在[企业版列表](../../../administration/sidekiq/processing_specific_job_classes.md#list-of-available-job-classes)中查找其他相关队列元数据。
1. 为一个又一个项目启用价值流分析。您可能需要根据您的性能要求进一步调整 Sidekiq 路由。

<!--
### How value stream analytics aggregates data

> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/335391) in GitLab 14.5 [with a flag](../../../administration/feature_flags.md) named `use_vsa_aggregated_tables`. Disabled by default.
> - Filter by stop date toggle [added](https://gitlab.com/gitlab-org/gitlab/-/issues/352428) in GitLab 14.9
> - Data refresh badge [added](https://gitlab.com/gitlab-org/gitlab/-/issues/341739) in GitLab 14.9

Plans for value stream analytics to filter items by stop event instead of start event are tracked in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/6046). With the completion of this work, value stream analytics will only display items with a stop event in the date range. 

To preview this functionality, you can use the **Filter by stop date** toggle to enable or disable this filter until the [default filtering mode is introduced](../../../update/deprecations.md#value-stream-analytics-filtering-calculation-change) and the toggle is removed.

If you turn on the **Filter by stop date** toggle, the results show items with a stop event within the date range. When this function is enabled, it may take up to 10 minutes for results to show due to data aggregation. There are occasions when it may take longer than 10 minutes for results to display:

- If this is the first time you are viewing value stream analytics and have not yet [created a value stream](#create-a-value-stream).
- If the group hierarchy has been re-arranged.
- If there have been bulk updates on issues and merge requests.

To view when the data was most recently updated, in the right corner next to **Edit**, hover over the **Last updated** badge. This badge is only available if you have turned on the **Filter by start date** toggle.
![Aggregated data toggle](img/vsa_aggregated_data_toggle_v14_9.png "Aggregated data toggle")
-->


