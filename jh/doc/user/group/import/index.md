---
type: reference, howto
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 迁移极狐GitLab 群组 **(FREE)**

迁移极狐GitLab 群组包括以下类型：

- 从私有化部署版极狐GitLab 迁移到 JiHuLab.com。
- 从 JiHuLab.com 迁移到私有化部署版极狐GitLab。
- 从一个私有化部署版极狐GitLab 实例迁移到另一个。
- 同一极狐GitLab 实例中的群组之间的迁移。

您可以通过两种方式迁移群组：

- 通过直接传输（推荐）。
- 通过上传导出文件。

如果您从私有化部署版极狐GitLab 迁移到 JiHuLab.com，管理员可以在私有化部署版实例上创建用户。

<a id="migrate-groups-by-direct-transfer-recommended"></a>

## 通过直接传输迁移群组（推荐）

> - 对群组资源的支持引入于 13.7 版本，功能标志为 `bulk_import`。默认禁用。
> - 对群组资源的支持在 SaaS 版和私有化部署版上启用于 14.3 版本。
> - 对项目资源的支持引入于 14.4 版本，功能标志为 `bulk_import_projects`。默认禁用。
> - 新的应用设置 `bulk_import_enabled` 引入于 15.8 版本，`bulk_import` 功能标志已删除。

FLAG:
在私有化部署版上，[迁移群组资源](#migrated-group-items)功能默认不可用，要在界面上显示此功能，请让管理员[在应用设置中启用它](../../admin_area/settings/visibility_and_access_controls.md#enable-migration-of-groups-and-projects-by-direct-transfer)。
同样在私有化部署版上，[迁移项目资源](#migrated-project-items-beta)功能默认不可用，要在界面上显示此功能，请让管理员[启用功能标志](../../../administration/feature_flags.md) `bulk_import_projects`。此功能尚未准备好用于生产。在 JiHuLab.com 上,

通过直接传输迁移群组，将群组从一个地方复制到另一个地方。您可以：

- 一次复制多个群组。
- 将顶级群组复制到：
  - 另一个顶级群组。
  - 任何现有顶级群组的子组。
  - 另一个实例，包括 JiHuLab.com
- 复制群组的同时复制项目（处于 beta 阶段，尚未准备好用于生产）或不复制项目。复制群组的同时复制项目在以下情况可用：
  - 在 JiHuLab.com 上默认可用。
  - 在管理员[启用了功能标志](../../../administration/feature_flags.md) `bulk_import_projects` 的私有化部署版实例上。

并非所有群组和项目资源都被复制。请参阅下面的复制资源列表：

- [迁移的群组资源](#migrated-group-items)。
- [迁移的项目资源](#migrated-project-items-beta)。

<!--
We invite you to leave your feedback about migrating by direct transfer in
[the feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/284495).
-->

如果您想移动群组而不是复制群组，如果群组在同一个实例中，您可以[转移群组](../manage.md#transfer-a-group)。转移群组是一种更快、更完整的选择。

### 可见性规则

迁移后：

- 私有群组和项目保持私密。
- 公开群组和项目：
   - 复制到公开群组时保持公开。
   - 复制到私有群组时变为私有。

如果在源实例上使用专用网络向公众隐藏内容，请确保在目标实例上进行类似设置，或导入到专用组中。

<a id="prerequisites"></a>

### 先决条件

> 在 16.0 版本中要求必须具有维护者角色，并向后移植到 15.11.1 版本和 15.10.5 版本。

通过直接传输迁移群组：

- 实例或 JiHuLab.com 之间的网络连接必须支持 HTTPS。
- 任何防火墙都不得阻止源实例和目标实例之间的连接。
- 两个实例都必须由实例管理员，[在应用程序设置中启用](../../admin_area/settings/visibility_and_access_controls.md#enable-migration-of-groups-and-projects-by-direct-transfer)通过直接传输群组迁移。
- 源实例必须运行 14.0 或更高版本。
- 您必须具有源实例的[个人访问令牌](../../../user/profile/personal_access_tokens.md)：
  - 对于 15.1 及更高版本的源实例，个人访问令牌必须具有 `api` 范围。
  - 对于 15.0 及更早版本的源实例，个人访问令牌必须同时具有 `api` 和 `read_repository` 范围。
- 您必须在源群组具有所有者角色。
- 您必须在目标群组至少具有维护者角色。

### 准备用户帐户

为确保极狐GitLab 正确映射用户及其贡献：

1. 在目标实例上创建所需的用户。迁移到 JiHuLab.com 时，您必须手动创建用户，除非使用 [SCIM](../../group/saml_sso/scim_setup.md)。使用 API 创建用户仅适用于私有化部署实例，因为它需要管理员访问权限。
1. 检查用户在源实例上的公开电子邮件是否与目标实例上的主电子邮件相匹配。
1. 确保用户在目标实例上确认他们的主要电子邮件地址。大多数用户都会收到一封电子邮件，要求他们确认他们的电子邮件地址。
1. 如果使用像 SAML 这样的 OmniAuth provider，请在极狐GitLab 上，将极狐GitLab 和用户的 SAML 帐户关联。目标实例上的所有用户都必须在目标实例上登录并验证他们的帐户。如果对 JiHuLab.com 群组使用 SAML SSO，用户必须将他们的 SAML 身份关联到他们的 JiHuLab.com 帐户。

### 连接源实例

创建要导入的群组并连接源实例：

1. 您可以选择创建：
   - 一个新群组。在顶部栏中，选择 **{plus-square}**，然后选择 **新建群组**，再选择 **导入群组**。
   - 一个新的子组。在现有群组的页面上，您可以：
     - 选择 **新建子组**。
     - 在顶部栏中，选择 **{plus-square}**，然后选择 **新建子组**。然后在左侧边栏上，选择 **导入现有群组** 链接。
1. 输入运行 14.0 或更高版本的极狐GitLab 实例的 URL。
1. 输入源实例的[个人访问令牌](../../../user/profile/personal_access_tokens.md)。
1. 选择 **连接实例**。

### 选择要导入的群组和项目

> 导入群组时是否同时导入项目的选项，引入于 15.8 版本。

在您授权访问源实例后，您将被重定向到极狐GitLab 组导入器页面。在这里，您可以看到已连接源实例上的顶级群组列表。

1. 默认情况下，建议的群组名称空间与源实例中存在的名称相匹配，但根据您的权限，您可以选择在继续导入任何名称之前编辑这些名称。
1. 在要导入的群组旁边，您可以选择：
   - **与项目一起导入**，处于 beta 阶段。此功能尚未准备好用于生产。
   - **不与项目一起导入**。
   - **导入**（适用于私有化部署版，`bulk_import_projects` 功能标志被禁用且该功能不可用时）。
1. **状态** 列显示每个群组的导入状态。如果您让页面保持打开状态，它会实时更新。
1. 导入群组后，选择其极狐GitLab 路径，打开其极狐GitLab URL。

### 群组导入历史

您可以在群组导入历史页面中，查看您直接迁移的所有群组。该列表包括：

- 源群组的路径。
- 目标群组的路径。
- 每次导入的开始日期。
- 每次导入的状态。
- 如果发生任何错误，则显示错误详细信息。

查看群组导入历史：

1. 登录极狐GitLab。
1. 在顶部栏中，选择 **创建新的…** (**{plus-square}**)。
1. 选择 **新建群组**。
1. 选择 **导入群组**。
1. 选择右上角的 **历史记录**。
1. 如果特定导入有任何错误，您可以通过选择 **详细信息** 来查看它们。

<a id="migrated-group-items"></a>

### 迁移的群组资源

群组的 [`import_export.yml`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/import_export/group/import_export.yml) 文件列出了使用迁移群组时迁移的许多资源。 

在您的极狐GitLab 版本的分支中查看此文件，可以查看与您相关的资源列表。

使用文件导出迁移项目，使用与在[群组](../custom_project_templates.md)和[实例](../../admin_area/custom_project_templates.md)级别从模板创建项目相同的导出和导入机制。因此，导出资源的列表是相同的。

迁移到目标实例的资源包括：

- 徽章（引入于 13.11 版本）
- 看板列表
- 看板 
- 史诗（引入于 13.7 版本）
  - 史诗资源状态事件（引入于 15.4 版本）
- 终结者
- 群组标记（引入于 13.9 版本）
- 迭代（引入于 13.10 版本）
- 迭代周期（引入于 15.4 版本）
- 成员（引入于 13.9 版本）
  在以下情况，群组成员与导入的群组相关联：
  - 用户已存在于目标实例中，并且
  - 用户在源实例中有与目标实例中确认的电子邮件匹配的公开电子邮件
- 里程碑（引入于 13.10 版本）
- 命名空间设置
- 发布
  - 里程碑（引入于 15.0 版本）
- 子组
- 上传文件

任何其他资源不会被迁移。

<a id="migrated-project-items-beta"></a>

### 迁移的项目资源 (beta)

> 引入于 14.4 版本，功能标志为 `bulk_import_projects`。默认禁用。

<!--
FLAG:
On self-managed GitLab, migrating project resources are not available by default. To make them available, ask an administrator to [enable the feature flag](../../../administration/feature_flags.md) named `bulk_import_projects`. On GitLab.com, migrating project resources are not available.
-->

如果您在[选择要迁移的群组](#select-the-groups-and-projects-to-import)时选择迁移项目，则项目中的资源将随项目一起迁移。

迁移的资源项取决于您在目标项目上使用的极狐GitLab 版本。要确定特定资源项是否迁移：

1. 检查目标项目的 [`projects/stage.rb`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/bulk_imports/projects/stage.rb) 文件和 [`projects/stage.rb`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/ee/bulk_imports/projects/stage.rb) 文件。例如，对于 15.9 版本：
   - <https://jihulab.com/gitlab-cn/gitlab/-/blob/15-9-stable-jh/lib/bulk_imports/projects/stage.rb>
   - <https://jihulab.com/gitlab-cn/gitlab/-/blob/15-9-stable-jh/ee/lib/ee/bulk_imports/projects/stage.rb>
1. 检查目标项目的 [`project/import_export.yml`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/import_export/project/import_export.yml) 文件。例如，对于 15.9 版本：<https://jihulab.com/gitlab-cn/gitlab/-/blob/15-9-stable-ee/lib/gitlab/import_export/project/import_export.yml>。

任何其他资源项都**不**迁移。

如果您选择不将项目与群组一起迁移，或者如果您想重试项目迁移，您可以使用 [API](../../../api/bulk_imports.md) 启动仅项目迁移。

WARNING:
通过直接传输迁移群组时迁移项目功能处于 [Beta](../../../policy/alpha-beta-support.md#beta) 状态，并且尚未准备好用于生产。

迁移到目标极狐GitLab 实例的资源项包括：

- 项目（引入于 14.4 版本）
  - Auto DevOps（引入于 14.6 版本）
  - 徽章（引入于 14.6 版本）
  - 分支（包括受保护的分支）（引入于 14.7 版本）
  - CI 流水线（引入于 14.6 版本）
  - 提交评论（引入于 15.10 版本）
  - 设计（引入于 15.1 版本）
  - 议题（引入于 14.4 版本）
    - 议题迭代（引入于 15.4 版本）
    - 议题资源状态事件（引入于 15.4 版本）
    - 议题资源里程碑事件（引入于 15.4 版本）
    - 议题资源迭代事件（引入于 15.4 版本）
    - 议题 URL 引用地址（引入于 15.6 版本）      
    - 工时统计（引入于 14.4 版本）
  - 议题看板（引入于 14.4 版本）
  - 标记（引入于 14.4 版本）
  - LFS 对象（引入于 14.8 版本）
  - 成员（引入于 14.8 版本）
  - 合并请求（引入于 14.5 版本）
    - 多个合并请求指派人（引入于 15.3 版本）
    - 合并请求审核者（引入于 15.3 版本）
    - 合并请求核准人（引入于 15.3 版本）
    - 合并请求资源状态事件（引入于 15.4 版本）
    - 合并请求资源里程碑事件（引入于 15.4 版本）
    - 合并请求 URL 引用地址（引入于 15.6 版本）
    - 工时统计（引入于 14.5 版本）
  - 推送规则（引入于 14.6 版本）
  - 里程碑（引入于 14.5 版本）
  - 外部拉取请求（引入于 14.5 版本）
  - 流水线历史（引入于 14.6 版本）
  - 流水线计划（引入于 14.8 版本）
  - 项目功能（引入于 14.6 版本）
  - 发布（引入于 15.1 版本）
  - 发布 Evidences（引入于 15.1 版本）
  - 仓库（引入于 14.4 版本）
  - 代码片段（引入于 14.6 版本）
  - 设置（引入于 14.6 版本）
    - 头像（引入于 14.6 版本）
    - 容器过期策略（引入于 14.6 版本）
    - 项目属性（引入于 14.6 版本）
    - 服务台（引入于 14.6 版本）
  - 上传文件（引入于 14.5 版本）
  - Wikis（引入于 14.6 版本）

因为包含敏感信息，被排除在迁移之外的资源包括：

- 流水线触发器

### 群组迁移故障排查

在 [rails 控制台会话](../../../administration/operations/rails_console.md#启动-rails-控制台会话)中，您可以使用以下命令查找群组导入尝试的失败或错误消息：

```ruby
# Get relevant import records
import = BulkImports::Entity.where(namespace_id: Group.id).map(&:bulk_import).last

# Alternative lookup by user
import = BulkImport.where(user_id: User.find(...)).last

# Get list of import entities. Each entity represents either a group or a project
entities = import.entities

# Get a list of entity failures
entities.map(&:failures).flatten

# Alternative failure lookup by status
entities.where(status: [-1]).pluck(:destination_name, :destination_namespace, :status)
```

#### 过时的导入

> 引入于 14.10 版本

在对群组迁移进行故障排除时，导入可能无法完成，因为导入 workers 的执行时间超过 8 小时。在这种情况下，`BulkImport` 或 `BulkImport::Entity` 的 `status` 为 `3`（`timeout`）：

```ruby
# Get relevant import records
import = BulkImports::Entity.where(namespace_id: Group.id).map(&:bulk_import)

import.status #=> 3 means that the import timed out.
```

#### 错误： `404 Group Not Found`

如果您尝试导入路径仅由数字组成的群组（例如，`5000`），极狐GitLab 会尝试通过 ID 而不是路径来查找群组。这会导致 `404 Group Not Found` 错误。要解决此问题，必须使用以下方法更改源组路径以包含非数字字符：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **高级**。
1. 在 **更改群组 URL** 下，更改群组 URL 以包含非数字字符。

