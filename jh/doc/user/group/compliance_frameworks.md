---
stage: Govern
group: Compliance
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 合规框架 **(PREMIUM)**

> - 引入于 13.9 版本。
> - 功能标志删除于 13.12 版本。

您可以创建一个作为标记的合规框架，标识您的项目具有某些合规要求或需要额外的监督。标签可以选择性地对应用它的项目强制执行合规流水线配置。

群组所有者可以创建、编辑和删除合规框架：

1. 在顶部栏上，选择 **主菜单 > 群组 > 查看所有群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置** > **通用**。
1. 展开 **合规框架** 部分。
1. 创建、编辑或删除合规框架。

## 设置默认合规框架

> 引入于 15.6 版本。

群组所有者可以设置默认的合规框架。默认框架应用于该群组中创建的所有新项目，不影响应用于现有项目的框架。无法删除默认框架。

### 用于设置默认合规框架的 GraphQL 变更示例

创建新的合规框架并将其设置为群组的默认框架。

```graphql
mutation {
    createComplianceFramework(
        input: {params: {name: "SOX", description: "Sarbanes-Oxley Act", color: "#87CEEB", default: true}, namespacePath: "gitlab-org"}
    ) {
        framework {
            id
            name
            default
            description
            color
            pipelineConfigurationFullPath
        }
        errors
    }
}
```

将现有合规框架设置为群组的默认框架。

```graphql
mutation {
    updateComplianceFramework(
        input: {id: "gid://gitlab/ComplianceManagement::Framework/<id>", params: {default: true}}
    ) {
        complianceFramework {
            id
            name
            default
            description
            color
            pipelineConfigurationFullPath
        }
    }
}
```

## 配置合规流水线 **(ULTIMATE)**

<!--
> - [Introduced](https://gitlab.com/groups/gitlab-org/-/epics/3156) in GitLab 13.9, disabled behind `ff_evaluate_group_level_compliance_pipeline` [feature flag](../../administration/feature_flags.md).
> - [Enabled by default](https://gitlab.com/gitlab-org/gitlab/-/issues/300324) in GitLab 13.11.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/331231) in GitLab 14.2.
-->

群组所有者可以在项目中配置与其他项目分开的合规流水线。默认情况下，运行合规流水线配置（`.gitlab-ci.yml` 文件）而不是标记项目的流水线配置。

但是，合规流水线配置可以引用标记项目的 `.gitlab-ci.yml` 文件，以便：

- 合规流水线可以运行标记项目流水线的作业，允许集中控制流水线配置。
- 合规流水线中定义的作业和变量不能被标记项目的 `.gitlab-ci.yml` 文件中的变量更改。

<!--
See [example configuration](#example-configuration) for help configuring a compliance pipeline that runs jobs from
labeled project pipeline configuration.
-->

要配置合规流水线：

1. 在顶部栏上，选择 **主菜单 > 群组 > 查看所有群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置** > **通用**。
1. 展开 **合规框架** 部分。
1. 在 **合规流水线配置（可选）** 中，添加合规框架配置的路径。使用 `path/file.y[a]ml@group-name/project-name` 格式。例如：

    - `.compliance-ci.yml@gitlab-org/gitlab`.
    - `.compliance-ci.yaml@gitlab-org/gitlab`.

此配置由应用了合规框架标记的项目继承。在应用了合规框架标记的项目中，运行合规流水线配置而不是标记项目自己的流水线配置。

在标记项目中运行流水线的用户必须至少具有合规项目的报告者角色。

当用于强制执行扫描时，此功能与扫描执行策略有一些重叠。我们还没有统一这两个功能的用户体验。

### 示例配置

以下示例 `.compliance-gitlab-ci.yml` 包含 `include` 关键字，以确保标记的项目流水线配置也被执行。

```yaml
# Allows compliance team to control the ordering and interweaving of stages/jobs.
# Stages without jobs defined will remain hidden.
stages:
  - pre-compliance
  - build
  - test
  - pre-deploy-compliance
  - deploy
  - post-compliance

variables:  # Can be overridden by setting a job-specific variable in project's local .gitlab-ci.yml
  FOO: sast

sast:  # None of these attributes can be overridden by a project's local .gitlab-ci.yml
  variables:
    FOO: sast
  image: ruby:2.6
  stage: pre-compliance
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never
    - when: always  # or when: on_success
  allow_failure: false
  before_script:
    - "# No before scripts."
  script:
    - echo "running $FOO"
  after_script:
    - "# No after scripts."

sanity check:
  image: ruby:2.6
  stage: pre-deploy-compliance
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never
    - when: always  # or when: on_success
  allow_failure: false
  before_script:
    - "# No before scripts."
  script:
    - echo "running $FOO"
  after_script:
    - "# No after scripts."

audit trail:
  image: ruby:2.7
  stage: post-compliance
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never
    - when: always  # or when: on_success
  allow_failure: false
  before_script:
    - "# No before scripts."
  script:
    - echo "running $FOO"
  after_script:
    - "# No after scripts."

include:  # Execute individual project's configuration (if project contains .gitlab-ci.yml)
  project: '$CI_PROJECT_PATH'
  file: '$CI_CONFIG_PATH'
  ref: '$CI_COMMIT_REF_NAME' # Must be defined or MR pipelines always use the use default branch
```

#### 源自项目派生的合并请求中的 CF 流水线

当一个 MR 起源于一个派生项目时，被合并的分支通常只存在于派生项目中。在针对具有 CF 流水线的项目创建此类 MR 时，上述代码段将失败，并显示 `Project <project-name> reference <branch-name> does not exist!` 错误消息。
这是因为在目标项目的上下文中，`$CI_COMMIT_REF_NAME` 被认为不存在的分支名称。

要获得正确的上下文，请使用 `$CI_MERGE_REQUEST_SOURCE_PROJECT_PATH` 而不是 `$CI_PROJECT_PATH`。此变量仅在[合并请求流水线](../../ci/pipelines/merge_request_pipelines.md)中可用。

例如，对于同时支持分支流水线以及源自项目分支的合并请求流水线的配置，您需要[将 `include` 指令与 `rules:if` 结合起来](../../ci/yaml/includes.md#use-rules-with-include)：

```yaml
include:  # Execute individual project's configuration (if project contains .gitlab-ci.yml)
  - project: '$CI_MERGE_REQUEST_SOURCE_PROJECT_PATH'
    file: '$CI_CONFIG_PATH'
    ref: '$CI_COMMIT_REF_NAME'
    rules:
      - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  - project: '$CI_PROJECT_PATH'
    file: '$CI_CONFIG_PATH'
    ref: '$CI_COMMIT_REF_NAME'
    rules:
      - if: $CI_PIPELINE_SOURCE != 'merge_request_event'
```

## 确保始终运行合规作业

合规流水线[使用极狐GitLab CI/CD](../../ci/index.md)，为您提供了难以置信的灵活性，来定义您喜欢的任何类型的合规作业。根据您的目标，这些作业可以配置为：

- 由用户修改。
- 不可修改。

通常，如果合规作业中的值：

- 已设置，它不能被项目级配置更改或覆盖。
- 未设置，可以设置项目级配置。

根据您的用例，可能需要或不需要。

有一些最佳实践可确保这些作业始终完全按照您定义的方式运行，并且下游的项目级流水线配置无法更改它们：

- 将 [`rules:when:always` 块](../../ci/yaml/index.md#when)添加到您的每个合规作业中，确保它们是不可修改的并且始终运行。
- 明确设置作业引用的任何[变量](../../ci/yaml/index.md#variables)：
    - 确保项目级流水线配置不会设置它们并改变它们的行为。
    - 包括任何推动作业逻辑的作业。
- 显式设置[容器镜像](../../ci/yaml/index.md#image)，在其中运行作业。这可确保您的脚本步骤在正确的环境中执行。
- 显式设置任何相关的极狐GitLab 预定义的[作业关键字](../../ci/yaml/index.md#job-keywords)。这可确保您的作业使用您想要的设置，并且它们不会被项目级流水线覆盖。

