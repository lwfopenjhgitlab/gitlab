---
type: reference
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 群组级 Kubernetes 集群（基于证书）（已弃用） **(FREE)**

> 废弃于 14.5 版本。

WARNING:
此功能已废弃于 14.5 版本。要将集群连接到极狐GitLab，请使用[极狐GitLab 代理](../../clusters/agent/index.md)。

与项目级和实例级 Kubernetes 集群类似，群组级 Kubernetes 集群允许您将 Kubernetes 集群连接到您的群组，使您能够跨多个项目使用同一个集群。

要查看您的群组级 Kubernetes 集群：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **Kubernetes**。

## 集群管理项目

将[集群管理项目](../../clusters/management_project.md)附加到您的集群，来管理需要 `cluster-admin` 权限才能安装的共享资源，例如 Ingress 控制器。

## RBAC 兼容性

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/29398) in GitLab 11.4.
> - Project namespace restriction was [introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/51716) in GitLab 11.5.
-->

对于具有 Kubernetes 集群的群组下的每个项目，极狐GitLab 在项目命名空间中，创建一个具有 [`edit` 权限](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#user-facing-roles)的受限服务帐户。

<a id="cluster-precedence"></a>

## 集群优先级

如果项目的集群可用且未被禁用，极狐GitLab 在使用属于包含该项目的群组的任何集群之前，优先使用项目的集群。
对于子组，极狐GitLab 使用与项目最近的上级群组的集群，前提是集群未禁用。

<a id="multiple-kubernetes-clusters"></a>

## 多个 Kubernetes 集群

您可以将多个 Kubernetes 集群关联到您的群组，并为不同的环境（例如开发、staging 和生产）维护不同的集群。

添加另一个集群时，[设置环境范围](#environment-scopes)，帮助将新集群与其它集群区分开来。

## 极狐GitLab 托管集群

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/22011) in GitLab 11.5.
> - Became [optional](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/26565) in GitLab 11.11.
-->

您可以选择允许极狐GitLab 为您管理集群。如果极狐GitLab 托管您的集群，则会自动为您的项目创建资源。有关极狐GitLab 为您创建的资源的详细信息，请参阅[访问控制](../../project/clusters/cluster_access.md)部分。

对于不受极狐GitLab 托管的集群，不会自动创建特定于项目的资源。如果您将 [Auto DevOps](../../../topics/autodevops/index.md) 用于未由极狐GitLab 托管的集群的部署，则必须确保：

- 项目的部署服务帐户有权部署到 [`KUBE_NAMESPACE`](../../project/clusters/deploy_to_cluster.md#deployment-variables)。
- `KUBECONFIG` 正确反映了对 `KUBE_NAMESPACE` 的任何更改（这不是自动的）。不鼓励直接编辑 `KUBE_NAMESPACE`。

### 清除集群缓存

如果您选择允许极狐GitLab 为您托管集群，极狐GitLab 会存储它为您的项目创建的命名空间和服务帐户的缓存版本。如果您手动修改集群中的这些资源，此缓存可能会与您的集群不同步，从而导致部署作业失败。

要清除缓存：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **Kubernetes**。
1. 选择您的集群。
1. 展开 **高级设置**。
1. 选择 **清除集群缓存**。

## 基础域名

集群级别的域名允许[多个 Kubernetes 集群](#multiple-kubernetes-clusters)支持多个域名，将在 [Auto DevOps](../../../topics/autodevops/index.md) 阶段自动设置为环境变量（`KUBE_INGRESS_BASE_DOMAIN`）。

域名应该有一个配置为入口 IP 地址的通配符 DNS。[更多详细信息](../../project/clusters/gitlab_managed_clusters.md#base-domain)。

<a id="environment-scopes"></a>

## 环境范围 **(PREMIUM)**

当向您的项目添加多个 Kubernetes 集群时，您需要使用环境范围来区分它们。环境范围将集群与[环境](../../../ci/environments/index.md)相关联，类似于[特定于环境的 CI/CD 变量](../../../ci/variables/index.md#limit-the-environment-scope-of-a-cicd-variable)作业。

在评估哪个环境与集群的环境范围匹配时，[集群优先级](#cluster-precedence)生效。项目级别的集群优先，其次是最近的上级组，然后是该组的父组，依此类推。

例如，您的项目具有以下 Kubernetes 集群：

| 集群    | 环境范围   | 哪个级别     |
| ---------- | ------------------- | ----------|
| Project    | `*`                 | 项目   |
| Staging    | `staging/*`         | 项目   |
| Production | `production/*`      | 项目   |
| Test       | `test`              | 群组     |
| Development | `*`                 | 群组     |

并且在 [`.gitlab-ci.yml`](../../../ci/yaml/index.md) 中设置了以下环境：

```yaml
stages:
  - test
  - deploy

test:
  stage: test
  script: sh test

deploy to staging:
  stage: deploy
  script: make deploy
  environment:
    name: staging/$CI_COMMIT_REF_NAME
    url: https://staging.example.com/

deploy to production:
  stage: deploy
  script: make deploy
  environment:
    name: production/$CI_COMMIT_REF_NAME
    url: https://example.com/
```

结果是：

- Project 集群用于 `test` 作业。
- Staging 集群用于 `deploy to staging` 作业。
- Production 集群用于 `deploy to production` 作业。

## 集群环境 **(PREMIUM)**

有关将哪些 CI [环境](../../../ci/environments/index.md)部署到 Kubernetes 集群的综合视图，请参阅[集群环境](../../clusters/environments.md)。

## runners 安全

有关安全配置 runner 的重要信息，请参阅项目级集群的 [runner 安全性](../../project/clusters/cluster_access.md#security-of-runners)文档。

## 更多信息

有关集成极狐GitLab 和 Kubernetes 的信息，请参阅 [Kubernetes 集群](../../infrastructure/clusters/index.md)。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
