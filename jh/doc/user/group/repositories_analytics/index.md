---
stage: Verify
group: Pipeline Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 群组的仓库分析 **(PREMIUM)**

群组的仓库分析提供有关群组中所有项目的测试覆盖率的信息。<!--An
[issue exists](https://gitlab.com/gitlab-org/gitlab/-/issues/273527) to also extend support for all projects in
subgroups.-->

它类似于[项目的仓库分析](../../analytics/repository_analytics.md)。

## 当前群组代码覆盖率

> 引入于 13.7 版本

**分析 > 仓库** 群组页面显示群组中所有项目的整体测试覆盖率。
在 **整体活动** 部分，您可以看到：

- 具有覆盖率报告的项目数量。
- 所有项目的平均覆盖率。
- 生成覆盖率报告的流水线作业总数。

## 过去 30 天的平均群组测试覆盖率

> 引入于 13.9 版本

**分析 > 仓库** 群组页面以图表的形式，显示过去 30 天您群组中所有项目的平均测试覆盖率。

## 最新项目测试覆盖率列表

> 引入于 13.6 版本

要查看群组中每个项目的最新代码覆盖率：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **分析 > 仓库**。
1. 在 **最新的测试覆盖率结果** 部分，从 **选择项目** 下拉列表中，选择您要检查的项目。

您可以使用代码覆盖率历史，下载特定项目的代码覆盖率数据。

## 下载历史测试覆盖率数据

您可以获得群组中所有项目的代码覆盖率数据的 CSV。此报告最多有 1000 条记录。代码覆盖率数据来自每个项目的默认分支。

获取报告：

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **分析 > 仓库**。
1. 选择 **下载历史测试覆盖率数据 (.csv)**。
1. 选择您要包含在报告中的项目和日期范围。
1. 选择 **下载测试覆盖率数据 (.csv)**。

项目下拉列表显示您群组中最多 100 个项目，如果您要检查的项目不在下拉列表中，您可以选择 **所有项目** 下载您群组中所有项目的报告，包括任何未列出的项目。<!--There is a plan to improve this behavior in this [related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/250684).-->

对于项目流水线中的作业生成的覆盖率报告的每一天，CSV 中的一行包括：

- 覆盖率作业运行的日期
- 生成覆盖率报告的作业名称
- 项目名称
- 覆盖率值

如果一天中多次计算项目的代码覆盖率，则使用当天的最后一个值。

NOTE:
在 13.7 及更高版本中，群组代码覆盖率数据取自配置的[默认分支](../../project/repository/branches/default.md)。在早期版本中，它取自 `master` 分支。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
