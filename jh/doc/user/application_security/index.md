---
stage: Secure
group: Static Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 保护您的应用 **(ULTIMATE)**

极狐GitLab 可以检查您的应用程序是否存在安全漏洞，包括：

- 越权存取。
- 数据泄露。
- 拒绝服务攻击。

合并请求中包含有关漏洞的统计信息和详细信息。在合并更改之前，提供可操作的信息，使您能够积极主动。

为了帮助管理和解决漏洞的任务，极狐GitLab 提供了一个安全仪表盘，您可以从您的项目或群组访问。有关更多详细信息，请参阅[安全仪表盘](security_dashboard/index.md)。

<a id="application-coverage"></a>

## 应用范围

极狐GitLab 分析应用程序的各种细节，无论是作为 CI/CD 流水线的一部分还是在计划中的内容。覆盖范围包括：

- 源代码。
- 项目或容器镜像中的依赖项。
- 正在运行的 Web 应用程序中的漏洞。
- 基础设施即代码配置。

每个极狐GitLab 应用程序安全工具都与功能开发工作流程的特定阶段相关。

- Commit
  - SAST
  - Secret 检测
  - IaC 扫描
  - 依赖项扫描
  - 许可证扫描
  - 覆盖率引导的模糊测试
- Build
  - 容器扫描
- Test
  - API 安全
  - DAST
- Deploy
  - 运营容器扫描

![CI/CD stages and matching GitLab application security tools](img/secure_tools_and_cicd_stages.png)

### 源码分析

每次代码提交时都会进行源代码分析。合并请求中提供了检测到的漏洞的详细信息。

源代码分析可以：

- 分析源代码的漏洞 - 静态应用程序安全测试 (SAST)<!--[静态应用程序安全测试 (SAST)](sast/index.md)-->。
- 分析 Git 仓库的 secret 历史记录 - [Secret 检测](secret_detection/index.md)。

### 分析正在运行的 Web 应用程序

每次代码提交时都会对 Web 应用程序进行分析。作为 CI/CD 流水线的一部分，您的应用程序将被构建、部署到测试环境，并接受以下测试：

- 测试已知应用程序向量 - 动态应用程序安全测试 (DAST)<!--[动态应用程序安全测试 (DAST)](dast/index.md)-->。
- 分析已知攻击向量的 API - DAST API<!--[DAST API](dast_api/index.md)-->。
- 分析 Web API 的未知错误和漏洞 - API fuzzing<!--[API fuzzing](api_fuzzing/index.md)-->。

### 依赖分析

每次代码提交时都会进行依赖分析。您的应用程序的依赖项会根据已知漏洞的数据库进行整理和检查。

依赖分析可以运行：

- 在构建时 - [依赖扫描](dependency_scanning/index.md)。
- 对于使用容器镜像的项目，也在构建最终容器镜像之后 - [容器扫描](container_scanning/index.md)。

有关更多详细信息，请参阅[依赖扫描与容器扫描对比](dependency_scanning/index.md#dependency-scanning-compared-to-container-scanning)。

此外，可以定期或有周期地分析运营中的容器镜像的依赖关系是否存在漏洞。<!--For more details, see [Cluster Image Scanning](cluster_image_scanning/index.md).-->

### 基础设施分析

您的应用程序的基础设施是潜在漏洞的来源。为了帮助防御这种情况，每个合并请求都会进行基础架构分析：

- 定义应用程序部署环境的基础架构即代码 (IaC) 配置文件 - 基础架构即代码 (IaC) 扫描<!--[基础架构即代码 (IaC) 扫描](iac_scanning/index.md)-->。

<a id="using-private-maven-repositories"></a>

## 使用私有 Maven 仓库

如果您有一个需要登录凭据的私有 Apache Maven 仓库，您可以使用 `MAVEN_CLI_OPTS` CI/CD 变量来传递用户名和密码。您可以在项目设置下设置它，这样您的凭据就不会暴露在 `.gitlab-ci.yml` 中。

如果用户名是 `myuser` 并且密码是 `verysecret`，那么您可以在项目设置中，[设置以下变量](../../ci/variables/index.md#custom-cicd-variables)：

| 类型    | 键              | 值 |
| -------- | ---------------- | ----- |
| Variable | `MAVEN_CLI_OPTS` | `--settings mysettings.xml -Drepository.password=verysecret -Drepository.user=myuser` |

```xml
<!-- mysettings.xml -->
<settings>
    ...
    <servers>
        <server>
            <id>private_server</id>
            <username>${private.username}</username>
            <password>${private.password}</password>
        </server>
    </servers>
</settings>
```
