---
stage: Secure
group: Static Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 安全和治理术语 **(FREE)**

术语表旨在实现以下目标：

- 推广一种可以在任何地方使用的无处不在的语言 —— 针对客户、在议题上、在 Slack 中、在代码中。
- 提高团队成员之间沟通的有效性。
- 减少沟通不畅的可能性。
- 让新的团队成员和社区贡献者加快速度，缩短达到生产力的时间。

本文档中概述的术语定义在极狐GitLab 产品的上下文中。因此，对于极狐GitLab 之外的用户，一个术语可能具有不同的含义。

## 术语

### 分析器

执行扫描的软件。扫描分析攻击面的漏洞并生成包含结果的报告。报告遵循[安全报告格式](#secure-report-format)。

分析器使用 CI 作业集成到极狐GitLab。分析器生成的报告在作业完成后作为产物发布。极狐GitLab 提取此报告，允许用户可视化和管理发现的漏洞。<!--For more information, see [Security Scanner Integration](../../../development/integrations/secure.md).-->

许多极狐GitLab 分析器遵循使用 Docker 运行包装扫描器的标准方法。例如，Docker 镜像 `semgrep` 是一个包装了扫描器 `Semgrep` 的分析器。

### 攻击面

应用程序中易受攻击的不同位置。安全产品在扫描期间发现并搜索攻击面。每个产品对攻击面的定义不同。例如，SAST 使用文件和行号，而 DAST 使用 URL。

### 语料库

模糊测试运行时生成的一组有意义的测试用例。每个有意义的测试用例都会在被测程序中产生新的覆盖率。建议重新使用语料库并将其传递给后续运行。

### CNA

[CVE](#cve) 编号机构 (CNA) 是来自世界各地的组织，由 [Mitre Corporation](https://cve.mitre.org/) 授权，可将 [CVE](#cve) 分配给各自范围内的产品或服务中的漏洞。<!--[GitLab is a CNA](https://about.gitlab.com/security/cve/).-->

### CVE

Common Vulnerabilities and Exposures (CVE®) 是网络安全漏洞的常见标识符列表。该列表由 [Mitre Corporation] (https://cve.mitre.org/) 管理。

### CVSS

通用漏洞评分系统 (CVSS) 是一个免费且开放的行业标准，用于评估计算机系统安全漏洞的严重性。

### CWE

Common Weakness Enumeration (CWE™) 是社区开发的具有安全影响的常见软件和硬件漏洞类型列表。弱点是软件或硬件实现、代码、设计或架构中的缺陷、故障、错误、漏洞或其他错误。如果不加以解决，弱点可能会导致系统、网络或硬件容易受到攻击。CWE 列表和相关的分类分类作为一种语言，您可以使用它来识别和描述 CWE 方面的这些弱点。

<!--
### Deduplication

When a category's process deems findings to be the same, or if they are similar enough that a noise reduction is
required, only one finding is kept and the others are eliminated. Read more about the [deduplication process](../vulnerability_report/pipeline.md#deduplication-process).
-->

### 重复发现

多次报告的合法发现。当不同的扫描器发现相同的发现时，或者当一次扫描无意中多次报告相同的发现时，可能会发生这种情况。

### False positive

不存在但被错误地报告为存在的发现。

<a id="finding"></a>

### 发现

由分析器在项目中识别出的可能易受攻击的 assets，包括但不限于源代码、二进制包、容器、依赖项、网络、应用程序和基础设施。

调查结果是扫描程序在 MR/功能分支中识别的所有潜在漏洞项目。只有在合并为默认值后，发现才会成为[漏洞](#vulnerability)。

您可以通过两种方式与漏洞发现进行交互。

1. 您可以为漏洞发现打开一个议题或合并请求。
1. 您可以忽略漏洞发现。忽略该发现会将其隐藏在默认视图中。

### 分组

当有多个可能相关但不符合重复数据删除条件的发现时，一种灵活且非破坏性的方式，以可视化方式将漏洞分组。例如，您可以包含应该一起评估、将由相同操作修复、或来自相同来源的发现。

### 微不足道的发现

特定客户不关心的合法发现。

<a id="location-fingerprint"></a>

### 位置指纹

发现的位置指纹是一个文本值，对于攻击面上的每个位置都是唯一的。每个安全产品都根据其攻击面的类型来定义这一点。例如，SAST 包含文件路径和行号。

### 包管理器和包类型

#### 包管理器

包管理器是一个管理项目依赖项的系统。

包管理器提供了一种方法来安装新的依赖项（也称为“软件包”），管理包在文件系统上的存储位置，并为您提供发布自己的包的功能。

#### 包类型

每个包管理器、平台、类型或生态系统都有自己的约定和协议来识别、定位和提供软件包。

下表是文档和软件工具中引用的一些包管理器和类型的非详尽列表。

<style>
table.package-managers-and-types tr:nth-child(even) {
    background-color: transparent;
}

table.package-managers-and-types td {
    border-left: 1px solid #dbdbdb;
    border-right: 1px solid #dbdbdb;
    border-bottom: 1px solid #dbdbdb;
}

table.package-managers-and-types tr td:first-child {
    border-left: 0;
}

table.package-managers-and-types tr td:last-child {
    border-right: 0;
}

table.package-managers-and-types ul {
    font-size: 1em;
    list-style-type: none;
    padding-left: 0px;
    margin-bottom: 0px;
}
</style>

<table class="package-managers-and-types">
  <thead>
    <tr>
      <th>包类型</th>
      <th>包管理器</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>gem</td>
      <td><a href="https://bundler.io/">Bundler</a></td>
    </tr>
    <tr>
      <td>Packagist</td>
      <td><a href="https://getcomposer.org/">Composer</a></td>
    </tr>
    <tr>
      <td>Conan</td>
      <td><a href="https://conan.io/">Conan</a></td>
    </tr>
    <tr>
      <td>go</td>
      <td><a href="https://go.dev/blog/using-go-modules">go</a></td>
    </tr>
    <tr>
      <td rowspan="3">maven</td>
      <td><a href="https://gradle.org/">Gradle</a></td>
    </tr>
    <tr>
      <td><a href="https://maven.apache.org/">Maven</a></td>
    </tr>
    <tr>
      <td><a href="https://www.scala-sbt.org">sbt</a></td>
    </tr>
    <tr>
      <td rowspan="2">npm</td>
      <td><a href="https://www.npmjs.com">npm</a></td>
    </tr>
    <tr>
      <td><a href="https://classic.yarnpkg.com/en">yarn</a></td>
    </tr>
    <tr>
      <td>Nuget</td>
      <td><a href="https://www.nuget.org/">Nuget</a></td>
    </tr>
    <tr>
      <td rowspan="4">Pypi</td>
      <td><a href="https://setuptools.pypa.io/en/latest/">Setuptools</a></td>
    </tr>
    <tr>
      <td><a href="https://pip.pypa.io/en/stable">pip</a></td>
    </tr>
    <tr>
      <td><a href="https://pipenv.pypa.io/en/latest">Pipenv</a></td>
    </tr>
    <tr>
      <td><a href="https://python-poetry.org/">Poetry</a></td>
    </tr>
  </tbody>
</table>

### 流水线安全选项卡

显示在关联 CI 流水线中发现的结果的页面。

### 后置过滤器（Post-filter）

后置过滤器有助于减少扫描器结果中的噪音并自动执行手动任务。您可以指定根据扫描程序结果更新或修改漏洞数据的标准。例如，您可以将发现标记为可能的误报，并自动解决不再检测到的漏洞。这些不是永久性操作，可以更改。

### 前置过滤器（Pre-filter）

在分析发生之前为过滤掉目标而执行的不可逆转的操作。这通常是为了让用户减少范围和噪音以及加快分析速度。如果需要记录，则不应这样做，因为我们目前不存储与跳过/排除的代码或 assets 相关的任何内容。

示例：`DS_EXCLUDED_PATHS` 应根据提供的路径从扫描中排除文件和目录。`

### 主要标识符

结果的主要标识符是该结果的唯一值。 结果的第一个标识符的外部类型和外部 ID 结合起来创建值。

主要标识符的示例包括 OWASP Zed 攻击代理 (ZAP) 的 `PluginID` 或 Trivy 的 `CVE`。请注意，标识符必须是稳定的。后续扫描必须为相同的结果返回相同的值，即使位置略有变化。

<a id="report-finding"></a>

### 报告发现

[发现](#finding)仅存在于分析器生成的报告中，尚未持久化到数据库中。报告结果一旦导入数据库，就会变成[漏洞发现](#vulnerability-finding)。

<a id="scan-type-report-type"></a>

### 扫描类型（报告类型）

描述扫描类型，必须是以下之一：

- `api_fuzzing`
- `cluster_image_scanning`
- `container_scanning`
- `coverage_fuzzing`
- `dast`
- `dependency_scanning`
- `sast`
- `secret_detection`

随着扫描器的添加，此列表可能会发生变化。

<a id="scanner"></a>

### 扫描器

可以扫描漏洞的软件。生成的扫描报告通常不是[安全报告格式](#secure-report-format)。示例包括 ESLint、Trivy 和 ZAP。

### 安全产品

一组与特定应用程序安全领域相关的功能，由极狐GitLab 提供一流的支持。产品包括容器扫描、依赖扫描、动态应用程序安全测试 (DAST)、Secret 检测、静态应用程序安全测试 (SAST) 和模糊测试。这些产品中的每一个通常都包括一个或多个分析器。

<a id="secure-report-format"></a>

### 安全报告格式

安全产品在创建 JSON 报告时遵守的标准报告格式。<!--The format is described by a
[JSON schema](https://gitlab.com/gitlab-org/security-products/security-report-schemas).-->

### 安全仪表盘

概述项目、群组或实例的所有漏洞。漏洞仅根据在项目默认分支上发现的结果创建。

### 源语料库

作为模糊测试目标的初始输入给出的一组测试用例，通常会大大加快模糊测试目标的速度，可以是手动创建的测试用例，也可以使用之前运行的模糊测试目标本身自动生成。

<!--
### Vendor

The party maintaining an analyzer. As such, a vendor is responsible for integrating a scanner into
GitLab and keeping it compatible as they evolve. A vendor isn't necessarily the author or maintainer
of the scanner, as in the case of using an open core or OSS project as a base solution of an
offering. For scanners included as part of a GitLab distribution or GitLab subscription, the vendor
is listed as GitLab.
-->

<a id="vulnerability"></a>

### 漏洞

对其环境的安全性产生负面影响的缺陷。漏洞描述错误或弱点，而不描述错误所在的位置（请参阅[发现](#finding)）。每个漏洞都对应一个独特的发现。

默认分支中存在漏洞。发现是扫描程序在 MR/功能分支中识别的所有潜在漏洞项目。只有在合并为默认值后，发现才会成为漏洞。

<a id="vulnerability-finding"></a>

### 漏洞发现

当[报告发现](#report-finding)存储到数据库时，它就变成了漏洞[发现](#finding)。

### 漏洞跟踪

处理跨扫描匹配结果的责任，以便可以理解发现的生命周期。工程师和安全团队使用此信息来决定是否合并代码更改，并查看未解决的发现以及何时引入。通过比较位置指纹、主要标识符和报告类型来跟踪漏洞。

<!--
### 漏洞发生

Deprecated, see [finding](#finding).
-->
