---
stage: Secure
group: Dynamic Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference, howto
---

# API 安全 **(ULTIMATE)**

API 安全是指为保护 Web 应用程序编程接口 (API) 免受未经授权的访问、滥用和攻击而采取的措施。
API 是现代应用程序开发的重要组成部分，因为它们允许应用程序相互交互和交换数据。
但是，这也使它们对攻击者具有吸引力，并且如果没有得到适当的保护，则容易受到安全威胁。
在本节中，我们将讨论可用于确保应用程序中 Web API 安全的极狐GitLab 功能。所讨论的一些功能是特定于 Web API 的，而其他功能则是更通用的解决方案，也可用于 Web API 应用程序。

- [SAST](../sast) 通过分析应用程序的代码库来识别漏洞。
- [依赖扫描](../dependency_scanning)检查项目第三方依赖项是否存在已知漏洞（例如 CVE）。
- [容器扫描](../container_scanning)分析容器镜像，识别已知的操作系统包漏洞和已安装的语言依赖项。
- [API 发现](api_discovery)检查包含 REST API 的应用程序，并直观地了解该 API 的 OpenAPI 规范。其他极狐GitLab 安全工具使用 OpenAPI 规范文档。
- DAST API 执行 Web API 的动态分析安全测试。它可以识别应用程序中的各种安全漏洞，包括 OWASP Top 10。
- [API 模糊测试](../api_fuzzing)在应用程序中寻找以前未知的问题，并且不会映射到传统漏洞类型，例如 SQL 注入。
