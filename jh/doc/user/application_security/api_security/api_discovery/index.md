---
stage: Secure
group: Dynamic Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference, howto
---

# API 发现 **(ULTIMATE)**

> 引入于 15.9 版本。API 发现功能处于 Beta 状态。

API 发现分析您的应用程序，并生成描述它公开的 Web API 的 OpenAPI 文档。然后 [DAST API](../../dast_api/index.md) 或 [API Fuzzing](../../api_fuzzing/index.md) 可以使用此 schema 文档，来执行 Web API 安全扫描。

## 支持的架构

- [Java Spring-Boot](#java-spring-boot)

## API 发现何时运行？

API 发现在您的流水线中作为独立作业运行。生成的 OpenAPI 文档被捕获为作业产物，可以在后续阶段被其他作业使用。

API 发现默认在 `test` 阶段运行。选择 `test` 阶段是因为它通常在其他 API 安全功能（例如 DAST API 和 API 模糊测试）使用的阶段之前执行。

<!--
## Example API Discovery configurations

The following projects demonstrate API Discovery:

- [Example Java Spring Boot v2 Pet Store](https://gitlab.com/gitlab-org/security-products/demos/api-discovery/java-spring-boot-v2-petstore)
-->

<a id="java-spring-boot"></a>

## Java Spring-Boot

[Spring Boot](https://spring.io/projects/spring-boot) 是一个流行的框架，用于创建独立的、生产级的基于 Spring 的应用程序。

### 支持的应用程序

- Spring Boot:v2.X（>= 2.1）
- Java:11, 17（LTS 版本）
- Executable JARs

API 发现支持 Spring Boot 2.1 及更高版本。由于存在影响 API 发现并在 2.1 版本中修复的已知错误，不支持版本 2.0.X。

计划在未来支持 3.0+。未计划支持主要版本 1.0+。

API 发现经过测试并正式支持 Java 运行时的 LTS 版本。其他版本也可以工作，欢迎来自非 LTS 版本的错误报告。

只支持构建为 Spring Boot [executable JARs](https://docs.spring.io/spring-boot/docs/current/reference/html/executable-jar.html#appendix.executable-jar.nested-jars.jar-structure) 的应用。

### 配置为流水线作业

运行 API 发现的最简单方法，是通过基于我们的 CI 模板的流水线作业。
在此方法中运行时，您提供了一个安装了所需依赖项（例如适当的 Java runtime）的容器镜像。有关详细信息，请参阅[镜像要求](#image-requirements)。

1. 将满足[镜像要求](#image-requirements)的容器镜像被上传到容器镜像库。如果容器镜像库需要身份验证，请参阅[此帮助部分](../../../../ci/docker/using_docker_images.md#access-an-image-from-a-private-container-registry)。
1. 在 `build` 阶段的作业中，构建您的应用程序并将生成的 Spring Boot Executable JARs 配置为作业产物。
1. 在您的 `.gitlab-ci.yml` 文件中包含 API 发现模板。

    ```yaml
    include:
       - template: Security/API-Discovery.gitlab-ci.yml
    ```

   每个 `.gitlab-ci.yml` 文件只允许一个 `include` 语句。如果您要包含其他文件，请将它们合并到一个 `include` 语句中。

    ```yaml
    include:
       - template: Security/API-Discovery.gitlab-ci.yml
       - template: Security/DAST-API.gitlab-ci.yml
    ```

1. 创建一个从 `.api_discovery_java_spring_boot` 扩展的新作业。默认阶段是 `test`，可以将其更改为任何值。

   ```yaml
   api_discovery:
       extends: .api_discovery_java_spring_boot
   ```

1. 为作业配置 `image`。

   ```yaml
   api_discovery:
       extends: .api_discovery_java_spring_boot
       image: openjdk:11-jre-slim
   ```

1. 提供应用程序所需的 Java 类路径，包括来自第 2 步的兼容构建产物，以及任何其他依赖项。在以下示例中，构建产物是 `build/libs/spring-boot-app-0.0.0.jar` 并包含所有需要的依赖项。变量 `API_DISCOVERY_JAVA_CLASSPATH` 用于提供类路径。

   ```yaml
   api_discovery:
       extends: .api_discovery_java_spring_boot
       image: openjdk:11-jre-slim
       variables:
           API_DISCOVERY_JAVA_CLASSPATH: build/libs/spring-boot-app-0.0.0.jar
   ```

1. 可选。如果提供的镜像缺少 API 发现所需的依赖项，可以使用 `before_script` 进行添加。在以下示例中，`openjdk:11-jre-slim` 容器不包含 API 发现所需的 `curl`。可以使用 Debian 包管理器 `apt` 安装依赖项：

   ```yaml
   api_discovery:
       extends: .api_discovery_java_spring_boot
       image: openjdk:11-jre-slim
       variables:
           API_DISCOVERY_JAVA_CLASSPATH: build/libs/spring-boot-app-0.0.0.jar
       before_script:
           - apt-get update && apt-get install -y curl
   ```

1. 可选。如果提供的镜像没有自动设置 `JAVA_HOME` 环境变量，或者在路径中包含 `java`，则可以使用 `API_DISCOVERY_JAVA_HOME` 变量。

   ```yaml
   api_discovery:
       extends: .api_discovery_java_spring_boot
       image: openjdk:11-jre-slim
       variables:
           API_DISCOVERY_JAVA_CLASSPATH: build/libs/spring-boot-app-0.0.0.jar
           API_DISCOVERY_JAVA_HOME: /opt/java
   ```

1. 可选。如果 `API_DISCOVERY_PACKAGES` 的软件包库不是公开的，请使用 `API_DISCOVERY_PACKAGE_TOKEN` 变量提供对极狐GitLab API 和软件包库具有读取权限的令牌。如果您使用的是 SaaS 版，并且没有自定义 `API_DISCOVERY_PACKAGES` 变量，则不需要这样做。以下示例使用名为 `GITLAB_READ_TOKEN` 的[自定义 CI/CD 变量](../../../../ci/variables/index.md#define-a-cicd-variable-in-the-ui)来存储令牌。

   ```yaml
   api_discovery:
       extends: .api_discovery_java_spring_boot
       image: openjdk:8-jre-alpine
       variables:
           API_DISCOVERY_JAVA_CLASSPATH: build/libs/spring-boot-app-0.0.0.jar
           API_DISCOVERY_PACKAGE_TOKEN: $GITLAB_READ_TOKEN
   ```

API 发现作业成功运行后，OpenAPI 文档可用作名为 `gl-api-discovery-openapi.json` 的作业产物。

<a id="image-requirements"></a>

#### 镜像要求

- Linux 容器镜像。
- 支持 Java 11 或 17，但其他版本也可能兼容。
- `curl` 命令。
- 位于 `/bin/sh` 的 shell（如 `busybox`、`sh` 或 `bash`）。

### 可用的 CI/CD 变量

| CI/CD 变量                              | 描述        |
|---------------------------------------------|--------------------|
| `API_DISCOVERY_DISABLED`                    | 使用模板作业规则时禁用 API 发现作业。 |
| `API_DISCOVERY_DISABLED_FOR_DEFAULT_BRANCH` | 使用模板作业规则时，禁用默认分支流水线的 API 发现作业。 |
| `API_DISCOVERY_JAVA_CLASSPATH`              | 包含目标 Spring Boot 应用程序的 Java 类路径。(`build/libs/sample-0.0.0.jar`) |
| `API_DISCOVERY_JAVA_HOME`                   | 如果提供，则用于设置 `JAVA_HOME`。 |
| `API_DISCOVERY_PACKAGES`                    | 极狐GitLab 项目软件包 API 前缀（默认为 `$CI_API_V4_URL/projects/42503323/packages`）。 |
| `API_DISCOVERY_PACKAGE_TOKEN`               | 用于调用极狐GitLab 软件包 API 的极狐GitLab 令牌。仅当 `API_DISCOVERY_PACKAGES` 设置为非公开项目时才需要。 |
| `API_DISCOVERY_VERSION`                     | 要使用的 API 发现版本（默认为 `1`）。您可以通过提供完整版本号 `1.1.0` 来固定版本。|

<!--
## Get support or request an improvement

To get support for your particular problem, use the [getting help channels](https://about.gitlab.com/get-help/).

The [GitLab issue tracker on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues) is the right place for bugs and feature proposals about API Discovery.
Use `~"Category:API Security"` [label](../../../../development/labels/index.md) when opening a new issue regarding API Discovery to ensure it is quickly reviewed by the right people. Refer to our [review response SLO](https://about.gitlab.com/handbook/engineering/workflow/code-review/#review-response-slo) to understand when you should receive a response.

[Search the issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues) for similar entries before submitting your own, there's a good chance somebody else had the same issue or feature proposal. Show your support with an award emoji and or join the discussion.

When experiencing a behavior not working as expected, consider providing contextual information:

- GitLab version if using a self-managed instance.
- `.gitlab-ci.yml` job definition.
- Full job console output.
- Framework in use with version (for example Spring Boot v2.3.2).
- Language runtime with version (for example OpenJDK v17.0.1).
-->

<!-- - Scanner log file is available as a job artifact named `gl-api-discovery.log`. -->

<!--
WARNING:
**Sanitize data attached to a support issue**. Remove sensitive information, including: credentials, passwords, tokens, keys, and secrets.
-->
