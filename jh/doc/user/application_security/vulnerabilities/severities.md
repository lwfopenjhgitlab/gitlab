---
type: reference
stage: Secure
group: Threat Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 漏洞严重性级别 **(ULTIMATE)**

GitLab 漏洞分析器会尽可能尝试返回漏洞严重性级别。以下是可用的 GitLab 漏洞严重性级别列表，从最严重到最不严重排列：

- `Critical`
- `High`
- `Medium`
- `Low`
- `Info`
- `Unknown`

大多数 GitLab 漏洞分析器都是流行的开源扫描工具的包装。每个开源扫描工具都提供自己本身的漏洞严重性级别的值。如下表所述：

| 原生漏洞严重性级别类型                                                                                          | 示例                                       |
|-----------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------|
| String                                                                                                                            | `WARNING`、`ERROR`、`Critical`、`Negligible`   |
| Integer                                                                                                                           | `1`、`2`、`5`                                  |
| [CVSS v2.0 Rating](https://nvd.nist.gov/vuln-metrics/cvss)                                                                        | `(AV:N/AC:L/Au:S/C:P/I:P/A:N)`                 |
| [CVSS v3.1 Qualitative Severity Rating](https://www.first.org/cvss/v3.1/specification-document#Qualitative-Severity-Rating-Scale) | `CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H` |

为了提供一致的漏洞严重性级别值，GitLab 漏洞分析器将上述值转换为标准化的 GitLab 漏洞严重性级别，如下表所述：

## SAST

|  GitLab 分析器                                                                                         | 输出严重性级别？ | 原生严重性级别类型 | 原生严重性级别示例     |
|----------------------------------------------------------------------------------------------------------|--------------------------|----------------------------|------------------------------------|
| `security-code-scan`    | **{check-circle}** Yes   | String                     | `CRITICAL`、`HIGH`、`MEDIUM`（分析器版本为 3.2.0 或更高）。在早期版本中，硬编码为 `Unknown`。 |
| `brakeman`                         | **{check-circle}** Yes   | String                     | `HIGH`、`MEDIUM`、`LOW`            |
| `sobelow`                           | **{check-circle}** Yes   | N/A                        | 硬编码所有安全性级别为 `Unknown` |
| `nodejs-scan`                   | **{check-circle}** Yes   | String                     | `INFO`、`WARNING`、`ERROR`         |
| `flawfinder`                     | **{check-circle}** Yes   | Integer                    | `0`、`1`、`2`、`3`、`4`、`5`       |
| `SpotBugs`                        | **{check-circle}** Yes   | Integer                    | `1`、`2`、`3`、`11`、`12`、`18`    |
| `phpcs-security-audit` | **{check-circle}** Yes   | String                     | `ERROR`、`WARNING`                 |
| `pmd-apex`                         | **{check-circle}** Yes   | Integer                    | `1`、`2`、`3`、`4`、`5`            |
| `kubesec`                          | **{check-circle}** Yes   | String                     | `CriticalSeverity`、`InfoSeverity` |
| `secrets`                           | **{check-circle}** Yes   | N/A                        | 硬编码所有安全性级别为 `Critical` |
| `semgrep`                           | **{check-circle}** Yes   | String                     | `error`、`warning`、`note`、`none` |
| `kics`                                 | **{check-circle}** Yes   | String                     | `error`、`warning`、`note`、`none` （在分析器版本 3.7.0 及更高版本中映射到 `info`） |

## Dependency Scanning

| GitLab 分析器                                                                          | 输出严重性级别？     | 原生严重性级别类型 | 原生严重性级别示例       |
|------------------------------------------------------------------------------------------|------------------------------|----------------------------|-------------------------------------|
| `gemnasium`         | **{check-circle}** Yes       | CVSS v2.0 Rating 和 CVSS v3.1 Qualitative Severity Rating <sup>1</sup> | `(AV:N/AC:L/Au:S/C:P/I:P/A:N)`、`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H` |

1. CVSS v3.1 Rating 用于计算严重性级别。如果它不可用，则使用 CVSS v2.0 Rating。

## Container Scanning

| GitLab 分析器                                                                          | 输出严重性级别？     | 原生严重性级别类型 | 原生严重性级别示例       |
|------------------------------------------------------------------------|--------------------------|----------------------------|--------------------------------------------------------------|
| `container-scanning` | **{check-circle}** Yes | String | `Unknown`、`Low`、`Medium`、`High`、`Critical` |

如果可用，供应商严重性级别优先，并由分析器使用。如果不可用，那么将退回到 CVSS v3.1 评级。如果仍不可用，则改为使用 CVSS v2.0 评级。有关此实现的详细信息，请参见 [trivy](https://github.com/aquasecurity/trivy/issues/310) 和 [grype](https://github.com/anchore/grype/issues/287)。

## 模糊测试

所有模糊测试结果都报告为未知，您应该对它们进行手动审核和分类，找到可利用的故障并优先修复。
