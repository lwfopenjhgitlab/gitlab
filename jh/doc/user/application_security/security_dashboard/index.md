---
type: reference, howto
stage: Secure
group: Threat Insights
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab 安全仪表盘和安全中心 **(ULTIMATE)**

您可以使用安全仪表盘，查看有关[安全扫描程序](../index.md#application-coverage)检测到的漏洞的趋势。
这些趋势显示在项目、群组和安全中心。

要使用安全仪表盘，您必须：

- 在一个项目中配置至少一个[安全扫描器](../index.md#security-scanning-tools)。
- 配置作业使用 [`reports` 语法](../../../ci/yaml/index.md#artifactsreports)。
- 使用 GitLab Runner 11.5 或更高版本。如果您使用 SaaS 上的共享 runner，则您使用的是正确的版本。

## 当安全仪表盘更新时

安全仪表盘显示[默认分支](../../project/repository/branches/default.md)上的最新安全扫描结果。
安全扫描仅在默认分支更新时运行，因此安全仪表盘上的信息可能不会反映新发现的漏洞。

要运行每日安全扫描，[配置计划流水线](../../../ci/pipelines/schedules.md)。

## 减少依赖扫描中的误报

WARNING:
当您在扫描期间解决依赖项版本时会出现误报，这与您的项目在之前的流水线中构建和发布时解决的版本不同。

要减少计划流水线中的[依赖扫描](../../../user/application_security/dependency_scanning/index.md)中的误报，请确保：

- 在您的项目中包含一个锁定文件。锁定文件列出所有瞬态依赖项并跟踪它们的版本。
   - Java 项目不能有锁定文件。
   - Python 项目可以有锁定文件，但极狐GitLab 安全工具不支持它们。
- 将您的项目配置为[持续交付](../../../ci/introduction/index.md)。

## 查看项目中随时间变化的漏洞

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/235558) in GitLab 13.6.
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/285476) in GitLab 13.10, options to zoom in on a date range, and download the vulnerabilities chart.
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/285477) in GitLab 13.11, date range slider to visualize data between given dates.
-->

项目安全仪表盘显示了一段时间内的漏洞总数，以及长达 365 天的历史数据。每天 01:15 UTC 通过计划作业开始数据刷新。每次刷新都会捕获开放漏洞的快照。数据不会移植到前几天，因此在当天的作业已经运行之后打开的漏洞，将不会在计数中，直到第二天的刷新作业。
项目安全仪表盘显示当前状态为 `Needs triage` 或 `Confirmed` 的所有漏洞的统计信息。

要查看一段时间内的漏洞总数：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **安全与合规 > 安全仪表盘**。
1. 过滤并搜索您需要的内容。
    - 要按严重性过滤图表，请选择图例名称。
    - 要查看特定时间范围，请使用时间范围句柄 (**{scroll-handle}**)。
    - 要查看图表的特定区域，请选择最左侧的图标 (**{marquee-selection}**) 并在图表上拖动。
    - 要重置为原始范围，请选择 **删除选择** (**{redo}**)。

### 下载漏洞图表

要下载漏洞图表的 SVG 图表：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **安全与合规 > 安全仪表盘**。
1. 选择 **将图表另存为图片** (**{download}**)。

## 查看群组中随时间变化的漏洞

群组安全仪表盘概述了在群组及其子组中的项目的默认分支中发现的漏洞。

要查看群组中随时间变化的漏洞：

1. 在顶部栏上，选择 **主菜单 > 群组** 并选择一个群组。
1. 选择 **安全 > 安全仪表盘**。
1. 将鼠标悬停在图表上，获取有关漏洞的更多详细信息。
    - 您可以显示 30、60 或 90 天时间范围内的漏洞趋势（默认为 90 天）。
    - 要查看超过 90 天时间范围的聚合数据，请使用 VulnerabilitiesCountByDay GraphQL API。极狐GitLab 将数据保留 365 天。

## 查看群组的项目安全状态

使用群组安全仪表盘查看项目的安全状态。安全状态基于检测到的漏洞数量。

查看群组的项目安全状态：

1. 在顶部栏上，选择 **主菜单 > 群组** 并选择一个群组。
1. 选择 **安全 > 安全仪表盘**。

根据最高严重性的开放漏洞，为每个项目分配一个[级别](#project-vulnerability-grades)。
已排除或已解决的漏洞不包括在内。每个项目只能获得一个级别，并且只会在项目安全状态报告中出现一次。

要查看漏洞，请转到该群组的[漏洞报告](../vulnerability_report/index.md)。

<a id="project-vulnerability-grades"></a>

### 项目漏洞级别

| 级别 | 描述 |
| --- | --- |
| **F** | 一个或更多 `critical` 漏洞 |
| **D** | 一个或更多 `high` 或 `unknown` 漏洞 |
| **C** | 一个或更多 `medium` 漏洞 |
| **B** | 一个或更多 `low` 漏洞 |
| **A** | 零漏洞 |

<a id="security-center"></a>

## 安全中心

安全中心是一个个人空间，您可以在其中查看所有项目的漏洞。它显示了项目默认分支中存在的漏洞。

安全中心包括：

- 群组安全仪表盘。
- [漏洞报告](../vulnerability_report/index.md)。
- 用于配置要显示哪些项目的设置区域。

![Security Center Dashboard with projects](img/security_center_dashboard_v13_4.png)

### 查看安全中心

要查看安全中心，请在顶部栏上选择 **主菜单 > 安全**。

### 将项目添加到安全中心

将项目添加到安全中心：

1. 在顶部栏上，选择 **主菜单 > 安全**。
1. 在左侧边栏，选择 **设置**，或选择 **添加项目**。
1. 使用 **搜索您的项目** 文本框搜索和选择项目。
1. 选择 **添加项目**。

添加项目后，安全仪表盘和漏洞报告会显示在这些项目的默认分支中发现的漏洞。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->

## 相关主题

- [解决漏洞](../vulnerabilities/index.md)
- [漏洞报告](../vulnerability_report/index.md)
- [漏洞页面](../vulnerabilities/index.md)
