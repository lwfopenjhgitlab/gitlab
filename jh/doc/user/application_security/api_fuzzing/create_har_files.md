---
stage: Secure
group: Dynamic Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# HTTP 存档格式 **(ULTIMATE)**

HTTP 存档 (HAR) 格式文件是用于交换有关 HTTP 请求和 HTTP 响应的信息的行业标准。HAR 文件的内容是 JSON 格式，包含浏览器与网站的交互。文件扩展名 `.har` 是常用的。

HAR 文件可用于执行 [web API 模糊测试](index.md#http-archive-har) 作为[极狐GitLab CI/CD](../../../ci/index.md) 流水线的一部分。

WARNING:
HAR 文件存储在 Web 客户端和 Web 服务器之间交换的信息。它还可以存储敏感信息，例如身份验证令牌、API 密钥和会话 cookie。我们建议您在将 HAR 文件内容添加到存储库之前查看它们。

## HAR 文件创建

您可以手动创建 HAR 文件，也可以使用专门的工具来记录 Web 会话。我们建议使用专门的工具。但是，重要的是要确保这些工具创建的文件不会暴露敏感信息，并且可以安全使用。

以下工具可用于根据您的网络活动生成 HAR 文件。它们会自动记录您的网络活动并生成 HAR 文件：

1. [极狐GitLab HAR 记录器](#gitlab-har-recorder)。
1. [Insomnia API 客户端](#insomnia-api-client)。
1. [Fiddler 调试代理](#fiddler-debugging-proxy)。
1. [Safari web 浏览器](#safari-web-browser)。
1. [Chrome web 浏览器](#chrome-web-browser)。
1. [Firefox web 浏览器](#firefox-web-browser)。

WARNING:
HAR 文件可能包含敏感信息，例如身份验证令牌、API 密钥和会话 cookie。我们建议您在将 HAR 文件内容添加到仓库之前查看它们。

<a id="gitlab-har-recorder"></a>

### 极狐GitLab HAR 记录器

极狐GitLab HAR 记录器是一个命令行工具，用于记录 HTTP 消息并将它们保存到 HTTP 存档 (HAR) 文件。<!--For more details
about the GitLab HAR Recorder, see the [homepage](https://gitlab.com/gitlab-org/security-products/har-recorder).-->

#### 安装极狐GitLab HAR 记录器

先决条件：

- 安装 Python 3.6 或更大版本。
- 对于 Microsoft Windows，您还必须安装 `Microsoft Visual C++ 14.0`。它包含在来自 [Visual Studio 下载页面](https://visualstudio.microsoft.com/downloads/) 的 *Build Tools for Visual Studio* 中。
- 安装 HAR 记录器。

安装极狐GitLab HAR 记录器：

  ```shell
  pip install gitlab-har-recorder --extra-index-url https://gitlab.com/api/v4/projects/22441624/packages/pypi/simple
  ```

#### 使用极狐GitLab HAR 记录器创建 HAR 文件

1. 使用代理端口和 HAR 文件名启动记录器。
1. 使用代理完成浏览器操作。
    1. 确保使用代理！
1. 停止记录器。

要验证 HAR 是否包含所有请求，请使用在线 HAR 查看器，例如：

- [HAR Viewer](http://www.softwareishard.com/har/viewer/)
<!-- vale gitlab.Admin = NO -->
- [Google Admin Toolbox HAR Analyzer](https://toolbox.googleapps.com/apps/har_analyzer/)
<!-- vale gitlab.Admin = YES -->

<a id="insomnia-api-client"></a>

### Insomnia API 客户端

[Insomnia API Client](https://insomnia.rest/) 是一个 API 设计工具，在众多用途中，帮助您设计、描述和测试您的 API。您还可以使用它来生成可用于 [Web API 模糊测试](index.md#http-archive-har)的 HAR 文件。

#### 使用 Insomnia API 客户端创建 HAR 文件

1. 定义或导入您的 API。
    - Postman v2。
    - Curl。
    - OpenAPI v2、v3。
1. 验证每个 API 调用是否有效。
    - 如果您导入了 OpenAPI 规范，请检查并添加工作数据。
1. 选择 **API > 导入/导出**。
1. 选择 **导出数据 > 当前工作区**。
1. 选择要包含在 HAR 文件中的请求。
1. 选择 **导出**。
1. 在 **选择导出类型** 下拉列表中选择 **HAR -- HTTP 存档格式**。
1. 选择 **完成**。
1. 输入 HAR 文件的位置和文件名。

<a id="fiddler-debugging-proxy"></a>

### Fiddler 调试代理

[Fiddler](https://www.telerik.com/fiddler) 是一个网页调试工具。它捕获 HTTP 和 HTTP(S) 网络流量并允许您检查每个请求。它还允许您以 HAR 格式导出请求和响应。

#### 使用 Fiddler 创建 HAR 文件

1. 进入 [Fiddler 主页](https://www.telerik.com/fiddler)并登录。如果您还没有账号，请先创建一个账号。
1. 浏览调用 API 的页面。Fiddler 会自动捕获请求。
1. 选择一个或多个请求，然后从上下文菜单中选择 **Export > Selected Sessions**。
1. 在 **Choose Format** 下拉列表中选择 **HTTPArchive v1.2**。
1. 输入文件名并选择 **保存**。

Fiddler 显示一条弹出消息，确认导出已成功。

<a id="safari-web-browser"></a>

### Safari web 浏览器

[Safari](https://www.apple.com/safari/) 是 Apple 维护的网络浏览器。随着 Web 开发的发展，浏览器支持新功能。使用 Safari，您可以探索网络流量并将其导出为 HAR 文件。

#### 使用 Safari 创建 HAR 文件

先决条件：

- 启用“开发”菜单项。
   1. 打开 Safari 的偏好设置。按 <kbd>Command</kbd>+<kbd>,</kbd> 或从菜单中选择 **Safari > Preferences...**。
   1. 选择 **Advanced** 选项卡，然后选择 `Show Develop menu item in menu bar`。
   1. 关闭 **偏好设置** 窗口。

1. 打开 **Web Inspector**。按 <kbd>Option</kbd>+<kbd>Command</kbd>+<kbd>i</kbd>，或从菜单中选择 **Develop > Show Web Inspector**。
1. 选择 **Network** 选项卡，然后选择 **Preserve Log**。
1. 浏览调用 API 的页面。
1. 打开 **Web Inspector** 并选择 **Network** 选项卡
1. 右键单击要导出的请求并选择 **Export HAR**。
1. 输入文件名并选择 **Save**。

<a id="chrome-web-browser"></a>

### Chrome web 浏览器

[Chrome](https://www.google.com/chrome/) 是由 Google 维护的网络浏览器。随着 Web 开发的发展，浏览器支持新功能。使用 Chrome，您可以探索网络流量并将其导出为 HAR 文件。

#### 使用 Chrome 创建 HAR 文件

1. 从 Chrome 上下文菜单中，选择 **Inspect**。
1. 选择 **Network** 选项卡。
1. 选择 **Preserve log**。
1. 浏览调用 API 的页面。
1. 选择一个或多个请求。
1. 右键单击并选择 **Save all as HAR with content**。
1. 输入文件名并选择 **Save**。
1. 要附加其他请求，请选择并将它们保存到同一文件中。

<a id="firefox-web-browser"></a>

### Firefox Web 浏览器

[Firefox](https://www.mozilla.org/en-US/firefox/new/) 是由 Mozilla 维护的网络浏览器。随着 Web 开发的发展，浏览器支持新功能。使用 Firefox，您可以探索网络流量并将其导出为 HAR 文件。

#### 使用 Firefox 创建 HAR 文件

1. 从 Firefox 上下文菜单中，选择 **Inspect**。
1. 选择 **Network** 选项卡。
1. 浏览调用 API 的页面。
1. 检查 **Network** 选项卡并确认正在记录请求。如果有消息 `Perform a request or Reload the page to see detailed information about network activity`，请选择 **Reload** 开始记录请求。
1. 选择一个或多个请求。
1. 右键单击并选择 **Save All As HAR**。
1. 输入文件名并选择 **Save**。
1. 要附加其他请求，请选择并将它们保存到同一文件中。

## HAR 验证

在使用 HAR 文件之前，确保它们不会暴露任何敏感信息很重要。

对于每个 HAR 文件，您应该：

- 查看 HAR 文件的内容
- 查看 HAR 文件中的敏感信息
- 编辑或删除敏感信息

### 查看 HAR 文件内容

我们建议在可以以结构化方式呈现其内容的工具中查看 HAR 文件的内容。在线提供了几个 HAR 文件查看器。如果您不想上传 HAR 文件，可以使用计算机上安装的工具。HAR 文件使用 JSON 格式，因此也可以在文本编辑器中查看。

推荐用于查看 HAR 文件的工具包括：

- [HAR Viewer](http://www.softwareishard.com/har/viewer/) -（在线）
<!-- vale gitlab.Admin = NO -->
- [Google Admin Toolbox HAR Analyzer](https://toolbox.googleapps.com/apps/har_analyzer/) -（在线）
<!-- vale gitlab.Admin = YES -->
- [Fiddler](https://www.telerik.com/fiddler) - 本地
- [Insomnia API Client](https://insomnia.rest/) - 本地

<a id="review-har-file-content"></a>

## 查看 HAR 文件内容

查看 HAR 文件中的任何以下内容：

- 有助于授予应用程序访问权限的信息，例如：身份验证令牌、cookie、API 密钥。
- 个人身份信息（PII）。

我们强烈建议您[编辑或删除](#edit-or-remove-sensitive-information)任何敏感信息。

使用以下内容作为清单开始。请注意，这不是一个详尽的列表。

- 寻找 secret。例如：您的应用程序需要身份验证，请检查常见位置或身份验证信息：
   - 身份验证相关的标头。例如：cookies、授权。 这些标头可能包含有效信息。
   - 与身份验证相关的请求。这些请求的正文可能包含用户凭据或令牌等信息。
   - 会话令牌。会话令牌可以授予对您的应用程序的访问权限。这些令牌的位置可能会有所不同。它们可以在标题、查询参数或正文中。
- 寻找个人身份信息
   - 例如，如果您的应用程序检索用户列表及其个人数据：电话、姓名、电子邮件。
   - 身份验证信息也可能包含个人信息。

## 编辑或删除敏感信息

编辑或删除在[查看 HAR 文件](#review-har-file-content)期间发现的敏感信息。
HAR 文件是 JSON 文件，可以在任何文本编辑器中进行编辑。

编辑 HAR 文件后，在 HAR 文件查看器中打开它以验证其格式和结构是否完好。

以下示例演示了使用 [Visual Studio Code](https://code.visualstudio.com/) 文本编辑器来编辑在 header 中找到的授权令牌。

![Authorization token edited in Visual Studio Code](img/vscode_har_edit_auth_header.png)
