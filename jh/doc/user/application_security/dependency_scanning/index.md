---
type: reference, howto
stage: Secure
group: Composition Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 依赖扫描 **(ULTIMATE)**

依赖扫描功能可以在您开发和测试应用程序时，自动查找软件依赖项中的安全漏洞。例如，依赖项扫描可以让您知道您的应用程序是否使用已知易受攻击的外部（开源）库。然后，您可以采取措施保护您的应用程序。

依赖扫描通常被认为是软件组合分析 (SCA) 的一部分。SCA 可以包含检查代码使用的项目的各个方面，这些项目通常包括几乎总是从外部源导入的应用程序和系统依赖项，而不是来自您自己编写的项目。

如果您使用[极狐GitLab CI/CD](../../../ci/index.md)，您可以使用依赖扫描来分析已知漏洞的依赖关系。极狐GitLab 扫描所有依赖项，包括传递依赖项（也称为嵌套依赖项）。您可以通过以下任一方式使用依赖项扫描：

- 在您现有的 `.gitlab-ci.yml` 文件中，[包含依赖扫描模板](#configuration) 。
- 隐式使用 [Auto DevOps](../../../topics/autodevops/index.md) 提供的自动依赖扫描。

极狐GitLab 检查依赖扫描报告，比较源分支和目标分支之间发现的漏洞，并显示合并请求的信息。结果按漏洞的[严重性](../vulnerabilities/severities.md)排序。

![Dependency scanning Widget](img/dependency_scanning_v13_2.png)

<a id="dependency-scanning-compared-to-container-scanning"></a>

## 依赖扫描与容器扫描的比较

极狐GitLab 提供依赖扫描和容器扫描，确保覆盖所有依赖类型。为了尽可能多地覆盖您的风险区域，我们鼓励您使用我们所有的安全扫描器：

- 依赖项扫描分析您的项目，并告诉您哪些软件依赖项（包括上游依赖项）已包含在您的项目中，以及依赖项包含哪些已知风险。依赖扫描根据项目的语言和包管理器修改其行为，它通常会查找锁定文件，然后执行构建来获取上游依赖信息。在容器的情况下，依赖扫描使用兼容的 manifest 并仅报告这些声明的软件依赖项（以及作为子依赖项安装的）。依赖项扫描无法检测预先捆绑到容器基础镜像中的软件依赖项，要识别预先捆绑的依赖项，请使用 `CS_DISABLE_LANGUAGE_VULNERABILITY_SCAN` 变量启用容器扫描中的语言扫描。
- 容器扫描分析您的容器，并告诉您操作系统 (OS) 包中的已知风险。如果您启用并使用 `CS_DISABLE_LANGUAGE_VULNERABILITY_SCAN` 变量，可以将其配置同时报告软件和语言依赖。打开此变量可能会导致一些重复的结果，因为尚未对容器扫描和依赖项扫描之间的结果进行重复数据删除。

下表总结了每个扫描工具可以检测到的依赖类型：

| 功能                                                                                      | 依赖扫描 | 容器扫描          |
| -----------------------------------------------------------                                  | ------------------- | ------------------          |
| 识别引入依赖的 manifest、锁定文件或静态文件              | **{check-circle}**      | **{dotted-circle}**             |
| 开发依赖                                                                     | **{check-circle}**      | **{dotted-circle}**             |
| 提交到仓库的锁定文件中的依赖项                                     | **{check-circle}**      | **{check-circle}** <sup>1</sup> |
| Go 构建的二进制文件                                                                         | **{dotted-circle}**     | **{check-circle}** <sup>2</sup> <sup>3</sup> |
| 操作系统安装的动态关联的特定于语言的依赖项 <sup>3</sup>         | **{dotted-circle}**     | **{check-circle}**              |
| 操作系统依赖                                                                | **{dotted-circle}**     | **{check-circle}**              |
| 安装在操作系统上的特定于语言的依赖项（不是由您的项目构建的） | **{dotted-circle}**     | **{check-circle}**              |

1. 锁定文件必须存在于要检测的镜像中。
1. 二进制文件必须存在于要检测的镜像中。
1. 仅当使用 Trivy 时。


## 要求

依赖扫描在 `test` 阶段运行，默认情况下可用。如果在 `.gitlab-ci.yml` 文件中重新定义阶段，则需要 `test` 阶段。

要运行依赖扫描作业，默认情况下，您需要带有 `docker` 或 `kubernetes` executor 的 GitLab Runner。
如果您在 SaaS 上使用共享 runner，则默认启用此功能。提供的分析器镜像适用于 Linux/amd64 架构。

WARNING:
如果您使用自己的 runner，请确保您安装的 Docker 版本**不是** `19.03.0`。有关详细信息，请参阅[故障排查信息](#error-response-from-daemon-error-processing-tar-file-docker-tar-relocation-error)。

WARNING:
依赖扫描不支持编译器和解释器的运行时安装。
<!--If you have need of this, please explain why by filling out the survey [here](https://docs.google.com/forms/d/e/1FAIpQLScKo7xEYA65rOjPTGIufAyfjPGnCALSJZoTxBlvskfFMEOZMw/viewform).-->

<a id="supported-languages-and-package-managers"></a>

## 支持的语言和包管理器

依赖扫描自动检测仓库中使用的语言，运行与检测到的语言匹配的所有分析器，通常不需要自定义分析器的选择。我们建议不要指定分析器，以便您自动使用完整选择从而获得最佳覆盖范围，避免在出现弃用或删除时进行调整。
但是，您可以使用变量 `DS_EXCLUDED_ANALYZERS` 覆盖选择。

语言检测依赖于 CI 作业 [`rules`](../../../ci/yaml/index.md#rules)，并从仓库的根目录中搜索最多两个目录级别。例如，如果仓库包含`Gemfile`、`api/Gemfile` 或 `api/client/Gemfile`，则启用 `gemnasium-dependency_scanning` 作业，但如果唯一支持的依赖文件是 `api/v1/client/Gemfile`，则不会启用。

对于 Java 和 Python，当检测到支持的依赖文件时，依赖扫描会尝试构建项目并执行一些 Java 或 Python 命令以获取依赖项列表。对于所有其他项目，无需先构建项目，即可解析锁定文件以获取依赖项列表。

当检测到支持的依赖文件时，将分析所有依赖项，包括传递依赖项。分析的嵌套或传递依赖项的深度没有限制。

支持以下语言和依赖项管理器：

<style>
table.supported-languages tr:nth-child(even) {
    background-color: transparent;
}

table.supported-languages td {
    border-left: 1px solid #dbdbdb;
    border-right: 1px solid #dbdbdb;
    border-bottom: 1px solid #dbdbdb;
}

table.supported-languages tr td:first-child {
    border-left: 0;
}

table.supported-languages tr td:last-child {
    border-right: 0;
}

table.supported-languages ul {
    font-size: 1em;
    list-style-type: none;
    padding-left: 0px;
    margin-bottom: 0px;
}
</style>

<!-- markdownlint-disable MD044 -->
<table class="supported-languages">
  <thead>
    <tr>
      <th>语言</th>
      <th>语言版本</th>
      <th>包管理器</th>
      <th>支持的文件</th>
      <th><a href="#how-multiple-files-are-processed">处理多个文件？</a></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Ruby</td>
      <td>所有版本</td>
      <td><a href="https://bundler.io/">Bundler</a></td>
      <td>
        <ul>
            <li><code>Gemfile.lock</code></li>
            <li><code>gems.locked</code></li>
        </ul>
      </td>
      <td>Y</td>
    </tr>
    <tr>
      <td>PHP</td>
      <td>所有版本</td>
      <td><a href="https://getcomposer.org/">Composer</a></td>
      <td><code>composer.lock</code></td>
      <td>Y</td>
    </tr>
    <tr>
      <td>C</td>
      <td rowspan="2">所有版本</td>
      <td rowspan="2"><a href="https://conan.io/">Conan</a></td>
      <td rowspan="2"><a href="https://docs.conan.io/en/latest/versioning/lockfiles.html"><code>conan.lock</code></a></td>
      <td rowspan="2">Y</td>
    </tr>
    <tr>
      <td>C++</td>
    </tr>
    <tr>
      <td>Go</td>
      <td>所有版本</td>
      <td><a href="https://go.dev/">Go</a></td>
      <td>
        <ul>
          <li><code>go.mod</code></li>
          <li><code>go.sum</code></li>
        </ul>
      </td>
      <td>Y</td>
    </tr>
    <tr>
      <td rowspan="2">Java 和 Kotlin（非 Android）</td>
      <td rowspan="2">
        8 LTS,
        11 LTS,
        或 17 LTS
      </td>
      <td><a href="https://gradle.org/">Gradle</a><sup><b><a href="#notes-regarding-supported-languages-and-package-managers-2">2</a></b></sup></td>
      <td>
        <ul>
            <li><code>build.gradle</code></li>
            <li><code>build.gradle.kts</code></li>
        </ul>
      </td>
      <td>Gemnasium</td>
      <td>N</td>
    </tr>
    <tr>
      <td><a href="https://maven.apache.org/">Maven</a></td>
      <td><code>pom.xml</code></td>
      <td>N</td>
    </tr>
    <tr>
      <td rowspan="3">JavaScript 和 TypeScript</td>
      <td rowspan="3">所有版本</td>
      <td><a href="https://www.npmjs.com/">npm</a></td>
      <td>
        <ul>
            <li><code>package-lock.json</code><sup><b><a href="#notes-regarding-supported-languages-and-package-managers-3">3</a></b></sup></li>
            <li><code>npm-shrinkwrap.json</code></li>
        </ul>
      </td>
      <td>Y</td>
    </tr>
    <tr>
      <td>所有版本</td>
      <td><a href="https://classic.yarnpkg.com/en/">yarn</a></td>
      <td><code>yarn.lock</code></td>
      <td>Y</td>
    </tr>
    <tr>
      <td><a href="https://pnpm.io/">pnpm</a><sup><b><a href="#notes-regarding-supported-languages-and-package-managers-3">3</a></b></sup></td>
      <td><code>pnpm-lock.yaml</code></td>
      <td>Y</td>
    </tr>
    <tr>
      <td>.NET</td>
      <td rowspan="2">所有版本</td>
      <td rowspan="2"><a href="https://www.nuget.org/">NuGet</a></td>
      <td rowspan="2"><a href="https://learn.microsoft.com/en-us/nuget/consume-packages/package-references-in-project-files#enabling-lock-file"><code>packages.lock.json</code></a></td>
      <td rowspan="2">Y</td>
    </tr>
    <tr>
      <td>C#</td>
    </tr>
    <tr>
      <td rowspan="4">Python</td>
      <td rowspan="4">3.9, 3.10<sup><b><a href="#notes-regarding-supported-languages-and-package-managers-4">4</a></b></sup></td>
      <td><a href="https://setuptools.readthedocs.io/en/latest/">setuptools</a></td>
      <td><code>setup.py</code></td>
      <td>N</td>
    </tr>
    <tr>
      <td><a href="https://pip.pypa.io/en/stable/">pip</a></td>
      <td>
        <ul>
            <li><code>requirements.txt</code></li>
            <li><code>requirements.pip</code></li>
            <li><code>requires.txt</code></li>
        </ul>
      </td>
      <td>N</td>
    </tr>
    <tr>
      <td><a href="https://pipenv.pypa.io/en/latest/">Pipenv</a></td>
      <td>
        <ul>
            <li><a href="https://pipenv.pypa.io/en/latest/pipfile/#example-pipfile"><code>Pipfile</code></a></li>
            <li><a href="https://pipenv.pypa.io/en/latest/pipfile/#example-pipfile-lock"><code>Pipfile.lock</code></a></li>
        </ul>
      </td>
      <td>N</td>
    </tr>
    <tr>
      <td><a href="https://python-poetry.org/">Poetry</a><a href="#notes-regarding-supported-languages-and-package-managers-5">5</a></td>
      <td><code>poetry.lock</code></td>
      <td>Gemnasium</td>
      <td>N</td>
    </tr>
    <tr>
      <td>Scala</td>
      <td>所有版本</td>
       <td><a href="https://www.scala-sbt.org/">sbt</a><sup><b><a href="#notes-regarding-supported-languages-and-package-managers-6">6</a></b></sup></td>
      <td><code>build.sbt</code></td>
      <td>N</td>
    </tr>
  </tbody>
</table>

<ol>
  <li>
    <a id="notes-regarding-supported-languages-and-package-managers-1"></a>
    <p>
      对适用于 Android 的 Kotlin 项目的支持正在开发中。
    </p>
  </li>
  <li>
    <a id="notes-regarding-supported-languages-and-package-managers-2"></a>
    <p>
      当启用 FIPS 模式时，不支持 Gradle。
    </p>
  </li>
  <li>
    <a id="notes-regarding-supported-languages-and-package-managers-3"></a>
    <p>
      对 <code>pnpm</code> 锁定文件的支持引入于 15.11 版本</a>。<code>pnpm</code> 锁定文件不存储绑定的依赖，因此报告的依赖项可能与 <code>npm</code> 或 <code>yarn</code> 不同。
    </p>
  </li>
  <li>
    <a id="notes-regarding-supported-languages-and-package-managers-4"></a>
    <p>
      对于 <code>Python 3.10</code> 的支持，将以下节添加到极狐GitLab CI/CD 配置文件中。此配置指定了使用 <code>Python 3.10</code> 镜像，代替默认的 <code>Python 3.9</code>。
      <div class="language-yaml highlighter-rouge">
        <div class="highlight">
<pre class="highlight"><code><span class="na">gemnasium-dependency_scanning</span><span class="pi">:</span>
  <span class="na">image</span><span class="pi">:</span>
    <span class="na">name</span><span class="pi">:</span> <span class="s">$CI_TEMPLATE_REGISTRY_HOST/security-products/gemnasium-python:4-python-3.10</span></code></pre></div></div>
    </p>
  </li>
  <li>
    <a id="notes-regarding-supported-languages-and-package-managers-5"></a>
    <p>
      对带有 <code>poetry.lock</code> 文件的 Poetry 项目的支持添加于 15.0 版本。
    </p>
  </li>
  <li>
    <a id="notes-regarding-supported-languages-and-package-managers-6"></a>
    <p>
      对 <a href="https://www.scala-sbt.org/">sbt</a> 1.3 及更高版本的支持添加于 13.9 版本。
    </p>
  </li>
</ol>
<!-- markdownlint-enable MD044 -->

### 分析器如何获取依赖信息

极狐GitLab 分析器使用以下两种方法之一获取依赖关系信息：

1. [直接解析 lockfiles。](#obtaining-dependency-information-by-parsing-lockfiles)
1. [运行包管理器或构建工具来生成依赖信息文件，然后对其进行解析。](#obtaining-dependency-information-by-running-a-package-manager-to-generate-a-parsable-file)

<a id="obtaining-dependency-information-by-parsing-lockfiles"></a>

#### 通过解析 lockfile 获取依赖信息

以下包管理器使用 GitLab 分析器能够直接解析的 lockfile：

| 包管理器 | 支持的文件格式版本 | 测试版本                                                                                                                                                                                                                    |
| ------          | ------                         | ------                                                                                                                                                                                                                             |
| Bundler         | N/A                            | 1.17.3、2.1.4 |
| Composer        | N/A                            | 1.x                                                                                                                              |
| Conan           | 0.4                            | 1.x                                                                                                                                      |
| Go              | N/A                            | 1.x<sup><strong><a href="#notes-regarding-parsing-lockfiles-1">1</a></strong></sup>                                                                                                                                       |
| NuGet           | v1                             | 4.9                                                                                               |
| npm             | v1、v2、v3                         | 6.x、7.x、9.x         |
| pnpm            | v5.3、v5.4、v6                     | 7.x、8.x |
| yarn            | v1、v2<sup><b><a href="#notes-regarding-parsing-lockfiles-3">3</a></b></sup>、v3<sup><b><a href="#notes-regarding-parsing-lockfiles-3">3</a></b></sup>                       | 1.x、2.x、3.x                                                                                                                                                           |
| Poetry          | v1                             | 1.x                                                                                                          |

<ol>
  <li>
    <a id="notes-regarding-parsing-lockfiles-1"></a>
    <p>
      如果无法生成 Go 项目使用的构建列表，依赖扫描只会解析 <code>go.sum</code>。
    </p>
  </li>
  <li>
    <a id="notes-regarding-parsing-lockfiles-2"></a>
    <p>
      对 <code>lockfileVersion = 3</code> 的支持引入于 15.7 版本。
    </p>
  </li>
  <li>
    <a id="notes-regarding-parsing-lockfiles-3"></a>
    <p>
      对 Yarn <code>v2</code> 和 <code>v3</code> 的支持引入于 15.11 版本。但是，此功能也适用于 15.0 及更高版本。
    </p>
    <p>
      以下 Yarn <code>v2</code> 或 <code>v3</code> 的功能不受支持：
    </p>
    <ul>
      <li>
        <a href="https://yarnpkg.com/features/workspaces">工作区</a>
      </li>
      <li>
        <a href="https://yarnpkg.com/cli/patch">yarn 补丁</a>
      </li>
    </ul>
    <p>
      包含补丁、工作区或两者都有的 Yarn 文件仍会被处理，但这些功能将被忽略。
    </p>
  </li>
</ol>


<a id="obtaining-dependency-information-by-running-a-package-manager-to-generate-a-parsable-file"></a>

#### 通过运行包管理器生成可解析文件获取依赖信息

为了支持以下包管理器，GitLab 分析器分两个步骤进行：

1. 执行包管理器或特定任务，导出依赖信息。
1. 解析导出的依赖信息。

| 包管理器 | 预装版本                                                                                                                                                                    | 测试版本                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ------          | ------                                                                                                                                                                                   | ------                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| sbt             | 1.6.1                                                               | 1.0.4, 1.1.4, 1.1.6, 1.2.8, 1.3.12, 1.4.6, 1.6.1 |
| Maven           | 3.6.3                                                               | 3.6.3<sup><b><a href="#exported-dependency-information-notes-1">1</a></b></sup>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| Gradle          | 6.7.1<sup><b><a href="#exported-dependency-information-notes-2">2</a></b></sup>, 7.3.3<sup><b><a href="#exported-dependency-information-notes-2">2</a></b></sup>                                                               | 5.6.4、6.7、6.9, 7.3                               |
| setuptools      | 58.1.0                                                                              | >=65.6.3                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| pip             | 22.0.4                                                                              | 20.x                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| Pipenv          | 2022.1.8                                                             | 2022.1.8<sup><b><a href="#exported-dependency-information-notes-3">3</a></b></sup>                                                                                                                                                                                                                                                                                                                                                                                                                              |
| Go              | 1.18 | 1.18<sup><strong><a href="#exported-dependency-information-notes-4">4</a></strong></sup> |


<!-- markdownlint-disable MD044 -->
<ol>
  <li>
    <a id="exported-dependency-information-notes-1"></a>
    <p>
      此测试使用 <code>maven</code> 指定的默认版本 `.tool-versions` 文件。
    </p>
  </li>
  <li>
    <a id="exported-dependency-information-notes-2"></a>
    <p>
      不同版本的 Java 需要不同版本的 Gradle。上表中列出的 Gradle 版本已预安装在分析器镜像中。分析器使用的 Gradle 版本取决于您的项目是否使用 <code>gradlew</code>（Gradle 包装器）文件：
    </p>
    <ul>
      <li>
        <p>
          如果您的项目 <i>不使用</i> <code>gradlew</code> 文件, 然后分析器会自动切换到预安装的 Gradle 版本之一，基于由 <a href="#configuring-specific-analyzers-used-by-dependency-scanning"><code>DS_JAVA_VERSION</code></a> 变量指定的 Java 版本。
        </p>
        <p>对于 Java 版本 <code>8</code> 和 <code>11</code>，自动选择 Gradle <code>6.7.1</code>，对于 Java 版本  <code>17</code>，自动选择 Gradle <code>7.3.3</code>。
        </p>
      </li>
      <li>
        <p>
          如果您的项目 <i>使用</i> <code>gradlew</code> 文件，那么分析器镜像中预装的 Gradle 版本将被忽略，而使用 <code>gradlew</code> 文件中指定的版本。
        </p>
      </li>
    </ul>
  </li>
  <li>
    <a id="exported-dependency-information-notes-3"></a>
    <p>
      此测试确认如果找到 <code>Pipfile.lock</code> 文件，Gemnasium 将使用该文件扫描此文件中列出的确切软件包版本。
    </p>
  </li>
  <li>
    <a id="exported-dependency-information-notes-4"></a>
    <p>
      由于执行 <code>go build</code>，Go 构建过程需要网络访问，通过 <code>go mod download</code> 获得一个预加载的 modcache via，或供应的依赖项。获取更多信息，参考 Go 文档：<a href="https://pkg.go.dev/cmd/go#hdr-Compile_packages_and_dependencies">编译包和依赖</a>。
    </p>
  </li>
</ol>
<!-- markdownlint-enable MD044 -->

### 如何触发分析器

极狐GitLab 依赖 [`rules:exists`](../../../ci/yaml/index.md#rulesexists) 来启动相关分析器，针对仓库中存在的 `Supported files` 检测到的语言，如[上表](#supported-languages-and-package-managers)所示。

当前的检测逻辑将最大搜索深度限制为两个级别。例如，如果仓库包含 `Gemfile.lock`、`api/Gemfile.lock` 或 `api/client/Gemfile.lock`，则启用 `gemnasium-dependency_scanning` 作业，但如果唯一支持的依赖文件是 `api/v1/client/Gemfile.lock`，则不然。

当检测到支持的依赖文件时，将分析所有依赖项，包括传递依赖项。分析的嵌套或传递依赖项的深度没有限制。

<!--
### How multiple files are processed

NOTE:
If you've run into problems while scanning multiple files, please contribute a comment to
[this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/337056).
-->

#### Python

我们只在检测到需求文件或锁定文件的目录中执行一次安装。依赖关系仅由 `gemnasium-python` 分析检测到的第一个文件。按以下顺序搜索文件：

1. `requirements.txt`、`requirements.pip` 或 `requires.txt` 用于使用 Pip 的项目。
1. `Pipfile` 或 `Pipfile.lock` 用于使用 Pipenv 的项目。
1. `poetry.lock` 用于使用 Poetry 的项目。
1. 使用 Setuptools 的项目的`setup.py`。

搜索从根目录开始，如果在根目录中未找到任何构建，则继续搜索子目录。因此，将在子目录中的 Pipenv 文件之前，检测到根目录中的 Poetry 锁定文件。

#### Java and Scala

我们只在检测到构建文件的目录中执行一个构建。对于包含多个 Gradle、Maven 或 sbt 构建或这些构建的任何组合的大型项目，`gemnasium-maven` 仅分析检测到的第一个构建文件的依赖关系。构建文件按以下顺序搜索：

1. `build.gradle` 或 `build.gradle.kts` 用于单个或[多项目](https://docs.gradle.org/current/userguide/intro_multi_project_builds.html) Gradle 构建。
1. `pom.xml` 用于单个或[多模块](https://maven.apache.org/pom.html#Aggregation) Maven 项目。
1. `build.sbt` 用于单个或[多项目](https://www.scala-sbt.org/1.x/docs/Multi-Project.html) sbt 构建。

搜索从根目录开始，如果在根目录中未找到任何构建，则继续搜索子目录。因此，将在子目录中的 Gradle 构建文件之前，检测到根目录中的 sbt 构建文件。

#### JavaScript

执行以下分析器，每个分析器在处理多个文件时具有不同的行为：

- Gemnasium

   支持多个 lockfile。

- [Retire.js](https://retirejs.github.io/retire.js/)

   不支持多个 lockfile。当存在多个 lockfile 时，`Retire.js` 会分析在按字母顺序遍历目录树时发现的第一个 lockfile。

从 14.8 版本开始，`gemnasium` 分析器会扫描支持的 JavaScript 项目，查找供应商库（即检入项目但不受包管理器管理的库）。

#### Go

支持多个文件。当检测到 `go.mod` 文件时，分析器会尝试使用[最小版本选择](https://go.dev/ref/mod#glos-minimal-version-selection)。如果遇到非致命错误，分析器会回退到解析可用的 `go.sum` 文件。对每个检测到的 `go.mod` 和 `go.sum` 文件重复该过程。

#### PHP、C、C++、.NET、C&#35;、Ruby、JavaScript

这些语言的分析器支持多个 lockfile。

#### 支持其它语言

在以下问题中跟踪对其他语言、依赖管理器和依赖文件的支持：

| 包管理器    | 语言 | 支持的文件 | 扫描工具 | 
| ------------------- | --------- | --------------- | ---------- | ----- |
| [Poetry](https://python-poetry.org/) | Python | `poetry.lock` | [Gemnasium](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium) | 

<!--
## Contribute your scanner

The [Security Scanner Integration](../../../development/integrations/secure.md) documentation explains how to integrate other security scanners into GitLab.
-->

<a id="configuration"></a>

## 配置

要启用依赖扫描，您必须[包含](../../../ci/yaml/index.md#includetemplate) [`Dependency-Scanning.gitlab-ci.yml` 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Dependency-Scanning.gitlab-ci.yml)，其作为极狐GitLab 安装的一部分提供。

<!--
For GitLab versions earlier than 11.9, you can copy and use the job as defined
that template.
-->

将以下内容添加到您的 `.gitlab-ci.yml` 文件中：

```yaml
include:
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
```

包含的模板在 CI/CD 流水线中创建依赖项扫描作业，并扫描项目的源代码以查找可能的漏洞。
结果保存为依赖扫描报告产物<!--[依赖扫描报告产物](../../../ci/yaml/artifacts_reports.md#artifactsreportsdependency_scanning)-->，您可以稍后下载和分析。由于实施限制，我们始终采用可用的最新依赖扫描产物。

<a id="enable-dependency-scanning-via-an-automatic-merge-request"></a>

### 通过自动合并请求启用依赖扫描

> - 引入于 14.1 版本，功能标志名为 `sec_dependency_scanning_ui_enable`。默认启用。
> - 于 14.1 版本适用于私有化部署版
> - 功能标志移除于 14.2 版本。

要在项目中启用依赖扫描，您可以创建合并请求：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **安全与合规 > 配置**。
1. 在 **依赖项扫描** 行中，选择 **使用合并请求进行配置**。
1. 查看并合并合并请求，来启用依赖扫描。

流水线现在包括依赖项扫描作业。

<a id="customizing-the-dependency-scanning-settings"></a>

### 自定义依赖项扫描设置

可以使用 [`variables`](../../../ci/yaml/index.md#variables) 通过 [CI/CD 变量](#available-cicd-variables)（设置 `.gitlab-ci.yml` 中的参数）更改依赖项扫描。
例如：

```yaml
include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml

variables:
  SECURE_LOG_LEVEL: error
```

因为模板在流水线配置[之前取值](../../../ci/yaml/index.md#include)，所以最后提及的变量优先。

### 覆盖依赖项扫描作业

WARNING:
从 13.0 开始，不再支持使用 [`only` 和 `except`](../../../ci/yaml/index.md#only--except)。覆盖模板时，您必须改用 [`rules`](../../../ci/yaml/index.md#rules)。

要覆盖作业定义（例如，更改 `variables` 或 `dependencies` 等属性），请声明一个与要覆盖的作业同名的新作业。将此新作业放在模板 include 之后，并在其下指定任何其它的 key。例如，禁用 `gemnasium` 分析器的 `DS_REMEDIATE`：

```yaml
include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml

gemnasium-dependency_scanning:
  variables:
    DS_REMEDIATE: "false"
```

要覆盖 `dependencies: []` 属性，请像上面一样添加一个覆盖作业，以该属性为目标：

```yaml
include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml

gemnasium-dependency_scanning:
  dependencies: ["build"]
```

### 可用的 CI/CD 变量

可以使用环境变量[配置](#customizing-the-dependency-scanning-settings)依赖扫描。

<a id="configuring-dependency-scanning"></a>

#### 配置依赖扫描

以下变量允许配置全局依赖扫描设置。

WARNING:
在将这些更改合并到默认分支之前，应在合并请求中测试极狐GitLab 安全扫描工具的所有自定义。不这样做会产生意想不到的结果，包括大量误报。

| CI/CD 变量             | 描述 |
| ----------------------------|------------ |
| `ADDITIONAL_CA_CERT_BUNDLE` | 要信任的 CA 证书捆绑包。此处提供的证书包也被其他工具在扫描过程中使用，例如 `git`、`yarn` 或 `npm`。查看[使用自定义 SSL CA 证书颁发机构](#using-a-custom-ssl-ca-certificate-authority)，获取更多信息。 |
| `DS_EXCLUDED_ANALYZERS`      | 指定要从依赖扫描中排除的分析器（按名称）。有关详细信息，请参阅[依赖扫描分析器](analyzers.md)。 |
| `DS_EXCLUDED_PATHS`         | 根据路径从扫描中排除文件和目录，以逗号分隔的 pattern 列表。Pattern 可以是 glob（有关支持的模板，请参阅 [`doublestar.Match`](https://pkg.go.dev/github.com/bmatcuk/doublestar/v4@v4.0.2#Match)）、文件或文件夹路径（例如，`doc、spec`），父目录也匹配 pattern。默认值：`"spec，test，tests，tmp"`。 |
| `DS_IMAGE_SUFFIX`           | 后缀添加到镜像名称。如果设置为 `-fips`，则使用 `FIPS-enabled` 镜像进行扫描。有关详细信息，请参阅[启用 FIPS 的镜像](#fips-enabled-images)。引入于 14.10 版本。 |
| `SECURE_ANALYZERS_PREFIX`   | 覆盖提供官方默认镜像（代理）的 Docker 镜像库的名称。阅读有关[自定义分析器](analyzers.md)的更多信息。 |
| `SECURE_LOG_LEVEL`          | 设置最低日志记录级别，输出此日志级别或更高级别的消息，从最高到最低严重性，日志记录级别是：`fatal`、`error`、`warn`、`info`、`debug`。引入于 13.1 版本，默认值：`info`。 |

<a id="configuring-specific-analyzers-used-by-dependency-scanning"></a>

#### 配置依赖扫描使用的特定分析器

以下变量用于配置特定分析器（用于特定语言/框架）。

| CI/CD 变量                      | 分析器          | 默认值                      | 描述 |
|--------------------------------------| ------------------ | ---------------------------- |------------ |
| `GEMNASIUM_DB_LOCAL_PATH`            | `gemnasium`        | `/gemnasium-db`              | 本地 Gemnasium 数据库的路径。 |
| `GEMNASIUM_DB_UPDATE_DISABLED`       | `gemnasium`        | `"false"`                    | 禁用 `gemnasium-db` 咨询数据库的自动更新<!--(For usage see: [examples](#hosting-a-copy-of-the-gemnasium_db-advisory-database))--> |
| `GEMNASIUM_DB_REMOTE_URL`            | `gemnasium`        | <!--`https://gitlab.com/gitlab-org/security-products/gemnasium-db.git`--> | 用于获取 Gemnasium 数据库的仓库 URL。 |
| `GEMNASIUM_DB_REF_NAME`              | `gemnasium`        | `master`                     | 远端仓库数据库的分支名称。需要先配置 `GEMNASIUM_DB_REMOTE_URL`。 |
| `DS_REMEDIATE`                       | `gemnasium`        | `"true"`，FIPS 模式中为 `"false"`                     | 启用易受攻击的依赖项的自动修复。 |
| `DS_REMEDIATE_TIMEOUT`               | `gemnasium`        | `5m`                       | 自动修复超时。 |
| `GEMNASIUM_LIBRARY_SCAN_ENABLED`     | `gemnasium`        | `"true"`                     | 启用检测供应商 JavaScript 库中的漏洞。目前，`gemnasium` 利用 [`Retire.js`](https://github.com/RetireJS/retire.js) 来完成这项工作。引入于 14.8 版本。 |
| `DS_JAVA_VERSION`                    | `gemnasium-maven`  | `17`                         | Java 版本。可用版本：`8`、`11`、`17`。 |
| `MAVEN_CLI_OPTS`                     | `gemnasium-maven`  | `"-DskipTests --batch-mode"` | 分析器传递给 `maven` 的命令行参数列表。 |
| `GRADLE_CLI_OPTS`                    | `gemnasium-maven`  |                              | 分析器传递给 `gradle` 的命令行参数列表。 |
| `SBT_CLI_OPTS`                       | `gemnasium-maven`  |                              | 分析器传递给 `sbt` 的命令行参数列表。 |
| `PIP_INDEX_URL`                      | `gemnasium-python` | `https://pypi.org/simple`    | Python 包索引的基本 URL。 |
| `PIP_EXTRA_INDEX_URL`                | `gemnasium-python` |                              | 除了 `PIP_INDEX_URL` 之外要使用的包索引的[额外 URL](https://pip.pypa.io/en/stable/reference/pip_install/#cmdoption-extra-index-url) 数组，逗号分隔。**警告：** 使用此环境变量时请阅读[以下安全注意事项](#python-projects)。 |
| `PIP_REQUIREMENTS_FILE`              | `gemnasium-python` |                              | 要扫描的 Pip 需求文件。 |
| `PIPENV_PYPI_MIRROR`                 | `gemnasium-python` |                              | 如果设置，则使用[镜像](https://github.com/pypa/pipenv/blob/v2022.1.8/pipenv/environments.py#L263)覆盖 Pipenv 使用的 PyPi 索引。 |
| `DS_PIP_VERSION`                     | `gemnasium-python` |                              | 强制安装特定的 pip 版本（例如：`"19.3"`），否则使用 Docker 镜像中安装的 pip。 |
| `DS_PIP_DEPENDENCY_PATH`             | `gemnasium-python` |                              | 从中加载 Python pip 依赖项的路径。 |
| `DS_INCLUDE_DEV_DEPENDENCIES`        | `gemnasium`        | `"true"`                     | 当设置为 `false` 时，不报告开发依赖项及其漏洞。仅支持 Composer、NPM 和 Poetry 项目。引入于 15.1 版本。 |
| `GOOS`                               | `gemnasium`        | `"linux"`                    | 编译 Go 代码的操作系统。 |
| `GOARCH`                             | `gemnasium`        | `"amd64"`                    | 编译 Go 代码的处理器架构。 |
| `GOFLAGS`                            | `gemnasium`        |                              | 传递给 `go build` 工具的标志。 |
| `GOPRIVATE`                          | `gemnasium`        |                              | 要从源中获取的 glob pattern 和前缀列表。阅读 Go 私有模块[文档](https://go.dev/ref/mod#private-modules)，了解更多信息。 |

#### 其它变量

前面的表格并不是可以使用的所有变量的详尽列表，包含我们支持和测试的所有特定极狐GitLab 和分析器变量。有许多变量，例如环境变量，您可以传入并且它们会起作用。这是一个很大的列表，其中许多我们可能不知道，因此没有记录在案。

例如，要将非 GitLab 环境变量 `HTTPS_PROXY` 传递给所有依赖扫描作业，请将其设置为 `.gitlab-ci.yml` 文件中的自定义 CI/CD 变量，如下所示：

```yaml
variables:
  HTTPS_PROXY: "https://squid-proxy:3128"
```

或者，我们可以在特定的工作中使用它，比如依赖扫描：

```yaml
dependency_scanning:
  variables:
    HTTPS_PROXY: $HTTPS_PROXY
```

<!--
As we have not tested all variables you may find some will work and others will not.
If one does not work and you need it we suggest
[submitting a feature request](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20proposal%20-%20detailed&issue[title]=Docs%20feedback%20-%20feature%20proposal:%20Write%20your%20title)
or [contributing to the code](../../../development/index.md) to enable it to be used.
-->

<a id="using-a-custom-ssl-ca-certificate-authority"></a>

### 使用自定义 SSL CA 证书颁发机构

您可以使用 `ADDITIONAL_CA_CERT_BUNDLE` CI/CD 变量来配置自定义 SSL CA 证书颁发机构。`ADDITIONAL_CA_CERT_BUNDLE` 值应包含 [X.509 PEM 公钥证书的文本表示形式](https://www.rfc-editor.org/rfc/rfc7468#section-5.1)。例如，要在 `.gitlab-ci.yml` 文件中配置此值，请使用以下命令：

```yaml
variables:
  ADDITIONAL_CA_CERT_BUNDLE: |
      -----BEGIN CERTIFICATE-----
      MIIGqTCCBJGgAwIBAgIQI7AVxxVwg2kch4d56XNdDjANBgkqhkiG9w0BAQsFADCB
      ...
      jWgmPqF3vUbZE0EyScetPJquRFRKIesyJuBFMAs=
      -----END CERTIFICATE-----
```

`ADDITIONAL_CA_CERT_BUNDLE` 值也可以配置为 [UI 中的自定义变量](../../../ci/variables/index.md#for-a-project)，或者配置为 `file`，需要证书的路径；或者作为变量，需要证书的文本表示。

### 使用私有 Maven 仓库

如果您的私有 Maven 仓库需要登录凭据，您可以使用 `MAVEN_CLI_OPTS` CI/CD 变量。

<!--
Read more on [how to use private Maven repositories](../index.md#using-private-maven-repositories).
-->

<a id="fips-enabled-images"></a>

#### FIPS-enabled 镜像

> 引入于 14.10 版本

极狐GitLab 还提供[支持 FIPS 的 Red Hat UBI](https://www.redhat.com/en/blog/introducing-red-hat-universal-base-image) 版本的 Gemnasium 镜像。因此，您可以用启用 FIPS 的镜像替换标准镜像。

当在实例中启用 FIPS 模式时，Gemnasium 扫描作业会自动使用启用 FIPS 的镜像。（在 15.0 版本中引入。）

要手动切换到启用 FIPS 的镜像，请将变量 `DS_IMAGE_SUFFIX` 设置为 `"-fips"`。

FIPS 模式不支持 Gradle 项目的依赖扫描和 Yarn 项目的自动修复。

## 报告 JSON 格式

依赖项扫描工具会发出一个 JSON 报告文件。<!--For more information, see the
[schema for this report](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/dependency-scanning-report-format.json).-->

以下是一个示例依赖扫描报告：

```json
{
  "version": "2.0",
  "vulnerabilities": [
    {
      "id": "51e83874-0ff6-4677-a4c5-249060554eae",
      "category": "dependency_scanning",
      "name": "Regular Expression Denial of Service",
      "message": "Regular Expression Denial of Service in debug",
      "description": "The debug module is vulnerable to regular expression denial of service when untrusted user input is passed into the `o` formatter. It takes around 50k characters to block for 2 seconds making this a low severity issue.",
      "severity": "Unknown",
      "solution": "Upgrade to latest versions.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "yarn.lock",
        "dependency": {
          "package": {
            "name": "debug"
          },
          "version": "1.0.5"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-37283ed4-0380-40d7-ada7-2d994afcc62a",
          "value": "37283ed4-0380-40d7-ada7-2d994afcc62a",
          "url": "https://deps.sec.gitlab.com/packages/npm/debug/versions/1.0.5/advisories"
        }
      ],
      "links": [
        {
          "url": "https://nodesecurity.io/advisories/534"
        },
        {
          "url": "https://github.com/visionmedia/debug/issues/501"
        },
        {
          "url": "https://github.com/visionmedia/debug/pull/504"
        }
      ]
    },
    {
      "id": "5d681b13-e8fa-4668-957e-8d88f932ddc7",
      "category": "dependency_scanning",
      "name": "Authentication bypass via incorrect DOM traversal and canonicalization",
      "message": "Authentication bypass via incorrect DOM traversal and canonicalization in saml2-js",
      "description": "Some XML DOM traversal and canonicalization APIs may be inconsistent in handling of comments within XML nodes. Incorrect use of these APIs by some SAML libraries results in incorrect parsing of the inner text of XML nodes such that any inner text after the comment is lost prior to cryptographically signing the SAML message. Text after the comment, therefore, has no impact on the signature on the SAML message.\r\n\r\nA remote attacker can modify SAML content for a SAML service provider without invalidating the cryptographic signature, which may allow attackers to bypass primary authentication for the affected SAML service provider.",
      "severity": "Unknown",
      "solution": "Upgrade to fixed version.\r\n",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "yarn.lock",
        "dependency": {
          "package": {
            "name": "saml2-js"
          },
          "version": "1.5.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-9952e574-7b5b-46fa-a270-aeb694198a98",
          "value": "9952e574-7b5b-46fa-a270-aeb694198a98",
          "url": "https://deps.sec.gitlab.com/packages/npm/saml2-js/versions/1.5.0/advisories"
        },
        {
          "type": "cve",
          "name": "CVE-2017-11429",
          "value": "CVE-2017-11429",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-11429"
        }
      ],
      "links": [
        {
          "url": "https://github.com/Clever/saml2/commit/3546cb61fd541f219abda364c5b919633609ef3d#diff-af730f9f738de1c9ad87596df3f6de84R279"
        },
        {
          "url": "https://github.com/Clever/saml2/issues/127"
        },
        {
          "url": "https://www.kb.cert.org/vuls/id/475445"
        }
      ]
    }
  ],
  "remediations": [
    {
      "fixes": [
        {
          "id": "5d681b13-e8fa-4668-957e-8d88f932ddc7",
        }
      ],
      "summary": "Upgrade saml2-js",
      "diff": "ZGlmZiAtLWdpdCBhL...OR0d1ZUc2THh3UT09Cg==" // some content is omitted for brevity
    }
  ]
}
```

### CycloneDX 软件物料清单

> - 引入于 14.8 版本，Beta 功能。
> - 一般可用于 15.7 版本。

除了 [JSON 报告文件](#reports-json-format)，[Gemnasium](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium) 依赖扫描工具为它检测到的每个支持的锁定或构建文件输出一个 [CycloneDX](https://cyclonedx.org/) Software Bill of Materials (SBOM)。这些 CycloneDX SBOM 被命名为 `gl-sbom-<package-type>-<package-manager>.json`，并保存在与检测到的锁定或构建文件相同的目录中。

例如，如果您的项目具有以下结构：

```plaintext
.
├── ruby-project/
│   └── Gemfile.lock
├── ruby-project-2/
│   └── Gemfile.lock
├── php-project/
│   └── composer.lock
└── go-project/
    └── go.sum
```

然后 Gemnasium 扫描器生成以下 CycloneDX SBOM：

```plaintext
.
├── ruby-project/
│   ├── Gemfile.lock
│   └── gl-sbom-gem-bundler.cdx.json
├── ruby-project-2/
│   ├── Gemfile.lock
│   └── gl-sbom-gem-bundler.cdx.json
├── php-project/
│   ├── composer.lock
│   └── gl-sbom-packagist-composer.cdx.json
└── go-project/
    ├── go.sum
    └── gl-sbom-go-go.cdx.json
```

可以下载 CycloneDX SBOM，[与下载其他作业产物的方式相同](../../../ci/pipelines/job_artifacts.md#下载作业产物)。

### 合并多个 CycloneDX SBOM

您可以使用 CI/CD 作业将多个 CycloneDX SBOM 合并为一个 SBOM。
例如：

```yaml
stages:
  - test
  - merge-cyclonedx-sboms

include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml

merge cyclonedx sboms:
  stage: merge-cyclonedx-sboms
  image:
    name: cyclonedx/cyclonedx-cli:0.24.0
    entrypoint: [""]
  script:
    - find . -name "gl-sbom-*.cdx.json" -exec /cyclonedx merge --output-file gl-sbom-all.cdx.json --input-files "{}" +
  artifacts:
    paths:
      - gl-sbom-all.cdx.json
```

极狐GitLab 使用 [CycloneDX 属性](https://cyclonedx.org/use-cases/#properties--name-value-store)，在每个 CycloneDX SBOM 的元数据中存储实现特定的详细信息，例如构建和锁定的文件位置。如果多个 CycloneDX SBOM 合并在一起，则此信息将从生成的合并文件中删除。

<!--
## Versioning and release process

Please check the [Release Process documentation](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/release_process.md).

## Contributing to the vulnerability database

You can search the [`gemnasium-db`](https://gitlab.com/gitlab-org/security-products/gemnasium-db) project
to find a vulnerability in the Gemnasium database.
You can also [submit new vulnerabilities](https://gitlab.com/gitlab-org/security-products/gemnasium-db/blob/master/CONTRIBUTING.md).


## 在离线环境中运行依赖扫描

对于通过 Internet 对外部资源进行有限、受限或间歇性访问的环境中的私有化部署实例，需要进行一些调整才能成功运行依赖项扫描作业。<!--For more information, see [Offline environments](../offline_deployments/index.md).

### 离线依赖扫描的要求

以下是在离线环境中使用依赖扫描的要求：

- 带有 `docker` 或 `kubernetes` executor 的 GitLab Runner。
- 具有本地可用的依赖扫描分析器镜像副本的 Docker 容器镜像库。
- If you have a limited access environment you need to allow access, such as using a proxy, to the advisory database: `https://gitlab.com/gitlab-org/security-products/gemnasium-db.git`.
  If you are unable to permit access to `https://gitlab.com/gitlab-org/security-products/gemnasium-db.git` you must host an offline copy of this `git` repository and set the `GEMNASIUM_DB_REMOTE_URL` CI/CD variable to the URL of this repository. For more information on configuration variables, see [Dependency Scanning](#configuring-dependency-scanning).

  This advisory database is constantly being updated, so you must periodically sync your local copy with GitLab.

- _Only if scanning Ruby projects_: Host an offline Git copy of the [advisory database](https://github.com/rubysec/ruby-advisory-db).
- _Only if scanning npm/yarn projects_: Host an offline copy of the [`retire.js`](https://github.com/RetireJS/retire.js/) [node](https://github.com/RetireJS/retire.js/blob/master/repository/npmrepository.json) and [`js`](https://github.com/RetireJS/retire.js/blob/master/repository/jsrepository.json) advisory databases.

Note that GitLab Runner has a [default `pull policy` of `always`](https://docs.gitlab.com/runner/executors/docker.html#using-the-always-pull-policy),
meaning the runner tries to pull Docker images from the GitLab container registry even if a local
copy is available. The GitLab Runner [`pull_policy` can be set to `if-not-present`](https://docs.gitlab.com/runner/executors/docker.html#using-the-if-not-present-pull-policy)
in an offline environment if you prefer using only locally available Docker images. However, we
recommend keeping the pull policy setting to `always` if not in an offline environment, as this
enables the use of updated scanners in your CI/CD pipelines.

### Make GitLab dependency scanning analyzer images available inside your Docker registry

For dependency scanning with all [supported languages and frameworks](#supported-languages-and-package-managers),
import the following default dependency scanning analyzer images from `registry.gitlab.com` into
your [local Docker container registry](../../packages/container_registry/index.md):

```plaintext
registry.gitlab.cn/security-products/analyzers/gemnasium:2
registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven:2
registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python:2
registry.gitlab.com/gitlab-org/security-products/analyzers/retire.js:2
registry.gitlab.com/gitlab-org/security-products/analyzers/bundler-audit:2
```

The process for importing Docker images into a local offline Docker registry depends on
**your network security policy**. Please consult your IT staff to find an accepted and approved
process by which external resources can be imported or temporarily accessed.
These scanners are [periodically updated](../index.md#vulnerability-scanner-maintenance)
with new definitions, and you may be able to make occasional updates on your own.

For details on saving and transporting Docker images as a file, see Docker's documentation on
[`docker save`](https://docs.docker.com/engine/reference/commandline/save/), [`docker load`](https://docs.docker.com/engine/reference/commandline/load/),
[`docker export`](https://docs.docker.com/engine/reference/commandline/export/), and [`docker import`](https://docs.docker.com/engine/reference/commandline/import/).

#### Support for Custom Certificate Authorities

Support for custom certificate authorities was introduced in the following versions.

| Analyzer | Version |
| -------- | ------- |
| `gemnasium` | [v2.8.0](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/releases/v2.8.0) |
| `gemnasium-maven` | [v2.9.0](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/-/releases/v2.9.0) |
| `gemnasium-python` | [v2.7.0](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/-/releases/v2.7.0) |
| `retire.js` | [v2.4.0](https://gitlab.com/gitlab-org/security-products/analyzers/retire.js/-/releases/v2.4.0) |
| `bundler-audit` | [v2.4.0](https://gitlab.com/gitlab-org/security-products/analyzers/bundler-audit/-/releases/v2.4.0) |

### Set dependency scanning CI/CD job variables to use local dependency scanning analyzers

Add the following configuration to your `.gitlab-ci.yml` file. You must change the value of
`SECURE_ANALYZERS_PREFIX` to refer to your local Docker container registry. You must also change the
value of `GEMNASIUM_DB_REMOTE_URL` to the location of your offline Git copy of the
[`gemnasium-db` advisory database](https://gitlab.com/gitlab-org/security-products/gemnasium-db/):

```yaml
include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml

variables:
  SECURE_ANALYZERS_PREFIX: "docker-registry.example.com/analyzers"
  GEMNASIUM_DB_REMOTE_URL: "gitlab.example.com/gemnasium-db.git"
```

See explanations of the variables above in the [configuration section](#configuration).

### 语言和包管理器的特定设置

有关配置特定语言和包管理器的信息，请参阅以下部分。

#### JavaScript（npm 和 yarn）项目

将以下内容添加到 `.gitlab-ci.yml` 的变量部分：

```yaml
RETIREJS_JS_ADVISORY_DB: "example.com/jsrepository.json"
RETIREJS_NODE_ADVISORY_DB: "example.com/npmrepository.json"
```

#### Ruby（gem）项目

将以下内容添加到 `.gitlab-ci.yml` 的变量部分：

```yaml
BUNDLER_AUDIT_ADVISORY_DB_REF_NAME: "master"
BUNDLER_AUDIT_ADVISORY_DB_URL: "gitlab.example.com/ruby-advisory-db.git"
```

#### Python（设置工具）

当为您的私有 PyPi 仓库使用自签名证书时，不需要额外的作业配置（除了上面的模板 `.gitlab-ci.yml`）。但是，您必须更新您的 `setup.py`，确保它可以访问您的私有仓库。一个示例配置：

1. 更新 `setup.py`，为 `install_requires` 列表中的每个依赖项创建一个指向您的私有仓库的 `dependency_links` 属性：

   ```python
   install_requires=['pyparsing>=2.0.3'],
   dependency_links=['https://pypi.example.com/simple/pyparsing'],
   ```

1. 从您的仓库 URL 获取证书并将其添加到项目中：

   ```shell
   echo -n | openssl s_client -connect pypi.example.com:443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > internal.crt
   ```

1. 将 `setup.py` 指向新下载的证书：

   ```python
   import setuptools.ssl_support
   setuptools.ssl_support.cert_paths = ['internal.crt']
   ```

## Hosting a copy of the gemnasium_db advisory database

The [`gemnasium_db`](https://gitlab.com/gitlab-org/security-products/gemnasium-db) Git repository is
used by `gemnasium`, `gemnasium-maven`, and `gemnasium-python` as the source of vulnerability data.
This repository updates at scan time to fetch the latest advisories. However, due to a restricted
networking environment, running this update is sometimes not possible. In this case, a user can do
one of the following:

- [Host a copy of the advisory database](#host-a-copy-of-the-advisory-database)
- [Use a local clone](#use-a-local-clone)

### Host a copy of the advisory database

If [gemnasium-db](https://gitlab.com/gitlab-org/security-products/gemnasium-db) is not reachable
from within the environment, the user can host their own Git copy. Then the analyzer can be
instructed to update the database from the user's copy by using `GEMNASIUM_DB_REMOTE_URL`:

```yaml
variables:
  GEMNASIUM_DB_REMOTE_URL: https://users-own-copy.example.com/gemnasium-db/.git

...
```

### Use a local clone

If a hosted copy is not possible, then the user can clone [gemnasium-db](https://gitlab.com/gitlab-org/security-products/gemnasium-db)
or create an archive before the scan and point the analyzer to the directory (using:
`GEMNASIUM_DB_LOCAL_PATH`). Turn off the analyzer's self-update mechanism (using:
`GEMNASIUM_DB_UPDATE_DISABLED`). In this example, the database directory is created in the
`before_script`, before the `gemnasium` analyzer's scan job:

```yaml
...

gemnasium-dependency_scanning:
  variables:
    GEMNASIUM_DB_LOCAL_PATH: ./gemnasium-db-local
    GEMNASIUM_DB_UPDATE_DISABLED: "true"
  before_script:
    - mkdir $GEMNASIUM_DB_LOCAL_PATH
    - tar -xzf gemnasium_db.tar.gz -C $GEMNASIUM_DB_LOCAL_PATH
```
-->

## 警告

我们建议您使用所有容器的最新版本，以及所有包管理器和语言的最新支持版本。使用以前的版本会增加安全风险，因为不受支持的版本可能不再受益于主动安全报告和安全修复的反向移植。

<a id="python-projects"></a>

### Python 项目

使用 [`PIP_EXTRA_INDEX_URL`](https://pipenv.pypa.io/en/latest/cli/#envvar-PIP_EXTRA_INDEX_URL) 环境变量时需要格外小心，因为 [CVE-2018-20225](https://nvd.nist.gov/vuln/detail/CVE-2018-20225)：

> 在 pip（所有版本）中发现了一个问题，因为它安装了具有最高版本号的版本，即使用户打算从私有索引中获取私有包。这只会影响 `PIP_EXTRA_INDEX_URL` 选项的使用，并且利用需要包不存在于公共索引中（因此攻击者可以将包以任意版本号放在那里）。

<a id="troubleshooting"></a>

## 故障排查

### 增加日志详细程度

当[作业日志](../../../ci/jobs/index.md#expand-and-collapse-job-log-sections)不包含有关依赖扫描失败的足够信息时，[ 将 `SECURE_LOG_LEVEL` 设置为 `debug`](#configuring-dependency-scanning)，并检查生成的更详细的日志。

### 解决缺少对某些语言或包管理器的支持

如["支持的语言" 部分](#supported-languages-and-package-managers)中所述，某些依赖项定义文件尚不支持。
但是，如果语言、包管理器或第三方工具可以将定义文件转换为支持的格式，则可以实现依赖扫描。

一般来说，方法如下：

1. 在 `.gitlab-ci.yml` 文件中定义一个专用的转换器作业。使用合适的 Docker 镜像、脚本或两者皆有，来促进转换。
1. 让该作业将转换后的受支持文件作为产物上传。
1. 将 `dependencies: [<your-converter-job>]` 添加到您的 `dependency_scanning` 作业中，来使用转换后的定义文件。

例如，*只*有 `pyproject.toml` 文件的 Poetry 项目可以生成 `poetry.lock` 文件，如下所示。

```yaml
include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml

stages:
  - test

gemnasium-python-dependency_scanning:
  # Work around https://gitlab.com/gitlab-org/gitlab/-/issues/32774
  before_script:
    - pip install "poetry>=1,<2"  # Or via another method: https://python-poetry.org/docs/#installation
    - poetry update --lock # Generates the lock file to be analyzed.
```

### `Error response from daemon: error processing tar file: docker-tar: relocation error`

当运行依赖扫描作业的 Docker 版本为 19.03.0 时会出现此错误。考虑更新到 Docker `19.03.1` 或更高版本。旧版本不受影响。

### 警告消息 `gl-dependency-scanning-report.json: no matching files`

获取更多信息，查阅[一般应用程序安全故障排查部分](../../../ci/jobs/job_artifacts_troubleshooting.md#error-message-no-files-to-upload)。

### 使用 rules:exists 的限制

[依赖项扫描 CI 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/Dependency-Scanning.gitlab-ci.yml)使用 [`rules:exists`](../../../ci/yaml/index.md#rulesexists) 语法。该指令限制为 10000 次检查，并且在达到此数量后始终返回 `true`。因此，根据仓库中文件的数量，即使扫描程序不支持您的项目，也可能会触发依赖项扫描作业。

<!--
### Error: `dependency_scanning is used for configuration only, and its script should not be executed`

For information on this, see the [GitLab Secure troubleshooting section](../index.md#error-job-is-used-for-configuration-only-and-its-script-should-not-be-executed).
-->

### 为基于 Java 的项目导入多个证书

`gemnasium-maven` 分析器使用 `keytool` 读取 `ADDITIONAL_CA_CERT_BUNDLE` 变量的内容，导入单个证书或证书链。多个不相关的证书被忽略，只有第一个由 `keytool` 导入。

要将多个不相关的证书添加到分析器，您可以在 `gemnasium-maven-dependency_scanning` 作业的定义中声明一个 `before_script`，例如：

```yaml
gemnasium-maven-dependency_scanning:
  before_script:
    - . $HOME/.bashrc # make the java tools available to the script
    - OIFS="$IFS"; IFS=""; echo $ADDITIONAL_CA_CERT_BUNDLE > multi.pem; IFS="$OIFS" # write ADDITIONAL_CA_CERT_BUNDLE variable to a PEM file
    - csplit -z --digits=2 --prefix=cert multi.pem "/-----END CERTIFICATE-----/+1" "{*}" # split the file into individual certificates
    - for i in `ls cert*`; do keytool -v -importcert -alias "custom-cert-$i" -file $i -trustcacerts -noprompt -storepass changeit -keystore /opt/asdf/installs/java/adoptopenjdk-11.0.7+10.1/lib/security/cacerts 1>/dev/null 2>&1 || true; done # import each certificate using keytool (note the keystore location is related to the Java version being used and should be changed accordingly for other versions)
    - unset ADDITIONAL_CA_CERT_BUNDLE # unset the variable so that the analyzer doesn't duplicate the import
```

### 依赖项扫描作业失败并显示消息 `strconv.ParseUint: parsing "0.0": invalid syntax`

调用 Docker-in-Docker 可能是导致此错误的原因。Docker-in-Docker：

- 在 13.0 及更高版本中默认禁用。
- 13.4 及更高版本不支持。

要修复此错误，请禁用 Docker-in-Docker 进行依赖扫描，为在 CI/CD 流水线中运行的每个分析器创建单独的 `<analyzer-name>-dependency_scanning` 作业。

```yaml
include:
  - template: Dependency-Scanning.gitlab-ci.yml

variables:
  DS_DISABLE_DIND: "true"
```

### 消息 `<file> does not exist in <commit SHA>`

当显示文件中依赖项的 `Location` 时，链接中的路径会转到特定的 Git SHA。

但是，如果我们的依赖项扫描工具检查的 lockfile 已被缓存，则选择该链接会将您重定向到仓库根目录，并显示消息：`<file> does not exist in <commit SHA>`。

Lockfile 在构建阶段被缓存，并在扫描发生之前传递给依赖扫描作业。因为缓存是在分析器运行之前下载的，所以在 `CI_BUILDS_DIR` 目录中存在 lockfile 会触发依赖项扫描作业。

我们建议提交 lockfile，以防止出现此警告。

### 设置 `DS_MAJOR_VERSION` 或 `DS_ANALYZER_IMAGE` 后，我不再获得最新的 Docker 镜像

如果您出于特定原因手动设置了 `DS_MAJOR_VERSION` 或 `DS_ANALYZER_IMAGE`，现在必须更新配置以再次获取我们分析器的最新修补版本，请编辑 `gitlab-ci.yml` 文件，然后：

- 设置您的 `DS_MAJOR_VERSION` 来匹配最新版本，如[我们当前的依赖扫描模板](https://jihulab.cn/gitlab-cn/gitlab/blob/master/lib/gitlab/ci/templates/Security/Dependency-Scanning.gitlab-ci.yml#L18)。
- 如果您直接硬编码 `DS_ANALYZER_IMAGE` 变量，请将其更改为与我们[当前的依赖扫描模板](https://jihulab.com/gitlab-cn/gitlab/blob/master/lib/gitlab/ci/templates/Security/Dependency-Scanning.gitlab-ci.yml)中找到的最新行匹配。行号将根据您编辑的扫描作业而有所不同。

  例如，当前 `gemnasium-maven-dependency_scanning` 作业会拉取最新的 `gemnasium-maven` Docker 镜像，因为 `DS_ANALYZER_IMAGE` 设置为 `"$SECURE_ANALYZERS_PREFIX/gemnasium-maven:$DS_MAJOR_VERSION"`。

### setuptools 项目的依赖项扫描失败，出现 `use_2to3 is invalid` 错误

对 [2to3](https://docs.python.org/3/library/2to3.html) 的支持已在 `setuptools` 版本 `v58.0.0` 中[删除](https://setuptools.pypa.io/en/latest/history.html#v58-0-0)。依赖扫描（运行 `python 3.9`）使用 `setuptools` 版本 `58.1.0+`，不支持 `2to3`。 因此，依赖于 `lib2to3` 的 `setuptools` 依赖项将失败并显示以下消息：

```plaintext
error in <dependency name> setup command: use_2to3 is invalid
```

要解决此错误，请降级分析器的 `setuptools` 版本（例如 `v57.5.0`）：

```yaml
gemnasium-python-dependency_scanning:
  before_script:
    - pip install setuptools==57.5.0
```

### 使用 psycopg2 对项目的依赖项扫描失败，并出现 `pg_config executable not found` 错误

扫描依赖于 `psycopg2` 的 Python 项目可能会失败并显示以下消息：

```plaintext
Error: pg_config executable not found.
```

[psycopg2](https://pypi.org/project/psycopg2/) 依赖于 `libpq-dev` Debian 软件包，它没有安装在 `gemnasium-python` Docker 镜像中。要解决此错误，请在 `before_script` 中安装 `libpq-dev` 包：

```yaml
gemnasium-python-dependency_scanning:
  before_script:
    - apt-get update && apt-get install -y libpq-dev
```

### 使用带有 `CI_JOB_TOKEN` 的 `poetry config http-basic` 时出现错误 `NoSuchOptionException`

当自动生成的 `CI_JOB_TOKEN` 以连字符 (`-`) 开头时，可能会发生此错误。
为避免此错误，请遵循 [Poetry 的配置建议](https://python-poetry.org/docs/repositories/#configuring-credentials)。

<!--
### Error: Project has `<number>` unresolved dependencies

The error message `Project has <number> unresolved dependencies` indicates a dependency resolution problem caused by your `gradle.build` or `gradle.build.kts` file. In the current release, `gemnasium-maven` cannot continue processing when an unresolved dependency is encountered. However, There is an [open issue](https://gitlab.com/gitlab-org/gitlab/-/issues/337083) to allow `gemnasium-maven` to recover from unresolved dependency errors and produce a dependency graph. Until this issue has been resolved, you'll need to consult the [Gradle dependency resolution docs](https://docs.gradle.org/current/userguide/dependency_resolution.html) for details on how to fix your `gradle.build` file.

### Setting build constraints when scanning Go projects

Dependency scanning runs within a `linux/amd64` container. As a result, the build list generated
for a Go project will contain dependencies that are compatible with this environment. If your deployment environment is not
`linux/amd64`, the final list of dependencies might contain additional incompatible
modules. The dependency list might also omit modules that are only compatible with your deployment environment. To prevent
this issue, you can configure the build process to target the operating system and architecture of the deployment
environment by setting the `GOOS` and `GOARCH` [environment variables](https://go.dev/ref/mod#minimal-version-selection)
of your `.gitlab-ci.yml` file.

For example:

```yaml
variables:
  GOOS: "darwin"
  GOARCH: "arm64"
```

You can also supply build tag constraints by using the `GOFLAGS` variable:

```yaml
variables:
  GOFLAGS: "-tags=test_feature"
```

### Dependency Scanning of Go projects returns false positives

The `go.sum` file contains an entry of every module that was considered while generating the project's [build list](https://go.dev/ref/mod#glos-build-list).
Multiple versions of a module are included in the `go.sum` file, but the [MVS](https://go.dev/ref/mod#minimal-version-selection)
algorithm used by `go build` only selects one. As a result, when dependency scanning uses `go.sum`, it might report false positives.

To prevent false positives, gemnasium only uses `go.sum` if it is unable to generate the build list for the Go project. If `go.sum` is selected, a warning occurs:

```shell
[WARN] [Gemnasium] [2022-09-14T20:59:38Z] ▶ Selecting "go.sum" parser for "/test-projects/gitlab-shell/go.sum". False positives may occur. See https://gitlab.com/gitlab-org/gitlab/-/issues/321081.
```
-->
