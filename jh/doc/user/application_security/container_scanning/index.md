---
type: reference, howto
stage: Protect
group: Container Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 容器扫描 **(FREE)**

您的应用程序的 Docker 镜像本身可能基于包含已知漏洞的 Docker 镜像。通过在流水线中包含一个额外的容器扫描作业来扫描这些漏洞，并在合并请求中显示它们，您可以使用极狐GitLab 来审核基于 Docker 的应用程序。

容器扫描通常被认为是软件组合分析 (SCA) 的一部分。SCA 可以包含检查代码使用的项目的各个方面。这些项目通常来自从外部源导入的应用程序和系统依赖项，而不是来自您自己编写的项目。

极狐GitLab 提供容器扫描和[依赖扫描](../dependency_scanning/index.md)，确保覆盖所有依赖类型。为了尽可能多地覆盖您的风险区域，我们鼓励您使用我们所有的安全扫描器。

## 概览

极狐GitLab 与开源工具集成，用于容器中的漏洞静态分析：

- [Trivy](https://github.com/aquasecurity/trivy)
- [Grype](https://github.com/anchore/grype)

<!--
To integrate GitLab with security scanners other than those listed here, see
[Security scanner integration](../../../development/integrations/secure.md).
-->

您可以通过执行以下操作之一来启用容器扫描：

- 在现有的 `.gitlab-ci.yml` 文件中[包含 CI 作业](#configuration)。
- 隐式使用由 [Auto DevOps](../../../topics/autodevops/index.md) 提供的 [Auto Container Scanning](../../../topics/autodevops/stages.md#auto-container-scanning)。

极狐GitLab 比较源分支和目标分支之间发现的漏洞，并在合并请求中直接显示信息。

![Container Scanning Widget](img/container_scanning_v13_2.png)

### 兼容性

| 兼容性 | 免费版 | 旗舰版 |
| --- | ------ | ------ |
| [配置扫描器](#configuration) | 是 | 是 |
| 自定义设置 <!--([变量](#available-cicd-variables), [覆盖](#overriding-the-container-scanning-template), [offline environment support](#running-container-scanning-in-an-offline-environment), etc)--> | 是 | 是 |
| [查看作为 CI 作业产物的 JSON 报告](#reports-json-format) | 是 | 是 |
| 生成[依赖项](#dependency-list)的 JSON 报告 | 是 | 是 |
| 能够通过 UI 中的 MR 启用容器扫描 | 是 | 是 |
| [UBI 镜像支持](#fips-enabled-images) | 是 | 是 |
| 支持 Trivy | 是 | 是 |
| 支持 Grype | 是 | 是 |
| 在 CI 流水线作业的合并请求和安全选项卡中展示报告数据 | 否 | 是 |
| [与漏洞的交互](#interacting-with-the-vulnerabilities)，例如合并请求批准 | 否 | 是 |
| [漏洞解决方案（自动修复）](#solutions-for-vulnerabilities-auto-remediation) | 否 | 是 |
| 支持[漏洞允许列表](#vulnerability-allowlisting) | 否 | 是 |
| [访问安全仪表盘页面](#security-dashboard) | 否 | 是 |
| [访问依赖项列表页面](../dependency_list/index.md) | 否 | 是 |

<!--
| Inclusion of GitLab Advisory Database | Limited to the time-delayed content from GitLab [advisories-communities](https://gitlab.com/gitlab-org/advisories-community/) project | 是 - all the latest content from [Gemnasium DB](https://gitlab.com/gitlab-org/security-products/gemnasium-db) |
-->

<a id="prerequisites"></a>

## 先决条件

要在流水线中启用容器扫描，您需要满足以下条件：

- 极狐GitLab CI/CD 流水线必须包含 `test` 阶段，除非被 [`stages`](../../../ci/yaml/index.md#stages) 关键字覆盖，否则该阶段可用。
- 极狐GitLab Runner 与 `docker` 或在 Linux/amd64 上的 `kubernetes` 执行器。
- 在私有化部署版上，Docker `18.09.03` 或更高版本与 runner 安装在同一台计算机上。
- 与[支持的发行版](#supported-distributions)匹配的镜像。
- [构建并推送](../../packages/container_registry/build_and_push_images.md#use-gitlab-cicd) Docker 镜像到您项目的容器镜像库。
- 如果您使用第三方容器镜像库，您可能需要通过 `CS_REGISTRY_USER` 和 `CS_REGISTRY_PASSWORD` [配置变量](#available-cicd-variables)提供身份验证凭据。有关如何使用这些变量的更多详细信息，请参阅[向远端镜像库进行身份验证](#authenticate-to-a-remote-registry)。

<a id="configuration"></a>

## 配置

要启用容器扫描，请添加 [`Container-Scanning.gitlab-ci.yml` 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Container-Scanning.gitlab-ci.yml) 到 `.gitlab-ci.yml` 文件：

```yaml
include:
  - template: Jobs/Container-Scanning.gitlab-ci.yml
```

包含的模板：

- 在 CI/CD 流水线中创建一个 `container_scanning` 作业。
- 从项目的[容器镜像库](../../packages/container_registry/index.md)中提取构建的 Docker 镜像（请参阅[先决条件](#prerequisites)）并扫描它，查找可能的漏洞。

系统将结果保存为 Container Scanning 报告产物，您可以稍后下载和分析。下载时，您始终会收到最新的产物。如果[启用依赖扫描](#dependency-list)，还会创建依赖扫描报告产物。

以下是构建 Docker 镜像、将其推送到容器镜像库并扫描镜像的示例 `.gitlab-ci.yml`：

```yaml
include:
  - template: Jobs/Build.gitlab-ci.yml
  - template: Jobs/Container-Scanning.gitlab-ci.yml

container_scanning:
  variables:
    CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
```

设置 `CS_DEFAULT_BRANCH_IMAGE` 可以避免在镜像名称跨分支不同时重复发现漏洞。
`CS_DEFAULT_BRANCH_IMAGE` 的值表示扫描镜像的名称，它出现在默认分支上。
有关如何实现此重复数据删除的更多详细信息，请参阅[设置默认分支镜像](#setting-the-default-branch-image)。

### 自定义容器扫描设置

在某些情况下，您可能想要自定义极狐GitLab 扫描容器的方式。例如，您可能希望启用更详细的输出、访问需要身份验证的 Docker 镜像库等等。要更改此类设置，请使用 `.gitlab-ci.yml` 中的 [`variables`](../../../ci/yaml/index.md#variables) 参数来设置 [CI/CD 变量](#available-cicd-variables)。
您在 `.gitlab-ci.yml` 中设置的变量会覆盖 `Container-Scanning.gitlab-ci.yml`。

此示例[包含](../../../ci/yaml/index.md#include)容器扫描模板并为分析器启用详细输出：

```yaml
include:
  - template: Jobs/Container-Scanning.gitlab-ci.yml

variables:
    SECURE_LOG_LEVEL: 'debug'
```

#### 扫描远端镜像库中的镜像

要扫描位于项目以外的镜像库中的镜像，请使用以下 `.gitlab-ci.yml`：

```yaml
include:
  - template: Jobs/Container-Scanning.gitlab-ci.yml

container_scanning:
  variables:
    CS_IMAGE: example.com/user/image:tag
```

<a id="authenticate-to-a-remote-registry"></a>

##### 向远端镜像库进行身份验证

扫描私有镜像库中的镜像需要身份验证。在 `CS_REGISTRY_USER` 变量中提供用户名，在 `CS_REGISTRY_PASSWORD` 配置变量中提供密码。

例如，要从 AWS Elastic Container Registry 扫描镜像：

```yaml
container_scanning:
  before_script:
    - ruby -r open-uri -e "IO.copy_stream(URI.open('https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip'), 'awscliv2.zip')"
    - unzip awscliv2.zip
    - sudo ./aws/install
    - aws --version
    - export AWS_ECR_PASSWORD=$(aws ecr get-login-password --region region)

include:
  - template: Security/Container-Scanning.gitlab-ci.yml

variables:
    CS_IMAGE: <aws_account_id>.dkr.ecr.<region>.amazonaws.com/<image>:<tag>
    CS_REGISTRY_USER: AWS
    CS_REGISTRY_PASSWORD: "$AWS_ECR_PASSWORD"
```

启用 FIPS 模式时，不支持对远程镜像库进行身份验证。

<a id="dependency-list"></a>

#### 依赖列表

> 引入于 14.6 版本。

`CS_DISABLE_DEPENDENCY_LIST` CI/CD 变量控制扫描是否创建依赖列表报告。当前仅使用 `trivy` 分析器时才支持此变量。变量的默认设置 `"false"` 使扫描创建报告。要禁用报告，请将变量设置为 `"true"`：

例如：

```yaml
include:
  - template: Jobs/Container-Scanning.gitlab-ci.yml

container_scanning:
  variables:
    CS_DISABLE_DEPENDENCY_LIST: "true"
```

#### 报告特定语言的 findings

> 引入于 14.6 版本。

`CS_DISABLE_LANGUAGE_VULNERABILITY_SCAN` CI/CD 变量控制扫描是否报告与编程语言相关的 findings。支持的语言取决于[使用的扫描器](#change-scanners)：

- [Trivy](https://aquasecurity.github.io/trivy/latest/docs/vulnerability/detection/language/).
- [Grype](https://github.com/anchore/grype#features).

默认情况下，报告仅包括由操作系统 (OS) 包管理器管理的包（例如，`yum`、`apt`、`apk`、`tdnf`）。要报告非操作系统包中的安全 findings，请将 `CS_DISABLE_LANGUAGE_VULNERABILITY_SCAN` 设置为 `"false"`：

```yaml
include:
  - template: Jobs/Container-Scanning.gitlab-ci.yml

container_scanning:
  variables:
    CS_DISABLE_LANGUAGE_VULNERABILITY_SCAN: "false"
```

当您启用此功能时，如果为您的项目启用了依赖扫描，您可能会在漏洞报告中看到重复的 findings。发生这种情况是因为夏天无法跨不同类型的扫描工具自动删除重复数据。请参考依赖扫描和容器扫描之间的[比较](../dependency_scanning/index.md#dependency-scanning-compared-to-container-scanning)，获取有关哪些类型的依赖项可能重复的详细信息。

<a id="available-cicd-variables"></a>

#### 可用的 CI/CD 变量

您可以使用以下 CI/CD 变量[配置](#customizing-the-container-scanning-settings)分析器：

| CI/CD 变量                 | 默认值       | 描述 | 扫描器 |
| ------------------------------ | ------------- | ----------- | ------------ |
| `ADDITIONAL_CA_CERT_BUNDLE`    | `""`          | 您想要信任的 CA 证书捆绑包。查看[使用自定义 SSL CA 证书颁发机构](#using-a-custom-ssl-ca-certificate-authority)获取详情。 | All |
| `CI_APPLICATION_REPOSITORY`    | `$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG` | 要扫描的镜像的 Docker 仓库 URL。 | All |
| `CI_APPLICATION_TAG`           | `$CI_COMMIT_SHA` | 要扫描的镜像的 Docker 仓库标记。 | All |
| `CS_ANALYZER_IMAGE`            | `registry.gitlab.cn/security-products/container-scanning:6` | 分析器的 Docker 镜像。 | All |
| `CS_DEFAULT_BRANCH_IMAGE`      | `""` | 默认分支上的 `CS_IMAGE` 的名称。查看[设置默认分支镜像](#setting-the-default-branch-image)获取详情。引入于 14.5 版本。 | All |
| `CS_DISABLE_DEPENDENCY_LIST`   | `"false"`      | 禁用扫描镜像中安装的软件包的依赖扫描。引入于 14.6 版本。 | All |
| `CS_DISABLE_LANGUAGE_VULNERABILITY_SCAN` | `"true"` | 禁用扫描安装在扫描镜像中的特定语言包。引入于 14.6 版本。 | All |
| `CS_DOCKER_INSECURE`           | `"false"`     | 允许在不验证证书的情况下使用 HTTPS 访问安全的 Docker 镜像库。 | All |
| `CS_IMAGE_SUFFIX`              | `""`          | 后缀添加到 `CS_ANALYZER_IMAGE`。如果设置为 `-fips`，则使用 `FIPS-enabled` 镜像进行扫描。有关详细信息，请参阅[启用 FIPS 的镜像](#fips-enabled-images)。引入于 14.10 版本。 | All |
| `CS_IGNORE_UNFIXED`            | `"false"`     | 忽略未修复的漏洞。 | All |
| `CS_REGISTRY_INSECURE`         | `"false"`     | 允许访问不安全的镜像库（仅限 HTTP）。仅应在本地测试镜像时设置为 `true`。适用于所有扫描器，但镜像库必须侦听端口 80/tcp 才能使 Trivy 工作。 | All |
| `CS_SEVERITY_THRESHOLD`        | `UNKNOWN`     | 严重级别阈值。扫描器输出严重级别高于或等于此阈值的漏洞。支持的级别为 `UNKNOWN`、`LOW`、`MEDIUM`、`HIGH` 和 `CRITICAL`。 | Trivy |
|  `DOCKER_IMAGE`                 | `$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG` | **已废弃** 将在 16.0 版本中删除。替换为 `CS_IMAGE`。要扫描的 Docker 镜像。 如果设置，此变量将覆盖 `$CI_APPLICATION_REPOSITORY` 和 `$CI_APPLICATION_TAG` 变量。 | All |
| `CS_IMAGE`                 | `$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG` | 要扫描的 Docker 镜像。如果设置，此变量将覆盖 `$CI_APPLICATION_REPOSITORY` 和 `$CI_APPLICATION_TAG` 变量。 | All |
| `CS_REGISTRY_PASSWORD`              | `$CI_REGISTRY_PASSWORD` | 用于访问需要身份验证的 Docker 镜像库的密码。仅当 `$CS_IMAGE` 位于 [`$CI_REGISTRY`](../../../ci/variables/predefined_variables.md) 时才设置默认值。启用 FIPS 模式时不支持。 | All |
| `CS_REGISTRY_USER`                  | `$CI_REGISTRY_USER` | 用于访问需要身份验证的 Docker 镜像库的用户名。仅当 `$CS_IMAGE` 位于 [`$CI_REGISTRY`](../../../ci/variables/predefined_variables.md) 时才设置默认值。启用 FIPS 模式时不支持。 | All |
| `CS_DOCKERFILE_PATH`              | `Dockerfile`  | 用于生成修复的 `Dockerfile` 的路径。默认情况下，扫描器会在项目的根目录中查找名为“Dockerfile”的文件。仅当 `Dockerfile` 位于非标准位置（例如子目录）时，才应配置此变量。有关详细信息，请参阅[漏洞解决方案](#solutions-for-vulnerabilities-auto-remediation)。 | All |
| `SECURE_LOG_LEVEL`             | `info`        | 设置最低日志记录级别。输出此日志级别或更高级别的消息。从最高到最低严重性，日志记录级别是：`fatal`、`error`、`warn`、`info`、`debug`。引入于 13.1 版本。 | All |

<a id="supported-distributions"></a>

### 支持的发行版

支持取决于扫描器：

| 发行版   | Grype | Trivy |
| -------------- | ----- | ----- |
| Alma Linux     |       |  ✅   |
| CentOS         |  ✅   |  ✅   |
| Debian         |  ✅   |  ✅   |
| Oracle Linux   |  ✅   |  ✅   |
| Red Hat (RHEL) |  ✅   |  ✅   |
| Ubuntu         |  ✅   |  ✅   |

<a id="fips-enabled-images"></a>

#### 启用 FIPS 的镜像

> 引入于 14.1 版本。

提供 [FIPS-enabled Red Hat UBI](https://www.redhat.com/en/blog/introducing-red-hat-universal-base-image) 版本的容器扫描镜像。因此，您可以使用基于 UBI 的镜像替换标准镜像。要配置镜像，将 `CS_IMAGE_SUFFIX` 设置为 `-fips` 或修改 `CS_ANALYZER_IMAGE` 变量为标准标签加上 `-fips` 扩展名。

| 扫描器名称    | `CS_ANALYZER_IMAGE` |
| --------------- | ------------------- |
| 默认 (Trivy) | `registry.gitlab.cn/security-products/container-scanning:6-fips` |
| Grype           | `registry.gitlab.cn/security-products/container-scanning/grype:6-fips` |
| Trivy           | `registry.gitlab.cn/security-products/container-scanning/trivy:6-fips` |

NOTE:
在 15.0 版本之前，还可以使用 `-ubi` 镜像扩展名。15.0 及更高版本仅支持 `-fips`。

从 14.10 版本开始，当在极狐GitLab 实例中启用 FIPS 模式时，`-fips` 会自动添加到 `CS_ANALYZER_IMAGE`。

启用 FIPS 模式时，不支持对经过身份验证的镜像库中的镜像进行容器扫描。当 `CI_GITLAB_FIPS_MODE` 为 `"true"`，并且设置了 `CS_REGISTRY_USER` 或 `CS_REGISTRY_PASSWORD` 时，分析器会出错退出并且不执行扫描。

<a id="enable-container-scanning-through-an-automatic-merge-request"></a>

### 启用自动合并请求上的容器扫描

> 引入于 14.9 版本。

要在项目中启用容器扫描，请从“安全配置”页面创建合并请求：

1. 在要启用容器扫描的项目中，转到 **安全与合规 > 配置**。
1. 在 **容器扫描** 行中，选择 **使用合并请求进行配置**。

将自动创建一个合并请求，其中包含启用容器扫描所需的更改。
要完成配置，请查看并合并此合并请求。

配置工具在无现有 `.gitlab-ci.yml` 文件或最小配置文件的情况下效果最佳。如果您有一个复杂的 GitLab 配置文件，可能解析不成功，可能会报错。

<a id="overriding-the-container-scanning-template"></a>

### 覆盖容器扫描模板

如果您想覆盖作业定义（例如，更改诸如 `variables` 之类的属性），则必须在模板包含后，声明并覆盖作业，然后指定任何其它键。

此示例将 `GIT_STRATEGY` 设置为 `fetch`：

```yaml
include:
  - template: Jobs/Container-Scanning.gitlab-ci.yml

container_scanning:
  variables:
    GIT_STRATEGY: fetch
```

<a id="change-scanners"></a>

### 更换扫描器

容器扫描分析器可以使用不同的扫描器，具体取决于 `CS_ANALYZER_IMAGE` 配置变量的值。

可以使用以下选项：

| 扫描器名称 | `CS_ANALYZER_IMAGE` |
| ------------ | ------------------- |
| 默认 ([Trivy](https://github.com/aquasecurity/trivy)) | `registry.gitlab.cn/security-products/container-scanning:6` |
| [Grype](https://github.com/anchore/grype)                | `registry.gitlab.cn/security-products/container-scanning/grype:6` |
| Trivy                                                    | `registry.gitlab.cn/security-products/container-scanning/trivy:6` |

<a id="setting-the-default-branch-image"></a>

### 设置默认分支镜像

> 引入于 14.5 版本。

默认情况下，容器扫描假定镜像命名约定将任何特定于分支的标识符存储在镜像标签中，而不是镜像名称中。当默认分支和非默认分支之间的镜像名称不同时，先前检测到的漏洞会在合并请求中显示为新检测到的漏洞。

当同一镜像在默认分支和非默认分支上具有不同的名称时，您可以使用 `CS_DEFAULT_BRANCH_IMAGE` 变量来指示该镜像在默认分支上的名称。
然后系统在非默认分支上运行扫描时可以正确确定是否存在漏洞。

例如，假设以下情况：

- 非默认分支使用命名约定 `$CI_REGISTRY_IMAGE/$CI_COMMIT_BRANCH:$CI_COMMIT_SHA` 发布镜像。
- 默认分支使用命名约定 `$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA` 发布镜像。

在此示例中，您可以使用以下 CI/CD 配置来确保不重复漏洞：

```yaml
include:
  - template: Jobs/Container-Scanning.gitlab-ci.yml

container_scanning:
  variables:
    CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  before_script:
    - export CS_IMAGE="$CI_REGISTRY_IMAGE/$CI_COMMIT_BRANCH:$CI_COMMIT_SHA"
    - |
      if [ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]; then
        export CS_IMAGE="$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA"
      fi
```

对于给定的 `CS_IMAGE`，`CS_DEFAULT_BRANCH_IMAGE` 应该保持不变。如果它发生变化，则会创建一组重复的漏洞，必须手动消除这些漏洞。

使用 [Auto DevOps](../../../topics/autodevops/index.md) 时，`CS_DEFAULT_BRANCH_IMAGE` 会自动设置为 `$CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_APPLICATION_TAG`。

<a id="using-a-custom-ssl-ca-certificate-authority"></a>

### 使用自定义 SSL CA 证书颁发机构

您可以使用 `ADDITIONAL_CA_CERT_BUNDLE` CI/CD 变量来配置自定义 SSL CA 证书颁发机构，用于在从使用 HTTPS 的镜像库获取 Docker 镜像时验证对等方。`ADDITIONAL_CA_CERT_BUNDLE` 值应包含 [X.509 PEM 公钥证书的文本表示形式](https://www.rfc-editor.org/rfc/rfc7468#section-5.1)。例如，要在 `.gitlab-ci.yml` 文件中配置此值，请使用以下命令：

```yaml
container_scanning:
  variables:
    ADDITIONAL_CA_CERT_BUNDLE: |
        -----BEGIN CERTIFICATE-----
        MIIGqTCCBJGgAwIBAgIQI7AVxxVwg2kch4d56XNdDjANBgkqhkiG9w0BAQsFADCB
        ...
        jWgmPqF3vUbZE0EyScetPJquRFRKIesyJuBFMAs=
        -----END CERTIFICATE-----
```

`ADDITIONAL_CA_CERT_BUNDLE` 值也可以配置为 [UI 中的自定义变量](../../../ci/variables/index.md#for-a-project)，或者配置为 `file`， 需要证书的路径，或者作为变量（需要证书的文本表示）。

<a id="vulnerability-allowlisting"></a>

### 漏洞许可名单  **(ULTIMATE)**

要将特定漏洞列入白名单，请执行以下步骤：

1. 按照 [覆盖容器扫描模板](#overriding-the-container-scanning-template) 中的说明，在 `.gitlab-ci.yml` 文件中设置 `GIT_STRATEGY: fetch`。
1. 在名为 `vulnerability-allowlist.yml` 的 YAML 文件中定义列入许可名单的漏洞。必须使用 [`vulnerability-allowlist.yml` 数据格式](#vulnerability-allowlistyml-data-format) 中描述的格式。
1. 将 `vulnerability-allowlist.yml` 文件添加到项目 Git 仓库的根文件夹中。

<a id="vulnerability-allowlistyml-data-format"></a>

#### `vulnerability-allowlist.yml` 数据格式

`vulnerability-allowlist.yml` 文件是一个 YAML 文件，它指定了**允许**存在的漏洞的 CVE ID 列表，主要原因包括 *误报* 或 *不适用*。

如果在 `vulnerability-allowlist.yml` 文件中找到匹配条目，则会发生以下情况：

- 当分析器生成 `gl-container-scanning-report.json` 文件时，漏洞**不包括**在内。
- 流水线的安全选项卡**不显示**漏洞。它不包含在 JSON 文件中，即安全选项卡内容的真实来源。

示例 `vulnerability-allowlist.yml` 文件：

```yaml
generalallowlist:
  CVE-2019-8696:
  CVE-2014-8166: cups
  CVE-2017-18248:
images:
  registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:
    CVE-2018-4180:
  your.private.registry:5000/centos:
    CVE-2015-1419: libxml2
    CVE-2015-1447:
```

此示例从 `gl-container-scanning-report.json` 中排除以下漏洞：

1. 所有具有 CVE ID 的漏洞：*CVE-2019-8696*、*CVE-2014-8166*、*CVE-2017-18248*。
1. 在  `registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256` 容器镜像中发现的所有 CVE ID 为 *CVE-2018-4180* 的漏洞。
1. 在 `your.private.registry:5000/centos` 容器中发现的所有 CVE ID 为 *CVE-2015-1419*、*CVE-2015-1447* 的漏洞。

##### 文件格式

- `generalallowlist` 块允许您在全局范围内指定 CVE ID。具有匹配 CVE ID 的所有漏洞都将从扫描报告中排除。

- `images` 块允许您为每个容器镜像独立指定 CVE ID。给定镜像中具有匹配 CVE ID 的所有漏洞都将从扫描报告中排除。镜像名称是从用于指定要扫描的 Docker 镜像的环境变量之一检索的，例如 `$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG` 或 `CS_IMAGE`。此块中提供的镜像**必须**匹配此值并且**不得**包含标签值。例如，如果您使用 `CS_IMAGE=alpine:3.7` 指定要扫描的镜像，则可以在 `images` 块中使用 `alpine`，但不能使用 `alpine:3.7`。

  您可以通过多种方式指定容器镜像：

  - 仅镜像名称（例如 `centos`）。
  - 带有主机名的完整镜像名称（例如 `your.private.registry:5000/centos`）。
  - 带有镜像库主机名和 sha256 标签的完整镜像名称（例如 `registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256`）。

NOTE:
CVE ID 后面的字符串（上例中的`cups` 和`libxml2`）是可选的注释格式，对漏洞的处理**没有影响**。您可以包含注释来描述漏洞。

##### 容器扫描作业日志格式

您可以通过查看容器扫描分析器在 `container_scanning` 作业详细信息中生成的日志来验证扫描结果和 `vulnerability-allowlist.yml` 文件的正确性。

该日志包含以表格形式列出的已发现漏洞列表，例如：

```plaintext
+------------+-------------------------+------------------------+-----------------------+------------------------------------------------------------------------+
|   STATUS   |      CVE SEVERITY       |      PACKAGE NAME      |    PACKAGE VERSION    |                            CVE DESCRIPTION                             |
+------------+-------------------------+------------------------+-----------------------+------------------------------------------------------------------------+
|  Approved  |   High CVE-2019-3462    |          apt           |         1.4.8         | Incorrect sanitation of the 302 redirect field in HTTP transport metho |
|            |                         |                        |                       | d of apt versions 1.4.8 and earlier can lead to content injection by a |
|            |                         |                        |                       |  MITM attacker, potentially leading to remote code execution on the ta |
|            |                         |                        |                       |                             rget machine.                              |
+------------+-------------------------+------------------------+-----------------------+------------------------------------------------------------------------+
| Unapproved |  Medium CVE-2020-27350  |          apt           |         1.4.8         | APT had several integer overflows and underflows while parsing .deb pa |
|            |                         |                        |                       | ckages, aka GHSL-2020-168 GHSL-2020-169, in files apt-pkg/contrib/extr |
|            |                         |                        |                       | acttar.cc, apt-pkg/deb/debfile.cc, and apt-pkg/contrib/arfile.cc. This |
|            |                         |                        |                       |  issue affects: apt 1.2.32ubuntu0 versions prior to 1.2.32ubuntu0.2; 1 |
|            |                         |                        |                       | .6.12ubuntu0 versions prior to 1.6.12ubuntu0.2; 2.0.2ubuntu0 versions  |
|            |                         |                        |                       | prior to 2.0.2ubuntu0.2; 2.1.10ubuntu0 versions prior to 2.1.10ubuntu0 |
|            |                         |                        |                       |                                  .1;                                   |
+------------+-------------------------+------------------------+-----------------------+------------------------------------------------------------------------+
| Unapproved |  Medium CVE-2020-3810   |          apt           |         1.4.8         | Missing input validation in the ar/tar implementations of APT before v |
|            |                         |                        |                       | ersion 2.1.2 could result in denial of service when processing special |
|            |                         |                        |                       |                         ly crafted deb files.                          |
+------------+-------------------------+------------------------+-----------------------+------------------------------------------------------------------------+
```

将对应的 CVE ID 添加到 `vulnerability-allowlist.yml` 文件时，日志中的漏洞会被标记为 `Approved`。

### 在离线环境中运行容器扫描

对于通过 Internet 对外部资源进行有限、受限或间歇性访问的环境中的自我管理 GitLab 实例，需要进行一些调整才能使容器扫描作业成功运行。<!--For more information, see [Offline environments](../offline_deployments/index.md).-->

#### 离线容器扫描的要求

要在离线环境中使用容器扫描，您需要：

- 带有 [`docker` 或 `kubernetes` 执行器](#prerequisites) 的极狐GitLab Runner。
- 使用容器扫描镜像的副本配置本地 Docker 容器镜像库。您可以在它们各自的镜像库中找到这些镜像：

| GitLab Analyzer | Container Registry |
| --- | --- |
| [Container-Scanning](https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning) | [Container-Scanning container registry](https://gitlab.com/security-products/container-scanning/container_registry/) |

请注意，GitLab Runner 的默认 `pull policy` 为 `always`，这意味着即使本地副本可用，runner 也会尝试从 GitLab 容器镜像库中提取 Docker 镜像。如果您更喜欢仅使用本地可用的 Docker 镜像，则可以在离线环境中将 GitLab Runner `pull_policy` 设置为 `if-not-present`。但是，如果不在离线环境中，我们建议将拉取策略设置保持为 `always`，因为这样可以在 CI/CD 流水线中使用更新的扫描程序。

##### 支持自定义证书颁发机构

以下版本中引入了对自定义证书颁发机构的支持：

| 扫描器 | 版本 |
| -------- | ------- |
| `Trivy` | 4.0.0 |
| `Grype` | 4.3.0 |

#### 在 Docker 镜像库中提供极狐GitLab 容器扫描分析器镜像

对于容器扫描，将以下镜像从 `registry.gitlab.cn` 导入您的[本地 Docker 容器镜像库](../../packages/container_registry/index.md)：

```plaintext
registry.gitlab.cn/security-products/container-scanning:6
registry.gitlab.cn/security-products/container-scanning/grype:6
registry.gitlab.cn/security-products/container-scanning/trivy:6
```

将 Docker 镜像导入本地离线 Docker 镜像库的过程取决于**您的网络安全策略**。请咨询您的 IT 人员，找到一个接受和批准的流程，您可以通过该流程导入或临时访问外部资源。这些扫描程序定期更新，您可以自己进行不定期更新。

<!--
For more information, see [the specific steps on how to update an image with a pipeline](#automating-container-scanning-vulnerability-database-updates-with-a-pipeline).
-->

有关将 Docker 镜像作为文件保存和传输的详细信息，请参阅 Docker 关于 [`docker save`](https://docs.docker.com/engine/reference/commandline/save/) 的文档，[`docker load`]( https://docs.docker.com/engine/reference/commandline/load/)、[`docker export`](https://docs.docker.com/engine/reference/commandline/export/) 和 [`docker import`](https://docs.docker.com/engine/reference/commandline/import/)。

#### 设置容器扫描 CI/CD 变量来使用本地容器扫描分析器

1. [覆盖容器扫描模板](#overriding-the-container-scanning-template) 在您的 `.gitlab-ci.yml` 文件中引用托管在本地 Docker 容器镜像库上的 Docker 镜像：

   ```yaml
   include:
     - template: Jobs/Container-Scanning.gitlab-ci.yml

   container_scanning:
     image: $CI_REGISTRY/namespace/gitlab-container-scanning
   ```

1. 如果您的本地 Docker 容器镜像库通过 `HTTPS` 安全运行，但您使用的是自签名证书，则必须在 `.gitlab-ci.yml` 的上述 `container_scanning` 部分中设置 `CS_DOCKER_INSECURE: "true"`。

#### 使用流水线自动进行容器扫描漏洞数据库更新

我们建议您设置一个[计划流水线](../../../ci/pipelines/schedules.md)，按照预设的计划获取最新的漏洞数据库。
使用流水线自动执行此操作意味着您不必每次都手动执行。您可以使用以下 `.gitlab-ci.yml` 示例作为模板。

```yaml
variables:
  SOURCE_IMAGE: registry.gitlab.cn/security-products/container-scanning:6
  TARGET_IMAGE: $CI_REGISTRY/namespace/gitlab-container-scanning

image: docker:stable

update-scanner-image:
  services:
    - docker:dind
  script:
    - docker pull $SOURCE_IMAGE
    - docker tag $SOURCE_IMAGE $TARGET_IMAGE
    - echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY --username $CI_REGISTRY_USER --password-stdin
    - docker push $TARGET_IMAGE
```

上述模板适用于在本地安装上运行的 GitLab Docker 镜像库。但是，如果您使用的是非 GitLab Docker 镜像库，则必须更改 `$CI_REGISTRY` 值和 `docker login` 凭据来匹配本地镜像库的详细信息。

#### 扫描外部私有镜像库中的镜像

要扫描外部私有镜像库中的镜像，您必须配置访问凭据，以便容器扫描分析器可以在尝试访问要扫描的镜像之前对其进行身份验证。

如果您使用极狐GitLab [Container Registry](../../packages/container_registry/)，`CS_REGISTRY_USER` 和 `CS_REGISTRY_PASSWORD` [配置变量](#available-cicd-variables)会自动设置，您可以跳过这个配置。

此示例显示了在私有 [Google Container Registry](https://cloud.google.com/container-registry/) 中扫描镜像所需的配置：

```yaml
include:
  - template: Jobs/Container-Scanning.gitlab-ci.yml

container_scanning:
  variables:
    CS_REGISTRY_USER: _json_key
    CS_REGISTRY_PASSWORD: "$GCP_CREDENTIALS"
    CS_IMAGE: "gcr.io/path-to-you-registry/image:tag"
```

在提交此配置之前，为包含 JSON 的 `GCP_CREDENTIALS` [添加 CI/CD 变量](../../../ci/variables/index.md#for-a-project)密钥。
同时：

- 变量的值可能不符合 **隐藏变量** 选项的隐藏要求，因此该值可能会暴露在作业日志中。
- 如果您选择 **保护变量** 选项，扫描可能不会在不受保护的功能分支中运行。
- 如果未选择这些选项，请考虑创建具有只读权限的凭据并定期轮换它们。

启用 FIPS 模式时，不支持扫描外部私有镜像库中的镜像。

## 运行独立的容器扫描工具

可以针对 Docker 容器运行极狐GitLab 容器扫描工具，而无需在 CI 作业的上下文中运行它。要直接扫描镜像，请执行以下步骤：

1. 运行 [Docker Desktop](https://www.docker.com/products/docker-desktop/) 或 [Docker Machine](https://github.com/docker/machine)。

1. 运行分析器的 Docker 镜像，在 `CI_APPLICATION_REPOSITORY` 和 `CI_APPLICATION_TAG` 变量中传递要分析的镜像和标签：

   ```shell
   docker run \
     --interactive --rm \
     --volume "$PWD":/tmp/app \
     -e CI_PROJECT_DIR=/tmp/app \
     -e CI_APPLICATION_REPOSITORY=registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256 \
     -e CI_APPLICATION_TAG=bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e \
     registry.gitlab.cn/security-products/container-scanning
   ```

结果存储在 `gl-container-scanning-report.json` 中。

<a id="reports-json-format"></a>

## 报告 JSON 格式

容器扫描工具发出 JSON 报告，[极狐GitLab Runner](https://docs.gitlab.cn/runner/) 通过 [`artifacts:reports`](../../../ci/yaml/index.md#artifactsreports) CI 配置文件中的关键字识别该报告。

CI 作业完成后，Runner 会将这些报告上传到极狐GitLab，然后在 CI 作业产物中可用。在旗舰版中，这些报告可以在相应的[流水线](../vulnerability_report/pipeline.md)中查看，并成为[漏洞报告](../vulnerability_report/index.md)的一部分。

<!--
These reports must follow a format defined in the
[security report schemas](https://gitlab.com/gitlab-org/security-products/security-report-schemas/). See:

- [Latest schema for the container scanning report](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/container-scanning-report-format.json).
- [Example container scanning report](https://gitlab.com/gitlab-examples/security/security-reports/-/blob/master/samples/container-scanning.json)
-->

### CycloneDX 软件物料清单

> 引入于 15.11 版本。

除了 [JSON 报告文件](#reports-json-format)之外，容器扫描工具还为扫描镜像输出 [CycloneDX](https://cyclonedx.org/) 软件物料清单 (SBOM)。此 CycloneDX SBOM 名为 `gl-sbom-report.cdx.json`，并保存在与 JSON 报告文件相同的目录中。只有在使用 Trivy 分析器时才支持此功能。

您可以[使用与其他作业产物相同的方式](../../../ci/jobs/job_artifacts.md#download-job-artifacts)下载 CycloneDX SBOM。

<a id="security-dashboard"></a>

## 安全仪表盘

[安全仪表盘](../security_dashboard/index.md)向您展示了您的群组、项目和流水线中的所有安全漏洞的概览。

<!--
## Vulnerabilities database update

All analyzer images are [updated daily](https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning/-/blob/master/README.md#image-updates).

The images use data from upstream advisory databases depending on which scanner is used:

| 数据源                    | Trivy | Grype |
| ------------------------------ | ----- | ----- |
| AlmaLinux Security Advisory    |  ✅   | ✅    |
| Amazon Linux Security Center   |  ✅   | ✅    |
| Arch Linux Security Tracker    |  ✅   |       |
| SUSE CVRF                      |  ✅   | ✅    |
| CWE Advisories                 |  ✅   |       |
| Debian Security Bug Tracker    |  ✅   | ✅    |
| GitHub Security Advisory       |  ✅   | ✅    |
| Go Vulnerability Database      |  ✅   |       |
| CBL-Mariner Vulnerability Data |  ✅   |       |
| NVD                            |  ✅   | ✅    |
| OSV                            |  ✅   |       |
| Red Hat OVAL v2                |  ✅   | ✅    |
| Red Hat Security Data API      |  ✅   | ✅    |
| Photon Security Advisories     |  ✅   |       |
| Rocky Linux UpdateInfo         |  ✅   |       |
| Ubuntu CVE Tracker (only data sources from mid 2021 and later) |  ✅   | ✅    |

In addition to the sources provided by these scanners, GitLab maintains the following vulnerability databases:

- The proprietary
[GitLab Advisory Database](https://gitlab.com/gitlab-org/security-products/gemnasium-db).
- The open source [GitLab Advisory Database (Open Source Edition)](https://gitlab.com/gitlab-org/advisories-community).

In the GitLab Ultimate tier, the data from the [GitLab Advisory Database](https://gitlab.com/gitlab-org/security-products/gemnasium-db) is merged in to augment the data from the external sources. In the GitLab Premium and Free tiers, the data from the [GitLab Advisory Database (Open Source Edition)](https://gitlab.com/gitlab-org/advisories-community) is merged in to augment the data from the external sources. This augmentation currently only applies to the analyzer images for the Trivy scanner.

Database update information for other analyzers is available in the
[maintenance table](../index.md#vulnerability-scanner-maintenance).
-->

<a id="change-vulnerability-status"></a>

## 与漏洞交互

发现漏洞后，您可以[解决它](../vulnerabilities/index.md)。

<a id="solutions-for-vulnerabilities-auto-remediation"></a>

## 漏洞解决方案（自动修复） **(ULTIMATE)**

一些漏洞可以通过应用极狐GitLab 自动生成的解决方案来修复。

要启用补救支持，扫描工具必须有权访问由 [`CS_DOCKERFILE_PATH`](#available-cicd-variables) CI/CD 变量指定的 `Dockerfile`。

为确保扫描工具可以访问此文件，有必要按照本文档覆盖容器扫描模板部分中描述的说明在您的 `.gitlab-ci.yml` 文件中设置 `GIT_STRATEGY: fetch`。

## 故障排查

### `docker: Error response from daemon: failed to copy xattrs`

当 runner 使用 `docker` 执行器并使用 NFS 时（例如，`/var/lib/docker` 在 NFS 挂载上），容器扫描可能会失败，并出现如下错误：

```plaintext
docker: Error response from daemon: failed to copy xattrs: failed to set xattr "security.selinux" on /path/to/file: operation not supported.
```

这是 Docker 中的一个错误的结果，现在 [已修复](https://github.com/containerd/continuity/pull/138 "fs: add WithAllowXAttrErrors CopyOpt")。为防止出现错误，请确保 runner 使用的 Docker 版本为 18.09.03 或更高版本。


### 收到警告消息 `gl-container-scanning-report.json: no matching files`

要获取更多信息，查看[通用应用安全故障排除部分](../../../ci/jobs/job_artifacts_troubleshooting.md#error-message-no-files-to-upload)。

## 变更

您可以在项目的 [changelog](https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning/-/blob/master/CHANGELOG.md) 中找到对容器扫描分析器的更改。
