---
stage: Secure
group: Dynamic Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# DAST 基于代理的分析器故障排除 **(ULTIMATE)**

从客户支持案例中收集了以下故障排除方案。如果您遇到此处未解决的问题，或者此处的信息无法解决您的问题，请创建技术支持工单。有关更多详细信息，请参阅[技术支持](https://gitlab.cn/support/)页面。

## 调试 DAST 作业

DAST 作业有两个执行进程：

- ZAP 服务器。
- 一系列启动、控制和停止 ZAP 服务器的脚本。

启用 `DAST_DEBUG` CI/CD 变量可以调试脚本，有助于对作业进行故障排除，并输出指示扫描完成百分比的语句。有关使用变量的详细信息，请参阅[覆盖 DAST 模板](proxy-based.md#customize-dast-settings)。

可以使用 `DAST_ZAP_LOG_CONFIGURATION` 变量启用 ZAP 服务器的调试模式。下表概述了可以设置的值的示例以及它们对记录的输出的影响。可以指定多个值，用分号分隔。

例如，`log4j.logger.org.parosproxy.paros.network.HttpSender=DEBUG;log4j.logger.com.crawljax=DEBUG`。

| 日志配置值                                      | 影响                                                            |
|--------------------------------------------------            | ----------------------------------------------------------------- |
| `log4j.rootLogger=DEBUG`                                     | 启用所有调试日志记录语句。                              |
| `log4j.logger.org.apache.commons.httpclient=DEBUG`           | 记录 ZAP 服务器发出的每个 HTTP 请求和响应。       |
| `log4j.logger.org.zaproxy.zap.spider.SpiderController=DEBUG` | 记录在目标 spider 扫描期间找到的 URL。              |
| `log4j.logger.com.crawljax=DEBUG`                            | 启用 Ajax Crawler 调试日志记录语句。                     |
| `log4j.logger.org.parosproxy.paros=DEBUG`                    | 启用 ZAP 服务器代理调试日志记录语句。                 |
| `log4j.logger.org.zaproxy.zap=DEBUG`                         | 启用通用 ZAP 服务器代码的调试日志记录语句。   |

## 内存不足

默认情况下，DAST 所依赖的 ZAProxy 分配的内存总和为主机上总内存的 25%。
由于它在扫描期间将大部分信息保存在内存中，因此 DAST 在扫描大型应用程序时可能会耗尽内存。
这会导致以下错误：

```plaintext
[zap.out] java.lang.OutOfMemoryError: Java heap space
```

幸运的是，使用 `DAST_ZAP_CLI_OPTIONS` CI/CD 变量可以直接增加 DAST 的可用内存量：

```yaml
include:
  - template: DAST.gitlab-ci.yml

variables:
  DAST_ZAP_CLI_OPTIONS: "-Xmx3072m"
```

此示例将 3072 MB 分配给 DAST。将 `-Xmx` 后面的数字更改为所需的内存量。

<!--
## DAST 作业超过作业超时

If your DAST job exceeds the job timeout and you need to reduce the scan duration, we shared some
tips for optimizing DAST scans in a [blog post](https://about.gitlab.com/blog/2020/08/31/how-to-configure-dast-full-scans-for-complex-web-applications/).
-->

## 收到警告消息 `gl-dast-report.json: no matching files`

有关这方面的信息，请参阅[一般应用程序安全故障排除部分](../../../ci/jobs/job_artifacts_troubleshooting.md#error-message-no-files-to-upload)。

## 包含 DAST CI 模板时，收到错误 `dast job: chosen stage does not exist`

为避免从其他 CI 文件覆盖阶段，较新版本的 DAST CI 模板不定义阶段。如果您最近开始使用 `DAST.latest.gitlab-ci.yml` 或升级到极狐GitLab 的新主要版本并开始收到此错误，则必须与其他阶段一起定义 `dast` 阶段。请注意，您必须有一个正在运行的应用程序才能让 DAST 进行扫描。如果您的应用程序是在您的流水线中设置的，则它必须部署在 `dast` 阶段之前的阶段：

```yaml
stages:
  - deploy  # DAST needs a running application to scan
  - dast

include:
  - template: DAST.latest.gitlab-ci.yml
```

## 使用 DAST CI/CD 模板时，收到错误 `shell not found`

当包含文档中描述的 DAST CI/CD 模板时，作业可能会失败，并在作业日志中记录如下错误：

```shell
shell not found
```

为避免此错误，请确保您使用的是最新稳定版本的 Docker。

## 缺乏 IPv6 支持

由于底层 [ZAProxy 引擎不支持 IPv6](https://github.com/zaproxy/zaproxy/issues/3705)，DAST 无法扫描或爬取基于 IPv6 的应用程序。

## 深入了解 DAST 扫描活动

要进一步了解 DAST 扫描在给定时间执行的操作，您可能会发现在扫描期间或之后查看 DAST 目标端点的 Web 服务器访问日志会很有帮助。
