---
stage: Secure
group: Dynamic Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference, howto
---

# DAST 基于代理的分析器 **(ULTIMATE)**

DAST 基于代理的分析器可以添加到您的[极狐GitLab CI/CD](../../../ci/index.md) 流水线。有助于您发现未大量使用 JavaScript 的 Web 应用程序中的漏洞。对于大量使用 JavaScript 的 Web 应用程序，请参阅[基于浏览器的分析器](browser_based.md)。

WARNING:
不要对生产服务器运行 DAST 扫描。它不仅可以执行用户可以执行的*任何*功能，例如单击按钮或提交表单，而且还可能触发错误，导致生产数据被修改或丢失。仅对测试服务器运行 DAST 扫描。

分析器使用 [OWASP Zed Attack Proxy](https://www.zaproxy.org/) (ZAP) 以两种不同的方式进行扫描：

- 仅被动扫描（默认）。DAST 执行 [ZAP 的基线扫描](https://www.zaproxy.org/docs/docker/baseline-scan/)，并且不会主动攻击您的应用程序。
- 被动和主动（或完整）扫描。DAST 可以[配置](#full-scan)执行主动扫描，攻击您的应用程序并生成更广泛的安全报告。与 [Review Apps](../../../ci/review_apps/index.md) 结合使用时会非常有用。

## 模板

> - DAST 最新模板引入于 13.8 版本。
> - 所有 DAST 模板更新到 DAST_VERSION: 2 于 14.0 版本。
> - 所有 DAST 模板更新到 DAST_VERSION: 3 于 15.0 版本。

极狐GitLab DAST 配置在 CI/CD 模板中定义。模板的更新随极狐GitLab 升级一起提供，使您可以从任何改进和添加中受益。

可用的模板

- [`DAST.gitlab-ci.yml`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/DAST.gitlab-ci.yml)：DAST CI/CD 模板的稳定版本。
- [`DAST.latest.gitlab-ci.yml`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/DAST.latest.gitlab-ci.yml)：最新版本的 DAST 模板。

WARNING:
最新版本的模板可能包含重大更改。除非您需要仅在最新模板中提供的功能，否则请使用稳定模板。

<!--
For more information about template versioning, see the
[CI/CD documentation](../../../development/cicd/templates.md#latest-version).
-->

## DAST 版本

默认情况下，DAST 模板使用最新主要版本的 DAST Docker 镜像。您可以使用 `DAST_VERSION` 变量选择 DAST 更新的方式：

- 通过固定到主要版本（例如 `1`），使用新功能和修复程序自动更新 DAST。
- 仅通过固定到次要版本（例如 `1.6`）来更新修复。
- 通过固定到特定版本（例如 `1.6.4`）来阻止所有更新。

<!--
Find the latest DAST versions on the [DAST releases](https://gitlab.com/gitlab-org/security-products/dast/-/releases)
page.
-->

## DAST 运行选项

您可以使用 DAST 来检查您的 Web 应用程序：

- 自动，由合并请求启动。
- 手动，按需启动。

运行选项之间的部分差异如下：

| 自动扫描                                                   | 按需扫描                |
|:-----------------------------------------------------------------|:------------------------------|
| DAST 扫描由合并请求启动。                       | DAST 扫描是在 DevOps 生命周期之外手动启动的。 |
| CI/CD 变量来自 `.gitlab-ci.yml`。              | UI 中提供了 CI/CD 变量。 |
| 所有 [DAST CI/CD 变量](#available-cicd-variables)可用。 | 部分 [DAST CI/CD 变量](#available-cicd-variables)可用。 |
| `DAST.gitlab-ci.yml` 模板。                                   | `DAST-On-Demand-Scan.gitlab-ci.yml` 模板。 |

### 启用自动 DAST 运行

要使 DAST 自动运行，可以：

- 启用 [Auto DAST](../../../topics/autodevops/stages.md#auto-dast)（由 [Auto DevOps](../../../topics/autodevops/index.md) 提供）。
- [手动编辑 `.gitlab-ci.yml` 文件](#edit-the-gitlab-ciyml-file-manually)。
- [使用 UI 配置 DAST](#configure-dast-using-the-ui)。

<a id="edit-the-gitlab-ciyml-file-manually"></a>

#### 手动编辑 `.gitlab-ci.yml` 文件

手动编辑现有的 `.gitlab-ci.yml` 文件。如果您的极狐GitLab CI/CD 配置文件很复杂，请使用此方法。

要包含 DAST 模板：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **CI/CD > 编辑器**。
1. 将以下内容复制并粘贴到 `.gitlab-ci.yml` 文件的底部。

   使用 DAST 稳定模板：

   ```yaml
   include:
     - template: DAST.gitlab-ci.yml
   ```

   使用 DAST 最新模板：

   ```yaml
   include:
     - template: DAST.latest.gitlab-ci.yml
   ```

1. 使用以下方法之一定义要由 DAST 扫描的 URL：

   - 设置 `DAST_WEBSITE` [CI/CD 变量](../../../ci/yaml/index.md#variables)。如果设置，则此值优先。

   - 在项目根目录下的 `environment_url.txt` 文件中添加 URL，对于在动态环境中进行测试很有用。要针对在极狐GitLab CI/CD 流水线期间动态创建的应用程序运行 DAST，在 DAST 扫描之前运行的作业必须将应用程序的域保存在 `environment_url.txt` 文件中。DAST 自动解析 `environment_url.txt` 文件以找到其扫描目标。

     例如，在 DAST 之前运行的作业中，您可以包含类似于以下内容的代码：

     ```yaml
     script:
       - echo http://${CI_PROJECT_ID}-${CI_ENVIRONMENT_SLUG}.domain.com > environment_url.txt
     artifacts:
       paths: [environment_url.txt]
       when: always
     ```

     您可以在我们的 [Auto DevOps CI YAML](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml) 文件中找到示例。
1. 选择 **验证** 选项卡，然后选择 **验证流水线**。出现消息 **Simulation completed successfully** 表示文件有效。
1. 选择 **编辑** 选项卡。
1. 可选。在 **提交消息** 中，自定义提交消息。
1. 选择 **提交更改**。

流水线现在包括一个 DAST 作业。

结果保存为 [DAST 报告产物](../../../ci/yaml/artifacts_reports.md#artifactsreportsdast)，您可以稍后下载和分析。由于实施限制，我们始终采用最新的 DAST 产物。在幕后，<!--[GitLab DAST Docker 镜像](https://gitlab.com/security-products/dast)-->DAST Docker 镜像用于在指定的 URL 上运行测试，并扫描是否存在可能的漏洞。

<a id="configure-dast-using-the-ui"></a>

#### 使用 UI 配置 DAST

在此方法中，您可以在 UI 中选择选项。 根据您的选择，将创建一个代码片段，您可以将其粘贴到 `.gitlab-ci.yml` 文件中。

要使用 UI 配置 DAST：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **安全与合规 > 配置**。
1. 在 **动态应用程序安全测试 (DAST)** 部分，选择 **启用 DAST** 或 **配置 DAST**。
1. 选择所需的 **扫描器配置文件**，或选择 **创建扫描器配置文件**，并保存扫描仪配置文件。有关详细信息，请参阅[扫描器配置文件](#scanner-profile)。
1. 选择所需的 **站点配置文件**，或选择 **创建站点配置文件** 并保存站点配置文件。有关详细信息，请参阅[站点配置文件](#site-profile)。
1. 选择 **生成代码片段**。将打开一个窗口，其中包含与您选择的选项相对应的 YAML 片段。
1. 执行以下操作之一：
   1. 要将代码片段复制到剪贴板，请选择 **仅复制代码**。
   1. 要将代码段添加到项目的 `.gitlab-ci.yml` 文件中，请选择 **复制代码并打开 `.gitlab-ci.yml` 文件**。流水线编辑器打开。
      1. 将代码片段粘贴到 `.gitlab-ci.yml` 文件中。
      1. 选择 **验证** 选项卡，然后选择 **验证流水线**。出现消息 **Simulation completed successfully** 表示文件有效。
         1. 选择**编辑**选项卡。
         1. 可选。在 **提交消息** 中，自定义提交消息。
         1. 选择 **提交更改**。

流水线现在包括一个 DAST 作业。

<!--
### API scan

- The [DAST API analyzer](../dast_api/index.md) is used for scanning web APIs. Web API technologies such as GraphQL, REST, and SOAP are supported.
-->

### URL 扫描

> - 引入于 13.4 版本。
> - 优化于 13.11 版本。

URL 扫描允许您指定 DAST 扫描网站的哪些部分。

#### 定义要扫描的 URL

可以通过以下任一方法指定要扫描的 URL：

- 使用 `DAST_PATHS_FILE` CI/CD 变量来指定包含路径的文件的名称。
- 使用 `DAST_PATHS` 变量列出路径。

##### 使用 `DAST_PATHS_FILE` CI/CD 变量

> 引入于 13.6 版本。

要在文件中定义要扫描的 URL，请创建一个每行一个路径的纯文本文件。

```plaintext
page1.html
/page2.html
category/shoes/page1.html
```

要扫描该文件中的 URL，请将 CI/CD 变量 `DAST_PATHS_FILE` 设置为该文件的路径。
该文件可以通过检入项目仓库或由在 DAST 之前运行的作业作为产物生成。

默认情况下，DAST 扫描不会克隆项目仓库。通过将 `GIT_STRATEGY` 设置为 fetch 来指示 DAST 作业克隆项目。将相对于 `CI_PROJECT_DIR` 的文件路径提供给 `DAST_PATHS_FILE`。

```yaml
include:
  - template: DAST.gitlab-ci.yml

variables:
  GIT_STRATEGY: fetch
  DAST_PATHS_FILE: url_file.txt  # url_file.txt lives in the root directory of the project
  DAST_BROWSER_SCAN: "true" # use the browser-based GitLab DAST crawler
```

##### 使用 `DAST_PATHS` CI/CD 变量

> 引入于 13.4 版本。

要在 CI/CD 变量中指定要扫描的路径，请将路径的逗号分隔列表添加到 `DAST_PATHS` 变量。请注意，您只能扫描单个主机的路径。

```yaml
include:
  - template: DAST.gitlab-ci.yml

variables:
  DAST_PATHS: "/page1.html,/category1/page1.html,/page3.html"
  DAST_BROWSER_SCAN: "true" # use the browser-based GitLab DAST crawler
```

使用 `DAST_PATHS` 和 `DAST_PATHS_FILE` 时，请注意以下事项：

- 使用 `DAST_PATHS_FILE` 或 `DAST_PATHS` 时必须定义 `DAST_WEBSITE`。其中列出的路径使用 `DAST_WEBSITE` 构建要扫描的 URL。
- 定义 `DAST_PATHS` 或 `DAST_PATHS_FILE` 时禁用爬虫。
- `DAST_PATHS_FILE` 和 `DAST_PATHS` 不能一起使用。
- `DAST_PATHS` 变量的限制约为 130kb。如果您有一个更大的列表或路径，请使用 `DAST_PATHS_FILE`。

#### 完整扫描

要对列出的路径执行完整扫描，请使用 `DAST_FULL_SCAN_ENABLED` CI/CD 变量。

## 验证

基于代理的分析器使用基于浏览器的分析器，在扫描之前对用户进行身份验证。<!--有关配置说明，请参阅[身份验证](authentication.md)。-->

## 自定义 DAST 设置

您可以使用 CI/CD 变量和命令行选项自定义 DAST 的行为。使用 CI/CD 变量会覆盖 DAST 模板中包含的值。

### 使用 CI/CD 变量自定义 DAST 

WARNING:
从 13.0 版本开始，不再支持使用 [`only` 和 `except`](../../../ci/yaml/index.md#only--except)。您必须改用 [`rules`](../../../ci/yaml/index.md#rules)。

可以使用 `.gitlab-ci.yml` 中的 [`variables`](../../../ci/yaml/index.md#variables) 参数通过 CI/CD 变量更改 DAST 设置。有关所有 DAST CI/CD 变量的详细信息，请阅读[可用 CI/CD 变量](#available-cicd-variables)。

例如：

```yaml
include:
  - template: DAST.gitlab-ci.yml

variables:
  DAST_WEBSITE: https://example.com
  DAST_SPIDER_MINS: 120
  DAST_BROWSER_SCAN: "true" # use the browser-based GitLab DAST crawler
```

因为模板[优先于](../../../ci/yaml/index.md#include)流水线配置，所以最后提到的变量优先。 

#### 启用或禁用规则

DAST 用于扫描漏洞的规则的完整列表可以在 [ZAP 文档](https://www.zaproxy.org/docs/alerts/)中找到。

`DAST_EXCLUDE_RULES` 禁用具有特定 ID 的规则。

`DAST_ONLY_INCLUDE_RULES` 将扫描中使用的规则集限制为具有特定 ID 的规则。

`DAST_EXCLUDE_RULES` 和 `DAST_ONLY_INCLUDE_RULES` 是互斥的，并且带有两个配置退出的 DAST 扫描出现错误。

默认情况下，一些规则被禁用，因为它们要么需要很长时间才能运行，要么经常产生误报。<!--The complete list of disabled rules
can be found in [`exclude_rules.yml`](https://gitlab.com/gitlab-org/security-products/dast/-/blob/main/src/config/exclude_rules.yml).-->

`DAST_EXCLUDE_RULES` 和 `DAST_ONLY_INCLUDE_RULES` 的列表**必须**用双引号 (`"`) 括起来，否则它们将被认为是数值。

<a id="hide-sensitive-information"></a>

#### 隐藏敏感信息

> 引入于 13.1 版本。

HTTP 请求和响应头可能包含敏感信息，包括 cookie 和授权凭据。默认情况下，以下头被隐藏：

- `Authorization`。
- `Proxy-Authorization`。
- `Set-Cookie`（只有值）。
- `Cookie`（只有值）。

使用 [`DAST_MASK_HTTP_HEADERS` CI/CD 变量](#available-cicd-variables)，您可以列出要隐藏其值的标头。<!--有关如何隐藏头的详细信息，请参阅[自定义 DAST 设置](#customize-dast-settings)。-->

#### 使用双向 TLS

> 引入于 14.8 版本。

双向 TLS 允许目标应用程序服务器验证请求是否来自已知来源。基于浏览器的扫描不支持双向 TLS。

**要求**

- Base64 编码的 PKCS12 证书
- base64 编码的 PKCS12 证书的密码

要启用双向 TLS：

1. 如果 PKCS12 证书尚未经过 base64 编码，请将其转换为 base64 编码。出于安全原因，我们建议在本地对证书进行编码，**不**使用网络托管的转换服务。例如，要在 macOS 或 Linux 上对证书进行编码：

   ```shell
   base64 <path-to-pkcs12-certificate-file>
   ```

1. 创建一个名为 `DAST_PKCS12_CERTIFICATE_BASE64` 的[隐藏变量](../../../ci/variables/index.md)，并将 base64 编码的 PKCS12 证书的值存储在该变量中。
1. 创建一个掩码变量 `DAST_PKCS12_PASSWORD` 并将 PKCS12 证书的密码存储在该变量中。

<a id="available-cicd-variables"></a>

#### 可用的 CI/CD 变量

这些 CI/CD 变量特定于 DAST。它们可用于根据您的要求自定义 DAST 的行为。

WARNING:
在将这些更改合并到默认分支之前，应在合并请求中测试极狐GitLab 安全扫描工具的所有自定义设置。不这样做可能会产生意想不到的结果，包括大量误报。

| CI/CD 变量                                   | 类型          | 描述                   |
|:-------------------------------------------------|:--------------|:------------------------------|
| `DAST_ADVERTISE_SCAN`                            | boolean       | 设置为 `true`，向每个发送的请求添加 `Via` 标头，宣传该请求是作为 DAST 扫描的一部分发送的。引入于 14.1 版本。 |
| `DAST_AGGREGATE_VULNERABILITIES`                 | boolean       | 漏洞聚合默认设置为 `true`。要禁用此功能并查看每个漏洞单独设置为 `false`。引入于 14.0 版本。 |
| `DAST_ALLOWED_HOSTS`                            | 逗号分隔的字符串列表 | 爬取时，此变量中包含的主机名被视为在范围内。默认情况下， `DAST_WEBSITE` 主机名包含在允许的主机列表中。使用 `DAST_REQUEST_HEADERS` 设置的 header 会添加到对这些主机名发出的每个请求中。例如，`site.com,another.com`。 |
| `DAST_API_HOST_OVERRIDE` <sup>1</sup>            | string        | **{warning}** **废弃于** 15.7 版本。替换为 `DAST_API_OPENAPI`。用于覆盖 API 规范文件中定义的域。仅在从 URL 导入 API 规格时支持。示例：`example.com:8080`。 |
| `DAST_API_SPECIFICATION` <sup>1</sup>            | URL or string | **{warning}** **废弃于** 15.7 版本。替换为 `DAST_API_OPENAPI`。要导入的 API 规格，可以托管在 URL 上，或者存在于 `/zap/wrk` 目录中的文件名上。如果省略，则必须指定变量`DAST_WEBSITE`。 |
| `DAST_AUTH_EXCLUDE_URLS`             | URLs          | **{warning}** **废弃于** 14.0 版本。替换为 `DAST_EXCLUDE_URLS`。在经过身份验证的扫描期间要跳过的 URL；逗号分隔。正则表达式语法可用于匹配多个 URL。例如，`.*` 匹配任意字符序列。不支持 API 扫描。 |
| `DAST_AUTO_UPDATE_ADDONS`                        | boolean       | ZAP add-ons 固定到 DAST Docker 镜像中的特定版本。设置为 `true`，在扫描开始时下载最新版本。默认值：`false`。 |
| `DAST_DEBUG` <sup>1</sup>                        | boolean       | 启用调试消息输出。默认值：`false`。引入于 13.1 版本。 |
| `DAST_EXCLUDE_RULES`                             | string        | 设置为以逗号分隔的漏洞规则 ID 列表，在扫描期间将其排除在运行之外。规则 ID 是数字，可以从 DAST 日志或 [ZAP 项目](https://www.zaproxy.org/docs/alerts/)中找到。例如，`HTTP Parameter Override` 的规则 ID 为 `10026`。设置了 `DAST_ONLY_INCLUDE_RULES` 时不能使用。**注意：** 在早期版本中，排除的规则被执行，但它们产生的漏洞被抑制。引入于 12.10 版本。 |
| `DAST_EXCLUDE_URLS` <sup>1</sup>               | URLs          | 在经过身份验证的扫描期间要跳过的 URL；逗号分隔。正则表达式语法可用于匹配多个 URL。例如，`.*` 匹配任意字符序列。不支持 API 扫描。例如，`http://example.com/sign-out`。 |
| `DAST_FULL_SCAN_ENABLED` <sup>1</sup>            | boolean       | 设置为 `true` 来运行 [ZAP 完整扫描](https://github.com/zaproxy/zaproxy/wiki/ZAP-Full-Scan)，而不是 [ZAP 基线扫描](https://github.com/zaproxy/zaproxy/wiki/ZAP-Baseline-Scan)。默认值：`false`。 |
| `DAST_HTML_REPORT`                               | string        | 扫描结束时写入的 HTML 报告的文件名。引入于 13.1 版本。 |
| `DAST_INCLUDE_ALPHA_VULNERABILITIES`             | boolean       | 设置为 `true`，包括 alpha 被动和主动扫描规则。默认值：`false`。引入于 13.1 版本。 |
| `DAST_MARKDOWN_REPORT`                           | string        | 扫描结束时写入的 Markdown 报告的文件名。引入于 13.1 版本。 |
| `DAST_MASK_HTTP_HEADERS`                         | string        | 要隐藏的请求和响应头的逗号分隔列表（13.1 版本）。必须包含要隐藏的**所有**头。请参阅[默认隐藏的头列表](#hide-sensitive-information)。 |
| `DAST_MAX_URLS_PER_VULNERABILITY`                | number        | 针对单个漏洞报告的最大 URL 数。`DAST_MAX_URLS_PER_VULNERABILITY` 默认设置为 `50`。列出所有设置为 `0` 的 URL。引入于 13.12 版本。 |
| `DAST_ONLY_INCLUDE_RULES`                        | string        | 设置为以逗号分隔的漏洞规则 ID 列表，将扫描配置为仅运行它们。规则 ID 是数字，可以从 DAST 日志或 [ZAP 项目](https://www.zaproxy.org/docs/alerts/)中找到。设置了 `DAST_EXCLUDE_RULES` 时不能使用。引入于 13.12 版本。 |
| `DAST_PATHS`                                     | string        | 设置为 DAST 扫描的以逗号分隔的 URL 列表。例如，`/page1.html,/category1/page3.html,/page2.html`。引入于 13.4 版本。 |
| `DAST_PATHS_FILE`                                | string        | 包含要扫描的 `DAST_WEBSITE` 中的路径的文件路径。该文件必须是纯文本，每行一个路径。引入于 13.6 版本。 |
| `DAST_PKCS12_CERTIFICATE_BASE64`                 | string        | 用于需要双向 TLS 的站点的 PKCS12 证书。必须编码为 base64 文本。 |
| `DAST_PKCS12_PASSWORD`                           | string        | `DAST_PKCS12_CERTIFICATE_BASE64` 中使用的证书密码。 |
| `DAST_REQUEST_HEADERS` <sup>1</sup>              | string        | 设置为请求头名称和值的逗号分隔列表。Headers 会添加到 DAST 发出的每个请求中。例如，`Cache-control: no-cache,User-Agent: DAST/1.0`。 |
| `DAST_SKIP_TARGET_CHECK`                         | boolean       | 设置为 `true`，防止 DAST 在扫描前检查目标是否可用。默认值：`false`。引入于 13.8 版本。 |
| `DAST_SPIDER_MINS` <sup>1</sup>                  | number        | 蜘蛛扫描的最大持续时间（以分钟为单位）。设置为 `0` 表示无限制。默认值：一分钟，或在扫描为完整扫描时不受限制。引入于 13.1 版本。 |
| `DAST_SPIDER_START_AT_HOST`                      | boolean       | 设置为 `false`，防止 DAST 在扫描之前将目标重置为其主机。当为 `true` 时，非主机目标 `http://test.site/some_path` 在扫描前被重置为 `http://test.site`。默认值：`true`。引入于 13.6 版本。 |
| `DAST_TARGET_AVAILABILITY_TIMEOUT` <sup>1</sup>  | number        | 等待目标可用性的时间限制（以秒为单位）。 |
| `DAST_USE_AJAX_SPIDER` <sup>1</sup>              | boolean       | 设置为 `true` 以在传统 spider 的基础上使用 AJAX spider，这对于爬取需要 JavaScript 的站点很有用。默认值：`false`。引入于 13.1 版本。 |
| `DAST_XML_REPORT`                                | string        | **{warning}** **废弃于** 15.7 版本。扫描结束时写入的 XML 报告的文件名。引入于 13.1 版本。 |
| `DAST_ZAP_CLI_OPTIONS`                           | string        | **{warning}** **废弃于** 15.7 版本。ZAP 服务器命令行选项。例如，`-Xmx3072m` 将设置 Java 最大内存分配池大小。引入于 13.1 版本。 |
| `DAST_ZAP_LOG_CONFIGURATION`                     | string        | **{warning}** **废弃于** 15.7 版本。设置为 ZAP 服务器的附加 log4j 属性的分号分隔列表。示例：`logger.httpsender.name=org.parosproxy.paros.network.HttpSender;logger.httpsender.level=debug;logger.sitemap.name=org.parosproxy.paros.model.SiteMap;logger.sitemap.level=debug ;`。 |
| `SECURE_ANALYZERS_PREFIX`                        | URL           | 设置下载分析器的 Docker 仓库 base 地址。 |

1. 可用于按需 DAST 扫描。


### 使用命令行选项自定义 DAST

并非所有 DAST 配置都可通过 CI/CD 变量获得。要找出所有可能的选项，请运行以下配置。
可用的命令行选项会打印到作业日志中：

```yaml
include:
  template: DAST.gitlab-ci.yml

dast:
  script:
    - /analyze --help
```

然后，您必须覆盖 `script` 命令以传入适当的参数。例如，alpha 中的漏洞定义可以包含在 `-a` 中。以下配置包括这些定义：

```yaml
include:
  template: DAST.gitlab-ci.yml

dast:
  script:
    - export DAST_WEBSITE=${DAST_WEBSITE:-$(cat environment_url.txt)}
    - /analyze -a -t $DAST_WEBSITE
```

### 自定义 ZAProxy 配置

ZAProxy 服务器包含许多有用的可配置值。
`-config` 的许多键/值仍未记录，但有一个未经测试的可能的键列表。
请注意，这些选项不受 DAST 支持，使用时可能会中断 DAST 扫描。以下是如何使用 `TOKEN` 重写 Authorization header 值的示例：

```yaml
include:
  template: DAST.gitlab-ci.yml

variables:
  DAST_ZAP_CLI_OPTIONS: "-config replacer.full_list(0).description=auth -config replacer.full_list(0).enabled=true -config replacer.full_list(0).matchtype=REQ_HEADER -config replacer.full_list(0).matchstr=Authorization -config replacer.full_list(0).regex=false -config replacer.full_list(0).replacement=TOKEN"
```

### 前沿漏洞定义

ZAP 首先在 `alpha` 类中创建规则。在社区测试一段时间后，他们被提升为 `beta`。DAST 默认使用 `beta` 定义。要请求 `alpha` 定义，请使用 `DAST_INCLUDE_ALPHA_VULNERABILITIES` CI/CD 变量，如以下配置所示：

```yaml
include:
  template: DAST.gitlab-ci.yml

variables:
  DAST_INCLUDE_ALPHA_VULNERABILITIES: "true"
```

### 克隆项目的仓库

DAST 作业在运行时不需要项目的仓库，因此默认情况下 [`GIT_STRATEGY`](../../../ci/runners/configure_runners.md#git-strategy) 设置为 `none`。

<a id="on-demand-scans"></a>

## 按需扫描

> - 引入于 13.2 版本。
> - 优化于 13.3 版本。
> - 保存的扫描功能引入于 13.9 版本。
> - 选择分支选项引入于 13.10 版本。
> - DAST 分支选择功能标志删除于 13.11 版本。
> - DAST 配置文件管理的审计引入于 14.1 版本。

按需 DAST 扫描在 DevOps 生命周期之外运行。仓库中的更改不会触发扫描。您必须手动启动它，或者安排它运行。

按需 DAST 扫描：

- 可以运行[站点配置文件](#site-profile)和[扫描器配置文件](#scanner-profile)的特定组合。
- 与您项目的默认分支相关联。
- 在创建时保存，以便以后运行。

### 按需扫描模式

按需扫描可以在主动或被动模式下运行：

- 被动模式是默认模式，运行 ZAP 基线扫描。
- 主动模式运行 ZAP 全面扫描，这可能对正在扫描的站点有害。为了最大限度地降低意外损坏的风险，运行主动扫描需要[已验证的站点配置文件](#site-profile-validation)。

### 查看按需 DAST 扫描
 
要查看按需扫描，请从您的项目主页转到左侧栏中的 **安全与合规性 > 按需扫描**。

按需扫描按其状态分组。扫描库包含所有可用的按需扫描。

从 **按需扫描** 页面，您可以：

- [运行](#run-an-on-demand-dast-scan)按需扫描。
- 查看按需扫描的结果。
- 取消 (**{cancel}**) 挂起或运行的按需扫描。
- 重试 (**{retry}**) 扫描失败或成功但出现警告。
- [编辑](#edit-an-on-demand-scan) (**{pencil}**) 按需扫描的设置。
- [删除](#delete-an-on-demand-scan)按需扫描。

<a id="run-an-on-demand-dast-scan"></a>

### 运行按需 DAST 扫描

先决条件：

- 您必须有权对受保护的分支运行按需 DAST 扫描。默认分支被自动保护。有关更多信息，请阅读[受保护分支上的流水线安全性](../../../ci/pipelines/index.md#pipeline-security-on-protected-branches)。
- [扫描器配置文件](#create-a-scanner-profile)。
- [站点配置文件](#create-a-site-profile)。
- 如果您正在运行主动扫描，则站点配置文件必须已[验证](#validate-a-site-profile)。

您可以立即运行按需扫描，在预定的日期和时间或以指定的频率运行一次：

- 每天
- 每周
- 每个月
- 每 3 个月
- 每 6 个月
- 每年

要立即运行按需扫描，请执行以下任一操作：

- [立即创建并运行按需扫描](#create-and-run-an-on-demand-scan-immediately)。
- [运行以前保存的按需扫描](#run-a-saved-on-demand-scan)。

要在计划的日期或频率运行按需扫描，请阅读[计划按需扫描](#schedule-an-on-demand-scan)。

<a id="create-and-run-an-on-demand-scan-immediately"></a>

#### 立即创建并运行按需扫描

1. 在您的项目主页中，转到左侧边栏中的 **安全与合规 > 按需扫描**。
1. 选择 **新建扫描**。
1. 填写 **扫描名称** 和 **描述** 字段。
1. 在 13.10 及更高版本中，从**分支**下拉列表中选择所需的分支。
1. 在 **扫描器配置文件** 中，从下拉列表中选择一个扫描器配置文件。
1. 在 **站点配置文件** 中，从下拉列表中选择站点配置文件。
1. 要立即运行按需扫描，请选择 **保存并运行扫描**。否则，请稍后选择 **保存扫描**，然后[运行](#run-a-saved-on-demand-scan)。

按需 DAST 扫描运行，项目的仪表盘显示结果。

<a id="run-a-saved-on-demand-scan"></a>

#### 运行保存的按需扫描

要运行保存的按需扫描：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **安全与合规 > 按需扫描**。
1. 选择 **扫描库** 选项卡。
1. 在扫描行中，选择 **运行扫描**。

   如果扫描中保存的分支不再存在，则必须先[编辑扫描](#edit-an-on-demand-scan)，选择新的分支，保存编辑后的扫描。

按需 DAST 扫描运行，项目的仪表盘显示结果。

<a id="schedule-an-on-demand-scan"></a>

#### 计划按需扫描

> - 引入于 14.3 版本。[功能标志为 `dast_on_demand_scans_scheduler`](../../../administration/feature_flags.md)，默认禁用。
> - 在 SaaS 上启用于 14.4 版本。
> - 在私有化部署版上启用于 14.4 版本。
> - 功能标志 `dast_on_demand_scans_scheduler` 删除于 14.5 版本。

要计划扫描：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **安全与合规 > 按需扫描**。
1. 选择 **新建扫描**。
1. 填写 **扫描名称** 和 **描述** 文本框。
1. 在 13.10 及更高版本中，从 **分支** 下拉列表中，选择所需的分支。
1. 在 **扫描器配置文件** 部分，从下拉列表中选择一个扫描器配置文件。
1. 在 **站点配置文件** 部分，从下拉列表中选择站点配置文件。
1. 选择 **计划扫描**。
1. 在 **开始时间** 部分，选择时区、日期和时间。
1. 从 **重复** 下拉列表中，选择您想要的频率：
    - 要运行一次扫描，请选择 **从不**。
    - 对于重复扫描，请选择任何其他选项。
1. 要立即运行按需扫描，请选择 **保存并运行扫描**。要根据您设置的计划[运行](#run-a-saved-on-demand-scan)它，请选择 **保存扫描**。

#### 查看按需扫描的详细信息

要查看按需扫描的详细信息：

1. 从您的项目主页，转到 **安全与合规 > 按需扫描**。
1. 选择 **扫描库** 选项卡。
1. 在已保存扫描的行中选择 **更多操作** (**{ellipsis_v}**)，然后选择 **编辑**。

<a id="edit-an-on-demand-scan"></a>

#### 编辑按需扫描

要编辑按需扫描：

1. 从您的项目主页，转到 **安全与合规 > 按需扫描**。
1. 选择 **扫描库** 选项卡。
1. 在已保存扫描的行中选择 **更多操作** (**{ellipsis_v}**)，然后选择 **编辑**。
1. 编辑表格。
1. 选择 **保存扫描**。

<a id="delete-an-on-demand-scan"></a>

#### 删除按需扫描

要删除按需扫描：

1. 从您的项目主页，转到 **安全与合规 > 按需扫描**。
1. 选择 **扫描库** 选项卡。
1. 在已保存扫描的行中选择 **更多操作** (**{ellipsis_v}**)，然后选择 **删除**。
1. 选择 **删除** 确认删除。

<a id="site-profile"></a>

## 站点配置文件

> - 站点配置文件功能、扫描方法和文件 URL 在 SaaS 和私有化部署版上启用于 15.6 版本。
> - GraphQL 端点路径功能引入于 15.7 版本。

站点配置文件定义了要由 DAST 扫描的已部署应用程序、网站或 API 的属性和配置详细信息。可以在 `.gitlab-ci.yml` 和按需扫描中引用站点配置文件。

站点配置文件包含：

- **配置文件名称**：您分配给要扫描的站点的名称。虽然在 `.gitlab-ci.yml` 或按需扫描中引用了站点配置文件，但它**不能**被重命名。
- **站点类型**：要扫描的目标类型，网站或 API 扫描。
- **目标 URL**：运行 DAST 的 URL。
- **排除的 URLs**：要从扫描中排除的 URL 的逗号分隔列表。
- **请求头**：以逗号分隔的 HTTP 请求头列表，包括名称和值。这些头会添加到 DAST 发出的每个请求中。
- **验证（用于网站）**：
  - **验证 URL**：目标网站上包含登录 HTML 表单的页面的 URL。用户名和密码与登录表单一起提交以创建经过身份验证的扫描。
  - **用户名**：用于对网站进行身份验证的用户名。
  - **密码**：用于对网站进行身份验证的密码。
  - **用户名表单字段**：登录 HTML 表单中的用户名字段的名称。
  - **密码表单字段**：登录 HTML 表单中的密码字段名称。
  - **提交表单字段**：单击时提交登录 HTML 表单的元素的 `id` 或 `name`。

- **扫描方法**：一种执行 API 测试的方法。支持的方法是 OpenAPI、Postman Collections、HTTP Archive (HAR) 或 GraphQL。
  - **GraphQL 端点路径**：GraphQL 端点的路径。此路径与目标 URL 连接在一起，以提供用于扫描测试的 URI。GraphQL 端点必须支持内省查询。
  - **文件 URL**：OpenAPI、Postman 集合或 HTTP 存档文件的 URL。

选择 API 站点类型时，将使用主机覆盖，来确保被扫描的 API 与目标位于同一主机上。这样做是为了降低针对错误 API 运行主动扫描的风险。

配置后，请求头和密码字段在存储到数据库之前使用 `aes-256-gcm` 加密。
只能使用有效的机密文件读取和解密此数据。

<a id="site-profile-validation"></a>

### 站点配置文件验证

> - 站点配置文件验证引入于 13.8 版本。
> - Meta tag 验证引入于 14.2 版本。

站点配置文件验证降低了针对错误网站运行主动扫描的风险。必须先验证站点，然后才能对其运行主动扫描。现场验证方法如下：

- *文本文件验证*需要将文本文件上传到目标站点。文本文件被分配了一个项目唯一的名称和内容。验证过程检查文件的内容。
- *Header 验证*需要将头 `Gitlab-On-Demand-DAST` 添加到目标站点，并具有项目独有的值。验证过程检查头是否存在，并检查其值。
- *Meta tag 验证*需要将名为 `gitlab-dast-validation` 的 Meta tag 添加到目标站点，并具有项目独有的值。确保将其添加到页面的 `<head>` 部分。验证过程检查元标记是否存在，并检查其值。

所有这些方法在功能上都是等效的。使用任何可行的方法。

在 14.2 及更高版本，站点配置文件验证发生在使用[极狐GitLab Runner](../../../ci/runners/index.md) 的 CI 作业中。

<a id="create-a-site-profile"></a>

### 创建站点配置文件

要创建站点配置文件：

1. 从您项目的主页，转到 **安全与合规 > 配置**。
1. 在 **DAST 配置文件** 行中选择 **管理**。
1. 选择 **新建 > 站点配置文件**。
1. 填写字段，然后选择 **保存配置文件**。

站点配置文件已创建。

### 编辑站点配置文件

如果站点配置文件链接到安全策略，则用户无法从此页面编辑配置文件。有关详细信息，请参阅[扫描执行策略](../policies/scan-execution-policies.md)。

编辑经过验证的站点配置文件的文件、标题或 Meta tag 时，该站点的[验证状态](#site-profile-validation)将被撤销。

要编辑站点配置文件：

1. 从您项目的主页，转到 **安全与合规 > 配置**。
1. 在 **DAST 配置文件** 行中选择 **管理**。
1. 选择 **站点配置文件** 选项卡。
1. 在配置文件行中选择 **更多操作** (**{ellipsis_v}**) 菜单，然后选择 **编辑**。
1. 编辑字段，然后选择 **保存配置文件**。

### 删除站点配置文件

如果站点配置文件链接到安全策略，则用户无法从此页面删除配置文件。
有关详细信息，请参阅[扫描执行策略](../policies/scan-execution-policies.md)。

要删除站点配置文件：

1. 从您项目的主页，转到 **安全与合规 > 配置**。
1. 在 **DAST 配置文件** 行中选择 **管理**。
1. 选择 **站点配置文件** 选项卡。
1. 在配置文件行中，选择 **更多操作** (**{ellipsis_v}**) 菜单，然后选择 **删除**。
1. 选择 **删除** 确认删除。

### 验证站点配置文件

需要验证站点才能运行主动扫描。

要验证站点配置文件：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **安全与合规 > 配置**。
1. 在 **动态应用程序安全测试 (DAST)** 部分中，选择 **管理配置文件**。
1. 选择 **站点配置文件** 选项卡。
1. 在配置文件行中，选择 **验证**。
1. 选择验证方法。
   1. 对于 **文本文件验证**：
      1. 下载**步骤 2** 中列出的验证文件。
      1. 将验证文件上传到主机、**步骤 3** 中的位置或您喜欢的任何位置。
      1. 如果需要，请在**步骤 3** 中编辑文件位置。
      1. 选择 **验证**。
   1. 对于 **Header 验证**：
      1. 在 **步骤 2** 中选择剪贴板图标。
      1. 编辑要验证的站点 header，并粘贴剪贴板内容。
      1. 选择 **步骤 3** 中的输入框，输入 header 的位置。
      1. 选择 **验证**。
   1. 对于 **Meta tag 验证**：
      1. 在 **步骤 2** 中选择剪贴板图标。
      1. 编辑要验证的站点内容，并粘贴剪贴板内容。
      1. 选择 **步骤 3** 中的输入框，输入元标记的位置。
      1. 选择 **验证**。

该站点已经过验证，并且可以对其运行主动扫描。站点配置文件的验证状态仅在手动撤销或编辑其文件、标题或元标记时才被撤销。

### 重试失败的验证

> - 引入于 14.3 版本。
> - [功能标志为 `dast_failed_site_validations`](../../../administration/feature_flags.md)，默认启用。
> - [功能标志 `dast_failed_site_validations` 删除于 14.4 版本。

**管理配置文件**页面的**站点配置文件**选项卡上列出了失败的站点验证尝试。

要重试站点配置文件的失败验证：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **安全与合规 > 配置**。
1. 在 **动态应用程序安全测试 (DAST)** 部分中，选择 **管理配置文件**。
1. 选择 **站点配置文件** 选项卡。
1. 在配置文件行中，选择 **重试验证**。

### 撤销站点配置文件的验证状态

WARNING:
当站点配置文件的验证状态被撤销时，共享相同 URL 的所有站点配置文件的验证状态也会被撤销。

要撤销站点配置文件的验证状态：

1. 从您项目的主页，转到 **安全与合规 > 配置**。
1. 在 **DAST 配置文件** 行中选择 **管理**。
1. 在经过验证的配置文件旁边，选择 **撤销验证**。

站点配置文件的验证状态已撤销。

### 已验证的站点配置文件 header

以下是有关如何在应用程序中提供所需站点配置文件 header 的代码示例。

#### 用于按需扫描的 Ruby on Rails 示例

以下是在 Ruby on Rails 应用程序中添加自定义 header 的方法：

```ruby
class DastWebsiteTargetController < ActionController::Base
  def dast_website_target
    response.headers['Gitlab-On-Demand-DAST'] = '0dd79c9a-7b29-4e26-a815-eaaf53fcab1c'
    head :ok
  end
end
```

#### 用于按需扫描的 Django 示例

以下是如何在 Django 中添加[自定义 header](https://docs.djangoproject.com/en/2.2/ref/request-response/#setting-header-fields)：

```python
class DastWebsiteTargetView(View):
    def head(self, *args, **kwargs):
      response = HttpResponse()
      response['Gitlab-On-Demand-DAST'] = '0dd79c9a-7b29-4e26-a815-eaaf53fcab1c'

      return response
```

#### 用于按需扫描的 Node（使用 Express）示例

以下是如何在 Node 中添加[自定义 header（使用 Express）](https://expressjs.com/en/5x/api.html#res.append)：

```javascript
app.get('/dast-website-target', function(req, res) {
  res.append('Gitlab-On-Demand-DAST', '0dd79c9a-7b29-4e26-a815-eaaf53fcab1c')
  res.send('Respond to DAST ping')
})
```

<a id="scanner-profile"></a>

## 扫描器配置文件

> - 引入于 13.4 版本。
> - 添加于 13.5 版本：扫描模式、AJAX spider、debug 消息。

扫描器配置文件定义了安全扫描器的配置细节。可以在 `.gitlab-ci.yml` 和按需扫描中引用扫描器配置文件。

扫描器配置文件包含：

- **配置文件名称：** 您为扫描仪配置文件指定的名称。例如，“Spider_15”。虽然在 `.gitlab-ci.yml` 或按需扫描中引用了扫描器配置文件，但它**不能**被重命名。
- **扫描模式：**被动扫描监视发送到目标的所有 HTTP 消息（请求和响应）。主动扫描攻击目标以发现潜在漏洞。
- **Spider 超时：**允许 Spider 遍历站点的最大分钟数。
- **目标超时：**DAST 在开始扫描之前等待站点可用的最大秒数。
- **AJAX Spider：**除了传统的 Spider 之外，还运行 AJAX Spider 来抓取目标站点。
- **调试消息：**在 DAST 控制台输出中包含调试消息。

<a id="create-a-scanner-profile"></a>

### 创建扫描器配置文件

要创建扫描仪配置文件：

1. 从您项目的主页，转到 **安全与合规 > 配置**。
1. 在 **DAST 配置文件** 行中，选择 **管理**。
1. 选择 **新建 > 扫描器配置文件**。
1. 填写表格。有关每个字段的详细信息，请参阅[扫描器配置文件](#scanner-profile)。
1. 选择 **保存配置文件**。

### 编辑扫描器配置文件

如果扫描器配置文件链接到安全策略，则用户无法从此页面编辑配置文件。
有关详细信息，请参阅[扫描执行策略](../policies/scan-execution-policies.md)。

要编辑扫描器配置文件：

1. 从您项目的主页，转到 **安全与合规 > 配置**。
1. 在 **DAST 配置文件** 行中，选择 **管理**。
1. 选择 **扫描器配置文件** 选项卡。
1. 在扫描器行中，选择 **更多操作** (**{ellipsis_v}**) 菜单，然后选择 **编辑**。
1. 编辑表格。
1. 选择 **保存配置文件**。

### 删除扫描器配置文件

如果扫描器配置文件链接到安全策略，则用户无法从此页面删除配置文件。有关详细信息，请参阅[扫描执行策略](../policies/scan-execution-policies.md)。

要删除扫描器配置文件：

1. 从您项目的主页，转到 **安全与合规 > 配置**。
1. 在 **DAST 配置文件** 行中，选择 **管理**。
1. 选择 **扫描器配置文件** 选项卡。
1. 在扫描器行中，选择 **更多操作** (**{ellipsis_v}**) 菜单，然后选择 **删除**。
1. 选择 **删除**。

## 审计

> 引入于 14.1 版本。

DAST 配置文件、DAST 扫描程序配置文件和 DAST 站点配置文件的创建、更新和删除包含在[审计日志](../../../administration/audit_events.md)中。

## 报告

DAST 工具会输出一个 `gl-dast-report.json` 报告文件，其中包含扫描及其结果的详细信息。
该文件包含在作业的产物中。JSON 是默认格式，但您可以以 Markdown、HTML 和 XML 格式输出报告。要指定替代格式，请使用 [CI/CD 变量](#available-cicd-variables)。您还可以使用 CI/CD 变量来配置作业，输出 `gl-dast-debug-auth-report.html` 文件，有助于调试身份验证问题。

<!--
For details of the report's schema, see the [schema for DAST reports](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/dast-report-format.json). Example reports can be found in the
[DAST repository](https://gitlab.com/gitlab-org/security-products/dast/-/tree/main/test/end-to-end/expect).
-->

WARNING:
JSON 报告产物不是 DAST 的公共 API，其格式预计会在未来发生变化。
