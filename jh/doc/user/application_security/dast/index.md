---
stage: Secure
group: Dynamic Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# 动态应用程序安全测试（DAST） **(ULTIMATE)**

如果您将 Web 应用程序部署到新环境中，您的应用程序可能会受到新类型的攻击。例如，您的应用程序服务器的错误配置或对安全控制的错误假设可能无法从源代码中看到。

动态应用程序安全测试 (DAST) 在已部署环境中检查应用程序是否存在此类漏洞。

<!--
NOTE:
To learn how four of the top six attacks were application-based and how
to protect your organization, download our
["A Seismic Shift in Application Security"](https://about.gitlab.com/resources/whitepaper-seismic-shift-application-security/)
whitepaper.
-->

## 极狐GitLab DAST

极狐GitLab 提供以下 DAST 分析器，其中一个或多个可能有用，具体取决于您正在测试的应用程序的类型。

对于扫描网站，请使用以下之一：

- [DAST 基于代理的分析器](proxy-based.md)，用于扫描提供简单 HTML 服务的传统应用程序。基于代理的分析器可以自动或按需运行。
- [DAST 基于浏览器的分析器](browser_based.md)，用于扫描大量使用 JavaScript 的应用程序。包括单页 Web 应用程序。

<!--
For scanning APIs, use:

- The [DAST API analyzer](../dast_api/index.md) for scanning web APIs. Web API technologies such as GraphQL, REST, and SOAP are supported.
-->

分析器遵循[保护您的应用程序](../index.md)中描述的架构模式。
可以使用 CI 模板在流水线中配置每个分析器，并在 Docker 容器中运行扫描。扫描输出一个 [DAST 报告产物](../../../ci/yaml/artifacts_reports.md#artifactsreportsdast)，极狐GitLab 使用它，根据源分支和目标分支上的扫描结果之间的差异来确定发现的漏洞。

### 开始使用

#### 先决条件

- [极狐GitLab Runner](../../../ci/runners/index.md) 可用，使用 [`docker` 执行器](https://docs.gitlab.cn/runner/executors/docker.html)（Linux/amd64）。
- 已部署目标应用程序。有关详细信息，请阅读[部署选项](#application-deployment-options)。
- `dast` 阶段已添加到 CI/CD 流水线定义中，应该在部署步骤之后添加，例如：

  ```yaml
  stages:
    - build
    - test
    - deploy
    - dast
  ```

#### 建议

- 请注意您的流水线是否配置为在每次运行时部署到同一 Web 服务器。在更新服务器时，运行 DAST 扫描将导致不准确和不确定的结果。
- 配置 runner 使用 [always 拉取策略](https://docs.gitlab.com/runner/executors/docker.html#using-the-always-pull-policy)，运行最新版本的分析器。
- 默认情况下，DAST 下载流水线中先前作业定义的所有产物。如果您的 DAST 作业不依赖 `environment_url.txt` 来定义被测 URL 或在之前的作业中创建的任何其他文件，我们建议您不要下载产物。为避免下载产物，请扩展分析器 CI/CD 作业来指定无依赖项。例如，对于基于 DAST 代理的分析器，将以下内容添加到您的 `.gitlab-ci.yml` 文件中：

  ```yaml
  dast:
    dependencies: []
  ```

<!--
#### Analyzer configuration

Please see [DAST proxy-based analyzer](proxy-based.md), [DAST browser-based analyzer](browser_based.md) or [DAST API analyzer](../dast_api/index.md) for
analyzer-specific configuration instructions.
-->

### 查看扫描结果

<!--
Detected vulnerabilities are shown in [Merge requests](../index.md#view-security-scan-information-in-merge-requests), the [Pipeline security tab](../index.md#view-security-scan-information-in-the-pipeline-security-tab),
and the [Vulnerability report](../index.md#view-security-scan-information-in-the-vulnerability-report).
-->

检测到的漏洞显示在合并请求、流水线安全选项卡和漏洞报告中。

1. 要查看检测到的所有漏洞，可以：
    - 从您的项目中，选择 **安全与合规**，然后选择 **漏洞报告**。
    - 在您的流水线中，单击 **安全** 选项卡。
    - 从合并请求中，转到 **安全扫描** 部件，并单击 **完整报告** 选项卡。

1. 选择 DAST 漏洞的描述。以下字段是 DAST 分析器可能生成的示例，用于帮助调查和纠正根本原因。每个分析器可能输出不同的字段。

   | 字段            | 描述                                                                      |
   |:--------------------------------|:----------- |
   | Description      | 漏洞描述。                                                                                                                                            |
   | Evidence         | 验证了漏洞的数据发现。通常是请求或响应的片段，可用于帮助验证发现的是否为漏洞。  |
   | Identifiers      | 漏洞的标识符。                                                                                                                                             |
   | Links            | 指向检测到的漏洞的更多详细信息的链接。                                                                                                                       |
   | Method           | 用于检测漏洞的 HTTP 方法。                                                                                                                                 |
   | Project          | 检测到漏洞的命名空间和项目。                                                                                                                |
   | Request Headers  | 请求的 headers。                                                                                                                                                       |
   | Response Headers | 从应用程序收到的响应的 headers。                                                                                                                        |
   | Response Status  | 从应用程序收到的响应状态。                                                                                                                                |
   | Scanner Type     | 漏洞报告的类型。                                                                                                                                                 |
   | Severity         | 漏洞的严重性。                                                                                                                                               |
   | Solution         | 漏洞的推荐解决方案的详细信息。                                                                                                                       |
   | URL              | 检测到漏洞的 URL。                                                                                                                                  |


NOTE:
一个流水线可能包含多个作业，包括 SAST 和 DAST 扫描。如果任何作业因任何原因未能完成，安全仪表盘不会显示 DAST 扫描程序输出。例如，如果 DAST 作业完成但 SAST 作业失败，则安全仪表盘不会显示 DAST 结果。失败时，分析器会输出退出码。

#### 列出扫描的 URL

当 DAST 完成扫描时，合并请求页面会说明扫描的 URL 数量。
选择 **查看详细信息**，查看 Web 控制台输出，其中包括扫描的 URL 列表。

![DAST Widget](img/dast_urls_scanned_v12_10.png)

<a id="application-deployment-options"></a>

### 应用程序部署选项

DAST 要求已部署的应用程序可供扫描。

根据目标应用程序的复杂性，有一些关于如何部署和配置 DAST 模板的选项。<!--We provided a set of example applications with their configurations in our
[DAST demonstrations](https://gitlab.com/gitlab-org/security-products/demos/dast/) project.-->

#### Review Apps

Review Apps 是部署 DAST 目标应用程序最复杂的方法。<!--To assist in the process,
we created a Review App deployment using Google Kubernetes Engine (GKE). This example can be found in our
[Review Apps - GKE](https://gitlab.com/gitlab-org/security-products/demos/dast/review-app-gke) project, along with detailed
instructions in the [README.md](https://gitlab.com/gitlab-org/security-products/demos/dast/review-app-gke/-/blob/master/README.md)
on how to configure Review Apps for DAST.-->

#### Docker 服务

如果您的应用程序使用 Docker 容器，则您可以选择使用 DAST 进行部署和扫描。
在 Docker 构建作业完成并将镜像添加到容器镜像库后，您可以将镜像用作[服务](../../../ci/services/index.md)。

通过使用 `.gitlab-ci.yml` 中的服务定义，您可以使用 DAST 分析器扫描服务。

向作业添加 `services` 部分时，`alias` 用于定义可用于访问服务的主机名。在以下示例中，`dast` 作业定义的 `alias: yourapp` 部分表示已部署应用程序的 URL 将使用 `yourapp` 作为主机名（`https://yourapp/`）。

```yaml
stages:
  - build
  - dast

include:
  - template: DAST.gitlab-ci.yml

# Deploys the container to the GitLab container registry
deploy:
  services:
  - name: docker:dind
    alias: dind
  image: docker:20.10.16
  stage: build
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE:latest

dast:
  services: # use services to link your app container to the dast job
    - name: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
      alias: yourapp

variables:
  DAST_WEBSITE: https://yourapp
  DAST_FULL_SCAN_ENABLED: "true" # do a full scan
  DAST_BROWSER_SCAN: "true" # use the browser-based GitLab DAST crawler
```

大多数应用程序依赖于多种服务，例如数据库或缓存服务。默认情况下，服务字段中定义的服务不能相互通信。要允许服务之间的通信，请启用 `FF_NETWORK_PER_BUILD` [功能标志](https://docs.gitlab.cn/runner/configuration/feature-flags.html#available-feature-flags)。

```yaml
variables:
  FF_NETWORK_PER_BUILD: "true" # enable network per build so all services can communicate on the same network

services: # use services to link the container to the dast job
  - name: mongo:latest
    alias: mongo
  - name: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    alias: yourapp
```
