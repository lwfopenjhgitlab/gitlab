---
stage: Secure
group: Static Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# SAST 分析器 **(FREE)**

> 从旗舰版移动到免费版于 13.3 版本。

静态应用程序安全测试 (SAST) 使用分析器来检测源代码中的漏洞。每个分析器都是第三方代码分析工具 [scanner](../terminology/index.md#scanner) 的包装器。

分析器作为 Docker 镜像发布，SAST 使用这些镜像，为每次分析启动专用容器。

极狐GitLab 提供 SAST 默认镜像，但您也可以集成自己的自定义镜像。

对于每个扫描器，一个分析器：

- 暴露其检测逻辑。
- 处理其执行。
- 将其输出转换为[标准格式](../terminology/index.md#secure-report-format)。

<a id="sast-analyzers"></a>

## SAST 分析器

SAST 支持以下官方分析器：

- `brakeman` (Brakeman)
- `flawfinder` (Flawfinder)
- `kubesec` (Kubesec)
- `mobsf` (MobSF (beta))
- `nodejs-scan` (NodeJsScan)
- `phpcs-security-audit` (PHP CS security-audit)
- `pmd-apex` (PMD (仅 Apex))
- `semgrep` (Semgrep)
- `sobelow` (Sobelow (Elixir Phoenix))
- `spotbugs`（带有 Find Sec Bugs 插件的 SpotBugs（Ant、Gradle 和 wrapper、Grails、Maven 和 wrapper、SBT））

SAST 在以前的版本中使用了其他分析器。以下分析仪已达到结束支持状态并且不会收到更新：

- `bandit`（Bandit）；结束支持于 15.4 版本。由使用 GitLab-managed 规则的 `semgrep` 分析器代替。
- `eslint`（ESLint (JavaScript 和 React)）；结束支持于 15.4 版本。由使用 GitLab-managed 规则的 `semgrep` 分析器代替。
- `gosec`（Gosec）；结束支持于 15.4 版本。由使用 GitLab-managed 规则的 `semgrep` 分析器代替。
- `security-code-scan`（Security Code Scan (.NET)）；结束支持于 16.0 版本。由使用 GitLab-managed 规则的 `semgrep` 分析器代替。

## SAST 分析器功能

对于被视为可用的分析器，预计至少支持以下功能：

- [自定义配置](index.md#available-cicd-variables)
- [自定义规则集](customize_rulesets.md#customize-rulesets)
- [扫描项目](index.md#supported-languages-and-frameworks)
- [多项目支持](index.md#multi-project-support)
- 离线支持
- [发出 JSON 报告格式](index.md#reports-json-format)
- [SELinux 支持](index.md#running-sast-in-selinux)

## Post 分析器

Post 分析器丰富了分析器的报告输出。它不会直接修改报告内容，而是通过其他属性增强了结果，包括：

- CWE。
- 位置跟踪字段。
- 一种识别误报或无关紧要的发现的方法。 **(ULTIMATE)**

## 过渡到基于 Semgrep 的扫描

SAST 包括一个基于 Semgrep 的分析器，涵盖[多种语言](index.md#supported-languages-and-frameworks)。<!--GitLab 维护分析器并为其编写检测规则。-->

如果您使用[极狐GitLab 管理的 CI/CD 模板](index.md#configuration)，则基于 Semgrep 的分析器与其他特定于语言的分析器一起运行。它使用极狐GitLab 管理的检测规则运行，这些规则模仿其他分析器的检测规则。

<!--Work to remove language-specific analyzers and replace them with the Semgrep-based analyzer is tracked in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/5245).-->

您可以选择提前禁用其他分析器，并在默认行为更改之前,，使用基于 Semgrep 的扫描来扫描支持的语言。如果您这样做：

- 您将享受到更快的扫描速度、更少的 CI 分钟使用量以及更可自定义的扫描规则。
- 但是，特定语言分析器之前报告的漏洞将在某些条件下再次报告，包括如果您之前已消除漏洞。系统行为取决于：
  - 您过去是否排除了基于 Semgrep 的分析器的运行。
  - 哪个分析器首先发现了项目的[漏洞报告](../vulnerability_report/index.md)中显示的漏洞。

### 漏洞转化

当您为一种语言切换分析器时，漏洞可能不匹配。

对于某些语言，漏洞管理系统会自动将漏洞从旧分析器移至 Semgrep：

- 对于 C，如果漏洞仅在 Semgrep 也检测到的中被 Flawfinder 检测到，则会移动漏洞。在 14.4（2021 年 10 月）版本的 CI/CD 模板中默认引入了 C 的 Semgrep 覆盖。
- 对于 Go，如果 Gosec 仅在 Semgrep 也检测到该漏洞的管道中检测到该漏洞，则该漏洞将被移动。在 14.2（2021 年 8 月）版本的 CI/CD 模板中默认引入了 Go 的 Semgrep 覆盖。
- 对于 JavaScript 和 TypeScript，如果 ESLint 仅在 Semgrep 也检测到该漏洞的管道中检测到该漏洞，则该漏洞将被移动。这些语言的 Semgrep 覆盖已被引入 13.12（2021 年 5 月）版本的 CI/CD 模板中。

但是，如果出现以下情况，您将看到根据 Semgrep 结果重新创建的旧漏洞：

- Bandit 或 SpotBugs 创建了一个漏洞，您禁用了这些分析器。如果分析器不工作，我们只建议现在禁用 Bandit 和 SpotBugs。<!--在 [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/328062) 中跟踪了将 Bandit 和 SpotBugs 漏洞自动转换为 Semgrep 的工作。-->
- ESLint、Gosec 或 Flawfinder 在默认分支管道中创建了一个漏洞，其中 Semgrep 扫描未成功运行（在为该语言引入 Semgrep 覆盖之前，因为您明确禁用了 Semgrep，或者因为 Semgrep 扫描在该流水线中失败）。如果这些漏洞已经存在，我们目前不打算合并它们。

重新创建漏洞时，在漏洞报告中将原始漏洞标记为 "no longer detected"。然后根据 Semgrep 的发现创建一个新漏洞。

### 尽早激活基于 Semgrep 的扫描

在默认行为更改之前，您可以选择使用基于 Semgrep 的扫描而不是特定于语言的分析器。

如果适用以下任何一种情况，我们建议采用这种方法：

- 您之前没有在项目中使用过 SAST，因此您的[漏洞报告](../vulnerability_report/index.md)中还没有 SAST 漏洞。
- 您在配置覆盖范围与基于 Semgrep 的覆盖范围重叠的分析器之一时遇到问题。例如，您可能无法设置基于 SpotBugs 的分析器来编译您的代码。
- 您已经看到并消除了由 ESLint、Gosec 或 Flawfinder 扫描创建的漏洞，并且您保留了由 Semgrep 重新创建的漏洞。

您可以为每个特定于语言的分析器做出单独的选择，也可以将它们全部禁用。

#### 激活基于 Semgrep 的扫描

要尽早切换到基于 Semgrep 的扫描，您可以：

1. 创建合并请求（MR），将 [`SAST_EXCLUDED_ANALYZERS` CI/CD 变量](#disable-specific-default-analyzers) 设置为 `"bandit,gosec,eslint"`。
    - 如果您还想禁用 SpotBugs 扫描，请将 `spotbugs` 添加到列表中。我们只推荐这个用于 Java 项目。 SpotBugs 是当前唯一可以扫描 Groovy、Kotlin 和 Scala 的分析器。
    - 如果您还想禁用 Flawfinder 扫描，请将“flawfinder”添加到列表中。我们只推荐这个用于 C 项目。 Flawfinder 是目前唯一可以扫描 C++ 的分析器。
2. 验证扫描作业在 MR 中是否成功。您会注意到 *Fixed* 中已删除分析器的发现以及 *New* 中 Semgrep 的发现。（一些发现可能会显示不同的名称、描述和严重性，因为极狐GitLab 管理和编辑 Semgrep 规则集。）
3. 合并 MR，等待默认分支流水线运行。
4. 使用漏洞报告排除语言特定分析器不再检测到的结果。

#### 预览基于 Semgrep 的扫描

在极狐GitLab 管理的 SAST Stable CI/CD 模板更新之前，您可以看到基于 Semgrep 的扫描将如何在您的项目中工作。
我们建议您在合并请求中测试此更改，但继续在默认分支流水线配置中使用 Stable 模板。

在 15.3 版本中，我们激活了一个功能标志，将默认分支上的安全发现从其他分析器迁移到 Semgrep。
我们计划从 15.4 中的 Stable CI/CD 模板中删除已弃用的分析器。

预览即将对 CI/CD 配置进行的更改：

1. 打开 MR，从 Stable CI/CD 模板 `SAST.gitlab-ci.yaml` 切换到[最新模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.latest.gitlab-ci.yml)，`SAST.latest.gitlab-ci.yaml`。
    - 在 SaaS 上，直接使用最新模板：

      ```yaml
      include:
        template: 'SAST.latest.gitlab-ci.yaml'
      ```

    - 在私有化部署实例上，下载模板：

      ```yaml
      include:
        remote: 'https://jihulab.com/gitlab-cn/gitlab/-/raw/2851f4d5/lib/gitlab/ci/templates/Jobs/SAST.latest.gitlab-ci.yml'
      ```

1. 验证扫描作业在 MR 中是否成功。您会注意到 **Fixed** 中已删除分析器的结果，和 **New** 中 Semgrep 的结果。（一些发现可能会显示不同的名称、描述和严重性，因为极狐GitLab 管理和编辑 Semgrep 规则集。）
1. 关闭 MR。

<!--
To learn more about Stable and Latest templates, see documentation on [CI/CD template versioning](../../../development/cicd/templates.md#versioning).
-->

<a id="customize-analyzers"></a>

## 自定义分析器

在 `.gitlab-ci.yml` 文件中使用 [CI/CD 变量](index.md#available-cicd-variables)来自定义分析器的行为。

### 使用自定义 Docker 镜像

您可以使用自定义 Docker 镜像库而不是极狐GitLab 镜像库来托管分析器的镜像。

先决条件：

- 自定义 Docker 镜像库必须为所有官方分析器提供镜像。

NOTE:
此变量影响所有安全分析器，而不仅仅是 SAST 分析器。

要让极狐GitLab 从自定义 Docker 镜像库下载分析器的镜像，请使用 `SECURE_ANALYZERS_PREFIX` CI/CD 变量定义前缀。

例如，以下命令 SAST 拉取 `my-docker-registry/gitlab-images/bandit` 而不是 `registry.gitlab.com/security-products/bandit`：

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml

variables:
  SECURE_ANALYZERS_PREFIX: my-docker-registry/gitlab-images
```

### 禁用所有默认分析器

您可以禁用所有默认 SAST 分析器，仅启用[自定义分析器](#custom-analyzers)。

要禁用所有默认分析器，请在 `.gitlab-ci.yml` 文件中将 CI/CD 变量 `SAST_DISABLED` 设置为 `"true"`。

示例：

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml

variables:
  SAST_DISABLED: "true"
```

<a id="disable-specific-default-analyzers"></a>

### 禁用特定的默认分析器

分析器根据检测到的源代码语言自动运行。但是，您可以禁用选择分析器。

要禁用选择分析器，请将 CI/CD 变量 `SAST_EXCLUDED_ANALYZERS` 设置为以逗号分隔的字符串，列出要阻止运行的分析器。

例如，要禁用 `eslint` 分析器：

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml

variables:
  SAST_EXCLUDED_ANALYZERS: "eslint"
```

<a id="custom-analyzers"></a>

### 自定义分析器

您可以通过在 CI/CD 配置中定义作业来提供自己的分析器。为了与默认分析器保持一致，您应该将后缀 `-sast` 添加到自定义 SAST 作业中。

<!--
For more details on integrating a custom security scanner into GitLab, see [Security Scanner Integration](../../../development/integrations/secure.md).
-->

#### 示例自定义分析器

此示例显示如何添加基于 Docker 镜像 `my-docker-registry/analyzers/csharp` 的扫描作业。它运行脚本 `/analyzer run` 并输出一个 SAST 报告 `gl-sast-report.json`。

在 `.gitlab-ci.yml` 文件中定义以下内容：

```yaml
csharp-sast:
  image:
    name: "my-docker-registry/analyzers/csharp"
  script:
    - /analyzer run
  artifacts:
    reports:
      sast: gl-sast-report.json
```

## 分析器提供的数据

每个分析器都提供有关它检测到的漏洞的数据。下表详细列出了每个分析器的可用数据。这些工具提供的值是异构的，因此它们有时会标准化为通用值，例如 `severity` 和 `confidence`。

| 参数/工具                | Apex | Bandit<sup>1</sup> | Brakeman | ESLint security<sup>1</sup> | SpotBugs | Flawfinder | Gosec<sup>1</sup> | Kubesec Scanner | MobSF | NodeJsScan | PHP CS Security Audit | Security code Scan (.NET)<sup>1</sup> | Semgrep | Sobelow |
|--------------------------------|------|--------|----------|-----------------|----------|------------|-------|-----------------|-------|------------|-----------------------|---------------------------|---------|---------|
| Affected item（例如，class 或 package） | ✓ | ✗ | ✓ | ✗               | ✓        | ✓          | ✗     | ✓               | ✗     | ✗          | ✗                     | ✗                         | ✗       | ✗       |
| Confidence                     | ✗    | ✓      | ✓        | ✗               | ✓        | x          | ✓     | ✓               | ✗     | ✗          | ✗                     | ✗                         | ⚠       | ✓       |
| Description                    | ✓    | ✗      | ✗        | ✓               | ✓        | ✗          | ✗     | ✓               | ✓     | ✓          | ✗                     | ✗                         | ✓       | ✓       |
| End column                     | ✓    | ✗      | ✗        | ✓               | ✓        | ✗          | ✗     | ✗               | ✗     | ✗          | ✗                     | ✗                         | ✗       | ✗       |
| End line                       | ✓    | ✓      | ✗        | ✓               | ✓        | ✗          | ✗     | ✗               | ✗     | ✗          | ✗                     | ✗                         | ✗       | ✗       |
| External ID（例如，CVE） | ✗    | ✗      | ⚠        | ✗               | ⚠        | ✓          | ✗     | ✗               | ✗     | ✗          | ✗                     | ✗                         | ⚠       | ✗       |
| File                           | ✓    | ✓      | ✓        | ✓               | ✓        | ✓          | ✓     | ✓               | ✓     | ✓          | ✓                     | ✓                         | ✓       | ✓       |
| Internal doc/explanation       | ✓    | ⚠      | ✓        | ✗               | ✓        | ✗          | ✗     | ✗               | ✗     | ✗          | ✗                     | ✗                         | ✗       | ✓       |
| Internal ID                    | ✓    | ✓      | ✓        | ✓               | ✓        | ✓          | ✓     | ✗               | ✗     | ✗          | ✓                     | ✓                         | ✓       | ✓       |
| Severity                       | ✓    | ✓      | ✓        | ✓               | ✓        | ✓          | ✓     | ✓               | ✓     | ✓          | ✓                     | ✗                         | ⚠       | ✗       |
| Solution                       | ✓    | ✗      | ✗        | ✗               | ⚠        | ✓          | ✗     | ✗               | ✗     | ✗          | ✗                     | ✗                         | ⚠       | ✗       |
| Source code extract            | ✗    | ✓      | ✓        | ✓               | ✗        | ✓          | ✓     | ✗               | ✗     | ✗          | ✗                     | ✗                         | ✗       | ✗       |
| Start column                   | ✓    | ✗      | ✗        | ✓               | ✓        | ✓          | ✓     | ✗               | ✗     | ✗          | ✓                     | ✓                         | ✓       | ✗       |
| Start line                     | ✓    | ✓      | ✓        | ✓               | ✓        | ✓          | ✓     | ✗               | ✓     | ✓          | ✓                     | ✓                         | ✓       | ✓       |
| Title                          | ✓    | ✓      | ✓        | ✓               | ✓        | ✓          | ✓     | ✓               | ✓     | ✓          | ✓                     | ✓                         | ✓       | ✓       |
| URLs                           | ✓    | ✗      | ✓        | ✗               | ⚠        | ✗          | ⚠     | ✗               | ✗     | ✗          | ✗                     | ✗                         | ✗       | ✗       |

- ✓ => 数据可用。
- ⚠ => 数据可用，但部分可靠，或者必须从非结构化内容中提取。
- ✗ => 数据不可用，或者需要特定、低效或不可靠的逻辑来获取数据。

1. 这些分析器已达到结束支持状态。获取更多信息，查看 [SAST 分析器](#sast-analyzers)部分。
