---
stage: Secure
group: Static Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 静态应用程序安全测试（SAST） **(FREE)**

> 在 13.3 版本中，所有开源 (OSS) 分析器都从旗舰版迁移到了免费版。

<!--
NOTE:
The whitepaper ["A Seismic Shift in Application Security"](https://about.gitlab.com/resources/whitepaper-seismic-shift-application-security/)
explains how 4 of the top 6 attacks were application based. Download it to learn how to protect your
organization.
-->

如果您使用[极狐GitLab CI/CD](../../../ci/index.md)，您可以使用静态应用程序安全测试 (SAST) 检查源代码中的已知漏洞。您可以在任何级别的版本中运行 SAST 分析器。分析器将 JSON 格式的报告作为作业产物输出。

使用旗舰版，还可以处理 SAST 结果：

- 在合并请求中查看它们。
- 在审核工作流程中使用它们。
- 在安全仪表盘中查看它们。

有关更多详细信息，请参阅[功能摘要](#summary-of-features-per-tier)。

![SAST results shown in the MR widget](img/sast_results_in_mr_v14_0.png)

结果按漏洞的优先级排序：

<!-- vale gitlab.SubstitutionWarning = NO -->

1. Critical
1. High
1. Medium
1. Low
1. Info
1. Unknown

<!-- vale gitlab.SubstitutionWarning = YES -->

一个流水线由多个作业组成，包括 SAST 和 DAST 扫描。如果任何作业因任何原因未能完成，安全仪表盘不会显示 SAST 扫描仪输出。例如，SAST 作业完成但 DAST 作业失败，则安全仪表盘不会显示 SAST 结果。失败时，分析器会输出退出代码。

## 用例

- 您的代码在类中具有潜在危险属性，或者可能导致意外代码执行的不安全代码。
- 您的应用程序容易受到跨站点脚本 (XSS) 攻击，这些攻击可用于未经授权访问会话数据。

## 要求

SAST 在 `test` 阶段运行，默认情况下可用。如果在 `.gitlab-ci.yml` 文件中重新定义阶段，则需要 `test` 阶段。

要运行 SAST 作业，默认情况下，您需要带有 [`docker`](https://docs.gitlab.cn/runner/executors/docker.html) 或 [`kubernetes`](https://docs.gitlab.cn/runner/install/kubernetes.html) 执行器。
如果您在 JihuLab.com 上使用共享 runner，则默认启用此功能。

WARNING:
极狐GitLab SAST 分析器不支持在 Windows 或除 amd64 以外的任何 CPU 架构上运行。

WARNING:
如果您使用自己的 runner，请确保安装的 Docker 版本**不是** `19.03.0`。有关详细信息，请参阅[故障排除信息](#error-response-from-daemon-error-processing-tar-file-docker-tar-relocation-error)。

<a id="supported-languages-and-frameworks"></a>

## 支持的语言和框架

极狐GitLab SAST 支持扫描多种编程语言和框架。 一旦您[启用 SAST](#configuration)，即使您的项目使用多种语言，正确的分析器集也会自动运行。

<!--
Check the [SAST direction page](https://about.gitlab.com/direction/secure/static-analysis/sast/#language-support) to learn more about our plans for language support in SAST.
-->

| 语言/ 框架        | 扫描工具                                                                                                 | 引入版本                                                            |
|------------------------------------------------|-----------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|
| .NET Core<sup>3</sup>                                      | [Security Code Scan](https://security-code-scan.github.io)                                                | 11.0                                                                                    |
| .NET Framework<sup>3</sup>                     | [Security Code Scan](https://security-code-scan.github.io)                                                | 13.0                                                                                    |
| .NET (all versions, C# only)                   | [Semgrep](https://semgrep.dev)                                                                            | 15.4                                                                                    |
| Apex (Salesforce)                              | [PMD](https://pmd.github.io/pmd/index.html)                                                               | 12.1                                                                                    |
| C                                              | [Semgrep](https://semgrep.dev)                                                                            | 14.2                                                                                    |
| C/C++                                          | [Flawfinder](https://github.com/david-a-wheeler/flawfinder)                                               | 10.7                                                                                    |
| Elixir (Phoenix)                               | [Sobelow](https://github.com/nccgroup/sobelow)                                                            | 11.1                                                                                    |
| Go<sup>2</sup>                                             | [Gosec](https://github.com/securego/gosec)                                                                | 10.7                                                                                    |
| Go                                             | [Semgrep](https://semgrep.dev)                                                                            | 14.4                                                                                    |
| Groovy<sup>1</sup>                             | [SpotBugs](https://spotbugs.github.io/)，具有 [find-sec-bugs](https://find-sec-bugs.github.io/) 插件 | 11.3 (Gradle) & 11.9 (Maven, SBT)                                                  |
| Helm Charts                                    | [Kubesec](https://github.com/controlplaneio/kubesec)                                                      | 13.1                                                                                    |
| Java (any build system)                        | [Semgrep](https://semgrep.dev)                                                                            | 14.10                                                                                   |
| Java<sup>1, 2</sup>                               | [SpotBugs](https://spotbugs.github.io/)，具有 [find-sec-bugs](https://find-sec-bugs.github.io/) 插件 | 10.6 (Maven)、10.8 (Gradle) 和 11.9 (SBT)                                           |
| Java (Android)                                 | [MobSF (beta)](https://github.com/MobSF/Mobile-Security-Framework-MobSF)                                  | 13.5                                                                                    |
| JavaScript<sup>2</sup>                                     | [ESLint security plugin](https://github.com/nodesecurity/eslint-plugin-security)                          | 11.8                                                                                    |
| JavaScript                                     | [Semgrep](https://semgrep.dev)                                                                            | 13.10                                                                                   |
| Kotlin (Android)                               | [MobSF (beta)](https://github.com/MobSF/Mobile-Security-Framework-MobSF)                                  | 13.5                                                                                    |
| Kotlin (General)<sup>1</sup>                   | [SpotBugs](https://spotbugs.github.io/)，具有 [find-sec-bugs](https://find-sec-bugs.github.io/) 插件 | 13.11                                                                                   |
| Kubernetes manifests                           | [Kubesec](https://github.com/controlplaneio/kubesec)                                                      | 12.6                                                                                    |
| Node.js                                        | [NodeJsScan](https://github.com/ajinabraham/NodeJsScan)                                                   | 11.1                                                                                    |
| Objective-C (iOS)                              | [MobSF (beta)](https://github.com/MobSF/Mobile-Security-Framework-MobSF)                                  | 13.5                                                                                    |
| PHP                                            | [phpcs-security-audit](https://github.com/FloeDesignTechnologies/phpcs-security-audit)                    | 10.8                                                                                    |
| Python<sup>2</sup> | [bandit](https://github.com/PyCQA/bandit)                                                                 | 10.3                                                                                    |
| Python                                         | [Semgrep](https://semgrep.dev)                                                                            | 13.9                                                                                    |
| React<sup>2</sup>                                          | [ESLint react plugin](https://github.com/yannickcr/eslint-plugin-react)                                   | 12.5                                                                                    |
| React                                          | [Semgrep](https://semgrep.dev)                                                                            | 13.10                                                                                   |
| Ruby                                           | [brakeman](https://brakemanscanner.org)                                                                   | 13.9                                                                                    |
| Ruby on Rails                                  | [brakeman](https://brakemanscanner.org)                                                                   | 10.3                                                                                    |
| Scala (any build system)     | 使用 GitLab-managed 规则的 Semgrep     | 16.0                                                                                    |
| Scala<sup>1</sup>                              | [SpotBugs](https://spotbugs.github.io/)，具有 [find-sec-bugs](https://find-sec-bugs.github.io/) 插件 | 11.0（SBT）、11.9（Gradle、Maven）                                                 |
| Swift (iOS)                                    | [MobSF (beta)](https://github.com/MobSF/Mobile-Security-Framework-MobSF)                                  | 13.5                                                                                    |
| TypeScript<sup>2</sup>                                     | [ESLint security plugin](https://github.com/nodesecurity/eslint-plugin-security)                          | 11.9、13.2（合并时使用 Eslint） |
| TypeScript                                     | [Semgrep](https://semgrep.dev)                                                                            | 13.10                                                                                   |

1. 基于 SpotBugs 的分析器支持 [Gradle](https://gradle.org/)、[Maven](https://maven.apache.org/ ) 和 [SBT](https://www.scala-sbt.org/)。它还可以与 [Gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html)、[Grails](https://grails.org/) 和 [Maven 包装器](https://github.com/takari/maven-wrapper)。但是，当用于基于 [Ant](https://ant.apache.org/) 的项目时，SpotBugs 有局限性。我们建议对基于 Ant 的 Java 项目使用基于 Semgrep 的分析器。
1. 这些分析器在 16.0 版本中达到结束支持状态。
1. 安全代码扫描在 16.0 版本中达到结束支持状态。

<a id="multi-project-support"></a>

## 多项目支持

> 引入于 13.7 版本。

SAST 可以扫描包含多个项目的仓库。

以下分析器具有多项目支持：

- Bandit
- ESLint
- Gosec
- Kubesec
- NodeJsScan
- MobSF
- PMD
- Security Code Scan
- Semgrep
- SpotBugs
- Sobelow

### 为安全代码扫描启用多项目支持

安全代码扫描中的多项目支持需要仓库根目录中的解决方案 (`.sln`) 文件。有关解决方案格式的详细信息，请参阅 Microsoft [解决方案 (`.sln`) 文件](https://docs.microsoft.com/en-us/visualstudio/extensibility/internals/solution-dot-sln-file?view=vs-2019)。

<a id="false-positive-detection"></a>

### 误报检测 **(ULTIMATE)**

> - 对 Ruby 的支持引入于 14.2 版本。
> - 对 Go 的支持引入于 15.8 版本。

极狐GitLab SAST 可以在其他工具的输出中识别某些类型的误报结果。
这些结果在[漏洞报告](../vulnerability_report/index.md)和[漏洞页面](../vulnerabilities/index.md)中被标记为误报。

[支持的语言](#supported-languages-and-frameworks)和[分析器](analyzers.md)的子集提供了误报检测：

- Go，在基于 Semgrep 的分析器中
- Ruby，在基于 Brakeman 的分析器中

![SAST false-positives show in Vulnerability Pages](img/sast_vulnerability_page_fp_detection_v15_2.png)

<a id="advanced-vulnerability-tracking"></a>

### 高级漏洞跟踪 **(ULTIMATE)**

> 引入于 14.2 版本。

源代码易变；随着开发人员进行更改，源代码可能会在文件内或文件之间移动。
安全分析器可能已经报告了[漏洞报告](../vulnerability_report/index.md)中正在跟踪的漏洞。
这些漏洞与特定的有问题的代码片段相关联，以便可以找到并修复它们。
如果代码片段在移动时没有被可靠地跟踪，则漏洞管理会更加困难，因为可能会再次报告相同的漏洞。

SAST 使用先进的漏洞跟踪算法来更准确地识别由于重构或无关更改而在文件中移动的相同漏洞。

高级漏洞跟踪可用于[支持的语言](#supported-languages-and-frameworks)和[分析器](analyzers.md)的子集：

- C，仅在基于 Semgrep 的分析器中
- Go，仅在基于 Semgrep 的分析器中
- Java，仅在基于 Semgrep 的分析器中
- JavaScript，仅在基于 Semgrep 的分析器中
- Python，仅在基于 Semgrep 的分析器中
- Ruby，在基于 Brakeman 的分析器中

<!--
Support for more languages and analyzers is tracked in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/5144).

For more information, see the confidential project `https://gitlab.com/gitlab-org/security-products/post-analyzers/tracking-calculator`. The content of this project is available only to GitLab team members.
-->

<a id="automatic-vulnerability-resolution"></a>

## 自动解决漏洞

> - 引入于 15.9 版本，[项目级功能标志](../../../administration/feature_flags.md)为 `sec_mark_dropped_findings_as_resolved`。
> - 默认启用于 15.10 版本。在 SaaS 版上，如果您需要为您的项目禁用此功能，[请联系技术支持](https://gitlab.cn/support/)。

为了帮助您关注仍然相关的漏洞，极狐GitLab SAST 在以下情况自动[解决](../vulnerabilities/index.md#vulnerability-status-values)漏洞：

- 您[禁用了预定义规则](customize_rulesets.md#disable-predefined-rules)。
- 我们从默认规则集中删除了一条规则。

自动解决仅适用于基于 Semgrep 的分析器的结果。
漏洞管理系统会在自动解决的漏洞上留下评论，因此您仍然拥有漏洞的历史记录。

如果您稍后重新启用该规则，将重新打开发现（findings）以便进行分类。

## 支持的发行版

默认扫描器镜像是在基本 Alpine 镜像的基础上构建的，以确保大小和可维护性。

### FIPS-enabled 镜像

> 引入于 14.10 版本。

极狐GitLab 提供了一个基于 [Red Hat UBI](https://www.redhat.com/en/blog/introducing-red-hat-universal-base-image) 基础镜像的镜像版本，它使用 FIPS 140-验证的密码模块。要使用 FIPS-enabled 的镜像，您可以：

- 将 `SAST_IMAGE_SUFFIX` 设置为 `-fips`。
- 将 `-fips` 扩展添加到默认图像名称。

```yaml
variables:
  SAST_IMAGE_SUFFIX: '-fips'

include:
  - template: Security/SAST.gitlab-ci.yml
```

FIPS-compliant 镜像仅适用于基于 Semgrep 的分析器。

要以 FIPS-compliant 的方式使用 SAST，您必须[排除其他分析器运行](analyzers.md#customize-analyzers)。

<a id="summary-of-features-per-tier"></a>

#### 每个级别的功能摘要

不同的[产品级别](https://gitlab.cn/pricing/)提供不同的功能，如下表所示：

| 功能                                                      | 免费版和专业版   | 旗舰版        |
|:----------------------------------------------------------------|:--------------------|:-------------------|
| 使用[适当的分析器](#supported-languages-and-frameworks)自动扫描代码 | **{check-circle}**  | **{check-circle}** |

| [配置 SAST 扫描器](#configuration)                       | **{check-circle}**  | **{check-circle}** |
| [自定义 SAST 设置](#available-cicd-variables)            | **{check-circle}**  | **{check-circle}** |
| 下载 [JSON 报告](#reports-json-format)                    | **{check-circle}**  | **{check-circle}** |
| 在合并请求部件中查看新的发现                        | **{dotted-circle}** | **{check-circle}** |
| [管理漏洞](../vulnerabilities/index.md)           | **{dotted-circle}** | **{check-circle}** |
| [访问安全仪表盘](../security_dashboard/index.md) | **{dotted-circle}** | **{check-circle}** |
| [在 UI 中配置 SAST](#configure-sast-in-the-ui)           | **{dotted-circle}** | **{check-circle}** |
| [自定义 SAST 规则集](customize_rulesets.md)                  | **{dotted-circle}** | **{check-circle}** |
| [检测误报](#false-positive-detection)             | **{dotted-circle}** | **{check-circle}** |
| [跟踪移动的漏洞](#advanced-vulnerability-tracking) | **{dotted-circle}** | **{check-circle}** |

<!--
## Contribute your scanner

The [Security Scanner Integration](../../../development/integrations/secure.md) documentation explains how to integrate other security scanners into GitLab.
-->

<a id="configuration"></a>

## 配置

SAST 扫描在您的 CI/CD 流水线中运行。
当您将极狐GitLab 管理的 CI/CD 模板添加到您的流水线时，正确的 [SAST 分析器](analyzers.md)会自动扫描您的代码并将结果保存为 [SAST 报告产物](../../../ci/yaml/artifacts_reports.md#artifactsreportssast)。

要为项目配置 SAST，您可以：

- 使用 [Auto DevOps](../../../topics/autodevops/index.md) 提供的 [Auto SAST](../../../topics/autodevops/stages.md#auto-sast)。
- [在 CI/CD YAML 中配置 SAST](#configure-sast-in-your-cicd-yaml)。
- [使用 UI 配置 SAST](#configure-sast-in-the-ui)（引入于 13.3 版本）。

您可以通过[强制扫描执行](../index.md#enforce-scan-execution)，在许多项目中启用 SAST。

<a id="configure-sast-in-your-cicd-yaml"></a>

### 在 CI/CD YAML 中配置 SAST

要启用 SAST，您必须[包括](../../../ci/yaml/index.md#includetemplate) [`SAST.gitlab-ci.yml` 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml)，模板作为极狐GitLab 安装实例的一部分提供。

将以下内容添加到您的 `.gitlab-ci.yml` 文件中：

```yaml
include:
  - template: Jobs/SAST.gitlab-ci.yml
```

包含的模板在您的 CI/CD 流水线中创建 SAST 作业，并扫描您项目的源代码以查找可能的漏洞。

结果保存为 [SAST 报告产物](../../../ci/yaml/artifacts_reports.md#artifactsreportssast)，下载时，您始终会收到可用的最新 SAST 产物。

<a id="configure-sast-by-using-the-ui"></a>

### 使用 UI 配置 SAST

您可以使用默认设置或自定义在 UI 中启用和配置 SAST。
您可以使用的方法取决于您的极狐GitLab 产品级别。

- [在 UI 中使用自定义配置 SAST](#configure-sast-in-the-ui-with-customizations)。 **(ULTIMATE)**
- [在 UI 中仅使用默认配置 SAST](#configure-sast-in-the-ui-with-default-settings-only)。

<a id="configure-sast-with-customizations"></a>

#### 使用自定义配置 SAST **(ULTIMATE)**

> - 引入于 13.3 版本。
> - 优化于 13.4 版本。
> - 优化于 13.5 版本。

NOTE:
配置工具在没有现有 `.gitlab-ci.yml` 文件或最小配置文件的情况下效果最佳。如果您有一个复杂的极狐GitLab 配置文件，它可能无法解析成功，并且可能会出现错误。

要使用自定义启用和配置 SAST：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **安全与合规** > **配置**。
1. 如果项目没有 `.gitlab-ci.yml` 文件，在静态应用程序安全测试（SAST）行选择 **启用 SAST**，否则选择 **配置 SAST**。
1. 输入自定义 SAST 值。

    自定义值存储在 `.gitlab-ci.yml` 文件中。对于不在 SAST 配置页面中的 CI/CD 变量，它们的值继承自 SAST 模板。

1. 或者，展开 **SAST 分析器** 部分，选择单个 [SAST 分析器](analyzers.md)并输入自定义分析器值。
1. 选择 **创建合并请求**。
1. 查看并合并合并请求。

流水线现在包括一个 SAST 作业。

<a id="configure-sast-with-default-settings-only"></a>

#### 仅使用默认设置 SAST

> 引入于 13.9 版本。

NOTE:
配置工具在没有现有 `.gitlab-ci.yml` 文件或最小配置文件的情况下效果最佳。如果您有一个复杂的极狐GitLab 配置文件，它可能无法解析成功，并且可能会出现错误。

使用默认设置启用和配置 SAST：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **安全与合规** > **配置**。
1. 在 SAST 部分中，选择 **使用合并请求配置**。
1. 查看并合并合并请求，来启用 SAST。

流水线现在包括一个 SAST 作业。

<a id="overriding-sast-jobs"></a>

### 覆盖 SAST 作业

WARNING:
从 13.0 版本开始，不再支持使用 [`only` 和 `except`](../../../ci/yaml/index.md#only--except)。覆盖模板时，您必须改用 [`rules`](../../../ci/yaml/index.md#rules)。

要覆盖作业定义（例如，更改 `variables` 或 `dependencies` 等属性），请声明与要覆盖的 SAST 作业同名的作业。将此新作业放在模板包含之后，并在其下指定任何其他键。例如，为 `spotbugs` 分析器启用 `FAIL_NEVER`：

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml

spotbugs-sast:
  variables:
    FAIL_NEVER: 1
```

<a id="pinning-to-minor-image-version"></a>

### 固定到镜像小版本

极狐GitLab 管理的 CI/CD 模板指定一个主要版本，并自动在该主要版本中提取最新的分析器版本。

在某些情况下，您可能需要使用特定版本。
例如，您可能需要避免在以后的版本中出现回归。

要覆盖自动更新行为，请在包含的 [`SAST.gitlab-ci.yml` 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml)中设置 `SAST_ANALYZER_IMAGE_TAG` CI/CD 变量。

仅在特定作业中设置此变量。
如果您将其设置在顶层，则您设置的版本将用于其他 SAST 分析器。

您可以将标签设置为：

- 主要版本，例如 `3`。您的流水线将使用在此主要版本中发布的任何次要更新或补丁更新。
- 次要版本，例如 `3.7`。您的流水线将使用在此次要版本中发布的任何补丁更新。
- 补丁版本，比如 `3.7.0`。您的流水线不会收到任何更新。

此示例使用特定次要版本的 `semgrep` 分析器和特定补丁版本的 `brakeman` 分析器：

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml

semgrep-sast:
  variables:
    SAST_ANALYZER_IMAGE_TAG: "3.7"

brakeman-sast:
  variables:
    SAST_ANALYZER_IMAGE_TAG: "3.1.1"
```

### 使用 CI/CD 变量为私有仓库传递凭据

一些分析器需要下载项目的依赖项才能执行分析。反过来，此类依赖项可能存在于私有 Git 仓库中，因此需要用户名和密码等凭据才能下载它们。根据分析器的不同，可以通过[自定义 CI/CD 变量](#custom-cicd-variables)向其提供此类凭据。

#### 使用 CI/CD 变量将用户名和密码传递给私有 Go 仓库

如果您的 Go 项目依赖于私有模块，请参阅[从私有项目中获取模块](../../packages/go_proxy/index.md#fetch-modules-from-private-projects)，了解如何通过 HTTPS 提供身份验证。

要通过 `~/.netrc` 指定凭据，请提供包含以下内容的 `before_script`：

```yaml
gosec-sast:
  before_script:
    - |
      cat <<EOF > ~/.netrc
      machine gitlab.com
      login $CI_DEPLOY_USER
      password $CI_DEPLOY_PASSWORD
      EOF
```

#### 使用 CI/CD 变量将用户名和密码传递给私有 Maven 仓库

如果您的私有 Maven 仓库需要登录凭据，您可以使用 `MAVEN_CLI_OPTS` CI/CD 变量。

<!--
Read more on [how to use private Maven repositories](../index.md#using-private-maven-repositories).
-->

### 启用 Kubesec 分析器

您需要将 `SCAN_KUBERNETES_MANIFESTS` 设置为 `"true"`，来启用 Kubesec 分析器。在 `.gitlab-ci.yml` 中，定义：

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml

variables:
  SCAN_KUBERNETES_MANIFESTS: "true"
```

<a id="pre-compilation"></a>

### 预编译

大多数 SAST 分析器直接扫描您的源代码，而无需先编译它。
但是，出于技术原因，一些分析器只能扫描编译后的代码。

默认情况下，这些分析器会自动尝试获取依赖项并编译您的代码，以便对其进行扫描。如果出现以下情况，自动编译可能会失败：

- 您的项目需要自定义构建配置。
- 您使用分析器中未内置的语言版本。

要解决这些问题，您可以跳过分析器的编译步骤，而是直接提供流水线早期阶段的产物。这种策略称为*预编译*。

预编译可用于支持 `COMPILE` CI/CD 变量的分析器。
有关当前列表，请参阅[分析器设置](#analyzer-settings)。

要使用预编译：

1. 将项目的依赖输出到项目工作目录中的一个目录，然后通过[设置 `artifacts:paths` 配置](../../../ci/yaml/index.md#artifactspaths)，将该目录保存为产物。
1. 向分析器提供 `COMPILE: "false"` CI/CD 变量，禁用自动编译。
1. 将您的编译阶段添加为分析器作业的依赖项。

要允许分析器识别编译的产物，您必须明确指定 vendored 目录的路径。
此配置可能因分析仪而异。对于 Maven 项目，您可以使用 `MAVEN_REPO_PATH`。
有关可用选项的完整列表，请参阅[分析器设置](#analyzer-settings)。

以下示例预编译了一个 Maven 项目并将其提供给 SpotBugs SAST 分析器：

```yaml
stages:
  - build
  - test

include:
  - template: Security/SAST.gitlab-ci.yml

build:
  image: maven:3.6-jdk-8-slim
  stage: build
  script:
    - mvn package -Dmaven.repo.local=./.m2/repository
  artifacts:
    paths:
      - .m2/
      - target/

spotbugs-sast:
  dependencies:
    - build
  variables:
    MAVEN_REPO_PATH: $CI_PROJECT_DIR/.m2/repository
    COMPILE: "false"
  artifacts:
    reports:
      sast: gl-sast-report.json
```

<a id="available-cicd-variables"></a>

### 可用的 CI/CD 变量

可以使用 `.gitlab-ci.yml` 中的 [`variables`](../../../ci/yaml/index.md#variables) 参数配置 SAST。

WARNING:
在将这些更改合并到默认分支之前，应在合并请求中测试极狐GitLab 安全扫描工具的所有自定义。不这样做会产生意想不到的结果，包括大量误报。

以下示例包含用于将 `SAST_GOSEC_LEVEL` 变量覆盖为 `2` 的 SAST 模板。模板是[之前评估的](../../../ci/yaml/index.md#include)流水线配置，因此最后提及的变量优先。

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml

variables:
  SAST_GOSEC_LEVEL: 2
```

<a id="logging-level"></a>

#### 日志级别

> 引入于 13.1 版本。

要控制日志的详细程度，请设置 `SECURE_LOG_LEVEL` 环境变量。输出此日志级别或更高级别的消息。

从最高到最低严重性，日志记录级别是：

- `fatal`
- `error`
- `warn`
- `info`（默认）
- `debug`

#### 自定义证书颁发机构

要信任自定义证书颁发机构，请将 `ADDITIONAL_CA_CERT_BUNDLE` 变量设置为您希望在 SAST 环境中信任的 CA 证书包。`ADDITIONAL_CA_CERT_BUNDLE` 值应包含 [X.509 PEM 公钥证书的文本表示形式](https://www.rfc-editor.org/rfc/rfc7468#section-5.1)。例如，要在 `.gitlab-ci.yml` 文件中配置此值，请使用以下命令：

```yaml
variables:
  ADDITIONAL_CA_CERT_BUNDLE: |
      -----BEGIN CERTIFICATE-----
      MIIGqTCCBJGgAwIBAgIQI7AVxxVwg2kch4d56XNdDjANBgkqhkiG9w0BAQsFADCB
      ...
      jWgmPqF3vUbZE0EyScetPJquRFRKIesyJuBFMAs=
      -----END CERTIFICATE-----
```

`ADDITIONAL_CA_CERT_BUNDLE` 值也可以配置为 [UI 中的自定义变量](../../../ci/variables/index.md#for-a-project)，或者配置为 `file`，它需要证书的路径；或者作为变量，它需要证书的文本表示。

#### Docker 镜像

以下是 Docker 镜像相关的 CI/CD 变量。

| CI/CD 变量           | 描述                                                                                                                           |
|---------------------------|---------------------------------------------------------------------------------------------------------------------------------------|
| `SECURE_ANALYZERS_PREFIX` | 覆盖提供默认镜像（代理）的 Docker 镜像库的名称。阅读有关[自定义分析器](analyzers.md)的更多信息。 |
| `SAST_EXCLUDED_ANALYZERS` | 不应运行的默认镜像的名称。阅读有关[自定义分析器](analyzers.md)的更多信息。                                 |
| `SAST_ANALYZER_IMAGE_TAG` | 覆盖默认版本的分析器镜像。阅读更多关于[固定分析器镜像版本](#pinning-to-minor-image-version)。                                 |
| `SAST_IMAGE_SUFFIX`       | 添加到镜像名称的后缀。如果设置为 `-fips`，则使用 `FIPS-enabled` 镜像进行扫描。有关详细信息，请参阅[FIPS-enabled 镜像](#fips-enabled-images)。引入于 14.10 版本。 |

#### 漏洞过滤器

一些分析器可以过滤掉特定阈值下的漏洞。

| CI/CD 变量               | 默认值            | 描述                                                                                                                                                                                                                 |
|------------------------------|--------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `SAST_EXCLUDED_PATHS`        | `spec, test, tests, tmp` | 根据路径从输出中排除漏洞。这是一个逗号分隔的模式列表。Patterns 可以是 glob（有关支持的 patterns，请参阅 [`doublestar.Match`](https://pkg.go.dev/github.com/bmatcuk/doublestar/v4@v4.0.2#Match)）、文件或文件夹路径（例如，`doc、spec`）。父目录也匹配模式。您可能需要排除构建工具使用的临时目录，因为这些可能会产生误报。要排除路径，请复制并粘贴默认排除路径，然后**添加**您自己的要排除的路径。如果您未指定默认排除路径，您将覆盖默认值，并且只有您指定的路径将从 SAST 扫描中排除。 |
| `SEARCH_MAX_DEPTH`           | 4                        | SAST 搜索仓库，检测使用的编程语言，并选择匹配的分析器。设置 `SEARCH_MAX_DEPTH` 的值来指定搜索阶段应该跨越多少个目录级别。选择分析器后，将分析整个仓库。 |
| `SAST_BANDIT_EXCLUDED_PATHS` |                          | 要从扫描中排除的路径的逗号分隔列表。使用 Python 的 [`fnmatch` 语法](https://docs.python.org/2/library/fnmatch.html)；例如：`'*/tests/*, */venv/*'`。                                                  |
| `SAST_BRAKEMAN_LEVEL`        | 1                        | 在给定的置信水平下忽略 Brakeman 漏洞。整数，1=低 3=高。                                                                                                                                        |
| `SAST_FLAWFINDER_LEVEL`      | 1                        | 忽略给定风险级别下的 Flawfinder 漏洞。整数，0=无风险，5=高风险。                                                                                                                                  |
| `SAST_GOSEC_LEVEL`           | 0                        | 在给定的置信水平下忽略 Gosec 漏洞。整数，0=未定义，1=低，2=中，3=高。                                                                                                                   |

<a id="analyzer-settings"></a>

#### 分析器设置

一些分析器可以使用 CI/CD 变量进行自定义。

| CI/CD 变量              | 分析器   | 描述                                                                                                                                                                                                                        |
|-----------------------------|------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `SCAN_KUBERNETES_MANIFESTS` | Kubesec    | 设置为 `true`，扫描 Kubernetes 清单。                                                                                                                                                                                      |
| `KUBESEC_HELM_CHARTS_PATH`  | Kubesec    | `helm` 用于生成 `kubesec` 扫描的 Kubernetes 清单的 Helm chart 的可选路径。 如果定义了依赖项，则应该在 `before_script` 中运行 `helm dependency build` 以获取必要的依赖项。 |
| `KUBESEC_HELM_OPTIONS`      | Kubesec    | `helm` 可执行文件的附加参数。                                                                                                                                                                                    |
| `COMPILE`                   | Gosec, SpotBugs   | 设置为 `false` 以禁用项目编译和依赖项获取。`SpotBugs` 分析器引入于 13.1 版本，`Gosec` 分析器引入于 14.0 版本。  |
| `ANT_HOME`                  | SpotBugs   | `ANT_HOME` 变量。                                                                                                                                                                                                           |
| `ANT_PATH`                  | SpotBugs   | `ant` 可执行文件的路径。                                                                                                                                                                                                     |
| `GRADLE_PATH`               | SpotBugs   | `gradle` 可执行文件的路径。                                                                                                                                                                                                   |
| `JAVA_OPTS`                 | SpotBugs   | `java` 可执行文件的附加参数。                                                                                                                                                                                    |
| `JAVA_PATH`                 | SpotBugs   | `java` 可执行文件的路径。                                                                                                                                                                                                     |
| `SAST_JAVA_VERSION`         | SpotBugs   | 使用哪个 Java 版本。从 15.0 版本开始，支持的版本是 `11` 和 `17`（默认）。在 15.0 版本之前，支持的版本是 `8`（默认）和 `11`。     |
| `MAVEN_CLI_OPTS`            | SpotBugs   | `mvn` 或 `mvnw` 可执行文件的附加参数。                                                                                                                                                                           |
| `MAVEN_PATH`                | SpotBugs   | `mvn` 可执行文件的路径。                                                                                                                                                                                                      |
| `MAVEN_REPO_PATH`           | SpotBugs   | Maven 本地仓库的路径（`maven.repo.local` 属性的快捷方式）。                                                                                                                                                 |
| `SBT_PATH`                  | SpotBugs   | `sbt` 可执行文件的路径。                                                                                                                                                                                                     |
| `FAIL_NEVER`                | SpotBugs   | 设置为 `1`，可以忽略编译失败。                                                                                                                                                                                          |
| `SAST_GOSEC_CONFIG`         | Gosec      | **{warning}** **删除于** 14.0 版本 - 改用自定义规则集。Gosec 的配置路径（可选）。                                                                                                                                                                                       |
| `PHPCS_SECURITY_AUDIT_PHP_EXTENSIONS` | phpcs-security-audit | 逗号分隔的附加 PHP 扩展列表。                                                                                                                                                             |
| `SAST_DISABLE_BABEL`        | NodeJsScan | **{warning}** **删除于** 13.5 版本。 |
| `SAST_SEMGREP_METRICS` | Semgrep | 设置为 `false`，禁用向 [r2c](https://semgrep.dev) 发送匿名扫描指标。默认值：`true`。      |
| `SAST_SCANNER_ALLOWED_CLI_OPTS`        | Semgrep | 运行扫描操作时传递给底层安全扫描器的 CLI 选项（带有值或标志的参数）。仅接受一组受限制的[选项](#security-scanner-configuration)。使用空格或等号 (`=`) 字符分隔 CLI 选项及其值，例如：`name1 value1` 或 `name1=value1`。多个选项必须用空格分隔，例如：`name1 value1 name2 value2`。引入于 15.3 版本。 |

#### 安全扫描器配置

SAST 分析器在内部使用 OSS 安全扫描器来执行分析。我们为安全扫描程序设置了推荐配置，这样您就不必担心调整它们。但是，在极少数情况下，我们的默认扫描器配置不符合您的要求。

要允许对扫描器行为进行一些自定义，您可以向底层扫描器添加一组受限制的标志。在 `SAST_SCANNER_ALLOWED_CLI_OPTS` CI/CD 变量中指定标志。 这些标志被添加到扫描器的 CLI 选项中。

| 分析器                                                                     | CLI 选项         | 描述 |
|------------------------------------------------------------------------------|--------------------|------------------------------------------------------------------------------|
| Semgrep | `--max-memory`     | 设置对单个文件运行规则时要使用的最大系统内存。以 MB 为单位。 |
| Flawfinder | `--neverignore` | 永远不要忽略安全问题，即使在评论中有忽略指令。添加此选项可能会导致分析器检测到无法自动解决的其他漏洞发现。 |

<a id="custom-cicd-variables"></a>

#### 自定义 CI/CD 变量

除了上述 SAST 配置 CI/CD 变量之外，如果使用 [SAST 供应模板](#configuration)，所有[自定义变量](../../../ci/variables/index.md#define-a-cicd-variable-in-the-ui)都会传播到底层 SAST 分析器镜像。

NOTE:
在 13.3 及更早版本中，名称以以下前缀开头的变量**不会**传播到分析器容器或 SAST Docker 容器：`DOCKER_`、`CI`、`GITLAB_`、`FF_`、`HOME` 、`PWD`、`OLDPWD`、`PATH`、`SHLVL`、`HOSTNAME`。

<!--
### 实验功能

You can receive early access to experimental features. Experimental features might be added,
removed, or promoted to regular features at any time.

Experimental features available are:

- Enable scanning of iOS and Android apps using the [MobSF analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/mobsf/).

#### Enable experimental features

To enable experimental features, add the following to your `.gitlab-ci.yml` file:

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml

variables:
  SAST_EXPERIMENTAL_FEATURES: "true"
```
-->

<a id="reports-json-format"></a>

## 报告 JSON 格式

SAST 以 JSON 格式输出报告文件。报告文件包含所有发现的漏洞的详细信息。
要下载报告文件，您可以：

- 从 CI/CD 流水线页面下载文件。
- 在合并请求的流水线选项卡中，将 [`artifacts: paths`](../../../ci/yaml/index.md#artifactspaths) 设置为 `gl-sast-report.json`。

有关信息，请参阅[下载作业产物](../../../ci/jobs/job_artifacts.md#download-job-artifacts)。

<!--
For details of the report file's schema, see
[SAST report file schema](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/sast-report-format.json).

For an example SAST report file, see [`gl-secret-detection-report.json`](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/qa/expect/secrets/gl-secret-detection-report.json) example.

## Running SAST in an offline environment

For self-managed GitLab instances in an environment with limited, restricted, or intermittent access
to external resources through the internet, some adjustments are required for the SAST job to
run successfully. For more information, see [Offline environments](../offline_deployments/index.md).

### Requirements for offline SAST

To use SAST in an offline environment, you need:

- GitLab Runner with the [`docker` or `kubernetes` executor](#requirements).
- A Docker Container Registry with locally available copies of SAST [analyzer](https://gitlab.com/gitlab-org/security-products/analyzers) images.
- Configure certificate checking of packages (optional).

GitLab Runner has a [default `pull_policy` of `always`](https://docs.gitlab.com/runner/executors/docker.html#using-the-always-pull-policy),
meaning the runner tries to pull Docker images from the GitLab container registry even if a local
copy is available. The GitLab Runner [`pull_policy` can be set to `if-not-present`](https://docs.gitlab.com/runner/executors/docker.html#using-the-if-not-present-pull-policy)
in an offline environment if you prefer using only locally available Docker images. However, we
recommend keeping the pull policy setting to `always` if not in an offline environment, as this
enables the use of updated scanners in your CI/CD pipelines.

### Make GitLab SAST analyzer images available inside your Docker registry

For SAST with all [supported languages and frameworks](#supported-languages-and-frameworks),
import the following default SAST analyzer images from `registry.gitlab.com` into your
[local Docker container registry](../../packages/container_registry/index.md):

```plaintext
registry.gitlab.com/security-products/bandit:2
registry.gitlab.com/security-products/brakeman:2
registry.gitlab.com/security-products/eslint:2
registry.gitlab.com/security-products/flawfinder:2
registry.gitlab.com/security-products/gosec:3
registry.gitlab.com/security-products/kubesec:2
registry.gitlab.com/security-products/nodejs-scan:2
registry.gitlab.com/security-products/phpcs-security-audit:2
registry.gitlab.com/security-products/pmd-apex:2
registry.gitlab.com/security-products/security-code-scan:2
registry.gitlab.com/security-products/semgrep:2
registry.gitlab.com/security-products/sobelow:2
registry.gitlab.com/security-products/spotbugs:2
```

The process for importing Docker images into a local offline Docker registry depends on
**your network security policy**. Please consult your IT staff to find an accepted and approved
process by which external resources can be imported or temporarily accessed. These scanners are [periodically updated](../index.md#vulnerability-scanner-maintenance)
with new definitions, and you may be able to make occasional updates on your own.

For details on saving and transporting Docker images as a file, see Docker's documentation on
[`docker save`](https://docs.docker.com/engine/reference/commandline/save/), [`docker load`](https://docs.docker.com/engine/reference/commandline/load/),
[`docker export`](https://docs.docker.com/engine/reference/commandline/export/), and [`docker import`](https://docs.docker.com/engine/reference/commandline/import/).

#### If support for Custom Certificate Authorities are needed

Support for custom certificate authorities was introduced in the following versions.

| Analyzer               | Version                                                                                                    |
| --------               | -------                                                                                                    |
| `bandit`               | [v2.3.0](https://gitlab.com/gitlab-org/security-products/analyzers/bandit/-/releases/v2.3.0)               |
| `brakeman`             | [v2.1.0](https://gitlab.com/gitlab-org/security-products/analyzers/brakeman/-/releases/v2.1.0)             |
| `eslint`               | [v2.9.2](https://gitlab.com/gitlab-org/security-products/analyzers/eslint/-/releases/v2.9.2)               |
| `flawfinder`           | [v2.3.0](https://gitlab.com/gitlab-org/security-products/analyzers/flawfinder/-/releases/v2.3.0)           |
| `gosec`                | [v2.5.0](https://gitlab.com/gitlab-org/security-products/analyzers/gosec/-/releases/v2.5.0)                |
| `kubesec`              | [v2.1.0](https://gitlab.com/gitlab-org/security-products/analyzers/kubesec/-/releases/v2.1.0)              |
| `nodejs-scan`          | [v2.9.5](https://gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/-/releases/v2.9.5)          |
| `phpcs-security-audit` | [v2.8.2](https://gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit/-/releases/v2.8.2) |
| `pmd-apex`             | [v2.1.0](https://gitlab.com/gitlab-org/security-products/analyzers/pmd-apex/-/releases/v2.1.0)             |
| `security-code-scan`   | [v2.7.3](https://gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/-/releases/v2.7.3)   |
| `semgrep`              | [v0.0.1](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/releases/v0.0.1)              |
| `sobelow`              | [v2.2.0](https://gitlab.com/gitlab-org/security-products/analyzers/sobelow/-/releases/v2.2.0)              |
| `spotbugs`             | [v2.7.1](https://gitlab.com/gitlab-org/security-products/analyzers/spotbugs/-/releases/v2.7.1)             |

### Set SAST CI/CD variables to use local SAST analyzers

Add the following configuration to your `.gitlab-ci.yml` file. You must replace
`SECURE_ANALYZERS_PREFIX` to refer to your local Docker container registry:

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml

variables:
  SECURE_ANALYZERS_PREFIX: "localhost:5000/analyzers"
```

The SAST job should now use local copies of the SAST analyzers to scan your code and generate
security reports without requiring internet access.

### Configure certificate checking of packages

If a SAST job invokes a package manager, you must configure its certificate verification. In an
offline environment, certificate verification with an external source is not possible. Either use a
self-signed certificate or disable certificate verification. Refer to the package manager's
documentation for instructions.
-->

<a id="running-sast-in-selinux"></a>

## 在 SELinux 中运行 SAST

默认情况下，托管在 SELinux 上的实例支持 SAST 分析器。在[被覆盖的 SAST 作业](#overriding-sast-jobs)中添加 `before_script` 可能不起作用，因为托管在 SELinux 上的 runner 权限受限。

## 故障排除

### SAST 调试日志记录

将全局 CI 变量中的[安全扫描器日志详细程度](#logging-level)调整到 `debug`，以帮助排除 SAST 作业的故障。

```yaml
variables:
  SECURE_LOG_LEVEL: "debug"
```

<!--
### Pipeline errors related to changes in the GitLab-managed CI/CD template

The [GitLab-managed SAST CI/CD template](#configure-sast-manually) controls which [analyzer](analyzers.md) jobs run and how they're configured. While using the template, you might experience a job failure or other pipeline error. For example, you might:

- See an error message like `'<your job>' needs 'spotbugs-sast' job, but 'spotbugs-sast' is not in any previous stage` when you view an affected pipeline.
- Experience another type of unexpected issue with your CI/CD pipeline configuration.

If you're experiencing a job failure or seeing a SAST-related `yaml invalid` pipeline status, you can temporarily revert to an older version of the template so your pipelines keep working while you investigate the issue. To use an older version of the template, change the existing `include` statement in your CI/CD YAML file to refer to a specific template version, such as `v15.3.3-ee`:

```yaml
include:
  remote: 'https://gitlab.com/gitlab-org/gitlab/-/raw/v15.3.3-ee/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml'
```

If your GitLab instance has limited network connectivity, you can also download the file and host it elsewhere.

We recommend that you only use this solution temporarily and that you return to [the standard template](#configure-sast-manually) as soon as possible.

### Errors in a specific analyzer job

GitLab SAST [analyzers](analyzers.md) are released as container images.
If you're seeing a new error that doesn't appear to be related to [the GitLab-managed SAST CI/CD template](#configure-sast-manually) or changes in your own project, you can try [pinning the affected analyzer to a specific older version](#pinning-to-minor-image-version).

Each [analyzer project](analyzers.md#sast-analyzers) has a `CHANGELOG.md` file listing the changes made in each available version.
-->

### 作业日志中的 `exec /bin/sh: exec format error` 消息

极狐GitLab SAST 分析器[仅支持](#requirements)在 `amd64` CPU 架构上运行。
此消息表明该作业正在不同的体系结构上运行，例如 `arm`。

<a id="error-response-from-daemon-error-processing-tar-file-docker-tar-relocation-error"></a>

### `Error response from daemon: error processing tar file: docker-tar: relocation error`

当运行 SAST 作业的 Docker 版本为 `19.03.0` 时会出现此错误。考虑更新到 Docker `19.03.1` 或更高版本。旧版本不受影响。<!--Read more in
[this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/13830#note_211354992 "Current SAST container fails").-->

### 收到警告信息 `gl-sast-report.json: no matching files`

有关这方面的信息，请参阅[一般应用程序安全故障排除部分](../../../ci/jobs/job_artifacts_troubleshooting.md#error-message-no-files-to-upload)。

<!--
### Error: `sast is used for configuration only, and its script should not be executed`

For information on this, see the [GitLab Secure troubleshooting section](../index.md#error-job-is-used-for-configuration-only-and-its-script-should-not-be-executed).
-->

### 使用 `rules:exists` 的限制

[SAST CI 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml)使用`rules:exists`参数。出于性能原因，针对给定的 glob 模式进行最大匹配数。 果匹配数超过最大值，则 `rules:exists` 参数返回 `true`。根据仓库中的文件数量，即使扫描程序不支持您的项目，也可能会触发 SAST 作业。有关此问题的更多详细信息，请参阅 [`rules:exists` 文档](../../../ci/yaml/index.md#rulesexists)。

### SpotBugs UTF-8 无法映射的字符错误

当 SpotBugs 构建中未启用 UTF-8 编码并且源代码中有 UTF-8 字符时，会发生这些错误。要修复此错误，请为项目的构建工具启用 UTF-8。

对于 Gradle 构建，将以下内容添加到您的 `build.gradle` 文件中：

```gradle
compileJava.options.encoding = 'UTF-8'
tasks.withType(JavaCompile) {
    options.encoding = 'UTF-8'
}
```

对于 Maven 构建，将以下内容添加到您的 `pom.xml` 文件中：

```xml
<properties>
  <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
</properties>
```

### SpotBugs Error: `Project couldn't be built`

如果您的作业在构建步骤中失败并显示 `Project couldn't be built` 消息，这很可能是因为您的作业要求 SpotBugs 使用不属于其默认工具的工具进行构建。<!-- For a list of the SpotBugs default tools, see [SpotBugs' asdf dependencies](https://gitlab.com/gitlab-org/security-products/analyzers/spotbugs/-/raw/master/config/.tool-versions).-->

解决方案是使用[预编译](#pre-compilation)。预编译确保 SpotBugs 所需的图像在作业的容器中可用。

### Flawfinder 编码错误

当 Flawfinder 遇到无效的 UTF-8 字符时会发生这种情况。要解决此问题，请将项目中的所有源代码转换为 UTF-8 字符编码。可以通过 [`cvt2utf`](https://github.com/x1angli/cvt2utf) 或 [`iconv`](https://www.gnu.org/software/libiconv/documentation/libiconv-1.13/iconv.1.html)，在整个项目或每个作业中使用 [`before_script`](../../../ci/yaml/index.md#before_script) 功能。

### Semgrep 缓慢、意外结果或其他错误

如果 Semgrep 速度慢、报告太多误报或误报、崩溃、失败或以其他方式损坏，请参阅 Semgrep 文档以了解[如何进行故障排除](https://semgrep.dev/docs/troubleshooting/gitlab-sast/)。

<!--
### SAST job fails with message `strconv.ParseUint: parsing "0.0": invalid syntax`

Invoking Docker-in-Docker is the likely cause of this error. Docker-in-Docker is:

- Disabled by default in GitLab 13.0 and later.
- Unsupported from GitLab 13.4 and later.

Several workarounds are available. From GitLab version 13.0 and later, you must not use
Docker-in-Docker.

#### Workaround 1: Pin analyzer versions (GitLab 12.1 and earlier)

Set the following variables for the SAST job. This pins the analyzer versions to the last known
working version, allowing SAST with Docker-in-Docker to complete as it did previously:

```yaml
sast:
  variables:
    SAST_DEFAULT_ANALYZERS: ""
    SAST_ANALYZER_IMAGES: "registry.gitlab.com/gitlab-org/security-products/analyzers/bandit:2.9.6, registry.gitlab.com/gitlab-org/security-products/analyzers/brakeman:2.11.0, registry.gitlab.com/gitlab-org/security-products/analyzers/eslint:2.10.0, registry.gitlab.com/gitlab-org/security-products/analyzers/flawfinder:2.11.1, registry.gitlab.com/gitlab-org/security-products/analyzers/gosec:2.14.0, registry.gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan:2.11.0, registry.gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit:2.9.1, registry.gitlab.com/gitlab-org/security-products/analyzers/pmd-apex:2.9.0, registry.gitlab.com/gitlab-org/security-products/analyzers/secrets:3.12.0, registry.gitlab.com/gitlab-org/security-products/analyzers/security-code-scan:2.13.0, registry.gitlab.com/gitlab-org/security-products/analyzers/sobelow:2.8.0, registry.gitlab.com/gitlab-org/security-products/analyzers/spotbugs:2.13.6, registry.gitlab.com/gitlab-org/security-products/analyzers/tslint:2.4.0"
```

Remove any analyzers you don't need from the `SAST_ANALYZER_IMAGES` list. Keep
`SAST_DEFAULT_ANALYZERS` set to an empty string `""`.

#### Workaround 2: Disable Docker-in-Docker for SAST and Dependency Scanning (GitLab 12.3 and later)

Disable Docker-in-Docker for SAST. Individual `<analyzer-name>-sast` jobs are created for each
analyzer that runs in your CI/CD pipeline.

```yaml
include:
  - template: SAST.gitlab-ci.yml

variables:
  SAST_DISABLE_DIND: "true"
```

#### Workaround 3: Upgrade to GitLab 13.x and use the defaults

From GitLab 13.0, SAST defaults to not using Docker-in-Docker. In GitLab 13.4 and later, SAST using
Docker-in-Docker is [no longer supported](https://gitlab.com/gitlab-org/gitlab/-/issues/220540).
If you have this problem on GitLab 13.x and later, you have customized your SAST job to
use Docker-in-Docker. To resolve this, comment out any customizations you've made to
your SAST CI job definition and [follow the documentation](index.md#configuration)
to reconfigure, using the new and improved job definition default values.

```yaml
include:
  - template: Security/SAST.gitlab-ci.yml
```
-->

### MobSF 作业失败，错误消息 `Reading from Info.plist`

当 `Info.plist` 文件缺少 `CFBundleIdentifier` 键和字符串值时，会发生此错误。

