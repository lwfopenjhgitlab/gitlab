---
stage: Secure
group: Static Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 基础设施即代码 (IaC) 扫描 **(FREE)**

> 引入于 14.5 版本。

基础架构即代码 (IaC) 扫描会扫描您的 IaC 配置文件中的已知漏洞。

IaC 扫描支持 Terraform、Ansible、AWS CloudFormation 和 Kubernetes 的配置文件。

## 要求

IaC 扫描在 `test` 阶段运行，默认情况下可用。如果在 `.gitlab-ci.yml` 文件中重新定义阶段，则需要 `test` 阶段。

我们建议至少使用 4GB RAM 以确保一致的性能。

要运行 IaC 扫描作业，默认情况下，您需要带有 [`docker`](https://docs.gitlab.cn/runner/executors/docker.html) 或 [`kubernetes`](https://docs.gitlab.cn/runner/install/kubernetes.html) 执行器。
如果您在 JihuLab.com 上使用共享 runner，则默认启用此功能。

WARNING:
我们的 IaC 扫描作业需要 Linux/amd64 容器类型。尚不支持 Windows 容器。

WARNING:
如果您使用自己的 runner，请确保安装的 Docker 版本**不是** `19.03.0`。有关详细信息，请参阅[故障排除信息](../sast/index.md#error-response-from-daemon-error-processing-tar-file-docker-tar-relocation-error)。

## 支持的语言和框架

极狐GitLab IaC 扫描支持多种 IaC 配置文件。我们的 IaC 安全扫描器还具有自动语言检测功能，甚至适用于混合语言项目。如果在项目源代码中检测到任何受支持的配置文件，我们会自动运行相应的 IaC 分析器。

| 配置文件类型                  | 扫描工具                        | 引入的版本  |
|------------------------------------------|----------------------------------|-------------------------------|
| Ansible                                  | [KICS](https://kics.io/)         | 14.5                          |
| AWS CloudFormation                       | [KICS](https://kics.io/)         | 14.5                          |
| Azure Resource Manager <sup>1</sup>      | [KICS](https://kics.io/)         | 14.5                          |
| Dockerfile                               | [KICS](https://kics.io/)         | 14.5                          |
| Google Deployment Manager                | [KICS](https://kics.io/)         | 14.5                          |
| Kubernetes                               | [KICS](https://kics.io/)         | 14.5                          |
| OpenAPI                                  | [KICS](https://kics.io/)         | 14.5                          |
| Terraform <sup>2</sup>                   | [KICS](https://kics.io/)         | 14.5                          |

1. IaC 扫描可以分析 JSON 格式的 Azure 资源管理器模板。如果您使用 [Bicep](https://learn.microsoft.com/en-us/azure/azure-resource-manager/bicep/overview) 语言编写模板，则必须使用 [bicep CLI](https://docs.microsoft.com/en-us/azure/azure-resource-manager/bicep/bicep-cli)，在极狐GitLab IaC 扫描可以分析它们之前将您的 Bicep 文件转换为 JSON。
1. 自定义镜像库中的 Terraform 模块不会被扫描来查找漏洞。<!--You can follow [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/357004) for the proposed feature.-->

### 支持的发行版

极狐GitLab 扫描器提供了一个基本的 alpine 镜像，用于大小和可维护性。

#### FIPS-enabled 镜像

> 引入于 14.10 版本。

极狐GitLab 还提供 [FIPS-enabled 的 Red Hat UBI](https://www.redhat.com/en/blog/introducing-red-hat-universal-base-image) 版本的镜像。因此，您可以用启用 FIPS 的镜像替换标准镜像。要配置镜像，请将 `SAST_IMAGE_SUFFIX` 设置为 `-fips` 或修改标准标签加上 `-fips` 扩展名。

```yaml
variables:
  SAST_IMAGE_SUFFIX: '-fips'

include:
  - template: Jobs/SAST-IaC.gitlab-ci.yml
```

<!--
### Making IaC analyzers available to all GitLab tiers

All open source (OSS) analyzers are available with the GitLab Free tier. Future proprietary analyzers may be restricted to higher tiers.
-->

#### 每个级别的功能摘要

不同的[产品级别](https://gitlab.cn/pricing/)提供不同的功能，如下表所示：

| 兼容性                                                      | 免费版和专业版  | 旗舰版        |
|:----------------------------------------------------------------|:--------------------|:-------------------|
| [配置 IaC 扫描器](#configuration)                         | **{check-circle}**  | **{check-circle}** |
| 下载 [JSON 报告](#reports-json-format)                    | **{check-circle}**  | **{check-circle}** |
| 在合并请求部件中查看新发现                        | **{dotted-circle}** | **{check-circle}** |
| [管理漏洞](../vulnerabilities/index.md)           | **{dotted-circle}** | **{check-circle}** |
| [访问安全仪表盘](../security_dashboard/index.md) | **{dotted-circle}** | **{check-circle}** |

<!--
## Contribute your scanner

The [Security Scanner Integration](../../../development/integrations/secure.md) documentation explains how to integrate other security scanners into GitLab.
-->

<a id="configuration"></a>

## 配置

要为项目配置 IaC 扫描，您可以：

- [手动配置 IaC 扫描](#configure-iac-scanning-manually)
- [通过自动合并请求启用 IaC 扫描](#enable-iac-scanning-via-an-automatic-merge-request)

<a id="configure-iac-scanning-manually"></a>

### 手动配置 IaC 扫描

要启用 IaC 扫描，您必须[包含](../../../ci/yaml/index.md#includetemplate)作为极狐GitLab 安装的一部分提供的 [`SAST-IaC.latest.gitlab-ci.yml 模板`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST-IaC.latest.gitlab-ci.yml)。以下是如何包含它的示例：

```yaml
include:
  - template: Jobs/SAST-IaC.gitlab-ci.yml
```

包含的模板会在您的 CI/CD 流水线中创建 IaC 扫描作业，并扫描您项目的配置文件中可能存在的漏洞。

结果保存为 [SAST 报告产物](../../../ci/yaml/artifacts_reports.md#artifactsreportssast)，您可以下载和分析。

<a id="enable-iac-scanning-via-an-automatic-merge-request"></a>

### 通过自动合并请求启用 IaC 扫描

要在项目中启用 IaC 扫描，您可以创建合并请求：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **安全与合规 > 配置**。
1. 在 **基础设施即代码 (IaC) 扫描** 行中，选择 **使用合并请求进行配置**。
1. 查看并合并合并请求，来启用 IaC 扫描。

流水线现在包括 IaC 扫描作业。

# 自定义规则集 **(ULTIMATE)**

> 覆盖规则引入于 14.8 版本。

您可以自定义极狐GitLab 提供的默认 IaC 扫描规则。

以下自定义选项可以单独使用，也可以一起使用：

- [禁用预定义规则](#disable-predefined-analyzer-rules)。
- [覆盖预定义规则](#override-predefined-analyzer-rules)。

<a id="disable-predefined-analyzer-rules"></a>

### 禁用预定义分析器规则

如果有您不想激活的特定 IaC 扫描规则，您可以禁用它们。

要禁用分析器规则：

1. 在项目的根目录下创建一个 `.gitlab` 目录（如果不存在的话）。
1. 在 `.gitlab` 目录中创建一个名为 `sast-ruleset.toml` 的自定义规则集文件（如果尚不存在）。
1. 在 `ruleset` 部分的上下文中将 `disabled` 标志设置为 `true`。
1. 在一个或多个 `ruleset` 子段中，列出要禁用的规则。每个 `ruleset.identifier` 部分都有：
   - 规则的 `type` 字段。对于 IaC 扫描，标识符类型是 `kics_id`。
   - 规则标识符的 `value` 字段。KICS 规则标识符是字母数字字符串。要查找规则标识符，您可以：
     - 在 [JSON 报告产物](#reports-json-format)中找到它。
     - 在 [KICS 查询列表](https://docs.kics.io/latest/queries/all-queries/) 中搜索规则名称，并复制显示的字母数字标识符。当检测到违反规则时，规则名称会显示在[漏洞页面](../vulnerabilities/index.md)上。

将 `sast-ruleset.toml` 文件合并到默认分支后，禁用规则的现有结果将[自动解决](#automatic-vulnerability-resolution)。

在以下 `sast-ruleset.toml` 文件示例中，通过匹配标识符的 `type` 和 `value` 将禁用的规则分配给 `kics` 分析器：

```toml
[kics]
  [[kics.ruleset]]
    disable = true
    [kics.ruleset.identifier]
      type = "kics_id"
      value = "8212e2d7-e683-49bc-bf78-d6799075c5a7"

  [[kics.ruleset]]
    disable = true
    [kics.ruleset.identifier]
      type = "kics_id"
      value = "b03a748a-542d-44f4-bb86-9199ab4fd2d5"
```

<a id="override-predefined-analyzer-rules"></a>

### 覆盖预定义分析器规则

如果有您想要自定义的特定 IaC 扫描规则，您可以覆盖它们。例如，您可以降低规则的严重性或关联到您自己的关于如何修复发现的文档。

覆盖规则：

1. 在项目的根目录下创建一个 `.gitlab` 目录（如果不存在的话）。
1. 在 `.gitlab` 目录中创建一个名为 `sast-ruleset.toml` 的自定义规则集文件（如果尚不存在）。
1. 在一个或多个 `ruleset` 子段中，列出要禁用的规则。每个 `ruleset.identifier` 部分都有：
   - 规则的 `type` 字段。对于 IaC 扫描，标识符类型是 `kics_id`。
   - 规则标识符的 `value` 字段。KICS 规则标识符是字母数字字符串。要查找规则标识符，您可以：
     - 在 [JSON 报告产物](#reports-json-format)中找到它。
     - 在 [KICS 查询列表](https://docs.kics.io/latest/queries/all-queries/) 中搜索规则名称，并复制显示的字母数字标识符。当检测到违反规则时，规则名称会显示在[漏洞页面](../vulnerabilities/index.md)上。
1. 在 `ruleset` 部分的 `ruleset.override` 上下文中，提供要覆盖的键。任何键的组合都可以被覆盖。有效键是：
   - description
   - message
   - name
   - severity（有效项为：Critical、High、Medium、Low、Unknown、Info）

在以下 `sast-ruleset.toml` 文件示例中，规则与标识符的 `type` 和 `value` 匹配，然后被覆盖：

```toml
[kics]
  [[kics.ruleset]]
    [kics.ruleset.identifier]
      type = "kics_id"
      value = "8212e2d7-e683-49bc-bf78-d6799075c5a7"
    [kics.ruleset.override]
      description = "OVERRIDDEN description"
      message = "OVERRIDDEN message"
      name = "OVERRIDDEN name"
      severity = "Info"
```

## 固定到特定的分析器版本

极狐GitLab 管理的 CI/CD 模板指定一个主要版本，并自动在该主要版本中提取最新的分析器版本。

在某些情况下，您可能需要使用特定版本。
例如，您可能需要避免在以后的版本中出现回归。

要覆盖自动更新行为，请在包含 [`SAST-IaC.gitlab-ci.yml` 模板](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST-IaC.gitlab-ci.yml)中设置 `SAST_ANALYZER_IMAGE_TAG` CI/CD 变量。

仅在特定作业中设置此变量。如果您将其设置在顶层，则您设置的版本将用于其他 SAST 分析器。

您可以将标签设置为：

- 主要版本，例如 `3`。您的流水线将使用在此主要版本中发布的任何次要更新或补丁更新。
- 次要版本，例如 `3.7`。您的流水线将使用在此次要版本中发布的任何补丁更新。
- 补丁版本，比如 `3.7.0`。您的流水线不会收到任何更新。

此示例使用特定次要版本的 `KICS` 分析器：

```yaml
include:
  - template: Security/SAST-IaC.gitlab-ci.yml

kics-iac-sast:
  variables:
    SAST_ANALYZER_IMAGE_TAG: "3.1"
```

<a id="automatic-vulnerability-resolution"></a>

## 自动漏洞解决

> - 引入于 15.9 版本，[项目级功能标志](../../../administration/feature_flags.md)为 `sec_mark_dropped_findings_as_resolved`。
> - 默认启用于 15.10 版本。

为了帮助您关注仍然相关的漏洞，IaC 扫描会在以下情况下自动[解决](../vulnerabilities/index.md#vulnerability-status-values)漏洞：

- 您[禁用了预定义规则](#disable-predefined-analyzer-rules)。
- 我们从默认规则集中删除了一条规则。

漏洞管理系统会在自动解决的漏洞上留下评论，因此您仍然拥有漏洞的历史记录。

如果您稍后重新启用该规则，将重新打开漏洞发现，来进行分类。

<a id="reports-json-format"></a>

## 报告 JSON 格式

IaC 工具以现有 SAST 报告格式发出 JSON 报告文件。<!--For more information, see the
[schema for this report](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/sast-report-format.json).-->

JSON 报告文件可以从 CI 流水线页面或合并请求上的流水线选项卡，通过 [`artifacts:paths` 设置](../../../ci/yaml/index.md#artifactspaths)下载到 `gl-sast-report.json`。有关更多信息，请参阅[下载产物](../../../ci/pipelines/job_artifacts.md)。

## 故障排除

### IaC debug 日志

为了帮助对 IaC 作业进行故障排除，您可以通过使用设置为 `debug` 的全局 CI/CD 变量来增加[安全扫描程序日志详细程度](../sast/index.md#logging-level)：

```yaml
variables:
  SECURE_LOG_LEVEL: "debug"
```

### IaC 扫描发现显示 `No longer detected`

如果先前检测到的发现，意外显示为 `No longer detected`，则可能是由于扫描器更新所致。更新可以禁用被发现无效或误报的规则，并将结果标记为 `No longer detected`：

- 在 15.3 版本，KICS SAST IaC 扫描器中的 secret 检测已禁用，因此“密码和 Secrets” 系列中的 IaC 发现显示为 `No longer detected`。

