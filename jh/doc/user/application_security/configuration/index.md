---
type: reference, howto
stage: Secure
group: Static Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 安全配置 **(FREE)**

安全配置页面列出了安全测试和合规工具的以下内容：

- 名称、描述和文档链接。
- 是否可用。
- 配置按钮或其配置指南的链接。

为了确定每个安全控制的状态，极狐GitLab 在默认分支的最近提交中检查 [CI/CD 流水线](../../../ci/pipelines/index.md)。

如果极狐GitLab 找到 CI/CD 流水线，则它会检查 `.gitlab-ci.yml` 文件中的每个作业。

- 如果作业为安全扫描器定义了 [`artifacts:reports` 关键字](../../../ci/yaml/artifacts_reports.md)，则极狐GitLab 认为安全扫描器已启用并显示**已启用**状态。
- 如果没有作业为安全扫描器定义 `artifacts:reports` 关键字，则极狐GitLab 认为安全扫描器已禁用并显示**未启用**状态。

如果极狐GitLab 没有找到 CI/CD 流水线，则它认为所有安全扫描程序都已禁用并显示**未启用**状态。

失败的流水线和作业包含在此过程中。如果已配置扫描器但作业失败，则仍认为该扫描器已启用。此过程还确定通过 API 返回的扫描程序和状态。

如果最新的流水线使用 [Auto DevOps](../../../topics/autodevops/index.md)，则默认配置所有安全功能。

查看项目的安全配置：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **安全与合规 > 配置**。

选择 **配置历史** 以查看 `.gitlab-ci.yml` 文件的历史记录。

## 安全测试

您可以配置以下安全控制：

- [静态应用安全测试](../sast/index.md)（SAST）
  - 选择 **启用 SAST** 为当前项目配置 SAST。有关更多详细信息，请阅读[在 UI 中配置 SAST](../sast/index.md#configure-sast-by-using-the-ui)。
- [动态应用安全测试](../dast/index.md)（DAST）
  - 选择 **启用 DAST** 为当前项目配置 DAST。
  - 选择 **管理扫描**，管理保存的 DAST 扫描、站点配置文件和扫描仪配置文件。<!--有关更多详细信息，请阅读 [DAST 按需扫描](../dast/proxy-based.md#on-demand-scans)。-->
- [依赖项扫描](../dependency_scanning/index.md)
  - 选择 **使用合并请求进行配置**，创建包含启用依赖扫描所需更改的合并请求。有关更多详细信息，请参阅[通过自动合并请求启用依赖扫描](../dependency_scanning/index.md#enable-dependency-scanning-via-an-automatic-merge-request)。
- [容器扫描](../container_scanning/index.md)
  - 选择 **使用合并请求进行配置**，创建包含启用容器扫描所需更改的合并请求。有关详细信息，请参阅[通过自动合并请求启用容器扫描](../container_scanning/index.md#enable-container-scanning-through-an-automatic-merge-request)。
- [运营容器扫描](../../clusters/agent/vulnerabilities.md)
  - 可以通过将配置块添加到您的代理配置来进行配置。有关更多详细信息，请阅读[运营容器扫描](../../clusters/agent/vulnerabilities.md#enable-operational-container-scanning)。
- [Secret 检测](../secret_detection/index.md)
  - 选择 **使用合并请求进行配置**，创建包含启用 Secret 检测所需更改的合并请求。有关详细信息，请阅读[通过自动合并请求启用 Secret 检测](../secret_detection/index.md#use-an-automatically-configured-merge-request)。
- [API Fuzzing](../api_fuzzing/index.md)
  - 选择 **启用 API Fuzzing** 为当前项目使用 API Fuzzing。有关更多详细信息，请阅读 [API Fuzzing](../../../user/application_security/api_fuzzing/index.md#enable-web-api-fuzzing)。
- [Coverage Fuzzing](../coverage_fuzzing/index.md)
  - 可以使用 `.gitlab-ci.yml` 进行配置。有关更多详细信息，请阅读 [Coverage Fuzzing](../../../user/application_security/coverage_fuzzing/index.md#enable-coverage-guided-fuzz-testing)。

## 合规 **(ULTIMATE)**

您可以配置以下安全控制：

- [许可证合规](../../../user/compliance/license_compliance/index.md)
  - 可以使用 `.gitlab-ci.yml` 进行配置。有关更多详细信息，请阅读[许可证合规性](../../../user/compliance/license_compliance/index.md#enable-license-compliance)。

- [安全培训](../../../user/application_security/vulnerabilities/index.md#enable-security-training-for-vulnerabilities)
  - 为当前项目启用 **安全培训**。有关更多详细信息，请阅读[安全培训](../../../user/application_security/vulnerabilities/index.md#enable-security-training-for-vulnerabilities)。
