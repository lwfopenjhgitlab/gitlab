---
stage: DevSecOps
group: Technical writing
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab 应用程序安全入门 **(ULTIMATE)**

完成以下步骤以充分利用极狐GitLab 应用程序安全工具。

1. 为您的默认分支启用 [Secret 检测](secret_detection/index.md)扫描。
1. 为您的默认分支启用[依赖扫描](dependency_scanning/index.md)，以便您可以开始识别代码库中现有的易受攻击的包。
1. 将安全扫描添加到功能分支流水线。应该启用与在默认分支上运行的相同的扫描。通过将功能分支与默认分支结果进行比较，后续扫描将仅显示新漏洞。
1. 让您的团队熟悉[漏洞报告](vulnerability_report/index.md)并建立漏洞分类工作流程。
1. 考虑创建[标记](../project/labels.md)和[议题看板](../project/issue_board.md)，来帮助管理由漏洞造成的议题。议题看板为所有利益相关者提供所有议题的常见视角。
1. 创建一个[扫描结果策略](policies/index.md)，限制新漏洞被合并到您的默认分支中。
1. 监控[安全仪表盘](security_dashboard/index.md)趋势，以衡量在修复现有漏洞和防止引入新漏洞方面的成功。
1. 启用其他扫描类型，例如 [SAST](sast/index.md)、[DAST](dast/index.md)、[模糊测试](coverage_fuzzing/index.md)或[容器扫描](container_scanning/index.md)，确保向功能流水线和默认分支流水线添加相同的扫描类型。
1. 使用[合规流水线](../group/compliance_frameworks.md#configure-a-compliance-pipeline)或[扫描执行策略](policies/scan-execution-policies.md)，强制执行所需的扫描类型，并确保安全和工程之间的职责分离。
1. 考虑启用 Review Apps 以允许在临时测试环境中使用 DAST 和 [Web API fuzzing](api_fuzzing/index.md)。
1. 启用[运营容器扫描](../../user/clusters/agent/vulnerabilities.md)，扫描生产集群中的容器镜像以查找安全漏洞。
