---
stage: Secure
group: Incubation
info: Breach and Attack Simulation is a GitLab Incubation Engineering program. No technical writer assigned to this group.
type: reference, howto
---

# 破坏和攻击模拟 **(ULTIMATE)**

> 引入于 15.11 版本。

DISCLAIMER:
突破和攻击模拟是正在开发的实验功能，并且会随着时间的推移发生重大变化。

破坏和攻击模拟 (BAS) 使用额外的安全测试技术来评估检测到的漏洞的风险，并优先修复可利用的漏洞。

<!--
For feedback, bug reports, and feature requests, see the [feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/404809).
-->

WARNING:
只对测试服务器运行 BAS 扫描。测试攻击者的行为可能会导致数据被修改或丢失。

<a id="extend-dynamic-application-security-testing-dast"></a>

## 扩展动态应用程序安全测试（DAST）

您可以使用 [DAST](../dast/index.md) 模拟攻击来检测漏洞。
默认情况下，DAST 主动检查与预期响应相匹配，或通过响应时间确定漏洞是否被利用。

在 DAST 中启用 BAS 功能标志：

- 在主动检查中启用回调、匹配响应和定时攻击。
- 通过主动检查中的回调攻击执行带外应用程序安全测试（OAST）。

启用 BAS：

1. 使用 [DAST 基于浏览器的分析器](../dast/browser_based.md#create-a-dast-cicd-job)创建 CI/CD 作业。
1. 将 `DAST_FF_ENABLE_BAS` [CI/CD 变量](../dast/browser_based.md#available-cicd-variables)设置为 `true`。

```yaml
include:
  - template: DAST.gitlab-ci.yml

dast:
  variables:
    DAST_BROWSER_SCAN: "true"
    DAST_FF_ENABLE_BAS: "true"
    DAST_WEBSITE: "https://my.site.com"
```
