---
stage: Protect
group: Container Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 扫描结果策略 **(ULTIMATE)**

> 群组级扫描结果策略引入于 15.6 版本。

您可以使用扫描结果策略根据扫描结果采取行动。例如，一种类型的扫描结果策略是允许根据一个或多个安全扫描作业的 findings，要求批准的安全批准策略。在 CI 扫描作业完全执行后评估扫描结果策略。

<a id="scan-result-policy-editor"></a>

## 扫描结果策略编辑器

> 引入于 14.8 版本，功能标志名为 `scan_result_policy`。默认禁用。

NOTE:
只有项目所有者有权选择安全策略项目。

完成策略后，通过选择编辑器底部的 **创建合并请求** 来保存，将您重定向到项目的已配置安全策略项目的合并请求。
如果安全策略项目未关联到您的项目，系统会为您创建项目。
通过选择编辑器底部的 **删除策略**，也可以从编辑器界面中删除现有策略。

大多数策略更改会在合并请求合并后立即生效。未通过合并请求并直接提交到默认分支的任何更改，可能需要长达 10 分钟才能使策略更改生效。

策略编辑器支持 YAML 模式和规则模式。

NOTE:
对于具有大量项目的群组创建的扫描结果策略，其传播将需要一段时间才能完成。

## 扫描结果策略 schema

带有扫描结果策略的 YAML 文件由一组与嵌套在 `scan_result_policy` 键下的扫描结果策略模式匹配的对象组成。您可以在 `scan_result_policy` 键下配置最多五个策略。

当您保存新策略时，GitLab 会根据[此 JSON 模式](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/app/validators/json_schemas/security_orchestration_policy.json) 验证其内容。
如果您不熟悉如何阅读 [JSON 模式](https://json-schema.org/)，以下部分和表格提供另一种选择。

| 字段 | 类型 | 可能的值 | 描述 |
|-------|------|-----------------|-------------|
| `scan_result_policy` | 扫描结果策略的 `array` |  | 扫描执行策略列表（最多 5 个） |

## 扫描结果策略 schema

| 字段 | 类型 | 可能的值 | 描述 |
|-------|------|-----------------|-------------|
| `name` | `string` |  | 策略名称。最多 255 个字符。 |
| `description`（可选） | `string` |  | 策略的描述。 |
| `enabled` | `boolean` | `true`, `false` | 启用 (`true`) 或禁用 (`false`) 策略的标志。 |
| `rules` | 规则的 `array` |  | 策略应用的规则列表。 |
| `actions` | 操作的 `array` |  | 策略强制执行的操作列表。 |

## `scan_finding` 规则类型

此规则根据提供的信息强制执行定义的操作。

| 字段 | 类型 | 可能的值 | 描述 |
|------------|------|-----------------|-------------|
| `type`     | `string` | `scan_finding` | 规则的类型 |
| `branches` | `string` 的 `array` | `[]` 或分支名称 | 仅适用于受保护的目标分支。一个空数组 `[]` 可以将规则应用于所有受保护的目标分支。 |
| `scanners`  | `string` 的 `array` | `sast`、`secret_detection`、`dependency_scanning`、`container_scanning`、`dast`、`coverage_fuzzing`、`api_fuzzing` | 此规则要考虑的安全扫描程序。 |
| `vulnerabilities_allowed`  | `integer` | 大于或等于零 | 在考虑此规则之前允许的漏洞数。 |
| `severity_levels`  | `string` 的 `array` | `info`、`unknown`、`low`、`medium`、`high`、`critical`| 此规则要考虑的严重性级别。 |
| `vulnerability_states`  | `string` 的 `array` | `newly_detected`、`detected`、`confirmed`、`resolved`、`dismissed`、`new_needs_triage`、`new_dismissed`  | 所有漏洞分为两类：<br><br>**新检测到的漏洞** - `newly_detected` 策略选项涵盖合并请求本身中的 `Detected` 或 `Dismissed` 漏洞，这些漏洞目前在默认分支上不存在。此策略选项要求流水线在按规则评估之前完成，以便它知道是否新检测到漏洞。合并请求在流水线之前被阻止，并且必要的安全扫描已完成。<br><br> • `Detected`<br> • Dismissed<br><br> `new_needs_triage` 选项可选状态<br><br> • Detected<br><br> `new_dismissed` 选项可选状态<br><br>  • `Dismissed`<br><br>**预先存在的漏洞** - 这些策略选项会立即进行评估，并且不需要完整的流水线，因为它们只考虑先前在默认分支中检测到的漏洞。<br><br> • `Detected` - 该策略寻找在 `Detected` 状态下的漏洞。<br> • `Confirmed` - 该策略寻找在 `Confirmed` 状态下的漏洞。<br> • `Dismissed` - 该策略寻找在 `Dismissed` 状态下的漏洞。<br> • `Resolved` - 该策略寻找在 `Resolved` 状态下的漏洞。 |

## `license_finding` 规则类型

> - 引入于 15.9 版本，[功能标志](../../../administration/feature_flags.md)为 `license_scanning_policies`。
> - 一般可用于 15.11 版本。删除功能标志 `license_scanning_policies`。

此规则根据许可证调查结果强制执行定义的操作。

| 字段 | 类型 | 可能的值 | 描述 |
|------------|------|-----------------|-------------|
| `type` | `string` | `license_finding` | 规则的类型 |
| `branches` | `string` 的 `array` | `[]` 或分支的名称 | 仅适用于受保护的目标分支。空数组 `[]` 将规则应用于所有受保护的目标分支。 |
| `match_on_inclusion` | `boolean` | `true`、`false` | 该规则是否匹配包含或排除 `license_types` 中列出的许可证。 |
| `license_types` | `string` 的 `array` | 许可证类型 | 要匹配的许可证类型，例如 `BSD` 或 `MIT`。 |
| `license_states` | `string` 的 `array` | `newly_detected`、`detected` | 是否匹配新检测到的和/或以前检测到的许可证。 |

## `require_approval` 操作类型

此操作设置在满足已定义策略中的至少一个规则的条件时，需要的批准规则。

| 字段 | 类型 | 可能的值 | 描述 |
|-------|------|-----------------|-------------|
| `type` | `string` | `require_approval` | 操作的类型。 |
| `approvals_required` | `integer` | 大于或等于零 | 所需的 MR 批准数。 |
| `user_approvers` | `array` of `string` | 一位或多位用户的用户名 | 被视为核准人的用户。用户必须有权访问该项目才有资格进行批准。 |
| `user_approvers_ids` | `array` of `integer` | 一个或多个用户的 ID | 被视为核准人的用户的 ID。用户必须有权访问该项目才有资格进行批准。 |
| `group_approvers` | `array` of `string` | 一个或多个群组的路径 | 被视为核准人的群组。具有[群组中直接成员资格](../../project/merge_requests/approvals/rules.md#group-approvers)的用户有资格进行批准。 |
| `group_approvers_ids` | `array` of `integer` | 一个或多个群组的 ID | 被视为核准人的群组的 ID。具有[群组中直接成员资格](../../project/merge_requests/approvals/rules.md#group-approvers)的用户有资格进行批准。 |
| `role_approvers` | `array` of `string` | 一个或多个[角色](../../../user/permissions.md#roles)（例如：`owner`、`maintainer`）  | 作为有资格批准的批准者的角色。 |

要求和限制：

- 您必须添加相应的安全扫描工具<!--[安全扫描工具](../index.md#security-scanning-tools)-->。否则，扫描结果策略将不起作用。
- 策略的最大数量为五个。
- 每个策略最多可以有五个规则。
- 所有配置的扫描器都必须存在于合并请求的最新流水线中。否则，即使未满足某些漏洞标准，也需要批准。

<a id="example-security-scan-result-policies-project"></a>

## 安全扫描结果策略项目示例

您可以在 `.gitlab/security-policies/policy.yml` 中使用此示例，如[安全策略项目](index.md#security-policy-project)中所述：

```yaml
---
scan_result_policy:
- name: critical vulnerability CS approvals
  description: critical severity level only for container scanning
  enabled: true
  rules:
  - type: scan_finding
    branches:
    - main
    scanners:
    - container_scanning
    vulnerabilities_allowed: 0
    severity_levels:
    - critical
    vulnerability_states:
    - newly_detected
  actions:
  - type: require_approval
    approvals_required: 1
    user_approvers:
    - adalberto.dare
- name: secondary CS approvals
  description: secondary only for container scanning
  enabled: true
  rules:
  - type: scan_finding
    branches:
    - main
    scanners:
    - container_scanning
    vulnerabilities_allowed: 1
    severity_levels:
    - low
    - unknown
    vulnerability_states:
    - newly_detected
  actions:
  - type: require_approval
    approvals_required: 1
    user_approvers:
    - sam.white
    role_approvers:
    - owner
```

在此例中：

- 每个包含由容器扫描识别的新 `critical` 级别的漏洞的 MR，都需要 `alberto.dare` 的批准。
- 每个 MR 都包含一个以上由容器扫描识别的新的 `low` 或 `unknown` 级别的漏洞，需要 `sam.white` 的一次批准。

## 扫描结果策略编辑器示例

您可以在[扫描结果策略编辑器](#scan-result-policy-editor)的 YAML 模式下使用此示例。
它对应于上一个示例中的单个对象：

```yaml
- name: critical vulnerability CS approvals
  description: critical severity level only for container scanning
  enabled: true
  rules:
  - type: scan_finding
    branches:
    - main
    scanners:
    - container_scanning
    vulnerabilities_allowed: 1
    severity_levels:
    - critical
    vulnerability_states:
    - newly_detected
  actions:
  - type: require_approval
    approvals_required: 1
    user_approvers:
    - adalberto.dare
```

## 扫描结果策略需要额外批准的示例情况

在几种情况下，扫描结果策略需要额外的批准步骤。例如：

- 工作分支中的安全作业数量减少，不再与目标分支中的安全作业数量匹配。用户无法通过从 CI 配置中删除扫描作业，来跳过扫描结果策略。
- 有人停止了流水线安全作业，用户无法跳过安全扫描。
- 合并请求中的作业失败并配置为 `allow_failure: false`，结果流水线处于阻塞状态。
- 流水线有一个手动作业，必须成功运行才能使整个流水线通过。
