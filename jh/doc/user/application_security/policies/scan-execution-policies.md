---
stage: Protect
group: Container Security
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 扫描执行策略 **(ULTIMATE)**

> 群组级别安全策略引入于 15.2 版本。

群组、子组或项目所有者可以使用扫描执行策略来要求安全扫描按指定的计划运行，或与项目（支持多个项目，如果策略是在群组或子组级别定义的）流水线一起运行。所需的扫描将作为新作业注入 CI 流水线，具有长且随机的作业名称。在不太可能发生作业名称冲突的情况下，安全策略作业会覆盖流水线中任何预先存在的作业。如果在群组级别创建策略，它将应用于每个子项目或子组。不能从子项目或子组编辑群组级策略。

<!--
This feature has some overlap with [compliance framework pipelines](../../project/settings/#compliance-pipeline-configuration),
as we have not [unified the user experience for these two features](https://gitlab.com/groups/gitlab-org/-/epics/7312).
For details on the similarities and differences between these features, see
[Enforce scan execution](../#enforce-scan-execution).
-->

NOTE:
策略作业是在流水线的 `test` 阶段创建的。如果修改默认流水线 [`stages`](../../../ci/yaml/index.md#stages)，则必须确保 `test` 阶段存在于列表中。否则，流水线将无法运行，并且会出现 `chosen stage does not exist` 状态的错误。

<a id="scan-execution-policy-editor"></a>

## 扫描执行策略编辑器

NOTE:
只有群组、子组或项目所有者拥有选择安全策略项目的权限。

完成策略后，通过选择编辑器底部的 **通过合并请求创建** 来保存。您将被重定向到项目的已配置安全策略项目的合并请求。如果没有关联到您的项目，则会自动创建一个安全策略项目。通过选择编辑器底部的 **删除策略**，也可以从编辑器界面中删除现有策略。

大多数策略更改会在合并请求合并后立即生效。未通过合并请求并直接提交到默认分支的任何更改，可能需要长达 10 分钟才能使策略更改生效。

![Scan Execution Policy Editor YAML Mode](img/scan_execution_policy_rule_mode_v15_11.png)

<!--
The policy editor currently only supports the YAML mode. The Rule mode is tracked in the [Allow Users to Edit Rule-mode Scan Execution Policies in the Policy UI](https://gitlab.com/groups/gitlab-org/-/epics/5363) epic.
-->

## 扫描执行策略 schema

带有扫描执行策略的 YAML 文件由一组与扫描执行策略模式匹配的对象组成，这些对象嵌套在 `scan_execution_policy` 键下。您可以在 `scan_execution_policy` 键下配置最多 5 个策略。前 5 个之后配置的任何其他策略都不会应用。

当您保存新策略时，系统会根据[此 JSON 模式](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/app/validators/json_schemas/security_orchestration_policy.json)验证其内容。
如果您不熟悉如何阅读[JSON 模式](https://json-schema.org/)，以下部分和表格提供另一种选择。

| 字段 | 类型 | 可能的值 | 描述 |
|-------|------|-----------------|-------------|
| `scan_execution_policy` | 扫描执行策略的 `array` |  | 扫描执行策略列表（最多 5 个） |

## 扫描执行策略 schema

| 字段 | 类型 | 可能的值 | 描述 |
|-------|------|-----------------|-------------|
| `name` | `string` |  | 策略名称。最多 255 个字符。 |
| `description`（可选） | `string` |  | 策略的描述。 |
| `enabled` | `boolean` | `true`, `false` | 启用 (`true`) 或禁用 (`false`) 策略的标志。 |
| `rules` | 规则的 `array` |  | 策略应用的规则列表。 |
| `actions` | 操作的 `array` |  | 策略强制执行的操作列表。 |

## `pipeline` 规则类型

每当流水线为选定的分支运行时，此规则就会强制执行定义的操作。

| 字段 | 类型 | 可能的值 | 描述 |
|-------|------|-----------------|-------------|
| `type` | `string` | `pipeline` | 规则的类型。 |
| `branches` | `string` 的 `array` | `*` 或分支名称 | 给定策略适用的分支（支持通配符）。 |

## `schedule` 规则类型

此规则强制执行定义的操作并在提供的日期/时间安排扫描。

| 字段 | 类型 | 可能的值 | 描述 |
|------------|------|-----------------|-------------|
| `type`     | `string` | `schedule` | 规则的类型。 |
| `branches` | `string` 的 `array` | `*` 或分支名称 | 给定策略适用的分支（支持通配符）。如果未设置 `agents` 字段，则此字段是必需的。 |
| `cadence`  | `string` | CRON 表达式（例如，`0 0 * * *`） | 一个以空格分隔的字符串，包含五个表示计划时间的字段。 |
| `agents`   | `object` | | [集群镜像扫描](../../clusters/agent/vulnerabilities.md)将运行的[极狐GitLab 代理](../../clusters/agent/index.md)的名称。对象键是在极狐GitLab 中为您的项目配置的 Kubernetes 代理的名称。如果未设置 `branches` 字段，则此字段是必需的。 |

极狐GitLab 支持 `cadence` 字段的以下类型的 CRON 语法：

- 在指定时间每小时一次的每日周期，例如：`0 18 * * *`
- 在指定日期和指定时间每周一次的每周周期，例如：`0 13 * * 0`

NOTE:
如果我们在实施中使用的 cron 支持，则 CRON 语法的其他元素可能会在 `cadence` 字段中生效。但是，极狐GitLab 并未正式测试或支持。

NOTE:
如果使用 `Operational Container Scanning` 所需的 `agents` 字段，则使用 Kubernetes-agent pod 的系统时间以 UTC 计算 CRON 表达式。如果不使用 `agents` 字段，则 CRON 表达式将以来自 SaaS 的标准 UTC 时间进行计算。如果您有一个私有化部署实例，并且[更改了服务器时区](../../../administration/timezone.md)，CRON 表达式将使用新时区进行计算。

### `agent` schema

使用此 schema，在 [`schedule` 规则类型](#schedule-rule-type)中定义 `agents` 对象。

| 字段 | 类型 | 可能的值 | 描述 |
|--------------|---------------------|--------------------------|-------------|
| `namespaces` | `array` of `string` | | 被扫描的命名空间。如果为空，将扫描所有命名空间。 |

#### 策略示例

```yaml
- name: Enforce Container Scanning in cluster connected through my-gitlab-agent for default and kube-system namespaces
  enabled: true
  rules:
  - type: schedule
    cadence: '0 10 * * *'
    agents:
      <agent-name>:
        namespaces:
        - 'default'
        - 'kube-system'
  actions:
  - scan: container_scanning
```

调度规则的键是：

- `cadence`（必需）：一个 [CRON 表达式](https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm)，表示何时运行扫描。
- `agents:<agent-name>`（必需）：用于扫描的代理名称。
- `agents:<agent-name>:namespaces`（可选）：要扫描的 Kubernetes 命名空间。如果省略，将扫描所有命名空间。

## `scan` 操作类型

当满足已定义策略中至少一条规则的条件时，此操作会使用附加参数执行选定的 `scan`。

| 字段 | 类型 | 可能的值 | 描述 |
|-------|------|-----------------|-------------|
| `scan` | `string` | `dast`、`secret_detection`、`sast`、`container_scanning`、`dependency_scanning` | 操作的类型。 |
| `site_profile` | `string` | 所选 DAST 站点配置文件<!--[DAST 站点配置文件](../dast/index.md#site-profile)-->的名称。 | 用于执行 DAST 扫描的 DAST 站点配置文件。仅当 `scan` 类型为 `dast` 时才应设置此字段。 |
| `scanner_profile` | `string` 或 `null` | 所选 DAST 扫描器配置文件<!--[DAST 扫描器配置文件](../dast/index.md#scanner-profile)-->的名称。 | 用于执行 DAST 扫描的 DAST 扫描仪配置文件。仅当 `scan` 类型为 `dast` 时才应设置此字段。 |
| `variables` | `object` | | 一组 CI 变量，以 `key: value` 对数组的形式提供，用于应用和执行选定的扫描。`key` 是变量名，其 `value` 以字符串形式提供。此参数支持 GitLab CI 作业支持指定扫描的任何变量。 |
| `tags` | `string` 形式的 `array`  | | 该策略的 runner 标签列表。策略作业将由具有指定标签的 runner 运行。 |

注意：

- 您必须为分配的每个项目创建具有选定名称的站点配置文件<!--[站点配置文件](../dast/index.md#site-profile)-->和扫描器配置文件<!--[扫描器配置文件](../dast/index.md#scanner-profile)-->到选定的安全策略项目。否则，不会应用该策略，而是创建带有错误消息的作业。
- 一旦您在策略中按名称关联站点配置文件和扫描器配置文件，就无法修改或删除它们。如果要修改它们，必须首先通过将 `active` 标志设置为 `false` 来禁用策略。
- 使用计划的 DAST 扫描配置策略时，安全策略项目的仓库中提交的作者必须有权访问扫描程序和站点配置文件。否则，扫描不会成功调度。
- 对于 secret 检测扫描，仅支持具有默认规则集的规则。不支持自定义规则集<!--[自定义规则集](../secret_detection/index.md#custom-rulesets)-->。
- Secret 检测扫描在作为流水线的一部分执行时以 `normal` 模式运行，当作为流水线的一部分执行时，以 `history` 模式运行计划扫描。
- 为 `pipeline` 规则类型配置的容器扫描和集群镜像扫描会忽略 `agents` 对象中定义的集群，`agents` 对象仅适用于 `schedule` 规则类型。 必须为项目创建和配置具有在 `agents` 对象中提供的名称的代理。

<a id="example-security-policies-project"></a>

## 示例安全策略项目

您可以在 `.gitlab/security-policies/policy.yml` 中使用此示例，如[安全策略项目](index.md#security-policy-project)中所述。

```yaml
---
scan_execution_policy:
- name: Enforce DAST in every release pipeline
  description: This policy enforces pipeline configuration to have a job with DAST scan for release branches
  enabled: true
  rules:
  - type: pipeline
    branches:
    - release/*
  actions:
  - scan: dast
    scanner_profile: Scanner Profile A
    site_profile: Site Profile B
- name: Enforce DAST and secret detection scans every 10 minutes
  description: This policy enforces DAST and secret detection scans to run every 10 minutes
  enabled: true
  rules:
  - type: schedule
    branches:
    - main
    cadence: "*/10 * * * *"
  actions:
  - scan: dast
    scanner_profile: Scanner Profile C
    site_profile: Site Profile D
  - scan: secret_detection
- name: Enforce Secret Detection and Container Scanning in every default branch pipeline
  description: This policy enforces pipeline configuration to have a job with Secret Detection and Container Scanning scans for the default branch
  enabled: true
  rules:
  - type: pipeline
    branches:
    - main
  actions:
  - scan: secret_detection
  - scan: sast
    variables:
      SAST_EXCLUDED_ANALYZERS: brakeman
  - scan: container_scanning
```

在此示例中：

- 对于在匹配 `release/*` 通配符的分支上执行的每个流水线（例如，分支 `release/v1.2.1`），DAST 扫描使用 `Scanner Profile A` 和 `Site Profile B` 运行。
- DAST 和 secret 检测扫描每 10 分钟运行一次。DAST 扫描使用 `Scanner Profile C` 和 `Site Profile D`。
- Secret 检测、容器扫描和 SAST 扫描针对在 `main` 分支上执行的每个流水线运行。SAST 扫描运行时将 `SAST_EXCLUDED_ANALYZER` 变量设置为 `"brakeman"`。

## 扫描执行策略编辑器示例

您可以在[扫描执行策略编辑器](#scan-execution-policy-editor)的 YAML 模式下使用此示例。它对应于上一个示例中的单个对象。

```yaml
name: Enforce Secret Detection and Container Scanning in every default branch pipeline
description: This policy enforces pipeline configuration to have a job with Secret Detection and Container Scanning scans for the default branch
enabled: true
rules:
  - type: pipeline
    branches:
      - main
actions:
  - scan: secret_detection
  - scan: container_scanning
```
