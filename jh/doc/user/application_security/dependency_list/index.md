---
type: reference, howto
stage: Secure
group: Composition Analysis
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 依赖项列表 **(ULTIMATE)**

> - 应用依赖项引入于 12.0 版本。
> - 系统依赖项引入于 14.6 版本。

使用依赖项列表查看项目的依赖项和有关这些依赖项的关键详细信息，包括它们的已知漏洞。它是项目中依赖项的集合，包括现有的和新的发现。

要查看依赖项列表，请转到您的项目并选择 **安全与合规 > 依赖项列表**。

此信息有时称为软件物料清单、SBOM 或 BOM。

## 先决条件

要查看项目的依赖项，请确保满足以下要求：

- 必须为您的项目配置[依赖扫描](../dependency_scanning/index.md)或[容器扫描](../container_scanning/index.md) CI 作业。
- 您的项目至少使用 Gemnasium 支持的[语言和包管理器](../dependency_scanning/index.md#supported-languages-and-package-managers)之一。
- 在默认分支上运行了一条成功的流水线。您不应更改允许[应用程序安全作业](../../application_security/index.md#application-coverage)失败的默认行为。

## 查看项目的依赖项

![Dependency list](img/dependency_list_v13_11.png)

极狐GitLab 显示具有以下信息的依赖项：

| 字段    | 描述 |
|-----------|-------------|
| Component | 依赖项的名称和版本。 |
| Packager  | 用于安装依赖项的打包程序。 |
| Location  | 对于系统依赖项，会列出已扫描的镜像。对于应用程序依赖项，显示一个链接，指向您的项目中声明依赖项的特定于打包程序的锁定文件。如果存在并且支持的话，还显示顶级依赖项的[依赖项路径](#dependency-paths)。 |
| License   | 链接到依赖项的软件许可证。 |

显示的依赖项最初按其已知漏洞的严重性排序（如果有）。它们也可以按名称或安装它们的打包程序排序。

### 漏洞

如果依赖项存在已知漏洞，请通过单击依赖项名称旁边的箭头或指示存在多少已知漏洞的标记来查看它们。对于每个漏洞，其严重性和描述显示在其下方。要查看漏洞的更多详细信息，请选择漏洞的描述，然后[漏洞详情](../vulnerabilities)页面打开。

### 依赖路径

依赖项列表显示了依赖项和它所连接的顶级依赖项之间的路径（如果有）。有许多可能的路径将瞬态依赖项连接到顶级依赖项，但用户界面仅显示最短路径之一。

![Dependency path](img/yarn_dependency_path_v13_6.png)

以下包管理器支持依赖路径：

- [NuGet](https://www.nuget.org/)
- [Yarn 1.x](https://classic.yarnpkg.com/lang/en/)
- [sbt](https://www.scala-sbt.org)

## 许可证

如果配置了许可证合规<!--[许可证合规](../../compliance/license_compliance/index.md)--> CI 作业，<!--[已发现的许可证](../../compliance/license_compliance/index.md#supported-languages-and-package-managers)-->已发现的许可证显示在此页面上。

## 下载依赖项列表

您可以以 `JSON` 格式下载项目的完整依赖项列表及其详细信息。

### UI

您可以通过选择**导出**按钮，以 JSON 格式下载项目的依赖项列表及其详细信息。请注意，依赖项列表仅显示在默认分支上运行的最后一个成功流水线的结果。

### API

您可以[使用 API](../../../api/dependencies.md#list-project-dependencies) 下载项目的依赖项列表。请注意，仅提供由 Gemnasium 系列分析器识别的依赖项，[不是任何其他极狐GitLab 依赖项分析器](../dependency_scanning/analyzers.md)。
