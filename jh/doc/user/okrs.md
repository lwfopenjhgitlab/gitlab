---
stage: Plan
group: Product Planning
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 目标和关键结果（OKR） **(ULTIMATE)**

> 引入于 15.6 版本，[功能标志](../administration/feature_flags.md)为 `okrs_mvc`。默认禁用。

WARNING:
OKR 功能处于 **Alpha** 阶段。

FLAG:
在私有化部署版上，此功能默认不可用。要按项目启用，需要管理员[启用功能标志](../administration/feature_flags.md) `okrs_mvc`。在 SaaS 版上，此功能不可用。此功能尚未准备好用于生产。

目标和关键结果（OKR）是用于设置和跟踪与组织的总体战略和愿景保持一致的目标的框架。

极狐GitLab 中的目标和关键结果的很多特性功能相近。在文档中，术语 **OKR** 指的是目标和关键结果。

OKR 是一种工作项。
<!--For the roadmap of migrating [issues](project/issues/index.md) and [epics](group/epics/index.md)
to work items and adding custom work item types, see
[epic 6033](https://gitlab.com/groups/gitlab-org/-/epics/6033) or the
[Plan direction page](https://about.gitlab.com/direction/plan/).-->

## 设计有效的 OKR

使用目标和关键结果使您的员工朝着共同目标迈进并跟踪进度。
设定一个大目标为 O，并使用[子目标和关键结果](#child-objectives-and-key-results)来衡量大目标的完成情况。

**O** 是要实现的理想目标，并定义**您打算做什么**，通过将个人、团队或部门的工作与公司整体战略联系起来，展示他们的工作如何影响组织的总体方向。

**KR** 是针对一致目标的进展衡量标准，表达了**您如何知道是否达到了目标**（O）。
通过实现特定结果（KR），您可以为实现相关 O 取得进展。

您可以用这句话确定您的 OKR 是否有意义：

<!-- vale gitlab.FutureTense = NO -->
> 我/我们将通过获得和实现以下指标（KR），在（日期）之前完成（O）。
<!-- vale gitlab.FutureTense = YES -->

<!--
To learn how to create better OKRs and how we use them at GitLab, see the
[Objectives and Key Results handbook page](https://about.gitlab.com/company/okrs/).
-->

## 创建目标

先决条件：

- 您必须至少具有该项目的访客角色。

创建目标：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **议题**。
1. 在右上角的 **新建议题** 旁边，选择向下箭头 **{chevron-lg-down}**，然后选择 **新建目标**。
1. 再次选择 **新建目标**。
1. 输入目标名称。
1. 选择 **创建目标**。

要创建关键结果，请将其[添加为现有目标的子项](#add-a-child-key-result)。

<a id="view-an-objective"></a>

## 查看目标

先决条件：

- 您必须至少具有该项目的访客角色。

查看目标：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **议题**。
1. 使用 `Type = objective` [过滤议题列表](project/issues/managing_issues.md#filter-the-list-of-issues)。
1. 从列表中选择一个目标的标题。

<a id="view-a-key-result"></a>

## 查看关键结果

先决条件：

- 您必须至少具有该项目的访客角色。

查看关键结果：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **议题**。
1. 使用 `Type = key_result` [筛选议题列表](project/issues/managing_issues.md#filter-the-list-of-issues)。
1. 从列表中选择关键结果的标题。

或者，您可以从其父级目标中的 **子目标和关键结果** 部分访问关键结果。

## 编辑标题和描述

先决条件：

- 您必须至少具有该项目的报告者角色。

要编辑 OKR：

1. [打开您要编辑的目标](#view-an-objective)或[关键结果](#view-a-key-result)。
1. 可选。要编辑标题，请选择它，进行更改，然后选择标题文本框之外的任何区域。
1. 可选。要编辑说明，请选择编辑图标 (**{pencil}**)，进行更改，然后选择 **保存**。

## 查看 OKR 系统备注

> - 引入于 15.7 版本，[功能标志](../administration/feature_flags.md)为 `work_items_mvc_2`。默认禁用。
> - 功能标志变更于 15.8 版本，变更为 `work_items_mvc`。默认禁用。
> - 更改活动排序顺序功能引入于 15.8 版本。
> - 过滤活动功能引入于 15.10 版本。
> - 在 SaaS 和私有化部署版上默认启用于 15.10 版本。

先决条件：

- 您必须至少具有该项目的报告者角色。

您可以查看与该任务相关的所有系统备注。默认情况下，它们按 **最旧的优先** 排序。
您始终可以将排序顺序更改为 **最新的优先**，支持跨会话生效。

## 评论和主题

您可以在任务中添加[评论](discussions/index.md)和回复主题。

## 指派用户

要显示谁负责 OKR，您可以将用户分配给它。

免费版的用户可以为每个 OKR 分配一个用户。
专业版及更高版本的用户可以将多个用户分配给一个 OKR。
另请参阅[议题的多个指派人](project/issues/multiple_assignees_for_issues.md)。

先决条件：

- 您必须至少具有该项目的报告者角色。

要更改 OKR 的指派人：

1. [打开您要编辑的目标](#view-an-objective)或[关键结果](#view-a-key-result)。
1. 在 **指派人** 旁边，选择 **添加指派人**。
1. 从下拉列表中选择要添加为指派人的用户。
1. 选择下拉列表之外的任何区域。

## 分配标记

先决条件：

- 您必须至少具有该项目的报告者角色。

使用[标记](project/labels.md)，在团队之间组织 OKR。

为 OKR 添加标记：

1. [打开您要编辑的目标](okrs.md#view-an-objective)或[关键结果](#view-a-key-result)。
1. 在 **标记** 旁边，选择 **添加标记**。
1. 从下拉列表中选择要添加的标记。
1. 选择下拉列表之外的任何区域。

## 添加目标到里程碑

> 引入于 15.7 版本。

您可以将目标添加到[里程碑](project/milestones/index.md)。
查看目标时，您可以看到里程碑标题。

先决条件：

- 您必须至少具有该项目的报告者角色。

向里程碑添加目标：

1. [打开您要编辑的目标](#view-an-objective)。
1. 在 **里程碑** 旁边，选择 **添加到里程碑**。如果目标已经属于里程碑，则下拉列表会显示当前里程碑。
1. 从下拉列表中选择要与目标关联的里程碑。

## 设置目标进度

显示完成目标所需的工作量。

您只能在目标上手动设置进度，不支持从子目标或关键结果中自动汇总。

先决条件：

- 您必须至少具有该项目的报告者角色。

设定目标的进度：

1. [打开您要编辑的目标](#view-an-objective)。
1. 选择 **进度** 旁边的文本框。
1. 输入 0 到 100 之间的数字。

## 设置健康状态

> 引入于 15.7 版本。

为了更好地跟踪实现目标的风险，您可以为每个目标和关键结果分配一个[健康状态](project/issues/managing_issues.md#health-status)。
您可以使用健康状态向组织中的其他人发出信号，表明 OKR 是按计划进行还是需要注意保持进度。

先决条件：

- 您必须至少具有该项目的报告者角色。

设置 OKR 的健康状态：

1. [打开要编辑的关键结果](#view-a-key-result)。
1. 在 **健康状态** 旁边，选择下拉列表并选择所需的健康状态。

## 关闭 OKR

完成 OKR 后，您可以关闭它。
OKR 标记为已关闭但未删除。

先决条件：

- 您必须至少具有该项目的报告者角色。

关闭 OKR：

1. [打开您要编辑的目标](#view-an-objective)。
1. 在 **状态** 旁边，选择 **已关闭**。

您可以用同样的方式重新打开一个关闭的 OKR。

<a id="child-objectives-and-key-results"></a>

## 子目标和关键结果

在极狐GitLab 中，O 与 KR 类似。
在您的工作流程中，使用 KR 来衡量 O 中描述的目标。

您可以将子目标添加到总共 9 个级别。一个目标最多可以有 100 个子 OKR。
关键结果是目标的子项，关键结果本身不能有子项。

目标描述下方的 **子目标和关键结果** 部分，提供了子目标和关键结果。

### 添加子目标

先决条件：

- 您必须至少具有该项目的访客角色。

向目标添加新的子目标：

1. 在一个目标中，在 **子目标和关键结果** 部分，选择 **添加**，然后选择 **新建目标**。
1. 输入新目标的标题。
1. 选择 **创建目标**。

将现有目标添加到其它目标：

1. 在一个目标中，在 **子目标和关键结果** 部分，选择 **添加**，然后选择 **现有目标**。
1. 通过输入部分标题，搜索所需的目标，然后选择所需的匹配项。

    要添加多个目标，请重复此步骤。

1. 选择 **添加目标**。

### 添加子关键结果

先决条件：

- 您必须至少具有该项目的访客角色。

要向目标添加新的关键结果：

1. 在一个目标中，在 **子目标和关键结果** 部分，选择 **添加**，然后选择 **新建关键结果**。
1. 输入新关键结果的标题。
1. 选择 **创建关键结果**。

要将现有关键结果添加到目标：

1. 在一个目标中，在 **子目标和关键结果** 部分，选择 **添加**，然后选择 **现有关键结果**。
1. 通过输入部分标题，搜索所需的 OKR，然后选择所需的匹配项。

    要添加多个目标，请重复此步骤。

1. 选择 **添加关键结果**。
