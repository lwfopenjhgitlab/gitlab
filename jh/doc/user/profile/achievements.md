---
stage: Data Stores
group: Tenant Scale
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 成就（Alpha） **(FREE)**

> 引入于 15.10 版本，[功能标志](../../administration/feature_flags.md)为 `achievements`。默认禁用。

FLAG:
此功能尚未准备好用于生产。

成就是在极狐GitLab 上奖励用户的一种方式。
作为命名空间维护者或所有者，您可以为特定贡献创建自定义成就，您可以根据您的标准，授予用户成就或撤销用户成就。

作为用户，您可以收集成就，在您的个人资料中突出您对不同项目或小组的贡献。
成就由名称、描述和徽章组成。

![Achievements on user profile page](img/user_profile_achievements_v15_11.png)

此功能处于 Alpha 阶段。
<!--For more information about planned work, see [epic 9429](https://gitlab.com/groups/gitlab-org/-/epics/9429).
Tell us about your use cases by leaving comments in the epic.-->

## 成就类型

授予的成就包括以下类型：

- 只授予一次且不可撤销。例如，“首次贡献合并”成就。
- 只授予一次且可撤销。例如，“核心团队成员”成就。
- 可多次授予。例如，“月度贡献者”成就。

## 查看用户的成就

您可以在用户的个人资料页面上查看用户的成就。

先决条件：

- 用户资料必须公开。
- 您必须是授予成就的命名空间的成员，或者命名空间必须是公开的。

查看用户的成就：

1. 转到用户的个人资料页面。
1. 在用户头像下方，查看成就。
1. 要查看有关成就的详细信息，请将鼠标悬停在它上面，显示以下信息：

   - 成就名称
   - 成就描述
   - 授予用户成就的日期
   - 授予成就的命名空间

要获取用户的成就列表，请查询 `user` GraphQL 类型。

```graphql
query {
  user(username: "<username>") {
    userAchievements {
      nodes {
        achievement {
          name
          description
          avatarUrl
          namespace {
            fullPath
            name
          }
        }
      }
    }
  }
}
```

## 创建成就

您可以创建自定义成就来表彰特定贡献。

先决条件：

- 您必须具有命名空间的维护者或所有者角色。

要创建成就，请调用 `achievementsCreate` GraphQL mutation。

```graphql
mutation achievementsCreate($file: Upload!) {
  achievementsCreate(
    input: {
      namespaceId: "gid://gitlab/Namespace/<namespace id>",
      name: "<name>",
      description: "<description>",
      avatar: $file}
  ) {
    errors
    achievement {
      id
      name
      description
      avatarUrl
    }
  }
}
```

## 更新成就

您可以随时更改成就的名称、描述和徽章图片。

先决条件：

- 您必须具有命名空间的维护者或所有者角色。

要更新成就，请调用 `achievementsUpdate` GraphQL mutation。

```graphql
mutation achievementsUpdate($file: Upload!) {
  achievementsUpdate(
    input: {
      achievementId: "gid://gitlab/Achievements::Achievement/<achievement id>",
      name: "<new name>",
      description: "<new description>",
      avatar: $file}
  ) {
    errors
    achievement {
      id
      name
      description
      avatarUrl
    }
  }
}
```

## 授予成就

您可以向用户授予成就，表彰他们的贡献。
当用户获得成就时，他们会收到一封电子邮件通知。

先决条件：

- 您必须具有命名空间的维护者或所有者角色。

要向用户授予成就，请调用 `achievementsAward` GraphQL mutation。

```graphql
mutation {
  achievementsAward(input: {
    achievementId: "gid://gitlab/Achievements::Achievement/<achievement id>",
    userId: "gid://gitlab/User/<user id>" }) {
    userAchievement {
      id
      achievement {
        id
        name
      }
      user {
        id
        username
      }
    }
    errors
  }
}
```

## 撤销成就

如果您认为用户不再符合奖励标准，您可以撤销该用户的成就。

先决条件：

- 您必须具有命名空间的维护者或所有者角色。

要撤销成就，请调用 `achievementsRevoke` GraphQL mutation。

```graphql
mutation {
  achievementsRevoke(input: {
    userAchievementId: "gid://gitlab/Achievements::UserAchievement/<user achievement id>" }) {
    userAchievement {
      id
      achievement {
        id
        name
      }
      user {
        id
        username
      }
      revokedAt
    }
    errors
  }
}
```

## 删除成就

如果您认为您不再需要某项成就，则可以将其删除，将删除成就的所有相关授予和撤销实例。

先决条件：

- 您必须具有命名空间的维护者或所有者角色。

要删除成就，请调用 `achievementsDelete` GraphQL mutation。

```graphql
mutation {
  achievementsDelete(input: {
    achievementId: "gid://gitlab/Achievements::Achievement/<achievement id>" }) {
    achievement {
      id
      name
    }
    errors
  }
}
```

## 隐藏成就

如果您不想在您的个人资料上显示成就，您可以选择退出：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **主要设置** 部分，清除 **在您的个人资料上显示成就** 复选框。
1. 选择 **更新个人资料设置**。
