---
type: reference
stage: Manage
group: Authentication & Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 创建用户 **(FREE SELF)**

您可以通过以下方式创建用户：

- 手动通过登录页面或管理中心。
- 自动通过用户身份验证集成。

## 在登录页面创建用户

如果您已启用[注册](../../admin_area/settings/sign_up_restrictions.md)，用户可以通过以下任一方式创建自己的帐户：

- 选择登录页面上的 **立即注册** 链接。
- 导航到 `https://gitlab.example.com/users/sign_up`。

![Register Tab](img/register_v13_6.png)

<a id="create-users-in-admin-area"></a>

## 在管理中心创建用户

作为管理员用户，您可以手动创建用户：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **概览 > 用户** (`/admin/users`)。
1. 选择 **新建用户**。
1. 填写必填字段，例如姓名、用户名和电子邮件。
1. 选择 **创建用户**。

重置链接会发送到用户的电子邮件，他们必须在首次登录时设置密码。

要在不依赖电子邮件确认的情况下设置用户密码，请在按照前面的步骤创建用户后：

1. 选择用户。
1. 选择 **编辑**。
1. 填写密码和密码确认字段。
1. 选择 **保存更改**。

NOTE:
如果您想创建一个测试用户，您可以按照前面的步骤提供一个虚假的电子邮件并在最终确认中使用相同的密码。

## 通过身份验证集成创建用户

- 用户在首次使用 [LDAP 集成] (../../../administration/auth/ldap/index.md)登录时自动创建。
- 如果存在 `allow_single_sign_on` 设置，则用户在首次使用 [OmniAuth provider](../../../integration/omniauth.md) 登录时创建。
- 用户在首次使用 Group SAML<!--[Group SAML](../../group/saml_sso/index.md)--> 签名时创建。
- 在身份供应商中创建用户时，由 SCIM<!--[SCIM](../../group/saml_sso/scim_setup.md)--> 自动创建。

## 通过 Rails 控制台创建用户

WARNING:
无论运行是否正确，更改数据的命令可能会导致损坏。始终首先在测试环境中运行命令，并准备好要恢复的备份实例。

通过 Rails 控制台创建用户：

1. [启动 Rails 控制台会话](../../../administration/operations/rails_console.md#starting-a-rails-console-session)。
1. 运行以下命令：

   ```ruby
   u = User.new(username: 'test_user', email: 'test@example.com', name: 'Test User', password: 'password', password_confirmation: 'password')
   u.skip_confirmation! # Use it only if you wish user to be automatically confirmed. If skipped, user receives confirmation e-mail
   u.save!
   ```
