---
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 双重验证 **(FREE)**

双重身份验证 (2FA) 为您的帐户提供了额外的安全级别。要让其它人访问您的帐户，他们将需要您的用户名和密码以及访问您的第二个身份验证因素的权限。

极狐GitLab 支持作为身份验证的第二个因素：

- 基于时间的一次性密码 ([TOTP](https://datatracker.ietf.org/doc/html/rfc6238))。启用后，系统会在您登录时提示您输入验证码。验证码由您的一次性密码验证器（例如，您的一台设备上的密码管理器）生成。
- U2F 或 WebAuthn 设备。当您提供用户名和密码进行登录时，系统会提示您激活 U2F 或 WebAuthn 设备（通常通过按下按钮）。这代表您执行安全身份验证。

如果您设置了设备，还需要设置 TOTP，以便在设备丢失时仍然可以访问您的帐户。

## 使用具有双重身份验证的个人访问令牌

启用 2FA 后，您无法使用密码通过 HTTPS 或 GitLab API<!--[GitLab API](../../../api/index.md)--> 对 Git 进行身份验证。
您可以改用[个人访问令牌](../personal_access_tokens.md)。

<a id="oauth-credential-helpers"></a>

## OAuth 凭据 helpers

以下 Git 凭据 helpers 使用 OAuth 向极狐GitLab 进行身份验证，与双重身份验证兼容。第一次进行身份验证时，helper 会打开 Web 浏览器，极狐GitLab 会要求您对应用程序进行授权，后续身份验证不需要交互。

<a id="git-credential-manager"></a>

### Git 凭证管理器（GCM）

[Git 凭证管理器](https://github.com/GitCredentialManager/git-credential-manager) (GCM) 默认使用 OAuth 进行身份验证。GCM 无需任何手动配置即可支持 SaaS。要将 GCM 与私有化部署版结合使用，请参阅[技术支持文档](https://github.com/GitCredentialManager/git-credential-manager/blob/main/docs/gitlab.md)。

因此，您无需在每次推送时都重新进行身份验证，GCM 支持缓存以及在会话之间持续存在的各种特定于平台的凭据存储。无论您使用个人访问令牌还是 OAuth，此功能都很有用。

适用于 Windows 的 Git 包括 Git 凭据管理器。

Git Credential Manager 主要由 GitHub, Inc. 开发。它是一个开源项目，并得到社区的支持。

### git-credential-oauth

[git-credential-oauth](https://github.com/hickford/git-credential-oauth) 支持 SaaS 和几个流行的公开 hosts，无需任何手动配置。要与私有化部署版一起使用，请参阅 [git-credential-oauth 自定义 hosts 文档](https://github.com/hickford/git-credential-oauth#custom-hosts)。

许多 Linux 发行版都将 git-credential-oauth 作为一个软件包包含在内。

git-credential-oauth 是社区支持的开源项目。

## 启用双重验证

> - 帐户邮箱确认要求引入于 14.3。部署在 `ensure_verified_primary_email_for_2fa` 功能标志后，默认启用。
> - 帐户邮箱确认要求可用，功能标志 `ensure_verified_primary_email_for_2fa` 移除于 14.4。

有多种方法可以启用双因素身份验证 (2FA)：

- 使用一次性密码验证器。启用 2FA 后，备份您的[恢复码](#恢复码)。
- 使用 U2F 或 WebAuthn 设备。

在 14.3 和更高版本，必须确认您的帐户电子邮件才能启用双重身份验证。

### 一次性密码

要使用一次性密码启用 2FA：

1. **在极狐GitLab 中：**
    1. 访问您的[**用户设置**](../index.md#访问您的用户设置)。
    1. 选择 **帐户**。
    1. 选择 **启用双重认证**。
1. **在您的设备（通常是您的手机）上：**
   1. 安装兼容的应用程序，例如：
      - 基于云（推荐，因为如果您丢失了硬件设备，您可以恢复访问）：
        - [Authy](https://authy.com/)
      - 其他：
        - [Google Authenticator](https://support.google.com/accounts/answer/1066447?hl=en)
        - [Microsoft Authenticator](https://www.microsoft.com/en-us/security/mobile-authenticator-app)
   1. 在应用程序中，通过以下两种方式之一添加新条目：
      - 使用设备的相机扫描极狐GitLab 中显示的二维码以自动添加条目。
      - 输入提供的详细信息以手动添加条目。
1. **在极狐GitLab 中：**
   1. 在 **Pin 码** 字段中输入您设备上输入的六位数 PIN 码。
   1. 输入您的当前密码。
   1. 选择 **提交**。

NOTE:
DUO 不能用于 2FA。

如果您输入了正确的密码，系统会显示[恢复码](#恢复码)列表。下载它们并将它们保存在安全的地方。

### 使用 FortiAuthenticator 启用一次性密码

FLAG:
在自助管理实例上默认禁用，要使其对每个用户可用，请让管理员启用名为 `forti_authenticator` 的功能标志。

您可以将 FortiAuthenticator 用作极狐GitLab 中的一次性密码 (OTP) 提供程序。用户必须：

- 以相同的用户名存在于 FortiAuthenticator 和极狐GitLab 中。
- 在 FortiAuthenticator 中配置 FortiToken。

您需要 FortiAuthenticator 的用户名和访问令牌。下面显示的代码示例中的 `access_token` 是 FortAuthenticator 访问密钥。要获取令牌，请参阅 [`Fortinet 文档库`](https://docs.fortinet.com/document/fortiauthenticator/6.2.0/rest-api-solution-guide/158294/the-fortiauthenticator-api)中的 `REST API 解决方案指南`。

在极狐GitLab 中配置 FortiAuthenticator。在您的 GitLab 服务器上：

1. 打开配置文件：

   对于 Omnibus 实例：

   ```shell
   sudo editor /etc/gitlab/gitlab.rb
   ```

   对于源安装实例：

   ```shell
   cd /home/git/gitlab
   sudo -u git -H editor config/gitlab.yml
   ```

1. 添加 provider 配置：

   对于 Omnibus 实例：

   ```ruby
   gitlab_rails['forti_authenticator_enabled'] = true
   gitlab_rails['forti_authenticator_host'] = 'forti_authenticator.example.com'
   gitlab_rails['forti_authenticator_port'] = 443
   gitlab_rails['forti_authenticator_username'] = '<some_username>'
   gitlab_rails['forti_authenticator_access_token'] = 's3cr3t'
   ```

   对于源安装实例：

   ```yaml
   forti_authenticator:
     enabled: true
     host: forti_authenticator.example.com
     port: 443
     username: <some_username>
     access_token: s3cr3t
   ```

1. 保存配置文件。
1. 如果您通过 Omnibus 或从源安装 GitLab，[重新配置](../../../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)或 [重启极狐GitLab](../../../administration/restart_gitlab.md#源安装实例) 使更改生效。

### 使用 FortiToken Cloud 启用一次性密码

FLAG:
在自助管理实例上默认禁用，要使其对每个用户可用，请让管理员启用名为 `forti_token_cloud` 的功能标志。该功能尚未准备好用于生产用途。

您可以使用 FortiToken Cloud 作为 GitLab 中的一次性密码 (OTP) 提供程序。 用户必须：

- 以相同的用户名存在于 FortiToken Cloud 和 GitLab。
- 在 FortiToken Cloud 中配置 FortiToken。

您需要一个 `client_id` 和 `client_secret` 来配置 FortiToken Cloud。要获取这些信息，请参阅 [Fortinet 文档库] (https://docs.fortinet.com/document/fortitoken-cloud/latest/rest-api/456035/overview) 上的 REST API 指南。

在极狐GitLab 中配置 FortiToken Cloud。在您的 GitLab 服务器上：

1. 打开配置文件：

   对于 Omnibus 实例：

   ```shell
   sudo editor /etc/gitlab/gitlab.rb
   ```

   对于源安装实例：

   ```shell
   cd /home/git/gitlab
   sudo -u git -H editor config/gitlab.yml
   ```

1. 添加 provider 配置：

   对于 Omnibus 实例：

   ```ruby
   gitlab_rails['forti_token_cloud_enabled'] = true
   gitlab_rails['forti_token_cloud_client_id'] = '<your_fortinet_cloud_client_id>'
   gitlab_rails['forti_token_cloud_client_secret'] = '<your_fortinet_cloud_client_secret>'
   ```

   对于源安装实例：

   ```yaml
   forti_token_cloud:
     enabled: true
     client_id: YOUR_FORTI_TOKEN_CLOUD_CLIENT_ID
     client_secret: YOUR_FORTI_TOKEN_CLOUD_CLIENT_SECRET
   ```

1. 保存配置文件。
1. 如果您通过 Omnibus 或从源安装 GitLab，[重新配置](../../../administration/restart_gitlab.md#omnibus-gitlab-reconfigure)或[重启极狐GitLab](../../../administration/restart_gitlab.md#源安装实例) 以使更改生效。

### 设置 U2F 设备

官方只支持 [YubiKey](https://www.yubico.com/products/) U2F 设备，但用户已经成功使用过 [SoloKeys](https://solokeys.com/) 或 [Google Titan Security Key](https://cloud.google.com/titan-security-key)。

U2F 工作流由以下桌面浏览器[支持](https://caniuse.com/#search=U2F)：

- Chrome
- Edge
- Opera
- Firefox 67+，对于 Firefox 47-66：
  1. 在 [`about:config`](https://support.mozilla.org/en-US/kb/about-config-editor-firefox) 中启用 FIDO U2F API。
  1. 搜索 `security.webauth.u2f` 并选择它，切换到 `true`。


要使用 U2F 设备设置 2FA：

1. 访问您的[**用户设置**](../index.md#访问您的用户设置)。
1. 选择 **帐户**。
1. 选择 **启用双重认证**。
1. 连接您的 U2F 设备。
1. 选择 **设置新的 U2F 设备**。
1. 设备上的灯开始闪烁。通过按下它的按钮来激活它。

将显示一条消息，表明您的设备已成功设置。
单击 **注册 U2F 设备** 完成该过程。不会为 U2F 设备生成恢复代码。

### 设置 WebAuthn 设备

> - 引入于 13.4 版本，功能标志为 `webauthn`，默认禁用。
> - 在私有化部署版上启用于 14.6 版本。
> - WebAuthn 设备的可选一次性密码身份验证功能引入于 15.10 版本，[功能标志](../../../administration/feature_flags.md)为 `webauthn_without_topt`。在 SaaS 和私有化部署版上默认启用。

FLAG:
在私有化部署实例上，默认情况下此功能可用。要禁用该功能，请让管理员[禁用命名为 `webauthn` 的功能标志](../../../administration/feature_flags.md)。如果您在注册 WebAuthn 设备后禁用 WebAuthn 功能标志，则在您重新启用此功能之前，这些设备将无法使用。<!--在 GitLab.com 上，此功能可用。-->

WebAuthn 工作流由以下桌面浏览器[支持](https://caniuse.com/#search=webauthn)：

- Chrome
- Edge
- Firefox
- Opera
- Safari

以及以下移动浏览器：

- Chrome for Android
- Firefox for Android
- iOS Safari (since iOS 13.3)

要使用 WebAuthn 兼容设备设置 2FA：

1. 访问您的[**用户设置**](../index.md#访问您的用户设置)。
1. 选择 **帐户**。
1. 选择 **启用双重认证**。
1. 插入您的 WebAuthn 设备。
1. 选择 **设置新的 WebAuthn 设备**。
1. 根据您的设备，您可能需要按下按钮或触摸传感器。

将显示一条消息，表明您的设备已成功设置。
不会为 WebAuthn 设备生成恢复码。 

<a id="recovery-codes"></a>

## 恢复码

使用一次性密码成功启用 2FA 后，系统会立即提示您下载一组生成的恢复码。如果您无法访问一次性密码验证器，您可以使用这些恢复码之一登录您的帐户。

我们建议您复制和打印它们，或使用 **下载代码** 按钮下载它们，存储在安全的地方。如果您选择下载它们，该文件名为 `gitlab-recovery-codes.txt`。

NOTE:
不会为 U2F 或 WebAuthn 设备生成恢复码。

如果您丢失了恢复码，或者想要生成新码，您可以使用以下任一方法：

- [2FA 帐户设置](#重新生成双重身份验证恢复码)页面。
- [SSH](#使用-ssh-生成新恢复码)。

### 重新生成双重身份验证恢复码

要重新生成 2FA 恢复码，您需要访问桌面浏览器：

1. 访问您的[**用户设置**](../index.md#访问您的用户设置)。
1. 选择 **帐户 > 双重身份验证 (2FA)**。
1. 如果您已经配置了 2FA，请选择 **管理双重认证**。
1. 在 **注册双重认证** 窗格中，输入您的当前密码并选择 **重新生成恢复码**。

NOTE:
如果您重新生成 2FA 恢复码，请保存它们。您不能使用任何以前创建的 2FA 码。

## 在启用 2FA 的情况下登录

在启用 2FA 的情况下登录仅与正常登录过程略有不同。输入您的用户名和密码，您会看到第二个提示，具体取决于您启用的 2FA 类型。

### 使用一次性密码登录

当系统询问时，输入来自一次性密码验证器应用程序的 PIN 码或恢复码以登录。

### 使用 U2F 设备登录

要使用 U2F 设备登录：

1. 选择 **通过 U2F 设备登录**。
1. 设备上的灯开始闪烁。通过触摸/按下其按钮激活它。

将显示一条消息，表明您的设备已响应身份验证请求，并且您已自动登录。

### 使用 WebAuthn 设备登录

在支持的浏览器中，您应该在输入您的凭据后自动提示您激活您的 WebAuthn 设备（例如，通过触摸/按下其按钮）。

将显示一条消息，表明您的设备已响应身份验证请求并且您已自动登录。

## 禁用双重认证

如果您需要禁用 2FA：

1. 访问您的[**用户设置**](../index.md#访问您的用户设置)。
1. 进入 **帐户**。
1. 选择 **管理双重认证**。
1. 在 **注册双重认证**下，输入您当前的密码并选择 **禁用双重认证**。

将清除您所有的双重身份验证注册，包括移动应用程序和 U2F/WebAuthn 设备。

## 恢复选项

如果您无权访问您的代码生成设备，您可以使用以下方法恢复对您帐户的访问权限：

- [使用保存的恢复码](#使用保存的恢复码)，如果您在启用双重身份认证时保存了它们。
- [使用 SSH 生成新的恢复码](#使用-ssh-生成新恢复码)，如果您没有保存原始恢复码但有 SSH 密钥。

<!--
- [在您的帐户上禁用 2FA](#have-two-factor-authentication-disabled-on-your-account), 如果您没有恢复码或 SSH 密钥。
-->

### 使用保存的恢复码

为您的帐户启用双重身份验证会生成多个恢复码。如果您保存了这些代码，则可以使用其中之一登录。

要使用恢复代码，请在登录页面上输入您的用户名/电子邮件和密码。当提示输入双重码时，输入恢复码。

使用恢复码后，您将无法重复使用它。您仍然可以使用您保存的其他恢复码。

### 使用 SSH 生成新恢复码

用户在启用双重身份验证时经常忘记保存他们的恢复码。如果将 SSH 密钥添加到您的帐户，您可以使用 SSH 生成一组新的恢复码：

1. 在终端中运行：

   ```shell
   ssh git@jihulab.com 2fa_recovery_codes
   ```

   NOTE:
   在自助实例上，将上面命令中的 **`jihulab.com`** 替换为 GitLab 服务器主机名（`gitlab.example.com`）。

1. 系统会提示您确认要生成新码。继续这个过程会使之前保存的码失效：

   ```shell
   Are you sure you want to generate new two-factor recovery codes?
   Any existing recovery codes you saved will be invalidated. (yes/no)

   yes

   Your two-factor authentication recovery codes are:

   119135e5a3ebce8e
   11f6v2a498810dcd
   3924c7ab2089c902
   e79a3398bfe4f224
   34bd7b74adbc8861
   f061691d5107df1a
   169bf32a18e63e7f
   b510e7422e81c947
   20dbed24c5e74663
   df9d3b9403b9c9f0

   During sign in, use one of the codes above when prompted for your
   two-factor code. Then, visit your Profile Settings and add a new device
   so you do not lose access to your account again.
   ```

1. 转到登录页面并输入您的用户名/电子邮件和密码。当提示输入双重认证码时，输入从命令行输出中获得的恢复码之一。

登录后，立即使用新设备设置 2FA。

<!--
### Have two-factor authentication disabled on your account **(PREMIUM SAAS)**

If other methods are unavailable, submit a [support ticket](https://support.gitlab.com/hc/en-us/requests/new) to request
a GitLab global administrator disable 2FA for your account:

- Only the owner of the account can make this request.
- This service is only available for accounts that have a GitLab.com subscription. For more information, see our
  [blog post](https://about.gitlab.com/blog/2020/08/04/gitlab-support-no-longer-processing-mfa-resets-for-free-users/).
- Disabling this setting temporarily leaves your account in a less secure state. You should sign in and re-enable two-factor
  authentication as soon as possible.
-->

## 管理员注意事项

- 您需要特别注意在 <!--[恢复 GitLab 备份](../../../raketasks/backup_restore.md)-->恢复 GitLab 备份后 2FA 继续工作。
- 为确保 2FA 与 TOTP 服务器正确授权，您可能需要确保您的 GitLab 服务器的时间通过 NTP 等服务同步。否则，您可能会遇到由于时差导致授权总是失败的情况。
- 当 GitLab 实例从多个主机名或 FQDN 访问时，GitLab U2F 和 WebAuthn 实现不会工作。每个 U2F 或 WebAuthn 注册都链接到注册时的*当前主机名*，并且不能用于其他主机名/FQDN。这同样适用于 WebAuthn 注册。

  例如，如果用户尝试从 `first.host.xyz` 和 `second.host.xyz` 访问实例：

   - 用户使用 `first.host.xyz` 登录并注册他们的 U2F 密钥。
   - 用户注销并尝试使用 `first.host.xyz` 登录 - U2F 身份验证成功。
   - 用户注销并尝试使用 `second.host.xyz` 登录 - U2F 身份验证失败，因为 U2F 密钥仅在 `first.host.xyz` 上注册。

<!--
- To enforce 2FA at the system or group levels see [Enforce Two-factor Authentication](../../../security/two_factor_authentication.md).
-->

## 故障排查

### 错误："HTTP Basic: Access denied. The provided password or token ..."

发出请求时，您可能会收到以下错误：

```plaintext
HTTP Basic: Access denied. The provided password or token is incorrect or your account has 2FA enabled and you must use a personal
access token instead of a password.
```

在以下情况下会出现此错误：

- 您启用了 2FA 并尝试使用用户名和密码进行身份验证。
- 您没有启用 2FA，并且在您的请求中发送了错误的用户名或密码。
- 您没有启用 2FA，但管理员已启用[对所有用户强制执行 2FA](../../../security/two_factor_authentication.md#enforce-2fa-for-all-users) 设置。
- 您没有启用 2FA，但管理员已禁用[通过 HTTP(S) 为 Git 启用密码身份验证](../../admin_area/settings/sign_in_restrictions.md#password-authentication-enabled)设置。

相反，您可以进行身份验证：

- 使用[个人访问令牌](../personal_access_tokens.md) (PAT)：
  - 对于通过 HTTP(S) 的 Git 请求，需要具有 `read_repository` 或 `write_repository` 范围的 PAT。
  - 对于[极狐GitLab 容器镜像库](../../packages/container_registry/authenticate_with_container_registry.md)请求，需要具有 `read_registry` 或 `write_registry` 范围的 PAT。
  - 对于[依赖代理](../../packages/dependency_proxy/index.md#authenticate-with-the-dependency-proxy)请求，需要具有 `read_registry` 和 `write_registry` 范围的 PAT。
- 如果您配置了 LDAP，使用 [LDAP 密码](../../../administration/auth/ldap/index.md)。
- 使用 [OAuth 凭据 helpers](#oauth-credential-helpers)。

### 错误："invalid pin code"

如果您收到 `invalid pin code` 错误，这可能表明身份验证应用程序和实例本身之间存在时间同步问题。

为避免时间同步问题，请在生成代码的设备中启用时间同步。 例如：

- 对于 Android（谷歌身份验证器）：
   1. 进入 Google Authenticator 的主菜单。
   1. 选择设置。
   1. 选择代码的时间校正。
   1. 选择立即同步。
- 对于 iOS：
   1. 进入设置。
   1. 选择常规。
   1. 选择日期和时间。
   1. 启用自动设置。如果已启用，请将其禁用，等待几秒钟，然后重新启用。
