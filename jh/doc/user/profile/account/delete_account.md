---
type: howto
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 删除一个用户帐户 **(FREE)**

可以通过以下任一方式从实例中删除用户：

- 用户自己。
- 管理员。

NOTE:
删除用户会删除该用户命名空间中的所有项目。

## 删除您自己的帐户

作为用户，要删除您自己的帐户：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **帐户**。
1. 选择 **删除帐户**。

## 删除用户和用户贡献 **(FREE SELF)**

作为管理员，要删除用户帐户：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏上，选择 **概览 > 用户**。
1. 选择用户。
1. 在 **帐户** 选项卡下，选择：
    - **删除用户**：仅删除用户但保留其[关联记录](#关联记录)。如果所选用户是任何群组的唯一所有者，则不能使用此选项。
    - **删除用户和贡献**：删除用户及其相关记录。此选项还会删除用户是群组的唯一直接所有者的所有群组（以及这些群组中的项目）。继承的所有权不适用。

WARNING:
使用 **删除用户和贡献** 选项可能会导致删除比预期更多的数据。有关其他详细信息，请参阅[关联记录](#关联记录)。

<a id="associated-records"></a>

## 关联记录

- 只删除用户，并非所有关联的记录都会随用户一起删除。这些记录不会被删除，而是被移动到用户名为 Ghost User 的系统范围用户。Ghost 用户的目的是充当此类记录的容器。已删除用户所做的任何提交仍会显示原始用户的用户名。
- 删除用户及其贡献，包括：
  - 滥用报告
  - Emoji 回复
  - 史诗
  - 用户是唯一具有所有者角色的用户的群组。
  - 议题
  - 合并请求
  - 备注和评论
  - 个人访问令牌
  - 代码片段

删除的替代方法是[禁用用户](../../admin_area/moderate_users.md#禁用用户)。

当用户从滥用报告或垃圾邮件日志中删除时，这些关联的记录总是被删除。

<!--
The deleting associated records option can be requested in the [API](../../../api/users.md#user-deletion) as well as
the Admin Area.
-->

<!--
## 故障排查

### 删除用户会导致 PostgreSQL 空值错误

生成的错误如下：

```plaintext
ERROR: null value in column "user_id" violates not-null constraint
```

该错误可以在 PostgreSQL 日志和管理中心后台作业视图的 **Retries** 部分中找到。

If the user being deleted used the [iterations](../../group/iterations/index.md) feature, such
as adding an issue to an iteration, you must use
[the workaround documented in the issue](https://gitlab.com/gitlab-org/gitlab/-/issues/349411#workaround)
to delete the user.
-->
