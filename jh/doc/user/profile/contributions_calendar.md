---
stage: Manage
group: Workspace
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: concepts, howto
---

# 贡献日历 **(FREE)**

贡献日历显示过去 12 个月的[用户事件](#user-contribution-events)，包括在派生项目和[非公开](#show-private-contributions-on-your-user-profile-page)项目代码库中所做的贡献。

![Contributions calendar](img/contributions_calendar_v15_6.png)

方块的渐变颜色表示每天的贡献数。渐变范围从空白（0 个贡献）到深蓝色（超过 30 个贡献）。

<!--
NOTE:
The contribution calendar only displays contributions from the last 12 months, but issue [24264](https://gitlab.com/gitlab-org/gitlab/-/issues/24264) proposes to change this to more than 12 months. General improvements to the user profile are proposed in issue [8488](https://gitlab.com/groups/gitlab-org/-/epics/8488).
-->

<a id="user-contribution-events"></a>

## 用户贡献事件

极狐GitLab 跟踪以下贡献事件：

- `approved`
  - 合并请求
- `closed`
  - [史诗](../group/epics/index.md)
  - 议题
  - 合并请求
  - 里程碑
- 任何 `Noteable` 记录的 `commented`
  - 警报
  - 提交
  - 设计
  - 议题
  - 合并请求
  - 代码片段
- `created`
  - 设计
  - [史诗](../group/epics/index.md)
  - 议题
  - 合并请求
  - 里程碑
  - 项目
  - Wiki 页面
- `destroyed`
  - 设计
  - 里程碑
  - Wiki 页面
- `expired`
  - 项目成员资格
- `joined`
  - 项目成员资格
- `left`
  - 项目成员资格
- `merged`
  - 合并请求
- 单独或批量向存储库 `pushed` 提交（或从中删除提交）
  - 项目
- `reopened`
  - [史诗](../group/epics/index.md)
  - 议题
  - 合并请求
  - 里程碑
- `updated`
  - 设计
  - Wiki 页面

### 查看每日贡献

查看您的每日贡献：

1. 在顶部栏的右上角，选择您的头像。
1. 从下拉列表中选择您的姓名。
1. 在贡献日历中：
    - 要查看特定日期的贡献数量，请将鼠标悬停在图块上。
    - 要查看特定日期的所有贡献，请选择一个图块，将会出现一个列表显示所有的贡献和做出贡献的时间。

<a id="show-private-contributions-on-your-user-profile-page"></a>

### 在您的用户个人资料页面上显示非公开贡献

贡献日历图和最近的活动列表显示您对非公开项目的[贡献行动](#user-contribution-events)。

要查看非公开贡献：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **主要设置** 部分，选中 **在个人资料中包含非公开贡献** 复选框。
1. 选择 **更新配置文件设置**。

## 用户活动

### 关注用户的活动

您可以关注感兴趣的用户。
在 15.5 及更高版本中，您可以关注的最大用户数为 300。

要关注用户，您可以：

- 在用户的个人资料中，选择 **关注**。
- 将鼠标箭头悬停在用户名上，选择 **关注**。（引入于 15.0 版本）

要查看您关注的用户的活动：

1. 在菜单中，选择 **活动**。
1. 选择 **关注的用户** 选项卡。

### 检索用户活动作为 feed

极狐GitLab 提供用户活动的 RSS 源。订阅用户活动的 RSS 源：

1. 转到[用户的个人资料](#access-your-user-profile)。
1. 在右上角，选择提要符号 **{rss}**，将结果显示为 Atom 格式的 RSS 源。

结果的 URL 包含 feed 令牌和您有权查看的用户活动。
您可以将此 URL 添加到您的 feed 阅读器。

### 重置用户活动 feed 令牌

Feed 令牌很敏感，可以泄露机密议题中的信息。
如果您认为您的 feed 令牌已暴露，您应该重置它。

重置您的提要令牌：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏，选择 **访问令牌**。
1. 向下滚动。在 **Feed 令牌** 部分，选择 **重置此令牌** 链接。
1. 在确认框中，选择 **确定**。

一个新的令牌已生成。
