---
type: concepts, howto
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

<a id="personal-access-tokens"></a>

# 个人访问令牌 **(FREE)**

> - 引入于 12.6 版本：令牌到期通知。
> - 引入于旗舰版 12.6 版本：令牌生命周期限制。
> - 引入于 13.3 版本：令牌过期的附加通知。
> - 引入于 14.1 版本：预填充令牌名称和范围。

个人访问令牌可以替代 OAuth2<!--[OAuth2](../../api/oauth2.md)--> 并用于：

- 使用 GitLab API<!--[GitLab API](../../api/index.md#personalproject-access-tokens)--> 进行身份验证。
- 使用 HTTP 基本身份验证与 Git 进行身份验证。

在这两种情况下，您都使用个人访问令牌代替密码进行身份验证。

WARNING:
在 15.4 版本中已弃用创建没有过期的个人访问令牌的功能，并计划在 16.0 版本中删除。删除此功能后，需要为无到期时间的现有个人访问令牌添加到期时间。

<!--当自我管理的实例升级到 GitLab 16.0 时，会自动添加到期时间。 这种变化是一个突破性的变化。-->

个人访问令牌是：

- 启用双重身份验证（2FA）<!--[双重身份验证 (2FA)](account/two_factor_authentication.md)-->时需要。
- 与极狐GitLab 用户名一起使用，需要用户名的功能可以进行身份验证。例如，极狐GitLab 管理的 Terraform 状态后端<!--[GitLab managed Terraform state backend](../infrastructure/iac/terraform_state.md#using-a-gitlab-managed-terraform-state-backend-as-a-remote-data-source)-->和 Docker container registry<!--[Docker container registry](../packages/container_registry/index.md#authenticate-with-the-container-registry)-->，
- 类似于项目访问令牌<!--[项目访问令牌](../project/settings/project_access_tokens.md)-->，但附加到用户而不是项目或群组。

NOTE:
尽管是必需的，但在使用个人访问令牌进行身份验证时，会忽略极狐GitLab 用户名。
<!--There is an [issue for tracking](https://gitlab.com/gitlab-org/gitlab/-/issues/212953) to make GitLab
use the username.-->

<!--有关如何使用个人访问令牌对 API 进行身份验证的示例，请参阅 [API 文档](../../api/index.md#personalproject-access-tokens)。

或者，GitLab 管理员可以使用 API 创建 [模拟令牌](../../api/index.md#impersonation-tokens)。 使用模拟令牌以特定用户身份自动进行身份验证。-->

NOTE:
个人访问令牌不符合 FIPS 标准，并且在启用 [FIPS 模式](../../development/fips_compliance.md)时会禁用创建和使用。

## 创建个人访问令牌

> - 引入于 15.3 版本，UI 中填入了默认到期时间 30 天。
> - 创建不过期的个人访问令牌的功能删除于 16.0 版本。

您可以根据需要创建任意数量的个人访问令牌。

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **访问令牌**。
1. 输入令牌的名称和到期日期。
   - 令牌在该日期的午夜 UTC 到期。
   - 如果您不输入到期日期，到期日期将自动设置为比当前日期晚 365 天。
   - 默认情况下，此日期最多可以比当前日期晚 365 天。
1. 设置[选择范围](#personal-access-token-scopes)。
1. 选择 **创建个人访问令牌**。

将个人访问令牌保存在安全的地方。离开页面后，您将无法再访问令牌。

### 预填充个人访问令牌名称和范围

您可以直接链接到个人访问令牌页面，并在表单中预先填写名称和范围列表。为此，您可以向 URL 附加一个 `name` 参数和以逗号分隔的范围列表。例如：

```plaintext
https://gitlab.example.com/-/profile/personal_access_tokens?name=Example+Access+token&scopes=api,read_user,read_registry
```

WARNING:
必须谨慎对待个人访问令牌。<!--Read our [token security considerations](../../security/token_overview.md#security-considerations)
for guidance on managing personal access tokens (for example, setting a short expiry and using minimal scopes).-->

## 撤销个人访问令牌

您可以随时撤销个人访问令牌。

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **访问令牌**。
1. 在 **有效的个人访问令牌** 区域，在密钥旁边，选择 **撤销**。

## 查看上次使用令牌的时间

令牌使用情况每 24 小时更新一次。每次使用令牌请求 API 资源<!--[API 资源](../../api/api_resources.md)-->和 <!--[GraphQL API](../../api/graphql/index.md)-->GraphQL API 时都会更新它。

查看上次使用令牌的时间：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **访问令牌**。
1. 在 **有效的个人访问令牌** 区域，在密钥旁边，查看 **最近使用** 日期。

<a id="personal-access-token-scopes"></a>

## 个人访问令牌范围

> 个人访问令牌不再能够访问容器镜像库或软件包库于 16.0 版本。

个人访问令牌可以根据分配的范围执行操作。

| 范围              | 访问 |
|--------------------|--------|
| `api`              | 完整 API 读写，包括所有群组和项目、容器镜像库和软件包库。 |
| `read_user`        | `/users` 下的端点只读。本质上，访问 <!--[Users API](../../api/users.md)--> 中的任何 `GET` 请求。 |
| `read_api`         | 完整 API 只读，包括所有群组和项目、容器镜像库和软件包库。（引入于 12.10 版本） |
| `read_repository`  | 通过 `git clone` 对仓库进行只读（拉取）。 |
| `write_repository` | 通过 `git clone` 对仓库进行读写（拉取、推送）。 |
| `read_registry`    | 如果项目是私有的并且需要授权，则 Container Registry<!--[Container Registry](../packages/container_registry/index.md)--> 镜像只读（拉取）。仅当 Container Registry 启用时适用。 |
| `write_registry`   | 如果项目是私有的并且需要授权，则对 Container Registry<!--[Container Registry](../packages/container_registry/index.md)--> 镜像进行读写（推送）。仅当 Container Registry 启用时适用。（引入于 12.10 版本） |
| `sudo`             | 作为系统中任何用户的 API 操作（如果经过身份验证的用户是管理员）。 |

WARNING:
如果启用了[外部授权](../admin_area/settings/external_authorization.md)，个人访问令牌无法访问容器镜像库或软件包库。如果您使用个人访问令牌访问它们，系统会中断对这些令牌的使用。禁用外部授权可以将个人访问令牌用于容器镜像库或软件包库。

## 个人访问令牌到期时

个人访问令牌在您定义的日期（UTC 午夜）到期。

- 极狐GitLab 在每天 01:00 AM UTC 运行检查，以识别在接下来的 7 天内到期的个人访问令牌。这些令牌的所有者会收到电子邮件通知。
- 极狐GitLab 每天在 UTC 时间凌晨 02:00 运行检查，以识别在当前日期过期的个人访问令牌。这些令牌的所有者会收到电子邮件通知。
- 在旗舰版中，管理员可以[限制个人访问令牌的生命周期](../admin_area/settings/account_and_limit_settings.md#limit-the-lifetime-of-access-tokens)。

## 以编程方式创建个人访问令牌 **(FREE SELF)**

您可以创建预先确定的个人访问令牌作为测试或自动化的一部分。

先决条件：

- 您需要足够的访问权限才能为您的实例运行 Rails 控制台会话<!--[Rails 控制台会话](../../administration/operations/rails_console.md#starting-a-rails-console-session)-->。

以编程方式创建个人访问令牌：

1. 打开 Rails 控制台：

   ```shell
   sudo gitlab-rails console
   ```

1. 运行以下命令以引用用户名、令牌和范围。

    令牌长度必须为 20 个字符。范围必须有效并且[在源代码中](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/auth.rb)可见。

    例如，要创建属于用户名为 `automation-bot` 的用户的令牌：

   ```ruby
   user = User.find_by_username('automation-bot')
   token = user.personal_access_tokens.create(scopes: [:read_user, :read_repository], name: 'Automation token')
   token.set_token('token-string-here123')
   token.save!
   ```

可以使用 Rails runner<!--[Rails runner](../../administration/troubleshooting/debug.md#using-the-rails-runner)--> 将此代码缩短为单行 shell 命令：

```shell
sudo gitlab-rails runner "token = User.find_by_username('automation-bot').personal_access_tokens.create(scopes: [:read_user, :read_repository], name: 'Automation token'); token.set_token('token-string-here123'); token.save!"
```

## 以编程方式撤销个人访问令牌 **(FREE SELF)**

作为测试或自动化的一部分，您可以以编程方式撤销个人访问令牌。

先决条件：

- 您需要足够的访问权限才能为您的实例运行 Rails 控制台会话<!--[Rails 控制台会话](../../administration/operations/rails_console.md#starting-a-rails-console-session)-->。

以编程方式撤销令牌：

1. 打开 Rails 控制台：

   ```shell
   sudo gitlab-rails console
   ```

1. 要撤销 `token-string-here123`的令牌，请运行以下命令：

   ```ruby
   token = PersonalAccessToken.find_by_token('token-string-here123')
   token.revoke!
   ```

可以使用 Rails runner<!--[Rails runner](../../administration/troubleshooting/debug.md#using-the-rails-runner)--> 将此代码缩短为单行 shell 命令：

```shell
sudo gitlab-rails runner "PersonalAccessToken.find_by_token('token-string-here123').revoke!"
```

## 使用个人访问令牌克隆仓库 **(FREE SELF)**

要在禁用 SSH 时克隆仓库，需通过运行以下命令使用个人访问令牌克隆：

```shell
git clone https://<username>:<personal_token>@jihulab.com/gitlab-cn/gitlab.git
```

此方法将您的个人访问令牌保存在您的 bash 历史记录中。为避免这种情况，请运行以下命令：

```shell
git clone https://<username>@jihulab.com/gitlab-cn/gitlab.git
```

当系统要求您输入 `https://jihulab.com` 的密码时，请输入您的个人访问令牌。

`clone` 命令中的 `username`：

- 可以是任何字符串值。
- 不得为空字符串。

如果您设置依赖于身份验证的自动化流水线，需要注意这一点。

## 故障排除

### 取消撤销个人访问令牌 **(FREE SELF)**

如果个人访问令牌被任何方法意外撤销，管理员可以取消撤销该令牌。默认情况下，每日作业会在系统时间凌晨 1:00 删除已撤销的令牌。

WARNING:
运行以下命令直接更改数据。无论操作是否正确，都可能会造成损害。您应该首先在测试环境中运行这些命令，并准备好要恢复的实例备份，以防万一。

1. 打开 [Rails 控制台](../../administration/operations/rails_console.md#starting-a-rails-console-session)。
1. 撤销令牌：

   ```ruby
   token = PersonalAccessToken.find_by_token('<token_string>')
   token.update!(revoked:false)
   ```

   例如，取消撤销 `token-string-here123` 的令牌：

   ```ruby
   token = PersonalAccessToken.find_by_token('token-string-here123')
   token.update!(revoked:false)
   ```

## 个人访问令牌的替代品

对于基于 Git over HTTPS，个人访问令牌的替代方法是 [Oauth 凭据 helper](account/two_factor_authentication.md#oauth-credential-helpers)。
