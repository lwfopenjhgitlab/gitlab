---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: concepts, howto
---

# 个人资料偏好设置

用户的个人资料偏好设置页面允许用户根据自己的喜好自定义极狐GitLab 的各个方面。

要导航到您的个人资料的偏好设置：

1. 在右上角，选择您的头像。
1. 选择 **偏好设置**。

<a id="navigation-theme"></a>

## 导航主题

应用导航主题设置允许您个性化您的应用体验。您可以从多个颜色主题中进行选择，为顶部导航和左侧导航添加独特的颜色。使用单独的颜色主题可能会帮助您区分不同的极狐GitLab 实例。

默认主题是 Indigo。您可以选择 10 个主题：

- Indigo
- Light Indigo
- Blue
- Light Blue
- Green
- Light Green
- Red
- Light Red
- Dark
- Light
- 深色模式<!--[深色模式](#深色模式)-->

<!--
## 深色模式

> 引入于 13.1 版本，处于 Alpha 状态。


GitLab has started work on dark mode! The dark mode Alpha release is available in the
spirit of iteration and the lower expectations of
[Alpha versions](https://about.gitlab.com/handbook/product/gitlab-the-product/#alpha).

Progress on dark mode is tracked in the [Dark theme epic](https://gitlab.com/groups/gitlab-org/-/epics/2902).
See the epic for:

- A list of known issues.
- Our planned direction and next steps.

If you find an issue that isn't listed, please leave a comment on the epic or create a
new issue.


出于 MVC 和兼容性原因，深色模式可用作导航主题。[存在问题](https://gitlab.com/gitlab-org/gitlab/-/issues/219512) 使其在自己的部分中进行配置，并支持不同的导航主题。

深色主题仅适用于 **Dark** 语法高亮主题。
-->

<a id="syntax-highlighting-theme"></a>

## 语法高亮主题

极狐GitLab 使用 rouge Ruby 库在任何编辑器上下文之外突出显示语法。WebIDE（如 Snippets）使用 [Monaco Editor](https://microsoft.github.io/monaco-editor/) 并提供 [Monarch](https://microsoft.github.io/monaco-editor/monarch.html) 语法高亮库。 有关受支持语言的列表，请访问相应库的文档。

更改此设置允许您在极狐GitLab 上查看任何语法突出显示的代码时自定义颜色主题。


![Profile preferences syntax highlighting themes](img/profile-preferences-syntax-themes_v15_11.png)

在 13.6 版本中引入了主题 Solarized 和 Monokai 也适用于 Web IDE <!--[Web IDE](../project/web_ide/index.md)--> 和代码片段<!--[Snippets](../snippets.md)-->。

## 差异颜色

差异是将旧的/删除的内容与新的/添加的内容进行比较（例如，当[审核合并请求](../project/merge_requests/reviews/index.md#review-a-merge-request) 或 [Markdown 内联差异](../markdown.md#inline-diff))。
通常，红色和绿色用于差异中删除和添加的行。
确切的颜色取决于所选的[语法高亮主题](#syntax-highlighting-theme)。
在红绿色盲的情况下，颜色可能会导致困难。

因此，您可以自定义以下颜色：

- 删除行的颜色
- 添加行的颜色

## 表现

以下设置允许您自定义布局的表现，以及仪表板和项目登录页面的默认视图。

### 版面宽度

根据您的喜好使用不同的宽度。在固定（最大`1280px`）和流体（`100%`）应用程序布局之间进行选择。

NOTE:
虽然 `1280px` 是使用固定布局时的标准最大宽度，但某些页面仍然使用 100% 宽度，具体取决于内容。

### 主页

使用此设置可以更改极狐GitLab 页面左上角的图标。

### 群组概述内容

**群组概述内容** 下拉菜单允许您选择在群组主页上显示哪些信息。

您可以在 2 个选项之间进行选择：

- 详细信息（默认）
- 安全仪表盘<!--[安全仪表盘](../application_security/security_dashboard/index.md)-->

### 项目概览内容

**项目概览内容** 设置允许您选择要在项目主页上看到的内容。

### 制表符宽度

您可以在极狐GitLab 的各个部分设置制表符的显示宽度，例如，blobs、差异和代码片段。

NOTE:
某些部分不遵守此设置，包括 WebIDE、文件编辑器和 Markdown 编辑器。

## 本地化

### 语言

从支持的语言列表中选择您的首选语言。

*此功能是实验性的，翻译尚未完成。*

### 一周的第一天

可以为日历视图和日期选择器自定义一周的第一天。

您可以选择以下选项之一作为一周的第一天：

- 周六
- 周日
- 周一

如果您选择 **系统默认**，则使用实例默认<!--[instance default](../admin_area/settings/index.md#default-first-day-of-the-week)-->设置。

## 时间偏好

### 使用相对时间

> 引入于 14.1 版本。

您可以为用户界面选择您喜欢的时间格式：

- 相对时间，例如 `30 分钟前`。
- 绝对时间，例如 `May 18, 2021, 3:57 PM`。

时间的格式取决于您选择的语言和浏览器区域设置。

设置您的时间偏好：

1. 在 **偏好设置** 页面，进入 **时间偏好**。
1. 选中 **使用相对时间** 复选框以使用相对时间，或清除复选框以使用绝对时间。
1. 选择 **保存修改**。

NOTE:
此功能是实验性的，选择绝对时间可能会破坏某些布局。<!--如果您注意到使用绝对时间会破坏布局，请打开一个问题。-->

## 集成

使用第三方服务配置您的首选项，这些服务可增强您的体验。

### Sourcegraph

NOTE:
只有当管理员启用了 Sourcegraph 时，此设置才可见。

管理由 Sourcegraph 提供支持的集成代码智能功能的可用性。<!--查看 [Sourcegraph 功能文档](../../integration/sourcegraph.md#enable-sourcegraph-in-user-preferences) 了解更多信息。-->

### Gitpod

启用和禁用 GitLab-Gitpod 集成<!--[GitLab-Gitpod 集成](../../integration/gitpod.md)-->。仅在管理员配置集成后可见。<!--查看 [Gitpod 功能文档](../../integration/gitpod.md) 了解更多信息。-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
