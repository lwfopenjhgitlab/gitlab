---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: howto
---

# 评论模板 **(FREE)**

> 引入于 14.9 版本，[功能标志](../../administration/feature_flags.md)为 `saved_replies`。默认禁用。

使用评论模板可以在以下对象的文本区域，创建和重复使用文本：

- 合并请求，包括差异。
- 议题，包括设计管理评论。
- 史诗。
- 工作项。

评论模板可以很小，比如批准合并请求并取消指派自己；也可以很大，比如您经常使用的样板文本块：

![Comment templates dropdown list](img/saved_replies_dropdown_v15_10.png)

## 在文本区域中使用评论模板

要在您的评论中包含评论模板的文本：

1. 在评论的编辑器工具栏中，选择 **评论模板** (**{symlink}**)。
1. 选择您想要的评论模板。

## 创建评论模板

创建评论模板以供将来使用：

1. 在顶部栏的右上角，选择您的头像。
1. 从下拉列表中选择 **偏好设置**。
1. 在左侧边栏中，选择 **评论模板** (**{symlink}**)。
1. 为您的评论模板提供一个 **名称**。
1. 输入您回复的 **内容**。您可以使用在其他文本区域中使用的任何格式。
1. 选择 **保存**，页面将重新加载并显示您的评论模板。

## 查看您的评论模板

查看您的评论模板：

1. 在顶部栏的右上角，选择您的头像。
1. 从下拉列表中选择 **偏好设置**。
1. 在左侧边栏中，选择 **评论模板** (**{symlink}**)。
1. 滚动到 **我的评论模板**。

## 编辑或删除评论模板

编辑或删除以前的评论模板：

1. 在顶部栏的右上角，选择您的头像。
1. 从下拉列表中选择 **偏好设置**。
1. 在左侧边栏中，选择 **评论模板** (**{symlink}**)。
1. 滚动到 **我的评论模板**，然后确定您要编辑的评论模板。
1. 要编辑，请选择 **编辑** (**{pencil}**)。
1. 要删除，请选择 **删除** (**{remove}**)，然后从窗口中再次选择 **删除**。
