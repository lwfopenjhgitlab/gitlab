---
type: index, howto
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 用户帐户 **(FREE)**

每个帐户都有用户资料，其中包含有关您和您的极狐GitLab 活动的信息。

您的个人资料还包括用于自定义 GitLab 体验的设置。

<a id="access-your-user-profile"></a>

## 访问您的用户资料

要访问您的用户资料：

1. 在顶部栏的右上角，选择您的头像。
1. 选择您的姓名或用户名。

<a id="access-your-user-settings"></a>

## 访问您的用户设置

要访问您的用户设置：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。

## 更改您的用户名

您的用户名有一个唯一的[命名空间](../namespace/index.md)，当您更改用户名时会更新。<!--在更改用户名之前，请阅读 [重定向行为的方式](../project/repository/index.md#what-happens-when-a-repository-path-changes)。-->如果您不想更新命名空间，您可以创建一个新用户或群组并将项目转移到其中。

先决条件：

- 您的命名空间不能包含带有 Container Registry<!--[Container Registry](../packages/container_registry/index.md)--> 标签的项目。
- 您的命名空间不能有托管 GitLab Pages <!--[GitLab Pages](../project/pages/index.md)-->的项目。<!--For more information,
  see [this procedure in the GitLab Team Handbook](https://about.gitlab.com/handbook/tools-and-tips/#how-to-change-your-username-at-gitlabcom).-->

要更改您的用户名：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **帐号**。
1. 在 **更改用户名** 部分，输入新用户名作为路径。
1. 选择 **更新用户名**。

## 将电子邮件添加到您的用户个人资料

要将新电子邮件添加到您的帐户：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **电子邮件**。
1. 在 **电子邮件** 文本框中，输入新的电子邮件。
1. 选择 **添加电子邮件地址**。
1. 使用收到的验证电子邮件验证您的电子邮件地址。

## 将您的用户个人资料页面设为私密

您可以使您的用户个人资料仅对您和管理员可见。

要将您的个人资料设为私密：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 选中 **非公开资料** 复选框。
1. 选择 **更新个人资料设置**。

以下内容从您的用户个人资料页面 (`https://gitlab.example.com/username`) 中隐藏：

- Atom feed
- 帐户创建日期
- 活动、群组、参与贡献的项目、个人项目、星标项目和代码片段的标签页。

NOTE:
将您的所有用户个人资料页面设为私有不会对 REST 或 GraphQL API 隐藏您的公共资源。

### 用户可见性

位于 `/username` 的用户公共页面始终可见，无论您是否登录。

访问用户的公共页面时，您只能看到您有权访问的项目。

如果公开级别受到限制<!--[公开级别受到限制](../admin_area/settings/visibility_and_access_controls.md#restrict-visibility-levels)-->，则用户个人资料仅对登录用户可见。

<a id="add-details-to-your-profile-with-a-readme"></a>

## 使用 README 向您的个人资料添加详细信息

> 引入于 14.5 版本。

您可以使用 README 文件将更多信息添加到您的个人资料页面。当您使用信息填充 README 文件时，它会包含在您的个人资料页面中。

### 从一个新项目

要创建一个新项目并将其 README 添加到您的个人资料中：

1. 在顶部栏上，选择 **菜单 > 项目 > 查看所有项目**。
1. 在页面右侧，选择 **新建项目**。
1. 选择 **创建空白项目**。
1. 输入项目详情：
    - 在 **项目名称** 字段中，输入新项目的名称。
    - 在 **项目 URL** 字段中，选择您的用户名。
    - 在 **项目标识串** 字段中，输入您的用户名。
1. 在 **可见性级别** 中，选择**公开**。
   ![Proper project path for an individual on the hosted product](img/personal_readme_setup_v14_5.png)
1. 对于 **项目配置**，确保选中 **使用自述文件初始化仓库**。
1. 选择 **创建项目**。
1. 在这个项目中创建一个 README 文件。该文件可以是任何有效的 README 或索引文件。
1. 使用 [Markdown](../markdown.md) 填写 README 文件，, 或其它 [支持的标记语言](../project/repository/index.md#supported-markup-languages)。

极狐GitLab 在贡献图下方显示 README 的内容。

### 从现有项目

要将现有项目中的 README 添加到您的个人资料，请更新路径来匹配您的用户名。

<!--
## Add external accounts to your user profile page

You can add links to certain other external accounts you might have, like Skype and Twitter.
They can help other users connect with you on other platforms.

To add links to other accounts:

1. On the top bar, in the top-right corner, select your avatar.
1. Select **Edit profile**.
1. In the **Main settings** section, add your information from:
   - Skype
   - LinkedIn
   - Twitter
1. Select **Update profile settings**.
-->

## 在您的用户个人资料页面上显示私人贡献

在用户贡献日历图表和最近活动列表中，您可以看到您对私人项目的贡献活动<!--[贡献活动](../../api/events.md#action-types)-->。

要显示私人贡献：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **主要设置** 部分，选中 **在个人资料中包含非公开贡献** 复选框。
1. 选择 **更新个人资料设置**。

## 添加您的性别代词

> 引入于 14.0 版本。

您可以将性别代词添加到您的帐户，在您的个人资料中，显示在您的姓名旁边。

要指定您的代词：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **称呼** 文本框中，输入您的代词。文本不得超过 50 个字符。
1. 选择 **更新个人资料设置**。

## 添加您的姓名发音

> 引入于 14.2 版本。

您可以将您的姓名发音添加到您的帐户。显示在您的个人资料中，在您的姓名下方。

要添加您的姓名发音：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **称呼** 文本框中，输入您的姓名的发音方式。必须是纯文本且不超过 255 个字符。
1. 选择 **更新个人资料设置**。

## 设置您的当前状态

> 引入于 13.10 版本，用户可以安排清除他们的状态。

您可以为您的用户个人资料提供自定义状态消息以及描述它的表情符号。
当您不在办公室或因其他原因无法联系时，这可能会有所帮助。

即使您的[个人资料是私密的](#将您的用户个人资料页面设为私密)，您的状态也是公开可见的。

要设置您的当前状态：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **设置状态**，或者，如果您已经设置了状态，则选择 **编辑状态**。
1. 设置所需的表情符号和状态信息。状态消息必须是纯文本且不超过 100 个字符。它们还可以包含表情符号代码，例如“我在度假 :palm_tree:”。
1. 从 **清除状态** 下拉列表中选择一个值。
1. 选择 **设置状态**。或者，您可以选择 **删除状态** 以完全删除您的用户状态。

<!--You can also set your current status by [using the API](../../api/users.md#user-status).-->

如果您选中 **忙碌中** 复选框，请记住在您再次有空时将其清除。

## 设置忙碌状态指示

> - 引入于 13.6 版本。
> - 部署在功能标志后，默认禁用。
> - 于 13.8 版本变为默认启用。
> - 功能标志移除于 13.12 版本。

要向其他人表明您很忙，您可以设置一个指示器。

要设置忙碌状态指示，请执行以下任一操作：

- 直接设置：
   1. 在顶部栏的右上角，选择您的头像。
   1. 选择 **设置状态**，或者，如果您已经设置了状态，则选择 **编辑状态**。
   1. 选中 **设置自己为忙碌中** 复选框。

- 在您的个人资料中设置：
   1. 在顶部栏的右上角，选择您的头像。
   1. 选择 **编辑个人资料**。
   1. 在 **当前状态** 部分，选中 **设置自己为忙碌中** 复选框。

   每次在用户界面中显示您的姓名时，忙碌状态都会显示在您的姓名旁边。

<a id="set-your-time-zone"></a>

## 设置您的时区

您可以将本地时区设置为：

- 在您的个人资料中显示您的当地时间，以及将鼠标悬停在您的姓名上方会显示您的信息的地方。
- 将您的贡献日历与您的当地时间保持一致，以更好地反映您的贡献时间(引入于 14.5 版本)。

要设置您的时区：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **时间设置** 部分，从下拉列表中选择您的时区。

<a id="change-the-email-displayed-on-your-commits"></a>

## 更改提交中显示的电子邮件

提交电子邮件是通过极狐GitLab 界面执行的每个 Git 相关操作中显示的电子邮件地址。

您自己的任何经过验证的电子邮件地址都可以用作提交电子邮件。默认情况下使用您的主要电子邮件。

要更改您的提交电子邮件：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **提交邮件** 下拉列表中，选择一个电子邮件地址。
1. 选择 **更新个人资料设置**。

## 更改您的主要电子邮件

您的主要电子邮件：

- 是您登录、提交电子邮件和通知电子邮件的默认电子邮件地址。
- 必须已经[链接到您的用户个人资料](#将电子邮件添加到您的用户个人资料)。

要更改您的主要电子邮件：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **电子邮件** 字段中，输入您的新电子邮件地址。
1. 选择 **更新个人资料设置**。

## 设置您的公开电子邮件

您可以选择您的[已配置电子邮件地址](#将电子邮件添加到您的用户个人资料) 之一以显示在您的公开个人资料中：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **公开邮件** 字段中，选择可用的电子邮件地址之一。
1. 选择 **更新个人资料设置**。

<a id="use-an-automatically-generated-private-commit-email"></a>

### 使用自动生成的私人提交电子邮件

系统提供了一个自动生成的私人提交电子邮件地址，因此您可以将您的电子邮件信息保密。

要使用私人提交电子邮件：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在 **提交邮件** 下拉列表中，选择 **使用私人电子邮件**。
1. 选择 **更新个人资料设置**。

每个与 Git 相关的操作都使用私人提交电子邮件。

要保持完全匿名，您还可以复制私人提交电子邮件并使用以下命令在本地计算机上配置它：

```shell
git config --global user.email <your email address>
```

## 关注用户

极狐GitLab 跟踪[用户贡献活动](contributions_calendar.md)。您可以从以下位置关注或取消关注其他用户。

- 他们的[用户个人资料](#access-your-user-profile)。
- 将鼠标悬停在用户名上时出现的小弹出框（引入于 15.0 版本）。

在 15.5 及更高版本中，您可以关注的最大用户数为 300。

### 禁止关注和被其他用户关注

> 引入于 16.0 版本，[功能标志](../feature_flags.md)为 `disable_follow_users`。默认禁用。

您可以禁用关注和被其他用户关注。

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 选择 **偏好设置**。
1. 清除 **启用关注用户** 复选框。
1. 选择 **保存更改**。

NOTE:
禁用此功能时，将删除所有关注/被关注的连接。

## 查看您的活动

极狐GitLab 跟踪[用户贡献活动](contributions_calendar.md)。
要查看您的活动摘要或其他用户的活动：

1. 从用户的个人资料中，选择 **关注**。
1. 在菜单中，选择 **活动**。
1. 选择 **关注的用户** 标签。

## 会话持续时间

### 保持登录状态两周

默认情况下，您会在 7 天（10080 分钟）不活动后，或直到您关闭浏览器窗口后退出极狐GitLab（以先到者为准）。

极狐GitLab 管理员可以[更改此默认设置](../admin_area/settings/account_and_limit_settings.md#customize-the-default-session-duration)。

### 无限期保持登录状态

> 打开或关闭**记住我**设置的功能引入于 16.0 版本。

要无限期保持登录状态，请选中极狐GitLab 登录页面上的**记住我**复选框。

您保持登录状态是因为，虽然服务器将会话时间设置为一周，但您的浏览器存储了一个安全令牌，可以启用自动重新身份验证。

对于出于安全或合规目的要求会话定期过期的环境，极狐GitLab 管理员可以[关闭**记住我**设置](../admin_area/settings/account_and_limit_settings.md#session-duration)。

<a id="cookies-used-for-sign-in"></a>

### 用于登录的 Cookies

> 引入于 13.1 版本。

当您登录时，会设置三个 cookie：

- 一个名为 `_gitlab_session` 的会话 cookie。此 cookie 没有设置过期日期。但是，它会根据 `session_expire_delay` 的设置过期。
- 一个名为 `about_gitlab_active_user` 的会话 cookie。营销网站使用此 cookie 来确定用户是否具有活动的极狐GitLab 会话。没有用户信息被传递给此 cookie，它会随着会话而过期。
- 一个名为 `remember_user_token` 的持久 cookie，仅当您在登录页面上选择了 **记住我** 时才会设置。

当您关闭浏览器时，`_gitlab_session` 和 `about_gitlab_active_user` cookie 通常会在客户端被清除。
当它过期或不可用时，极狐GitLab：

- 使用 `remember_user_token` cookie 为您获取新的 `_gitlab_session` cookie 并让您保持登录状态，即使您关闭浏览器也是如此。
- 将 `about_gitlab_active_user` 设置为 `true`。

当 `remember_user_token` 和 `_gitlab_session` 两个 cookie 都消失或过期时，您必须重新登录。

NOTE:
当任何会话被注销，或者当一个会话通过 [Active Sessions](active_sessions.md) 被撤销时，所有 **记住我** 令牌都会被撤销。当其他会话保持活动状态时，如果浏览器关闭或现有会话过期，**记住我** 功能不会恢复会话。

<!--
## Related topics

- [Create users](account/create_accounts.md)
- [Sign in to your GitLab account](../../topics/authentication/index.md)
- [Receive emails for sign-ins from unknown IP addresses or devices](unknown_sign_in_notification.md)
- Manage applications that can [use GitLab as an OAuth provider](../../integration/oauth_provider.md#introduction-to-oauth)
- Manage [personal access tokens](personal_access_tokens.md) to access your account via API and authorized applications
- Manage [SSH keys](../../ssh/index.md) to access your account via SSH
- Change your [syntax highlighting theme](preferences.md#syntax-highlighting-theme)
- [View your active sessions](active_sessions.md) and revoke any of them if necessary
-->
