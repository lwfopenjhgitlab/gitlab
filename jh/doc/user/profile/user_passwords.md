---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 用户密码 **(FREE)**

如果您使用密码登录极狐GitLab，则强密码非常重要。弱密码或可猜测的密码使未经授权的人更容易登录您的帐户。

某些组织要求您在选择密码时满足某些要求。

使用[双重身份验证](account/two_factor_authentication.md)提高您帐户的安全性。

## 选择您的密码

您可以在[创建用户帐户](account/create_accounts.md)时选择密码。

如果您使用外部身份验证和授权提供商注册您的帐户，则无需选择密码。极狐GitLab 为您设置一个随机、唯一且安全的密码。

## 更改您的密码

您可以更改密码。当您选择新密码时，极狐GitLab 会强制执行[密码要求](#password-requirements)。

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏中，选择 **密码**。
1. 在 **当前密码** 文本框中，输入您的当前密码。
1. 在 **新密码** 和 **密码确认** 文本框中，输入您的新密码。
1. 选择 **保存密码**。

如果您不知道当前密码，请选择 **我忘记了密码** 链接。密码重置电子邮件将发送到帐户的**主要**电子邮件地址。

## 密码要求

在以下情况下，您的密码必须满足一组要求：

- 您在注册时选择密码。
- 您使用忘记密码重置流程选择新密码。
- 您主动更改密码。
- 您在密码过期后更改密码。
- 管理员创建您的帐户。
- 管理员更新您的帐户。

默认情况下，极狐GitLab 强制执行以下密码要求：

- 最小和最大密码长度。 例如，请参阅 [SaaS 的设置](../jihulab_com/index.md#password-requirements)。
- 禁止[弱密码](#block-weak-passwords)。

私有化部署的安装实例可以配置以下额外的密码要求：

- [密码最小和最大长度限制](../../security/password_length_limits.md)。
- [密码复杂度要求](../admin_area/settings/sign_up_restrictions.md#password-complexity-requirements)。

<a id="block-weak-passwords"></a>

## 阻止弱密码

> 引入于 15.4 版本，功能标志为 `block_weak_passwords`。默认禁用。

<!--
FLAG:
On self-managed GitLab, by default blocking weak passwords is not available. To make it available, ask an administrator
to [enable the feature flag](../../administration/feature_flags.md) named `block_weak_passwords`. On GitLab.com, this
feature is available but can be configured by GitLab.com administrators only.
-->

不允许使用弱密码。您的密码在以下情况下被视为弱密码：

- 匹配 4500 多个已知的已泄露密码之一。
- 包含您的姓名、用户名或电子邮件地址的一部分。
- 包含一个可预测的词（例如，`gitlab` 或 `devops`）。

弱密码被拒绝并显示错误消息：**密码不得包含常用的单词和字母组合**。
