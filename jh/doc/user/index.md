---
stage: 
group: 
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用极狐GitLab **(FREE)**

<!--
Get to know the GitLab end-to-end workflow. Configure permissions,
organize your work, create and secure your application, and analyze its performance. Report on team productivity throughout the process.
-->

了解极狐GitLab 端到端工作流程。配置权限、组织您的工作、创建和保护您的应用程序，并分析其性能。在整个过程中报告团队效能。

- [设置您的组织](../topics/set_up_organization.md)
- [用项目组织工作](../user/project/index.md)
- [计划并跟踪工作](../topics/plan_and_track.md)
- [构建您的应用](../topics/build_your_application.md)
- [保护您的应用](../user/application_security/index.md)
- [部署并发布您的应用](../topics/release_your_application.md)
- [监控应用性能](../operations/index.md)
- [分析极狐GitLab 使用](../user/analytics/index.md)
