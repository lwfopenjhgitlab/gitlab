---
stage: Manage
group: Authentication and Authorization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 权限和角色 **(FREE)**

用户具有不同的能力，具体取决于他们在特定群组或项目中的角色。如果用户同时在项目所在的群组和项目本身中，则使用最高角色。

在[公开和内部项目](../api/projects.md#project-visibility-level)上，不强制执行访客角色（不要与[访客用户](#free-guest-users)混淆）。

当成员离开团队的项目时，所有已分配的[议题](project/issues/index.md)和[合并请求](project/merge_requests/index.md)都会自动取消指派。

极狐GitLab [管理员](../administration/index.md)获得所有权限。

## 角色

可用的角色包括：

- 访客（此角色仅适用于[私有和内部项目](../user/public_access.md)）
- 报告者
- 开发者
- 维护者
- 所有者
- 最小权限（仅适用于顶级群组）

分配了访客角色的用户拥有最少的权限，而所有者拥有最多的权限。

默认情况下，所有用户都可以创建顶级群组并更改其用户名。极狐GitLab 管理员可以为极狐GitLab 实例[更改此设置](../administration/user_settings.md)。

<a id="project-members-permissions"></a>

## 项目成员权限

- 引入于 14.8 版本，个人命名空间所有者在其命名空间中的新项目中显示为所有者角色。[功能标志](../administration/feature_flags.md)为 `personal_project_owner_with_owner_access`。默认禁用。一般可用于 14.9 版本。功能标志 `personal_project_owner_with_owner_access` 删除。

用户的角色决定了他们对项目拥有的权限。所有者角色提供所有权限，但仅适用于：

- 群组所有者。该角色是被群组的项目继承的。
- 管理员。

对于个人[命名空间](group/index.md#namespaces)所有者：

- 在命名空间中的项目上显示为具有维护者角色，但具有与所有者角色相同的权限。
- 在 14.9 及更高版本，对于命名空间中的新项目，显示为具有所有者角色。

有关如何管理项目成员的更多信息，请参阅[项目成员](project/members/index.md)。

下表列出了每个角色可用的项目权限：

<!-- Keep this table sorted: By topic first, then by minimum role, then alphabetically. -->

| 操作                                                                                                                                                                               | 访客    | 报告者 | 开发者 | 维护者 | 所有者    |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|----------|-----------|------------|----------|
| [分析](analytics/index.md)：<br>查看[议题分析](analytics/issue_analytics.md)                                                                                             | ✓        | ✓        | ✓         | ✓          | ✓        |
| [分析](analytics/index.md)：<br>查看[合并请求分析](analytics/merge_request_analytics.md)                                                                             | ✓        | ✓        | ✓         | ✓          | ✓        |
| [分析](analytics/index.md)：<br>查看[价值流分析](group/value_stream_analytics/index.md)                                                                               | ✓        | ✓        | ✓         | ✓          | ✓        |
| [分析](analytics/index.md)：<br>查看 [DORA 指标](analytics/ci_cd_analytics.md)                                                                                                |          | ✓        | ✓         | ✓          | ✓        |
| [分析](analytics/index.md)：<br>查看 [CI/CD 分析](analytics/ci_cd_analytics.md)                                                                                             |          | ✓        | ✓         | ✓          | ✓        |
| [分析](analytics/index.md)：<br>查看[代码评审分析](analytics/code_review_analytics.md)                                                                                 |          | ✓        | ✓         | ✓          | ✓        |
| [分析](analytics/index.md)：<br>查看[仓库分析](analytics/repository_analytics.md)                                                                                   |          | ✓        | ✓         | ✓          | ✓        |
| [应用安全](application_security/index.md)：<br>查看依赖项列表中的许可证                                          |   |        | ✓         | ✓          | ✓        |
| [应用安全](application_security/index.md)：<br>创建并运行按需 DAST 扫描                                  |          |          | ✓         | ✓          | ✓        |
| [应用安全](application_security/index.md)：<br>管理[安全策略](application_security/policies/index.md)                                                           |          |          | ✓         | ✓          | ✓        |
| [应用安全](application_security/index.md)：<br>查看依赖项列表                                                     |          |          | ✓         | ✓          | ✓        |
| [应用安全](application_security/index.md)：<br>创建或指派[安全策略项目](application_security/policies/index.md)   |          |          |           |            | ✓        |
| [集群](infrastructure/clusters/index.md)：<br>查看集群                                                                                                                       |          |          | ✓         | ✓          | ✓        |
| [集群](infrastructure/clusters/index.md)：<br>管理集群                                                                                                                    |          |          |           | ✓          | ✓        |
| [容器镜像库](packages/container_registry/index.md)：<br>创建、编辑、删除[清理策略](packages/container_registry/index.md#delete-images-by-using-a-cleanup-policy) |          |          |          | ✓          | ✓        |
| [容器镜像库](packages/container_registry/index.md)：<br>将镜像推送到容器镜像库                                                                               |          |          | ✓         | ✓          | ✓        |
| [容器镜像库](packages/container_registry/index.md)：<br>从容器镜像库拉取镜像                                                                             | ✓ (*20*) | ✓ (*20*) | ✓         | ✓          | ✓        |
| [容器镜像库](packages/container_registry/index.md)：<br>删除容器镜像库镜像                                                                                     |          |          | ✓         | ✓          | ✓        |
| [极狐GitLab Pages](project/pages/index.md)：<br>查看受[访问控制](project/pages/introduction.md#gitlab-pages-access-control)保护的 Pages                                       | ✓        | ✓        | ✓         | ✓          | ✓        |
| [极狐GitLab Pages](project/pages/index.md)：<br>管理                                                                                                                                    |          |          |           | ✓          | ✓        |
| [极狐GitLab Pages](project/pages/index.md)：<br>管理极狐GitLab Pages 域名和证书                                                                                              |          |          |           | ✓          | ✓        |
| [极狐GitLab Pages](project/pages/index.md)：<br>删除极狐GitLab Pages                                                                                                                       |          |          |           | ✓          | ✓        |
| [事件管理](../operations/incident_management/index.md)：<br>查看[警报](../operations/incident_management/alerts.md)                                                     |          | ✓        | ✓         | ✓          | ✓        |
| [事件管理](../operations/incident_management/index.md)：<br>指派警报                                                                                                | ✓        | ✓        | ✓         | ✓          | ✓        |
| [事件管理](../operations/incident_management/index.md)：<br>查看[事件](../operations/incident_management/incidents.md)                                                | ✓        | ✓        | ✓         | ✓          | ✓        |
| [事件管理](../operations/incident_management/index.md)：<br>创建[事件](../operations/incident_management/incidents.md)                                              | (*16*)   | ✓        | ✓         | ✓          | ✓        |
| [事件管理](../operations/incident_management/index.md)：<br>查看 [on-call 计划](../operations/incident_management/oncall_schedules.md)                                |          | ✓        | ✓         | ✓          | ✓        |
| [事件管理](../operations/incident_management/index.md)：<br>参加 on-call 轮换                                                                                | ✓        | ✓        | ✓         | ✓          | ✓        |
| [事件管理](../operations/incident_management/index.md)：<br>查看[升级策略](../operations/incident_management/escalation_policies.md)                           |          | ✓        | ✓         | ✓          | ✓        |
| [事件管理](../operations/incident_management/index.md)：<br>管理 [on-call 计划](../operations/incident_management/oncall_schedules.md)                              |          |          |           | ✓          | ✓        |
| [事件管理](../operations/incident_management/index.md)：<br>管理[升级策略](../operations/incident_management/escalation_policies.md)                         |          |          |           | ✓          | ✓        |
| [议题看板](project/issue_board.md):<br>创建或删除列表                                                                                                                    |          | ✓        | ✓         | ✓          | ✓        |
| [议题看板](project/issue_board.md):<br>在列表之间移动议题                                                                                                                 |          | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>添加标记                                                                                                                                     | ✓ (*15*) | ✓        | ✓         | ✓          | ✓        |
| [Issues](project/issues/index.md):<br>添加到史诗                                                                                                                                    |           | ✓ (*23*) | ✓ (*23*)  | ✓ (*23*)   | ✓ (*23*) |
| [议题](project/issues/index.md)：<br>指派                                                                                                                                         | ✓ (*15*) | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>创建 (*18*)                                                                                                                                  | ✓        | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>创建[私密议题](project/issues/confidential_issues.md)                                                                            | ✓        | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>查看[设计管理](project/issues/design_management.md)页面                                                                           | ✓        | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>查看[关联议题](project/issues/related_issues.md)                                                                                        | ✓        | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>设置[权重](project/issues/issue_weight.md)                                                                                                   | ✓ (*15*) | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>创建议题时设置标记、里程碑或指派人等元数据                                                             | ✓ (*15*)  | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>编辑元数据，例如现有议题的标记、里程碑或指派人                                                                |   (*15*)  | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>查看[私密议题](project/issues/confidential_issues.md)                                                                              | (*2*)    | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>关闭/重开 (*19*)                                                                                                                          |          | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>锁定主题                                                                                                                              |          | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>管理[关联议题](project/issues/related_issues.md)                                                                                      |          | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>管理跟踪器                                                                                                                                 |          | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>移动议题 (*14*)                                                                                                                             |          | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>设置议题的[时间跟踪](project/time_tracking.md)估计和时间花费                                                                    |          | ✓        | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>存档[设计管理](project/issues/design_management.md)文件                                                                         |          |          | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>上传[设计管理](project/issues/design_management.md)文件                                                                          |          |          | ✓         | ✓          | ✓        |
| [议题](project/issues/index.md)：<br>删除                                                                                                                                         |          |          |           |            | ✓        |
| [许可证合规](compliance/license_compliance/index.md)：<br>查看允许的和拒绝的许可证                                                                                    | ✓ (*1*)  | ✓        | ✓         | ✓          | ✓        |
| [许可证合规](compliance/license_compliance/index.md)：<br>查看许可证合规报告                                                                                     | ✓ (*1*)  | ✓        | ✓         | ✓          | ✓        |
| [许可证合规](compliance/license_compliance/index.md)：<br>查看许可证列表                                                                                                   |          | ✓        | ✓         | ✓          | ✓        |
| [许可证合规](compliance/license_compliance/index.md)：<br>管理许可证策略                                                                                               |          |          |           | ✓          | ✓        |
| [合并请求](project/merge_requests/index.md)：<br>指派审核人                                                                                                                |          | ✓        | ✓         | ✓          | ✓        |
| [合并请求](project/merge_requests/index.md)：<br>查看列表                                                                                                                       |          | ✓        | ✓         | ✓          | ✓        |
| [合并请求](project/merge_requests/index.md)：<br>应用代码变更建议                                                                                                  |          |          | ✓         | ✓          | ✓        |
| [合并请求](project/merge_requests/index.md)：<br>核准 (*8*)                                                                                                                  |          |          | ✓         | ✓          | ✓        |
| [合并请求](project/merge_requests/index.md)：<br>指派                                                                                                                         |          |          | ✓         | ✓          | ✓        |
| [合并请求](project/merge_requests/index.md)：<br>创建 (*17*)                                                                                                                  |          |          | ✓         | ✓          | ✓        |
| [合并请求](project/merge_requests/index.md)：<br>添加标记                                                                                                                     |          |          | ✓         | ✓          | ✓        |
| [合并请求](project/merge_requests/index.md)：<br>锁定主题                                                                                                                   |          |          | ✓         | ✓          | ✓        |
| [合并请求](project/merge_requests/index.md)：<br>管理或接受                                                                                                               |          |          | ✓         | ✓          | ✓        |
| [合并请求](project/merge_requests/index.md)：<br>[解决主题](discussions/#resolve-a-thread)                                                                              |          |          | ✓         | ✓          | ✓        |
| [合并请求](project/merge_requests/index.md)：<br>管理[合并批准规则](project/merge_requests/approvals/settings.md)（项目设置）                                 |          |          |           | ✓          | ✓        |
| [合并请求](project/merge_requests/index.md)：<br>删除                                                                                                                         |          |          |           |            | ✓        |
| 指标看板：<br>管理用户加星标的指标仪表盘 (*6*)                                                                     | ✓        | ✓        | ✓         | ✓          | ✓        |
| 指标看板：<br>查看指标仪表盘注释                                                                               |          | ✓        | ✓         | ✓          | ✓        |
| 指标看板：<br>创建/编辑/删除指标仪表盘注释                                                                 |          |          | ✓         | ✓          | ✓        |
| [软件包库](packages/index.md)：<br>拉取软件包                                                                                                                             | ✓ (*1*)  | ✓        | ✓         | ✓          | ✓        |
| [软件包库](packages/index.md)：<br>发布软件包                                                                                                                          |          |          | ✓         | ✓          | ✓        |
| [软件包库](packages/index.md)：<br>删除软件包                                                                                                                           |          |          |           | ✓          | ✓        |
| [软件包库](packages/index.md)：<br>删除与软件包关联的文件                                                                                                    |          |          |           | ✓          | ✓        |
| [项目维护](../operations/index.md)：<br>查看[错误跟踪](../operations/error_tracking.md)列表                                                                         |          | ✓        | ✓         | ✓          | ✓        |
| [项目维护](../operations/index.md)：<br>管理[功能标志](../operations/feature_flags.md)                                                                              |          |          | ✓         | ✓          | ✓        |
| [项目维护](../operations/index.md)：<br>管理[错误跟踪](../operations/error_tracking.md)                                                                            |          |          |           | ✓          | ✓        |
| [项目](project/index.md)：<br>下载项目                                                                                                                                    | ✓ (*1*)  | ✓        | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>留下评论                                                                                                                                      | ✓        | ✓        | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>重新定位图像上的评论（由任何用户发布）                                                                                                  | ✓ (*9*)  | ✓ (*9*)  | ✓ (*9*)   | ✓          | ✓        |
| [项目](project/index.md)：<br>查看[洞察](project/insights/index.md)                                                                                                          | ✓        | ✓        | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>查看[发布](project/releases/index.md)                                                                                                          | ✓ (*5*)  | ✓        | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>查看[需求](project/requirements/index.md)                                                                                                  | ✓        | ✓        | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>查看[时间跟踪](project/time_tracking.md)报告                                                                                              | ✓ (*1*)  | ✓        | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>查看 [wiki](project/wiki/index.md) 页面                                                                                                            | ✓        | ✓        | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>创建[代码片段](snippets.md)                                                                                                                      |          | ✓        | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>管理标记                                                                                                                                       |          | ✓        | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>查看[项目流量统计](../api/project_statistics.md)                                                                                     |          | ✓        | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>创建、编辑、删除[里程碑](project/milestones/index.md)                                                                                   |          | ✓        | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>创建、编辑、删除[发布](project/releases/index.md)                                                                                          |          |          | ✓ (*12*)  | ✓ (*12*)   | ✓ (*12*) |
| [项目](project/index.md)：<br>创建、编辑 [wiki](project/wiki/index.md) 页面                                                                                                    |          |          | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>启用 [Review Apps](../ci/review_apps/index.md)                                                                                                    |          |          | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>查看项目[审计事件](../administration/audit_events.md)                                                                                      |          |          | ✓ (*10*)  | ✓          | ✓        |
| [项目](project/index.md)：<br>添加[部署密钥](project/deploy_keys/index.md)                                                                                                     |          |          |           | ✓          | ✓        |
| [项目](project/index.md)：<br>添加新的[团队成员](project/members/index.md)                                                                                                    |          |          |           | ✓          | ✓        |
| [项目](project/index.md)：<br>管理[团队成员](project/members/index.md)                                                                                                     |          |          |           | ✓ (*21*)   | ✓        |
| [项目](project/index.md)：<br>变更[项目功能可见性](public_access.md)级别                                                                                        |          |          |           | ✓ (*13*)   | ✓        |
| [项目](project/index.md)：<br>配置 [webhooks](project/integrations/webhooks.md)                                                                                              |          |          |           | ✓          | ✓        |
| [项目](project/index.md)：<br>删除 [wiki](project/wiki/index.md) 页面                                                                                                          |          |          | ✓         | ✓          | ✓        |
| [项目](project/index.md)：<br>编辑评论（由任何用户发布）                                                                                                                  |          |          |           | ✓          | ✓        |
| [项目](project/index.md)：<br>编辑项目徽章                                                                                                                                 |          |          |           | ✓          | ✓        |
| [项目](project/index.md)：<br>编辑项目设置                                                                                                                               |          |          |           | ✓          | ✓        |
| [项目](project/index.md)：<br>导出项目                                                                                                                                      |          |          |           | ✓          | ✓        |
| [项目](project/index.md)：<br>管理[项目访问令牌](project/settings/project_access_tokens.md) (*11*)                                                                    |          |          |           | ✓ (*21*)   | ✓        |
| [项目](project/index.md)：<br>管理[项目维护](../operations/index.md)                                                                                                 |          |          |           | ✓          | ✓        |
| [项目](project/index.md)：<br>重命名项目                                                                                                                                      |          |          |           | ✓          | ✓        |
| [项目](project/index.md)：<br>与群组共享（邀请）项目                                                                                                                 |          |          |           | ✓ (*7*)    | ✓ (*7*)  |
| [项目](project/index.md)：<br>查看成员的 2FA 状态                                                                                                                          |          |          |           | ✓          | ✓        |
| [项目](project/index.md)：<br>将项目分配给合规框架                                                         |          |          |           |            | ✓        |
| [项目](project/index.md)：<br>归档项目                                                                                                                                     |          |          |           |            | ✓        |
| [项目](project/index.md)：<br>变更项目可见性级别                                                                                                                     |          |          |           |            | ✓        |
| [项目](project/index.md)：<br>删除项目                                                                                                                                      |          |          |           |            | ✓        |
| [项目](project/index.md)：<br>禁用通知电子邮件                                                                                                                         |          |          |           |            | ✓        |
| [项目](project/index.md)：<br>将项目转移到另一个命名空间                                                                                                               |          |          |           |            | ✓        |
| [项目](project/index.md)：<br>查看[使用量配额](usage_quotas.md)页面                                                                                                              |          |          |           | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>拉取项目代码                                                                                                                      | ✓ (*1*)  | ✓        | ✓         | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>查看项目代码                                                                                                                      | ✓ (*1*)  | ✓        | ✓         | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>查看提交状态                                                                                                                   |          | ✓        | ✓         | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>添加标签                                                                                                                               |          |          | ✓         | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>创建新的分支                                                                                                                    |          |          | ✓         | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>创建或更新提交状态                                                                                                         |          |          | ✓ (*4*)   | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>强制推送到不受保护的分支                                                                                                   |          |          | ✓         | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>推送到不受保护的分支                                                                                                         |          |          | ✓         | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>删除不受保护的分支                                                                                                          |          |          | ✓         | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>重写或删除 Git 标签                                                                                                             |          |          | ✓         | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>启用或禁用分支保护                                                                                                    |          |          |           | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>启用或禁用标签保护                                                                                                       |          |          |           | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>管理[推送规则](project/repository/push_rules.md)                                                                                  |          |          |           | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>推送到受保护的分支 (*4*)                                                                                                       |          |          |           | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>为开发者打开或关闭受保护的分支推送                                                                                    |          |          |           | ✓          | ✓        |
| [仓库](project/repository/index.md)：<br>删除派生关系                                                                                                               |          |          |           |            | ✓        |
| [仓库](project/repository/index.md)：<br>强制推送到受保护的分支 (*3*)                                                                                                 |          |          |           |            |          |
| [仓库](project/repository/index.md)：<br>删除受保护的分支 (*3*)                                                                                                        |          |          |           |            |          |
| [需求管理](project/requirements/index.md)：<br>归档/重开                                                                                                        |          | ✓        | ✓         | ✓          | ✓        |
| [需求管理](project/requirements/index.md)：<br>创建/编辑                                                                                                           |          | ✓        | ✓         | ✓          | ✓        |
| [需求管理](project/requirements/index.md)：<br>导入/导出                                                                                                         |          | ✓        | ✓         | ✓          | ✓        |
| [安全看板](application_security/security_dashboard/index.md)：<br>从漏洞发现中创建议题                                                                   |          |          | ✓         | ✓          | ✓        |
| [安全看板](application_security/security_dashboard/index.md)：<br>从漏洞发现创建漏洞                                                          |          |          | ✓         | ✓          | ✓        |
| [安全看板](application_security/security_dashboard/index.md)：<br>忽略漏洞                                                                                     |          |          | ✓         | ✓          | ✓        |
| [安全看板](application_security/security_dashboard/index.md)：<br>忽略漏洞发现                                                                             |          |          | ✓         | ✓          | ✓        |
| [安全看板](application_security/security_dashboard/index.md)：<br>解决lou'dong                                                                                      |          |          | ✓         | ✓          | ✓        |
| [安全看板](application_security/security_dashboard/index.md)：<br>将漏洞恢复到已检测状态                                                                    |          |          | ✓         | ✓          | ✓        |
| [安全看板](application_security/security_dashboard/index.md)：<br>使用安全看板                                                                                    |          |          | ✓         | ✓          | ✓        |
| [安全看板](application_security/security_dashboard/index.md)：<br>查看漏洞看板                                                                                        |          |          | ✓         | ✓          | ✓        |
| [安全看板](application_security/security_dashboard/index.md)：<br>查看依赖列表中的漏洞发现           |          |          | ✓         | ✓          | ✓        |
| [Terraform](infrastructure/index.md)：<br>读取 Terraform state                                                                                                                        |          |          | ✓         | ✓          | ✓        |
| [Terraform](infrastructure/index.md)：<br>管理 Terraform state                                                                                                                      |          |          |           | ✓          | ✓        |
| 测试用例：<br>归档                                                                                                                                  |          | ✓        | ✓         | ✓          | ✓        |
| 测试用例：<br>创建                                                                                                                                   |          | ✓        | ✓         | ✓          | ✓        |
| 测试用例：<br>移动                                                                                                                                     |          | ✓        | ✓         | ✓          | ✓        |
| 测试用例：<br>重开                                                                                                                                   |          | ✓        | ✓         | ✓          | ✓        |

<!-- markdownlint-disable MD029 -->

1. 在私有化部署实例上，访客用户只能对公开和内部项目（不能对私有项目）执行此操作。[外部用户](#external-users)必须被明确授予访问权限，即使项目是内部的。对于 JihuLab.com，查看 [JihuLab.com 可见性设置](gitlab_com/index.md#visibility-settings)。
2. 访客用户只能查看他们自己创建或被指派的私密议题。
3. 不允许访客、报告者、开发者、维护者或所有者使用。请参阅[受保护的分支](project/protected_branches.md)。
4. 如果[分支是受保护的](project/protected_branches.md)，取决于给予开发者和维护者的访问权限。
5. 访客用户可以访问极狐GitLab [**发布**](project/releases/index.md)来下载 assets，但不能下载源代码，也不能查看[提交和发布 evidence 等仓库信息](project/releases/index.md#view-a-release-and-download-assets)。
6. 操作仅限于用户拥有（引用）的记录。
7. 当[共享群组锁定](group/index.md#prevent-a-project-from-being-shared-with-groups)启用时，项目不能与其他组共享。它不影响具有群组共享的群组。
8. 有关合并请求的合格审批者的信息，请参阅[合格审批者](project/merge_requests/approvals/rules.md#eligible-approvers)。
9. 仅适用于对[设计管理](project/issues/design_management.md)设计的评论。
10. 用户只能根据他们的个人操作查看事件。
11. 免费版及更高版本的私有化部署实例支持项目访问令牌。极狐GitLab SaaS 专业版及更高版本也支持它们（不包括[试用许可证](https://gitlab.cn/free-trial/)）。
12. 如果[标签是受保护的](#release-permissions-with-protected-tags)，取决于开发人员和维护人员的访问权限。
13. 如果[项目可见性](public_access.md)设置为私有，维护者无法更改项目功能可见性级别。
14. 即使用户没有开发者角色，附加的设计文件也会与议题一起移动。
15. 访客用户只能在创建议题时设置元数据（例如，标签、指派人或里程碑）。他们无法更改现有议题的元数据。
16. 在 14.5 或更高版本中，不允许访客[创建事件](../operations/incident_management/incidents.md#incident-creation)。在 15.1 及更高版本中，创建已提升为事件的议题的访客无法编辑、关闭或重新打开其事件。
17. 在接受外部成员贡献的项目中，用户可以创建、编辑和关闭自己的合并请求。
18. 议题的作者和指派人可以修改标题和描述，即使他们没有报告者角色。
19. 作者和指派人即使没有报告者角色也可以关闭和重新打开议题。
20. 查看容器镜像库和拉取镜像的能力由[容器镜像库的可见性权限](packages/container_registry/index.md#container-registry-visibility-permissions)控制。
21. 维护者不能创建、降级或删除所有者，也不能将用户提升为所有者角色。他们也无法批准所有者角色访问请求。
22. 任务的作者即使没有所有者角色也可以删除它们，但他们必须至少具有项目的访客角色。
23. 您必须有权[查看史诗](group/epics/manage_epics.md#who-can-view-an-epic)。

<!-- markdownlint-enable MD029 -->

## 项目功能权限

有关某些项目级功能的权限的更多详细信息如下。

### 极狐GitLab CI/CD 权限

[极狐GitLab CI/CD](../ci/index.md) 某些角色的权限可以通过以下设置进行修改：

- [公开流水线](../ci/pipelines/settings.md#change-which-users-can-view-your-pipelines)：设置为公开时，向*访客*项目成员提供对某些 CI/CD 功能的访问权限。
- [流水线可见性](../ci/pipelines/settings.md#change-pipeline-visibility-for-non-project-members-in-public-projects)：当设置为**具有访问权限的任何人**时，授予*非项目*成员某些 CI/CD “查看”功能的访问权限。

| 操作                                                                                                                    | 非成员 | 访客   | 报告者 | 开发者 | 维护者 | 所有者 |
|---------------------------------------------------------------------------------------------------------------------------|------------|---------|----------|-----------|------------|-------|
| 看到产物的存在                                                                                                  | ✓ (*3*)    | ✓ (*3*) | ✓        | ✓         | ✓          | ✓     |
| 查看作业列表                                                                                                       | ✓ (*1*)    | ✓ (*2*) | ✓        | ✓         | ✓          | ✓     |
| 查看并下载产物                                                                                               | ✓ (*1*)    | ✓ (*2*) | ✓        | ✓         | ✓          | ✓     |
| 查看[环境](../ci/environments/index.md)                                                                          | ✓ (*3*)    | ✓ (*3*) | ✓        | ✓         | ✓          | ✓     |
| 查看作业日志和作业详细信息页面                                                                                        | ✓ (*1*)    | ✓ (*2*) | ✓        | ✓         | ✓          | ✓     |
| 查看流水线详细信息页面                                                                                                | ✓ (*1*)    | ✓ (*2*) | ✓        | ✓         | ✓          | ✓     |
| 查看流水线页面                                                                                                      | ✓ (*1*)    | ✓ (*2*) | ✓        | ✓         | ✓          | ✓     |
| 查看合并请求的流水线选项卡                                                                                                  | ✓ (*3*)    | ✓ (*3*) | ✓        | ✓         | ✓          | ✓     |
| [查看流水线中的漏洞](application_security/vulnerability_report/pipeline.md#view-vulnerabilities-in-a-pipeline) |            | ✓ (*2*) | ✓        | ✓         | ✓          | ✓     |
| 查看并下载项目级安全文件                                                   |            |         |          | ✓         | ✓          | ✓     |
| 取消和重试作业                                                                                                     |            |         |          | ✓         | ✓          | ✓     |
| 创建新的[环境](../ci/environments/index.md)                                                                    |            |         |          | ✓         | ✓          | ✓     |
| 删除作业日志或作业产物                                                                                          |            |         |          | ✓ (*4*)   | ✓          | ✓     |
| 运行 CI/CD 流水线                                                                                                        |            |         |          | ✓         | ✓          | ✓     |
| 为受保护的分支运行 CI/CD 流水线                                                                                 |            |         |          | ✓ (*5*)   | ✓ (*5*)    | ✓     |
| 停止[环境](../ci/environments/index.md)                                                                          |            |         |          | ✓         | ✓          | ✓     |
| 使用 [debug 日志记录](../ci/variables/index.md#debug-logging)查看作业                                                   |            |         |          | ✓         | ✓          | ✓     |
| 使用流水线编辑器                                                                                                       |            |         |          | ✓         | ✓          | ✓     |
| 运行[交互式网络终端](../ci/interactive_web_terminal/index.md)                                                  |            |         |          | ✓         | ✓          | ✓     |
| 将特定 runner 添加到项目中                                                                                           |            |         |          |           | ✓          | ✓     |
| 手动清除 runner 缓存                                                                                              |            |         |          |           | ✓          | ✓     |
| 在项目中启用共享 runners                                                                                         |            |         |          |           | ✓          | ✓     |
| 管理 CI/CD 设置                                                                                                     |            |         |          |           | ✓          | ✓     |
| 管理作业触发器                                                                                                       |            |         |          |           | ✓          | ✓     |
| 管理项目级 CI/CD 变量                                                                                      |            |         |          |           | ✓          | ✓     |
| 管理项目级安全文件                                                               |            |         |          |           | ✓          | ✓     |
| 使用[环境终端](../ci/environments/index.md#web-terminals-deprecated)                                         |            |         |          |           | ✓          | ✓     |
| 删除流水线                                                                                                          |            |         |          |           |            | ✓     |

<!-- markdownlint-disable MD029 -->

1. 如果项目是公开的并且在 **项目设置 > CI/CD** 中启用了 **公开流水线**。
2. 如果 **公开流水线** 在 **项目设置 > CI/CD** 中已启用。
3. 如果项目是公开的。
4. 仅当作业满足以下两者：
   - 由用户触发。
   - 在 13.0 及更高版本，为不受保护的分支运行。
5. 如果用户[允许合并或推送到受保护的分支](../ci/pipelines/index.md#pipeline-security-on-protected-branches)。

<!-- markdownlint-enable MD029 -->

#### 作业权限

此表显示由特定类型的用户触发的作业的授予权限：

| 操作                                       | 访客、报告者 | 开发者 | 维护者 | 管理员 |
|----------------------------------------------|-----------------|-----------|------------|---------------|
| 运行 CI 作业                                   |                 | ✓         | ✓          | ✓             |
| 从当前项目克隆源和 LFS    |                 | ✓         | ✓          | ✓             |
| 从公开项目中克隆源和 LFS    |                 | ✓         | ✓          | ✓             |
| 从内部项目中克隆源和 LFS  |                 | ✓ (*1*)   | ✓  (*1*)   | ✓             |
| 从私有项目中克隆源和 LFS   |                 | ✓ (*2*)   | ✓  (*2*)   | ✓ (*2*)       |
| 从当前项目中拉取容器镜像   |                 | ✓         | ✓          | ✓             |
| 从公开项目中拉取容器镜像   |                 | ✓         | ✓          | ✓             |
| 从内部项目中拉取容器镜像 |                 | ✓ (*1*)   | ✓  (*1*)   | ✓             |
| 从私有项目中拉取容器镜像  |                 | ✓ (*2*)   | ✓  (*2*)   | ✓ (*2*)       |
| 将容器镜像推送到当前项目     |                 | ✓         | ✓          | ✓             |
| 将容器镜像推送到其他项目      |                 |           |            |               |
| 推送源和 LFS                          |                 |           |            |               |

1. 仅当触发用户不是外部用户时。
1. 仅当触发用户是项目成员时。

### Wiki 和议题

根据您在项目设置中选择的可见性级别，可以对用户隐藏诸如 [wikis](project/wiki/index.md) 和议题之类的项目功能。

- 禁用：对所有人禁用
- 只有团队成员：即使您的项目是公开的或内部的，也只有团队成员可以看到
- 每个人都有访问权限：每个人都可以看到，具体取决于您项目的可见性级别
- 所有人：为所有人启用（仅适用于极狐GitLab Pages）

### 受保护的分支

可以使用[受保护的分支](project/protected_branches.md)，在每个分支的基础上应用额外的限制。
此外，您可以自定义权限，允许或阻止项目维护人员和开发人员推送到受保护的分支。通读[受保护的分支](project/protected_branches.md)文档以了解更多信息。

### 价值流分析权限

在价值流分析仪表盘上查找当前权限，如[相关文档](analytics/value_stream_analytics.md#access-permissions-for-value-stream-analytics)中所述。

### 文件锁定权限 **(PREMIUM)**

锁定文件或目录的用户是唯一可以编辑并将其更改推送回锁定对象所在的仓库的用户。

阅读有关[文件锁定权限](project/file_lock.md#permissions)的文档以了解更多信息。

### 私密议题权限

[私密议题](project/issues/confidential_issues.md)可以由具有报告者和更高权限级别的用户，以及创建私密议题或被指派给它的来宾用户访问。要了解更多信息，请通读有关[权限和访问私密议题](project/issues/confidential_issues.md#permissions-and-access-to-confidential-issues)的文档。

### 容器镜像库可见性权限

查看容器镜像库，和拉取镜像的能力由容器镜像库的可见性权限控制。按照[相关文档](packages/container_registry/index.md#container-registry-visibility-permissions)中的说明查找容器镜像库的这些权限。

## 群组成员权限

任何用户都可以将自己从群组中删除，除非他们是群组的最后一个所有者。

下表列出了每个角色可用的群组权限：

<!-- Keep this table sorted: first, by minimum role, then alphabetically. -->

| 操作                                                                                  | 访客 | 报告者 | 开发者 | 维护者 | 所有者 |
|-----------------------------------------------------------------------------------------|-------|----------|-----------|------------|-------|
| 添加议题到[史诗](group/epics/index.md)                                         | ✓ (7) | ✓ (7)    | ✓ (7)     | ✓ (7)      | ✓ (7) |
| 浏览群组                                                                            | ✓     | ✓        | ✓         | ✓          | ✓     |
| 使用依赖代理拉取容器镜像                                       | ✓     | ✓        | ✓         | ✓          | ✓     |
| 查看贡献分析                                                             | ✓     | ✓        | ✓         | ✓          | ✓     |
| 查看群组[史诗](group/epics/index.md)                                                 | ✓     | ✓        | ✓         | ✓          | ✓     |
| 查看[群组 wiki](project/wiki/group.md) 页面                                          | ✓ (5) | ✓        | ✓         | ✓          | ✓     |
| 查看[洞察](project/insights/index.md)                                              | ✓     | ✓        | ✓         | ✓          | ✓     |
| 查看[洞察](project/insights/index.md)图表                                       | ✓     | ✓        | ✓         | ✓          | ✓     |
| 查看[议题分析](analytics/issue_analytics.md)                                    | ✓     | ✓        | ✓         | ✓          | ✓     |
| 查看价值流分析                                                             | ✓     | ✓        | ✓         | ✓          | ✓     |
| 创建/编辑群组[史诗](group/epics/index.md)                                          |       | ✓        | ✓         | ✓          | ✓     |
| 创建/编辑/删除[史诗看板](group/epics/epic_boards.md)                            |       | ✓        | ✓         | ✓          | ✓     |
| 管理群组标记                                                                    |       | ✓        | ✓         | ✓          | ✓     |
| 发布[软件包](packages/index.md)                                                   |       |          | ✓         | ✓          | ✓     |
| 拉取[软件包](packages/index.md)                                                      |       | ✓        | ✓         | ✓          | ✓     |
| 删除[软件包](packages/index.md)                                                    |       |          |           | ✓          | ✓     |
| 创建/编辑/删除 [Maven 和通用包重复设置](packages/generic_packages/index.md#do-not-allow-duplicate-generic-packages)                                                    |       |          |           | ✓          | ✓     |
| 拉取容器镜像库镜像                                                         | ✓ (6) | ✓        | ✓         | ✓          | ✓     |
| 删除容器镜像库镜像                                                       |       |          | ✓         | ✓          | ✓     |
| 查看[群组 DevOps 采用](group/devops_adoption/index.md)                            |       | ✓        | ✓         | ✓          | ✓     |
| 查看指标看板注释                           |       | ✓        | ✓         | ✓          | ✓     |
| 查看[效率分析](analytics/productivity_analytics.md)                      |       | ✓        | ✓         | ✓          | ✓     |
| 创建和编辑[群组 wiki](project/wiki/group.md) 页面                             |       |          | ✓         | ✓          | ✓     |
| 在群组中创建项目                                                                 |       |          | ✓ (2)(4)  | ✓ (2)      | ✓ (2) |
| 创建/编辑/删除群组里程碑                                                     |       | ✓        | ✓         | ✓          | ✓     |
| 创建/编辑/删除迭代                                                           |       | ✓        | ✓         | ✓          | ✓     |
| 创建/编辑/删除指标看板注释                                        |       |          | ✓         | ✓          | ✓     |
| 启用/禁用依赖代理                                                       |       |          |           | ✓          | ✓     |
| 清除群组的依赖代理                                                  |       |          |           |            | ✓     |
| 创建/编辑/删除依赖代理[清理策略](packages/dependency_proxy/reduce_dependency_proxy_storage.md#cleanup-policies)                                                  |       |          |           | ✓          | ✓     |
| 使用[安全看板](application_security/security_dashboard/index.md)              |       |          | ✓         | ✓          | ✓     |
| 查看群组审计事件                                                                 |       |          | ✓ (6)     | ✓ (6)      | ✓     |
| 创建子组                                                                         |       |          |           | ✓ (1)      | ✓     |
| 删除[群组 wiki](project/wiki/group.md) 页面                                        |       |          | ✓         | ✓          | ✓     |
| 编辑[史诗](group/epics/index.md)评论（由任何用户发布）                        |       |          |           | ✓       | ✓  |
| 列出群组部署令牌                                                                |       |          |           | ✓          | ✓     |
| 管理[群组推送规则](group/index.md#group-push-rules)                              |       |          |           | ✓          | ✓     |
| 查看/管理群组级 Kubernetes 集群                                              |       |          |           | ✓          | ✓     |
| 创建和管理合规框架                                                 |       |          |           |            | ✓     |
| 创建/删除群组部署令牌                                                       |       |          |           |            | ✓     |
| 更改群组可见性级别                                                           |       |          |           |            | ✓     |
| 删除群组                                                                            |       |          |           |            | ✓     |
| 删除群组[史诗](group/epics/index.md)                                               |       |          |           |            | ✓     |
| 禁用通知电子邮件                                                             |       |          |           |            | ✓     |
| 编辑群组设置                                                                     |       |          |           |            | ✓     |
| 编辑 SAML SSO                                                |       |          |           |            | ✓ (3) |
| 按 2FA 状态过滤成员                                                            |       |          |           |            | ✓     |
| 管理群组级 CI/CD 变量                                                      |       |          |           |            | ✓     |
| 管理群组成员                                                                    |       |          |           |            | ✓     |
| 与群组共享（邀请）群组                                                       |       |          |           |            | ✓     |
| 查看成员的 2FA 状态                                                              |       |          |           |            | ✓     |
| 查看群组[使用量配额](usage_quotas.md)页面                                         |       |          |           |            | ✓ (3) |
| 管理群组 runners                                                                    |       |          |           |            | ✓     |
| [迁移群组](group/import/index.md)                                                 |       |          |           |            | ✓     |
| 管理订阅以及购买 CI/CD 分钟数和存储         |       |          |           |            | ✓     |

<!-- markdownlint-disable MD029 -->

1. 可以将群组设置为，允许所有者或具有维护者角色的所有者和用户[创建子组](group/subgroups/index.md#create-a-subgroup)。
2. 可以在以下位置更改默认项目创建角色：
   - 实例级别<!--[实例级别](admin_area/settings/visibility_and_access_controls.md#define-which-roles-can-create-projects)-->。
   - [群组级别](group/index.md#specify-who-can-add-projects-to-a-group)。
3. 不适用于子组。
4. 仅当[默认分支保护](group/index.md#change-the-default-branch-protection-of-a-group)设置为“部分保护”或“不受保护”时，开发人员才能将提交推送到新项目的默认分支。
5. 此外，如果您的群组是公开的或内部的，则所有可以查看该群组的用户也可以查看群组 Wiki 页面。
6. 用户只能根据他们的个人操作查看事件。
7. 您必须有权[查看史诗](group/epics/manage_epics.md#who-can-view-an-epic)并编辑议题。

<!-- markdownlint-enable MD029 -->

### 子组权限

当您将成员添加到子组时，他们会从父组继承成员资格和权限级别。如果您在其父组中拥有成员资格，则此模型允许访问嵌套组。

要了解更多信息，请阅读有关[子组成员资格](group/subgroups/index.md#subgroup-membership)的文档。

<a id="external-users"></a>

## 外部用户 **(FREE SELF)**

如果希望用户只能访问某些内部或私人项目，则可以选择创建**外部用户**。例如，当承包商正在处理特定项目并且应该只能访问该项目时，此功能可能很有用。

外部用户：

- 无法在其个人命名空间中创建项目、群组和代码片段。
- 只能在其所属的顶级群组内创建项目（包括派生项目）、子组和代码片段。
- 只能访问他们被明确授予访问权限的项目和公开项目，从而对他们隐藏所有其他内部或私有项目（看起来像被注销）。
- 只能访问他们被明确授予访问权限的群组和公开群组，从而对他们隐藏所有其他内部或私有群组（看起来像被注销）。
- 只能访问公开代码片段。

可以通过将用户添加为项目或群组的成员来授予访问权限。
与普通用户一样，他们在项目或群组中获得一个角色，具有[上面的权限表](#project-members-permissions)中提到的所有能力。
例如，如果将外部用户添加为访客，并且您的项目是内部或私有的，则他们无权访问代码；如果您希望外部用户有权访问代码，则需要授予外部用户报告者或更高级别的访问权限。您应该始终考虑[项目的可见性和权限设置](project/settings/index.md#configure-project-visibility-features-and-permissions)以及用户的权限级别。

NOTE:
外部用户仍计入许可证席位。

管理员可以通过以下任一方法将用户标记为外部用户：

- 通过 API<!--[通过 API](../api/users.md#user-modification)-->。
- 使用 UI：
  1. 在顶部栏上，选择 **主菜单 > 管理员**。
  1. 在左侧边栏上，选择 **概览 > 用户** 创建新用户或编辑现有用户。在那里，您可以找到将用户标记为外部用户的选项。

此外，可以使用以下方法将用户设置为外部用户：

- SAML 群组<!--[SAML 群组](../integration/saml.md#external-groups)-->。
- [LDAP 群组](../administration/auth/ldap/ldap_synchronization.md#external-groups)。

### 将新用户设置为外部用户

默认情况下，新用户不设置为外部用户。管理员可以更改：

1. 在顶部栏上，选择 **主菜单 > 管理员**。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 展开 **帐户和限制** 部分。

如果您更改将新用户创建为外部用户的默认行为，您可以选择通过定义一组内部用户来缩小范围。
**内部用户**字段允许指定电子邮件地址正则表达式来识别默认内部用户。电子邮件地址与正则表达式匹配的新用户默认设置为内部而不是外部协作者。

正则表达式模式格式在 Ruby 中，但它需要可转换为 JavaScript，并且设置了忽略大小写标志 (`/regex pattern/i`)。示例：

- 使用 `\.internal@domain\.com$` 将以 `.internal@domain.com` 结尾的电子邮件地址标记为内部地址。
- 使用 `^(?:(?!\.ext@domain\.com).)*$\r?` 将电子邮件地址不包括 `.ext@domain.com` 的用户标记为内部用户。

WARNING:
请注意，此正则表达式可能会导致正则表达式拒绝服务 (ReDoS) 攻击。

<a id="free-guest-users"></a>

## 免费访客用户 **(ULTIMATE)**

当用户在项目、群组或两者中被赋予访客角色，并且在极狐GitLab 实例上的任何其他项目或群组上没有更高的权限级别时，该用户被极狐GitLab 视为访客用户并且不消耗许可证席位。
新创建的用户没有特定的“访客”角色。

如果用户在任何项目或群组中被分配了更高的角色，则用户将占用许可证席位。如果用户创建项目，则用户将成为项目的维护者，从而占用许可证席位。此外，请注意，如果您的项目是内部项目或私有项目，访客用户具有[权限表](#project-members-permissions)中提到的所有能力（例如，他们无法浏览项目的仓库）。

NOTE:
要防止访客用户创建项目，作为管理员，您可以编辑用户的配置文件，将用户标记为[外部](#external-users)。请注意，即使用户是外部用户，如果他们在任何项目或群组中已经拥有报告或更高权限，则他们**不**算作免费访客用户。

## 审计员用户 **(PREMIUM SELF)**

审计员用户被授予对极狐GitLab 实例上所有项目、群组和其他资源的只读访问权限。

审计员用户应该能够使用[审计员用户权限](../administration/auditor_users.md#auditor-user-permissions-and-restrictions)文档中描述的权限，访问极狐GitLab 实例的所有项目和群组。

[阅读有关审计员用户的更多信息。](../administration/auditor_users.md)

## 最小访问权限用户 **(PREMIUM)**

> - 引入于 13.4 版本。
> - 支持邀请具有最小访问权限的用户于 15.9 版本。

具有最小访问权限角色的用户不能：

- 被计为私有化部署实例的旗舰版订阅或任何 SaaS 订阅的许可席位。
- 自动访问该根群组中的项目和子组。

所有者必须明确地将这些用户添加到特定的子组和项目中。

您可以使用最小访问权限角色为同一个成员赋予群组中的多个角色：

1. 将成员添加到根群组，赋予最小访问权限角色。
1. 邀请该成员作为在该群组的任何子组或项目中具有特定角色的直接成员。

由于已知问题，当具有最小访问权限角色的用户：

- 使用标准 Web 身份验证登录，他们在访问父组时收到 `404` 错误。
- 使用群组 SSO 登录，他们立即收到 `404` 错误，他们被重定向到父组页面。

要解决此问题，请为这些用户授予父组内任何项目或子组的访客角色或更高级别。

<a id="custom-roles"></a>

## 自定义角色 **(ULTIMATE)**

> - 引入于 15.7 版本，[功能标志](../administration/feature_flags.md)为 `customizable_roles`。
> - 默认启用于 15.9 版本。

自定义角色允许具有所有者角色的群组成员，创建特定于其组织需求的角色。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For a demo of the custom roles feature, see [[Demo] Ultimate Guest can view code on private repositories via custom role](https://www.youtube.com/watch?v=46cp_-Rtxps).
-->

### 创建自定义角色

要为您的群组启用自定义角色，具有所有者角色的群组成员：

1. 确保该群组或其子组中至少有一个私有项目，这样您就可以看到为访客自定义角色的效果。
1. 创建具有 API 范围的个人访问令牌。
1. 使用 API<!--[API](../api/member_roles.md#add-a-member-role-to-a-group)--> 为群组创建 Guest+1 角色。

### 将自定义角色与现有群组成员相关联

要将自定义角色与现有群组成员关联，具有所有者角色的群组成员应执行以下操作：

1. 邀请测试用户帐户作为访客加入根群组。此时，访客用户看不到群组中项目的任何代码。
1. 可选。如果所有者不知道接收自定义角色的来宾用户的 `ID`，则通过发出 <!--[API 请求](../api/member_roles.md#list-all-member-roles-of-a-group)-->API 请求查找 ID。

1. 使用群组和项目成员 API 端点<!--[群组和项目成员 API 端点](../api/members.md#edit-a-member-of-a-group-or-project)-->，将群组成员与 Guest+1 角色相关联。

    ```shell
    curl --request PUT --header "Content-Type: application/json" --header "Authorization: Bearer $YOUR_ACCESS_TOKEN" --data '{"member_role_id": '$MEMBER_ROLE_ID', "access_level": 10}' "https://example.gitlab.com/api/v4/groups/$GROUP_PATH/members/$GUEST_USER_ID"
    ```

   - `$MEMBER_ROLE_ID`：上一节创建的成员角色的 `ID`。
   - `$GUEST_USER_ID`：接收自定义角色的访客用户的 `ID`。

   现在 Guest+1 用户可以查看根群组中所有项目的代码。

### 从群组成员中删除自定义角色

要从群组成员中删除自定义角色，请使用群组和项目成员 API 端点<!--[群组和项目成员 API 端点](../api/members.md#edit-a-member-of-a-group-or-project)-->，并传递一个空的 `member_role_id` 值。

```shell
curl --request PUT --header "Content-Type: application/json" --header "Authorization: Bearer $YOUR_ACCESS_TOKEN" --data '{"member_role_id": "", "access_level": 10}' "https://example.gitlab.com/api/v4/groups/$GROUP_PATH/members/$GUEST_USER_ID"
```

现在用户是普通访客。

### 删除自定义角色

删除自定义角色还会从群组中删除所有具有该自定义角色的成员。如果您决定删除自定义角色，则必须将具有该自定义角色的所有用户重新添加到群组中。

要从群组中删除自定义角色，具有所有者角色的群组成员：

1. 可选。如果所有者不知道接收自定义角色的来宾用户的 `ID`，则通过发出 <!--[API 请求](../api/member_roles.md#list-all-member-roles-of-a-group)-->API 请求查找 ID。
1. 使用 API<!--[API](../api/member_roles.md#remove-member-role-of-a-group)--> 删除自定义角色。

### 已知问题

- 附加权限只能应用于具有访客角色的用户。
- 访客角色和具有附加权限的访客角色在 UI 中没有视觉上的区别。
- 如果具有自定义角色的用户与群组或项目共享，则他们的自定义角色不会随之转移。用户在新的群组或项目中具有一般的访客角色。
- 如果删除自定义角色，则与该自定义角色关联的用户也会从群组中删除。
- 用于将自定义角色与用户相关联的 API 端点，仅适用于群组中具有访客角色的用户。项目成员可以与自定义角色相关联，但还不能通过 API。
- 从用户的群组成员资格中删除自定义角色的唯一方法是删除自定义角色，这将完全删除用户成员资格。

