---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
disqus_identifier: 'https://docs.gitlab.com/ee/workflow/shortcuts.html'
---

# 键盘快捷键 **(FREE)**

极狐GitLab 有一些键盘快捷键，您可以使用它们来访问其不同的功能。

要在极狐GitLab 中显示一个列出其键盘快捷键的窗口，请使用以下方法之一：

- 按 <kbd>?</kbd>。
- 在应用程序右上角的帮助菜单中，选择**键盘快捷键**。

<!--In [GitLab 12.8 and later](https://gitlab.com/gitlab-org/gitlab/-/issues/22113),-->
您可以使用键盘快捷键窗口顶部的 **键盘快捷键** 切换来禁用键盘快捷键。

尽管[全局快捷方式](#全局快捷方式)可以在任何区域工作，但您必须在特定页面中才能使用部分其它快捷方式，如以下每个部分所述。

## 全局快捷方式

以下快捷方式在大多数区域都可用：

| 键盘快捷键              | 描述 |
|---------------------------------|-------------|
| <kbd>?</kbd>                    | 显示或隐藏快捷方式参考表。 |
| <kbd>Shift</kbd> + <kbd>p</kbd> | 访问项目页面。 |
| <kbd>Shift</kbd> + <kbd>g</kbd> | 访问群组页面。 |
| <kbd>Shift</kbd> + <kbd>a</kbd> | 访问活动页面。 |
| <kbd>Shift</kbd> + <kbd>l</kbd> | 访问里程碑页面。 |
| <kbd>Shift</kbd> + <kbd>s</kbd> | 访问代码片段页面 |
| <kbd>s</kbd> / <kbd>/</kbd>     | 将光标放在搜索栏中。 |
| <kbd>f</kbd>                    | 将光标放在过滤器栏中。 |
| <kbd>Shift</kbd> + <kbd>i</kbd> | 访问议题页面。 |
| <kbd>Shift</kbd> + <kbd>m</kbd> | 访问合并请求页面。 |
| <kbd>Shift</kbd> + <kbd>r</kbd>    | 访问审核请求页面。 |
| <kbd>Shift</kbd> + <kbd>t</kbd> | 访问待办事项列表页面。 |
| <kbd>p</kbd> + <kbd>b</kbd>     | 显示或隐藏性能栏。 |
| <kbd>Escape</kbd>                  | 隐藏工具提示或弹出框。 |
| <kbd>.</kbd>     | 打开 Web IDE<!--[Web IDE](project/web_ide/index.md)-->。 |

<!--
| <kbd>g</kbd> + <kbd>x</kbd>     | 在当前版本和 [GitLab Next](https://next.gitlab.com/) (GitLab SaaS only). |
-->

此外，在文本字段（例如评论、回复、议题描述和合并请求描述）中编辑文本时，可以使用以下快捷方式：

| macOS 快捷键 | Windows 快捷键 | 描述 |
|----------------|------------------|-------------|
| <kbd>↑</kbd>   | <kbd>↑</kbd>     | 编辑您的最后一条评论。您必须位于主题下方的空白文本字段中，并且您必须在主题中至少有一条评论。 |
| <kbd>Command</kbd> + <kbd>Shift</kbd> + <kbd>p</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>p</kbd> | 在顶部具有 **编辑** 和 **预览** 选项卡的文本字段中编辑文本时切换 Markdown 预览。 |
| <kbd>Command</kbd> + <kbd>b</kbd>       | <kbd>Control</kbd> + <kbd>b</kbd> | 加粗所选文本（用 `**` 括起来）。 |
| <kbd>Command</kbd> + <kbd>i</kbd>       | <kbd>Control</kbd> + <kbd>i</kbd> | 所选文本使用斜体（用`_`括起来）。 |
| <kbd>Command</kbd> + <kbd>Shift</kbd> + <kbd>x</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>x</kbd> | 删除选定的文本（用 `~~` 括起来）。 |
| <kbd>Command</kbd> + <kbd>k</kbd>       | <kbd>Control</kbd> + <kbd>k</kbd> | 添加一个链接（用 `[]()` 包围所选文本）。 |
| <kbd>Command</kbd> + <kbd>&#93;</kbd> | <kbd>Control</kbd> + <kbd>&#93;</kbd> |  减少缩进列表项。引入于 15.3 版本。 |
| <kbd>Command</kbd> + <kbd>&#91;</kbd> | <kbd>Control</kbd> + <kbd>&#91;</kbd> |  缩进列表项。引入于 15.3 版本。 |

即使其他键盘快捷键被禁用，用于在文本字段中编辑的快捷键始终处于启用状态。

## 项目

这些快捷方式可从项目中的任何页面使用。您必须相对较快地键入它们才能工作，它们会将您带到项目中的另一个页面。

| 键盘快捷键           | 描述 |
|-----------------------------|-------------|
| <kbd>g</kbd> + <kbd>o</kbd> | 访问项目概览页。 <!--(**Project > Details**).--> |
| <kbd>g</kbd> + <kbd>v</kbd> | 访问项目活动订阅 (**项目信息 > 动态**)。 |
| <kbd>g</kbd> + <kbd>r</kbd> | 访问项目发布列表 (**项目 > 发布**)。 |
| <kbd>g</kbd> + <kbd>f</kbd> | 访问[项目文件](#项目文件)列表 (**仓库 > 文件**)。 |
| <kbd>t</kbd>                | 访问项目文件搜索页面 (**仓库 > 文件**，选择 **查找文件**)。 |
| <kbd>g</kbd> + <kbd>c</kbd> | 访问项目提交列表 (**仓库 > 提交**)。 |
| <kbd>g</kbd> + <kbd>n</kbd> | 访问[仓库分支图](#仓库分支图)页面 (**仓库 > 分支图**)。 |
| <kbd>g</kbd> + <kbd>d</kbd> | 访问仓库图标 (**分析 > 仓库**)。 |
| <kbd>g</kbd> + <kbd>i</kbd> | 访问项目议题列表 (**议题 > 列表**)。 |
| <kbd>i</kbd>                | 访问新建议题页面 (**议题**，选择 **新建议题**)。 |
| <kbd>g</kbd> + <kbd>b</kbd> | 访问项目议题看板列表 (**议题 > 看板**)。 |
| <kbd>g</kbd> + <kbd>m</kbd> | 访问项目合并请求列表 (**合并请求**)。 |
| <kbd>g</kbd> + <kbd>p</kbd> | 访问 CI/CD 流水线列表 (**CI/CD > 流水线**)。 |
| <kbd>g</kbd> + <kbd>j</kbd> | 访问 CI/CD 作业列表 (**CI/CD > 作业**)。 |
| <kbd>g</kbd> + <kbd>e</kbd> | 访问项目环境 (**部署 > 环境**)。 |
| <kbd>g</kbd> + <kbd>k</kbd> | 访问项目 Kubernetes 集群集成页面 (**基础设施 > Kubernetes 集群**)。请注意，您必须至少具有维护者权限<!--[维护者权限](permissions.md)-->才能访问此页面。 |
| <kbd>g</kbd> + <kbd>s</kbd> | 访问项目代码片段列表 (**代码片段**)。 |
| <kbd>g</kbd> + <kbd>w</kbd> | 访问项目 wiki (**Wiki**)，如果已启用。 |
| <kbd>.</kbd>                | 打开 [Web IDE](project/web_ide/index.md)。 |

<a id="issues"></a>

### 议题

查看议题和合并请求时可以使用以下快捷方式：

| 键盘快捷键             | 描述 |
|------------------------------|-------------|
| <kbd>e</kbd>                 | 编辑描述。 |
| <kbd>a</kbd>                 | 更改指派人。 |
| <kbd>m</kbd>                 | 更改里程碑。 |
| <kbd>l</kbd>                 | 更改标记。 |
| <kbd>r</kbd>                 | 开始写评论。评论中引用了预先选择的文本。不能用于在主题中回复。 |
| <kbd>b</kbd>                 | 复制源分支名称（仅限合并请求）。 |
| <kbd>.</kbd>     | 打开 Web IDE<!--[Web IDE](project/web_ide/index.md)-->。 |
| <kbd>→</kbd>                 | 转到下一个设计。 |
| <kbd>←</kbd>                 | 转到上一个设计。 |
| <kbd>Escape</kbd>            | 关闭设计。 |

<a id="merge-requests"></a>

### 合并请求

查看[合并请求](project/merge_requests/index.md)时可以使用这些快捷方式：

| macOS 快捷键                  | Windows 快捷键    | 描述 |
|---------------------------------|---------------------|-------------|
| <kbd>]</kbd> 或 <kbd>j</kbd>      |                     | 移动到下一个文件。 |
| <kbd>&#91;</kbd> 或 <kbd>k</kbd>     |                     | 移动到上一个文件。 |
| <kbd>Command</kbd> + <kbd>p</kbd> | <kbd>Control</kbd> + <kbd>p</kbd> | 搜索，然后跳转到文件进行审核。 |
| <kbd>n</kbd>                      |                     | 转到下一个未解决的讨论。 |
| <kbd>p</kbd>                      |                     | 转到上一个未解决的讨论。 |
| <kbd>b</kbd>                      |                     |  复制源分支名称。 |
| <kbd>r</kbd>                      |                     |  开始写评论。评论中引用了预先选择的文本。不能用于在主题中回复。 |
| <kbd>c</kbd>                      |                     |  移动到下一个提交。 |
| <kbd>x</kbd>                      |                     |  移动到上一个提交。 |


### 项目文件

浏览项目中的文件时可以使用这些快捷方式 (赚到 **仓库 > 文件**)：

| 键盘快捷键  | 描述 |
|-------------------|-------------|
| <kbd>↑</kbd>      | 上移选择。 |
| <kbd>↓</kbd>      | 下移选择。 |
| <kbd>enter</kbd>  | 打开所选。 |
| <kbd>Escape</kbd> | 返回文件列表屏幕 (仅在搜索文件时，**仓库 > 文件**，然后选择 **查找文件**)。 |
| <kbd>y</kbd>      | 转到文件永久链接（仅在查看文件时）。 |
| <kbd>.</kbd>     | 打开 Web IDE<!--[Web IDE](project/web_ide/index.md)-->。 |

### Web IDE

使用 Web IDE<!--[Web IDE](project/web_ide/index.md)--> 编辑文件时，可以使用以下快捷方式：

| macOS 快捷键                  | Windows/Linux 快捷键    | 描述 |
|---------------------------------|---------------------|-------------|
| <kbd>Option</kbd> + <kbd>Command</kbd> + <kbd>↑</kbd> | <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>↑</kbd> | 在上方添加光标 |
| <kbd>Option</kbd> + <kbd>Command</kbd> + <kbd>↓</kbd>   | <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>↓</kbd> | 在下方添加光标 |
| <kbd>Shift</kbd> + <kbd>Option</kbd> + <kbd>I</kbd>   | <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>I</kbd> | 将光标添加到行尾 |
| <kbd>Command</kbd> + <kbd>K</kbd>, <kbd>Command</kbd> + <kbd>C</kbd>   | <kbd>Control</kbd> + <kbd>K</kbd>, <kbd>Control</kbd> + <kbd>C</kbd> _or_ <kbd>Control</kbd> + <kbd>/</kbd> | 添加行评论 |
| <kbd>Command</kbd> + <kbd>D</kbd>   | <kbd>Control</kbd> + <kbd>D</kbd> | 将选择添加到下一个查找匹配项 |
| <kbd>Command</kbd> + <kbd>F2</kbd>   | <kbd>Control</kbd> + <kbd>F2</kbd> | 更改所有事件 |
| <kbd>F1</kbd>   | <kbd>F1</kbd> | 命令面板 |
| <kbd>Shift</kbd> + <kbd>Option</kbd> + <kbd>↓</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>↓</kbd> | 向下复制行 |
| <kbd>Shift</kbd> + <kbd>Option</kbd> + <kbd>↑</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>↑</kbd> | 向上复制行[（Linux 注意事项）](#linux-shortcuts) |
| <kbd>Command</kbd> + <kbd>U</kbd> | <kbd>Control</kbd> + <kbd>U</kbd> | 光标撤消 |
| <kbd>Command</kbd> + <kbd>Backspace</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>Backspace</kbd> | 删除左侧全部 |
| <kbd>Control</kbd> + <kbd>K</kbd> |  | 删除右侧全部 |
| <kbd>Shift</kbd> + <kbd>Command</kbd> + <kbd>K</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>K</kbd> | 删除行 |
| | <kbd>Control</kbd> + <kbd>Backspace</kbd> | 删除单词 |
| <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>Command</kbd> + <kbd>→</kbd> | <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>→</kbd> | 展开选择 |
| <kbd>Command</kbd> + <kbd>P</kbd> | <kbd>Control</kbd> + <kbd>P</kbd> | 文件查找器 |
| <kbd>Command</kbd> + <kbd>F</kbd> | <kbd>Control</kbd> + <kbd>F</kbd> | 查找 |
| <kbd>Enter</kbd> | <kbd>Enter</kbd> or <kbd>F3</kbd> | 查找下一个 |
| <kbd>Command</kbd> + <kbd>F3</kbd> | <kbd>F3</kbd> | 查找下一个选择[（Linux 注意事项）](#linux-shortcuts) |
| <kbd>Shift</kbd> + <kbd>Enter</kbd> + <kbd>F3</kbd> | <kbd>Shift</kbd> + <kbd>F3</kbd> | 查找上一个 |
| <kbd>Shift</kbd> + <kbd>Command</kbd> + <kbd>F3</kbd> | <kbd>Shift</kbd> + <kbd>F3</kbd> | 查找上一个选择 |
| <kbd>Command</kbd> + <kbd>E</kbd> |  | 选择查找 |
| <kbd>Option</kbd> + <kbd>Command</kbd> + <kbd>&#91;</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>&#91;</kbd> | 折叠 |
| <kbd>Command</kbd> + <kbd>K</kbd>，然后 <kbd>Command</kbd> + <kbd>O</kbd> | <kbd>Control</kbd> + <kbd>K</kbd>，然后 <kbd>Control</kbd> + <kbd>O</kbd> | 折叠所有 |
| <kbd>Command</kbd> + <kbd>K</kbd>，然后 <kbd>Command</kbd> + <kbd>/</kbd> | <kbd>Control</kbd> + <kbd>K</kbd>，然后 <kbd>Control</kbd> + <kbd>/</kbd> | 折叠所有块评论 |
| <kbd>Command</kbd> + <kbd>K</kbd> ，然后 <kbd>Command</kbd> + <kbd>8</kbd> | <kbd>Control</kbd> + <kbd>K</kbd> ，然后 <kbd>Control</kbd> + <kbd>8</kbd> | 折叠所有区域 |
| <kbd>Command</kbd> + <kbd>K</kbd> ，然后 <kbd>Command</kbd> + <kbd>-</kbd> | <kbd>Control</kbd> + <kbd>K</kbd> ，然后 <kbd>Control</kbd> + <kbd>-</kbd> | 折叠除选定区域以外的所有区域 |
| <kbd>Command</kbd> + <kbd>K</kbd> ，然后 <kbd>Command</kbd> + <kbd>1</kbd> | <kbd>Control</kbd> + <kbd>K</kbd> ，然后 <kbd>Control</kbd> + <kbd>1</kbd> | 折叠级别 1 |
| <kbd>Command</kbd> + <kbd>K</kbd> ，然后 <kbd>Command</kbd> + <kbd>2</kbd> | <kbd>Control</kbd> + <kbd>K</kbd> ，然后 <kbd>Control</kbd> + <kbd>2</kbd> | 折叠级别 2 |
| <kbd>Command</kbd> + <kbd>K</kbd> ，然后 <kbd>Command</kbd> + <kbd>3</kbd> | <kbd>Control</kbd> + <kbd>K</kbd> ，然后 <kbd>Control</kbd> + <kbd>3</kbd> | 折叠级别 3 |
| <kbd>Command</kbd> + <kbd>K</kbd> ，然后 <kbd>Command</kbd> + <kbd>4</kbd> | <kbd>Control</kbd> + <kbd>K</kbd> ，然后 <kbd>Control</kbd> + <kbd>4</kbd> | 折叠级别 4 |
| <kbd>Command</kbd> + <kbd>K</kbd> ，然后 <kbd>Command</kbd> + <kbd>5</kbd> | <kbd>Control</kbd> + <kbd>K</kbd> ，然后 <kbd>Control</kbd> + <kbd>5</kbd> | 折叠级别 5 |
| <kbd>Command</kbd> + <kbd>K</kbd> ，然后 <kbd>Command</kbd> + <kbd>6</kbd> | <kbd>Control</kbd> + <kbd>K</kbd> ，然后 <kbd>Control</kbd> + <kbd>6</kbd> | 折叠级别 6 |
| <kbd>Command</kbd> + <kbd>K</kbd> ，然后 <kbd>Command</kbd> + <kbd>7</kbd> | <kbd>Control</kbd> + <kbd>K</kbd> ，然后 <kbd>Control</kbd> + <kbd>7</kbd> | 折叠级别 7 |
| <kbd>Command</kbd> + <kbd>K</kbd> ，然后 <kbd>Command</kbd> + <kbd>&#91;</kbd> | <kbd>Control</kbd> + <kbd>K</kbd> ，然后 <kbd>Control</kbd> + <kbd>&#91;</kbd> | 递归折叠 |
| <kbd>Shift</kbd> + <kbd>Command</kbd> + <kbd>&#92;</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>&#92;</kbd> | 转到括号 |
| <kbd>Control</kbd> + <kbd>G</kbd> | <kbd>Control</kbd> + <kbd>G</kbd> | 转到行或列 |
| <kbd>Option</kbd> + <kbd>F8</kbd> | <kbd>Alt</kbd> + <kbd>F8</kbd> | 转到下一个问题（错误、警告、信息） |
| <kbd>F8</kbd> | <kbd>F8</kbd> | 转到文件中的下一个问题（错误、警告、信息） |
| <kbd>Shift</kbd> + <kbd>Option</kbd> + <kbd>F8</kbd> | <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>F8</kbd> | 转到上一个问题（错误、警告、信息） |
| <kbd>Shift</kbd> + <kbd>F8</kbd> | <kbd>Shift</kbd> + <kbd>F8</kbd> | 转到文件中的上一个问题（错误、警告、信息） |
| <kbd>Command</kbd> + <kbd>&#93;</kbd> | <kbd>Control</kbd> + <kbd>&#93;</kbd> |  缩进线 |
| <kbd>Shift</kbd> + <kbd>Command</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>Enter</kbd> |  在上面插入行 |
| <kbd>Command</kbd> + <kbd>Enter</kbd> | <kbd>Control</kbd> + <kbd>Enter</kbd> |  在下面插入行 |
| <kbd>Control</kbd> + <kbd>J</kbd> | <kbd>Control</kbd> + <kbd>J</kbd> |  连接行[（Linux 注意事项）](#linux-shortcuts)  |
| <kbd>Command</kbd> + <kbd>K</kbd>，然后 <kbd>Command</kbd> + <kbd>D</kbd> | <kbd>Control</kbd> + <kbd>K</kbd>，然后 <kbd>Control</kbd> + <kbd>D</kbd>  |  将最后一个选择移动到下一个匹配项 |
| <kbd>Option</kbd> + <kbd>↓</kbd> | <kbd>Alt</kbd> + <kbd>↓</kbd> |  下移一行 |
| <kbd>Option</kbd> + <kbd>↑</kbd> | <kbd>Alt</kbd> + <kbd>↑</kbd> |  上移一行 |
| <kbd>Command</kbd> + <kbd>&#91;</kbd> | <kbd>Control</kbd> + <kbd>&#91;</kbd> |  凸出行 |
| <kbd>Shift</kbd> + <kbd>Command</kbd> + <kbd>P</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>P</kbd> |  预览 Markdown[（Linux 注意事项）](#linux-shortcuts) |
| <kbd>Command</kbd> + <kbd>K</kbd>，然后 <kbd>Command</kbd> + <kbd>U</kbd> | <kbd>Control</kbd> + <kbd>K</kbd>，然后 <kbd>Control</kbd> + <kbd>U</kbd> or <kbd>Control</kbd> + <kbd>/</kbd> |  删除行评论 |
| <kbd>Option</kbd> + <kbd>Command</kbd> + <kbd>F</kbd> | <kbd>Control</kbd> + <kbd>F</kbd> |  替换 |
| <kbd>Shift</kbd> + <kbd>Command</kbd> + <kbd>.</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>.</kbd> | 替换为下一个值 |
| <kbd>Shift</kbd> + <kbd>Command</kbd> + <kbd>,</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>,</kbd> |  替换为上一个值 |
| <kbd>Command</kbd> + <kbd>S</kbd> | <kbd>Control</kbd> + <kbd>S</kbd> | 保存文件 |
| <kbd>Shift</kbd> + <kbd>Command</kbd> + <kbd>L</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>L</kbd> |  选择所有出现的查找匹配项  |
| <kbd>Command</kbd> + <kbd>K</kbd>，然后 <kbd>Command</kbd> + <kbd>B</kbd> | <kbd>Control</kbd> + <kbd>K</kbd>，然后 <kbd>Control</kbd> + <kbd>B</kbd> |  设置选择锚点  |
| <kbd>Option</kbd> + <kbd>F1</kbd> | <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>F1</kbd> |  显示辅助功能帮助 |
| <kbd>Shift</kbd> + <kbd>F10</kbd> | <kbd>Shift</kbd> + <kbd>F10</kbd> |  显示编辑器上下文菜单 |
| <kbd>Command</kbd> + <kbd>K</kbd>，然后 <kbd>Command</kbd> + <kbd>I</kbd> | <kbd>Control</kbd> + <kbd>K</kbd>，然后 <kbd>Control</kbd> + <kbd>I</kbd> |  显示悬停 |
| <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>Command</kbd> + <kbd>←</kbd> | <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>←</kbd> |  收缩选择 |
| <kbd>Shift</kbd> + <kbd>Option</kbd> + <kbd>A</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>A</kbd> |  切换块评论 |
| <kbd>Command</kbd> + <kbd>K</kbd>，然后 <kbd>Command</kbd> + <kbd>L</kbd> | <kbd>Control</kbd> + <kbd>K</kbd>，然后 <kbd>Control</kbd> + <kbd>L</kbd> |  切换折叠 |
| <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>M</kbd> | <kbd>Control</kbd> + <kbd>M</kbd> |  切换 Tab 键移动焦点  |
| <kbd>Command</kbd> + <kbd>/</kbd> | <kbd>Control</kbd> + <kbd>/</kbd> |  切换行评论 |
| <kbd>Control</kbd> + <kbd>T</kbd> |  |  转置字母 |
| <kbd>Control</kbd> + <kbd>Space</kbd> | <kbd>Control</kbd> + <kbd>Space</kbd> |  触发建议 |
| <kbd>Command</kbd> + <kbd>K</kbd>，然后 <kbd>Command</kbd> + <kbd>X</kbd> | <kbd>Control</kbd> + <kbd>K</kbd>，然后 <kbd>Control</kbd> + <kbd>X</kbd> | 修剪尾随空格  |
| <kbd>Option</kbd> + <kbd>Command</kbd> + <kbd>&#93;</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>&#93;</kbd> | 展开  |
| <kbd>Command</kbd> + <kbd>K</kbd>，然后 <kbd>Command</kbd> + <kbd>J</kbd> | <kbd>Control</kbd> + <kbd>K</kbd>，然后 <kbd>Control</kbd> + <kbd>J</kbd> |  展开全部 |
| <kbd>Command</kbd> + <kbd>K</kbd>，然后 <kbd>Command</kbd> + <kbd>9</kbd> | <kbd>Control</kbd> + <kbd>K</kbd>，然后 <kbd>Control</kbd> + <kbd>9</kbd> |  展开所有区域 |
| <kbd>Command</kbd> + <kbd>K</kbd>，然后 <kbd>Command</kbd> + <kbd>=</kbd> | <kbd>Control</kbd> + <kbd>K</kbd>，然后 <kbd>Control</kbd> + <kbd>=</kbd> |  展开除选定区域之外的所有区域 |
| <kbd>Command</kbd> + <kbd>K</kbd>，然后 <kbd>Command</kbd> + <kbd>&#93;</kbd> | <kbd>Control</kbd> + <kbd>K</kbd>，然后 <kbd>Control</kbd> + <kbd>&#93;</kbd> |  递归展开 |
| <kbd>Command</kbd> + <kbd>Enter</kbd> | <kbd>Control</kbd> + <kbd>Enter</kbd> | 提交（在编辑提交消息时） |


### 仓库分支图

查看仓库分支图<!--[仓库分支图](project/repository/index.md#repository-history-graph)-->页面时（导航到 **仓库 > 分支图**），可以使用以下快捷方式：

| 键盘快捷键                                                   | 描述 |
|--------------------------------------------------------------------|-------------|
| <kbd>←</kbd> or <kbd>h</kbd>                                       | 向左滚动。 |
| <kbd>→</kbd> or <kbd>l</kbd>                                       | 向右滚动。 |
| <kbd>↑</kbd> or <kbd>k</kbd>                                       | 向上滚动。 |
| <kbd>↓</kbd> or <kbd>j</kbd>                                       | 向下滚动。 |
| <kbd>Shift</kbd> + <kbd>↑</kbd> or <kbd>Shift</kbd> + <kbd>k</kbd> | 滚动到顶部。 |
| <kbd>Shift</kbd> + <kbd>↓</kbd> or <kbd>Shift</kbd> + <kbd>j</kbd> | 滚动到底部。 |

### Wiki 页面

查看 wiki 页面<!--[wiki 页面](project/wiki/index.md)-->时，可以使用以下快捷方式：

| 键盘快捷键  | 描述 |
|-------------------|-------------|
| <kbd>e</kbd>      | 编辑 wiki 页面。 |

### 内容编辑器

以下快捷方式在使用内容编辑器编辑文件时可用：

| 键盘快捷键  | 描述 |
|-------------------|-------------|
| <kbd>⌘</kbd> + <kbd>C</kbd> (Mac) / <kbd>Control</kbd> + <kbd>C</kbd> | 复制 |
| <kbd>⌘</kbd> + <kbd>X</kbd> (Mac) / <kbd>Control</kbd> + <kbd>X</kbd> | 剪切 |
| <kbd>⌘</kbd> + <kbd>V</kbd> (Mac) / <kbd>Control</kbd> + <kbd>V</kbd> | 粘贴 |
| <kbd>⌘</kbd> + <kbd>Shift</kbd> + <kbd>V</kbd> (Mac) / <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>V</kbd> | 无格式粘贴 |
| <kbd>⌘</kbd> + <kbd>Z</kbd> (Mac) / <kbd>Control</kbd> + <kbd>Z</kbd> | 撤销 |
| <kbd>Shift</kbd> + <kbd>Enter</kbd> | 添加换行符 |

#### 格式

| Mac | Windows/Linux | 描述 |
|-----|---------------|-------------|
| <kbd>⌘</kbd> + <kbd>b</kbd> | <kbd>Control</kbd> + <kbd>b</kbd>  | 加粗 |
| <kbd>⌘</kbd> + <kbd>i</kbd> | <kbd>Control</kbd> + <kbd>i</kbd>   | 斜体 |
| <kbd>⌘</kbd> + <kbd>Shift</kbd> + <kbd>s</kbd>  | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>s</kbd>   | 删除线 |
| <kbd>⌘</kbd> + <kbd>e</kbd> | <kbd>Control</kbd> + <kbd>e</kbd>   | 代码 |
| <kbd>⌘</kbd> + <kbd>Alt</kbd> + <kbd>0</kbd> | <kbd>Control</kbd> + <kbd>Alt</kbd> + <kbd>0</kbd> | 应用普通文本样式 |
| <kbd>⌘</kbd> + <kbd>Alt</kbd> + <kbd>1</kbd> | <kbd>Control</kbd> + <kbd>Alt</kbd> + <kbd>1</kbd> | 应用标题样式 1 |
| <kbd>⌘</kbd> + <kbd>Alt</kbd> + <kbd>2</kbd> | <kbd>Control</kbd> + <kbd>Alt</kbd> + <kbd>2</kbd> | 应用标题样式 2 |
| <kbd>⌘</kbd> + <kbd>Alt</kbd> + <kbd>3</kbd> | <kbd>Control</kbd> + <kbd>Alt</kbd> + <kbd>3</kbd> | 应用标题样式 3 |
| <kbd>⌘</kbd> + <kbd>Alt</kbd> + <kbd>4</kbd> | <kbd>Control</kbd> + <kbd>Alt</kbd> + <kbd>4</kbd> | 应用标题样式 4 |
| <kbd>⌘</kbd> + <kbd>Alt</kbd> + <kbd>5</kbd> | <kbd>Control</kbd> + <kbd>Alt</kbd> + <kbd>5</kbd> | 应用标题样式 5 |
| <kbd>⌘</kbd> + <kbd>Alt</kbd> + <kbd>6</kbd> | <kbd>Control</kbd> + <kbd>Alt</kbd> + <kbd>6</kbd> | 应用标题样式 6 |
| <kbd>⌘</kbd> + <kbd>Shift</kbd> + <kbd>7</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>7</kbd> | 有序列表 |
| <kbd>⌘</kbd> + <kbd>Shift</kbd> + <kbd>8</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>8</kbd> | 无序列表 |
| <kbd>⌘</kbd> + <kbd>Shift</kbd> + <kbd>9</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>9</kbd> | 任务列表 |
| <kbd>⌘</kbd> + <kbd>Shift</kbd> + <kbd>b</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>b</kbd> | 块引用 |
| <kbd>Command</kbd> + <kbd>Alt</kbd> + <kbd>c</kbd> | <kbd>Control</kbd> + <kbd>Alt</kbd> + <kbd>c</kbd> | 代码块 |
| <kbd>Command</kbd> + <kbd>Shift</kbd> + <kbd>h</kbd> | <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>h</kbd> | 高亮 |
| <kbd>⌘</kbd> + <kbd>,</kbd> | <kbd>Control</kbd> + <kbd>,</kbd> | 下标 |
| <kbd>⌘</kbd> + <kbd>.</kbd> | <kbd>Control</kbd> + <kbd>,</kbd> | 上标 |
| <kbd>Tab</kbd> | | 缩进列表 |
| <kbd>Shift</kbd> + <kbd>Tab</kbd> | | 减小缩进列表 |

#### 文本选择

| 键盘快捷键  | 描述 |
|-------------------|-------------|
| <kbd>⌘</kbd> + <kbd>a</kbd> (Mac) / <kbd>Control</kbd> + <kbd>a</kbd> | 选择所有 |
| <kbd>Shift</kbd> + <kbd>←</kbd> | 将所选内容向左扩展一个字符 |
| <kbd>Shift</kbd> + <kbd>→</kbd> | 将所选内容向右扩展一个字符 |
| <kbd>Shift</kbd> + <kbd>↑</kbd> | 将选择向上扩展一行 |
| <kbd>Shift</kbd> + <kbd>↓</kbd> | 将选择向下扩展一行 |
| <kbd>⌘</kbd> + <kbd>Shift</kbd> + <kbd>↑</kbd> (Mac) / <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>↑</kbd> | 将选择范围扩展到文档的开头 |
| <kbd>⌘</kbd> + <kbd>Shift</kbd> + <kbd>↓</kbd> (Mac) / <kbd>Control</kbd> + <kbd>Shift</kbd> + <kbd>↓</kbd>  | 将选择范围扩展到文档末尾 |

### 过滤搜索

使用过滤搜索输入<!--[过滤搜索输入](search/index.md)-->时，可以使用以下快捷方式：

| 键盘快捷键                                       | 描述 |
|--------------------------------------------------------|-------------|
| <kbd>⌘</kbd> (Mac) + <kbd>⌫</kbd>                      | 清除整个搜索过滤器。 |
| <kbd>⌥</kbd> (Mac) / <kbd>Control</kbd> + <kbd>⌫</kbd> | 一次清除一个令牌。 |

## 史诗 **(PREMIUM)**

查看[史诗](group/epics/index.md)时可以使用以下快捷方式：

| 键盘快捷键  | 描述   |
|-------------------|-------------|
| <kbd>r</kbd>      | 开始写评论。评论中引用了预先选择的文本。不能用于在主题中回复。 |
| <kbd>e</kbd>      | 编辑描述。 |
| <kbd>l</kbd>      | 更改标记。 |

## 指标

使用指标时可以使用以下快捷方式：

| 键盘快捷键  | 描述   |
|-------------------|---------------------|
| <kbd>e</kbd>      | 展开面板。       |
| <kbd>l</kbd>      | 查看日志。          |
| <kbd>d</kbd>      | 下载 CSV。       |
| <kbd>c</kbd>      | 将链接复制到图表。 |
| <kbd>a</kbd>      | 警报。             |

## 禁用键盘快捷键

要禁用键盘快捷键：

1. 在查看支持键盘快捷键的页面时，在文本框外按 <kbd>?</kbd> 显示快捷键列表。
1. 选择 **切换快捷方式**。

## 启用键盘快捷键

要启用键盘快捷键：

1. 在顶部栏中，选择帮助菜单（**{question}**），然后选择 **快键键**。
1. 选择 **切换快捷方式**。

## 故障排查

<a id="linux-shortcuts"></a>

### Linux 快捷键

Linux 用户可能会遇到被他们的操作系统或浏览器覆盖的极狐GitLab 键盘快捷键。
