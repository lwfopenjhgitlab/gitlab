---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 适用于极狐GitLab 的 Go 代理 **(FREE)**

> - 引入于 13.1 版本。
> - 存在功能标志，默认禁用。
> - 在 SaaS 上禁用。
> - 要在私有化部署实例中使用它，请让极狐GitLab 管理员[启用它](#enable-the-go-proxy)。
> - 从旗舰版移动到免费版于 13.3 版本。

WARNING:
极狐GitLab 的 Go 软件包库正在开发中，由于功能有限，尚未准备好用于生产。<!--This [epic](https://gitlab.com/groups/gitlab-org/-/epics/3043) details the remaining
work and timelines to make it production ready.-->

使用适用于极狐GitLab 的 Go 代理，可以使用 [Go 代理协议](https://proxy.golang.org/) 获取极狐GitLab 中的每个项目。

<!--
For documentation of the specific API endpoints that the Go Proxy uses, see the
[Go Proxy API documentation](../../../api/packages/go_proxy.md).
-->

<a id="enable-the-go-proxy"></a>

## 启用 Go 代理

<!--
The Go proxy for GitLab is under development, and isn't ready for production use
due to [potential performance issues with large repositories](https://gitlab.com/gitlab-org/gitlab/-/issues/218083).

It's deployed behind a feature flag that is _disabled by default_.
-->

[有权访问 Rails 控制台的极狐GitLab 管理员](../../../administration/feature_flags.md)可以为您的实例启用它。

启用：

```ruby
Feature.enable(:go_proxy) # or
```

禁用：

```ruby
Feature.disable(:go_proxy)
```

为特定项目启用或禁用：

```ruby
Feature.enable(:go_proxy, Project.find(1))
Feature.disable(:go_proxy, Project.find(2))
```

NOTE:
即使已启用，极狐GitLab 也不会在**软件包库**中显示 Go 模块。<!--Follow [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/213770) for
details.-->

## 将极狐GitLab 添加为 Go 代理

要将极狐GitLab 用作 Go 代理，您必须使用 Go 1.13 或更高版本。

可用的代理端点用于按项目获取模块：`/api/v4/projects/:id/packages/go`

要从极狐GitLab 获取 Go 模块，请将特定于项目的端点添加到 `GOPROXY`。

Go 查询端点并回退到默认行为：

```shell
go env -w GOPROXY='https://gitlab.example.com/api/v4/projects/1234/packages/go,https://proxy.golang.org,direct'
```

使用此配置，Go 按以下顺序获取依赖项：

1. Go 尝试从项目特定的 Go 代理中获取。
1. Go 尝试从 [proxy.golang.org](https://proxy.golang.org) 获取。
1. Go 直接使用版本控制系统操作（如 `git clone`、`svn checkout` 等）获取。

如果没有指定 `GOPROXY`，Go 会执行第 2 步和第 3 步，这对应于将 `GOPROXY` 设置为 `https://proxy.golang.org,direct`。如果 `GOPROXY` 仅包含特定于项目的端点，则 Go 仅查询该端点。

关于如何设置 Go 环境变量的详细信息，请参见[设置环境变量](#set-environment-variables)。

<!--
For details about configuring `GOPROXY`, see
[Dependency Management in Go > Proxies](../../../development/go_guide/dependencies.md#proxies).
-->

<a id="fetch-modules-from-private-projects"></a>

## 从私有项目中获取模块

`go` 不支持通过不安全的连接传输凭据。以下步骤仅在为 HTTPS 配置极狐GitLab 时有效：

1. 配置 Go，在从极狐GitLab 的 Go 代理获取时包含 HTTP 基本身份验证凭据。
1. 配置 Go，跳过从公共校验和数据库下载私有极狐GitLab 项目的校验和。

### 启用请求身份验证

创建一个 [个人访问令牌](../../profile/personal_access_tokens.md)，范围设置为 `api` 或 `read_api`。

打开您的 [`~/.netrc`](https://everything.curl.dev/usingcurl/netrc) 文件并添加以下文本。将 `< >` 中的变量替换为您的值。

WARNING:
如果您使用名为 `NETRC` 的环境变量，Go 将使用其值作为文件名并忽略 `~/.netrc`。如果您打算在 CI 中使用 `~/.netrc`，**请勿使用 `NETRC` 作为环境变量名称**。

```plaintext
machine <url> login <username> password <token>
```

- `<url>`：极狐GitLab URL，例如 `jihulab.com`。
- `<用户名>`：您的用户名。
- `<token>`：您的个人访问令牌。

### 禁用校验和数据库查询

当使用 Go 1.13 及更高版本下载依赖项时，获取的源将根据校验和数据库 `sum.golang.org` 进行验证。

如果获取的源的校验和与数据库中的校验和不匹配，Go 不会构建依赖项。

私有模块无法构建，因为 `sum.golang.org` 无法获取私有模块的来源，因此无法提供校验和。

要解决此问题，请将 `GONOSUMDB` 设置为以逗号分隔的私有项目列表。有关设置 Go 环境变量的详细信息，请参阅[设置环境变量](#set-environment-variables)。<!--For more details about
disabling this feature of Go, see
[Dependency Management in Go > Checksums](../../../development/go_guide/dependencies.md#checksums).-->

例如，要禁用 `gitlab.com/my/project` 的校验和查询，请设置 `GONOSUMDB`：

```shell
go env -w GONOSUMDB='gitlab.com/my/project,<previous value>'
```

## 使用 Go

如果您不熟悉在 Go 中管理依赖项，或者一般来说不熟悉 Go，请查看以下文档：


- [Go 模块参考](https://go.dev/ref/mod)
- [文档 (`golang.org`)](https://go.dev/doc/)
- [学习 (`go.dev/learn`)](https://go.dev/learn/)

<!--- [Dependency Management in Go](../../../development/go_guide/dependencies.md)-->

<a id="set-environment-variables"></a>

### 设置环境变量

Go 使用环境变量来控制各种功能。您可以通过所有常用方式管理这些变量。然而，Go 1.14 默认从一个特殊的 Go 环境文件 `~/.go/env` 读取和写入 Go 环境变量。

- 如果 `GOENV` 设置为一个文件，Go 会改为读取和写入该文件。
- 如果 `GOENV` 未设置但 `GOPATH` 已设置，Go 会读取和写入 `$GOPATH/env`。

Go 环境变量可以使用 `go env <var>` 读取，并且在 Go 1.14 及更高版本中，可以使用`go env -w <var>=<value>` 编写。例如，`go env GOPATH` 或 `go env -w GOPATH=/go`。

### 发布一个模块

Go 模块和模块版本由源仓库定义，例如 Git、SVN 和 Mercurial。模块是包含 `go.mod` 和 Go 文件的仓库。模块版本由版本控制系统 (VCS) 标签定义。

要发布模块，请将 go.mod 和源文件推送到 VCS 仓库。要发布模块版本，请推送 VCS 标签。
<!--See [Dependency Management in Go > Versioning](../../../development/go_guide/dependencies.md#versioning)
for more details about what constitutes a valid module or module version.-->
