---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 减少 Package Registry 存储 **(FREE)**

如果不进行清理，软件包库会随着时间的推移而变大。添加大量包及其 assets 时：

- 获取包列表变得更慢。
- 它们占用服务器上的大量存储空间，影响您的[存储使用配额](../../usage_quotas.md)。

我们建议删除不必要的包和 assets。此页面提供了如何执行此操作的示例。

## 检查 Package Registry 存储使用

使用量配额页面（**设置 > 使用量配额 > 存储**）显示包的存储使用情况。

## 删除包

在软件包库中发布包后，您将无法对其进行编辑。相反，您必须删除并重新创建它。

要删除包，您必须具有合适的权限。

您可以使用 API 或 UI 删除包。

要在 UI 中从您的组或项目中删除包：

1. 进入 **软件包与镜像库 > 软件包库**。
1. 找到要删除的包的名称。
1. 点击 **删除**。

包被永久删除。

## 删除与包关联的 assets

要删除 assets，您必须具有合适的权限。

您可以使用 API 或 UI 删除包。

要从您的群组或项目中删除 UI 中的包文件：

1. 进入 **软件包与镜像库 > 软件包库**。
1. 找到要删除的包的名称。
1. 选择包以查看其他详细信息。
1. 找到您要删除的 assets 的名称。
1. 展开省略号并选择 **删除 asset**。

软件包 assets 被永久删除。

## 清理策略

> 引入于 15.2 版本。

根据要删除的包的数量，手动删除包的过程可能需要很长时间才能完成。
清理策略定义了一组规则，这些规则应用于项目，定义了您可以自动删除哪些包 assets。

### 启用清理策略

默认情况下，包清理策略是禁用的。要启用它：

1. 转到您的项目 **设置 > 软件包与镜像库**。
1. 展开 **管理软件包 assets 使用的存储**。
1. 适当设定规则。

NOTE:
要访问这些项目设置，您必须至少是相关项目的维护者。

<!--
### Available rules

- `Number of duplicated assets to keep`: The number of duplicated assets to keep. Some package formats allow you
  to upload more than one copy of an asset. You can limit the number of duplicated assets to keep and automatically
  delete the oldest assets once the limit is reached. Unique filenames, such as those produced by Maven snapshots, are not considered when evaluating the number of duplicated assets to keep.
-->

### 设置清理限制以节省资源

后台进程执行包清理策略。此过程可能需要很长时间才能完成，并且在运行时会消耗服务器资源。

您可以使用以下设置来限制清理 worker 的数量：

- `package_registry_cleanup_policies_worker_capacity`：同时运行的清理 worker 的最大数量。此数字必须大于或等于 `0`。我们建议从一个较小的数字开始，并在监控后台 worker 使用的资源后增加它。要删除所有 worker 而不执行清理策略，请将其设置为 `0`。默认值为 `2`。
