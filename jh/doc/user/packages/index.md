---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 软件包和仓库 **(FREE)**

极狐GitLab [软件包库](package_registry/index.md) 充当各种常见包管理器的私有或公共库。您可以发布和共享软件包，这些包可以很容易地作为下游项目的依赖项使用。

## Container Registry

GitLab [Container Registry](container_registry/index.md) 是容器镜像的安全私有镜像库。它建立在开源软件上，并完全集成在极狐GitLab 中。使用 GitLab CI/CD 创建和发布镜像。<!--使用 GitLab [API](../../api/container_registry.md) 管理跨组和项目的注册表。-->

<!--
## Infrastructure Registry

The GitLab [Infrastructure Registry](infrastructure_registry/index.md) is a secure and private registry for infrastructure packages. You can use GitLab CI/CD to create and publish infrastructure packages.

The Infrastructure Registry supports the following formats:

| Package type | GitLab version |
| ------------ | -------------- |
| [Terraform Module](terraform_module_registry/index.md) | 14.0+ |

## Dependency Proxy

The [Dependency Proxy](dependency_proxy/index.md) is a local proxy for frequently-used upstream images and packages.
-->