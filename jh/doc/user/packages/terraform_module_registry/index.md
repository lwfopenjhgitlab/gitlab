---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Terraform 模块库 **(FREE)**

> - 引入于 14.0 版本。
> - 基础设施库和 Terraform 模块库合并于 15.11 版本，合并为单个 Terraform 模块库功能。

借助 Terraform 模块库，您可以将极狐GitLab 项目用作 Terraform 模块私有库。您可以使用极狐GitLab CI/CD 创建和发布模块，然后可以从其他私有项目中使用这些模块。

## 查看 Terraform 模块

查看项目中的 Terraform 模块：

1. 进入项目。
1. 在左侧边栏中，选择 **软件包和镜像库 > Terraform 模块**。

您可以在此页面上搜索、排序和过滤模块。

有关如何创建和上传软件包的信息，请查看适用于您的软件包类型的极狐GitLab 文档：

- [Terraform 模块](../terraform_module_registry/index.md)

## Terraform 模块库身份验证

要对 Terraform 模块库进行身份验证，您需要：

- 至少具有 `read_api` 权限的个人访问令牌<!--[个人访问令牌](../../../api/index.md#personalproject-access-tokens)-->。
- [CI/CD 作业令牌](../../../ci/jobs/ci_job_token.md)。

不要使用此处记录的方法以外的身份验证方法。将来可能会删除未记录的身份验证方法。

## 发布 Terraform 模块

当您发布 Terraform 模块时，如果它不存在，则会创建它。

先决条件：

- 顶级命名空间中必须不存在具有相同名称和版本的包。
- 您的项目和组名称不得包含点 (`.`)。例如，`source = "gitlab.example.com/my.group/project.name"`。
- 您必须使用 API 进行身份验证<!--[使用 API 进行身份验证](../../../api/index.md#authentication)-->。如果使用部署令牌进行身份验证，则必须使用 `write_package_registry` 范围进行配置。

```plaintext
PUT /projects/:id/packages/terraform/modules/:module-name/:module-system/:module-version/file
```

| 属性          | 类型           | 是否必需 | 描述                                                                                                                      |
| -------------------| --------------- | ---------| -------------------------------------------------------------------------------------------------------------------------------- |
| `id`               | integer/string  | yes      | ID 或项目的 URL 编码路径<!--[项目的 URL 编码路径](../../../api/index.md#namespaced-path-encoding)-->。                                    |
| `module-name`      | string          | yes      | 模块名称。**支持的语法**：1 到 64 个 ASCII 字符，包括小写字母 (a-z)、数字 (0-9) 和连字符 (`-`)。
| `module-system`    | string          | yes      | 模块系统。**支持的语法**：1 到 64 个 ASCII 字符，包括小写字母 (a-z)、数字 (0-9) 和连字符 (`-`)。查看更多信息：[Terraform Module Registry Protocol documentation](https://www.terraform.io/internals/module-registry-protocol)。
| `module-version`   | string          | yes      | 模块版本。根据[语义版本规范](https://semver.org/)，它必须是有效的。

在请求正文中提供文件内容。

请注意，在以下示例中，请求必须以 `/file` 结尾。
如果您发送以其他内容结尾的请求，则会导致 404 错误 `{"error":"404 Not Found"}`。

使用个人访问令牌的示例请求：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
     --upload-file path/to/file.tgz \
     "https://gitlab.example.com/api/v4/projects/<your_project_id>/packages/terraform/modules/my-module/my-system/0.0.1/file"
```

使用部署令牌的示例请求：

```shell
curl --header "DEPLOY-TOKEN: <deploy_token>" \
     --upload-file path/to/file.tgz \
     "https://gitlab.example.com/api/v4/projects/<your_project_id>/packages/terraform/modules/my-module/my-system/0.0.1/file"
```

示例响应：

```json
{
  "message":"201 Created"
}
```

### 使用 CI/CD 模板（推荐）

> 引入于 15.9 版本。

您可以使用 [`Terraform-Module.gitlab-ci.yml`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform-Module.gitlab-ci.yml) 或高级 [`Terraform/Module-Base.gitlab-ci.yml`](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform/Module-Base.gitlab-ci.yml) CI/CD 模板，将 Terraform 模块发布到极狐GitLab terraform 库：

```yaml
include:
  template: Terraform-Module.gitlab-ci.yml
```

流水线包含以下作业：

- `fmt` - 验证 Terraform 模块的格式。
- `kics-iac-sast` - 测试 Terraform 模块的安全问题。
- `deploy` - 仅适用于标签流水线。将 Terraform 模块部署到 Terraform 模块库。

#### 流水线变量

您可以使用以下变量配置流水线：

| 变量                  | 默认值             | 描述                                                                                    |
|----------------------------|----------------------|-------------------------------------------------------------------------------------------------|
| `TERRAFORM_MODULE_DIR`     | `${CI_PROJECT_DIR}`  | Terraform 项目根目录的相对路径。                               |
| `TERRAFORM_MODULE_NAME`    | `${CI_PROJECT_NAME}` | Terraform 模块的名称。不得包含任何空格或下划线。                  |
| `TERRAFORM_MODULE_SYSTEM`  | `local`              | Terraform 模块目标的系统或提供者。例如，`local`、`aws`、`google`。 |
| `TERRAFORM_MODULE_VERSION` | `${CI_COMMIT_TAG}`   | Terraform 模块版本。您应该遵循语义版本控制规范。          |

## 手动使用 CI/CD

要在[极狐GitLab CI/CD](../../../ci/index.md) 中使用 Terraform 模块，您可以使用 `CI_JOB_TOKEN` 代替命令中的个人访问令牌。

例如：

```yaml
stages:
  - upload

upload:
  stage: upload
  image: curlimages/curl:latest
  variables:
    TERRAFORM_MODULE_DIR: ${CI_PROJECT_DIR} # The path to your Terraform module
    TERRAFORM_MODULE_NAME: ${CI_PROJECT_NAME} # The name of your Terraform module
    TERRAFORM_MODULE_SYSTEM: local # The system or provider your Terraform module targets (ex. local, aws, google)
    TERRAFORM_MODULE_VERSION: ${CI_COMMIT_TAG} # Tag commits with SemVer for the version of your Terraform module to be published
  script:
    - TERRAFORM_MODULE_NAME=$(echo "${TERRAFORM_MODULE_NAME}" | tr " _" -) # module-name must not have spaces or underscores, so translate them to hyphens
    - tar -vczf ${TERRAFORM_MODULE_NAME}-${TERRAFORM_MODULE_SYSTEM}-${TERRAFORM_MODULE_VERSION}.tgz -C ${TERRAFORM_MODULE_DIR} --exclude=./.git .
    - 'curl --location --header "JOB-TOKEN: ${CI_JOB_TOKEN}"
         --upload-file ${TERRAFORM_MODULE_NAME}-${TERRAFORM_MODULE_SYSTEM}-${TERRAFORM_MODULE_VERSION}.tgz
         ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/terraform/modules/${TERRAFORM_MODULE_NAME}/${TERRAFORM_MODULE_SYSTEM}/${TERRAFORM_MODULE_VERSION}/file'
  rules:
    - if: $CI_COMMIT_TAG
```

要触发此上传作业，请将 Git 标签添加到您的提交中。`rules:if: $CI_COMMIT_TAG` 定义这一点，因此不是每次提交到您的仓库都会触发上传。
有关在 CI/CD 流水线中控制作业的其他方法，请参阅 [`.gitlab-ci.yml`](../../../ci/yaml/index.md) 关键字参考。

## 引用 Terraform 模块

先决条件：

- 您需要使用 API 进行身份验证<!--[使用 API 进行身份验证](../../../api/index.md#authentication)-->。如果使用个人访问令牌进行身份验证，则必须使用 `read_api` 范围进行配置。

可以在您的 `~/.terraformrc` 文件中为 `terraform` 提供身份验证令牌（作业令牌或个人访问令牌）：

```terraform
credentials "jihulab.com" {
  token = "<TOKEN>"
}
```

其中 `jihulab.com` 可以替换为您的私有化部署实例的主机名。

然后，您可以从下游 Terraform 项目中引用您的 Terraform 模块：

```terraform
module "<module>" {
  source = "jihulab.com/<namespace>/<module-name>/<module-system>"
}
```

其中 `<namespace>` 是 Terraform 模块库的[命名空间](../../../user/namespace/index.md)。

## 下载 Terraform 模块

下载 Terraform 模块：

1. 在左侧边栏中，选择 **软件包和镜像库 > Terraform 模块**。
1. 选择您要下载的模块名称。
1. 在 **活动** 部分，选择您要下载的模块的名称。

## 模块解析工作原理

当您上传一个新模块时，极狐GitLab 会为该模块生成一个路径，例如 `https://gitlab.example.com/parent-group/my-infra-package`。

- 此路径符合 [Terraform 规范](https://www.terraform.io/internals/module-registry-protocol)。
- 路径名称在命名空间中必须是唯一的。

对于子组中的项目，极狐GitLab 检查模块名称是否已存在于命名空间的任何位置，包括所有子组和父组。

例如：

- 项目为 `gitlab.example.com/parent-group/sub-group/my-project`。
- Terraform 模块为 `my-infra-package`。

项目名称在 `parent-group` 下所有群组的所有项目中必须是唯一的。

## 删除 Terraform 模块

在 Terraform 模块库中发布 Terraform 模块后，您将无法对其进行编辑，您必须删除并重新创建它。

要删除模块，您必须具有合适的[权限](../../permissions.md)。

您可以使用[软件包 API](../../../api/packages.md#delete-a-project-package) 或 UI 删除模块。

要从您的项目中删除 UI 中的模块：

1. 在左侧边栏中，选择 **软件包与镜像库 > Terraform 模块**。
1. 找到要删除的软件包的名称。
1. 选择 **删除**。

软件包被永久删除。

## 禁用 Terraform 模块库

Terraform 模块库自动启用。

对于私有化部署实例，极狐GitLab 管理员可以[禁用](../../../administration/packages/index.md) **软件包与镜像库**，系统会从侧边栏中删除此菜单项。

您还可以删除特定项目的 Terraform 模块库：

1. 在您的项目中，转到 **设置 > 通用**。
1. 展开 **可视化、项目功能和权限** 部分并关闭 **软件包**（灰色）。
1. 选择 **保存更改**。

要重新启用它，请按照上述相同步骤将其打开（蓝色）。

<!--
## Example projects

For examples of the Terraform module registry, check the projects below:

- The [_GitLab local file_ project](https://gitlab.com/mattkasa/gitlab-local-file) creates a minimal Terraform module and uploads it into the Terraform module registry using GitLab CI/CD.
- The [_Terraform module test_ project](https://gitlab.com/mattkasa/terraform-module-test) uses the module from the previous example.
-->
