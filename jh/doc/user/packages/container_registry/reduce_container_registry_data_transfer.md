---
stage: Package
group: Container Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 减少 Container Registry 数据传输 **(FREE)**

根据从 Container Registry 下载镜像或标签的频率，数据传输可能会超过 JiHuLab.com 的限制。此页面提供了一些建议和技巧，可帮助您减少使用 Container Registry 传输的数据量。

## 检查数据传输使用情况

目前无法在 UI 中查看传输使用量。<!--In the interim, GitLab team members may reach out to customers that have
significantly exceeded their transfer limits to better understand their use case and offer ways to reduce data transfer
usage.-->

## 确定镜像大小

使用以下工具和技术来确定图像的大小：

- [Skopeo](https://github.com/containers/skopeo)：使用 Skopeo `inspect` 命令，通过 API 调用检查镜像层和大小。因此，您可以在运行 `docker pull IMAGE` 之前检查此数据。

- CI 中的 Docker：在使用 Docker 推送镜像之前，使用极狐GitLab CI 检查并记录镜像大小。例如：

  ```yaml
  docker inspect "$CI_REGISTRY_IMAGE:$IMAGE_TAG" \
        | awk '/"Size": ([0-9]+)[,]?/{ printf "Final Image Size: %d\n", $2 }'
  ```

- [Dive](https://github.com/wagoodman/dive) 是一个用于探索 Docker 镜像、分层内容并发现减少其大小的方法的工具。

## 减少镜像大小

### 使用更小的基础镜像

考虑使用较小的基础镜像，例如 [Alpine Linux](https://alpinelinux.org/)。
一个 Alpine 镜像大约 5 MB，比 [Debian](https://hub.docker.com/_/debian) 等流行的基础镜像小几倍。
如果您的应用程序作为独立的静态二进制文件分发，例如 Go 应用程序，您还可以考虑使用 Docker [scratch](https://hub.docker.com/_/scratch/) 基础镜像。

如果您需要使用特定的基础镜像操作系统，请寻找 `-slim` 或 `-minimal` 版本，有助于减少镜像大小。

还要注意安装在基本镜像之上的操作系统包。这些文件加起来可达数百兆字节。尝试将已安装软件包的数量保持在最低限度。

[多阶段构建](#use-multi-stage-builds)可以成为清理瞬态构建依赖项的强大助力。

您还可以考虑使用以下工具：

- [DockerSlim](https://github.com/docker-slim/docker-slim) 提供了一组命令来减少容器镜像的大小。
- [Distroless](https://github.com/GoogleContainerTools/distroless) 镜像仅包含您的应用程序及其运行时依赖项。

它们不包含程序包管理器、shell 或您希望在标准 Linux 发行版中找到的任何其他程序。

### 最小化镜像层

Dockerfile 中的每条指令都指向一个新镜像层，镜像层记录在此类指令期间应用的文件系统更改。通常，更多或更大的镜像层会导致更大的镜像。尽量减少在 Dockerfile 中安装包的层数，否则可能会导致构建过程中的每个步骤都增加镜像大小。

有多种策略可以减少镜像层的数量和大小。例如，您可以在单个 `RUN` 命令上安装所有包，而不是对要安装的每个操作系统包使用一个 `RUN` 命令（这将导致每个包一个镜像层），这样可以以在构建过程中减少步骤数，从而减少镜像的大小。

另一个有用的策略是确保在安装包之前和之后，删除所有临时构建依赖项，并禁用或清空操作系统包管理器缓存。

构建镜像时，请确保只复制相关文件。对于 Docker，使用 [`.dockerignore`](https://docs.docker.com/engine/reference/builder/#dockerignore-file) 有助于确保构建过程忽略不相关的文件。

您可以使用其他第三方工具来减少您的镜像大小，例如 [DockerSlim](https://github.com/docker-slim/docker-slim)。
请注意，如果使用不当，此类工具可能会删除您的应用程序在某些条件下运行所需的依赖项。因此，最好在构建过程中争取减少镜像大小，而不是在之后尝试减少大小。

<a id="use-multi-stage-builds"></a>

### 使用多阶段构建

对于[多阶段构建](https://docs.docker.com/build/building/multi-stage/)，您可以在 Dockerfile 中使用多个 `FROM` 语句。每个 `FROM` 指令都可以使用不同的基础镜像，并且每个指令都开始一个新的构建阶段。您可以有选择地将产物从一个阶段复制到另一个阶段，在最终镜像中，您不想要的所有内容都不会留下。当您需要安装构建依赖项，但不需要它们出现在最终镜像中时，这种方法很有用。

## 实用镜像拉取策略

使用 `docker` 或 `docker+machine` 执行器时，您可以在 runner `config.toml` 中，设置一个 [`pull_policy`](https://docs.gitlab.cn/runner/executors/docker.html#using-the-if-not-present-pull-policy) 参数，它定义了 runner 在拉取 Docker 镜像时是如何工作的。
为避免在使用大型且很少更新的镜像时传输数据，请考虑在从远端镜像库中拉取镜像时，使用 `if-not-present` 拉取策略。

## 使用 Docker 镜像层缓存

运行 `docker build` 时，`Dockerfile` 中的每个命令都会生成一个镜像层。这些镜像层作为缓存保存，如果没有任何更改，可以重复使用。您可以使用 `--cache-from` 参数指定要用作 `docker build` 命令的缓存源的标签镜像。您可以使用多个 `--cache-from` 参数将多个镜像指定为缓存源，可以加快构建速度并减少传输的数据量。有关详细信息，请参阅[有关 Docker 镜像层缓存的文档](../../../ci/docker/using_docker_build.md#make-docker-in-docker-builds-faster-with-docker-layer-caching)。

## 检查自动化频率

我们经常创建捆绑到容器镜像中的自动化脚本，按特定时间间隔执行常规任务。
在自动化将容器镜像从极狐GitLab 镜像库拉到 JiHuLab.com 之外的服务的情况下，您可以减少间隔频率。

## 升级到极狐GitLab 专业版或旗舰版

JiHuLab.com 数据传输限制是根据订阅级别设置的。如果您需要更高的限制，请考虑升级到专业版或旗舰版。

## 购买额外的数据传输配额

详细了解如何管理您的[数据传输限制](../../../subscriptions/jihulab_com/index.md#purchase-more-storage-and-transfer)。

<!--
## Related issues

- You may want to rebuild your image when the base Docker image is updated. However, the
  [pipeline subscription limit is too low](https://gitlab.com/gitlab-org/gitlab/-/issues/225278)
  to leverage this feature. As a workaround, you can rebuild daily or multiple times per day.
  [GitLab-#225278](https://gitlab.com/gitlab-org/gitlab/-/issues/225278)
  proposes raising the limit to help with this workflow.
-->
