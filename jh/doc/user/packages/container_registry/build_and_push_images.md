---
stage: Package
group: Container Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 构建容器镜像并将其推送到容器镜像库 **(FREE)**

在构建和推送容器镜像之前，您必须通过容器镜像库的[身份验证](authenticate_with_container_registry.md)。

## 使用 Docker 命令

您可以使用 Docker 命令构建容器镜像并将其推送到您的容器镜像库：

1. 使用容器镜像库进行[身份验证](authenticate_with_container_registry.md)。
1. 运行 Docker 命令构建或推送。例如：

   - 构建：

     ```shell
     docker build -t registry.example.com/group/project/image .
     ```

   - 推送：

     ```shell
     docker push registry.example.com/group/project/image
     ```

## 配置您的 `.gitlab-ci.yml` 文件

您可以配置 `.gitlab-ci.yml` 文件来构建容器镜像并将其推送到容器镜像库。

- 如果多个作业需要身份验证，请将身份验证命令放在 `before_script` 中。
- 在构建之前，使用 `docker build --pull` 获取对基础镜像的更改，它需要稍长的时间，但可以确保您的镜像是最新的。
- 在每次 `docker run` 之前，执行 `docker pull` 来获取刚刚构建的镜像。如果您使用多个在本地缓存镜像的 runner，此步骤尤为重要。如果您在镜像标签中使用 Git SHA，则每个作业都是独一无二的，您永远不应该拥有陈旧的镜像。但是，如果您在依赖项发生更改后重建特定的提交，仍然可能会出现陈旧的镜像。
- 不要直接构建到 `latest` 标签，因为多个作业可能同时发生。

<a id="use-gitlab-cicd"></a>

## 使用极狐GitLab CI/CD

您可以使用[极狐GitLab CI/CD](../../../ci/yaml/index.md) 构建容器镜像并将其推送到容器镜像库。您可以使用 CI/CD 从您创建的容器镜像测试、构建和部署您的项目。

### 使用容器镜像库中的 Docker-in-Docker 容器镜像

您可以将自己的容器镜像用于 Docker-in-Docker。

1. 设置 [Docker-in-Docker](../../../ci/docker/using_docker_build.md#use-docker-in-docker)。
1. 更新 `image` 和 `service` 指向您的镜像库。
1. 添加服务[别名](../../../ci/services/index.md#available-settings-for-services)。

`.gitlab-ci.yml` 示例：

```yaml
build:
  image: $CI_REGISTRY/group/project/docker:20.10.16
  services:
    - name: $CI_REGISTRY/group/project/docker:20.10.16-dind
      alias: docker
  stage: build
  script:
    - docker build -t my-docker-image .
    - docker run my-docker-image /script/to/run/tests
```

如果忘记设置服务别名，容器镜像会找不到 `dind` 服务，报如下错误：

```plaintext
error during connect: Get http://docker:2376/v1.39/info: dial tcp: lookup docker on 192.168.0.1:53: no such host
```

### 将 Docker-in-Docker 容器镜像与依赖代理结合使用

您可以将自己的容器镜像与依赖代理结合使用。

1. 设置 [Docker-in-Docker](../../../ci/docker/using_docker_build.md#use-docker-in-docker)。
1. 更新 `image` 和 `service` 指向您的镜像库。
1. 添加服务[别名](../../../ci/services/index.md#available-settings-for-services)。

`.gitlab-ci.yml` 示例：

```yaml
build:
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:20.10.16
  services:
    - name: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/docker:18.09.7-dind
      alias: docker
  stage: build
  script:
    - docker build -t my-docker-image .
    - docker run my-docker-image /script/to/run/tests
```

如果忘记设置服务别名，容器镜像会找不到 `dind` 服务，报如下错误：

```plaintext
error during connect: Get http://docker:2376/v1.39/info: dial tcp: lookup docker on 192.168.0.1:53: no such host
```

## 使用极狐GitLab CI/CD 的容器镜像库示例

如果您在 runner 上使用 Docker-in-Docker，`.gitlab-ci.yml` 文件示例如下：

```yaml
build:
  image: docker:20.10.16
  stage: build
  services:
    - docker:20.10.16-dind
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY/group/project/image:latest .
    - docker push $CI_REGISTRY/group/project/image:latest
```

您可以在 `.gitlab-ci.yml` 文件中使用 [CI/CD 变量](../../../ci/variables/index.md)。例如：

```yaml
build:
  image: docker:20.10.16
  stage: build
  services:
    - docker:20.10.16-dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
```

在此示例中，`$CI_REGISTRY_IMAGE` 解析为与此项目关联的镜像库地址。`$CI_COMMIT_REF_NAME` 解析为分支或标签名称，可以包含正斜杠。镜像标签不能包含正斜杠。使用 `$CI_COMMIT_REF_SLUG` 作为镜像标签。您可以声明变量 `$IMAGE_TAG`，结合 `$CI_REGISTRY_IMAGE` 和 `$CI_REGISTRY_IMAGE`，节省 `script` 部分中的一些输入。

此示例将任务拆分为 4 个流水线阶段，包括两个并行运行的测试。`build` 存储在容器镜像库中，供后续阶段使用，在需要时下载容器镜像。对 `main` 的更改也被标记为 `latest` 并使用特定于应用程序的部署脚本进行部署：

```yaml
image: docker:20.10.16
services:
  - docker:20.10.16-dind

stages:
  - build
  - test
  - release
  - deploy

variables:
  # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest

before_script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

build:
  stage: build
  script:
    - docker build --pull -t $CONTAINER_TEST_IMAGE .
    - docker push $CONTAINER_TEST_IMAGE

test1:
  stage: test
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker run $CONTAINER_TEST_IMAGE /script/to/run/tests

test2:
  stage: test
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker run $CONTAINER_TEST_IMAGE /script/to/run/another/test

release-image:
  stage: release
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push $CONTAINER_RELEASE_IMAGE
  only:
    - main

deploy:
  stage: deploy
  script:
    - ./deploy.sh
  only:
    - main
  environment: production
```

NOTE:
此示例明确调用 `docker pull`。如果您更喜欢使用 `image:` 拉取容器镜像，并使用 [Docker](https://docs.gitlab.cn/runner/executors/docker.html) 或 [Kubernetes](https://docs.gitlab.cn/runner/executors/kubernetes.html) 执行器，确保 [`pull_policy`](https://docs.gitlab.cn/runner/executors/docker.html#how-pull-policies-work) 设置为 `always`。
