---
stage: Package
group: Container Registry
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 容器镜像库身份验证 **(FREE)**

WARNING:
在 16.0 及更高版本中，[外部授权](../../admin_area/settings/external_authorization.md)阻止个人访问令牌和部署令牌访问容器镜像库和软件包库，并影响所有使用这些令牌进行访问的用户。如果您想在容器镜像库或软件包库使用个人访问令牌和部署令牌，您可以禁用外部授权。

要进行容器镜像库身份验证，您可以使用：

- [个人访问令牌](../../profile/personal_access_tokens.md)
- [部署令牌](../../project/deploy_tokens/index.md)
- [项目访问令牌](../../project/settings/project_access_tokens.md)
- [群组访问令牌](../../group/settings/group_access_tokens.md)

所有这些身份验证方法都需要最小范围：

- 对于读（拉取）访问，范围为 `read_registry`。
- 对于写（推送）访问，范围为 `write_registry` 和 `read_registry`。

要进行身份验证，请运行 `docker login` 命令。例如：

   ```shell
   docker login registry.example.com -u <username> -p <token>
   ```

## 使用极狐GitLab CI/CD 进行身份验证

要使用 CI/CD 向容器镜像库进行身份验证，您可以使用：

- `CI_REGISTRY_USER` CI/CD 变量。

  此变量对容器镜像库具有读写权限，并且仅对一个作业有效。它的密码也会自动创建并分配给 `CI_REGISTRY_PASSWORD`。

  ```shell
  docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  ```

- [CI 作业令牌](../../../ci/jobs/ci_job_token.md)

  ```shell
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  ```

- [部署令牌](../../project/deploy_tokens/index.md#gitlab-deploy-token)，最小范围：
  - 对于读（拉取）访问，范围为 `read_registry`。
  - 对于写（推送）访问，范围为 `write_registry`。

  ```shell
  docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
  ```

- [个人访问令牌](../../profile/personal_access_tokens.md)，最小范围：
  - 对于读（拉取）访问，范围为 `read_registry`。
  - 对于写（推送）访问，范围为 `write_registry`。

  ```shell
  docker login -u <username> -p <access_token> $CI_REGISTRY
  ```
