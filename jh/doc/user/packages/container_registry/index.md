---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab 容器镜像库 **(FREE)**

您可以使用集成的容器镜像库，来存储每个极狐GitLab 项目的容器镜像。

要为您的极狐GitLab 实例启用容器镜像库，请参阅[管理员文档](../../../administration/packages/container_registry.md)。

NOTE:
如果您从 Docker Hub 拉取容器镜像，您还可以使用 GitLab Dependency Proxy 来避免遇到速率限制并加快流水线速度。

通过将 Docker Container Registry 集成到极狐GitLab 中，每个极狐GitLab 项目都可以拥有自己的空间来存储其 Docker 镜像。

您可以在 <https://docs.docker.com/registry/introduction/> 阅读有关 Docker Registry 的更多信息。

本文档是用户指南。要了解如何为您的极狐GitLab 实例启用容器镜像库，请访问[管理员文档](../../../administration/packages/container_registry.md)。

## 查看 Container Registry

您可以查看项目或群组的 Container Registry。

1. 转到您的项目或群组。
1. 转至 **软件包与镜像库 > 容器镜像库**。

您可以在此页面上搜索、排序、过滤和[删除](delete_container_registry_images.md#use-the-gitlab-ui)容器镜像。您可以通过从浏览器复制 URL 来共享过滤视图。

只有项目或群组的成员才能访问私有项目的 Container Registry。

如果项目是公开的，那么 Container Registry 也是公开的。

### 查看特定镜像的标签

您可以查看与给定容器镜像关联的标签列表：

1. 前往您的项目或小组。
1. 转至 **软件包与镜像库 > 容器镜像库**。
1. 选择您感兴趣的容器镜像。

将打开 Container Registry **标签详情** 页面。您可以查看每个标签的详细信息，例如发布时间、消耗的存储量以及清单和配置摘要。

您可以在此页面上搜索、排序（按标签名称）、过滤和[删除](#delete_container_registry_images.md#use-the-gitlab-ui)标签。 您可以通过从浏览器复制 URL 来共享过滤视图。

## 使用来自 Container Registry 的镜像

要下载并运行托管在 GitLab Container Registry 中的容器镜像：

1. 将链接复制到您的容器镜像：
    - 转到您的项目或群组的 **软件包与镜像库 > 容器镜像库** 并找到您想要的镜像。
    - 在镜像名称旁边，单击 **复制** 按钮。

    ![Container Registry image URL](img/container_registry_hover_path_13_4.png)

1. 将 `docker run` 与镜像链接一起使用：

   ```shell
   docker run [options] registry.example.com/group/project/image [arguments]
   ```

[身份验证](authenticate_with_container_registry.md)需要从私有仓库下载镜像。

有关运行 Docker 容器的更多信息，请访问 [Docker 文档](https://docs.docker.com/engine/userguide/intro/)。

## 镜像命名约定

镜像遵循以下命名约定：

```plaintext
<registry URL>/<namespace>/<project>/<image>
```

例如，如果您的项目是 `gitlab.example.com/mynamespace/myproject`，那么您的镜像必须至少命名为 `gitlab.example.com/mynamespace/myproject/my-app`。

您可以将其他名称附加到镜像名称的末尾，深度最多为三层。

例如，这些都是名为 `myproject` 的项目中镜像的所有有效镜像名称：

```plaintext
registry.example.com/mynamespace/myproject:some-tag
```

```plaintext
registry.example.com/mynamespace/myproject/image:latest
```

```plaintext
registry.example.com/mynamespace/myproject/my/image:rc1
```

## 移动或重命名容器镜像库镜像

推送容器镜像后，不支持移动或重命名现有容器镜像库镜像。容器镜像存储在与镜像库路径匹配的路径中。要使用容器镜像库移动或重命名镜像，您必须删除所有现有的容器镜像。

## 为项目禁用 Container Registry

默认情况下启用容器镜像库。

但是，您可以删除项目的 Container Registry：

1. 转到您项目的 **设置 > 常规** 页面。
1. 展开 **可见性、项目功能、权限** 部分并禁用 **Container Registry**。
1. 单击**保存更改**。

**软件包与镜像库 > 容器镜像库** 条目从项目的侧边栏中删除。

<a id="change-visibility-of-the-container-registry"></a>

## 更改容器镜像库的可见性

> 引入于 14.2 版本。

默认情况下，每个有权访问项目的人都可以看到 Container Registry。
但是，您可以更改项目的 Container Registry 的可见性。

有关此设置授予用户的权限的更多详细信息，请参阅 [Container Registry 可见性权限](#container-registry-visibility-permissions)。

1. 转到您项目的 **设置 > 通用** 页面。
1. 展开**可见性、项目功能、权限**部分。
1. 在 **容器镜像库** 下，从下拉列表中选择一个选项：

   - **具有访问权限的任何人**（默认）：Container Registry 对所有有权访问项目的人可见。如果项目是公开的，那么 Container Registry 也是公开的。如果项目是内部的或私有的，那么 Container Registry 也是内部的或私有的。

   - **仅项目成员**：Container Registry 仅对具有报告者角色或更高角色的项目成员可见。这类似于将 Container Registry 可见性设置为 **具有访问权限的任何人** 的私有项目的行为。

1. 选择 **保存修改**。

<a id="container-registry-visibility-permissions"></a>

## 容器镜像库可见性权限

查看 Container Registry 和拉取镜像的能力由 Container Registry 的可见性权限控制。您可以通过 [UI 上的可见性设置](#change-visibility-of-the-container-registry) 或 API 更改<!--[API](../../../api/container_registry.md#change-容器注册表的可见性-->。其它权限如更新 Container Registry、推送或删除镜像等不受此设置影响。但是，禁用 Container Registry 会禁用所有 Container Registry 操作。

|                      |                       | 匿名<br/>（互联网上的任何人） | Guest | 报告者、开发者、维护者和所有者 |
| -------------------- | --------------------- | --------- | ----- | ------------------------------------------ |
| 具有 Container Registry 可见性的公开项目 <br/> 设置为 **具有访问权限的任何人** (UI) 或 `enabled` (API)   | 查看容器镜像库 <br/> 并拉取镜像 | Yes       | Yes   | Yes      |
| 具有 Container Registry 可见性的公开项目 <br/> 设置为 **仅项目成员** (UI) 或 `private` (API)   | 查看容器镜像库 <br/> 并拉取镜像 | No        | No    | Yes      |
| 具有 Container Registry 可见性的内部项目 <br/> 设置为 **具有访问权限的任何人** (UI) 或 `enabled` (API) | 查看容器镜像库 <br/> 并拉取镜像 | No        | Yes   | Yes      |
| 具有 Container Registry 可见性的内部项目 <br/> 设置为 **仅项目成员** (UI) 或 `private` (API) | 查看容器镜像库 <br/> 并拉取镜像 | No        | No    | Yes      |
| 具有 Container Registry 可见性的私有项目 <br/> 设置为 **具有访问权限的任何人** (UI) 或 `enabled` (API) | 查看容器镜像库 <br/> 并拉取镜像 | No        | No    | Yes      |
| 具有 Container Registry 可见性的私有项目 <br/> 设置为 **仅项目成员** (UI) 或 `private` (API)  | 查看容器镜像库 <br/> 并拉取镜像 | No        | No    | Yes      |
| 容器镜像库设置为 `disabled` 的任何项目 | Container Registry 上的所有操作 | No | No | No |
