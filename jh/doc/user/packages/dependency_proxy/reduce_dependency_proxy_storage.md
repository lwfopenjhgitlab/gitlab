---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 减少 Dependency Proxy 存储 **(FREE)**

没有针对 blob 的自动删除过程。除非您手动删除它们，否则它们将无限期存储。由于这会影响您的[存储使用配额](../../usage_quotas.md)，因此从缓存中清除未使用的项目非常重要。此页面涵盖了几种做法。

## 检查 Dependency Proxy 存储使用

使用量配额页面（**设置 > 使用量配额 > 存储**）显示包的存储使用情况，其中包括 Dependency Proxy，但是尚未显示存储。

<!--
## Use the API to clear the cache

To reclaim disk space used by image blobs that are no longer needed, use the
[Dependency Proxy API](../../../api/dependency_proxy.md)
to clear the entire cache. If you clear the cache, the next time a pipeline runs it must pull an
image or tag from Docker Hub.
-->

<a id="cleanup-policies"></a>

## 清理策略

> 于 15.0 版本，所需的权限从开发者更改为维护者。

### 从极狐GitLab 中启用清理策略

> 引入于 14.6 版本。

您可以从用户界面，为 Dependency Proxy 启用自动生存时间 (TTL) 策略，请导航到您群组的 **设置 > 软件包与镜像库 > 依赖项代理** 并启用该设置，在 90 天后自动从缓存中清除项目。

### 使用 GraphQL 启用清理策略

> 引入于 14.4 版本。

清理策略是一项计划作业，您可以使用它来清除不再使用的缓存镜像，从而释放额外的存储空间。这些策略使用生存时间 (TTL) 逻辑：

- 配置天数。
- 所有多天未拉取的缓存依赖代理文件都被删除。

使用 GraphQL API 启用和配置清理策略：

```graphql
mutation {
  updateDependencyProxyImageTtlGroupPolicy(input:
    {
      groupPath: "<your-full-group-path>",
      enabled: true,
      ttl: 90
    }
  ) {
    dependencyProxyImageTtlPolicy {
      enabled
      ttl
    }
    errors
  }
}
```

<!--
See the [Getting started with GraphQL](../../../api/graphql/getting_started.md)
guide to learn how to make GraphQL queries.
-->

最初启用策略时，默认 TTL 设置为 90 天。
启用后，陈旧的依赖代理文件每天都会排队等待删除。由于处理时间的原因，可能不会立即删除。如果在缓存文件标记为过期后拉取镜像，则忽略过期文件并从外部仓库下载和缓存新文件。
