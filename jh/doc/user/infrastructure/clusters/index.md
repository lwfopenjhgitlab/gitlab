---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Kubernetes 集群 **(FREE)**

要将集群连接到极狐GitLab，请使用[极狐GitLab 代理](../../clusters/agent/index.md)。

## 基于证书的 Kubernetes 集成（已废弃）

WARNING:
在 14.5 版本，基于证书的方法将 Kubernetes 集群连接到极狐GitLab，及其相关的[功能](#deprecated-features)已被废弃。

不推荐使用基于证书的 Kubernetes 与极狐GitLab 的集成，其存在以下问题：

- 存在安全问题，因为它需要极狐GitLab 直接访问 Kubernetes API。
- 配置选项不灵活。
- 集成不稳定。
- 不断报告基于此模型的功能问题。

出于这个原因，我们开始基于新模型构建功能，即[极狐GitLab 代理](../../clusters/agent/index.md)。

<!--
Maintaining both methods in parallel caused a lot of confusion
and significantly increased the complexity to use, develop, maintain, and
document them. For this reason, we decided to deprecate them to focus on the
new model.
-->

基于证书的功能将继续获得支持，基于它构建的功能将继续与受支持的 Kubernetes 版本一起使用。尚未计划从极狐GitLab 中删除这些功能。

<!--
Follow this [epic](https://gitlab.com/groups/gitlab-org/configure/-/epics/8)
for updates.

You can find technical information about why we moved away from cluster certificates into
the GitLab agent model on the [agent's blueprint documentation](../../../architecture/blueprints/gitlab_to_kubernetes_communication/index.md).
-->

<a id="deprecated-features"></a>

## 废弃的功能

<!--
- [Connect an existing cluster through cluster certificates](../../project/clusters/add_existing_cluster.md)
- [Access controls](../../project/clusters/cluster_access.md)
- [GitLab-managed clusters](../../project/clusters/gitlab_managed_clusters.md)
- [Deploy applications through certificate-based connection](../../project/clusters/deploy_to_cluster.md)
- [Cluster Management Project](../../clusters/management_project.md)
- [Cluster integrations](../../clusters/integrations.md)
- [Cluster cost management](../../clusters/cost_management.md)
- [Cluster environments](../../clusters/environments.md)
- [Show Canary Ingress deployments on deploy boards](../../project/canary_deployments.md#show-canary-ingress-deployments-on-deploy-boards-deprecated)
- [Deploy Boards](../../project/deploy_boards.md)
- [Pod logs](../../project/clusters/kubernetes_pod_logs.md)
- [Clusters health](manage/clusters_health.md)
- [Web terminals](../../../administration/integration/terminal.md)
-->

- 通过集群证书连接现有集群
- 访问控制
- 极狐GitLab 托管集群
- 通过基于证书的连接部署应用程序
- 集群管理项目
- 集群集成
- 集群成本管理
- 集群环境
- 在部署看板上显示金丝雀 Ingress 部署
- 部署看板
- Pod 日志
- 集群健康
- Web 终端

### 集群级别

项目级、群组级和实例级集群的概念在新模型中消失了，尽管功能在一定程度上保留了下来。

代理始终在单个极狐GitLab 项目中配置，您可以将集群连接公开给其它项目和群组，以便[从极狐GitLab CI/CD 访问它](../../clusters/agent/ci_cd_workflow.md)。
这样，您将授予这些项目和群组对同一集群的访问权限，类似于群组级集群的用例。
