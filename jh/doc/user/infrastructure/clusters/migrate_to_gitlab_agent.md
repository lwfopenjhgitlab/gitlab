---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 迁移到用于 Kubernetes 的极狐GitLab 代理 **(FREE)**

要将 Kubernetes 集群与极狐GitLab 连接，您可以使用：

- [GitOps 工作流](../../clusters/agent/gitops.md)。
- [极狐GitLab CI/CD 工作流](../../clusters/agent/ci_cd_workflow.md)。
- [基于证书的集成](index.md)。


基于证书的集成废弃于 14.5 版本。

<!--The sunsetting plans are described:

- for [GitLab.com customers](../../../update/deprecations.md#saas-certificate-based-integration-with-kubernetes).
- for [Self-managed customers](../../../update/deprecations.md#self-managed-certificate-based-integration-with-kubernetes).-->

如果您正在使用基于证书的集成，您应该尽快转移到另一个工作流。

作为一般规则，要迁移依赖极狐GitLab CI/CD 的集群，您可以使用 [CI/CD 工作流](../../clusters/agent/ci_cd_workflow.md)。
此工作流使用代理连接到您的集群，并且：

- 不接触互联网。
- 不需要对极狐GitLab 的完全集群管理员访问权限。

NOTE:
基于证书的集成用于流行的极狐GitLab 功能，例如极狐GitLab 托管应用程序、极狐GitLab 托管集群和 Auto DevOps。
某些功能当前仅在使用基于证书的集成时可用。

## 迁移集群应用程序部署

### 从极狐GitLab 托管集群迁移

使用极狐GitLab 托管集群，极狐GitLab 为每个分支创建单独的服务帐户和命名空间，并使用这些资源进行部署。

极狐GitLab 代理使用[模拟](../../clusters/agent/ci_cd_workflow.md#use-impersonation-to-restrict-project-and-group-access)策略，部署到具有受限帐户访问权限的集群：

1. 选择适合您需求的模拟策略。
1. 使用 Kubernetes RBAC 规则管理 Kubernetes 中的模拟账户权限。
1. 使用代理配置文件中的 `access_as` 属性来定义模拟。

### 从 Auto DevOps 迁移

在您的 Auto DevOps 项目中，您可以使用极狐GitLab 代理连接您的 Kubernetes 集群。

1. 在您的集群中[安装代理](../../clusters/agent/install/index.md)。
1. 在极狐GitLab 中，转到您使用 Auto DevOps 的项目。
1. 添加三个变量。在左侧边栏上，选择 **设置 > CI/CD** 并展开 **变量**。
   - 添加一个名为 `KUBE_INGRESS_BASE_DOMAIN` 的键，并将应用程序部署域名作为值。
   - 添加一个名为 `KUBE_CONTEXT` 的键，其值类似于 `path/to/agent/project:agent-name`。选择您确定的环境范围。如果您不确定您的代理的上下文是什么，请编辑您的 `.gitlab-ci.yml` 文件，并添加一个作业来查看可用的上下文：

     ```yaml
      deploy:
       image:
         name: bitnami/kubectl:latest
         entrypoint: [""]
       script:
       - kubectl config get-contexts
      ```

   - 添加一个名为 `KUBE_NAMESPACE` 的键，其值为 Kubernetes 命名空间，用于定位您的部署。设置相同的环境范围。
1. 选择 **添加变量**。
1. 在左侧边栏中，选择 **基础架构 > Kubernetes 集群**。
1. 从基于证书的集群部分，打开服务于相同环境范围的集群。
1. 选择 **详情** 选项卡并禁用集群。
1. 编辑您的 `.gitlab-ci.yml` 文件并确保它使用 Auto DevOps 模板。例如：

   ```yaml
   include:
     template: Auto-DevOps.gitlab-ci.yml

   variables:
     KUBE_INGRESS_BASE_DOMAIN: 74.220.23.215.nip.io
     KUBE_CONTEXT: "gitlab-examples/ops/gitops-demo/k8s-agents:demo-agent"
     KUBE_NAMESPACE: "demo-agent"
   ```

1. 要测试您的流水线，请在左侧边栏中选择 **CI/CD > 流水线**，然后选择 **运行流水线**。

<!--
For an example, [view this project](https://jihulab.com/gitlab-examples/ops/gitops-demo/hello-world-service).
-->

### 迁移一般部署

遵循 [CI/CD 工作流](../../clusters/agent/ci_cd_workflow.md)的流程。

## 从极狐GitLab 托管应用程序迁移

极狐GitLab 托管应用程序（GMA）废弃于 14.0 版本，删除于 15.0 版本。
用于 Kubernetes 的代理不支持它，要从 GMA 迁移到代理，请执行以下步骤：

1. [从极狐GitLab 托管应用程序迁移到集群管理项目](../../clusters/migrating_from_gma_to_project_template.md)。
1. [迁移集群管理项目，使用代理](../../clusters/management_project_template.md)。

## 迁移集群管理项目

请参阅[如何通过极狐GitLab 代理，使用集群管理项目](../../clusters/management_project_template.md)。

## 迁移集群监控功能

用于 Kubernetes 的极狐GitLab 代理尚不支持集群监控功能。
