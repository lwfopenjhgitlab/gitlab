---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用 Terraform 和极狐GitLab 的基础设施即代码 **(FREE)**

要使用极狐GitLab 管理您的基础架构，您可以使用与 Terraform 的集成来定义您可以版本、重用和共享的资源：

- 管理计算、存储和网络资源等低级组件。
- 管理高级组件，如 DNS 条目和 SaaS 功能。
- 整合 GitOps 部署和基础设施即代码 (IaC) 工作流程。
- 使用极狐GitLab 作为 Terraform 状态存储。
- 存储和使用 Terraform 模块，来简化常见和复杂的基础架构模式。

<a id="integrate-your-project-with-terraform"></a>

## 将您的项目与 Terraform 集成

> IaC 扫描引入于 14.6 版本。

与极狐GitLab 和 Terraform 的集成通过极狐GitLab CI/CD 进行。
使用 `include` 属性将 Terraform 模板添加到您的项目并从那里进行自定义。

要开始使用，请选择最适合您需求的模板：

- [最新模板](#latest-terraform-template)
- [稳定模板和高级模板](#stable-and-advanced-terraform-templates)

所有模板：

- 使用[极狐GitLab 管理的 Terraform 状态](terraform_state.md)作为 Terraform 状态存储后端。
- 触发四个流水线阶段：`test`、`validate`、`build` 和 `deploy`。
- 运行 Terraform 命令：`test`、`validate`、`plan` 和 `plan-json`。它还仅在默认分支上运行 `apply`。
- 使用 [IaC 扫描](../../application_security/iac_scanning/index.md#configure-iac-scanning-manually)查找安全问题。

<a id="latest-terraform-template"></a>

### 最新的 Terraform 模板

[最新模板](https://gitlab.cn/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform.latest.gitlab-ci.yml)与最新极狐GitLab 版本兼容。它提供了最新的极狐GitLab 功能，可能包括重大更改。

您可以安全地使用最新的 Terraform 模板：

- 如果您使用 SaaS。
- 如果您使用随每个新极狐GitLab 版本更新的私有化部署实例。

<a id="stable-and-advanced-terraform-templates"></a>

### 稳定和高级的 Terraform 模板

如果您使用较早版本的极狐GitLab，您可能会遇到极狐GitLab 版本和模板版本之间的不兼容错误。在这种情况下，您可以选择使用以下模板之一：

- [稳定模板](https://gitlab.cn/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform.gitlab-ci.yml)，可以在其上构建的骨架。
- [高级模板](https://gitlab.cn/gitlab-cn/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform/Base.gitlab-ci.yml)，可完全自定义您的设置。

NOTE:
在每个极狐GitLab 主要版本（例如，15.0）中，最新的模板会替换旧的模板。此过程可能会引入重大更改。如果需要，您可以[使用旧版本的模板](troubleshooting.md#use-an-older-version-of-the-template)。

### 使用 Terraform 模板

使用 Terraform 模板：

1. 在顶部栏，选择 **主菜单 > 项目** 并找到要与 Terraform 集成的项目。
1. 在左侧边栏上，选择 **仓库 > 文件**。
1. 编辑 `.gitlab-ci.yml` 文件，使用 `include` 属性获取 Terraform 模板：

   ```yaml
   include:
    # To fetch the latest template, use:
     - template: Terraform.latest.gitlab-ci.yml
    # To fetch the stable template, use:
     - template: Terraform/Base.gitlab-ci.yml
    # To fetch the advanced template, use:
     - template: Terraform/Base.latest.gitlab-ci.yml
   ```

1. 按如下所述添加变量：

   ```yaml
   variables:
    TF_STATE_NAME: default
    TF_CACHE_KEY: default
    # If your terraform files are in a subdirectory, set TF_ROOT accordingly. For example:
    # TF_ROOT: terraform/production
   ```

1. 可选。在 `.gitlab-ci.yml` 文件中覆盖您获取的模板中存在的属性以自定义配置。


<!--
## Related topics

- View [the images that contain the `gitlab-terraform` shell script](https://gitlab.com/gitlab-org/terraform-images).
- Use GitLab as a [Terraform module registry](../../packages/terraform_module_registry/index.md).
- To store state files in local storage or in a remote store, use the [GitLab-managed Terraform state](terraform_state.md).
- To collaborate on Terraform code changes and Infrastructure-as-Code workflows, use the
  [Terraform integration in merge requests](mr_integration.md).
- To manage GitLab resources like users, groups, and projects, use the
  [GitLab Terraform provider](https://github.com/gitlabhq/terraform-provider-gitlab). It is released separately from GitLab
  and its documentation is available on [the Terraform docs site](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs).
- [Create a new cluster on Amazon Elastic Kubernetes Service (EKS)](../clusters/connect/new_eks_cluster.md).
- [Create a new cluster on Google Kubernetes Engine (GKE)](../clusters/connect/new_gke_cluster.md).
- [Troubleshoot](troubleshooting.md) issues with GitLab and Terraform.
-->
