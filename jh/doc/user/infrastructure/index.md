---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 基础设施管理 **(FREE)**

随着 DevOps 和 SRE 方法的兴起，基础设施管理变得规范化、可自动化，软件开发最佳实践也在基础设施管理中占据了一席之地。一方面，传统运维人的日常任务发生了变化，与传统的软件开发更加相似。另一方面，软件工程师更有可能控制整个 DevOps 生命周期，包括部署和交付。

极狐GitLab 提供各种功能来加速和简化您的基础设施管理实践。

## 基础设施即代码

极狐GitLab 与 Terraform 深度集成，通过运行基础设施即代码流水线并支持各种流程。Terraform 被认为是云基础设施配置的标准。
各种极狐GitLab 集成可帮助您：

- 无需任何设置即可快速上手。
- 围绕合并请求中的基础架构更改进行协作，就像您可能对代码更改进行协作一样。
- 使用模块库进行扩展。

详细了解极狐GitLab 如何帮助您运行基础设施即代码<!--[基础设施即代码](iac/index.md)-->。

## 集成的 Kubernetes 管理

极狐GitLab 与 Kubernetes 的集成可帮助您安装、配置、管理、部署和排除集群应用程序故障。使用极狐GitLab 代理，您可以连接防火墙后的集群，实时访问 API 端点，为生产和非生产环境执行基于拉取或基于推送的部署，等等。

详细了解[极狐GitLab 代理](../clusters/agent/index.md)。

## 极狐GitLab 中的 Runbooks

Runbooks 是一组记录过程，用于解释如何执行任务，例如启动、停止、调试或对系统进行故障排除。

<!--阅读更多关于[如何在极狐GitLab 中运行可执行 runbooks](../project/clusters/runbooks/index.md)。-->
