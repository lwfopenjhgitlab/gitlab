---
stage: Plan
group: Knowledge
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: index, reference
---

# 富文本编辑器 **(FREE)**

> - 引入于 14.0 版本。
> - 编辑议题描述功能引入于 15.5 版本，[功能标志](../administration/feature_flags.md)为 `content_editor_on_issues`。默认禁用。
> - 针对[讨论功能](discussions/index.md)的支持，以及创建、编辑议题和合并请求的功能引入于 15.11 版本，功能标志与原来相同。
> - 针对史诗功能的支持引入于 16.1 版本，功能标志与原来相同。
> - 功能标志 `content_editor_on_issues` 默认启用于 16.2 版本。

FLAG:
在私有化部署版上，此功能默认可用。要隐藏此功能，需要管理员[禁用功能标志](../administration/feature_flags.md) `content_editor_on_issues`。
在 SaaS 版上，此功能可用。

富文本编辑器是一个“所见即所得”（WYSIWYG）编辑器，因此您可以在描述和注释中使用 [GitLab Flavored Markdown](markdown.md)。

![Rich text editor in GitLab](img/rich_text_editor_01_v16_2.png)

富文本编辑器适用于：

- [Wikis](project/wiki/index.md)
- 议题
- 史诗
- 合并请求
- [设计](project/issues/design_management.md)

编辑器的功能包括：

- 设置文本格式，包括粗体、斜体、块引号、标题和行内代码。
- 设置有序列表、无序列表和检查清单的格式。
- 插入链接、附件、图像、视频和音频。
- 创建和编辑表格结构。
- 插入和格式化代码块支持语法高亮显示。
- 实时预览 Mermaid、PlantUML 和 Kroki 图表。

<!--
To track work on adding the rich text editor to more places across GitLab, see
[epic 7098](https://gitlab.com/groups/gitlab-org/-/epics/7098).
-->

## 切换到富文本编辑器

使用富文本编辑器编辑描述、wiki 页面、添加评论。

要切换到富文本编辑器：在文本框的左下角，选择 **切换到富文本编辑**。

## 切换到纯文本编辑器

如果要在文本框中输入 Markdown 源代码，请返回使用纯文本编辑器。

要切换到纯文本编辑器：在文本框的左下角，选择 **切换到纯文本编辑**。

## 与 GitLab Flavored Markdown 兼容

富文本编辑器与 [GitLab Flavored Markdown](markdown.md) 完全兼容。
这意味着您可以在纯文本和富文本模式之间切换，而不会丢失任何数据。

### 输入规则

富文本编辑器还支持一些输入规则，让您可以像键入 Markdown 一样处理丰富的内容。

支持的输入规则：

| 输入规则语法                                        | 插入内容     |
| --------------------------------------------------------- | -------------------- |
| `# Heading 1` <br>... <br> `###### Heading 6`             | 标题 1 到标题 6 |
| `**bold**` 或 `__bold__`                                  | 加粗字体            |
| `_italics_` 或 `*italics*`                                | 斜体字体      |
| `~~strike~~`                                              | 删除线        |
| `[link](url)`                                             | 超链接            |
| `code`                                                    | 行内代码          |
| <code>&#96;&#96;&#96;rb</code> + <kbd>Enter</kbd> <br> <code>&#96;&#96;&#96;js</code> + <kbd>Enter</kbd> | 代码块           |
| `* List item`, or<br> `- List item`, or<br> `+ List item` | 无序列表      |
| `1. List item`                                            | 有序列表        |
| `<details>`                                               | 可折叠部分  |

## 表格

与原始 Markdown 不同，您可以使用富文本编辑器在表格单元格中，插入块内容段落、列表项、图表（甚至另一个表格！）。

### 插入表格

插入表格：

1. 选择 **插入表格** **{table}**。
1. 从下拉列表中选择新表格的大小。

![Alt text](img/rich_text_editor_02_v16_2.png)

### 编辑表格

在表格单元格内，您可以使用菜单插入或删除行或列。

要打开菜单：在单元格的右上角，选择向下箭头 **{chevron-down}**。

![Alt text](img/rich_text_editor_03_v16_2.png)

### 对多个单元格的操作

合并或拆分多个单元格。

要将选定的单元格合并为一个：

1. 选择多个单元格 - 选择一个单元格并拖动光标。
1. 在单元格的右上角，选择向下箭头 **{chevron-down}** **> 合并 N 个单元格**。

要拆分合并的单元格：在单元格的右上角，选择向下箭头 **{chevron-down}** **> 拆分单元格**。

## 插入图表

插入 [Mermaid](https://mermaidjs.github.io/) 和 [PlantUML](https://plantuml.com/) 图表，并在输入图表代码时，实时预览。

要插入图表：

1. 在文本框的顶部栏中，选择 **{plus}** **更多选项**，然后选择 **Mermaid 图表** 或 **PlantUML 图表**。
1. 输入图表的代码。图表预览将出现在文本框中。

![Mermaid diagrams in rich text editor](img/rich_text_editor_04_v16_2.png)

<!--
## Related topics

- [Keyboard shortcuts](shortcuts.md#rich-text-editor) for rich text editor
- [GitLab Flavored Markdown](markdown.md)
-->
