---
stage: Create
group: IDE
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 工作区 (Beta) **(PREMIUM)**

> - 引入于 15.11 版本，[功能标志](../../administration/feature_flags.md)为 `remote_development_feature_flag`。默认禁用。
> - 在 SaaS 和私有化部署版上启用于 16.0 版本。

FLAG:
在私有化部署版上，此功能默认可用。要隐藏此功能，需要管理员[禁用功能标志](../../administration/feature_flags.md) `remote_development_feature_flag`。在 SaaS 版上，此功能可用。此功能尚未准备好用于生产。

WARNING:
此功能处于 [Beta](../../policy/alpha-beta-support.md#beta) 状态，如有更改，恕不另行通知。

工作区是极狐GitLab 中代码的虚拟沙箱环境。您可以使用工作区为您的极狐GitLab 项目创建和管理隔离的开发环境。这些环境确保不同的项目不会相互干扰。

每个工作区都包含自己的一组依赖项、库和工具，您可以对其进行自定义以满足每个项目的特定需求。工作区使用 AMD64 架构。

## 设置工作区

<a id="prerequisites"></a>

### 先决条件

- 设置适用于 Kubernetes 的极狐GitLab 代理所支持的 Kubernetes 集群。请参阅[支持的 Kubernetes 版本](../clusters/agent/index.md#gitlab-agent-for-kubernetes-supported-cluster-versions)。
- 确保启用 Kubernetes 集群的自动伸缩。
- 在 Kubernetes 集群中，验证是否定义了[默认存储类](https://kubernetes.io/docs/concepts/storage/storage-classes/)，以便可以为每个工作区动态配置卷。
- 在 Kubernetes 集群中，安装您选择的 Ingress 控制器（例如，`ingress-nginx`），并使该控制器可通过域名访问。例如，将 `*.workspaces.example.dev` 和 `workspaces.example.dev` 指向 Ingress 控制器公开的负载均衡器。
- 在 Kubernetes 集群中，[安装 `gitlab-workspaces-proxy`](https://gitlab.com/gitlab-org/remote-development/gitlab-workspaces-proxy#installation-instructions)。
- 在 Kubernetes 集群中，[安装适用于 Kubernetes 的极狐GitLab 代理](../clusters/agent/install/index.md)。
- 使用以下代码片段，为极狐GitLab 代理配置远端开发设置：

   ```yaml
   remote_development:
     enabled: true
     dns_zone: "workspaces.example.dev"
   ```

   根据需要更新 `dns_zone`。

- 在每个要使用此功能的公开项目中，定义一个 [devfile](#devfile)。确保 devfile 中使用的容器镜像支持[任意用户 ID](#arbitrary-user-ids)。

### 创建工作区

要在极狐GitLab 中创建工作区：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在项目的根目录中，创建一个名为 `.devfile.yaml` 的文件。
1. 在左侧边栏中，选择 **工作区**。
1. 在右上角，选择 **新建工作区**。
1. 从 **选择项目** 下拉列表中，选择一个带有 `.devfile.yaml` 文件的项目。您只能为公开项目创建工作区。
1. 从 **选择集群代理** 下拉列表中，选择项目所属群组拥有的集群代理。
1. 在 **自动终止前的时间** 中，输入工作区自动终止前的小时数。此超时是一种安全措施，可防止工作区消耗过多资源或无限期运行。
1. 选择 **创建工作区**。

工作区可能需要几分钟才能启动。要访问工作区，请在 **预览** 下选择工作区链接。
您还可以访问终端并可以安装任何必要的依赖项。

## Devfile

devfile 是一个文件，它通过为极狐GitLab 项目指定必要的工具、语言、运行时和其他组件来定义开发环境。

工作区内置了对开发文件的支持。您可以在极狐GitLab 配置文件中为您的项目指定一个 devfile。devfile 用于根据定义的规范自动配置开发环境。

这样，无论您使用何种机器或平台，您都可以创建一致且可重现的开发环境。

### 相关 schema 属性

极狐GitLab 仅支持 [devfile 2.2.0](https://devfile.io/docs/2.2.0/devfile-schema) 中的 `container` 和 `volume` 组件。
使用 `container` 组件将容器镜像定义为 devfile 工作区的执行环境。
您可以指定基础镜像、依赖项和其他设置。

只有以下属性与 `container` 组件的极狐GitLab 实现相关：

| 属性     | 定义                                                                        |
|----------------| --------------------------------------------------- |
| `image`        | 用于工作区的容器镜像的名称。                             |
| `memoryRequest`| 容器可以使用的最小内存量。                                  |
| `memoryLimit`  | 容器可以使用的最大内存量。                                   |
| `cpuRequest`   | 容器可以使用的最小 CPU 数量。                                      |
| `cpuLimit`     | 容器可以使用的最大 CPU 数量。                                      |
| `env`          | 在容器中使用的环境变量。                                    |
| `endpoints`    | 容器暴露的端口映射。                                       |
| `volumeMounts` | 要挂载在容器中的存储卷。                                         |

### 定义示例

以下是一个 devfile 示例：

```yaml
schemaVersion: 2.2.0
components:
  - name: tooling-container
    attributes:
      gl/inject-editor: true
    container:
      image: registry.gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/debian-bullseye-ruby-3.2-node-18.12:rubygems-3.4-git-2.33-lfs-2.9-yarn-1.22-graphicsmagick-1.3.36-gitlab-workspaces
      env:
        - name: KEY
          value: VALUE
      endpoints:
      - name: http-3000
        targetPort: 3000
```

有关详细信息，请参阅 [devfile 文档](https://devfile.io/docs/2.2.0/devfile-schema)。
有关其他示例，请参阅 [`examples` 项目](https://gitlab.com/gitlab-org/remote-development/examples)。

此容器镜像仅用于演示目的。要使用您自己的容器镜像，请参阅[任意用户 ID](#arbitrary-user-ids)。

## Web IDE

默认情况下，工作区与 Web IDE 捆绑在一起。Web IDE 是唯一可用于工作区的代码编辑器。

Web IDE 由 [VS Code 派生项目](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork)提供支持。有关详细信息，请参阅 [Web IDE](../project/web_ide/index.md)。

## 私有仓库

您不能为私有仓库创建工作区，因为极狐GitLab 不会将任何凭据注入工作区。您只能为具有 devfile 的公开仓库创建工作区。

从工作区，您可以手动克隆任何仓库。

## 集群中的 Pod 交互

工作区在 Kubernetes 集群中作为 Pod 运行。极狐GitLab 不对 Pod 相互交互的方式施加任何限制。

由于此要求，您可能希望将此功能与集群中的其他容器隔离开来。

## 网络访问和工作区授权

限制对 Kubernetes 控制平面的网络访问是客户端的责任，因为极狐GitLab 无法控制 API。

只有工作区创建者可以访问工作区和该工作区中公开的任何端点。工作区创建者仅在使用 OAuth 进行用户身份验证后才有权访问工作区。

## 计算资源和卷存储

当您停止工作区时，该工作区的计算资源将缩减为零。但是，为工作区配置的卷仍然存在。

要删除配置的卷，您必须终止工作区。

## 在适用于 Kubernetes 的极狐GitLab 代理中禁用远端开发

您可以停止适用于 Kubernetes 的极狐GitLab 代理的 `remote_development` 模块与极狐GitLab 通信。要在极狐GitLab 代理配置中禁用远端开发，请设置此属性：

```yaml
remote_development:
  enabled: false
```

如果您已经有正在运行的工作区，管理员必须在 Kubernetes 中手动删除这些工作区。

<a id="arbitrary-user-ids"></a>

## 任意用户 ID

您可以提供自己的容器镜像，它可以作为任何 Linux 用户 ID 运行。极狐GitLab 无法预测容器镜像的 Linux 用户 ID。极狐GitLab 使用 Linux 根群组 ID 权限来创建、更新或删除容器中的文件。CRI-O 是 Kubernetes 使用的容器运行时接口，所有容器的默认组 ID 为 `0`。

如果您的容器镜像不支持任意用户 ID，则无法在工作区中创建、更新或删除文件。要创建支持任意用户 ID 的容器镜像，请参阅 [OpenShift 文档](https://docs.openshift.com/container-platform/4.12/openshift_images/create-images.html#use-uid_create-images)。
