---
stage: Manage
group: Workspace
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 项目和群组可见性 **(FREE)**

极狐GitLab 中的项目可见性包括私有、内部和公开。

## 私有项目和群组

对于私有项目，只有项目成员可以：

- 克隆项目。
- 查看公开访问目录（`/public`）。

具有访客角色的用户无法克隆项目。

私有群组只能包含私有子组。

## 内部项目和群组 **(FREE SELF)**

对于内部项目，**任何经过身份验证的用户**，包括具有访客角色的用户，可以：

- 克隆项目。
- 查看公开访问目录（`/public`）。

[外部用户](admin_area/external_users.md)无法克隆项目。

<!--
NOTE:
From July 2019, the `Internal` visibility setting is disabled for new projects, groups,
and snippets on GitLab.com. Existing projects, groups, and snippets using the `Internal`
visibility setting keep this setting. You can read more about the change in the
[relevant issue](https://gitlab.com/gitlab-org/gitlab/-/issues/12388).-->

## 公开项目和群组

对于公开项目，**未经过身份验证的用户**，包括具有访客角色的用户，可以：

- 克隆项目。
- 查看公开访问目录 (`/public`)。

公开群组可以包括公开、内部或私有子组。

NOTE:
如果管理员限制[**公开**可见性级别](admin_area/settings/visibility_and_access_controls.md#restrict-visibility-levels)，则 `/public` 仅对经过身份验证的用户可见。

<a id="change-project-visibility"></a>

## 更改项目可见性

您可以更改项目的可见性。

先决条件：

- 您必须拥有项目的所有者角色。

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **可见性, 项目功能, 权限**。
1. 将 **项目可见性** 更改为 **私有**、**内部** 或 **公开**。项目的可见性设置必须至少与其父组的可见性一样严格。
1. 选择 **保存更改**。

## 更改群组可见性

您可以更改群组中所有项目的可见性。

先决条件：

- 您必须拥有群组的所有者角色。
- 子组和项目必须已经具有至少与父组的新设置一样严格的可见性设置。例如，如果该群组中的子组或项目是公开的，则不能将群组设置为私有。

1. 在顶部栏上，选择 **主菜单 > 群组** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 通用**。
1. 展开 **名称与可见性**。
1. 在 **可见性级别** 下，选择 **私有**、**内部** 或 **公开**。项目的可见性设置必须至少与其父组的可见性一样严格。
1. 选择 **保存更改**。

<a id="restrict-use-of-public-or-internal-projects"></a>

## 限制使用公开或内部项目 **(FREE SELF)**

管理员可以限制用户在创建项目或代码片段时可以选择的可见性级别。
此设置有助于防止用户意外公开其代码仓库。

有关详细信息，请参阅[限制可见性级别](admin_area/settings/visibility_and_access_controls.md#restrict-visibility-levels)。

<!--有关详细信息，请参阅[受限可见性级别](admin_area/settings/visibility_and_access_controls.md#restrict-visibility-levels)。-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
