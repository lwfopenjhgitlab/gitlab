---
stage: Plan
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 价值流仪表盘（Beta） **(ULTIMATE)**

> - 引入于 15.8 版本，作为内部 [Beta](../../policy/alpha-beta-support.md#beta) 功能，[功能标志](../../administration/feature_flags.md)为 `group_analytics_dashboards_page`。默认禁用。
> - 发布于 15.11 版本，作为 [Beta](../../policy/alpha-beta-support.md#beta) 功能，[功能标志](../../administration/feature_flags.md)为 `group_analytics_dashboards_page`。默认启用。
> - 一般可用于 16.0 版本。删除功能标志 `group_analytics_dashboards_page`。

价值流仪表盘是一个可定制的仪表盘，使决策者能够识别数字化转型改进的趋势和机会。此页面正在继续开发中，我们会在添加更多功能时更新信息。
<!--For more information, see the [Value Stream Management category direction page](https://about.gitlab.com/direction/plan/value_stream_management/).-->

## 初始用例

我们最初的用例侧重于提供比较软件交付指标的能力。
这种比较可以帮助决策者了解项目和团队是否正在改进。

价值流仪表盘的测试版包括以下指标：

- [DORA 指标](dora_metrics.md)
- [价值流分析（VSA）- 工作流指标](../group/value_stream_analytics/index.md)

价值流仪表盘允许您：

- 聚合来自不同 API 的数据记录。
- 跟踪整个组织的软件性能（DORA）和价值流（VSA）。

## DevOps 指标比较

DevOps 指标比较显示一个群组或项目，在本月至今、上个月、前一个月和过去 180 天内的 DORA4 和工作流指标。

此可视化可帮助您获得对多个 DevOps 指标的高级自定义视图，并了解它们是否逐月改进。您可以一目了然地比较群组、项目和团队之间的表现，可帮助您确定价值贡献最大、表现出色或表现不佳的团队和项目。

![DevOps metrics comparison](img/devops_metrics_comparison_v15_8.png)

您还可以向下钻取指标以进行进一步分析。
当您将鼠标悬停在某个指标上时，工具提示会显示该指标的说明以及指向相关文档页面的链接。

## 查看价值流仪表盘

先决条件：

- 要查看某个群组的价值流仪表盘，您必须至少具有该群组的报告者角色。

查看价值流仪表盘：

1. 在顶部栏上，选择 **主菜单**，并且：
   - 对于项目，选择 **项目** 并找到您的项目。
   - 对于群组，选择 **群组** 并找到您的群组。
1. 在左侧边栏中，选择 **分析 > 价值流**。
1. 在 **筛选结果** 文本框下方的 **关键指标** 行中，选择 **价值流仪表盘 / DORA**。
1. 可选。要打开新页面，请将以下路径 `/analytics/dashboards/value_streams_dashboard` 附加到群组 URL（例如，`https://jihulab.com/groups/gitlab-cn/-/analytics/dashboards/value_streams_dashboard`）。

## 自定义仪表盘面板

您可以自定义价值流仪表盘并配置要包含在页面中的子组和项目。

一个视图最多可以显示四个子组或项目。

### 使用查询参数

要显示多个子组和项目，请将它们的路径指定为 URL 参数。

例如，参数 `query=gitlab-org/gitlab-ui,gitlab-org/plan-stage` 显示三个独立的面板，分别用于：

- `gitlab-org` 群组
- `gitlab-ui` 项目
- `gitlab-org/plan-stage` 子组

### 使用 YAML 配置

要更改页面的默认内容，您需要在您选择的项目中创建一个 YAML 配置文件。查询参数仍可用于覆盖 YAML 配置。

首先，您需要设置项目。

先决条件：

- 您必须至少具有该群组的维护者角色。

1. 在顶部栏中，选择 **主菜单 > 群组** 并找到您的群组。
1. 在左侧边栏中，选择 **设置 > 通用**。
1. 滚动到 **分析仪表盘** 并选择 **展开**。
1. 选择您要存储 YAML 配置文件的项目。
1. 选择 **保存更改**。

设置项目后，设置配置文件：

1. 在顶部栏中，选择 **主菜单 > 项目** 并找到您的项目。
1. 在默认分支中，创建配置文件：`.gitlab/analytics/dashboards/value_streams/value_streams.yaml`。
1. 在 `value_streams.yaml` 配置文件中，填写配置选项：

```yaml
# title - Change the title of the Value Streams Dashboard. [optional]
title: 'Custom Dashboard title'

# description - Change the description of the Value Streams Dashboard. [optional]
description: 'Custom description'

# panels - List of panels that contain panel settings.
#   title - Change the title of the panel. [optional]
#   data.namespace - The Group or Project path to use for the chart panel.
panels:
  - title: 'My Custom Project'
    data:
      namespace: group/my-custom-project
  - data:
      namespace: group/another-project
  - title: 'My Custom Group'
    data:
      namespace: group/my-custom-group
  - data:
      namespace: group/another-group
```

  以下示例为 `my-group` 命名空间的面板提供了一个选项配置：

  ```yaml
  panels:
    - data:
        namespace: my-group
  ```

## 仪表盘指标和深入钻取报告

| 指标 | 描述 | 深入钻取报告 | 文档页面 |
| ------ | ----------- | --------------- | ------------------ |
| 部署频率 | 每天平均部署到生产的次数。指标衡量向最终用户交付价值的频率。 | [部署频率选项卡](https://jihulab.com/groups/gitlab-cn/-/analytics/ci_cd?tab=deployment-frequency) | [部署频率](dora_metrics.md#deployment-frequency) |
| 变更的前置时间 | 将提交成功交付到生产环境的时间。该指标反映了 CI/CD 流水线的效率。 | [前置时间选项卡](https://jihulab.com/groups/gitlab-cn/-/analytics/ci_cd?tab=lead-time) | [变更的前置时间](dora_metrics.md#lead-time-for-changes) |
| 恢复服务时间 | 组织从生产故障中恢复所需的时间。 | [恢复服务时间选项卡](https://jihulab.com/groups/gitlab-cn/-/analytics/ci_cd?tab=time-to-restore-service) | [恢复服务时间](dora_metrics.md#time-to-restore-service) |
| 变更失败率 | 在生产中导致事件的部署百分比。 | [变更失败率选项卡](https://jihulab.com/groups/gitlab-cn/-/analytics/ci_cd?tab=change-failure-rate) | [变更失败率](dora_metrics.md#change-failure-rate) |
| 前置时间 | 从议题创建到议题关闭的中位时间。 | [价值流分析](https://jihulab.com/groups/gitlab-cn/-/analytics/value_stream_analytics) | [查看议题的前置时间和周期时间](../group/value_stream_analytics/index.md#key-metrics) |
| 周期时间 | 从最早提交关联议题的合并请求，到该议题关闭的中位时间。 | [VSA 概览](https://jihulab.com/groups/gitlab-cn/-/analytics/value_stream_analytics) | [查看议题的前置时间和周期时间](../group/value_stream_analytics/index.md#key-metrics) |
| 新议题 | 创建的新议题数。 | [议题分析](https://jihulab.com/groups/gitlab-cn/-/issues_analytics) | [项目](issue_analytics.md)和[群组](../../user/group/issues_analytics/index.md)的议题分析 |
| 部署次数 | 部署到生产环境的总数。 | [合并请求分析](https://jihulab.com/gitlab-cn/gitlab/-/analytics/merge_request_analytics) | [合并请求分析](merge_request_analytics.md) |
| 严重漏洞 | 项目或群组中随时间推移的严重漏洞 | [漏洞报告](https://jihulab.com/gitlab-cn/gitlab/-/security/vulnerability_report) | [漏洞报告](../application_security/vulnerability_report/index.md) |
| 高危漏洞 | 项目或群组中随时间推移的高危漏洞 | [漏洞报告](https://jihulab.com/gitlab-cn/gitlab/-/security/vulnerability_report) | [漏洞报告](../application_security/vulnerability_report/index.md) |
