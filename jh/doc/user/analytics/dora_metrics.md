---
stage: Plan
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

<a id="devops-research-and-assessment-dora-key-metrics"></a>

# DevOps 研究和评估 (DORA) 关键指标 **(ULTIMATE)**

> - 引入于 13.7 版本
> - 于 13.10 版本添加对于变更的前置时间的支持。
> - 极狐GitLab 自 15.4 版本开始，将该功能从旗舰版降入专业版。而 GitLab CE 与 EE 版本中，该功能依旧为旗舰版。
> - 极狐GitLab 自 15.8 版本开始，将该功能从专业版调整为旗舰版。

DevOps 研究和评估 (DORA) 团队确定了衡量 DevOps 性能的四个指标。
使用这些指标有助于提高 DevOps 效率并向业务利益相关者传达性能，从而加速业务成果。

DORA 包括四个关键指标，分为 DevOps 的两个核心领域：

- [部署频率](#deployment-frequency)和[变更的前置时间](#lead-time-for-changes)衡量团队速度。
- [变更失败率](#change-failure-rate)和[恢复服务的时间](#time-to-restore-service)衡量稳定性。

对于软件开发领导者而言，跟踪速度和质量指标可确保他们不会为了速度而牺牲质量。

<!--
## DORA Metrics dashboard in Value Stream Analytics

The four DORA metrics are available out-of-the-box in the [Value Stream Analytics (VSA) overview dashboard](../group/value_stream_analytics/index.md#view-dora-metrics-and-key-metrics-for-a-group).
This helps you visualize the engineering work in the context of end-to-end value delivery.

The One DevOps Platform [Value Stream Management](https://gitlab.com/gitlab-org/gitlab/-/value_stream_analytics) provides end-to-end visibility to the entire software delivery lifecycle.
This enables teams and managers to understand all aspects of productivity, quality, and delivery, without the ["toolchain tax"](https://about.gitlab.com/solutions/value-stream-management/).
-->

<a id="deployment-frequency"></a>

## 部署频率

> `all` 和 `monthly` 间隔的频率计算公式修复于 16.0 版本。

部署频率是在特定日期范围内（每小时、每天、每周、每月或每年）成功部署到生产环境的频率。

软件开发领导者可以使用部署频率指标，来了解团队将软件成功部署到生产环境的频率，以及团队响应客户请求或新市场机会的速度。
高部署频率意味着您可以更快地获得反馈并更快地迭代以提供改进和功能。

### 如何计算部署频率

在极狐GitLab 中，部署频率是根据部署的结束时间（`finished_at` 属性），通过特定环境每天的平均部署次数来衡量的。
极狐GitLab 根据特定日期完成的部署次数来计算部署频率。

生产环境或名为 `production/prod` 的环境计算在内，环境必须是生产部署级别的一部分，其部署信息才能显示在图表上。

### 如何提高部署频率

第一步是对团队和项目之间代码发布的节奏进行基准测试。接下来，您应该考虑：

- 添加更多自动化测试。
- 添加更多自动化代码验证。
- 将更改分解为更小的迭代。

<a id="lead-time-for-changes"></a>

## 变更的前置时间

变更的前置时间是代码更改投入生产所需的时间。

“变更的前置时间”与“前置时间”不同。在价值流中，“前置时间”度量的是解决议题的工作从提出请求（议题创建）到完成并交付（议题关闭）所需的时间。

对于软件开发领导者来说，变更的前置时间反映了 CI/CD 流水线的效率，并可视化了向客户交付工作的速度。
随着时间的推移，变更的前置时间应该会减少，您的团队的效率应该会提高。更短的变更的前置时间意味着更高效的 CI/CD 流水线。
在极狐GitLab 中，变更的前置时间是度量将合并请求合并到生产（来自 master 分支）环境所需的中位时间。

### 如何计算变更的前置时间

极狐GitLab 根据成功将提交交付到生产环境中的秒数，计算变更的前置时间 - **从**代码提交**到**代码在生产中成功运行，而不将编码时间计算在内。

### 如何缩短变更的前置时间

第一步是在群组和项目之间对 CI/CD 流水线的效率进行基准测试。接下来，您应该考虑：

- 使用价值流分析来识别流程中的瓶颈。
- 将更改分解为更小的迭代。
- 添加更多自动化。

<a id="time-to-restore-service"></a>

## 恢复服务时间

恢复服务时间是组织从生产故障中恢复所需的时间。

对于软件开发领导者，恢复服务时间反映了组织从生产故障中恢复所需的时间。
恢复服务时间越短，意味着组织可以承担创新功能的风险，推动竞争优势并提高业务成果。

### 如何计算恢复服务时间

在极狐GitLab 中，恢复服务的时间衡量为生产环境中事件开放的中位时间。
极狐GitLab 计算给定时间段内事件在生产环境中打开的秒数。这假设：

- [极狐GitLab 事件](../../operations/incident_management/incidents.md) 被跟踪。
- 所有事件都与生产环境有关。
- 事件和部署具有严格的一对一关系。一个事件只与一个生产部署相关，任何生产部署都只与一个事件相关。

### 如何缩短恢复服务时间

第一步是对团队响应进行基准测试，并从团队和项目的服务中断中恢复。接下来，您应该考虑：

- 提高生产环境的可观察性。
- 改进响应工作流程。

<a id="change-failure-rate"></a>

## 变更失败率

变更失败率是变更导致生产失败的频率。

软件开发领导者可以使用变更失败率指标来深入了解所交付代码的质量。
高变更失败率可能表明部署过程效率低下或自动化测试覆盖率不足。

### 如何计算变更失败率

在极狐GitLab 中，变更失败率为在特定时间段内导致生产事件的部署所占百分比。
极狐GitLab 通过用事件数除以部署到生产环境的数量来计算。需要满足以下条件：

- [极狐GitLab 事件](../../operations/incident_management/incidents.md)被跟踪。
- 所有事件都与生产环境有关。
- 事件和部署具有严格的一对一关系。一个事件只与一个生产部署相关，任何生产部署都只与一个事件相关。

### 如何降低变更失败率

第一步是对团队和项目之间的质量和稳定性进行基准测试。

要改进此指标，您应该考虑：

- 在稳定性和吞吐量（部署频率和变更准备时间）之间找到正确的平衡，而不是为了速度而牺牲质量。
- 提高代码审核流程的效率。
- 添加更多自动化测试。

## 极狐GitLab 中的 DORA 指标

DORA 指标显示在以下图表中：

- [价值流仪表盘](value_streams_dashboard.md)，可帮助您识别趋势、模式和改进机会。
- [CI/CD 分析图表](ci_cd_analytics.md)，显示流水线成功率和持续时间，以及 DORA 指标随时间变化的历史。
- [群组](../group/insights/index.md)和[项目](value_stream_analytics.md)的洞见报告，您还可以在其中使用 [DORA 查询参数](../../user/project/insights/index.md#dora-query-parameters)创建自定义图表。

下表概述了 DORA 指标在不同图表中的数据聚合。

| 指标名称 | 度量值 | [价值流仪表盘](value_streams_dashboard.md) | [CI/CD 分析图表](ci_cd_analytics.md)中的数据聚合 | [自定义洞见报告](../../user/project/insights/index.md#dora-query-parameters)中的数据聚合 |
|---------------------------|-------------------|-----------------------------------------------------|------------------------|----------|
| 部署频率 | 成功部署的数量 | 每月日均 | 每日平均 | `day`（默认）或 `month` |
| 变更的前置时间 | 将提交成功交付到生产环境的秒数 | 每月按日计算的中位数 | 中位时间 |  `day`（默认）或 `month` |
| 恢复服务时间 | 事件处于开放状态的秒数           | 每月按日计算的中位数 | 每日中位数 | `day`（默认）或 `month` |
| 变更失败率 | 在生产中导致事件的部署所占百分比 |每月按日计算的中位数 | 部署失败的百分比 | `day`（默认）或 `month` |

## 获取 DORA 指标数据

要获取 DORA 数据，请使用 [GraphQL](../../api/graphql/reference/index.md) 或 [REST](../../api/dora/metrics.md) API。

### 在不使用极狐GitLab CI/CD 流水线的情况下测量 DORA 指标

部署频率是根据部署记录计算的，该记录是为基于推送的部署创建的。
这些部署记录不是为基于拉取的部署创建的，例如当容器镜像通过代理连接到极狐GitLab 的情况。

要在这些情况下跟踪 DORA 指标，您可以使用部署 API [创建部署记录](../../api/deployments.md#create-a-deployment)。<!--另请参阅[跟踪外部部署工具的部署](../../ci/environments/external_deployment_tools.md)的文档页面。-->

### 使用 Jira 测量 DORA 指标

- 部署频率和变更的前置时间是根据极狐GitLab CI/CD 和合并请求 (MR) 计算的，不需要 Jira 数据。
- 恢复服务时间和变更失败率需要极狐GitLab 事件进行计算。有关详细信息，请参阅[测量 DORA 恢复服务时间和使用外部事件更改故障率](#measure-dora-time-to-restore-service-and-change-failure-rate-with-external-incidents)的文档。

<a id="measure-dora-time-to-restore-service-and-change-failure-rate-with-external-incidents"></a>

### 通过外部事件测量 DORA 恢复服务时间和变更失败率

[恢复服务时间](#time-to-restore-service)和[变更失败率](#change-failure-rate)需要[极狐GitLab 事件](../../operations/incident_management/manage_incidents.md)来计算指标。

对于 PagerDuty，您可以设置一个 [Webhook 来自动为每个 PagerDuty 事件创建一个极狐GitLab 事件](../../operations/incident_management/manage_incidents.md#using-the-pagerduty-webhook)。
此配置要求您在 PagerDuty 和极狐GitLab 中进行更改。

对于其他事件管理工具，您可以设置 [HTTP 集成](../../operations/incident_management/integrations.md#http-endpoints)，并自动使用它：

1. [在触发警报时创建事件](../../operations/incident_management/manage_incidents.md#automatically-when-an-alert-is-triggered)。
1. [通过恢复警报关闭事件](../../operations/incident_management/manage_incidents.md#automatically-close-incidents-via-recovery-alerts)。

## 极狐GitLab 中支持的指标

下表显示了受支持的指标、它们在哪个级别上受支持以及引入于哪个版本（API 和 UI）：

| 指标                   | 级别               | API 版本                        | 图表（UI）版本                    | 备注  |
|---------------------------|---------------------|--------------------------------------|---------------------------------------|-----------|
| `deployment_frequency`    | 项目级别      | 13.7+   | 14.8+ | <!--The [old API endpoint](../../api/dora4_project_analytics.md) was [deprecated](https://gitlab.com/gitlab-org/gitlab/-/issues/323713) in 13.10.--> |
| `deployment_frequency`    | 群组级别         | 13.10+  | 13.12+                    | |
| `lead_time_for_changes`   | 项目级别       | 13.10+  | 13.11+           | 以秒为单位。聚合方法为中位数。 |
| `lead_time_for_changes`   | 群组级别        |  13.10+ | 14.0+               | 以秒为单位。聚合方法为中位数。 |
| `change_failure_rate`     | 项目/群组级别  |  14.9+                     | 15.1+                       | 以天为单位。聚合方法是中位数。 |
| `time_to_restore_service` | 项目/群组级别 |  14.10+                     | 15.2+                       | 部署百分比。 |
