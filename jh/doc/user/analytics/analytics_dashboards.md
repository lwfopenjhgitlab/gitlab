---
stage: Analyze
group: Product Analytics
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# 分析仪表盘（实验） **(ULTIMATE)**

> 引入于 15.9 版本，作为[实验](../../policy/experiment-beta-support.md#experiment)功能，[功能标志](../../administration/feature_flags.md)为 `combined_analytics_dashboards`。默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其对每个项目或整个实例可用，需要管理员[启用功能标志](../../administration/feature_flags.md) `combined_analytics_dashboards`。
在 SaaS 版上，此功能不可用。此功能尚未准备好用于生产。

## 仪表盘

每个项目可以拥有无限数量的仪表盘，仅受实例[仓库大小限制](../project/repository/reducing_the_repo_size_using_git.md#storage-limits)。
这些仪表盘是使用极狐GitLab YAML 架构定义的，并存储在项目仓库的 `.gitlab/analytics/dashboards/` 目录中。
仪表盘文件名和包含目录应该相同，例如 `my_dashboard/my_dashboard.yaml`。有关更多信息，请参阅[定义仪表盘](#define-a-dashboard)。每个仪表盘都可以引用一个或多个[可视化](#define-a-chart-visualization)分析，这些可视化分析在仪表盘之间共享。

项目维护人员可以使用[代码所有者](../project/codeowners/index.md)和[批准规则](../project/merge_requests/approvals/rules.md)等功能，对仪表盘更改强制实施批准规则。
您的仪表盘文件与项目代码的其余部分一起在源代码管理中进行版本控制。

### 数据源

数据源是与数据库或数据集合的连接，仪表盘过滤器和可视化分析可以使用它来查询和检索结果。

为分析仪表盘配置以下数据源：

- [产品分析](../product_analytics/index.md)

### 查看项目仪表盘

要查看项目的仪表盘列表：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab**（**{search}**）来查找您的项目。
1. 选择 **分析 > 仪表盘**。
1. 从可用仪表盘列表中，选择您要查看的仪表盘。

<a id="define-a-dashboard"></a>

### 定义仪表盘

定义仪表盘：

1. 在 `.gitlab/analytics/dashboards/` 中，创建一个与仪表盘名称类似的目录。

   每个仪表盘应该有自己的目录。

1. 在新目录中，创建一个与目录同名的 `.yaml` 文件，例如 `.gitlab/analytics/dashboards/my_dashboard/my_dashboard.yaml`。

   该文件包含仪表盘定义。它必须符合 `ee/app/validators/json_schemas/analytics_dashboard.json` 中定义的 JSON 模式。

1. 可选。要创建新的可视化分析并添加到仪表盘，请参阅[定义图表可视化分析](#define-a-chart-visualization)。

[例如](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/gitlab/analytics/product_analytics/dashboards/audience.yaml)您想创建三个仪表盘和适用于所有仪表盘的一种可视化分析（折线图），文件结构为：

```plaintext
.gitlab/analytics/dashboards
├── conversion_funnels
│  └── conversion_funnels.yaml
├── demographic_breakdown
│  └── demographic_breakdown.yaml
├── north_star_metrics
|  └── north_star_metrics.yaml
├── visualizations
│  └── example_line_chart.yaml
```

<a id="define-a-chart-visualization"></a>

### 定义图表可视化分析

您可以定义不同的图表，并向其中一些图表添加可视化选项：

- 折线图，选项在 [ECharts 文档](https://echarts.apache.org/en/option.html)中列出。
- 柱形图，选项在 [ECharts 文档](https://echarts.apache.org/en/option.html)中列出。
- 数据表，具有渲染 `links` 的唯一选项（对象数组，每个对象都具有 `text` 和 `href` 属性来指定 `links` 中使用的维度）。请参阅[示例](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/app/validators/json_schemas/analytics_visualization.json?ref_type=heads#L112)。
- 单一统计数据，唯一可以设置 `decimalPlaces` 的选项（数字，默认值为 0）。

要为仪表盘定义图表：

1. 在 `.gitlab/analytics/dashboards/visualizations/` 目录中，创建一个 `.yaml` 文件。文件名应该描述它定义的可视化分析内容。
1. 在 `.yaml` 文件中，根据 `ee/app/validators/json_schemas/analytics_visualization.json` 中的架构定义可视化配置。

[例如](https://jihulab.com/gitlab-cn/gitlab/-/blob/master/ee/lib/gitlab/analytics/product_analytics/visualizations/events_over_time.yaml)，创建一个说明事件的折线图。随着时间的推移，在 `visualizations` 文件夹中创建一个 `line_chart.yaml` 文件，其中包含以下必填字段：

- 版本
- 类型
- 数据
- 选项

### 更改项目仪表盘的位置

仪表盘通常在检索分析数据的项目中定义。
但是，您也可以为仪表盘创建一个单独的项目。
如果您想要对仪表盘定义实施特定的访问规则或跨多个项目共享仪表盘，建议您更改项目仪表盘的位置。

NOTE:
您只能在位于同一群组的项目之间共享仪表盘。

要更改项目仪表盘的位置：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的项目，或选择 **创建新...**（**{plus}**）和 **新建项目/仓库** 创建用于存储仪表盘文件的项目。
1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 并找到您要使用仪表盘的项目。
1. 选择 **设置 > 通用**。
1. 展开 **分析**。
1. 在 **分析仪表盘** 部分中，选择包含仪表盘文件的项目。
1. 选择 **保存更改**。

### 更改群组仪表盘的位置

<!--
NOTE:
This feature will be connected to group-level dashboards in [issue 411572](https://gitlab.com/gitlab-org/gitlab/-/issues/411572).
-->

如果要使用某个群组的仪表盘，则必须将仪表盘文件存储在属于该群组的项目中。
您可以随时更改群组仪表盘的源项目。

要更改群组仪表盘的位置：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的群组。
1. 选择 **设置 > 通用**。
1. 展开 **分析**。
1. 在 **分析仪表盘** 部分中，选择包含仪表盘文件的项目。
1. 选择 **保存更改**。

## 仪表盘设计器

> 引入于 16.1 版本，[功能标志](../../administration/feature_flags.md)为 `combined_analytics_dashboards_editor`，默认禁用。

FLAG:
在私有化部署版上，此功能默认不可用。要使其对每个项目或整个实例可用，需要管理员[启用功能标志](../../administration/feature_flags.md) `combined_analytics_dashboards_editor`。
在 SaaS 版上，此功能不可用。此功能尚未准备好用于生产。

NOTE:
此功能不能与 `product_analytics_snowplow_support` 功能标志一起使用。

您可以使用仪表盘设计器来：

- 创建自定义仪表盘
- 重命名自定义仪表盘
- 将可视化分析添加到新的和现有的自定义仪表盘
- 调整自定义仪表盘中的面板大小或移动面板

您无法编辑标记为 `By GitLab` 的内置仪表盘。
要编辑这些仪表盘，您应该创建一个使用相同可视化分析的新自定义仪表盘。

### 创建自定义仪表盘

要创建自定义仪表盘：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的项目。
1. 选择 **分析 > 仪表盘**。
1. 选择 **新建仪表板**。
1. 在 **新建仪表板** 输入框中，输入仪表盘的名称。
1. 从右侧的 **添加可视化分析** 列表中，选择要添加到仪表盘的可视化分析。
1. 可选。根据您的喜好拖动或调整所选面板的大小。
1. 选择 **保存**。

### 编辑自定义仪表盘

您可以在仪表盘设计器中，编辑自定义仪表盘的标题并添加可视化分析或调整可视化分析的大小。

要编辑现有的自定义仪表盘：

1. 在左侧边栏的顶部，选择 **搜索极狐GitLab** (**{search}**) 来查找您的项目。
1. 选择 **分析 > 仪表盘**。
1. 从可用仪表盘列表中，选择要编辑的自定义仪表盘（没有 `By GitLab` 标记）。
1. 选择 **编辑**。
1. 可选。更改仪表盘的标题。
1. 可选。从右侧的 **添加可视化分析** 列表中，选择要添加到仪表盘的其他可视化分析。
1. 可选。在仪表盘中，选择一个面板并根据您的喜好拖动或调整其大小。
1. 选择 **保存**。
