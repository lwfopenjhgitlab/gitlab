---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# CI/CD 分析 **(FREE)**

使用 CI/CD 分析页面查看流水线成功率和持续时间，以及 [DORA 指标](index.md#devops-research-and-assessment-dora-key-metrics)的历史记录。

## 流水线成功和持续时间图表

CI/CD 分析显示流水线成功和失败的历史记录，以及每个流水线运行的时间。

通过收集项目的所有可用流水线来收集流水线统计信息，无论状态如何。每一天的可用数据基于流水线的创建时间。

总流水线计算包括子流水线和因无效 YAML 而失败的流水线。<!--要根据其它属性过滤流水线，请使用 [Pipelines API](../../api/pipelines.md#list-project-pipelines)。-->

查看成功的流水线：

![Successful pipelines](img/pipelines_success_chart.png)

查看流水线持续时间历史记录：

![Pipeline duration](img/pipelines_duration_chart.png)

## 查看 CI/CD 分析

查看 CI/CD 分析：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **分析 > CI/CD 分析**。

<a id="view-dora-deployment-frequency-chart"></a>

## 查看部署频率图表 **(ULTIMATE)**

> 引入于 13.8 版本

部署频率图表显示有关“生产”环境的部署频率的信息。环境必须是生产部署级别的一部分，其部署信息才能显示在图表上。

部署频率是 DevOps 团队用于衡量软件交付卓越性的四个 [DORA 指标](index.md#devops-research-and-assessment-dora-key-metrics)之一。

部署频率图表可用于群组和项目。

查看部署频率图表：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **分析 > CI/CD 分析**。
1. 选择 **部署频率** 选项卡。

![Deployment frequency](img/deployment_frequency_charts_v13_12.png)

<a id="view-dora-lead-time-for-changes-chart"></a>

## 查看变更的前置时间图表 **(ULTIMATE)**

> 引入于 13.11 版本。

变更的前置时间图表显示了有关将合并请求部署到生产环境所需的时间的信息。此图表可用于群组和项目。

- 变更的前置时间短，表明部署流程快速、高效。
- 对于未部署合并请求的时间段，图表呈现红色虚线。

变更的前置时间是 DevOps 团队用来衡量软件交付卓越性的四个 [DORA 指标](index.md#devops-research-and-assessment-dora-key-metrics)之一。

要查看变更的前置时间：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **分析 > CI/CD 分析**。
1. 选择 **前置时间** 选项卡。

![Lead time](img/lead_time_chart_v13_11.png)

## 查看恢复服务时间图表 **(ULTIMATE)**

> 引入于 15.1 版本

恢复服务时间图表显示有关事件在生产环境中解决的中位时间的信息。此图表可用于群组和项目。

恢复服务时间是 DevOps 团队用于衡量软件交付卓越性的四个 [DORA 指标](index.md#devops-research-and-assessment-dora-key-metrics)之一。

查看恢复服务时间图表：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **分析 > CI/CD 分析**。
1. 选择 **恢复服务时间** 选项卡。

![Lead time](img/time_to_restore_service_charts_v15_1.png)

## 查看更改失败率图表 **(ULTIMATE)**

> 引入于 15.2 版本。

更改失败率图表显示了有关在生产环境中导致事件的部署百分比的信息。此图表可用于群组和项目。

更改失败率是 DevOps 团队用来衡量软件交付卓越性的四个 [DORA 指标](index.md#devops-research-and-assessment-dora-key-metrics)之一。

查看更改失败率图表：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **分析 > CI/CD 分析**。
1. 选择 **更改失败率** 选项卡。
