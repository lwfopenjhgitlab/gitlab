---
description: "Merge request analytics help you understand the efficiency of your code review process, and the productivity of your team." # Up to ~200 chars long. They will be displayed in Google Search snippets. It may help to write the page intro first, and then reuse it here.
stage: Manage
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 合并请求分析 **(PREMIUM)**

> - 引入于 13.3 版本
> - 于 13.9 版本移动到专业版

使用合并请求分析来查看：

- 您的组织每月合并的合并请求数。
- 合并请求创建和合并之间的平均时间。
- 关于每个合并的合并请求的信息。

您可以使用合并请求分析来识别：

- 效率低或高的月份。
- 合并请求流程的效率和效率。
- 代码评审过程的效率。

## 查看合并请求分析

您必须至少具有报告者角色才能查看合并请求分析。

查看合并请求分析：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **分析 > 合并请求**。

## 查看某个日期范围内的合并请求数

> - 引入于 13.3 版本
> - 过滤引入于 13.4 版本

查看特定日期范围内合并的合并请求数：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **分析 > 合并请求**。
1. 可选。过滤结果：
   1. 选择过滤栏。
   1. 选择一个参数。
   1. 选择一个值或输入文本来优化结果。
   1. 要调整日期范围：
       - 在 **从** 字段中，选择开始日期。
       - 在 **到** 字段中，选择结束日期。

**吞吐量**图表显示了一段时间内关闭的议题，或合并（未关闭）的合并请求。

该表每页最多显示 20 个合并请求，并包含有关每个合并请求的信息。

## 查看合并请求创建和合并之间的平均时间

> 引入于 13.9 版本

使用 **平均合并时间** 中的数字来查看合并请求从创建到合并之间的平均时间。不包括已关闭和未合并的合并请求。

查看**平均合并时间**：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏中，选择 **分析 > 合并请求**。**平均合并时间**数字显示在仪表盘上。
