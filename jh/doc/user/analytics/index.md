---
stage: Manage
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 分析极狐GitLab 使用情况 **(FREE)**

## 实例级分析

实例级分析使跨极狐GitLab 聚合分析成为可能，用户可以在一个地方查看多个项目和群组的信息。

[了解更多关于实例级分析](../admin_area/analytics/index.md)。

## 群组级分析

> - 于 13.9 版本移动到专业版

极狐GitLab 在群组级别提供了几个分析功能。其中一些功能要求您使用专业版或更高版本。

- [应用安全](../application_security/security_dashboard/index.md)
- [贡献](../group/contribution_analytics/index.md)
- [DevOps Adoption](../group/devops_adoption/index.md)
- [洞察](../group/insights/index.md)
- [议题](../group/issues_analytics/index.md)
- [效率](productivity_analytics.md)
- [仓库](../group/repositories_analytics/index.md)
- [价值流](../group/value_stream_analytics/index.md)

## 项目级分析

您可以使用极狐GitLab 在项目级别查看分析。其中一些功能要求您使用专业版或更高版本。

- [应用安全](../application_security/security_dashboard/index.md)
- [CI/CD 和 DORA](ci_cd_analytics.md)
- [代码评审](code_review_analytics.md)
- [洞察](../project/insights/index.md)
- [议题](../group/issues_analytics/index.md)
- [合并请求](merge_request_analytics.md)，使用 `project_merge_request_analytics` 功能标志启用
- [仓库](repository_analytics.md)
- [价值流](value_stream_analytics.md)

## 用户配置分析

以下分析功能可供用户创建个性化视图：

- [应用安全](../application_security/security_dashboard/index.md#security-center)

请务必查看此功能的文档页面，了解对极狐GitLab 产品级别的要求。

<a id="devops-research-and-assessment-dora-key-metrics"></a>

## DevOps 研究和评估 (DORA) 关键指标 **(ULTIMATE)**

> - 引入于 13.7 版本
> - 于 13.10 版本添加对于变更的前置时间的支持。
> - 极狐GitLab 自 15.4 版本开始，将该功能从旗舰版降入专业版。而 GitLab CE 与 EE 版本中，该功能依旧为旗舰版。
> - 极狐GitLab 自 15.8 版本开始，将该功能从专业版调整为旗舰版。

DevOps 研究和评估 (DORA) 团队开发了几个关键指标，您可以将它们用作软件开发团队的绩效指标：

- 部署频率：组织成功发布到生产环境的频率。
- 变更的前置时间：从代码到达生产所需的时间。
- 变更失败率：导致生产失败的部署百分比。
- 恢复服务的时间：组织从生产故障中恢复需要多长时间。

### 部署频率

成功部署到生产环境的相对频率（每小时、每天、每周、每月或每年），衡量了您向最终用户交付价值的频率。更高的部署频率意味着您能够获得反馈并更快地迭代来提供改进和功能。极狐GitLab 测量在特定时间段内部署到生产环境的数量。

部署频率显示在以下几个图表中：

- [群组级价值流分析](../group/value_stream_analytics/index.md)
- [项目级价值流分析](value_stream_analytics.md)
- [CI/CD 分析](ci_cd_analytics.md)

### 变更的前置时间

DORA 变更的前置时间衡量的是成功将功能交付到生产中的时间。
该指标反映了 CI/CD 流水线的效率。

在极狐GitLab 中，变更的前置时间计算合并请求合并到生产环境中所需的中位时间。
我们测量“从代码提交到代码在生产中成功运行”，而不将 “coding 时间”添加到计算中。

随着时间的推移，变更的前置时间应该会减少，而您的团队的绩效应该会提高。

变更的前置时间显示在以下几个图表中：

- [群组级价值流分析](../group/value_stream_analytics/index.md)
- [项目级价值流分析](value_stream_analytics.md)
- [CI/CD 分析](ci_cd_analytics.md)

### 恢复服务时间

组织从生产环境失败中恢复需要多长时间。极狐GitLab 测量在特定时间段内，关闭事件（incidents）所需的平均时间。假设：

- 所有事件都与生产环境相关。
- 事件和部署具有严格的一对一关系（意味着任何事件仅与一个生产部署相关，任何生产部署与不超过一个事件相关）。

恢复服务的时间显示在几个图表中：

- [群组级价值流分析](../group/value_stream_analytics/index.md)
- [项目级价值流分析](value_stream_analytics.md)
- [CI/CD 分析](ci_cd_analytics.md)

要获取恢复服务时间指标，使用 GraphQL 或 REST APIs。

### 变更失败率

导致生产环境失败的部署百分比。极狐GitLab 测量在特定时间段内，事件（incidents）数量除以生产环境的部署数量的值。假设：

- 所有事件都与生产环境有关。
- 事件和部署具有严格的一对一关系（意味着任何事件仅与一个生产部署相关，任何生产部署与不超过一个事件相关）。

要获取变更失败率，使用 GraphQL 或 REST APIs。

### 极狐GitLab 中支持的指标

下表显示了受支持的指标、它们在哪个级别上受支持以及引入于哪个版本（API 和 UI）：

| 指标                   | 级别               | API 版本                        | 图表（UI）版本                    | 备注  |
|---------------------------|---------------------|--------------------------------------|---------------------------------------|-----------|
| `deployment_frequency`    | 项目级别      | 13.7+   | 14.8+ | <!--The [old API endpoint](../../api/dora4_project_analytics.md) was [deprecated](https://gitlab.com/gitlab-org/gitlab/-/issues/323713) in 13.10.--> |
| `deployment_frequency`    | 群组级别         | 13.10+  | 13.12+                    | |
| `lead_time_for_changes`   | 项目级别       | 13.10+  | 13.11+           | 以秒为单位。聚合方法为中位数。 |
| `lead_time_for_changes`   | 群组级别        |  13.10+ | 14.0+               | 以秒为单位。聚合方法为中位数。 |
| `change_failure_rate`     | 项目/群组级别  |  14.9+                     | 15.1+                       | 以天为单位。聚合方法是中位数。 |
| `time_to_restore_service` | 项目/群组级别 |  14.10+                     | 15.2+                       | 部署百分比。 |

## 定义

我们使用以下术语来描述极狐GitLab 分析：

- **周期时间：**仅执行工作的持续时间。周期时间通常与比其更长的前置事件结合显示。极狐GitLab 测量从关联议题的合并请求的最早提交，到该议题关闭的周期时间。周期时间方法低估了前置时间，因为合并请求的创建总是晚于提交时间。极狐GitLab 在[群组级价值流分析](../group/value_stream_analytics/index.md)和[项目级价值流分析](../analytics/value_stream_analytics.md)中展示周期时间。
- **部署：**在特定时间范围内成功部署到生产的总数（跨所有适用项目）。极狐GitLab 在[群组级价值流分析](../group/value_stream_analytics/index.md)和[项目级价值流分析](../analytics/value_stream_analytics.md)中展示部署。
- **前置时间：**您的价值流的持续时间，从开始到结束，不同于变更的前置时间。该时间通常与更短的“周期时间”结合显示。极狐GitLab 测量从议题创建到议题关闭的前置时间。极狐GitLab 在[群组级价值流分析](../group/value_stream_analytics/index.md)中展示前置时间。
- **平均变更等待时间（MTTC）：**从想法和交付之间的持续时间平均值。极狐GitLab 测量从议题创建到议题的最新相关合并请求部署到生产的 MTTC。
- **平均检测时间（MTTD）：**在生产中未检测到错误的持续时间平均值。极狐GitLab 测量从错误部署到议题创建的 MTTD。
- **平均合并时间（MTTM）：**合并请求的平均寿命。极狐GitLab 测量从合并请求创建到合并请求合并的 MTTM（不包括已关闭/未合并的合并请求）。获取更多信息，查看[合并请求分析](merge_request_analytics.md)。
- **平均恢复/修复/解决时间（MTTR）：**生产中未修复错误的持续时间平均值。极狐GitLab 测量从错误部署到修复部署的 MTTR。
- **吞吐量：**一段时间内关闭议题，或合并（未关闭）的合并请求数量。通常在每个 sprint 中测量。极狐GitLab 在[合并请求分析](merge_request_analytics.md)中显示合并请求吞吐量。
- **价值流：**为客户创造价值所遵循的整个工作流程。例如，[DevOps 生命周期](https://about.gitlab.cn/stages-devops-lifecycle/)是从
"规划"开始，到"监控"结束的价值流。极狐GitLab 使用[价值流分析](value_stream_analytics.md)，帮助您跟踪您的价值流。
- **速度：**在一段时间内完成的总的议题负担。负担通常以点数或权重来衡量，通常按每个 sprint。例如，您的速度可能是“每个 sprint 30 点”。极狐GitLab 测量在特定时间段内关闭的议题的总点数或权重。
