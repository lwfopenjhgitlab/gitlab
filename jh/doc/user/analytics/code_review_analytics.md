---
description: "Learn how long your open merge requests have spent in code review, and what distinguishes the longest-running." # Up to ~200 chars long. They will be displayed in Google Search snippets. It may help to write the page intro first, and then reuse it here.
stage: Manage
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---


# 代码评审分析 **(PREMIUM)**

> - 于 13.9 版本移动到专业版

使用代码评审分析，来查看每个合并请求的评审指标并改进您的代码评审流程：

- 大量评论或提交可能表明：
  - 代码太复杂。
  - 作者需要更多培训。
- 较长的评审时间可能表明：
  - 此工作类型推进比其他类型慢。
  - 加快开发周期的机会。
- 更少的评论和批准者可能表明更多的人员配备需求。

代码评审分析显示一个开放的合并请求表，其中至少有一个非作者评论。
评审时间从提交第一个非作者评论开始计算。

## 查看代码评审分析

先决条件：

- 您必须至少具有报告者角色。

查看代码评审分析：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **分析 > 代码评审**。
1. 按里程碑和标记过滤合并请求。
