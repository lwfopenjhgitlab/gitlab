---
stage: Manage
group: Optimize
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 项目的仓库分析 **(FREE)**

使用仓库分析查看有关项目 Git 仓库的信息：

- 仓库中使用的编程语言。
- 过去 3 个月的代码覆盖历史。
- 提交统计数据（上个月）。
- 每月中每天的提交。
- 每个工作日的提交。
- 每天每小时 (UTC) 的提交。

<!--
Repository analytics is part of [GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-foss). It's available to anyone who has permission to clone the repository.
-->

仓库分析需要：

- 一个初始化的 Git 仓库。
- 默认分支中至少有一个提交（默认为 `master`）。

NOTE:
如果默认分支中没有 Git 提交，则菜单项不可见。项目 wiki 中的提交不包括在分析中。

## 查看仓库分析

要查看项目的仓库分析：

1. 在顶部栏上，选择 **主菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **分析 > 仓库**。

## 如何更新仓库分析图表数据

图表中的数据需排队。后台 worker 在默认分支中每次提交后 10 分钟更新图表。
根据极狐GitLab 安装实例的大小，由于后台作业队列大小的变化，数据刷新可能需要更长的时间。
