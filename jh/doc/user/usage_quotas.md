---
type: howto
stage: Fulfillment
group: Utilization
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# 存储使用配额 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/13294) in GitLab 12.0.
> - Moved to GitLab Free.
-->

存储使用情况统计信息可用于项目和命名空间。您可以使用该信息来管理适用配额内的存储使用情况。

统计数据包括：

- 命名空间中跨项目的存储使用。
- 超过存储配额的存储使用。
- 可用的购买的存储空间。

<a id="view-storage-usage"></a>

## 查看存储使用情况

您可以查看项目或命名空间<!--[命名空间](../user/group/#namespaces)-->的存储使用情况。

1. 转到您的项目或命名空间：
    - 对于项目，在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
    - 对于命名空间，请在浏览器的工具栏中输入 URL。
1. 从左侧边栏中，选择 **设置 > 使用量配额**。
1. 选择 **存储** 选项卡。

显示统计信息。选择任何标题以查看详细信息。此页面上的信息每 90 分钟更新一次。

如果您的命名空间显示 `“不适用。”`，请将提交推送到命名空间中的任何项目以重新计算存储。

### 容器镜像库使用 **(FREE SAAS)**

容器镜像库的使用配额功能仅适用于 JiHuLab.com。 <!--This feature requires a
[new version](https://about.gitlab.com/blog/2022/04/12/next-generation-container-registry/)
of the GitLab Container Registry. To learn about the proposed release for self-managed
installations, see [epic 5521](https://gitlab.com/groups/gitlab-org/-/epics/5521).-->

## 存储使用统计

> - 项目级图表引入于 14.4 版本，功能标志为 `project_storage_ui`。默认禁用。
> - 于 14.5 版本支持私有化部署版。
> - 功能标志移除于 14.5 版本。

<!--
> - Enabled on GitLab.com in GitLab 14.4.
-->

以下存储使用统计信息可供所有者使用：

- 已使用的命名空间总存储量：此命名空间中跨项目使用的总存储量。
- 已使用的超额存储总量：已使用的存储总量超过其分配的存储量。
- 已购买的可用存储空间：已购买但尚未使用的总存储空间。

## 管理您的存储使用配额

要管理您的存储，如果您是命名空间所有者，您可以[为命名空间购买更多存储空间](../subscriptions/jihulab_com/index.md#purchase-more-storage-and-transfer)。

根据您的角色，您还可以使用以下方法来管理或减少存储空间：

- [减少软件包库存储](packages/package_registry/reduce_package_registry_storage.md)。
- [减少依赖代理存储](packages/dependency_proxy/reduce_dependency_proxy_storage.md)。
- [减少仓库大小](project/repository/reducing_the_repo_size_using_git.md)。
- [减少容器镜像库存储](packages/container_registry/reduce_container_registry_storage.md)。
- [减少 wiki 仓库大小](../administration/wikis/index.md#reduce-wiki-repository-size)。

## 管理您的传输使用量

根据您的角色，要管理您的传输使用量，您可以[减少容器镜像库数据传输](packages/container_registry/reduce_container_registry_data_transfer.md)。

## 项目存储限制

在 [SaaS 版](https://jihulab.com/)中，项目仓库的免费存储配额如下：

| 级别 | 免费存储配额 | 
| ----------- | -------- |
| 免费版             | 2 GB        | 
| 专业版（试用） | 2 GB        |
| 专业版             | 5 GB        |
| 旗舰版（试用） | 2 GB        |
| 旗舰版             | 5 GB        |

当项目的仓库和 LFS 达到配额时，项目将被设置为只读状态。
锁定后，您不能将更改推送到只读项目。要监控命名空间中每个仓库的大小，包括每个项目的使用情况，请[查看存储使用情况](#view-storage-usage)。要允许项目的仓库和 LFS 超过免费配额，您必须购买额外的存储空间。有关更多详细信息，请参阅[超额存储使用](#excess-storage-usage)。

<a id="excess-storage-usage"></a>

### 超额存储使用

超额存储使用量是项目仓库超出免费存储配额的使用量。如果没有购买的存储可用，则项目将被设置为只读状态。您无法将更改推送到只读项目。
要删除项目的只读状态，您必须为命名空间购买更多存储空间。购买完成后，只读项目会自动恢复为标准状态。购买的可用存储量必须始终大于零。

**使用量配额**页面的**存储**选项卡可能会警告您以下信息：

- Purchased storage available is running low.（购买的可用存储空间不足。）
- Projects that are at risk of becoming read-only if purchased storage available is zero.（如果购买的可用存储空间为 0，项目可能有变为只读的风险。）
- Projects that are read-only because purchased storage available is zero. Read-only projects are marked with an information icon (**{information-o}**) beside their name.（由于购买的可用存储空间为 0，项目处于只读状态。对于只读项目，在其名称旁边显示 (**{information-o}**) 图标。）

<a id="excess-storage-example"></a>

#### 超额存储示例

以下示例描述了命名空间 *Example Company* 的超额存储场景：

| 仓库 | 已使用的存储 | 超额存储 | 配额  | 状态            |
|------------|--------------|----------------|--------|-------------------|
| Red        | 2 GB        | 0 GB           | 2 GB  | 只读 **{lock}** |
| Blue       | 1 GB         | 0 GB           | 2 GB  | 非只读        |
| Green      | 2 GB        | 0 GB           | 2 GB  | 只读 **{lock}** |
| Yellow     | 1 GB         | 0 GB           | 2 GB  | 非只读        |
| **总数** | **6 GB**    | **0 GB**       | -      | -                 |

Red 和 Green 项目变为只读状态，因为它们的仓库存储已达到配额。在此示例中，尚未购买额外的存储空间。

要删除 Red 和 Green 项目的只读状态，需要购买 10 GB 的额外存储空间。

假设 Red 和 Green 项目的仓库增长超过 2 GB 配额，则会扣减购买存储的用量。如下表所述，所有项目保持非只读状态，因为还有 5 GB 购买的存储可用：10 GB（购买的存储）- 5 GB（使用的超额存储）。

| 仓库 | 已使用的存储 | 超额存储 | 配额  | 状态            |
|------------|--------------|----------------|---------|-------------------|
| Red        | 4 GB        | 2 GB           | 2 GB   | 非只读        |
| Blue       | 4 GB        | 2 GB           | 2 GB   | 非只读        |
| Green      | 3 GB        | 1 GB           | 2 GB   | 非只读        |
| Yellow     | 2 GB         | 0 GB           | 2 GB   | 非只读        |
| **Totals** | **13 GB**    | **5 GB**      | -       | -                 |

## 命名空间存储限制

SaaS 版上的命名空间有存储限制。有关详细信息，请参阅我们的[定价页面](https://gitlab.cn/pricing/)。此限制在**使用配额**页面上不可见，但优先于上面的限制。私有化部署实例不受影响。

添加到总命名空间存储的存储类型包括：

- Git 仓库
- Git LFS
- 产物
- 容器镜像库
- 软件包库
- 依赖代理
- Wiki
- 代码片段

如果您的命名空间存储总量超过可用命名空间存储配额，则该命名空间下的所有项目都将变为只读。在删除只读状态之前，您写入新数据的能力将受到限制。<!--For more information, see [Restricted actions](../user/read_only_namespaces.md#restricted-actions).-->

在以下情况，系统通知您即将超出命名空间存储配额：

- 在命令行界面中，当您达到命名空间存储配额的 95% 和 100% 时，在每次 `git push` 操作后都会显示一条通知。
- 在 UI 中，当您达到命名空间存储配额的 75%、95% 和 100% 时，会显示一条通知。
- 当命名空间存储使用率达到 70%、85%、95% 和 100% 时，系统会向具有所有者角色的成员发送电子邮件通知他们。
