# frozen_string_literal: true

require 'openssl'
require 'bundler-checksum'
require 'gitlab/json'

require Rails.root.join('vendor/gems/bundler-checksum/lib/bundler_checksum/command/helper.rb')

# Ref: vendor/gems/bundler-checksum/lib/bundler_checksum/command/init.rb
module BundlerChecksum
  module Command
    module Jh
      extend self

      def execute
        File.write(checksum_file, "#{Gitlab::Json.generate(checksums, array_nl: "\n")}\n")
      end

      def checksums
        logger.info "Initializing JH additional checksum file #{checksum_file}"

        jh_additional_specs.sort_by(&:name).reduce([]) do |checksums, spec|
          next checksums unless spec.source.is_a?(Bundler::Source::Rubygems)

          remote_checksum = ::BundlerChecksum::Command::Helper.remote_checksums_for_gem(spec.name, spec.version.to_s)
          raise "#{spec.name} #{spec.version} not found on Rubygems!" if remote_checksum.empty?

          checksums + remote_checksum.sort_by(&:values)
        end
      end

      private

      def logger
        Logger.new($stdout)
      end

      def checksum_file
        Rails.root.join('jh/Gemfile.checksum.jh')
      end

      def jh_lockfile
        path = Rails.root.join('jh/Gemfile.lock')
        Bundler::LockfileParser.new(Bundler.read_file(path))
      end

      def upstream_lockfile
        path = Rails.root.join('Gemfile.lock')
        Bundler::LockfileParser.new(Bundler.read_file(path))
      end

      def jh_additional_specs
        jh_lockfile.specs - upstream_lockfile.specs
      end
    end
  end
end
