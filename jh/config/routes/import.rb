# frozen_string_literal: true

namespace :import do
  resource :gitee, only: [:create, :new], controller: :gitee do
    post :personal_access_token
    post :reset_access_token
    get :status
    get :realtime_changes
  end
end
