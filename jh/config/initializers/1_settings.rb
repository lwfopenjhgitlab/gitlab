# frozen_string_literal: true

Gitlab.jh do
  Settings.omniauth['cas3'] ||= {}
  Settings.omniauth.cas3['session_duration'] ||= 8.hours
  Settings.omniauth['session_tickets'] ||= {}
  Settings.omniauth.session_tickets['cas3'] = 'ticket'
end
