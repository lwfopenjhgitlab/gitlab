# frozen_string_literal: true

return unless defined? GettextI18nRailsJs::Task

GettextI18nRailsJs::Task.prepend(::Gitlab::Patch::GettextI18nRailsJsTask)
