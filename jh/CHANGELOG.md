## 16.2.3 (2023-08-03)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 16.2.2 (2023-08-02)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Added (1 change)



### Fixed (2 changes)




### Security (17 changes)

## 16.2.1 (2023-07-26)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)

## 16.1.4 (2023-08-04)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 16.1.3 (2023-08-02)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Added (1 change)



### Fixed (2 changes)




### Security (14 changes)

## 16.1.2 (2023-07-06)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (4 changes)






### Security (1 change)

## 16.1.1 (2023-06-30)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (12 changes)

## 16.0.8 (2023-08-02)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)



### Security (13 changes)

## 16.0.7 (2023-07-06)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (1 change)

## 16.0.6 (2023-06-30)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (12 changes)

## 16.0.5 (2023-06-18)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)



### Changed (1 change)

## 16.0.4 (2023-06-09)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)

## 16.0.3 (2023-06-08)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (3 changes)





### Performance (1 change)

## 16.0.2 (2023-06-06)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)



### Changed (1 change)



### Security (16 changes)

## 16.0.1 (2023-05-25)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (1 change)

## 15.11.13 (2023-07-28)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)

## 15.11.12 (2023-07-17)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 15.11.11 (2023-07-06)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (1 change)

## 15.11.10 (2023-06-30)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Changed (1 change)



### Security (10 changes)

## 15.11.9 (2023-06-16)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 15.11.8 (2023-06-08)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (2 changes)




### Security (1 change)



### Performance (1 change)

## 15.11.7 (2023-06-07)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (16 changes)

## 15.11.6 (2023-05-25)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Changed (1 change)

## 15.11.5 (2023-05-20)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (5 changes)

## 15.11.4 (2023-05-17)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (2 changes)




### Changed (1 change)

## 15.11.3 (2023-05-12)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (2 changes)




### Changed (1 change)

## 15.11.2 (2023-05-05)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (2 changes)

## 15.11.1 (2023-05-04)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (2 changes)




### Security (9 changes)

## 15.10.8 (2023-06-06)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)



### Security (15 changes)

## 15.10.7 (2023-05-12)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)

## 15.10.6 (2023-05-05)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (1 change)

## 15.10.5 (2023-05-03)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (9 changes)

## 15.10.4 (2023-04-22)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)

## 15.10.3 (2023-04-16)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (3 changes)





### Changed (1 change)

## 15.10.2 (2023-04-06)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (3 changes)





### Changed (1 change)

## 15.10.1 (2023-03-31)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (2 changes)




### Security (15 changes)

## 15.9.8 (2023-05-12)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 15.9.7 (2023-05-06)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (1 change)

## 15.9.6 (2023-05-03)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (8 changes)

## 15.9.5 (2023-04-22)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)

## 15.9.4 (2023-03-31)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (16 changes)

## 15.9.3 (2023-03-10)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (4 changes)

## 15.9.2 (2023-03-03)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (12 changes)

## 15.9.1 (2023-02-24)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (2 changes)

## 15.8.6 (2023-04-19)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 15.8.5 (2023-03-31)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (16 changes)

## 15.8.4 (2023-03-03)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (12 changes)

## 15.8.3 (2023-02-16)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (3 changes)





### Changed (1 change)

## 15.8.2 (2023-02-15)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 15.8.1 (2023-02-01)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (5 changes)

## 15.7.9 (2023-04-22)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)

## 15.7.8 (2023-03-03)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (12 changes)

## 15.7.7 (2023-02-15)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 15.7.6 (2023-02-01)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (5 changes)

## 15.7.5 (2023-01-18)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 15.7.3 (2023-01-12)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (3 changes)

## 15.7.2 (2023-01-10)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (9 changes)

## 15.7.1 (2023-01-06)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (2 changes)




### Performance (1 change)

## 15.6.8 (2023-02-15)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 15.6.7 (2023-02-01)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (2 changes)




### Security (5 changes)

## 15.6.6 (2023-01-18)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 15.6.4 (2023-01-10)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (9 changes)

## 15.6.3 (2022-12-22)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 15.6.2 (2022-12-06)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Added (1 change)



### Fixed (4 changes)

## 15.6.1 (2022-12-01)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)



### Security (12 changes)

## 15.5.9 (2023-01-18)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 15.5.7 (2023-01-10)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (10 changes)

## 15.5.6 (2022-12-08)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
No changes.

## 15.5.5 (2022-12-01)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (11 changes)

## 15.5.4 (2022-11-12)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (3 changes)

## 15.5.3 (2022-11-08)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)

## 15.5.2 (2022-11-03)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (11 changes)

## 15.5.1 (2022-10-27)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (2 changes)

## 15.4.6 (2022-12-01)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (11 changes)

## 15.4.5 (2022-11-16)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)

## 15.4.4 (2022-11-03)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (12 changes)

## 15.4.3 (2022-10-20)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (4 changes)

## 15.4.2 (2022-10-05)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (1 change)

## 15.4.1 (2022-09-30)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (15 changes)

## 15.3.5 (2022-11-03)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (12 changes)

## 15.3.4 (2022-09-30)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (15 changes)

## 15.3.3 (2022-09-06)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Fixed (5 changes)

## 15.3.2 (2022-08-31)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (17 changes)

## 15.2.5 (2022-09-30)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (16 changes)

## 15.2.4 (2022-08-31)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (18 changes)

## 15.1.6 (2022-08-31)

JH changes only. For others see [CHANGELOG.md](../CHANGELOG.md)
### Security (17 changes)
