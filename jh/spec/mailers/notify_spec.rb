# frozen_string_literal: true

require 'spec_helper'
require 'email_spec'

RSpec.describe Notify do
  include EmailSpec::Helpers
  include EmailSpec::Matchers
  include EmailHelpers
  describe 'rollback visibility level' do
    let_it_be(:project) { create(:project) }

    context 'when an issue content invalid' do
      let(:issue) { create(:issue) }

      subject do
        described_class.rollback_visibility_level_email(project.id, project.owner.id, issue)
      end

      it do
        is_expected.to have_body_text "Administrator"
      end
    end

    context 'when a merge request title or description invalid' do
      let(:merge_request) { create(:merge_request) }

      subject do
        described_class.rollback_visibility_level_email(project.id, project.owner.id, merge_request)
      end

      it do
        is_expected.to have_body_text "Administrator"
      end
    end

    context 'when a note on commit title or description invalid' do
      let(:note) { create(:note_on_commit) }

      subject do
        described_class.rollback_visibility_level_email(project.id, project.owner.id, note)
      end

      it do
        is_expected.to have_body_text "Administrator"
      end
    end

    context 'when a note on issue title or description invalid' do
      let(:note) { create(:note_on_issue) }

      subject do
        described_class.rollback_visibility_level_email(project.id, project.owner.id, note)
      end

      it do
        is_expected.to have_body_text "Administrator"
      end
    end

    context 'when a note on project snippet title or description invalid' do
      let(:note) { create(:note_on_project_snippet) }

      subject do
        described_class.rollback_visibility_level_email(project.id, project.owner.id, note)
      end

      it do
        is_expected.to have_body_text "Administrator"
      end
    end
  end

  describe 'Issue are reassigned' do
    let_it_be(:current_user, reload: true) { create(:user, email: "current@email.com", name: 'www.example.com') }
    let_it_be(:assignee, reload: true) do
      create(:user, email: 'assignee@example.com',
        name: 'John Doe',
        preferred_language: 'zh_CN')
    end

    let_it_be(:previous_assignee) { create(:user, name: 'Previous Assignee') }
    let_it_be(:project) { create(:project, :public) }
    let_it_be(:issue, reload: true) do
      create(:issue, author: current_user,
        assignees: [assignee],
        project: project,
        description: 'My awesome description!')
    end

    context 'when preferred_language is zh_CN' do
      subject { described_class.reassigned_issue_email(assignee.id, issue.id, [previous_assignee.id], current_user.id) }

      it do
        is_expected.to have_body_text "指派人从 <strong>#{previous_assignee.name}</strong> 更改为 <strong>#{assignee.name}"
      end
    end
  end

  describe '.notice_password_is_expiring_email' do
    let_it_be(:expiration_time) { Time.current }
    let_it_be(:user) { create(:user) }

    subject { described_class.notice_password_is_expiring_email(user, expiration_time.to_date) }

    it 'have the content' do
      is_expected.to have_body_text "Your account password will expire on #{expiration_time.to_date}, " \
                                    "it is recommended that you change your password as soon as possible."
      is_expected.to have_body_text "If your password has expired. After you log in, " \
                                    "you must update your password to access your account."
      is_expected.to have_link('Reset password')
    end
  end
end
