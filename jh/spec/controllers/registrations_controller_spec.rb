# frozen_string_literal: true

require 'spec_helper'

RSpec.describe RegistrationsController, feature_category: :user_management do
  include TermsHelper

  before do
    stub_application_setting(require_admin_approval_after_user_signup: false)
    stub_feature_flags(arkose_labs_signup_challenge: false)
  end

  describe '#create' do
    before do
      allow(::Gitlab::ApplicationRateLimiter).to receive(:throttled?).and_return(false)
    end

    let_it_be(:base_user_params) do
      {
        first_name: 'first',
        last_name: 'last',
        username: FFaker::InternetSE.login_user_name,
        email: FFaker::Internet.email,
        password: User.random_password
      }
    end

    let_it_be(:user_params) { { user: base_user_params } }

    let(:session_params) { {} }

    subject { post(:create, params: user_params, session: session_params) }

    context 'when user posts register form' do
      before do
        post :create, params: { new_user: base_user_params }
      end

      context 'when tracking is enabled' do
        before do
          allow(::Gitlab::Tracking).to receive(:enabled?).and_return(true)
        end

        it 'triggers the event' do
          expect(::Gitlab::Tracking).to have_received(:event).exactly(3).times
        end
      end
    end
  end
end
