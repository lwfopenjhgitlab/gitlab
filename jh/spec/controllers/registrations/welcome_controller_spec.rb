# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Registrations::WelcomeController do
  let(:user) { create(:user) }

  context 'when user visits welcome page' do
    before do
      sign_in(user)

      get :show
    end

    context 'with uncompleted welcome step' do
      it_behaves_like 'triggers tracking event'
    end
  end
end
