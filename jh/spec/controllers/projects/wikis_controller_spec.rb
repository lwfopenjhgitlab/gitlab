# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Projects::WikisController do
  include JH::ContentValidationMessagesTestHelper
  describe 'GET #show' do
    render_views

    let_it_be(:user) { create(:user) }
    let_it_be(:project) { create(:project, :public, :repository) }
    let_it_be(:wiki) { create(:project_wiki, project: project) }
    let_it_be(:wiki_page) { create(:wiki_page, wiki: wiki) }

    context "in content validation" do
      before do
        allow(ContentValidation::Setting).to receive(:block_enabled?).and_return(true)
        sign_in(user)
      end

      context "with blocked file blob" do
        let!(:content_blocked_state) do
          create(:content_blocked_state, container: wiki, commit_sha: wiki_page.version.commit.id, path: wiki_page.path)
        end

        it "render blocked message" do
          get :show, params: {
            namespace_id: project.namespace,
            project_id: project,
            id: wiki_page.path
          }

          expect(response.body).to include(illegal_tips_with_appeal_email)
        end
      end
    end
  end
end
