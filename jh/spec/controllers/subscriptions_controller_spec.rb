# frozen_string_literal: true

require 'spec_helper'

RSpec.describe SubscriptionsController do
  let_it_be(:user) { create(:user) }

  describe 'POST #create', :snowplow do
    subject do
      post :create,
        params: params,
        as: :json
    end

    let(:params) do
      {
        setup_for_company: setup_for_company,
        customer: { company: 'My company', country: 'NL' },
        subscription: { plan_id: 'x', quantity: 2, source: 'some_source' }
      }
    end

    let(:setup_for_company) { true }

    context 'with authorized user' do
      let_it_be(:pay_url) { "http://gdk.test:5000/invoices/uuid/payments/new" }
      let_it_be(:service_response) do
        {
          success: true,
          data: {
            success: true,
            invoice_data: {
              invoice: {
                pay_url: pay_url
              }
            }
          }
        }
      end

      let_it_be(:group) { create(:group) }

      before do
        sign_in(user)
        allow_any_instance_of(GitlabSubscriptions::CreateService).to receive(:execute).and_return(service_response)
        allow_any_instance_of(EE::Groups::CreateService).to receive(:execute).and_return(group)
      end

      context 'on successful creation of a subscription' do
        it 'returns the group edit location in JSON format' do
          allow(Gitlab.config.gitlab).to receive(:url).and_return(::Gitlab::Saas.staging_com_url)
          subject

          callback_url = ::Gitlab::Utils.append_path(::Gitlab::Saas.staging_com_url,
            "/-/subscriptions/groups/#{group.path}/edit")
          callback_url = "#{callback_url}?plan_id=x&quantity=2"
          location = "#{pay_url}?redirect_after_success=#{CGI.escape(callback_url)}"
          expect(response.parsed_body).to eq({ "location" => location })
        end
      end
    end
  end
end
