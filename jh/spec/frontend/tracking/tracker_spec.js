import { Tracker } from 'jh/tracking/tracker';
import { posthogMockData } from './mock_data';

describe('Tracker', () => {
  const TEST_CATEGORY = 'root:index';
  const TEST_ACTION = 'test:action';
  const TEST_LABEL = 'test:label';
  let oldGon;
  beforeAll(() => {
    document.body.dataset.page = TEST_CATEGORY;
  });

  beforeEach(() => {
    oldGon = window.gon;
    window.gon = posthogMockData;
    Tracker.flushPendingEvents();
    Tracker.initialized = false;
  });

  afterEach(() => {
    window.gon = oldGon;
  });

  it('should be enabled when posthog is presented', () => {
    window.posthog = {
      init: jest.fn(),
    };

    jest.spyOn(Tracker, 'trackable').mockReturnValue(true);

    expect(Tracker.posthogEnabled()).toBe(true);
  });

  it('should append events to flush list', () => {
    Tracker.event(TEST_CATEGORY, TEST_ACTION, { label: TEST_LABEL });
    expect(Tracker.nonInitializedQueue).toHaveLength(1);
  });

  it('should flush pending events after initialized', () => {
    Tracker.event(TEST_CATEGORY, TEST_ACTION, { label: TEST_LABEL });
    expect(Tracker.nonInitializedQueue).toHaveLength(1);
    Tracker.flushPendingEvents();
    expect(Tracker.nonInitializedQueue).toHaveLength(0);
  });
});
