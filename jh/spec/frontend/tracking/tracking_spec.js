import { initUserTracking, initDefaultTrackers } from 'jh/tracking';
import { posthogMockData } from './mock_data';

describe('tracking', () => {
  const posthogInitSpy = jest.fn();
  const identifySpy = jest.fn();

  beforeAll(() => {
    window.posthog = {};
  });

  function mockPosthog() {
    const oldGon = window.gon;
    posthogInitSpy.mockClear();
    identifySpy.mockClear();
    window.gon = posthogMockData;

    window.posthog = {
      init: posthogInitSpy,
      identify: identifySpy,
    };

    return () => {
      window.posthog = {};
      window.gon = oldGon;
    };
  }

  describe('initUserTracking', () => {
    it('should initialize correctly', () => {
      const unmock = mockPosthog();

      initUserTracking();

      expect(posthogInitSpy).toHaveBeenCalledWith(window.gon.posthog_api_key, {
        api_host: posthogMockData.posthog_api_host,
      });

      unmock();
    });

    it('should not init when posthog is not enabled', () => {
      initUserTracking();
      expect(posthogInitSpy).not.toHaveBeenCalled();
    });
  });

  describe('initDefaultTracking', () => {
    let unmock;
    beforeEach(() => {
      unmock = mockPosthog();
    });

    afterEach(() => {
      unmock();
    });

    it('should init default tracking correctly', () => {
      const currentUserId = 1;
      const currentUserFullName = 'test-name';
      window.gon = {
        ...window.gon,
        current_user_fullname: currentUserFullName,
        current_user_id: currentUserId,
      };
      initDefaultTrackers();

      expect(identifySpy).toHaveBeenCalledWith(currentUserId, {
        PERSON: currentUserFullName,
      });
    });

    it('should not trace user identification at visitor mode', () => {
      initDefaultTrackers();

      expect(identifySpy).not.toHaveBeenCalled();
    });
  });
});
