const stubURL = 'some_url';

export const provide = {
  emailFormAction: stubURL,
  phoneFormAction: stubURL,
};
