import $ from 'jquery';
import preserveUrlFragment from '~/pages/sessions/new/preserve_url_fragment';

const htmlTemplate = `<div id="signin-container">
<div class="clearfix">
  <div class="omniauth-container gl-mt-5 gl-p-5 gl-text-center gl-w-90p gl-ml-auto gl-mr-auto restyle-login-page">
    <label class="gl-font-weight-normal gl-mb-5">
      Sign in with
    </label>
    <div class="gl-display-flex gl-flex-wrap gl-justify-content-center gl-gap-3">
      <form class="gl-mb-3 gl-w-full" method="post" action="http://test.host/users/auth/cas3">
        <button id="oauth-login-cas3" class="btn gl-button btn-default gl-w-full gl-mb-2 js-oauth-login" type="submit">
          <span class="gl-button-text"> Cas3 </span>
        </button>
      </form>
    </div>
    <div class="gl-form-checkbox custom-control custom-checkbox">
      <input type="checkbox" name="remember_me_omniauth" id="remember_me_omniauth" class="custom-control-input">
      <label class="custom-control-label" for="remember_me_omniauth"><span>Remember me </span></label>
    </div>
  </div>
</div>
</div>`;

describe('preserve_url_fragment', () => {
  const setupDOM = () => {
    document.body.appendChild($(htmlTemplate)[0]);
  };

  const findFormAction = (selector) => {
    return $(`.omniauth-container ${selector}`).parent('form').attr('action');
  };

  beforeEach(() => {
    setupDOM();
  });

  afterEach(() => {
    document.body.innerHTML = '';
  });

  it('does not add an empty query parameter to OmniAuth login buttons', () => {
    preserveUrlFragment();

    expect(findFormAction('#oauth-login-cas3')).toBe('http://test.host/users/auth/cas3');
  });

  describe('adds "redirect_fragment" query parameter to OmniAuth login buttons', () => {
    it('when "remember_me" is not present', () => {
      preserveUrlFragment('#L65');

      expect(findFormAction('#oauth-login-cas3')).toBe(
        'http://test.host/users/auth/cas3?redirect_fragment=L65',
      );
    });

    it('when "remember-me" is present', () => {
      $('.js-oauth-login')
        .parent('form')
        .attr('action', (i, href) => `${href}?remember_me=1`);

      preserveUrlFragment('#L65');

      expect(findFormAction('#oauth-login-cas3')).toBe(
        'http://test.host/users/auth/cas3?remember_me=1&redirect_fragment=L65',
      );
    });
  });
});
