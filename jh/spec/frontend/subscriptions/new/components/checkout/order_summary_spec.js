import Vue from 'vue';
import Vuex from 'vuex';
import VueApollo from 'vue-apollo';
import Component from 'jh/subscriptions/new/components/order_summary.vue';
import createStore from 'ee/subscriptions/new/store';
import * as types from 'ee/subscriptions/new/store/mutation_types';
import { mountExtended } from 'helpers/vue_test_utils_helper';
import PromoCodeInput from 'ee/subscriptions/new/components/promo_code_input.vue';
import { createMockClient } from 'helpers/mock_apollo_helper';
import waitForPromises from 'helpers/wait_for_promises';
import { CUSTOMERSDOT_CLIENT } from 'ee/subscriptions/buy_addons_shared/constants';
import invoicePreviewQuery from 'ee/subscriptions/graphql/queries/new_subscription_invoice_preview.customer.query.graphql';
import {
  VALIDATION_ERROR_CODE,
  PROMO_CODE_ERROR_ATTRIBUTE,
  INVALID_PROMO_CODE_ERROR_CODE,
  PROMO_CODE_USER_QUANTITY_ERROR_MESSAGE,
  INVALID_PROMO_CODE_ERROR_MESSAGE,
  PROMO_CODE_SUCCESS_MESSAGE,
} from 'ee/subscriptions/new/constants';
import {
  mockDiscountItem,
  mockInvoicePreviewBronze,
  mockInvoicePreviewUltimate,
  mockInvoicePreviewUltimateWithMultipleUsers,
  mockNamespaces,
  mockInvoicePreviewWithDiscount,
} from 'ee_jest/subscriptions/mock_data';
import { createAlert } from '~/alert';

jest.mock('~/alert');

describe('Order Summary', () => {
  Vue.use(Vuex);
  Vue.use(VueApollo);

  let wrapper;

  const availablePlans = [
    { id: 'firstPlanId', code: 'bronze', price_per_year: 48, name: 'bronze plan' },
    {
      id: 'secondPlanId',
      code: 'silver',
      price_per_year: 228,
      name: 'silver plan',
      eligible_to_use_promo_code: true,
    },
    {
      id: 'thirdPlanId',
      code: 'gold',
      price_per_year: 1188,
      name: 'gold plan',
      eligible_to_use_promo_code: false,
    },
  ];

  const initialData = {
    availablePlans: JSON.stringify(availablePlans),
    planId: 'thirdPlanId',
    namespaceId: null,
    fullName: 'Full Name',
    groupData: mockNamespaces,
  };

  const findPromoCodeInput = () => wrapper.findComponent(PromoCodeInput);

  const discount = () => wrapper.findByTestId('discount').text();
  const totalOriginalPrice = () => wrapper.findByTestId('amount').text();
  const totalOriginalPriceExcludingVat = () => wrapper.findByTestId('total-ex-vat').text();
  const totalPriceToBeCharged = () => wrapper.findByTestId('total-amount').text();
  const perUserPriceInfo = () => wrapper.findByTestId('per-user').text();
  const numberOfUsers = () => wrapper.findByTestId('number-of-users').text();
  const selectedPlan = () => wrapper.findByTestId('selected-plan').text();
  const subscriptionTerm = () => wrapper.findByTestId('dates').text();

  const assertEmptyPriceDetails = () => {
    expect(totalOriginalPrice()).toBe('-');
    expect(totalOriginalPriceExcludingVat()).toBe('-');
    expect(totalPriceToBeCharged()).toBe('-');
  };

  let store;

  const initialiseStore = () => {
    store = createStore(initialData);
  };

  const invoicePreviewQuerySpy = jest.fn().mockResolvedValue(mockInvoicePreviewUltimate);

  const createComponent = async (invoicePreviewSpy = invoicePreviewQuerySpy) => {
    const mockCustomersDotClient = createMockClient([[invoicePreviewQuery, invoicePreviewSpy]]);
    const mockApollo = new VueApollo({
      defaultClient: mockCustomersDotClient,
      clients: {
        [CUSTOMERSDOT_CLIENT]: mockCustomersDotClient,
      },
    });
    wrapper = mountExtended(Component, {
      apolloProvider: mockApollo,
      store,
    });
    await waitForPromises();
  };

  beforeEach(() => {
    initialiseStore();
  });

  afterEach(() => {
    invoicePreviewQuerySpy.mockClear();
    createAlert.mockClear();
  });

  describe('Changing the company name', () => {
    beforeEach(() => {
      return createComponent();
    });

    describe('When purchasing for a single user', () => {
      beforeEach(() => {
        store.commit(types.UPDATE_IS_SETUP_FOR_COMPANY, false);
      });

      it('displays the title with the passed name', () => {
        expect(wrapper.find('h4').text()).toContain("Full Name's GitLab subscription");
      });
    });

    describe('When purchasing for a company or group', () => {
      beforeEach(() => {
        store.commit(types.UPDATE_IS_SETUP_FOR_COMPANY, true);
      });

      describe('Without a group name provided', () => {
        it('displays the title with the default name', () => {
          expect(wrapper.find('h4').text()).toContain("Your organization's GitLab subscription");
        });
      });

      describe('With a group name provided', () => {
        beforeEach(() => {
          store.commit(types.UPDATE_ORGANIZATION_NAME, 'My group');
        });

        it('displays the title with the group name', () => {
          expect(wrapper.find('h4').text()).toContain("My group's GitLab subscription");
        });
      });
    });
  });

  describe('changing the plan', () => {
    describe('with default initial selected plan', () => {
      beforeEach(() => {
        return createComponent();
      });

      it('displays the chosen plan', () => {
        expect(selectedPlan()).toContain('Gold plan');
      });

      it('displays the correct formatted amount price per user', () => {
        expect(perUserPriceInfo()).toBe('$1,188 per user per year');
      });

      it('calls invoice preview API with appropriate params', () => {
        expect(invoicePreviewQuerySpy).toHaveBeenCalledWith({
          namespaceId: null,
          planId: 'thirdPlanId',
          quantity: 1,
        });
      });
    });

    describe('with the selected plan', () => {
      const invoicePreviewSpy = jest.fn().mockResolvedValue(mockInvoicePreviewBronze);

      beforeEach(async () => {
        await createComponent(invoicePreviewSpy);

        store.commit(types.UPDATE_SELECTED_PLAN, 'firstPlanId');
        await waitForPromises();
      });

      it('displays the chosen plan', () => {
        expect(selectedPlan()).toContain('Bronze plan');
      });

      it('displays the correct formatted amount price per user', () => {
        expect(perUserPriceInfo()).toBe('$48 per user per year');
      });

      it('displays the correct formatted total amount', () => {
        expect(totalPriceToBeCharged()).toBe('¥48');
      });

      it('calls invoice preview API with appropriate params', () => {
        expect(invoicePreviewSpy).toHaveBeenCalledWith({
          namespaceId: null,
          planId: 'firstPlanId',
          quantity: 1,
        });
      });
    });
  });

  describe('Changing the number of users', () => {
    describe('with the default of 1 selected user', () => {
      beforeEach(() => {
        return createComponent();
      });
      it('displays the correct number of users', () => {
        expect(numberOfUsers()).toBe('(x1)');
      });

      it('displays the correct formatted amount price per user', () => {
        expect(perUserPriceInfo()).toBe('$1,188 per user per year');
      });

      it('displays the correct formatted total amount', () => {
        expect(totalPriceToBeCharged()).toBe('¥1,188');
      });

      it('calls invoice preview API with appropriate params', () => {
        expect(invoicePreviewQuerySpy).toHaveBeenCalledWith({
          namespaceId: null,
          planId: 'thirdPlanId',
          quantity: 1,
        });
      });
    });

    describe('with 3 selected users', () => {
      const invoicePreviewSpy = jest
        .fn()
        .mockResolvedValue(mockInvoicePreviewUltimateWithMultipleUsers);

      beforeEach(async () => {
        await createComponent(invoicePreviewSpy);
        store.commit(types.UPDATE_NUMBER_OF_USERS, 3);
        await waitForPromises();
      });

      it('displays the correct number of users', () => {
        expect(numberOfUsers()).toBe('(x3)');
      });

      it('displays the correct formatted amount price per user', () => {
        expect(perUserPriceInfo()).toBe('$1,188 per user per year');
      });

      it('displays the correct multiplied formatted amount of the chosen plan', () => {
        expect(totalOriginalPrice()).toBe('¥3,564');
      });

      it('displays the correct formatted total amount', () => {
        expect(totalPriceToBeCharged()).toBe('¥3,564');
      });

      it('calls invoice preview API with appropriate params', () => {
        expect(invoicePreviewSpy).toHaveBeenCalledWith({
          namespaceId: null,
          planId: 'thirdPlanId',
          quantity: 3,
        });
      });
    });

    describe('with no selected users', () => {
      beforeEach(async () => {
        await createComponent();
        store.commit(types.UPDATE_NUMBER_OF_USERS, 0);
        await waitForPromises();
      });

      it('should not display the number of users', () => {
        expect(wrapper.findByTestId('number-of-users').exists()).toBe(false);
      });

      it('displays the correct formatted amount price per user', () => {
        expect(perUserPriceInfo()).toBe('$1,188 per user per year');
      });

      it('does not show price details', () => {
        assertEmptyPriceDetails();
      });
    });

    describe('date range', () => {
      beforeEach(() => {
        return createComponent();
      });

      it('shows the formatted date range from the start date to one year in the future', () => {
        expect(subscriptionTerm()).toBe('2020-07-06 - 2021-07-06');
      });
    });

    describe('with selected group', () => {
      const invoicePreviewSpy = jest.fn().mockResolvedValue(mockInvoicePreviewUltimate);

      beforeEach(async () => {
        await createComponent(invoicePreviewSpy);
        await store.commit(types.UPDATE_SELECTED_GROUP, 132);
        await waitForPromises();
      });

      it('displays the correct multiplied formatted amount of the chosen plan', () => {
        expect(totalOriginalPrice()).toBe('¥1,188');
      });

      it('displays the correct formatted total amount', () => {
        expect(totalPriceToBeCharged()).toBe('¥1,188');
      });
    });
  });

  describe('Error handling', () => {
    const errorMessage = 'I failed!';

    describe('when API has errors in the response', () => {
      it('creates an alert with received error message', async () => {
        const invoicePreviewSpy = jest
          .fn()
          .mockResolvedValue({ data: {}, errors: [{ extensions: { message: errorMessage } }] });
        await createComponent(invoicePreviewSpy);

        expect(createAlert).toHaveBeenCalledWith({
          message: errorMessage,
          captureError: true,
          error: expect.any(Object),
        });
      });

      it('does not show price details', async () => {
        const invoicePreviewSpy = jest
          .fn()
          .mockResolvedValue({ data: {}, errors: [{ extensions: { message: errorMessage } }] });
        await createComponent(invoicePreviewSpy);

        assertEmptyPriceDetails();
      });

      it('does not capture exception on Sentry for validation errors', async () => {
        const invoicePreviewSpy = jest.fn().mockResolvedValue({
          data: {},
          errors: [{ extensions: { message: errorMessage, code: VALIDATION_ERROR_CODE } }],
        });
        await createComponent(invoicePreviewSpy);

        expect(createAlert).toHaveBeenCalledWith({
          message: errorMessage,
          captureError: false,
          error: expect.any(Object),
        });
      });

      it('captures exception on Sentry for non-validation errors', async () => {
        const invoicePreviewSpy = jest
          .fn()
          .mockResolvedValue({ data: {}, errors: [{ extensions: { message: errorMessage } }] });
        await createComponent(invoicePreviewSpy);

        expect(createAlert).toHaveBeenCalledWith({
          message: errorMessage,
          captureError: true,
          error: expect.any(Object),
        });
      });
    });

    describe('when API has errors in unrecognisable format', () => {
      beforeEach(() => {
        const invoicePreviewSpy = jest
          .fn()
          .mockResolvedValue({ data: {}, errors: [{ somethingElse: 'Error' }] });
        return createComponent(invoicePreviewSpy);
      });

      it('creates an alert', () => {
        expect(createAlert).toHaveBeenCalledWith({
          message: 'Something went wrong while loading price details.',
          captureError: true,
          error: expect.any(Object),
        });
      });

      it('does not show price details', () => {
        assertEmptyPriceDetails();
      });
    });

    describe('when there are network errors', () => {
      beforeEach(() => {
        const invoicePreviewSpy = jest.fn().mockRejectedValue(new Error('Error'));
        return createComponent(invoicePreviewSpy);
      });

      it('creates an alert', () => {
        expect(createAlert).toHaveBeenCalledWith({
          message: 'Network Error: Error',
          captureError: true,
          error: expect.any(Object),
        });
      });

      it('does not show price details', () => {
        assertEmptyPriceDetails();
      });
    });
  });

  describe('promo code', () => {
    it('shows promo code input if eligible', async () => {
      await createComponent();
      await store.commit(types.UPDATE_SELECTED_PLAN, 'secondPlanId');

      expect(findPromoCodeInput().exists()).toBe(true);
    });

    it('doesnt show promo code input if not eligible', async () => {
      await createComponent();
      await store.commit(types.UPDATE_SELECTED_PLAN, 'firstPlanId');
      const promoCodeInput = findPromoCodeInput();

      expect(promoCodeInput.exists()).toBe(false);

      await store.commit(types.UPDATE_SELECTED_PLAN, 'thirdPlanId');

      expect(promoCodeInput.exists()).toBe(false);
    });

    describe('when promo code is valid', () => {
      const invoicePreviewSpy = jest.fn().mockResolvedValue(mockInvoicePreviewWithDiscount);
      let promoCodeInput;

      beforeEach(async () => {
        await createComponent(invoicePreviewSpy);
        await store.commit(types.UPDATE_SELECTED_PLAN, 'secondPlanId');
        await store.commit(types.UPDATE_NUMBER_OF_USERS, 3);
        promoCodeInput = findPromoCodeInput();

        promoCodeInput.vm.$emit('apply-promo-code', 'promoCode');

        await waitForPromises();
      });

      it('shows success message for promo code', () => {
        expect(promoCodeInput.props()).toMatchObject({
          errorMessage: '',
          successMessage: PROMO_CODE_SUCCESS_MESSAGE,
          canShowSuccessAlert: true,
        });
      });

      it('shows discount details', () => {
        expect(discount()).toBe(`¥${mockDiscountItem.chargeAmount}`);
      });

      it('calls invoice preview API with appropriate params', () => {
        expect(invoicePreviewSpy).toHaveBeenCalledWith({
          namespaceId: null,
          planId: 'secondPlanId',
          quantity: 3,
          promoCode: 'promoCode',
        });
      });
    });

    describe('when promo code is valid but price is not shown', () => {
      it('does not show success message for promo code', async () => {
        const invoicePreviewSpy = jest.fn().mockResolvedValue(mockInvoicePreviewWithDiscount);
        await createComponent(invoicePreviewSpy);
        await store.commit(types.UPDATE_SELECTED_PLAN, 'secondPlanId');
        await store.commit(types.UPDATE_NUMBER_OF_USERS, 3);
        const promoCodeInput = findPromoCodeInput();
        promoCodeInput.vm.$emit('apply-promo-code', 'promoCode');

        await store.commit(types.UPDATE_NUMBER_OF_USERS, 0);

        expect(promoCodeInput.props()).toMatchObject({
          successMessage: PROMO_CODE_SUCCESS_MESSAGE,
          canShowSuccessAlert: false,
        });
      });
    });

    describe('when promo code is invalid', () => {
      let promoCodeInput;
      beforeEach(async () => {
        const invoicePreviewSpy = jest.fn().mockResolvedValue({
          data: {},
          errors: [
            {
              extensions: {
                message: 'Error',
                code: INVALID_PROMO_CODE_ERROR_CODE,
                attributes: [PROMO_CODE_ERROR_ATTRIBUTE],
              },
            },
          ],
        });
        await createComponent(invoicePreviewSpy);
        await store.commit(types.UPDATE_SELECTED_PLAN, 'secondPlanId');
        promoCodeInput = findPromoCodeInput();

        promoCodeInput.vm.$emit('apply-promo-code', 'promoCode');

        await waitForPromises();
      });

      it('shows error message for promo code', () => {
        expect(promoCodeInput.props('errorMessage')).toBe(INVALID_PROMO_CODE_ERROR_MESSAGE);
      });

      it('does not show price details', () => {
        assertEmptyPriceDetails();
      });
    });

    describe('when promo code is updated after an invalid promo code is applied', () => {
      let promoCodeInput;
      const invoicePreviewSpy = jest.fn().mockResolvedValue({
        data: {},
        errors: [
          {
            extensions: {
              message: 'Error',
              code: INVALID_PROMO_CODE_ERROR_CODE,
              attributes: [PROMO_CODE_ERROR_ATTRIBUTE],
            },
          },
        ],
      });

      beforeEach(async () => {
        await createComponent(invoicePreviewSpy);
        await store.commit(types.UPDATE_SELECTED_PLAN, 'secondPlanId');
        promoCodeInput = findPromoCodeInput();

        promoCodeInput.vm.$emit('apply-promo-code', 'promoCode');

        await waitForPromises();
      });

      it('shows error message for promo code', () => {
        expect(promoCodeInput.props('errorMessage')).toBe(INVALID_PROMO_CODE_ERROR_MESSAGE);
      });

      it('resets promo code value on update', () => {
        promoCodeInput.vm.$emit('promo-code-updated');

        expect(invoicePreviewSpy).toHaveBeenCalledWith({
          namespaceId: null,
          planId: 'secondPlanId',
          quantity: 1,
        });
      });
    });

    it('shows error message when promo code is applied without valid users', async () => {
      await createComponent();
      await store.commit(types.UPDATE_SELECTED_PLAN, 'secondPlanId');
      await store.commit(types.UPDATE_NUMBER_OF_USERS, 0);

      const promoCodeInput = findPromoCodeInput();
      promoCodeInput.vm.$emit('apply-promo-code', 'promoCode');

      await waitForPromises();

      expect(promoCodeInput.props('errorMessage')).toBe(PROMO_CODE_USER_QUANTITY_ERROR_MESSAGE);
    });

    it('resets error message when quantity is specified after promo code is applied without valid users', async () => {
      await createComponent();
      await store.commit(types.UPDATE_SELECTED_PLAN, 'secondPlanId');
      await store.commit(types.UPDATE_NUMBER_OF_USERS, 0);

      const promoCodeInput = findPromoCodeInput();
      promoCodeInput.vm.$emit('apply-promo-code', 'promoCode');

      await waitForPromises();
      await store.commit(types.UPDATE_NUMBER_OF_USERS, 1);

      expect(promoCodeInput.props('errorMessage')).toBe('');
    });
  });
});
