import { shallowMount } from '@vue/test-utils';
import TreeContent from 'jh_else_ce/repository/components/tree_content.vue';
import waitForPromises from 'helpers/wait_for_promises';
import ContentBlocked from 'jh/vue_shared/components/content_blocked.vue';
import { getTreeContentBlockedState } from 'jh/api/appeal_api';

jest.mock('jh/api/appeal_api');

let vm;
let $apollo;

function factory(path, data = () => ({})) {
  $apollo = {
    query: jest.fn().mockReturnValue(Promise.resolve({ data: data() })),
  };

  vm = shallowMount(TreeContent, {
    propsData: {
      path,
    },
    mocks: {
      $apollo,
    },
    provide: {
      refType: 'heads',
    },
  });
}

describe('Repository table component', () => {
  let originalGon;
  const dummyGon = {
    content_validation_enabled: true,
  };

  beforeEach(() => {
    originalGon = window.gon;
    window.gon = { ...dummyGon };
  });

  afterEach(() => {
    window.gon = originalGon;
    getTreeContentBlockedState.mockReset();
  });

  it('render contentBlocked component when blocked by content validation service', async () => {
    factory('/');
    getTreeContentBlockedState.mockReturnValue(
      Promise.resolve({
        data: {
          id: 1,
          project_full_path: '/test/test',
          path: 'test',
        },
      }),
    );

    await waitForPromises();

    expect(vm.findComponent(ContentBlocked).exists()).toBe(true);
  });
});
