import { shallowMountExtended } from 'helpers/vue_test_utils_helper';
import NewProjectTerms from 'jh/projects/new/components/new_project_terms.vue';
import { PRIVATE_VISIBILITY_LEVEL, PUBLIC_VISIBILITY_LEVEL } from 'jh/projects/new/constants';

const { gon } = window;

describe('Other project settings component', () => {
  let wrapper;
  const findTermsComponent = () => wrapper.findByTestId('new-project-terms');

  beforeEach(() => {
    wrapper = shallowMountExtended(NewProjectTerms, {
      propsData: {
        visibilityLevel: PRIVATE_VISIBILITY_LEVEL,
      },
    });
    window.gon = {
      ...gon,
      dot_com: true,
    };
  });

  afterEach(() => {
    window.gon = gon;
  });

  it('should show project terms when choose project visibility level to be public', async () => {
    expect(findTermsComponent().exists()).toBe(false);

    await wrapper.setProps({ visibilityLevel: PUBLIC_VISIBILITY_LEVEL });

    expect(findTermsComponent().exists()).toBe(true);

    await wrapper.setProps({ visibilityLevel: PRIVATE_VISIBILITY_LEVEL });

    expect(findTermsComponent().exists()).toBe(false);
  });
});
