import GroupFolder from '~/groups/components/group_folder.vue';
import GroupItem from 'jh_else_ce/groups/components/group_item.vue';
import { VISIBILITY_LEVEL_PRIVATE_STRING } from '~/visibility_level/constants';
import { mountExtended } from 'helpers/vue_test_utils_helper';
import { mockParentGroupItem } from 'jest/groups/mock_data';

const createComponent = (
  propsData = { group: mockParentGroupItem },
  provide = {
    currentGroupVisibility: VISIBILITY_LEVEL_PRIVATE_STRING,
  },
) => {
  return mountExtended(GroupItem, {
    propsData,
    components: { GroupFolder },
    provide,
  });
};

describe('GroupItemComponent', () => {
  let wrapper;

  describe('computed', () => {
    describe('hasChildren', () => {
      it('should return true if group has not return children count', () => {
        const group = {
          ...mockParentGroupItem,
          isOpen: false,
          childrenCount: undefined,
        };
        wrapper = createComponent({ group });

        expect(wrapper.vm.hasChildren).toBe(true);
        wrapper.destroy();
      });
    });

    describe('rowClass', () => {
      it('should return map of classes based on group details', () => {
        const group = { ...mockParentGroupItem, isOpen: false };
        wrapper = createComponent({ group });

        const classes = [
          'is-open',
          'has-children',
          'has-description',
          'being-removed',
          'no-access',
        ];
        const { rowClass } = wrapper.vm;

        expect(Object.keys(rowClass).length).toBe(classes.length);
        Object.keys(rowClass).forEach((className) => {
          expect(classes.indexOf(className)).toBeGreaterThan(-1);
        });
      });
    });
  });
});
