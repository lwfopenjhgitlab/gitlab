# frozen_string_literal: true

require 'spec_helper'

RSpec.describe MergeRequests::RebaseService, feature_category: :source_code_management do
  include ProjectForksHelper

  let_it_be(:user) { create(:user) }
  let_it_be_with_reload(:merge_request) do
    create(:merge_request,
      source_branch: 'feature_conflict',
      target_branch: 'master',
      rebase_jid: 'fake-rebase-jid')
  end

  let_it_be(:project) { merge_request.project }

  subject(:service) { described_class.new(project: project, current_user: user) }

  before_all do
    project.add_maintainer(user)
  end

  describe '#execute' do
    context 'when skip_ci flag is set' do
      let(:skip_ci) { true }

      subject(:rebase_without_pipeline) { service.execute(merge_request, skip_ci: skip_ci) }

      context 'without external status check rules' do
        it 'writes none of ci skipped status check records' do
          expect { rebase_without_pipeline }
            .to change { merge_request.status_check_responses.count }.by(0)
        end
      end

      context 'with external status check added to project' do
        before do
          2.times do |i|
            create(:external_status_check,
              project: merge_request.project,
              name: "Status check rule #{i}",
              protected_branches: [])
          end
        end

        context 'when feature flag disabled' do
          before do
            stub_feature_flags(rebase_without_pipeline_and_status_check: false)
          end

          it 'does not write skipped status check records' do
            expect { rebase_without_pipeline }
              .to change { merge_request.status_check_responses.count }.by(0)
          end
        end

        context 'when feature flag enabled' do
          before do
            rebase_without_pipeline
          end

          it 'writes the correct number of ci skipped status check records' do
            expect(merge_request.status_check_responses.count)
              .to eq(merge_request.project.external_status_checks.count)
          end

          it 'records the needed ci skipped status check result' do
            status_check_result = merge_request.status_check_responses.last

            expect(status_check_result.sha).to eq(merge_request.reset.rebase_commit_sha)
            expect(status_check_result.status).to eq('skipped')
          end
        end
      end
    end
  end
end
