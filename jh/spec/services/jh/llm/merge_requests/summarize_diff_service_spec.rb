# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::Llm::MergeRequests::SummarizeDiffService, ai_provider: :chat_glm do
  let_it_be(:user) { create(:user) }
  let_it_be(:project) { create(:project, :with_namespace_settings, :repository, :public) }
  let_it_be(:merge_request) { create(:merge_request, source_project: project) }

  describe '#execute' do
    include_context 'with ChatGLM client shared context'

    subject(:service) do
      described_class.new(title: merge_request.title, user: user, diff:
        merge_request.merge_request_diff)
    end

    before_all do
      project.add_developer(user)

      project.namespace.namespace_settings.update_attribute(:experiment_features_enabled, true)
      project.namespace.namespace_settings.update_attribute(:third_party_ai_features_enabled, true)
    end

    before do
      stub_licensed_features(summarize_mr_changes: true)

      stub_env('CHAT_GLM_API_KEY' => api_key)
    end

    context "when #llm_client.chat returns a typical response" do
      before do
        stub_chat_request
      end

      it "returns the content from the ChatGLM response" do
        expect(service.execute).to eq chat_response_body.dig('data', 'outputText')
      end
    end

    context "when #llm_client.chat returns a failed response" do
      before do
        stub_failed_chat_request
      end

      it "returns nil" do
        expect(service.execute).to be_nil
      end
    end
  end
end
