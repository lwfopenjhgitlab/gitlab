# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::MergeRequests::Llm::SummarizeMergeRequestWorker, ai_provider: :chat_glm do
  let_it_be(:user) { create(:user) }
  let_it_be(:merge_request) { create(:merge_request) }
  let_it_be(:project) { merge_request.project }
  let_it_be(:message) { 'this is a message from the llm' }
  let_it_be(:ai_provider) { 'chat_glm' }

  subject(:worker) { described_class.new }

  before do
    allow_next_instance_of(::Llm::MergeRequests::SummarizeDiffService,
      title: merge_request.title,
      user: user,
      diff: merge_request.merge_request_diff) do |service|
      allow(service).to receive(:execute).and_return(message)
    end
  end

  context "when provided an invalid user_id" do
    let(:params) { [non_existing_record_id, { merge_request_id: merge_request.id }] }

    it "returns nil" do
      expect(worker.perform(*params)).to be_nil
    end

    it "does not create a new note" do
      expect { worker.perform(*params) }.not_to change { Note.count }
    end
  end

  context 'when type is prepare_diff_summary' do
    context 'when summarize_diff_vertex is disabled' do
      let(:params) do
        [user.id,
          { type: ::MergeRequests::Llm::SummarizeMergeRequestWorker::PREPARE_DIFF_SUMMARY,
            diff_id: merge_request.merge_request_diff.id }]
      end

      before do
        stub_feature_flags(summarize_diff_vertex: false)
      end

      it 'creates a diff llm summary' do
        expect { worker.perform(*params) }.to change { ::MergeRequest::DiffLlmSummary.count }.by(1)

        expect(::MergeRequest::DiffLlmSummary.last)
          .to have_attributes(
            merge_request_diff: merge_request.merge_request_diff,
            content: message,
            provider: ai_provider)
      end
    end

    context 'when the diff does not exist' do
      let(:params) do
        [user.id,
          { type: ::MergeRequests::Llm::SummarizeMergeRequestWorker::PREPARE_DIFF_SUMMARY,
            diff_id: non_existing_record_id }]
      end

      it 'does not create a diff llm summary' do
        expect { worker.perform(*params) }.not_to change { ::MergeRequest::DiffLlmSummary.count }
      end
    end
  end
end
