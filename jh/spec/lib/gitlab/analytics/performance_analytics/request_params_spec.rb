# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Analytics::PerformanceAnalytics::RequestParams do
  let_it_be(:user) { create(:user) }
  let_it_be(:root_group) { create(:group) }
  let_it_be(:sub_group) { create(:group, parent: root_group) }
  let_it_be(:sub_group_project) { create(:project, id: 1, group: sub_group) }
  let_it_be(:root_group_projects) do
    [
      create(:project, id: 2, group: root_group),
      create(:project, id: 3, group: root_group)
    ]
  end

  let(:project_ids) { root_group_projects.collect(&:id) }
  let(:params) do
    {
      start_date: '2020-01-01',
      end_date: '2020-03-01',
      project_ids: [2, 3],
      current_user: user
    }
  end

  subject { described_class.new(params) }

  before_all do
    root_group.add_owner(user)
  end

  describe 'validations' do
    it 'is valid' do
      expect(subject).to be_valid
    end

    context 'when `start_date` is missing' do
      before do
        params[:start_date] = nil
      end

      it 'is valid', time_travel_to: '2019-03-01' do
        expect(subject).to be_valid
      end
    end

    context 'when `start_date` is earlier than `end_date`' do
      before do
        params[:start_date] = '2021-01-01'
        params[:end_date] = '2020-01-01'
      end

      it 'is invalid' do
        expect(subject).not_to be_valid
        expect(subject.errors.messages[:end_date]).not_to be_empty
      end
    end

    context 'when the date range exceeds 180 days' do
      before do
        params[:start_date] = '2021-01-01'
        params[:end_date] = '2022-01-01'
      end

      it 'is invalid' do
        expect(subject).not_to be_valid
        expect(subject.errors.messages[:end_date]).to include(
          s_('JH|PerformanceAnalytics|The given date range is larger than 180 days')
        )
      end
    end
  end

  it 'casts `end_date` to `Time`' do
    expect(subject.end_date).to be_a_kind_of(Time)
  end

  it 'casts `start_date` to `Time`' do
    expect(subject.start_date).to be_a_kind_of(Time)
  end

  describe 'optional `project_ids`' do
    context 'when `project_ids` is not an array' do
      before do
        params[:project_ids] = 1
      end

      it { expect(subject.project_ids).to eq([1]) }
    end

    context 'when `project_ids` is nil' do
      before do
        params[:project_ids] = nil
      end

      it { expect(subject.project_ids).to eq([]) }
    end

    context 'when `project_ids` is empty' do
      before do
        params[:project_ids] = []
      end

      it { expect(subject.project_ids).to eq([]) }
    end
  end

  describe 'sorting params' do
    before do
      params.merge!(sort: "username", direction: "asc")
    end

    it 'adds sorting params to options' do
      options = subject.to_options

      expect(options[:sort]).to eq("username")
      expect(options[:direction]).to eq("asc")
    end
  end
end
