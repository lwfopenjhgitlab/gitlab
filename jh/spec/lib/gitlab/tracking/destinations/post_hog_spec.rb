# frozen_string_literal: true

require 'spec_helper'

# modified by PostHogMock in test environment
RSpec.describe Gitlab::Tracking::Destinations::PostHog do
  describe '#event' do
    let(:enable_post_hog) { true }

    before do
      allow_next_instance_of(described_class) do |client|
        allow(client).to receive(:enabled?).and_return(enable_post_hog)
      end
    end

    subject(:send_event) do
      tracker = described_class.new
      tracker.event('category', 'action', label: 'label', property: 'property', value: 1.5)
    end

    context 'when PostHog is enabled' do
      let(:enable_post_hog) { true }

      it 'sends event to tracker' do
        expect(send_event).to be_truthy
      end
    end

    context 'when PostHog is not enabled' do
      let(:enable_post_hog) { false }

      it 'does not send event to tracker' do
        expect(send_event).to be_nil
      end
    end
  end
end
