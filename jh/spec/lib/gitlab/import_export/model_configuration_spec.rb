# frozen_string_literal: true

require 'spec_helper'

# Override from upstream: jh/spec/lib/gitlab/import_export/model_configuration_spec.rb
#
# Part of the test security suite for the Import/Export feature
# Finds if a new model has been added that can potentially be part of the Import/Export
# If it finds a new model, it will show a +failure_message+ with the options available.
RSpec.describe 'Import/Export model configuration' do
  include ConfigurationHelper

  let(:upstream_models_yml) { 'spec/lib/gitlab/import_export/all_models.yml' }
  let(:jh_models_yml) { 'jh/spec/lib/gitlab/import_export/all_models.yml' }
  let(:all_models_hash) do
    # Combine the models of JH and Upstream together
    YAML.load_file(upstream_models_yml).deep_merge(YAML.load_file(jh_models_yml)) do |_key, old_val, new_val|
      if old_val.is_a?(Array) && new_val.is_a?(Array)
        old_val + new_val
      else
        new_val
      end
    end
  end

  let(:current_models) { setup_models }
  let(:model_names) { relation_names_for(:project) }

  it 'has no new models' do
    model_names.each do |model_name|
      new_models = Array(current_models[model_name]) - Array(all_models_hash[model_name])
      expect(new_models).to be_empty, failure_message(model_name.classify, new_models)
    end
  end

  # List of current models between models, in the format of
  # {model: [model_2, model3], ...}
  def setup_models
    model_names.index_with do |model_name|
      associations_for(relation_class_for_name(model_name)) - ['project']
    end
  end

  def failure_message(parent_model_name, new_models)
    <<~MSG
      New model(s) <#{new_models.join(',')}> have been added, related to #{parent_model_name}, which is exported by
      the Import/Export feature.

      If you think this model should be included in the export, please add it to `#{Gitlab::ImportExport.config_file}`.

      Definitely add it to `#{File.expand_path(jh_models_yml)}`
      to signal that you've handled this error and to prevent it from showing up in the future.
    MSG
  end
end
