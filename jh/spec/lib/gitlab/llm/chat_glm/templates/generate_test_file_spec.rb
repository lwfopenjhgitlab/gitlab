# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Llm::ChatGlm::Templates::GenerateTestFile, feature_category: :code_review_workflow do
  let_it_be(:merge_request) { create(:merge_request) }

  let(:path) { "files/js/commit.coffee" }

  subject { described_class.new(merge_request, path) }

  describe '#to_prompt' do
    let(:prompt_text) { subject.to_prompt }

    it 'returns correct parameters' do
      expect(prompt_text).to include("class Commit")
      expect(prompt_text).to include(path)
      expect(prompt_text).to include('请为其编写单元测试代码来确保其正确运行')
    end
  end
end
