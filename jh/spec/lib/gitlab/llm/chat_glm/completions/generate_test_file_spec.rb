# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::Gitlab::Llm::ChatGlm::Completions::GenerateTestFile, ai_provider: :chat_glm, feature_category: :source_code_management do
  include_context 'with ChatGLM client shared context'

  let_it_be(:user) { create(:user) }
  let_it_be(:group) { create(:group, :public) }
  let_it_be(:project) { create(:project, :public, group: group) }
  let_it_be(:merge_request) { create(:merge_request, source_project: project) }

  let(:template_class) { ::Gitlab::Llm::CompletionsFactory.completions.dig(:generate_test_file, :prompt_class) }
  let(:prompt_text) { 'prompt and content' }
  let(:content) { chat_response_body.dig('data', 'outputText') }
  let(:ai_response) { chat_response_body.to_json }

  subject(:generate_test_file) do
    described_class.new(template_class).execute(user, merge_request, { file_path: 'index.js' })
  end

  before_all do
    group.namespace_settings.update!(third_party_ai_features_enabled: true)
  end

  describe "#execute" do
    context 'with valid params' do
      it 'performs the ChatGLM request' do
        expect_next_instance_of(described_class) do |completion_service|
          expect(completion_service).to receive(:execute).with(user, merge_request, { file_path: 'index.js' })
            .and_call_original
        end

        allow_next_instance_of(template_class) do |instance|
          allow(instance).to receive(:to_prompt).and_return(prompt_text)
        end

        allow_next_instance_of(::Gitlab::Llm::ClientFactory.client) do |instance|
          params = { content: prompt_text, moderated: true, max_tokens: 1000 }
          allow(instance).to receive(:chat).with(params).and_return(ai_response)
        end

        uuid = 'uuid'

        expect(SecureRandom).to receive(:uuid).and_return(uuid)

        data = {
          id: uuid,
          model_name: 'MergeRequest',
          content: content,
          request_id: nil,
          role: 'assistant',
          timestamp: an_instance_of(ActiveSupport::TimeWithZone),
          errors: []
        }

        expect(GraphqlTriggers).to receive(:ai_completion_response).with(
          user.to_global_id, merge_request.to_global_id, data
        )

        generate_test_file
      end
    end

    context 'with request error' do
      it 'renders error msg' do
        expect_next_instance_of(described_class) do |completion_service|
          expect(completion_service).to receive(:response_for).and_raise(StandardError)
        end

        uuid = 'uuid'
        expect(SecureRandom).to receive(:uuid).and_return(uuid)
        data = {
          id: uuid,
          model_name: 'MergeRequest',
          content: nil,
          request_id: nil,
          role: 'assistant',
          timestamp: an_instance_of(ActiveSupport::TimeWithZone),
          errors: [described_class::DEFAULT_ERROR]
        }

        expect(GraphqlTriggers).to receive(:ai_completion_response).with(
          user.to_global_id, merge_request.to_global_id, data
        )

        generate_test_file
      end
    end
  end
end
