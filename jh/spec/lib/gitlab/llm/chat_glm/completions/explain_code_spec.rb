# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Llm::ChatGlm::Completions::ExplainCode, ai_provider: :chat_glm, feature_category: :source_code_management do
  let_it_be(:user) { create(:user) }
  let_it_be(:project) { create(:project, :public) }

  let(:content) { "some random content" }
  let(:template_class) { ::Gitlab::Llm::ChatGlm::Templates::ExplainCode }
  let(:options) do
    {
      messages: [{
        'role' => 'system',
        'content' => 'You are a knowledgeable assistant explaining to an engineer'
      }, {
        'role' => 'user',
        'content' => content
      }]
    }
  end

  let(:ai_template) do
    {
      content: [options[:messages][1]], # drop system role message
      temperature: 0.3
    }
  end

  let(:ai_response) do
    {
      data: {
        choices: [
          {
            role: 'assistant',
            content: "some ai response text"
          }
        ]
      }
    }.to_json
  end

  let(:params) { { request_id: 'uuid' } }

  subject(:explain_code) { described_class.new(template_class, params).execute(user, project, options) }

  describe "#execute" do
    it 'performs an chatglm request' do
      expect_next_instance_of(::Gitlab::Llm::ChatGlm::Query, user) do |instance|
        expect(instance).to receive(:chat_v3).with(content: ai_template[:content],
**ai_template).and_return(ai_response)
      end

      response_modifier = double
      response_service = double
      params = [user, project, response_modifier, { options: { request_id: 'uuid' } }]

      expect(Gitlab::Llm::ChatGlm::ResponseModifiers::ChatV3).to receive(:new).with(ai_response).and_return(
        response_modifier
      )
      expect(::Gitlab::Llm::GraphqlSubscriptionResponseService).to receive(:new).with(*params).and_return(
        response_service
      )
      expect(response_service).to receive(:execute)

      explain_code
    end
  end
end
