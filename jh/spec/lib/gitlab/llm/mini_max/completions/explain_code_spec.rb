# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Llm::MiniMax::Completions::ExplainCode, ai_provider: :mini_max, feature_category: :source_code_management do
  include_context "with MiniMax client shared context"

  let_it_be(:user) { create(:user) }
  let(:content) { "帮我翻译下面这句话：我是谁" }
  let(:template_class) { ::Gitlab::Llm::CompletionsFactory.completions.dig(:explain_code_open_ai, :prompt_class) }
  let(:options) do
    {
      messages: [
        {
          'role' => 'system',
          'content' => 'You are a knowledgeable assistant explaining to an engineer'
        },
        {
          'role' => 'user',
          'content' => content
        }
      ]
    }
  end

  let(:ai_options) do
    {
      bot_setting: [{ bot_name: "JIHU_BOT", content: "你是一个软件研发专家，请帮助我解释代码的相关问题" }],
      messages: [{ sender_name: "JIHU_BOT",
                   sender_type: "BOT",
                   text: "You are a knowledgeable assistant explaining to an engineer" },
        { sender_name: "user", sender_type: "USER", text: "帮我翻译下面这句话：我是谁" }],
      temperature: 0.3
    }
  end

  let(:ai_response) { chat_response_body.to_json }
  let(:params) { { request_id: 'uuid' } }
  let_it_be(:project) { create(:project, :public) }

  before do
    stub_env('CHAT_MINIMAX_GROUP_ID', group_id)
    stub_env('CHAT_MINIMAX_API_KEY', api_key)
  end

  subject(:explain_code) { described_class.new(template_class, params).execute(user, project, options) }

  describe "#execute" do
    it 'performs an mini_max request' do
      expect_next_instance_of(::Gitlab::Llm::ClientFactory.client, user) do |instance|
        expect(instance).to receive(:chat).with(ai_options).and_return(ai_response)
      end

      response_modifier = double
      response_service = double
      params = [user, project, response_modifier, { options: { request_id: 'uuid' } }]

      expect(::Gitlab::Llm::ClientFactory.response_modifier).to receive(:new).with(ai_response).and_return(
        response_modifier
      )
      expect(::Gitlab::Llm::GraphqlSubscriptionResponseService).to receive(:new).with(*params).and_return(
        response_service
      )
      expect(response_service).to receive(:execute)

      explain_code
    end
  end
end
