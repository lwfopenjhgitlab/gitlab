# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::Gitlab::Patch::GettextI18nRailsJsTask do
  subject do
    klass = Class.new do
      def self.files_list
        []
      end

      def self.lang_for(_)
        'zh_CN'
      end
    end
    klass.prepend(described_class)
  end

  describe '.file_list' do
    it 'returns po files under jh/locale' do
      expect(subject.files_list.any? { |path| path.to_s.include?("jh/locale") }).to eq true
    end
  end

  describe '.json_for' do
    it 'merges po file content' do
      file = instance_double(File)
      po_to_json = instance_double(Gitlab::Patch::PoToJson)

      allow(Gitlab::Patch::PoToJson).to receive(:new).with(file, subject.jh_translation_cache).and_return(po_to_json)
      allow(po_to_json).to(
        receive(:generate_for_jed)
        .with('zh_CN', GettextI18nRailsJs.config.jed_options)
        .and_return('translate_result'))

      expect(subject.json_for(file)).to eq('translate_result')
    end
  end
end
