# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::Gitlab::Patch::PoToJson do
  subject do
    described_class.new(files, jh_translation_cache)
  end

  let(:files) do
    instance_double("File")
  end

  before do
    allow(subject).to receive(:parse_document).and_call_original
    allow(subject).to receive(:inject_meta).and_call_original
  end

  describe '.generate_for_jed' do
    context 'when cache is nil' do
      let(:jh_translation_cache) do
        { 'zh_CN' => nil }
      end

      it 'merges only' do
        allow(subject).to receive(:parse_document).and_return({ "name" => "GitLab" })

        expect(subject.generate_for_jed('zh_CN')).to eq ''
        expect(jh_translation_cache).to eq({ 'zh_CN' => { "name" => "GitLab" } })
      end
    end

    context 'when cache is present' do
      let(:jh_translation_cache) do
        { 'zh_CN' => { 'name' => "GitLab", 'type' => "ee" } }
      end

      it 'merges and overrides' do
        allow(subject).to receive(:parse_document).and_return({ "name" => "极狐", "version" => "1.0" })
        allow(subject).to receive(:inject_meta).and_return({ "name" => "极狐", "version" => "1.0" })

        expect(subject.generate_for_jed('zh_CN')).to include(Gitlab::Json.generate(
          {
             "name" => "极狐",
             "type" => 'ee',
             "version" => "1.0"
          })
                                                            )
      end
    end
  end
end
