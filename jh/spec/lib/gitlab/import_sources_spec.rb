# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::ImportSources do
  describe '.options' do
    it 'returns a hash' do
      expected =
        {
          'GitHub' => 'github',
          'Bitbucket Cloud' => 'bitbucket',
          'Bitbucket Server' => 'bitbucket_server',
          'FogBugz' => 'fogbugz',
          'Repository by URL' => 'git',
          'GitLab export' => 'gitlab_project',
          'Gitea' => 'gitea',
          'Manifest file' => 'manifest',
          'Gitee' => 'gitee'
        }

      expect(described_class.options).to eq(expected)
    end
  end

  describe '.values' do
    it 'returns an array' do
      expected =
        %w[
          github
          bitbucket
          bitbucket_server
          fogbugz
          git
          gitlab_project
          gitea
          manifest
          gitee
        ]

      expect(described_class.values).to eq(expected)
    end
  end

  describe '.importer_names' do
    it 'returns an array of importer names' do
      expected =
        %w[
          github
          bitbucket
          bitbucket_server
          fogbugz
          gitlab_project
          gitea
          gitee
        ]

      expect(described_class.importer_names).to eq(expected)
    end
  end

  describe '.import_table' do
    it 'includes specific JH imports types when the license supports them' do
      expect(described_class.jh_import_table).not_to be_empty
      expect(described_class.import_table).to include(*described_class.jh_import_table)
    end
  end
end
