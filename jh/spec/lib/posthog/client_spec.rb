# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Posthog::Client do
  describe '#initialize' do
    let(:posthog_api_key) { 'posthog_api_key' }
    let(:posthog_url) { 'http://example.com' }
    let(:post_hog_client) { described_class.new(posthog_api_key: posthog_api_key, posthog_url: posthog_url) }

    it 'initializes a new instance' do
      expect(post_hog_client).to be_a described_class
    end
  end

  describe '#request_persons_funnel' do
    it 'do request persons/funnel' do
      fake_response = 'abc'
      allow(Gitlab::HTTP).to receive(:get).and_return(fake_response)
      posthog_api_caller = described_class.new(posthog_api_key: 'key', posthog_url: 'http://example.com')
      response = posthog_api_caller.request_persons_funnel(
        utm_source: 'utm_source',
        project_id: 'project_id',
        limit: 'limit',
        date_from: 'date_from',
        date_to: 'date_to'
      )
      expect(response).to eq(fake_response)
    end
  end
end
