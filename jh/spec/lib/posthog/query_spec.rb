# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Posthog::Query do
  describe '#get_user_ids' do
    before do
      response = instance_double(Net::HTTPResponse)
      allow(response).to receive(:body).and_return(
        ::Gitlab::Json.dump(
          {
            results: [
              {
                people: [
                  {
                    name: '123'
                  }
                ]
              }
            ]
          }
        )
      )
      allow_next_instance_of(Posthog::Client) do |client|
        allow(client).to receive(:request_persons_funnel).and_return(response)
      end
    end

    it 'get user ids' do
      user_ids = described_class.get_user_ids(
        posthog_url: "http://example.com",
        posthog_api_key: 1,
        utm_source: 1,
        project_id: 1,
        limit: 1,
        date_from: 1,
        date_to: 1
      )
      expect(user_ids).to eq([123])
    end

    it 'users to csv' do
      user1 = create(:user)
      user2 = create(:user)
      expected_csv = "id,email,name,created_at\n" \
                     "#{user1.id},#{user1.email},#{user1.name},#{user1.created_at}\n" \
                     "#{user2.id},#{user2.email},#{user2.name},#{user2.created_at}\n"
      actual_csv = described_class.users_to_csv([user1, user2])
      expect(actual_csv).to eq(expected_csv)
    end
  end
end
