# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::Feature, stub_feature_flags: false do
  before do
    # reset Flipper AR-engine
    described_class.reset
    skip_feature_flags_yaml_validation
  end

  describe '.flipper with ENV variable DISABLE_FF_PROCESS_MEMORY_CACHE' do
    subject(:adapter_delegate_sd_obj) { described_class.flipper.adapter.instance_variable_get(:@delegate_sd_obj) }

    context 'with nil or false by default' do
      it 'uses process memory cache' do
        expect(adapter_delegate_sd_obj.class).to eq ::Flipper::Adapters::ActiveSupportCacheStore
        expect(adapter_delegate_sd_obj.instance_variable_get(:@write_options)).to include(expires_in: 1.minute)
      end
    end

    context 'with true value to support E2E on JH staging' do
      before do
        stub_env('DISABLE_FF_PROCESS_MEMORY_CACHE', 'true')
      end

      it 'uses rails cache' do
        expect(adapter_delegate_sd_obj.class).to eq ::Feature::ActiveSupportCacheStoreAdapter
        expect(adapter_delegate_sd_obj.instance_variable_get(:@write_options)).to include(expires_in: 1.hour)
      end
    end
  end
end
