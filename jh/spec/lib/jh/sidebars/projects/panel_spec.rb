# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Sidebars::Projects::Panel, feature_category: :navigation do
  using RSpec::Parameterized::TableSyntax
  let_it_be(:project) { create(:project) }

  let(:context) { Sidebars::Projects::Context.new(current_user: nil, container: project) }

  subject(:panel) { described_class.new(context) }

  describe 'ExternalIssueTrackerMenu' do
    # rubocop:disable Lint/BinaryOperatorWithIdenticalOperands
    where(:show_ones_menu_items, :show_ligaai_menu_items, :expect_to_contains_external_issue_tracker_menu) do
      true  | true  | false
      true  | false | false
      false | true  | false
      false | false | true
    end
    # rubocop:enable Lint/BinaryOperatorWithIdenticalOperands

    with_them do
      before do
        allow_next_instance_of(Sidebars::Projects::Menus::IssuesMenu) do |issues_menu|
          allow(issues_menu).to receive(:show_ones_menu_items?).and_return(show_ones_menu_items)
          allow(issues_menu).to receive(:show_ligaai_menu_items?).and_return(show_ligaai_menu_items)
        end
      end

      it 'contains ExternalIssueTracker menu as expected' do
        expect(contains_external_issue_tracker_menu?).to be(expect_to_contains_external_issue_tracker_menu)
      end
    end

    def contains_external_issue_tracker_menu?
      contains_menu?(Sidebars::Projects::Menus::ExternalIssueTrackerMenu)
    end
  end

  def contains_menu?(menu)
    panel.instance_variable_get(:@menus).any?(menu)
  end
end
