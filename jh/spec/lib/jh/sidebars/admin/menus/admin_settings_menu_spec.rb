# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Sidebars::Admin::Menus::AdminSettingsMenu, feature_category: :navigation do
  let(:user) { build_stubbed(:user) }
  let(:context) { Sidebars::Context.new(current_user: user, container: nil) }

  describe 'Group Menu items' do
    let(:item_id) { :admin_service_usage }

    subject { described_class.new(context).renderable_items.index { |e| e.item_id == item_id } }

    context 'when flag jh_usage_statistics is disabled' do
      before do
        stub_feature_flags(jh_usage_statistics: false)
      end

      specify { is_expected.to be_nil }
    end

    context 'when flag jh_usage_statistics is enabled' do
      specify { is_expected.not_to be_nil }
    end
  end
end
