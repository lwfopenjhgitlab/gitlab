# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Backup::Manager do
  include StubENV

  let(:progress) { StringIO.new }
  let(:definitions) { nil }
  let(:backup_information) do
    {
      backup_created_at: Time.zone.parse('2019-01-01'),
      gitlab_version: '12.3'
    }
  end

  subject { described_class.new(progress, definitions: definitions) }

  before do
    allow(progress).to receive(:puts)
    allow(progress).to receive(:print)
    FileUtils.mkdir_p('tmp/tests/public/uploads')
    allow(Kernel).to receive(:system).and_return(true)
    allow(YAML).to receive(:load_file).and_return(backup_information)

    allow(subject).to receive(:backup_information).and_return(backup_information)
  end

  after do
    FileUtils.rm_rf('tmp/tests/public/uploads', secure: true)
  end

  describe '#create' do
    let(:task) { instance_double(Backup::Task) }
    let(:enabled) { true }
    let(:definitions) do
      {
        'my_task' => Backup::Manager::TaskDefinition.new(
          task: task,
          enabled: enabled,
          destination_path: 'my_task.tar.gz',
          human_name: 'my task'
        )
      }
    end

    let(:files) do
      [
        '1451520000_2015_12_31_4.5.6-pre-jh_gitlab_backup.tar'
      ]
    end

    before do
      allow(task).to receive(:dump)
      allow(Dir).to receive(:chdir).and_yield
      allow(Dir).to receive(:glob).and_return(files)
      allow(FileUtils).to receive(:rm)
      allow(Time).to receive(:now).and_return(Time.utc(2016))
    end

    context 'when keep_time is set to remove files' do
      before do
        # Set to 1 second
        allow(Gitlab.config.backup).to receive(:keep_time).and_return(1)

        subject.create
      end

      it 'removes matching files with a human-readable versioned timestamp with tagged JH' do
        expect(FileUtils).to have_received(:rm).with(files[0])
      end
    end
  end
end
