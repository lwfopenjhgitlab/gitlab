# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::Gitlab::Llm::CompletionsFactory do
  describe '.completion' do
    context 'with ChatGLM provider' do
      let(:ai_provider) { :chat_glm }

      before do
        allow(::Gitlab::Llm::ClientFactory).to receive(:ai_provider).and_return(ai_provider)
      end

      context 'with extended completion' do
        let(:extended_completions) { described_class::EXTENDED_COMPLETIONS[ai_provider] }

        it 'returns correct completion service' do
          extended_completions.each do |completion_name, completion_classes|
            completion_class = completion_classes.fetch(:service_class)
            prompt_class = completion_classes.fetch(:prompt_class)

            expect(completion_class).to receive(:new).with(prompt_class, {}).and_call_original

            expect(described_class.completion(completion_name)).to be_a(completion_class)
          end
        end
      end

      context 'with invalid completion' do
        let(:completion_name) { :invalid_name }

        it 'returns completion service' do
          completion = described_class.completion(completion_name)

          expect(completion).to be_nil
        end
      end
    end
  end
end
