# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::Gitlab::Saas, feature_category: :environment_management do
  using RSpec::Parameterized::TableSyntax

  where(:method_name, :test, :development, :result) do
    :com_url                        | false | false  | 'https://jihulab.com'
    :com_url                        | false | true   | 'https://jihulab.com'
    :com_url                        | true  | false  | 'https://jihulab.com'
    :staging_com_url                | false | false  | 'https://staging.jihulab.com'
    :staging_com_url                | false | true   | 'https://staging.jihulab.com'
    :staging_com_url                | true  | false  | 'https://staging.jihulab.com'
    :canary_toggle_com_url          | false | false  | 'https://next.jihulab.com'
    :canary_toggle_com_url          | false | true   | 'https://next.jihulab.com'
    :canary_toggle_com_url          | true  | false  | 'https://next.jihulab.com'
    :subdomain_regex                | false | false  | %r{\Ahttps://[a-z0-9-]+\.jihulab\.com\z}
    :subdomain_regex                | false | true   | %r{\Ahttps://[a-z0-9-]+\.jihulab\.com\z}
    :subdomain_regex                | true  | false  | %r{\Ahttps://[a-z0-9-]+\.jihulab\.com\z}
    :dev_url                        | false | false  | 'https://dev-ops.gitlab.cn'
    :dev_url                        | false | true   | 'https://dev-ops.gitlab.cn'
    :dev_url                        | true  | false  | 'https://dev-ops.gitlab.cn'
    :registry_prefix                | false | false  | 'registry.gitlab.cn'
    :registry_prefix                | false | true   | 'registry.gitlab.cn'
    :registry_prefix                | true  | false  | 'registry.gitlab.cn'
    :customer_support_url           | false | false  | 'https://support.gitlab.cn'
    :customer_support_url           | false | true   | 'https://support.gitlab.cn'
    :customer_support_url           | true  | false  | 'https://support.gitlab.cn'
    :customer_license_support_url   | false | false  | 'https://support.gitlab.cn/#/portal/submitticket/3'
    :customer_license_support_url   | false | true   | 'https://support.gitlab.cn/#/portal/submitticket/3'
    :customer_license_support_url   | true  | false  | 'https://support.gitlab.cn/#/portal/submitticket/3'
    :gitlab_com_status_url          | false | false  | 'https://status.gitlab.cn'
    :gitlab_com_status_url          | false | true   | 'https://status.gitlab.cn'
    :gitlab_com_status_url          | true  | false  | 'https://status.gitlab.cn'
    :commom_purchase_url            | false | false  | 'https://about.gitlab.cn/upgrade-plan'
    :commom_purchase_url            | false | true   | 'https://about.gitlab.cn/upgrade-plan'
    :commom_purchase_url            | true  | false  | 'https://about.gitlab.cn/upgrade-plan'
    :about_pricing_url              | false | false  | 'https://about.gitlab.cn/pricing'
    :about_pricing_url              | false | true   | 'https://about.gitlab.cn/pricing'
    :about_pricing_url              | true  | false  | 'https://about.gitlab.cn/pricing'
    :about_pricing_faq_url          | false | false  | 'https://about.gitlab.cn/pricing#faq'
    :about_pricing_faq_url          | false | true   | 'https://about.gitlab.cn/pricing#faq'
    :about_pricing_faq_url          | true  | false  | 'https://about.gitlab.cn/pricing#faq'
    :about_feature_comparison_url   | false | false  | 'https://about.gitlab.cn/pricing/saas/feature-comparison'
    :about_feature_comparison_url   | false | true   | 'https://about.gitlab.cn/pricing/saas/feature-comparison'
    :about_feature_comparison_url   | true  | false  | 'https://about.gitlab.cn/pricing/saas/feature-comparison'
  end

  with_them do
    subject { described_class.method(method_name).call }

    before do
      allow(Rails).to receive_message_chain(:env, :test?).and_return(test)
      allow(Rails).to receive_message_chain(:env, :development?).and_return(development)
    end

    it { is_expected.to eq(result) }
  end

  context 'when is in HK region' do
    before do
      stub_env('SAAS_REGION', 'HK')
    end

    it 'returns HK urls' do
      expect(described_class.hk?).to eq(true)
      expect(described_class.com_url).to eq('https://gitlab.hk')
      expect(described_class.staging_com_url).to eq('https://staging.gitlab.hk')
      expect(described_class.canary_toggle_com_url).to eq('https://next.gitlab.hk')
    end

    it 'matches the hk subdomain' do
      expect(described_class.subdomain_regex).to match('https://staging.gitlab.hk')
      expect(described_class.subdomain_regex).not_to match('https://staging.jihulab.com')
    end
  end
end
