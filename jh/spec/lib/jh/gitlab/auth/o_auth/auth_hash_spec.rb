# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Auth::OAuth::AuthHash do
  let(:username) { FFaker::NameCN.last_first }

  let(:auth_hash_params) do
    {
      provider: "gitlab",
      uid: SecureRandom.hex,
      info: {
        unionid: SecureRandom.hex,
        ding_id: SecureRandom.hex,
        name: username,
        username: username,
        openid: SecureRandom.hex
      }
    }
  end

  let(:auth_hash) { described_class.new(OmniAuth::AuthHash.new(auth_hash_params)) }

  describe '#email' do
    subject { auth_hash.email }

    it 'avoids invalid characters' do
      is_expected.not_to match(/\p{Han}/)
    end

    it 'avoids email conflicts with same name users' do
      is_expected.not_to eq "temp-email-for-oauth-#{username}@gitlab.localhost"
    end
  end
end
