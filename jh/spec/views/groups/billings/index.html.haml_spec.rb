# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'groups/billings/index', :saas, :aggregate_failures do
  include SubscriptionPortalHelpers

  let_it_be(:user) { create(:user) }
  let_it_be(:group) { create(:group_with_plan, plan: :free_plan) }
  let_it_be(:plans_data) { billing_plans_data.map { |plan| Hashie::Mash.new(plan) } }

  before do
    allow(view).to receive(:current_user).and_return(user)
    assign(:group, group)
    assign(:plans_data, plans_data)
  end

  context 'when the group is the top level' do
    context 'with free plan' do
      it 'renders the billing page' do
        render
        expect(rendered).to have_link('Talk to an expert today.', href: 'https://gitlab.cn/sales/')
        customers = ['禅道', '中智行', '云骥', '普华永道', '广发证券', 'New Development Bank', '齐碳科技', '途游游戏']
        expect_to_have_customer_logo(customers)
      end
    end
  end

  def expect_to_have_customer_logo(customers)
    customers.each do |customer|
      expect(rendered).to have_css("img[alt='#{customer}']")
    end
  end
end
