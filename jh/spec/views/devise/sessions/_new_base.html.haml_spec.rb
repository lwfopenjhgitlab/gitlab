# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'devise/sessions/_new_base.html.haml' do
  describe 'user login label' do
    before do
      stub_devise
      allow(view).to receive(:captcha_enabled?).and_return(false)
      allow(view).to receive(:captcha_on_login_required?).and_return(false)
    end

    context 'when it is not COM' do
      it 'does not contains phone' do
        render partial: 'devise/sessions/new_base', formats: [:html]

        expect(rendered).to have_content(_('Username or email'))
        expect(rendered).not_to have_content(s_('JH|Username, email or phone'))
      end
    end

    context 'when it is COM' do
      before do
        allow(::Gitlab).to receive(:com?).and_return true
      end

      it 'contians phone' do
        render partial: 'devise/sessions/new_base', formats: [:html]

        expect(rendered).to have_content(s_('JH|Username, email or phone'))
      end

      context 'when phone_authenticatable feature is not enabled' do
        before do
          stub_feature_flags(phone_authenticatable: false)
        end

        it 'does not contains phone' do
          render partial: 'devise/sessions/new_base', formats: [:html]

          expect(rendered).to have_content(s_('Username or email'))
          expect(rendered).not_to have_content(s_('JH|Username, email or phone'))
        end
      end
    end
  end

  def stub_devise
    allow(view).to receive(:devise_mapping).and_return(Devise.mappings[:user])
    allow(view).to receive(:resource).and_return(spy)
    allow(view).to receive(:resource_name).and_return(:user)
  end
end
