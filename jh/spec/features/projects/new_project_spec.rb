# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'New project', :js do
  let(:terms_selector) { '[data-testid="new-project-terms"]' }
  let(:jihu_term_selector) { '#project_agree_jihu_terms' }
  let(:html5_validation_message) { 'Please check this box if you want to proceed.' }

  context 'as a user' do
    let(:user) { create(:user) }

    before do
      stub_application_setting(import_sources: Gitlab::ImportSources.values)
      stub_ee_application_setting(custom_project_templates_enabled: true)
      sign_in(user)
    end

    it 'shows terms when create blank project', :js, :saas do
      visit new_project_path

      click_link 'Create blank project'
      expect(page).not_to have_selector(terms_selector)

      choose 'Public'
      expect(page).to have_selector(terms_selector)

      click_button 'Create project'
      message = page.find(jihu_term_selector).native.attribute("validationMessage")
      expect(message).to eq html5_validation_message

      choose 'Private'
      expect(page).not_to have_selector(terms_selector)
    end

    it 'shows terms when import project from url', :js, :saas do
      visit new_project_path

      click_link 'Import project'
      click_button 'Repository by URL'
      expect(page).not_to have_selector(terms_selector)

      choose 'Public'
      expect(page).to have_selector(terms_selector)

      click_button 'Create project'
      message = page.find(jihu_term_selector).native.attribute("validationMessage")
      expect(message).to eq html5_validation_message

      choose 'Private'
      expect(page).not_to have_selector(terms_selector)
    end

    it 'shows terms when create project from template', :js, :saas do
      visit new_project_path

      click_link 'Create from template'
      find('span', text: 'Use template', match: :first).click
      expect(page).not_to have_selector(terms_selector)

      choose 'Public'
      expect(page).to have_selector(terms_selector)

      click_button 'Create project'
      message = page.find(jihu_term_selector).native.attribute("validationMessage")
      expect(message).to eq html5_validation_message

      choose 'Private'
      expect(page).not_to have_selector(terms_selector)
    end

    context 'when creating CI/CD for external repositories', :js, :saas do
      before do
        stub_licensed_features(ci_cd_projects: true)
        stub_feature_flags(remove_legacy_github_client: false)
      end

      it 'creates CI/CD project from repo URL', :sidekiq_might_not_need_inline do
        visit new_project_path
        click_link 'Run CI/CD for external repository'

        page.within '#ci-cd-project-pane' do
          find('.js-import-git-toggle-button').click

          choose 'Public'
          expect(page).to have_selector(terms_selector)

          click_button 'Create project'
          message = page.find(jihu_term_selector).native.attribute("validationMessage")
          expect(message).to eq html5_validation_message

          choose 'Private'
          expect(page).not_to have_selector(terms_selector)
        end
      end
    end

    it 'can creat new project when terms agreed', :js, :saas do
      visit new_project_path

      click_link 'Create blank project'
      fill_in(:project_name, with: 'Project with terms agreed')
      choose 'Public'
      check 'project_agree_jihu_terms'
      check 'project_agree_intellectual_property'
      click_button 'Create project'

      project = Project.last

      expect(page).to have_current_path(project_path(project))
    end
  end
end
