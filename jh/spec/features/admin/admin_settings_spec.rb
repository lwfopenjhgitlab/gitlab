# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Admin updates settings', type: :feature do
  include StubENV

  let(:admin) { create(:admin) }
  let(:dot_com?) { false }

  describe 'dingtalk integration' do
    before do
      allow(Gitlab).to receive(:com?).and_return(dot_com?)
      stub_env('IN_MEMORY_APPLICATION_SETTINGS', 'false')
      sign_in(admin)
      gitlab_enable_admin_mode_sign_in(admin)
    end

    it 'update dingtalk integration settings' do
      visit general_admin_application_settings_path

      page.within('.as-dingtalk-integration') do
        click_button _('Expand')
        check s_('JH|Integrations|Enable DingTalk Integration')
        fill_in s_('JH|Integrations|DingTalk Corp Id'), with: 'ding-corp-id'
        fill_in s_('JH|Integrations|DingTalk App Key'), with: 'ding-app-key'
        fill_in s_('JH|Integrations|DingTalk App Secret'), with: 'ding-app-secret'
        click_button _('Save changes')
      end

      expect(page).to have_content _('Application settings saved successfully')
      expect(find('#application_setting_dingtalk_integration_enabled')).to be_checked
      expect(find('#application_setting_dingtalk_corpid').value).to eq 'ding-corp-id'
      expect(find('#application_setting_dingtalk_app_key').value).to eq 'ding-app-key'
      expect(find('#application_setting_dingtalk_app_secret').value).to eq 'ding-app-secret'
    end
  end

  describe 'feishu integration' do
    before do
      allow(Gitlab).to receive(:com?).and_return(dot_com?)
      stub_env('IN_MEMORY_APPLICATION_SETTINGS', 'false')
      sign_in(admin)
      gitlab_enable_admin_mode_sign_in(admin)
    end

    it 'update feishu integration settings' do
      visit general_admin_application_settings_path

      page.within('.as-feishu-integration') do
        click_button _('Expand')
        check s_('JH|INTEGRATION|Enable FeiShu Integration')
        fill_in s_('JH|INTEGRATION|FeiShu App ID'), with: 'feishu-app-key'
        fill_in s_('JH|INTEGRATION|FeiShu App Secret'), with: 'feishu-app-secret'
        click_button _('Save changes')
      end

      expect(page).to have_content _('Application settings saved successfully')
      expect(find('#application_setting_feishu_integration_enabled')).to be_checked
      expect(find('#application_setting_feishu_app_key').value).to eq 'feishu-app-key'
      expect(find('#application_setting_feishu_app_secret').value).to eq 'feishu-app-secret'
    end
  end
end
