# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "Users::Signup", feature_category: :system_access do
  include SetDefaultPreferredLanguage

  let(:first_name) { FFaker::Name.first_name }
  let(:last_name) { FFaker::Name.last_name }
  let(:user_login_name) { FFaker::InternetSE.login_user_name }
  let(:email) { FFaker::Internet.email }
  let(:password) { User.random_password }
  let(:phone_number) { '15612341234' }
  let(:verification_code) { '123456' }

  before do
    stub_feature_flags(arkose_labs_signup_challenge: false)
  end

  def sign_up
    visit new_user_registration_path

    page.within('.email-registration-form') do
      fill_in 'First name', with: first_name
      fill_in 'Last name', with: last_name
      fill_in 'Username', with: user_login_name
      fill_in 'Email', with: email
      fill_in 'Password', with: password
      click_button 'Register'
    end
  end

  describe "#form_validation" do
    context 'when it is JH saas', :saas, :phone_verification_code_enabled do
      context 'with valid email' do
        it 'renders email is available' do
          visit new_user_registration_path
          fill_in 'new_user_email', with: 'foo@gitlab.com'
          fill_in 'new_user_first_name', with: ''

          expect(page).to have_content(s_('JH|Email is available.'))
        end
      end

      context 'when matching password complexity' do
        it 'returns that user signed up' do
          sign_up

          expect(page).to have_current_path(new_user_session_path)
          expect(page).to have_content("You have signed up successfully.")
        end
      end

      context 'when matching password complexity and enable zh default local' do
        before do
          set_default_preferred_language_to_zh_cn
        end

        it 'returns that user signed up' do
          visit new_user_registration_path

          page.within('.email-registration-form') do
            fill_in '名字', with: first_name
            fill_in '姓', with: last_name
            fill_in '用户名', with: user_login_name
            fill_in '电子邮件', with: email
            fill_in '密码', with: password
            click_button '注册'
          end

          created_user = User.find_by(email: email)
          expect(created_user.preferred_language).to eq('zh_CN')

          expect(page).to have_current_path(new_user_session_path)
        end
      end
    end
  end

  describe "#phone verification" do
    context 'when it is not SaaS', :phone_verification_code_enabled do
      context 'when admin approval is required' do
        it 'redirects to sign in page' do
          sign_up

          expect(page).to have_current_path(new_user_session_path)
          expect(page).to have_content(_('You have signed up successfully. However, we could not sign you in ' \
                                         'because your account is awaiting approval from your GitLab administrator.'))
        end
      end
    end

    context 'when phone verification is not required', :saas do
      context 'when admin approval is not required', :js do
        before do
          stub_application_setting(require_admin_approval_after_user_signup: false)
        end

        it 'redirects to welcome path' do
          sign_up

          expect(page).to have_current_path(users_sign_up_welcome_path)
        end
      end
    end

    context 'when it is SaaS', :saas, :phone_verification_code_enabled do
      context 'when admin approval is required' do
        it 'redirects to sign in page' do
          sign_up

          expect(page).to have_current_path(new_user_session_path)
          expect(page).to have_content(_('You have signed up successfully. However, we could not sign you in ' \
                                         'because your account is awaiting approval from your GitLab administrator.'))
        end
      end

      context 'when admin approval is not required', :js do
        before do
          stub_application_setting(require_admin_approval_after_user_signup: false)
        end

        it 'redirects to phone verification path' do
          sign_up

          expect(page).to have_current_path(phone_path)
          expect(page).to have_content(s_('JH|RealName|Thank you for using Jihu GitLab, please verify your phone'))
        end

        context 'when terms are not required' do
          before do
            sign_up
          end

          context 'as after phone verification' do
            before do
              allow_any_instance_of(CheckPhoneAndCode).to \
                receive(:verification_message).and_return(nil)
            end

            it 'redirects to welcome path' do
              fill_in 'phone', with: phone_number
              fill_in 'verification_code', with: verification_code
              click_button _('Continue')

              expect(page).to have_current_path(users_sign_up_welcome_path)
            end

            context 'when welcome is required' do
              it 'signs out' do
                find('span', text: _('Sign out')).click

                expect(User.count).to eq(1)
                expect(User.last.role_required?).to be(true)
                expect(page).to have_current_path(new_user_session_path)
              end
            end
          end
        end
      end
    end
  end

  describe 'Signup' do
    include TermsHelper

    let(:new_user) { build_stubbed(:user) }

    def fill_in_signup_form
      fill_in 'new_user_username', with: new_user.username
      fill_in 'new_user_email', with: new_user.email
      fill_in 'new_user_first_name', with: new_user.first_name
      fill_in 'new_user_last_name', with: new_user.last_name
      fill_in 'new_user_password', with: new_user.password
    end

    context 'when terms are enforced' do
      before do
        stub_application_setting(require_admin_approval_after_user_signup: false)
        enforce_terms
      end

      it 'renders text that the user confirms terms by signing in' do
        visit new_user_registration_path

        expect(page).to have_content(/By clicking Register/)
      end
    end

    context 'with invalid email in jh', :saas, :js do
      it_behaves_like 'user email validation in jh' do
        let(:path) { new_user_registration_path }
      end
    end
  end
end
