# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "Signup with phone" do
  include RandomNumberString

  let(:username) { FFaker::InternetSE.login_user_name }
  let(:password) { User.random_password }
  let(:area_code) { '+86' }
  let(:phone) { "136#{random_number_string(11 - 3)}" }
  let(:verification_code) { random_number_string(6) }

  let(:full_phone) { "#{area_code}#{phone}" }
  let(:encrypted_phone) { ::Gitlab::CryptoHelper.aes256_gcm_encrypt(full_phone) }
  let(:visitor_id_code) { SecureRandom.hex(16) }

  before do
    stub_application_setting(require_admin_approval_after_user_signup: false)
    stub_feature_flags(arkose_labs_signup_challenge: false)
    allow(::Gitlab::ApplicationRateLimiter).to receive(:throttled?).and_return(false)

    allow_next_instance_of(::RegistrationsController) do |instance|
      allow(instance).to receive(:visitor_id_code).and_return visitor_id_code
    end
    Phone::VerificationCode.create!(visitor_id_code: visitor_id_code,
      code: verification_code, phone: encrypted_phone, created_at: 30.seconds.ago)

    visit new_user_registration_path(registration_type: :phone)
  end

  context 'with self-managed environment' do
    it 'renders registration form with email type' do
      expect(page).to have_field(_('First name'))
      expect(page).to have_field(_('Last name'))
      expect(page).not_to have_field(_('Phone'))
    end
  end

  context 'with SaaS environment', :saas, :phone_verification_code_enabled do
    def sign_up(form_data = {})
      page.within('.phone-registration-form') do
        fill_in 'new_user_username', with: form_data[:username] || username
        fill_in 'new_user_phone', with: form_data[:phone] || phone
        fill_in 'new_user_verification_code', with: form_data[:verification_code] || verification_code
        fill_in 'new_user_password', with: form_data[:password] || password

        click_button 'Register'
      end
    end

    context 'when email is not required' do
      context 'in experience period' do
        it 'creates a new user successfully as normal' do
          expect { sign_up }.to change { User.count }.by(1)

          expect(page).to have_current_path users_sign_up_welcome_path
          expect(page).to have_content(
            format(s_('JH|Please complete your profile with email address before %{expires_at}'),
              expires_at: User.last.phone_registration_experience_expires_at.strftime('%F %H:%M %:z')))
        end
      end

      context 'if experience expires' do
        it 'redirects to profile page' do
          expect { sign_up }.to change { User.count }.by(1)

          travel_to 2.days.after do
            visit users_sign_up_welcome_path

            expect(page).to have_current_path profile_path
            expect(page).to have_content(_('Please complete your profile with email address'))
          end
        end
      end
    end

    context 'when email is required' do
      before do
        stub_feature_flags(soft_email_required_flow: false)
      end

      it 'creates a new user successfully' do
        expect { sign_up }.to change { User.count }.by(1)

        expect(page).to have_current_path profile_path
        expect(page).to have_content(_('Please complete your profile with email address'))
      end
    end

    context 'with incorrect verification code' do
      it 'alerts error message' do
        expect { sign_up(verification_code: '') }.not_to change { User.count }

        expect(page).to have_current_path(user_registration_path, ignore_query: true)
        expect(page).to have_field('Phone', with: phone)
        expect(page).to have_content s_('JH|RealName|Verification code is incorrect.')
      end
    end

    context 'with invalid password' do
      it 'renders error message' do
        expect { sign_up(password: '12345678') }.not_to change { User.count }

        expect(page).to have_current_path(user_registration_path, ignore_query: true)
        expect(page).to have_field('Phone', with: phone)
        expect(page).to have_content _('Password must not contain commonly used combinations of words and letters')
      end
    end
  end
end
