# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Registration group and project creation flow', :js, :saas do
  let_it_be(:user) { create(:user) }

  let(:feature_flags) { { about_your_company_registration_flow: false } }

  before do
    stub_application_setting(import_sources: %w[gitlab_project])
    # https://gitlab.com/gitlab-org/gitlab/-/issues/340302
    allow(Gitlab::QueryLimiting::Transaction).to receive(:threshold).and_return(153)
    stub_feature_flags(feature_flags)
    sign_in(user)
    visit users_sign_up_welcome_path

    expect(page).to have_content('Welcome to GitLab') # rubocop:disable RSpec/ExpectInHook

    choose 'Just me'
    choose 'Create a new project'
    click_on 'Continue'
  end

  it 'A user can create a group and project' do
    page.within '[data-testid="url-group-path"]' do
      expect(page).to have_content('{group}')
    end

    page.within '[data-testid="url-project-path"]' do
      expect(page).to have_content('{project}')
    end

    fill_in 'group_name', with: 'test group'

    fill_in 'blank_project_name', with: 'test project'

    page.within '[data-testid="url-group-path"]' do
      expect(page).to have_content('test-group')
    end

    page.within '[data-testid="url-project-path"]' do
      expect(page).to have_content('test-project')
    end

    click_on 'Create project'

    expect(page).to have_content('Get started with GitLab Ready to get started with GitLab?')
  end
end
