# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'New/edit issue', :js, feature_category: :team_planning do
  include ActionView::Helpers::JavaScriptHelper
  include ListboxHelpers
  include ContentEditorHelpers

  let_it_be(:project)   { create(:project, :repository) }
  let_it_be(:user)      { create(:user) }
  let_it_be(:user2)     { create(:user) }
  let_it_be(:guest)     { create(:user) }
  let_it_be(:milestone) { create(:milestone, project: project) }
  let_it_be(:label)     { create(:label, project: project) }
  let_it_be(:label2)    { create(:label, project: project) }
  let_it_be(:scoped_label) { create(:label, name: 'priority::value', project: project) }
  let_it_be(:scoped_label2) { create(:label, name: 'priority::value2', project: project) }
  let_it_be(:issue)     { create(:issue, project: project, assignees: [user], milestone: milestone) }
  let_it_be(:issue2)    { create(:issue, project: project, assignees: [user], milestone: milestone) }
  let_it_be(:confidential_issue) do
    create(:issue, project: project, assignees: [user], milestone: milestone, confidential: true)
  end

  let_it_be(:options) do
    {
      "issue" => [{
        "scope" => "priority",
        "required" => true
      }],
      "epic" => [{
        "scope" => "sub_type",
        "required" => true
      }]
    }
  end

  let(:current_user) { user }

  before_all do
    project.add_maintainer(user)
    project.add_maintainer(user2)
    project.add_guest(guest)
  end

  before do
    stub_feature_flags(visible_label_selection_on_metadata: false)
    stub_licensed_features(multiple_issue_assignees: false, issue_weights: false)

    sign_in(current_user)
  end

  describe 'new issue' do
    before do
      stub_feature_flags(jh_custom_labels: true)
      stub_jh_application_setting(jh_custom_labels: options)
      visit new_project_issue_path(project)
      close_rich_text_promo_popover_if_present
    end

    it 'allows user to create new issue' do
      fill_in 'issue_title', with: 'title'
      fill_in 'issue_description', with: 'title'

      expect(find('a', text: 'Assign to me')).to be_visible
      click_button 'Unassigned'

      wait_for_requests

      page.within '.dropdown-menu-user' do
        click_link user2.name
      end
      expect(find('input[name="issue[assignee_ids][]"]', visible: false).value).to match(user2.id.to_s)
      page.within '.js-assignee-search' do
        expect(page).to have_content user2.name
      end
      expect(find('a', text: 'Assign to me')).to be_visible

      click_link 'Assign to me'
      assignee_ids = page.all('input[name="issue[assignee_ids][]"]', visible: false)

      expect(assignee_ids[0].value).to match(user.id.to_s)

      page.within '.js-assignee-search' do
        expect(page).to have_content user.name
      end
      expect(find('a', text: 'Assign to me', visible: false)).not_to be_visible

      click_button 'Select milestone'
      click_button milestone.title
      expect(find('input[name="issue[milestone_id]"]', visible: false).value).to match(milestone.id.to_s)
      expect(page).to have_button milestone.title

      click_button 'Labels'
      page.within '.dropdown-menu-labels' do
        click_link label.title
        click_link label2.title
      end

      find('.js-issuable-form-dropdown.js-label-select').click

      page.within '.js-label-select' do
        expect(page).to have_content label.title
      end
      expect(page.all('input[name="issue[label_ids][]"]', visible: false)[2].value).to match(label.id.to_s)
      expect(page.all('input[name="issue[label_ids][]"]', visible: false)[3].value).to match(label2.id.to_s)

      click_button format(s_('JH|CustomLabels|Select %{scope}'), scope: scoped_label.scoped_label_key)

      page.within "[data-testid='listbox-#{scoped_label.scoped_label_key}']" do
        find("[data-testid='listbox-item-#{scoped_label.id}']").click
      end

      click_button 'Create issue'

      page.within '.issuable-sidebar' do
        page.within '.assignee' do
          expect(page).to have_content "Assignee"
        end

        page.within '.milestone' do
          expect(page).to have_content milestone.title
        end

        page.within '.labels' do
          expect(page).to have_content label.title
          expect(page).to have_content label2.title
          expect(page).to have_content scoped_label.title
        end
      end

      page.within '.breadcrumbs' do
        issue = Issue.find_by(title: 'title')

        expect(page).to have_text("Issues #{issue.to_reference}")
      end
    end

    it 'displays an error message when submitting an invalid form' do
      fill_in 'issue_title', with: 'title'

      click_button 'Create issue'

      page.within "[data-testid='new-fields-col']" do
        expect(page).to have_text(_('This field is required'))
      end
    end
  end
end
