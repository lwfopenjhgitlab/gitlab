# frozen_string_literal: true

require 'spec_helper'

RSpec.describe SessionsController do
  include SetDefaultPreferredLanguage

  describe '#create' do
    let(:user) { create(:user) }
    let(:wrong_password) { "wrong+#{user.password}" }

    subject(:perform_request) do
      post(
        user_session_path,
        params: { user: { login: user.username, password: wrong_password } }
      )
    end

    context 'when the I18n.locale is zh_CN ' do
      before do
        set_default_preferred_language_to_zh_cn
      end

      context 'when Self-managed environment' do
        it 'invalid password' do
          perform_request

          expect(flash[:alert]).to include '无效的登录信息或密码。'
        end
      end

      context 'when Saas environment', :saas do
        it 'invalid password' do
          perform_request

          expect(flash[:alert]).to include '密码错误或该账户不存在，请检查后重试，非中国大陆手机号需添加地区码。'
        end
      end
    end
  end
end
