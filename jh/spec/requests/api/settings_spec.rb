# frozen_string_literal: true

require 'spec_helper'

RSpec.describe API::Settings, 'JH Settings', feature_category: :api do
  let(:admin) { create(:admin) }

  before do
    stub_application_setting(admin_mode: false)
  end

  describe 'password expiration settings' do
    using RSpec::Parameterized::TableSyntax
    let(:settings) do
      {
        password_expiration_enabled: true,
        password_expires_in_days: 100,
        password_expires_notice_before_days: 17
      }
    end

    let(:attribute_names) { settings.keys.map(&:to_s) }

    # rubocop:disable Lint/BinaryOperatorWithIdenticalOperands
    where(:licensed_feature, :feature_flag, :visible) do
      true        |     true    |   true
      true        |     false   |   false
      false       |     true    |   false
      false       |     false   |   false
    end
    # rubocop:enable Lint/BinaryOperatorWithIdenticalOperands

    with_them do
      before do
        stub_licensed_features(password_expiration: licensed_feature)
        stub_feature_flags(password_expiration: feature_flag)
        # Make sure the settings exist before the specs
        get api("/application/settings", admin)
      end

      it 'works as expected' do
        get api("/application/settings", admin)
        expect(response).to have_gitlab_http_status(:ok)

        if visible
          expect(json_response.keys).to include(*attribute_names)
        else
          expect(json_response.keys).not_to include(*attribute_names)
        end
      end

      it 'works as expected' do
        if visible
          expect { put api("/application/settings", admin), params: settings }
            .to change { ApplicationSetting.current.reset.attributes.slice(*attribute_names) }
        else
          expect { put api("/application/settings", admin), params: settings }
            .not_to change { ApplicationSetting.current.reset.attributes.slice(*attribute_names) }
        end
      end
    end
  end
end
