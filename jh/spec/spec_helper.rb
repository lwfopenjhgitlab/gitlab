# frozen_string_literal: true

require Rails.root.join("spec/support/helpers/stub_requests.rb")

# JH support helpers has been loaded, see: jh/config/initializers/require_jh_spec_support_helpers.rb
Dir[Rails.root.join("jh/spec/support/helpers/**/*.rb")].each { |f| require f }

Dir[Rails.root.join("jh/spec/support/shared_contexts/**/*.rb")].each { |f| require f }
Dir[Rails.root.join("jh/spec/support/shared_examples/**/*.rb")].each { |f| require f }
Dir[Rails.root.join("jh/spec/support/**/*.rb")].each { |f| require f }

require_relative '../lib/jh/skip_specs'

config_path = File.expand_path("config/skip_specs.yml", __dir__)
skip_specs = JH::SkipSpecs.new(config_path)

RSpec.configure do |config|
  config.before do |example|
    # We use the tag `phone_verification_code_enabled` to enable phone verification code in tests.
    # Add it to your test if you want to enable the phone verification code feature.
    # For example: describe '...', :phone_verification_code_enabled do
    # But we do not mock application_settings on test that use tag `do_not_mock_admin_mode_setting`
    if example.metadata[:phone_verification_code_enabled]
      stub_application_setting(phone_verification_code_enabled: true)
    end

    # Set default value of `JH_AI_PROVIDER`
    stub_env('JH_AI_PROVIDER', example.metadata[:ai_provider].presence || :open_ai)
  end

  if skip_specs.skipped_list.any?
    config.around do |example|
      Timeout.timeout(900) { example.run } unless skip_specs.skipped?(example)
    end
  end
end
