# frozen_string_literal: true

require "spec_helper"

RSpec.describe Billing::PlanComponent, :aggregate_failures, type: :component do
  include SubscriptionPortalHelpers

  let(:namespace) { build(:group) }
  let(:plans_data) { billing_plans_data.map { |plan| Hashie::Mash.new(plan) } }
  let(:plan) { plans_data.detect { |x| x.code == plan_name } }

  subject(:component) { described_class.new(plan: plan, namespace: namespace) }

  before do
    allow(component).to receive(:plan_purchase_url).and_return('_purchase_url_')

    render_inline(component)
  end

  shared_examples 'plan tracking' do
    it 'has expected tracking attributes' do
      css = "[data-track-action='click_button'][data-track-label='plan_cta'][data-track-property='#{plan_name}']" \
            "[data-track-experiment='promote_premium_billing_page']"

      expect(page).to have_css(css)
    end
  end

  context 'with free plan' do
    let(:plan_name) { 'free' }

    it 'has pricing info' do
      expect(page).to have_content('¥0')
      expect(page).not_to have_content('Billed annually')
    end

    it 'has expected feature titles' do
      expected_titles = ['Spans the DevOps lifecycle', 'Open Source - MIT License',
        'Bring your own JiHu GitLab CI runners', '2GB storage per project',
        '400 compute minutes per month']
      expect_to_plan_feature_titles(component.send(:plan), expected_titles)
    end

    it 'has expected annual_price_text' do
      expected_annual_price_text = "Billed annually at #{component.send(:price_per_year)}"
      expect(component.send(:annual_price_text)).to eq(expected_annual_price_text)
    end
  end

  context 'with premium plan' do
    let(:plan_name) { 'premium' }

    it 'has expected feature titles' do
      expected_titles = ['Everything from Free', 'Efficient code review',
        'Advanced CI/CD', 'Enterprise agile management', 'Release controls',
        'Self-managed reliability', '10,000 compute minutes per month',
        'Professional technical support', '5GB storage per project']
      expect_to_plan_feature_titles(component.send(:plan), expected_titles)
    end
  end

  context 'with ultimate plan' do
    let(:plan_name) { 'ultimate' }

    it 'has expected feature titles' do
      expected_titles = ['Everything from Premium', 'Advanced security testing',
        'Security risk mitigation', 'Compliance', 'Portfolio management',
        'Value stream analysis', 'Free guest users', '50,000 compute minutes per month',
        'Professional technical support', '5GB storage per project']
      expect_to_plan_feature_titles(component.send(:plan), expected_titles)
    end
  end

  def expect_to_plan_feature_titles(plan, expected_titles)
    titles = plan['features'].map { |feature| feature.fetch('title', '') }
    expect(titles).to eq(expected_titles)
  end
end
