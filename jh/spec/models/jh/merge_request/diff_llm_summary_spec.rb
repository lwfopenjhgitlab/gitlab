# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::MergeRequest::DiffLlmSummary, feature_category: :code_review_workflow do
  let_it_be_with_reload(:project) { create(:project, :repository) }

  describe '#provider' do
    ::Gitlab::Llm::ClientFactory::JH_PROVIDERS.each do |ai_provider|
      it 'returns correct enum' do
        expect(described_class.providers[ai_provider]).not_to be_nil
        expect(build(:merge_request_diff_llm_summary, provider: ai_provider).provider).to eq ai_provider.to_s
      end
    end
  end
end
