# frozen_string_literal: true

require 'spec_helper'

RSpec.describe MergeRequests::ExternalStatusCheck, type: :model do
  describe '#status with skipped check response' do
    let_it_be(:rule) { create(:external_status_check) }
    let_it_be(:merge_request) { create(:merge_request) }

    subject { rule.status(merge_request, merge_request.source_branch_sha) }

    context 'with feature flag enabled' do
      context 'when a rule has a positive then a skipped status check response' do
        before do
          create(:status_check_response, merge_request: merge_request,
            external_status_check: rule, sha: 'abc1234', status: 'passed')
          create(:status_check_response, merge_request: merge_request,
            external_status_check: rule, sha: merge_request.source_branch_sha, status: 'skipped')
        end

        it { is_expected.to eq('passed') }
      end

      context 'when a rule has a negative then a skipped status check response' do
        before do
          create(:status_check_response, merge_request: merge_request, external_status_check: rule,
            sha: 'abc1234', status: 'failed')
          create(:status_check_response, merge_request: merge_request, external_status_check: rule,
            sha: merge_request.source_branch_sha, status: 'skipped')
        end

        it { is_expected.to eq('failed') }
      end

      context 'when a rule has nothing but only skipped status check response' do
        before do
          create(:status_check_response, merge_request: merge_request, external_status_check: rule,
            sha: merge_request.source_branch_sha, status: 'skipped')
        end

        it { is_expected.to eq('pending') }
      end
    end

    context 'with feature flag disabled' do
      before do
        stub_feature_flags(rebase_without_pipeline_and_status_check: false)

        create(:status_check_response, merge_request: merge_request, external_status_check: rule,
          sha: 'abc1234', status: 'skipped')
      end

      context 'when a rule has a positive status check response' do
        before do
          create(:status_check_response, merge_request: merge_request,
            external_status_check: rule, sha: merge_request.source_branch_sha, status: 'passed')
        end

        it { is_expected.to eq('passed') }
      end

      context 'when a rule has a negative status check response' do
        before do
          create(:status_check_response, merge_request: merge_request, external_status_check: rule,
            sha: merge_request.source_branch_sha, status: 'failed')
        end

        it { is_expected.to eq('failed') }
      end

      context 'when a rule has nothing but only skipped status check response' do
        it { is_expected.to eq('pending') }
      end
    end
  end
end
