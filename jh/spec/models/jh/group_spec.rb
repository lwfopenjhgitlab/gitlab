# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Group do
  describe 'validations' do
    it_behaves_like "content validation", :group, :name
    it_behaves_like "content validation", :group, :description
  end

  describe '#actual_size_limit', :saas do
    let_it_be(:group) { create(:group) }
    let_it_be(:ultimate_limits) { create(:plan_limits, plan: create(:ultimate_plan), repository_size: 100) }
    let_it_be(:ultimate_group) { create(:group_with_plan, plan: :ultimate_plan) }

    before do
      allow(::Gitlab).to receive(:jh?).and_return(true)
    end

    it 'returns the actual size limit' do
      expect(ultimate_group.actual_size_limit).to eq(100)
      expect(group.actual_size_limit).to eq(::Gitlab::CurrentSettings.repository_size_limit)
    end
  end
end
