# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Namespace, :saas do
  describe '#actual_size_limit' do
    let_it_be(:namespace) { create(:namespace) }
    let_it_be(:ultimate_limits) { create(:plan_limits, plan: create(:ultimate_plan), repository_size: 100) }
    let_it_be(:ultimate_namespace) { create(:namespace_with_plan, plan: :ultimate_plan) }

    before do
      allow(::Gitlab).to receive(:jh?).and_return(true)
    end

    it 'returns the actual size limit' do
      expect(ultimate_namespace.actual_size_limit).to eq(100)
      expect(namespace.actual_size_limit).to eq(::Gitlab::CurrentSettings.repository_size_limit)
    end
  end
end
