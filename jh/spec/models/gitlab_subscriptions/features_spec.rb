# frozen_string_literal: true

require 'spec_helper'

RSpec.describe GitlabSubscriptions::Features do
  describe 'JH specific license features' do
    let(:plan) {} # rubocop:disable Lint/EmptyBlock

    subject(:features) do
      described_class.features(plan: plan, add_ons: {})
    end

    context 'with starter plan' do
      let(:plan) { License::STARTER_PLAN }

      it "includes JH specific starter feature" do
        expect(features).to contain_exactly(*described_class::ALL_STARTER_FEATURES)

        expect(features).not_to include(*described_class::JH_PREMIUM_ADDITION)
        expect(features).not_to include(*described_class::JH_ULTIMATE_ADDITION)
      end
    end

    context 'with premium plan' do
      let(:plan) { License::PREMIUM_PLAN }

      it "includes JH specific premium feature" do
        expect(features).to contain_exactly(*described_class::ALL_PREMIUM_FEATURES)
        expect(features).to include(*described_class::ALL_STARTER_FEATURES)

        expect(features).to include(*described_class::JH_STARTER_FEATURES)
        expect(features).to include(*described_class::EE_STARTER_FEATURES)
        expect(features).to include(*described_class::JH_PREMIUM_ADDITION)
        expect(features).not_to include(*described_class::JH_ULTIMATE_ADDITION)
      end
    end

    context 'with ultimate plan' do
      let(:plan) { License::ULTIMATE_PLAN }

      it "includes JH specific ultimate feature" do
        expect(features).to include(*described_class::ALL_STARTER_FEATURES)
        expect(features).to include(*described_class::ALL_PREMIUM_FEATURES)
        expect(features).to contain_exactly(*described_class::ALL_ULTIMATE_FEATURES)
        expect(features).to include(*described_class::ULTIMATE_FEATURES_WITH_USAGE_PING)

        expect(features).to include(*described_class::JH_PREMIUM_ADDITION)
        expect(features).to include(*described_class::JH_ULTIMATE_ADDITION)
      end
    end

    context 'for password_expiration' do
      it 'JH_STARTER_FEATURES should contain password_expiration' do
        expect(described_class::JH_STARTER_FEATURES).to include(:password_expiration)
      end
    end
  end

  it 'ensures that there is no same names between licensed features and feature flags', :aggregate_failures do
    all_features = GitlabSubscriptions::Features::FEATURES_BY_PLAN.values.flatten
    skipped = %i[group_protected_branches password_expiration performance_analytics]

    all_features.each do |licensed_feature|
      next if licensed_feature.in?(skipped)

      expect { Feature.enabled?(licensed_feature) }.to raise_error(Feature::InvalidFeatureFlagError),
        "Licensed feature '#{licensed_feature}' has a matching feature flag. Please rename the feature or the FF"
    end
  end
end
