# frozen_string_literal: true

module PostHogMock
  # mock client in posthog-ruby Gem
  module Client
    def initialize(_options) end

    # compatible with spec/support/snowplow.rb
    def flush
      true
    end

    def capture(_params)
      true
    end
  end

  # mock tracker in Destinations::PostHog
  module PostHog
    def enabled?
      Gitlab::CurrentSettings.snowplow_enabled?
    end
  end
end

::PostHog::Client.prepend PostHogMock::Client
::Gitlab::Tracking::Destinations::PostHog.prepend PostHogMock::PostHog
