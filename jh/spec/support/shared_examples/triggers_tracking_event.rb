# frozen_string_literal: true

RSpec.shared_examples 'triggers tracking event' do
  context 'when tracking is enabled' do
    before do
      allow(::Gitlab::Tracking).to receive(:enabled?).and_return(true)
    end

    it 'triggers the event' do
      expect(::Gitlab::Tracking).to have_received(:event).at_least(:once)
    end
  end
end
