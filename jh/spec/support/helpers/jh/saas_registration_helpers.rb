# frozen_string_literal: true

module JH
  module SaasRegistrationHelpers
    extend ::Gitlab::Utils::Override

    override :user_signs_up_with_sso
    def user_signs_up_with_sso(params = {}, provider: 'gitlab', name: 'Registering User')
      super(params, provider: provider, name: name)
    end

    override :user_signs_up_through_trial_with_sso
    def user_signs_up_through_trial_with_sso(params = {}, provider: 'gitlab', name: 'Registering User')
      super(params, provider: provider, name: name)
    end

    override :fill_in_checkout_form
    def fill_in_checkout_form
      if user.setup_for_company
        within_fieldset('Name of company or organization using GitLab') do
          fill_in with: 'Test company'
        end
      end

      click_button 'Continue to billing'

      within_fieldset('Country') do
        select 'United States of America'
      end

      within_fieldset('Street address') do
        first("input[type='text']").fill_in with: '123 fake street'
      end

      # SKIP on JH Side
      # within_fieldset('City') do
      #   fill_in with: 'Fake city'
      # end

      within_fieldset('State') do
        select 'Florida'
      end

      # within_fieldset('Zip code') do
      #   fill_in with: 'A1B 2C3'
      # end

      click_button 'Continue to payment'

      stub_confirm_purchase
    end

    override :stub_confirm_purchase
    def stub_confirm_purchase
      allow_next_instance_of(::GitlabSubscriptions::CreateService) do |instance|
        allow(instance).to receive(:execute).and_return({ success: true, data: nil })
      end

      expect(::GitlabSubscriptions::CreateService).to receive(:new).with(
        user,
        group: an_instance_of(::Group),
        customer_params: customer_params,
        subscription_params: subscription_params
      )

      # this is an ad-hoc solution to skip the zuora step and allow 'confirm purchase' button to show up
      page.execute_script <<~JS
        document.querySelector('[data-testid="subscription_app"]').__vue__.$store.dispatch('fetchPaymentMethodDetailsSuccess')
      JS

      click_button 'Confirm purchase'
    end

    override :customer_params
    def customer_params
      company = user.setup_for_company ? 'Test company' : nil

      ActionController::Parameters.new(
        country: 'US',
        address_1: '123 fake street',
        address_2: nil,
        city: nil,
        state: 'FL',
        zip_code: nil,
        company: company
      ).permit!
    end
  end
end
