# frozen_string_literal: true

module JH
  module EverySidekiqWorkerTestHelper
    extend ::Gitlab::Utils::Override

    override :extra_retry_exceptions
    def extra_retry_exceptions
      {
        'ContentValidation::CommitServiceWorker' => 3
      }
    end
  end
end
