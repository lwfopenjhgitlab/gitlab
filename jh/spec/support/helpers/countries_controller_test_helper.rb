# frozen_string_literal: true

module JH
  module CountriesControllerTestHelper
    def world_deny_list
      ::World::DENYLIST
    end
  end
end
