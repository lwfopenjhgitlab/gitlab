# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ProjectsHelper, feature_category: :source_code_management do
  include ProjectForksHelper
  include AfterNextHelpers

  let_it_be_with_reload(:project) { create(:project) }
  let_it_be(:user) { create(:user) }

  before do
    helper.instance_variable_set(:@project, project)
  end

  describe '#project_permissions_panel_data', :saas do
    subject { helper.project_permissions_panel_data(project) }

    before do
      allow(helper).to receive(:can?).and_return(true)
      allow(helper).to receive(:current_user).and_return(user)
      allow(::Gitlab::Pages).to receive(:access_control_is_forced?).and_return(true)
    end

    it 'disable pages access control' do
      expect(subject).to include(
        pagesAccessControlEnabled: false
      )
    end
  end
end
