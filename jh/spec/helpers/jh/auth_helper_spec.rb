# frozen_string_literal: true

require "spec_helper"

RSpec.describe AuthHelper do
  describe 'popular_enabled_button_based_providers', :saas do
    it 'returns the intersection set of popular & enabled providers' do
      allow(helper).to receive(:enabled_button_based_providers).and_return(%w[twitter github gitlab])
      expect(helper.popular_enabled_button_based_providers).to eq(%w[github gitlab])

      allow(helper).to receive(:enabled_button_based_providers).and_return(%w[gitlab bitbucket])
      expect(helper.popular_enabled_button_based_providers).to eq(%w[gitlab])

      allow(helper).to receive(:enabled_button_based_providers).and_return(%w[bitbucket])
      expect(helper.popular_enabled_button_based_providers).to be_empty

      allow(helper).to receive(:enabled_button_based_providers).and_return(%w[gitlab github dingtalk alicloud])
      expect(helper.popular_enabled_button_based_providers).to eq(%w[gitlab github dingtalk alicloud])
    end

    it 'returns enabled providers with sorted order' do
      allow(helper).to receive(:button_based_providers).and_return(%w[alicloud dingtalk gitlab github])
      expect(helper.enabled_button_based_providers).to eq(%w[github gitlab dingtalk alicloud])
    end
  end
end
