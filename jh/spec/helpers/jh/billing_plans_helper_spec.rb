# frozen_string_literal: true

require 'spec_helper'

RSpec.describe BillingPlansHelper do
  describe '#number_to_plan_currency' do
    it 'returns the correct value and unit' do
      expect(helper.number_to_plan_currency(3.14)).to eq '¥3.14'
    end
  end

  describe '#use_new_purchase_flow?' do
    describe "when jh_new_purchase_flow feature is disabled" do
      before do
        stub_feature_flags(jh_new_purchase_flow: false)
      end

      it 'returns false in JiHu' do
        expect(helper.use_new_purchase_flow?(1)).to be(false)
      end
    end

    describe "when jh_new_purchase_flow feature is enabled" do
      where type: [Group.sti_name, Namespaces::UserNamespace.sti_name],
        plan: Plan.all_plans,
        trial_active: [true, false]

      with_them do
        let_it_be(:user) { create(:user) }
        let(:namespace) { create(:namespace_with_plan, plan: "#{plan}_plan".to_sym, type: type) }

        before do
          stub_feature_flags(jh_new_purchase_flow: true)
          allow(Gitlab).to receive(:com?).and_return(true)
          allow(helper).to receive(:current_user).and_return(user)
          allow(namespace).to receive(:trial_active?).and_return(trial_active)
        end

        subject { helper.use_new_purchase_flow?(namespace) }

        it do
          result = type == Group.sti_name && (plan == Plan::FREE || trial_active)

          is_expected.to be(result)
        end
      end
    end
  end
end
