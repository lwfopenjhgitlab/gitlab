# frozen_string_literal: true

require 'spec_helper'

RSpec.describe JH::LicenseHelper do
  describe '#new_trial_url' do
    it "return JH_Doc url" do
      expect(helper.new_trial_url).to eq(::Gitlab::SubscriptionPortal.free_trial_url)
    end
  end
end
