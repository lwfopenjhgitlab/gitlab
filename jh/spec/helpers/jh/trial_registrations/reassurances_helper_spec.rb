# frozen_string_literal: true

require 'spec_helper'

RSpec.describe JH::TrialRegistrations::ReassurancesHelper do
  describe '#reassurance_logo_data' do
    it 'returns a collection of data in the expected format' do
      expect(reassurance_logo_data).to all(match({
        name: an_instance_of(String),
        w: an_instance_of(Integer),
        image_path: a_string_matching(
          %r{\Athird_party_logos/[a-z0-9_]+\.svg\z}
        ),
        image_alt_text: a_string_ending_with(' logo')
      }))
    end
  end
end
