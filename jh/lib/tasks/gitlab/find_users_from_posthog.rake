# frozen_string_literal: true

# Based on `Posthog`, get user information under specific conditions.
# Acceptable parameters are `personal_api_key` `date_from` and `date_to`.
# The `date_to` is optional and defaults to `Date.tomorrow.to_s`.
#
# Usage:
#   $ bundle exec rake gitlab:features:find_users_from_posthog -- --posthog-api-key=<posthog-api-key>
#
# Args:
#   --posthog-api-key (String)
#   --date-from (String), defaults to `(Date.today - 14).to_s`
#   --date-to (String), defaults to `Date.tomorrow.to_s`
#   --limit (Integer), defaults to `10000`
#   --utm-source (String), defaults to `migrate`
#   --project-id (String), defaults to `2`
#   --posthog-url (String), defaults to `https://jhposthog.gitlab.cn`

namespace :gitlab do
  namespace :features do
    task find_users_from_posthog: :environment do
      default_options = {
        date_from: (Date.today - 14).to_s,
        date_to: Date.tomorrow.to_s,
        limit: 10000,
        utm_source: "migrate",
        project_id: 2,
        posthog_url: "https://jhposthog.gitlab.cn"
      }
      options = default_options
      opts = OptionParser.new
      opts.banner = "title~"
      opts.on("--posthog-api-key=posthog_api_key", String) { |v| options[:posthog_api_key] = v }
      opts.on("--date-from=date_from", String) { |v| options[:date_from] = v }
      opts.on("--date-to=date_to", String) { |v| options[:date_to] = v }
      opts.on("--limit=limit", Integer) { |v| options[:limit] = v }
      opts.on("--utm-source=utm_source", String) { |v| options[:utm_source] = v }
      opts.on("--project-id=project_id", Integer) { |v| options[:project_id] = v }
      opts.on("--posthog-url=posthog_url", String) { |v| options[:posthog_url] = v }
      args = opts.order!(ARGV) {} # rubocop:disable Lint/EmptyBlock
      opts.parse!(args)

      user_ids = Posthog::Query.get_user_ids(
        posthog_url: options[:posthog_url],
        posthog_api_key: options[:posthog_api_key],
        utm_source: options[:utm_source],
        project_id: options[:project_id],
        limit: options[:limit],
        date_from: options[:date_from],
        date_to: options[:date_to]
      )
      puts "user_ids = #{user_ids}"
      puts Posthog::Query.users_to_csv(User.where(id: user_ids))
    end
  end
end
