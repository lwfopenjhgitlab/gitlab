# frozen_string_literal: true

module Jihulab
  # This client is for JiHu GitLab API: https://docs.gitlab.cn/jh/api/
  # You can use the GDK/staging/production environment by changing the endpoint and token
  class Client
    attr_reader :endpoint, :token, :options

    def self.build(...)
      new(...).build
    end

    def initialize(endpoint, token, options = {})
      @endpoint = endpoint
      @token = token
      @options = options.with_defaults(
        logger: ::Logger.new($stdout)
      )
    end

    def build
      Class.new(::Gitlab::HTTP).tap do |klass|
        klass.base_uri endpoint
        klass.headers headers
        klass.default_options.merge!(options)
      end
    end

    private

    def headers
      { "Content-Type" => "application/json", "PRIVATE-TOKEN" => token }
    end
  end
end
