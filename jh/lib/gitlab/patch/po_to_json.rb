# frozen_string_literal: true

module Gitlab
  module Patch
    class PoToJson < PoToJson
      attr_accessor :jh_translation_cache

      # This is required for processing jh related cache translations
      # https://github.com/webhippie/po_to_json/blob/master/lib/po_to_json.rb#L41
      def initialize(files, jh_translation_cache, glue = "|")
        @files = files
        @glue = glue
        @jh_translation_cache = jh_translation_cache
      end

      # This is required to modify the JS locale file output to our import needs
      # Overwrites: https://github.com/webhippie/po_to_json/blob/master/lib/po_to_json.rb#L46
      def generate_for_jed(language, overwrite = {})
        @options = parse_options(overwrite.merge(language: language))
        if jh_lang_not_set?(language)
          jh_translation_cache[language] = parse_document
          return ''
        end

        @parsed ||= inject_meta(parse_document)
        @parsed = inject_cache(jh_translation_cache[language], @parsed) if jh_lang?(language)

        generated = build_json_for(build_jed_for(@parsed))
        [
          "window.translations = #{generated};"
        ].join(" ")
      end

      def jh_lang_not_set?(language)
        jh_lang?(language) && jh_translation_cache[language].nil?
      end

      def jh_lang?(language)
        Gitlab.jh? && jh_translation_cache.has_key?(language)
      end

      def inject_cache(hash, cache)
        hash.merge(cache)
      end
    end
  end
end
