# frozen_string_literal: true

module Gitlab
  module Patch
    module GettextI18nRailsJsTask
      extend ActiveSupport::Concern

      class_methods do
        def files_list
          super +
            ::Pathname.glob(Rails.root.join('jh/locale/*/gitlab.po'))
        end

        def jh_translation_cache
          @jh_translation_cache ||= { 'zh_CN' => nil, 'zh_HK' => nil }
        end

        def json_for(file)
          ::Gitlab::Patch::PoToJson.new(
            file,
            jh_translation_cache
          ).generate_for_jed(
            lang_for(file),
            GettextI18nRailsJs.config.jed_options
          )
        end
      end
    end
  end
end
