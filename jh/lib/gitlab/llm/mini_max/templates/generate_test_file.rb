# frozen_string_literal: true

module Gitlab
  module Llm
    module MiniMax
      module Templates
        class GenerateTestFile
          OUTPUT_TOKEN_LIMIT = 1000
          TEMPERATURE = 0.3

          def initialize(merge_request, path)
            @merge_request = merge_request
            @path = path
          end

          def to_prompt
            [{
              sender_type: "USER",
              sender_name: "user",
              text: <<~PROMPT
                如果 #{path} 文件包含代码，请为其编写单元测试代码来确保其正确运行
                """
                #{file_contents}
                """
              PROMPT
            }]
          end

          def options(_client)
            {
              tokens_to_generate: OUTPUT_TOKEN_LIMIT,
              temperature: TEMPERATURE,
              bot_setting: [
                {
                  bot_name: "JIHU_BOT",
                  content: "你是一个软件研发专家，可以为代码生成单元测试"
                }
              ]
            }
          end

          private

          attr_reader :merge_request, :path

          def file_contents
            file = merge_request.diffs.diff_files.find { |file| file.paths.include?(path) }

            file&.blob&.data
          end
        end
      end
    end
  end
end
