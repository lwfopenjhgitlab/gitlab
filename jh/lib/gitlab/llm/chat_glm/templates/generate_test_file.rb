# frozen_string_literal: true

module Gitlab
  module Llm
    module ChatGlm
      module Templates
        class GenerateTestFile
          TOTAL_MODEL_TOKEN_LIMIT = 3000
          OUTPUT_TOKEN_LIMIT = 1000

          def initialize(merge_request, path)
            @merge_request = merge_request
            @path = path
          end

          def to_prompt
            <<~PROMPT
              如果 #{path} 文件包含代码，请为其编写单元测试代码来确保其正确运行
              """
              #{file_contents}
              """
            PROMPT
          end

          def options(_client)
            { moderated: true, max_tokens: OUTPUT_TOKEN_LIMIT }
          end

          private

          attr_reader :merge_request, :path

          def file_contents
            file = merge_request.diffs.diff_files.find { |file| file.paths.include?(path) }

            file&.blob&.data
          end
        end
      end
    end
  end
end
