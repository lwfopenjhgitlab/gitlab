# frozen_string_literal: true

module JH
  module VersionCheck
    extend ActiveSupport::Concern

    class_methods do
      extend ::Gitlab::Utils::Override

      override :host
      def host
        ENV['VERSION_DOT_HOST'] || ::ServicePing::SubmitService::JH_BASE_URL
      end
    end
  end
end
