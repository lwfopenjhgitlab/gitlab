# frozen_string_literal: true

module JH
  module Gitlab
    CN_URL = 'https://jihulab.com'

    def self.hk?
      ENV['SAAS_REGION'] == 'HK'
    end
  end
end
