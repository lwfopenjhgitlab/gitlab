# frozen_string_literal: true

module JH
  module Gitlab
    module Tracking
      extend ActiveSupport::Concern

      class_methods do
        extend ::Gitlab::Utils::Override

        override :tracker
        def tracker
          @tracker ||= ::Gitlab::Tracking::Destinations::PostHog.new
        end
      end
    end
  end
end
