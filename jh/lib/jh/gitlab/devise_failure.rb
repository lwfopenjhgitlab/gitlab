# frozen_string_literal: true

module JH
  module Gitlab
    module DeviseFailure
      include PreferredLanguageSwitcher
      extend ::Gitlab::Utils::Override

      protected

      override :i18n_message
      def i18n_message(default = nil)
        message = warden_message || default

        ::Gitlab::I18n.with_locale(locale) do
          if ::Gitlab.jh? &&
              ::Gitlab.com? &&
              [:invalid, :not_found_in_database].include?(message) &&
              ::Feature.enabled?(:phone_authenticatable)
            phone_login_invalid_message
          else
            super(default)
          end
        end
      end

      private

      def phone_login_invalid_message
        s_('JH|Your password is incorrect or this account doesn\'t ' \
           'exist. Please check and try again. Add country calling codes ' \
           'if your mobile phone number is outside the Chinese mainland.')
      end

      def locale
        preferred_language
      end
    end
  end
end
