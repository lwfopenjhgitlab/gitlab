# frozen_string_literal: true

module JH
  module Gitlab
    module GonHelper
      extend ::Gitlab::Utils::Override

      override :add_gon_variables
      def add_gon_variables
        super

        gon.recaptcha_enabled = ::Gitlab::CurrentSettings.recaptcha_enabled
        gon.tencent_captcha_enabled = true
        gon.tencent_captcha_replacement_enabled = ::JH::Captcha::TencentCloud.enabled?
        gon.tencent_captcha_app_id = ENV['TC_CAPTCHA_APP_ID']
        gon.geetest_captcha_id = ENV['GEETEST_CAPTCHA_ID']
        gon.geetest_captcha_replacement_enabled = ::JH::Captcha::Geetest.enabled?
        gon.real_name_system = ::Gitlab::RealNameSystem.enabled?
        gon.phone_registration = ::Feature.enabled?(:registrations_with_phone)
        gon.customer_support_url = ::Gitlab::Saas.customer_support_url
        gon.posthog_api_key = ENV['POSTHOG_API_KEY'] if ::Gitlab.com?
        gon.posthog_api_host = ENV['POSTHOG_API_HOST'] if ::Gitlab.com?
        gon.content_validation_enabled = ::ContentValidation::Setting.content_validation_enable?
        gon.disable_download_button = ::Gitlab::CurrentSettings.disable_download_button_enabled?
        gon.jh_custom_labels_enabled = ::Gitlab::CurrentSettings.jh_custom_labels_enabled?
        gon.jh_custom_labels = ::Gitlab::CurrentSettings.jh_custom_labels
      end
    end
  end
end
