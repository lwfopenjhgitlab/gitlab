# frozen_string_literal: true

module JH
  module Gitlab
    module ApplicationRateLimiter
      extend ActiveSupport::Concern

      class_methods do
        extend ::Gitlab::Utils::Override

        override :rate_limits
        def rate_limits
          super.merge({
            email_exists: { threshold: 20, interval: 1.minute }
          })
        end
      end
    end
  end
end
