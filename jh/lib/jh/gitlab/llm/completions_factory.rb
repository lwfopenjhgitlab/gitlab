# frozen_string_literal: true

module JH
  module Gitlab
    module Llm
      module CompletionsFactory
        extend ActiveSupport::Concern

        # TODO:
        # service_class should be general and move out of specific ai module
        # prompt_class should be couple with specific ai module
        EXTENDED_COMPLETIONS = {
          chat_glm: {
            explain_code_open_ai: {
              service_class: ::Gitlab::Llm::ChatGlm::Completions::ExplainCode,
              prompt_class: ::Gitlab::Llm::ChatGlm::Templates::ExplainCode
            },
            generate_test_file: {
              service_class: ::Gitlab::Llm::ChatGlm::Completions::GenerateTestFile,
              prompt_class: ::Gitlab::Llm::ChatGlm::Templates::GenerateTestFile
            }
          },
          mini_max: {
            explain_code_open_ai: {
              service_class: ::Gitlab::Llm::MiniMax::Completions::ExplainCode,
              prompt_class: ::Gitlab::Llm::MiniMax::Templates::ExplainCode
            },
            generate_test_file: {
              service_class: ::Gitlab::Llm::ChatGlm::Completions::GenerateTestFile,
              prompt_class: ::Gitlab::Llm::MiniMax::Templates::GenerateTestFile
            }
          }
        }.freeze

        class_methods do
          extend ::Gitlab::Utils::Override

          override :completion
          def completion(name, params = {})
            return unless completions.key?(name)

            service_class, prompt_class = completions[name].values_at(:service_class, :prompt_class)
            service_class.new(prompt_class, params)
          end

          def completions
            jh_completions = EXTENDED_COMPLETIONS.fetch(::Gitlab::Llm::ClientFactory.ai_provider, {})

            self::COMPLETIONS.merge jh_completions
          end
        end
      end
    end
  end
end
