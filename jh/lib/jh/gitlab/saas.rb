# frozen_string_literal: true

module JH
  module Gitlab
    module Saas
      extend ActiveSupport::Concern

      class_methods do
        extend ::Gitlab::Utils::Override

        def hk?
          Gitlab.hk?
        end

        override :com_url
        def com_url
          Gitlab.hk? ? 'https://gitlab.hk' : 'https://jihulab.com'
        end

        override :staging_com_url
        def staging_com_url
          Gitlab.hk? ? 'https://staging.gitlab.hk' : 'https://staging.jihulab.com'
        end

        override :canary_toggle_com_url
        def canary_toggle_com_url
          Gitlab.hk? ? 'https://next.gitlab.hk' : 'https://next.jihulab.com'
        end

        override :subdomain_regex
        def subdomain_regex
          Gitlab.hk? ? %r{\Ahttps://[a-z0-9-]+\.gitlab\.hk\z} : %r{\Ahttps://[a-z0-9-]+\.jihulab\.com\z}
        end

        override :dev_url
        def dev_url
          'https://dev-ops.gitlab.cn'
        end

        override :registry_prefix
        def registry_prefix
          'registry.gitlab.cn'
        end

        override :customer_support_url
        def customer_support_url
          'https://support.gitlab.cn'
        end

        override :customer_license_support_url
        def customer_license_support_url
          'https://support.gitlab.cn/#/portal/submitticket/3'
        end

        override :gitlab_com_status_url
        def gitlab_com_status_url
          'https://status.gitlab.cn'
        end

        def commom_purchase_url
          'https://about.gitlab.cn/upgrade-plan'
        end

        override :about_pricing_url
        def about_pricing_url
          "https://about.gitlab.cn/pricing"
        end

        override :about_pricing_faq_url
        def about_pricing_faq_url
          "https://about.gitlab.cn/pricing#faq"
        end

        override :about_feature_comparison_url
        def about_feature_comparison_url
          "https://about.gitlab.cn/pricing/saas/feature-comparison"
        end

        override :doc_url
        def doc_url
          'https://docs.gitlab.cn'
        end
      end
    end
  end
end
