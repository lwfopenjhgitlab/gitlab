# frozen_string_literal: true

module JH
  module API
    module Helpers
      module IntegrationsHelpers
        extend ActiveSupport::Concern

        class_methods do
          extend ::Gitlab::Utils::Override

          override :integrations
          def integrations
            super.merge(
              'feishu_bot' => [],
              'dingtalk' => [
                {
                  required: true,
                  name: :corpid,
                  type: String,
                  desc: 'DingTalk corp id'
                }
              ],
              'ones' => ::Integrations::OnesFields.api_fields,
              'ligaai' => ::Integrations::LigaaiFields.api_fields
            )
          end

          override :integration_classes
          def integration_classes
            [
              ::Integrations::FeishuBot,
              ::Integrations::Dingtalk,
              ::Integrations::Ones,
              *super
            ]
          end
        end
      end
    end
  end
end
