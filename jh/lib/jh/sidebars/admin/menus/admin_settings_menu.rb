# frozen_string_literal: true

module JH
  module Sidebars
    module Admin
      module Menus
        module AdminSettingsMenu
          extend ::Gitlab::Utils::Override

          override :configure_menu_items
          def configure_menu_items
            super
            remove_item(service_usage_data_menu_item) if ::Feature.disabled?(:jh_usage_statistics)

            true
          end
        end
      end
    end
  end
end
