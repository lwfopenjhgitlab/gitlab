# frozen_string_literal: true

module JH
  module Sidebars
    module Projects
      module Panel
        extend ::Gitlab::Utils::Override

        override :configure_menus
        def configure_menus
          super

          return unless need_remove_external_issue_tracker?

          remove_menu(::Sidebars::Projects::Menus::ExternalIssueTrackerMenu)
        end

        def need_remove_external_issue_tracker?
          ::Sidebars::Projects::Menus::IssuesMenu.new(context).show_ones_menu_items? ||
            ::Sidebars::Projects::Menus::IssuesMenu.new(context).show_ligaai_menu_items?
        end
      end
    end
  end
end
