# frozen_string_literal: true

module Posthog
  class Query
    class << self
      # @param [String] posthog_url, posthog_api_key, utm_source
      # @param [Integer] project_id, limit
      # @param [Date] date_from, date_to
      # @return [Array<Integer>]
      def get_user_ids(
        posthog_url:, posthog_api_key:, utm_source:, project_id:, limit:, date_from:, date_to:
      )
        posthog_api_caller = Posthog::Client.new(
          posthog_api_key: posthog_api_key,
          posthog_url: posthog_url
        )
        posthog_response = posthog_api_caller.request_persons_funnel(
          utm_source: utm_source,
          project_id: project_id,
          limit: limit,
          date_from: date_from,
          date_to: date_to
        )
        response_body = ::Gitlab::Json.parse(posthog_response.body)

        raise "response is invalid" unless response_body["results"].count == 1

        peoples = response_body["results"][0]["people"]
        peoples.map { |p| p["name"].to_i }
      end

      # params [Array<User>] users
      def users_to_csv(users)
        CSV.generate do |csv|
          csv << %w[id email name created_at]
          users.each do |user|
            csv << [user.id, user.email, user.name, user.created_at]
          end
        end
      end
    end
  end
end
