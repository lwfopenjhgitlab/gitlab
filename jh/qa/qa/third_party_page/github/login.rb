# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Github
      class Login < ::QA::Page::Base
        def go_to_github_oauth
          QA::Runtime::Logger.info('go to login page')
          page.find('#oauth-login-github', text: /GitHub/).click
          QA::Runtime::Logger.info('redirect to github oauth page')
        end

        def login_github(username = '', password = '')
          QA::Runtime::Logger.info('input github username and password')

          if Runtime::User.github_user?
            page.find('#login_field').set(Runtime::Env.qa_oauth_github_username)
            page.find('#password').set(Runtime::Env.qa_oauth_github_password)
          else
            page.find('#login_field').set(username)
            page.find('#password').set(password)
          end

          page.find('.js-sign-in-button').click
        end
      end
    end
  end
end
