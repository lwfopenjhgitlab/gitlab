# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Trials
      class New < ::QA::Page::Base
        def fill_first_name(first_name)
          fill_element(:first_name, first_name)
        end

        def fill_last_name(last_name)
          fill_element(:last_name, last_name)
        end

        def fill_company_name(company_name)
          fill_element(:company_name, company_name)
        end

        def select_quantity_of_employees(quantity)
          select_element(:number_of_employees, quantity)
        end

        def select_country(country)
          select_element(:country, country)
        end

        def select_province(province)
          select_element(:state, province)
        end

        def fill_telephone(telephone)
          fill_element(:telephone_number, telephone)
        end

        def continue_to_trial
          click_element(:continue)
        end
      end
    end
  end
end
