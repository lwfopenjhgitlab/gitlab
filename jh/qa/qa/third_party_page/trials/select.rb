# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Trials
      class Select < ::QA::Page::Base
        def select_group(group)
          find('svg[data-testid=chevron-down-icon]').click
          find('li', text: group.to_s).click
          QA::Runtime::Logger.info("Choose #{group}")
        end

        def choose_company
          click_element(:trial_company)
        end

        def choose_personal
          click_element(:trial_individual)
        end

        def start_your_free_trial
          click_element(:start_your_free_trial)
        end
      end
    end
  end
end
