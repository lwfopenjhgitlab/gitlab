# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Customerdot
      class Invoice < Page::Base
        def choose_alipay
          click_element(:third_pay)
        end

        def choose_union_pay
          # Add explicit wait for the element to be clickable
          sleep 30
          click_element(:union_pay)
        end

        def choose_china_pay
          # Add explicit wait for the element to be clickable
          sleep 30
          click_element(:china_pay)
        end

        def choose_offline_pay
          # Add explicit wait for the element to be clickable
          sleep 30
          click_element(:offline_pay)
        end

        def choose_china_b2b_pay
          find('label.custom-control-label', text: /企业网银/).click
          QA::Runtime::Logger.info("Go to payment")
        end

        def continue_to_payment
          find('span.gl-button-text', text: /立即支付/).click
          QA::Runtime::Logger.info("Go to payment")
        end
      end
    end
  end
end
