# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Customerdot
      class Unionpay < ::QA::Page::Base
        def fill_bank_account
          Support::Waiter.wait_until do
            find('#cardNumber').fill_in(with: ::QA::Runtime::Env.bank_account)
          end
          has_css?('span.close_val.dn')
          QA::Runtime::Logger.info("Card number is entered")
        end

        def go_to_phone_verfication
          Support::Waiter.wait_until do
            find('#btnNext').click
          end
          QA::Runtime::Logger.info("Go to get phone code")
        end

        def sent_phone_sms_code
          unless has_css?('#btnGetCode')
            fill_bank_account
            go_to_phone_verfication
          end

          Support::Waiter.wait_until do
            find('#btnGetCode').click
          end
          QA::Runtime::Logger.info("Click get code button")
        end

        def fill_phone_sms_code
          Support::Waiter.wait_until do
            find('#smsCode').fill_in(with: ::QA::Runtime::Env.phone_sms_code) if find('#btnGetCode').disabled?
          end
          QA::Runtime::Logger.info("Phone code is entered")
        end

        def confirm_payment
          Support::Waiter.wait_until do
            find('#btnCardPay').click
          end
          QA::Runtime::Logger.info("Confirm payment")
        end

        def back_to_customer_dot
          Support::Waiter.wait_until do
            find('#btnBack').click
          end
          QA::Runtime::Logger.info("Back to customerdot")
        end
      end
    end
  end
end
