# frozen_string_literal: true

module QA
  module ThirdPartyPage
    module Customerdot
      class Subscription < ::QA::Page::Base
        def enter_quantity(quantity)
          find('input[data-qa-selector=number_of_users]').fill_in(with: quantity)
          QA::Runtime::Logger.info("Filling #{quantity} in number of users")
        end

        def continue_to_billing
          find('span.gl-button-text', text: /Continue to billing/).click
          QA::Runtime::Logger.info("Continue to billing")
        end

        def choose_the_province(province = '北京市')
          find('select[data-qa-selector=state]').select(province)
          QA::Runtime::Logger.info("Fill :state with #{province}")
        end

        def fill_the_street_address(street_address = 'jh-qa')
          find('input[data-qa-selector=street_address_1]').fill_in(with: street_address)
          QA::Runtime::Logger.info("Fill :street_address with #{street_address}")
        end

        def continue_to_payment
          find('span.gl-button-text', text: /Continue to payment/).click
          QA::Runtime::Logger.info("Continue to payment")
        end

        def confirm_purchase
          find('span.gl-button-text', text: /Confirm purchase/).click
          QA::Runtime::Logger.info("Continue to payment")
        end

        def fill_addon_quantity(quantity)
          find('input[data-qa-selector=quantity]').fill_in(with: quantity)
          QA::Runtime::Logger.info("Filling #{quantity} in addon page")
        end
      end
    end
  end
end
