# frozen_string_literal: true

module QA
  module Flow
    module JhPurchase
      extend self

      def upgrade_subscription(plan)
        ::QA::Page::Group::Menu.perform(&:go_to_billing)
        ::QA::Page::Group::Settings::Billing.perform do |billing|
          case plan
          when 'premium'
            billing.go_to_upgrade_premium
          when 'ultimate'
            billing.go_to_upgrade_ultimate
          end
        end

        fill_subscription_quantity(1)
        choose_payment
        complete_payment
      end

      def purchase_ci_minutes(quantity)
        ::QA::Page::Group::Menu.perform(&:go_to_usage_quotas)
        ::QA::Page::Group::Settings::UsageQuotas.perform do |usage_quota|
          usage_quota.switch_to_ci_minutes
          usage_quota.buy_ci_minutes
        end

        fill_addon_quantity(quantity)
        choose_payment
        complete_payment
      end

      def purchase_storage(quantity)
        ::QA::Page::Group::Menu.perform(&:go_to_usage_quotas)

        ::QA::Page::Group::Settings::UsageQuotas.perform do |usage_quota|
          usage_quota.switch_to_storage

          if usage_quota.has_purchase_more_button?
            usage_quota.buy_more_storage
          else
            usage_quota.buy_storage
          end

          usage_quota.switch_window
        end

        fill_addon_quantity(quantity)
        choose_payment
        complete_payment

        ::QA::Page::Group::Settings::UsageQuotas.perform(&:close_window)
      end

      def oauth_with_saas_account
        ::QA::ThirdPartyPage::Customerdot::Subscription.perform do |subscription|
          subscription.click_oauth_with_jihulab_button if subscription.has_oauth_with_jihulab_button?
        end
        ::QA::ThirdPartyPage::Customerdot::Customers.perform do |customers|
          customers.fill_the_customer_form if customers.has_form?
        end
      end

      def fill_subscription_quantity(quantity)
        ::QA::ThirdPartyPage::Customerdot::Subscription.perform do |subscription|
          subscription.enter_quantity(quantity)
          subscription.continue_to_billing
          subscription.choose_the_province
          subscription.fill_the_street_address
          subscription.continue_to_payment
          subscription.confirm_purchase
        end
      end

      def fill_addon_quantity(quantity)
        ::QA::ThirdPartyPage::Customerdot::Subscription.perform do |subscription|
          subscription.fill_addon_quantity(quantity)
          subscription.continue_to_billing
          subscription.choose_the_province
          subscription.fill_the_street_address
          subscription.continue_to_payment
          subscription.confirm_purchase
        end
      end

      def choose_payment
        payment = ::QA::Runtime::Env.fulfillment_payment
        ::QA::ThirdPartyPage::Customerdot::Invoice.perform do |invoice|
          case payment
          when 'unionpay'
            invoice.choose_union_pay
          when 'alipay'
            invoice.choose_alipay
          when 'chinapay'
            invoice.choose_china_pay
            invoice.choose_china_b2b_pay
          end
          invoice.continue_to_payment
        end
      end

      def complete_payment
        payment = ::QA::Runtime::Env.fulfillment_payment
        case payment
        when 'unionpay'
          ::QA::ThirdPartyPage::Customerdot::Unionpay.perform do |unionpay|
            unionpay.fill_bank_account
            unionpay.go_to_phone_verfication
            unionpay.sent_phone_sms_code
            sleep 1
            unionpay.fill_phone_sms_code
            unionpay.confirm_payment
            unionpay.back_to_customer_dot
          end
        when 'chinapay'
          ::QA::ThirdPartyPage::Customerdot::Chinapay.perform do |chinapay|
            chinapay.choose_card
            chinapay.go_to_payment
            chinapay.payment_success
          end
        end
      end
    end
  end
end
