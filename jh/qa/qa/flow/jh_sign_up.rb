# frozen_string_literal: true

module QA
  module Flow
    module JhSignUp
      extend self

      def page
        Capybara.current_session
      end

      def sign_up!(user)
        Page::Main::Menu.perform(&:sign_out_if_signed_in)
        Page::Main::Login.perform(&:switch_to_register_page)
        Page::Registration::SignUp.perform do |sign_up|
          sign_up.fill_new_user_first_name_field(user.first_name)
          sign_up.fill_new_user_last_name_field(user.last_name)
          sign_up.fill_new_user_username_field(user.username)
          sign_up.fill_new_user_email_field(user.email)
          sign_up.fill_new_user_password_field(user.password)

          Support::Waiter.wait_until(sleep_interval: 0.5) do
            page.has_text?("用户名可用。") || page.has_text?('Username is available.')
          end
          sign_up.click_new_user_register_button
        end
        Flow::JhUserOnboarding.jh_onboard_user
      end
    end
  end
end
