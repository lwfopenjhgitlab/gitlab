# frozen_string_literal: true

module QA
  module Resource
    class JhUser < User
      def fabricate!
        # Don't try to log-out if we're not logged-in
        Page::Main::Menu.perform(&:sign_out) if Page::Main::Menu.perform { |p| p.has_personal_area?(wait: 0) }

        if credentials_given?
          Page::Main::Login.perform do |login|
            login.sign_in_using_credentials(user: self)
          end
        else
          Flow::JhSignUp.sign_up!(self)
        end
      end
    end
  end
end
