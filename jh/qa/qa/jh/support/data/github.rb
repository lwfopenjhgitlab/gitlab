# frozen_string_literal: true

module QA
  module JH
    module Support
      module Data
        module Github
          # @override github_username
          def github_username
            'jihulab-qa-github'
          end
        end
      end
    end
  end
end
