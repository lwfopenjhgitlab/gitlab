# frozen_string_literal: true

module QA
  module JH
    module Page
      module Project
        module New
          # @override :set_visibility
          def set_visibility(visibility)
            super

            # self-managed does not support declarations
            return unless visibility.capitalize.to_s == 'Public' && has_declarations?

            check_element(:agree_jihu_terms_checkbox, true)
            check_element(:agree_intellectual_property_checkbox, true)
          end

          def has_declarations?
            has_element?(:agree_jihu_terms_checkbox, wait: 1) &&
              has_element?(:agree_intellectual_property_checkbox, wait: 1)
          end

          def click_gitee_link
            click_link 'Gitee'
          end
        end
      end
    end
  end
end
