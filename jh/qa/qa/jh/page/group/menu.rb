# frozen_string_literal: true

module QA
  module JH
    module Page
      module Group
        module Menu
          def go_to_performance_analytics_with_super_side_bar
            click_element(:menu_section_button, menu_item: 'Analyze')
            click_element(:nav_item_link, submenu_item: 'Performance Analytics')
          end

          def go_to_performance_analytics
            return go_to_performance_analytics_with_super_side_bar if QA::Runtime::Env.super_sidebar_enabled?

            hover_analytics do
              within_submenu do
                click_element(:sidebar_menu_item_link, menu_item: 'Performance Analytics')
              end
            end
          end

          private

          def hover_analytics
            within_sidebar do
              scroll_to_element(:sidebar_menu_link, menu_item: 'Analytics')
              find_element(:sidebar_menu_link, menu_item: 'Analytics').hover

              yield
            end
          end
        end
      end
    end
  end
end
