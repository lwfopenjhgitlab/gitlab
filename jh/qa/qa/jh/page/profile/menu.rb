# frozen_string_literal: true

module QA
  module JH
    module Page
      module Profile
        module Menu
          # @!override :click_access_tokens
          def click_access_tokens
            return super if QA::Runtime::Env.super_sidebar_enabled?

            within_sidebar do
              has_text?('访问令牌') ? click_link('访问令牌') : click_link('Access Tokens')
            end
          end

          # Add click_preferences
          def click_preferences
            within_sidebar do
              has_text?('偏好设置') ? click_link('偏好设置') : click_link('Preferences')
            end
          end
        end
      end
    end
  end
end
