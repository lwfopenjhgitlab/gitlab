# frozen_string_literal: true

# Prepended onto ::QA::Runtime::Env
module QA
  module JH
    module Runtime
      module Env
        def skip_update_driver?
          enabled?(ENV['QA_SKIP_UPDATE_DRIVER'], default: false)
        end

        def user_phone
          ENV['QA_USER_PHONE']
        end

        def gitee_username
          ENV['QA_GITEE_USERNAME']
        end

        def gitee_password
          ENV['QA_GITEE_PASSWORD']
        end

        def gitee_access_token
          ENV['QA_GITEE_ACCESS_TOKEN'].to_s.strip
        end

        def chrome_default_download_path
          ENV['QA_DEFAULT_CHROME_DOWNLOAD_PATH'] || Dir.tmpdir
        end

        def gitee_api_address
          ENV['QA_GITEE_API_ADDRESS'].to_s.strip
        end

        def ones_service_url
          ENV['QA_ONES_SERVICE_URL']
        end

        def ones_service_namespace
          ENV['QA_ONES_SERVICE_NAMESPACE']
        end

        def ones_service_project_key
          ENV['QA_ONES_SERVICE_PROJECT_KEY']
        end

        def ones_service_user_key
          ENV['QA_ONES_SERVICE_USER_KEY']
        end

        def ones_service_api_token
          ENV['QA_ONES_SERVICE_API_TOKEN']
        end

        def fulfillment_payment
          ENV['QA_PAYMENT'] || 'unionpay'
        end

        def qa_oauth_github_username
          ENV['QA_OAUTH_GITHUB_USERNAME']
        end

        def qa_oauth_github_password
          ENV['QA_OAUTH_GITHUB_PASSWORD']
        end

        def phone_sms_code
          ENV['QA_PHONE_SMS_CODE'] || '123456'
        end

        def bank_account
          ENV['QA_BANK_ACCOUNT'] || '6216261000000000018'
        end

        def default_storage
          ENV['QA_DEFAULT_STORAGE'] || 2
        end
      end
    end
  end
end
