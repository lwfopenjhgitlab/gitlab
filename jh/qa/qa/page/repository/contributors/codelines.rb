# frozen_string_literal: true

module QA
  module Page
    module Repository
      module Contributors
        class Codelines < QA::Page::Base
          view 'jh/app/assets/javascripts/contributors/components/contributors.vue' do
            element :code_lines_additions
            element :code_lines_deletions
            element :effective_code_lines_additions
            element :effective_code_lines_deletions
          end

          def has_addition_lines_element?(text)
            has_element?(:code_lines_additions, text: text)
          end

          def has_deletion_lines_element?(text)
            has_element?(:code_lines_deletions, text: text)
          end

          def has_addition_effective_lines_element?(text)
            has_element?(:effective_code_lines_additions, text: text)
          end

          def has_deletion_effective_lines_element?(text)
            has_element?(:effective_code_lines_deletions, text: text)
          end
        end
      end
    end
  end
end
