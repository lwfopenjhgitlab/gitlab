# frozen_string_literal: true

module QA
  module Page
    module Registration
      module GroupsProjects
        class CreateOrImport < Page::Base
          view 'ee/app/views/projects/learn_gitlab/onboarding.html.haml' do
            element :confirm_button
          end

          def create_org(group_name, project_name)
            fill_element(:group_name_field, group_name) if has_element?(:group_name_field)
            fill_element(:project_name_field, project_name)
            click_element(:submit_button)
            Support::WaitForRequests.wait_for_requests
          end

          def get_started
            click_element(:confirm_button) if has_element?(:confirm_button, wait: 3)
            Support::WaitForRequests.wait_for_requests
          end
        end
      end
    end
  end
end
