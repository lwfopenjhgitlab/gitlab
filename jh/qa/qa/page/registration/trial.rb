# frozen_string_literal: true

module QA
  module Page
    module Registration
      class Trial < Page::Base
        view 'ee/app/assets/javascripts/registrations/components/company_form.vue' do
          element :company_name_field
          element :company_size_field
          element :phone_number_field
          element :country_option
          element :website_url_field
          element :trial_onboarding_flow_toggle
          element :confirm_button
        end

        def has_company_name?
          has_element?(:company_name_field, wait: 1)
        end

        def submit_company_trial_info
          fill_element(:company_name_field, "QA company name #{Time.new.to_i}") if has_company_name?
          select_element(:company_size_field, "1 - 99")
          select_element(:country_option, "China")
          fill_element(:phone_number_field, "130#{Time.new.to_i}")
          fill_element(:website_url_field, "wwww.#{Time.new.to_i}.com")
          click_element(:confirm_button) if has_element?(:confirm_button, wait: 1)
        end
      end
    end
  end
end
