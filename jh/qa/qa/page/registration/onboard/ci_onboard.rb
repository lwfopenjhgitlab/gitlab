# frozen_string_literal: true

module QA
  module Page
    module Registration
      module Onboard
        class CiOnboard < Page::Base
          view 'ee/app/assets/javascripts/pages/projects/learn_gitlab/components/learn_gitlab_section_link.vue' do
            element :uncompleted_learn_gitlab_link
          end
          view 'app/assets/javascripts/sidebar/components/assignees/assignees.vue' do
            element :assign_yourself_button
          end

          view 'app/views/shared/issuable/_sidebar.html.haml' do
            element :assignee_block_container
          end

          view 'app/helpers/dropdowns_helper.rb' do
            element :dropdown_list_content
          end

          view 'app/assets/javascripts/sidebar/components/assignees/assignee_title.vue' do
            element :edit_link
          end

          view 'app/assets/javascripts/sidebar/components/assignees/uncollapsed_assignee_list.vue' do
            element :username
          end

          def go_to_on_boarding_ci_page
            click_on '设置您的第一个项目的 CI/CD'
          end

          def assign_assignee(user)
            click_element(:assign_yourself_button)

            wait_until(reload: false) do
              has_element?(:username, text: user, wait: 0)
            end

            refresh
          end
        end
      end
    end
  end
end
