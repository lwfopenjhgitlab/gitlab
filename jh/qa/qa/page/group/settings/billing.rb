# frozen_string_literal: true

module QA
  module Page
    module Group
      module Settings
        class Billing < ::QA::Page::Base
          view 'ee/app/helpers/billing_plans_helper.rb' do
            element :start_your_free_trial
          end

          view 'ee/app/components/billing/plan_component.rb' do
            element :upgrade_to_premium
            element :upgrade_to_ultimate
          end

          view 'ee/app/views/profiles/billings/_billing_plan_header.html.haml' do
            element :billing_plan_header
          end

          def go_to_free_trial
            click_element(:start_your_free_trial)
          end

          def go_to_upgrade_premium
            click_element(:upgrade_to_premium)
          end

          def go_to_upgrade_ultimate
            click_element(:upgrade_to_ultimate)
          end

          def has_free_trial_widget?
            has_css?("#trial-status-sidebar-widget")
          end
        end
      end
    end
  end
end
