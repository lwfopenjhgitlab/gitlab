# frozen_string_literal: true

module QA
  module Page
    module Group
      module Settings
        class UsageQuotas < ::QA::Page::Base
          view 'ee/app/views/groups/usage_quotas/index.html.haml' do
            element :seats_tab
            element :pipelines_tab
            element :storage_tab
          end

          view 'ee/app/assets/javascripts/usage_quotas/storage/components/storage_usage_statistics.vue' do
            element :purchase_more_storage
            element :namespace_usage_total
          end

          view 'ee/app/assets/javascripts/usage_quotas/pipelines/components/app.vue' do
            element :additional_ci_minutes
            element :buy_ci_minutes
            element :plan_ci_minutes
          end

          def purchased_ci_minutes
            find_element(:additional_ci_minutes).text.split('/').last.match(/\d+/)[0].to_f
          end

          def plan_ci_minutes
            find_element(:plan_ci_minutes).text.split('/').last.match(/\d+/)[0].to_f
          end

          def switch_to_ci_minutes
            click_element(:pipelines_tab)
          end

          def buy_ci_minutes
            click_element(:buy_ci_minutes)
          end

          def switch_to_storage
            click_element(:storage_tab)
          end

          def buy_storage
            click_element(:purchase_more_storage)
          end

          def has_purchase_more_button?
            has_css?('span.gl-button-text', text: /Purchase more storage/)
          end

          def buy_more_storage
            find('span.gl-button-text', text: /Purchase more storage/).click
          end

          def purchased_storage
            if has_css?('span[data-testid=denominator-total]')
              find('span[data-testid=denominator-total]').text.split('/').last.match(/\d+\.\d+/)[0].to_f
            else
              find_element(:purchased_usage_total).text.split('/').last.match(/\d+\.\d+/)[0].to_f
            end
          end

          def switch_window
            browser = page.driver.browser
            browser.switch_to.window(browser.window_handles.last) if browser.window_handles.length == 2
          end

          def close_window
            browser = page.driver.browser
            return unless browser.window_handles.length == 2

            browser.close
            browser.switch_to.window(browser.window_handles.first)
            refresh
          end
        end
      end
    end
  end
end
