# frozen_string_literal: true

module QA
  RSpec.describe 'JH Create', :skip, :reliable, :ones, :require_admin,
    feature_flag: { name: 'ff_ones_issues_integration', scope: :project } do
    describe 'Ones integration' do
      let(:group) do
        Resource::Group.fabricate_via_api! do |group|
          group.path = "ones-integration-group-test-#{SecureRandom.hex(8)}"
        end
      end

      let(:project) do
        Resource::Project.fabricate_via_api! do |project|
          project.name = "ones-integration-project-test-#{SecureRandom.hex(8)}"
          project.group = group
        end
      end

      before do
        Runtime::Feature.enable(:ff_ones_issues_integration, project: project)
        Flow::Login.sign_in
        group.visit!
        project.visit!
        Page::Project::Menu.perform(&:click_settings)
        Page::Project::Menu.perform(&:go_to_integrations_settings)
        Page::Project::Settings::Services::Ones.perform(&:click_ones_link)
        Page::Project::Settings::Services::Ones.perform(&:fill_in_field)
        Page::Project::Menu.perform(&:go_to_ones_issues)
      end

      after do
        Runtime::Feature.disable(:ff_ones_issues_integration, project: project)

        project.remove_via_api!
        group.remove_via_api!
      end

      it 'show item link of ones',
        testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/issues/11' do
        Page::Project::Settings::Services::Ones.perform do |open_button|
          expect(open_button).to have_open_button_element("Open")
        end
      end
    end
  end
end
