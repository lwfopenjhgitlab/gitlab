# frozen_string_literal: true

module QA
  RSpec.describe 'JH Plan', :reliable, :require_admin, only: { subdomain: 'staging' } do
    describe 'Pages' do
      let(:admin_api_client) { Runtime::API::Client.as_admin }

      let!(:project) do
        Resource::Project.fabricate_via_api! do |project|
          project.name = 'gitlab-pages-project'
          project.template_name = :plainhtml
        end
      end

      let(:pipeline) do
        Resource::Pipeline.fabricate_via_api! do |pipeline|
          pipeline.project = project
          pipeline.variables = [
            { key: :CI_PAGES_DOMAIN, value: 'nip.io', variable_type: :env_var },
            { key: :CI_PAGES_URL, value: 'http://127.0.0.1.nip.io', variable_type: :env_var }
          ]
        end
      end

      let(:anoymous_user) do
        Resource::User.fabricate_via_api! do |resource|
          resource.api_client = admin_api_client
        end
      end

      before do
        Runtime::Feature.disable(:show_pages_in_deployments_menu)
        Flow::Login.sign_in
        Resource::ProjectRunner.fabricate_via_api! do |runner|
          runner.project = project
          runner.executor = :docker
        end
        pipeline.visit!

        # Need to use expect to make sure job is success
        # rubocop:disable RSpec/ExpectInHook
        Page::Project::Pipeline::Show.perform do |show|
          expect(show).to have_job(:pages)
          show.click_on_first_job
        end

        Page::Project::Job::Show.perform do |show|
          expect(show).to have_passed(timeout: 300)
        end
        # rubocop:enable RSpec/ExpectInHook
        Page::Project::Menu.perform(&:go_to_pages_settings)
        Page::Project::Settings::Pages.perform(&:go_to_access_page)
        @pages_url = page.current_url
      end

      after do
        Runtime::Feature.enable(:show_pages_in_deployments_menu)
        anoymous_user.remove_via_api!
      end

      it 'user without project permission cannot access pages' do
        page.driver.browser.close
        switch_to_window windows.first
        Capybara.current_session.driver.browser.manage.delete_all_cookies

        Flow::Login.sign_in(as: anoymous_user)
        visit(@pages_url) # rubocop:disable RSpec/InstanceVariable

        # wait until authorization modal
        until Page::Main::OAuth.perform(&:needs_authorization?)
          page.refresh
          sleep 5
        end
        Page::Main::OAuth.perform(&:authorize!)
        expectation_str = "The resource that you are attempting to access does not exist or you don't have the necessary permissions to view it." # rubocop:disable Layout/LineLength
        expect(page).to have_content(expectation_str)
      end
    end
  end
end
