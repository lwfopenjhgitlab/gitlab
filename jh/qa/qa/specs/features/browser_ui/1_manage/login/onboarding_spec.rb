# frozen_string_literal: true

module QA
  RSpec.describe 'JH Manage', :reliable, :require_admin, only: { job: 'jh-zh-qa-reliable' },
    except: { tld: '.cn', subdomain: 'release' } do
    include Support::API

    describe 'user onboarding' do
      let!(:owner_user) do
        Resource::User.fabricate_via_api! do |usr|
          usr.api_client = Runtime::API::Client.as_admin
        end
      end

      let(:api_client) do
        Runtime::API::Client.as_admin
      end

      group_name = "onboard_group_#{Time.new.to_i}"

      project_name = "onboard_project_#{Time.new.to_i}"

      step1_yaml =
        "test1:\n" \
        "stage: test\n" \
        "script:\n    " \
        "- echo 'Do a test here'\n" \
        "- echo 'For example run a test suite'"

      step2_yaml =
        "deploy test2:\n" \
        "stage: test\n" \
        "script:\n    " \
        "- echo 'Do an other test here'"

      step3_yaml =
        "deploy production:\n" \
        "stage: deploy\n" \
        "script:\n   " \
        "- echo 'Deploy to a production environment'"

      step4_yaml =
        "deploy production:\n" \
        "stage: deploy\n" \
        "script:\n" \
        "- echo 'Deploy to a production environment'\n" \
        "rules:\n" \
        "- when: manual"

      step5_yaml =
        "build app:\n" \
        "stage: build\n" \
        "script:\n" \
        "- echo 'Deploy to a production environment'\n" \
        "artifacts:\n  " \
        "expire_in: 1 hour\n" \
        "paths:\n  " \
        "- artifact1"

      before do
        Support::Retrier.retry_until(sleep_interval: 5) do
          refresh
          page.has_content?("用户名")
        end
      end

      after do
        owner_user.remove_via_api!
        delete_group_by_name(group_name)
      end

      it 'user can onboarding' do
        Flow::Login.sign_in(as: owner_user)
        visit_path("/users/sign_up/groups_projects/new")

        Page::Registration::GroupsProjects::CreateOrImport.perform do |new|
          new.create_org(group_name, project_name)
          new.get_started
        end

        add_member_into_group(group_name, 'gitlab-qa')

        Page::Registration::GroupsProjects::InviteMembers.perform do |invit|
          invit.add_member(Runtime::Env.user_username)
        end
        Page::Registration::Onboard::CiOnboard.perform do |onboard_ci|
          # assaign user
          onboard_ci.go_to_on_boarding_ci_page
          current_url = page.current_url
          onboard_ci.assign_assignee(owner_user.name)
          verify_step_pass("第一步：添加 .gitlab-ci.yml 文件")

          # step1
          visit_ci_edit_page(group_name)
          ci_onboard_step_start(step1_yaml)
          visit current_url
          verify_step_pass("第二步：流水线增加作业(job)")
          # step2
          visit_ci_edit_page(group_name)
          ci_onboard_step(step2_yaml)
          visit current_url
          verify_step_pass("第三步：流水线增加阶段(stage)")
          # step3
          visit_ci_edit_page(group_name)
          ci_onboard_step(step3_yaml)
          visit current_url
          verify_step_pass("第四步：配置作业运行的条件")
          # step4
          visit_ci_edit_page(group_name)
          ci_onboard_step(step4_yaml)
          visit current_url
          verify_step_pass("第五步：CI 制品")
          # step5
          visit_ci_edit_page(group_name)
          ci_onboard_step(step5_yaml)
          visit current_url
          verify_step_pass("完成本课程，现在关闭本议题")
        end
      end

      def visit_ci_edit_page(group)
        visit_path("/#{group}/learn-gitlab/-/ci/editor?branch_name=master")
      end

      def visit_path(path)
        visit Runtime::Env.gitlab_url + path
      end

      def verify_step_pass(content)
        Support::Retrier.retry_until(sleep_interval: 1) do
          refresh
          expect(page).to have_text(content)
        end
      end

      def ci_onboard_step_start(ci_yaml)
        Page::Project::PipelineEditor::New.perform(&:create_new_ci)
        Page::Project::PipelineEditor::Show.perform do |show|
          show.auto_clear_and_write_to_editor(ci_yaml)
          show.submit_changes
        end
      end

      def ci_onboard_step(ci_yaml)
        Page::Project::PipelineEditor::Show.perform do |show|
          show.auto_clear_and_write_to_editor(ci_yaml)
          show.submit_changes
        end
      end

      def delete_group_by_name(name)
        get_group_response = get Runtime::API::Request.new(api_client, "/groups?search=#{name}").url
        group_id = parse_body(get_group_response)[0][:id]
        delete_response = delete Runtime::API::Request.new(api_client, "/groups/#{group_id}").url
        expect(delete_response.code).to eq 202
      end

      def add_member_into_group(group_name, member_name)
        get_user_response = get Runtime::API::Request.new(api_client, "/users?username=#{member_name}").url
        user_id = parse_body(get_user_response)[0][:id]
        get_group_response = get Runtime::API::Request.new(api_client, "/groups?search=#{group_name}").url
        group_id = parse_body(get_group_response)[0][:id]
        response = post Runtime::API::Request.new(api_client, "/groups/#{group_id}/invitations").url,
          { format: "json", access_level: 50, invite_source: "group-members-page", tasks_to_be_done: [],
            tasks_project_id: "", user_id: user_id }
        expect(response.code).to eq 201
      end
    end
  end
end
