# frozen_string_literal: true

module QA
  RSpec.describe 'JH Manage', :reliable, only: { subdomain: /(staging.)?/, domain: 'jihulab' } do
    describe 'github oauth user login' do
      it 'user log in use github oauth', \
        :skip, testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/15' do
        QA::Support::Retrier.retry_on_exception do
          QA::Runtime::Browser.visit(:gitlab, QA::Page::Main::Login)
        end

        ThirdPartyPage::Github::Login.perform do |github|
          github.go_to_github_oauth
          github.login_github
        end
        verify_oauth_success('Welcome to GitLab')
      end

      def verify_oauth_success(content)
        Support::Retrier.retry_until(sleep_interval: 1) do
          expect(page).to have_text(content)
        end
      end
    end
  end
end
