# frozen_string_literal: true

module QA
  # Todo need add a method to remove test data
  RSpec.describe 'JH Fulfillment', :reliable, except: { subdomain: :staging } do
    describe 'Purchase' do
      let(:group_for_trial) do
        Resource::Sandbox.fabricate! do |sandbox|
          sandbox.path = "test-group-fulfillment#{SecureRandom.hex(4)}"
          sandbox.visibility = 'private'
        end
      end

      before do
        Flow::Login.sign_in
        group_for_trial.visit!
      end

      describe 'starts a free trial' do
        context 'when on about page with multiple eligible namespaces' do
          let!(:group) do
            Resource::Sandbox.fabricate! do |sandbox|
              sandbox.path = "test-group-fulfillment#{SecureRandom.hex(4)}"
              sandbox.visibility = 'private'
            end
          end

          before do
            group.visit!
            Page::Group::Menu.perform(&:go_to_billing)
          end

          it 'registers for a new trial',
            testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/3' do
            Page::Group::Settings::Billing.perform(&:go_to_free_trial)

            register_for_trial
            ThirdPartyPage::Alert::FreeTrial.perform do |free_trial_alert|
              expect(free_trial_alert.trial_actived_message?)
                .to eq(true)
            end

            Page::Group::Menu.perform(&:go_to_billing)
            Page::Group::Settings::Billing.perform do |billing|
              expect(billing.has_free_trial_widget?).to be(true)
              expect(billing.has_text?("#{group_for_trial.path} is currently using the Ultimate Trial Plan"))
                .to be(true)
            end
          end
        end
      end

      private

      def customer_trial_info
        {
          company_name: 'Jihu QA',
          number_of_employees: '500 - 1,999',
          telephone_number: '13800000000',
          country: 'China',
          province: '北京市'
        }
      end

      def register_for_trial(skip_select: false)
        ::QA::ThirdPartyPage::Trials::New.perform do |new|
          # setter
          new.fill_company_name(customer_trial_info[:company_name])
          new.select_quantity_of_employees(customer_trial_info[:number_of_employees])
          new.select_province(customer_trial_info[:province])
          new.fill_telephone(customer_trial_info[:telephone_number])
          new.continue_to_trial
        end

        return if skip_select

        ::QA::ThirdPartyPage::Trials::Select.perform do |select|
          select.select_group(group_for_trial.path)
          select.choose_company
          select.start_your_free_trial
        end
      end
    end
  end
end
