# frozen_string_literal: true

module QA
  RSpec.describe 'JH Fulfillment', :reliable, :require_admin, only: { subdomain: :staging } do
    describe 'Purchase' do
      let(:api_client) { Runtime::API::Client.as_admin }

      let(:user) do
        Resource::User.fabricate_via_api! do |user|
          user.email = "jihu-qa+#{SecureRandom.hex(4)}@jihulab.com"
          user.api_client = api_client
          user.hard_delete_on_api_removal = true
        end
      end

      let(:group_for_trial) do
        Resource::Sandbox.fabricate! do |sandbox|
          sandbox.path = "test-group-fulfillment#{SecureRandom.hex(4)}"
          sandbox.api_client = api_client
        end
      end

      before do
        Flow::Login.sign_in(as: user)
        group_for_trial.visit!
      end

      describe 'starts a free trial' do
        context 'when on billing page with multiple eligible namespaces' do
          let!(:group) do
            Resource::Sandbox.fabricate! do |sandbox|
              sandbox.path = "test-group-fulfillment#{SecureRandom.hex(4)}"
              sandbox.api_client = api_client
            end
          end

          before do
            group.visit!
            Page::Group::Menu.perform(&:go_to_billing)
          end

          after do
            user.remove_via_api!
          end

          it 'registers for a new trial',
            testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/3' do
            Page::Group::Settings::Billing.perform(&:go_to_free_trial)

            register_for_trial
            ::QA::ThirdPartyPage::Alert::FreeTrial.perform do |free_trial_alert|
              expect(free_trial_alert.trial_actived_message?)
                .to eq(true)
            end

            Page::Group::Menu.perform(&:go_to_billing)
            Page::Group::Settings::Billing.perform do |billing|
              expect(billing.has_free_trial_widget?).to be(true)
              expect(billing.has_text?("#{group_for_trial.path} is currently using the Ultimate Trial Plan"))
                .to be(true)
            end
          end
        end

        context 'when on billing page with only one eligible namespace' do
          before do
            group_for_trial.visit!
            Page::Group::Menu.perform(&:go_to_billing)
          end

          it 'registers for a new trial',
            testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/16' do
            Page::Group::Settings::Billing.perform(&:go_to_free_trial)
            register_for_trial(skip_select: true)

            ThirdPartyPage::Alert::FreeTrial.perform do |free_trial_alert|
              expect(free_trial_alert.trial_actived_message?)
                .to eq(true)
            end

            Page::Group::Menu.perform(&:go_to_billing)

            Page::Group::Settings::Billing.perform do |billing|
              expect(billing.has_free_trial_widget?).to be(true)
              expect(billing.has_text?("#{group_for_trial.path} is currently using the Ultimate Trial Plan"))
                .to be(true)
            end
          end
        end

        context 'when with only on eligible namespace from homepage', only: { subdomain: :staging, tld: '.com' } do
          let(:home_page_trial) { 'https://about1s.gitlab.cn/free-trial/' }

          before do
            visit(home_page_trial)

            ::QA::ThirdPartyPage::Gitlabcn::About.perform(&:switch_to_saas)

            ::QA::ThirdPartyPage::Gitlabcn::About.perform(&:go_to_trial)
          end

          it 'register for new trial',
            testcase: 'https://jihulab.com/gitlab-cn/quality/testcases/-/quality/test_cases/17' do
            switch_to_window windows.last
            register_for_trial(skip_select: true)

            ThirdPartyPage::Alert::FreeTrial.perform do |free_trial_alert|
              expect(free_trial_alert.trial_actived_message?)
                .to eq(true)
            end

            Page::Group::Menu.perform(&:go_to_billing)

            Page::Group::Settings::Billing.perform do |billing|
              expect(billing.has_free_trial_widget?).to be(true)
              expect(billing.has_text?("#{group_for_trial.path} is currently using the Ultimate Trial Plan"))
                .to be(true)
            end
          end
        end
      end

      private

      def customer_trial_info
        {
          company_name: 'Jihu QA',
          number_of_employees: '500 - 1,999',
          telephone_number: '13800000000',
          country: 'China',
          province: '北京市'
        }
      end

      def register_for_trial(skip_select: false)
        ::QA::ThirdPartyPage::Trials::New.perform do |new|
          # setter
          new.fill_company_name(customer_trial_info[:company_name])
          new.select_quantity_of_employees(customer_trial_info[:number_of_employees])
          new.select_province(customer_trial_info[:province])
          new.fill_telephone(customer_trial_info[:telephone_number])
          new.continue_to_trial
        end

        return if skip_select

        ::QA::ThirdPartyPage::Trials::Select.perform do |select|
          select.select_group(group_for_trial.path)
          select.choose_company
          select.start_your_free_trial
        end
      end
    end
  end
end
