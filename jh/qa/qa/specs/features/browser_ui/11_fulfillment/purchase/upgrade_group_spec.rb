# frozen_string_literal: true

module QA
  RSpec.describe 'JH Fulfillment', :reliable, :requires_admin, only: { subdomain: :staging, tld: '.com' } do
    describe 'Purchase' do
      describe 'group plan' do
        let(:admin_api_client) { Runtime::API::Client.as_admin }
        let(:ci_minutes) { 1000 }
        let(:premium_ci_minutes) { 10000 }
        let(:user) do
          Resource::User.fabricate_via_api! do |user|
            user.email = "jihu-qa+#{SecureRandom.hex(4)}@jihulab.com"
            user.api_client = admin_api_client
            user.hard_delete_on_api_removal = true
          end
        end

        let(:group) do
          Resource::Sandbox.fabricate! do |sandbox|
            sandbox.path = "test-group-fulfillment#{SecureRandom.hex(4)}"
          end
        end

        before do
          Flow::Login.sign_in(as: user)
          group.visit!
        end

        after do
          user.remove_via_api!
        end

        it 'upgrades from free to ultimate' do
          Flow::JhPurchase.upgrade_subscription('ultimate')
          group.visit!
          Page::Group::Menu.perform(&:go_to_billing)

          Page::Group::Settings::Billing.perform do |billing|
            expect(billing.has_content?("#{group.path} is currently using the Ultimate Plan")).to be(true)
          end
        end

        it 'upgrade from free to premium' do
          # upgrade to premium
          Flow::JhPurchase.upgrade_subscription('premium')
          group.visit!
          Page::Group::Menu.perform(&:go_to_billing)

          Page::Group::Settings::Billing.perform do |billing|
            expect(billing.has_content?("#{group.path} is currently using the Premium Plan")).to be(true)
          end
        end

        context 'when with existing CI minutes pack' do
          let(:ci_minutes_quantity) { 5 }

          before do
            Resource::Project.fabricate_via_api! do |project|
              project.name = 'ci-minutes'
              project.group = group
              project.initialize_with_readme = true
              project.api_client = Runtime::API::Client.as_admin
            end

            Flow::JhPurchase.purchase_ci_minutes(ci_minutes_quantity)
          end

          it 'upgrades from free to premium with correct CI minutes' do
            Flow::JhPurchase.upgrade_subscription('premium')

            expected_minutes = ci_minutes * ci_minutes_quantity
            plan_limits = premium_ci_minutes

            Page::Group::Menu.perform(&:go_to_billing)
            Page::Group::Settings::Billing.perform do |billing|
              expect(billing.has_content?("#{group.path} is currently using the Premium Plan")).to be(true)
            end

            Page::Group::Menu.perform(&:go_to_usage_quotas)
            Page::Group::Settings::UsageQuotas.perform do |usage_quotas|
              usage_quotas.switch_to_ci_minutes
              expect(usage_quotas.plan_ci_minutes).to eq(plan_limits.to_i)
              expect(usage_quotas.purchased_ci_minutes).to eq(expected_minutes.to_i)
            end
          end
        end
      end
    end
  end
end
