# frozen_string_literal: true

module Integrations
  module LigaaiSerializers
    class IssueDetailSerializer < BaseSerializer
      entity ::Integrations::LigaaiSerializers::IssueDetailEntity
    end
  end
end
