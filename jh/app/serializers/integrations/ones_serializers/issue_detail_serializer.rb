# frozen_string_literal: true

module Integrations
  module OnesSerializers
    class IssueDetailSerializer < BaseSerializer
      entity ::Integrations::OnesSerializers::IssueDetailEntity
    end
  end
end
