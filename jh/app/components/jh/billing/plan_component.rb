# frozen_string_literal: true

module JH
  module Billing
    module PlanComponent
      extend ::Gitlab::Utils::Override

      override :annual_price_text
      def annual_price_text
        s_("JH|BillingPlans|Billed annually at %{price_per_year}") % { price_per_year: price_per_year }
      end

      override :plans_data
      def plans_data
        plans = {
          'free' => {
            features: [
              {
                title: s_("BillingPlans|Spans the DevOps lifecycle")
              },
              {
                title: s_("BillingPlans|Open Source - MIT License")
              },
              {
                title: s_("JH|BillingPlans|Bring your own JiHu GitLab CI runners")
              },
              {
                title: s_("JH|BillingPlans|2GB storage per project")
              },
              {
                title: s_("BillingPlans|400 compute minutes per month")
              }
            ]
          },
          "premium" => {
            features: [
              {
                title: s_("JH|BillingPlans|Everything from Free")
              },
              {
                title: s_("JH|BillingPlans|Efficient code review")
              },
              {
                title: s_("BillingPlans|Advanced CI/CD")
              },
              {
                title: s_("JH|BillingPlans|Enterprise agile management")
              },
              {
                title: s_("BillingPlans|Release controls")
              },
              {
                title: s_("BillingPlans|Self-managed reliability")
              },
              {
                title: s_("BillingPlans|10,000 compute minutes per month")
              },
              {
                title: s_("JH|BillingPlans|Professional technical support")
              },
              {
                title: s_("JH|BillingPlans|5GB storage per project")
              }
            ]
          },
          "ultimate" => {
            features: [
              {
                title: s_("JH|BillingPlans|Everything from Premium")
              },
              {
                title: s_("JH|BillingPlans|Advanced security testing")
              },
              {
                title: s_("BillingPlans|Security risk mitigation")
              },
              {
                title: s_("BillingPlans|Compliance")
              },
              {
                title: s_("BillingPlans|Portfolio management")
              },
              {
                title: s_("JH|BillingPlans|Value stream analysis")
              },
              {
                title: s_("BillingPlans|Free guest users")
              },
              {
                title: s_("BillingPlans|50,000 compute minutes per month")
              },
              {
                title: s_("JH|BillingPlans|Professional technical support")
              },
              {
                title: s_("JH|BillingPlans|5GB storage per project")
              }
            ]
          }
        }
        super.tap do |data|
          data.each do |k, v|
            v.merge!(plans.fetch(k, {}))
          end
        end
      end
    end
  end
end
