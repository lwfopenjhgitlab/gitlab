# frozen_string_literal: true

module JH
  module Emails
    module Profile
      def notice_password_is_expiring_email(user, expiration_date)
        @user = user
        @expiration_date = expiration_date
        @token = @user.set_reset_password_token

        email = @user.notification_email_or_default
        mail_with_locale to: email, subject: s_('JH|Password is expiring soon') do |format|
          format.html { render layout: 'mailer/devise' }
          format.text
        end
      end
    end
  end
end
