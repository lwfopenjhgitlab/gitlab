# frozen_string_literal: true

module JH
  module TrialRegistrations
    module ReassurancesHelper
      def reassurance_logo_data
        # Create the basic data structure for the logos we want to showcase
        data = [
          { name: 'Zhongzhixing', w: 12 },
          { name: 'guangfa_securities', w: 12 },
          { name: 'suiyuan_tech', w: 12 },
          { name: 'authine', w: 14 },
          { name: 'zhangyue_tech', w: 12 },
          { name: 'pegasus', w: 12 },
          { name: 'pwc', w: 14 }
        ]

        # Update each entry with a logo image path and alt text derived from the org's name
        data.each do |hash|
          hash[:image_path] = reassurance_logo_image_path(hash[:name])
          hash[:image_alt_text] = reassurance_logo_image_alt_text(hash[:name])
        end

        data
      end

      private

      def reassurance_logo_image_path(org_name)
        format('third_party_logos/%s.svg', org_name.parameterize)
      end

      def reassurance_logo_image_alt_text(org_name)
        format(s_('InProductMarketing|%{organization_name} logo'), organization_name: org_name)
      end
    end
  end
end
