# frozen_string_literal: true

module JH
  module AppearancesHelper
    extend ::Gitlab::Utils::Override

    override :brand_image
    def brand_image
      image_tag(brand_image_path, alt: brand_title, class: ::Gitlab.com? ? 'gl-w-15' : 'gl-w-10')
    end
  end
end
