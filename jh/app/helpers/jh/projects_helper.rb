# frozen_string_literal: true

module JH
  module ProjectsHelper
    extend ::Gitlab::Utils::Override

    override :project_permissions_panel_data
    def project_permissions_panel_data(project)
      panel_data = super(project)
      panel_data[:pagesAccessControlEnabled] = false if ::Gitlab.com? && ::Gitlab::Pages.access_control_is_forced?

      panel_data
    end
  end
end
