# frozen_string_literal: true

module JH
  module LicenseHelper
    extend ::Gitlab::Utils::Override

    override :new_trial_url
    def new_trial_url
      ::Gitlab::SubscriptionPortal.free_trial_url
    end
  end
end
