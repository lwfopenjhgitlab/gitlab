# frozen_string_literal: true

module JH
  module AuthHelper
    extend ::Gitlab::Utils::Override

    JH_POPULAR_PROVIDERS = %w[github gitlab dingtalk alicloud cas3].freeze

    override :enabled_button_based_providers
    def enabled_button_based_providers
      return super unless ::Gitlab.com? && ::Gitlab.jh?

      super.sort_by do |provider|
        JH_POPULAR_PROVIDERS.index(provider) || JH_POPULAR_PROVIDERS.length
      end
    end

    override :popular_enabled_button_based_providers
    def popular_enabled_button_based_providers
      return super unless ::Gitlab.com? && ::Gitlab.jh?

      enabled_button_based_providers & JH_POPULAR_PROVIDERS
    end
  end
end
