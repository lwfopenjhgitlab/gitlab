# frozen_string_literal: true

module JH
  module NamespacesHelper
    extend ::Gitlab::Utils::Override

    override :buy_additional_minutes_path
    def buy_additional_minutes_path(namespace)
      if ::Feature.enabled?(:jh_new_purchase_flow)
        super(namespace)
      else
        ::Gitlab::Routing.url_helpers.subscription_portal_more_minutes_url
      end
    end

    override :buy_storage_path
    def buy_storage_path(namespace)
      if ::Feature.enabled?(:jh_new_purchase_flow)
        super(namespace)
      else
        ::Gitlab::Routing.url_helpers.subscription_portal_more_storage_url
      end
    end
  end
end
