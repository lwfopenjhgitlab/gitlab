import Vue from 'vue';
import NewFields from 'jh/issues/components/new_fields.vue';

export function addNewFieldsOnForm() {
  if (!(gon.jh_custom_labels_enabled && gon.jh_custom_labels?.issue?.length > 0)) {
    return;
  }
  const assigneeRow = document.querySelector('.merge-request-assignee');

  if (!assigneeRow) {
    return;
  }

  const originalFirstCol = assigneeRow.parentNode;
  const metaDataRow = originalFirstCol.parentNode;
  const labelSelector = document.querySelector('.js-label-select');
  const isOriginTwoCols = originalFirstCol.classList.contains('col-lg-6');
  if (isOriginTwoCols) {
    const originalSecondCol = originalFirstCol.nextElementSibling;

    originalFirstCol.classList.replace('col-lg-6', 'col-lg-4');
    originalSecondCol.classList.replace('col-lg-6', 'col-lg-4');
  } else {
    originalFirstCol.classList.replace('col-12', 'col-lg-6');
  }

  const newFieldsRoot = metaDataRow.insertBefore(document.createElement('div'), originalFirstCol);

  // eslint-disable-next-line no-new
  new Vue({
    el: newFieldsRoot,
    render(createElement) {
      return createElement(NewFields, {
        props: {
          labelsApi: labelSelector.dataset.labels,
          customLabels: gon.jh_custom_labels.issue,
          rootClassNames: isOriginTwoCols ? ['col-lg-4'] : ['col-lg-6'],
        },
      });
    },
  });
}
