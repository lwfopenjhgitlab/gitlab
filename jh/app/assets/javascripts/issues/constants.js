import { __, s__ } from '~/locale';

export const i18n = {
  REQUIRED: __('(required)'),
  FETCH_LABELS_ERROR_MESSAGE: __('There was an error fetching label data for the selected group'),
  REQUIRED_FIELD_ERROR_MESSAGE: __('This field is required'),
  NO_LABELS: s__('JH|CustomLabels|No related labels found'),
  SELECT_SCOPE: s__(`JH|CustomLabels|Select %{scope}`),
};

export const SCOPE_SEPARATOR = '::';
