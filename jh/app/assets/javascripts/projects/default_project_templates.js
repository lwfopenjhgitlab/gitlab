import { s__ } from '~/locale';
import EE_TEMPLATES from 'ee/projects/default_project_templates';

export default {
  ...EE_TEMPLATES,
  dongtai_iast: {
    text: s__('JH|ProjectTemplates|DongTai IAST'),
    icon: '.template-option .icon-dongtai_iast',
  },
};
