import 'ee/pages/registrations/new';
import EmailValidator from 'jh/pages/sessions/new/email_validator';
import { initRegistration } from 'jh/registration';

const { dot_com: dotCom, phone_registration: phoneRegistration } = window.gon;
if (dotCom && phoneRegistration) {
  initRegistration();
} else {
  new EmailValidator(); // eslint-disable-line no-new
}
