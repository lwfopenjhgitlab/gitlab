import initOnesIssuesList from 'jh/integrations/ones/issues_list/ones_issues_list_bundle';

initOnesIssuesList({ mountPointSelector: '.js-ones-issues-list' });
