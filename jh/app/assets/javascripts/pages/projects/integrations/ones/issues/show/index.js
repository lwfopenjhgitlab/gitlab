import initOnesIssueShow from 'jh/integrations/ones/issues_show/ones_issues_show_bundle';

initOnesIssueShow({ mountPointSelector: '.js-ones-issues-show-app' });
