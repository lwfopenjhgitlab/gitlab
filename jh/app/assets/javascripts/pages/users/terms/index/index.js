import { initTermsApp } from 'jh/terms';
import { waitForCSSLoaded } from '~/helpers/startup_css_helper';

waitForCSSLoaded(initTermsApp);
