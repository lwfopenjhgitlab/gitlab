import { s__ } from '~/locale';

export const GREETING_MESSAGE = s__(
  'JH|RealName|Thank you for using Jihu GitLab, please verify your phone',
);
export const REGULATIONS = s__(
  'JH|RealName|Based on the regulation and the law, using internet services requires identity verification, by fill in your phone number to continue using Jihu GitLab.',
);
