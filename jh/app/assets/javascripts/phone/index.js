import Vue from 'vue';
import { convertObjectPropsToCamelCase } from '~/lib/utils/common_utils';
import PhoneVerificationApp from './app.vue';

export const initPhoneVerification = () => {
  const mountEl = document.querySelector('#js-phone-verification-show');
  const paths = convertObjectPropsToCamelCase(JSON.parse(mountEl.dataset.paths), { deep: true });
  // eslint-disable-next-line no-new
  new Vue({
    el: mountEl,
    provide: {
      paths,
    },
    render(h) {
      return h(PhoneVerificationApp);
    },
  });
};
