import './vue_overrides';
import { addAppealModalPopupListener } from 'jh/appeal';
import { initCaptcha } from 'jh/captcha/init_captcha';
import { initUserTracking, initDefaultTrackers } from 'jh/tracking';

function deferredInitialisation() {
  initDefaultTrackers();
}

if (window.posthog) {
  // set window.snowplow to a noop function, avoid upstream snowplow bootstrapping.
  window.snowplow = () => undefined;
  window.snowplowOptions = {};
  initUserTracking();

  requestIdleCallback(deferredInitialisation);
}

addAppealModalPopupListener();

// init captcha button if there exists a .js-captcha stub element in HTML
initCaptcha();
