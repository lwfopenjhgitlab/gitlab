# frozen_string_literal: true

module JH
  module Types
    module Repository
      module BlobType
        extend ActiveSupport::Concern
        extend ::Gitlab::Utils::Override
        include ContentValidationMessages

        prepended do
          remove_field :raw_text_blob
          field :raw_text_blob, GraphQL::Types::String, calls_gitaly: true, null: true, method: :text_only_data,
            description: 'Raw content of the blob, if the blob is text data.'
        end

        override :raw_text_blob
        def raw_text_blob
          return super unless ::ContentValidation::Setting.block_enabled?(object.project)

          commit_sha = ::Gitlab::Git::Commit.last_for_path(object.repository, object.commit_id, object.path,
            literal_pathspec: true).sha
          content_blocked_state = ::ContentValidation::ContentBlockedState.find_by_container_commit_path(
            object.project, commit_sha, object.path)
          return super if content_blocked_state.blank?

          illegal_tips_with_appeal_email
        end
      end
    end
  end
end
