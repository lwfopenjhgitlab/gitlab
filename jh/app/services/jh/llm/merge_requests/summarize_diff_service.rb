# frozen_string_literal: true

module JH
  module Llm
    module MergeRequests
      module SummarizeDiffService
        extend ActiveSupport::Concern
        extend ::Gitlab::Utils::Override

        private

        def jh_provider?
          ::Gitlab::Llm::ClientFactory.jh_provider?
        end

        def prompt
          return super unless jh_provider?

          "以上是合并请求的修改。合并请求的标题是: '#{title}'\n\n" \
            "你要扮演资深工程师，请使用非技术人员也能理解的简单的术语，总结以上合并请求的修改。"
        end

        override :response_modifier

        def response_modifier
          return super unless jh_provider?

          ::Gitlab::Llm::ClientFactory.response_modifier
        end

        override :response

        def response
          return super unless jh_provider?

          ::Gitlab::Llm::ClientFactory.client.new(user).chat(**chat_params)
        end

        def chat_params
          case ::Gitlab::Llm::ClientFactory.ai_provider
          when :chat_glm
            { content: summary_message, moderated: true }
          when :mini_max
            { messages: [{
              "sender_type" => "USER",
              "sender_name" => "user",
              "text" => summary_message
            }] }
          else
            {}
          end
        end
      end
    end
  end
end
