# frozen_string_literal: true

module JH
  module MergeRequests
    module RebaseService
      extend ActiveSupport::Concern
      extend ::Gitlab::Utils::Override

      override :execute
      def execute(merge_request, skip_ci: false)
        return super unless ::Feature.enabled?(:rebase_without_pipeline_and_status_check)

        super.tap do |result|
          create_status_check_skip_ci_records! if skip_ci && result[:status] == :success
        end
      end

      private

      def create_status_check_skip_ci_records!
        status_checks = merge_request.project.external_status_checks.applicable_to_branch(merge_request.target_branch)
        return if status_checks.empty?

        records = status_checks.map do |rule|
          merge_request.status_check_responses.new(
            external_status_check: rule,
            sha: merge_request.rebase_commit_sha,
            status: 'skipped')
        end
        ::MergeRequests::StatusCheckResponse.bulk_insert!(records)
      end
    end
  end
end
