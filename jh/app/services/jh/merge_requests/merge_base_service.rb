# frozen_string_literal: true

module JH
  module MergeRequests
    module MergeBaseService
      extend ::Gitlab::Utils::Override

      private

      override :squash_sha!
      def squash_sha!
        return super unless ::Feature.enabled?(:single_squash_merge_ff, project)
        return super unless project.merge_method == :single_squash_merge

        result = ::MergeRequests::SingleSquashService.new(
          merge_request: merge_request, current_user: current_user, commit_message: params[:squash_commit_message]
        ).execute

        raise ::MergeRequests::MergeService::MergeError, result[:message] unless result[:status] == :success

        result[:squash_sha]
      end
    end
  end
end
