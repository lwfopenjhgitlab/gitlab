# frozen_string_literal: true

module JH
  module MergeRequests
    module Llm
      module SummarizeMergeRequestWorker
        def perform(user_id, params = {})
          return super unless ::Gitlab::Llm::ClientFactory.jh_provider?

          params = params.with_indifferent_access
          @user = ::User.find_by_id(user_id) # rubocop:disable Gitlab/ModuleWithInstanceVariables
          return unless user

          # We removed a param, and added the params, so need to handle the case
          # when type is not given
          if params['type'] == self.class::SUMMARIZE_QUICK_ACTION || params['type'].nil?
            merge_request = MergeRequest.find_by_id(params['merge_request_id'])
            return unless merge_request

            project = merge_request.project

            return unless user.can?(:create_note, project)

            summary = service(title: merge_request.title, diff: merge_request.merge_request_diff).execute
            return unless summary

            opts = {
              note: note_content(merge_request, summary),
              noteable_type: 'MergeRequest',
              noteable_id: merge_request.id
            }

            # Create the note, but attribute it to the LLM bot
            #
            ::Notes::CreateService.new(project, llm_service_bot, opts).execute
          elsif params['type'] == self.class::PREPARE_DIFF_SUMMARY
            diff = MergeRequestDiff.find_by_id(params['diff_id'])
            return unless diff

            summary = service(title: diff.merge_request.title, diff: diff).execute

            return unless summary

            ::MergeRequest::DiffLlmSummary.create!(merge_request_diff: diff,
              content: summary,
              provider: ::MergeRequest::DiffLlmSummary.providers[::Gitlab::Llm::ClientFactory.ai_provider])
          end
        end
      end
    end
  end
end
