# frozen_string_literal: true

module UserResourceKey
  private

  def user_resource_key
    @user_resource_key ||= if params[:new_user]
                             :new_user
                           else
                             :user
                           end
  end
end
