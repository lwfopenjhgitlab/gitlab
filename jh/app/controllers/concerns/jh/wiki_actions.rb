# frozen_string_literal: true

module JH
  module WikiActions
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override
    include ContentValidationMessages

    private

    override :send_wiki_file_blob
    def send_wiki_file_blob(wiki, file_blob)
      return super unless ::ContentValidation::Setting.block_enabled?(wiki)

      commit = wiki.repository.commit(wiki.default_branch)
      content_blocked_state = ::ContentValidation::ContentBlockedState.find_by_container_commit_path(
        wiki,
        commit&.id,
        file_blob.path)

      return super unless content_blocked_state.present?

      render plain: illegal_tips_with_appeal_email
    end
  end
end
