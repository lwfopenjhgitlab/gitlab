# frozen_string_literal: true

module JH
  module PreferredLanguageSwitcher
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    include ActionController::Cookies

    private

    override :preferred_language
    def preferred_language
      default_preferred_language =
        ::Feature.enabled?(:qa_enforce_locale_to_en) ? 'en' : ::Gitlab::CurrentSettings.default_preferred_language

      cookies[:preferred_language].presence_in(::Gitlab::I18n.available_locales) || default_preferred_language
    end
  end
end
