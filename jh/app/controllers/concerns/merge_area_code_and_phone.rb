# frozen_string_literal: true

module MergeAreaCodeAndPhone
  include UserResourceKey

  private

  # e.g: "+86" + "15612341234" = "+8615612341234"
  def merge_area_code_and_phone
    return if params[:phone].start_with?("+")

    params[:phone] = params[:area_code] + params[:phone] if params[:area_code]
  end

  def merge_area_code_and_phone_with_user
    return if params[user_resource_key][:phone].start_with?("+") || params[user_resource_key][:area_code].blank?

    params[user_resource_key][:original_phone] = params[user_resource_key][:phone]
    params[user_resource_key][:phone] = params[user_resource_key][:area_code] + params[user_resource_key][:phone]
  end
end
