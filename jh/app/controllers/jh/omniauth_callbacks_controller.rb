# frozen_string_literal: true

module JH
  module OmniauthCallbacksController
    extend ::Gitlab::Utils::Override

    prepended do
      protect_from_forgery except: [:cas3, :failure] + ::AuthHelper.saml_providers, with: :exception, prepend: true
    end

    def cas3
      ticket = params['ticket']
      handle_service_ticket oauth['provider'], ticket if ticket

      handle_omniauth
    end

    private

    def handle_service_ticket(provider, ticket)
      ::Gitlab::Auth::OAuth::Session.create provider, ticket
      session[:service_tickets] ||= {}
      session[:service_tickets][provider] = ticket
    end
  end
end
