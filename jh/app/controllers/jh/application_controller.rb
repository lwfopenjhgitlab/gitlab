# frozen_string_literal: true

module JH
  module ApplicationController
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    prepended do
      before_action :validate_user_service_ticket!
      before_action :enforce_phone_verification!, if: :should_enforce_phone_verification?
      helper_method :gitee_import_enabled?
    end

    override :require_email
    def require_email
      return unless user_with_temp_email?

      if current_user.require_email_skippable?
        flash[:notice] = format(s_('JH|Please complete your profile with email address before %{expires_at}'),
          expires_at: current_user.phone_registration_experience_expires_at.strftime('%F %H:%M %:z'))
      else
        super
      end
    end

    override :required_signup_info
    def required_signup_info
      return if ::Gitlab::RealNameSystem.enabled? && devise_controller?

      super
    end

    override :check_password_expiration
    def check_password_expiration
      return if session[:impersonator_id] || !current_user&.allow_password_authentication?

      if current_user&.jh_password_expired?
        redirect_to new_profile_password_path, notice: s_('JH|Your password has expired.')
      end

      redirect_to new_profile_password_path if current_user&.password_expired?
    end

    def gitee_import_enabled?
      ::Gitlab::CurrentSettings.import_sources.include?('gitee')
    end

    override :route_not_found
    def route_not_found
      return super unless ::Gitlab.com?
      return super if Gitlab.hk?

      if current_user || browser.bot.search_engine?
        not_found
      else
        store_location_for(:user, request.fullpath) unless request.xhr?

        unauthorized!
      end
    end

    def should_redirect_to_home_page?
      ::Gitlab.com? && !Gitlab.hk? && !user_signed_in? && ::Gitlab::CurrentSettings.home_page_url.present?
    end

    def validate_user_service_ticket!
      return unless signed_in? && session[:service_tickets]

      valid = session[:service_tickets].all? do |provider, ticket|
        ::Gitlab::Auth::OAuth::Session.valid?(provider, ticket)
      end

      return if valid

      session[:service_tickets] = nil
      sign_out current_user
      redirect_to new_user_session_path
    end

    private

    def unauthorized!
      render_401
    end

    def render_401
      respond_to do |format|
        format.html { render template: "errors/unauthorized", formats: :html, layout: "errors", status: :unauthorized }
        format.any { head :unauthorized }
      end
    end

    def user_with_temp_email?
      current_user && current_user.temp_oauth_email? && session[:impersonator_id].nil?
    end

    def enforce_phone_verification!
      return unless current_user
      return unless current_user.phone_required?
      return if current_user.phone_present?

      message = s_("JH|RealName|Please verify your phone before continuing.")

      if sessionless_user?
        access_denied!(message)
      else
        redirect_to phone_path, status: :found
      end
    end

    def should_enforce_phone_verification?
      return false unless ::Gitlab::RealNameSystem.enabled?

      html_request? && !devise_controller?
    end
  end
end
