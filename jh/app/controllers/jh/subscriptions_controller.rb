# frozen_string_literal: true

module JH
  module SubscriptionsController
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    override :create
    def create
      current_user.update(setup_for_company: true) if params[:setup_for_company]
      group = params[:selected_group] ? current_group : create_group

      return not_found if group.nil?

      unless group.persisted?
        track_purchase message: group.errors.full_messages.to_s
        return render json: ::Gitlab::Json.generate(group.errors)
      end

      response = ::GitlabSubscriptions::CreateService.new(
        current_user,
        group: group,
        customer_params: customer_params,
        subscription_params: subscription_params
      ).execute

      if response[:success]
        track_purchase message: track_success_message, namespace: group

        response[:data] = { location: redirect_location_for_jh(group, response) }
      else
        track_purchase message: response.dig(:data, :errors), namespace: group
      end

      render json: response[:data]
    end

    def redirect_location_for_jh(group, response)
      resp_data = response.with_indifferent_access['data']
      pay_url = resp_data if resp_data.is_a?(String)
      pay_url ||= resp_data.dig('invoice_data', 'invoice', 'pay_url') if resp_data.respond_to?(:dig)
      pay_url ||= resp_data['pay_url'] if resp_data.respond_to?(:dig)

      if pay_url
        callback_url = ::Gitlab::Utils.append_path(::Gitlab.config.gitlab.url, redirect_location(group))
        "#{pay_url}?redirect_after_success=#{CGI.escape(callback_url)}"
      else
        redirect_location(group)
      end
    end
  end
end
