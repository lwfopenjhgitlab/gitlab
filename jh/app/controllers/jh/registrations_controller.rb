# frozen_string_literal: true

module JH
  module RegistrationsController
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    include PhoneRegistrationsHelper
    include CheckPhoneAndCode

    prepended do
      prepend_before_action :set_phone_resource_fields, only: [:create], if: :registration_by_phone?
      before_action :verify_code_received_by_phone, only: [:create], if: :registration_by_phone?
    end

    private

    override :skip_confirmation?
    def skip_confirmation?
      registered_with_invite_email? || registration_by_phone?
    end

    def set_phone_resource_fields
      params[user_resource_key][:first_name] = params[user_resource_key][:username]
      params[user_resource_key][:last_name] = 'User'
      params[user_resource_key][:email] = temporarily_email
    end

    def temporarily_email
      "temp-email-for-phone-#{SecureRandom.uuid}@gitlab.localhost"
    end

    def verify_code_received_by_phone
      message = check_verification_code

      return unless message

      set_user_original_phone(resource)
      flash[:alert] = message

      render :new, registration_type: registration_type
    end

    override :sign_up_params_attributes
    def sign_up_params_attributes
      super.tap do |attributes|
        attributes << [:phone] if registration_by_phone?
      end
    end

    def set_user_original_phone(new_user)
      new_user.phone = params.dig(:user, :original_phone) if new_user&.new_record?
    end

    override :after_request_hook
    def after_request_hook(new_user)
      super

      set_user_original_phone(new_user)

      ::Gitlab::Tracking.event('Register_Submit', 'User_Register',
        username: new_user.username,
        email: new_user.email,
        name: new_user.name,
        first_name: new_user.first_name,
        last_name: new_user.last_name)
    end
  end
end
