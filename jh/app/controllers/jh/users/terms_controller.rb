# frozen_string_literal: true

module JH
  module Users
    module TermsController
      extend ActiveSupport::Concern
      extend ::Gitlab::Utils::Override
      include CheckPhoneAndCode

      prepended do
        skip_before_action :required_signup_info # term check first, then user role check in JH
        skip_before_action :enforce_phone_verification!
      end
    end
  end
end
