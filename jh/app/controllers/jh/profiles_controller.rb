# frozen_string_literal: true

module JH
  module ProfilesController
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override
    include CheckPhoneAndCode

    prepended do
      before_action :verify_code_received_by_phone, only: [:update], if: :verify_code_received_by_phone?
      before_action :set_empty_phone_param_to_nil, only: [:update], if: -> { user.skip_real_name_verification? }
      skip_before_action :required_signup_info, if: -> { ::Gitlab::RealNameSystem.enabled? }
    end

    override :show
    def show
      @user.phone = ::Gitlab::CryptoHelper.aes256_gcm_decrypt(@user.phone)

      super
    end

    private

    override :user_params_attributes
    def user_params_attributes
      if ::Gitlab::RealNameSystem.enabled?
        super + [:phone, :verification_code, :area_code]
      else
        super
      end
    end

    def verify_code_received_by_phone
      check_verification_code
    end

    def verify_code_received_by_phone?
      ::Gitlab::RealNameSystem.enabled? && params[:user][:phone].present?
    end

    def set_empty_phone_param_to_nil
      # In postgreSQL, the `''` and the `nil` are different values.
      # For a unique index, multiple `nil` are allowed, but multiple `''` will raise a duplicate key error.
      # Users who skip real name verification are allowed to set `phone` to be empty. Convert blank to nil.
      params[:user][:phone] = nil if params[:user].key?(:phone) && params[:user][:phone].blank?
    end
  end
end
