# frozen_string_literal: true

module JH
  module TrialsController
    extend ::Gitlab::Utils::Override

    private

    override :lead_params
    def lead_params
      super.merge(utm_params)
    end

    override :trial_params
    def trial_params
      super.merge(utm_params)
    end
  end
end
