# frozen_string_literal: true

module JH
  module Registrations
    module WelcomeController
      extend ::Gitlab::Utils::Override

      override :show
      def show
        unless completed_welcome_step?
          ::Gitlab::Tracking.event('Welcome_Page', 'User_Register', \
            user: current_user, email: current_user.email)
        end

        super
      end

      protected

      # override the ConfirmEmailWarning method in order to skip
      def show_confirm_warning?
        return super unless ::Gitlab.jh? && ::Gitlab.com?

        false
      end
    end
  end
end
