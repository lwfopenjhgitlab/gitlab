# frozen_string_literal: true

module JH
  module MergeRequests
    module StatusCheckResponse
      extend ActiveSupport::Concern

      prepended do
        include ::BulkInsertSafe

        enum status: { passed: 0, failed: 1, skipped: 9 }, _prefix: :jh
      end
    end
  end
end
