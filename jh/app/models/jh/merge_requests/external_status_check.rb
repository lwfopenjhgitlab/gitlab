# frozen_string_literal: true

module JH
  module MergeRequests
    module ExternalStatusCheck
      extend ActiveSupport::Concern
      extend ::Gitlab::Utils::Override

      override :status
      def status(merge_request, sha)
        return super unless ::Feature.enabled?(:rebase_without_pipeline_and_status_check)

        last_response = response_for(merge_request, sha)
        return 'pending' if !last_response || !last_response.retried_at.nil?
        return 'pending' if last_response.status.nil?
        return last_response.status unless last_response.status == 'skipped'

        merge_request.status_check_responses.where.not(status: 'skipped')
          .where(external_status_check: self).last&.status || 'pending'
      end
    end
  end
end
