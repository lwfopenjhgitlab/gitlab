# frozen_string_literal: true

module JH
  module Namespace
    extend ::Gitlab::Utils::Override
    include ::Gitlab::Utils::StrongMemoize

    override :actual_size_limit
    def actual_size_limit
      return super unless ::Gitlab.jh? && ::Gitlab.com?
      return super if actual_limits.repository_size == 0

      actual_limits.repository_size
    end

    # rubocop:disable Gitlab/StrongMemoizeAttr
    override :licensed_feature_available?
    def licensed_feature_available?(feature)
      available_features = strong_memoize(:jh_licensed_feature_available) do
        Hash.new do |h, f|
          h[f] = load_feature_available(f)
        end
      end

      available_features[feature]
    end
    # rubocop:enable Gitlab/StrongMemoizeAttr

    override :clear_feature_available_cache
    def clear_feature_available_cache
      clear_memoization(:jh_licensed_feature_available)

      super
    end
  end
end
