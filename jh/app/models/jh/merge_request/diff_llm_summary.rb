# frozen_string_literal: true

module JH
  module MergeRequest
    module DiffLlmSummary # rubocop:disable Style/ClassAndModuleChildren
      extend ActiveSupport::Concern

      prepended do
        enum provider: { open_ai: 0, vertex_ai: 1, chat_glm: 100, mini_max: 101 }, _prefix: :jh
      end
    end
  end
end
