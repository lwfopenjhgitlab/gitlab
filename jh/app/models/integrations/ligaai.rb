# frozen_string_literal: true

module Integrations
  class Ligaai < Integration
    include Gitlab::Routing
    include IssueTrackerLimitation

    prop_accessor :url, :api_url, :project_key, :user_key, :api_token

    validates :url, public_url: true, presence: true, if: :activated?
    validates :api_url, public_url: true, allow_blank: true
    validates :project_key, presence: true, if: :activated?
    validates :user_key, presence: true, if: :activated?
    validates :api_token, presence: true, if: :activated?

    class << self
      def to_param
        'ligaai'
      end

      def supported_events
        %w[]
      end

      def issues_license_available?(project)
        project&.licensed_feature_available?(:integration_with_ligaai_issues)
      end
    end

    def activated?
      active
    end

    def title
      s_('JH|LigaaiIntegration|LigaAI')
    end

    def description
      s_("JH|LigaaiIntegration|Use LigaAI as this project's issue tracker.")
    end

    def help
      help_text = <<~TEXT.strip
        JH|LigaaiIntegration|\
        Before you enable this integration, you must configure LigaAI. \
        For more details, read the %{link_start}LigaAI integration documentation%{link_end}.
      TEXT

      format(
        s_(help_text),
        link_start: format('<a href="%{url}" target="_blank" rel="noopener noreferrer">'.html_safe,
          url: help_page_url('user/project/integrations/ligaai')),
        link_end: '</a>'.html_safe)
    end

    def render?
      return false unless ::Feature.enabled?(:integration_with_ligaai_issues, project)

      valid? && activated?
    end

    def test(*_args)
      ::Gitlab::Ligaai::Client.new(self).ping
    end

    def fields
      LigaaiFields.fields
    end

    def issue_tracker_path
      url
    end
  end
end
