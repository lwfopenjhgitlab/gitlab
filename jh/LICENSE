The JiHu Edition License (the "JH License")
Copyright (c) 2021-present GitLab Information Technology (Hubei) Co., Ltd.

With regard to the JiHu Software:

This software and associated documentation files (the "Software") may only be
used in production, if you (and any entity that you represent) have agreed to,
and are in compliance with, the Subscription Terms of Service, available at
https://about.gitlab.cn/terms/#subscription, or other agreement governing the
use of the Software, as agreed by you and JiHu, and otherwise have a valid
JiHu subscription for the correct number of user seats. Subject to the
foregoing sentence, you are free to modify this Software and publish patches
to the Software. You agree that JiHu and/or its licensors (as applicable)
retain all right, title and interest in and to all such modifications and/or
patches, and all such modifications and/or patches may only be used, copied,
modified, displayed, distributed or otherwise exploited with a valid JiHu
subscription for the correct number of user seats. Notwithstanding the
foregoing, you may copy and modify the Software for development and testing
purposes, without requiring a subscription. You agree that JiHu and/or its
licensors (as applicable) retain all right, title and interest in and to all
such modifications. You are not granted any other rights beyond what is
expressly stated herein. Subject to the foregoing, it is forbidden to copy,
merge, publish, distribute, sublicense, and/or sell the Software.

This JH License applies only to the part of this Software that is not
distributed as part of GitLab Community Edition (CE) and GitLab Enterprise
Edition (EE). Any part of this Software distributed as part of GitLab CE or
is served client-side as an image, font, cascading stylesheet (CSS), file
which produces or is compiled, arranged, augmented, or combined into
client-side JavaScript, in whole or in part, is copyrighted under the MIT
Expat license. Any part of this Software distributed as part of GitLab EE is
copyrighted under the GitLab EE license. Any copyright notices of JiHu and/or
its licensors (as applicable) shall not be removed, altered or obscured.
The full text of this JH License shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

For all third party components incorporated into the Software, those components
are licensed under the original license provided by the owner of the applicable
component.
