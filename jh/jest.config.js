const path = require('path');
const baseConfig = require('../jest.config.base');
const SKIP_CONFIG = require('./spec/frontend/skip_list');

const ROOT_PATH = path.resolve(__dirname, '..');

const config = { ...baseConfig('spec/frontend') };

function generateSkipList(globalConfig) {
  let globalIgnoreTestList = Array.isArray(globalConfig.testPathIgnorePatterns)
    ? globalConfig.testPathIgnorePatterns
    : [];

  if (SKIP_CONFIG) {
    const skipListJH = SKIP_CONFIG.by_file;
    globalIgnoreTestList = globalIgnoreTestList.concat(skipListJH);
  }

  return globalIgnoreTestList;
}

const ignoreTestList = generateSkipList(config);
(config.setupFiles ||= []).push(path.join(ROOT_PATH, 'jh/spec/frontend/setup/jh_setup.js'));
config.setupFilesAfterEnv.push(path.join(ROOT_PATH, 'jh/spec/frontend/setup/jh_test_setup.js'));

module.exports = {
  ...config,
  testPathIgnorePatterns: ignoreTestList,
};
